/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import jssc.SerialPortException;
import org.apache.http.client.HttpResponseException;
import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.EntradaPA.SdLotePa;
import sysdeliz2.models.sysdeliz.SdTela;
import sysdeliz2.models.sysdeliz.comercial.SdCatalogo;
import sysdeliz2.models.sysdeliz.comercial.SdItensListaFavCli;
import sysdeliz2.models.sysdeliz.comercial.SdListaFavoritosCliente;
import sysdeliz2.models.sysdeliz.comercial.SdListaFavoritosClientePK;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.apis.b2cEcom.ServicoB2CEcom;
import sysdeliz2.utils.apis.nitroecom.ServicoNitroEcom;
import sysdeliz2.utils.apis.nitroecom.models.ResponseItensFavoritos;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ExportUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.SceneLoginView;
import sysdeliz2.views.expedicao.*;
import sysdeliz2.views.expedicao.mostruario.RetornoMostruarioView;
import sysdeliz2.views.expedicao.mostruario.SaidaMostruarioView;
import sysdeliz2.views.pcp.almoxarifado.ColetaOfView;
import tornadofx.FX;
import tornadofx.FXKt;
import tornadofx.NodesKt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystemLoopException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SysDeliz2 extends Application {

    private static final Logger logger = Logger.getLogger(SysDeliz2.class);
    private static String paramLoad = "";
    private static boolean isTelaMinuta = false;
    private Scene scene = null;

    private void loadSysDeliz(Stage stage) throws IOException {
        stage.setTitle("SysDeliz 2");
        stage.initStyle(StageStyle.DECORATED);
        startTornadoFX(stage);
        scene.setRoot(new SceneLoginView());
    }

    private void loadApontamento(Stage stage) throws IOException {
        JPAUtils.getEntityManager();
        scene.setRoot(new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/pcp/shopfloor/ScenePcpLancamentoProducao.fxml")).load());
        stage.setTitle("Lançamento de Produção");
    }

    private void loadColetaAlmox(Stage stage) throws IOException {
        Globals.setPortatil(true);
        scene.setRoot(new ColetaOfView());
        stage.setTitle("Coleta de Materiais");
    }

    private void loadColetaExpedicao(Stage stage) throws IOException {
        scene.setRoot(new ColetaExpedicaoView());
        stage.setTitle("Coleta de Produtos");

//        stage.setMaximized(false);
//        stage.setHeight(780.0);
//        stage.setWidth(1250.0);
    }

    private void loadLeituraMostruario(Stage stage) throws IOException {
        Globals.setPortatil(true);
        scene.setRoot(new SaidaMostruarioView());
        stage.setTitle("Leitura de Mostruários");

//        stage.setMaximized(false);
//        stage.setHeight(780.0);
//        stage.setWidth(1250.0);
    }

    private void loadInventarioExpedicao(Stage stage) throws IOException {
        scene.setRoot(new InventarioExpedicaoView());
        stage.setTitle("Inventário");
    }

    private void loadReposicaoEstoque(Stage stage) throws IOException, SchedulerException {
        Globals.setPortatil(true);
        scene.setRoot(new ReposicaoEstoqueView());
        stage.setTitle("Coleta de Produtos");
    }

    private void loadFechamentoCaixaExpedicao(Stage stage) throws IOException {
        Globals.setPortatil(true);
        scene.setRoot(new FechamentoCaixaView());
        stage.setTitle("Coleta de Produtos");
    }

    private void loadRetornoMostruario(Stage stage) throws IOException {
        Globals.setPortatil(true);
        scene.setRoot(new RetornoMostruarioView());
        stage.setTitle("Retorno Mostruário");
    }

    private void loadMinutaExpedicao(Stage stage) throws IOException {
        Globals.setPortatil(true);
        scene.setRoot(new MinutaView());
        stage.setTitle("Coleta de Produtos");
    }

    private void loadKpi(Stage stage) throws IOException {
        JPAUtils.getEntityManager();
        scene.setRoot(new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/pcp/shopfloor/relatorios/SceneDashboardCostura.fxml")).load());
        stage.setTitle("Dashboard Costura");
    }

    private void loadEntradaProdutosAcabados(Stage stage) throws IOException {
        JPAUtils.getEntityManager();
        scene.setRoot(new EntradaProdutosAcabadosView());
        stage.setTitle("Entrada de Produtos Acabados");
    }

    private void loadTests(Stage stage) throws IOException, SerialPortException {

    }

    private void loadTestsDiego(Stage stage) {

    }

    @Override
    public void start(Stage stage) throws Exception {
        preLoad(stage);
        try {
            DAOFactory.getUsuariosDAO().getDadosUsuario("LOAD_ARGS");
            switch (paramLoad) {
                case "teste":
                    loadTests(stage);
                    break;
                case "diego":
                    loadTestsDiego(stage);
                    break;
                case "apontamento":
                    loadApontamento(stage);
                    break;
                case "kpi":
                    loadKpi(stage);
                    break;
                case "coleta-almox":
                    loadColetaAlmox(stage);
                    break;
                case "coleta-exped":
                    loadColetaExpedicao(stage);
                    break;
                case "rep-estoque":
                    loadReposicaoEstoque(stage);
                    break;
                case "fecha-cx-exped":
                    loadFechamentoCaixaExpedicao(stage);
                    break;
                case "minuta-exped":
                    loadMinutaExpedicao(stage);
                    break;
                case "entrada-pa":
                    loadEntradaProdutosAcabados(stage);
                    break;
                case "inventario":
                    loadInventarioExpedicao(stage);
                    break;
                case "ret_mostr":
                    loadRetornoMostruario(stage);
                    break;
                case "leitura_mostr":
                    loadLeituraMostruario(stage);
                    break;
                default:
                    loadSysDeliz(stage);
                    break;
            }
            if (stage.getScene().getRoot().getChildrenUnmodifiable().size() > 0)
                stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            logger.trace(e);
        }
    }

    private void preLoad(Stage stage) {
        Locale.setDefault(new Locale("pt", "BR"));
        Globals.setMainStage(stage);
        Globals.setNomeUsuario(System.getProperty("user.name"));

        scene = new Scene(new FormBox());
        scene.getStylesheets().add("/styles/bootstrap2.css");
        scene.getStylesheets().add("/styles/bootstrap3.css");
        scene.getStylesheets().add("/styles/styles.css");
        scene.getStylesheets().add("/styles/stylePadrao.css");
        scene.getStylesheets().add("/styles/sysDelizDesktop.css");

        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo deliz (4).png")));
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setOnCloseRequest((WindowEvent arg0) -> stage.close());

        stage.setMaximized(true);

        if (isTelaMinuta) {
            stage.setMaximized(false);
            stage.setHeight(290);
            stage.setWidth(240);
        } else if (paramLoad.equals("fullhd")) {
            stage.setMaximized(false);
            stage.setHeight(1080);
            stage.setWidth(1920);
        }
    }

    private void startTornadoFX(Stage stage) {
        // Apenas para o Tornado FX
        FX.registerApplication(this, stage);
        NodesKt.hookGlobalShortcuts(stage);
        FX.Companion.setLayoutDebuggerShortcut(new KeyCodeCombination(KeyCode.J, KeyCodeCombination.META_DOWN, KeyCodeCombination.CONTROL_DOWN));
        FX.Companion.getStylesheets().add("/styles/bootstrap2.css");
        FX.Companion.getStylesheets().add("/styles/bootstrap3.css");
        FX.Companion.getStylesheets().add("/styles/styles.css");
        FX.Companion.applyStylesheetsTo(scene);
        FX.Companion.getInitialized().set(true);
        FXKt.setAboutToBeShown(stage, true);

        /*
        stage.apply {
                view.muteDocking = true
                scene = createPrimaryScene(view)
                view.properties["tornadofx.scene"] = scene
                titleProperty().bind(view.titleProperty)
                hookGlobalShortcuts()
                view.onBeforeShow()
                onBeforeShow(view)
                view.muteDocking = false
                // No need to call view.callOnDock() if show() is called since it
                // will be called by the stage's showingProperty listener
                if (view !is NoPrimaryViewSpecified && shouldShowPrimaryStage())
                    show()
                else
                    view.callOnDock()
                stage.aboutToBeShown = false
            }
         */
    }

    @Override
    public void stop() throws Exception {
        JPAUtils.destroy();
        super.stop();
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length >= 1)
            paramLoad = args[0];
        if (args.length == 2)
            isTelaMinuta = (args[1].equals("true"));
        launch(args);
    }
}
