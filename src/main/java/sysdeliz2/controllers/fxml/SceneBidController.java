/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.procura.SceneProcurarFamiliaController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarRepresentanteController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.*;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.sys.MailUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneBidController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    Integer larguraTela = 0;
    Boolean displayedBid = Boolean.FALSE;
    Boolean abrirCaixaEmail = Boolean.FALSE;
    Boolean abrirNovamente = Boolean.TRUE;
    Locale BRAZIL = new Locale("pt", "BR");
    private Image imgDlz = new Image(getClass().getResource("/images/LogoDLZ2019_OK-2.png").toExternalForm());
    private Image imgFlorDeLis = new Image(getClass().getResource("/images/FlordeLis_LOGO_2019.png").toExternalForm());
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis/Componentes FXML">
    @FXML
    private Button btnAtualizarDados;
    @FXML
    private Label lbUltimaAtualizacao;
    @FXML
    private Label lbQuartaColecao;
    @FXML
    private Button btnImprimirCidades;
    @FXML
    private Label lbJornada;
    @FXML
    private Button btnImprimirBidCompleto;
    @FXML
    private Button btnEnviarBid;
    @FXML
    private Button btnImprimirBid;
    @FXML
    private TextField tboxCodColecao;
    @FXML
    private Button btnProcurarColecao;
    @FXML
    private TextField tboxCodFamilia;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private Button btnProcurar;
    @FXML
    private TextField tboxCodRepresentante;
    @FXML
    private Button btnProcurarPresentante;
    @FXML
    private TextField clmAttMeta;
    @FXML
    private Button btnFecharJanela;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private SplitPane splitWindow;
    @FXML
    private TableView<Bid> tblRepresentantes;
    @FXML
    private Button btnShowBid;
    @FXML
    private TextField pmRpm;
    @FXML
    private Label pmAttMeta;
    @FXML
    private TextField pmLy;
    @FXML
    private TextField pmMta;
    @FXML
    private TextField pmRcm;
    @FXML
    private TextField pgRpm;
    @FXML
    private Label pgAttMeta;
    @FXML
    private TextField pgMta;
    @FXML
    private TextField pgRcm;
    @FXML
    private TextField pgLy;
    @FXML
    private Label prAttMeta;
    @FXML
    private TextField prRpm;
    @FXML
    private TextField prMta;
    @FXML
    private TextField prRcm;
    @FXML
    private TextField prLy;
    @FXML
    private Label cdAttMeta;
    @FXML
    private TextField cdMta;
    @FXML
    private TextField cdRcm;
    @FXML
    private TextField cdLy;
    @FXML
    private TextField cdRpm;
    @FXML
    private Label pvAttMeta;
    @FXML
    private TextField pvMta;
    @FXML
    private TextField pvRcm;
    @FXML
    private TextField pvLy;
    @FXML
    private TextField pvRpm;
    @FXML
    private Label kaAttMeta;
    @FXML
    private TextField kaMta;
    @FXML
    private TextField kaRcm;
    @FXML
    private TextField kaLy;
    @FXML
    private TextField kaRpm;
    @FXML
    private Label tfAttMeta;
    @FXML
    private TextField tfMta;
    @FXML
    private TextField tfRcm;
    @FXML
    private TextField tfLy;
    @FXML
    private TextField tfRpm;
    @FXML
    private TextField tpRcm;
    @FXML
    private TextField tpLy;
    @FXML
    private TextField tpRpm;
    @FXML
    private TextField tpMta;
    @FXML
    private Label tpAttMeta;
    @FXML
    private Label vpAttMeta;
    @FXML
    private TextField vpMta;
    @FXML
    private TextField vpRcm;
    @FXML
    private TextField vpLy;
    @FXML
    private TextField vpRpm;
    @FXML
    private Label ppAttMeta;
    @FXML
    private TextField ppMta;
    @FXML
    private TextField ppRcm;
    @FXML
    private TextField ppLy;
    @FXML
    private TextField ppRpm;
    @FXML
    private TableView<AtendimentoCidadeRep> tblSica;
    @FXML
    private TableColumn<AtendimentoCidadeRep, Integer> clmStatus;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmClasse;
    @FXML
    private TextField tpMtaBas;
    @FXML
    private TextField tfMtaBas;
    @FXML
    private TextField perTfBas;
    @FXML
    private TextField tpRcmBas;
    @FXML
    private TextField tfRcmBas;
    @FXML
    private TextField perTpBas;
    @FXML
    private TextField tpMtaDen;
    @FXML
    private TextField tfMtaDen;
    @FXML
    private TextField perTfDen;
    @FXML
    private TextField tpRcmDen;
    @FXML
    private TextField tfRcmDen;
    @FXML
    private TextField perTpDen;
    @FXML
    private TextField tpMtaCol;
    @FXML
    private TextField tfMtaCol;
    @FXML
    private TextField perTfCol;
    @FXML
    private TextField tpRcmCol;
    @FXML
    private TextField tfRcmCol;
    @FXML
    private TextField perTpCol;
    @FXML
    private TextField tpMtaCas;
    @FXML
    private TextField tfMtaCas;
    @FXML
    private TextField perTfCas;
    @FXML
    private TextField tpRcmCas;
    @FXML
    private TextField tfRcmCas;
    @FXML
    private TextField perTpCas;
    @FXML
    private TextField clnMta;
    @FXML
    private TextField clmReal;
    @FXML
    private TextField lyTfBas;
    @FXML
    private TextField lyTpBas;
    @FXML
    private TextField lyTfDen;
    @FXML
    private TextField lyTpDen;
    @FXML
    private TextField lyTfCol;
    @FXML
    private TextField lyTpCol;
    @FXML
    private TextField lyTfCas;
    @FXML
    private TextField lyTpCas;
    @FXML
    private TextField lyClnAtt;
    @FXML
    private ChoiceBox<String> cboxMarca;
    @FXML
    private Label lbTerceiraColecao;
    @FXML
    private Label lbSegundaColecao;
    @FXML
    private Label lbPrimeiraColecao;
    @FXML
    private TableView<BidPremiado> tblBidPremiado;
    @FXML
    private TableColumn<BidPremiado, String> clmIndicadorObrigatorio;
    @FXML
    private TableColumn<BidPremiado, Double> clmRealMeta;
    @FXML
    private VBox vboxBid;
    @FXML
    private TextField mostrRep;
    @FXML
    private Label lbCodRep;
    @FXML
    private Label lbRep;
    @FXML
    private Label lbRegiaoRep;
    @FXML
    private Label lbSemanaBid;
    @FXML
    private ImageView imgLogoMarca;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaTp;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealTp;
    @FXML
    private TableColumn<Bid, Double> clmBidAttTp;
    @FXML
    private TableColumn<Bid, Double> clmBidMtaTf;
    @FXML
    private TableColumn<Bid, Double> clmBidRealTf;
    @FXML
    private TableColumn<Bid, Double> clmBidAttTf;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaCli;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealCli;
    @FXML
    private TableColumn<Bid, Double> clmBidAttCli;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaCln;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealCln;
    @FXML
    private TableColumn<Bid, Double> clmBidAttCln;
    @FXML
    private TableColumn<Bid, Double> clmBidMtaPm;
    @FXML
    private TableColumn<Bid, Double> clmBidRealPm;
    @FXML
    private TableColumn<Bid, Double> clmBidAttPm;
    @FXML
    private Pane gridJornada;
    @FXML
    private GridPane gridPm;
    @FXML
    private GridPane gridPg;
    @FXML
    private GridPane gridPr;
    @FXML
    private GridPane gridCd;
    @FXML
    private GridPane gridPv;
    @FXML
    private GridPane gridKa;
    @FXML
    private GridPane gridTf;
    @FXML
    private GridPane gridTp;
    @FXML
    private GridPane gridVp;
    @FXML
    private GridPane gridPp;
    @FXML
    private Pane gridPerformance;
    @FXML
    private TableColumn<BidPremiado, Double> clmGraduacao;
    @FXML
    private TableColumn<BidPremiado, Double> clmRealizadoRep;
    @FXML
    private TableColumn<BidPremiado, Double> clmPercPremio;
    @FXML
    private TableColumn<BidPremiado, Double> clmPremio;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaPopulacao;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaCidade;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaIpca;
    @FXML
    private GridPane gridBasico;
    @FXML
    private GridPane gridDenim;
    @FXML
    private GridPane gridCollection;
    @FXML
    private GridPane gridCasual;
    @FXML
    private GridPane gridMostruario;
    @FXML
    private AnchorPane anchorBid;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tblRepresentantes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData(oldValue);
            try {
                bindData(newValue);
            } catch (SQLException ex) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension scrnsize = toolkit.getScreenSize();
        larguraTela = scrnsize.width;
        splitWindow.setDividerPositions(1.0);
        cboxMarca.setItems(FXCollections.observableArrayList("FLOR DE LIS", "DLZ"));
        try {
            String[] colecoes = DAOFactory.getColecaoDAO().getTresUltimasColecoes();
            lbPrimeiraColecao.setText(colecoes[0]);
            lbSegundaColecao.setText(colecoes[1]);
            lbTerceiraColecao.setText(colecoes[2]);
            lbQuartaColecao.setText(colecoes[3]);

        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        this.factoryColumns();
        btnShowBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        btnImprimirBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        btnImprimirBidCompleto.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        btnEnviarBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());

    }

    // <editor-fold defaultstate="collapsed" desc="Métodos Locais">
    private void factoryColumns() {

        clmClasse.setCellFactory((TableColumn<AtendimentoCidadeRep, String> param) -> {
            TableCell cell = new TableCell<AtendimentoCidadeRep, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    AtendimentoCidadeRep currentCidade = currentRow == null ? null : (AtendimentoCidadeRep) currentRow.getItem();
                    if (currentCidade != null) {
                        String status = currentCidade.getClasse();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "VV":
                            setStyle("-fx-background-color: #00FFFF; ");
                            break;
                        case "V":
                            setStyle("-fx-background-color: #00FFFF;");
                            break;
                        case "P":
                            setStyle("-fx-background-color: #FF8C00;");
                            break;
                        case "S":
                            setStyle("-fx-background-color: #9932CC;");
                            break;
                        case "E":
                            setStyle("-fx-background-color: #A9A9A9;");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmStatus.setCellFactory((TableColumn<AtendimentoCidadeRep, Integer> param) -> {
            TableCell cell = new TableCell<AtendimentoCidadeRep, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString() + "");
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    AtendimentoCidadeRep currentCidade = currentRow == null ? null : (AtendimentoCidadeRep) currentRow.getItem();
                    if (currentCidade != null) {
                        Integer status = currentCidade.getStatus_comp_col();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Integer priority) {
                    switch (priority) {
                        case 1:
                            setStyle("-fx-background-color: #00FF00; -fx-text-fill: #00FF00;");
                            break;
                        case 2:
                            setStyle("-fx-background-color: #FFFF00; -fx-text-fill: #FFFF00;");
                            break;
                        case 3:
                            setStyle("-fx-background-color: #FFA500; -fx-text-fill: #FFA500;");
                            break;
                        case 4:
                            setStyle("-fx-background-color: #FF0000; -fx-text-fill: #FF0000;");
                            break;
                    }
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmIndicadorObrigatorio.setCellFactory((TableColumn<BidPremiado, String> param) -> {
            TableCell cell = new TableCell<BidPremiado, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidPremiado currentReserva = currentRow == null ? null : (BidPremiado) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getObrigatorio();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "X":
                            getStyleClass().add("statusLido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmRealMeta.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidPremiado currentCidade = currentRow == null ? null : (BidPremiado) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getRel_meta();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority <= 79.999) {
                        setStyle("-fx-background-color: #FF0000; -fx-font-weight: bold;");
                    } else if (priority > 79.999 && priority < 99.999) {
                        setStyle("-fx-background-color: #FFFF00; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #32CD32; -fx-font-weight: bold;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmRealizadoRep.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmPercPremio.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmPremio.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidMtaTp.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidRealTp.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidMtaCli.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidRealCli.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidMtaCln.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidRealCln.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {

                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });

        clmBidAttTp.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getTp_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidAttTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getTf_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidAttPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getPm_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidAttCln.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getCln_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidAttCli.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getPv_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidMtaPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidRealPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidMtaTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clmBidRealTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
    }

    private void bindData(Bid selectedBid) throws SQLException {
        if (selectedBid != null) {
            NumberFormat z = NumberFormat.getIntegerInstance(BRAZIL);
            NumberFormat w = NumberFormat.getCurrencyInstance(BRAZIL);

            pmRcm.textProperty().bind(selectedBid.pm_rcmProperty().asString("%12.2f"));
            pmRpm.textProperty().bind(selectedBid.pm_rpmProperty().asString("%12.2f"));
            pmMta.textProperty().bind(selectedBid.pm_mtaProperty().asString("%12.2f"));
            pmLy.textProperty().bind(selectedBid.pm_lyProperty().asString("%12.2f"));
            pmAttMeta.textProperty().bind(selectedBid.pm_perProperty().asString());
            cdRcm.textProperty().bind(selectedBid.cd_rcmProperty().asString());
            cdRpm.textProperty().bind(selectedBid.cd_rpmProperty().asString());
            cdMta.textProperty().bind(selectedBid.cd_mtaProperty().asString());
            cdLy.textProperty().bind(selectedBid.cd_lyProperty().asString());
            cdAttMeta.textProperty().bind(selectedBid.cd_perProperty().asString());
            pvRcm.textProperty().bind(selectedBid.pv_rcmProperty().asString());
            pvRpm.textProperty().bind(selectedBid.pv_rpmProperty().asString());
            pvMta.textProperty().bind(selectedBid.pv_mtaProperty().asString());
            pvLy.textProperty().bind(selectedBid.pv_lyProperty().asString());
            pvAttMeta.textProperty().bind(selectedBid.pv_perProperty().asString());
            kaRcm.textProperty().bind(selectedBid.ka_rcmProperty().asString());
            kaRpm.textProperty().bind(selectedBid.ka_rpmProperty().asString());
            kaMta.textProperty().bind(selectedBid.ka_mtaProperty().asString());
            kaLy.textProperty().bind(selectedBid.ka_lyProperty().asString());
            kaAttMeta.textProperty().bind(selectedBid.ka_perProperty().asString());
            ppRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_rcmProperty().get())));
            ppRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_rpmProperty().get())));
            ppMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_mtaProperty().get())));
            ppLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_lyProperty().get())));
            ppAttMeta.textProperty().bind(selectedBid.pp_perProperty().asString());
            vpRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_rcmProperty().get())));
            vpRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_rpmProperty().get())));
            vpMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_mtaProperty().get())));
            vpLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_lyProperty().get())));
            vpAttMeta.textProperty().bind(selectedBid.vp_perProperty().asString());
            tpRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_rcmProperty().get())));
            tpRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_rpmProperty().get())));
            tpMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_mtaProperty().get())));
            tpLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_lyProperty().get())));
            tpAttMeta.textProperty().bind(selectedBid.tp_perProperty().asString());
            tfRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_rcmProperty().get())));
            tfRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_rpmProperty().get())));
            tfMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_mtaProperty().get())));
            tfLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_lyProperty().get())));
            tfAttMeta.textProperty().bind(selectedBid.tf_perProperty().asString());
            pgRcm.textProperty().bind(selectedBid.pg_rcmProperty().asString("%12.2f"));
            pgRpm.textProperty().bind(selectedBid.pg_rpmProperty().asString("%12.2f"));
            pgMta.textProperty().bind(selectedBid.pg_mtaProperty().asString("%12.2f"));
            pgLy.textProperty().bind(selectedBid.pg_lyProperty().asString("%12.2f"));
            pgAttMeta.textProperty().bind(selectedBid.pg_perProperty().asString());
            prRcm.textProperty().bind(selectedBid.pr_rcmProperty().asString("%12.2f"));
            prRpm.textProperty().bind(selectedBid.pr_rpmProperty().multiply(100).asString("%12.2f"));
            prMta.textProperty().bind(selectedBid.pr_mtaProperty().multiply(100).asString("%12.2f"));
            prLy.textProperty().bind(selectedBid.pr_lyProperty().asString("%12.2f"));
            prAttMeta.textProperty().bind(selectedBid.pr_perProperty().asString());
            clmReal.textProperty().bind(selectedBid.cln_rcmProperty().asString());
            clnMta.textProperty().bind(selectedBid.cln_mtaProperty().asString());
            clmAttMeta.textProperty().bind(selectedBid.cln_perProperty().asString("%4.2f"));
            lyClnAtt.textProperty().bind(selectedBid.cln_att_lyProperty().asString());
            mostrRep.textProperty().bind(selectedBid.mostrProperty().asString());

            tpRcmBas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.bas_tp_rcmProperty().get())));
            tpMtaBas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.bas_tp_mtaProperty().get())));
            perTpBas.textProperty().bind(selectedBid.per_bas_tpProperty().asString("%12.2f"));
            tfRcmBas.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.bas_tf_rcmProperty().get())));
            lyTpBas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.bas_tp_lyProperty().get())));
            lyTfBas.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.bas_tf_lyProperty().get())));

            tpRcmDen.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.den_tp_rcmProperty().get())));
            tpMtaDen.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.den_tp_mtaProperty().get())));
            perTpDen.textProperty().bind(selectedBid.per_den_tpProperty().asString("%12.2f"));
            tfRcmDen.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.den_tf_rcmProperty().get())));
            lyTpDen.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.den_tp_lyProperty().get())));
            lyTfDen.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.den_tf_lyProperty().get())));

            tpRcmCol.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.col_tp_rcmProperty().get())));
            tpMtaCol.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.col_tp_mtaProperty().get())));
            perTpCol.textProperty().bind(selectedBid.per_col_tpProperty().asString("%12.2f"));
            tfRcmCol.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.col_tf_rcmProperty().get())));
            lyTpCol.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.col_tp_lyProperty().get())));
            lyTfCol.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.col_tf_lyProperty().get())));

            tpRcmCas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.cas_tp_rcmProperty().get())));
            tpMtaCas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.cas_tp_mtaProperty().get())));
            perTpCas.textProperty().bind(selectedBid.per_cas_tpProperty().asString("%12.2f"));
            tfRcmCas.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.cas_tf_rcmProperty().get())));
            lyTpCas.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.cas_tp_lyProperty().get())));
            lyTfCas.textProperty().bind(new SimpleStringProperty(w.format(selectedBid.cas_tf_lyProperty().get())));

            lbCodRep.textProperty().bind(selectedBid.codrepProperty());
            lbRep.textProperty().bind(selectedBid.representProperty());
            lbRegiaoRep.textProperty().bind(selectedBid.regiaoProperty());
            lbSemanaBid.textProperty().bind(selectedBid.sem_hojeProperty().asString().concat("/").concat(selectedBid.sem_totalProperty().asString()));

            tblSica.setItems(DAOFactory.getBidDAO().getCidadesRep(selectedBid.codrepProperty().get(), selectedBid.codmarProperty().get()));

            if (selectedBid.getCodrep().equals("000") || selectedBid.getCodrep().equals("001")) {
                tblBidPremiado.getItems().clear();
            } else {
                tblBidPremiado.setItems(DAOFactory.getBidDAO().getBidPremiadoRep(selectedBid.codrepProperty().get(), selectedBid.codmarProperty().get(), selectedBid.codcolProperty().get()));
            }

            gridJornada.getStyleClass().clear();
            gridCd.getStyleClass().clear();
            gridPg.getStyleClass().clear();
            gridPm.getStyleClass().clear();
            gridPr.getStyleClass().clear();
            gridPv.getStyleClass().clear();
            lbJornada.getStyleClass().clear();

            gridPerformance.getStyleClass().clear();
            gridKa.getStyleClass().clear();
            gridPp.getStyleClass().clear();
            gridTf.getStyleClass().clear();
            gridTp.getStyleClass().clear();
            gridVp.getStyleClass().clear();
            gridBasico.getStyleClass().clear();
            gridDenim.getStyleClass().clear();
            gridCollection.getStyleClass().clear();
            gridCasual.getStyleClass().clear();
            gridMostruario.getStyleClass().clear();

            this.factoryLabelAttMeta();
            if (selectedBid.getCodmar().equals("D")) {
                imgLogoMarca.setImage(imgDlz);
                gridJornada.getStyleClass().add("gridBidJornadaDlz");
                lbJornada.getStyleClass().add("gridBidJornadaDlz");
                gridCd.getStyleClass().add("gridBidJornadaDlz");
                gridPg.getStyleClass().add("gridBidJornadaDlz");
                gridPm.getStyleClass().add("gridBidJornadaDlz");
                gridPr.getStyleClass().add("gridBidJornadaDlz");
                gridPv.getStyleClass().add("gridBidJornadaDlz");

                gridPerformance.getStyleClass().add("gridBidPerformanceDlz");
                gridKa.getStyleClass().add("gridBidPerformanceDlz");
                gridPp.getStyleClass().add("gridBidPerformanceDlz");
                gridTf.getStyleClass().add("gridBidPerformanceDlz");
                gridTp.getStyleClass().add("gridBidPerformanceDlz");
                gridVp.getStyleClass().add("gridBidPerformanceDlz");
                gridBasico.getStyleClass().add("gridBidPerformanceDlz");
                gridDenim.getStyleClass().add("gridBidPerformanceDlz");
                gridCollection.getStyleClass().add("gridBidPerformanceDlz");
                gridCasual.getStyleClass().add("gridBidPerformanceDlz");
                gridMostruario.getStyleClass().add("gridBidPerformanceDlz");

            } else {
                imgLogoMarca.setImage(imgFlorDeLis);
                gridJornada.getStyleClass().add("gridBidJornadaFlor");
                gridCd.getStyleClass().add("gridBidJornadaFlor");
                gridPg.getStyleClass().add("gridBidJornadaFlor");
                gridPm.getStyleClass().add("gridBidJornadaFlor");
                gridPr.getStyleClass().add("gridBidJornadaFlor");
                gridPv.getStyleClass().add("gridBidJornadaFlor");
                lbJornada.getStyleClass().add("gridBidJornadaFlor");

                gridPerformance.getStyleClass().add("gridBidPerformanceFlor");
                gridKa.getStyleClass().add("gridBidPerformanceFlor");
                gridPp.getStyleClass().add("gridBidPerformanceFlor");
                gridTf.getStyleClass().add("gridBidPerformanceFlor");
                gridTp.getStyleClass().add("gridBidPerformanceFlor");
                gridVp.getStyleClass().add("gridBidPerformanceFlor");
                gridBasico.getStyleClass().add("gridBidPerformanceFlor");
                gridDenim.getStyleClass().add("gridBidPerformanceFlor");
                gridCollection.getStyleClass().add("gridBidPerformanceFlor");
                gridCasual.getStyleClass().add("gridBidPerformanceFlor");
                gridMostruario.getStyleClass().add("gridBidPerformanceFlor");
            }
        }
    }

    private void unbindData(Bid selectedBid) {
        if (selectedBid != null) {
            pmRcm.textProperty().unbind();
            pmRpm.textProperty().unbind();
            pmMta.textProperty().unbind();
            pmLy.textProperty().unbind();
            pmAttMeta.textProperty().unbind();
            cdRcm.textProperty().unbind();
            cdRpm.textProperty().unbind();
            cdMta.textProperty().unbind();
            cdLy.textProperty().unbind();
            cdAttMeta.textProperty().unbind();
            pvRcm.textProperty().unbind();
            pvRpm.textProperty().unbind();
            pvMta.textProperty().unbind();
            pvLy.textProperty().unbind();
            pvAttMeta.textProperty().unbind();
            kaRcm.textProperty().unbind();
            kaRpm.textProperty().unbind();
            kaMta.textProperty().unbind();
            kaLy.textProperty().unbind();
            kaAttMeta.textProperty().unbind();
            ppRcm.textProperty().unbind();
            ppRpm.textProperty().unbind();
            ppMta.textProperty().unbind();
            ppLy.textProperty().unbind();
            ppAttMeta.textProperty().unbind();
            vpRcm.textProperty().unbind();
            vpRpm.textProperty().unbind();
            vpMta.textProperty().unbind();
            vpLy.textProperty().unbind();
            vpAttMeta.textProperty().unbind();
            tpRcm.textProperty().unbind();
            tpRpm.textProperty().unbind();
            tpMta.textProperty().unbind();
            tpLy.textProperty().unbind();
            tpAttMeta.textProperty().unbind();
            tfRcm.textProperty().unbind();
            tfRpm.textProperty().unbind();
            tfMta.textProperty().unbind();
            tfLy.textProperty().unbind();
            tfAttMeta.textProperty().unbind();
            pgRcm.textProperty().unbind();
            pgRpm.textProperty().unbind();
            pgMta.textProperty().unbind();
            pgLy.textProperty().unbind();
            pgAttMeta.textProperty().unbind();
            prRcm.textProperty().unbind();
            prRpm.textProperty().unbind();
            prMta.textProperty().unbind();
            prLy.textProperty().unbind();
            prAttMeta.textProperty().unbind();
            clmReal.textProperty().unbind();
            clnMta.textProperty().unbind();
            clmAttMeta.textProperty().unbind();
            mostrRep.textProperty().unbind();

            tpRcmBas.textProperty().unbind();
            tpMtaBas.textProperty().unbind();
            perTpBas.textProperty().unbind();

            tpRcmDen.textProperty().unbind();
            tpMtaDen.textProperty().unbind();
            perTpDen.textProperty().unbind();

            tpRcmCol.textProperty().unbind();
            tpMtaCol.textProperty().unbind();
            perTpCol.textProperty().unbind();

            tpRcmCas.textProperty().unbind();
            tpMtaCas.textProperty().unbind();
            perTpCas.textProperty().unbind();

            lbCodRep.textProperty().unbind();
            lbRep.textProperty().unbind();
            lbRegiaoRep.textProperty().unbind();
            lbSemanaBid.textProperty().unbind();

            tblSica.setItems(FXCollections.observableArrayList());
            tblBidPremiado.setItems(FXCollections.observableArrayList());

            gridJornada.getStyleClass().clear();
            gridCd.getStyleClass().clear();
            gridPg.getStyleClass().clear();
            gridPm.getStyleClass().clear();
            gridPr.getStyleClass().clear();
            gridPv.getStyleClass().clear();
            lbJornada.getStyleClass().clear();

            gridPerformance.getStyleClass().clear();
            gridKa.getStyleClass().clear();
            gridPp.getStyleClass().clear();
            gridTf.getStyleClass().clear();
            gridTp.getStyleClass().clear();
            gridVp.getStyleClass().clear();
            gridBasico.getStyleClass().clear();
            gridDenim.getStyleClass().clear();
            gridCollection.getStyleClass().clear();
            gridCasual.getStyleClass().clear();
            gridMostruario.getStyleClass().clear();

            gridJornada.getStyleClass().add("gridBidJornada");
            gridCd.getStyleClass().add("gridBidJornada");
            gridPg.getStyleClass().add("gridBidJornada");
            gridPm.getStyleClass().add("gridBidJornada");
            gridPr.getStyleClass().add("gridBidJornada");
            gridPv.getStyleClass().add("gridBidJornada");
            lbJornada.getStyleClass().add("gridBidJornada");

            gridPerformance.getStyleClass().add("gridBidPerformance");
            gridKa.getStyleClass().add("gridBidPerformance");
            gridPp.getStyleClass().add("gridBidPerformance");
            gridTf.getStyleClass().add("gridBidPerformance");
            gridTp.getStyleClass().add("gridBidPerformance");
            gridVp.getStyleClass().add("gridBidPerformance");
            gridBasico.getStyleClass().add("gridBidPerformance");
            gridDenim.getStyleClass().add("gridBidPerformance");
            gridCollection.getStyleClass().add("gridBidPerformance");
            gridCasual.getStyleClass().add("gridBidPerformance");
            gridMostruario.getStyleClass().add("gridBidPerformance");

        }
    }

    private void factoryLabelAttMeta() {
        backgroundLabel(pmAttMeta);
        backgroundLabel(cdAttMeta);
        backgroundLabel(pvAttMeta);
        backgroundLabel(kaAttMeta);
        backgroundLabel(ppAttMeta);
        backgroundLabel(vpAttMeta);
        backgroundLabel(tpAttMeta);
        backgroundLabel(tfAttMeta);
        backgroundLabel(pgAttMeta);
        backgroundLabel(prAttMeta);
    }

    private void backgroundLabel(Label attMeta) {
        Double metaAtt = Double.parseDouble(attMeta.getText());
        if (metaAtt <= 79.999) {
            attMeta.setStyle("-fx-background-color: #FF0000; -fx-text-fill: #000;");
        } else if (metaAtt > 79.999 && metaAtt < 99.999) {
            attMeta.setStyle("-fx-background-color: #FFFF00; -fx-text-fill: #000;");
        } else {
            attMeta.setStyle("-fx-background-color: #32CD32; -fx-text-fill: #000;");
        }
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos FXML">
    @FXML
    private void btnShowBidOnAction(ActionEvent event) {
        Double larguraBid = 1.0 - (1300.0 / larguraTela.doubleValue());

        if (displayedBid) {
            displayedBid = Boolean.FALSE;
            splitWindow.setDividerPositions(1.0);
            btnShowBid.setText("Exibir BID");
        } else {
            displayedBid = Boolean.TRUE;
            splitWindow.setDividerPositions(larguraBid);
            btnShowBid.setText("Esconder BID");
        }
    }

    @FXML
    private void tboxCodRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarPresentanteOnAction(null);
        }
    }

    @FXML
    private void btnProcurarPresentanteOnAction(ActionEvent event) {
        try {
            SceneProcurarRepresentanteController ctrlSceneRepresentante = new SceneProcurarRepresentanteController(Modals.FXMLWindow.SceneProcurarRepresentante);
            if (!ctrlSceneRepresentante.cods_retorno.isEmpty()) {
                tboxCodRepresentante.setText(ctrlSceneRepresentante.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        FilterColecoesView filterColecoesView = new FilterColecoesView();
        filterColecoesView.show(false);

        if( filterColecoesView.itensSelecionados.size() > 0){
            tboxCodColecao.setText(filterColecoesView.getResultAsString(true));
        }
    }

    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
        try {
            SceneProcurarFamiliaController ctrlSceneFamilia = new SceneProcurarFamiliaController(Modals.FXMLWindow.SceneProcurarFamilia);
            if (!ctrlSceneFamilia.cods_retorno.isEmpty()) {
                tboxCodFamilia.setText(ctrlSceneFamilia.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblRepresentantes.setItems(DAOFactory.getBidDAO().loadBid(tboxCodRepresentante.getText(),
                    tboxCodColecao.getText(), cboxMarca.getValue(), tboxCodFamilia.getText()));
            abrirCaixaEmail = Boolean.TRUE;
            abrirNovamente = Boolean.TRUE;

            lbUltimaAtualizacao.setText(DAOFactory.getLogDAO().getLastLogJobOracle().toUpperCase());
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarColecaoOnAction(null);
        }
    }

    @FXML
    private void tboxCodFamiliaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFamiliaOnAction(null);
        }
    }

    @FXML
    private void btnEnviarBidOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");
        WritableImage imagePremio = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File filePremio = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imagePremio, null), "png", filePremio);

            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());

            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCompleto.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdfNoOpen(romaneiosParaImpressao, "BidCompleto");

        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String body_text = "";
        try {
            String line = null;
            String filePath = "c:/SysDelizLocal/body.mail";
            byte[] buffer = new byte[1000];
            FileInputStream inputStream = new FileInputStream(filePath);
            int total = 0;
            int nRead = 0;
            while ((nRead = inputStream.read(buffer)) != -1) {
                total += nRead;
                line += new String(buffer);
            }
            inputStream.close();

            if (abrirCaixaEmail || abrirNovamente) {
                String text_file = "";
                String[] dadosReturn = GUIUtils.showTextAreaDialogWithRemember("Conteúdo e-mail:", line);
                String[] lines = dadosReturn[0].split("\n");
                abrirNovamente = !Boolean.parseBoolean(dadosReturn[1]);
                FileWriter fileWriter = new FileWriter(filePath);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                for (String lineText : lines) {
                    body_text += lineText + "<br/>";
                    text_file += lineText + "\n";
                }
                bufferedWriter.write(text_file);
                bufferedWriter.close();
                abrirCaixaEmail = Boolean.FALSE;
            }
        } catch (FileNotFoundException ex) {
            String text_file = "";
            String filePath = "c:/SysDelizLocal/body.mail";
            String[] dadosReturn = GUIUtils.showTextAreaDialogWithRemember("Conteúdo e-mail:", null);
            String[] lines = dadosReturn[0].split("\n");
            abrirNovamente = !Boolean.parseBoolean(dadosReturn[1]);
            try {
                FileWriter fileWriter = new FileWriter(filePath);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                for (String lineText : lines) {
                    body_text += lineText + "<br/>";
                    text_file += lineText + "\n";
                }
                bufferedWriter.write(text_file);
                bufferedWriter.close();
            } catch (IOException ex1) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex1);
                GUIUtils.showException(ex1);
            }
            abrirCaixaEmail = Boolean.FALSE;
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        String emailRepGer = tblRepresentantes.getSelectionModel().getSelectedItem().getEmail_rep();
//        if (!tblRepresentantes.getSelectionModel().getSelectedItem().getGerente().equals("ALVARO")) {
//            emailRepGer += "," + tblRepresentantes.getSelectionModel().getSelectedItem().getEmail_ger();
//        }
        MailUtils.sendMailBid(MailUtils.AttachMail.FULL_BID,
                emailRepGer,
                body_text,
                tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent());
        try {
            DAOFactory.getLogDAO().saveLog(new Log(null, SceneMainController.getAcesso().getUsuario(), "Enviar", "BID",
                    tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep(), "Envio de BID por e-mail. Rep.: " + tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent()));
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnImprimirBidOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");

        WritableImage imageP = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File fileP = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imageP, null), "png", fileP);

            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBid.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);

        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnImprimirBidCompletoOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");
        WritableImage imagePremio = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File filePremio = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imagePremio, null), "png", filePremio);

            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());
            parametros.put("col_qua", lbQuartaColecao.getText());

            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCompleto.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);

        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnImprimirCidadesOnAction(ActionEvent event) {
        try {
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("represent", tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());
            parametros.put("col_qua", lbQuartaColecao.getText());

            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCidadesRep.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);

        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnAtualizarDadosOnAction(ActionEvent event) {
    }
    //</editor-fold>
}
