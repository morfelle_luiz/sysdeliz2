/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.control.table.TableFilter;
import sysdeliz2.controllers.fxml.procura.SceneProcurarCidadeController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarClienteController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarMarcaController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarRepresentanteController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.BidCliente;
import sysdeliz2.models.BidClienteIndicadores;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.utils.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComercialBidClientesController implements Initializable {

    Locale BRAZIL = new Locale("pt", "BR");
    private BidCliente bidCliente = null;

    //<editor-fold>
    @FXML
    private CheckBox chboxColecaoAtual;
    @FXML
    private CheckBox chboxColecaoAnterior;
    @FXML
    private CheckBox chboxAmbasColecoes;
    @FXML
    private TableColumn<BidCliente, String> clnCompra;
    @FXML
    private Button btnFecharJanela;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private TextField tboxCodCliente;
    @FXML
    private Button btnProcurarCliente;
    @FXML
    private TextField tboxCodCidade;
    @FXML
    private Button btnProcurarCidade;
    @FXML
    private TextField tboxCodRepresentante;
    @FXML
    private Button btnProcurarRepresentante;
    @FXML
    private TextField tboxCodColecao;
    @FXML
    private Button btnProcurarColecao;
    @FXML
    private TextField tboxCodMarca;
    @FXML
    private Button btnProcurarMarca;
    @FXML
    private TableView<BidCliente> tblClientes;
    @FXML
    private Button btnProcurar;
    @FXML
    private Button btnImprimir;
    @FXML
    private Button btnExportarExcel;
    @FXML
    private TableView<BidClienteIndicadores> tblBidBasicoCliente;
    @FXML
    private TableColumn<BidClienteIndicadores, String> clnIndicadorBasico;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAntBasico;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAtuBasico;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnDiferencialBasico;
    @FXML
    private TableView<BidClienteIndicadores> tblBidDenimCliente;
    @FXML
    private TableColumn<BidClienteIndicadores, String> clnIndicadorDenim;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAntDenim;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAtuDenim;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnDiferencialDenim;
    @FXML
    private TableView<BidClienteIndicadores> tblBidMarcaCliente;
    @FXML
    private TableColumn<BidClienteIndicadores, String> clnIndicadorMarca;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAntMarca;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAtuMarca;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnDiferencialMarca;
    @FXML
    private TableView<BidClienteIndicadores> tblBidTotalCliente;
    @FXML
    private TableColumn<BidClienteIndicadores, String> clnIndicadorTotal;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAntTotal;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnColAtuTotal;
    @FXML
    private TableColumn<BidClienteIndicadores, Double> clnDiferencialTotal;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableUtils.autoFitTable(tblClientes, TableUtils.COLUNAS_TBL_COMERCIAL_BID_CLIENTE);
        tblClientes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                unbindData(oldValue);
                bindData(newValue);
            } catch (SQLException ex) {
                Logger.getLogger(SceneComercialBidClientesController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
    }

    private void factoryColumns() {
        clnCompra.setCellFactory((TableColumn<BidCliente, String> param) -> {
            TableCell cell = new TableCell<BidCliente, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidCliente currentCliente = currentRow == null ? null : (BidCliente) currentRow.getItem();
                    if (currentCliente != null) {
                        String status = currentCliente.getCompra();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(String priority) {
                    ImageView imgCol = new ImageView();
                    imgCol.setFitWidth(22.0);
                    imgCol.setFitHeight(22.0);
                    switch (priority) {
                        case "AMBAS":
                            imgCol.setImage(new Image("/images/icons8-flag-2-96-green.png"));
                            setGraphic(imgCol);
                            break;
                        case "ATUAL":
                            imgCol.setImage(new Image("/images/icons8-flag-2-96-blue.png"));
                            setGraphic(imgCol);
                            break;
                        case "ANTERIOR":
                            imgCol.setImage(new Image("/images/icons8-flag-2-96-red.png"));
                            setGraphic(imgCol);
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clnColAntBasico.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && formatValueCell != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #E0FFFF; -fx-font-weight: bold;");
                    }

                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAtuBasico.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #FFE4B5; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAntDenim.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #E0FFFF; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAtuDenim.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #FFE4B5; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAntMarca.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #E0FFFF; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAtuMarca.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #FFE4B5; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAntTotal.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #E0FFFF; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnColAtuTotal.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores indicador = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    NumberFormat formatValueCell = null;
                    Double valueCell = getString();
                    if (valueCell != null && indicador != null) {
                        if (indicador.getIndicador().contains("PG")) {
                            formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PM") || indicador.getIndicador().contains("TF")) {
                            formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("RF") || indicador.getIndicador().contains("TP")) {
                            formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                        } else if (indicador.getIndicador().contains("PR")) {
                            formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                            formatValueCell.setMaximumFractionDigits(2);
                            valueCell = valueCell / 100;
                        }
                    }
                    if (valueCell != null && indicador != null) {
                        setText(empty ? null : formatValueCell.format(valueCell));
                        setGraphic(null);
                        setStyle("-fx-background-color: #FFE4B5; -fx-font-weight: bold;");
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnDiferencialBasico.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores currentCliente = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    if (currentCliente != null) {
                        Double realmeta = currentCliente.getDiferencial();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority == 0.0) {
                        setStyle("-fx-background-color: #FFDEAD; -fx-text-fill: #B8860B; -fx-font-weight: bold;");
                    } else if (priority > 0.0) {
                        setStyle("-fx-background-color: #98FB98; -fx-text-fill: #006400; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #FFA07A; -fx-text-fill: #800000; -fx-font-weight: bold;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnDiferencialDenim.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores currentCliente = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    if (currentCliente != null) {
                        Double realmeta = currentCliente.getDiferencial();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority == 0.0) {
                        setStyle("-fx-background-color: #FFDEAD; -fx-text-fill: #B8860B; -fx-font-weight: bold;");
                    } else if (priority > 0.0) {
                        setStyle("-fx-background-color: #98FB98; -fx-text-fill: #006400; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #FFA07A; -fx-text-fill: #800000; -fx-font-weight: bold;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnDiferencialMarca.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores currentCliente = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    if (currentCliente != null) {
                        Double realmeta = currentCliente.getDiferencial();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority == 0.0) {
                        setStyle("-fx-background-color: #FFDEAD; -fx-text-fill: #B8860B; -fx-font-weight: bold;");
                    } else if (priority > 0.0) {
                        setStyle("-fx-background-color: #98FB98; -fx-text-fill: #006400; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #FFA07A; -fx-text-fill: #800000; -fx-font-weight: bold;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnDiferencialTotal.setCellFactory((TableColumn<BidClienteIndicadores, Double> param) -> {
            TableCell cell = new TableCell<BidClienteIndicadores, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidClienteIndicadores currentCliente = currentRow == null ? null : (BidClienteIndicadores) currentRow.getItem();
                    if (currentCliente != null) {
                        Double realmeta = currentCliente.getDiferencial();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void setPriorityStyle(Double priority) {
                    if (priority == 0.0) {
                        setStyle("-fx-background-color: #FFDEAD; -fx-text-fill: #B8860B; -fx-font-weight: bold;");
                    } else if (priority > 0.0) {
                        setStyle("-fx-background-color: #98FB98; -fx-text-fill: #006400; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #FFA07A; -fx-text-fill: #800000; -fx-font-weight: bold;");
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
    }

    private String getCodsCliente() {
        String cods = "";

        for (BidCliente cliente : tblClientes.getItems()) {
            if (!cods.contains(cliente.getCodcli())) {
                cods += "'" + cliente.getCodcli() + "',";
            }
        }

        return cods.substring(0, cods.length() - 1);
    }

    private void bindData(BidCliente cliente) throws SQLException {
        if (cliente != null) {
            if (!cliente.getCodcli().equals("0")) {
                this.bidCliente = cliente;
                String linhaMarca = cliente.getCodmar().equals("F") ? "COLLECTION" : "CASUAL";
                tblBidBasicoCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "BASICO"));
                tblBidDenimCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "DENIM"));
                tblBidMarcaCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), linhaMarca));
                tblBidTotalCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "TOTAL"));
            } else {
                tblBidBasicoCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "BASICO"));
                tblBidDenimCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "DENIM"));
                tblBidMarcaCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "COLLECTION|CASUAL"));
                tblBidTotalCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "TOTAL"));
            }
            this.factoryColumns();
        }
    }

    private void unbindData(BidCliente cliente) throws SQLException {
        if (cliente != null) {
            if (!cliente.getCodcli().equals("0")) {
                this.bidCliente = cliente;
                String linhaMarca = cliente.getCodmar().equals("F") ? "COLLECTION" : "CASUAL";
                tblBidBasicoCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "BASICO"));
                tblBidDenimCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "DENIM"));
                tblBidMarcaCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), linhaMarca));
                tblBidTotalCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadores(cliente.getCodcli(), cliente.getCodmar(), tboxCodColecao.getText(), "TOTAL"));
            } else {
                tblBidBasicoCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "BASICO"));
                tblBidDenimCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "DENIM"));
                tblBidMarcaCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "COLLECTION|CASUAL"));
                tblBidTotalCliente.setItems(DAOFactory.getBidDAO().getBidClienteIndicadoresTotais(this.getCodsCliente(), tboxCodMarca.getText(), tboxCodColecao.getText(), "TOTAL"));
            }
            this.factoryColumns();
        }
    }

    @FXML
    private void tboxCodClienteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarClienteOnAction(null);
        }
    }

    @FXML
    private void btnProcurarClienteOnAction(ActionEvent event) {
        try {
            SceneProcurarClienteController ctrlSceneFamilia = new SceneProcurarClienteController(Modals.FXMLWindow.SceneProcurarCliente);
            if (!ctrlSceneFamilia.cods_retorno.isEmpty()) {
                tboxCodCliente.setText(ctrlSceneFamilia.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodCidadeOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarCidadeOnAction(null);
        }
    }

    @FXML
    private void btnProcurarCidadeOnAction(ActionEvent event) {
        try {
            SceneProcurarCidadeController ctrlSceneRepresentante = new SceneProcurarCidadeController(Modals.FXMLWindow.SceneProcurarCidade);
            if (!ctrlSceneRepresentante.cods_retorno.isEmpty()) {
                tboxCodCidade.setText(ctrlSceneRepresentante.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarRepresentanteOnAction(null);
        }
    }

    @FXML
    private void btnProcurarRepresentanteOnAction(ActionEvent event) {
        try {
            SceneProcurarRepresentanteController ctrlSceneRepresentante = new SceneProcurarRepresentanteController(Modals.FXMLWindow.SceneProcurarRepresentante);
            if (!ctrlSceneRepresentante.cods_retorno.isEmpty()) {
                tboxCodRepresentante.setText(ctrlSceneRepresentante.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarColecaoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        FilterColecoesView filterColecoesView = new FilterColecoesView();
        filterColecoesView.show(false);

        if (filterColecoesView.itensSelecionados.size() > 0) {
            tboxCodColecao.setText(filterColecoesView.getResultAsString(true));
        }
    }

    @FXML
    private void btnImprimirOnAction(ActionEvent event) {
        if (bidCliente != null) {
            try {
                new ReportUtils().config().addReport(ReportUtils.ReportFile.BID_CLIENTES,
                        new ReportUtils.ParameterReport("codcli", bidCliente.getCodcli()),
                        new ReportUtils.ParameterReport("colecao", tboxCodColecao.getText()),
                        new ReportUtils.ParameterReport("marca", bidCliente.getCodmar()),
                        new ReportUtils.ParameterReport("colecaoDesc", getDescricaoColecao(tboxCodColecao.getText()))).view().toView();
            } catch (JRException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String getDescricaoColecao(String codColecao) {
        return ((Colecao) new FluentDao().selectFrom(Colecao.class).where(it -> it.equal("codigo", codColecao)).singleResult()).getDescricao();
    }

    @FXML
    private void btnExportarExcelOnAction(ActionEvent event) {
        if (bidCliente != null) {
            try {
                new ReportUtils().config().addReport(ReportUtils.ReportFile.BID_CLIENTES,
                        new ReportUtils.ParameterReport("codcli", bidCliente.getCodcli()),
                        new ReportUtils.ParameterReport("colecao", tboxCodColecao.getText()),
                        new ReportUtils.ParameterReport("marca", bidCliente.getCodmar()),
                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()),
                        new ReportUtils.ParameterReport("colecaoDesc", getDescricaoColecao(tboxCodColecao.getText()))).view().toExcelWithDialog();
            } catch (JRException | SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void tboxCodMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        try {
            SceneProcurarMarcaController ctrlSceneMarca = new SceneProcurarMarcaController(Modals.FXMLWindow.SceneProcurarMarca);
            if (!ctrlSceneMarca.cods_retorno.isEmpty()) {
                tboxCodMarca.setText(ctrlSceneMarca.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            String compra = "";
            compra += chboxColecaoAtual.isSelected() ? "ATUAL|" : "";
            compra += chboxAmbasColecoes.isSelected() ? "AMBAS|" : "";
            compra += chboxColecaoAnterior.isSelected() ? "ANTERIOR|" : "";

            tblClientes.setItems(DAOFactory.getBidDAO().getBidCliente(tboxCodRepresentante.getText(), tboxCodCliente.getText(),
                    tboxCodCidade.getText(), tboxCodColecao.getText(), tboxCodMarca.getText(), compra.substring(0, compra.length() - 1)));
            tblClientes.getItems().add(new BidCliente("0", "TOTAIS CONSULTAS", "", "", "", "", "", "", "", null));

            try {
                clnColAntBasico.setText((Integer.parseInt(tboxCodColecao.getText()) - 100) + "");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            clnColAtuBasico.setText(tboxCodColecao.getText());
            try {
                clnColAntDenim.setText((Integer.parseInt(tboxCodColecao.getText()) - 100) + "");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            clnColAtuDenim.setText(tboxCodColecao.getText());
            try {
                clnColAntMarca.setText((Integer.parseInt(tboxCodColecao.getText()) - 100) + "");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            clnColAtuMarca.setText(tboxCodColecao.getText());
            try {
                clnColAntTotal.setText((Integer.parseInt(tboxCodColecao.getText()) - 100) + "");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            clnColAtuTotal.setText(tboxCodColecao.getText());

            TableFilter filter = new TableFilter(tblClientes);
            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialBidClientesController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

}
