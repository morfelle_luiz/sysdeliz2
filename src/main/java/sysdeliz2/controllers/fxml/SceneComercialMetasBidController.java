/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;
import sysdeliz2.controllers.fxml.pcp.ScenePcpPriorizarReferenciasController;
import sysdeliz2.controllers.fxml.procura.FilterMarcaController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarRepresentanteController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.IndicadorMetaBid;
import sysdeliz2.models.properties.ComercialMetasBid;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComercialMetasBidController implements Initializable {

    @FXML
    public TextField tboxCodigoMarca;
    @FXML
    public TextField tboxNovoCodigoMarca;
    @FXML
    private TextField tboxCodRepresentante_c;
    @FXML
    private TextField tboxCodColecao_c;
    @FXML
    private Button btnProcurar;
    @FXML
    private TabPane tabMainPane;
    @FXML
    private TableView<ComercialMetasBid> tblMetas;
    @FXML
    private Button btnVisualizar;
    @FXML
    private Button btnNovo;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnExcluir;
    @FXML
    private TextField tboxCodColecao;
    @FXML
    private TextField tboxCodRepresentante;
    @FXML
    private Button btnSalvar;
    @FXML
    private Button btnCancelar;
    @FXML
    private VBox vboxContainerSpreadsheet;

    private SpreadsheetView tblMetasRep = null;
    private BooleanProperty isEditMode = new SimpleBooleanProperty(false);
    private BooleanProperty isNewMode = new SimpleBooleanProperty(false);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblMetas.setItems(DAOFactory.getBidDAO().getMetasAll());
            tblMetas.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
                unbindData();
                bindData(newValue);
            });
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        btnVisualizar.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnEditar.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnExcluir.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnSalvar.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
    }

    private void unbindData() {
        tboxCodColecao.textProperty().unbind();
        tboxCodRepresentante.textProperty().unbind();
        tboxCodigoMarca.textProperty().unbind();
        tboxCodColecao.clear();
        tboxCodRepresentante.clear();

        //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
        GridBase grid = new GridBase(12, 8);
        grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "MTA", "RPM", "BAS", "DEN", "COLL", "CASU", "WIS"));
        tblMetasRep = new SpreadsheetView(grid);
        vboxContainerSpreadsheet.getChildren().clear();
        vboxContainerSpreadsheet.getChildren().add(tblMetasRep);
        VBox.setVgrow(tblMetasRep, Priority.ALWAYS);
        tblMetasRep.setEditable(true);
        tblMetasRep.setShowRowHeader(true);
        tblMetasRep.setShowColumnHeader(true);
        tblMetasRep.getStyleClass().add("spreadsheet");

        tblMetasRep.getColumns().get(0).setPrefWidth(200);
        tblMetasRep.getColumns().get(1).setPrefWidth(70);
        tblMetasRep.getColumns().get(2).setPrefWidth(70);
        tblMetasRep.getColumns().get(3).setPrefWidth(70);
        tblMetasRep.getColumns().get(4).setPrefWidth(70);
        tblMetasRep.getColumns().get(5).setPrefWidth(70);
        tblMetasRep.getColumns().get(6).setPrefWidth(70);
        tblMetasRep.getColumns().get(7).setPrefWidth(70);

        ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
        ObservableList<SpreadsheetCell> metasMostruario = FXCollections.observableArrayList();
        SpreadsheetCell cell1Mos = SpreadsheetCellType.STRING.createCell(0, 0, 1, 1, "Mostruário (Pçs Coleção)");
        SpreadsheetCell cell2Mos = SpreadsheetCellType.INTEGER.createCell(0, 1, 1, 1, 0);
        SpreadsheetCell cell3Mos = SpreadsheetCellType.INTEGER.createCell(0, 2, 1, 1, 0);
        SpreadsheetCell cell4Mos = SpreadsheetCellType.INTEGER.createCell(0, 3, 1, 1, 0);
        SpreadsheetCell cell5Mos = SpreadsheetCellType.INTEGER.createCell(0, 4, 1, 1, 0);
        SpreadsheetCell cell6Mos = SpreadsheetCellType.INTEGER.createCell(0, 5, 1, 1, 0);
        SpreadsheetCell cell7Mos = SpreadsheetCellType.INTEGER.createCell(0, 6, 1, 1, 0);
        SpreadsheetCell cell8Mos = SpreadsheetCellType.INTEGER.createCell(0, 7, 1, 1, 0);
        cell1Mos.setEditable(false);
        cell2Mos.setEditable(true);
        cell3Mos.setEditable(false);
        cell4Mos.setEditable(false);
        cell5Mos.setEditable(false);
        cell6Mos.setEditable(false);
        cell7Mos.setEditable(false);
        cell8Mos.setEditable(false);
        metasMostruario.addAll(cell1Mos, cell2Mos, cell3Mos, cell4Mos, cell5Mos, cell6Mos, cell7Mos, cell8Mos);
        ObservableList<SpreadsheetCell> metasClienteNovo = FXCollections.observableArrayList();
        SpreadsheetCell cell1Cln = SpreadsheetCellType.STRING.createCell(1, 0, 1, 1, "Cliente Novo");
        SpreadsheetCell cell2Cln = SpreadsheetCellType.INTEGER.createCell(1, 1, 1, 1, 0);
        SpreadsheetCell cell3Cln = SpreadsheetCellType.INTEGER.createCell(1, 2, 1, 1, 0);
        SpreadsheetCell cell4Cln = SpreadsheetCellType.INTEGER.createCell(1, 3, 1, 1, 0);
        SpreadsheetCell cell5Cln = SpreadsheetCellType.INTEGER.createCell(1, 4, 1, 1, 0);
        SpreadsheetCell cell6Cln = SpreadsheetCellType.INTEGER.createCell(1, 5, 1, 1, 0);
        SpreadsheetCell cell7Cln = SpreadsheetCellType.INTEGER.createCell(1, 6, 1, 1, 0);
        SpreadsheetCell cell8Cln = SpreadsheetCellType.INTEGER.createCell(1, 7, 1, 1, 0);
        cell1Cln.setEditable(false);
        cell2Cln.setEditable(true);
        cell3Cln.setEditable(false);
        cell4Cln.setEditable(false);
        cell5Cln.setEditable(false);
        cell6Cln.setEditable(false);
        cell7Cln.setEditable(false);
        cell8Cln.setEditable(false);
        metasClienteNovo.addAll(cell1Cln, cell2Cln, cell3Cln, cell4Cln, cell5Cln, cell6Cln, cell7Cln, cell8Cln);
        ObservableList<SpreadsheetCell> metasPm = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pm = SpreadsheetCellType.STRING.createCell(2, 0, 1, 1, "Preço Médio R$ (PM)");
        SpreadsheetCell cell2Pm = SpreadsheetCellType.DOUBLE.createCell(2, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pm = SpreadsheetCellType.DOUBLE.createCell(2, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pm = SpreadsheetCellType.DOUBLE.createCell(2, 3, 1, 1, 0.0);
        SpreadsheetCell cell5Pm = SpreadsheetCellType.DOUBLE.createCell(2, 4, 1, 1, 0.0);
        SpreadsheetCell cell6Pm = SpreadsheetCellType.DOUBLE.createCell(2, 5, 1, 1, 0.0);
        SpreadsheetCell cell7Pm = SpreadsheetCellType.DOUBLE.createCell(2, 6, 1, 1, 0.0);
        SpreadsheetCell cell8Pm = SpreadsheetCellType.DOUBLE.createCell(2, 7, 1, 1, 0.0);
        cell1Pm.setEditable(false);
        cell2Pm.setEditable(true);
        cell3Pm.setEditable(true);
        cell4Pm.setEditable(false);
        cell5Pm.setEditable(false);
        cell6Pm.setEditable(false);
        cell7Pm.setEditable(false);
        cell8Pm.setEditable(false);
        metasPm.addAll(cell1Pm, cell2Pm, cell3Pm, cell4Pm, cell5Pm, cell6Pm, cell7Pm, cell7Pm);
        ObservableList<SpreadsheetCell> metasCd = FXCollections.observableArrayList();
        SpreadsheetCell cell1Cd = SpreadsheetCellType.STRING.createCell(3, 0, 1, 1, "Cidades Atendidas (CD)");
        SpreadsheetCell cell2Cd = SpreadsheetCellType.INTEGER.createCell(3, 1, 1, 1, 0);
        SpreadsheetCell cell3Cd = SpreadsheetCellType.INTEGER.createCell(3, 2, 1, 1, 0);
        SpreadsheetCell cell4Cd = SpreadsheetCellType.INTEGER.createCell(3, 3, 1, 1, 0);
        SpreadsheetCell cell5Cd = SpreadsheetCellType.INTEGER.createCell(3, 4, 1, 1, 0);
        SpreadsheetCell cell6Cd = SpreadsheetCellType.INTEGER.createCell(3, 5, 1, 1, 0);
        SpreadsheetCell cell7Cd = SpreadsheetCellType.INTEGER.createCell(3, 6, 1, 1, 0);
        SpreadsheetCell cell8Cd = SpreadsheetCellType.INTEGER.createCell(3, 7, 1, 1, 0);
        cell1Cd.setEditable(false);
        cell2Cd.setEditable(true);
        cell3Cd.setEditable(true);
        cell4Cd.setEditable(false);
        cell5Cd.setEditable(false);
        cell6Cd.setEditable(false);
        cell7Cd.setEditable(false);
        cell8Cd.setEditable(false);
        metasCd.addAll(cell1Cd, cell2Cd, cell3Cd, cell4Cd, cell5Cd, cell6Cd, cell7Cd, cell8Cd);
        ObservableList<SpreadsheetCell> metasPv = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pv = SpreadsheetCellType.STRING.createCell(4, 0, 1, 1, "Clientes Atendidos (PV)");
        SpreadsheetCell cell2Pv = SpreadsheetCellType.INTEGER.createCell(4, 1, 1, 1, 0);
        SpreadsheetCell cell3Pv = SpreadsheetCellType.INTEGER.createCell(4, 2, 1, 1, 0);
        SpreadsheetCell cell4Pv = SpreadsheetCellType.INTEGER.createCell(4, 3, 1, 1, 0);
        SpreadsheetCell cell5Pv = SpreadsheetCellType.INTEGER.createCell(4, 4, 1, 1, 0);
        SpreadsheetCell cell6Pv = SpreadsheetCellType.INTEGER.createCell(4, 5, 1, 1, 0);
        SpreadsheetCell cell7Pv = SpreadsheetCellType.INTEGER.createCell(4, 6, 1, 1, 0);
        SpreadsheetCell cell8Pv = SpreadsheetCellType.INTEGER.createCell(4, 7, 1, 1, 0);
        cell1Pv.setEditable(false);
        cell2Pv.setEditable(true);
        cell3Pv.setEditable(true);
        cell4Pv.setEditable(false);
        cell5Pv.setEditable(false);
        cell6Pv.setEditable(false);
        cell7Pv.setEditable(false);
        cell8Pv.setEditable(false);
        metasPv.addAll(cell1Pv, cell2Pv, cell3Pv, cell4Pv, cell5Pv, cell6Pv, cell7Pv,cell8Pv);
        ObservableList<SpreadsheetCell> metasKa = FXCollections.observableArrayList();
        SpreadsheetCell cell1Ka = SpreadsheetCellType.STRING.createCell(5, 0, 1, 1, "Key Acounts (KA)");
        SpreadsheetCell cell2Ka = SpreadsheetCellType.INTEGER.createCell(5, 1, 1, 1, 0);
        SpreadsheetCell cell3Ka = SpreadsheetCellType.INTEGER.createCell(5, 2, 1, 1, 0);
        SpreadsheetCell cell4Ka = SpreadsheetCellType.INTEGER.createCell(5, 3, 1, 1, 0);
        SpreadsheetCell cell5Ka = SpreadsheetCellType.INTEGER.createCell(5, 4, 1, 1, 0);
        SpreadsheetCell cell6Ka = SpreadsheetCellType.INTEGER.createCell(5, 5, 1, 1, 0);
        SpreadsheetCell cell7Ka = SpreadsheetCellType.INTEGER.createCell(5, 6, 1, 1, 0);
        SpreadsheetCell cell8Ka = SpreadsheetCellType.INTEGER.createCell(5, 7, 1, 1, 0);
        cell1Ka.setEditable(false);
        cell2Ka.setEditable(true);
        cell3Ka.setEditable(true);
        cell4Ka.setEditable(false);
        cell5Ka.setEditable(false);
        cell6Ka.setEditable(false);
        cell7Ka.setEditable(false);
        cell8Ka.setEditable(false);
        metasKa.addAll(cell1Ka, cell2Ka, cell3Ka, cell4Ka, cell5Ka, cell6Ka, cell7Ka, cell8Ka);
        ObservableList<SpreadsheetCell> metasPp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pp = SpreadsheetCellType.STRING.createCell(6, 0, 1, 1, "Peças por Pedido (PP)");
        SpreadsheetCell cell2Pp = SpreadsheetCellType.INTEGER.createCell(6, 1, 1, 1, 0);
        SpreadsheetCell cell3Pp = SpreadsheetCellType.INTEGER.createCell(6, 2, 1, 1, 0);
        SpreadsheetCell cell4Pp = SpreadsheetCellType.INTEGER.createCell(6, 3, 1, 1, 0);
        SpreadsheetCell cell5Pp = SpreadsheetCellType.INTEGER.createCell(6, 4, 1, 1, 0);
        SpreadsheetCell cell6Pp = SpreadsheetCellType.INTEGER.createCell(6, 5, 1, 1, 0);
        SpreadsheetCell cell7Pp = SpreadsheetCellType.INTEGER.createCell(6, 6, 1, 1, 0);
        SpreadsheetCell cell8Pp = SpreadsheetCellType.INTEGER.createCell(6, 7, 1, 1, 0);
        cell1Pp.setEditable(false);
        cell2Pp.setEditable(true);
        cell3Pp.setEditable(false);
        cell4Pp.setEditable(false);
        cell5Pp.setEditable(false);
        cell6Pp.setEditable(false);
        cell7Pp.setEditable(false);
        cell8Pp.setEditable(false);
        metasPp.addAll(cell1Pp, cell2Pp, cell3Pp, cell4Pp, cell5Pp, cell6Pp, cell7Pp, cell8Pp);
        ObservableList<SpreadsheetCell> metasVp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Vp = SpreadsheetCellType.STRING.createCell(7, 0, 1, 1, "Valor por Pedido (VP)");
        SpreadsheetCell cell2Vp = SpreadsheetCellType.DOUBLE.createCell(7, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Vp = SpreadsheetCellType.DOUBLE.createCell(7, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Vp = SpreadsheetCellType.DOUBLE.createCell(7, 3, 1, 1, 0.0);
        SpreadsheetCell cell5Vp = SpreadsheetCellType.DOUBLE.createCell(7, 4, 1, 1, 0.0);
        SpreadsheetCell cell6Vp = SpreadsheetCellType.DOUBLE.createCell(7, 5, 1, 1, 0.0);
        SpreadsheetCell cell7Vp = SpreadsheetCellType.DOUBLE.createCell(7, 6, 1, 1, 0.0);
        SpreadsheetCell cell8Vp = SpreadsheetCellType.DOUBLE.createCell(7, 7, 1, 1, 0.0);
        cell1Vp.setEditable(false);
        cell2Vp.setEditable(true);
        cell3Vp.setEditable(false);
        cell4Vp.setEditable(false);
        cell5Vp.setEditable(false);
        cell6Vp.setEditable(false);
        cell7Vp.setEditable(false);
        cell8Vp.setEditable(false);
        metasVp.addAll(cell1Vp, cell2Vp, cell3Vp, cell4Vp, cell5Vp, cell6Vp, cell7Vp, cell8Vp);
        ObservableList<SpreadsheetCell> metasTp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Tp = SpreadsheetCellType.STRING.createCell(8, 0, 1, 1, "Total de Peças (TP)");
        SpreadsheetCell cell2Tp = SpreadsheetCellType.INTEGER.createCell(8, 1, 1, 1, 0);
        SpreadsheetCell cell3Tp = SpreadsheetCellType.INTEGER.createCell(8, 2, 1, 1, 0);
        SpreadsheetCell cell4Tp = SpreadsheetCellType.INTEGER.createCell(8, 3, 1, 1, 0);
        SpreadsheetCell cell5Tp = SpreadsheetCellType.INTEGER.createCell(8, 4, 1, 1, 0);
        SpreadsheetCell cell6Tp = SpreadsheetCellType.INTEGER.createCell(8, 5, 1, 1, 0);
        SpreadsheetCell cell7Tp = SpreadsheetCellType.INTEGER.createCell(8, 6, 1, 1, 0);
        SpreadsheetCell cell8Tp = SpreadsheetCellType.INTEGER.createCell(8, 7, 1, 1, 0);
        cell1Tp.setEditable(false);
        cell2Tp.setEditable(true);
        cell3Tp.setEditable(false);
        cell4Tp.setEditable(true);
        cell5Tp.setEditable(true);
        cell6Tp.setEditable(true);
        cell7Tp.setEditable(true);
        cell8Tp.setEditable(true);
        metasTp.addAll(cell1Tp, cell2Tp, cell3Tp, cell4Tp, cell5Tp, cell6Tp, cell7Tp, cell8Tp);
        ObservableList<SpreadsheetCell> metasTf = FXCollections.observableArrayList();
        SpreadsheetCell cell1Tf = SpreadsheetCellType.STRING.createCell(9, 0, 1, 1, "Total Financeiro (TF)");
        SpreadsheetCell cell2Tf = SpreadsheetCellType.DOUBLE.createCell(9, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Tf = SpreadsheetCellType.DOUBLE.createCell(9, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Tf = SpreadsheetCellType.DOUBLE.createCell(9, 3, 1, 1, 0.0);
        SpreadsheetCell cell5Tf = SpreadsheetCellType.DOUBLE.createCell(9, 4, 1, 1, 0.0);
        SpreadsheetCell cell6Tf = SpreadsheetCellType.DOUBLE.createCell(9, 5, 1, 1, 0.0);
        SpreadsheetCell cell7Tf = SpreadsheetCellType.DOUBLE.createCell(9, 6, 1, 1, 0.0);
        SpreadsheetCell cell8Tf = SpreadsheetCellType.DOUBLE.createCell(9, 7, 1, 1, 0.0);
        cell1Tf.setEditable(false);
        cell2Tf.setEditable(true);
        cell3Tf.setEditable(false);
        cell4Tf.setEditable(true);
        cell5Tf.setEditable(true);
        cell6Tf.setEditable(true);
        cell7Tf.setEditable(true);
        cell8Tf.setEditable(true);
        metasTf.addAll(cell1Tf, cell2Tf, cell3Tf, cell4Tf, cell5Tf, cell6Tf, cell7Tf, cell8Tf);
        ObservableList<SpreadsheetCell> metasPg = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pg = SpreadsheetCellType.STRING.createCell(10, 0, 1, 1, "Profundidade da Grade (PG)");
        SpreadsheetCell cell2Pg = SpreadsheetCellType.DOUBLE.createCell(10, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pg = SpreadsheetCellType.DOUBLE.createCell(10, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pg = SpreadsheetCellType.DOUBLE.createCell(10, 3, 1, 1, 0.0);
        SpreadsheetCell cell5Pg = SpreadsheetCellType.DOUBLE.createCell(10, 4, 1, 1, 0.0);
        SpreadsheetCell cell6Pg = SpreadsheetCellType.DOUBLE.createCell(10, 5, 1, 1, 0.0);
        SpreadsheetCell cell7Pg = SpreadsheetCellType.DOUBLE.createCell(10, 6, 1, 1, 0.0);
        SpreadsheetCell cell8Pg = SpreadsheetCellType.DOUBLE.createCell(10, 7, 1, 1, 0.0);
        cell1Pg.setEditable(false);
        cell2Pg.setEditable(true);
        cell3Pg.setEditable(true);
        cell4Pg.setEditable(false);
        cell5Pg.setEditable(false);
        cell6Pg.setEditable(false);
        cell7Pg.setEditable(false);
        cell8Pg.setEditable(false);
        metasPg.addAll(cell1Pg, cell2Pg, cell3Pg, cell4Pg, cell5Pg, cell6Pg, cell7Pg, cell8Pg);
        ObservableList<SpreadsheetCell> metasPr = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pr = SpreadsheetCellType.STRING.createCell(11, 0, 1, 1, "Profundidade das Referências (PR) (%)");
        SpreadsheetCell cell2Pr = SpreadsheetCellType.DOUBLE.createCell(11, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pr = SpreadsheetCellType.DOUBLE.createCell(11, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pr = SpreadsheetCellType.DOUBLE.createCell(11, 3, 1, 1, 0.0);
        SpreadsheetCell cell5Pr = SpreadsheetCellType.DOUBLE.createCell(11, 4, 1, 1, 0.0);
        SpreadsheetCell cell6Pr = SpreadsheetCellType.DOUBLE.createCell(11, 5, 1, 1, 0.0);
        SpreadsheetCell cell7Pr = SpreadsheetCellType.DOUBLE.createCell(11, 6, 1, 1, 0.0);
        SpreadsheetCell cell8Pr = SpreadsheetCellType.DOUBLE.createCell(11, 6, 1, 1, 0.0);
        cell1Pr.setEditable(false);
        cell2Pr.setEditable(true);
        cell3Pr.setEditable(true);
        cell4Pr.setEditable(false);
        cell5Pr.setEditable(false);
        cell6Pr.setEditable(false);
        cell7Pr.setEditable(false);
        cell8Pr.setEditable(false);
        metasPr.addAll(cell1Pr, cell2Pr, cell3Pr, cell4Pr, cell5Pr, cell6Pr, cell7Pr, cell8Pr);

        rowsGrid.add(metasMostruario);
        rowsGrid.add(metasClienteNovo);
        rowsGrid.add(metasPm);
        rowsGrid.add(metasCd);
        rowsGrid.add(metasPv);
        rowsGrid.add(metasKa);
        rowsGrid.add(metasPp);
        rowsGrid.add(metasVp);
        rowsGrid.add(metasTp);
        rowsGrid.add(metasTf);
        rowsGrid.add(metasPg);
        rowsGrid.add(metasPr);

        grid.setRows(rowsGrid);
        tblMetasRep.setContextMenu(null);
        //</editor-fold>
    }

    private void bindData(ComercialMetasBid indicadorRep) {

        if (indicadorRep != null) {
            tboxCodColecao.textProperty().bind(indicadorRep.codcolProperty());
            tboxCodRepresentante.textProperty().bind(indicadorRep.codrepProperty());
            tboxCodigoMarca.textProperty().bind(indicadorRep.codmarProperty());

            //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
            GridBase grid = new GridBase(12, 8);
            grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "MTA", "RPM", "BAS", "DEN", "COLL", "CASU", "WIS"));
            tblMetasRep = new SpreadsheetView(grid);
            vboxContainerSpreadsheet.getChildren().clear();
            vboxContainerSpreadsheet.getChildren().add(tblMetasRep);
            VBox.setVgrow(tblMetasRep, Priority.ALWAYS);
            tblMetasRep.setEditable(true);
            tblMetasRep.setShowRowHeader(true);
            tblMetasRep.setShowColumnHeader(true);
            tblMetasRep.getStyleClass().add("spreadsheet");

            tblMetasRep.getColumns().get(0).setPrefWidth(200);
            tblMetasRep.getColumns().get(1).setPrefWidth(70);
            tblMetasRep.getColumns().get(2).setPrefWidth(70);
            tblMetasRep.getColumns().get(3).setPrefWidth(70);
            tblMetasRep.getColumns().get(4).setPrefWidth(70);
            tblMetasRep.getColumns().get(5).setPrefWidth(70);
            tblMetasRep.getColumns().get(6).setPrefWidth(70);
            tblMetasRep.getColumns().get(7).setPrefWidth(70);

            ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
            int linha = 0;
            for (IndicadorMetaBid indicador : indicadorRep.getMetas()) {
                ObservableList<SpreadsheetCell> metasMostruario = FXCollections.observableArrayList();
                SpreadsheetCell cell1Mos = SpreadsheetCellType.STRING.createCell(linha, 0, 1, 1, indicador.getIndicador());
                SpreadsheetCell cell2Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 1, 1, 1, linha == 11 ? indicador.getMta() * 100 : indicador.getMta());
                SpreadsheetCell cell3Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 2, 1, 1, linha == 11 ? indicador.getRpm() * 100 : indicador.getRpm());
                SpreadsheetCell cell4Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 3, 1, 1, indicador.getBas());
                SpreadsheetCell cell5Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 4, 1, 1, indicador.getDen());
                SpreadsheetCell cell6Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 5, 1, 1, indicador.getColl());
                SpreadsheetCell cell7Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 6, 1, 1, indicador.getCasu());
                SpreadsheetCell cell8Mos = SpreadsheetCellType.DOUBLE.createCell(linha, 7, 1, 1, indicador.getWish());
                cell1Mos.setEditable(false);
                cell2Mos.setEditable(false);
                cell3Mos.setEditable(false);
                cell4Mos.setEditable(false);
                cell5Mos.setEditable(false);
                cell6Mos.setEditable(false);
                cell7Mos.setEditable(false);
                cell8Mos.setEditable(false);
                metasMostruario.addAll(cell1Mos, cell2Mos, cell3Mos, cell4Mos, cell5Mos, cell6Mos, cell7Mos, cell8Mos);

                rowsGrid.add(metasMostruario);
                linha++;
            }

            grid.setRows(rowsGrid);
            tblMetasRep.setContextMenu(null);
            //</editor-fold>
        }
    }

    private void bindDataEdit(ComercialMetasBid indicadorRep) {

        if (indicadorRep != null) {
            tboxCodColecao.textProperty().bind(indicadorRep.codcolProperty());
            tboxCodRepresentante.textProperty().bind(indicadorRep.codrepProperty());
            tboxCodigoMarca.textProperty().bind(indicadorRep.codmarProperty());

            //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
            GridBase grid = new GridBase(12, 8);
            grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "MTA", "RPM", "BAS", "DEN", "COLL", "CASU", "WIS"));
            tblMetasRep = new SpreadsheetView(grid);
            vboxContainerSpreadsheet.getChildren().clear();
            vboxContainerSpreadsheet.getChildren().add(tblMetasRep);
            VBox.setVgrow(tblMetasRep, Priority.ALWAYS);
            tblMetasRep.setEditable(true);
            tblMetasRep.setShowRowHeader(true);
            tblMetasRep.setShowColumnHeader(true);
            tblMetasRep.getStyleClass().add("spreadsheet");

            tblMetasRep.getColumns().get(0).setPrefWidth(200);
            tblMetasRep.getColumns().get(1).setPrefWidth(70);
            tblMetasRep.getColumns().get(2).setPrefWidth(70);
            tblMetasRep.getColumns().get(3).setPrefWidth(70);
            tblMetasRep.getColumns().get(4).setPrefWidth(70);
            tblMetasRep.getColumns().get(5).setPrefWidth(70);
            tblMetasRep.getColumns().get(6).setPrefWidth(70);
            tblMetasRep.getColumns().get(7).setPrefWidth(70);

            ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
            int line = 0;
            for (IndicadorMetaBid indicador : indicadorRep.getMetas()) {
                ObservableList<SpreadsheetCell> metasMostruario = FXCollections.observableArrayList();
                SpreadsheetCell cell1Mos = SpreadsheetCellType.STRING.createCell(line, 0, 1, 1, indicador.getIndicador());
                SpreadsheetCell cell2Mos = SpreadsheetCellType.DOUBLE.createCell(line, 1, 1, 1, line == 11 ? indicador.getMta() * 100 : indicador.getMta());
                SpreadsheetCell cell3Mos = SpreadsheetCellType.DOUBLE.createCell(line, 2, 1, 1, line == 11 ? indicador.getRpm() * 100 : indicador.getRpm());
                SpreadsheetCell cell4Mos = SpreadsheetCellType.DOUBLE.createCell(line, 3, 1, 1, indicador.getBas());
                SpreadsheetCell cell5Mos = SpreadsheetCellType.DOUBLE.createCell(line, 4, 1, 1, indicador.getDen());
                SpreadsheetCell cell6Mos = SpreadsheetCellType.DOUBLE.createCell(line, 5, 1, 1, indicador.getColl());
                SpreadsheetCell cell7Mos = SpreadsheetCellType.DOUBLE.createCell(line, 6, 1, 1, indicador.getCasu());
                SpreadsheetCell cell8Mos = SpreadsheetCellType.DOUBLE.createCell(line, 7, 1, 1, indicador.getWish());
                cell1Mos.setEditable(false);
                cell2Mos.setEditable(true);
                cell3Mos.setEditable(true);
                cell4Mos.setEditable(true);
                cell5Mos.setEditable(true);
                cell6Mos.setEditable(true);
                cell7Mos.setEditable(true);
                cell8Mos.setEditable(true);
                metasMostruario.addAll(cell1Mos, cell2Mos, cell3Mos, cell4Mos, cell5Mos, cell6Mos, cell7Mos, cell8Mos);

                line++;
                rowsGrid.add(metasMostruario);
            }

            grid.setRows(rowsGrid);
            tblMetasRep.setContextMenu(null);
            //</editor-fold>
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblMetas.setItems(DAOFactory.getBidDAO().getMetasReps(tboxCodRepresentante_c.getText().replace(',', '|'),
                    tboxCodigoMarca.getText().replaceAll("'", ""),
                    tboxCodColecao_c.getText().replace(',', '|')));
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnVisualizarOnAction(ActionEvent event) {
        bindData(tblMetas.getSelectionModel().getSelectedItem());
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnNovoOnAction(ActionEvent event) {
        isNewMode.set(true);
        unbindData();
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnEditarOnAction(ActionEvent event) {
        isEditMode.set(true);
        bindDataEdit(tblMetas.getSelectionModel().getSelectedItem());
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnExcluirOnAction(ActionEvent event) {
        if (!GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir as metas deste representante?")) {
            return;
        }
        ComercialMetasBid metaRepr = tblMetas.getSelectionModel().getSelectedItem();
        try {
            DAOFactory.getBidDAO().deleteMetaRepr(metaRepr.codrepProperty().get(),
                    metaRepr.codmarProperty().get(),
                    metaRepr.codcolProperty().get());
            tblMetas.getItems().remove(metaRepr);
            tblMetas.refresh();
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        if (tboxCodColecao.getText().isEmpty()) {
            GUIUtils.showMessage("Você deve preencher o código da coleção.", Alert.AlertType.INFORMATION);
            return;
        }
        if (tboxCodigoMarca.getText().isEmpty()) {
            GUIUtils.showMessage("Você deve selecionar a marca.", Alert.AlertType.INFORMATION);
            return;
        }
        if (tboxCodRepresentante.getText().isEmpty()) {
            GUIUtils.showMessage("Você deve preencher o código do representante.", Alert.AlertType.INFORMATION);
            return;
        }

        String[] colecoes = tboxCodColecao.getText().replace('|', ';').split(";");
        String[] representantes = tboxCodRepresentante.getText().replace('|', ';').split(";");

        for (String colecao : colecoes) {
            for (String representante : representantes) {
                String metasMta = "";
                String metasRpm = "";
                String metasBas = "";
                String metasDen = "";
                String metasColl = "";
                String metasCasu = "";
                String metasWish = "";

                for (int i = 0; i < 12; i++) {
                    ObservableList<SpreadsheetCell> cells = tblMetasRep.getItems().get(i);
                    metasMta += ";" + cells.get(1).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasRpm += ";" + cells.get(2).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasBas += ";" + cells.get(3).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasDen += ";" + cells.get(4).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasColl += ";" + cells.get(5).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasCasu += ";" + cells.get(6).getText().replaceAll("\\.", "").replace(',', '.').trim();
                    metasWish += ";" + cells.get(7).getText().replaceAll("\\.", "").replace(',', '.').trim();
                }

                try {
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "MTA", metasMta.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "RPM", metasRpm.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "BAS", metasBas.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "DEN", metasDen.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "COL", metasColl.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "CAS", metasCasu.split(";"));
                    DAOFactory.getBidDAO().mergeMetaRepr(representante.trim(), tboxCodigoMarca.getText().replaceAll("'", ""), colecao.trim(), "WIS", metasWish.split(";"));

                    if (isEditMode.get()) {
                        tblMetas.getSelectionModel().getSelectedItem().setMetas(DAOFactory.getBidDAO().
                                getMetaReps(tboxCodRepresentante.getText(), tboxCodigoMarca.getText().replaceAll("'", ""), tboxCodColecao.getText()).getMetas());
                    } else if (isNewMode.get()) {
                        tblMetas.getItems().add(DAOFactory.getBidDAO().
                                getMetaReps(tboxCodRepresentante.getText(), tboxCodigoMarca.getText().replaceAll("'", ""), tboxCodColecao.getText()));
                    }
                    tblMetas.refresh();
                } catch (SQLException ex) {
                    Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                    isEditMode.set(false);
                    isNewMode.set(false);
                }
            }
        }
        isEditMode.set(false);
        isNewMode.set(false);
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        isEditMode.set(false);
        isNewMode.set(false);
        unbindData();
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void tboxCodColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            FilterColecoesView filterColecoesView = new FilterColecoesView();
            filterColecoesView.show();
            if (filterColecoesView.itensSelecionados.size() > 0) {
                tboxCodColecao.setText(filterColecoesView.getResultAsString(true));
            }
        }
    }

    @FXML
    private void tboxCodRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            try {
                SceneProcurarRepresentanteController ctrlSceneRepresentante = new SceneProcurarRepresentanteController(Modals.FXMLWindow.SceneProcurarRepresentante);
                if (!ctrlSceneRepresentante.cods_retorno.isEmpty()) {
                    tboxCodRepresentante.setText(ctrlSceneRepresentante.cods_retorno);
                }
            } catch (IOException ex) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void tboxCodigoMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        try {
            tboxCodigoMarca.textProperty().unbind();
            FilterMarcaController controlFilterMarca = new FilterMarcaController(Modals.FXMLWindow.FilterMarca, tboxCodigoMarca.getText());
            if (!controlFilterMarca.returnValue.isEmpty()) {
                tboxCodigoMarca.setText(controlFilterMarca.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (event != null) {
            event.consume();
        }
    }

    @FXML
    private void tboxCodRepresentante_cOnKeyRelease(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            try {
                SceneProcurarRepresentanteController ctrlSceneRepresentante = new SceneProcurarRepresentanteController(Modals.FXMLWindow.SceneProcurarRepresentante);
                if (!ctrlSceneRepresentante.cods_retorno.isEmpty()) {
                    tboxCodRepresentante_c.setText(ctrlSceneRepresentante.cods_retorno);
                }
            } catch (IOException ex) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void tboxCodColecao_cOnKeyRelease(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            FilterColecoesView filterColecoesView = new FilterColecoesView();
            filterColecoesView.show(false);
            if (filterColecoesView.itensSelecionados.size() > 0) {
                tboxCodColecao_c.setText(filterColecoesView.getResultAsString(true));
            }
        }
    }

    @FXML
    public void tboxNovoCodigoMarcaOnKeyReleased(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.F4) {
            btnProcurarNovoMarcaOnAction(null);
        }
    }

    @FXML
    public void btnProcurarNovoMarcaOnAction(ActionEvent event) {
        try {
            FilterMarcaController controlFilterMarca = new FilterMarcaController(Modals.FXMLWindow.FilterMarca, tboxCodigoMarca.getText());
            if (!controlFilterMarca.returnValue.isEmpty()) {
                tboxNovoCodigoMarca.setText(controlFilterMarca.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (event != null) {
            event.consume();
        }
    }
}
