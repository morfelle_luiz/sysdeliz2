/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;
import sysdeliz2.controllers.fxml.procura.SceneProcurarMarcaController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.IndicadorMetaBidPremiado;
import sysdeliz2.models.properties.ComercialMetasBidPremiado;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComercialMetasBidPremiadoController implements Initializable {

    @FXML
    private TextField tboxCodColecao_c;
    @FXML
    private Button btnProcurar;
    @FXML
    private TextField tboxCodMarca_c;
    @FXML
    private TabPane tabMainPane;
    @FXML
    private TableView<ComercialMetasBidPremiado> tblMetas;
    @FXML
    private Button btnVisualizar;
    @FXML
    private Button btnNovo;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnExcluir;
    @FXML
    private TextField tboxCodColecao;
    @FXML
    private TextField tboxCodMarca;
    @FXML
    private VBox vboxContainerSpreadsheet;
    @FXML
    private Button btnSalvar;
    @FXML
    private Button btnCancelar;

    private SpreadsheetView tblMetasPremio = null;
    private BooleanProperty isEditMode = new SimpleBooleanProperty(false);
    private BooleanProperty isNewMode = new SimpleBooleanProperty(false);
    private Integer[] ordens = {1, 2, 3, 6, 4, 5, 7, 8, 9, 10};

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblMetas.setItems(DAOFactory.getBidDAO().getMetasPremioAll());
            tblMetas.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
                unbindData();
                bindData(newValue);
            });
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidPremiadoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        btnVisualizar.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnEditar.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnExcluir.disableProperty().bind(tblMetas.getSelectionModel().selectedItemProperty().isNull());
        btnSalvar.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
    }

    private void unbindData() {
        tboxCodColecao.textProperty().unbind();
        tboxCodMarca.textProperty().unbind();
        tboxCodColecao.clear();
        tboxCodMarca.clear();

        //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
        GridBase grid = new GridBase(10, 4);
        grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "Graduação", "% Premio", "Obrigadório"));
        tblMetasPremio = new SpreadsheetView(grid);
        vboxContainerSpreadsheet.getChildren().clear();
        vboxContainerSpreadsheet.getChildren().add(tblMetasPremio);
        VBox.setVgrow(tblMetasPremio, Priority.ALWAYS);
        tblMetasPremio.setEditable(true);
        tblMetasPremio.setShowRowHeader(true);
        tblMetasPremio.setShowColumnHeader(true);
        tblMetasPremio.getStyleClass().add("spreadsheet");

        tblMetasPremio.getColumns().get(0).setPrefWidth(80);
        tblMetasPremio.getColumns().get(1).setPrefWidth(80);
        tblMetasPremio.getColumns().get(2).setPrefWidth(80);
        tblMetasPremio.getColumns().get(3).setPrefWidth(80);

        ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
        ObservableList<SpreadsheetCell> metasPm = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pm = SpreadsheetCellType.STRING.createCell(0, 0, 1, 1, "PM");
        SpreadsheetCell cell2Pm = SpreadsheetCellType.DOUBLE.createCell(0, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pm = SpreadsheetCellType.DOUBLE.createCell(0, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pm = SpreadsheetCellType.STRING.createCell(0, 3, 1, 1, "");
        cell1Pm.setEditable(false);
        cell2Pm.setEditable(true);
        cell3Pm.setEditable(true);
        cell4Pm.setEditable(true);
        metasPm.addAll(cell1Pm, cell2Pm, cell3Pm, cell4Pm);
        ObservableList<SpreadsheetCell> metasCd = FXCollections.observableArrayList();
        SpreadsheetCell cell1Cd = SpreadsheetCellType.STRING.createCell(1, 0, 1, 1, "CD");
        SpreadsheetCell cell2Cd = SpreadsheetCellType.DOUBLE.createCell(1, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Cd = SpreadsheetCellType.DOUBLE.createCell(1, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Cd = SpreadsheetCellType.STRING.createCell(1, 3, 1, 1, "");
        cell1Cd.setEditable(false);
        cell2Cd.setEditable(true);
        cell3Cd.setEditable(true);
        cell4Cd.setEditable(true);
        metasCd.addAll(cell1Cd, cell2Cd, cell3Cd, cell4Cd);
        ObservableList<SpreadsheetCell> metasPv = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pv = SpreadsheetCellType.STRING.createCell(2, 0, 1, 1, "PV");
        SpreadsheetCell cell2Pv = SpreadsheetCellType.DOUBLE.createCell(2, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pv = SpreadsheetCellType.DOUBLE.createCell(2, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pv = SpreadsheetCellType.STRING.createCell(2, 3, 1, 1, "");
        cell1Pv.setEditable(false);
        cell2Pv.setEditable(true);
        cell3Pv.setEditable(true);
        cell4Pv.setEditable(true);
        metasPv.addAll(cell1Pv, cell2Pv, cell3Pv, cell4Pv);
        ObservableList<SpreadsheetCell> metasKa = FXCollections.observableArrayList();
        SpreadsheetCell cell1Ka = SpreadsheetCellType.STRING.createCell(3, 0, 1, 1, "KA");
        SpreadsheetCell cell2Ka = SpreadsheetCellType.DOUBLE.createCell(3, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Ka = SpreadsheetCellType.DOUBLE.createCell(3, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Ka = SpreadsheetCellType.STRING.createCell(3, 3, 1, 1, "");
        cell1Ka.setEditable(false);
        cell2Ka.setEditable(true);
        cell3Ka.setEditable(true);
        cell4Ka.setEditable(true);
        metasKa.addAll(cell1Ka, cell2Ka, cell3Ka, cell4Ka);
        ObservableList<SpreadsheetCell> metasPp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pp = SpreadsheetCellType.STRING.createCell(4, 0, 1, 1, "PP");
        SpreadsheetCell cell2Pp = SpreadsheetCellType.DOUBLE.createCell(4, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pp = SpreadsheetCellType.DOUBLE.createCell(4, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pp = SpreadsheetCellType.STRING.createCell(4, 3, 1, 1, "");
        cell1Pp.setEditable(false);
        cell2Pp.setEditable(true);
        cell3Pp.setEditable(true);
        cell4Pp.setEditable(true);
        metasPp.addAll(cell1Pp, cell2Pp, cell3Pp, cell4Pp);
        ObservableList<SpreadsheetCell> metasVp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Vp = SpreadsheetCellType.STRING.createCell(5, 0, 1, 1, "VP");
        SpreadsheetCell cell2Vp = SpreadsheetCellType.DOUBLE.createCell(5, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Vp = SpreadsheetCellType.DOUBLE.createCell(5, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Vp = SpreadsheetCellType.STRING.createCell(5, 3, 1, 1, "");
        cell1Vp.setEditable(false);
        cell2Vp.setEditable(true);
        cell3Vp.setEditable(true);
        cell4Vp.setEditable(true);
        metasVp.addAll(cell1Vp, cell2Vp, cell3Vp, cell4Vp);
        ObservableList<SpreadsheetCell> metasTp = FXCollections.observableArrayList();
        SpreadsheetCell cell1Tp = SpreadsheetCellType.STRING.createCell(6, 0, 1, 1, "TP");
        SpreadsheetCell cell2Tp = SpreadsheetCellType.DOUBLE.createCell(6, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Tp = SpreadsheetCellType.DOUBLE.createCell(6, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Tp = SpreadsheetCellType.STRING.createCell(6, 3, 1, 1, "");
        cell1Tp.setEditable(false);
        cell2Tp.setEditable(true);
        cell3Tp.setEditable(true);
        cell4Tp.setEditable(true);
        metasTp.addAll(cell1Tp, cell2Tp, cell3Tp, cell4Tp);
        ObservableList<SpreadsheetCell> metasTf = FXCollections.observableArrayList();
        SpreadsheetCell cell1Tf = SpreadsheetCellType.STRING.createCell(7, 0, 1, 1, "TF");
        SpreadsheetCell cell2Tf = SpreadsheetCellType.DOUBLE.createCell(7, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Tf = SpreadsheetCellType.DOUBLE.createCell(7, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Tf = SpreadsheetCellType.STRING.createCell(7, 3, 1, 1, "");
        cell1Tf.setEditable(false);
        cell2Tf.setEditable(true);
        cell3Tf.setEditable(true);
        cell4Tf.setEditable(true);
        metasTf.addAll(cell1Tf, cell2Tf, cell3Tf, cell4Tf);
        ObservableList<SpreadsheetCell> metasPg = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pg = SpreadsheetCellType.STRING.createCell(8, 0, 1, 1, "PG");
        SpreadsheetCell cell2Pg = SpreadsheetCellType.DOUBLE.createCell(8, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pg = SpreadsheetCellType.DOUBLE.createCell(8, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pg = SpreadsheetCellType.STRING.createCell(8, 3, 1, 1, "");
        cell1Pg.setEditable(false);
        cell2Pg.setEditable(true);
        cell3Pg.setEditable(true);
        cell4Pg.setEditable(true);
        metasPg.addAll(cell1Pg, cell2Pg, cell3Pg, cell4Pg);
        ObservableList<SpreadsheetCell> metasPr = FXCollections.observableArrayList();
        SpreadsheetCell cell1Pr = SpreadsheetCellType.STRING.createCell(9, 0, 1, 1, "PR");
        SpreadsheetCell cell2Pr = SpreadsheetCellType.DOUBLE.createCell(9, 1, 1, 1, 0.0);
        SpreadsheetCell cell3Pr = SpreadsheetCellType.DOUBLE.createCell(9, 2, 1, 1, 0.0);
        SpreadsheetCell cell4Pr = SpreadsheetCellType.STRING.createCell(9, 3, 1, 1, "");
        cell1Pr.setEditable(false);
        cell2Pr.setEditable(true);
        cell3Pr.setEditable(true);
        cell4Pr.setEditable(true);
        metasPr.addAll(cell1Pr, cell2Pr, cell3Pr, cell4Pr);

        rowsGrid.add(metasPm);
        rowsGrid.add(metasCd);
        rowsGrid.add(metasPv);
        rowsGrid.add(metasKa);
        rowsGrid.add(metasPp);
        rowsGrid.add(metasVp);
        rowsGrid.add(metasTp);
        rowsGrid.add(metasTf);
        rowsGrid.add(metasPg);
        rowsGrid.add(metasPr);

        grid.setRows(rowsGrid);
        tblMetasPremio.setContextMenu(null);

        tblMetasPremio.setStyle("-fx-background-color: #FFF;");
        //</editor-fold>
    }

    private void bindData(ComercialMetasBidPremiado indicadorRep) {

        if (indicadorRep != null) {
            tboxCodColecao.textProperty().bind(indicadorRep.colecaoProperty());
            tboxCodMarca.textProperty().bind(indicadorRep.marcaProperty());

            //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
            GridBase grid = new GridBase(10, 4);
            grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "Graduação", "% Premio", "Obrigadório"));
            tblMetasPremio = new SpreadsheetView(grid);
            vboxContainerSpreadsheet.getChildren().clear();
            vboxContainerSpreadsheet.getChildren().add(tblMetasPremio);
            VBox.setVgrow(tblMetasPremio, Priority.ALWAYS);
            tblMetasPremio.setEditable(true);
            tblMetasPremio.setShowRowHeader(true);
            tblMetasPremio.setShowColumnHeader(true);
            tblMetasPremio.getStyleClass().add("spreadsheet");

            tblMetasPremio.getColumns().get(0).setPrefWidth(80);
            tblMetasPremio.getColumns().get(1).setPrefWidth(80);
            tblMetasPremio.getColumns().get(2).setPrefWidth(80);
            tblMetasPremio.getColumns().get(3).setPrefWidth(80);

            ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
            int linha = 0;
            for (IndicadorMetaBidPremiado indicador : indicadorRep.getIndicadores()) {
                ObservableList<SpreadsheetCell> metasPm = FXCollections.observableArrayList();
                SpreadsheetCell cell1Pm = SpreadsheetCellType.STRING.createCell(linha, 0, 1, 1, indicador.getIndicador());
                SpreadsheetCell cell2Pm = SpreadsheetCellType.DOUBLE.createCell(linha, 1, 1, 1, indicador.getGraduacao());
                SpreadsheetCell cell3Pm = SpreadsheetCellType.DOUBLE.createCell(linha, 2, 1, 1, indicador.getPerc_premio());
                SpreadsheetCell cell4Pm = SpreadsheetCellType.STRING.createCell(linha, 3, 1, 1, indicador.getObrigatorio());
                cell1Pm.setEditable(false);
                cell2Pm.setEditable(false);
                cell3Pm.setEditable(false);
                cell4Pm.setEditable(false);
                metasPm.addAll(cell1Pm, cell2Pm, cell3Pm, cell4Pm);
                rowsGrid.add(metasPm);
                linha++;
            }

            grid.setRows(rowsGrid);
            tblMetasPremio.setContextMenu(null);

            tblMetasPremio.setStyle("-fx-background-color: #FFF;");
            //</editor-fold>

        }
    }

    private void bindDataEdit(ComercialMetasBidPremiado indicadorRep) {

        if (indicadorRep != null) {
            tboxCodColecao.textProperty().bind(indicadorRep.colecaoProperty());
            tboxCodMarca.textProperty().bind(indicadorRep.marcaProperty());

            //<editor-fold defaultstate="collapsed" desc="SpreadSheet dos Indicadores">
            GridBase grid = new GridBase(10, 4);
            grid.getColumnHeaders().addAll(FXCollections.observableArrayList("Indicador", "Graduação", "% Premio", "Obrigadório"));
            tblMetasPremio = new SpreadsheetView(grid);
            vboxContainerSpreadsheet.getChildren().clear();
            vboxContainerSpreadsheet.getChildren().add(tblMetasPremio);
            VBox.setVgrow(tblMetasPremio, Priority.ALWAYS);
            tblMetasPremio.setEditable(true);
            tblMetasPremio.setShowRowHeader(true);
            tblMetasPremio.setShowColumnHeader(true);
            tblMetasPremio.getStyleClass().add("spreadsheet");

            tblMetasPremio.getColumns().get(0).setPrefWidth(80);
            tblMetasPremio.getColumns().get(1).setPrefWidth(80);
            tblMetasPremio.getColumns().get(2).setPrefWidth(80);
            tblMetasPremio.getColumns().get(3).setPrefWidth(80);

            ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(12);
            int linha = 0;
            for (IndicadorMetaBidPremiado indicador : indicadorRep.getIndicadores()) {
                ObservableList<SpreadsheetCell> metasPm = FXCollections.observableArrayList();
                SpreadsheetCell cell1Pm = SpreadsheetCellType.STRING.createCell(linha, 0, 1, 1, indicador.getIndicador());
                SpreadsheetCell cell2Pm = SpreadsheetCellType.DOUBLE.createCell(linha, 1, 1, 1, indicador.getGraduacao());
                SpreadsheetCell cell3Pm = SpreadsheetCellType.DOUBLE.createCell(linha, 2, 1, 1, indicador.getPerc_premio());
                SpreadsheetCell cell4Pm = SpreadsheetCellType.STRING.createCell(linha, 3, 1, 1, indicador.getObrigatorio());
                cell1Pm.setEditable(false);
                cell2Pm.setEditable(true);
                cell3Pm.setEditable(true);
                cell4Pm.setEditable(true);
                metasPm.addAll(cell1Pm, cell2Pm, cell3Pm, cell4Pm);
                rowsGrid.add(metasPm);
                linha++;
            }

            grid.setRows(rowsGrid);
            tblMetasPremio.setContextMenu(null);

            tblMetasPremio.setStyle("-fx-background-color: #FFF;");
            //</editor-fold>

        }
    }

    @FXML
    private void tboxCodColecao_cOnKeyRelease(KeyEvent event) {
        this.tboxCodColecaoOnKeyReleased(null);
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblMetas.setItems(DAOFactory.getBidDAO().getMetasPremioByWindowsForm(tboxCodMarca_c.getText(), tboxCodColecao_c.getText()));
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidPremiadoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnVisualizarOnAction(ActionEvent event) {
        bindData(tblMetas.getSelectionModel().getSelectedItem());
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnNovoOnAction(ActionEvent event) {
        isNewMode.set(true);
        unbindData();
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnEditarOnAction(ActionEvent event) {
        isEditMode.set(true);
        bindDataEdit(tblMetas.getSelectionModel().getSelectedItem());
        tabMainPane.getSelectionModel().select(1);
    }

    @FXML
    private void btnExcluirOnAction(ActionEvent event) {
        if (!GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir as metas do prêmio?")) {
            return;
        }
        ComercialMetasBidPremiado metaRepr = tblMetas.getSelectionModel().getSelectedItem();
        try {
            DAOFactory.getBidDAO().deleteMetaPremio(metaRepr.marcaProperty().get(), metaRepr.colecaoProperty().get());
            tblMetas.getItems().remove(metaRepr);
            tblMetas.refresh();
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodColecaoOnKeyReleased(KeyEvent event) {
        FilterColecoesView filterColecoesView = new FilterColecoesView();
        filterColecoesView.show(false);

        if(filterColecoesView.itensSelecionados.size() > 0){
            tboxCodColecao_c.setText(filterColecoesView.getResultAsString(true));
        }
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        if (tboxCodColecao.getText().isEmpty()) {
            GUIUtils.showMessage("Você deve preencher o código da coleção.", Alert.AlertType.INFORMATION);
            return;
        }
        if (tboxCodMarca.getText().isEmpty()) {
            GUIUtils.showMessage("Você deve preencher o código da marca.", Alert.AlertType.INFORMATION);
            return;
        }

        String[] colecoes = tboxCodColecao.getText().replace('|', ';').split(";");
        String[] marcas = tboxCodMarca.getText().replace('|', ';').split(";");

        for (String colecao : colecoes) {
            for (String marca : marcas) {
                try {
                    for (int i = 0; i < 10; i++) {
                        ObservableList<SpreadsheetCell> cells = tblMetasPremio.getItems().get(i);
                        DAOFactory.getBidDAO().mergeMetaPremio(marca.trim(), colecao.trim(),
                                cells.get(0).getText(),
                                cells.get(1).getText().replaceAll("\\.", "").replace(',', '.').trim(),
                                cells.get(2).getText().replaceAll("\\.", "").replace(',', '.').trim(),
                                cells.get(3).getText().toUpperCase(),
                                ordens[i]
                        );
                    }

                    if (isEditMode.get()) {
                        tblMetas.getSelectionModel().getSelectedItem().setIndicadores(DAOFactory.getBidDAO().
                                getMetaPremio(marca, colecao).getIndicadores());
                    } else if (isNewMode.get()) {
                        tblMetas.getItems().add(DAOFactory.getBidDAO().getMetaPremio(marca, colecao));
                    }
                    tblMetas.refresh();
                } catch (SQLException ex) {
                    Logger.getLogger(SceneComercialMetasBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                    isEditMode.set(false);
                    isNewMode.set(false);
                }
            }
        }
        isEditMode.set(false);
        isNewMode.set(false);
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        isEditMode.set(false);
        isNewMode.set(false);
        unbindData();
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void tboxCodMarca_cOnKeyReleased(KeyEvent event) {
        try {
            SceneProcurarMarcaController ctrlSceneMarca = new SceneProcurarMarcaController(Modals.FXMLWindow.SceneProcurarMarca);
            if (!ctrlSceneMarca.cods_retorno.isEmpty()) {
                tboxCodMarca_c.setText(ctrlSceneMarca.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodMarcaOnKeyReleased(KeyEvent event) {
        try {
            SceneProcurarMarcaController ctrlSceneMarca = new SceneProcurarMarcaController(Modals.FXMLWindow.SceneProcurarMarca);
            if (!ctrlSceneMarca.cods_retorno.isEmpty()) {
                tboxCodMarca.setText(ctrlSceneMarca.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

}
