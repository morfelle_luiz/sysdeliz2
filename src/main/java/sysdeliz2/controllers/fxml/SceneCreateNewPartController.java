/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.PeriodoDAO;
import sysdeliz2.models.properties.GestaoDeLote;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCreateNewPartController extends Modals implements Initializable {

    public SceneCreateNewPartController(Modals.FXMLWindow file, ObservableList<GestaoDeLote> parts) throws IOException {

        super(file);
        parts.forEach(part -> {
            this.parts.add(part);
        });

        super.show(this);
    }

    /**
     * LOCAL variables
     */
    public ObservableList<GestaoDeLote> newParts;
    private PeriodoDAO daoPeriodo;
    private ObservableList<GestaoDeLote> parts = FXCollections.observableArrayList();
    private Integer lastPart = 0;
    private Integer firstPart = 0;

    /**
     * FXML variables
     */
    @FXML
    private Button btnConfirmar;
    @FXML
    private Button btnCancelar;
    @FXML
    private TableView<GestaoDeLote> tblLotes;
    @FXML
    private TextField tboxNumeroLote;
    @FXML
    private Label lbLoteNaoEncontrado;
    @FXML
    private TextField tboxDataInicio;
    @FXML
    private TextField tboxDataFim;
    @FXML
    private TextField tboxQuantidade;
    @FXML
    private TextField tboxColecao;
    @FXML
    private Button btnAdicionarLote;
    @FXML
    private Button btnLimpar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        daoPeriodo = DAOFactory.getPeriodoDAO();
        if (!parts.isEmpty()) {
            lastPart = this.getLastPart() + 1;
            firstPart = Integer.parseInt(parts.get(0).getLote());
        }
        tboxNumeroLote.setText(lastPart + "");
        GestaoDeLote lote = null;
        try {
            lote = daoPeriodo.getByPrazo(tboxNumeroLote.getText());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCreateNewPartController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (lote != null) {
            tboxDataInicio.setText(lote.getData_inicio());
            tboxDataFim.setText(lote.getData_fim());
            lbLoteNaoEncontrado.setVisible(false);
        } else {
            lbLoteNaoEncontrado.setVisible(true);
        }
        tboxNumeroLote.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    GestaoDeLote lote;
                    try {
                        if (!tboxNumeroLote.getText().isEmpty()) {
                            lote = daoPeriodo.getByPrazo(tboxNumeroLote.getText());
                            if (lote != null) {
                                tboxDataInicio.setText(lote.getData_inicio());
                                tboxDataFim.setText(lote.getData_fim());
                                lbLoteNaoEncontrado.setVisible(false);
                                tboxQuantidade.requestFocus();
                            } else {
                                lbLoteNaoEncontrado.setVisible(true);
                            }
                        } else {
                            tboxNumeroLote.requestFocus();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                }
            }
        });
        tblLotes.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
            unbindData(oldValue);
            bindData(newValue);
        });
    }

    /**
     * LOCAL methods
     */
    private void bindData(GestaoDeLote lote) {
        if (lote != null) {
            tboxNumeroLote.textProperty().bindBidirectional(lote.loteProperty());
            tboxDataInicio.textProperty().bindBidirectional(lote.data_inicioProperty());
            tboxDataFim.textProperty().bindBidirectional(lote.data_fimProperty());
            tboxColecao.textProperty().bindBidirectional(lote.colecaoProperty());
            StringConverter<Number> converter = new NumberStringConverter();
            Bindings.bindBidirectional(tboxQuantidade.textProperty(), lote.planejadoProperty(), converter);
        }
    }

    private void unbindData(GestaoDeLote lote) {
        if (lote != null) {
            tboxNumeroLote.textProperty().unbindBidirectional(lote.loteProperty());
            tboxDataInicio.textProperty().unbindBidirectional(lote.data_inicioProperty());
            tboxDataFim.textProperty().unbindBidirectional(lote.data_fimProperty());
            tboxQuantidade.textProperty().unbindBidirectional(lote.planejadoProperty());
            tboxColecao.textProperty().unbindBidirectional(lote.colecaoProperty());
        }
    }

    private Integer getLastPart() {
        return Integer.parseInt(parts.get(parts.size() - 1).getLote());
    }

    /**
     * FXML methods
     */
    @FXML
    private void btnConfirmarOnAction(ActionEvent event) {
        newParts = tblLotes.getItems();
        Stage main = (Stage) btnCancelar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        Stage main = (Stage) btnCancelar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnAdicionarLoteOnAction(ActionEvent event) {
        tblLotes.getItems().add(new GestaoDeLote("0", tboxNumeroLote.getText(), tboxDataInicio.getText(), tboxDataFim.getText(), "100", Integer.parseInt(tboxQuantidade.getText()), null, tboxColecao.getText()));
        parts.add(new GestaoDeLote("0", tboxNumeroLote.getText(), tboxDataInicio.getText(), tboxDataFim.getText(), "100", Integer.parseInt(tboxQuantidade.getText()), null, tboxColecao.getText()));
        btnLimparOnAction(null);
        lastPart = getLastPart() + 1;
        tboxNumeroLote.setText(lastPart.toString());
        GestaoDeLote lote = null;
        try {
            lote = daoPeriodo.getByPrazo(tboxNumeroLote.getText());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCreateNewPartController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (lote != null) {
            tboxDataInicio.setText(lote.getData_inicio());
            tboxDataFim.setText(lote.getData_fim());
            lbLoteNaoEncontrado.setVisible(false);
        } else {
            lbLoteNaoEncontrado.setVisible(true);
        }
    }

    @FXML
    private void btnLimparOnAction(ActionEvent event) {

        unbindData(tblLotes.getSelectionModel().getSelectedItem());

        tboxQuantidade.clear();

    }
}
