/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.procura.SceneProcurarClienteController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ClienteFaturaAReceber;
import sysdeliz2.models.EmailBoleto;
import sysdeliz2.models.FaturaAReceber;
import sysdeliz2.models.Log;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.TableUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.sys.MailUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneFinanceiroEmailBoletosController implements Initializable {

    private ToggleSwitch tsSomenteVencidas = new ToggleSwitch(28, 50);
    private ToggleSwitch tsSomenteAVencer = new ToggleSwitch(28, 50);
    private final Image btnImage = new Image(getClass().getResource("/images/icons8-send-20.png").toExternalForm());
    Locale BRAZIL = new Locale("pt", "BR");

    @FXML
    private Button btnFecharJanela;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private Button btnProcurar;
    @FXML
    private TextField tboxCliente;
    @FXML
    private DatePicker tboxData;
    @FXML
    private Pane pnToggleAtrasados;
    @FXML
    private Pane pnToggleVencer;
    @FXML
    private TableView<ClienteFaturaAReceber> tblClientes;
    @FXML
    private TableColumn<ClienteFaturaAReceber, ClienteFaturaAReceber> clnTblClientesAcao;
    @FXML
    private TableColumn<ClienteFaturaAReceber, Boolean> clnTblClientesIsSelected;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesCodigo;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesCliente;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesTipo;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesEmail;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesTelefone;
    @FXML
    private TableColumn<ClienteFaturaAReceber, String> clnTblClientesRepresentante;
    @FXML
    private Button btnEnviarTodos;
    @FXML
    private TableView<FaturaAReceber> tblDuplicatasCliente;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClientePedido;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClienteDataEmissao;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClienteDataVencto;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClienteFatura;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClienteDuplicata;
    @FXML
    private TableColumn<FaturaAReceber, Double> clnTblDuplicatasClienteValorDuplicata;
    @FXML
    private TableColumn<FaturaAReceber, Double> clnTblDuplicatasClienteValorFatura;
    @FXML
    private TableColumn<FaturaAReceber, String> clnTblDuplicatasClienteObs;
    @FXML
    private TableView<EmailBoleto> tblEmailsEnviados;
    @FXML
    private TableColumn<EmailBoleto, String> clnTblEmailsEnviadosDataEnvio;
    @FXML
    private CheckBox chboxMarcarTudo;
    @FXML
    private Label lbToggleAtrasados;
    @FXML
    private Label lbToggleVencer;
    @FXML
    private Button btnProcuraCliente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            TableUtils.autoFitTable(tblClientes, TableUtils.COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_CLIENTES);
            TableUtils.autoFitTable(tblDuplicatasCliente, TableUtils.COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_DUPLICATAS_CLIENTE);
            TableUtils.autoFitTable(tblEmailsEnviados, TableUtils.COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_EMAILS_ENVIADOS);

            pnToggleAtrasados.getChildren().add(tsSomenteVencidas);
            tsSomenteVencidas.switchedOnProperty().set(true);
            lbToggleAtrasados.textProperty().bind(Bindings.when(tsSomenteVencidas.switchedOnProperty()).then("SIM").otherwise("NÃO"));
            pnToggleVencer.getChildren().add(tsSomenteAVencer);
            tsSomenteAVencer.switchedOnProperty().set(true);
            lbToggleVencer.textProperty().bind(Bindings.when(tsSomenteAVencer.switchedOnProperty()).then("SIM").otherwise("NÃO"));

            this.factoryTblClientes();
            tblClientes.setItems(DAOFactory.getFaturaAReceberDAO().getAll());
            tblClientes.refresh();
            tblClientes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                unbindData(oldValue);
                bindData(newValue);
            });
            this.factoryColumnsTblClientes();

            tboxData.setValue(LocalDate.now());
            tblEmailsEnviados.setItems(DAOFactory.getFaturaAReceberDAO().getLogMailDia(tboxData.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
            this.factoryColumnsTblLogMail();

        } catch (SQLException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void factoryTblClientes() {
        clnTblClientesIsSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnTblClientesIsSelected.setCellValueFactory((cellData) -> {
            ClienteFaturaAReceber cellValue = cellData.getValue();
            BooleanProperty property = cellValue.is_selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setIs_selected(newValue));
            return property;
        });

        clnTblClientesAcao.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ClienteFaturaAReceber, ClienteFaturaAReceber>, ObservableValue<ClienteFaturaAReceber>>() {
            @Override
            public ObservableValue<ClienteFaturaAReceber> call(TableColumn.CellDataFeatures<ClienteFaturaAReceber, ClienteFaturaAReceber> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnTblClientesAcao.setComparator(new Comparator<ClienteFaturaAReceber>() {
            @Override
            public int compare(ClienteFaturaAReceber p1, ClienteFaturaAReceber p2) {
                return p1.getCliente().compareTo(p2.getCliente());
            }
        });
        clnTblClientesAcao.setCellFactory(new Callback<TableColumn<ClienteFaturaAReceber, ClienteFaturaAReceber>, TableCell<ClienteFaturaAReceber, ClienteFaturaAReceber>>() {
            @Override
            public TableCell<ClienteFaturaAReceber, ClienteFaturaAReceber> call(TableColumn<ClienteFaturaAReceber, ClienteFaturaAReceber> btnCol) {
                return new TableCell<ClienteFaturaAReceber, ClienteFaturaAReceber>() {
                    final ImageView buttonGraphic = new ImageView();
                    final Button button = new Button();

                    {
                        button.setGraphic(buttonGraphic);
                        button.setMinWidth(130);
                    }

                    @Override
                    public void updateItem(final ClienteFaturaAReceber cliente, boolean empty) {
                        super.updateItem(cliente, empty);
                        if (cliente != null) {
                            if (cliente.getStatus().equals("A")) {
                                button.setText("Enviar Boleto");
                                buttonGraphic.setImage(btnImage);

                                getStyleClass().remove("statusBoletoAVencer");
                                setGraphic(button);
                                button.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        button.disableProperty().set(true);
                                        sendMail(cliente);
                                        button.disableProperty().set(false);
                                    }
                                });
                            } else {
                                getStyleClass().add("statusBoletoAVencer");
                                setText("E-mail Enviado");
                                setGraphic(null);
                            }
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });
    }

    private void factoryColumnsTblClientes() {
        clnTblClientesCodigo.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblClientesCliente.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblClientesEmail.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblClientesRepresentante.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblClientesTelefone.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblClientesTipo.setCellFactory((TableColumn<ClienteFaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<ClienteFaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ClienteFaturaAReceber currentFatura = currentRow == null ? null : (ClienteFaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getTipo_env();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLigarCliente");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "SL":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

    }

    private void factoryColumnsTblDuplicatas() {
        clnTblDuplicatasClientePedido.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteDuplicata.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteFatura.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteObs.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteDataEmissao.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    try {
                        DateFormat formatValueCell = DateFormat.getDateInstance(DateFormat.MEDIUM, BRAZIL);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        setText(empty ? null : formatValueCell.format(format.parse(getString())));
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                        if (currentFatura != null) {
                            String status = currentFatura.getStatus();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused()) {
                                setPriorityStyle(status);
                            }
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteDataVencto.setCellFactory((TableColumn<FaturaAReceber, String> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    try {
                        DateFormat formatValueCell = DateFormat.getDateInstance(DateFormat.MEDIUM, BRAZIL);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        setText(empty ? null : formatValueCell.format(format.parse(getString())));
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                        if (currentFatura != null) {
                            String status = currentFatura.getStatus();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused()) {
                                setPriorityStyle(status);
                            }
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteValorDuplicata.setCellFactory((TableColumn<FaturaAReceber, Double> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

        clnTblDuplicatasClienteValorFatura.setCellFactory((TableColumn<FaturaAReceber, Double> param) -> {
            TableCell cell = new TableCell<FaturaAReceber, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    FaturaAReceber currentFatura = currentRow == null ? null : (FaturaAReceber) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "A":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                        case "B":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
    }

    private void factoryColumnsTblLogMail() {
        clnTblEmailsEnviadosDataEnvio.setCellFactory((TableColumn<EmailBoleto, String> param) -> {
            TableCell cell = new TableCell<EmailBoleto, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    try {
                        DateFormat formatValueCell = DateFormat.getDateInstance(DateFormat.MEDIUM, BRAZIL);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        setText(empty ? null : formatValueCell.format(format.parse(getString())));
                        setGraphic(null);
                    } catch (ParseException ex) {
                        Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });
    }

    private void bindData(ClienteFaturaAReceber cliente) {
        if (cliente != null) {
            tblDuplicatasCliente.setItems(cliente.faturasProperty());
        }
        this.factoryColumnsTblDuplicatas();
    }

    private void unbindData(ClienteFaturaAReceber cliente) {
        if (cliente != null) {
            tblDuplicatasCliente.setItems(FXCollections.observableArrayList());
        }
    }

    private void sendMail(ClienteFaturaAReceber cliente) {
        try {
            for (FaturaAReceber fatura : cliente.faturasProperty().get()) {
                if (fatura.getStatus().equals("A")) {
                    MailUtils.sendMailBoletoAberto(cliente.getEmail(), cliente);
                    break;
                }
            }
            for (FaturaAReceber fatura : cliente.faturasProperty().get()) {
                if (fatura.getStatus().equals("B")) {
                    MailUtils.sendMailBoletoVencido(cliente.getEmail(), cliente);
                    break;
                }
            }

            String boletosVencidos = "Vencidos: ", boletosAbertos = " À vencer: ";
            for (FaturaAReceber fatura : cliente.faturasProperty().get()) {
                if (fatura.getStatus().equals("A")) {
                    boletosAbertos += fatura.getDuplicata() + ", ";
                }
                if (fatura.getStatus().equals("B")) {
                    boletosVencidos += fatura.getDuplicata() + ", ";
                }
            }
            DAOFactory.getFaturaAReceberDAO().saveLogMailBoleto(cliente.getCod_cli(), SceneMainController.getAcesso().getUsuario(), (boletosVencidos + boletosAbertos),
                    tboxData.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            DateFormat formatDateValue = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
            tblEmailsEnviados.getItems().add(new EmailBoleto(cliente.getCod_cli(), cliente.getCliente(), (boletosVencidos + boletosAbertos),
                    SceneMainController.getAcesso().getUsuario(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    tboxData.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
            cliente.setStatus("E");
            tblClientes.refresh();
            tblEmailsEnviados.refresh();
            DAOFactory.getLogDAO().saveLog(new Log(null, SceneMainController.getAcesso().getUsuario(), "Enviar", "Boletos Cliente",
                    cliente.getCod_cli(), "Envio de aviso de boleto por e-mail. Cli.: " + cliente.getCliente() + " Boletos: " + (boletosVencidos + boletosAbertos)));

        } catch (MessagingException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (ParseException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void chboxMarcarTudoOnMouseClicked(MouseEvent event) {
        tblClientes.getItems().forEach(clientes -> {
            clientes.is_selectedProperty().set(chboxMarcarTudo.selectedProperty().get());
        });
    }

    @FXML
    private void btnEnviarTodosOnAction(ActionEvent event) {
        btnEnviarTodos.disableProperty().set(true);
        for (ClienteFaturaAReceber cliente : tblClientes.getItems()) {
            if (cliente.isIs_selected()) {
                sendMail(cliente);
            }
        }
        btnEnviarTodos.disableProperty().set(false);
    }

    @FXML
    private void btnProcuraClienteOnAction(ActionEvent event) {
        try {
            SceneProcurarClienteController ctrlSceneFamilia = new SceneProcurarClienteController(Modals.FXMLWindow.SceneProcurarCliente);
            if (!ctrlSceneFamilia.cods_retorno.isEmpty()) {
                tboxCliente.setText(ctrlSceneFamilia.cods_retorno);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblClientes.setItems(FXCollections.observableArrayList());
            tblClientes.getItems().clear();
            this.factoryTblClientes();
            tblClientes.setItems(DAOFactory.getFaturaAReceberDAO().getByForm(tboxCliente.getText(), tboxData.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    tsSomenteVencidas.switchedOnProperty().get(), tsSomenteAVencer.switchedOnProperty().get()));
            tblEmailsEnviados.setItems(DAOFactory.getFaturaAReceberDAO().getLogMailDia(tboxData.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
            this.factoryColumnsTblLogMail();
            this.factoryColumnsTblClientes();
        } catch (SQLException ex) {
            Logger.getLogger(SceneFinanceiroEmailBoletosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxClienteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcuraClienteOnAction(null);
        }
    }

}
