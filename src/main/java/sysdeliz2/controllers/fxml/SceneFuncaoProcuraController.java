/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.ProdutoDAO;
import sysdeliz2.models.ProdutosMarketing;
import sysdeliz2.utils.GUIUtils;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneFuncaoProcuraController implements Initializable {

    
    @FXML private Button btnSair;
    @FXML private Button btnUsar;
    @FXML private TextField tboxCodigo;
    @FXML private TextField tboxDescricao;
    @FXML private Button btnProcurar;
    @FXML private TableView<ProdutosMarketing> tblProdutos;

    private String returnValue = "";
    private Stage main;
    
    public String getReturnValue() {
        return returnValue;
    }
    
    private ProdutoDAO daoProduto;
    private List<ProdutosMarketing> loadData;   
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        daoProduto = DAOFactory.getProdutoDAO();
        try {
            loadData = daoProduto.loadProdutosMarketing();
            tblProdutos.setItems(FXCollections.observableArrayList(loadData));
        } catch (SQLException ex) {
            Logger.getLogger(SceneFuncaoProcuraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        
        btnUsar.disableProperty().bind(tblProdutos.getSelectionModel().selectedItemProperty().isNull());
    }    

    @FXML
    public void btnSairOnAction(ActionEvent event) {
        returnValue = "";
        main = (Stage) tblProdutos.getScene().getWindow();
        main.close();
    }

    @FXML
    public void btnUsarOnAction(ActionEvent event) {
        returnValue = ((ProdutosMarketing) tblProdutos.getSelectionModel().getSelectedItem()).getStrCodigo();
        main = (Stage) tblProdutos.getScene().getWindow();
        main.close();
    }

    @FXML
    public void btnProcurarOnAction(ActionEvent event) {
        try {
            loadData = daoProduto.loadProdutosMarketingByForm(tboxCodigo.getText(), tboxDescricao.getText());
            tblProdutos.setItems(FXCollections.observableArrayList(loadData));
        } catch (SQLException ex) {
            Logger.getLogger(SceneFuncaoProcuraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
}
