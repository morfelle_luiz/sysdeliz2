/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.LiberacaoPcp;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneHistLiberacaoPcpController extends Modals implements Initializable {

    String codigo, colecao;
    ObservableList<LiberacaoPcp> listLiberacao;
    Integer registro = 0;

    // <editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private Button btnBack;
    @FXML
    private Label lbReg;
    @FXML
    private Button btnNext;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxColecao;
    @FXML
    private TextField tboxStatus;
    @FXML
    private TextField tboxData;
    @FXML
    private TextField tboxUsuario;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private Button btnFechar;
    // </editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            listLiberacao = DAOFactory.getGestaoProducaoDAO().getHistLiberacaoCodigo(codigo, colecao);
            if (!listLiberacao.isEmpty()) {
                btnNext.disableProperty().set(false);
                this.unbind();
                this.bind(listLiberacao.get(registro));
                lbReg.textProperty().set(registro + 1 + "");
                if (listLiberacao.size() == 1) {
                    btnNext.disableProperty().set(true);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneHistLiberacaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    public SceneHistLiberacaoPcpController(FXMLWindow file, String codigo, String colecao) {
        super(file);
        this.codigo = codigo;
        this.colecao = colecao;
    }

    public void show() throws IOException {
        super.show(this);
    }

    private void bind(LiberacaoPcp liberacao) {
        tboxCodigo.textProperty().bind(liberacao.codigoProperty());
        tboxColecao.textProperty().bind(liberacao.colecaoProperty());
        tboxData.textProperty().bind(liberacao.dt_manutencaoProperty());
        tboxObservacao.textProperty().bind(liberacao.obsProperty());
        tboxStatus.textProperty().bind(liberacao.statusProperty());
        tboxUsuario.textProperty().bind(liberacao.usuarioProperty());
    }

    private void unbind() {
        tboxCodigo.textProperty().unbind();
        tboxColecao.textProperty().unbind();
        tboxData.textProperty().unbind();
        tboxObservacao.textProperty().unbind();
        tboxStatus.textProperty().unbind();
        tboxUsuario.textProperty().unbind();
    }

    @FXML
    private void btnBackOnAction(ActionEvent event) {
        btnNext.disableProperty().set(false);
        registro--;
        lbReg.textProperty().set(registro + 1 + "");
        this.unbind();
        this.bind(listLiberacao.get(registro));
        if (registro == 0) {
            btnBack.disableProperty().set(true);
        }
    }

    @FXML
    private void btnNextOnAction(ActionEvent event) {
        btnBack.disableProperty().set(false);
        registro++;
        lbReg.textProperty().set(registro + 1 + "");
        this.unbind();
        this.bind(listLiberacao.get(registro));
        if (registro == listLiberacao.size() - 1) {
            btnNext.disableProperty().set(true);
        }
    }

    @FXML
    private void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }

}
