/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.HistoricoRevisaoMeta;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneHistRevisaoPcpController extends Modals implements Initializable {

    String codigo, colecao, usuario;
    ObservableList<HistoricoRevisaoMeta> listLiberacao;
    Integer registro = 0;

    @FXML
    private Button btnFechar;
    @FXML
    private Button btnNovoHistorico;
    @FXML
    private Button btnBack;
    @FXML
    private Label lbReg;
    @FXML
    private Button btnNext;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxColecao;
    @FXML
    private TextField tboxData;
    @FXML
    private TextField tboxUsuario;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private TextField tboxRevisao;

    public SceneHistRevisaoPcpController(FXMLWindow file, String codigo, String colecao, String usuario) {
        super(file);
        this.codigo = codigo;
        this.colecao = colecao;
        this.usuario = usuario;
    }

    public void show() throws IOException {
        super.show(this);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            listLiberacao = DAOFactory.getGestaoProducaoDAO().getHistRevisaoMeta(codigo, colecao);
            if (!listLiberacao.isEmpty()) {
                btnNext.disableProperty().set(false);
                this.unbind();
                this.bind(listLiberacao.get(registro));
                lbReg.textProperty().set(registro + 1 + "");
                if (listLiberacao.size() == 1) {
                    btnNext.disableProperty().set(true);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneHistLiberacaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void bind(HistoricoRevisaoMeta liberacao) {
        tboxCodigo.textProperty().bind(liberacao.codigoProperty());
        tboxColecao.textProperty().bind(liberacao.colecaoProperty());
        tboxData.textProperty().bind(liberacao.dt_revisaoProperty());
        tboxObservacao.textProperty().bind(liberacao.obsProperty());
        tboxRevisao.textProperty().bind(liberacao.revisaoProperty());
        tboxUsuario.textProperty().bind(liberacao.usuarioProperty());
    }

    private void unbind() {
        tboxCodigo.textProperty().unbind();
        tboxColecao.textProperty().unbind();
        tboxData.textProperty().unbind();
        tboxObservacao.textProperty().unbind();
        tboxRevisao.textProperty().unbind();
        tboxUsuario.textProperty().unbind();
    }

    @FXML
    private void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnNovoHistoricoOnAction(ActionEvent event) {
        tboxObservacao.editableProperty().set(true);
        tboxCodigo.textProperty().set(codigo);
        tboxColecao.textProperty().set(colecao);
        tboxData.textProperty().set(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(LocalDateTime.now()));
        tboxRevisao.textProperty().set((listLiberacao.size() + 1) + "");
        tboxUsuario.textProperty().set(usuario);
    }

    @FXML
    private void btnBackOnAction(ActionEvent event) {
        btnNext.disableProperty().set(false);
        registro--;
        lbReg.textProperty().set(registro + 1 + "");
        this.unbind();
        this.bind(listLiberacao.get(registro));
        if (registro == 0) {
            btnBack.disableProperty().set(true);
        }
    }

    @FXML
    private void btnNextOnAction(ActionEvent event) {
        btnBack.disableProperty().set(false);
        registro++;
        lbReg.textProperty().set(registro + 1 + "");
        this.unbind();
        this.bind(listLiberacao.get(registro));
        if (registro == listLiberacao.size() - 1) {
            btnNext.disableProperty().set(true);
        }
    }

    @FXML
    private void tboxObservacaoOnKeyRelease(KeyEvent event) {
        if (event.isShiftDown() && event.getCode() == KeyCode.ENTER) {
            try {
                DAOFactory.getGestaoProducaoDAO().saveHistoricoRevMeta(tboxCodigo.getText(), tboxColecao.getText(),
                        tboxObservacao.getText(), tboxUsuario.getText(), tboxRevisao.getText());

                registro = listLiberacao.size() - 1;
                listLiberacao.add(new HistoricoRevisaoMeta(tboxData.getText(), tboxCodigo.getText(), tboxColecao.getText(),
                        tboxObservacao.getText(), tboxUsuario.getText(), tboxRevisao.getText()));
                this.btnNextOnAction(null);
            } catch (SQLException ex) {
                Logger.getLogger(SceneHistRevisaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

}
