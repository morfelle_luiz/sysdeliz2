/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import sysdeliz2.utils.Globals;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneLoginController extends StackPane {

    protected static Random random = new Random();
    protected static int colecao;

    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void btnSairOnAction(ActionEvent event) {
        Globals.getMainStage().close();
    }

}
