/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.ColorInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdAcessoTela;
import sysdeliz2.models.sysdeliz.SdTela;
import sysdeliz2.utils.AcessoSistema;
import sysdeliz2.utils.DraggingTabPaneSupport;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.StaticVersion;
import sysdeliz2.utils.gui.components.FormBoxPane;
import sysdeliz2.utils.gui.components.FormContextMenu;
import sysdeliz2.utils.gui.components.FormTab;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import tornadofx.View;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SceneMainController implements Initializable {

    private static AcessoSistema usuarioLogado;
    private int colecao;
    private static TabPane tabPaneGlobal;
    public static String styleSheet;
    private List<SdTela> acessos;
    List<SdTela> menusSecundarios;

    public SceneMainController(AcessoSistema usuarioLogado) {
        SceneMainController.usuarioLogado = usuarioLogado;
        Globals.setUsuarioLogado(usuarioLogado);
        colecao = SceneLoginController.colecao;
    }

    public static AcessoSistema getAcesso() {
        return usuarioLogado;
    }

    public static TabPane getTabPaneGlobal() {
        return tabPaneGlobal;
    }

    //<editor-fold desc="FXML Components">
    @FXML
    private AnchorPane telaPrincipal;
    @FXML
    private BorderPane borderPaneMain;
    @FXML
    private Menu menuBoasVindas;
    @FXML
    private ImageView logoMenuBoasVindas;
    @FXML
    private Label lbEmpresaLogada;
    @FXML
    private Label lbDataHora;
    @FXML
    private Label lbUsuarioLogado;
    @FXML
    private Label lbDisplayName;
    @FXML
    private Label lbVersaoSistema;
    @FXML
    private StackPane stackMain;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabHome;
    @FXML
    private MenuBar menuBar;
    @FXML
    private TextField buscarCodTelaField;
    @FXML
    private VBox boxTelasFavoritas;
    @FXML
    private ImageView primeiraImagem;
    @FXML
    private ImageView segundaImagem;
    @FXML
    private ImageView logoEmpresa;
    @FXML
    private ImageView logoPrimeiraMarca;
    @FXML
    private ImageView logoSegundaMarca;
    @FXML
    private Menu menuFavoritos;
    //</editor-fold>

    private void openTab(SdTela tela) {
        if (tabPane.getTabs().stream().noneMatch(it -> it.getId().equals(tela.getCodTela()))) {
            FormTab tab = new FormTab();
            tab.setText(tela.getNomeTela() + "  ");
            tab.setId(tela.getCodTela());
            tab.setGraphic(criarBoxIcones(tela));
            tab.setContent(getNode(tela));
            tab.setContextMenu(criarContextMenuTab(tab));
            tab.closable();
            if (tab.getContent() != null) {
                tabPane.getTabs().add(tab);
                tabPane.getSelectionModel().select(tab);
            }
        } else {
            tabPane.getSelectionModel()
                    .select(tabPane.getTabs().stream()
                            .filter(it -> it.getId().equals(tela.getCodTela()))
                            .findFirst()
                            .get());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        carregaImagens();
        carregaMenus();
        carregaMenuFavoritos();
        configuraCampoDeBuscaCodTela();
        configuraCtrl();
//        carregaTabHome();

        tabHome.setClosable(false);
        DraggingTabPaneSupport dts = new DraggingTabPaneSupport();
        dts.addSupport(tabPane);
        Globals.getMainStage().getScene().getStylesheets().remove("/styles/sysDelizDesktop.css");
        Globals.getMainStage().getScene().getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");
        tabHome.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.HOME, ImageUtils.IconSize._16));
        lbVersaoSistema.setText("v. " + StaticVersion.versaoSistema);
        lbUsuarioLogado.setText(usuarioLogado.getUsuario());
        lbDataHora.setText(usuarioLogado.getDataAcesso() + " " + usuarioLogado.getHoraAcesso());
        menuBoasVindas.setText("Olá " + usuarioLogado.getApelido() + "!");
        lbDisplayName.setText(usuarioLogado.getDisplayName());
        usuarioLogado.getListPermissoesUsuario().forEach(grupo -> System.out.println("Grupo: " + grupo));
        tabPaneGlobal = tabPane;

        /* POG
         * Necessário para evitar a sensação de "travamento" ao pedir um entity manager pela primeira vez. Visto que o factory ainda não foi criado;
         */
    }

    private void carregaTabHome() {
        tabHome.setContent(FormBoxPane.create(root -> {
            root.expanded();
            int codFoto = new Random().nextInt(4);
            int codMarca = new Random().nextInt(2);

            root.setStyle(
//                    "-fx-background-image: url(" + "file:///K:/Java_NFE/Imagens%20Background/" + (codMarca == 1 ? "D" : "F") + codFoto + ".jpg" + "); " +
                    "-fx-background-repeat: no-repeat; " +
                            "-fx-background-position: center center;" +
//                            "-fx-background-color: rgba(255, 255, 255, 0.8); " +
//                            "-fx-opacity: 0.2;" +
                            "-fx-background-size: cover;"
            );

            ColorAdjust monochrome = new ColorAdjust();
            monochrome.setSaturation(-1.0);

            Blend blush = new Blend(
                    BlendMode.MULTIPLY,
                    monochrome,
                    new ColorInput(
                            0,
                            0,
                            500,
                            500,
                            Color.RED
                    )
            );
            ImageView imgView = null;
            try {
                imgView = new ImageView(new Image(Files.newInputStream(Paths.get("K:\\Java_NFE\\Imagens Background\\D0.jpg"))));
//                imgView.setEffect(blush);
                root.setBackground(new Background(new BackgroundImage(imgView.getImage(), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
//                root.center(imgView);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    private void carregaMenus() {
        List<SdTela> menusPrincipais = (List<SdTela>) new FluentDao().selectFrom(SdTela.class)
                .where(it -> it
                        .equal("menu", true)
                        .isNull("idMenu")
                ).orderBy("ordem", OrderType.ASC).resultList();

        menusSecundarios = (List<SdTela>) new FluentDao().selectFrom(SdTela.class)
                .where(it -> it
                        .equal("ativo", true)
                        .isNotNull("idMenu"))
                .resultList();

        List<SdAcessoTela> telasUsuario = ((List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class)
                .where(it -> it
                        .equal("id.nomeUsu", Globals.getUsuarioLogado().getUsuario()))
                .resultList());


        if (Globals.getUsuarioLogado().getListPermissoesUsuario().size() == 0) {
            return;
        }

        acessos = telasUsuario.stream().map(it -> it.getId().getIdTela()).collect(Collectors.toList());

        for (SdTela menuPrincipal : menusPrincipais.stream().filter(it -> Globals.getUsuarioLogado().getListPermissoesUsuario().stream().anyMatch(eb -> eb.equals(it.getGrupoAldap()))).collect(Collectors.toList())) {
            Menu menu = new Menu(menuPrincipal.getNomeTela());
            ImageView imageView = getIconeItem(menuPrincipal, 30);
            menu.setGraphic(imageView);
            menu.setMnemonicParsing(false);
            menu.setId(String.valueOf(menuPrincipal.getId()));

            List<SdTela> itens = menusSecundarios.stream().filter(menuSec -> menuSec.getIdMenu().equals(menuPrincipal)).sorted(Comparator.comparing(SdTela::getOrdem)).collect(Collectors.toList());
            menuBar.getMenus().add(menu);
            gerarItens(menu, menuPrincipal, itens);
        }
    }

    private void gerarItens(Menu menu, SdTela menuTela, List<SdTela> itens) {

        for (SdTela tela : itens) {
            ImageView imageView = getIconeItem(tela, 20);

            if (tela.isMenu()) {
                Menu menuFilho = new Menu(tela.getNomeTela());
                menuFilho.setGraphic(imageView);
                menuFilho.setId(String.valueOf(tela.getId()));
                menuFilho.setMnemonicParsing(true);
                List<SdTela> itensMenu = menusSecundarios.stream().filter(menuSec -> menuSec.getIdMenu().equals(tela)).sorted(Comparator.comparing(SdTela::getOrdem)).collect(Collectors.toList());
                if (itensMenu != null && itensMenu.size() > 0) {
                    menu.getItems().add(menuFilho);
                    gerarItens(menuFilho, tela, itensMenu);
                }
            } else {
                MenuItem menuItem = new MenuItem(tela.getCodTela() + " " + tela.getNomeTela());
                menuItem.setId(tela.getCodTela());
                menuItem.setGraphic(imageView);
                menuItem.setMnemonicParsing(true);
                menuItem.setDisable(acessos.stream().noneMatch(it -> it.getId() == tela.getId()));
                menuItem.setOnAction(evt -> {
                    openTab(tela);
                });
                menu.getItems().add(menuItem);
            }
        }
    }

    private ImageView getIconeItem(SdTela tela, int tamanhoIcone) {
        ImageView imageView = new ImageView();
        try {
            imageView = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
        } catch (Exception e) {
            imageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/alert warning_50.png")));
        }
        imageView.setFitHeight(tamanhoIcone);
        imageView.setFitWidth(tamanhoIcone);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        return imageView;
    }

    private void clearMenus() {
        menuBar.getMenus().removeIf(it -> menuBar.getMenus().indexOf(it) != 0 && menuBar.getMenus().indexOf(it) != 1);
    }

    private void carregaImagens() {
        Random random = new Random();
        int numeroPrimeiraFoto = random.nextInt(10);
        try {
            primeiraImagem.setImage(new Image(new FileInputStream("K:\\Java_NFE\\Imagens Background\\" + Globals.getEmpresaLogada().getCodigo() + "\\Opcao " + colecao + "\\" + numeroPrimeiraFoto + ".jpeg")));

            int numeroSegundaFoto = numeroPrimeiraFoto;
            while (numeroSegundaFoto == numeroPrimeiraFoto) {
                numeroSegundaFoto = random.nextInt(10);
            }

            segundaImagem.setImage(new Image(new FileInputStream("K:\\Java_NFE\\Imagens Background\\" + Globals.getEmpresaLogada().getCodigo() + "\\Opcao " + colecao + "\\" + numeroSegundaFoto + ".jpeg")));
            if (Globals.getEmpresaLogada().getCodigo().equals("2000")) {

                logoEmpresa.setImage(new Image((this.getClass().getResource("/images/logo-upwave.png").toExternalForm())));
                logoMenuBoasVindas.setImage(new Image(this.getClass().getResource("/images/icon-upwave.png").toExternalForm()));
                logoPrimeiraMarca.setImage(null);
                logoSegundaMarca.setImage(null);
            } else {

                logoEmpresa.setImage(new Image(this.getClass().getResource("/images/fg_bg_r.jpg").toExternalForm()));
                logoMenuBoasVindas.setImage(new Image(this.getClass().getResource("/images/logo-deliz.png").toExternalForm()));
                logoPrimeiraMarca.setImage(new Image(this.getClass().getResource("/images/logo-flor-de-liz-branca.png").toExternalForm()));
                logoSegundaMarca.setImage(new Image(this.getClass().getResource("/images/logo-dlz-branca.png").toExternalForm()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void removeFavorito(SdAcessoTela tela) {
        if (tela != null) {
            tela.setFavorita(false);
            new FluentDao().merge(tela);
        }
        carregaMenuFavoritos();
    }

    public void adicionaFavorito(SdAcessoTela tela) {
        if (tela != null) {
            tela.setFavorita(true);
            new FluentDao().merge(tela);
        }
        carregaMenuFavoritos();
    }

    public void carregaMenuFavoritos() {
        menuFavoritos.getItems().clear();
        boxTelasFavoritas.getChildren().clear();

        List<SdAcessoTela> telasFavoritas = (List<SdAcessoTela>) new FluentDao()
                .selectFrom(SdAcessoTela.class)
                .where(it -> it.equal("id.nomeUsu", Globals.getUsuarioLogado().getUsuario()).equal("favorita", true)).resultList();

        for (SdAcessoTela telasFavorita : telasFavoritas) {
            try {
                SdTela tela = telasFavorita.getId().getIdTela();

                MenuItem newMenu = new MenuItem();
                newMenu.setText(tela.getCodTela() + " " + tela.getNomeTela());

                ImageView graphic = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
                graphic.setFitWidth(20.0);
                graphic.setFitHeight(20.0);
                newMenu.setGraphic(graphic);

                newMenu.setOnAction(evt -> openTab(tela));

                menuFavoritos.getItems().add(newMenu);
                HBox linha = new HBox();
                Button buttonRemove = new Button();

                buttonRemove.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                buttonRemove.setOnAction(event -> {
                    removeFavorito(telasFavorita);
                });
                buttonRemove.getStyleClass().add("danger");

                Button button = new Button();
                button.setText(tela.getCodTela() + " " + tela.getNomeTela());

                ImageView graphicBtn = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
                graphicBtn.setFitWidth(20.0);
                graphicBtn.setFitHeight(20.0);
                button.setGraphic(graphicBtn);
                button.setOnAction(evt -> openTab(tela));
                button.setPrefWidth(220);
                button.setAlignment(Pos.CENTER_LEFT);
                linha.setMargin(button, new Insets(2, 5, 2, 0));
                linha.setMargin(buttonRemove, new Insets(2, 5, 2, 0));
                linha.getChildren().add(button);
                linha.getChildren().add(buttonRemove);

                boxTelasFavoritas.getChildren().add(linha);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void configuraCtrl() {
        AtomicBoolean ctrl = new AtomicBoolean(false);
        borderPaneMain.setOnKeyPressed(event -> {
            if (event.getCode().isDigitKey() && ctrl.get()) {
                int numero = Integer.parseInt(event.getCode().getName());
                if (tabPane.getTabs().size() >= numero) {
                    tabPane.getSelectionModel().select(numero - 1);
                }
            }
            if (event.getCode() == KeyCode.CONTROL) ctrl.set(true);
        });
        borderPaneMain.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.CONTROL) ctrl.set(false);
        });
    }

    private void configuraCampoDeBuscaCodTela() {
        buscarCodTelaField.setPromptText("Buscar Tela");
        buscarCodTelaField.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER && buscarCodTelaField.getText() != null && !buscarCodTelaField.getText().equals("")) {
                SdAcessoTela acessoTela = new FluentDao()
                        .selectFrom(SdAcessoTela.class)
                        .where(it -> it
                                .equal("id.nomeUsu", Globals.getUsuarioLogado().getUsuario())
                                .equal("id.idTela.codTela", buscarCodTelaField.getText()))
                        .singleResult();

                if (acessoTela == null) {
                    MessageBox.create(message -> {
                        message.message("Você não tem acesso a esta tela, favor entrar em contato com a TI.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                    buscarCodTelaField.clear();
                    return;
                } else {
                    buscarCodTelaField.clear();
                    openTab(acessoTela.getId().getIdTela());
                }
            }
        });
    }

    private boolean usuarioTemAcessoTela(String codTela) {
        return new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.nomeUsu", Globals.getUsuarioLogado().getUsuario()).equal("id.codTela.codTela", codTela)).singleResult() != null;
    }

    private ContextMenu criarContextMenuTab(FormTab tab) {
        FormContextMenu contextMenu = new FormContextMenu();

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar");
            menuItem.setOnAction(evt -> {
                tabPane.getTabs().remove(tab);
            });
        });

        contextMenu.addSeparator();

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar Todas");
            menuItem.setOnAction(evt -> {
                tabPane.getTabs().removeIf(it -> tabPane.getTabs().indexOf(it) != 0);
            });
        });

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar Outras");
            menuItem.setOnAction(evt -> {
                tabPane.getTabs().removeIf(it -> it != tab && tabPane.getTabs().indexOf(it) != 0);
            });
        });

        contextMenu.addSeparator();

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar à Esquerda");
            menuItem.setOnAction(evt -> {
                if (tabPane.getTabs().indexOf(tab) != 1) {
                    tabPane.getTabs().remove(tabPane.getTabs().indexOf(tab) - 1);
                }
            });
        });

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar à Direita");
            menuItem.setOnAction(evt -> {
                if (tabPane.getTabs().indexOf(tab) != tabPane.getTabs().size() - 1) {
                    tabPane.getTabs().remove(tabPane.getTabs().indexOf(tab) + 1);
                }
            });
        });

        contextMenu.addSeparator();

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar Todas à Esquerda");
            menuItem.setOnAction(evt -> {
                if (tabPane.getTabs().indexOf(tab) != 1) {
                    tabPane.getTabs().removeAll(tabPane.getTabs().stream().filter(it -> tabPane.getTabs().indexOf(it) != 0).filter(it -> tabPane.getTabs().indexOf(it) < tabPane.getTabs().indexOf(tab)).collect(Collectors.toList()));
                }
            });
        });

        contextMenu.addItem(menuItem -> {
            menuItem.setText("Fechar Todas à Direita");
            menuItem.setOnAction(evt -> {
                if (tabPane.getTabs().indexOf(tab) != tabPane.getTabs().size() - 1) {
                    tabPane.getTabs().removeAll(tabPane.getTabs().stream().filter(it -> tabPane.getTabs().indexOf(it) > tabPane.getTabs().indexOf(tab)).collect(Collectors.toList()));
                }
            });
        });

        return contextMenu;
    }

    private Node getNode(SdTela tela) {
        Node node = null;
        try {
            switch (tela.getTipo().name()) {
                case "JV":
                    node = (Node) Class.forName(tela.getPath()).newInstance();
                    break;
                case "KT":
                    node = ((View) Class.forName(tela.getPath()).newInstance()).getRoot();
                    break;
                case "FX":
                    node = new FXMLLoader(getClass().getResource(tela.getPath())).load();
                    break;
            }
        } catch (IOException | ClassCastException | InstantiationException | ClassNotFoundException |
                 IllegalAccessException e1) {
            e1.printStackTrace();
            ExceptionBox.build(message -> {
                message.message("Aconteceu um erro durante a abertura da tela. Informe o erro ao TI.");
                message.exception(e1);
                message.showAndWait();
            });
        }
        return node;
    }

    private HBox criarBoxIcones(SdTela tela) {
        ImageView iconTab = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
        iconTab.setFitHeight(19.0);
        iconTab.setFitWidth(19.0);
        SdAcessoTela telaAcesso = new FluentDao()
                .selectFrom(SdAcessoTela.class)
                .where(it -> it
                        .equal("id.idTela.id", tela.getId())
                        .equal("id.nomeUsu", Globals.getUsuarioLogado().getUsuario()))
                .singleResult();
        HBox boxIcons = new HBox();
        if (telaAcesso != null) {
            Button favorito = new Button();
            favorito.setId(telaAcesso.getFavorita() ? "S" : "N");
            favorito.setGraphic(favorito.getId().equals("S") ? ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.N_FAVORITO, ImageUtils.IconSize._16));
            favorito.setOnAction(event -> {
                if (favorito.getId().equals("S")) {
                    removeFavorito(telaAcesso);
                    favorito.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.N_FAVORITO, ImageUtils.IconSize._16));
                    favorito.setId("N");
                } else {
                    if (menuFavoritos.getItems().size() <= 9) {
                        adicionaFavorito(telaAcesso);
                        favorito.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                        favorito.setId("S");
                    } else {
                        MessageBox.create(message -> {
                            message.message("Limite de Telas Favoritas Excedido!\nNúmero máximo de telas favoritadas é 10.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                            message.position(Pos.TOP_RIGHT);
                        });
                    }
                }
            });

            favorito.setMaxHeight(10.0);
            favorito.setMaxWidth(10.0);
            favorito.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");

            boxIcons.setAlignment(Pos.CENTER);
            boxIcons.getChildren().add(favorito);
        }
        boxIcons.getChildren().add(iconTab);
        return boxIcons;
    }

    @FXML
    private void menuMeuMenuMeusRoteirosOnAction(ActionEvent evt) {
        openTab(new SdTela("B0", "Meus Roteiros", "/images/icons/adicionar entrega_50.png", "/sysdeliz2/controllers/fxml/meumenu/SceneMeusRoteiros.fxml", SdTela.TipoTela.FX));
    }

    @FXML
    private void menuMeuMenuCadastroEntradasOnAction(ActionEvent evt) {
        openTab(new SdTela("B1", "Registro de Entradas", "/images/icons/adicionar entrada_50.png", "/sysdeliz2/controllers/fxml/meumenu/SceneRegistroDeEntradas.fxml", SdTela.TipoTela.FX));
    }

    @FXML
    private void menuMeuMenuSolicitacaoPcsExpOnAction(ActionEvent evt) {
        openTab(new SdTela("B2", "Solicitação de Peças Expedição", "/images/icons/expedicao_50.png", "sysdeliz2.views.meumenu.SolicitacaoPecasExpedicaoView", SdTela.TipoTela.JV));
    }
}
