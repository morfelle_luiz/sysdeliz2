/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import sysdeliz2.controllers.fxml.expedicao.SceneExpedicaoReservasController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.MarketingPedidoDAO;
import sysdeliz2.models.MarketingPedido;
import sysdeliz2.models.ProdutosMarketing;
import sysdeliz2.models.ProdutosMarketingPedido;
import sysdeliz2.utils.AcessoSistema;
import sysdeliz2.utils.FuncaoProcura;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneMarketingPedidoController implements Initializable {

    @FXML
    private Button btnFecharJanela;
    @FXML
    private TextField tboxCliente;
    @FXML
    private TextField tboxNumeroPedido;
    @FXML
    private Button btnProcuraReserva;
    @FXML
    private TableView<MarketingPedido> tblMktsPedidos;
    @FXML
    private Button btnEditar;
    @FXML
    private TextField tboxCadNumeroPedido;
    @FXML
    private TableView<ProdutosMarketingPedido> tblItensMktPedido;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmStatusMkt;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmCodigo;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmDescProduto;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmQtdeMkt;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmReserva;
    @FXML
    private TableColumn<ProdutosMarketingPedido, String> clmMarca;
    @FXML
    private TextField tboxCadCodigo;
    @FXML
    private TextField tboxMarca;
    @FXML
    private TextField tboxDescProduto;
    @FXML
    private TextField tboxQtdeMktPedido;
    @FXML
    private TextArea tboxObs;
    @FXML
    private Button btnAdicionar;
    @FXML
    private Button btnLimpar;
    @FXML
    private Button btnSalvarAdd;
    @FXML
    private Button btnExcluir;
    @FXML
    private Button btnCancelar;
    @FXML
    private Button btnSalvar;

    /**
     * Variáveis do Controller
     */
    private MarketingPedidoDAO daoMarketingPedido;
    private MarketingPedido mpCurrentSelected;
    private ProdutosMarketingPedido pmpCurrentSelected;
    private BooleanProperty isModeEdit = new SimpleBooleanProperty(false);
    private BooleanProperty isModeAdd = new SimpleBooleanProperty(false);
    private List<MarketingPedido> loadData;
    private List<ProdutosMarketingPedido> novosProdutos = new ArrayList();
    private String codigoProduto;
    private AcessoSistema usuarioLogado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuarioLogado = SceneMainController.getAcesso();
        daoMarketingPedido = DAOFactory.getMarketingPedidoDAO();
        GUIUtils.autoFitTable(tblMktsPedidos, GUIUtils.COLUNAS_TBL_MARKETING_PEDIDO);
        /**
         * try { loadData = daoMarketingPedido.load();
         * tblMktsPedidos.setItems(FXCollections.observableArrayList(loadData));
         * GUIUtils.autoFitTable(tblMktsPedidos,
         * GUIUtils.COLUNAS_TBL_MARKETING_PEDIDO); printTable(); } catch
         * (SQLException ex) {
         * LoggerExtensions.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE,
         * null, ex); GUIUtils.showException(ex); }*
         */

        btnEditar.disableProperty().bind(tblMktsPedidos.getSelectionModel().selectedItemProperty().isNull());
        btnExcluir.disableProperty().bind(isModeEdit.not().or(tblItensMktPedido.getSelectionModel().selectedItemProperty().isNull()));
        btnSalvar.disableProperty().bind(isModeEdit.not());
        btnCancelar.disableProperty().bind(isModeEdit.not());
        btnAdicionar.disableProperty().bind(isModeEdit.not());
        btnLimpar.disableProperty().bind(isModeEdit.not().or(isModeAdd.not()));
        btnSalvarAdd.disableProperty().bind(isModeEdit.not().and(isModeAdd.not()));

        tboxCadCodigo.editableProperty().bind(isModeAdd);
        tboxQtdeMktPedido.editableProperty().bind(isModeAdd);
        tboxObs.editableProperty().bind(isModeAdd);

        tblMktsPedidos.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
            unbindData(oldValue);
            bindData(newValue);
        });

        tblItensMktPedido.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
            unbindDataItens(oldValue);
            bindDataItens(newValue);
        });

        tboxCadCodigo.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    ProdutosMarketing produto;
                    try {
                        if (!tboxCadCodigo.getText().isEmpty()) {
                            produto = DAOFactory.getProdutoDAO().getProdutosMarketingByCodigo(tboxCadCodigo.getText());
                            tboxDescProduto.setText(produto != null ? produto.getStrDescricao() : "Produto não encontrado.");
                        } else {
                            tboxDescProduto.setText("Digite o código do produto.");
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                }
            }
        });
    }

    private void printTable() {

        clmStatusMkt.setCellFactory((TableColumn<ProdutosMarketingPedido, String> param) -> {
            TableCell cell = new TableCell<ProdutosMarketingPedido, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ProdutosMarketingPedido currentReserva = currentRow == null ? null : (ProdutosMarketingPedido) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStrStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");
                    styleClasses.remove("statusExpedido");
                    styleClasses.remove("statusImpresso");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "Cadastrado":
                            getStyleClass().add("statusLido");
                            break;
                        case "Em Reserva":
                            getStyleClass().add("statusImpresso");
                            break;
                        case "Expedido":
                            getStyleClass().add("statusExpedido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmCodigo.setCellFactory((TableColumn<ProdutosMarketingPedido, String> param) -> {
            TableCell cell = new TableCell<ProdutosMarketingPedido, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ProdutosMarketingPedido currentReserva = currentRow == null ? null : (ProdutosMarketingPedido) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStrStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");
                    styleClasses.remove("statusExpedido");
                    styleClasses.remove("statusImpresso");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "Cadastrado":
                            getStyleClass().add("statusLido");
                            break;
                        case "Em Reserva":
                            getStyleClass().add("statusImpresso");
                            break;
                        case "Expedido":
                            getStyleClass().add("statusExpedido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmDescProduto.setCellFactory((TableColumn<ProdutosMarketingPedido, String> param) -> {
            TableCell cell = new TableCell<ProdutosMarketingPedido, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ProdutosMarketingPedido currentReserva = currentRow == null ? null : (ProdutosMarketingPedido) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStrStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");
                    styleClasses.remove("statusExpedido");
                    styleClasses.remove("statusImpresso");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "Cadastrado":
                            getStyleClass().add("statusLido");
                            break;
                        case "Em Reserva":
                            getStyleClass().add("statusImpresso");
                            break;
                        case "Expedido":
                            getStyleClass().add("statusExpedido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmReserva.setCellFactory((TableColumn<ProdutosMarketingPedido, String> param) -> {
            TableCell cell = new TableCell<ProdutosMarketingPedido, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ProdutosMarketingPedido currentReserva = currentRow == null ? null : (ProdutosMarketingPedido) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStrStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");
                    styleClasses.remove("statusExpedido");
                    styleClasses.remove("statusImpresso");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "Cadastrado":
                            getStyleClass().add("statusLido");
                            break;
                        case "Em Reserva":
                            getStyleClass().add("statusImpresso");
                            break;
                        case "Expedido":
                            getStyleClass().add("statusExpedido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clmQtdeMkt.setCellFactory((TableColumn<ProdutosMarketingPedido, String> param) -> {
            TableCell cell = new TableCell<ProdutosMarketingPedido, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ProdutosMarketingPedido currentReserva = currentRow == null ? null : (ProdutosMarketingPedido) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStrStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusImpresso");
                    styleClasses.remove("statusLido");
                    styleClasses.remove("statusExpedido");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "Em Reserva":
                            getStyleClass().add("statusImpresso");
                            break;
                        case "Cadastrado":
                            getStyleClass().add("statusLido");
                            break;
                        case "Expedido":
                            getStyleClass().add("statusExpedido");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "0" : getItem();
                }
            };
            return cell;
        });
    }

    private void bindData(MarketingPedido rpReserva) {
        if (rpReserva != null) {
            tboxCadNumeroPedido.textProperty().bind(rpReserva.strNumeroPedidoProperty());
            tblItensMktPedido.setItems(FXCollections.observableArrayList(rpReserva.listProdutosProperty()));
        }
    }

    private void unbindData(MarketingPedido rpReserva) {
        if (rpReserva != null) {
            tboxCadNumeroPedido.textProperty().unbind();
            tblItensMktPedido.setItems(FXCollections.observableArrayList());

            clearForm();
        }
    }

    private void bindDataItens(ProdutosMarketingPedido pmpItem) {
        if (pmpItem != null) {
            tboxDescProduto.textProperty().bindBidirectional(pmpItem.strDescProdutoProperty());
            tboxObs.textProperty().bindBidirectional(pmpItem.strObsProperty());
            tboxCadCodigo.textProperty().bindBidirectional(pmpItem.strCodigoProperty());
            tboxQtdeMktPedido.textProperty().bindBidirectional(pmpItem.intQtdeProperty());
        }
    }

    private void unbindDataItens(ProdutosMarketingPedido pmpItem) {
        if (pmpItem != null) {
            tboxDescProduto.textProperty().unbindBidirectional(pmpItem.strDescProdutoProperty());
            tboxObs.textProperty().unbindBidirectional(pmpItem.strObsProperty());
            tboxCadCodigo.textProperty().unbindBidirectional(pmpItem.strCodigoProperty());
            tboxQtdeMktPedido.textProperty().unbindBidirectional(pmpItem.intQtdeProperty());

            clearFormItens();
        }
    }

    private void clearForm() {
        tboxCadNumeroPedido.clear();
        tboxDescProduto.clear();
        tboxObs.clear();
        tboxCadCodigo.clear();
        tboxQtdeMktPedido.clear();
    }

    private void clearFormItens() {
        tboxDescProduto.clear();
        tboxObs.clear();
        tboxCadCodigo.clear();
        tboxQtdeMktPedido.clear();
    }

    private String validateForm() throws SQLException {
        StringBuilder errorMessage = new StringBuilder();

        if (StringUtils.isEmptyOrNull(pmpCurrentSelected.getStrCodigo())) {
            errorMessage.append("Preencha o campo PRODUTO.").append(StringUtils.newLine());
        }
        if (StringUtils.isEmptyOrNull(pmpCurrentSelected.getIntQtde())) {
            errorMessage.append("Preencha o campo QUANTIDADE.").append(StringUtils.newLine());
        }
        if (StringUtils.isNotInt(pmpCurrentSelected.getIntQtde())) {
            errorMessage.append("O campo QUANTIDADE deve ser um número.").append(StringUtils.newLine());
        }
        if (DAOFactory.getProdutoDAO().getProdutosMarketingByCodigo(tboxCadCodigo.getText()) == null) {
            errorMessage.append("Preencha um código de PRODUTO cadastrado.").append(StringUtils.newLine());
        }
        return errorMessage.toString();
    }

    @FXML
    public void btnProcuraReservaOnAction(ActionEvent event) {
        try {
            loadData = daoMarketingPedido.loadByForm(tboxNumeroPedido.getText() + " ", tboxCliente.getText() + " ", tboxMarca.getText() + " ");
            tblMktsPedidos.setItems(FXCollections.observableArrayList(loadData));
            GUIUtils.autoFitTable(tblMktsPedidos, GUIUtils.COLUNAS_TBL_MARKETING_PEDIDO);
            printTable();
        } catch (SQLException ex) {
            Logger.getLogger(SceneExpedicaoReservasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    public void btnEditarOnAction(ActionEvent event) {
        mpCurrentSelected = tblMktsPedidos.getSelectionModel().getSelectedItem();
        novosProdutos.clear();
        isModeEdit.set(true);
    }

    @FXML
    public void btnAdicionarOnAction(ActionEvent event) {
        tblItensMktPedido.getSelectionModel().clearSelection();
        isModeAdd.set(true);
        pmpCurrentSelected = new ProdutosMarketingPedido();
        bindDataItens(pmpCurrentSelected);
        tboxCadCodigo.requestFocus();
    }

    @FXML
    public void btnLimparOnAction(ActionEvent event) {
        unbindDataItens(pmpCurrentSelected);
        clearFormItens();
        isModeAdd.set(false);
    }

    @FXML
    public void btnSalvarAddOnAction(ActionEvent event) {
        String errorMessage = "";
        try {
            errorMessage = validateForm();
        } catch (SQLException ex) {
            Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
            return;
        }
        if (!errorMessage.isEmpty()) {
            GUIUtils.showMessage(errorMessage, Alert.AlertType.ERROR);
            return;
        }

        pmpCurrentSelected.strReservaProperty().set("0");
        pmpCurrentSelected.strStatusProperty().set("Cadastrado");
        mpCurrentSelected.listProdutosProperty().add(pmpCurrentSelected);
        tblItensMktPedido.getItems().add(pmpCurrentSelected);
        novosProdutos.add(pmpCurrentSelected);
        unbindDataItens(pmpCurrentSelected);
        clearFormItens();
        tblItensMktPedido.getSelectionModel().select(pmpCurrentSelected);
    }

    @FXML
    public void btnExcluirOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultSim("Deseja realmente excluir o marketing?")) {
            try {
                pmpCurrentSelected = tblItensMktPedido.getSelectionModel().getSelectedItem();
                daoMarketingPedido.delete(pmpCurrentSelected, mpCurrentSelected.getStrNumeroPedido());
                mpCurrentSelected.listProdutosProperty().remove(pmpCurrentSelected);
                novosProdutos.remove(pmpCurrentSelected);
                tblItensMktPedido.getItems().removeIf(tblItensMktPedido.getSelectionModel().getSelectedItem()::equals);
            } catch (SQLException ex) {
                Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    public void btnCancelarOnAction(ActionEvent event) {
        tblItensMktPedido.getSelectionModel().clearSelection();
        tblItensMktPedido.getItems().removeAll(novosProdutos);
        mpCurrentSelected.listProdutosProperty().removeAll(novosProdutos);
        clearFormItens();
        isModeAdd.set(false);
        isModeEdit.set(false);
        bindData(mpCurrentSelected);
    }

    @FXML
    public void btnSalvarOnAction(ActionEvent event) {

        try {
            daoMarketingPedido.save(mpCurrentSelected, novosProdutos, usuarioLogado);
            isModeAdd.set(false);
            isModeEdit.set(false);
            tblMktsPedidos.getSelectionModel().select(mpCurrentSelected);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    public void tboxCadCodigoOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4 && tboxCadCodigo.editableProperty().get()) {
            try {
                codigoProduto = new FuncaoProcura().showProcura(FuncaoProcura.SCENE_PROCURA_PRODUTO_MARKETING);
                tboxCadCodigo.setText(codigoProduto);
                tboxQtdeMktPedido.requestFocus();
            } catch (IOException ex) {
                Logger.getLogger(SceneMarketingPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

}
