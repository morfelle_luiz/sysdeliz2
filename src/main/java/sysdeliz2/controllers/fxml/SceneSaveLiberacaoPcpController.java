/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.GestaoProducaoDAO;
import sysdeliz2.utils.AcessoSistema;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneSaveLiberacaoPcpController extends Modals implements Initializable {

    private String codigo, colecao, descricao, novoValor, antigoValor;
    GestaoProducaoDAO daoGestao;
    AcessoSistema usuarioLogado;

    @FXML
    private Label lbProduto;
    @FXML
    private Label lbValorAnterior;
    @FXML
    private Label lbValorNovo;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private Button btnSalvar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        daoGestao = DAOFactory.getGestaoProducaoDAO();
        usuarioLogado = SceneMainController.getAcesso();
        lbProduto.setText("[" + codigo + "] " + descricao);
        lbValorAnterior.setText(antigoValor);
        lbValorNovo.setText(novoValor);
    }

    public SceneSaveLiberacaoPcpController(FXMLWindow file, String colecao, String codigo, String descricao, String novoValor, String antigoValor) {
        super(file);
        this.codigo = codigo;
        this.descricao = descricao;
        this.antigoValor = antigoValor;
        this.novoValor = novoValor;
        this.colecao = colecao;
    }

    public void show() throws IOException {
        super.show(this);
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {

        try {
            daoGestao.saveLiberacaoPcp(codigo, colecao, novoValor.toUpperCase(), tboxObservacao.getText(), usuarioLogado.getUsuario());

            Stage main = (Stage) btnSalvar.getScene().getWindow();
            main.close();
        } catch (SQLException ex) {
            Logger.getLogger(SceneSaveLiberacaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

}
