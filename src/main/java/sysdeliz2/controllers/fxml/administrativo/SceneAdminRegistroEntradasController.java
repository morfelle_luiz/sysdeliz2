/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.administrativo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdRegistroEntrada001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.ReportUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneAdminRegistroEntradasController implements Initializable {

    @FXML
    private Button btnMarcarTodos;
    @FXML
    private ChoiceBox<String> cboxStatus;
    @FXML
    private Button btnProgramar;
    @FXML
    private TableView<SdRegistroEntrada001> tblRegistroEntradas;
    @FXML
    private Button btnImprimir;
    @FXML
    private TextField tboxNomeSetor;
    @FXML
    private Button btnProcurar;
    @FXML
    private DatePicker tboxFilterDataEntrada;
    @FXML
    private DatePicker tboxFilterDataEmissao;
    @FXML
    private TableColumn<SdRegistroEntrada001, Boolean> clnSelected;
    @FXML
    private ChoiceBox<String> cboxComLanche;
    @FXML
    private TextField tboxFilterNomeFuncionario;

    //    private final Image btnImage = new Image(getClass().getResource("/images/icons8-truck-20.png").toExternalForm());
    //String selectTiposConsulta = "";
    private List<ObservableList<String>> whereClauses;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tboxFilterDataEntrada.setValue(LocalDate.now());

            whereClauses = FXCollections.observableArrayList();

            whereClauses.add(FXCollections.observableArrayList("1 = 1"));
            whereClauses.add(FXCollections.observableArrayList("AND situacao = 'Aberto'"));

            // Por data de Entrada
            LocalDate dtEntrada = tboxFilterDataEntrada.getValue();
            if (dtEntrada != null) {
                String dt_formatted = DateTimeFormatter.ofPattern("dd/MM/yyy").format(dtEntrada);
                whereClauses.add(
                        FXCollections
                                .observableArrayList(" and dh_entrada BETWEEN to_date('"
                                        + dt_formatted
                                        + " 00:00:00', 'dd/MM/yyyy HH24:MI:SS') AND to_date('"
                                        + dt_formatted
                                        + " 23:59:59', 'dd/MM/yyyy HH24:MI:SS') "));
            }

            tblRegistroEntradas.setItems(Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).getBy(whereClauses));

            this.factoryColumns();

            cboxComLanche.setItems(FXCollections.observableArrayList("Ambos", "Com Lanche", "Sem Lanche"));
            cboxStatus.setItems(FXCollections.observableArrayList("Ambos", "Abertos", "Fechado"));

            cboxComLanche.getSelectionModel().select(0);
            cboxStatus.getSelectionModel().select(1);

        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRegistroEntradasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        whereClauses = FXCollections.observableArrayList();

        whereClauses.add(FXCollections.observableArrayList("1 = 1"));

        // Por data de Entrada
        LocalDate dtEntrada = tboxFilterDataEntrada.getValue();
        if (dtEntrada != null) {
            String dt_formatted = DateTimeFormatter.ofPattern("dd/MM/yyy").format(dtEntrada);
            whereClauses.add(
                    FXCollections
                            .observableArrayList(" and dh_entrada BETWEEN to_date('"
                                    + dt_formatted
                                    + " 00:00:00', 'dd/MM/yyyy HH24:MI:SS') AND to_date('"
                                    + dt_formatted
                                    + " 23:59:59', 'dd/MM/yyyy HH24:MI:SS') "));
        }

        // Por data de Emissão
        LocalDate dtEmissao = tboxFilterDataEmissao.getValue();
        if (dtEmissao != null) {
            String dt_formatted = DateTimeFormatter.ofPattern("dd/MM/yyy").format(dtEmissao);
            whereClauses.add(
                    FXCollections
                            .observableArrayList(" and dh_emissao BETWEEN to_date('"
                                    + dt_formatted
                                    + " 00:00:00', 'dd/MM/yyyy HH24:MI:SS') AND to_date('"
                                    + dt_formatted
                                    + " 23:59:59', 'dd/MM/yyyy HH24:MI:SS') "));
        }

        // Com / Sem Lanche
        switch (cboxComLanche.getSelectionModel().getSelectedIndex()) {
            case 1:
                whereClauses.add(FXCollections.observableArrayList(" and pedir_lanche = 'S' "));
                break;
            case 2:
                whereClauses.add(FXCollections.observableArrayList(" and pedir_lanche = 'N' "));
                break;
        }

        // Status
        switch (cboxStatus.getSelectionModel().getSelectedIndex()) {
            case 1:
                whereClauses.add(FXCollections.observableArrayList(" and situacao = 'Aberto' "));
                break;
            case 2:
                whereClauses.add(FXCollections.observableArrayList(" and situacao = 'Fechado' "));
                break;
        }

        // Nome Funcionario
        if (!tboxFilterNomeFuncionario.getText().equals("")) {
            whereClauses.add(FXCollections
                    .observableArrayList(" AND (SELECT sir.nomfun FROM senior_ind.r034fun sir WHERE sir.NUMCAD = s.cod_func AND sir.NUMEMP = s.num_emp AND sir.TIPCOL = s.tipo_col) LIKE '%" + tboxFilterNomeFuncionario.getText() + "%' "));
        }

        // Nome Setor
        if (!tboxNomeSetor.getText().equals("")) {
            whereClauses.add(FXCollections
                    .observableArrayList(" AND (SELECT st.nomloc FROM senior_ind.r016orn st WHERE st.NUMLOC = s.num_loc) LIKE '%" + tboxNomeSetor.getText() + "%' "));
        }

        try {
            tblRegistroEntradas.setItems(Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).getBy(whereClauses));
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProgramarOnAction(ActionEvent event) {
        if (!hasSelected().get()) {
            GUIUtils.showMessage("Nenhum regsitro selecionado para programação.", Alert.AlertType.INFORMATION);
            return;
        }
        for (SdRegistroEntrada001 sdRegistroEntrada001 : tblRegistroEntradas.getItems().filtered(SdRegistroEntrada001::isSelected)) {
            sdRegistroEntrada001.setImpresso("N");
            fechaRegistroEntrada(sdRegistroEntrada001);
        }
        tblRegistroEntradas.refresh();
    }

    @FXML
    private void btnImprimirOnAction(ActionEvent event) {
        if (!hasSelected().get()) {
            GUIUtils.showMessage("Nenhuma registro selecionada para Impressão.", Alert.AlertType.INFORMATION);
            return;
        }
        imprimeSelecionados();
    }

    @FXML
    private void btnMarcarTodosOnAction(ActionEvent event) {
        tblRegistroEntradas.getItems().forEach(registro -> {
            registro.setSelected(!registro.isSelected());
        });
    }

    private void imprimeSelecionados() {
        try {

            List<SdRegistroEntrada001> list = new ArrayList<>();

            for (SdRegistroEntrada001 sdRegistroEntrada001 : tblRegistroEntradas.getItems().filtered(SdRegistroEntrada001::isSelected)) {
                sdRegistroEntrada001.setImpresso("N");
                fechaRegistroEntrada(sdRegistroEntrada001);
                list.add(sdRegistroEntrada001);
            }

            List<JasperPrint> registrosParaImpressao = new ArrayList<>();
            JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/RegistrosDeEntradas.jasper"));

            JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(list);
            HashMap<String, Object> parameter = new HashMap<>();

            JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parameter, beanColDataSource);
            registrosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(registrosParaImpressao, "reg_entradas");

            for (SdRegistroEntrada001 sdRegistroEntrada001 : tblRegistroEntradas.getItems().filtered(SdRegistroEntrada001::isSelected)) {
                sdRegistroEntrada001.setImpresso("S");
                sdRegistroEntrada001.setDataImpressao(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(LocalDateTime.now()));
                fechaRegistroEntrada(sdRegistroEntrada001);
            }

            tblRegistroEntradas.refresh();
            this.factoryColumns();
        } catch (JRException | IOException ex) {
            Logger.getLogger(SceneAdminRegistroEntradasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void fechaRegistroEntrada(SdRegistroEntrada001 sdRegistroEntrada001) {
        try {
            if (sdRegistroEntrada001.getSituacao().equals("Aberto")) {
                sdRegistroEntrada001.setSituacao("Fechado");
            }

            Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).updateStatus(sdRegistroEntrada001);
            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRegistroEntradasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private BooleanProperty hasSelected() {
        if (tblRegistroEntradas.getItems().filtered(SdRegistroEntrada001::isSelected).size() > 0) {
            return new SimpleBooleanProperty(true);
        } else {
            return new SimpleBooleanProperty(false);
        }
    }

    private void factoryColumns() {
        clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnSelected.setCellValueFactory((cellData) -> {
            SdRegistroEntrada001 cellDataValue = cellData.getValue();
            BooleanProperty property = cellDataValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellDataValue.setSelected(newValue));
            return property;
        });
    }

}
