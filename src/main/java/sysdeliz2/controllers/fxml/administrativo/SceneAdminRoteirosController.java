/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.administrativo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.properties.Roteiros;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.TableUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneAdminRoteirosController implements Initializable {

    private final Image btnImage = new Image(getClass().getResource("/images/icons8-truck-20.png").toExternalForm());
    String selectTiposConsulta = "";

    @FXML
    private DatePicker tboxData_C;
    @FXML
    private CheckBox chboxRetirar_C;
    @FXML
    private CheckBox chboxEntregar_C;
    @FXML
    private CheckBox chboxAmbos_C;
    @FXML
    private TextField tboxFornecedor_C;
    @FXML
    private TextField tboxCidade_C;
    @FXML
    private TextField tboxMercadoria_C;
    @FXML
    private Button btnProcurar;
    @FXML
    private TableView<Roteiros> tblRoteiros;
    @FXML
    private TableColumn<Roteiros, Boolean> clnSelected;
    @FXML
    private TableColumn<Roteiros, Roteiros> clnAction;
    @FXML
    private Button btnProgramar;
    @FXML
    private Button btnImprimir;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblRoteiros.setItems(DAOFactory.getRoteirosDAO().getAllAbertos());
            TableUtils.autoFitTable(tblRoteiros, TableUtils.COLUNAS_TBL_ADMINISTRATIVO_ADMIN_ROTEIROS);
            this.factoryColumns();

            chboxRetirar_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Retirar|");
                    } else {
                        selectTiposConsulta.replace("Retirar|", "");
                    }
                }
            });
            chboxEntregar_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Entregar|");
                    } else {
                        selectTiposConsulta.replace("Entregar|", "");
                    }
                }
            });
            chboxAmbos_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Ambos|");
                    } else {
                        selectTiposConsulta.replace("Ambos|", "");
                    }
                }
            });

            //btnProgramar.disableProperty().bind(hasSelected().not());
            //btnImprimir.disableProperty().bind(hasSelected().not());
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private BooleanProperty hasSelected() {
        for (Roteiros rota : tblRoteiros.getItems()) {
            if (rota.isSelected()) {
                return new SimpleBooleanProperty(true);
            }
        }
        return new SimpleBooleanProperty(false);
    }

    private void updateStatusRoteiro(Roteiros rota) {
        try {
            if (rota.getStatus().equals("Aberto")) {
                rota.setStatus("Imprimir");
            } else if (rota.getStatus().equals("Imprimir")) {
                rota.setStatus("Programado");
            } else {
                rota.setStatus("Finalizado");
            }
            DAOFactory.getRoteirosDAO().updateStatus(rota);
            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void factoryColumns() {
        clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnSelected.setCellValueFactory((cellData) -> {
            Roteiros cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
            return property;
        });

        clnAction.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Roteiros, Roteiros>, ObservableValue<Roteiros>>() {
            @Override
            public ObservableValue<Roteiros> call(TableColumn.CellDataFeatures<Roteiros, Roteiros> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnAction.setComparator(new Comparator<Roteiros>() {
            @Override
            public int compare(Roteiros p1, Roteiros p2) {
                return p1.getSolicitante().compareTo(p2.getSolicitante());
            }
        });
        clnAction.setCellFactory(new Callback<TableColumn<Roteiros, Roteiros>, TableCell<Roteiros, Roteiros>>() {
            @Override
            public TableCell<Roteiros, Roteiros> call(TableColumn<Roteiros, Roteiros> btnCol) {
                return new TableCell<Roteiros, Roteiros>() {
                    final ImageView buttonGraphic = new ImageView();
                    final Button button = new Button();

                    {
                        button.setGraphic(buttonGraphic);
                        button.setMinWidth(130);
                    }

                    @Override
                    public void updateItem(final Roteiros rota, boolean empty) {
                        super.updateItem(rota, empty);
                        if (rota != null) {
                            if (rota.getStatus().equals("Programado")) {
                                button.setText("Finalizar");
                                buttonGraphic.setImage(btnImage);
                                button.getStyleClass().add("success");

                                getStyleClass().remove("statusBoletoAVencer");
                                getStyleClass().remove("statusBoletoVencido");
                                setGraphic(button);
                                button.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        button.disableProperty().set(true);
                                        updateStatusRoteiro(rota);
                                        button.disableProperty().set(false);
                                    }
                                });
                            } else if (rota.getStatus().equals("Finalizado")) {
                                getStyleClass().remove("statusBoletoVencido");
                                getStyleClass().add("statusBoletoAVencer");
                                setText(rota.getStatus());
                                setGraphic(null);
                            } else {
                                getStyleClass().remove("statusBoletoAVencer");
                                getStyleClass().add("statusBoletoVencido");
                                setText(rota.getStatus());
                                setGraphic(null);
                            }
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblRoteiros.setItems(DAOFactory.getRoteirosDAO().getByFormDtRoteiro(tboxData_C.getValue() == null ? "" : tboxData_C.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    selectTiposConsulta, tboxFornecedor_C.getText(), tboxCidade_C.getText(), tboxMercadoria_C.getText()));
            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProgramarOnAction(ActionEvent event) {
        try {
            if (!hasSelected().get()) {
                GUIUtils.showMessage("Nenhuma rota selecionada para programação.", Alert.AlertType.INFORMATION);
                return;
            }

            for (Roteiros rota : tblRoteiros.getItems()) {
                if (rota.isSelected()) {
                    this.updateStatusRoteiro(rota);
                }
            }

            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map<String,Object> parametros = new HashMap<>();
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RoteiroViagem.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);

            for (Roteiros rota : tblRoteiros.getItems()) {
                if (rota.isSelected()) {
                    this.updateStatusRoteiro(rota);
                }
            }
            tblRoteiros.refresh();
            this.factoryColumns();
        } catch (JRException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (IOException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnImprimirOnAction(ActionEvent event) {
        try {
            if (!hasSelected().get()) {
                GUIUtils.showMessage("Nenhuma rota selecionada para impressão.", Alert.AlertType.INFORMATION);
                return;
            }

            for (Roteiros rota : tblRoteiros.getItems()) {
                if (rota.isSelected()) {
                    rota.setStatus("Aberto");
                    this.updateStatusRoteiro(rota);
                }
            }

            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RoteiroViagem.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);

            for (Roteiros rota : tblRoteiros.getItems()) {
                if (rota.isSelected()) {
                    rota.setStatus("Programado");
                    this.updateStatusRoteiro(rota);
                }
            }
            tblRoteiros.refresh();
            this.factoryColumns();
        } catch (JRException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (IOException ex) {
            Logger.getLogger(SceneAdminRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

}
