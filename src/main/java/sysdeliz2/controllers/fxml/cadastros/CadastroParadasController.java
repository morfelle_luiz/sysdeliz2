package sysdeliz2.controllers.fxml.cadastros;

import javafx.scene.Node;
import javafx.scene.control.TextField;
import sysdeliz2.models.sysdeliz.SdTiposParada001;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.components.FormGroup;
import sysdeliz2.utils.gui.components.FormTextField;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.WindowCadastroBasic;
import sysdeliz2.utils.sys.ImageUtils;

import java.sql.SQLException;

public class CadastroParadasController extends WindowCadastroBasic<SdTiposParada001> {
    
    private FormGroup grupoDadosGerais = new FormGroup("dados gerais");
    private FormTextField codigo = new FormTextField("código", false) {
        @Override
        protected void setMaskField(TextField tboxField) {
        }
    };
    private FormTextField descricao = new FormTextField("descrição", 300) {
        @Override
        protected void setMaskField(TextField tboxField) {
            TextFieldUtils.upperCase(tboxField);
        }
    };
    private FormToggleField dedutivel = new FormToggleField("detutível efic.");
    private FormToggleField deduzProd = new FormToggleField("detutível prod.");
    private FormToggleField ativo = new FormToggleField("ativo");
    private FormToggleField comTempo = new FormToggleField("com tempo");
    
    public CadastroParadasController() throws SQLException {
        super("Cadastro de Paradas", ImageUtils.getImage(ImageUtils.Icon.PARADA));
        
        grupoDadosGerais.contentPaneGroup.getChildren().add(codigo);
        grupoDadosGerais.contentPaneGroup.getChildren().add(descricao);
        grupoDadosGerais.contentPaneGroup.getChildren().add(dedutivel);
        grupoDadosGerais.contentPaneGroup.getChildren().add(deduzProd);
        grupoDadosGerais.contentPaneGroup.getChildren().add(comTempo);
        grupoDadosGerais.contentPaneGroup.getChildren().add(ativo);
        super.cpaneFormulario.getChildren().add(grupoDadosGerais);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    @Override
    protected void bindData(SdTiposParada001 selectItem) {
        if(selectItem != null && grupoDadosGerais != null) {
            codigo.tboxField.textProperty().bind(selectItem.codigoProperty().asString());
            descricao.tboxField.textProperty().bind(selectItem.descricaoProperty());
            dedutivel.isSwitchedOn.bind(selectItem.dedutivelProperty());
            deduzProd.isSwitchedOn.bind(selectItem.deduzProdProperty());
            comTempo.isSwitchedOn.bind(selectItem.comTempoProperty());
            ativo.isSwitchedOn.bind(selectItem.ativoProperty());
        }
    }
    
    @Override
    protected void unbindData() {
        try {
            codigo.tboxField.textProperty().unbind();
            descricao.tboxField.textProperty().unbind();
            dedutivel.isSwitchedOn.unbind();
            deduzProd.isSwitchedOn.unbind();
            comTempo.isSwitchedOn.unbind();
            ativo.isSwitchedOn.unbind();
        }catch (NullPointerException ex){
        
        }
    }
    
    @Override
    protected void bindBidirectionalData(SdTiposParada001 selectItem) {
        if(selectItem != null) {
            codigo.tboxField.textProperty().bind(selectItem.codigoProperty().asString());
            descricao.tboxField.textProperty().bindBidirectional(selectItem.descricaoProperty());
            dedutivel.isSwitchedOn.bindBidirectional(selectItem.dedutivelProperty());
            deduzProd.isSwitchedOn.bindBidirectional(selectItem.deduzProdProperty());
            comTempo.isSwitchedOn.bindBidirectional(selectItem.comTempoProperty());
            ativo.isSwitchedOn.bindBidirectional(selectItem.ativoProperty());
        }
    }
    
    @Override
    protected void unbindBidirectionalData(SdTiposParada001 selectItem) {
        if(selectItem != null) {
            codigo.tboxField.textProperty().unbind();
            descricao.tboxField.textProperty().unbindBidirectional(selectItem.descricaoProperty());
            dedutivel.isSwitchedOn.unbindBidirectional(selectItem.dedutivelProperty());
            deduzProd.isSwitchedOn.unbindBidirectional(selectItem.dedutivelProperty());
            comTempo.isSwitchedOn.unbindBidirectional(selectItem.comTempoProperty());
            ativo.isSwitchedOn.unbindBidirectional(selectItem.ativoProperty());
        }
    }
    
    @Override
    protected void clearForm() {
    
    }
    
    @Override
    protected boolean validationForm() {
        try {
            ValidationForm.validationEmpty(descricao.tboxField);
        } catch (FormValidationException ex) {
            return false;
        }
        return true;
    }
    
    @Override
    protected Node setContentBuscaAvancada() {
        return null;
    }
}
