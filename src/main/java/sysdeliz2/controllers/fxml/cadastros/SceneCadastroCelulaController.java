/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterSetorOperacaoController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Log;
import sysdeliz2.models.SdFamiliasCelula001;
import sysdeliz2.models.SdTurnoCelula001;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColabCelula001;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdSetorOp001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.sys.EnumsController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastroCelulaController implements Initializable {

    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdCelula> listRegisters = new SimpleListProperty();
    private final ListProperty<SdFamiliasCelula001> listFamilyGroupsAvailable = new SimpleListProperty();
    private final ListProperty<SdFamiliasCelula001> listFamilyGroupsSelected = new SimpleListProperty();
    private final ObservableList<SdFamiliasCelula001> listFamilyGroupsAvailableAll = FXCollections.observableArrayList();
    private final ListProperty<SdColabCelula001> listPersonsAvailable = new SimpleListProperty();
    private final ListProperty<SdColabCelula001> listPersonsSelected = new SimpleListProperty();
    private final ListProperty<SdTurnoCelula001> listTurnosAvailable = new SimpleListProperty(FXCollections.observableArrayList());
    private final ListProperty<SdTurnoCelula001> listTurnosSelected = new SimpleListProperty(FXCollections.observableArrayList());
    private final ObservableList<SdColabCelula001> listPersonsAvailableAll = FXCollections.observableArrayList();
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private SdCelula selectedItem = null;
    private SdColaborador cellLeaderSelected = null;
    private SdSetorOp001 cellSetorOpSelected = null;
    private ToggleSwitch tsActive = new ToggleSwitch(24, 55);

    @FXML
    private TabPane tabMainPane;
    @FXML
    private Button btnAddRegister;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private TableView<SdCelula> tblRegisters;
    @FXML
    private TableColumn<SdCelula, SdCelula> clnActions;
    @FXML
    private TableColumn<SdCelula, Boolean> clnActive;
    @FXML
    private TableColumn<SdCelula, SdColaborador> clnLeader;
    @FXML
    private TableColumn<SdCelula, SdSetorOp001> clnSectionOp;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxRegisterCode;
    @FXML
    private TextField tboxDescriptionCode;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private Pane pnToggleActive;
    @FXML
    private Label lbToggleActive;
    @FXML
    private ListView<SdFamiliasCelula001> listAvailable;
    @FXML
    private Button btnAddSelected;
    @FXML
    private Button btnAddAll;
    @FXML
    private Button btnRemoveSelected;
    @FXML
    private Button btnRemoveAll;
    @FXML
    private ListView<SdFamiliasCelula001> listSelected;
    @FXML
    private TextField tboxCodeSectionOp;
    @FXML
    private Label lbDescriptionSectionOp;
    @FXML
    private Button btnFindSectionOp;
    @FXML
    private TextField tboxCodeLeader;
    @FXML
    private Label lbDescritpionLeader;
    @FXML
    private Button btnFindLeader;
    @FXML
    private TextField tboxQtdWorkers;
    @FXML
    private TextField tboxFindFamilyGroup;
    @FXML
    private Label lbSelectedCount;
    @FXML
    private ListView<SdColabCelula001> listAvailablePerson;
    @FXML
    private TextField tboxFindPerson;
    @FXML
    private Button btnAddSelectedPerson;
    @FXML
    private Button btnAddAllPerson;
    @FXML
    private Button btnRemoveSelectedPerson;
    @FXML
    private Button btnRemoveAllPerson;
    @FXML
    private ListView<SdColabCelula001> listSelectedPerson;
    @FXML
    private Label lbSelectedPersonCount;
    @FXML
    private ComboBox<SdTurnoCelula001> cboxTurnos;
    @FXML
    private Button btnAddTurno;
    @FXML
    private ListView<SdTurnoCelula001> listTurnosCelula;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxDescriptionCode);
        TextFieldUtils.upperCase(tboxFindFamilyGroup);
        tboxFindFamilyGroup.disableProperty().bind(inEdition.not());

        cboxTurnos.itemsProperty().bind(listTurnosAvailable);
        listTurnosCelula.itemsProperty().bind(listTurnosSelected);

        listAvailable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listSelected.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listAvailable.itemsProperty().bind(listFamilyGroupsAvailable);
        listSelected.itemsProperty().bind(listFamilyGroupsSelected);
        btnAddSelected.disableProperty().bind(inEdition.not().or(listAvailable.getSelectionModel().selectedItemProperty().isNull()));
        btnAddAll.disableProperty().bind(inEdition.not().or(listFamilyGroupsAvailable.emptyProperty()));
        btnRemoveSelected.disableProperty().bind(inEdition.not().or(listSelected.getSelectionModel().selectedItemProperty().isNull()));
        btnRemoveAll.disableProperty().bind(inEdition.not().or(listFamilyGroupsSelected.emptyProperty()));

        listAvailablePerson.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listSelectedPerson.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listAvailablePerson.itemsProperty().bind(listPersonsAvailable);
        listSelectedPerson.itemsProperty().bind(listPersonsSelected);
        btnAddSelectedPerson.disableProperty().bind(inEdition.not().or(listAvailablePerson.getSelectionModel().selectedItemProperty().isNull()));
        btnAddAllPerson.disableProperty().bind(inEdition.not().or(listPersonsAvailable.emptyProperty()));
        btnRemoveSelectedPerson.disableProperty().bind(inEdition.not().or(listSelectedPerson.getSelectionModel().selectedItemProperty().isNull()));
        btnRemoveAllPerson.disableProperty().bind(inEdition.not().or(listPersonsSelected.emptyProperty()));

        btnFindSectionOp.disableProperty().bind(inEdition.not());
        btnFindLeader.disableProperty().bind(inEdition.not());
        makeCellListOperations();

        pnToggleActive.getChildren().add(tsActive);
        lbToggleActive.textProperty().bind(Bindings.when(tsActive.switchedOnProperty()).then("SIM").otherwise("NÃO"));

        lbSelectedCount.textProperty().bind(new SimpleStringProperty("Total de ").concat(listFamilyGroupsSelected.sizeProperty()).concat(" famílias selecionadas"));
        lbSelectedPersonCount.textProperty().bind(new SimpleStringProperty("Total de ").concat(listPersonsSelected.sizeProperty()).concat(" colaboradores selecionados"));
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));

        try {
            listRegisters.set(DAOFactory.getSdCelula001DAO().getAll());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeButtonsActions();
        makeColunsFactory();
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();
    }

    public void searchAvailable(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listFamilyGroupsAvailable.set(listFamilyGroupsAvailableAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdFamiliasCelula001> subentries = FXCollections.observableArrayList();
        for (SdFamiliasCelula001 entry : listFamilyGroupsAvailable) {
            boolean match = true;
            if (!entry.getFamilia().getDescricao().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listFamilyGroupsAvailable.set(subentries);

    }

    public void searchAvailablePerson(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listPersonsAvailable.set(listPersonsAvailableAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdColabCelula001> subentries = FXCollections.observableArrayList();
        for (SdColabCelula001 entry : listPersonsAvailable) {
            boolean match = true;
            if (!entry.getColaborador().getNome().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listPersonsAvailable.set(subentries);

    }

    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdCelula, SdCelula>, ObservableValue<SdCelula>>() {
            @Override
            public ObservableValue<SdCelula> call(TableColumn.CellDataFeatures<SdCelula, SdCelula> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdCelula>() {
            @Override
            public int compare(SdCelula p1, SdCelula p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdCelula, SdCelula>, TableCell<SdCelula, SdCelula>>() {
            @Override
            public TableCell<SdCelula, SdCelula> call(TableColumn<SdCelula, SdCelula> btnCol) {
                return new TableCell<SdCelula, SdCelula>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(SdCelula seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    btnDeleteRegisterOnAction(null);
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }

        });

    }

    private void makeColunsFactory() {
        clnActive.setCellFactory(column -> {
            TableCell<SdCelula, Boolean> cell = new TableCell<SdCelula, Boolean>() {

                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item ? "S" : "N");
                    }
                }
            };

            return cell;
        });

        clnLeader.setCellFactory(column -> {
            TableCell<SdCelula, SdColaborador> cell = new TableCell<SdCelula, SdColaborador>() {

                @Override
                protected void updateItem(SdColaborador item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item.getNome());
                    }
                }
            };

            return cell;
        });

        clnSectionOp.setCellFactory(column -> {
            TableCell<SdCelula, SdSetorOp001> cell = new TableCell<SdCelula, SdSetorOp001>() {

                @Override
                protected void updateItem(SdSetorOp001 item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item.getDescricao());
                    }
                }
            };

            return cell;
        });
    }

    private void makeCellListOperations() {
        listAvailable.setCellFactory(new Callback<ListView<SdFamiliasCelula001>, ListCell<SdFamiliasCelula001>>() {
            @Override
            public ListCell<SdFamiliasCelula001> call(ListView<SdFamiliasCelula001> param) {
                return new ListOperationsCell();
            }
        });
        listSelected.setCellFactory(new Callback<ListView<SdFamiliasCelula001>, ListCell<SdFamiliasCelula001>>() {
            @Override
            public ListCell<SdFamiliasCelula001> call(ListView<SdFamiliasCelula001> param) {
                return new ListOperationsCell();
            }
        });
        tboxFindFamilyGroup.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchAvailable((String) oldValue, (String) newValue);
            }
        });

        listAvailablePerson.setCellFactory(new Callback<ListView<SdColabCelula001>, ListCell<SdColabCelula001>>() {
            @Override
            public ListCell<SdColabCelula001> call(ListView<SdColabCelula001> param) {
                return new ListPersonsCell();
            }
        });
        listSelectedPerson.setCellFactory(new Callback<ListView<SdColabCelula001>, ListCell<SdColabCelula001>>() {
            @Override
            public ListCell<SdColabCelula001> call(ListView<SdColabCelula001> param) {
                return new ListPersonsCell();
            }
        });
        tboxFindPerson.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchAvailablePerson((String) oldValue, (String) newValue);
            }
        });

        btnAddTurno.disableProperty().bind(inEdition.not().or(cboxTurnos.getSelectionModel().selectedItemProperty().isNull()));
        cboxTurnos.editableProperty().bind(inEdition);
        cboxTurnos.setCellFactory(new Callback<ListView<SdTurnoCelula001>, ListCell<SdTurnoCelula001>>() {
            @Override
            public ListCell<SdTurnoCelula001> call(ListView<SdTurnoCelula001> param) {
                return new ListCell<SdTurnoCelula001>() {
                    @Override
                    protected void updateItem(SdTurnoCelula001 item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText("[" + item.getTurno().getCodigo() + "] " + item.getTurno().getDescricao());
                        }
                    }

                };
            }
        });
        cboxTurnos.setConverter(new StringConverter<SdTurnoCelula001>() {
            @Override
            public String toString(SdTurnoCelula001 object) {
                if (object != null) {
                    return "[" + object.getTurno().getCodigo() + "] " + object.getTurno().getDescricao();
                }
                return null;
            }

            @Override
            public SdTurnoCelula001 fromString(String string) {
                Integer codigoTurno = Integer.parseInt((string.split("]")[0]).substring(1, (string.split("]")[0]).length()));
                return listTurnosAvailable.stream()
                        .filter(turno -> turno.getCodigoTurno() == codigoTurno)
                        .collect(Collectors.toList())
                        .get(0);
            }
        });
        listTurnosCelula.setCellFactory(new Callback<ListView<SdTurnoCelula001>, ListCell<SdTurnoCelula001>>() {
            @Override
            public ListCell<SdTurnoCelula001> call(ListView<SdTurnoCelula001> param) {
                return new ListCell<SdTurnoCelula001>() {
                    @Override
                    protected void updateItem(SdTurnoCelula001 item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText("[" + item.getTurno().getCodigo() + "] " + item.getTurno().getDescricao());
                        }
                    }

                };
            }
        });
        listTurnosCelula.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2) {
                SdTurnoCelula001 turnoSelecionado = listTurnosCelula.getSelectionModel().getSelectedItem();
                listTurnosAvailable.add(turnoSelecionado);
                listTurnosSelected.remove(turnoSelecionado);
            }
        });
    }

    private void prepareListAvailable(SdCelula cellCode) {
        try {
            listFamilyGroupsAvailable.set(DAOFactory.getSdFamiliasCelula001DAO().getAvailableFamilyGroups(cellCode.getCodigo()));
            listFamilyGroupsAvailableAll.clear();
            listFamilyGroupsAvailableAll.addAll(listFamilyGroupsAvailable);

            listPersonsAvailable.set(DAOFactory.getSdColabCelula001DAO().getAvailablePersons(cellCode.getCodigo()));
            listPersonsAvailableAll.clear();
            listPersonsAvailableAll.addAll(listPersonsAvailable);

            final StringProperty codsTurno = new SimpleStringProperty("");
            listTurnosAvailable.clear();
            listTurnosAvailable.set(DAOFactory.getSdTurnoCelula001DAO().getByAvailable(cellCode.getTurnos().stream().map(turno -> turno.getCodigoTurno() + "").collect(Collectors.toList()),cellCode.getCodigo()));

        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void clearForm() {
        tboxRegisterCode.clear();
        tboxDescriptionCode.clear();

        tboxCodeSectionOp.clear();
        lbDescriptionSectionOp.textProperty().bind(new SimpleStringProperty(""));
        cellSetorOpSelected = null;
        tboxCodeLeader.clear();
        lbDescritpionLeader.textProperty().bind(new SimpleStringProperty(""));
        cellLeaderSelected = null;
        tboxQtdWorkers.clear();

    }

    private void getSelectedFamilies(SdCelula selectItem) {
        if (selectItem.familiasProperty().sizeProperty().get() <= 0) {
            try {
                selectItem.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(selectItem.getCodigo()));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    private void getSelectedColaboradores(SdCelula selectItem) {
        if (selectItem.familiasProperty().sizeProperty().get() <= 0) {
            try {
                selectItem.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(selectItem.getCodigo()));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    private void getSelectedTurnos(SdCelula selectItem) {
        if (selectItem.turnosProperty().sizeProperty().get() <= 0) {
            try {
                selectItem.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(selectItem.getCodigo()));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    private void bindData(SdCelula selectItem) {
        if (selectItem != null) {
            this.getSelectedFamilies(selectItem);
            this.getSelectedColaboradores(selectItem);
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bind(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().bind(selectItem.ativoProperty());

            tboxCodeSectionOp.textProperty().bind(selectItem.codigoSetorOpProperty().asString());
            tboxCodeLeader.textProperty().bind(selectItem.codigoLiderProperty().asString());

            listFamilyGroupsSelected.bind(selectItem.familiasProperty());
            listPersonsSelected.bind(selectItem.colaboradoresProperty());
            listTurnosSelected.bind(selectItem.turnosProperty());
            selectItem.qtdeOperadoresProperty().bind(listPersonsSelected.sizeProperty());
    
            tboxQtdWorkers.textProperty().bind(selectItem.qtdeOperadoresProperty().asString());
            
            lbDescriptionSectionOp.textProperty().bind(selectItem.setorOpProperty().get().descricaoProperty());
            lbDescritpionLeader.textProperty().bind(selectItem.liderProperty().get().nomeProperty());
        }
    }

    private void unbindData() {
        tboxRegisterCode.textProperty().unbind();
        tboxDescriptionCode.textProperty().unbind();
        tsActive.switchedOnProperty().unbind();

        tboxCodeSectionOp.textProperty().unbind();
        tboxCodeLeader.textProperty().unbind();
        tboxQtdWorkers.textProperty().unbind();

        listFamilyGroupsSelected.unbind();
        listPersonsSelected.unbind();
        listTurnosSelected.unbind();

        clearForm();
    }

    private void bindBidirectionalData(SdCelula selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bindBidirectional(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().bindBidirectional(selectItem.ativoProperty());

            tboxCodeSectionOp.textProperty().bindBidirectional(selectItem.codigoSetorOpProperty(), new NumberStringConverter());
            tboxCodeLeader.textProperty().bindBidirectional(selectItem.codigoLiderProperty(), new NumberStringConverter());

            getSelectedFamilies(selectItem);
            getSelectedColaboradores(selectItem);
            getSelectedTurnos(selectItem);
            prepareListAvailable(selectItem);
            listFamilyGroupsSelected.bindBidirectional(selectItem.familiasProperty());
            listPersonsSelected.bindBidirectional(selectItem.colaboradoresProperty());
            listTurnosSelected.bindBidirectional(selectItem.turnosProperty());
            selectItem.qtdeOperadoresProperty().bind(listPersonsSelected.sizeProperty());
    
            tboxQtdWorkers.textProperty().bind(selectItem.qtdeOperadoresProperty().asString());

            lbDescritpionLeader.textProperty().bind(selectItem.liderProperty().get().nomeProperty());
            lbDescriptionSectionOp.textProperty().bind(selectItem.setorOpProperty().get().descricaoProperty());
        }
    }

    private void unbindBidirectionalData(SdCelula selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().unbind();
            tboxDescriptionCode.textProperty().unbindBidirectional(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().unbindBidirectional(selectItem.ativoProperty());

            tboxCodeSectionOp.textProperty().unbindBidirectional(selectItem.codigoSetorOpProperty());
            lbDescriptionSectionOp.textProperty().unbind();
            tboxCodeLeader.textProperty().unbindBidirectional(selectItem.codigoLiderProperty());
            lbDescritpionLeader.textProperty().unbind();
    
            selectItem.qtdeOperadoresProperty().unbind();
            tboxQtdWorkers.textProperty().unbind();

            listFamilyGroupsSelected.unbindBidirectional(selectItem.familiasProperty());
            listPersonsSelected.unbindBidirectional(selectItem.colaboradoresProperty());
            listTurnosSelected.unbindBidirectional(selectItem.turnosProperty());

            clearForm();
        }
    }

    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdCelula();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);

        tabMainPane.getSelectionModel().select(1);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(DAOFactory.getSdCelula001DAO().getByDefault(tboxDefaultFilter.textProperty().get()));
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                DAOFactory.getSdCelula001DAO().delete(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro Células",
                        selectedItem.getCodigo() + "",
                        "Excluído a célula " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) throws InterruptedException {
        try {
            ValidationForm.validationEmpty(tboxDescriptionCode);
        } catch (FormValidationException ex) {
            return;
        }

        unbindBidirectionalData(selectedItem);
        SdCelula objectEdited = selectedItem;
        try {
            if (selectedItem.getCodigo() == 0) {
                DAOFactory.getSdCelula001DAO().save(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro Células",
                        selectedItem.getCodigo() + "",
                        "Cadastrada a célula " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                if (!listRegisters.contains(selectedItem)) {
                    listRegisters.add(selectedItem);
                }
            } else {
                DAOFactory.getSdCelula001DAO().update(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro Células",
                        selectedItem.getCodigo() + "",
                        "Alterada a célula " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
            listFamilyGroupsAvailable.clear();
            listPersonsAvailable.clear();
            clearForm();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        inEdition.set(false);
        tblRegisters.getSelectionModel().select(objectEdited);
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        listFamilyGroupsAvailable.clear();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdCelula();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnAddSelectedOnAction(ActionEvent event) {
        ObservableList<SdFamiliasCelula001> listToAdd = listAvailable.getSelectionModel().getSelectedItems();
        listFamilyGroupsSelected.addAll(listToAdd);
        listFamilyGroupsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnAddAllOnAction(ActionEvent event) {
        ObservableList<SdFamiliasCelula001> listToAdd = listAvailable.getItems();
        listFamilyGroupsSelected.addAll(listToAdd);
        listFamilyGroupsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveSelectedOnAction(ActionEvent event) {
        ObservableList<SdFamiliasCelula001> listToAdd = listSelected.getSelectionModel().getSelectedItems();
        listFamilyGroupsAvailable.addAll(listToAdd);
        listFamilyGroupsSelected.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveAllOnAction(ActionEvent event) {
        ObservableList<SdFamiliasCelula001> listToAdd = listSelected.getItems();
        listFamilyGroupsAvailable.addAll(listToAdd);
        listFamilyGroupsSelected.removeAll(listToAdd);
    }

    @FXML
    private void tboxCodeSectionOpOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindSectionOpOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                cellSetorOpSelected = DAOFactory.getSdSetorOp001DAO().getByCode(Integer.parseInt(tboxCodeSectionOp.getText()));
                if (cellSetorOpSelected == null) {
                    GUIUtils.showMessage("Código de setor de operação não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    cellSetorOpSelected.setSelected(true);
                    selectedItem.copySetorOp(cellSetorOpSelected);
                    tboxCodeLeader.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnFindSectionOpOnAction(ActionEvent event) {
        try {
            ObservableList<SdSetorOp001> sendSelected = FXCollections.observableArrayList();
            if (cellSetorOpSelected != null) {
                sendSelected.add(cellSetorOpSelected);
            }
            FilterSetorOperacaoController ctrlFilter = new FilterSetorOperacaoController(Modals.FXMLWindow.FilterSetorOp,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                cellSetorOpSelected = ctrlFilter.selectedObjects.get(0);
                selectedItem.copySetorOp(cellSetorOpSelected);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxCodeLeaderOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindLeaderOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                cellLeaderSelected = DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(tboxCodeLeader.getText()));
                if (cellLeaderSelected == null) {
                    GUIUtils.showMessage("Código de setor de operação não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    cellLeaderSelected.setSelected(true);
                    selectedItem.copyLider(cellLeaderSelected);
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnFindLeaderOnAction(ActionEvent event) {
        try {
            ObservableList<SdColaborador> sendSelected = FXCollections.observableArrayList();
            if (cellLeaderSelected != null) {
                sendSelected.add(cellLeaderSelected);
            }
            FilterColaboradorController ctrlFilter = new FilterColaboradorController(Modals.FXMLWindow.FilterColaborador,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                cellLeaderSelected = ctrlFilter.selectedObjects.get(0);
                selectedItem.copyLider(cellLeaderSelected);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnAddSelectedPersonOnAction(ActionEvent event) {
        ObservableList<SdColabCelula001> listToAdd = listAvailablePerson.getSelectionModel().getSelectedItems();
        listPersonsSelected.addAll(listToAdd);
        listPersonsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnAddAllPersonOnAction(ActionEvent event) {
        ObservableList<SdColabCelula001> listToAdd = listAvailablePerson.getItems();
        listPersonsSelected.addAll(listToAdd);
        listPersonsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveSelectedPersonOnAction(ActionEvent event) {
        ObservableList<SdColabCelula001> listToAdd = listSelectedPerson.getSelectionModel().getSelectedItems();
        listPersonsAvailable.addAll(listToAdd);
        listPersonsSelected.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveAllPersonOnAction(ActionEvent event) {
        ObservableList<SdColabCelula001> listToAdd = listSelectedPerson.getItems();
        listPersonsAvailable.addAll(listToAdd);
        listPersonsSelected.removeAll(listToAdd);
    }

    @FXML
    private void btnAddTurnoOnAction(ActionEvent event) {
        SdTurnoCelula001 turnoSelecionado = cboxTurnos.getValue();
        listTurnosSelected.add(turnoSelecionado);
        listTurnosAvailable.remove(turnoSelecionado);
    }

    private class ListOperationsCell extends ListCell<SdFamiliasCelula001> {

        @Override
        public void updateItem(SdFamiliasCelula001 item, boolean empty) {
            super.updateItem(item, empty);
            this.setText(null);
            this.setGraphic(null);
            if (item != null || !empty) {
                int index = this.getIndex();
                String codigo = item.getFamiliaGrp();
                this.setText(item.getFamilia().getDescricao());
            }
        }
    }

    private class ListPersonsCell extends ListCell<SdColabCelula001> {

        @Override
        public void updateItem(SdColabCelula001 item, boolean empty) {
            super.updateItem(item, empty);
            this.setText(null);
            this.setGraphic(null);
            if (item != null || !empty) {
                int index = this.getIndex();
                //String codigo = item.getCodigoColaborador();
                this.setText(item.getColaborador().getNome());
            }
        }
    }

}
