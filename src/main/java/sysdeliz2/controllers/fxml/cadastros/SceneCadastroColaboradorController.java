/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterFuncaoColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterGrupoOperacaoController;
import sysdeliz2.controllers.fxml.procura.FilterNivelController;
import sysdeliz2.controllers.fxml.procura.FilterTurnoController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.*;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.utils.*;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.components.FormFieldToggle;
import sysdeliz2.utils.sys.EnumsController;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
@SuppressWarnings("unused")
public class SceneCadastroColaboradorController implements Initializable {
    
    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdColaborador> listRegisters = new SimpleListProperty<>();
    private final ListProperty<SdOperacoesGrp001> listOperationsAvailable = new SimpleListProperty<>();
    private final ListProperty<SdPolivalencia001> listOperationsSelected = new SimpleListProperty<>();
    private final ObservableList<SdOperacoesGrp001> listOperationsAvailableAll = FXCollections.observableArrayList();
    private final ObservableList<SdPolivalencia001> listOperationsSelectedAll = FXCollections.observableArrayList();
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private ObservableList<SdNivelPolivalencia001> listLevelsDb = FXCollections.observableArrayList();
    private SdColaborador selectedItem = null;
    private SdTurno sdTurnoPerson = null;
    private SdGrupoOperacao001 sdGrupoOperacaoPerson = new SdGrupoOperacao001();
    private SdNivelPolivalencia001 sdLevelOperationPerson = new SdNivelPolivalencia001();
    private SdFuncao001 sdFuncaoPerson = null;
    private final FormFieldToggle fieldAtivo = FormFieldToggle.create(field -> {
        field.title("Ativo");
    });
    private final FormFieldToggle fieldImpDefault = FormFieldToggle.create(field -> {
        field.title("Impressora Padrão");
    });
    private final FormFieldToggle fieldPerdidasAtivas = FormFieldToggle.create(field -> {
        field.title("Perdidas Ativas");
    });
    private final FormFieldToggle fieldEntradaDireta = FormFieldToggle.create(field -> {
        field.title("Entrada Direta");
    });
    private final FormFieldToggle fieldSegundoColetor = FormFieldToggle.create(field -> {
        field.title("Segundo Coletor");
    });
    private final FormFieldToggle fieldIncompletasAtivas = FormFieldToggle.create(field -> {
        field.title("Incompletas Ativas");
    });
    private final FormFieldText fieldPisoColeta = FormFieldText.create(field -> {
        field.title("Piso");
        field.mask(FormFieldText.Mask.INTEGER);
    });

    //<editor-fold defaultstate="collapsed" desc="FXML Components">
    @FXML
    private TabPane tabMainPane;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private TableView<SdColaborador> tblRegisters;
    @FXML
    private TableColumn<SdColaborador, SdColaborador> clnActions;
    @FXML
    private TableColumn<SdColaborador, Boolean> clnActive;
    @FXML
    private TableColumn<SdColaborador, SdTurno> clnPeriod;
    @FXML
    private TableColumn<SdColaborador, SdFuncao001> clnFunction;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxRegisterCode;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private Pane pnToggleActive;
    @FXML
    private Label lbToggleActive;
    @FXML
    private Pane pnToggleImpDefault;
    @FXML
    private Label lbToggleImpDefault;
    @FXML
    private Pane pnTogglePerdidasAtivas;
    @FXML
    private Label lbTogglePerdidasAtivas;
    @FXML
    private TextField tboxName;
    @FXML
    private TextField tboxCodePeriod;
    @FXML
    private Label lbDescriptionPeriod;
    @FXML
    private Button btnFindPeriod;
    @FXML
    private TextField tboxCodeFunction;
    @FXML
    private Label lbDescritpionFunction;
    @FXML
    private Button btnFindFunction;
    @FXML
    private TextField tboxCodeRH;
    @FXML
    private TextField tboxCodeOperationGroup;
    @FXML
    private Label lbDescriptionOperationGroup;
    @FXML
    private Button btnFindOperationGroup;
    @FXML
    private ListView<SdOperacoesGrp001> lviewOperationsAvailable;
    @FXML
    private TextField tboxFindOperationsAvailable;
    @FXML
    private Button btnAddSelected;
    @FXML
    private Button btnAddAll;
    @FXML
    private TextField tboxCodeLevel;
    @FXML
    private Label lbDescriptionLevel;
    @FXML
    private Button btnFindLevel;
    @FXML
    private Label lbOperationsCount;
    @FXML
    private TableView<SdPolivalencia001> tblOperactionsSelected;
    @FXML
    private TextField tboxFindOperationsSelected;
    @FXML
    private TableColumn<SdPolivalencia001, SdPolivalencia001> clnActiveOperation;
    @FXML
    private TableColumn<SdPolivalencia001, SdOperacaoOrganize001> clOperation;
    @FXML
    private TableColumn<SdPolivalencia001, SdPolivalencia001> clnLevelOperation;
    @FXML
    private Button btnDeleteSelected;
    @FXML
    private Button btnDeleteAll;
    @FXML
    private TextField tboxCellPerson;
    @FXML
    private TextField tboxUsuarioAD;
    @FXML
    private TextField tboxCodFor;
    @FXML
    private VBox boxInfosAdicionais;
    @FXML
    private VBox boxAtivo;

//</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxName);
        MaskTextField.numericField(tboxCodeRH);
        MaskTextField.numericField(tboxCodeFunction);
        MaskTextField.numericField(tboxCodeOperationGroup);
        MaskTextField.numericField(tboxCodeLevel);
        MaskTextField.numericField(tboxCodePeriod);
        
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);
        
        lviewOperationsAvailable.itemsProperty().bind(listOperationsAvailable);
        lviewOperationsAvailable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        lviewOperationsAvailable.setCellFactory(param -> new ListOperationsCell());
        
        tboxFindOperationsAvailable.textProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> searchAvailable((String) oldValue, (String) newValue));
        lbOperationsCount.textProperty().bind(new SimpleStringProperty("Selecionada(s) ").concat(listOperationsSelected.sizeProperty()).concat(" operação(ões)"));
        tblOperactionsSelected.itemsProperty().bind(listOperationsSelected);
        TextFieldUtils.upperCase(tboxFindOperationsAvailable);
        tboxFindOperationsAvailable.disableProperty().bind(inEdition.not());
        tboxFindOperationsSelected.textProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> searchSelected((String) oldValue, (String) newValue));
        tboxFindOperationsSelected.disableProperty().bind(inEdition.not());
        TextFieldUtils.upperCase(tboxFindOperationsSelected);
        tblOperactionsSelected.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        try {
            listLevelsDb = DAOFactory.getSdNivelPolivalenciaDAO().getAll();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeColumnsTblOperations();
        
        btnAddSelected.disableProperty().bind(inEdition.not().or(lviewOperationsAvailable.getSelectionModel().selectedItemProperty().isNull()));
        btnAddAll.disableProperty().bind(inEdition.not().or(listOperationsAvailable.emptyProperty()));
        btnDeleteSelected.disableProperty().bind(inEdition.not().or(tblOperactionsSelected.getSelectionModel().selectedItemProperty().isNull()));
        btnDeleteAll.disableProperty().bind(inEdition.not().or(listOperationsSelected.emptyProperty()));
        
        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);
        btnFindPeriod.disableProperty().bind(inEdition.not());
        btnFindFunction.disableProperty().bind(inEdition.not());
        btnFindLevel.disableProperty().bind(inEdition.not());
        btnFindOperationGroup.disableProperty().bind(inEdition.not());
        
        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        
        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        
        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));
        
        try {
            listRegisters.set(DAOFactory.getSdColaborador001DAO().getAll());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeButtonsActions();
        makeColunsFactory();
        tblRegisters.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();

        boxInfosAdicionais.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.add(FormBox.create(boxEntradaPa -> {
                boxEntradaPa.horizontal();
                boxEntradaPa.add(fieldImpDefault.build(), fieldPerdidasAtivas.build());
            }));
            box.add(FormBox.create(boxEntradaDireta -> {
                boxEntradaDireta.horizontal();
                boxEntradaDireta.add(fieldEntradaDireta.build(), fieldIncompletasAtivas.build());
            }));
            box.add(FormBox.create(boxColeta -> {
                boxColeta.horizontal();
                boxColeta.add(fieldSegundoColetor.build(), fieldPisoColeta.build());
            }));
        }));

        boxAtivo.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.add(fieldAtivo.build());
        }));
    }
    
    private void searchAvailable(String oldVal, String newVal) {
        
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listOperationsAvailable.set(listOperationsAvailableAll);
        }
        
        String value = newVal.toUpperCase();
        ObservableList<SdOperacoesGrp001> subentries = FXCollections.observableArrayList();
        for (SdOperacoesGrp001 entry : listOperationsAvailable) {
            if (entry.getOperacao().getDescricao().toUpperCase().contains(value)) {
                subentries.add(entry);
            }
        }
        listOperationsAvailable.set(subentries);
    }
    
    private void searchSelected(String oldVal, String newVal) {
        
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listOperationsSelected.set(listOperationsSelectedAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdPolivalencia001> subentries = FXCollections.observableArrayList();
        for (SdPolivalencia001 entry : listOperationsSelected) {
            if (entry.getOperacao().getDescricao().toUpperCase().contains(value)) {
                subentries.add(entry);
            }
        }
        listOperationsSelected.set(subentries);
        
    }
    
    private void makeColumnsTblOperations() {
        clOperation.setCellFactory(column -> new TableCell<SdPolivalencia001, SdOperacaoOrganize001>() {
            @Override
            protected void updateItem(SdOperacaoOrganize001 item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    this.setText(item.getDescricao());
                }
            }
        });
        clnActiveOperation.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
        clnActiveOperation.setCellFactory(column -> new TableCell<SdPolivalencia001, SdPolivalencia001>() {
            
            final ImageView btnIconAtivar = new ImageView(new Image(getClass().getResource("/images/icons/buttons/active (1).png").toExternalForm()));
            final ImageView btnIconInativar = new ImageView(new Image(getClass().getResource("/images/icons/buttons/inactive (1).png").toExternalForm()));
            final Button btnAtivarOperacao = new Button();
            
            final Tooltip tipBtnAtivar = new Tooltip("Ativar Operação");
            final Tooltip tipBtnInativar = new Tooltip("Inativar Operação");
            
            @Override
            protected void updateItem(SdPolivalencia001 item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    btnAtivarOperacao.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    btnAtivarOperacao.getStyleClass().add("xs");
                    if (item.isAtivo()) {
                        btnAtivarOperacao.setGraphic(btnIconInativar);
                        btnAtivarOperacao.setTooltip(tipBtnInativar);
                        btnAtivarOperacao.getStyleClass().remove("success");
                        btnAtivarOperacao.getStyleClass().add("danger");
                    } else {
                        btnAtivarOperacao.setGraphic(btnIconAtivar);
                        btnAtivarOperacao.setTooltip(tipBtnAtivar);
                        btnAtivarOperacao.getStyleClass().remove("danger");
                        btnAtivarOperacao.getStyleClass().add("success");
                    }
                    setGraphic(btnAtivarOperacao);
                    btnAtivarOperacao.setOnAction(event -> {
                        item.setAtivo(!item.isAtivo());
                        tblOperactionsSelected.refresh();
                    });
                }
            }
        });
        
        clnLevelOperation.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
        clnLevelOperation.setCellFactory((TableColumn<SdPolivalencia001, SdPolivalencia001> column) -> new TableCell<SdPolivalencia001, SdPolivalencia001>() {
            @Override
            protected void updateItem(SdPolivalencia001 item, boolean empty) {
                setText(null);
                setGraphic(null);
                
                if (item != null && !empty) {
                    setText(item.getNivel().getSigla() + " : " + item.getNivel().getDescricao());
                    setGraphic(null);
                }
            }
        });
        
    }
    
    private void makeButtonsActions() {
        clnActions.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
        clnActions.setComparator(new Comparator<SdColaborador>() {
            @Override
            public int compare(SdColaborador p1, SdColaborador p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdColaborador, SdColaborador>, TableCell<SdColaborador, SdColaborador>>() {
            @Override
            public TableCell<SdColaborador, SdColaborador> call(TableColumn<SdColaborador, SdColaborador> btnCol) {
                return new TableCell<SdColaborador, SdColaborador>() {
                    
                    final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
                    final Button btnEditRow = StaticButtonBuilder.genericEditButton();
                    final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();
                    final Button btnCardPrintRow = StaticButtonBuilder.genericButtom(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR_TAG, ImageUtils.IconSize._16),
                            "Imprimir registro selecionado",
                            "Imprimir Registro",
                            new String[]{"primary", "xs"},
                            ContentDisplay.GRAPHIC_ONLY);
                    
                    final HBox boxButtonsRow = new HBox(3);
                    
                    @Override
                    public void updateItem(SdColaborador seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow, btnCardPrintRow);
                            setGraphic(boxButtonsRow);
                            
                            btnInfoRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                bindData(selectedItem);
                                tabMainPane.getSelectionModel().select(1);
                            });
                            btnEditRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                tabMainPane.getSelectionModel().select(1);
                                btnEditRegisterOnAction(null);
                            });
                            btnDeleteRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                btnDeleteRegisterOnAction(null);
                            });
                            btnCardPrintRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    ObservableList<SdColaborador> registersSelected = FXCollections.observableArrayList();
                                    if (tblRegisters.getSelectionModel().getSelectedItems().size() > 1) {
                                        registersSelected.addAll(tblRegisters.getSelectionModel().getSelectedItems());
                                    } else {
                                        registersSelected.add(seletedRow);
                                    }
                                    
                                    // First, compile jrxml file.
                                    JasperReport jasperReport;
                                    try {
                                        jasperReport = (JasperReport) JRLoader
                                                .loadObject(getClass().getResourceAsStream("/relatorios/CrachaColaborador.jasper"));
                                        
                                        // Fields for report
                                        HashMap<String, Object> parameters = new HashMap<>();
                                        for (SdColaborador colaborador : registersSelected) {
                                            parameters.clear();
                                            parameters.put("codcol", colaborador.getCodigo());
                                            
                                            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
                                            if (print.getPages().size() > 0) {
                                                new PrintReportPreview().showReport(print);
                                            } else {
                                                GUIUtils.showMessage("Não foi possível gerar o crachá do colaborador!");
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "SceneCadastroColaboradorController:btnCardPrintRow.setOnAction");
                                        GUIUtils.showException(e);
                                    }
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        
    }
    
    private void makeColunsFactory() {
        clnActive.setCellFactory(column -> new TableCell<SdColaborador, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    this.setText(item ? "S" : "N");
                }
            }
        });
        
        clnFunction.setCellFactory(column -> new TableCell<SdColaborador, SdFuncao001>() {
            
            @Override
            protected void updateItem(SdFuncao001 item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    this.setText(item.getDescricao());
                }
            }
        });
        
        clnPeriod.setCellFactory(column -> new TableCell<SdColaborador, SdTurno>() {
            
            @Override
            protected void updateItem(SdTurno item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    this.setText(item.getDescricao());
                }
            }
        });
    }
    
    private void clearForm() {
        tboxRegisterCode.clear();
        tboxName.clear();
        tboxCellPerson.clear();
        tboxCodeFunction.clear();
        tboxCodePeriod.clear();
        tboxCodeRH.clear();
        tboxUsuarioAD.clear();
        tboxCodFor.clear();
        lbDescriptionPeriod.textProperty().bind(new SimpleStringProperty(""));
        lbDescritpionFunction.textProperty().bind(new SimpleStringProperty(""));
        
        tboxCodeLevel.clear();
        tboxCodeOperationGroup.clear();
        lbDescriptionLevel.setText("");
        lbDescriptionOperationGroup.setText("");
        
        sdFuncaoPerson = null;
        sdTurnoPerson = null;
        sdGrupoOperacaoPerson = new SdGrupoOperacao001();
        sdLevelOperationPerson = new SdNivelPolivalencia001();
        
        listOperationsAvailable.clear();
        listOperationsAvailableAll.clear();
        listOperationsSelected.clear();
        listOperationsSelectedAll.clear();
    }
    
    private void bindData(SdColaborador selectItem) {
        if (selectItem != null) {
            
            try {
                selectItem.setOperacoes(DAOFactory.getSdPolivalencia001DAO().getByColaborador(selectItem));
                SdColabCelula001 celula = DAOFactory.getSdColabCelula001DAO().getByPerson(selectItem.getCodigo());
                selectItem.setCelula(celula != null ? celula.getCelula() : null);
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
                return;
            }
            
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxName.textProperty().bind(selectItem.nomeProperty());
            tboxCellPerson.textProperty().bind(new SimpleStringProperty(selectItem.getCelula() != null
                    ? selectItem.getCelula().getDescricao().concat(" [" + selectItem.getCelula().getCodigo() + "]")
                    : "NÃO CADASTRADO EM UM CÉLULA"));
            tboxCodeRH.textProperty().bind(selectItem.codigoRHProperty().asString());
            tboxCodeFunction.textProperty().bind(selectItem.codigoFuncaoProperty().asString());
            tboxCodePeriod.textProperty().bind(selectItem.codigoTurnoProperty().asString());
            
            tboxUsuarioAD.textProperty().bind(selectItem.usuarioProperty());
            tboxCodFor.textProperty().bind(selectItem.codforProperty());

            fieldAtivo.value.bind(selectItem.ativoProperty());
            fieldImpDefault.value.bind(selectItem.impressoraPadraoProperty());
            fieldPerdidasAtivas.value.bind(selectItem.perdidasAtivasProperty());
            fieldEntradaDireta.value.bind(selectItem.entradaDiretaProperty());
            fieldIncompletasAtivas.value.bind(selectItem.incompletasAtivasProperty());

            fieldPisoColeta.value.bind(selectItem.pisoColetaProperty().asString());
            fieldSegundoColetor.value.bind(selectItem.segundoColetorProperty());

            listOperationsSelected.bind(selectItem.operacoesProperty());
            listOperationsSelectedAll.clear();
            listOperationsSelectedAll.addAll(listOperationsSelected);
            
            lbDescritpionFunction.textProperty().bind(selectItem.funcaoProperty().get().descricaoProperty());
            lbDescriptionPeriod.textProperty().bind(selectItem.turnoProperty().get().descricaoProperty());
        }
    }
    
    private void unbindData() {
        tboxRegisterCode.textProperty().unbind();
        tboxName.textProperty().unbind();
        tboxCellPerson.textProperty().unbind();
        tboxCodeRH.textProperty().unbind();
        tboxCodeFunction.textProperty().unbind();
        tboxCodePeriod.textProperty().unbind();
        listOperationsSelected.unbind();
        tboxUsuarioAD.textProperty().unbind();
        tboxCodFor.textProperty().unbind();

        fieldAtivo.value.unbind();
        fieldImpDefault.value.unbind();
        fieldPerdidasAtivas.value.unbind();
        fieldSegundoColetor.value.unbind();
        fieldPisoColeta.value.unbind();
        fieldEntradaDireta.value.unbind();
        fieldIncompletasAtivas.value.unbind();

        lbDescritpionFunction.textProperty().unbind();
        lbDescriptionPeriod.textProperty().unbind();
        clearForm();
    }
    
    private void bindBidirectionalData(SdColaborador selectItem) {
        if (selectItem != null) {
            
            if (selectItem.getCodigo() != 0) {
                try {
                    selectItem.setOperacoes(DAOFactory.getSdPolivalencia001DAO().getByColaborador(selectItem));
                    SdColabCelula001 celula = DAOFactory.getSdColabCelula001DAO().getByPerson(selectItem.getCodigo());
                    selectItem.setCelula(celula != null ? celula.getCelula() : null);
                } catch (SQLException ex) {
                    Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                    return;
                }
            }
            
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxName.textProperty().bindBidirectional(selectItem.nomeProperty());
            tboxCodeRH.textProperty().bindBidirectional(selectItem.codigoRHProperty(), new NumberStringConverter());
            tboxUsuarioAD.textProperty().bindBidirectional(selectItem.usuarioProperty());
            tboxCodFor.textProperty().bindBidirectional(selectItem.codforProperty());
            tboxCodeFunction.textProperty().bindBidirectional(selectItem.codigoFuncaoProperty(), new NumberStringConverter());
            tboxCodePeriod.textProperty().bindBidirectional(selectItem.codigoTurnoProperty(), new NumberStringConverter());
            listOperationsSelected.bindBidirectional(selectItem.operacoesProperty());
            listOperationsSelectedAll.clear();
            listOperationsSelectedAll.addAll(listOperationsSelected);

            fieldAtivo.value.bindBidirectional(selectItem.ativoProperty());
            fieldImpDefault.value.bindBidirectional(selectItem.impressoraPadraoProperty());
            fieldPerdidasAtivas.value.bindBidirectional(selectItem.perdidasAtivasProperty());
            fieldPisoColeta.value.bindBidirectional(selectItem.pisoColetaProperty(), new NumberStringConverter());
            fieldSegundoColetor.value.bindBidirectional(selectItem.segundoColetorProperty());
            fieldEntradaDireta.value.bindBidirectional(selectItem.entradaDiretaProperty());
            fieldIncompletasAtivas.value.bindBidirectional(selectItem.incompletasAtivasProperty());

            lbDescritpionFunction.textProperty().bind(selectItem.funcaoProperty().get().descricaoProperty());
            lbDescriptionPeriod.textProperty().bind(selectItem.turnoProperty().get().descricaoProperty());
        }
    }
    
    private void unbindBidirectionalData(SdColaborador selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().unbind();
            tboxName.textProperty().unbindBidirectional(selectItem.nomeProperty());
            tboxCodeRH.textProperty().unbindBidirectional(selectItem.codigoRHProperty());
            tboxUsuarioAD.textProperty().unbindBidirectional(selectItem.usuarioProperty());
            tboxCodFor.textProperty().unbindBidirectional(selectItem.codforProperty());
            tboxCodeFunction.textProperty().unbindBidirectional(selectItem.codigoFuncaoProperty());
            tboxCodePeriod.textProperty().unbindBidirectional(selectItem.codigoTurnoProperty());
            listOperationsSelected.unbindBidirectional(selectItem.operacoesProperty());
            lbDescritpionFunction.textProperty().unbind();
            lbDescriptionPeriod.textProperty().unbind();

            fieldAtivo.value.unbindBidirectional(selectItem.ativoProperty());
            fieldImpDefault.value.unbindBidirectional(selectItem.impressoraPadraoProperty());
            fieldPerdidasAtivas.value.unbindBidirectional(selectItem.perdidasAtivasProperty());
            fieldPisoColeta.value.unbindBidirectional(selectItem.pisoColetaProperty());
            fieldSegundoColetor.value.unbindBidirectional(selectItem.segundoColetorProperty());
            fieldEntradaDireta.value.unbindBidirectional(selectItem.entradaDiretaProperty());
            fieldIncompletasAtivas.value.unbindBidirectional(selectItem.incompletasAtivasProperty());
        }
    }
    
    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdColaborador();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        
        tabMainPane.getSelectionModel().select(1);
        tboxName.requestFocus();
        event.consume();
    }
    
    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(DAOFactory.getSdColaborador001DAO().getByDefault(tboxDefaultFilter.textProperty().get()));
            tboxDefaultFilter.clear();
            event.consume();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
        event.consume();
    }
    
    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
        event.consume();
    }
    
    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
        event.consume();
    }
    
    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
        event.consume();
    }
    
    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
        if (event != null) {
            event.consume();
        }
    }
    
    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                DAOFactory.getSdColaborador001DAO().delete(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro Turno",
                        selectedItem.getCodigo() + "",
                        "Excluído o turno " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                ));
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
        if (event != null) {
            event.consume();
        }
    }
    
    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
        event.consume();
    }
    
    @FXML
    private void btnSaveOnAction(ActionEvent event) {
        event.consume();
        try {
            ValidationForm.validationEmpty(tboxName);
            ValidationForm.validationEmpty(tboxCodeFunction);
            ValidationForm.validationEmpty(tboxCodePeriod);
            ValidationForm.validationEmpty(tboxCodeRH);
            
            if (tboxFindOperationsSelected.getText().length() > 0) {
                GUIUtils.showMessage("Você deve limpar o filtro das operações selecionadas.", Alert.AlertType.INFORMATION);
                return;
            }
            
        } catch (FormValidationException ex) {
            return;
        }
        
        unbindBidirectionalData(selectedItem);
        SdColaborador objectEdited = selectedItem;
        try {
            if (selectedItem.getCodigo() == 0) {
                DAOFactory.getSdColaborador001DAO().save(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro Colaborador",
                        selectedItem.getCodigo() + "",
                        "Cadastrado o colaborador " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                ));
                if (!listRegisters.contains(selectedItem)) {
                    listRegisters.add(selectedItem);
                }
            } else {
                DAOFactory.getSdColaborador001DAO().update(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro Colaborador",
                        selectedItem.getCodigo() + "",
                        "Alterado o colaborador " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().select(objectEdited);
    }
    
    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
        event.consume();
    }
    
    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdColaborador();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        tboxName.requestFocus();
        event.consume();
    }
    
    @FXML
    private void btnFindPeriodOnAction(ActionEvent event) {
        try {
            ObservableList<SdTurno> sendSelected = FXCollections.observableArrayList();
            if (sdTurnoPerson != null) {
                sendSelected.add(sdTurnoPerson);
            }
            FilterTurnoController ctrlFilter = new FilterTurnoController(Modals.FXMLWindow.FilterTurno,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                sdTurnoPerson = ctrlFilter.selectedObjects.get(0);
                selectedItem.copyTurno(sdTurnoPerson);
            }
            event.consume();
        } catch (IOException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnFindFunctionOnAction(ActionEvent event) {
        try {
            ObservableList<SdFuncao001> sendSelected = FXCollections.observableArrayList();
            if (sdFuncaoPerson != null) {
                sendSelected.add(sdFuncaoPerson);
            }
            FilterFuncaoColaboradorController ctrlFilter = new FilterFuncaoColaboradorController(Modals.FXMLWindow.FilterFuncaoColaborador,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                sdFuncaoPerson = ctrlFilter.selectedObjects.get(0);
                selectedItem.copyFuncao(sdFuncaoPerson);
            }
            event.consume();
        } catch (IOException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnAddSelectedOnAction(ActionEvent event) {
        if (sdLevelOperationPerson.getCodigo() == 0) {
            GUIUtils.showMessage("Você deve escolher o nível para as operações.", Alert.AlertType.INFORMATION);
            return;
        }
        listOperationsSelected.set(FXCollections.observableArrayList(listOperationsSelected));
        ObservableList<SdOperacoesGrp001> listToDelete = lviewOperationsAvailable.getSelectionModel().getSelectedItems();
        listToDelete.forEach(operation -> {
            SdNivelPolivalencia001 levelOperation = new SdNivelPolivalencia001(sdLevelOperationPerson);
            listOperationsSelected.add(new SdPolivalencia001(selectedItem.getCodigo(),
                    operation.getOperacao().getCodorg(),
                    levelOperation.getCodigo(),
                    "S",
                    selectedItem, operation.getOperacao(),
                    levelOperation));
        });
        listOperationsSelectedAll.clear();
        listOperationsSelectedAll.addAll(listOperationsSelected);
        listOperationsAvailable.removeAll(listToDelete);
        listOperationsAvailableAll.removeAll(listToDelete);
        event.consume();
    }
    
    @FXML
    private void btnAddAllOnAction(ActionEvent event) {
        if (sdLevelOperationPerson.getCodigo() == 0) {
            GUIUtils.showMessage("Você deve escolher o nível para as operações.", Alert.AlertType.INFORMATION);
            return;
        }
        listOperationsSelected.set(FXCollections.observableArrayList());
        ObservableList<SdOperacoesGrp001> listToDelete = lviewOperationsAvailable.getItems();
        listToDelete.forEach(operation -> {
            SdNivelPolivalencia001 levelOperation = new SdNivelPolivalencia001(sdLevelOperationPerson);
            listOperationsSelected.add(new SdPolivalencia001(selectedItem.getCodigo(),
                    operation.getOperacao().getCodorg(),
                    levelOperation.getCodigo(),
                    "S",
                    selectedItem, operation.getOperacao(),
                    levelOperation));
        });
        listOperationsAvailable.removeAll(listToDelete);
        listOperationsAvailableAll.removeAll(listToDelete);
        listOperationsSelectedAll.clear();
        listOperationsSelectedAll.addAll(listOperationsSelected);
        event.consume();
    }
    
    @FXML
    private void btnFindOperationGroupOnAction(ActionEvent event) {
        try {
            ObservableList<SdGrupoOperacao001> sendSelected = FXCollections.observableArrayList();
            if (sdGrupoOperacaoPerson.getCodigo() != 0) {
                sendSelected.add(sdGrupoOperacaoPerson);
            }
            FilterGrupoOperacaoController ctrlFilter = new FilterGrupoOperacaoController(Modals.FXMLWindow.FilterGrupoOperacao,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                sdGrupoOperacaoPerson.copy(ctrlFilter.selectedObjects.get(0));
                sdGrupoOperacaoPerson.setSelected(true);
                sdGrupoOperacaoPerson.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getAvailableOperationsGroup(sdGrupoOperacaoPerson.getCodigo(), selectedItem.getCodigo()));
                listOperationsAvailable.set(sdGrupoOperacaoPerson.getOperacoes());
                listOperationsAvailableAll.clear();
                listOperationsAvailableAll.addAll(listOperationsAvailable);
                tboxCodeOperationGroup.setText(sdGrupoOperacaoPerson.getCodigo() + "");
                lbDescriptionOperationGroup.setText(sdGrupoOperacaoPerson.getDescricao());
            }
            event.consume();
        } catch (IOException | SQLException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnFindLevelOnAction(ActionEvent event) {
        try {
            ObservableList<SdNivelPolivalencia001> sendSelected = FXCollections.observableArrayList();
            if (sdLevelOperationPerson.getCodigo() != 0) {
                sendSelected.add(sdLevelOperationPerson);
            }
            FilterNivelController ctrlFilter = new FilterNivelController(Modals.FXMLWindow.FilterNivel,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                sdLevelOperationPerson.copy(ctrlFilter.selectedObjects.get(0));
                sdLevelOperationPerson.setSelected(true);
                tboxCodeLevel.setText(sdLevelOperationPerson.getCodigo() + "");
                lbDescriptionLevel.setText(sdLevelOperationPerson.getDescricao());
                
                ObservableList<SdPolivalencia001> operationsSelecteds = tblOperactionsSelected.getSelectionModel().getSelectedItems();
                if (operationsSelecteds.size() > 0) {
                    operationsSelecteds.forEach(operation -> {
                        SdNivelPolivalencia001 levelOperation = new SdNivelPolivalencia001(sdLevelOperationPerson);
                        operation.setCodigoNivel(levelOperation.getCodigo());
                        operation.setNivel(levelOperation);
                    });
                    tblOperactionsSelected.refresh();
                }
            }
            event.consume();
        } catch (IOException ex) {
            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnDeleteSelectedOnAction(ActionEvent event) {
        ObservableList<SdPolivalencia001> listToDelete = tblOperactionsSelected.getSelectionModel().getSelectedItems();
        listOperationsSelected.removeAll(listToDelete);
        listOperationsSelectedAll.removeAll(listToDelete);
        event.consume();
    }
    
    @FXML
    private void btnDeleteAllOnAction(ActionEvent event) {
        ObservableList<SdPolivalencia001> listToDelete = tblOperactionsSelected.getItems();
        listOperationsSelected.removeAll(listToDelete);
        listOperationsSelectedAll.removeAll(listToDelete);
        event.consume();
    }
    
    @FXML
    private void tboxCodePeriodOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindPeriodOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                sdTurnoPerson = DAOFactory.getSdTurnoDAO().getByCode(Integer.parseInt(tboxCodePeriod.getText()));
                if (sdTurnoPerson == null) {
                    GUIUtils.showMessage("Código de turno não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    sdTurnoPerson.setSelected(true);
                    selectedItem.copyTurno(sdTurnoPerson);
                    tboxCodeFunction.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
    
    @FXML
    private void tboxCodeFunctionOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindFunctionOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                sdFuncaoPerson = DAOFactory.getSdFuncaoDAO().getByCode(Integer.parseInt(tboxCodeFunction.getText()));
                if (sdFuncaoPerson == null) {
                    GUIUtils.showMessage("Código de função não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    sdFuncaoPerson.setSelected(true);
                    selectedItem.copyFuncao(sdFuncaoPerson);
                    tboxCodeRH.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
    
    @FXML
    private void tboxCodeOperationGroupOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindOperationGroupOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                sdGrupoOperacaoPerson.copy(DAOFactory.getSdGrupoOperacao001DAO().getByCode(Integer.parseInt(tboxCodeOperationGroup.getText())));
                if (sdGrupoOperacaoPerson.getCodigo() == 0) {
                    GUIUtils.showMessage("Código de grupo não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    sdGrupoOperacaoPerson.setSelected(true);
                    sdGrupoOperacaoPerson.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getAvailableOperationsGroup(sdGrupoOperacaoPerson.getCodigo(), selectedItem.getCodigo()));
                    listOperationsAvailable.set(sdGrupoOperacaoPerson.getOperacoes());
                    listOperationsAvailableAll.clear();
                    listOperationsAvailableAll.addAll(listOperationsAvailable);
                    tboxCodeOperationGroup.setText(sdGrupoOperacaoPerson.getCodigo() + "");
                    lbDescriptionOperationGroup.setText(sdGrupoOperacaoPerson.getDescricao());
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
    
    @FXML
    private void tboxCodeLevelOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnFindLevelOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                sdLevelOperationPerson.copy(DAOFactory.getSdNivelPolivalenciaDAO().getByCode(Integer.parseInt(tboxCodeLevel.getText())));
                if (sdLevelOperationPerson.getCodigo() == 0) {
                    GUIUtils.showMessage("Código de nível não encontrado.", Alert.AlertType.INFORMATION);
                } else {
                    sdLevelOperationPerson.setSelected(true);
                    tboxCodeLevel.setText(sdLevelOperationPerson.getCodigo() + "");
                    lbDescriptionLevel.setText(sdLevelOperationPerson.getDescricao());
                    ObservableList<SdPolivalencia001> operationsSelecteds = tblOperactionsSelected.getSelectionModel().getSelectedItems();
                    if (operationsSelecteds.size() > 0) {
                        operationsSelecteds.forEach(operation -> {
                            SdNivelPolivalencia001 levelOperation = new SdNivelPolivalencia001(sdLevelOperationPerson);
                            operation.setCodigoNivel(levelOperation.getCodigo());
                            operation.setNivel(levelOperation);
                        });
                        tblOperactionsSelected.refresh();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
    
    
    private static class ListOperationsCell extends ListCell<SdOperacoesGrp001> {
        
        @Override
        public void updateItem(SdOperacoesGrp001 item, boolean empty) {
            super.updateItem(item, empty);
            this.setText(null);
            this.setGraphic(null);
            if (item != null || !empty) {
                assert item != null;
                item.getCodigoOperacao();
                this.setText(item.getOperacao().getDescricao());
            }
        }
    }
    
}
