package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import org.controlsfx.control.ToggleSwitch;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.cadastros.base.SceneCadastroBase;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.Log;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.Constantes;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author lima.joao
 * @since 21/08/2019 17:37
 */
public class SceneCadastroEntidadesController extends SceneCadastroBase<Entidade> implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private TableColumn<Entidade, String> clnCodigo;
    @FXML
    private TableColumn<Entidade, String> clnRazaoSocial;
    @FXML
    private TableColumn<Entidade, String> clnTelefone;
    @FXML
    private TableColumn<Entidade, String> clnCelular;
    @FXML
    private TableColumn<Entidade, String> clnDtUltNota;
    @FXML
    private TableColumn<Entidade, String> clnDtUltPedido;
    @FXML
    private TableColumn<Entidade, String> clnDtUltCompra;
    @FXML
    private TableColumn<Entidade, String> clnDtCadastro;
    @FXML
    private TableColumn<Entidade, String> clnNomeCidade;
    @FXML
    private TableColumn<Entidade, String> clnCodigoCidade;
    @FXML
    private TableColumn<Entidade, String> clnUf;
    @FXML
    private TableColumn<Entidade, String> clnTipoEntidade;
    @FXML
    private TableColumn<Entidade, ImageView> clnAtivo;
    @FXML
    private TableColumn<Entidade, ImageView> clnBloqueado;
    @FXML
    private TableColumn<Entidade, String> clnTipoCliente;
    @FXML
    private TableColumn<Entidade, String> clnMotivo;
    @FXML
    private TableColumn<Entidade, String> clnDescMotivo;
    @FXML
    private TableColumn<Entidade, String> clnClasse;
    @FXML
    private TableColumn<Entidade, String> clnDescClasse;

    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxDocumento;
    @FXML
    private Button btnConsultaReceitaWS;
    @FXML
    private TextField tboxTipo;
    @FXML
    private TextField tboxInscricao;
    @FXML
    private TextField tboxDtCadastro;
    @FXML
    private TextField tboxRazaoSocial;
    @FXML
    private TextField tboxNomeFantasia;
    @FXML
    private ToggleSwitch toggleCliente;
    @FXML
    private ToggleSwitch toggleFonecedor;
    @FXML
    private ToggleSwitch toggleTerceiro;
    @FXML
    private ToggleSwitch toggleAtivo;
    @FXML
    private ToggleSwitch toggleBloqueado;
    @FXML
    private TextField tboxMotivo;
    @FXML
    private Button btnFindMotivo;
    @FXML
    private TextField tboxTelefone;
    @FXML
    private TextField tboxCelular;
    @FXML
    private TextField tboxEmail;
    @FXML
    private TextField tboxNomeRamoAtividade;
    @FXML
    private Button btnFindRamoAtividade;
    @FXML
    private TextField tboxNomeSitCli;
    @FXML
    private Button btnFindSitCli;
    @FXML
    private TextField tboxNomeGrupo;
    @FXML
    private Button btnFindGrupo;
    @FXML
    private TextField tboxLimiteCredito;
    @FXML
    private TabPane tPanelEnderecos;
    @FXML
    private TextField tboxPais;
    @FXML
    private Button btnFindPais;
    @FXML
    private TextField tboxCep;
    @FXML
    private Button btnFindCep;
    @FXML
    private TextField tboxUf;
    @FXML
    private TextField tboxCidade;
    @FXML
    private TextField tboxBairro;
    @FXML
    private TextField tboxEndereco;
    @FXML
    private TextField tboxNumero;
    @FXML
    private TextField tboxComplemento;
    @FXML
    private TextField tboxCnpjEntrega;
    @FXML
    private Button btnConsultaReceitaWSEntrega;
    @FXML
    private TextField tboxInscricaoEntrega;
    @FXML
    private TextField tboxCepEntrega;
    @FXML
    private Button btnFindCepEntrega;
    @FXML
    private TextField tboxUfEntrega;
    @FXML
    private TextField tboxCidadeEntrega;
    @FXML
    private TextField tboxBairroEntrega;
    @FXML
    private TextField tboxEnderecoEntrega;
    @FXML
    private TextField tboxNumeroEntrega;
    @FXML
    private TextField tboxComplementoEntrega;
    @FXML
    private TextField tboxCnpjCobranca;
    @FXML
    private Button btnFindCnpjCobranca;
    @FXML
    private TextField tboxInscricaoCobranca;
    @FXML
    private TextField tboxCepCobranca;
    @FXML
    private Button btnFindCepCobranca;
    @FXML
    private TextField tboxUfCobranca;
    @FXML
    private TextField tboxCidadeCobranca;
    @FXML
    private TextField tboxBairroCobranca;
    @FXML
    private TextField tboxEnderecoCobranca;
    @FXML
    private TextField tboxNumeroCobranca;
    @FXML
    private TextField tboxComplementoCobranca;

    //</editor-fold>

    @Override
    protected void bindDados() {
        if (selectedItem != null) {
            tboxCodigo.textProperty().bind(selectedItem.codcliProperty().asString());

            //tboxDtCadastro.textProperty().bind( new SimpleStringProperty(DateUtils.getInstance().getSimpleDate(selectedItem.getDataCad().toString())));
            tboxDtCadastro.textProperty().bind(new SimpleStringProperty(new SimpleDateFormat("dd/MM/yyyy").format(selectedItem.getDataCad())));

            toggleCliente.setSelected(false);
            toggleFonecedor.setSelected(false);
            toggleTerceiro.setSelected(false);

            for (char c : selectedItem.getTipoEntidade().toCharArray()) {
                switch (c) {
                    case 'C':
                        toggleCliente.setSelected(true);
                        break;
                    case 'F':
                        toggleFonecedor.setSelected(true);
                        break;
                    case 'T':
                        toggleTerceiro.setSelected(true);
                        break;
                }
            }

            if (!isBindBiDirectional) {
                tboxDocumento.textProperty().bind(selectedItem.cnpjProperty());
                tboxInscricao.textProperty().bind(selectedItem.inscricaoProperty());

                tboxRazaoSocial.textProperty().bind(selectedItem.nomeProperty());
                tboxNomeFantasia.textProperty().bind(selectedItem.fantasiaProperty());

                toggleAtivo.selectedProperty().bind(selectedItem.ativoProperty());
                toggleBloqueado.selectedProperty().bind(selectedItem.bloqueioProperty());

                // Contato
                tboxTelefone.textProperty().bind(selectedItem.telefoneProperty());
                tboxCelular.textProperty().bind(selectedItem.faxProperty());
                tboxEmail.textProperty().bind(selectedItem.emailProperty());

                // Financeiro
                tboxLimiteCredito.textProperty().bind(selectedItem.creditoProperty().asString());
                // Endereço

            } else {
//                tboxNome.textProperty().bindBidirectional(selectedItem.nomeProperty());
//                tboxUrl.textProperty().bindBidirectional(selectedItem.urlProperty());
            }

            if (tboxDocumento.getText() == null || tboxDocumento.getText().length() > 14) {
                tboxTipo.setText("Pessoa Juridica");
            } else {
                tboxTipo.setText("Pessoa Física");
            }

            tboxTipo.setEditable(false);

            // Financeiro -- Somente leitura
            if (selectedItem.getAtividade() == null) {
                tboxNomeRamoAtividade.textProperty().bind(new SimpleStringProperty("Não informado"));
            } else {
                tboxNomeRamoAtividade.textProperty().bind(selectedItem.getAtividade().descricaoProperty());
            }

            if (selectedItem.getSitCli() == null) {
                tboxNomeSitCli.textProperty().bind(new SimpleStringProperty("Não informado"));
            } else {
                tboxNomeSitCli.textProperty().bind(selectedItem.getSitCli().descricaoProperty());
            }

            if (selectedItem.getGrupo() == null) {
                tboxNomeGrupo.textProperty().bind(new SimpleStringProperty("Não informado"));
            } else {
                tboxNomeGrupo.textProperty().bind(selectedItem.getGrupo().descricaoProperty());
            }

//            toggleAtivo.setSelected(selectedItem.getAtivo().equals("S"));
//            toggleBloqueado.setSelected(selectedItem.getBloqueio().equals("S"));

        }
    }

    @Override
    protected void unbindDados() {
        tboxCodigo.textProperty().unbind();
        tboxDtCadastro.textProperty().unbind();
        if (!isBindBiDirectional) {
            tboxDocumento.textProperty().unbind();
            tboxInscricao.textProperty().unbind();

            tboxRazaoSocial.textProperty().unbind();
            tboxNomeFantasia.textProperty().unbind();

        } else {
//            tboxNome.textProperty().unbindBidirectional(selectedItem.nomeProperty());
//            tboxUrl.textProperty().unbindBidirectional(selectedItem.urlProperty());
        }
        isBindBiDirectional = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //
        this.titulo = "Cadastro de Redes Sociais";
        this.genericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Entidade.class);

        // Carrega a lista de representantes
        try {
            lstRegistros = this.genericDao.list();
        } catch (SQLException e) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, e);
            GUIUtils.showException(e);
        }

        tblRegisters.setItems(lstRegistros);

        // Mantém o campo pesquisa sempre em caixa alta.
        TextFieldUtils.upperCase(tboxDefaultFilter);
//        TextFieldUtils.lowerCaseTextField(tboxUrl);
        MaskTextField.cpfCnpjField(tboxDocumento);
        MaskTextField.numericField(tboxLimiteCredito);

        // Binda para sempre que alterar a quantidade de registros corrigir a descricao.
        lbRegistersCount.textProperty()
                .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));

        // Controle dos botões
        //<editor-fold desc="Botões">
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnConsultaReceitaWS.disableProperty().bind(disableBtnSaveRegister);
        btnFindRamoAtividade.disableProperty().bind(disableBtnSaveRegister);
        btnFindSitCli.disableProperty().bind(disableBtnSaveRegister);
        btnFindGrupo.disableProperty().bind(disableBtnSaveRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(lstRegistros.size() - 1));
        //</editor-fold>

        makeButtonsActions(true, true, true);

        //<editor-fold desc="Colunas tela principal">
        // Configura as colunas
        clnCodigo.setCellValueFactory(param -> param.getValue().codcliProperty().asString());
        clnRazaoSocial.setCellValueFactory(param -> param.getValue().nomeProperty());
        clnTelefone.setCellValueFactory(param -> param.getValue().telefoneProperty());
        clnCelular.setCellValueFactory(param -> param.getValue().faxProperty());

        clnDtUltNota.setCellValueFactory(param -> new SimpleStringProperty("12/12/1990"));
        clnDtUltPedido.setCellValueFactory(param -> new SimpleStringProperty("12/12/1990"));
        clnDtUltCompra.setCellValueFactory(param -> new SimpleStringProperty("12/12/1990"));
        clnDtCadastro.setCellValueFactory(param -> new SimpleStringProperty(new SimpleDateFormat("dd/MM/yyyy").format(param.getValue().getDataCad())));

        clnNomeCidade.setCellValueFactory(param -> {
            if ((param.getValue().getCep() != null) && (param.getValue().getCep().getCidade() != null)) {
                return param.getValue().getCep().getCidade().nomeCidProperty();
            } else {
                return new SimpleStringProperty("Não Informado");
            }
        });

        clnCodigoCidade.setCellValueFactory(param -> {
            if ((param.getValue().getCep() != null) && (param.getValue().getCep().getCidade() != null)) {
                return param.getValue().getCep().getCidade().codCidProperty();
            } else {
                return new SimpleStringProperty("0000000");
            }
        });

        clnUf.setCellValueFactory(param -> {
            if ((param.getValue().getCep() != null) && (param.getValue().getCep().getCidade() != null)) {
                return param.getValue().getCep().getCidade().getCodEst().getId().siglaEstProperty();
            } else {
                return new SimpleStringProperty("XX");
            }
        });

        clnTipoEntidade.setCellValueFactory(param -> param.getValue().tipoEntidadeProperty());

        clnAtivo.setCellValueFactory(param -> {
            if (param.getValue().getAtivo()) {
                return new SimpleObjectProperty<>(Constantes.newImageViewActive());
            } else {
                return new SimpleObjectProperty<>(Constantes.newImageViewInactive());
            }
        });

        clnBloqueado.setCellValueFactory(param -> {
            if (!param.getValue().getBloqueio()) {
                return new SimpleObjectProperty<>(Constantes.newImageViewActive());
            } else {
                return new SimpleObjectProperty<>(Constantes.newImageViewInactive());
            }

        });

        clnTipoCliente.setCellValueFactory(param -> param.getValue().tipoProperty());
        clnMotivo.setCellValueFactory(param -> param.getValue().getCodMot().codmenProperty());
        clnDescMotivo.setCellValueFactory(param -> new SimpleStringProperty(""));
        clnClasse.setCellValueFactory(param -> param.getValue().getClasse().codigoProperty());
        clnDescClasse.setCellValueFactory(param -> new SimpleStringProperty(""));
        //</editor-fold>

        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindDados();
            selectedItem = newValue;
            bindDados();

            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });

        // Campos sempre inativos
        tboxCodigo.setEditable(false);

        toggleCliente.disableProperty().bind(inEdition.not());
        toggleFonecedor.disableProperty().bind(inEdition.not());
        toggleTerceiro.disableProperty().bind(inEdition.not());

//        toggleAtivo.disableProperty().bind(inEdition.not());
//        toggleBloqueado.disableProperty().bind(inEdition.not());

        // Campos inativados dinamicamente
//        tboxNome.disableProperty().bind(inEdition.not());
//        tboxUrl.disableProperty().bind(inEdition.not());
    }

    @FXML
    @Override
    public void btnAddRegisterOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            unbindDados();
            selectedItem = new Entidade();
            isBindBiDirectional = true;
            inEdition.set(true);
            bindDados();

            tabMainPane.getSelectionModel().select(1);
            //tboxNome.requestFocus();

            actionEvent.consume();
        }
    }

    @FXML
    @Override
    public void btnFindDefaultOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            try {
                this.genericDao
                        .initCriteria()
                        .addPredicateLike("nome", "%" + tboxDefaultFilter.textProperty().get() + "%");

                lstRegistros.setAll(this.genericDao.loadListByPredicate());

                lbRegistersCount.textProperty()
                        .bind(new SimpleStringProperty("Mostrando ")
                                .concat(lstRegistros.size())
                                .concat(" registros"));

                tboxDefaultFilter.clear();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
            actionEvent.consume();
        }
    }

    @FXML
    @Override
    protected void btnEditRegisterOnAction(ActionEvent actionEvent) {
        if (actionEvent == null || !actionEvent.isConsumed()) {
            unbindDados();
            inEdition.set(true);
            isBindBiDirectional = true;
            bindDados();

            if (actionEvent != null) {
                actionEvent.consume();
            }
        }
    }

    @FXML
    @Override
    public void btnDeleteRegisterOnAction(ActionEvent actionEvent) {
        if (actionEvent == null || !actionEvent.isConsumed()) {
            if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
                try {
                    this.genericDao.delete(selectedItem.getCodrep());
                    lstRegistros.remove(selectedItem);
                    tblRegisters.getSelectionModel().selectFirst();

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.EXCLUIR,
                            this.titulo,
                            selectedItem.getCodrep() + "",
                            "Removido a rede social " + selectedItem.getNome() + " [" + selectedItem.getCodrep() + "]"
                    ));
                } catch (SQLException ex) {
                    Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
            if (actionEvent != null) {
                actionEvent.consume();
            }
        }
    }

    @FXML
    public void btnSaveOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {

            try {
                if (selectedItem.getCodrep() == "0") {
                    // Atualiza/Grava na tabela Represen_001
                    selectedItem = this.genericDao.save(selectedItem);

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.CADASTRAR,
                            "Cadastro de Redes Sociais",
                            selectedItem.getCodrep() + "",
                            "Cadastrado o Marca concorrente " + selectedItem.getNome() + " [" + selectedItem.getCodrep() + "]"
                    ));

                    if (!lstRegistros.contains(selectedItem)) {
                        lstRegistros.add(selectedItem);
                    }

                } else {
                    selectedItem = this.genericDao.update(selectedItem);

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.EDITAR,
                            "Cadastro de Marcas concorrentes",
                            selectedItem.getCodrep() + "",
                            "Alterado a marca concorrente " + selectedItem.getNome() + " [" + selectedItem.getCodrep() + "]"
                    ));
                }

                unbindDados();

                lbRegistersCount.textProperty()
                        .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }

            inEdition.set(false);
            tblRegisters.getSelectionModel().select(selectedItem);
            actionEvent.consume();
        }
    }

    public void btnConsultaReceitaWS(ActionEvent actionEvent) {
    }

    public void btnFindRamoAtividadeOnAction(ActionEvent actionEvent) {
    }

    public void btnFindSitCliOnAction(ActionEvent actionEvent) {
    }

    public void btnFindGrupoOnAction(ActionEvent actionEvent) {
    }

    public void btnFindPaisOnAction(ActionEvent actionEvent) {
    }

    public void btnFindCepOnAction(ActionEvent actionEvent) {
    }

    public void btnConsultaReceitaWSEntregaOnAction(ActionEvent actionEvent) {
    }

    public void btnFindCepEntregaOnAction(ActionEvent actionEvent) {
    }

    public void btnFindCnpjCobrancaOnAction(ActionEvent actionEvent) {
    }

    public void btnFindCepCobrancaOnAction(ActionEvent actionEvent) {
    }

    public void btnFindMotivoOnAction(ActionEvent actionEvent) {
    }
}
