/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Log;
import sysdeliz2.models.SdGrupoOperacao001;
import sysdeliz2.models.SdOperacoesGrp001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;

import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastroGrupoOperacoesController implements Initializable {

    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdGrupoOperacao001> listRegisters = new SimpleListProperty();
    private final ListProperty<SdOperacoesGrp001> listOperationsAvailable = new SimpleListProperty();
    private final ListProperty<SdOperacoesGrp001> listOperationsSelected = new SimpleListProperty();
    private final ObservableList<SdOperacoesGrp001> listOperationsAvailableAll = FXCollections.observableArrayList();
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private SdGrupoOperacao001 selectedItem = null;
    private ToggleSwitch tsActive = new ToggleSwitch(24, 55);

    @FXML
    private TabPane tabMainPane;
    @FXML
    private Button btnAddRegister;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private TableView<SdGrupoOperacao001> tblRegisters;
    @FXML
    private TableColumn<SdGrupoOperacao001, SdGrupoOperacao001> clnActions;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxRegisterCode;
    @FXML
    private TextField tboxDescriptionCode;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private Pane pnToggleActive;
    @FXML
    private Label lbToggleActive;
    @FXML
    private TextField tboxFindOperation;
    @FXML
    private TableColumn<SdGrupoOperacao001, Boolean> clnActive;
    @FXML
    private Label lbOperationsCount;
    @FXML
    private ListView<SdOperacoesGrp001> listAvailable;
    @FXML
    private Button btnAddSelected;
    @FXML
    private Button btnAddAll;
    @FXML
    private Button btnRemoveSelected;
    @FXML
    private Button btnRemoveAll;
    @FXML
    private ListView<SdOperacoesGrp001> listSelected;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxDescriptionCode);
        TextFieldUtils.upperCase(tboxFindOperation);
        tboxFindOperation.disableProperty().bind(inEdition.not());

        listAvailable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listSelected.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listAvailable.itemsProperty().bind(listOperationsAvailable);
        listSelected.itemsProperty().bind(listOperationsSelected);
        btnAddSelected.disableProperty().bind(inEdition.not().or(listAvailable.getSelectionModel().selectedItemProperty().isNull()));
        btnAddAll.disableProperty().bind(inEdition.not().or(listOperationsAvailable.emptyProperty()));
        btnRemoveSelected.disableProperty().bind(inEdition.not().or(listSelected.getSelectionModel().selectedItemProperty().isNull()));
        btnRemoveAll.disableProperty().bind(inEdition.not().or(listOperationsSelected.emptyProperty()));
        makeCellListOperations();

        pnToggleActive.getChildren().add(tsActive);
        lbToggleActive.textProperty().bind(Bindings.when(tsActive.switchedOnProperty()).then("SIM").otherwise("NÃO"));

        lbOperationsCount.textProperty().bind(new SimpleStringProperty("Total de ").concat(listOperationsSelected.sizeProperty()).concat(" operações selecionadas"));
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));

        try {
            listRegisters.set(DAOFactory.getSdGrupoOperacao001DAO().getAll());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeButtonsActions();
        makeColunsFactory();
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();
    }

    public void searchAvailable(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listOperationsAvailable.set(listOperationsAvailableAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdOperacoesGrp001> subentries = FXCollections.observableArrayList();
        for (SdOperacoesGrp001 entry : listOperationsAvailable) {
            boolean match = true;
            if (!entry.getOperacao().getDescricao().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listOperationsAvailable.set(subentries);

    }

    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdGrupoOperacao001, SdGrupoOperacao001>, ObservableValue<SdGrupoOperacao001>>() {
            @Override
            public ObservableValue<SdGrupoOperacao001> call(TableColumn.CellDataFeatures<SdGrupoOperacao001, SdGrupoOperacao001> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdGrupoOperacao001>() {
            @Override
            public int compare(SdGrupoOperacao001 p1, SdGrupoOperacao001 p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdGrupoOperacao001, SdGrupoOperacao001>, TableCell<SdGrupoOperacao001, SdGrupoOperacao001>>() {
            @Override
            public TableCell<SdGrupoOperacao001, SdGrupoOperacao001> call(TableColumn<SdGrupoOperacao001, SdGrupoOperacao001> btnCol) {
                return new TableCell<SdGrupoOperacao001, SdGrupoOperacao001>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(SdGrupoOperacao001 seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    btnDeleteRegisterOnAction(null);
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }

        });

    }

    private void makeColunsFactory() {
        clnActive.setCellFactory(column -> {
            TableCell<SdGrupoOperacao001, Boolean> cell = new TableCell<SdGrupoOperacao001, Boolean>() {

                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item ? "S" : "N");
                    }
                }
            };

            return cell;
        });
    }

    private void makeCellListOperations() {
        listAvailable.setCellFactory(new Callback<ListView<SdOperacoesGrp001>, ListCell<SdOperacoesGrp001>>() {
            @Override
            public ListCell<SdOperacoesGrp001> call(ListView<SdOperacoesGrp001> param) {
                return new ListOperationsCell();
            }
        });
        listSelected.setCellFactory(new Callback<ListView<SdOperacoesGrp001>, ListCell<SdOperacoesGrp001>>() {
            @Override
            public ListCell<SdOperacoesGrp001> call(ListView<SdOperacoesGrp001> param) {
                return new ListOperationsCell();
            }
        });
        tboxFindOperation.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchAvailable((String) oldValue, (String) newValue);
            }
        });
    }

    private void prepareListAvailable(Integer codeGroup) {
        try {
            listOperationsAvailable.set(DAOFactory.getSdOperacoesGrp001DAO().getAvailableOperations(codeGroup));
            listOperationsAvailableAll.clear();
            listOperationsAvailableAll.addAll(listOperationsAvailable);

            //listOperationsGroup.sourceItemsProperty().unbind();
            //listOperationsGroup.sourceItemsProperty().bind(listOperationsAvailable);
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void clearForm() {
        tboxRegisterCode.clear();
        tboxDescriptionCode.clear();
    }

    private void getSelectedOperations(SdGrupoOperacao001 selectItem) {
        if (selectItem.operacoesProperty().sizeProperty().get() <= 0) {
            try {
                selectItem.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(selectItem.getCodigo()));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroGrupoOperacoesController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    private void bindData(SdGrupoOperacao001 selectItem) {
        if (selectItem != null) {
            this.getSelectedOperations(selectItem);
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bind(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().bind(selectItem.ativoProperty());
            listOperationsSelected.bind(selectItem.operacoesProperty());
        }
    }

    private void unbindData() {
        tboxRegisterCode.textProperty().unbind();
        tboxDescriptionCode.textProperty().unbind();
        tsActive.switchedOnProperty().unbind();
        listOperationsSelected.unbind();
        //listOperationsGroup.targetItemsProperty().unbind();
        clearForm();
    }

    private void bindBidirectionalData(SdGrupoOperacao001 selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bindBidirectional(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().bindBidirectional(selectItem.ativoProperty());

            getSelectedOperations(selectItem);
            prepareListAvailable(selectItem.getCodigo());
            listOperationsSelected.bindBidirectional(selectItem.operacoesProperty());
        }
    }

    private void unbindBidirectionalData(SdGrupoOperacao001 selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().unbindBidirectional(selectItem.codigoProperty());
            tboxDescriptionCode.textProperty().unbindBidirectional(selectItem.descricaoProperty());
            tsActive.switchedOnProperty().unbindBidirectional(selectItem.ativoProperty());
            listOperationsSelected.unbindBidirectional(selectItem.operacoesProperty());
        }
    }

    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdGrupoOperacao001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);

        tabMainPane.getSelectionModel().select(1);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(DAOFactory.getSdGrupoOperacao001DAO().getByDefault(tboxDefaultFilter.textProperty().get()));
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                DAOFactory.getSdGrupoOperacao001DAO().delete(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro Grupo Operações",
                        selectedItem.getCodigo() + "",
                        "Excluído o grupo " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) throws InterruptedException {
        try {
            ValidationForm.validationEmpty(tboxDescriptionCode);
        } catch (FormValidationException ex) {
            return;
        }

        unbindBidirectionalData(selectedItem);
        SdGrupoOperacao001 objectEdited = selectedItem;
        try {
            if (selectedItem.getCodigo() == 0) {
                DAOFactory.getSdGrupoOperacao001DAO().save(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro Grupo Operações",
                        selectedItem.getCodigo() + "",
                        "Cadastrado o grupo " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                if (!listRegisters.contains(selectedItem)) {
                    listRegisters.add(selectedItem);
                }
            } else {
                DAOFactory.getSdGrupoOperacao001DAO().update(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro Grupo Operações",
                        selectedItem.getCodigo() + "",
                        "Alterado o grupo " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
            listOperationsAvailable.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        inEdition.set(false);
        tblRegisters.getSelectionModel().select(objectEdited);
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        listOperationsAvailable.clear();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdGrupoOperacao001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnAddSelectedOnAction(ActionEvent event) {
        ObservableList<SdOperacoesGrp001> listToAdd = listAvailable.getSelectionModel().getSelectedItems();
        listOperationsSelected.addAll(listToAdd);
        listOperationsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnAddAllOnAction(ActionEvent event) {
        ObservableList<SdOperacoesGrp001> listToAdd = listAvailable.getItems();
        listOperationsSelected.addAll(listToAdd);
        listOperationsAvailable.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveSelectedOnAction(ActionEvent event) {
        ObservableList<SdOperacoesGrp001> listToAdd = listSelected.getSelectionModel().getSelectedItems();
        listOperationsAvailable.addAll(listToAdd);
        listOperationsSelected.removeAll(listToAdd);
    }

    @FXML
    private void btnRemoveAllOnAction(ActionEvent event) {
        ObservableList<SdOperacoesGrp001> listToAdd = listSelected.getItems();
        listOperationsAvailable.addAll(listToAdd);
        listOperationsSelected.removeAll(listToAdd);
    }

    private class ListOperationsCell extends ListCell<SdOperacoesGrp001> {

        @Override
        public void updateItem(SdOperacoesGrp001 item, boolean empty) {
            super.updateItem(item, empty);
            this.setText(null);
            this.setGraphic(null);
            if (item != null || !empty) {
                int index = this.getIndex();
                Integer codigo = item.getCodigoOperacao();
                this.setText(item.getOperacao().getDescricao());
            }
        }
    }

}
