/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Log;
import sysdeliz2.models.sysdeliz.SdNivelPolivalencia001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;

import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastroNivelPolivalenciaController implements Initializable {

    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdNivelPolivalencia001> listRegisters = new SimpleListProperty();
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private SdNivelPolivalencia001 selectedItem = null;

    @FXML
    private TabPane tabMainPane;
    @FXML
    private Button btnAddRegister;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private TableView<SdNivelPolivalencia001> tblRegisters;
    @FXML
    private TableColumn<SdNivelPolivalencia001, SdNivelPolivalencia001> clnActions;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxRegisterCode;
    @FXML
    private TextField tboxDescriptionCode;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private TextField tboxBeginGoal;
    @FXML
    private TextField tboxEndGoal;
    @FXML
    private TextField tboxAward;
    @FXML
    private TextField tboxAcronym;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxDescriptionCode);
        TextFieldUtils.upperCase(tboxAcronym);
//        MaskTextField.numericDotField(tboxBeginGoal);
//        MaskTextField.numericDotField(tboxEndGoal);
//        MaskTextField.numericDotField(tboxAward);

        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));

        try {
            listRegisters.set(DAOFactory.getSdNivelPolivalenciaDAO().getAll());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeButtonsActions();
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();
    }

    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdNivelPolivalencia001, SdNivelPolivalencia001>, ObservableValue<SdNivelPolivalencia001>>() {
            @Override
            public ObservableValue<SdNivelPolivalencia001> call(TableColumn.CellDataFeatures<SdNivelPolivalencia001, SdNivelPolivalencia001> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdNivelPolivalencia001>() {
            @Override
            public int compare(SdNivelPolivalencia001 p1, SdNivelPolivalencia001 p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdNivelPolivalencia001, SdNivelPolivalencia001>, TableCell<SdNivelPolivalencia001, SdNivelPolivalencia001>>() {
            @Override
            public TableCell<SdNivelPolivalencia001, SdNivelPolivalencia001> call(TableColumn<SdNivelPolivalencia001, SdNivelPolivalencia001> btnCol) {
                return new TableCell<SdNivelPolivalencia001, SdNivelPolivalencia001>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(SdNivelPolivalencia001 seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    btnDeleteRegisterOnAction(null);
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }

        });

    }

    private void clearForm() {
        tboxRegisterCode.clear();
        tboxDescriptionCode.clear();
        tboxAward.clear();
        tboxBeginGoal.clear();
        tboxEndGoal.clear();
        tboxAcronym.clear();
    }

    private void bindData(SdNivelPolivalencia001 selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bind(selectItem.descricaoProperty());
            tboxBeginGoal.textProperty().bind(selectItem.metaInicioProperty().asString());
            tboxEndGoal.textProperty().bind(selectItem.metaFimProperty().asString());
            tboxAward.textProperty().bind(selectItem.premioProperty().asString());
            tboxAcronym.textProperty().bind(selectItem.siglaProperty());
        }
    }

    private void unbindData() {
        tboxRegisterCode.textProperty().unbind();
        tboxDescriptionCode.textProperty().unbind();
        tboxBeginGoal.textProperty().unbind();
        tboxEndGoal.textProperty().unbind();
        tboxAward.textProperty().unbind();
        tboxAcronym.textProperty().unbind();
        clearForm();
    }

    private void bindBidirectionalData(SdNivelPolivalencia001 selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bindBidirectional(selectItem.descricaoProperty());
//            tboxBeginGoal.textProperty().bindBidirectional(selectItem.metaInicioProperty(), new NumberStringConverter());
//            tboxEndGoal.textProperty().bindBidirectional(selectItem.metaFimProperty(), new NumberStringConverter());
//            tboxAward.textProperty().bindBidirectional(selectItem.premioProperty(), new NumberStringConverter());
            tboxAcronym.textProperty().bindBidirectional(selectItem.siglaProperty());
        }
    }

    private void unbindBidirectionalData(SdNivelPolivalencia001 selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().unbindBidirectional(selectItem.codigoProperty());
            tboxDescriptionCode.textProperty().unbindBidirectional(selectItem.descricaoProperty());
            tboxBeginGoal.textProperty().unbindBidirectional(selectItem.metaInicioProperty());
            tboxEndGoal.textProperty().unbindBidirectional(selectItem.metaFimProperty());
            tboxAward.textProperty().unbindBidirectional(selectItem.premioProperty());
            tboxAcronym.textProperty().unbindBidirectional(selectItem.siglaProperty());
        }
    }

    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdNivelPolivalencia001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);

        tabMainPane.getSelectionModel().select(1);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(DAOFactory.getSdNivelPolivalenciaDAO().getByDefault(tboxDefaultFilter.textProperty().get()));
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                DAOFactory.getSdNivelPolivalenciaDAO().delete(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro de Nível",
                        selectedItem.getCodigo() + "",
                        "Excluído o nível " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) throws InterruptedException {
        try {
            ValidationForm.validationEmpty(tboxDescriptionCode);
            ValidationForm.validationEmpty(tboxAward);
            ValidationForm.validationEmpty(tboxBeginGoal);
            ValidationForm.validationEmpty(tboxEndGoal);
            ValidationForm.validationLenght(tboxAcronym, 3);
        } catch (FormValidationException ex) {
            return;
        }

        unbindBidirectionalData(selectedItem);
        SdNivelPolivalencia001 objectEdited = selectedItem;
        try {
            if (selectedItem.getCodigo() == 0) {
                DAOFactory.getSdNivelPolivalenciaDAO().save(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro de Nível",
                        selectedItem.getCodigo() + "",
                        "Cadastrado o nível " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                if (!listRegisters.contains(selectedItem)) {
                    listRegisters.add(selectedItem);
                }
            } else {
                DAOFactory.getSdNivelPolivalenciaDAO().update(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro de Nível",
                        selectedItem.getCodigo() + "",
                        "Alterado o nível " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        inEdition.set(false);
        tblRegisters.getSelectionModel().select(objectEdited);
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdNivelPolivalencia001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        tboxDescriptionCode.requestFocus();
    }

}
