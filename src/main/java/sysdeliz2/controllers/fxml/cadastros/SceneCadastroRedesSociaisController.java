package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.cadastros.base.SceneCadastroBase;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.Log;
import sysdeliz2.models.sysdeliz.SdRedesSociais;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author lima.joao
 * @since 21/08/2019 17:37
 */
public class SceneCadastroRedesSociaisController extends SceneCadastroBase<SdRedesSociais> implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private TableColumn<SdRedesSociais, String> clnNome;
    @FXML
    private TableColumn<SdRedesSociais, String> clnCodigo;
    @FXML
    private TableColumn<SdRedesSociais, String> clnUrl;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxNome;
    @FXML
    private TextField tboxUrl;
    //</editor-fold>

    @Override
    protected void bindDados() {
        if (selectedItem != null) {
            tboxCodigo.textProperty().bind(selectedItem.codigoProperty().asString());
            if( !isBindBiDirectional ){
                tboxNome.textProperty().bind(selectedItem.nomeProperty());
                tboxUrl.textProperty().bind(selectedItem.urlProperty());
            } else {
                tboxNome.textProperty().bindBidirectional(selectedItem.nomeProperty());
                tboxUrl.textProperty().bindBidirectional(selectedItem.urlProperty());
            }
        }
    }

    @Override
    protected void unbindDados(){
        tboxCodigo.textProperty().unbind();
        if( !isBindBiDirectional ){
            tboxNome.textProperty().unbind();
            tboxUrl.textProperty().unbind();
        } else {
            tboxNome.textProperty().unbindBidirectional(selectedItem.nomeProperty());
            tboxUrl.textProperty().unbindBidirectional(selectedItem.urlProperty());
        }
        isBindBiDirectional = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //
        this.titulo = "Cadastro de Redes Sociais";
        this.genericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRedesSociais.class);

        // Carrega a lista de representantes
        try {
            lstRegistros = this.genericDao.list();
        } catch (SQLException e) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, e);
            GUIUtils.showException(e);
        }

        tblRegisters.setItems(lstRegistros);

        // Mantém o campo pesquisa sempre em caixa alta.
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.lowerCase(tboxUrl);

        // Binda para sempre que alterar a quantidade de registros corrigir a descricao.
        lbRegistersCount.textProperty()
                .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));

        // Controle dos botões
        //<editor-fold desc="Botões">
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(lstRegistros.size() - 1));
        //</editor-fold>

        makeButtonsActions(true, true, true);

        //<editor-fold desc="Colunas tela principal">
        // Configura as colunas
        clnCodigo.setCellValueFactory(param -> param.getValue().codigoProperty().asString());
        clnNome.setCellValueFactory(param -> param.getValue().nomeProperty());
        clnUrl.setCellValueFactory(param -> param.getValue().urlProperty());
        //</editor-fold>

        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindDados();
            selectedItem = newValue;
            bindDados();

            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });

        // Campos sempre inativos
        tboxCodigo.setEditable(false);

        // Campos inativados dinamicamente
        tboxNome.disableProperty().bind(inEdition.not());
        tboxUrl.disableProperty().bind(inEdition.not());
    }

    @FXML
    @Override
    public void btnAddRegisterOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            unbindDados();
            selectedItem = new SdRedesSociais();
            isBindBiDirectional = true;
            inEdition.set(true);
            bindDados();

            tabMainPane.getSelectionModel().select(1);
            tboxNome.requestFocus();

            actionEvent.consume();
        }
    }

    @FXML
    @Override
    public void btnFindDefaultOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            try {
                this.genericDao
                        .initCriteria()
                        .addPredicateLike("nome", "%" + tboxDefaultFilter.textProperty().get() + "%");

                lstRegistros.setAll(this.genericDao.loadListByPredicate());

                lbRegistersCount.textProperty()
                        .bind(new SimpleStringProperty("Mostrando ")
                                .concat(lstRegistros.size())
                                .concat(" registros"));

                tboxDefaultFilter.clear();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
            actionEvent.consume();
        }
    }

    @FXML
    @Override
    protected void btnEditRegisterOnAction(ActionEvent actionEvent) {
        if (actionEvent == null || !actionEvent.isConsumed()) {
            unbindDados();
            inEdition.set(true);
            isBindBiDirectional = true;
            bindDados();

            if (actionEvent != null) {
                actionEvent.consume();
            }
        }
    }

    @FXML
    @Override
    public void btnDeleteRegisterOnAction(ActionEvent actionEvent) {
        if (actionEvent == null || !actionEvent.isConsumed()) {
            if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
                try {
                    this.genericDao.delete(selectedItem.getCodigo());
                    lstRegistros.remove(selectedItem);
                    tblRegisters.getSelectionModel().selectFirst();

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.EXCLUIR,
                            this.titulo,
                            selectedItem.getCodigo() + "",
                            "Removido a rede social " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                    ));
                } catch (SQLException ex) {
                    Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
            if (actionEvent != null) {
                actionEvent.consume();
            }
        }
    }

    @FXML
    public void btnSaveOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {

            try {
                if (selectedItem.getCodigo() == 0) {
                    // Atualiza/Grava na tabela Represen_001
                    selectedItem = this.genericDao.save(selectedItem);

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.CADASTRAR,
                            "Cadastro de Redes Sociais",
                            selectedItem.getCodigo() + "",
                            "Cadastrado o Marca concorrente " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                    ));

                    if (!lstRegistros.contains(selectedItem)) {
                        lstRegistros.add(selectedItem);
                    }

                } else {
                    selectedItem = this.genericDao.update(selectedItem);

                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                            SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.EDITAR,
                            "Cadastro de Marcas concorrentes",
                            selectedItem.getCodigo() + "",
                            "Alterado a marca concorrente " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
                    ));
                }

                unbindDados();

                lbRegistersCount.textProperty()
                        .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }

            inEdition.set(false);
            tblRegisters.getSelectionModel().select(selectedItem);
            actionEvent.consume();
        }
    }

}
