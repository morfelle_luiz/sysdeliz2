package sysdeliz2.controllers.fxml.cadastros;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.shapes.Polygon;
import com.lynden.gmapsfx.shapes.PolygonOptions;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterEstadosController;
import sysdeliz2.controllers.fxml.procura.FilterSdCidadeController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.Log;
import sysdeliz2.models.properties.LatLng;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.models.sysdeliz.SdRegiao;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.validator.SimpleMessage;
import sysdeliz2.utils.validator.Validator;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author lima.joao
 * @since 12/07/2019 16:56
 */
public class SceneCadastroRegiaoController implements Initializable {
    @FXML
    private Tab tabFormulario;
    @FXML
    private GoogleMapView gmapView;
    @FXML
    private Button btnAddCidades;
    @FXML
    private Button btnDeleteTodasCidades;
    @FXML
    private Button btnDeleteCidadeSelecionada;
    @FXML
    private Button btnAddUF;
    @FXML
    private Button btnDeleteUF;
    @FXML
    private TableColumn<TabUf, String> clnUfSigla;
    @FXML
    private TableColumn<TabUf, String> clnUfCodigo;
    @FXML
    private javafx.scene.control.TableView<TabUf> tblEstados;
    @FXML
    private javafx.scene.control.TableView<SdCidade> tblCidades;
    @FXML
    private javafx.scene.control.Button btnReturn;
    @FXML
    private javafx.scene.control.TextArea tboxObservacao;
    @FXML
    @SuppressWarnings("unused")
    private javafx.scene.control.Button btnFindDefault;
    @FXML
    private javafx.scene.control.TextField tboxCodigoTI;
    @FXML
    private javafx.scene.control.Button btnAddRegister2;
    @FXML
    private javafx.scene.control.Button btnSave;
    @FXML
    private javafx.scene.control.CheckBox cboxSituacao;
    @FXML
    private javafx.scene.control.TextField tboxCodigo;
    @FXML
    private javafx.scene.control.Button btnFirstRegister;
    @FXML
    private javafx.scene.control.Button btnLastRegister;
    @FXML
    private javafx.scene.control.Button btnEditRegister;
    @FXML
    private javafx.scene.control.Button btnPreviusRegister;
    @FXML
    private javafx.scene.control.Button btnCancel;
    @FXML
    private javafx.scene.control.TextField tboxDefaultFilter;
    @FXML
    private javafx.scene.control.TextField tboxDescription;
    @FXML
    private javafx.scene.control.Button btnNextRegister;
    @FXML
    private javafx.scene.control.TableColumn<SdRegiao, SdRegiao> clnActions;
    @FXML
    private javafx.scene.control.Button btnDeleteRegister;
    @FXML
    private javafx.scene.control.Label lbRegistersCount;
    @FXML
    private javafx.scene.control.TabPane tabMainPane;
    @FXML
    private javafx.scene.control.TableView<SdRegiao> tblRegisters;
    @FXML
    @SuppressWarnings("unused")
    private javafx.scene.control.Button btnAddRegister;

    private static GenericDao<SdRegiao> dao;
    private ObservableList<SdRegiao> listSdRegioes001;
    private ObservableList<SdCidade> listSdCidades;
    private SdRegiao selectedItem = null;

    private BooleanProperty cBoxSelected = new SimpleBooleanProperty(false);

    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);

    private boolean isBindBiDirectional = false;

    private GoogleMap map;
    private List<Polygon> polygonList;

    private String[] colors = new String[]{
            "#A37676", "#7D3E3E", "#7D5A3E", "#BA6523",
            "#B5AB4E", "#91AB6C", "#5E753E", "#6CA123",
            "#396100", "#3E7550", "#2E6E5D", "#6BC9B0",
            "#6BB7C9"
    };

    private void buildPoligonList(){

        if (!tabFormulario.isSelected()){
            return;
        }

        // Marker por cidade
        if (polygonList == null) {
            polygonList = new ArrayList<>();
        }

        if (map != null) {
            // Se existir estados
            if (tblEstados.getItems().size() > 0) {

                for (Polygon polygon : polygonList) {
                    map.removeMapShape(polygon);
                }
                polygonList.clear();
                map.clearMarkers();
                // lista de poligonos
                int count = 0;

                for (TabUf uf : tblEstados.getItems()) {
                    if(!uf.getPolygon().equals("")){

                        List<LatLng> pnts = new Gson().fromJson(uf.getPolygon(), new TypeToken<List<LatLng>>(){}.getType());

                        List<LatLong> points = new ArrayList<>();

                        for (LatLng pnt : pnts) {
                            points.add(new LatLong(pnt.getLatitude(), pnt.getLongitude()));
                        }

                        PolygonOptions options = new PolygonOptions()
                                .paths(new MVCArray( points.toArray() ))
                                //.strokeColor()
                                .strokeWeight(1)
                                .editable(false)
                                .draggable(false)
                                .fillColor(colors[count])
                                .fillOpacity(0.5);

                        Polygon polygon = new Polygon(options);

                        map.addMapShape(polygon);
                        polygonList.add(polygon);
                    }
                    count++;
                }
            }
        }
    }

    private void makeButtonsActions(){

        clnActions.setCellValueFactory(features ->
                new ReadOnlyObjectWrapper<>(features.getValue())
        );

//        clnActions.setComparator(new Comparator<SdRegiao001>() {
//            @Override
//            public int compare(SdRegiao001 p1, SdRegiao001 p2) {
//                return compare(p1, p2);
//            }
//        });

        clnActions.setCellFactory(new Callback<TableColumn<SdRegiao, SdRegiao>, TableCell<SdRegiao, SdRegiao>>() {
            @Override
            public TableCell<SdRegiao, SdRegiao> call(TableColumn<SdRegiao, SdRegiao> btnCol) {
                return new TableCell<SdRegiao, SdRegiao>(){
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));

                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    protected void updateItem(SdRegiao seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);

                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                isBindBiDirectional = false;
                                bindData();
                                tabMainPane.getSelectionModel().select(1);
                            });

                            btnEditRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                tabMainPane.getSelectionModel().select(1);
                                btnEditRegisterOnAction(null);
                            });
                            btnDeleteRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                btnDeleteRegisterOnAction(null);
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });
    }

    private void bindData(){
        if(selectedItem != null){
            tboxCodigo.textProperty().bind(selectedItem.codigoProperty().asString());

            cBoxSelected.setValue(selectedItem.getStatus().equals("A"));

            if(!isBindBiDirectional) {
                tboxDescription.textProperty().bind(selectedItem.descricaoProperty());
                tboxCodigoTI.textProperty().bind(selectedItem.siglaTiProperty());
                tboxObservacao.textProperty().bind(selectedItem.obsProperty());

                cboxSituacao.selectedProperty().bind(cBoxSelected);
            } else {
                tboxDescription.textProperty().bindBidirectional(selectedItem.descricaoProperty());
                tboxCodigoTI.textProperty().bindBidirectional(selectedItem.siglaTiProperty());
                tboxObservacao.textProperty().bindBidirectional(selectedItem.obsProperty());

                cboxSituacao.selectedProperty().bindBidirectional(cBoxSelected);
            }

            // Binda a lista de estados
            tblEstados.setItems(selectedItem.getUfObservableList());
            tblEstados.refresh();
            tblEstados.getSelectionModel().selectFirst();

            if(tblEstados.getItems().size() > 0){
                listSdCidades = selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst());
            } else {
                listSdCidades = FXCollections.observableArrayList();
            }
            tblCidades.setItems(listSdCidades);
            tblCidades.refresh();

            buildPoligonList();
        }
    }

    private void unBindData(){
        tboxCodigo.textProperty().unbind();

        cBoxSelected.setValue(false);

        if(!isBindBiDirectional) {
            tboxDescription.textProperty().unbind();
            tboxCodigoTI.textProperty().unbind();
            tboxObservacao.textProperty().unbind();
            cboxSituacao.selectedProperty().unbind();
        } else {
            tboxDescription.textProperty().unbindBidirectional(selectedItem.descricaoProperty());
            tboxCodigoTI.textProperty().unbindBidirectional(selectedItem.siglaTiProperty());
            tboxObservacao.textProperty().unbindBidirectional(selectedItem.obsProperty());
            cboxSituacao.selectedProperty().unbindBidirectional(cBoxSelected);
        }
        clearForm();

        if(map != null){
            map.clearMarkers();
        }
    }

    private void clearForm() {
        tboxCodigo.clear();
        tboxDescription.clear();
        tboxCodigoTI.clear();
        tboxObservacao.clear();
        cboxSituacao.setSelected(false);

        isBindBiDirectional = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gmapView.addMapInializedListener(this::mapInitialized);
        gmapView.setKey("AIzaSyD7Y7YZfcIlE8zhl99OM0G7otWFVWM62nc");

        // Inicializa o dao generico
        dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRegiao.class);

        // Mantém o campo pesquisa sempre em caixa alta.
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxDescription);
        TextFieldUtils.upperCase(tboxCodigoTI);
        TextFieldUtils.upperCase(tboxObservacao);

        try {
            // Carrega os dados para
            // Carrega todas regioes
            this.listSdRegioes001 = dao.list();

            // Insere os registros
            tblRegisters.setItems(listSdRegioes001);

            // Binda para sempre que alterar a quantidade de registros corrigir a descricao.
            lbRegistersCount.textProperty().bind(
                    new SimpleStringProperty("Mostrando ")
                            .concat(listSdRegioes001.size())
                            .concat(" registros"));

            // Controle dos botões
            btnCancel.disableProperty().bind(disableBtnCancelRegister);
            btnSave.disableProperty().bind(disableBtnSaveRegister);
            btnEditRegister.disableProperty().bind(disableBtnEditRegister);
            btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

            btnAddUF.disableProperty().bind(disableBtnSaveRegister);
            btnDeleteUF.disableProperty().bind(disableBtnSaveRegister);
            
            btnAddCidades.disableProperty().bind(disableBtnSaveRegister);
            btnDeleteCidadeSelecionada.disableProperty().bind(disableBtnSaveRegister);
            btnDeleteTodasCidades.disableProperty().bind(disableBtnSaveRegister);

            btnReturn.disableProperty().bind(inEdition);
            tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
            btnAddRegister2.disableProperty().bind(inEdition);

            btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
            btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
            btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
            btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

            disableBtnSaveRegister.bind(inEdition.not());
            disableBtnCancelRegister.bind(inEdition.not());
            disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
            disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

            disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
            disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
            disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listSdRegioes001.size() -1 ));

            tabFormulario.setOnSelectionChanged(event -> {
                tabFormulario.setDisable(true);
                buildPoligonList();
                tabFormulario.setDisable(false);
            });


        } catch (Exception e) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, e);
            GUIUtils.showException(e);
        }

        makeButtonsActions();

        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unBindData();
            selectedItem = newValue;
            bindData();
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });

        tblRegisters.getSelectionModel().selectFirst();

        clnUfCodigo.setCellValueFactory(param -> param.getValue().getId().codigoProperty());
        clnUfSigla.setCellValueFactory(param -> param.getValue().getId().siglaEstProperty());

        tblEstados.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if((selectedItem != null) && (newValue != null)){
                listSdCidades = selectedItem.getCidadeObservableList(newValue.getId().getSiglaEst());
                tblCidades.setItems(listSdCidades);
                tblCidades.refresh();
            }
        });

        tblCidades.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(map != null){
                if( newValue != null){
                    map.clearMarkers();

                    //
                    String mSql = "SELECT COUNT(*)" +
                            " FROM ENTIDADE_001 ent " +
                            " LEFT JOIN CADCEP_001 cep ON ent.cep = CEP.CEP" +
                            " WHERE " +
                            "    ENT.TIPO_ENTIDADE IN ('C', 'CF') AND" +
                            "    CEP.COD_CID = " + newValue.getCodigo();

                    BigDecimal qtd = (BigDecimal) JPAUtils
                            .getEntityManager()
                            .createNativeQuery(mSql).getResultList().get(0);

                    MarkerOptions options = new MarkerOptions()
                            .position(new LatLong(newValue.getLatitude().doubleValue(), newValue.getLongitude().doubleValue()))
                            .title(newValue.getNome())
                            .visible(true)
                            .label(qtd.toString())
                            .animation(Animation.DROP);


                    Marker marker = new Marker(options);
                    map.addMarker(marker);
                }
            }
        });

    }

    @FXML
    @SuppressWarnings("unused")
    private void btnAddRegisterOnAction(ActionEvent event) {
        unBindData();
        selectedItem = new SdRegiao();
        isBindBiDirectional = true;
        bindData();
        inEdition.set(true);

        tabMainPane.getSelectionModel().select(1);
        tboxDescription.requestFocus();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnFindDefaultOnAction(ActionEvent event) {
        try{
            dao.initCriteria();
            dao.addPredicateLike("descricao", "%" + tboxDefaultFilter.textProperty().get() + "%");
            listSdRegioes001.setAll(dao.loadListByPredicate());
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnPreviusRegisterOnAction(ActionEvent event){
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnNextRegisterOnAction(ActionEvent event){

        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnLastRegisterOnAtion(ActionEvent event){
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnAddRegister2OnAction(ActionEvent event){
        btnAddRegisterOnAction(event);
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnEditRegisterOnAction(ActionEvent event){
        unBindData();
        inEdition.set(true);
        isBindBiDirectional = true;
        bindData();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnDeleteRegisterOnAction(ActionEvent event){
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try{
                dao.delete(selectedItem.getCodigo());
                listSdRegioes001.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();

                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro de Regioes",
                        selectedItem.getCodigo() + "",
                        "Excluído a Região " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnSaveOnAction(ActionEvent event){
        Validator validator = new Validator();
        validator
                .addIf(selectedItem.getDescricao().trim().equals(""), new SimpleMessage("Erro", "Descrição precisa ser preenchida") )
                .addIf(selectedItem.getSiglaTi().trim().equals(""), new SimpleMessage("Erro", "Código do sistema TI precisa ser informado"));

        if (validator.hasError()){
            validator.showAlertErrors();
            return;
        }

        try {
            if(selectedItem.getCodigo() == 0) {
                selectedItem = dao.save(selectedItem);
                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                        SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro de Região",
                        selectedItem.getCodigo() + "",
                        "Cadastrado a região " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                       ));
                if(!listSdRegioes001.contains(selectedItem)){
                    listSdRegioes001.add(selectedItem);
                }
            } else {
                selectedItem = dao.update(selectedItem);
                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
                        SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro de Região",
                        selectedItem.getCodigo() + "",
                        "Alterado a região " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        inEdition.set(false);
        tblRegisters.getSelectionModel().select(selectedItem);
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnCancelOnAction(ActionEvent event){
        unBindData();
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnReturnOnAction(ActionEvent event){
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    @SuppressWarnings({"unused", "unchecked"})
    private void btnAddNovoEstado(ActionEvent event){
        try {
            FilterEstadosController filter = new FilterEstadosController(Modals.FXMLWindow.FilterEstados);
            if (filter.tabUf != null) {
                TabUf tabUf = filter.tabUf;
                selectedItem.addUF(tabUf);

                tblEstados.setItems(selectedItem.getUfObservableList());
                tblEstados.refresh();

                GenericDao<SdCidade> cidadesDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade.class);

                ObservableList list = cidadesDao
                        .initCriteria()
                        .addPredicateEq("siglaUf", tabUf.getId().getSiglaEst())
                        .loadListByPredicate();

                selectedItem.addCidades(list);

                tblCidades.setItems(selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst()));
            }
        } catch (IOException | SQLException e) {
            Logger.getLogger(SceneCadastroRegiaoController.class.getName()).log(Level.SEVERE, null, e);
            GUIUtils.showException(e);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnDeleteUFOnAction(ActionEvent event){
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente remover o estado selecionado desta região? Esta operação irá remover as cidades também")) {
            try{
                // pega as cidades para remover
                listSdCidades = selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst());
                listSdCidades.forEach(cidade -> selectedItem.getCidadeList().remove(cidade));

                tblCidades.setItems(selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst()));
                tblCidades.getSelectionModel().selectFirst();

                TabUf uf = tblEstados.getSelectionModel().getSelectedItem();
                selectedItem.getUfList().remove(uf);
                tblEstados.setItems(selectedItem.getUfObservableList());
                tblEstados.refresh();
                tblEstados.getSelectionModel().selectFirst();

                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro de Regioes",
                        selectedItem.getDescricao() + "",
                        "Removido estado " + uf.getDescricao() + " da região [" + selectedItem.getDescricao() + "]"
                ));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnAddNovaCidade(ActionEvent event){
        try {
            FilterSdCidadeController filter = new FilterSdCidadeController(Modals.FXMLWindow.FilterSdCidade, tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst());

            if (filter.returnValue != null && filter.returnValue.size() > 0) {
                selectedItem.addCidades(filter.returnValue);

                tblCidades.setItems(selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst()));
                tblEstados.refresh();
            }
        } catch (IOException e) {
            Logger.getLogger(SceneCadastroRegiaoController.class.getName()).log(Level.SEVERE, null, e);
            GUIUtils.showException(e);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnRemoveCidadeSelecionadaOnAction(ActionEvent event){
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente remover a cidade selecionada desta região?")) {
            try{
                SdCidade cidade = tblCidades.getSelectionModel().getSelectedItem();

                selectedItem.getCidadeList().remove(cidade);

                tblCidades.setItems(selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst()));
                tblCidades.refresh();
                tblCidades.getSelectionModel().selectFirst();

                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro de Regioes",
                        selectedItem.getDescricao() + "",
                        "Removido cidade " + cidade.getNome() + " da região [" + selectedItem.getDescricao() + "]"
                ));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnRemoveTodasCidadesOnAction(ActionEvent event){
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente remover TODAS as cidades do estado " + this.tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst() + " nesta região?")) {
            try{
                selectedItem.getCidadeList().clear();

                tblCidades.setItems(selectedItem.getCidadeObservableList(tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst()));
                tblCidades.refresh();
                tblCidades.getSelectionModel().selectFirst();

                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro de Regioes",
                        selectedItem.getDescricao() + "",
                        "Removido todas cidades do estado " + tblEstados.getSelectionModel().getSelectedItem().getId().getSiglaEst() + " da região [" + selectedItem.getDescricao() + "]"
                ));
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    private void mapInitialized() {
        MapOptions options = new MapOptions();

        options.center(new LatLong(-15.783995235520678, -47.353895312500015))
                .zoomControl(true)
                .zoom(4)
                .overviewMapControl(false)
                .mapType(MapTypeIdEnum.ROADMAP);

        map = gmapView.createMap(options, false);

        buildPoligonList();
    }

}