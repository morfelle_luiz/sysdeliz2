//package sysdeliz2.controllers.fxml.cadastros;
//
//import javafx.beans.binding.Bindings;
//import javafx.beans.property.*;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.*;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyCodeCombination;
//import javafx.scene.input.KeyCombination;
//import javafx.scene.layout.HBox;
//import javafx.util.StringConverter;
//import org.controlsfx.control.ToggleSwitch;
//import sysdeliz2.controllers.fxml.SceneMainController;
//import sysdeliz2.controllers.fxml.procura.FilterSdCidadeController;
//import sysdeliz2.controllers.fxml.procura.FilterSdCidadeSingleResultController;
//import sysdeliz2.controllers.fxml.procura.FilterSdRegiaoController;
//import sysdeliz2.daos.DAOFactory;
//import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
//import sysdeliz2.daos.generics.interfaces.GenericDao;
//import sysdeliz2.models.Log;
//import sysdeliz2.models.sysdeliz.SdCidade;
//import sysdeliz2.models.sysdeliz.SdRepresentante;
//import sysdeliz2.models.sysdeliz.SdRepresentanteColecao;
//import sysdeliz2.models.sysdeliz.SdRepresentanteColecaoMarca;
//import sysdeliz2.models.ti.*;
//import sysdeliz2.utils.*;
//import sysdeliz2.utils.apis.viacep.ServicoViaCEP;
//import sysdeliz2.utils.apis.viacep.models.CEP;
//import sysdeliz2.utils.gui.MaskTextField;
//import sysdeliz2.utils.gui.TextFieldUtils;
//import sysdeliz2.utils.apis.receitaws.ServicoReceitaWS;
//import sysdeliz2.utils.apis.receitaws.models.ReceitaWS;
//import sysdeliz2.utils.hibernate.JPAUtils;
//import sysdeliz2.utils.sys.LocalLogger;
//import sysdeliz2.utils.sys.StringUtils;
//
//import javax.validation.ConstraintViolation;
//import javax.validation.Validation;
//import javax.validation.Validator;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.net.URL;
//import java.sql.SQLException;
//import java.util.*;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// * @author lima.joao
// * @since 01/08/2019 10:35
// */
//public class SceneCadastroRepresentanteController implements Initializable {
//
//    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
//    @FXML
//    public TableView<SdRepresentanteColecaoMarca> tblMarcasAntedidas;
//    @FXML
//    public TableColumn<SdRepresentanteColecaoMarca, SdRepresentanteColecaoMarca> clnActionsMarcasAtendidas;
//    @FXML
//    public TableColumn<SdRepresentanteColecaoMarca, String> clnMarca;
//    @FXML
//    public TableView<SdCidade> tblCidadesAntedidas;
//    @FXML
//    public TableColumn<SdCidade, SdCidade> clnActionsCidadesAtendidas;
//    @FXML
//    public TableColumn<SdCidade, String> clnEstadoSigla;
//    @FXML
//    public TableColumn<SdCidade, String> clnCidadeNome;
//    @FXML
//    public TableColumn<SdCidade, String> clnCidadeCodigo;
//    @FXML
//    public Button btnAddColecao;
//    @FXML
//    public Button btnAddMarca;
//    @FXML
//    public Button btnAddCidades;
//    @FXML
//    private TableView<SdRepresentanteColecao> tblColecoesAntedidas;
//    @FXML
//    private ChoiceBox<Marca> cBoxMarca;
//    @FXML
//    private ChoiceBox<Colecao> cBoxColecao;
//    @FXML
//    private TableColumn<SdRepresentanteColecao, SdRepresentanteColecao> clnActionsColecoesAtendidas;
//    @FXML
//    private TableColumn<SdRepresentanteColecao, String> clnDescricaoColecao;
//    @FXML
//    private TableColumn<SdRepresentanteColecao, String> clnCodigoColecao;
//    @FXML
//    private TableColumn<SdRepresentante, SdRepresentante> clnActions;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnCodigo;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnNome;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnTelefone;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnCelular;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnResponsavel;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnCep;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnCidade;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnEndereco;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnDocumento;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnComissaoFat;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnRegiao;
//    @FXML
//    private TableColumn<SdRepresentante, ImageView> clnAtivo;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnEmail;
//    @FXML
//    private TableColumn<SdRepresentante, String> clnImpostoRenda;
//    @FXML
//    private ComboBox<String> cmbAtivo;
//    @FXML
//    private TextField tboxDocumento;
//    @FXML
//    private Button btnConsultaReceitaWS;
//    @FXML
//    private TextField tboxNome;
//    @FXML
//    private ChoiceBox<String> cBoxTipo;
//    @FXML
//    private TextField tboxInscricaoEstadual;
//    @FXML
//    private Button btnConsultaCep;
//    @FXML
//    private TextField tboxCep;
//    @FXML
//    private TextField tboxEndereco;
//    @FXML
//    private TextField tboxBairro;
//    @FXML
//    private TextField tboxResponsavel;
//    @FXML
//    private TextField tboxTelefone;
//    @FXML
//    private TextField tboxCelular;
//    @FXML
//    private TextField tboxFax;
//    @FXML
//    private TextField tboxEmail;
//    @FXML
//    private TextField tboxImpostoRenda;
//    @FXML
//    private TextField tboxComissaoFat;
//    @FXML
//    private TextField tboxComissaoRec;
//    @FXML
//    private TextField tboxComissaoFat2;
//    @FXML
//    private TextField tboxComissaoRec2;
//    @FXML
//    private TextField tboxCidade;
//    @FXML
//    private Button btnConsultaCidade;
//    @FXML
//    private ToggleSwitch cboxSituacao;
//    @FXML
//    private Label lblSituacao;
//    @FXML
//    private TextField tboxRegiao;
//    @FXML
//    private Button btnConsultaRegiao;
//    @FXML
//    private TabPane tabMainPane;
//    @FXML
//    private TextField tboxDefaultFilter;
//    @FXML
//    private TableView<SdRepresentante> tblRegisters;
//    @FXML
//    private Label lbRegistersCount;
//    @FXML
//    private Button btnFirstRegister;
//    @FXML
//    private Button btnPreviusRegister;
//    @FXML
//    private Button btnNextRegister;
//    @FXML
//    private Button btnLastRegister;
//    @FXML
//    private Button btnAddRegister2;
//    @FXML
//    private Button btnEditRegister;
//    @FXML
//    private Button btnDeleteRegister;
//    @FXML
//    private Button btnSave;
//    @FXML
//    private Button btnCancel;
//    @FXML
//    private Button btnReturn;
//    @FXML
//    private TextField tboxCodigo;
//    @FXML
//    private TextArea tboxObservacao;
//    //</editor-fold>
//
//    //<editor-fold defaultstate="collapsed" desc="Variaveis Locais">
//    private GenericDao<SdRepresentante>        sdRepresentanteDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRepresentante.class);
//    private GenericDao<SdRepresentanteColecao> sdRepresentanteColecao001Dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRepresentanteColecao.class);
//    private GenericDao<SdRepresentanteColecaoMarca> sdRepresentanteColecaoMarca001Dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(),
//            SdRepresentanteColecaoMarca.class);
//    private ObservableList<SdRepresentante> lstRegistros;
//    private SdRepresentante selectedItem;
//
//    private ObservableList<SdRepresentanteColecao> lstRepresentanteColecoes;
//    private SdRepresentanteColecao colecaoSelecionada;
//
//    private ObservableList<SdRepresentanteColecaoMarca> lstRepresentanteColecaoMarcas;
//    private SdRepresentanteColecaoMarca marcaSelecionada;
//
//    private ObservableList<SdCidade> lstRepresentanteColecaoMarcasCidades;
//    private SdCidade cidadeSelecionada;
//
//
//    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
//    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
//    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
//    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
//    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
//    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
//    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
//    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
//    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
//    //</editor-fold>
//
//    private void makeButtonsActions() {
//        clnActions.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
//        clnActions.setCellFactory(param -> new TableCell<SdRepresentante, SdRepresentante>() {
//            final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
//            final Button btnEditRow = StaticButtonBuilder.genericEditButton();
//            final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();
//
//            final HBox boxButtonsRow = new HBox(3);
//
//            @Override
//            protected void updateItem(SdRepresentante seletedRow, boolean empty) {
//                super.updateItem(seletedRow, empty);
//
//                if (seletedRow != null) {
//                    boxButtonsRow.getChildren().clear();
//                    boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
//                    setGraphic(boxButtonsRow);
//
//                    btnInfoRow.setOnAction(event -> {
//                        selectedItem = seletedRow;
//                        populateForm();
//                        tabMainPane.getSelectionModel().select(1);
//                    });
//
//                    btnEditRow.setOnAction(event -> {
//                        selectedItem = seletedRow;
//                        tabMainPane.getSelectionModel().select(1);
//                        btnEditRegisterOnAction(null);
//                    });
//
//                    btnDeleteRow.setOnAction(event -> {
//                        selectedItem = seletedRow;
//                        btnDeleteRegisterOnAction(null);
//                    });
//                } else {
//                    setText(null);
//                    setGraphic(null);
//                }
//            }
//
//        });
//
//        clnActionsColecoesAtendidas.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
//        clnActionsColecoesAtendidas.setCellFactory(param -> new TableCell<SdRepresentanteColecao, SdRepresentanteColecao>() {
//            final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();
//            final HBox boxButtonsRow = new HBox(3);
//
//            @Override
//            protected void updateItem(SdRepresentanteColecao seletedRow, boolean empty) {
//                super.updateItem(seletedRow, empty);
//
//                if (seletedRow != null) {
//                    boxButtonsRow.getChildren().clear();
//                    boxButtonsRow.getChildren().addAll(btnDeleteRow);
//                    setGraphic(boxButtonsRow);
//
//                    btnDeleteRow.setOnAction(event -> {
//                        colecaoSelecionada = seletedRow;
//                        deleteRepresentanteColecao();
//                    });
//                } else {
//                    setText(null);
//                    setGraphic(null);
//                }
//            }
//        });
//
//        clnActionsMarcasAtendidas.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
//        clnActionsMarcasAtendidas.setCellFactory(param -> new TableCell<SdRepresentanteColecaoMarca, SdRepresentanteColecaoMarca>() {
//            final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();
//            final HBox boxButtonsRow = new HBox(3);
//
//            @Override
//            protected void updateItem(SdRepresentanteColecaoMarca seletedRow, boolean empty) {
//                super.updateItem(seletedRow, empty);
//
//                if (seletedRow != null) {
//                    boxButtonsRow.getChildren().clear();
//                    boxButtonsRow.getChildren().addAll(btnDeleteRow);
//                    setGraphic(boxButtonsRow);
//
//                    btnDeleteRow.setOnAction(event -> {
//                        marcaSelecionada = seletedRow;
//                        deleteRepresentanteColecaoMarca();
//                    });
//                } else {
//                    setText(null);
//                    setGraphic(null);
//                }
//            }
//        });
//
//        clnActionsCidadesAtendidas.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
//        clnActionsCidadesAtendidas.setCellFactory(param -> new TableCell<SdCidade, SdCidade>() {
//            final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();
//            final HBox boxButtonsRow = new HBox(3);
//
//            @Override
//            protected void updateItem(SdCidade seletedRow, boolean empty) {
//                super.updateItem(seletedRow, empty);
//
//                if (seletedRow != null) {
//                    boxButtonsRow.getChildren().clear();
//                    boxButtonsRow.getChildren().addAll(btnDeleteRow);
//                    setGraphic(boxButtonsRow);
//
//                    btnDeleteRow.setOnAction(event -> {
//                        cidadeSelecionada = seletedRow;
//                        deleteRepresentanteColecaoMarcaCidade();
//                    });
//                } else {
//                    setText(null);
//                    setGraphic(null);
//                }
//            }
//        });
//    }
//
//    private void populateForm() {
//
//        if (selectedItem != null) {
//            tboxCodigo.setText(selectedItem.getCodigo().toString());
//
//            if (selectedItem.getCidade() != null) {
//                tboxCidade.setText(selectedItem.getCidade().getNome());
//            }
//
//            cBoxTipo.getSelectionModel().select((selectedItem.getTipo() == null || selectedItem.getTipo().equals("1")) ? 0 : 1);
//
//            if (selectedItem.getRegiao() != null) {
//                tboxRegiao.setText(selectedItem.getRegiao().getDescricao());
//            }
//
//            // Define se esta ativo ou inativo
//            lblSituacao.textProperty().bind(Bindings.when(cboxSituacao.selectedProperty()).then("ATIVO").otherwise("INATIVO"));
//            if (selectedItem.getAtivo().equals("S")) {
//                cboxSituacao.setSelected(true);
//            } else {
//                cboxSituacao.setSelected(false);
//            }
//
//            tboxDocumento.setText(StringUtils.formatCpfCnpj(selectedItem.getDocumento()));
//            tboxNome.setText(selectedItem.getNome());
//            tboxInscricaoEstadual.setText(selectedItem.getInscricao());
//            tboxCep.setText(StringUtils.formatCep(selectedItem.getCep()));
//            tboxEndereco.setText(selectedItem.getEndereco());
//            tboxBairro.setText(selectedItem.getBairro());
//            tboxResponsavel.setText(selectedItem.getResponsavel());
//            tboxTelefone.setText(StringUtils.formatFone(selectedItem.getTelefone()));
//            tboxCelular.setText(StringUtils.formatFone(selectedItem.getCelular()));
//            tboxFax.setText(StringUtils.formatFone(selectedItem.getFax()));
//            tboxEmail.setText(selectedItem.getEmail());
//
//            tboxImpostoRenda.setText(selectedItem.getImpRenda().toString());
//            tboxComissaoFat.setText(selectedItem.getComissaoFat().toString());
//            tboxComissaoFat2.setText(selectedItem.getComissaoFat2().toString());
//            tboxComissaoRec.setText(selectedItem.getComissaoRec().toString());
//            tboxComissaoRec2.setText(selectedItem.getComissaoRec2().toString());
//        }
//    }
//
//    private void loadFromForm(Boolean cleanForm) {
//
//        if (inEdition.get()) {
//            selectedItem.setCodigo(Integer.parseInt(tboxCodigo.getText()));
//
//            // Define se esta ativo ou inativo
//            selectedItem.setAtivo(cboxSituacao.selectedProperty().get() ? "S" : "N");
//            selectedItem.setDocumento(tboxDocumento.getText().replaceAll("[^0-9]", ""));
//            selectedItem.setNome(tboxNome.getText());
//            selectedItem.setInscricao(tboxInscricaoEstadual.getText());
//            selectedItem.setCep(tboxCep.getText().replaceAll("[^0-9]", ""));
//            selectedItem.setEndereco(tboxEndereco.getText());
//            selectedItem.setBairro(tboxBairro.getText());
//            selectedItem.setResponsavel(tboxResponsavel.getText());
//
//            selectedItem.setEmail(tboxEmail.getText());
//            selectedItem.setObs(tboxObservacao.getText());
//
//            if (tboxTelefone.getText() != null && !tboxTelefone.getText().equals("")) {
//                selectedItem.setTelefone(tboxTelefone.getText().replaceAll("[^0-9]", ""));
//            }
//
//            if (tboxCelular.getText() != null && !tboxCelular.getText().equals("")) {
//                selectedItem.setCelular(tboxCelular.getText().replaceAll("[^0-9]", ""));
//            }
//
//            if (tboxFax.getText() != null && !tboxFax.getText().equals("")) {
//                selectedItem.setCelular(tboxFax.getText().replaceAll("[^0-9]", ""));
//            }
//
//            selectedItem.setImpRenda(BigDecimal.valueOf(Double.parseDouble(tboxImpostoRenda.getText())));
//
//            selectedItem.setComissaoFat(BigDecimal.valueOf(Double.parseDouble(tboxComissaoFat.getText())));
//            selectedItem.setComissaoFat2(BigDecimal.valueOf(Double.parseDouble(tboxComissaoFat2.getText())));
//            selectedItem.setComissaoRec(BigDecimal.valueOf(Double.parseDouble(tboxComissaoRec.getText())));
//            selectedItem.setComissaoRec2(BigDecimal.valueOf(Double.parseDouble(tboxComissaoRec2.getText())));
//        }
//
//        if (cleanForm) {
//            clearForm();
//        }
//    }
//
//    private void clearForm() {
//        tboxCodigo.clear();
//
//        tboxDocumento.clear();
//        tboxNome.clear();
//        tboxInscricaoEstadual.clear();
//        tboxCep.clear();
//        tboxEndereco.clear();
//        tboxBairro.clear();
//        tboxResponsavel.clear();
//        tboxTelefone.clear();
//        tboxCelular.clear();
//        tboxFax.clear();
//        tboxEmail.clear();
//
//        tboxImpostoRenda.clear();
//        tboxComissaoFat.clear();
//        tboxComissaoFat2.clear();
//        tboxComissaoRec.clear();
//        tboxComissaoRec2.clear();
//
//        cboxSituacao.setSelected(false);
//        tboxCidade.clear();
//        tboxRegiao.clear();
//
//        cboxSituacao.setSelected(false);
//
//        inEdition.set(false);
//    }
//
//    private void loadDadosViaCEP(boolean showAvisoCepInvalido) {
//
//        String cep = tboxCep.getText();
//        cep = cep.replaceAll("[^0-9]", "");
//        if (cep.length() != 8) {
//            if (showAvisoCepInvalido) {
//                GUIUtils.showMessage("O Valor informado não é um CEP válido.", Alert.AlertType.INFORMATION);
//            }
//            return;
//        }
//
//        CEP mCep = ServicoViaCEP.getInstance().consultaCEP(cep);
//
//        if (tboxBairro.getText().equals("")) {
//            tboxBairro.setText(mCep.getBairro());
//        }
//
//        if (tboxEndereco.getText().equals("")) {
//            tboxEndereco.setText(mCep.getLogradouro());
//        }
//
//        GenericDao<SdCidade> mCidadeDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade.class);
//        try {
//            if (selectedItem.getCidade() == null) {
//                selectedItem.setCidade(mCidadeDao.load(Integer.parseInt(mCep.getIbge())));
//                // Se o nome não foi definido
//                tboxCidade.setText(selectedItem.getCidade().getNome());
//            }
//        } catch (SQLException e) {
//            GUIUtils.showException(e);
//            e.printStackTrace();
//        }
//
//    }
//
//    private void deleteRepresentanteColecao() {
//        if (GUIUtils.showQuestionMessageDefaultNao("Ao remover este registro serão removidas as marcas e cidades vinculadas préviamente e este processo não poderá ser " +
//                "desfeito. Deseja prosseguir?")) {
//
//            try {
//                // Remove as cidades / Marcas
//                colecaoSelecionada.getSdRepresentanteColecaoMarcas001().forEach(registro -> {
//                    try {
//                        registro.getCidadeList().removeAll(registro.getCidadeList());
//                        sdRepresentanteColecaoMarca001Dao.update(registro);
//
//                        // Remove as marcas
//                        sdRepresentanteColecaoMarca001Dao.delete(registro.getCodigo());
//
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                });
//
//                // Remove a Coleção
//                sdRepresentanteColecao001Dao.delete(colecaoSelecionada.getCodigo());
//
//                lstRepresentanteColecaoMarcasCidades.clear();
//                tblCidadesAntedidas.refresh();
//
//                lstRepresentanteColecaoMarcas.clear();
//                tblMarcasAntedidas.refresh();
//
//                lstRepresentanteColecoes.remove(colecaoSelecionada);
//                tblColecoesAntedidas.refresh();
//
//                List<SdRepresentanteColecao> listaTemp = selectedItem.getSdRepresentanteColecaoList();
//
//                for (SdRepresentanteColecao colecao001 : listaTemp) {
//                    if(colecao001.getColecao().getCodigo().equals(colecaoSelecionada.getColecao().getCodigo())){
//                        listaTemp.remove(colecao001);
//                    }
//                }
//
//                selectedItem.setSdRepresentanteColecaoList(listaTemp);
//
//                lstRepresentanteColecoes.sort((o1, o2) -> o2.getColecao().getCodigo().compareTo(o1.getColecao().getCodigo()));
//
//                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getStrLoginUsuario(),
//                        Log.TipoAcao.EXCLUIR,
//                        "Cadastro de Representantes",
//                        selectedItem.getCodigo() + "",
//                        "Removido vinculo entre representante e coleção" +
//                                selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]/" +
//                                colecaoSelecionada.getColecao().getDescricao())
//                );
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }
//    }
//
//    private void deleteRepresentanteColecaoMarca() {
//        if (GUIUtils.showQuestionMessageDefaultNao("Ao remover este registro serão removidas as cidades vinculadas préviamente e este processo não poderá ser" +
//                "desfeito. Deseja prosseguir?")) {
//
//            try {
//                sdRepresentanteColecaoMarca001Dao.delete(marcaSelecionada.getCodigo());
//                lstRepresentanteColecaoMarcas.remove(marcaSelecionada);
//                colecaoSelecionada.getSdRepresentanteColecaoMarcas001().remove(marcaSelecionada);
//                tblMarcasAntedidas.refresh();
//                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getStrLoginUsuario(),
//                        Log.TipoAcao.EXCLUIR,
//                        "Cadastro de Representantes",
//                        selectedItem.getCodigo() + "",
//                        "Removido vinculo entre coleção e marca" +
//                                selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]/ Coleção:" +
//                                colecaoSelecionada.getColecao().getDescricao() + " Marca: " + marcaSelecionada.getMarca().getDescricao())
//                );
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }
//    }
//
//    private void deleteRepresentanteColecaoMarcaCidade() {
//        if (GUIUtils.showQuestionMessageDefaultNao("Deseja remover a cidade selecionada?")) {
//
//            try {
//                marcaSelecionada.getCidadeList().remove(cidadeSelecionada);
//                sdRepresentanteColecaoMarca001Dao.update(marcaSelecionada);
//
//                lstRepresentanteColecaoMarcasCidades.remove(cidadeSelecionada);
//                tblCidadesAntedidas.refresh();
//
//                Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getStrLoginUsuario(),
//                        Log.TipoAcao.EXCLUIR,
//                        "Cadastro de Representantes",
//                        selectedItem.getCodigo() + "",
//                        "Removido Cidade " +
//                                selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]/ Coleção:" +
//                                colecaoSelecionada.getColecao().getDescricao() + " Marca: " + marcaSelecionada.getMarca().getDescricao() + " Cidade: " + cidadeSelecionada.getNome())
//                );
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }
//    }
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//
//        // Carrega a lista de representantes
//        try {
//            sdRepresentanteDao
//                    .initCriteria()
//                    .addPredicateEq("ativo", "S");
//
//            lstRegistros = sdRepresentanteDao.loadListByPredicate();
//        } catch (SQLException e) {
//            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, e);
//            GUIUtils.showException(e);
//        }
//
//        cmbAtivo.setItems(FXCollections.observableArrayList("Todos", "Ativos", "Inativos"));
//        cmbAtivo.getSelectionModel().select(1);
//
//        cBoxTipo.setItems(FXCollections.observableArrayList("Pessoa Física", "Pessoa Jurídica"));
//
//        GenericDao<Marca> marcaDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Marca.class);
//        GenericDao<Colecao> colecaoDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Colecao.class);
//
//        try {
//            marcaDao.initCriteria().addPredicateIn("codigo", new String[]{"D", "F"});
//            ObservableList<Marca> tmpMarcas = marcaDao.loadListByPredicate();
//
//            cBoxMarca.setItems(tmpMarcas);
//
//            colecaoDao
//                    .initCriteria()
//                    .addPredicateNIn("codigo", new String[]{"KYLY", "GR", "17CO", "HAV", "B2B", "HAP", "KTKT", "MKT", "FAR"})
//                    .addPredicateEq("sdAtivo", "X")
//                    .addOrderByAsc("codigo");
//
//            ObservableList<Colecao> tmpColecao = colecaoDao.loadListByPredicate();
//            cBoxColecao.setItems(tmpColecao);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        cBoxMarca.setConverter(new StringConverter<Marca>() {
//            @Override
//            public String toString(Marca object) {
//                return object.getDescricao();
//            }
//
//            @Override
//            public Marca fromString(String string) {
//                return null;
//            }
//        });
//
//        cBoxColecao.setConverter(new StringConverter<Colecao>() {
//            @Override
//            public String toString(Colecao object) {
//                return object.getCodigo() + " - " + object.getDescricao();
//            }
//
//            @Override
//            public Colecao fromString(String string) {
//                return null;
//            }
//        });
//
//        tblRegisters.setItems(lstRegistros);
//
//        lstRepresentanteColecoes = FXCollections.observableArrayList();
//        lstRepresentanteColecaoMarcas = FXCollections.observableArrayList();
//        lstRepresentanteColecaoMarcasCidades = FXCollections.observableArrayList();
//
//        tblColecoesAntedidas.setItems(lstRepresentanteColecoes);
//        tblMarcasAntedidas.setItems(lstRepresentanteColecaoMarcas);
//        tblCidadesAntedidas.setItems(lstRepresentanteColecaoMarcasCidades);
//
//        // Mantém o campo pesquisa sempre em caixa alta.
//        TextFieldUtils.upperCase(tboxDefaultFilter);
//        TextFieldUtils.upperCase(tboxNome);
//        TextFieldUtils.upperCase(tboxInscricaoEstadual);
//        TextFieldUtils.upperCase(tboxEndereco);
//        TextFieldUtils.upperCase(tboxBairro);
//        TextFieldUtils.upperCase(tboxResponsavel);
//        TextFieldUtils.lowerCase(tboxEmail);
//
//        MaskTextField.cpfCnpjField(tboxDocumento);
//        MaskTextField.cepField(tboxCep);
//
//        MaskTextField.foneField(tboxTelefone);
//        MaskTextField.foneField(tboxCelular);
//        MaskTextField.foneField(tboxFax);
//
//        // Binda para sempre que alterar a quantidade de registros corrigir a descricao.
//        lbRegistersCount.textProperty()
//                .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));
//
//        // Controle dos botões
//        btnCancel.disableProperty().bind(disableBtnCancelRegister);
//        btnSave.disableProperty().bind(disableBtnSaveRegister);
//        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
//        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);
//
//        btnReturn.disableProperty().bind(inEdition);
//        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
//        btnAddRegister2.disableProperty().bind(inEdition);
//
//        cBoxColecao.disableProperty().bind(inEdition);
//        btnAddColecao.disableProperty().bind(inEdition);
//
//        cBoxMarca.disableProperty().bind(inEdition);
//        btnAddMarca.disableProperty().bind(inEdition);
//
//        btnAddCidades.disableProperty().bind(inEdition);
//
//        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
//        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
//        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
//        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
//
//        disableBtnSaveRegister.bind(inEdition.not());
//        disableBtnCancelRegister.bind(inEdition.not());
//        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
//        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
//
//        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
//        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
//        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(lstRegistros.size() - 1));
//
//        btnConsultaReceitaWS.disableProperty().bind(inEdition.not());
//        btnConsultaCep.disableProperty().bind(inEdition.not());
//        btnConsultaCidade.disableProperty().bind(inEdition.not());
//        btnConsultaRegiao.disableProperty().bind(inEdition.not());
//        cboxSituacao.disableProperty().bind(inEdition.not());
//
//        makeButtonsActions();
//
//        //<editor-fold desc="Colunas tela principal">
//        // Configura as colunas
//        clnCodigo.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCodigo().toString()));
//        clnNome.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getNome()));
//        clnTelefone.setCellValueFactory(param -> new SimpleStringProperty(StringUtils.formatFone(param.getValue().getTelefone())));
//        clnCelular.setCellValueFactory(param -> new SimpleStringProperty(StringUtils.formatFone(param.getValue().getCelular())));
//        clnResponsavel.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getResponsavel()));
//        clnCep.setCellValueFactory(param -> new SimpleStringProperty(StringUtils.formatCep(param.getValue().getCep())));
//        clnCidade.setCellValueFactory(param -> param.getValue().getCidade().nomeProperty());
//        clnEndereco.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getEndereco()));
//        clnDocumento.setCellValueFactory(param -> new SimpleStringProperty(StringUtils.formatCpfCnpj(param.getValue().getDocumento())));
//        clnComissaoFat.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getComissaoFat().toString() + "%"));
//        clnRegiao.setCellValueFactory(param -> {
//            if (param.getValue().getRegiao() == null) {
//                return new SimpleStringProperty("SEM REGIÃO");
//            } else {
//                return new SimpleStringProperty(param.getValue().getRegiao().getDescricao());
//            }
//        });
//
//        clnAtivo.setCellValueFactory(param -> {
//            if (param.getValue().getAtivo().equals("S")) {
//                return new SimpleObjectProperty<>(Constantes.newImageViewActive());
//            } else {
//                return new SimpleObjectProperty<>(Constantes.newImageViewInactive());
//            }
//        });
//
//        clnEmail.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getEmail()));
//        clnImpostoRenda.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getImpRenda().toString().replace(".", ",") + "%"));
//        //</editor-fold>
//
//        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//            clearForm();
//            selectedItem = newValue;
//
//            lstRepresentanteColecoes.clear();
//            lstRepresentanteColecoes.addAll(selectedItem.getSdRepresentanteColecaoList());
//
//            lstRepresentanteColecoes.sort((o1, o2) -> o2.getColecao().getCodigo().compareTo(o1.getColecao().getCodigo()));
//
//            tblColecoesAntedidas.getSelectionModel().selectFirst();
//
//            populateForm();
//            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
//        });
//
//        tblColecoesAntedidas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//            colecaoSelecionada = newValue;
//
//            lstRepresentanteColecaoMarcas.clear();
//            lstRepresentanteColecaoMarcas.addAll(colecaoSelecionada.getSdRepresentanteColecaoMarcas001());
//
//            tblMarcasAntedidas.getSelectionModel().selectFirst();
//        });
//
//        // Tabela de marcas atendidas por representante.
//        clnCodigoColecao.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getColecao().getCodigo()));
//        clnDescricaoColecao.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getColecao().getDescricao()));
//
//        tblMarcasAntedidas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
//            marcaSelecionada = newValue;
//
//            lstRepresentanteColecaoMarcasCidades.clear();
//            lstRepresentanteColecaoMarcasCidades.addAll(marcaSelecionada.getCidadeList());
//
//            tblCidadesAntedidas.getSelectionModel().selectFirst();
//        });
//
//        clnMarca.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getMarca().getDescricao()));
//
//        tblCidadesAntedidas.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> cidadeSelecionada = newValue));
//
//        clnCidadeCodigo.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCodigo().toString()));
//        clnCidadeNome.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getNome()));
//        clnEstadoSigla.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getSiglaUf()));
//
//        tblRegisters.getSelectionModel().selectFirst();
//
//        // Campos sempre inativos
//        tboxCodigo.setEditable(false);
//        tboxCidade.setEditable(false);
//        tboxRegiao.setEditable(false);
//        cBoxTipo.disableProperty().set(true);
//
//        // Campos inativados dinamicamente
//        tboxCidade.disableProperty().bind(inEdition.not());
//        tboxRegiao.disableProperty().bind(inEdition.not());
//        tboxDocumento.disableProperty().bind(inEdition.not());
//        cboxSituacao.disableProperty().bind(inEdition.not());
//        tboxNome.disableProperty().bind(inEdition.not());
//        tboxInscricaoEstadual.disableProperty().bind(inEdition.not());
//        tboxCep.disableProperty().bind(inEdition.not());
//        tboxEndereco.disableProperty().bind(inEdition.not());
//        tboxBairro.disableProperty().bind(inEdition.not());
//        tboxResponsavel.disableProperty().bind(inEdition.not());
//        tboxTelefone.disableProperty().bind(inEdition.not());
//        tboxFax.disableProperty().bind(inEdition.not());
//        tboxCelular.disableProperty().bind(inEdition.not());
//        tboxEmail.disableProperty().bind(inEdition.not());
//        tboxImpostoRenda.disableProperty().bind(inEdition.not());
//        tboxComissaoFat.disableProperty().bind(inEdition.not());
//        tboxComissaoFat2.disableProperty().bind(inEdition.not());
//        tboxComissaoRec.disableProperty().bind(inEdition.not());
//        tboxComissaoRec2.disableProperty().bind(inEdition.not());
//        tboxObservacao.disableProperty().bind(inEdition.not());
//
//        tboxDocumento.setOnKeyPressed(k -> {
//            final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
//            final KeyCombination TAB = new KeyCodeCombination(KeyCode.TAB);
//            if (ENTER.match(k) || TAB.match(k)) {
//                if (selectedItem.getCodigo() <= 0) {
//                    // verifica se o documento já esta cadastrado para outro representante
//                    sdRepresentanteDao.initCriteria();
//                    String documento = tboxDocumento.getText();
//                    documento = documento.replaceAll("[^0-9]", "");
//                    sdRepresentanteDao.addPredicateEq("documento", documento);
//
//                    try {
//                        SdRepresentante rep = sdRepresentanteDao.loadEntityByPredicate();
//                        if (rep != null) {
//                            GUIUtils.showMessage("Já existe um representante com este documento cadastrado.", Alert.AlertType.INFORMATION);
//                        }
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (documento.length() == 14) {
//                        cBoxTipo.getSelectionModel().select(1);
//                        try {
//                            btnConsultaDadosReceitaWS(null);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
//                            GUIUtils.showException(e);
//                        }
//                    } else {
//                        cBoxTipo.getSelectionModel().select(0);
//                        tboxInscricaoEstadual.setText("ISENTO");
//                    }
//                }
//            }
//        });
//
//        tboxCep.setOnKeyPressed(k -> {
//            final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
//            final KeyCombination TAB = new KeyCodeCombination(KeyCode.TAB);
//            if (ENTER.match(k) || TAB.match(k)) {
//                if (selectedItem.getCodigo() <= 0) {
//                    String cep = tboxCep.getText();
//                    cep = cep.replaceAll("[^0-9]", "");
//
//                    if (cep.length() == 8) {
//                        btnConsultaDadosViaCEP(null);
//                    }
//                }
//            }
//        });
//    }
//
//    @FXML
//    public void btnAddRegisterOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            loadFromForm(true);
//            selectedItem = new SdRepresentante();
//
//            inEdition.set(true);
//            populateForm();
//
//            tabMainPane.getSelectionModel().select(1);
//            tboxDocumento.requestFocus();
//
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnFindDefaultOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            try {
//                sdRepresentanteDao.initCriteria();
//                sdRepresentanteDao
//                        .addPredicateLike("nome", "%" + tboxDefaultFilter.textProperty().get() + "%")
//                        .addPredicateLike("responsavel", "%" + tboxDefaultFilter.textProperty().get() + "%");
//
//                switch (cmbAtivo.getSelectionModel().getSelectedIndex()) {
//                    case 1:
//                        sdRepresentanteDao.addPredicateEq("ativo", "S");
//                        break;
//                    case 2:
//                        sdRepresentanteDao.addPredicateEq("ativo", "N");
//                        break;
//                }
//
//                lstRegistros.setAll(sdRepresentanteDao.loadListByPredicate());
//
//                lbRegistersCount.textProperty()
//                        .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));
//
//                tboxDefaultFilter.clear();
//                cmbAtivo.getSelectionModel().select(1);
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnFirstRegisterOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            tblRegisters.getSelectionModel().selectFirst();
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnPreviusRegisterOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            tblRegisters.getSelectionModel().selectPrevious();
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnNextRegisterOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            tblRegisters.getSelectionModel().selectNext();
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnLastRegisterOnAtion(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            tblRegisters.getSelectionModel().selectLast();
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnAddRegister2OnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            btnAddRegisterOnAction(actionEvent);
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnEditRegisterOnAction(ActionEvent actionEvent) {
//        if (actionEvent == null || !actionEvent.isConsumed()) {
//            loadFromForm(true);
//            inEdition.set(true);
//            populateForm();
//            if (actionEvent != null) {
//                actionEvent.consume();
//            }
//        }
//    }
//
//    @FXML
//    public void btnDeleteRegisterOnAction(ActionEvent actionEvent) {
//        if (actionEvent == null || !actionEvent.isConsumed()) {
//            GUIUtils.showMessage("Não é permitido remover registros nesta tabela. Se o representante não foi mais ativo, basta inativa-lo.");
////            if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
////                try {
////                    sdRepresentanteDao.delete(selectedItem.getCodigo());
////                    lstRegistros.remove(selectedItem);
////                    tblRegisters.getSelectionModel().selectFirst();
////
////                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(SceneMainController.getAcesso().getStrLoginUsuario(),
////                            Log.TipoAcao.EXCLUIR,
////                            "Cadastro de Representantes",
////                            selectedItem.getCodigo() + "",
////                            "Excluído a Representante " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
////                    ));
////                } catch (SQLException ex) {
////                    LoggerExtensions.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
////                    GUIUtils.showException(ex);
////                }
////            }
//            if (actionEvent != null) {
//                actionEvent.consume();
//            }
//        }
//    }
//
//    @FXML
//    public void btnSaveOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//
//            // Remove as mascaras dos campos para ficar mais limpo e padronizado
//            this.loadFromForm(false);
//            selectedItem.removeMascaras();
//
//            Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
//
//            Set<ConstraintViolation<SdRepresentante>> violations = validator.validate(selectedItem);
//
//            if (!violations.isEmpty()) {
//                StringBuilder mensagem = new StringBuilder("Existem problemas no cadastro:\r\n");
//                for (ConstraintViolation<SdRepresentante> violation : violations) {
//                    mensagem
//                            .append(violation.getMessage())
//                            .append("\r\n");
//                }
//
//                GUIUtils.showMessage(mensagem.toString());
//                return;
//            }
//
//            try {
//                // Atualiza/Grava na tabela Represen_001
//                GenericDao<RepresenTi> mRepresen001Dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), RepresenTi.class);
//                GenericDao<Codigos> mCodigos001Dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Codigos.class);
//
//                if (selectedItem.getCodigo() == 0) {
//                    List<Codigos> codigos = mCodigos001Dao.simpleList();
//                    BigDecimal codigoAtual = new BigDecimal(-1);
//                    for (Codigos codigo : codigos) {
//                        if (codigo.getCodigosId().getTabela().equals("REPRESEN")) {
//                            codigoAtual = codigo.getProximo();
//                            codigo.setProximo(codigoAtual.add(BigDecimal.ONE));
//                            mCodigos001Dao.update(codigo);
//                        }
//                    }
//
//                    if (codigoAtual.intValue() <= 0) {
//                        GUIUtils.showMessage("Não foi possivel gerar um código para o representante.", Alert.AlertType.ERROR);
//                    } else {
//                        selectedItem.setCodigo(codigoAtual.intValue());
//
//                        mRepresen001Dao.save(selectedItem.toRepresen001());
//
//                        // remove formato dos campos necessários.
//                        selectedItem.removeMascaras();
//                        selectedItem = sdRepresentanteDao.save(selectedItem);
//                        Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
//                                SceneMainController.getAcesso().getStrLoginUsuario(),
//                                Log.TipoAcao.CADASTRAR,
//                                "Cadastro de Representantes",
//                                selectedItem.getCodigo() + "",
//                                "Cadastrado o representante " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
//                        ));
//
//                        if (!lstRegistros.contains(selectedItem)) {
//                            lstRegistros.add(selectedItem);
//                        }
//                    }
//                } else {
//                    mRepresen001Dao.update(selectedItem.toRepresen001());
//
//                    // remove formato dos campos necessários.
//                    selectedItem = sdRepresentanteDao.update(selectedItem);
//
//                    this.clearForm();
//                    this.populateForm();
//
//                    Objects.requireNonNull(DAOFactory.getLogDAO()).saveLog(new Log(
//                            SceneMainController.getAcesso().getStrLoginUsuario(),
//                            Log.TipoAcao.EDITAR,
//                            "Cadastro de Representante",
//                            selectedItem.getCodigo() + "",
//                            "Alterado o representante " + selectedItem.getNome() + " [" + selectedItem.getCodigo() + "]"
//                    ));
//
//                }
//
//                lbRegistersCount.textProperty()
//                        .bind(new SimpleStringProperty("Mostrando ").concat(lstRegistros.size()).concat(" registros"));
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//
//            inEdition.set(false);
//            tblRegisters.getSelectionModel().select(selectedItem);
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnCancelOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            loadFromForm(true);
//            clearForm();
//            inEdition.set(false);
//            tblRegisters.getSelectionModel().selectFirst();
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnReturnOnAction(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            tabMainPane.getSelectionModel().select(0);
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnConsultaDadosReceitaWS(ActionEvent actionEvent) throws Exception {
//        if (actionEvent == null || !actionEvent.isConsumed()) {
//
//            int codigo = Integer.parseInt(tboxCodigo.getText());
//            boolean consultar = true;
//            if (codigo > 0) {
//                consultar = GUIUtils.showQuestionMessageDefaultNao("O Representante já esta cadastrado deseja executar a consulta mesmo assim. Isto poderá alterar as " +
//                        "informações já existentes.");
//            }
//
//            if (consultar) {
//                String documento = tboxDocumento.getText();
//                documento = documento.replaceAll("[^0-9]", "");
//
//                if (documento.length() < 14) {
//                    GUIUtils.showMessage("Consulta só possivel com CNPJ.", Alert.AlertType.INFORMATION);
//                    return;
//                }
//
//                ReceitaWS ws = ServicoReceitaWS.getInstance().consultaCNPJ(documento);
//
//                boolean alterarDados = true;
//
//                if ((tboxNome.getText() != null) && !tboxNome.getText().isEmpty()) {
//                    if (!ws.getNome().equals(tboxNome.getText())) {
//                        if (!GUIUtils.showQuestionMessageDefaultNao("O Nome vinculado a este Documento está diferente do esperado. Deseja alterar os dados?")) {
//                            alterarDados = false;
//                        }
//                    }
//                }
//
//                // Busca a cidade pelo cep informado
//                String cep = ws.getCep();
//                cep = cep.replaceAll("[^0-9]", "");
//
//                if (alterarDados) {
//                    tboxNome.setText(ws.getNome());
//                    tboxEndereco.setText(ws.getLogradouro());
//                    tboxBairro.setText(ws.getBairro());
//
//                    if (ws.getTelefone().indexOf("/") > 0) {
//                        String[] telefones = ws.getTelefone().split("/");
//
//                        tboxTelefone.setText(telefones[0]);
//                        tboxCelular.setText(telefones[1]);
//                    } else {
//                        tboxTelefone.setText(ws.getTelefone());
//                    }
//
//                    tboxEmail.setText(ws.getEmail());
//
//                    tboxCep.setText(cep);
//                }
//                loadDadosViaCEP(false);
//                if (actionEvent != null) {
//                    actionEvent.consume();
//                }
//            }
//        }
//    }
//
//    @FXML
//    public void btnConsultaDadosViaCEP(ActionEvent actionEvent) {
//        if (actionEvent == null || !actionEvent.isConsumed()) {
//            loadDadosViaCEP(true);
//            if (actionEvent != null) {
//                actionEvent.consume();
//            }
//        }
//    }
//
//    @FXML
//    public void btnConsultaCidade(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            try {
//                FilterSdCidadeSingleResultController controller = new FilterSdCidadeSingleResultController(Modals.FXMLWindow.FilterSdCidadeSingleResult, "");
//
//                if (controller.returnValue != null) {
//                    GenericDao<SdCidade> cidade001GenericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade.class);
//                    SdCidade cidade = cidade001GenericDao.load(controller.returnValue.getCodigo());
//                    selectedItem.setCidade(cidade);
//                    tboxCidade.setText(cidade.getNome());
//                }
//            } catch (IOException | SQLException e) {
//                e.printStackTrace();
//            }
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnConsultaRegiao(ActionEvent actionEvent) {
//        if (!actionEvent.isConsumed()) {
//            try {
//                FilterSdRegiaoController filter = new FilterSdRegiaoController(Modals.FXMLWindow.FilterSdRegiao);
//                if (filter.returnValue != null) {
//                    selectedItem.setRegiao(filter.returnValue);
//                    tboxRegiao.setText(filter.returnValue.getDescricao());
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            actionEvent.consume();
//        }
//    }
//
//    @FXML
//    public void btnAddRepresentanteColecaoOnAction(ActionEvent actionEvent) {
//        boolean existe = false;
//
//        for (SdRepresentanteColecao sdRepresentanteColecao : lstRepresentanteColecoes) {
//            if (sdRepresentanteColecao.getColecao().getCodigo().equals(cBoxColecao.getSelectionModel().getSelectedItem().getCodigo())) {
//                existe = true;
//            }
//        }
//
//        if (existe) {
//            GUIUtils.showMessage("Coleção já esta vinculada a este representante.");
//        } else {
//            SdRepresentanteColecao sdColecao001Tmp = new SdRepresentanteColecao(selectedItem.getCodigo(), cBoxColecao.getSelectionModel().getSelectedItem());
//
//            try {
//                SdRepresentanteColecao tmp = null;
//
//                if(selectedItem.getSdRepresentanteColecaoList().size()> 0) {
//                    tmp = selectedItem.getSdRepresentanteColecaoList().get(0);
//                }
//
//                // Grava a coleção para ter o código para poder executar o proximo nivel
//                sdColecao001Tmp = sdRepresentanteColecao001Dao.save(sdColecao001Tmp);
//                lstRepresentanteColecoes.add(sdColecao001Tmp);
//                lstRepresentanteColecoes.sort((o1, o2) -> o2.getColecao().getCodigo().compareTo(o1.getColecao().getCodigo()));
//
//                selectedItem.getSdRepresentanteColecaoList().add(sdColecao001Tmp);
//
//                tblColecoesAntedidas.refresh();
//                tblColecoesAntedidas.getSelectionModel().select(sdColecao001Tmp);
//
//                // Duplica os dados do ultimo registro
//                if(tmp != null){
//                    // Insere as marcas
//                    for (SdRepresentanteColecaoMarca sdRepresentanteColecaoMarca : tmp.getSdRepresentanteColecaoMarcas001()) {
//                        SdRepresentanteColecaoMarca sdColecaoMarca001Tmp = new SdRepresentanteColecaoMarca(
//                                sdColecao001Tmp.getCodigo(),
//                                sdRepresentanteColecaoMarca.getMarca()
//                        );
//
//                        for (SdCidade sdCidade : sdRepresentanteColecaoMarca.getCidadeList()) {
//                            sdColecaoMarca001Tmp.getCidadeList().add(sdCidade);
//                        }
//
//                        sdColecaoMarca001Tmp = sdRepresentanteColecaoMarca001Dao.save(sdColecaoMarca001Tmp);
//                        lstRepresentanteColecaoMarcas.add(sdColecaoMarca001Tmp);
//                        colecaoSelecionada.getSdRepresentanteColecaoMarcas001().add(sdColecaoMarca001Tmp);
//                        tblMarcasAntedidas.refresh();
//                        tblMarcasAntedidas.getSelectionModel().select(sdColecaoMarca001Tmp);
//                    }
//                }
//
//
//
//                GUIUtils.showMessageNotification("Adicionado", "Adicionado coleção com sucesso!");
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//
//
//        }
//
//        actionEvent.consume();
//    }
//
//    @FXML
//    public void btnAddColecaoMarcaOnAction(ActionEvent actionEvent) {
//        boolean existe = false;
//
//        for (SdRepresentanteColecaoMarca sdRepresentanteColecaoMarca : lstRepresentanteColecaoMarcas) {
//            if (sdRepresentanteColecaoMarca.getMarca().getCodigo().equals(cBoxMarca.getSelectionModel().getSelectedItem().getCodigo())) {
//                existe = true;
//            }
//        }
//
//        if (existe) {
//            GUIUtils.showMessage("Marca já esta vinculada a este representante.");
//        } else {
//            boolean inserirMunicipios = GUIUtils.showQuestionMessageDefaultNao("Inserir todas as cidades da região informada no representante?");
//
//            SdRepresentanteColecaoMarca tmp = new SdRepresentanteColecaoMarca(colecaoSelecionada.getCodigo(), cBoxMarca.getSelectionModel().getSelectedItem());
//
//            try {
//                // Insere os municipios da regiao aqui
//                if(inserirMunicipios) {
//                    for (SdCidade sdCidade : selectedItem.getRegiao().getCidadeList()) {
//                        tmp.getCidadeList().add(sdCidade);
//                    }
//                }
//
//                tmp = sdRepresentanteColecaoMarca001Dao.save(tmp);
//                lstRepresentanteColecaoMarcas.add(tmp);
//                colecaoSelecionada.getSdRepresentanteColecaoMarcas001().add(tmp);
//                tblMarcasAntedidas.refresh();
//                tblMarcasAntedidas.getSelectionModel().select(tmp);
//
//                GUIUtils.showMessageNotification("Adicionado", "Adicionado marca com sucesso!");
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }
//        actionEvent.consume();
//    }
//
//    @FXML
//    public void btnAddCidadesOnAction(ActionEvent actionEvent) {
//
//        if( tblMarcasAntedidas.getSelectionModel().isEmpty()){
//            GUIUtils.showMessage("Adicione uma marca antes.");
//            return;
//        }
//
//        List<String> list = new ArrayList<>();
//
//        for (TabUf tabUf : selectedItem.getRegiao().getUfList()) {
//            list.add(tabUf.getId().getSiglaEst());
//        }
//
//        Object[] ufs = list.toArray();
//
//        try {
//            FilterSdCidadeController filter = new FilterSdCidadeController(Modals.FXMLWindow.FilterSdCidade, ufs);
//
//            if (filter.returnValue != null && filter.returnValue.size() > 0) {
//                for (SdCidade sdCidade : filter.returnValue) {
//                    boolean exists = false;
//                    for (SdCidade cidade001 : lstRepresentanteColecaoMarcasCidades) {
//                        if(cidade001.getCodigo().equals(sdCidade.getCodigo())){
//                            exists = true;
//                        }
//                    }
//                    if (!exists){
//                        try {
//                            marcaSelecionada.getCidadeList().add(sdCidade);
//                            GenericDao<SdRepresentanteColecaoMarca> genericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRepresentanteColecaoMarca.class);
//                            marcaSelecionada = genericDao.save(marcaSelecionada);
//
//                            lstRepresentanteColecaoMarcasCidades.add(sdCidade);
//                            tblCidadesAntedidas.refresh();
//                        } catch (SQLException ex) {
//                            Logger.getLogger(SceneCadastroRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
//                            GUIUtils.showException(ex);
//                        }
//                        GUIUtils.showMessageNotification("Adicionado", "Cidade adicionada com sucesso!");
//                    }
//                }
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        actionEvent.consume();
//    }
//}
