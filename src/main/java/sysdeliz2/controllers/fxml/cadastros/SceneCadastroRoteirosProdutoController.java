/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.PrintReportPreview;
import sysdeliz2.utils.apis.organize.ServiceOrganize;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.SysLogger;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastroRoteirosProdutoController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc="Declaração: Controles de Navegação">
    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private final ListProperty<SdRoteiroOrganize001> listRegisters = new SimpleListProperty();
    private SdRoteiroOrganize001 selectedItem = null;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Components">
    @FXML
    private TabPane tabMainPane;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private TableView<SdRoteiroOrganize001> tblRegisters;
    @FXML
    private TableColumn<SdRoteiroOrganize001, SdRoteiroOrganize001> clnActions;
    @FXML
    private TableView<SdOperRoteiroOrganize001> tblRoteiroOperacoes;
    @FXML
    private TableColumn<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK> clnOrdemOperacao;
    @FXML
    private TableColumn<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK> clnOperacao;
    @FXML
    private TableColumn<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK> clnEquipamentoOperacao;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField tboxOrdemOperacao;
    @FXML
    private TextField tboxRoteiroCodigo;
    @FXML
    private TextField tboxRoteiroReferencia;
    @FXML
    private TextField tboxRoteiroDescricao;
    @FXML
    private TextField tboxRoteiroPorcentagem;
    @FXML
    private TextField tboxRoteiroSetor;
    @FXML
    private TextField tboxRoteiroDtCadastro;
    @FXML
    private TextField tboxRoteiroDtUltimaAlteracao;
    @FXML
    private TextField tboxRoteiroTempoHora;
    @FXML
    private TextField tboxRoteiroTempo;
    @FXML
    private TextField tboxFiltroOperacao;
    @FXML
    private TextField tboxEquipamentoCodigo;
    @FXML
    private TextField tboxEquipamentoDescricao;
    @FXML
    private TextField tboxEquipamentoInterferencia;
    @FXML
    private TextField tboxOperacaoCodigo;
    @FXML
    private TextField tboxOperacaoDescricao;
    @FXML
    private TextField tboxOperacaoTempo;
    @FXML
    private TextField tboxOperacaoTempoHora;
    @FXML
    private TextField tboxOperacaoCusto;
    @FXML
    private TextField tboxOperacaoConcessao;
    @FXML
    private TextField tboxOperacaoDtCadastro;
    @FXML
    private TextField tboxOperacaoDtUltimaAlteracao;
    @FXML
    private ComboBox<SdSetorOp001> cboxSetorOperacao;
    @FXML
    private Button btnSincronizarOrganize;
    @FXML
    private Button btnAtualizarTemposOrganize;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    GenericDao<SdRoteiroOrganize001> daoRoteiros = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRoteiroOrganize001.class);
    GenericDao<SdOperRoteiroOrganize001> daoOpersRoteiros = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdOperRoteiroOrganize001.class);
    GenericDao<SdOperacaoOrganize001> daoOperacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdOperacaoOrganize001.class);
    GenericDao<SdEquipamentosOrganize001> daoEquipamento = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdEquipamentosOrganize001.class);
    GenericDao<SdColecaoMarca001> daoColecao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdColecaoMarca001.class);
    GenericDao<SdSetorOp001> daoSetorOp = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdSetorOp001.class);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Controle DRAG/DROP">
    private ArrayList<SdOperRoteiroOrganize001> operacoesSelecionadas = new ArrayList<>();
    private final DataFormat SERIALIZED_MIME_TYPE_DROP = Globals.SERIALIZED_MIME_TYPE;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Controle de Operações">
    private final ListProperty<SdOperRoteiroOrganize001> listOperacoes = new SimpleListProperty();
    private final ObservableList<SdOperRoteiroOrganize001> listOperacoesAll = FXCollections.observableArrayList();
    private SdOperRoteiroOrganize001 selectedItemOperacaoEdit = null;
    private SdOperRoteiroOrganize001 selectedItemOperacaoOld = null;
    //</editor-fold>
    private final ListProperty<SdSetorOp001> listSetoresOperacao = new SimpleListProperty<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        this.initializeComponents();
        try {
            listSetoresOperacao.set(daoSetorOp.list());
            List<String> marcas = new FluentDao().selectFrom(SdColecaoMarca001.class).get().resultList()
                    .stream()
                    .map(SdColecaoMarca001.class::cast)
                    .map(it -> it.getId().getMarca().getCodigo()).distinct()
                    .collect(Collectors.toList());

            // <editor-fold defaultstate="collapsed" desc="Cargas produtos Flor de Lis">
            List<SdRoteiroOrganize001> list = new ArrayList<>();
            for (String marca : marcas) {
                SdColecaoMarca001 colecaoAtual = (SdColecaoMarca001) daoColecao.initCriteria()
                        .addPredicateEqCascateField("id.marca.codigo", marca, false)
                        .addPredicateBetween("inicioVend", "fimVend", LocalDateTime.now())
                        .loadEntityByPredicate();
                if (colecaoAtual != null) {
                    list.addAll(daoRoteiros.initCriteria()
                            .addPredicateEqCascateField("produto.marca.sdGrupo", marca, false)
                            .addPredicateEqCascateField("produto.colecao.codigo", colecaoAtual.getId().getColecao().getCodigo(), false)
                            .loadListByPredicate());
                }
            }
            listRegisters.set(FXCollections.observableList(list));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Cargas produtos DLZ">
//            colecaoAtual = (SdColecaoMarca001) daoColecao.initCriteria()
////                    .addPredicateEqCascateField("id.marca.codigo", "D", false)
//                    .addPredicateBetween("inicioVend", "fimVend", LocalDateTime.now())
//                    .loadEntityByPredicate();
//            listRegisters.addAll(daoRoteiros.initCriteria()
//                    .addPredicateEqCascateField("produto.marca.sdGrupo", "D", false)
//                    .addPredicateEqCascateField("produto.colecao.codigo", colecaoAtual.getId().getColecao().getCodigo(), false)
//                    .loadListByPredicate());
            // </editor-fold>
            tblRegisters.getSelectionModel().selectFirst();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void initializeComponents() {

        // <editor-fold defaultstate="collapsed" desc="TBOX DEFAULT FILTER">
        TextFieldUtils.upperCase(tboxDefaultFilter);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TBOX FILTER OPERACAO">
        tboxFiltroOperacao.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchOperacoes((String) oldValue, (String) newValue);
            }
        });
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="LB REGISTER COUNT">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="BTN ACOES REGISTRO">
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);
        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="BTN NAVEGACAO REGISTROS">
        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="BTN NAVEGACAO JANELA">
        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="BTN SINCRONIZAR ORGANIZE">
        btnSincronizarOrganize.disableProperty().bind(inEdition.not());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="BTN ATUALIZAR TEMPOS ORGANIZE">
        btnAtualizarTemposOrganize.disableProperty().bind(inEdition.not());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="CBOX SETOR DE OPERACAO">
        //cboxSetorOperacao.disableProperty().bind(inEdition.not());
        cboxSetorOperacao.itemsProperty().bind(listSetoresOperacao);
        cboxSetorOperacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && inEdition.get()) {
                selectedItemOperacaoEdit.setSetorOperacao(String.valueOf(newValue.getCodigo()));
                selectedItemOperacaoEdit.setDescSetOp(newValue.getDescricao());
                selectedItemOperacaoEdit.setSetorOp(newValue);
            }
        });
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TBL REGISTERS">
        tblRegisters.itemsProperty().bind(listRegisters);
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        makeButtonsActions();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TBL OPERACOES ROTEIRO">
        tblRoteiroOperacoes.editableProperty().bind(inEdition);
        tblRoteiroOperacoes.itemsProperty().bind(listOperacoes);
        tblRoteiroOperacoes.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblRoteiroOperacoes.setRowFactory(tv -> {
            TableRow<SdOperRoteiroOrganize001> row = new TableRow<SdOperRoteiroOrganize001>() {
            };
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();

                    operacoesSelecionadas.clear();//important...
                    ObservableList<SdOperRoteiroOrganize001> items = tblRoteiroOperacoes.getSelectionModel().getSelectedItems();
                    for (SdOperRoteiroOrganize001 iI : items) {
                        operacoesSelecionadas.add(iI);
                    }
                    Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
                    operacoesSelecionadas.sort(comparator);

                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SERIALIZED_MIME_TYPE_DROP, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE_DROP)) {
                    if (row.getIndex() != ((Integer) db.getContent(SERIALIZED_MIME_TYPE_DROP)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();

                if (db.hasContent(SERIALIZED_MIME_TYPE_DROP)) {

                    int dropIndex;
                    SdOperRoteiroOrganize001 dI = null;

                    if (row.isEmpty()) {
                        dropIndex = tblRoteiroOperacoes.getItems().size();
                    } else {
                        dropIndex = row.getIndex();
                        dI = tblRoteiroOperacoes.getItems().get(dropIndex);
                    }
                    int delta = 0;
                    if (dI != null)
                        while (operacoesSelecionadas.contains(dI)) {
                            delta = 1;
                            --dropIndex;
                            if (dropIndex < 0) {
                                dI = null;
                                dropIndex = 0;
                                break;
                            }
                            dI = tblRoteiroOperacoes.getItems().get(dropIndex);
                        }

                    for (SdOperRoteiroOrganize001 sI : operacoesSelecionadas) {
                        tblRoteiroOperacoes.getItems().remove(sI);
                    }

                    if (dI != null)
                        dropIndex = tblRoteiroOperacoes.getItems().indexOf(dI) + delta;
                    else if (dropIndex != 0)
                        dropIndex = tblRoteiroOperacoes.getItems().size();

                    tblRoteiroOperacoes.getSelectionModel().clearSelection();

                    for (SdOperRoteiroOrganize001 sI : operacoesSelecionadas) {
                        //draggedIndex = selections.get(i);
                        tblRoteiroOperacoes.getItems().add(dropIndex, sI);
                        tblRoteiroOperacoes.getSelectionModel().select(dropIndex);
                        dropIndex++;
                    }

                    event.setDropCompleted(true);
                    operacoesSelecionadas.clear();
                    event.consume();
                    this.updateOrdemOperacao();
                }
            });

            return row;
        });
        tblRoteiroOperacoes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedItemOperacaoEdit = newValue;
            selectedItemOperacaoOld = oldValue;
            unbindDataOperacoes(oldValue);
            bindDataOperacoes(newValue);
        });
        clnOrdemOperacao.setCellFactory(param -> {
            return new TableCell<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK>() {
                @Override
                protected void updateItem(SdOperRoteiroOrganize001PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(String.valueOf(item.getOrdem()));
                    }
                }
            };
        });
        clnOperacao.setCellFactory(param -> {
            return new TableCell<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK>() {
                @Override
                protected void updateItem(SdOperRoteiroOrganize001PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);

                    if (item != null && !empty) {
                        setText(item.getOperacao().toString());
                    }
                }
            };
        });
        clnEquipamentoOperacao.setCellFactory(param -> {
            return new TableCell<SdOperRoteiroOrganize001, SdOperRoteiroOrganize001PK>() {
                @Override
                protected void updateItem(SdOperRoteiroOrganize001PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);

                    if (item != null && !empty) {
                        setText(item.getOperacao().getEquipamentoOrganize().toString());
                    }
                }
            };
        });
        //</editor-fold>
    }

    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>() {
            @Override
            public ObservableValue<SdRoteiroOrganize001> call(TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdRoteiroOrganize001>() {
            @Override
            public int compare(SdRoteiroOrganize001 p1, SdRoteiroOrganize001 p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdRoteiroOrganize001, SdRoteiroOrganize001>, TableCell<SdRoteiroOrganize001, SdRoteiroOrganize001>>() {
            @Override
            public TableCell<SdRoteiroOrganize001, SdRoteiroOrganize001> call(TableColumn<SdRoteiroOrganize001, SdRoteiroOrganize001> btnCol) {
                return new TableCell<SdRoteiroOrganize001, SdRoteiroOrganize001>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final ImageView btnPrintRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/print (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final Button btnPrintRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnPrintRow = new Tooltip("Imprimir roteiro registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Excluir do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnPrintRow.setGraphic(btnPrintRowIcon);
                        btnPrintRow.setTooltip(tipBtnPrintRow);
                        btnPrintRow.setText("Imprimir do Registro");
                        btnPrintRow.getStyleClass().add("success");
                        btnPrintRow.getStyleClass().add("xs");
                        btnPrintRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(SdRoteiroOrganize001 seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow, btnPrintRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tblRegisters.getSelectionModel().select(seletedRow);
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                    tblRoteiroOperacoes.requestFocus();
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tblRegisters.getSelectionModel().select(seletedRow);
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnPrintRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tblRegisters.getSelectionModel().select(seletedRow);
                                    printRoteiroOperacoes(selectedItem);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tblRegisters.getSelectionModel().select(seletedRow);
                                    btnDeleteRegisterOnAction(null);
                                    tblRegisters.getItems().remove(seletedRow);
                                    tblRegisters.refresh();
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }

        });

    }

    private void printRoteiroOperacoes(SdRoteiroOrganize001 roteiro) {
        try {
            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RoteiroProducaoProduto.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();

            parameters.put("ROTEIRO", String.valueOf(roteiro.getCodorg()));
            parameters.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);

        } catch (JRException | NullPointerException ex) {
            GUIUtils.showException(ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            GUIUtils.showException(ex);
            ex.printStackTrace();
        }
    }

    public void searchOperacoes(String oldVal, String newVal) {
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listOperacoes.set(listOperacoesAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdOperRoteiroOrganize001> subentries = FXCollections.observableArrayList();
        for (SdOperRoteiroOrganize001 entry : listOperacoes) {
            boolean match = true;
            if (!entry.getId().getOperacao().getDescricao().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listOperacoes.set(subentries);
    }

    private void updateOrdemOperacao() {
        Integer menorOrdemSetorOperacao = listOperacoes.stream().mapToInt(operacao -> operacao.getId().getOrdem()).min().getAsInt();
        for (SdOperRoteiroOrganize001 operacaoProgramacao : listOperacoes) {
            operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao++);
        }

        Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
        listOperacoes.sort(comparator);

    }

    private void bindData(SdRoteiroOrganize001 selectItem) {
        if (selectItem != null) {
            tboxRoteiroCodigo.textProperty().bind(selectItem.codorgProperty().asString());
            tboxRoteiroDescricao.textProperty().bind(selectItem.descricaoProperty());
            tboxRoteiroDtCadastro.textProperty().bind(selectItem.dtInclusaoProperty());
            tboxRoteiroDtUltimaAlteracao.textProperty().bind(selectItem.dtAlteracaoProperty());
            tboxRoteiroPorcentagem.textProperty().bind(selectItem.percentualProperty().asString());
            tboxRoteiroReferencia.textProperty().bind(selectItem.referenciaProperty());
            tboxRoteiroSetor.textProperty().bind(selectItem.descSetorProperty());
            tboxRoteiroTempo.textProperty().bind(selectItem.tempoProperty().asString());
            tboxRoteiroTempoHora.textProperty().bind(selectItem.tempoHoraProperty().asString());

            try {
                selectItem.setOperacoes(daoOpersRoteiros.initCriteria().addPredicateEqPkEmbedded("id", "roteiro", selectItem.getCodorg(), false).loadListByPredicate());
                Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
                selectItem.getOperacoes().sort(comparator);

                listOperacoes.set(selectItem.operacoesProperty());
                listOperacoesAll.clear();
                listOperacoesAll.addAll(selectItem.operacoesProperty());
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }

            tblRoteiroOperacoes.getSelectionModel().select(0);
        }
    }

    private void unbindData() {
        tboxRoteiroCodigo.textProperty().unbind();
        tboxRoteiroDescricao.textProperty().unbind();
        tboxRoteiroDtCadastro.textProperty().unbind();
        tboxRoteiroDtUltimaAlteracao.textProperty().unbind();
        tboxRoteiroPorcentagem.textProperty().unbind();
        tboxRoteiroReferencia.textProperty().unbind();
        tboxRoteiroSetor.textProperty().unbind();
        tboxRoteiroTempo.textProperty().unbind();
        tboxRoteiroTempoHora.textProperty().unbind();
    }

    private void bindDataOperacoes(SdOperRoteiroOrganize001 selectItem) {
        if (selectItem != null) {
            tboxOrdemOperacao.textProperty().bind(selectItem.getId().ordemProperty().asString());
            tboxOperacaoCodigo.textProperty().bind(selectItem.getId().getOperacao().codorgProperty().asString());
            tboxOperacaoConcessao.textProperty().bind(selectItem.getId().getOperacao().concessaoProperty().asString());
            tboxOperacaoCusto.textProperty().bind(selectItem.getId().getOperacao().custoProperty().asString());
            tboxOperacaoDescricao.textProperty().bind(selectItem.getId().getOperacao().descricaoProperty());
            tboxOperacaoDtCadastro.textProperty().bind(selectItem.getId().getOperacao().dtInclusaoProperty());
            tboxOperacaoDtUltimaAlteracao.textProperty().bind(selectItem.getId().getOperacao().dtAlteracaoProperty());
            tboxOperacaoTempo.textProperty().bind(selectItem.getId().getOperacao().tempoProperty().asString());
            tboxOperacaoTempoHora.textProperty().bind(selectItem.getId().getOperacao().tempoHoraProperty().asString());
            tboxEquipamentoCodigo.textProperty().bind(selectItem.getId().getOperacao().getEquipamentoOrganize().codorgProperty().asString());
            tboxEquipamentoDescricao.textProperty().bind(selectItem.getId().getOperacao().getEquipamentoOrganize().descricaoProperty());
            tboxEquipamentoInterferencia.textProperty().bind(selectItem.getId().getOperacao().getEquipamentoOrganize().interferenciaProperty().asString());
            selectSetorOperacao(selectItem.getSetorOperacao());
        }
    }

    private void unbindDataOperacoes(SdOperRoteiroOrganize001 selectItem) {
        if (selectItem != null) {
            tboxOrdemOperacao.textProperty().unbind();
            tboxOperacaoCodigo.textProperty().unbind();
            tboxOperacaoConcessao.textProperty().unbind();
            tboxOperacaoCusto.textProperty().unbind();
            tboxOperacaoDescricao.textProperty().unbind();
            tboxOperacaoDtCadastro.textProperty().unbind();
            tboxOperacaoDtUltimaAlteracao.textProperty().unbind();
            tboxOperacaoTempo.textProperty().unbind();
            tboxOperacaoTempoHora.textProperty().unbind();
            tboxEquipamentoCodigo.textProperty().unbind();
            tboxEquipamentoDescricao.textProperty().unbind();
            tboxEquipamentoInterferencia.textProperty().unbind();
        }
    }

    private void selectSetorOperacao(String codigoSetor) {
        SdSetorOp001 setorSelecionado = cboxSetorOperacao.getItems().get(0);
        for (SdSetorOp001 setor : cboxSetorOperacao.getItems()) {
            if (setor.getCodigo() == Integer.parseInt(codigoSetor)) {
                setorSelecionado = setor;
            }
        }
        selectedItemOperacaoEdit.setSetorOperacao(setorSelecionado.getCodigo() + "");
        selectedItemOperacaoEdit.setDescSetOp(setorSelecionado.getDescricao());
        cboxSetorOperacao.getSelectionModel().select(setorSelecionado);
    }

    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(daoRoteiros.initCriteria()
                    .addPredicateEqCascateField("produto.codigo", tboxDefaultFilter.getText(), false)
                    .loadListByPredicate());

            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        if (tabMainPane.getSelectionModel().getSelectedIndex() == 0) {
            tabMainPane.getSelectionModel().select(1);
            tblRoteiroOperacoes.requestFocus();
        }
        unbindDataOperacoes(selectedItemOperacaoOld);
        unbindDataOperacoes(selectedItemOperacaoEdit);
        inEdition.set(true);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja excluir o roteiro?")) {
            try {
                for (SdOperRoteiroOrganize001 operacoe : tblRegisters.getSelectionModel().getSelectedItem().getOperacoes()) {
                    daoOpersRoteiros.delete(operacoe.getId());
                }
                daoRoteiros.delete(tblRegisters.getSelectionModel().getSelectedItem().getCodorg());
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindDataOperacoes(selectedItemOperacaoEdit);
        inEdition.set(false);
        tblRoteiroOperacoes.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnSincronizarOrganizeOnAction(ActionEvent event) {
        if (selectedItem.isAlteracaoMan()) {
            if (!GUIUtils.showQuestionMessageDefaultNao("Este roteiro tem uma alteração manual via SysDeliz.\n" +
                    "A sincronização com o Organize irá reverter as alterações realizadas manualmente.\n" +
                    "Desenja continuar?"))
                return;
        }

        ObservableList<SdOperRoteiroOrganize001> operacoesApi = new ServiceOrganize().importOperacaoesRoteiro(selectedItem);
        if (operacoesApi != null && operacoesApi.size() > 0) {
            Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
            operacoesApi.sort(comparator);
            selectedItem.setOperacoes(operacoesApi);

            listOperacoes.set(selectedItem.operacoesProperty());
            listOperacoesAll.clear();
            listOperacoesAll.addAll(selectedItem.operacoesProperty());

            tblRoteiroOperacoes.refresh();
            tblRoteiroOperacoes.getSelectionModel().select(0);
        }
        SysLogger.addTableLog(
                "Cadastro Operações de Roteiro",
                TipoAcao.EDITAR,
                String.valueOf(selectedItem.getCodorg()),
                "Carregado operações do roteiro " + selectedItem.getCodorg() + " da ref. " + selectedItem.getReferencia() + " manualmente para atualização.");

        GUIUtils.showMessage("Sincronização finalizada, você pode salvar o roteiro.");
    }

    @FXML
    private void btnAtualizarTemposOrganizeOnAction(ActionEvent event) {
        ObservableList<SdOperRoteiroOrganize001> operacoesApi = new ServiceOrganize().importOperacaoesRoteiro(selectedItem);
        if (operacoesApi != null && operacoesApi.size() > 0) {
            Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
            operacoesApi.sort(comparator);
            selectedItem.getOperacoes().forEach(operRoteiroBanco -> {
                for (SdOperRoteiroOrganize001 operRoteiroOrganize : operacoesApi) {
                    if (operRoteiroBanco.getId().getOperacao().getCodorg() == operRoteiroOrganize.getId().getOperacao().getCodorg()
                            && operRoteiroBanco.getId().getOrdem() == operRoteiroOrganize.getId().getOrdem()) {
                        operRoteiroBanco.setTempoOp(operRoteiroOrganize.getTempoOp());
                        try {
                            DAOFactory.getSdOperRoteiroOrganize001DAO().updateTempoOp(
                                    selectedItem.getReferencia(),
                                    operRoteiroBanco.getId().getOperacao().getCodorg(),
                                    operRoteiroOrganize.getTempoOp().doubleValue());

                            SysLogger.addTableLog(
                                    "Cadastro Operações de Roteiro",
                                    TipoAcao.EDITAR,
                                    String.valueOf(selectedItem.getCodorg()),
                                    "Carregado tempos do roteiro " + selectedItem.getCodorg() + " da ref. " + selectedItem.getReferencia() + " manualmente.");
                        } catch (SQLException e) {
                            e.printStackTrace();
                            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                            GUIUtils.showException(e);
                        }
                        break;
                    }
                }
            });

            tblRoteiroOperacoes.refresh();
            tblRoteiroOperacoes.getSelectionModel().select(0);
        }
        GUIUtils.showMessage("Atualização dos tempos de operação finalizada, você pode salvar o roteiro.");
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) {
        unbindDataOperacoes(selectedItemOperacaoEdit);
        try {
            DAOFactory.getSdOperRoteiroOrganize001DAO().deleteByRoteiro(selectedItem);
            listOperacoes.forEach(operacao -> {
                try {
                    daoEquipamento.update(operacao.getId().getOperacao().getEquipamentoOrganize());
                    daoOperacao.update(operacao.getId().getOperacao());
                    DAOFactory.getSdOperRoteiroOrganize001DAO().update(operacao);
                } catch (SQLException ex) {
                    Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            });

            selectedItem.setAlteracaoMan(true);
            daoRoteiros.update(selectedItem);
            SysLogger.addTableLog(
                    "Cadastro Operações de Roteiro",
                    TipoAcao.EDITAR,
                    String.valueOf(selectedItem.getCodorg()),
                    "Alterado roteiro " + selectedItem.getCodorg() + " manualmente.");
            inEdition.set(false);
            tblRoteiroOperacoes.getSelectionModel().select(0);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
}
