/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.Log;
import sysdeliz2.models.sysdeliz.SdParadasTurno001;
import sysdeliz2.models.sysdeliz.SdTurno;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastroTurnoController implements Initializable {

    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveBreak = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdTurno> listRegisters = new SimpleListProperty();
    private final ListProperty<SdParadasTurno001> listParadasTurno = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdParadasTurno001> listParadasTurnoExclusao = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private SdTurno selectedItem = null;
    private ToggleSwitch tsActive = new ToggleSwitch(24, 55);
    private final ObjectProperty<SdParadasTurno001> paradaTurnoSelecionada = new SimpleObjectProperty<>(null);
    
    private final GenericDao<SdParadasTurno001> daoParadasTurno = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdParadasTurno001.class);

    // <editor-fold defaultstate="collapsed" desc="FXML Componentes">
    @FXML
    private TabPane tabMainPane;
    @FXML
    private Button btnAddRegister;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private TableView<SdTurno> tblRegisters;
    @FXML
    private TableColumn<SdTurno, SdTurno> clnActions;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxRegisterCode;
    @FXML
    private TextField tboxDescriptionCode;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private Pane pnToggleActive;
    @FXML
    private Label lbToggleActive;
    @FXML
    private TextField tboxBeginPeriod;
    @FXML
    private TextField tboxEndPeriod;
    @FXML
    private TableColumn<SdTurno, Boolean> clnActive;
    @FXML
    private TextField tboxInicioIntervalo;
    @FXML
    private TextField tboxFimIntervalo;
    @FXML
    private TextField tboxDescricaoParada;
    @FXML
    private Button btnSalvarParadaTurno;
    @FXML
    private VBox vboxHorario;
    @FXML
    private ListView<SdParadasTurno001> lviewParadasTurno;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxDefaultFilter);
        TextFieldUtils.upperCase(tboxDescriptionCode);
        TextFieldUtils.upperCase(tboxDescricaoParada);
        pnToggleActive.getChildren().add(tsActive);
        lbToggleActive.textProperty().bind(Bindings.when(tsActive.switchedOnProperty()).then("SIM").otherwise("NÃO"));

        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnSalvarParadaTurno.disableProperty().bind(disableBtnSaveBreak);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);

        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnSaveBreak.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));

        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));

        tboxDescricaoParada.editableProperty().bind(inEdition);
        tboxInicioIntervalo.editableProperty().bind(inEdition);
        tboxFimIntervalo.editableProperty().bind(inEdition);
        
        lviewParadasTurno.itemsProperty().bind(listParadasTurno);
        lviewParadasTurno.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindDataParada();
            bindDataParada(newValue);
            paradaTurnoSelecionada.set(newValue);
        });
        
        try {
            listRegisters.set(DAOFactory.getSdTurnoDAO().getAll());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        makeButtonsActions();
        makeColunsFactory();
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();
    }

    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdTurno, SdTurno>, ObservableValue<SdTurno>>() {
            @Override
            public ObservableValue<SdTurno> call(TableColumn.CellDataFeatures<SdTurno, SdTurno> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdTurno>() {
            @Override
            public int compare(SdTurno p1, SdTurno p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdTurno, SdTurno>, TableCell<SdTurno, SdTurno>>() {
            @Override
            public TableCell<SdTurno, SdTurno> call(TableColumn<SdTurno, SdTurno> btnCol) {
                return new TableCell<SdTurno, SdTurno>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");

                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(SdTurno seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);

                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    btnDeleteRegisterOnAction(null);
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }

        });

    }

    private void makeColunsFactory() {
        clnActive.setCellFactory(column -> {
            TableCell<SdTurno, Boolean> cell = new TableCell<SdTurno, Boolean>() {

                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item ? "S" : "N");
                    }
                }
            };

            return cell;
        });
    }

    private void clearForm() {
        tboxRegisterCode.clear();
        tboxDescriptionCode.clear();
        tboxBeginPeriod.clear();
        tboxEndPeriod.clear();
        
        tboxFimIntervalo.clear();
        tboxInicioIntervalo.clear();
        tboxDescricaoParada.clear();
        listParadasTurno.clear();
        listParadasTurnoExclusao.clear();
    }

    private void bindData(SdTurno selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bind(selectItem.descricaoProperty());
            tboxBeginPeriod.textProperty().bind(selectItem.inicioTurnoProperty());
            tboxEndPeriod.textProperty().bind(selectItem.fimTurnoProperty());
            tsActive.switchedOnProperty().bind(selectItem.ativoProperty());
            
            try {
                listParadasTurno.set(daoParadasTurno.initCriteria().addPredicateEq("turno",selectItem.getCodigo()).loadListByPredicate());
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }

    private void unbindData() {
        tboxRegisterCode.textProperty().unbind();
        tboxDescriptionCode.textProperty().unbind();
        tboxBeginPeriod.textProperty().unbind();
        tboxEndPeriod.textProperty().unbind();
        tsActive.switchedOnProperty().unbind();
    
        clearForm();
    }
    
    private void bindDataParada(SdParadasTurno001 paradaSelecionada){
        if (paradaSelecionada != null) {
            tboxInicioIntervalo.setText(paradaSelecionada.getInicio().format(DateTimeFormatter.ofPattern("HH:mm")));
            tboxFimIntervalo.setText(paradaSelecionada.getFim().format(DateTimeFormatter.ofPattern("HH:mm")));
            tboxDescricaoParada.setText(paradaSelecionada.getDescricao());
        }
    }
    
    private void unbindDataParada(){
        tboxInicioIntervalo.clear();
        tboxFimIntervalo.clear();
        tboxDescricaoParada.clear();
    }

    private void bindBidirectionalData(SdTurno selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
            tboxDescriptionCode.textProperty().bindBidirectional(selectItem.descricaoProperty());
            tboxBeginPeriod.textProperty().bindBidirectional(selectItem.inicioTurnoProperty());
            tboxEndPeriod.textProperty().bindBidirectional(selectItem.fimTurnoProperty());
            tsActive.switchedOnProperty().bindBidirectional(selectItem.ativoProperty());
    
            try {
                listParadasTurno.set(daoParadasTurno.initCriteria().addPredicateEq("turno",selectItem.getCodigo()).loadListByPredicate());
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }

    private void unbindBidirectionalData(SdTurno selectItem) {
        if (selectItem != null) {
            tboxRegisterCode.textProperty().unbindBidirectional(selectItem.codigoProperty());
            tboxDescriptionCode.textProperty().unbindBidirectional(selectItem.descricaoProperty());
            tboxBeginPeriod.textProperty().unbindBidirectional(selectItem.inicioTurnoProperty());
            tboxEndPeriod.textProperty().unbindBidirectional(selectItem.fimTurnoProperty());
            tsActive.switchedOnProperty().unbindBidirectional(selectItem.ativoProperty());
        }
    }

    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdTurno();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);

        tabMainPane.getSelectionModel().select(1);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(DAOFactory.getSdTurnoDAO().getByDefault(tboxDefaultFilter.textProperty().get()));
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                for (SdParadasTurno001 parada : listParadasTurno) {
                    daoParadasTurno.delete(parada);
                }
                DAOFactory.getSdTurnoDAO().delete(selectedItem);
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EXCLUIR,
                        "Cadastro Turno",
                        selectedItem.getCodigo() + "",
                        "Excluído o turno " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) throws InterruptedException {
        try {
            ValidationForm.validationEmpty(tboxDescriptionCode);
        } catch (FormValidationException ex) {
            return;
        }

        unbindBidirectionalData(selectedItem);
        SdTurno objectEdited = selectedItem;
        try {
            if (selectedItem.getCodigo() == 0) {
                DAOFactory.getSdTurnoDAO().save(selectedItem);
                for(SdParadasTurno001 parada : listParadasTurno){
                    parada.setTurno(selectedItem.getCodigo());
                    daoParadasTurno.save(parada);
                }
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.CADASTRAR,
                        "Cadastro Turno",
                        selectedItem.getCodigo() + "",
                        "Cadastrado o turno " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
                if (!listRegisters.contains(selectedItem)) {
                    listRegisters.add(selectedItem);
                }
            } else {
                DAOFactory.getSdTurnoDAO().update(selectedItem);
                for(SdParadasTurno001 parada : listParadasTurno){
                    parada.setTurno(selectedItem.getCodigo());
                    daoParadasTurno.update(parada);
                }
                for(SdParadasTurno001 parada : listParadasTurnoExclusao){
                    daoParadasTurno.delete(parada);
                }
                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR,
                        "Cadastro Turno",
                        selectedItem.getCodigo() + "",
                        "Alterado o turno " + selectedItem.getDescricao() + " [" + selectedItem.getCodigo() + "]"
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        inEdition.set(false);
        tblRegisters.getSelectionModel().select(objectEdited);
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }

    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdTurno();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        tboxDescriptionCode.requestFocus();
    }

    @FXML
    private void tboxInicioIntervaloOnKeyPressed(KeyEvent event) {
        if(event.isControlDown() && event.getCode() == KeyCode.N && inEdition.get()){
            paradaTurnoSelecionada.set(null);
            unbindDataParada();
        }
    }

    @FXML
    private void btnSalvarParadaTurnoOnAction(ActionEvent event) {
        // <editor-fold defaultstate="collapsed" desc="Validação">
        if(tboxInicioIntervalo.getText().isEmpty()){
            GUIUtils.showMessage("O campo início da parada não pode ser em branco.");
            return;
        }
    
        if(tboxFimIntervalo.getText().isEmpty()){
            GUIUtils.showMessage("O campo fim da parada não pode ser em branco.");
            return;
        }
    
        if(tboxDescricaoParada.getText().isEmpty()){
            GUIUtils.showMessage("O campo descrição da parada não pode ser em branco.");
            return;
        }
    
        LocalDateTime inicio = LocalDateTime.now();
        LocalDateTime fim = LocalDateTime.now();
    
        try {
            inicio = LocalDateTime.of(LocalDate.of(1900, 1, 1), LocalTime.parse(tboxInicioIntervalo.getText(), DateTimeFormatter.ofPattern("HH:mm")));
            fim = LocalDateTime.of(LocalDate.of(1900, 1, 1), LocalTime.parse(tboxFimIntervalo.getText(), DateTimeFormatter.ofPattern("HH:mm")));
        }catch (DateTimeParseException ex){
            GUIUtils.showMessage("Os valores digitados para hora de início e fim da parada estão fora do padrão. Verifique os valores digitados.");
            return;
        }
        
        if(inicio.isAfter(fim)){
            GUIUtils.showMessage("A hora de início do intervalo não pode ser menor que a hora fim da parada.");
            return;
        }
        //</editor-fold>
        
        if(paradaTurnoSelecionada.get() == null) {
            SdParadasTurno001 parada = new SdParadasTurno001(inicio, fim, tboxDescricaoParada.getText(), (int) ChronoUnit.MINUTES.between(inicio, fim));
            listParadasTurno.add(parada);
            paradaTurnoSelecionada.set(parada);
        }else {
            paradaTurnoSelecionada.get().setInicio(inicio);
            paradaTurnoSelecionada.get().setFim(fim);
            paradaTurnoSelecionada.get().setDescricao(tboxDescricaoParada.getText());
            paradaTurnoSelecionada.get().setTempoIntervalo((int) ChronoUnit.MINUTES.between(inicio, fim));
        }
        
    }

    @FXML
    private void listParadasTurnoOnMouseClicked(MouseEvent event) {
        if(event.getClickCount() == 2 && inEdition.get()){
            SdParadasTurno001 paradaSelecionada = lviewParadasTurno.getSelectionModel().getSelectedItem();
            listParadasTurnoExclusao.add(paradaSelecionada);
            listParadasTurno.remove(paradaSelecionada);
        }
    }

}
