package sysdeliz2.controllers.fxml.cadastros.base;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.utils.StaticButtonBuilder;

/**
 * @author lima.joao
 * @since 23/08/2019 16:52
 */
public class SceneCadastroBase<T> {
    // Titulo da tela usado para logs
    protected String titulo = "Não definido";

    // Objeto em foco
    protected T selectedItem;

    // Lista de objetos
    protected ObservableList<T> lstRegistros;

    // Dao generico
    protected GenericDao<T> genericDao;

    // Variáveis de controle
    protected final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    protected final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    protected final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    protected final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    protected final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    protected final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    protected final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    protected final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    protected final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);

    protected boolean isBindBiDirectional = false;

    //<editor-fold desc="Variáveis FXML">
    @FXML
    protected TabPane tabMainPane;
    @FXML
    protected TableView<T> tblRegisters;
    @FXML
    protected Label lbRegistersCount;
    @FXML
    protected Button btnFirstRegister;
    @FXML
    protected Button btnPreviusRegister;
    @FXML
    protected Button btnNextRegister;
    @FXML
    protected Button btnLastRegister;
    @FXML
    protected Button btnAddRegister2;
    @FXML
    protected Button btnEditRegister;
    @FXML
    protected Button btnDeleteRegister;
    @FXML
    protected Button btnSave;
    @FXML
    protected Button btnCancel;
    @FXML
    protected Button btnReturn;
    @FXML
    @SuppressWarnings("unused")
    protected TableColumn<T, T> clnActions;
    @FXML
    @SuppressWarnings("unused")
    protected TextField tboxDefaultFilter;
    //</editor-fold>

    protected void bindDados() {
    }

    protected void unbindDados(){
    }

    protected void makeButtonsActions(boolean makeButtonInfo, boolean makeButtonEdit, boolean makeButtonDelete) {
        clnActions.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue()));
        clnActions.setCellFactory(param -> new TableCell<T, T>() {

            final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
            final Button btnEditRow = StaticButtonBuilder.genericEditButton();
            final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();

            final HBox boxButtonsRow = new HBox(3);

            @Override
            protected void updateItem(T seletedRow, boolean empty) {
                super.updateItem(seletedRow, empty);

                if (seletedRow != null) {
                    boxButtonsRow.getChildren().clear();

                    if(makeButtonInfo){
                        boxButtonsRow.getChildren().add(btnInfoRow);
                    }

                    if(makeButtonEdit){
                        boxButtonsRow.getChildren().add(btnEditRow);
                    }

                    if(makeButtonDelete){
                        boxButtonsRow.getChildren().add(btnDeleteRow);
                    }

                    setGraphic(boxButtonsRow);

                    if(makeButtonInfo){
                        btnInfoRow.setOnAction(event -> {
                            selectedItem = seletedRow;
                            isBindBiDirectional = false;
                            bindDados();

                            tabMainPane.getSelectionModel().select(1);
                        });
                    }

                    if(makeButtonEdit){
                        btnEditRow.setOnAction(event -> {
                            selectedItem = seletedRow;
                            tabMainPane.getSelectionModel().select(1);
                            btnEditRegisterOnAction(null);
                        });
                    }

                    if(makeButtonDelete){
                        btnDeleteRow.setOnAction(event -> {
                            selectedItem = seletedRow;
                            btnDeleteRegisterOnAction(null);
                        });
                    }
                } else {
                    setText(null);
                    setGraphic(null);
                }
            }
        });
    }

    /**
     * Implementa o método base para deleção visto ser necessário executar no makeButtonsActions
     * @param actionEvent algo
     */
    protected void btnEditRegisterOnAction(ActionEvent actionEvent) {}

    /**
     * Implementa o método base para deleção visto ser necessário executar no makeButtonsActions
     * @param actionEvent algo
     */
    protected void btnDeleteRegisterOnAction(ActionEvent actionEvent){}

    @FXML
    protected void btnCancelOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            unbindDados();
            inEdition.set(false);
            tblRegisters.getSelectionModel().selectFirst();
            bindDados();

            actionEvent.consume();
        }
    }

    @FXML
    protected void btnReturnOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            tabMainPane.getSelectionModel().select(0);
            actionEvent.consume();
        }
    }

    protected void btnAddRegisterOnAction(ActionEvent actionEvent) {}

    protected void btnFindDefaultOnAction(ActionEvent actionEvent) {}

    @FXML
    protected void btnFirstRegisterOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            tblRegisters.getSelectionModel().selectFirst();
            actionEvent.consume();
        }
    }

    @FXML
    protected void btnPreviusRegisterOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            tblRegisters.getSelectionModel().selectPrevious();
            actionEvent.consume();
        }
    }

    @FXML
    protected void btnNextRegisterOnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            tblRegisters.getSelectionModel().selectNext();
            actionEvent.consume();
        }
    }

    @FXML
    protected void btnLastRegisterOnAtion(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            tblRegisters.getSelectionModel().selectLast();
            actionEvent.consume();
        }
    }

    @FXML
    protected void btnAddRegister2OnAction(ActionEvent actionEvent) {
        if (!actionEvent.isConsumed()) {
            btnAddRegisterOnAction(actionEvent);
            actionEvent.consume();
        }
    }

}
