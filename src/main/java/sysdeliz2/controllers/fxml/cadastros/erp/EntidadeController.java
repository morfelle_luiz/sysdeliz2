package sysdeliz2.controllers.fxml.cadastros.erp;

import com.dansoftware.pdfdisplayer.PDFDisplayer;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.client.HttpResponseException;
import org.controlsfx.control.textfield.TextFields;
import org.jetbrains.annotations.NotNull;
import sysdeliz.apis.web.convertr.data.models.customers.AdditionalAttribute;
import sysdeliz.apis.web.convertr.data.models.customers.Address;
import sysdeliz.apis.web.convertr.data.models.customers.Customer;
import sysdeliz.apis.web.convertr.data.models.customers.CustomerStatus;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.cadastros.SdEntidadeCondicao;
import sysdeliz2.models.sysdeliz.cadastros.SdEntidadeRedesSociais;
import sysdeliz2.models.sysdeliz.cadastros.SdEntidadeTipoPagto;
import sysdeliz2.models.sysdeliz.cadastros.SdTipoPagto;
import sysdeliz2.models.sysdeliz.comercial.SdIndicadorComercial;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosPedido;
import sysdeliz2.models.view.VSdExistCodcli;
import sysdeliz2.models.view.comercial.VSdMetasEntidade;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.apis.nitroecom.ServicoNitroEcom;
import sysdeliz2.utils.apis.nitroecom.models.RequestClienteNitro;
import sysdeliz2.utils.apis.nitroecom.models.RequestSendCliente;
import sysdeliz2.utils.apis.receitaws.ServicoReceitaWS;
import sysdeliz2.utils.apis.receitaws.models.ReceitaWS;
import sysdeliz2.utils.apis.viacep.ServicoViaCEP;
import sysdeliz2.utils.apis.viacep.models.CEP;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.gui.window.GenericSelect;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.NoResultException;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lima.joao
 * @since 21/08/2019 17:37
 */
public class EntidadeController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    GenericDao<Entidade> daoEntidade = new GenericDaoImpl<>(Entidade.class);
    GenericDao<SdTipoContato001> daoTipoContato = new GenericDaoImpl<>(SdTipoContato001.class);
    GenericDao<SdPagadorFrete001> daoPagadorFrete = new GenericDaoImpl<>(SdPagadorFrete001.class);
    GenericDao<CadCep> daoCadCep = new GenericDaoImpl<>(CadCep.class);
    GenericDao<Contato> daoContato = new GenericDaoImpl<>(Contato.class);
    GenericDao<SdConcorrentesMarcaEnt001> daoConcorrentesMarcaEntidade = new GenericDaoImpl<>(SdConcorrentesMarcaEnt001.class);
    GenericDao<SdMarcasEntidade> daoMarcasEntidade = new GenericDaoImpl<>(SdMarcasEntidade.class);
    GenericDao<SdTipoTributacao001> daoTipoTributacao = new GenericDaoImpl<>(SdTipoTributacao001.class);
    GenericDao<Cidade> daoCidade = new GenericDaoImpl<>(Cidade.class);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Variáveis Controle">
    private Set<String> sugestoesRazaoSocial = null;
    private Set<String> sugestoesCnpjCepf = null;
    private Set<String> sugestoesCodigo = null;
    private final ObjectProperty<Entidade> entidadeSelecionada = new SimpleObjectProperty<>(null);
    private final ObjectProperty<Entidade> entidadeOriginal = new SimpleObjectProperty<>(null);
    private final ObjectProperty<SdEntidade> sdEntidadeOriginal = new SimpleObjectProperty<>(null);
    //novo cadastro
    private SdEntidade complementoNovaEntidade = null;
    private List<SdMarcasEntidade> marcasNovaEntidade = new ArrayList<>();
    private List<Contato> contatosNovaEntidade = new ArrayList<>();
    //tela
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty novaEntidade = new SimpleBooleanProperty(false);
    private final BooleanProperty sendEcommerce = new SimpleBooleanProperty(false);
    private List<String> marcaB2bEntidade = new ArrayList<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Controles Navegação">
    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    private final ListProperty<Entidade> entidades = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Map<String, String>> anexos = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdEntidadeRedesSociais> redesSociais = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdEntidadeCondicao> condicoesB2b = new SimpleListProperty<>();
    private final ListProperty<SdEntidadeTipoPagto> tiposPagtoB2b = new SimpleListProperty<>();
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declaração: Componentes FXML">
    // <editor-fold defaultstate="collapsed" desc="Listagems Componentes">
    @FXML
    private TabPane tabPaneRoot;
    @FXML
    public TabPane tabPaneCadastro;
    @FXML
    public Tab tabMarcas;
    // <editor-fold defaultstate="collapsed" desc="Filter Componentes">
    @FXML
    private TextField tboxFiltroRazao;
    @FXML
    private TextField tboxFiltroFantasia;
    @FXML
    private TextField tboxFiltroTipo;
    @FXML
    private Button btnProcurarFiltroTipo;
    @FXML
    private TextField tboxFiltroCidade;
    @FXML
    private Button btnProcurarFiltroCidade;
    @FXML
    private ComboBox<String> cboxFiltroUf;
    @FXML
    private TextField tboxFiltroRepresentante;
    @FXML
    private Button btnProcurarFiltroRepresentante;
    @FXML
    private TextField tboxFiltroCnpj;
    @FXML
    private TextField tboxFiltroMarca;
    @FXML
    private Button btnProcurarFiltroMarca;
    @FXML
    private TextField tboxFiltroGrupo;
    @FXML
    private Button btnProcurarFiltroGrupo;
    @FXML
    private CheckBox cboxFiltroJuridica;
    @FXML
    private CheckBox cboxFiltroFisica;
    @FXML
    private CheckBox cboxFiltroCliente;
    @FXML
    private CheckBox cboxFiltroFornecedor;
    @FXML
    private CheckBox cboxFiltroTerceiro;
    @FXML
    private CheckBox cboxFiltroAtivo;
    @FXML
    private CheckBox cboxFiltroInativo;
    @FXML
    private CheckBox cboxFiltroBloqueado;
    @FXML
    private CheckBox cboxFiltroLiberado;
    @FXML
    private CheckBox cboxFiltroBloqueadoB2B;
    @FXML
    private CheckBox cboxFiltroLiberadoB2B;
    @FXML
    private CheckBox cboxFiltroSincronizadoB2B;
    @FXML
    private CheckBox cboxFiltroNaoSincronizadoB2B;
    @FXML
    private DatePicker tboxFiltroDtInicioDe;
    @FXML
    private DatePicker tboxFiltroDtCadastroAte;
    @FXML
    private Button btnCarregarEntidades;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private VBox boxCadastroRevisadoDataPedido;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table Componentes">
    @FXML
    private TableView<Entidade> tblEntidades;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private TableColumn<Entidade, Entidade> clnEntAcoes;
    @FXML
    private TableColumn<Entidade, String> clnEntCodigo;
    @FXML
    private TableColumn<Entidade, String> clnEntRazao;
    @FXML
    private TableColumn<Entidade, String> clnEntFantasia;
    @FXML
    private TableColumn<Entidade, String> clnEntCnpj;
    @FXML
    private TableColumn<Entidade, String> clnEntInscricao;
    @FXML
    private TableColumn<Entidade, String> clnEntEntidade;
    @FXML
    private TableColumn<Entidade, SitCli> clnEntTipo;
    @FXML
    private TableColumn<Entidade, Boolean> clnEntAtivo;
    @FXML
    private TableColumn<Entidade, Boolean> clnEntBloqueio;
    @FXML
    private TableColumn<Entidade, SdEntidade> clnEntOrigem;
    @FXML
    private TableColumn<Entidade, String> clnEntTelefone;
    @FXML
    private TableColumn<Entidade, String> clnEntCelular;
    @FXML
    private TableColumn<Entidade, String> clnEntEmail;
    @FXML
    private TableColumn<Entidade, CadCep> clnEntCidade;
    @FXML
    private TableColumn<Entidade, CadCep> clnEntUf;
    @FXML
    private TableColumn<Entidade, SdEntidade> clnEntSincB2b;
    @FXML
    private TableColumn<Entidade, SdEntidade> clnEntLiberadoB2b;
    @FXML
    private Label lbContadorRegistros;
    //</editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Manutenção Componentes">
    // <editor-fold defaultstate="collapsed" desc="Navegação">
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnAddRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnReturn;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Cadastro">
    @FXML
    private TextField tboxCode;
    @FXML
    private TextField tboxCnpj;
    @FXML
    private TextField tboxRazaoSocial;
    @FXML
    private Button btnImportarDadosReceita;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Cadastro: Contato">
    @FXML
    private TextField tboxNomeContatoEntidade;
    @FXML
    private TextField tboxTelefoneContatoEntidade;
    @FXML
    private TextField tboxEmailContatoEntidade;
    @FXML
    private DatePicker tboxDataNascContatoEntidade;
    @FXML
    private TextField tboxObservacaoContatoEntidade;
    @FXML
    private ComboBox<SdTipoContato001> cboxTipoContatoEntidade;
    @FXML
    private Button btnSalvarContato;
    @FXML
    private TableView<Contato> tblContatos;
    @FXML
    private TableColumn<Contato, LocalDate> clnDtNascContatoEntidade;
    @FXML
    private TableColumn<Contato, SdTipoContato001> clnTipoContatoEntidade;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Cadastro: Observações">
    @FXML
    private TextArea tboxObservacaoComercial;
    @FXML
    private TextArea tboxObservacaoFinanceiro;
    @FXML
    private TextArea tboxObservacaoNfe;
    // </editor-fold>
    @FXML
    private VBox boxDadosGerais;
    @FXML
    private VBox boxAnexos;
    @FXML
    private VBox boxMarcas;
    @FXML
    private VBox boxRedesSociais;
    @FXML
    private VBox boxB2b;
    @FXML
    private VBox boxMetasComerciais;
    @FXML
    private HBox dadosGeraisBoxCabecalho;
    @FXML
    private HBox dadosGeraisAcessoEntidade;
    @FXML
    private VBox boxInformacoesAuxiliares;
    @FXML
    private VBox boxContato;
    @FXML
    private VBox boxObservacoes;
    // </editor-fold>
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Componentes Extras">
    // <editor-fold defaultstate="collapsed" desc="BOX FILTROS">
    private final FormFieldCheckBoxGroup cboxGroupStatusPedido = FormFieldCheckBoxGroup.create(cb -> {
        cb.withoutTitle();
        cb.items(
                cb.option("Liberado Comercial", "C1", true),
                cb.option("Liberado Financeiro", "F1", false),
                cb.option("Bloqueado Comercial", "C0", false),
                cb.option("Bloqueado Financeiro", "F0", false)
        );
        cb.getBox().setPadding(Insets.EMPTY);
    });
    private final FormFieldDatePeriod dataPedidoFilter = FormFieldDatePeriod.create(per -> {
        per.withoutTitle();
        per.verticalMode();
        per.withLabels();
    });

    private final FormFieldSegmentedButton segBtnRevisaoCadastro = FormFieldSegmentedButton.create(btn -> {
        btn.title("Cadastro Revisado");
        btn.options(
                btn.option("Sim", 1, FormFieldSegmentedButton.Style.SUCCESS),
                btn.option("Não", 2, FormFieldSegmentedButton.Style.WARNING),
                btn.option("Ambos", 3, FormFieldSegmentedButton.Style.PRIMARY)
        );
        btn.select(2);
    });

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX MARCAS">
    private final TabPane marcasTabPane = new TabPane();
    private final FormFieldSingleFind<Marca> marcaEntidadeField = FormFieldSingleFind.create(Marca.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Marca");
        field.width(280.0);
    });
    private final Button adicionarMarcaEntidadeButton = FormButton.create(formButton -> {
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/new register (2).png").toExternalForm()));
        formButton.addStyle("success");
        formButton.addStyle("lg");
        formButton.disable.bind(emEdicao.not());
        formButton.setAction(event -> {
            if (marcaEntidadeField.value.getValue().getCodigo() != null && marcaEntidadeField.value.getValue().getCodigo().length() > 0) {
                addMarcaEntidade(new SdMarcasEntidade(entidadeSelecionada.get().getCodcli(), marcaEntidadeField.value.getValue()));
                marcaEntidadeField.clear();
            } else {
                MessageBox.create(messageBox -> {
                    messageBox.message("Você deve selecionar uma marca para adicionar.");
                    messageBox.type(MessageBox.TypeMessageBox.IMPORTANT);
                }).showAndWait();
            }
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX INFORMAÇÕES AUXILIARES">
    private final FormFieldSingleFind<Natureza> naturezaField = FormFieldSingleFind.create(Natureza.class, field -> {
        field.editable.bind(emEdicao);
        field.codeReference.set("natureza");
        field.title("Natureza");
        field.width(500.0);
    });
    private final FormFieldDate dataFundacaoField = FormFieldDate.create(field -> field.editable.bind(emEdicao)).title("Dt. Fundação");
    private final FormFieldSingleFind<Subgrupo> classeGerencialField = FormFieldSingleFind.create(Subgrupo.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Classe Ger.");
        field.width(400.0);
    });
    private final FormFieldText inscricaoStField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Inscrição ST/Mun.").width(120.0);
    private final FormFieldSingleFind<Histcp> historicoField = FormFieldSingleFind.create(Histcp.class, field -> {
        field.editable.bind(emEdicao);
        field.codeReference.set("historico");
        field.title("Histórico");
        field.width(400.0);
    });
    private final FormFieldComboBox<SdTipoTributacao001> tipoTributacaoField = FormFieldComboBox.create(SdTipoTributacao001.class, combobox -> {
        combobox.title("Tributação");
        combobox.editable.bind(emEdicao);
        try {
            combobox.items(daoTipoTributacao.list());
            combobox.select(0);
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }).width(350.0);
    private final FormFieldSingleFind<TabTran> transportadoraField = FormFieldSingleFind.create(TabTran.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Transportadora");
        field.width(400.0);
    });
    private final FormFieldComboBox<SdPagadorFrete001> pagadorTransportadoraField = FormFieldComboBox.create(SdPagadorFrete001.class, field -> field.editable.bind(emEdicao)).title("Pagador").width(70.0);
    private final FormFieldSingleFind<TabTran> redespachoField = FormFieldSingleFind.create(TabTran.class, field -> {
        field.disable.bind(emEdicao.not());
        field.title("Redespacho");
        field.width(300.0);
    });
    private final FormFieldComboBox<SdPagadorFrete001> pagadorRedespachoField = FormFieldComboBox.create(SdPagadorFrete001.class, field -> field.editable.bind(emEdicao)).title("Pagador").width(70.0);
    private final FormFieldSingleFind<CadBan> bancoField = FormFieldSingleFind.create(CadBan.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Banco");
        field.width(400.0);
    });
    private final FormFieldText contaField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Conta").width(100.0);
    private final FormFieldText agenciaField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Agencia").width(70.0);
    private final FormFieldSingleFind<TabSit> situacaoDuplicataField = FormFieldSingleFind.create(TabSit.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Situação Duplicata/Título");
        field.width(280.0);
    });
    private final FormFieldSingleFind<Regiao> tablePrecoField = FormFieldSingleFind.create(Regiao.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Tabela Preço");
        field.width(270.0);
    });
    private final FormFieldSingleFind<SdCondicao001> condicaoPagamentoField = FormFieldSingleFind.create(SdCondicao001.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Cond. Pagamento");
        field.width(250.0);
    });
    private final FormFieldText descontoField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Desconto (%)").width(100.0);
    private final FormFieldText diasParaEntregaField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Dias para Entrega").width(120.0);
    private final FormFieldText suframaField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Suframa").width(100.0);
    private final FormFieldSingleFind<ContaCont> contaClienteField = FormFieldSingleFind.create(ContaCont.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Conta Cliente");
        field.width(400.0);
    });
    private final FormFieldSingleFind<ContaCont> contaFornecedorField = FormFieldSingleFind.create(ContaCont.class, field -> {
        field.editable.bind(emEdicao);
        field.title("Conta Fornecedor");
        field.width(400.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX ANEXOS">
    FormBox boxViewAnexo = FormBox.create();
    FormTableView<Map<String, String>> tblAnexos = FormTableView.create(Map.class, table -> {
        table.title("Documentos");
        table.items.bind(anexos);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            boxViewAnexo.clear();
            if (newValue != null) {
                try {
                    loadFileAnexo((Map<String, String>) newValue);
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });
        table.addColumn(FormTableColumn.create(column -> {
            column.title("Nome");
            column.width(500.0);
            column.value((Callback<TableColumn.CellDataFeatures<Map<String, String>, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().get("nome")));
        }));
        table.addColumn(FormTableColumn.create(column -> {
            column.title("Data");
            column.width(70);
            column.value((Callback<TableColumn.CellDataFeatures<Map<String, String>, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().get("data")));
        }));
        table.addColumn(FormTableColumn.create(column -> {
            column.title("Tamanho");
            column.width(110);
            column.value((Callback<TableColumn.CellDataFeatures<Map<String, String>, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().get("tamanho")));
        }));
        table.addColumn(FormTableColumn.create(column -> {
            column.title("Tipo");
            column.width(100);
            column.value((Callback<TableColumn.CellDataFeatures<Map<String, String>, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().get("tipo")));
        }));
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX DADOS GERAIS">
    private final FormFieldText bairroEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Bairro").width(180.0).toUpper();
    private final FormFieldText complementoEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Complemento").width(350.0).toUpper();
    private final FormFieldText numeroEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Número").width(70.0).toUpper();
    private final FormFieldText enderecoEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Endereço").width(400.0).toUpper();
    private final FormFieldText cidadeEntidadeField = FormFieldText.create().title("Cidade").width(250.0).editable(false);
    private final FormFieldText ufEntidadeField = FormFieldText.create().title("UF").width(50.0).editable(false);
    private final FormFieldSingleFind<Pais> paisEntidadeField = FormFieldSingleFind.create(Pais.class, field -> {
        field.editable.bind(emEdicao);
        field.codeReference.set("codPais");
        field.tabForm.set(enderecoEntidadeField.build());
        field.title("País");
    });
    private final FormFieldMultipleFind<CadCep> cepEntidadeField = FormFieldMultipleFind.create(CadCep.class, cepEntidade -> {
        cepEntidade.title("CEP");
        cepEntidade.width(180.0);
        cepEntidade.postSelected((observable, oldValue, newValue) -> {
            if (cepEntidade.objectValues.size() > 0) {
                cidadeEntidadeField.value.set(((CadCep) cepEntidade.objectValues.get(0)).getCidade().getNomeCid());
                ufEntidadeField.value.set(((CadCep) cepEntidade.objectValues.get(0)).getCidade().getCodEst().getId().getSiglaEst());
                paisEntidadeField.value.set(((CadCep) cepEntidade.objectValues.get(0)).getCidade().getCodEst().getCodPais());
                enderecoEntidadeField.value.set(((CadCep) cepEntidade.objectValues.get(0)).getLogrCep() != null ? ((CadCep) cepEntidade.objectValues.get(0)).getLogrCep() : entidadeSelecionada.get().getEndereco() != null ? entidadeSelecionada.get().getEndereco() : null);
                bairroEntidadeField.value.set(((CadCep) cepEntidade.objectValues.get(0)).getBairro() != null ? ((CadCep) cepEntidade.objectValues.get(0)).getBairro() : entidadeSelecionada.get().getBairro() != null ? entidadeSelecionada.get().getBairro() : null);
            }
        });
        cepEntidade.limit(8);
        cepEntidade.editable.bind(emEdicao);
        cepEntidade.keyReleased(event -> {
            KeyEvent keyEvent = (KeyEvent) event;
            if (cepEntidade.editable.get())
                if (keyEvent.getCode() == KeyCode.F4)
                    cepEntidade.findOnAction(null);
                else if (keyEvent.getCode() == KeyCode.ENTER || keyEvent.getCode() == KeyCode.TAB || cepEntidade.textValue.length().get() == 8) {
                    try {
                        CadCep cep = (CadCep) daoCadCep.initCriteria().addPredicate("cep", cepEntidade.textValue.getValue(), PredicateType.EQUAL).loadEntityByPredicate();
                        if (cep != null)
                            cepEntidade.value(cep);
                        else {
                            CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(cepEntidade.textValue.getValue());
                            if (viaCep.getCep() != null) {
                                CadCep novoCep = new CadCep();
                                novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                                novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                                novoCep.setBairro(viaCep.getBairro());
                                novoCep.setLogrCep(viaCep.getLogradouro());
                                daoCadCep.save(novoCep);
                                cepEntidade.value(novoCep);
                            } else {
                                MessageBox.create(message -> {
                                    message.message("CEP digitado é inválido ou não existente.");
                                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                                    message.showAndWait();
                                });
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                }
        });
    });

    private final FormFieldSingleFind<GrupoCli> grupoEntidadeField = FormFieldSingleFind.create(GrupoCli.class, field -> {
        field.editable.bind(emEdicao);
        field.tabForm.set(cepEntidadeField.build());
        field.title("Grupo");
        field.width(280.0);
    });
    private final FormFieldSingleFind<SitCli> sitCliEntidadeField = FormFieldSingleFind.create(SitCli.class, field -> {
        field.editable.bind(emEdicao);
        field.tabForm.set(grupoEntidadeField.build());
        field.title("Tipo");
        field.width(220.0);
    });
    private final FormFieldSingleFind<Atividade> ramoAtividadeEntidadeField = FormFieldSingleFind.create(Atividade.class, field -> {
        field.editable.bind(emEdicao);
        field.tabForm.set(sitCliEntidadeField.build());
        field.title("Ramo Atividade");
        field.width(300.0);
    });
    private final FormFieldText creditoEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Crédito").width(150.0).mask(FormFieldText.Mask.MONEY).label("R$");
    private final FormFieldText emailEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("E-mail").width(250.0).mask(FormFieldText.Mask.EMAIL).label("@");
    private final FormFieldText faxEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Fax/Celular").width(150.0).mask(FormFieldText.Mask.PHONE);
    private final FormFieldText telefoneEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Telefone").width(150.0).mask(FormFieldText.Mask.PHONE);
    private final FormFieldText inscricaoEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Insc. Estadual").width(200.0).toUpper();
    private final FormFieldText fantasiaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Fantasia").width(430.0).toUpper();
    private final FormFieldDate dtCadastroEntidadeField = FormFieldDate.create(field -> field.editable.bind(emEdicao)).title("Cadastro").defaultValue(LocalDate.now());

    private final FormFieldText cidadeEntregaEntidadeField = FormFieldText.create().title("Cidade").width(250.0).editable(false);
    private final FormFieldText ufEntregaEntidadeField = FormFieldText.create().title("UF").width(35.0).editable(false);
    private final FormFieldText enderecoEntregaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Endereço").width(400.0).toUpper();
    private final FormFieldText numeroEntregaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Número").width(70.0).toUpper();
    private final FormFieldText bairroEntregaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Bairro").width(180.0).toUpper();
    private final FormFieldText complementoEntregaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Complemento").width(250.0).toUpper();
    private final FormFieldText cnpjEntregaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("CNPJ").width(150.0);
    private final FormFieldText inscricaoEntregaEntidadeField = FormFieldText.create(field -> {
        field.editable.bind(emEdicao);
        field.focusedListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                String cnpjWithoutFormat = StringUtils.unformatCpfCnpj(field.value.get());
                field.value.set(StringUtils.formatCpfCnpj(cnpjWithoutFormat));
            }
        });
    }).title("Inscrição").width(120.0).toUpper();
    private final FormFieldMultipleFind<CadCep> cepEntregaEntidadeField = FormFieldMultipleFind.create(CadCep.class, cepEntrega -> {
        cepEntrega.title("CEP");
        cepEntrega.width(180.0);
        cepEntrega.editable.bind(emEdicao);
        cepEntrega.postSelected((observable, oldValue, newValue) -> {
            if (cepEntrega.objectValues.size() > 0) {
                cidadeEntregaEntidadeField.value.set(((CadCep) cepEntrega.first()).getCidade().getNomeCid());
                ufEntregaEntidadeField.value.set(((CadCep) cepEntrega.first()).getCidade().getCodEst().getId().getSiglaEst());
                enderecoEntregaEntidadeField.value.set(((CadCep) cepEntrega.objectValues.get(0)).getLogrCep() != null ? ((CadCep) cepEntrega.objectValues.get(0)).getLogrCep() : entidadeSelecionada.get().getEndEnt() != null ? entidadeSelecionada.get().getEndEnt() : null);
                bairroEntregaEntidadeField.value.set(((CadCep) cepEntrega.objectValues.get(0)).getBairro() != null ? ((CadCep) cepEntrega.objectValues.get(0)).getBairro() : entidadeSelecionada.get().getBairroEnt() != null ? entidadeSelecionada.get().getBairroEnt() : null);
            }
        });
        cepEntrega.limit(8);
        cepEntrega.keyReleased(event -> {
            KeyEvent keyEvent = (KeyEvent) event;
            if (cepEntrega.editable.get())
                if (keyEvent.getCode() == KeyCode.F4)
                    cepEntrega.findOnAction(null);
                else if (keyEvent.getCode() == KeyCode.ENTER || keyEvent.getCode() == KeyCode.TAB || cepEntrega.textValue.length().get() == 8) {
                    try {
                        CadCep cep = (CadCep) daoCadCep.initCriteria().addPredicate("cep", cepEntrega.textValue.getValue(), PredicateType.EQUAL).loadEntityByPredicate();
                        if (cep != null)
                            cepEntrega.value(cep);
                        else {
                            CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(cepEntrega.textValue.getValue());
                            if (viaCep.getCep() != null) {
                                CadCep novoCep = new CadCep();
                                novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                                novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                                novoCep.setBairro(viaCep.getBairro());
                                novoCep.setLogrCep(viaCep.getLogradouro());
                                daoCadCep.save(novoCep);
                                cepEntrega.value(novoCep);
                            } else {
                                MessageBox.create(message -> {
                                    message.message("CEP digitado é inválido ou não existente.");
                                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                                    message.showAndWait();
                                });
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                }
        });
    });

    private final FormFieldText cidadeCobrancaEntidadeField = FormFieldText.create().title("Cidade").width(250.0).editable(false);
    private final FormFieldText ufCobrancaEntidadeField = FormFieldText.create().title("UF").width(35.0).editable(false);
    private final FormFieldText enderecoCobrancaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Endereço").width(400.0).toUpper();
    private final FormFieldText numeroCobrancaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Número").width(70.0).toUpper();
    private final FormFieldText bairroCobrancaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Bairro").width(180.0).toUpper();
    private final FormFieldText cnpjCobrancaEntidadeField = FormFieldText.create(field -> {
        field.editable.bind(emEdicao);
        field.focusedListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                String cnpjWithoutFormat = StringUtils.unformatCpfCnpj(field.value.get());
                field.value.set(StringUtils.formatCpfCnpj(cnpjWithoutFormat));
            }
        });
    }).title("CNPJ").width(150.0);
    private final FormFieldText inscricaoCobrancaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Inscrição").width(120.0).toUpper();
    private final FormFieldText telefoneCobrancaEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Telefone").width(120.0).mask(FormFieldText.Mask.PHONE);
    private final FormFieldMultipleFind<CadCep> cepCobrancaEntidadeField = FormFieldMultipleFind.create(CadCep.class, cepCobranca -> {
        cepCobranca.title("CEP");
        cepCobranca.width(180.0);
        cepCobranca.editable(false);
        cepCobranca.postSelected((observable, oldValue, newValue) -> {
            if (cepCobranca.objectValues.size() > 0) {
                cidadeCobrancaEntidadeField.value.set(((CadCep) cepCobranca.objectValues.get(0)).getCidade().getNomeCid());
                ufCobrancaEntidadeField.value.set(((CadCep) cepCobranca.objectValues.get(0)).getCidade().getCodEst().getId().getSiglaEst());
                enderecoCobrancaEntidadeField.value.set(((CadCep) cepCobranca.objectValues.get(0)).getLogrCep() != null ? ((CadCep) cepCobranca.objectValues.get(0)).getLogrCep() : entidadeSelecionada.get().getEndCob() != null ? entidadeSelecionada.get().getEndCob() : null);
                bairroCobrancaEntidadeField.value.set(((CadCep) cepCobranca.objectValues.get(0)).getBairro() != null ? ((CadCep) cepCobranca.objectValues.get(0)).getBairro() : entidadeSelecionada.get().getBairroCob() != null ? entidadeSelecionada.get().getBairroCob() : null);
            }
        });
        cepCobranca.limit(8);
        cepCobranca.editable.bind(emEdicao);
        cepCobranca.keyReleased(event -> {
            KeyEvent keyEvent = (KeyEvent) event;
            if (cepCobranca.editable.get())
                if (keyEvent.getCode() == KeyCode.F4)
                    cepCobranca.findOnAction(null);
                else if (keyEvent.getCode() == KeyCode.ENTER || keyEvent.getCode() == KeyCode.TAB || cepCobranca.textValue.length().get() == 8) {
                    try {
                        CadCep cep = (CadCep) daoCadCep.initCriteria().addPredicate("cep", cepCobranca.textValue.getValue(), PredicateType.EQUAL).loadEntityByPredicate();
                        if (cep != null)
                            cepCobranca.value(cep);
                        else {
                            CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(cepCobranca.textValue.getValue());
                            if (viaCep.getCep() != null) {
                                CadCep novoCep = new CadCep();
                                novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                                novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                                novoCep.setBairro(viaCep.getBairro());
                                novoCep.setLogrCep(viaCep.getLogradouro());
                                daoCadCep.save(novoCep);
                                cepCobranca.value(novoCep);
                            } else {
                                MessageBox.create(message -> {
                                    message.message("CEP digitado é inválido ou não existente.");
                                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                                    message.showAndWait();
                                });
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                }
        });
    });

    private final FormFieldText rgEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("RG").width(120.0);
    private final FormFieldText emissorRgEntidadeField = FormFieldText.create(field -> field.editable.bind(emEdicao)).title("Emissor").width(120.0);
    private final FormFieldRadioButtonGroup<String> sexoEntidadeField = FormFieldRadioButtonGroup.create(field -> {
        field.title("Sexo");
        field.horizontalMode();
        field.editable.bind(emEdicao);
        field.items(
                field.option("Masculino", "M"),
                field.option("Feminino", "F")
        );
        field.select(0);
    });
    private final FormFieldDate dataNascEntidadeField = FormFieldDate.create(field -> field.editable.bind(emEdicao)).title("Dt. Nasc.");

    private final FormFieldText fieldValorMaximoReserva = FormFieldText.create(field -> {
        field.title("Valor Max. Reserva");
        field.label("R$");
        field.decimalField(2);
        field.width(120.0);
        field.alignment(Pos.CENTER_RIGHT);
        field.editable.bind(emEdicao);
    });
    private final FormFieldText fieldValorMinimoReserva = FormFieldText.create(field -> {
        field.title("Valor Min. Reserva");
        field.label("R$");
        field.decimalField(2);
        field.width(120.0);
        field.alignment(Pos.CENTER_RIGHT);
        field.editable.bind(emEdicao);
    });
    private final FormFieldText fieldFaturasMensal = FormFieldText.create(field -> {
        field.title("Fat. Mensais");
        field.width(80.0);
        field.editable.bind(emEdicao);
        field.mask(FormFieldText.Mask.INTEGER);
    });
    private final FormFieldToggleSingle fieldFaturaSeparaMarca = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Separa Marcas Fat.");

    private final FormFieldSegmentedButton<String> origemEntidadeField = FormFieldSegmentedButton.create(field -> {
        field.title("Origem");
        field.editable.set(false);
        field.options(
                field.option("ERP", "ERP", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("B2B", "B2B", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("B2C", "B2C", FormFieldSegmentedButton.Style.WARNING)
        );
        field.select(0);
    });
    private final CheckBox revisadoCheckBox = new CheckBox("Cadastro Revisado");
    private final FormFieldRadioButtonGroup<String> tipoDocumentoEntidadeField = FormFieldRadioButtonGroup.create(field -> {
        field.title("Tipo");
        field.horizontalMode();
        field.editable.bind(novaEntidade);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && tboxCnpj != null) {
                String documento = StringUtils.unformatCpfCnpj(tboxCnpj.getText());
                tboxCnpj.clear();
                if (((FormFieldRadioButtonGroup.OptionRadioButton) newValue).getText().equals("CPF")) {
                    tboxCnpj.setText(StringUtils.formatCpf(documento));
                } else {
                    tboxCnpj.setText(StringUtils.formatCnpj(documento));
                }
            }
        });
        field.items(
                field.option("CNPJ", "2"),
                field.option("CPF", "1")
        );
//        formFieldRadioButtonGroup.selectModel((observable, oldValue, newValue) -> {
//            if (newValue != null) {
//                if (newValue.getId().equals("2"))
//                    TextFieldUtils.cnpjMask(tboxCnpj);
//                else
//                    TextFieldUtils.cpfMask(tboxCnpj);
//            }
//
//        });
    });
    private final FormFieldCheckBoxGroup<String> tipoEntidadeField = FormFieldCheckBoxGroup.create(field -> {
        field.title("Entidade");
        field.horizontalMode();
        field.editable.bind(emEdicao);
        field.items(
                field.option("Cliente", "C"),
                field.option("Fornecedor", "F"),
                field.option("Terceiro", "T")
        );
    });
    private final FormFieldCheckBoxGroup<String> statusEntidadeField = FormFieldCheckBoxGroup.create(field -> {
        field.title("Status");
        field.horizontalMode();
        field.editable.bind(emEdicao);
        field.items(
                field.option("Ativo", ""),
                field.option("Bloqueado", "B")
        );
    });
    private final FormFieldComboBox<String> perfilEntidadeField = FormFieldComboBox.create(String.class, formFieldComboBox -> {
        formFieldComboBox.title("Perfil Entidade");
        formFieldComboBox.editable.bind(emEdicao);
        formFieldComboBox.items(FXCollections.observableList(Arrays.asList(new String[]{"Cliente", "Fornecedor", "Representante"})));
        formFieldComboBox.getSelectionModel((observable, oldValue, newValue) -> {
            if (newValue != null)
                if (novaEntidade.get())
                    if (newValue.equals("Cliente")) {
                        tipoEntidadeField.clear();
                        tipoEntidadeField.select(0);
                        classeGerencialField.setDefaultCode("0003");
                        historicoField.setDefaultCode("0301");
                        contaClienteField.setDefaultCode("6289");
                        contaFornecedorField.setDefaultCode(null);
                        transportadoraField.setDefaultCode("0018");
                        pagadorTransportadoraField.select(1);
                    } else if (newValue.equals("Fornecedor")) {
                        tipoEntidadeField.clear();
                        tipoEntidadeField.select(1);
                        classeGerencialField.setDefaultCode(null);
                        historicoField.setDefaultCode("0501");
                        contaClienteField.setDefaultCode(null);
                        contaFornecedorField.setDefaultCode("1505");
                    }
        });
        formFieldComboBox.select(0);
    });

    private final FormFieldSingleFind<Mensagem> motivoBloqueioEntidadeField = FormFieldSingleFind.create(Mensagem.class, field -> {
        field.editable.bind(((FormFieldCheckBoxGroup.OptionCheckBox) statusEntidadeField.items.get(1)).selectedProperty().and(emEdicao));
        field.codeReference.set("codmen");
        field.tabForm.set(cepEntidadeField.build());
        field.title("Motivo Bloqueio");
        field.width(270.0);
    });
    private final FormFieldToggleSingle imprimeConteudoCaixaField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Imprime Conteúdo Caixa: ");
    private final FormFieldToggleSingle atendeRoboField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Atendimento Robô: ");
    private final FormFieldToggleSingle antecipaProdutoField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Antecipa Produto: ");
    private final FormFieldToggleSingle atualizaGradeField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Atualiza Grade Pedido: ");
    private final FormFieldToggleSingle avisaBoletoEnviadoField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Avisar Boleto Enviado: ");
    private final FormFieldToggleSingle imprimeRemessa = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Remessa Impressa: ");
    private final FormFieldToggleSingle atualizaGradeRemessaField = FormFieldToggleSingle.create(field -> field.editable.bind(emEdicao)).title("Atualiza Grade Remessa: ");

    private final Button copiarEnderecoEntregaButton = FormButton.create(formButton -> {
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/copy (1).png").toExternalForm()));
        formButton.tooltip("Copiar Dados Gerais");
        formButton.addStyle("warning");
        formButton.disable.bind(emEdicao.not());
        formButton.setAction(event -> {
            cnpjEntregaEntidadeField.value.set(tboxCnpj.getText());
            inscricaoEntregaEntidadeField.value.set(inscricaoEntidadeField.value.getValue());
            cepEntregaEntidadeField.value(cepEntidadeField.first());
            enderecoEntregaEntidadeField.value.set(enderecoEntidadeField.value.get());
            numeroEntregaEntidadeField.value.set(numeroEntidadeField.value.get());
            bairroEntregaEntidadeField.value.set(bairroEntidadeField.value.get());
            complementoEntregaEntidadeField.value.set(complementoEntidadeField.value.getValue());
        });
    });
    private final Button copiarEnderecoCobrancaButton = FormButton.create(formButton -> {
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/copy (1).png").toExternalForm()));
        formButton.tooltip("Copiar Dados Gerais");
        formButton.addStyle("warning");
        formButton.disable.bind(emEdicao.not());
        formButton.setAction(event -> {
            cnpjCobrancaEntidadeField.value.set(tboxCnpj.getText());
            inscricaoCobrancaEntidadeField.value.set(inscricaoEntidadeField.value.getValue());
            cepCobrancaEntidadeField.value(cepEntidadeField.first());
            enderecoCobrancaEntidadeField.value.set(enderecoEntidadeField.value.get());
            numeroCobrancaEntidadeField.value.set(numeroEntidadeField.value.get());
            bairroCobrancaEntidadeField.value.set(bairroEntidadeField.value.get());
        });
    });
    private final Button carregarCnpjEnderecoEntregaButton = FormButton.create(formButton -> {
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/refresh (1).png").toExternalForm()));
        formButton.tooltip("Carregar dados do ReceitaWS");
        formButton.addStyle("info");
        formButton.disable.bind(emEdicao.not());
        formButton.setAction(event -> {
            ReceitaWS wsResponseEntity = null;
            try {
                wsResponseEntity = ServicoReceitaWS.getInstance().consultaCNPJ(cnpjEntregaEntidadeField.value.get().replace("-", "").replace("/", "").replace(".", ""));

                if (wsResponseEntity == null) {
                    MessageBox.create(message -> {
                        message.message("Não foi encontrado um cadastro com esse CNPJ no servidor da ReceitaWS.");
                        message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                        message.showAndWait();
                    });
                    return;
                }

                CadCep cepReceita = (CadCep) daoCadCep.initCriteria()
                        .addPredicate("cep", wsResponseEntity.getCep()
                                .replace(".", "")
                                .replace("-", "")
                                .replace("/", ""), PredicateType.EQUAL).loadEntityByPredicate();
                if (cepReceita == null) {
                    CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(StringUtils.unformatCep(wsResponseEntity.getCep()));
                    if (viaCep.getCep() != null) {
                        CadCep novoCep = new CadCep();
                        novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                        novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                        novoCep.setBairro(viaCep.getBairro());
                        novoCep.setLogrCep(viaCep.getLogradouro());
                        daoCadCep.save(novoCep);
                        cepReceita = novoCep;
                        cepEntregaEntidadeField.value(cepReceita);
                    } else {
                        MessageBox.create(message -> {
                            message.message("CEP digitado é inválido ou não existente.");
                            message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                            message.showAndWait();
                        });
                    }
                } else {
                    cepEntregaEntidadeField.value(cepReceita);
                }

                enderecoEntregaEntidadeField.value.set(wsResponseEntity.getLogradouro());
                numeroEntregaEntidadeField.value.set(wsResponseEntity.getNumero());
                bairroEntregaEntidadeField.value.set(wsResponseEntity.getBairro());
                complementoEntregaEntidadeField.value.set(wsResponseEntity.getComplemento());
            } catch (Exception e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    });
    private final Button carregarCnpjEnderecoCobrancaButton = FormButton.create(formButton -> {
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/refresh (1).png").toExternalForm()));
        formButton.tooltip("Carregar dados do ReceitaWS");
        formButton.addStyle("info");
        formButton.disable.bind(emEdicao.not());
        formButton.setAction(event -> {
            ReceitaWS wsResponseEntity = null;
            try {
                wsResponseEntity = ServicoReceitaWS.getInstance().consultaCNPJ(cnpjCobrancaEntidadeField.value.get().replace("-", "").replace("/", "").replace(".", ""));
                if (wsResponseEntity == null) {
                    MessageBox.create(message -> {
                        message.message("Não foi encontrado um cadastro com esse CNPJ no servidor da ReceitaWS.");
                        message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                        message.showAndWait();
                    });
                    return;
                }

                CadCep cepReceita = (CadCep) daoCadCep.initCriteria()
                        .addPredicate("cep", wsResponseEntity.getCep()
                                .replace(".", "")
                                .replace("-", "")
                                .replace("/", ""), PredicateType.EQUAL).loadEntityByPredicate();
                if (cepReceita == null) {
                    CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(StringUtils.unformatCep(wsResponseEntity.getCep()));
                    if (viaCep.getCep() != null) {
                        CadCep novoCep = new CadCep();
                        novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                        novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                        novoCep.setBairro(viaCep.getBairro());
                        novoCep.setLogrCep(viaCep.getLogradouro());
                        daoCadCep.save(novoCep);
                        cepReceita = novoCep;
                        cepCobrancaEntidadeField.value(cepReceita);
                    } else {
                        MessageBox.create(message -> {
                            message.message("CEP digitado é inválido ou não existente.");
                            message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                            message.showAndWait();
                        });
                    }
                } else {
                    cepCobrancaEntidadeField.value(cepReceita);
                }

                enderecoCobrancaEntidadeField.value.set(wsResponseEntity.getLogradouro());
                numeroCobrancaEntidadeField.value.set(wsResponseEntity.getNumero());
                bairroCobrancaEntidadeField.value.set(wsResponseEntity.getBairro());
            } catch (Exception e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX REDES SOCIAIS">
    private final FormTableView<SdEntidadeRedesSociais> tblRedesSociais = FormTableView.create(SdEntidadeRedesSociais.class, table -> {
        table.title("Redes Sociais");
        table.expanded();
        table.items.bind(redesSociais);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Rede Social");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeRedesSociais, SdEntidadeRedesSociais>, ObservableValue<SdEntidadeRedesSociais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRedesocial().getNome()));
                }).build() /*Rede Social*/,
                FormTableColumn.create(cln -> {
                    cln.title("Link");
                    cln.width(500.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeRedesSociais, SdEntidadeRedesSociais>, ObservableValue<SdEntidadeRedesSociais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLink()));
                    cln.format(param -> {
                        return new TableCell<SdEntidadeRedesSociais, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    Hyperlink hyperlink = new Hyperlink(item);
                                    setGraphic(hyperlink);
                                    hyperlink.setOnAction(evt -> {
                                        try {
                                            Desktop.getDesktop().browse(new URL(item).toURI());
                                        } catch (IOException | URISyntaxException e) {
                                            e.printStackTrace();
                                        }
                                    });
                                }
                            }
                        };
                    });
                }).build() /*Link*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeRedesSociais, SdEntidadeRedesSociais>, ObservableValue<SdEntidadeRedesSociais>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdEntidadeRedesSociais, SdEntidadeRedesSociais>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExclui = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir rede social");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(emEdicao.not());
                            });

                            @Override
                            protected void updateItem(SdEntidadeRedesSociais item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExclui.setOnAction(evt -> {
                                        new FluentDao().delete(item);
                                        redesSociais.remove(item);
                                        tblRedesSociais.refresh();
                                        MessageBox.create(message -> {
                                            message.message("Rede social excluída com sucesso");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExclui);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX B2B">
    private final FormFieldToggle ativoB2bEntidadeField = FormFieldToggle.create(formFieldToggle -> {
        formFieldToggle.title("Liberado B2B");
        formFieldToggle.editable.bind(emEdicao);
//        formFieldToggle.value.set(false);
    });
    private final FormFieldToggle ativoCatalogoEntidadeField = FormFieldToggle.create(formFieldToggle -> {
        formFieldToggle.title("Catálogo Digital");
        formFieldToggle.editable.bind(emEdicao);
//        formFieldToggle.value.set(false);
    });
    private final FormFieldSingleFind<SdMagentoSitCli> sitcliMagentoField = FormFieldSingleFind.create(SdMagentoSitCli.class, field -> {
        field.title("Grupo B2B");
        field.editable.bind(emEdicao.and(ativoB2bEntidadeField.value.or(ativoCatalogoEntidadeField.value)));
        field.width(300.0);
    });
    private final FormFieldText fieldValorMinimoPedidoB2b = FormFieldText.create(field -> {
        field.title("Mínimo Pedido");
        field.label("R$");
        field.width(150.0);
        field.editable.bind(emEdicao.and(ativoB2bEntidadeField.value));
    });
    private final FormFieldMultipleFind<SdTipoPagto> fieldTipoPagtoB2b = FormFieldMultipleFind.create(SdTipoPagto.class, field -> {
        field.title("Tipos Pagto");
        field.width(250.0);
        field.editable.bind(emEdicao.and(ativoB2bEntidadeField.value.or(ativoCatalogoEntidadeField.value)));
    });
    private final FormFieldMultipleFind<Condicao> fieldCondicaoB2b = FormFieldMultipleFind.create(Condicao.class, field -> {
        field.title("Condição Pagto");
        field.width(300.0);
        field.editable.bind(emEdicao.and(ativoB2bEntidadeField.value));
    });
    private final FormTableView<SdEntidadeTipoPagto> tblTiposPagtoB2b = FormTableView.create(SdEntidadeTipoPagto.class, table -> {
        table.title("Tipos Pagto");
        table.expanded();
        table.items.bind(tiposPagtoB2b);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeTipoPagto, SdEntidadeTipoPagto>, ObservableValue<SdEntidadeTipoPagto>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdEntidadeTipoPagto, SdEntidadeTipoPagto>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir tipo de pagto");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()));
                            });

                            @Override
                            protected void updateItem(SdEntidadeTipoPagto item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirTipoPagtoB2b(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeTipoPagto, SdEntidadeTipoPagto>, ObservableValue<SdEntidadeTipoPagto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodtipo().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeTipoPagto, SdEntidadeTipoPagto>, ObservableValue<SdEntidadeTipoPagto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodtipo().getDescricao()));
                }).build() /*Descrição*/
        );
    });
    private final FormTableView<SdEntidadeCondicao> tblCondicoesB2b = FormTableView.create(SdEntidadeCondicao.class, table -> {
        table.title("Condições");
        table.expanded();
        table.items.bind(condicoesB2b);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeCondicao, SdEntidadeCondicao>, ObservableValue<SdEntidadeCondicao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdEntidadeCondicao, SdEntidadeCondicao>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir condição");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()));
                            });

                            @Override
                            protected void updateItem(SdEntidadeCondicao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirCondicaoB2b(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeCondicao, SdEntidadeCondicao>, ObservableValue<SdEntidadeCondicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcond().getCodcond()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEntidadeCondicao, SdEntidadeCondicao>, ObservableValue<SdEntidadeCondicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcond().getDescricao()));
                }).build() /*Descrição*/
        );
    });
    private final FormBox boxMarcasB2b = FormBox.create(boxMarcasB2b -> {
        boxMarcasB2b.vertical();
        boxMarcasB2b.title("Marcas");
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BOX METAS/REGRAS">
    private final FormTableView<VSdMetasEntidade> tblMetasGlobais = FormTableView.create(VSdMetasEntidade.class, table -> {
        table.title("Metas/Regras Globais");
        table.height(180.0);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Índice");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getIndicador().toView()));
                }).build() /*Indice*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Módulo");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getJob()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Módulo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Gatilho");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTipo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Gatilho*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor Gatilho");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGatilhoTipo()));
                }).build() /*Valor Gatilho*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMetasEntidade, VSdMetasEntidade>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnCopiarRegra = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._16));
                                btn.tooltip("Copiar regra global.");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                                btn.disable.bind(emEdicao.not());
                            });

                            @Override
                            protected void updateItem(VSdMetasEntidade item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnCopiarRegra.setOnAction(evt -> {
                                        new Fragment().show(fragment -> {
                                            fragment.title("Copiar Regra/Meta Global");
                                            fragment.size(310.0, 200.0);
                                            final FormFieldSingleFind<Marca> fieldMarcaRegra = FormFieldSingleFind.create(Marca.class, field -> {
                                                field.title("Marca");
                                                field.toUpper().width(300.0);
                                                field.value.set(item.getId().getMarca());
                                            });
                                            final FormFieldSingleFind<Colecao> fieldColecaoRegra = FormFieldSingleFind.create(Colecao.class, field -> {
                                                field.title("Coleção");
                                                field.toUpper().width(300.0);
                                                field.value.set(item.getId().getColecao());
                                            });
                                            final FormFieldSingleFind<SdIndicadorComercial> fieldIndicadorRegra = FormFieldSingleFind.create(SdIndicadorComercial.class, field -> {
                                                field.title("Indicador");
                                                field.width(215.0);
                                                field.editable.set(false);
                                                field.value.set(item.getId().getIndicador());
                                            });
                                            final FormFieldText fieldValorRegra = FormFieldText.create(field -> {
                                                field.title("Valor");
                                                field.width(80.0);
                                                field.decimalField(2);
                                                field.value.set(item.getValor().toString().replaceAll("\\.", ","));
                                            });
                                            fragment.box.getChildren().add(FormBox.create(content -> {
                                                content.vertical();
                                                content.add(fieldMarcaRegra.build());
                                                content.add(fieldColecaoRegra.build());
                                                content.add(fields -> fields.addHorizontal(fieldIndicadorRegra.build(), fieldValorRegra.build()));
                                            }));
                                            fragment.buttonsBox.getChildren().add(FormButton.create(btnCopiarRegra -> {
                                                btnCopiarRegra.title("Copiar").icon(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._24));
                                                btnCopiarRegra.addStyle("primary");
                                                btnCopiarRegra.setAction(evtCopiar -> {
                                                    try {
                                                        fieldColecaoRegra.validate();
                                                        fieldMarcaRegra.validate();
                                                        fieldIndicadorRegra.validate();
                                                        fieldValorRegra.validate();
                                                        fieldValorRegra.validateDecimal();

                                                        mergeRegraEntidade(item, fieldMarcaRegra.value.get().getCodigo(), fieldColecaoRegra.value.get().getCodigo(), String.valueOf(fieldIndicadorRegra.value.get().getCodigo()), fieldValorRegra.decimalValue());
                                                        MessageBox.create(message -> {
                                                            message.message("Meta/Regra cadastrada com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.position(Pos.TOP_RIGHT);
                                                            message.notification();
                                                        });
                                                        carregarMetas(entidadeSelecionada.get().getStringCodcli());
                                                        fragment.close();
                                                    } catch (FormValidationException valid) {
                                                        MessageBox.create(message -> {
                                                            message.message(valid.getMessage());
                                                            message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                                                            message.showAndWait();
                                                        });
                                                    } catch (SQLException throwables) {
                                                        throwables.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(throwables);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                });
                                            }));
                                        });
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnCopiarRegra);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });

    private final FormTabPane tabPaneMarcasMetas = FormTabPane.create(tabPane -> {
        tabPane.expanded();
    });
    // </editor-fold>
    // </editor-fold>

    public EntidadeController() throws SQLException {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //entidades.set(daoEntidade.initCriteria().addPredicate("codcli", 12776, PredicateType.EQUAL).loadListByPredicate());
        entidades.set(FXCollections.observableList((List<Entidade>) new FluentDao().selectFrom(Entidade.class).where(eb -> eb.equal("ativo", true)).resultList(10)));

        sugestoesRazaoSocial = new HashSet<>(entidades.get().stream().map(entidade -> entidade.getNome()).collect(Collectors.toList()));
        sugestoesCnpjCepf = new HashSet<>(entidades.get().stream().map(entidade -> entidade.getCnpj()).collect(Collectors.toList()));
        sugestoesCodigo = new HashSet<>(entidades.get().stream().map(entidade -> String.valueOf(entidade.getCodcli())).collect(Collectors.toList()));

        this.iniFxmlComponents();
        this.initBoxDadosGerais();
        this.initBoxMarcas();
        this.initBoxInfosAuxiliares();
        this.initBoxAnexos();
        this.initBoxRedesSociais();
        this.initBoxB2b();
        this.initBoxMetasRegras();

        tabPaneRoot.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.getText().equals("Listagem"))
                btnReturnOnAction(null);
        });
    }

    private void iniFxmlComponents() {
        // <editor-fold defaultstate="collapsed" desc="init(tabela entidades)">
        tblEntidades.itemsProperty().bind(entidades);
        tblEntidades.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            entidadeSelecionada.set(newValue);
            navegationIndex.set(tblEntidades.getItems().indexOf(newValue));
            carregaEntidade(newValue);
        });
        tblEntidades.getSelectionModel().selectFirst();
        // <editor-fold defaultstate="collapsed" desc="Columns TableView">
        clnEntAcoes.setCellValueFactory(features -> new ReadOnlyObjectWrapper(features.getValue()));
        clnEntAcoes.setCellFactory(param -> {
            return new TableCell<Entidade, Entidade>() {
                @Override
                protected void updateItem(Entidade item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);

                    if (item != null && !empty) {
                        setGraphic(FormBox.create(boxButtonsAction -> {
                            boxButtonsAction.horizontal();
                            boxButtonsAction.add(FormButton.create(btnVisualizar -> {
                                btnVisualizar.icon(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                                btnVisualizar.addStyle("xs");
                                btnVisualizar.addStyle("info");
                                btnVisualizar.tooltip("Visualizar");
                                btnVisualizar.setAction(event -> {
                                    actionVisualizarEntidade(item);
                                });
                            }));
                            boxButtonsAction.add(FormButton.create(btnVisualizar -> {
                                btnVisualizar.icon(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                                btnVisualizar.addStyle("xs");
                                btnVisualizar.addStyle("warning");
                                btnVisualizar.tooltip("Editar");
                                btnVisualizar.setAction(event -> {
                                    actionEditarEntidade(item);
                                });
                            }));
                            boxButtonsAction.add(FormButton.create(btnVisualizar -> {
                                btnVisualizar.icon(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                                btnVisualizar.addStyle("xs");
                                btnVisualizar.addStyle("danger");
                                btnVisualizar.tooltip("Excluir");
                                btnVisualizar.setAction(event -> {
                                    actionDeletarEntidade(item);
                                });
                            }));
                        }));
                    }
                }
            };
        });
        clnEntAtivo.setCellFactory(param -> {
            return new TableCell<Entidade, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);

                    if (item != null && !empty) {
                        setGraphic(imgStatus(item));
                    }
                }
            };
        });
        clnEntBloqueio.setCellFactory(param -> {
            return new TableCell<Entidade, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item ? "Sim" : "Não");
                    }
                }
            };
        });
        clnEntSincB2b.setCellFactory(param -> {
            return new TableCell<Entidade, SdEntidade>() {
                @Override
                protected void updateItem(SdEntidade item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item.isSincB2b() ? "Sim" : "Não");
                    }
                }
            };
        });
        clnEntLiberadoB2b.setCellFactory(param -> {
            return new TableCell<Entidade, SdEntidade>() {
                @Override
                protected void updateItem(SdEntidade item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);

                    if (item != null && !empty) {
                        setGraphic(imgStatus(item.isLiberadoB2b()));
                    }
                }
            };
        });
        clnEntTipo.setCellFactory(param -> {
            return new TableCell<Entidade, SitCli>() {
                @Override
                protected void updateItem(SitCli item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item.getCodigo());
                    }
                }
            };
        });
        clnEntOrigem.setCellFactory(param -> {
            return new TableCell<Entidade, SdEntidade>() {
                @Override
                protected void updateItem(SdEntidade item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item.getOrigem());
                    }
                }
            };
        });
        clnEntCidade.setCellFactory(param -> {
            return new TableCell<Entidade, CadCep>() {
                @Override
                protected void updateItem(CadCep item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item.getCidade().getNomeCid());
                    }
                }
            };
        });
        clnEntUf.setCellFactory(param -> {
            return new TableCell<Entidade, CadCep>() {
                @Override
                protected void updateItem(CadCep item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item.getCidade().getCodEst().getId().getSiglaEst());
                    }
                }
            };
        });
        // </editor-fold>
        lbContadorRegistros.textProperty().bind(new SimpleStringProperty("Exibindo ").concat(entidades.sizeProperty()).concat(" registro(s)"));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="init(navegação)" >
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnAddRegister.disableProperty().bind(emEdicao);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);
        btnReturn.disableProperty().bind(emEdicao);

        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));

        disableBtnSaveRegister.bind(emEdicao.not());
        disableBtnCancelRegister.bind(emEdicao.not());
        disableBtnEditRegister.bind(entidadeSelecionada.isNull().or(emEdicao));
        disableBtnDeleteRegister.bind(entidadeSelecionada.isNull().or(emEdicao));

        disableButtonNavegation.bind(tblEntidades.itemsProperty().isNull().or(emEdicao));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(entidades.sizeProperty().subtract(1)));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="init(fxml fields)">
        TextFieldUtils.upperCase(tboxFiltroRazao);
        TextFieldUtils.upperCase(tboxFiltroFantasia);
        TextFieldUtils.phoneMask(tboxTelefoneContatoEntidade);
        TextFieldUtils.emailMask(tboxEmailContatoEntidade);
        TextFieldUtils.cnpjCpfMask(tboxFiltroCnpj);
        TextFieldUtils.integerMask(tboxCode);
        cboxFiltroUf.setItems(FXCollections.observableList(Arrays.asList("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "EX", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO")));
        tboxDataNascContatoEntidade.setValue(LocalDate.of(1899, 12, 30));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="init(fxml buttons)">
        btnImportarDadosReceita.disableProperty().bind(emEdicao.not());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="init(boxCadastroRevisadoDataPedido)">
        boxCadastroRevisadoDataPedido.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.add(boxPeriodoPedido -> {
                boxPeriodoPedido.horizontal();
                boxPeriodoPedido.title("Período dos Pedidos");
                boxPeriodoPedido.add(dataPedidoFilter.build());
                boxPeriodoPedido.add(cboxGroupStatusPedido.build());
            });
            root.add(boxCadastroRevisado -> {
                boxCadastroRevisado.horizontal();
                boxCadastroRevisado.add(segBtnRevisaoCadastro.build());
            });
        }));
        // </editor-fold>

        clnDtNascContatoEntidade.setCellFactory(param -> {
            return new TableCell<Contato, LocalDate>() {
                @Override
                protected void updateItem(LocalDate item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty)
                        setText(StringUtils.toDateFormat(item));
                }
            };
        });
        try {
            cboxTipoContatoEntidade.setItems(daoTipoContato.list());
            pagadorTransportadoraField.items.set(daoPagadorFrete.list());
            pagadorRedespachoField.items.set(daoPagadorFrete.list());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void initBoxDadosGerais() {
        TextFieldUtils.upperCase(tboxRazaoSocial);
        TextFieldUtils.upperCase(tboxCode);
        tboxCnpj.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                String cnpjWithoutFormat = StringUtils.unformatCpfCnpj(tboxCnpj.getText());
                tboxCnpj.setText(StringUtils.formatCpfCnpj(cnpjWithoutFormat));
                if (cnpjWithoutFormat.length() <= 11)
                    tipoDocumentoEntidadeField.select(1);
                else
                    tipoDocumentoEntidadeField.select(0);

                if (novaEntidade.get()) {
                    String cnpjFormatado = StringUtils.formatCpfCnpj(StringUtils.unformatCpfCnpj(tboxCnpj.getText()));
                    Entidade quickFind = (Entidade) new FluentDao().selectFrom(Entidade.class)
                            .where(eb -> eb.equal("cnpj", cnpjFormatado))
                            .singleResult();
                    if (quickFind != null) {
                        tboxCnpj.clear();
                        MessageBox.create(message -> {
                            message.message("O CNPJ/CPF " + quickFind.getCnpj() + " digitado já existe no banco de dados.\nEntidade: [" + quickFind.getCodcli() + "] " + quickFind.getNome());
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                    }
                }
            }
        });
        //TextFields.bindAutoCompletion(tboxRazaoSocial, sugestoesRazaoSocial);
        //TextFieldUtils.upperCase(tboxCnpj);
        //TextFieldUtils.cnpjMask(tboxCnpj);
        //TextFields.bindAutoCompletion(tboxCnpj, sugestoesCnpjCepf);
        //TextFields.bindAutoCompletion(tboxCode, sugestoesCodigo);
        tboxCnpj.disableProperty().bind(emEdicao.and(novaEntidade.not()));
        tboxCode.disableProperty().bind(emEdicao);
        tboxObservacaoNfe.editableProperty().bind(emEdicao);
        tboxObservacaoFinanceiro.editableProperty().bind(emEdicao);
        tboxObservacaoComercial.editableProperty().bind(emEdicao);
        btnSalvarContato.disableProperty().bind(emEdicao.not());
        dadosGeraisAcessoEntidade.getChildren().add(1, tipoDocumentoEntidadeField.build());
        dadosGeraisBoxCabecalho.getChildren().add(perfilEntidadeField.build());
        dadosGeraisBoxCabecalho.getChildren().add(FormBox.create(formBox -> {
            formBox.vertical();
            formBox.expanded();
            formBox.add(FormBox.create(formBox1 -> {
                formBox1.horizontal();
                formBox1.alignment(Pos.CENTER_LEFT);
                formBox1.add(tipoEntidadeField.build());
                formBox1.add(statusEntidadeField.build());
                formBox1.add(dtCadastroEntidadeField.build());
                formBox1.add(origemEntidadeField.build());
                formBox1.add(box -> {
                    box.vertical();
                    box.height(40);
                    box.alignment(Pos.BOTTOM_LEFT);
                    box.disable.bind(emEdicao.not().or(novaEntidade));
                    box.add(revisadoCheckBox);
                });
            }));
        }));
        boxDadosGerais.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.add(FormBox.create(formBox -> {
                formBox.horizontal();
                formBox.expanded();
                formBox.add(FormBox.create(boxCadastroAdicionais -> {
                    boxCadastroAdicionais.vertical();
                    boxCadastroAdicionais.add(FormBox.create(boxCadastro -> {
                        boxCadastro.vertical();
                        boxCadastro.title("Cadastro");
                        boxCadastro.add(FormBox.create(linha -> {
                            linha.horizontal();
                            linha.add(fantasiaEntidadeField.build());
                        }));
                        boxCadastro.add(FormBox.create(linha -> {
                            linha.horizontal();
                            linha.add(FormBox.create(formBox1 -> {
                                formBox1.horizontal();
                                formBox1.add(inscricaoEntidadeField.build());
                                formBox1.add(emailEntidadeField.build());
                            }));
                        }));
                        boxCadastro.add(FormBox.create(linha -> {
                            linha.horizontal();
                            linha.add(FormBox.create(formBox1 -> {
                                formBox1.horizontal();
                                formBox1.add(telefoneEntidadeField.build());
                                formBox1.add(faxEntidadeField.build());
                            }));
                        }));
                        boxCadastro.add(FormBox.create(linha -> {
                            linha.horizontal();
                            linha.add(FormBox.create(formBox1 -> {
                                formBox1.horizontal();
                                formBox1.add(creditoEntidadeField.build());
                                formBox1.add(ramoAtividadeEntidadeField.build());
                            }));
                        }));
                        boxCadastro.add(FormBox.create(linha -> {
                            linha.horizontal();
                            linha.add(FormBox.create(formBox1 -> {
                                formBox1.horizontal();
                                formBox1.add(sitCliEntidadeField.build());
                                formBox1.add(grupoEntidadeField.build());
                            }));
                        }));
                    }));
                    boxCadastroAdicionais.add(FormBox.create(boxAdicionais -> {
                        boxAdicionais.vertical();
                        boxAdicionais.add(FormBox.create(boxItemsAdicionais -> {
                            boxItemsAdicionais.horizontal();
                            boxItemsAdicionais.title("Adicionais");
                            boxItemsAdicionais.add(FormBox.create(boxDados1 -> {
                                boxDados1.vertical();
                                boxDados1.expanded();
                                boxDados1.add(FormBox.create(boxExtra -> {
                                    boxExtra.horizontal();
                                    boxExtra.add(motivoBloqueioEntidadeField.build());
                                }));
                                boxDados1.add(FormBox.create(boxExtra -> {
                                    boxExtra.horizontal();
                                    boxExtra.add(fieldValorMinimoReserva.build(), fieldValorMaximoReserva.build());
                                }));
                                boxDados1.add(FormBox.create(boxExtra -> {
                                    boxExtra.horizontal();
                                    boxExtra.add(fieldFaturasMensal.build(), fieldFaturaSeparaMarca.build());
                                }));
                            }));
                            boxItemsAdicionais.add(FormBox.create(boxDados2 -> {
                                boxDados2.vertical();
                                boxDados2.add(atualizaGradeField.build());
                                boxDados2.add(avisaBoletoEnviadoField.build());
                                boxDados2.add(imprimeConteudoCaixaField.build());
                                boxDados2.add(atendeRoboField.build());
                                boxDados2.add(antecipaProdutoField.build());
                                boxDados2.add(imprimeRemessa.build());
                                boxDados2.add(atualizaGradeRemessaField.build());
                            }));
                        }));
                    }));
                }));
                formBox.add(FormBox.create(boxEnderecosComplementar -> {
                    boxEnderecosComplementar.horizontal();
                    boxEnderecosComplementar.add(FormBox.create(coluna1 -> {
                        coluna1.vertical();
                        coluna1.add(FormBox.create(enderecoEntidade -> {
                            enderecoEntidade.vertical();
                            enderecoEntidade.title("Endereço");
                            enderecoEntidade.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(cepEntidadeField.build());
                                boxDados.add(cidadeEntidadeField.build());
                                boxDados.add(ufEntidadeField.build());
                            }));
                            enderecoEntidade.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.alignment(Pos.BOTTOM_LEFT);
                                boxDados.add(paisEntidadeField.build());
                                boxDados.add(bairroEntidadeField.build());
                                boxDados.add(FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LOCAL, ImageUtils.IconSize._24));
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("info");
                                    btn.setAction(evt -> {
                                        try {
                                            Runtime rt = Runtime.getRuntime();
                                            String url = "https://www.google.com.br/maps/search/" +
                                                    enderecoEntidadeField.value.get().trim().replaceAll(" ", "+").concat(",+") +
                                                    numeroEntidadeField.value.get().trim().concat("+-+").concat(bairroEntidadeField.value.get().trim().replace(" ", "+")).concat(",+") +
                                                    cidadeEntidadeField.value.get().trim().replaceAll(" ", "+").concat("+-+").concat(ufEntidadeField.value.get());
                                            rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                }));
                            }));
                            enderecoEntidade.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(enderecoEntidadeField.build());
                                boxDados.add(numeroEntidadeField.build());
                            }));
                            enderecoEntidade.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(complementoEntidadeField.build());
                            }));
                        }));
                        coluna1.add(FormBox.create(enderecoEntrega -> {
                            enderecoEntrega.vertical();
                            enderecoEntrega.title("Entrega");
                            enderecoEntrega.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.alignment(Pos.BOTTOM_LEFT);
                                boxDados.add(cnpjEntregaEntidadeField.build());
                                boxDados.add(carregarCnpjEnderecoEntregaButton);
                                boxDados.add(copiarEnderecoEntregaButton);
                                boxDados.add(inscricaoEntregaEntidadeField.build());
                            }));
                            enderecoEntrega.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(cepEntregaEntidadeField.build());
                                boxDados.add(cidadeEntregaEntidadeField.build());
                                boxDados.add(ufEntregaEntidadeField.build());
                            }));
                            enderecoEntrega.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(enderecoEntregaEntidadeField.build());
                                boxDados.add(numeroEntregaEntidadeField.build());
                            }));
                            enderecoEntrega.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(bairroEntregaEntidadeField.build());
                                boxDados.add(complementoEntregaEntidadeField.build());
                            }));
                        }));
                    }));
                    boxEnderecosComplementar.add(FormBox.create(coluna2 -> {
                        coluna2.vertical();
                        coluna2.add(FormBox.create(enderecoCobranca -> {
                            enderecoCobranca.vertical();
                            enderecoCobranca.title("Cobrança");
                            enderecoCobranca.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.alignment(Pos.BOTTOM_LEFT);
                                boxDados.add(cnpjCobrancaEntidadeField.build());
                                boxDados.add(carregarCnpjEnderecoCobrancaButton);
                                boxDados.add(copiarEnderecoCobrancaButton);
                                boxDados.add(inscricaoCobrancaEntidadeField.build());
                            }));
                            enderecoCobranca.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(cepCobrancaEntidadeField.build());
                                boxDados.add(cidadeCobrancaEntidadeField.build());
                                boxDados.add(ufCobrancaEntidadeField.build());
                            }));
                            enderecoCobranca.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(enderecoCobrancaEntidadeField.build());
                                boxDados.add(numeroCobrancaEntidadeField.build());
                            }));
                            enderecoCobranca.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(bairroCobrancaEntidadeField.build());
                                boxDados.add(telefoneCobrancaEntidadeField.build());
                            }));
                        }));
                        coluna2.add(FormBox.create(complementar -> {
                            complementar.vertical();
                            complementar.visible.bind(tipoDocumentoEntidadeField.items.get(1).selectedProperty());
                            complementar.title("Complementares");
                            complementar.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(rgEntidadeField.build());
                                boxDados.add(emissorRgEntidadeField.build());
                            }));
                            complementar.add(FormBox.create(boxDados -> {
                                boxDados.horizontal();
                                boxDados.add(sexoEntidadeField.build());
                                boxDados.add(dataNascEntidadeField.build());
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    private void initBoxInfosAuxiliares() {
        boxInformacoesAuxiliares.getChildren().add(FormBox.create(boxInfosAuxiliares -> {
            boxInfosAuxiliares.vertical();
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(naturezaField.build());
                linha.add(dataFundacaoField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(classeGerencialField.build());
                linha.add(inscricaoStField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(historicoField.build());
                linha.add(tipoTributacaoField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(transportadoraField.build());
                linha.add(pagadorTransportadoraField.build());
                linha.add(redespachoField.build());
                linha.add(pagadorRedespachoField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(bancoField.build());
                linha.add(contaField.build());
                linha.add(agenciaField.build());
                linha.add(situacaoDuplicataField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(tablePrecoField.build());
                linha.add(condicaoPagamentoField.build());
                linha.add(descontoField.build());
                linha.add(diasParaEntregaField.build());
                linha.add(suframaField.build());
            }));
            boxInfosAuxiliares.add(FormBox.create(linha -> {
                linha.horizontal();
                linha.add(contaClienteField.build());
                linha.add(contaFornecedorField.build());
            }));
        }));
    }

    private void initBoxMarcas() {
        boxMarcas.getChildren().add(FormBox.create(dadosMarca -> {
            dadosMarca.vertical();
            dadosMarca.expanded();
            dadosMarca.add(FormBox.create(formBox1 -> {
                formBox1.horizontal();
                formBox1.alignment(Pos.BOTTOM_LEFT);
                formBox1.add(marcaEntidadeField.build());
                formBox1.add(adicionarMarcaEntidadeButton);
            }));
            dadosMarca.add(marcasTabPane);
        }));
    }

    private void initBoxRedesSociais() {
        boxRedesSociais.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(containerCadastro -> {
                containerCadastro.horizontal();
                containerCadastro.alignment(Pos.BOTTOM_LEFT);
                final FormFieldSingleFind<SdRedesSociais> fieldRedeSocial = FormFieldSingleFind.create(SdRedesSociais.class, field -> {
                    field.title("Rede Social");
                    field.editable.bind(emEdicao);
                });
                final FormFieldText fieldLink = FormFieldText.create(field -> {
                    field.title("Link");
                    field.width(300.0);
                    field.editable.bind(emEdicao);
                });
                containerCadastro.add(fieldRedeSocial.build(), fieldLink.build());
                containerCadastro.add(FormButton.create(btn -> {
                    btn.title("Incluir");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                    btn.addStyle("success");
                    btn.disable.bind(emEdicao.not());
                    btn.setAction(evt -> {
                        try {
                            SdEntidadeRedesSociais redeSocialEntidade = new SdEntidadeRedesSociais(
                                    StringUtils.lpad(entidadeSelecionada.get().getCodcli(), 5, "0"),
                                    fieldRedeSocial.validate().value.get(),
                                    fieldLink.validate().value.get());
                            redeSocialEntidade = new FluentDao().persist(redeSocialEntidade);
                            redesSociais.add(redeSocialEntidade);
                            tblRedesSociais.refresh();
                            fieldRedeSocial.clear();
                            fieldLink.clear();
                            MessageBox.create(message -> {
                                message.message("Rede social adicionada com sucesso para a entidade.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (FormValidationException e) {
                            MessageBox.create(message -> {
                                message.message("Existem campos obrigatórios não preenchidos:\n" + e.getMessage());
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                }));
                containerCadastro.add(FormButton.create(btn -> {
                    btn.title("Cancelar");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                    btn.addStyle("danger");
                    btn.setAction(evt -> {
                        fieldRedeSocial.clear();
                        fieldLink.clear();
                    });
                }));
            }));
            container.add(new Separator(Orientation.HORIZONTAL));
            container.add(tblRedesSociais.build());
        }));
    }

    /**
     * TAB B2B
     */
    private void initBoxB2b() {
        boxB2b.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(ativoB2bEntidadeField.build());
                boxFields.add(ativoCatalogoEntidadeField.build());
                boxFields.add(sitcliMagentoField.build());
                boxFields.add(fieldValorMinimoPedidoB2b.build());
            }));
            container.add(FormBox.create(boxBlocos -> {
                boxBlocos.horizontal();
                boxBlocos.add(FormBox.create(boxTipoPagto -> {
                    boxTipoPagto.vertical();
                    boxTipoPagto.title("Tipo Pagto");
                    boxTipoPagto.height(400.0);
                    boxTipoPagto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldTipoPagtoB2b.build());
                        boxFields.add(FormButton.create(btn -> {
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                            btn.addStyle("lg").addStyle("success");
                            btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()));
                            btn.setAction(evt -> adicionarTipoPagtoB2b(entidadeSelecionada.get(), fieldTipoPagtoB2b.objectValues.get()));
                        }));
                    }));
                    boxTipoPagto.add(tblTiposPagtoB2b.build());
                    boxTipoPagto.add(FormButton.create(btn -> {
                        btn.title("Excluir Todos").icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btn.addStyle("danger");
                        btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()).or(tiposPagtoB2b.emptyProperty()));
                        btn.setAction(evt -> excluirTodosTipoPagtoB2b());
                    }));
                }));
                boxBlocos.add(FormBox.create(boxCondicao -> {
                    boxCondicao.vertical();
                    boxCondicao.title("Condição Pagto");
                    boxCondicao.height(400.0);
                    boxCondicao.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldCondicaoB2b.build());
                        boxFields.add(FormButton.create(btn -> {
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                            btn.addStyle("lg").addStyle("success");
                            btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()));
                            btn.setAction(evt -> adicionarCondicaoB2b(entidadeSelecionada.get(), fieldCondicaoB2b.objectValues.get()));
                        }));
                    }));
                    boxCondicao.add(tblCondicoesB2b.build());
                    boxCondicao.add(FormButton.create(btn -> {
                        btn.title("Excluir Todos").icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btn.addStyle("danger");
                        btn.disable.bind(emEdicao.not().or(ativoB2bEntidadeField.value.not()).or(condicoesB2b.emptyProperty()));
                        btn.setAction(evt -> excluirTodosCondicaoB2b());
                    }));
                }));
                boxBlocos.add(FormBox.create(boxConfigMarcasB2b -> {
                    boxConfigMarcasB2b.vertical();
                    boxConfigMarcasB2b.add(boxMarcasB2b);
                }));
            }));
        }));
    }

    private void excluirTodosCondicaoB2b() {
        condicoesB2b.forEach(this::excluirCondicaoB2b);
    }

    private void excluirTodosTipoPagtoB2b() {
        tiposPagtoB2b.forEach(this::excluirTipoPagtoB2b);
    }

    private void adicionarCondicaoB2b(Entidade entidade, ObservableList<Condicao> condicoes) {
        if (condicoes.size() > 0) {
            String codcli = StringUtils.lpad(entidade.getCodcli(), 5, "0");
            Integer countCadastro = 0;
            for (Condicao condicao : condicoes) {
                SdEntidadeCondicao entCondicao = new FluentDao().selectFrom(SdEntidadeCondicao.class)
                        .where(eb -> eb.equal("codcli", codcli).equal("codcond.codcond", condicao.getCodcond()))
                        .singleResult();
                if (entCondicao == null) {
                    entCondicao = new SdEntidadeCondicao();
                    entCondicao.setCodcli(codcli);
                    entCondicao.setCodcond(condicao);

                    try {
                        new FluentDao().persist(entCondicao);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                    condicoesB2b.add(entCondicao);
                    countCadastro++;
                }
            }

            Integer finalCountCadastro = countCadastro;
            MessageBox.create(message -> {
                message.message("Cadastrado " + finalCountCadastro + " condição(ões) para o cliente B2B.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            fieldCondicaoB2b.clear();
        }
    }

    private void adicionarTipoPagtoB2b(Entidade entidade, ObservableList<SdTipoPagto> tiposPagto) {
        if (tiposPagto.size() > 0) {
            String codcli = StringUtils.lpad(entidade.getCodcli(), 5, "0");
            Integer countCadastro = 0;
            for (SdTipoPagto tipoPagto : tiposPagto) {
                SdEntidadeTipoPagto entTipoPagto = new FluentDao().selectFrom(SdEntidadeTipoPagto.class)
                        .where(eb -> eb.equal("codcli", codcli).equal("codtipo.codigo", tipoPagto.getCodigo()))
                        .singleResult();
                if (entTipoPagto == null) {
                    entTipoPagto = new SdEntidadeTipoPagto();
                    entTipoPagto.setCodcli(codcli);
                    entTipoPagto.setCodtipo(tipoPagto);

                    try {
                        new FluentDao().persist(entTipoPagto);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                    tiposPagtoB2b.add(entTipoPagto);
                    countCadastro++;
                }
            }

            Integer finalCountCadastro = countCadastro;
            MessageBox.create(message -> {
                message.message("Cadastrado " + finalCountCadastro + " tipo(s) pagto para o cliente B2B.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            fieldTipoPagtoB2b.clear();
        }
    }

    private void excluirTipoPagtoB2b(SdEntidadeTipoPagto tipoPagto) {
        new FluentDao().delete(tipoPagto);
        tiposPagtoB2b.remove(tipoPagto);

        MessageBox.create(message -> {
            message.message("Tipo " + tipoPagto.getCodtipo().getDescricao() + " excluído do cliente B2B");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void excluirCondicaoB2b(SdEntidadeCondicao condicao) {
        new FluentDao().delete(condicao);
        condicoesB2b.remove(condicao);

        MessageBox.create(message -> {
            message.message("Condição " + condicao.getCodcond().getDescricao() + " excluída do cliente B2B");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    // FIM TAB B2B


    /**
     * TAB METAS
     */
    private void initBoxMetasRegras() {
        boxMetasComerciais.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.maxHeightSize(Double.MAX_VALUE);
            container.add(tblMetasGlobais.build());
            container.add(tabPaneMarcasMetas);
        }));
    }

    private void mergeRegraEntidade(VSdMetasEntidade item, String marca, String colecao, String indicador, BigDecimal valor) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                        "merge into sd_metas_entidade_001 mta\n" +
                        "using dual\n" +
                        "on (mta.cliente = '%s' and mta.tipo = '%s' and mta.indicador = '%s' and mta.colecao = '%s' and mta.marca = '%s' and job = '%s')\n" +
                        "when matched then\n" +
                        "  update set valor = %s\n" +
                        "when not matched then\n" +
                        "  insert\n" +
                        "    (cliente,\n" +
                        "     tipo,\n" +
                        "     indicador,\n" +
                        "     valor,\n" +
                        "     colecao,\n" +
                        "     marca,\n" +
                        "     gatilho_tipo,\n" +
                        "     acao,\n" +
                        "     job)\n" +
                        "  values\n" +
                        "    ('%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s')",
                entidadeSelecionada.get().getStringCodcli(), item.getId().getTipo(), indicador, colecao, marca, item.getId().getJob(),
                valor,
                entidadeSelecionada.get().getStringCodcli(), item.getId().getTipo(), indicador, valor, colecao, marca,
                item.getGatilhoTipo(), item.getAcao().replaceAll("'", "''"), item.getId().getJob());

        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.CADASTRAR, entidadeSelecionada.get().getStringCodcli(),
                "Cadastrando meta/regra " + item.getId().toView() + " com o valor " + valor);
    }

    private void carregarMetas(String codcli) {
        // limpeza de tela
        tblMetasGlobais.clear();
        tabPaneMarcasMetas.clear();

        String[] codcliConsulta = new String[]{"0", codcli};
        List<VSdMetasEntidade> metasEntidades = (List<VSdMetasEntidade>) new FluentDao().selectFrom(VSdMetasEntidade.class)
                .where(eb -> eb.isIn("id.cliente", codcliConsulta))
                .resultList();
        metasEntidades.forEach(VSdMetasEntidade::refresh);

        List<VSdMetasEntidade> metasGlobais = metasEntidades.stream().filter(it -> it.getId().getCliente().equals("0"))
                .collect(Collectors.toList());
        tblMetasGlobais.items.set(FXCollections.observableList(metasGlobais));

        List<VSdMetasEntidade> metasCliente = metasEntidades.stream().filter(it -> !it.getId().getCliente().equals("0"))
                .collect(Collectors.toList());
        metasCliente.stream().collect(Collectors.groupingBy(it -> it.getId().getMarca())).forEach((marca, metas) -> {
            final FormTableView<VSdMetasEntidade> tblMarca = FormTableView.create(VSdMetasEntidade.class, table -> {
                table.title("Metas/Regras");
                table.expanded();
                table.items.set(FXCollections.observableList(metas.stream().sorted((o1, o2) -> o1.getId().getColecao().getCodigo()
                        .compareTo(o2.getId().getColecao().getCodigo())).collect(Collectors.toList())));

                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Índice");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getIndicador().toView()));
                        }).build() /*Indice*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Valor");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.alignment(Pos.CENTER);
                            cln.format(param -> {
                                return new TableCell<VSdMetasEntidade, VSdMetasEntidade>() {
                                    @Override
                                    protected void updateItem(VSdMetasEntidade item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.expanded();
                                                field.editable.bind(emEdicao);
                                                field.value.set(item.getValor().toString());
                                                field.addStyle("xs");
                                                field.tooltip("Pressione ENTER após a digitação do valor para salvar a alteração");
                                                field.alignment(Pos.CENTER);
                                                field.keyReleased(evt -> {
                                                    if (evt.getCode().equals(KeyCode.ENTER)) {
                                                        try {
                                                            field.validate();
                                                            field.validateDecimal();

                                                            new NativeDAO().runNativeQueryUpdate("" +
                                                                            "update sd_metas_entidade_001\n" +
                                                                            "   set valor = %s\n" +
                                                                            " where cliente = '%s'\n" +
                                                                            "   and tipo = '%s'\n" +
                                                                            "   and indicador = '%s'\n" +
                                                                            "   and colecao = '%s'\n" +
                                                                            "   and marca = '%s'\n" +
                                                                            "   and job = '%s'",
                                                                    field.decimalValue(),
                                                                    entidadeSelecionada.get().getStringCodcli(), item.getId().getTipo(), item.getId().getIndicador().getCodigo(),
                                                                    item.getId().getColecao().getCodigo(), item.getId().getMarca().getCodigo(), item.getId().getJob());
                                                            SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EXCLUIR, entidadeSelecionada.get().getStringCodcli(),
                                                                    "Excluído a regra/meta " + item.getId().toView());

                                                            MessageBox.create(message -> {
                                                                message.message("Alterado valor da regra " + item.getId().toView() + " para " + field.decimalValue());
                                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                message.position(Pos.TOP_RIGHT);
                                                                message.notification();
                                                            });
                                                        } catch (FormValidationException valid) {
                                                            MessageBox.create(message -> {
                                                                message.message(valid.getMessage());
                                                                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                                                                message.showAndWait();
                                                            });
                                                        } catch (SQLException throwables) {
                                                            throwables.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(throwables);
                                                                message.showAndWait();
                                                            });
                                                        }
                                                    }
                                                });
                                            }).build());
                                        }
                                    }
                                };
                            });
                        }).build() /*Valor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Coleção");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                        }).build() /*Coleção*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Módulo");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getJob()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Módulo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Gatilho");
                            cln.width(90.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTipo()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Gatilho*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Valor Gatilho");
                            cln.width(130.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGatilhoTipo()));
                        }).build() /*Valor Gatilho*/,
                        FormTableColumn.create(cln -> {
                            cln.title("");
                            cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                            cln.width(80.0);
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<VSdMetasEntidade, VSdMetasEntidade>() {
                                    final HBox boxButtonsRow = new HBox(3);
                                    final Button btnExcluirRegraMarca = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                        btn.tooltip("Excluir Regra/Metas");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.disable.bind(emEdicao.not());
                                        btn.addStyle("xs").addStyle("danger");
                                    });
                                    final Button btnCopiarRegraMarca = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._16));
                                        btn.tooltip("Copiar Regra/Metas");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.disable.bind(emEdicao.not());
                                        btn.addStyle("xs").addStyle("primary");
                                    });

                                    @Override
                                    protected void updateItem(VSdMetasEntidade item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            btnExcluirRegraMarca.setOnAction(evt -> {
                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("Deseja realmente excluir a regra/meta?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    try {
                                                        new NativeDAO().runNativeQueryUpdate("" +
                                                                        "delete from sd_metas_entidade_001\n" +
                                                                        " where cliente = '%s'\n" +
                                                                        "   and tipo = '%s'\n" +
                                                                        "   and indicador = '%s'\n" +
                                                                        "   and colecao = '%s'\n" +
                                                                        "   and marca = '%s'\n" +
                                                                        "   and job = '%s'",
                                                                entidadeSelecionada.get().getStringCodcli(), item.getId().getTipo(), item.getId().getIndicador().getCodigo(),
                                                                item.getId().getColecao().getCodigo(), item.getId().getMarca().getCodigo(), item.getId().getJob());
                                                        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EXCLUIR, entidadeSelecionada.get().getStringCodcli(),
                                                                "Excluído a regra/meta " + item.getId().toView());

                                                        MessageBox.create(message -> {
                                                            message.message("Regra/Meta excluída com sucesso.");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.position(Pos.TOP_RIGHT);
                                                            message.notification();
                                                        });
                                                        table.items.remove(item);
                                                    } catch (SQLException e) {
                                                        e.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(e);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                }
                                            });
                                            btnCopiarRegraMarca.setOnAction(evt -> {
                                            });
                                            boxButtonsRow.getChildren().clear();
                                            boxButtonsRow.getChildren().addAll(btnExcluirRegraMarca);
                                            setGraphic(boxButtonsRow);
                                        }
                                    }
                                };
                            });
                        }).build()
                );
            });
            tabPaneMarcasMetas.addTab(FormTab.create(tab -> {
                tab.setText(marca.toString());
                tab.add(tblMarca.build());
//                tab.add(FormBox.create(toolbar -> {
//                    toolbar.horizontal();
//                    toolbar.add(FormButton.create(btnAdicionarMeta -> {
//                        btnAdicionarMeta.title("Adicionar");
//                        btnAdicionarMeta.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
//                        btnAdicionarMeta.addStyle("success");
//                        btnAdicionarMeta.disable.bind(emEdicao.not());
//                        btnAdicionarMeta.setAction(evt -> {
//
//                        });
//                    }));
//                }));
            }));
        });
    }
    // fim tab metas

    private void initBoxAnexos() {
        boxViewAnexo.vertical();
        boxViewAnexo.expanded();
        boxViewAnexo.title("Visualização do Arquivo");
        boxAnexos.getChildren().add(FormBox.create(boxButtons -> {
            boxButtons.horizontal();
            boxButtons.add(FormButton.create(add -> {
                add.setGraphic(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEW_REGISTER, ImageUtils.IconSize._24));
                add.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                add.disableProperty().bind(emEdicao.not());
                add.setAction(evt -> {
                    FileChooser fileChooser = new FileChooser();
                    File fileCopy = fileChooser.showOpenDialog(Globals.getMainStage());
                    if (fileCopy != null) {
                        try {
                            File directory = new File("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()));
                            if (!directory.exists())
                                directory.mkdir();

                            Files.copy(fileCopy.toPath(),
                                    (new File("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()) + "\\" + fileCopy.getName())).toPath(),
                                    StandardCopyOption.REPLACE_EXISTING);
                            loadAnexos(StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()));
                        } catch (IOException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    }
                });
            }));
            boxButtons.add(FormButton.create(delete -> {
                delete.setGraphic(ImageUtils.getIconButton(ImageUtils.ButtonIcon.DELETE_REGISTER, ImageUtils.IconSize._24));
                delete.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                delete.disableProperty().bind(emEdicao.not());
                delete.setAction(evt -> {
                    Map<String, String> selectFileTable = tblAnexos.selectedItem();
                    if (selectFileTable == null)
                        return;

                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o arquivo?");
                        message.showAndWait();
                    }).value.get()) {
                        File file = new File("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()) + "\\" + selectFileTable.get("nome"));
                        file.delete();
                        loadAnexos(StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()));
                    }
                });
            }));
        }));
        boxAnexos.getChildren().add(FormBox.create(boxTableView -> {
            boxTableView.horizontal();
            boxTableView.expanded();
            boxTableView.add(tblAnexos.build());
            boxTableView.add(boxViewAnexo.build());
        }));
    }

    private ImageView imgStatus(Boolean status) {
        ImageView imgActived = new ImageView(new Image(getClass().getResource("/images/icons/buttons/active (1).png").toExternalForm()));
        imgActived.setFitHeight(16.0);
        imgActived.setFitWidth(16.0);
        ImageView imgInactived = new ImageView(new Image(getClass().getResource("/images/icons/buttons/inactive (1).png").toExternalForm()));
        imgActived.setFitHeight(16.0);
        imgActived.setFitWidth(16.0);

        return status ? imgActived : imgInactived;
    }

    private void actionVisualizarEntidade(Entidade entidade) {
        carregaEntidade(entidade);
        tabPaneRoot.getSelectionModel().select(1);
    }

    private void actionEditarEntidade(Entidade entidade) {
        carregaEntidade(entidade);
        emEdicao.set(true);
        tabPaneRoot.getSelectionModel().select(1);

        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidade.getStringCodcli(), "Ativado modo edição do cadastro");
        MessageBox.create(message -> {
            message.message("Entidade em modo de edição.");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void actionDeletarEntidade(Entidade entidade) {
        VSdExistCodcli codcli = new FluentDao().selectFrom(VSdExistCodcli.class)
                .where(it -> it.equal("codcli", StringUtils.lpad(entidade.getCodcli().toString(), 5, "0")))
                .singleResult();
        if (codcli != null) {
            MessageBox.create(message -> {
                message.message("Entidade não pode ser excluída, a mesma encontra-se vinculada a outros módulos.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir a entidade selecionada?");
            message.showAndWait();
        }).value.get()) {
            new FluentDao().runNativeQueryUpdate("DELETE FROM SD_CONCORRENTES_MARCA_ENT_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM SD_MARCAS_ENTIDADE_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM SD_ENTIDADE_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM CLI_COM_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM ENTIDADE_CERT_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM CONTATO_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM sd_entidade_redes_sociais_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM sd_entidade_condicao_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM sd_entidade_tipo_pagto_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            new FluentDao().runNativeQueryUpdate("DELETE FROM ENTIDADE_001 WHERE CODCLI = '" + StringUtils.lpad(entidade.getCodcli().toString(), 5, "0") + "'");
            SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EXCLUIR, entidade.getStringCodcli(), "Entidade excluída");
            MessageBox.create(message -> {
                message.message("Entidade excluída com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            clearForm();
            entidades.remove(entidade);
        }
    }

    private void clearForm() {
        // <editor-fold defaultstate="collapsed" desc="clear(header)">
        tboxCode.clear();
        tboxCnpj.clear();
        tipoDocumentoEntidadeField.select(0);
        tboxRazaoSocial.clear();
        revisadoCheckBox.setSelected(false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(dados gerais)">
        perfilEntidadeField.select("Cliente");
        tipoEntidadeField.clear();
        statusEntidadeField.clear();
        dtCadastroEntidadeField.value.set(null);
        origemEntidadeField.select(0);
        ativoB2bEntidadeField.value.set(false);
        ativoCatalogoEntidadeField.value.set(false);
        fantasiaEntidadeField.clear();
        inscricaoEntidadeField.clear();
        emailEntidadeField.clear();
        telefoneEntidadeField.clear();
        faxEntidadeField.clear();
        creditoEntidadeField.clear();
        ramoAtividadeEntidadeField.clear();
        sitCliEntidadeField.clear();
        grupoEntidadeField.clear();
        atualizaGradeField.value.set(true);
        avisaBoletoEnviadoField.value.set(true);
        imprimeRemessa.value.set(false);
        atualizaGradeRemessaField.value.set(false);
        imprimeConteudoCaixaField.value.set(false);
        atendeRoboField.value.set(true);
        antecipaProdutoField.value.set(true);
        motivoBloqueioEntidadeField.clear();
        fieldValorMinimoPedidoB2b.clear();
        fieldValorMaximoReserva.setValue("999999");
        fieldValorMinimoReserva.setValue("0");
        fieldFaturasMensal.setValue("999");
        fieldFaturaSeparaMarca.value.set(false);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(endereco)">
        cepEntidadeField.clear();
        cidadeEntidadeField.clear();
        ufEntidadeField.clear();
        paisEntidadeField.clear();
        bairroEntidadeField.clear();
        enderecoEntidadeField.clear();
        numeroEntidadeField.clear();
        complementoEntidadeField.clear();
        cnpjEntregaEntidadeField.clear();
        inscricaoEntregaEntidadeField.clear();
        cepEntregaEntidadeField.clear();
        cidadeEntregaEntidadeField.clear();
        ufEntregaEntidadeField.clear();
        bairroEntregaEntidadeField.clear();
        enderecoEntregaEntidadeField.clear();
        numeroEntregaEntidadeField.clear();
        complementoEntregaEntidadeField.clear();
        cnpjCobrancaEntidadeField.clear();
        inscricaoCobrancaEntidadeField.clear();
        cepCobrancaEntidadeField.clear();
        cidadeCobrancaEntidadeField.clear();
        ufCobrancaEntidadeField.clear();
        bairroCobrancaEntidadeField.clear();
        enderecoCobrancaEntidadeField.clear();
        numeroCobrancaEntidadeField.clear();
        telefoneCobrancaEntidadeField.clear();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(complementares)">
        rgEntidadeField.clear();
        emissorRgEntidadeField.clear();
        sexoEntidadeField.select(0);
        dataNascEntidadeField.value.set(null);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(marcas)">
        marcasTabPane.getTabs().clear();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(infos auxiliares)">
        naturezaField.clear();
        dataFundacaoField.value.set(null);
        classeGerencialField.clear();
        historicoField.clear();
        transportadoraField.clear();
        redespachoField.clear();
        pagadorTransportadoraField.select(0);
        pagadorRedespachoField.select(0);
        bancoField.clear();
        contaField.clear();
        agenciaField.clear();
        situacaoDuplicataField.clear();
        tablePrecoField.clear();
        condicaoPagamentoField.clear();
        descontoField.clear();
        diasParaEntregaField.clear();
        suframaField.clear();
        contaClienteField.clear();
        contaFornecedorField.clear();
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(contato)">
        tblContatos.setItems(FXCollections.observableArrayList());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="clear(observacoes)">
        tboxObservacaoComercial.clear();
        tboxObservacaoFinanceiro.clear();
        tboxObservacaoNfe.clear();
        // </editor-fold>
        entidadeSelecionada.set(null);
        entidadeOriginal.set(null);
        sdEntidadeOriginal.set(null);
        marcaB2bEntidade.clear();
        condicoesB2b.clear();
        tiposPagtoB2b.clear();
        boxMarcasB2b.clear();
    }

    private void carregaEntidade(Entidade entidade) {
        if (entidade == null)
            return;

        clearForm();
        JPAUtils.refresh(entidade);
        entidadeOriginal.set(new Entidade(entidade));
        sdEntidadeOriginal.set(new SdEntidade(entidade.getSdEntidade()));
        entidadeSelecionada.set(entidade);

        // <editor-fold defaultstate="collapsed" desc="Contato: Lista">
        ListProperty contatosEntidade = new SimpleListProperty(FXCollections.observableArrayList());
        contatosEntidade.addAll(entidade.getContatos());
        tblContatos.setItems(contatosEntidade);
        tblContatos.refresh();
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Manutenção: Header">
        tboxCode.setText(entidade.getCodcli().toString());
        if (entidade.getTipo().equals("2")) {
            tipoDocumentoEntidadeField.select(0);
        } else {
            tipoDocumentoEntidadeField.select(1);
        }
        tboxCnpj.setText(entidade.getCnpj());
        tboxRazaoSocial.setText(entidade.getNome());
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dados Gerais: Header">
        if (entidade.getTipoEntidade().contains("C")) {
            tipoEntidadeField.items.get(0).setSelected(true);
        }
        if (entidade.getTipoEntidade().contains("F")) {
            tipoEntidadeField.items.get(1).setSelected(true);
        }
        if (entidade.getTipoEntidade().contains("T")) {
            tipoEntidadeField.items.get(2).setSelected(true);
        }
        if (entidade.getAtivo()) {
            statusEntidadeField.items.get(0).setSelected(true);
        }
        if (entidade.getBloqueio()) {
            statusEntidadeField.items.get(1).setSelected(true);
        }
        dtCadastroEntidadeField.value.set(entidade.getDataCad());
        if (entidade.getSdEntidade().getOrigem().equals("ERP")) {
            origemEntidadeField.select(0);
        } else if (entidade.getSdEntidade().getOrigem().equals("B2B")) {
            origemEntidadeField.select(1);
        } else if (entidade.getSdEntidade().getOrigem().equals("B2C")) {
            origemEntidadeField.select(2);
        }
        revisadoCheckBox.setSelected(entidade.getSdEntidade().getDataRevisao() != null);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dados Gerais: Cadastro">
        fantasiaEntidadeField.value.set(entidade.getFantasia());
        inscricaoEntidadeField.value.set(entidade.getInscricao());
        emailEntidadeField.value.set(entidade.getEmail());
        telefoneEntidadeField.value.set(entidade.getTelefone());
        faxEntidadeField.value.set(entidade.getFax());
        creditoEntidadeField.value.set(StringUtils.toDecimalFormat((entidade.getCredito() == null ? BigDecimal.ZERO : entidade.getCredito()).doubleValue(), 2));
        ramoAtividadeEntidadeField.value.set(entidade.getAtividade());
        sitCliEntidadeField.value.set(entidade.getSitCli());
        grupoEntidadeField.value.set(entidade.getGrupo());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dados Gerais: Adicionais">
        atualizaGradeField.value.set(entidade.getSdEntidade().isAtualizaGrade());
        avisaBoletoEnviadoField.value.set(entidade.getSdEntidade().isAvisaBoleto());
        imprimeRemessa.value.set(entidade.getSdEntidade().isImprimeRemessa());
        atualizaGradeRemessaField.value.set(entidade.getSdEntidade().isAtualizaGradeRemessa());
        imprimeConteudoCaixaField.value.set(entidade.getSdEntidade().isImprimeConteudoCaixa());
        atendeRoboField.value.set(entidade.getSdEntidade().isAtendeRobo());
        antecipaProdutoField.value.set(entidade.getSdEntidade().isAntecipaProduto());
        motivoBloqueioEntidadeField.value.set(entidade.getCodMot());
        sitcliMagentoField.value.set(entidade.getSdEntidade().getSitcliB2b());
        fieldValorMinimoPedidoB2b.value.set(String.valueOf(entidade.getSdEntidade().getMinimoB2b()));
        fieldValorMinimoReserva.value.set(String.valueOf(entidade.getSdEntidade().getMinimoReserva()));
        fieldValorMaximoReserva.value.set(String.valueOf(entidade.getSdEntidade().getMaximoReserva()));
        fieldFaturasMensal.value.set(String.valueOf(entidade.getSdEntidade().getFaturamentosMensais()));
        fieldFaturaSeparaMarca.value.set(entidade.getSdEntidade().isSeparaMarcasFatura());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dados Gerais: Endereço">
        if (entidade.getCep() != null)
            cepEntidadeField.value(entidade.getCep());
        if (entidade.getCepEnt() != null)
            cepEntregaEntidadeField.value(entidade.getCepEnt());
        if (entidade.getCepCob() != null)
            cepCobrancaEntidadeField.value(entidade.getCepCob());
        enderecoEntidadeField.value.set(entidade.getEndereco());
        numeroEntidadeField.value.set(entidade.getNumEnd());
        bairroEntidadeField.value.set(entidade.getBairro());
        complementoEntidadeField.value.set(entidade.getComplemento());
        cnpjEntregaEntidadeField.value.set(entidade.getCnpjEnt());
        inscricaoEntregaEntidadeField.value.set(entidade.getInscEnt());
        enderecoEntregaEntidadeField.value.set(entidade.getEndEnt());
        numeroEntregaEntidadeField.value.set(entidade.getNumEnt());
        bairroEntregaEntidadeField.value.set(entidade.getBairroEnt());
        complementoEntregaEntidadeField.value.set(entidade.getCompEnt());
        cnpjCobrancaEntidadeField.value.set(entidade.getCnpjCob());
        inscricaoCobrancaEntidadeField.value.set(entidade.getInscCob());
        enderecoCobrancaEntidadeField.value.set(entidade.getEndCob());
        numeroCobrancaEntidadeField.value.set(entidade.getNumCob());
        bairroCobrancaEntidadeField.value.set(entidade.getBairroCob());
        telefoneCobrancaEntidadeField.value.set(entidade.getFoneCob());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dados Gerais: Complementares">
        rgEntidadeField.value.set(entidade.getNumRg());
        emissorRgEntidadeField.value.set(entidade.getEmiRg());
        dataNascEntidadeField.value.set(entidade.getDtNasc());
        sexoEntidadeField.select(entidade.getSexo() == null || entidade.getSexo().equals("M") ? 0 : 1);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Informações Auxiliares: Geral">
        dataFundacaoField.value.set(entidade.getDataFund());
        naturezaField.value.set(entidade.getNatureza());
        classeGerencialField.value.set(entidade.getClasse());
        historicoField.value.set(entidade.getHistorico());
        transportadoraField.value.set(entidade.getTransporte());
        redespachoField.value.set(entidade.getRedesp());
        pagadorTransportadoraField.select(entidade.getCif()).value.set(entidade.getCif());
        pagadorRedespachoField.select(entidade.getRedespCif()).value.set(entidade.getRedespCif());
        bancoField.value.set(entidade.getBanco());
        situacaoDuplicataField.value.set(entidade.getSitDup());
        tablePrecoField.value.set(entidade.getTabela());
        condicaoPagamentoField.value.set(entidade.getSdEntidade().getCondPagto());
        suframaField.value.set(entidade.getSuframa() != null ? entidade.getSuframa() : "");
        contaClienteField.value.set(entidade.getContac());
        contaFornecedorField.value.set(entidade.getContad());
        tipoTributacaoField.select(entidade.getSimples());
        inscricaoStField.value.set(entidade.getInscEst());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Marca: Adiciona">
        boxMarcasB2b.clear();
        entidade.getMarcas().forEach(sdMarcasEntidade -> {
            addMarcaEntidade(sdMarcasEntidade);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Observações: Adiciona">
        tboxObservacaoComercial.setText(entidade.getObs());
        tboxObservacaoFinanceiro.setText(entidade.getObsFinan());
        tboxObservacaoNfe.setText(entidade.getObsNota());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Controle Lógica">
        sendEcommerce.set(entidade.getSdEntidade().isLiberadoB2b());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Carrega Arquivos">
        loadAnexos(entidade.getCnpj());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Carrega Redes Sociais">
        redesSociais.set(FXCollections.observableList((List<SdEntidadeRedesSociais>) new FluentDao().selectFrom(SdEntidadeRedesSociais.class)
                .where(eb -> eb.equal("codcli", StringUtils.lpad(entidade.getCodcli(), 5, "0")))
                .resultList()));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Carrega B2B">
        ativoB2bEntidadeField.value.set(entidade.getSdEntidade().isLiberadoB2b());
        ativoCatalogoEntidadeField.value.set(entidade.getSdEntidade().isLiberadoCatalogo());
        condicoesB2b.set(FXCollections.observableList((List<SdEntidadeCondicao>) new FluentDao().selectFrom(SdEntidadeCondicao.class)
                .where(eb -> eb.equal("codcli", StringUtils.lpad(entidade.getCodcli(), 5, "0")))
                .resultList()));
        tiposPagtoB2b.set(FXCollections.observableList((List<SdEntidadeTipoPagto>) new FluentDao().selectFrom(SdEntidadeTipoPagto.class)
                .where(eb -> eb.equal("codcli", StringUtils.lpad(entidade.getCodcli(), 5, "0")))
                .resultList()));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Carrega Metas/Regras">
        this.carregarMetas(entidadeSelecionada.get().getStringCodcli());
        // </editor-fold>
    }

    private void loadAnexos(String cnpj) {
        anexos.clear();
        try (Stream<Path> walk = Files.walk(Paths.get("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(cnpj)))) {

            for (Path path : walk.filter(Files::isRegularFile).collect(Collectors.toList())) {
                if (path.toFile().getName().equals("Thumbs.db"))
                    continue;
                BasicFileAttributes attributes = Files.readAttributes(path, BasicFileAttributes.class);
                Map<String, String> mapFile = new HashMap<>();
                mapFile.put("nome", path.toFile().getName());
                mapFile.put("data", attributes.creationTime().toString().substring(0, 10));
                mapFile.put("tamanho", (attributes.size() / 1024) + " Kb");
                mapFile.put("tipo", FilenameUtils.isExtension(path.toFile().getName(), new String[]{"jpg", "jpeg", "png", "bmp", "bitmap"}) ? "imagem" : FilenameUtils.isExtension(path.toFile().getName(), new String[]{"pdf"}) ? "pdf" : "outros");
                anexos.add(mapFile);
            }
        } catch (NoSuchFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(box -> {
                box.exception(e);
                box.showAndWait();
            });
        }
    }

    private void loadFileAnexo(@NotNull Map<String, String> file) throws MalformedURLException, FileNotFoundException, IOException {
        if (file.get("tipo").equals("imagem")) {
            FileInputStream input = new FileInputStream("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()) + "\\" + file.get("nome"));
            ImageView viewImage = new ImageView(new Image(input));
            VBox.setVgrow(viewImage, Priority.ALWAYS);
            boxViewAnexo.add(viewImage);
            input.close();
        } else if (file.get("tipo").equals("pdf")) {
            //Node viewPdf = PDFView.showPDF(new File("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()) + "\\" + file.get("nome")));
            File filePdf = new File("K:\\TI_ERP\\Arquivos\\documentos\\" + StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()) + "\\" + file.get("nome"));

            PDFDisplayer viewPdf = new PDFDisplayer();
            viewPdf.loadPDF(filePdf);

            Parent parentPdf = viewPdf.toNode();
            VBox.setVgrow(parentPdf, Priority.ALWAYS);
            boxViewAnexo.add(parentPdf);
        } else {
            boxViewAnexo.add(new Label("Tipo de arquivo não disponível para visualização."));
        }
    }

    private void addMarcaEntidade(SdMarcasEntidade marcaEntidade) {
        for (Tab tab : marcasTabPane.getTabs()) {
            if (tab.getId().equals(marcaEntidade.getId().getMarca().getCodigo())) {
                return;
            }
        }
        final FormFieldToggle marcaAtivaField = FormFieldToggle.create(formFieldToggle -> {
            formFieldToggle.title("Ativo");
            formFieldToggle.editable.bind(emEdicao);
            formFieldToggle.value.set(marcaEntidade.isAtivo());
        });
        final FormFieldSingleFind<SitCli> sitCliEntidadeMarcaField = FormFieldSingleFind.create(SitCli.class, sitCliField -> {
            sitCliField.title("Tipo");
            sitCliField.width(220.0);
            sitCliField.disable.bind(emEdicao.not());
            sitCliField.value.set(marcaEntidade.getTipo());
        });
        final FormFieldSingleFind<Represen> representanteEntidadeMarcaField = FormFieldSingleFind.create(Represen.class, representField -> {
            representField.title("Representante");
            representField.width(500.0);
            representField.disable.bind(emEdicao.not());
            representField.value.set(marcaEntidade.getCodrep());
        });
        final FormFieldSingleFind<Represen> consultorEntidadeMarcaField = FormFieldSingleFind.create(Represen.class, representField -> {
            representField.title("Consultor");
            representField.width(400.0);
            representField.disable.bind(emEdicao.not());
            representField.value.set(marcaEntidade.getConsultor());
        });
        final FormFieldRadioButtonGroup<String> prospeccaoEntidadeMarcaField = FormFieldRadioButtonGroup.create(field -> {
            field.title("Prospecção");
            field.editable.bind(emEdicao);
            field.items(
                    field.option("Sem Prospecção", "0", marcaEntidade.getProspeccao().equals("0")),
                    field.option("Em Prospecção", "1", marcaEntidade.getProspeccao().equals("1")),
                    field.option("Prospectado", "2", marcaEntidade.getProspeccao().equals("2")),
                    field.option("Não Prospectado", "3", marcaEntidade.getProspeccao().equals("3"))
            );
            //field.select(0);
            field.select(Integer.valueOf(marcaEntidade.getProspeccao()));
        });
        final FormFieldMultipleFind<SdMarcasConcorrentes001> marcasConcorrentesEntidadeMarcaField = FormFieldMultipleFind.create(SdMarcasConcorrentes001.class, field -> {
            field.editable.bind(emEdicao);
            field.title("Marcas Concorrentes");
            field.width(250.0);
            field.toUpper();
            field.codeReference.set("nome");
        });
        final ListView<SdConcorrentesMarcaEnt001> marcasConcorrentesListView = new ListView();
        marcasConcorrentesListView.setItems(marcaEntidade.getConcorrentes());
        final FormFieldText comissaoFaturamentoRepresentanteField = FormFieldText.create(formFieldText -> {
            formFieldText.mask(FormFieldText.Mask.DOUBLE);
            formFieldText.withoutTitle();
            formFieldText.editable.bind(emEdicao);
            formFieldText.label("Faturamento (%)");
            formFieldText.value.set(marcaEntidade.getComissaoFat().toString().replace(".", ","));
        });
        final FormFieldText comissaoRecebimentoRepresentanteField = FormFieldText.create(formFieldText -> {
            formFieldText.mask(FormFieldText.Mask.DOUBLE);
            formFieldText.withoutTitle();
            formFieldText.editable.bind(emEdicao);
            formFieldText.label("Recebimento (%)");
            formFieldText.value.set(marcaEntidade.getComissaoRec().toString().replace(".", ","));
        });
        final ObjectProperty<SdMarcasEntidade> marcaEntidadeOriginal = new SimpleObjectProperty<>(null);

        TextFields.bindAutoCompletion(marcasConcorrentesEntidadeMarcaField.code, ((List<SdMarcasConcorrentes001>) new FluentDao().selectFrom(SdMarcasConcorrentes001.class).get().resultList()).stream().map(SdMarcasConcorrentes001::getNome).distinct().collect(Collectors.toList()));
        // add for send to B2B ecommerce
        if ((marcaEntidade.getId().getMarca().getCodigo().equals("D") ||
                marcaEntidade.getId().getMarca().getCodigo().equals("F")) && marcaEntidade.isAtivo())
            marcaB2bEntidade.add(marcaEntidade.getId().getMarca().getCodigo().equals("D") ? "92" : "93");

        Tab tabMarca = new Tab(marcaEntidade.getId().getMarca().getDescricao());
        tabMarca.setClosable(false);
        tabMarca.setId(marcaEntidade.getId().getMarca().getCodigo());
        tabMarca.setContent(FormBox.create(formBox -> {
            formBox.horizontal();
            formBox.border();
            formBox.add(FormBox.create(formBox1 -> {
                formBox1.vertical();
                formBox1.expanded();
                formBox1.add(FormBox.create(formBox2 -> {
                    formBox2.horizontal();
                    formBox2.add(marcaAtivaField.build());
                    formBox2.add(sitCliEntidadeMarcaField.build());
                    formBox2.add(representanteEntidadeMarcaField.build());
                }));
                formBox1.add(FormBox.create(formBox2 -> {
                    formBox2.horizontal();
                    formBox2.add(prospeccaoEntidadeMarcaField.build());
                    formBox2.add(FormBox.create(boxMarcasConcorrentes -> {
                        boxMarcasConcorrentes.vertical();
                        boxMarcasConcorrentes.alignment(Pos.BOTTOM_RIGHT);
                        boxMarcasConcorrentes.add(FormBox.create(boxAddMarcaConcorrente -> {
                            boxAddMarcaConcorrente.horizontal();
                            boxAddMarcaConcorrente.alignment(Pos.BOTTOM_RIGHT);
                            boxAddMarcaConcorrente.add(marcasConcorrentesEntidadeMarcaField.build());
                            boxAddMarcaConcorrente.add(FormButton.create(btnAddMarcaConcorrente -> {
                                btnAddMarcaConcorrente.icon(new Image(getClass().getResource("/images/icons/buttons/new register (1).png").toExternalForm()));
                                btnAddMarcaConcorrente.addStyle("success");
                                btnAddMarcaConcorrente.disable.bind(emEdicao.not());
                                btnAddMarcaConcorrente.setAction(evtAddMarcarConcorrente -> {
                                    List<SdMarcasConcorrentes001> marcasToAdd = marcasConcorrentesEntidadeMarcaField.objectValues.getValue().stream().filter(it -> it.getCodigo() != 0)
                                            .filter(eb -> eb.getMarca().getCodigo().equals(marcaEntidade.getId().getMarca().getCodigo())).collect(Collectors.toList());
                                    marcasToAdd.removeIf(marcaToAdd -> marcasConcorrentesListView.getItems().stream().anyMatch(marca -> marcaToAdd.equals(marca.getId().getMarcaConcorrente())));
                                    marcasToAdd.forEach(marcaToAdd -> {
                                        SdConcorrentesMarcaEnt001 marcaConcorrente = new SdConcorrentesMarcaEnt001(marcaEntidade.getId(), marcaToAdd);
                                        try {
                                            if (novaEntidade.not().get()) {
                                                daoConcorrentesMarcaEntidade.save(marcaConcorrente);
                                            }
                                            marcaEntidade.getConcorrentes().add(marcaConcorrente);
                                            MessageBox.create(message -> {
                                                message.message("Marca concorrente adicionada");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                    marcasConcorrentesEntidadeMarcaField.clear();
//                                    if (marcasConcorrentesEntidadeMarcaField.objectValues.getValue().stream().noneMatch(it -> it.getCodigo() == 0)) {
//                                        for (SdConcorrentesMarcaEnt001 item : marcasConcorrentesListView.getItems()) {
//                                            if (item.getId().getMarcaConcorrente().getCodigo() == marcasConcorrentesEntidadeMarcaField.value.getValue().getCodigo()) {
//                                                return;
//                                            }
//                                        }
//                                        SdConcorrentesMarcaEnt001 marcaConcorrente = new SdConcorrentesMarcaEnt001(marcaEntidade.getId(), marcasConcorrentesEntidadeMarcaField.value.getValue());
//                                        try {
//                                            if (novaEntidade.not().get()) {
//                                                daoConcorrentesMarcaEntidade.save(marcaConcorrente);
//                                                marcaEntidade.getConcorrentes().add(marcaConcorrente);
//                                            } else
//                                                marcaEntidade.getConcorrentes().add(marcaConcorrente);
//                                            MessageBox.create(message -> {
//                                                message.message("Marca concorrente adicionada");
//                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                                message.position(Pos.TOP_RIGHT);
//                                                message.notification();
//                                            });
//                                        } catch (SQLException e) {
//                                            e.printStackTrace();
//                                            ExceptionBox.build(message -> {
//                                                message.exception(e);
//                                                message.showAndWait();
//                                            });
//                                        }
//                                    } else {
//                                        MessageBox.create(messageBox -> {
//                                            messageBox.message("Você deve selecionar uma marca para adicionar.");
//                                            messageBox.type(MessageBox.TypeMessageBox.IMPORTANT);
//                                        }).showAndWait();
//                                    }
                                });
                            }));
                        }));
                        boxMarcasConcorrentes.add(marcasConcorrentesListView);
                        boxMarcasConcorrentes.add(FormButton.create(btnDeleteMarcaConcorrente -> {
                            btnDeleteMarcaConcorrente.icon(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                            btnDeleteMarcaConcorrente.title("Excluir");
                            btnDeleteMarcaConcorrente.addStyle("danger");
                            btnDeleteMarcaConcorrente.disable.bind(marcasConcorrentesListView.getSelectionModel().selectedItemProperty().isNull().or(emEdicao.not()));
                            btnDeleteMarcaConcorrente.setAction(evtAddMarcarConcorrente -> {
                                SdConcorrentesMarcaEnt001 marcaSelecionada = marcasConcorrentesListView.getSelectionModel().getSelectedItem();
                                if (marcaSelecionada != null) {
                                    try {
                                        if (novaEntidade.not().get())
                                            daoConcorrentesMarcaEntidade.delete(marcaSelecionada.getId());
                                        else
                                            marcaEntidade.getConcorrentes().remove(marcaSelecionada);
                                        marcasConcorrentesListView.getItems().remove(marcaSelecionada);
                                        MessageBox.create(message -> {
                                            message.message("Marca concorrente excluída");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                }
                            });
                        }));
                    }));
                    formBox2.add(FormBox.create(boxComissoes -> {
                        boxComissoes.vertical();
                        boxComissoes.add(FormBox.create(boxComissoesRep -> {
                            boxComissoesRep.vertical();
                            boxComissoesRep.width(170.0);
                            boxComissoesRep.title("Comissões Representante");
                            boxComissoesRep.add(comissaoFaturamentoRepresentanteField.build());
                            boxComissoesRep.add(comissaoRecebimentoRepresentanteField.build());
                        }));
                        boxComissoes.add(consultorEntidadeMarcaField.build());
                    }));
                }));
            }));
            formBox.add(FormBox.create(formBox2 -> {
                formBox2.horizontal();
                formBox2.add(FormBox.create(formBox1 -> {
                    formBox1.vertical();
                    formBox1.alignment(Pos.BOTTOM_RIGHT);
                    formBox1.add(FormButton.create(button -> {
                        button.title("Salvar Marca");
                        button.disable.bind(emEdicao.not());
                        button.icon(new Image(getClass().getResource("/images/icons/buttons/select (2).png").toExternalForm()));
                        button.addStyle("success");
                        button.addStyle("lg");
                        button.setAction(eventMarca -> {
                            try {
                                marcaEntidadeOriginal.set(new SdMarcasEntidade(marcaEntidade));
                                marcaEntidade.setProspeccao(prospeccaoEntidadeMarcaField.value.getValue().valueOption);
                                marcaEntidade.setAtivo(marcaAtivaField.value.getValue());
                                marcaEntidade.setCodrep(representanteEntidadeMarcaField.validate().value.getValue());
                                marcaEntidade.setConsultor(consultorEntidadeMarcaField.validate().value.getValue());
                                marcaEntidade.setComissaoFat(new BigDecimal(comissaoFaturamentoRepresentanteField.value.getValue().replace(",", ".")));
                                marcaEntidade.setComissaoRec(new BigDecimal(comissaoRecebimentoRepresentanteField.value.getValue().replace(",", ".")));
                                marcaEntidade.setTipo(sitCliEntidadeMarcaField.validate().value.getValue());

                                if (!marcaEntidade.isAtivo() && marcaB2bEntidade.contains(marcaEntidade.getId().getMarca().getCodigo().equals("D") ? "92" : "93"))
                                    marcaB2bEntidade.remove(marcaEntidade.getId().getMarca().getCodigo().equals("D") ? "92" : "93");

                                SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(), "Alterando " + marcaEntidadeOriginal.get().diffObject(marcaEntidade));
                                salvarMarcaEntidade(marcaEntidade);
                            } catch (FormValidationException ex) {
                                ex.printStackTrace();
                                MessageBox.create(message -> {
                                    message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                            }
                        });
                    }));
                    formBox1.add(FormButton.create(button -> {
                        button.title("Excluir Marca");
                        button.icon(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                        button.addStyle("danger");
                        button.disable.bind(emEdicao.not());
                        button.setAction(eventMarca -> {
                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Deseja relamente excluir a marca do cliente?");
                                message.showAndWait();
                            }).value.get()) {
                                marcaEntidade.getConcorrentes().forEach(marca -> {
                                    try {
                                        daoConcorrentesMarcaEntidade.delete(marca.getId());
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                });

                                try {
                                    daoMarcasEntidade.delete(marcaEntidade.getId());

                                    boxMarcasB2b.itens().removeIf(marca -> marca.getId().equals(marcaEntidade.getId().getMarca().getCodigo()));
                                    entidadeSelecionada.get().getMarcas().remove(marcaEntidade);
                                    salvarMarcaCliCom(marcaEntidade);
                                    SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(), "Excluído marca " + marcaEntidade.getId().getMarca().getDescricao() + " no cadastro do cliente");

                                    entidadeSelecionada.get().getMarcasEntidade().remove(marcaEntidade);
                                    marcasNovaEntidade.remove(marcaEntidade);
                                } catch (SQLException | NoResultException e) {
                                    e.printStackTrace();
                                }
                                marcasTabPane.getTabs().remove(tabMarca);

                                MessageBox.create(message -> {
                                    message.message("Marca " + marcaEntidade.getId().getMarca().getDescricao() + " excluída do cliente.");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                            }
                        });
                    }));
                }));
            }));
        }));
        marcasTabPane.getTabs().add(tabMarca);
        marcasTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && newValue != null && marcasTabPane.getTabs().contains(oldValue) && oldValue.equals(tabMarca) && !newValue.equals(tabMarca)) {
                SdMarcasEntidade objToCompare = new SdMarcasEntidade(marcaEntidade);
                objToCompare.setProspeccao(prospeccaoEntidadeMarcaField.value.getValue().valueOption);
                objToCompare.setAtivo(marcaAtivaField.value.getValue());
                objToCompare.setCodrep(representanteEntidadeMarcaField.value.getValue());
                objToCompare.setConsultor(consultorEntidadeMarcaField.value.getValue());
                objToCompare.setComissaoFat(new BigDecimal(comissaoFaturamentoRepresentanteField.value.getValue().replace(",", ".")));
                objToCompare.setComissaoRec(new BigDecimal(comissaoRecebimentoRepresentanteField.value.getValue().replace(",", ".")));
                objToCompare.setTipo(sitCliEntidadeMarcaField.value.getValue());
                if (!objToCompare.equals(marcaEntidade)) {
                    marcasTabPane.getSelectionModel().select(tabMarca);
                    MessageBox.create(message -> {
                        message.message("Você fez alterações na marca, antes de mudar de aba salve a marca!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                }
            }
        });

        boxMarcasB2b.add(FormBox.create(boxDados -> {
            boxDados.horizontal();
            boxDados.setId(marcaEntidade.getId().getMarca().getCodigo());
            boxDados.add(FormFieldText.create(field -> {
                field.title("Marca");
                field.value.set(marcaEntidade.getId().getMarca().getDescricao());
                field.editable.set(false);
            }).build());
            boxDados.add(FormFieldToggle.create(formFieldToggle -> {
                formFieldToggle.title("Ativa");
                formFieldToggle.editable.bind(emEdicao.and(ativoB2bEntidadeField.value.or(ativoCatalogoEntidadeField.value)));
                formFieldToggle.value.set(marcaEntidade.isAtivoB2b());
                formFieldToggle.changed((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        try {
                            new NativeDAO().runNativeQueryUpdate("update sd_marcas_entidade_001 set ativo_b2b = '%s' where marca = '%s' and codcli = '%s'",
                                    (newValue ? "S" : "N"), marcaEntidade.getId().getMarca().getCodigo(), StringUtils.lpad(marcaEntidade.getId().getCodcli(), 5, "0"));
                            marcaEntidade.setAtivoB2b(newValue);
                            MessageBox.create(message -> {
                                message.message((newValue ? "A" : "Ina") + "tivado a marca " + marcaEntidade.getId().getMarca().getDescricao() + " para o cliente na plataforma B2B");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    }
                });
            }).build());
            boxDados.add(FormFieldSingleFind.create(SdMagentoSitCli.class, field -> {
                field.title("Grupo");
                field.editable.bind(emEdicao.and(ativoB2bEntidadeField.value.or(ativoCatalogoEntidadeField.value)));
                field.width(300.0);
                if (marcaEntidade.getGrupoB2b() != null)
                    field.setDefaultCode(String.valueOf(marcaEntidade.getGrupoB2b().getCodigo()));
                else if (entidadeSelecionada.get().getSdEntidade().getSitcliB2b() != null)
                    field.setDefaultCode(String.valueOf(entidadeSelecionada.get().getSdEntidade().getSitcliB2b().getCodigo()));

                field.postSelected((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        try {
                            new NativeDAO().runNativeQueryUpdate("update sd_marcas_entidade_001 set grupo_b2b = '%s' where marca = '%s' and codcli = '%s'",
                                    ((SdMagentoSitCli) field.value.get()).getCodigo(), marcaEntidade.getId().getMarca().getCodigo(), StringUtils.lpad(marcaEntidade.getId().getCodcli(), 5, "0"));
                            marcaEntidade.setGrupoB2b((SdMagentoSitCli) field.value.get());
                            MessageBox.create(message -> {
                                message.message("Incluindo a marca " + marcaEntidade.getId().getMarca().getDescricao() + " no grupo " + ((SdMagentoSitCli) field.value.get()).getDescricao() + " para o cliente na plataforma B2B.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    }
                });
            }).build());
        }));

        tabPaneCadastro.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && oldValue.equals(tabMarcas) && newValue != null && !newValue.equals(tabMarcas) && marcasTabPane.getSelectionModel().getSelectedItem() != null && marcasTabPane.getSelectionModel().getSelectedItem().equals(tabMarca)) {
                SdMarcasEntidade objToCompare = new SdMarcasEntidade(marcaEntidade);
                objToCompare.setProspeccao(prospeccaoEntidadeMarcaField.value.getValue().valueOption);
                objToCompare.setAtivo(marcaAtivaField.value.getValue());
                objToCompare.setCodrep(representanteEntidadeMarcaField.value.getValue());
                objToCompare.setConsultor(consultorEntidadeMarcaField.value.getValue());
                objToCompare.setComissaoFat(new BigDecimal(comissaoFaturamentoRepresentanteField.value.getValue().replace(",", ".")));
                objToCompare.setComissaoRec(new BigDecimal(comissaoRecebimentoRepresentanteField.value.getValue().replace(",", ".")));
                objToCompare.setTipo(sitCliEntidadeMarcaField.value.getValue());
                if (!objToCompare.equals(marcaEntidade)) {
                    tabPaneCadastro.getSelectionModel().select(tabMarcas);
                    MessageBox.create(message -> {
                        message.message("Você fez alterações na marca, antes de mudar de aba salve a marca!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                }
            }
        });
    }

    private void salvarMarcaEntidade(SdMarcasEntidade marcaEntidade) {
        if (novaEntidade.not().get()) {
            new FluentDao().merge(marcaEntidade);
            if (marcaEntidade.isMarcaNova()) {
                entidadeSelecionada.get().getMarcas().add(marcaEntidade);
                //entidadeSelecionada.get().getMarcasEntidade().add(marcaEntidade);
            }

            //Salvando a CLI_COM
            salvarMarcaCliCom(marcaEntidade);

            SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(), "Salvo marca " + marcaEntidade.getId().getMarca().getDescricao() + " no cadastro do cliente");
        } else {
            marcasNovaEntidade.add(marcaEntidade);
        }

        MessageBox.create(message -> {
            message.message("Marca " + marcaEntidade.getId().getMarca().getDescricao() + " cadastrada para o cliente.");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarMarcaCliCom(SdMarcasEntidade marcaEntidade) {
        List<String> repsMarcas = entidadeSelecionada.get().getMarcas().stream().map(marca -> marca.getCodrep().getCodRep()).distinct().collect(Collectors.toList());
        new FluentDao().deleteAll((List<CliCom>) new FluentDao().selectFrom(CliCom.class).where(it -> it.equal("id.codcli", StringUtils.lpad(entidadeSelecionada.get().getCodcli().toString(), 5, "0"))).resultList());
        new FluentDao().runNativeQueryUpdate(String.format("DELETE FROM CLI_COM_TEMP WHERE CODCLI = '%s'", StringUtils.lpad(entidadeSelecionada.get().getCodcli().toString(), 5, "0")));
        if (repsMarcas.size() == 1 && entidadeSelecionada.get().getMarcas().size() > 1) {
            CliCom cliCom = new CliCom(StringUtils.lpad(entidadeSelecionada.get().getCodcli().toString(), 5, "0"), repsMarcas.get(0),
                    "1",
                    marcaEntidade.getComissaoFat(), marcaEntidade.getComissaoRec());
            new FluentDao().runNativeQueryUpdate(String.format("INSERT INTO CLI_COM_001 VALUES ('%s','%s', %s, %s, '%s')",
                    cliCom.getId().getCodcli(),
                    cliCom.getId().getCodrep(),
                    cliCom.getCom1(),
                    cliCom.getCom2(),
                    cliCom.getMarca()
            ));
            /*try {
                JPAUtils.getEntityManager().detach(cliCom);
                new FluentDao().persist(cliCom);
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }*/
        } else {
            entidadeSelecionada.get().getMarcasEntidade().forEach(marca -> {
                CliCom cliCom = new CliCom(StringUtils.lpad(entidadeSelecionada.get().getCodcli().toString(), 5, "0"), marca.getCodrep().getCodRep(),
                        marca.getId().getMarca().getCodigo(),
                        marca.getComissaoFat(), marca.getComissaoRec());
                try {
                    new FluentDao().persist(cliCom);
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            });
        }
    }

    private void atualizaObjecto(@NotNull Entidade entidade) throws FormValidationException {
        entidade.setTipo(tipoDocumentoEntidadeField.value.getValue().valueOption);
        ValidationForm.validationEmpty(tboxCnpj, "CNPJ");
        entidade.setCnpj(tboxCnpj.getText());
        ValidationForm.validationEmpty(tboxRazaoSocial, "Razão");
        entidade.setNome(tboxRazaoSocial.getText().substring(0, (tboxRazaoSocial.getText().length() > 80 ? 79 : tboxRazaoSocial.getText().length())));
        entidade.setTipoEntidade(tipoEntidadeField.selecteds().stream().map(check -> check.valueOption).collect(Collectors.joining("")));
        entidade.setAtivo(statusEntidadeField.items.get(0).isSelected());
        entidade.setBloqueio(statusEntidadeField.items.get(1).isSelected());
        entidade.setDataCad(dtCadastroEntidadeField.value.getValue());
        entidade.setFantasia(fantasiaEntidadeField.value.getValue());
        entidade.setInscricao(inscricaoEntidadeField.validate().value.getValue());
        entidade.setEmail(emailEntidadeField.validate().value.getValue());
        entidade.setTelefone(telefoneEntidadeField.validate().value.getValue());
        entidade.setFax(faxEntidadeField.value.getValue());
        entidade.setCredito(creditoEntidadeField.value.getValue() == null || creditoEntidadeField.value.getValue().equals("") ? null : new BigDecimal(creditoEntidadeField.value.getValue().replace(".", "").replace(",", ".")));
        entidade.setAtividade(ramoAtividadeEntidadeField.validate().value.getValue());
        entidade.setSitCli(sitCliEntidadeField.validate().value.getValue());
        entidade.setGrupo(grupoEntidadeField.value.getValue());
        entidade.setCodMot(motivoBloqueioEntidadeField.value.getValue());
        entidade.setCep(cepEntidadeField.validate().first());
        entidade.setCodPais(paisEntidadeField.value.getValue().getCodPais());
        entidade.setBairro(bairroEntidadeField.validate().value.getValue());
        entidade.setEndereco(enderecoEntidadeField.validate().value.getValue());
        entidade.setNumEnd(numeroEntidadeField.validate().value.getValue());
        entidade.setComplemento(complementoEntidadeField.value.getValue());
        entidade.setCepEnt(cepEntregaEntidadeField.validate().first());
        entidade.setBairroEnt(bairroEntregaEntidadeField.validate().value.getValue());
        entidade.setEndEnt(enderecoEntregaEntidadeField.validate().value.getValue());
        entidade.setNumEnt(numeroEntregaEntidadeField.validate().value.getValue());
        entidade.setCompEnt(complementoEntregaEntidadeField.value.getValue());
        entidade.setCnpjEnt(cnpjEntregaEntidadeField.validate().value.getValue());
        entidade.setInscEnt(inscricaoEntregaEntidadeField.validate().value.getValue());
        entidade.setCepCob(cepCobrancaEntidadeField.validate().first());
        entidade.setBairroCob(bairroCobrancaEntidadeField.validate().value.getValue());
        entidade.setEndCob(enderecoCobrancaEntidadeField.validate().value.getValue());
        entidade.setNumCob(numeroCobrancaEntidadeField.validate().value.getValue());
        entidade.setCnpjCob(cnpjCobrancaEntidadeField.validate().value.getValue());
        entidade.setInscCob(inscricaoCobrancaEntidadeField.validate().value.getValue());
        entidade.setFoneCob(telefoneCobrancaEntidadeField.value.getValue());
        entidade.setNumRg(rgEntidadeField.value.getValue());
        entidade.setEmiRg(emissorRgEntidadeField.value.getValue());
        entidade.setDtNasc(dataNascEntidadeField.value.getValue());
        entidade.setSexo(sexoEntidadeField.value.get().getId());
        entidade.setNatureza(naturezaField.value.getValue());
        entidade.setDataFund(dataFundacaoField.value.getValue());
        entidade.setClasse(classeGerencialField.validate().value.getValue());
        entidade.setHistorico(historicoField.validate().value.getValue());
        entidade.setTransporte(transportadoraField.value.getValue());
        entidade.setCif(pagadorTransportadoraField.value.getValue());
        entidade.setRedesp(redespachoField.value.getValue());
        entidade.setRedespCif(pagadorRedespachoField.value.getValue());
        entidade.setBanco(bancoField.value.getValue());
        entidade.setConta(contaField.value.getValue());
        entidade.setAgencia(agenciaField.value.getValue());
        entidade.setSitDup(situacaoDuplicataField.validate().value.getValue());
        entidade.setTabela(tablePrecoField.value.getValue());
        entidade.setCondicao(condicaoPagamentoField.value.getValue() == null ? null : condicaoPagamentoField.value.getValue().getDescricao().replace("/", " "));
        entidade.setDesconto(descontoField.value.getValue() == null || descontoField.value.getValue().equals("") ? null : new BigDecimal(descontoField.value.getValue().replace(".", "").replace(",", ".")));
        entidade.setDias(diasParaEntregaField.value.getValue() == null || diasParaEntregaField.value.getValue().equals("") ? null : new Integer(diasParaEntregaField.value.getValue().replace(".", "")));
        entidade.setSuframa(suframaField.value.getValue());
        entidade.setContac(contaClienteField.value.getValue());
        entidade.setContad(contaFornecedorField.value.getValue());
        entidade.setSimples(tipoTributacaoField.value.get());
        entidade.setInscEst(inscricaoStField.value.getValue());
        entidade.setObs(tboxObservacaoComercial.getText());
        entidade.setObsFinan(tboxObservacaoFinanceiro.getText());
        entidade.setObsNota(tboxObservacaoNfe.getText());

        if (novaEntidade.get()) {
            complementoNovaEntidade.setCondPagto(condicaoPagamentoField.value.getValue());
            complementoNovaEntidade.setAtualizaGrade(atualizaGradeField.value.getValue());
            complementoNovaEntidade.setAvisaBoleto(avisaBoletoEnviadoField.value.getValue());
            complementoNovaEntidade.setImprimeConteudoCaixa(imprimeConteudoCaixaField.value.getValue());
            complementoNovaEntidade.setAntecipaProduto(antecipaProdutoField.value.getValue());
            complementoNovaEntidade.setAtendeRobo(atendeRoboField.value.getValue());
            complementoNovaEntidade.setPerfil(perfilEntidadeField.value.getValue());
            complementoNovaEntidade.setOrigem(origemEntidadeField.value.get());
            complementoNovaEntidade.setLiberadoB2b(ativoB2bEntidadeField.value.getValue());
            complementoNovaEntidade.setLiberadoCatalogo(ativoCatalogoEntidadeField.value.getValue());
            complementoNovaEntidade.setMinimoB2b(new BigDecimal(fieldValorMinimoPedidoB2b.value.getValue().replace(",", ".")));
            complementoNovaEntidade.setSitcliB2b(ativoB2bEntidadeField.value.get() ? sitcliMagentoField.validate().value.getValue() : sitcliMagentoField.value.getValue());
            complementoNovaEntidade.setMinimoReserva(new BigDecimal(fieldValorMinimoReserva.value.getValue().replace(",", ".")));
            complementoNovaEntidade.setMaximoReserva(new BigDecimal(fieldValorMaximoReserva.value.getValue().replace(",", ".")));
            complementoNovaEntidade.setFaturamentosMensais(new Integer(fieldFaturasMensal.value.get()));
            complementoNovaEntidade.setSeparaMarcasFatura(fieldFaturaSeparaMarca.value.get());
            complementoNovaEntidade.setImprimeRemessa(imprimeRemessa.value.get());
            complementoNovaEntidade.setAtualizaGradeRemessa(atualizaGradeRemessaField.value.get());

            //padrões
            if (marcasNovaEntidade.size() > 0)
                entidade.setCodrep(marcasNovaEntidade.stream().findFirst().get().getCodrep().getCodRep());
        } else {
            entidade.getSdEntidade().setCondPagto(condicaoPagamentoField.value.getValue());
            entidade.getSdEntidade().setAtualizaGrade(atualizaGradeField.value.getValue());
            entidade.getSdEntidade().setAvisaBoleto(avisaBoletoEnviadoField.value.getValue());
            entidade.getSdEntidade().setImprimeConteudoCaixa(imprimeConteudoCaixaField.value.getValue());
            entidade.getSdEntidade().setAntecipaProduto(antecipaProdutoField.value.getValue());
            entidade.getSdEntidade().setAtendeRobo(atendeRoboField.value.getValue());
            entidade.getSdEntidade().setPerfil(perfilEntidadeField.value.getValue());
            entidade.getSdEntidade().setOrigem(origemEntidadeField.value.get());
            entidade.getSdEntidade().setLiberadoB2b(ativoB2bEntidadeField.value.getValue());
            entidade.getSdEntidade().setLiberadoCatalogo(ativoCatalogoEntidadeField.value.getValue());
            entidade.getSdEntidade().setMinimoB2b(new BigDecimal(fieldValorMinimoPedidoB2b.value.getValue().replace(",", ".")));
            entidade.getSdEntidade().setSitcliB2b(ativoB2bEntidadeField.value.get() ? sitcliMagentoField.validate().value.getValue() : sitcliMagentoField.value.getValue());
            entidade.getSdEntidade().setMinimoReserva(new BigDecimal(fieldValorMinimoReserva.value.getValue().replace(",", ".")));
            entidade.getSdEntidade().setMaximoReserva(new BigDecimal(fieldValorMaximoReserva.value.getValue().replace(",", ".")));
            entidade.getSdEntidade().setSeparaMarcasFatura(fieldFaturaSeparaMarca.value.get());
            entidade.getSdEntidade().setImprimeRemessa(imprimeRemessa.value.get());
            entidade.getSdEntidade().setAtualizaGradeRemessa(atualizaGradeRemessaField.value.get());
            entidade.getSdEntidade().setFaturamentosMensais(new Integer(fieldFaturasMensal.value.get()));
            if (revisadoCheckBox.isSelected()) {
                entidade.getSdEntidade().setDataRevisao(LocalDate.now());
            }

            //padrões
            if (entidade.getMarcas().size() > 0)
                entidade.setCodrep(entidade.getMarcas().stream().findFirst().get().getCodrep().getCodRep());
        }
        // padrões
        entidade.setNrloja(atualizaGradeField.value.not().getValue() ? "NPA" : null);
        entidade.setSite(avisaBoletoEnviadoField.value.getValue() ? "S" : null);
    }

    private CustomerStatus convertToCustomerStatus(Entidade entidadeSelecionada) {
        return new CustomerStatus(
                "taxvat",
                entidadeSelecionada.getNome() + " [" + entidadeSelecionada.getCodcli() + "]",
                entidadeSelecionada.getEmail(),
                entidadeSelecionada.getCnpj().replace(".", "").replace("-", "").replace("/", ""),
                entidadeSelecionada.getSdEntidade().isLiberadoB2b() ? marcaB2bEntidade.size() > 0 ? String.valueOf(entidadeSelecionada.getSdEntidade().getSitcliB2b().getCodigo()) : "1" : "1",
                new AdditionalAttribute(marcaB2bEntidade.stream().collect(Collectors.joining(","))));
    }

    private Customer convertToCustomer(Entidade entidadeSelecionada) {
        List<Address> enderecosCliente = new ArrayList<>();
        enderecosCliente.add(new Address(
                entidadeSelecionada.getNome(),
                entidadeSelecionada.getFantasia(),
                entidadeSelecionada.getEndereco(),
                entidadeSelecionada.getNumEnd(),
                entidadeSelecionada.getComplemento(),
                entidadeSelecionada.getBairro(),
                entidadeSelecionada.getCep().getCep(),
                entidadeSelecionada.getCep().getCidade().getNomeCid(),
                entidadeSelecionada.getCep().getCidade().getCodEst().getDescricao(),
                entidadeSelecionada.getTelefone(),
                entidadeSelecionada.getFax(),
                entidadeSelecionada.getCep().getCidade().getCodEst().getId().getSiglaEst(),
                entidadeSelecionada.getCep().getCidade().getCodEst().getCodPais().getCodPais().equals("1058") ? "BR" : "EX",
                "1", "0"
        ));
        enderecosCliente.add(new Address(
                entidadeSelecionada.getNome(),
                entidadeSelecionada.getFantasia(),
                entidadeSelecionada.getEndEnt(),
                entidadeSelecionada.getNumEnt(),
                entidadeSelecionada.getCompEnt(),
                entidadeSelecionada.getBairroEnt(),
                entidadeSelecionada.getCepEnt().getCep(),
                entidadeSelecionada.getCepEnt().getCidade().getNomeCid(),
                entidadeSelecionada.getCepEnt().getCidade().getCodEst().getDescricao(),
                entidadeSelecionada.getTelefone(),
                entidadeSelecionada.getFax(),
                entidadeSelecionada.getCepEnt().getCidade().getCodEst().getId().getSiglaEst(),
                entidadeSelecionada.getCepEnt().getCidade().getCodEst().getCodPais().getCodPais().equals("1058") ? "BR" : "EX",
                "0", "1"
        ));

        return new Customer(
                entidadeSelecionada.getNome() + " [" + entidadeSelecionada.getCodcli() + "]",
                entidadeSelecionada.getFantasia(),
                entidadeSelecionada.getEmail(),
                entidadeSelecionada.getCnpj().replace(".", "").replace("-", "").replace("/", "").substring(0, 7),
                entidadeSelecionada.getCnpj().replace(".", "").replace("-", "").replace("/", ""),
                entidadeSelecionada.getNumRg(),
                entidadeSelecionada.getInscricao(),
                entidadeSelecionada.getDataFund() != null ? entidadeSelecionada.getDataFund().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "2000-01-01",
                entidadeSelecionada.getSdEntidade().isLiberadoB2b() ? marcaB2bEntidade.size() > 0 ? String.valueOf(entidadeSelecionada.getSdEntidade().getSitcliB2b().getCodigo()) : "1" : "1",
                entidadeSelecionada.getSdEntidade().isLiberadoB2b() ? marcaB2bEntidade.size() > 0 ? String.valueOf(entidadeSelecionada.getSdEntidade().getSitcliB2b().getCodigo()) : "1" : "1",
                entidadeSelecionada.getTipo().equals("2") ? "juridica" : "fisica",
                new AdditionalAttribute(marcaB2bEntidade.stream().collect(Collectors.joining(","))),
                enderecosCliente
        );
    }

    private void salvarEntidade() throws SQLException {
        try {
            if (!novaEntidade.get())
                JPAUtils.getEntityManager().refresh(entidadeSelecionada.get());
            atualizaObjecto(entidadeSelecionada.get());

            entidadeSelecionada.set(new FluentDao().merge(entidadeSelecionada.get()));
            if (novaEntidade.get()) {
                tboxCode.setText(entidadeSelecionada.get().getCodcli().toString());
                // atualiza codcli da sd_entidade e atribui a entidade
                complementoNovaEntidade.setCodcli(entidadeSelecionada.get().getCodcli());
                new FluentDao().merge(complementoNovaEntidade);
                // atualiza codcli da sd_marcas_entidade e atribui a entidade
                marcasNovaEntidade.forEach(marca -> {
                    marca.getId().setCodcli(entidadeSelecionada.get().getCodcli());
                    marca.getConcorrentes().forEach(concorrente -> concorrente.getId().setCodcli(entidadeSelecionada.get().getCodcli()));
                    new FluentDao().merge(marca);
                });
                // atualiza codcli da contato e atribui a entidade
                contatosNovaEntidade.forEach(contato -> {
                    contato.setCodcli(entidadeSelecionada.get().getCodcli());
                    try {
                        new FluentDao().persist(contato);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
                //CLI_COM registro
                marcasNovaEntidade.forEach(marca -> {
                    salvarMarcaCliCom(marca);
                });

                //entidadeSelecionada.get().setSdEntidade(complementoNovaEntidade);
                //if (marcasNovaEntidade.size() > 0)
                //    entidadeSelecionada.get().setMarcasEntidade(marcasNovaEntidade);
                //if (contatosNovaEntidade.size() > 0)
                //    entidadeSelecionada.get().getContatosEntidade().addAll(contatosNovaEntidade);
            }

            SysLogger.addSysDelizLog("ENTIDADE", novaEntidade.get() ? TipoAcao.CADASTRAR : TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(),
                    novaEntidade.get()
                            ? "Salvo informações no cadastro da entidade. " + entidadeSelecionada.get().stringToLog().concat(entidadeSelecionada.get().getSdEntidade().stringToLog())
                            : "Entidade alterada. " + entidadeOriginal.get().diffObject(entidadeSelecionada.get()).concat(sdEntidadeOriginal.get().diffObject(entidadeSelecionada.get().getSdEntidade())));
            if (!novaEntidade.get() && revisadoCheckBox.isSelected())
                SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.REVISAR, entidadeSelecionada.get().getStringCodcli(),
                        "Cadastro da Entidade " + entidadeSelecionada.get().getStringCodcli() + " revisado por " + Globals.getNomeUsuario());
            MessageBox.create(message -> {
                message.message("Entidade salva com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            if (novaEntidade.get()) {
                JPAUtils.getEntityManager().refresh(entidadeSelecionada.get());
            }

            if (sendEcommerce.or(entidadeSelecionada.get().getSdEntidade().liberadoB2bProperty().or(entidadeSelecionada.get().getSdEntidade().liberadoCatalogoProperty())).get()) {
                RequestClienteNitro clienteNitro = new RequestClienteNitro(StringUtils.unformatCnpj(entidadeSelecionada.get().getCnpj()),
                        entidadeSelecionada.get().getInscricao(),
                        entidadeSelecionada.get().getNome(),
                        entidadeSelecionada.get().getFantasia(),
                        entidadeSelecionada.get().getTelefone(),
                        entidadeSelecionada.get().getFax(),
                        entidadeSelecionada.get().getEmail(),
                        "S",
                        StringUtils.formatCep(entidadeSelecionada.get().getCep().getCep()),
                        entidadeSelecionada.get().getEndereco(),
                        entidadeSelecionada.get().getNumEnd(),
                        entidadeSelecionada.get().getComplemento(),
                        entidadeSelecionada.get().getBairro(),
                        entidadeSelecionada.get().getCep().getCidade().getNomeCid(),
                        entidadeSelecionada.get().getCep().getCidade().getCodEst().getId().getSiglaEst());

                RequestClienteNitro[] clientes = new RequestClienteNitro[]{clienteNitro};
                RequestSendCliente senderCliente = new RequestSendCliente(Arrays.asList(clientes));
                try {
                    new ServicoNitroEcom().sendCliente(senderCliente);

                    MessageBox.create(message -> {
                        message.message("Entidade enviada para E-commerce B2B.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    MessageBox.create(message -> {
                        message.message(e.getMessage());
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showAndWait();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
//                Rotina de envio de cliente para CONVERTR
//                List<Entidade> entidadesSincronizadas = (List<Entidade>) new FluentDao().selectFrom(Entidade.class)
//                        .where(it -> it.equal("email", entidadeSelecionada.get().getEmail())
//                                .equal("sdEntidade.sincB2b", true)
//                                .notEqual("codcli", entidadeSelecionada.get().getCodcli()))
//                        .resultList();
//                if (entidadesSincronizadas.size() > 0) {
//                    MessageBox.create(message -> {
//                        message.message("Já existe um outro cliente sincronizado à loja B2B com esse e-mail.\n" +
//                                "Verifique se o cliente tem um outro e-mail para cadastro.\n" +
//                                "O envio do cadastro para a loja está cancelado.");
//                        message.type(MessageBox.TypeMessageBox.ERROR);
//                        message.showAndWait();
//                    });
//
//                    entidadeSelecionada.get().getSdEntidade().setSincB2b(false);
//                    entidadeSelecionada.get().getSdEntidade().setLiberadoB2b(false);
//                    new FluentDao().merge(entidadeSelecionada.get());
//                } else {
//                    ResponseMagento response = null;
//
//                    if (entidadeSelecionada.get().getSdEntidade().isSincB2b()) {
//                        CustomerStatus upadateClienteB2b = convertToCustomerStatus(entidadeSelecionada.get());
//                        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.ENVIAR, entidadeSelecionada.get().getStringCodcli(), "Enviando JSON para integração B2B: " + new Gson().toJson(upadateClienteB2b));
//                        response = CustomersB2BMagentoWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true).update(upadateClienteB2b);
//                        if (response.getError()){
//                            if (response.getMessage().contains("customer not found")){
//                                Customer clienteB2b = convertToCustomer(entidadeSelecionada.get());
//                                response = CustomersB2BMagentoWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true).create(clienteB2b);
//                                if (response.getError()){
//                                    ResponseMagento finalResponse1 = response;
//                                    MessageBox.create(message -> {
//                                        message.message("Ocorreu um erro no envio do cliente para a Loja B2B.\nErro: "+ finalResponse1.getMessage());
//                                        message.type(MessageBox.TypeMessageBox.ERROR);
//                                        message.showAndWait();
//                                    });
//                                } else {
//                                    ResponseMagento finalResponse2 = response;
//                                    MessageBox.create(message -> {
//                                        message.message("Entidade enviada para E-commerce B2B.\nResposta do servidor: " + finalResponse2.getMessage());
//                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                        message.position(Pos.TOP_RIGHT);
//                                        message.notification();
//                                    });
//                                }
//                            } else {
//                                ResponseMagento finalResponse1 = response;
//                                MessageBox.create(message -> {
//                                    message.message("Ocorreu um erro no envio do cliente para a Loja B2B.\nErro: "+ finalResponse1.getMessage());
//                                    message.type(MessageBox.TypeMessageBox.ERROR);
//                                    message.showAndWait();
//                                });
//                            }
//                        } else {
//                            ResponseMagento finalResponse3 = response;
//                            MessageBox.create(message -> {
//                                message.message("Entidade enviada para E-commerce B2B.\nResposta do servidor: " + finalResponse3.getMessage());
//                                message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                message.position(Pos.TOP_RIGHT);
//                                message.notification();
//                            });
//                        }
//                        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.ENVIAR, entidadeSelecionada.get().getStringCodcli(), "Enviado dados da entidade para loja B2B. Resposta: " + response.getMessage());
//                    } else {
//                        Customer clienteB2b = convertToCustomer(entidadeSelecionada.get());
//                        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.ENVIAR, entidadeSelecionada.get().getStringCodcli(), "Enviando JSON para integração B2B: " + new Gson().toJson(clienteB2b));
//                        response = CustomersB2BMagentoWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true).create(clienteB2b);
//                        if (response.getError()){
//                            if (response.getMessage().contains("customer taxvat already exists") || response.getMessage().contains("customer email already exists")){
//                                CustomerStatus upadateClienteB2b = convertToCustomerStatus(entidadeSelecionada.get());
//                                response = CustomersB2BMagentoWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true).update(upadateClienteB2b);
//                                if (response.getError()){
//                                    ResponseMagento finalResponse1 = response;
//                                    MessageBox.create(message -> {
//                                        message.message("Ocorreu um erro no envio do cliente para a Loja B2B.\nErro: "+ finalResponse1.getMessage());
//                                        message.type(MessageBox.TypeMessageBox.ERROR);
//                                        message.showAndWait();
//                                    });
//                                } else {
//                                    ResponseMagento finalResponse3 = response;
//                                    MessageBox.create(message -> {
//                                        message.message("Entidade enviada para E-commerce B2B.\nResposta do servidor: " + finalResponse3.getMessage());
//                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                        message.position(Pos.TOP_RIGHT);
//                                        message.notification();
//                                    });
//                                    entidadeSelecionada.get().getSdEntidade().setSincB2b(true);
//                                    new FluentDao().merge(entidadeSelecionada.get());
//                                }
//                            } else {
//                                ResponseMagento finalResponse1 = response;
//                                MessageBox.create(message -> {
//                                    message.message("Ocorreu um erro no envio do cliente para a Loja B2B.\nErro: "+ finalResponse1.getMessage());
//                                    message.type(MessageBox.TypeMessageBox.ERROR);
//                                    message.showAndWait();
//                                });
//                            }
//                        } else {
//                            ResponseMagento finalResponse2 = response;
//                            MessageBox.create(message -> {
//                                message.message("Entidade enviada para E-commerce B2B.\nResposta do servidor: " + finalResponse2.getMessage());
//                                message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                message.position(Pos.TOP_RIGHT);
//                                message.notification();
//                            });
//                            entidadeSelecionada.get().getSdEntidade().setSincB2b(true);
//                            new FluentDao().merge(entidadeSelecionada.get());
//                        }
//                    }
//                }
            }

            emEdicao.set(false);
            novaEntidade.set(false);
            carregaEntidade(entidadeSelecionada.get());
        } catch (FormValidationException ex) {
            ex.printStackTrace();
            MessageBox.create(message -> {
                message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Actions: FILTRO">
    @FXML
    private void tboxFiltroTipoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroTipoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroTipoOnAction(ActionEvent event) {
        try {
            GenericFilter<SitCli> filter = new GenericFilter<SitCli>() {
            };
            filter.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroTipo.setText(filter.selectedsReturn.stream().map(o -> o.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroCidadeOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroCidadeOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroCidadeOnAction(ActionEvent event) {
        try {
            GenericFilter<Cidade> filter = new GenericFilter<Cidade>() {
            };
            filter.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCidade.setText(filter.selectedsReturn.stream().map(o -> o.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroRepresentanteOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroRepresentanteOnAction(ActionEvent event) {
        try {
            GenericFilter<Represen> filter = new GenericFilter<Represen>() {
            };
            filter.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroRepresentante.setText(filter.selectedsReturn.stream().map(o -> o.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroMarcaOnAction(ActionEvent event) {
        try {
            GenericFilter<Marca> filter = new GenericFilter<Marca>() {
            };
            filter.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMarca.setText(filter.selectedsReturn.stream().map(o -> o.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroGrupoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroGrupoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroGrupoOnAction(ActionEvent event) {
        try {
            GenericFilter<GrupoCli> filter = new GenericFilter<GrupoCli>() {
            };
            filter.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroGrupo.setText(filter.selectedsReturn.stream().map(o -> o.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroDtInicioDeOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroDtInicioDe.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxFiltroDtCadastroAteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroDtCadastroAte.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxFiltroDtCadastroAte.setValue(tboxFiltroDtInicioDe.getValue());
        }
    }

    @FXML
    private void btnCarregarEntidadesOnAction(ActionEvent event) {
        JPAUtils.clearEntitys(entidades.get());
        List<String> selecteds = ((List<FormFieldCheckBoxGroup.OptionCheckBox>) cboxGroupStatusPedido.selecteds()).stream().map(eb -> ((String) eb.valueOption)).collect(Collectors.toList());
        Object[] bloqueios = selecteds.stream().filter(it -> it.contains("C")).map(it -> it.replace("C", "")).toArray();
        Object[] financeiro = selecteds.stream().filter(it -> it.contains("F")).map(it -> it.replace("F", "")).toArray();

        List<Pedido> pedidosFiltrados = new ArrayList<>();

        if (dataPedidoFilter.valueBegin.getValue() != null || dataPedidoFilter.valueEnd.getValue() != null || selecteds.size() > 0) {
            pedidosFiltrados = (List<Pedido>) new FluentDao().selectFrom(Pedido.class).where(it -> it
                    .and(eb -> eb
                            .between("periodo.prazo", ((Object) "0000"), "9999", TipoExpressao.AND, when -> true)
                            .equal("periodo.prazo", "GVT", TipoExpressao.OR, when -> true))
                    .isIn("bloqueio", bloqueios, TipoExpressao.AND, when -> bloqueios.length > 0)
                    .isIn("financeiro", financeiro, TipoExpressao.AND, when -> financeiro.length > 0)
                    .between("dt_emissao", dataPedidoFilter.valueBegin.getValue() == null ? LocalDate.of(2000, 1, 1) : dataPedidoFilter.valueBegin.getValue(), dataPedidoFilter.valueEnd.getValue() == null ? LocalDate.of(2050, 1, 1) : dataPedidoFilter.valueEnd.getValue())
            ).resultList();
        }

//        entidades.set(FXCollections.observableList((List<Entidade>) new FluentDao()
//                .selectFrom(Entidade.class)
//                .where(eb ->
//                    eb
//                            .like("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .like("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .in("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .in("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .in("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .equal("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .equal("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .in("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .between("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .like("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                            .like("ativo", true, TipoExpressao.AND, cond -> StringUtils.isEmptyOrNull(tboxFiltroRazao.getText()))
//                )
//                .resultList()));
        daoEntidade = daoEntidade.initCriteria();
        daoEntidade = daoEntidade.addPredicate("nome", tboxFiltroRazao.getText(), PredicateType.LIKE);
        daoEntidade = daoEntidade.addPredicate("fantasia", tboxFiltroFantasia.getText(), PredicateType.LIKE);
        daoEntidade = daoEntidade.addPredicate("sitCli.codigo", tboxFiltroTipo.getText().split(","), PredicateType.IN);
        daoEntidade = daoEntidade.addPredicate("cep.cidade.codCid", tboxFiltroCidade.getText().split(","), PredicateType.IN);
        daoEntidade = daoEntidade.addPredicate("cep.cidade.codEst.id.siglaEst", cboxFiltroUf.getSelectionModel().getSelectedItem(), PredicateType.EQUAL);
        daoEntidade = daoEntidade.addPredicate("cnpj", tboxFiltroCnpj.getText(), PredicateType.EQUAL);
        daoEntidade = daoEntidade.addPredicate("grupo.codigo", tboxFiltroGrupo.getText().split(","), PredicateType.IN);
        daoEntidade = daoEntidade.addPredicateBetween("dataCad", tboxFiltroDtInicioDe.getValue(), tboxFiltroDtCadastroAte.getValue());
        List<String> tipos = new ArrayList<>();
        if (cboxFiltroFisica.isSelected()) {
            tipos.add("1");
        }
        if (cboxFiltroJuridica.isSelected()) {
            tipos.add("2");
        }
        daoEntidade = daoEntidade.addPredicate("tipo", tipos.toArray(), PredicateType.IN);

        if (cboxFiltroCliente.isSelected() || cboxFiltroFornecedor.isSelected() || cboxFiltroTerceiro.isSelected()) {
            if (cboxFiltroCliente.isSelected()) {
                daoEntidade = daoEntidade.addPredicate("tipoEntidade", "C", PredicateType.LIKE, true, "tipoEntidade");
            }
            if (cboxFiltroFornecedor.isSelected()) {
                daoEntidade = daoEntidade.addPredicate("tipoEntidade", "F", PredicateType.LIKE, true, "tipoEntidade");
            }
            if (cboxFiltroTerceiro.isSelected()) {
                daoEntidade = daoEntidade.addPredicate("tipoEntidade", "T", PredicateType.LIKE, true, "tipoEntidade");
            }
            daoEntidade = daoEntidade.addPredicateGroup("tipoEntidade", false, true);
        }

        List<Boolean> ativo = new ArrayList<>();
        if (cboxFiltroAtivo.isSelected()) {
            ativo.add(Boolean.TRUE);
        }
        if (cboxFiltroInativo.isSelected()) {
            ativo.add(Boolean.FALSE);
        }
        daoEntidade = daoEntidade.addPredicate("ativo", ativo.toArray(), PredicateType.IN);
        List<Boolean> bloqueio = new ArrayList<>();
        if (cboxFiltroBloqueado.isSelected()) {
            bloqueio.add(Boolean.TRUE);
        }
        if (cboxFiltroLiberado.isSelected()) {
            bloqueio.add(Boolean.FALSE);
        }
        daoEntidade = daoEntidade.addPredicate("bloqueio", bloqueio.toArray(), PredicateType.IN);
        List<Boolean> bloqueioB2b = new ArrayList<>();
        if (cboxFiltroBloqueadoB2B.isSelected()) {
            bloqueioB2b.add(Boolean.FALSE);
        }
        if (cboxFiltroLiberadoB2B.isSelected()) {
            bloqueioB2b.add(Boolean.TRUE);
        }
        daoEntidade = daoEntidade.addPredicate("sdEntidade.liberadoB2b", bloqueioB2b.toArray(), PredicateType.IN);
        List<Boolean> sincB2b = new ArrayList<>();
        if (cboxFiltroSincronizadoB2B.isSelected()) {
            sincB2b.add(Boolean.FALSE);
        }
        if (cboxFiltroNaoSincronizadoB2B.isSelected()) {
            sincB2b.add(Boolean.TRUE);
        }
        daoEntidade = daoEntidade.addPredicate("sdEntidade.sincB2b", sincB2b.toArray(), PredicateType.IN);
        if (segBtnRevisaoCadastro.value.getValue().equals(1))
            daoEntidade = daoEntidade.addPredicate("sdEntidade.dataRevisao", "--", PredicateType.IS_NOT_NULL);
        else if (segBtnRevisaoCadastro.value.getValue().equals(2))
            daoEntidade = daoEntidade.addPredicate("sdEntidade.dataRevisao", "--", PredicateType.IS_NULL);

        daoEntidade = daoEntidade.addPredicate("codcli", pedidosFiltrados.stream().map(it -> it.getCodcli().getStringCodcli()).distinct().toArray(), PredicateType.IN);
        try {
            entidades.set(daoEntidade.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroDtInicioDe.setValue(null);
        tboxFiltroDtCadastroAte.setValue(null);

        cboxFiltroJuridica.setSelected(false);
        cboxFiltroFisica.setSelected(false);
        cboxFiltroCliente.setSelected(false);
        cboxFiltroFornecedor.setSelected(false);
        cboxFiltroTerceiro.setSelected(false);
        cboxFiltroAtivo.setSelected(false);
        cboxFiltroInativo.setSelected(false);
        cboxFiltroBloqueado.setSelected(false);
        cboxFiltroLiberado.setSelected(false);
        cboxFiltroBloqueadoB2B.setSelected(false);
        cboxFiltroLiberadoB2B.setSelected(false);
        cboxFiltroNaoSincronizadoB2B.setSelected(false);
        cboxFiltroSincronizadoB2B.setSelected(false);

        tboxFiltroRazao.clear();
        tboxFiltroCnpj.clear();
        tboxFiltroCidade.clear();
        tboxFiltroFantasia.clear();
        tboxFiltroGrupo.clear();
        tboxFiltroMarca.clear();
        tboxFiltroRepresentante.clear();
        tboxFiltroTipo.clear();

        cboxGroupStatusPedido.clear();
        dataPedidoFilter.clear();
        segBtnRevisaoCadastro.select(2);
    }

    // Context MENU
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Actions: NAVEGAÇÃO">
    @FXML
    private void btnAddRegisterOnAction(ActionEvent actionEvent) {
        emEdicao.set(true);
        novaEntidade.set(true);
        if (JPAUtils.getEntityManager().contains(entidadeSelecionada.get()))
            JPAUtils.getEntityManager().detach(entidadeSelecionada.get());
        clearForm();
        entidadeSelecionada.set(new Entidade());
        complementoNovaEntidade = new SdEntidade();
        entidadeSelecionada.get().setSdEntidade(complementoNovaEntidade);
        marcasNovaEntidade.clear();
        contatosNovaEntidade.clear();
        dtCadastroEntidadeField.value.set(LocalDate.now());
        perfilEntidadeField.select(2);
        perfilEntidadeField.select(0);
        tipoEntidadeField.select(0);
        origemEntidadeField.select(0);
        avisaBoletoEnviadoField.value.set(false);
        atualizaGradeField.value.set(true);
        imprimeConteudoCaixaField.value.set(false);
        antecipaProdutoField.value.set(true);
        atendeRoboField.value.set(true);
        fieldValorMinimoPedidoB2b.value.set("0");
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent actionEvent) {
        actionEditarEntidade(entidadeSelecionada.get());
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent actionEvent) {
        actionDeletarEntidade(entidadeSelecionada.get());
    }

    @FXML
    private void btnSaveOnAction(ActionEvent actionEvent) throws SQLException {
        salvarEntidade();
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        emEdicao.set(false);
        clearForm();
        if (novaEntidade.get())
            tblEntidades.getSelectionModel().select(0);
        novaEntidade.set(false);
        carregaEntidade(entidadeSelecionada.get());
    }

    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblEntidades.getSelectionModel().selectFirst();
        carregaEntidade(tblEntidades.getSelectionModel().getSelectedItem());
        event.consume();
    }

    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblEntidades.getSelectionModel().selectPrevious();
        carregaEntidade(tblEntidades.getSelectionModel().getSelectedItem());
        event.consume();
    }

    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblEntidades.getSelectionModel().selectNext();
        carregaEntidade(tblEntidades.getSelectionModel().getSelectedItem());
        event.consume();
    }

    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblEntidades.getSelectionModel().selectLast();
        carregaEntidade(tblEntidades.getSelectionModel().getSelectedItem());
        event.consume();
    }

    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        btnCancelOnAction(null);
        tabPaneRoot.getSelectionModel().select(0);
    }
    // </editor-fold>

    @FXML
    private void tboxRazaoSocialOnKeyReleased(KeyEvent event) throws Exception {
        if (event.getCode() == KeyCode.ENTER && novaEntidade.not().get()) {
            JPAUtils.clearEntitys(entidades.get());
            entidades.clear();
            ObservableList<Entidade> quickFind = FXCollections.observableList((List<Entidade>) new FluentDao().selectFrom(Entidade.class)
                    .where(eb -> eb.like("nome", tboxRazaoSocial.getText()))
                    .resultList());
            //daoEntidade.initCriteria().addPredicate("nome", tboxRazaoSocial.getText(), PredicateType.LIKE).loadListByPredicate();
            if (quickFind.size() > 1) {
                GenericSelect<Entidade> showSelect = GenericSelect.create(Entidade.class, quickFind);
                entidadeSelecionada.set(showSelect.selectedReturn);
                carregaEntidade(showSelect.selectedReturn);
            } else if (quickFind.size() == 1) {
                entidadeSelecionada.set(quickFind.get(0));
                carregaEntidade(quickFind.get(0));
            } else {
                clearForm();
                MessageBox.create(message -> {
                    message.message("Entidade não encontrada.");
                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        }
    }

    @FXML
    private void tboxCodeOnKeyReleased(KeyEvent event) throws Exception {
        if (event.getCode() == KeyCode.ENTER && novaEntidade.not().get()) {
            JPAUtils.clearEntitys(entidades.get());
            entidades.clear();
            Entidade quickFind = (Entidade) new FluentDao().selectFrom(Entidade.class)
                    .where(eb -> eb.equal("codcli", Integer.parseInt(tboxCode.getText())))
                    .singleResult();
            //daoEntidade.initCriteria().addPredicate("codcli", Integer.parseInt(tboxCode.getText()), PredicateType.EQUAL).loadListByPredicate();
            if (quickFind != null) {
                JPAUtils.getEntityManager().refresh(quickFind);
                carregaEntidade(quickFind);
            } else {
                clearForm();
                MessageBox.create(message -> {
                    message.message("Entidade não encontrada.");
                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        }
    }

    @FXML
    private void tboxCnpjOnKeyReleased(KeyEvent event) throws Exception {
        if (event.getCode() == KeyCode.ENTER && novaEntidade.not().get()) {
            JPAUtils.clearEntitys(entidades.get());
            entidades.clear();
            String cnpjFormatado = StringUtils.formatCpfCnpj(StringUtils.unformatCpfCnpj(tboxCnpj.getText()));
            ObservableList<Entidade> quickFind = FXCollections.observableList((List<Entidade>) new FluentDao().selectFrom(Entidade.class)
                    .where(eb -> eb.like("cnpj", cnpjFormatado))
                    .resultList());
            //daoEntidade.initCriteria().addPredicate("cnpj", tboxCnpj.getText(), PredicateType.LIKE).loadListByPredicate();
            if (quickFind.size() > 1) {
                GenericSelect<Entidade> showSelect = GenericSelect.create(Entidade.class, quickFind);
                carregaEntidade(showSelect.selectedReturn);
            } else if (quickFind.size() == 1) {
                carregaEntidade(quickFind.get(0));
            } else {
                clearForm();
                MessageBox.create(message -> {
                    message.message("Entidade não encontrada.");
                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        } else if (tipoDocumentoEntidadeField.items.get(0).isSelected() && tboxCnpj.getText().length() >= 14 && novaEntidade.get()) {
            String cnpjFormatado = StringUtils.formatCpfCnpj(StringUtils.unformatCpfCnpj(tboxCnpj.getText()));
            Entidade quickFind = (Entidade) new FluentDao().selectFrom(Entidade.class)
                    .where(eb -> eb.equal("cnpj", cnpjFormatado))
                    .singleResult();
            if (quickFind != null) {
                tboxCnpj.clear();
                MessageBox.create(message -> {
                    message.message("O CNPJ " + quickFind.getCnpj() + " digitado já existe no banco de dados.\nEntidade: [" + quickFind.getCodcli() + "] " + quickFind.getNome());
                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                    message.showAndWait();
                });
            }
        } else if (tipoDocumentoEntidadeField.items.get(1).isSelected() && tboxCnpj.getText().length() >= 11 && novaEntidade.get()) {
            String cnpjFormatado = StringUtils.formatCpfCnpj(StringUtils.unformatCpfCnpj(tboxCnpj.getText()));
            Entidade quickFind = (Entidade) new FluentDao().selectFrom(Entidade.class)
                    .where(eb -> eb.equal("cnpj", cnpjFormatado))
                    .singleResult();
            if (quickFind != null) {
                tboxCnpj.clear();
                MessageBox.create(message -> {
                    message.message("O CPF " + quickFind.getCnpj() + " digitado já existe no banco de dados.\nEntidade: [" + quickFind.getCodcli() + "] " + quickFind.getNome());
                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                    message.showAndWait();
                });
            }
        }
    }

    @FXML
    private void btnImportarDadosReceitaOnAction(ActionEvent event) throws Exception {
        try {
            ReceitaWS wsResponseEntity = ServicoReceitaWS.getInstance().consultaCNPJ(tboxCnpj.getText().replace("-", "").replace("/", "").replace(".", ""));
            if (wsResponseEntity == null || wsResponseEntity.getNome() == null) {
                MessageBox.create(message -> {
                    message.message("Não foi encontrado um cadastro com esse CNPJ no servidor da ReceitaWS.");
                    message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                    message.showAndWait();
                });
                return;
            }

            if (wsResponseEntity.getCep() != null && wsResponseEntity.getCep().length() > 0) {
                CadCep cepReceita = (CadCep) daoCadCep.initCriteria()
                        .addPredicate("cep", StringUtils.unformatCep(wsResponseEntity.getCep()),
                                PredicateType.EQUAL).loadEntityByPredicate();
                if (cepReceita == null) {
//            MessageBox.create(message -> {
//                message.message("Não foi encontrado em nosso banco de dados o CEP retornado pelo site da ReceitaWS.\nAtualize o cadastro do CEP no ERP.");
//                message.type(MessageBox.TypeMessageBox.NO_ENTRY);
//                message.showAndWait();
//            });
                    CEP viaCep = ServicoViaCEP.getInstance().consultaCEP(StringUtils.unformatCep(wsResponseEntity.getCep()));
                    if (viaCep.getCep() != null) {
                        CadCep novoCep = new CadCep();
                        novoCep.setCep(StringUtils.unformatCep(viaCep.getCep()));
                        novoCep.setCidade((Cidade) daoCidade.initCriteria().addPredicate("codCid", viaCep.getIbge(), PredicateType.EQUAL).loadEntityByPredicate());
                        novoCep.setBairro(viaCep.getBairro());
                        novoCep.setLogrCep(viaCep.getLogradouro());
                        daoCadCep.save(novoCep);
                        cepReceita = novoCep;
                        cepEntidadeField.value(cepReceita);
                    } else {
                        MessageBox.create(message -> {
                            message.message("CEP digitado é inválido ou não existente.");
                            message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                            message.showAndWait();
                        });
                    }
                } else {
                    cepEntidadeField.value(cepReceita);
                }
            }

            tboxRazaoSocial.setText(wsResponseEntity.getNome());
            fantasiaEntidadeField.value.set(wsResponseEntity.getFantasia());
            enderecoEntidadeField.value.set(wsResponseEntity.getLogradouro());
            numeroEntidadeField.value.set(wsResponseEntity.getNumero());
            if (emailEntidadeField.value.getValue() == null || emailEntidadeField.value.getValue().length() == 0)
                emailEntidadeField.value.set(wsResponseEntity.getEmail());
            bairroEntidadeField.value.set(wsResponseEntity.getBairro());
            if (telefoneEntidadeField.value.getValue() == null || telefoneEntidadeField.value.getValue().length() == 0)
                telefoneEntidadeField.value.set(wsResponseEntity.getTelefone());
            complementoEntidadeField.value.set(wsResponseEntity.getComplemento());

            MessageBox.create(message -> {
                message.message("Informações atualizadas com o site da ReceitaWS.\nVocê já pode salvar o cadastro.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (SocketTimeoutException ex) {
            MessageBox.create(message -> {
                message.message("CNPJ não encontrado na base da ReceitaWS.");
                message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                message.showAndWait();
            });
        }
    }

    @FXML
    private void btnSalvarContatoOnAction(ActionEvent event) throws Exception {

        if (tboxNomeContatoEntidade.getText() == null || tboxNomeContatoEntidade.getText().equals("")) {
            MessageBox.create(message -> {
                message.message("Não é possível salvar o contato pois o campo NOME está vazio");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        Contato contato = new Contato();
        contato.setCodcli(entidadeSelecionada.get().getCodcli());
        contato.setDtNasc(tboxDataNascContatoEntidade.getValue());
        contato.setEmail(tboxEmailContatoEntidade.getText());
        contato.setFone(tboxTelefoneContatoEntidade.getText());
        contato.setNome(tboxNomeContatoEntidade.getText());
        contato.setObs(tboxObservacaoContatoEntidade.getText());
        contato.setTipo(cboxTipoContatoEntidade.getSelectionModel().getSelectedItem());

        if (novaEntidade.not().get())
            new FluentDao().persist(contato);
        else
            contatosNovaEntidade.add(contato);

        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(), "Salvo contato " + contato.toLog() + " no cadastro da entidade.");
        MessageBox.create(message -> {
            message.message("Cadastro de contato realizado!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });

        tblContatos.getItems().add(contato);
        tboxEmailContatoEntidade.clear();
        tboxTelefoneContatoEntidade.clear();
        tboxNomeContatoEntidade.clear();
        tboxObservacaoContatoEntidade.clear();
        cboxTipoContatoEntidade.getSelectionModel().select(0);
    }

    @FXML
    private void tblContatosOnKeyReleased(KeyEvent event) throws Exception {
        if (emEdicao.get())
            if (event.getCode() == KeyCode.DELETE) {
                Contato contato = tblContatos.getSelectionModel().getSelectedItem();
                if (contato != null)
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o contato selecionado?");
                        message.showAndWait();
                    }).value.get()) {
                        new FluentDao().delete(contato);
                        tblContatos.getItems().remove(contato);

                        SysLogger.addSysDelizLog("ENTIDADE", TipoAcao.EDITAR, entidadeSelecionada.get().getStringCodcli(), "Excluído contato " + contato.toLog() + " no cadastro da entidade.");
                        MessageBox.create(message -> {
                            message.message("Contato excluído com sucesso.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    }
            }
    }

    @FXML
    public void tboxCnpjOnMouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            tboxCnpj.selectAll();
        }
    }
}
