/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.comercial;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdEntregaProducao001;
import sysdeliz2.models.sysdeliz.SdEntregaProducao001PK;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCalendarioEntregaController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc="Declaração: Controle">
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty novoCadastro = new SimpleBooleanProperty(false);
    private final ObjectProperty<Colecao> colecaoSelecionada = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marcaSelecionada = new SimpleObjectProperty<>();
    private final ObjectProperty<TabPrz> periodoSelecionada = new SimpleObjectProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    private final ListProperty<SdEntregaProducao001> registros = new SimpleListProperty<>();
    private final ListProperty<SdEntregaProducao001> calendarioColecaoMarca = new SimpleListProperty<>();
    private final ListProperty<SdEntregaProducao001> calendarioColecaoMarcaParaExcluir = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<TabPrz> lotesEntrega = new SimpleListProperty<>();
    private final ListProperty<TabPrz> lotesProducaoSelecionado = new SimpleListProperty<>();
    private final ListProperty<SdEntregaProducao001> lotesProducaoEntrega = new SimpleListProperty<>();
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    GenericDao<SdEntregaProducao001> daoEntregaProducao = new GenericDaoImpl<>(SdEntregaProducao001.class);
    GenericDao<TabPrz> daoPeriodoEntrega = new GenericDaoImpl<>(TabPrz.class);
    GenericDao<Colecao> daoColecao = new GenericDaoImpl<>(Colecao.class);
    GenericDao<Marca> daoMarca = new GenericDaoImpl<>(Marca.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Componentes FXML">
    @FXML
    private TabPane tabPanePrincipal;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private VBox vboxFiltroIsAtivo;
    @FXML
    private TextField tboxFiltroLoteEntrega;
    @FXML
    private Button btnProcurarFiltroLoteEntrega;
    @FXML
    private TextField tboxFiltroLoteProducao;
    @FXML
    private Button btnProcurarFiltroLoteProducao;
    @FXML
    private TextField tboxFiltroColecao;
    @FXML
    private Button btnProcurarFiltroColecao;
    @FXML
    private TextField tboxFiltroMarca;
    @FXML
    private Button btnProcurarFiltroMarca;
    @FXML
    private Button btnCarregar;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private TableView<SdEntregaProducao001> tblRegistros;
    @FXML
    private TableColumn<SdEntregaProducao001, SdEntregaProducao001PK> clnMarca;
    @FXML
    private TableColumn<SdEntregaProducao001, SdEntregaProducao001PK> clnColecao;
    @FXML
    private TableColumn<SdEntregaProducao001, SdEntregaProducao001PK> clnLoteEntrega;
    @FXML
    private TableColumn<SdEntregaProducao001, SdEntregaProducao001PK> clnLoteProducao;
    @FXML
    private TableColumn<SdEntregaProducao001, Boolean> clnAtivo;
    @FXML
    private Label lbContadorRegistros;
    @FXML
    private Button btnAddRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnDesativarTudo;
    @FXML
    private TextField tboxFiltroCadastroColecao;
    @FXML
    private Button btnProcuraFiltroCadastroColecao;
    @FXML
    private TextField tboxFiltroCadastroMarca;
    @FXML
    private Button btnProcuraFiltroCadastroMarca;
    @FXML
    private ListView<TabPrz> lviewLotesEntrega;
    @FXML
    private Label lbContadorProdutos;
    @FXML
    private TextField tboxFiltroCadastroLoteProducao;
    @FXML
    private Button btnProcurarFiltroCadastroLoteProducao;
    @FXML
    private Button btnAddLoteProducao;
    @FXML
    private DatePicker tboxDtInicio;
    @FXML
    private DatePicker tboxDtFim;
    @FXML
    private TableView<SdEntregaProducao001> tblLoteProducao;
    @FXML
    private TableColumn<SdEntregaProducao001, SdEntregaProducao001PK> clnLoteProducaoCadastro;
    @FXML
    private TableColumn<SdEntregaProducao001, Boolean> clnAtivoLoteProducaoCadastro;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração; Componentes Extras">
    FormToggleField toggleFiltroAtivo = new FormToggleField("Ativo", true, false);
    // </editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponentesFxml();

        try {
            registros.set(daoEntregaProducao.initCriteria().addPredicate("ativo", true, PredicateType.EQUAL).loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void initializeComponentesFxml() {

        // <editor-fold defaultstate="collapsed" desc="ToggleSwitch ATIVO">
        vboxFiltroIsAtivo.getChildren().add(toggleFiltroAtivo);
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Label REGISTERS COUNT">
        lbContadorRegistros.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registros.sizeProperty()).concat(" registros"));
        lbContadorProdutos.textProperty().bind(new SimpleStringProperty("Selecionado(s) ").concat(lotesProducaoEntrega.sizeProperty()).concat(" lote(s)"));
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TextField FILTRO">
        TextFieldUtils.upperCase(tboxFiltroColecao);
        TextFieldUtils.upperCase(tboxFiltroLoteEntrega);
        TextFieldUtils.upperCase(tboxFiltroLoteProducao);
        TextFieldUtils.upperCase(tboxFiltroMarca);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField CADASTRO">
        TextFieldUtils.upperCase(tboxFiltroCadastroColecao);
        TextFieldUtils.upperCase(tboxFiltroCadastroMarca);
        tboxDtInicio.disableProperty().bind(emEdicao.not());
        tboxDtFim.disableProperty().bind(emEdicao.not());
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Buttons CADASTRO">
        btnProcurarFiltroCadastroLoteProducao.disableProperty().bind(emEdicao.not());
        btnAddLoteProducao.disableProperty().bind(emEdicao.not());
        btnCancel.disableProperty().bind(emEdicao.not());
        btnSave.disableProperty().bind(emEdicao.not());
        btnAddRegister.disableProperty().bind(emEdicao);
        btnEditRegister.disableProperty().bind(emEdicao);
        btnDeleteRegister.disableProperty().bind(emEdicao);
        btnDesativarTudo.disableProperty().bind(emEdicao.not());
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ListView ENTREGA">
        lviewLotesEntrega.itemsProperty().bind(lotesEntrega);
        lviewLotesEntrega.setCellFactory(param -> new ListCell<TabPrz>() {
            @Override
            protected void updateItem(TabPrz item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText("[" + item.getPrazo() + "] " + item.getDescricao());
                }
            }
        });
        lviewLotesEntrega.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                periodoSelecionada.set(newValue);
                this.carregaLoteProducaoEntrega();
            }
        });
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TableView REGISTROS">
        tblRegistros.itemsProperty().bind(registros);
        tblRegistros.setRowFactory(tv -> new TableRow<SdEntregaProducao001>() {
            @Override
            protected void updateItem(SdEntregaProducao001 item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item != null && !empty) {
                    if (!item.isAtivo()) {
                        getStyleClass().add("table-row-danger");
                    }
                }
            }

            private void clearStyle() {
                getStyleClass().remove("table-row-danger");
            }

        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView LOTES PRODUCAO">
        tblLoteProducao.itemsProperty().bind(lotesProducaoEntrega);
        tblLoteProducao.editableProperty().bind(emEdicao);
        tblLoteProducao.setRowFactory(tv -> new TableRow<SdEntregaProducao001>() {
            @Override
            protected void updateItem(SdEntregaProducao001 item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item != null && !empty) {
                    if (!item.isAtivo()) {
                        getStyleClass().add("table-row-danger");
                    }
                }
            }
        
            private void clearStyle() {
                getStyleClass().remove("table-row-danger");
            }
        
        });
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableColumn REGISTROS">
        clnMarca.setCellFactory(param -> new TableCell<SdEntregaProducao001, SdEntregaProducao001PK>() {
            @Override
            protected void updateItem(SdEntregaProducao001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText("[" + item.getMarca().getCodigo() + "] " + item.getMarca().getDescricao());
                }
            }
        });
        clnColecao.setCellFactory(param -> new TableCell<SdEntregaProducao001, SdEntregaProducao001PK>() {
            @Override
            protected void updateItem(SdEntregaProducao001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText("[" + item.getColecao().getCodigo() + "] " + item.getColecao().getDescricao());
                }
            }
        });
        clnLoteEntrega.setCellFactory(param -> new TableCell<SdEntregaProducao001, SdEntregaProducao001PK>() {
            @Override
            protected void updateItem(SdEntregaProducao001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText("[" + item.getEntrega().getPrazo() + "] " + item.getEntrega().getDescricao());
                }
            }
        });
        clnLoteProducao.setCellFactory(param -> new TableCell<SdEntregaProducao001, SdEntregaProducao001PK>() {
            @Override
            protected void updateItem(SdEntregaProducao001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText("[" + item.getProducao().getPrazo() + "] " + item.getProducao().getDescricao());
                }
            }
        });
        clnAtivo.setCellFactory(param -> new TableCell<SdEntregaProducao001, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(item ? "SIM" : "NÃO");
                }
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn LOTES PRODUCAO">
        clnLoteProducaoCadastro.setCellFactory(param -> new TableCell<SdEntregaProducao001, SdEntregaProducao001PK>() {
            @Override
            protected void updateItem(SdEntregaProducao001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText("[" + item.getProducao().getPrazo() + "] " + item.getProducao().getDescricao());
                }
            }
        });
        clnAtivoLoteProducaoCadastro.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnAtivoLoteProducaoCadastro.setCellValueFactory((cellData) -> {
                SdEntregaProducao001 cellValue = cellData.getValue();
                BooleanProperty property = ((SdEntregaProducao001) cellValue).ativoProperty();
                property.addListener((observable, oldValue, newValue) -> ((SdEntregaProducao001) cellValue).setAtivo(newValue));
                return property;
            });
        //</editor-fold>
    }

    private void carregaCalendarioColecaoMarca(String colecao, String marca) {
        try {
            JPAUtils.clearEntitys(calendarioColecaoMarca);
            calendarioColecaoMarca.set(daoEntregaProducao.initCriteria()
                    .addPredicate("id.colecao.codigo", colecao, PredicateType.EQUAL)
                    .addPredicate("id.marca.codigo", marca, PredicateType.EQUAL)
                    .loadListByPredicate());

            if (calendarioColecaoMarca.getSize() > 0) {
                this.getPeriodosEntrega();
                this.carregaLoteProducaoEntrega();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void getPeriodosEntrega() {
        try {
            lotesEntrega.set(daoPeriodoEntrega.initCriteria()
                    .addPredicate("tipo", "E", PredicateType.EQUAL)
                    .addOrderByAsc("prazo")
                    .loadListByPredicate());
            lviewLotesEntrega.getSelectionModel().select(0);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void carregaLoteProducaoEntrega() {
        lotesProducaoEntrega.set(FXCollections.observableList(calendarioColecaoMarca.stream()
                .filter(sdEntregaProducao001 -> sdEntregaProducao001.getId().getEntrega().getPrazo().equals(periodoSelecionada.get().getPrazo()))
                .collect(Collectors.toList())));
        if(lotesProducaoEntrega.getSize() > 0) {
            tboxDtInicio.setValue(lotesProducaoEntrega.get(0).getDtInicio());
            tboxDtFim.setValue(lotesProducaoEntrega.get(0).getDtFim());
        }else{
            tboxDtInicio.setValue(null);
            tboxDtFim.setValue(null);
        }
    }
    
    private boolean loteAdicionado(TabPrz lote, TabPrz periodo){
        for (SdEntregaProducao001 sdEntregaProducao001 : calendarioColecaoMarca) {
            if (sdEntregaProducao001.getId().getProducao().getPrazo().equals(lote.getPrazo())
                && !sdEntregaProducao001.getId().getEntrega().getPrazo().equals(periodo.getPrazo())){
                return true;
            }
        }
        
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Tab LISTAGEM">
    @FXML
    private void tboxFiltroLoteEntregaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroLoteEntregaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroLoteEntregaOnAction(ActionEvent event) {
        GenericFilter<TabPrz> procurarPeriodo = null;
        try {
            procurarPeriodo = new GenericFilter<TabPrz>() {
            };
            procurarPeriodo.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroLoteEntrega.setText(procurarPeriodo.selectedsReturn.stream().map(tabPrz -> tabPrz.getPrazo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroLoteProducaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroLoteProducaoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroLoteProducaoOnAction(ActionEvent event) {
        GenericFilter<TabPrz> procurarLote = null;
        try {
            procurarLote = new GenericFilter<TabPrz>() {
            };
            procurarLote.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroLoteProducao.setText(procurarLote.selectedsReturn.stream().map(tabPrz -> tabPrz.getPrazo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFiltroColecaoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarFiltroColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraColecao = null;
        try {
            procuraColecao = new GenericFilter<Colecao>() {
            };
            procuraColecao.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColecao.setText(procuraColecao.selectedsReturn.stream().map(colecao -> colecao.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.tboxFiltroMarcaOnKeyReleased(null);
        }
    }

    @FXML
    private void btnProcurarFiltroMarcaOnAction(ActionEvent event) {
        GenericFilter<Marca> procurarMarca = null;
        try {
            procurarMarca = new GenericFilter<Marca>() {
            };
            procurarMarca.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMarca.setText(procurarMarca.selectedsReturn.stream().map(marca -> marca.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnCarregarOnAction(ActionEvent event) {
        try {
            daoEntregaProducao = daoEntregaProducao.initCriteria().addPredicate("ativo", toggleFiltroAtivo.isSwitchedOn.get(), PredicateType.EQUAL);
            if (!tboxFiltroMarca.getText().isEmpty()) {
                daoEntregaProducao = daoEntregaProducao.addPredicate("id.marca.codigo", tboxFiltroMarca.getText().split(", "), PredicateType.IN);
            }
            if (!tboxFiltroColecao.getText().isEmpty()) {
                daoEntregaProducao = daoEntregaProducao.addPredicate("id.colecao.codigo", tboxFiltroColecao.getText().split(", "), PredicateType.IN);
            }
            if (!tboxFiltroLoteEntrega.getText().isEmpty()) {
                daoEntregaProducao = daoEntregaProducao.addPredicate("id.entrega.prazo", tboxFiltroLoteEntrega.getText().split(", "), PredicateType.IN);
            }
            if (!tboxFiltroLoteProducao.getText().isEmpty()) {
                daoEntregaProducao = daoEntregaProducao.addPredicate("id.producao.prazo", tboxFiltroLoteProducao.getText().split(", "), PredicateType.IN);
            }

            registros.set(daoEntregaProducao.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroColecao.clear();
        tboxFiltroLoteEntrega.clear();
        tboxFiltroLoteProducao.clear();
        tboxFiltroMarca.clear();
    }

    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel", "*.xlsx"))));
        try {
            new ExportExcel<SdEntregaProducao001>(tblRegistros).exportTableViewToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
    }

    @FXML
    private void tblRegistrosOnClickedMouse(MouseEvent event) {
        if (event.getClickCount() >= 2) {
            SdEntregaProducao001 produtoSelecionado = tblRegistros.getSelectionModel().getSelectedItem();

            if (produtoSelecionado != null) {
                carregaCalendarioColecaoMarca(produtoSelecionado.getId().getColecao().getCodigo(), produtoSelecionado.getId().getMarca().getCodigo());
                tboxFiltroCadastroColecao.setText(produtoSelecionado.getId().getColecao().getCodigo());
                tboxFiltroCadastroMarca.setText(produtoSelecionado.getId().getMarca().getCodigo());
                tabPanePrincipal.getSelectionModel().select(1);
            }
        }
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Tab MANUTENÇÃO">
    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        emEdicao.set(true);
        novoCadastro.set(true);
        calendarioColecaoMarca.clear();
        lotesProducaoSelecionado.clear();
        calendarioColecaoMarcaParaExcluir.clear();
        lotesEntrega.clear();
        lotesProducaoEntrega.clear();
        tboxDtInicio.setValue(null);
        tboxDtFim.setValue(null);
        colecaoSelecionada.set(null);
        marcaSelecionada.set(null);
        periodoSelecionada.set(null);
        tboxFiltroCadastroColecao.clear();
        tboxFiltroCadastroMarca.clear();
        this.getPeriodosEntrega();
        tboxFiltroCadastroColecao.requestFocus();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        lotesProducaoSelecionado.clear();
        calendarioColecaoMarcaParaExcluir.clear();
        emEdicao.set(true);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (calendarioColecaoMarca.isEmpty()) {
            GUIUtils.showMessage("Selecione uma coleção e marca cadastradas.");
            tboxFiltroCadastroColecao.requestFocus();
            return;
        }

        if (!GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja excluir os calendários da coleção " + tboxFiltroCadastroColecao.getText() + " e marca " + tboxFiltroCadastroMarca.getText())) {
            return;
        }

        try {
            for (SdEntregaProducao001 sdEntregaProducao001 : calendarioColecaoMarca) {
                daoEntregaProducao.delete(sdEntregaProducao001);
            }
            tboxFiltroCadastroColecao.clear();
            tboxFiltroCadastroMarca.clear();
            btnCancelOnAction(null);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) {
        try {
            for (SdEntregaProducao001 sdEntregaProducao001 : calendarioColecaoMarcaParaExcluir) {
                try {
                    daoEntregaProducao.delete(sdEntregaProducao001.getId());
                } catch (NoResultException e) {
                }
            }

            for (SdEntregaProducao001 sdEntregaProducao001 : calendarioColecaoMarca) {
                sdEntregaProducao001.setDtInicio(tboxDtInicio.getValue());
                sdEntregaProducao001.setDtFim(tboxDtFim.getValue());
                daoEntregaProducao.update(sdEntregaProducao001);
            }

            this.btnCancelOnAction(null);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        if (!tboxFiltroCadastroMarca.getText().isEmpty() && !tboxFiltroCadastroColecao.getText().isEmpty()) {
            this.carregaCalendarioColecaoMarca(tboxFiltroCadastroColecao.getText(), tboxFiltroCadastroMarca.getText());
        } else {
            calendarioColecaoMarca.clear();
        }
        calendarioColecaoMarcaParaExcluir.clear();
        lotesProducaoEntrega.clear();
        lotesProducaoSelecionado.clear();
        tboxDtInicio.setValue(null);
        tboxDtFim.setValue(null);
        emEdicao.set(false);
        novoCadastro.set(false);
    }

    @FXML
    private void btnDesativarTudoOnAction(ActionEvent event) {
        calendarioColecaoMarca.forEach(sdEntregaProducao001 -> {
            sdEntregaProducao001.setAtivo(false);
        });
        GUIUtils.showMessage("Todos os calendários foram desativados.");
        tblLoteProducao.refresh();
    }

    @FXML
    private void tboxFiltroCadastroColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcuraFiltroCadastroColecaoOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                colecaoSelecionada.set((Colecao) daoColecao.initCriteria()
                        .addPredicate("codigo", tboxFiltroCadastroColecao.getText(), PredicateType.EQUAL)
                        .loadEntityByPredicate());
                if (colecaoSelecionada.get() == null) {
                    GUIUtils.showMessage("Coleção não encontrada com este código.");
                    tboxFiltroCadastroColecao.clear();
                    tboxFiltroCadastroColecao.requestFocus();
                }

                if (!tboxFiltroCadastroMarca.getText().isEmpty()) {
                    this.carregaCalendarioColecaoMarca(tboxFiltroCadastroColecao.getText(), tboxFiltroCadastroMarca.getText());
                }
                tboxFiltroCadastroMarca.requestFocus();
            } catch (Exception e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }

    @FXML
    private void btnProcuraFiltroCadastroColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraColecao = null;
        try {
            procuraColecao = new GenericFilter<Colecao>() {
            };
            procuraColecao.show(ResultTypeFilter.SINGLE_RESULT);
            tboxFiltroCadastroColecao.setText(procuraColecao.selectedsReturn.stream().map(colecao -> colecao.getCodigo()).collect(Collectors.joining(",")));
            colecaoSelecionada.set(procuraColecao.selectedReturn);
            if (!tboxFiltroCadastroMarca.getText().isEmpty()) {
                this.carregaCalendarioColecaoMarca(tboxFiltroCadastroColecao.getText(), tboxFiltroCadastroMarca.getText());
            }
            tboxFiltroCadastroMarca.requestFocus();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroCadastroMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.tboxFiltroMarcaOnKeyReleased(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                marcaSelecionada.set((Marca) daoMarca.initCriteria()
                        .addPredicate("codigo", tboxFiltroCadastroMarca.getText(), PredicateType.EQUAL)
                        .loadEntityByPredicate());
                if (marcaSelecionada.get() == null) {
                    GUIUtils.showMessage("Marca não encontrada com este código.");
                    tboxFiltroCadastroMarca.clear();
                    tboxFiltroCadastroMarca.requestFocus();
                }

                if (!tboxFiltroCadastroColecao.getText().isEmpty()) {
                    this.carregaCalendarioColecaoMarca(tboxFiltroCadastroColecao.getText(), tboxFiltroCadastroMarca.getText());
                }
                tboxFiltroCadastroColecao.requestFocus();
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }

    @FXML
    private void btnProcuraFiltroCadastroMarcaOnAction(ActionEvent event) {
        GenericFilter<Marca> procurarMarca = null;
        try {
            procurarMarca = new GenericFilter<Marca>() {
            };
            procurarMarca.show(ResultTypeFilter.SINGLE_RESULT);
            tboxFiltroMarca.setText(procurarMarca.selectedsReturn.stream().map(marca -> marca.getCodigo()).collect(Collectors.joining(",")));
            marcaSelecionada.set(procurarMarca.selectedReturn);
            if (!tboxFiltroCadastroColecao.getText().isEmpty()) {
                this.carregaCalendarioColecaoMarca(tboxFiltroCadastroColecao.getText(), tboxFiltroCadastroMarca.getText());
            }
            tboxFiltroCadastroColecao.requestFocus();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnProcurarFiltroCadastroLoteProducaoOnAction(ActionEvent event) {
        GenericFilter<TabPrz> procurarLote = null;
        try {
            procurarLote = new GenericFilter<TabPrz>() {
            };
            procurarLote.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCadastroLoteProducao.setText(procurarLote.selectedsReturn.stream().map(tabPrz -> tabPrz.getPrazo()).collect(Collectors.joining(",")));
            lotesProducaoSelecionado.set(procurarLote.selectedsReturn);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnAddLoteProducaoOnAction(ActionEvent event) {
        if(colecaoSelecionada.get() == null || marcaSelecionada.get() == null){
            GUIUtils.showMessage("certifique-se se você carregou a coleção e marca nos campos acima. Você deve utilizar o botão de procura ou pressionar o enter após digitar o código.");
            tboxFiltroCadastroColecao.requestFocus();
            return;
        }
        
        if(tboxDtInicio.getValue() == null || tboxDtFim.getValue() == null){
            GUIUtils.showMessage("Você deve preencher a data de início e fim do período de entrega.");
            tboxDtInicio.requestFocus();
            return;
        } else if (tboxDtInicio.getValue().isAfter(tboxDtFim.getValue())){
            GUIUtils.showMessage("A data de início do período de entrega deve ser menor que a data fim.");
            tboxDtInicio.requestFocus();
            return;
        }
        
        if (lotesProducaoSelecionado.getSize() > 0) {
            for (TabPrz tabPrz : lotesProducaoSelecionado) {
                
                SdEntregaProducao001 novoCalendario = new SdEntregaProducao001(colecaoSelecionada.get(),
                        marcaSelecionada.get(),
                        periodoSelecionada.get(),
                        tabPrz,
                        tboxDtInicio.getValue(),
                        tboxDtFim.getValue(),
                        true);
                calendarioColecaoMarca.add(novoCalendario);
                lotesProducaoEntrega.add(novoCalendario);
            }

            tboxFiltroCadastroLoteProducao.clear();
            lotesProducaoSelecionado.clear();
        }
    }
    
    @FXML
    private void tboxDtInicioOnKeyReleased(KeyEvent event) {
        if(event.getCode() == KeyCode.T)
            tboxDtInicio.setValue(LocalDate.now());
    }
    
    @FXML
    private void tboxDtFimOnKeyReleased(KeyEvent event) {
        if(event.getCode() == KeyCode.T)
            tboxDtFim.setValue(LocalDate.now());
    }
    
    @FXML
    private void tblLoteProducaoOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() >= 2 && emEdicao.get()) {
            calendarioColecaoMarcaParaExcluir.add(tblLoteProducao.getSelectionModel().getSelectedItem());
            calendarioColecaoMarca.remove(tblLoteProducao.getSelectionModel().getSelectedItem());
            lotesProducaoEntrega.remove(tblLoteProducao.getSelectionModel().getSelectedItem());
        }
    }
    //</editor-fold>

}
