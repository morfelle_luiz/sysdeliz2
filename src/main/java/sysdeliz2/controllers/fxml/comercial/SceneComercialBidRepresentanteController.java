/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.comercial;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.controlsfx.control.table.TableFilter;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.pcp.ScenePcpPriorizarReferenciasController;
import sysdeliz2.controllers.fxml.procura.FilterMarcaController;
import sysdeliz2.controllers.fxml.procura.FilterRepresentanteController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.*;
import sysdeliz2.models.properties.IndicadoresBidLinha;
import sysdeliz2.models.sysdeliz.SdGerenteRepresentante;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.sys.MailUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComercialBidRepresentanteController implements Initializable {


    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    Integer larguraTela = 0;
    Boolean displayedBid = Boolean.FALSE;
    Boolean abrirCaixaEmail = Boolean.FALSE;
    Boolean abrirNovamente = Boolean.TRUE;
    Locale BRAZIL = new Locale("pt", "BR");
    private Image imgDlz = new Image(getClass().getResource("/images/LogoDLZ2019_OK-2.png").toExternalForm());
    private Image imgFlorDeLis = new Image(getClass().getResource("/images/FlordeLis_LOGO_2019.png").toExternalForm());
    private Image imgUpWave = new Image(getClass().getResource("/images/logo-preta-upwave.jpg").toExternalForm());
    private final List<String> ufsEdimilson = Arrays.asList("MG", "PE", "PB", "PI", "AL", "RN", "BA", "SE", "MA", "CE", "RR", "AC", "AP", "TO", "AM", "RO", "PA");
    private final List<String> ufsKatia = Arrays.asList("PR", "SP", "ES", "RJ");
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Variáveis/Componentes FXML">
    @FXML
    private Label lbCidadesAtendidasClasseE;
    @FXML
    private Label lbCidadesAtendidasClasseP;
    @FXML
    private Label lbCidadesAtendidasClasseV;
    @FXML
    private Label lbCidadesAtendidasClasseVV;
    @FXML
    private Label lbCidadesAtendidasClasseS;
    @FXML
    private Label lbCidadesAtendidasClientesInativos;
    @FXML
    private Label lbCidadesAtendidasTerceiraColecao;
    @FXML
    private Label lbCidadesAtendidasSegundaColecao;
    @FXML
    private Label lbCidadesAtendidasPrimeiraColecao;
    @FXML
    private Label lbCidadesAtendidasQuartaColecao;
    @FXML
    private Label lbClientesInativos;
    @FXML
    private SplitPane splitWindow;
    @FXML
    private TextField tboxCodRepresentante;
    @FXML
    private Button btnProcurarPresentante;
    @FXML
    private TextField tboxCodColecao;
    @FXML
    private Button btnProcurarColecao;
    @FXML
    private TextField tboxCodFamilia;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private Button btnProcurar;
    @FXML
    private TableView<Bid> tblRepresentantes;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaTp;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealTp;
    @FXML
    private TableColumn<Bid, Double> clmBidAttTp;
    @FXML
    private TableColumn<Bid, Double> clmBidMtaTf;
    @FXML
    private TableColumn<Bid, Double> clmBidRealTf;
    @FXML
    private TableColumn<Bid, Double> clmBidAttTf;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaCli;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealCli;
    @FXML
    private TableColumn<Bid, Double> clmBidAttCli;
    @FXML
    private TableColumn<Bid, Integer> clmBidMtaCln;
    @FXML
    private TableColumn<Bid, Integer> clmBidRealCln;
    @FXML
    private TableColumn<Bid, Double> clmBidAttCln;
    @FXML
    private TableColumn<Bid, Double> clmBidMtaPm;
    @FXML
    private TableColumn<Bid, Double> clmBidRealPm;
    @FXML
    private TableColumn<Bid, Double> clmBidAttPm;
    @FXML
    private Label lbUltimaAtualizacao;
    @FXML
    private Button btnAtualizarDados;
    @FXML
    private Button btnShowBid;
    @FXML
    private AnchorPane anchorBid;
    @FXML
    private VBox vboxBid;
    @FXML
    private Pane gridJornada;
    @FXML
    private GridPane gridPm;
    @FXML
    private TextField pmRpm;
    @FXML
    private Label pmAttMeta;
    @FXML
    private TextField pmLy;
    @FXML
    private TextField pmMta;
    @FXML
    private TextField pmRcm;
    @FXML
    private GridPane gridPg;
    @FXML
    private TextField pgRpm;
    @FXML
    private Label pgAttMeta;
    @FXML
    private TextField pgMta;
    @FXML
    private TextField pgRcm;
    @FXML
    private TextField pgLy;
    @FXML
    private GridPane gridPr;
    @FXML
    private Label prAttMeta;
    @FXML
    private TextField prRpm;
    @FXML
    private TextField prMta;
    @FXML
    private TextField prRcm;
    @FXML
    private TextField prLy;
    @FXML
    private GridPane gridCd;
    @FXML
    private Label cdAttMeta;
    @FXML
    private TextField cdMta;
    @FXML
    private TextField cdRcm;
    @FXML
    private TextField cdLy;
    @FXML
    private TextField cdRpm;
    @FXML
    private GridPane gridPv;
    @FXML
    private Label pvAttMeta;
    @FXML
    private TextField pvMta;
    @FXML
    private TextField pvRcm;
    @FXML
    private TextField pvLy;
    @FXML
    private TextField pvRpm;
    @FXML
    private Label lbJornada;
    @FXML
    private ImageView imgLogoMarca;
    @FXML
    private GridPane gridKa;
    @FXML
    private Label kaAttMeta;
    @FXML
    private TextField kaMta;
    @FXML
    private TextField kaRcm;
    @FXML
    private TextField kaLy;
    @FXML
    private TextField kaRpm;
    @FXML
    private GridPane gridTf;
    @FXML
    private Label tfAttMeta;
    @FXML
    private TextField tfMta;
    @FXML
    private TextField tfRcm;
    @FXML
    private TextField tfLy;
    @FXML
    private TextField tfRpm;
    @FXML
    private GridPane gridTp;
    @FXML
    private Label tpAttMeta;
    @FXML
    private TextField tpMta;
    @FXML
    private TextField tpRcm;
    @FXML
    private TextField tpLy;
    @FXML
    private TextField tpRpm;
    @FXML
    private GridPane gridVp;
    @FXML
    private Label vpAttMeta;
    @FXML
    private TextField vpMta;
    @FXML
    private TextField vpRcm;
    @FXML
    private TextField vpLy;
    @FXML
    private TextField vpRpm;
    @FXML
    private GridPane gridPp;
    @FXML
    private Label ppAttMeta;
    @FXML
    private TextField ppMta;
    @FXML
    private TextField ppRcm;
    @FXML
    private TextField ppLy;
    @FXML
    private TextField ppRpm;
    @FXML
    private Pane gridPerformance;
    @FXML
    private Label lbRegiaoRep;
    @FXML
    private Label lbSemanaBid;
    @FXML
    private Label lbCodRep;
    @FXML
    private Label lbRep;
    @FXML
    private GridPane gridBasico;
    @FXML
    private TextField tpMtaBas;
    @FXML
    private TextField tfMtaBas;
    @FXML
    private TextField perTfBas;
    @FXML
    private TextField tpRcmBas;
    @FXML
    private TextField tfRcmBas;
    @FXML
    private TextField perTpBas;
    @FXML
    private TextField lyTfBas;
    @FXML
    private TextField lyTpBas;
    @FXML
    private TextField rcmPgBas;
    @FXML
    private TextField rcmPrBas;
    @FXML
    private GridPane gridDenim;
    @FXML
    private TextField tpMtaDen;
    @FXML
    private TextField tfMtaDen;
    @FXML
    private TextField perTfDen;
    @FXML
    private TextField tpRcmDen;
    @FXML
    private TextField tfRcmDen;
    @FXML
    private TextField perTpDen;
    @FXML
    private TextField lyTfDen;
    @FXML
    private TextField lyTpDen;
    @FXML
    private TextField rcmPgDen;
    @FXML
    private TextField rcmPrDen;
    @FXML
    private GridPane gridCasual;
    @FXML
    private Label lbLinhaPrincipal;
    @FXML
    private TextField tpMtaLinPri;
    @FXML
    private TextField tfMtaLinPri;
    @FXML
    private TextField perTfLinPri;
    @FXML
    private TextField tpRcmLinPri;
    @FXML
    private TextField tfRcmLinPri;
    @FXML
    private TextField perTpLinPri;
    @FXML
    private TextField lyTfLinPri;
    @FXML
    private TextField lyTpLinPri;
    @FXML
    private TextField rcmPgLinPri;
    @FXML
    private TextField rcmPrLinPri;
    @FXML
    private GridPane gridCollection;
    @FXML
    private Label lbLinhaSecundaria;
    @FXML
    private TextField tpMtaLinSec;
    @FXML
    private TextField tfMtaLinSec;
    @FXML
    private TextField perTfLinSec;
    @FXML
    private TextField tpRcmLinSec;
    @FXML
    private TextField tfRcmLinSec;
    @FXML
    private TextField perTpLinSec;
    @FXML
    private TextField lyTfLinSec;
    @FXML
    private TextField lyTpLinSec;
    @FXML
    private TextField rcmPgLinSec;
    @FXML
    private TextField rcmPrLinSec;
    @FXML
    private GridPane gridMostruario;
    @FXML
    private TextField clnMta;
    @FXML
    private TextField clmReal;
    @FXML
    private TextField clmAttMeta;
    @FXML
    private TextField mostrRep;
    @FXML
    private TextField lyClnAtt;
    @FXML
    private Button btnEnviarBid;
    @FXML
    private SplitMenuButton splBtnImprimirBid;
    @FXML
    private MenuItem btnImprimirBidCompleto;
    @FXML
    private MenuItem btnImprimirCidades;
    @FXML
    private TableView<BidPremiado> tblBidPremiado;
    @FXML
    private TableColumn<BidPremiado, String> clmIndicadorObrigatorio;
    @FXML
    private TableColumn<BidPremiado, Double> clmRealMeta;
    @FXML
    private TableColumn<BidPremiado, Double> clmGraduacao;
    @FXML
    private TableColumn<BidPremiado, Double> clmRealizadoRep;
    @FXML
    private TableColumn<BidPremiado, Double> clmPercPremio;
    @FXML
    private TableColumn<BidPremiado, Double> clmPremio;
    @FXML
    private Label lbTerceiraColecao;
    @FXML
    private Label lbSegundaColecao;
    @FXML
    private Label lbPrimeiraColecao;
    @FXML
    private Label lbQuartaColecao;
    @FXML
    private TableView<AtendimentoCidadeRep> tblSica;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaPopulacao;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaCidade;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmSicaIpca;
    @FXML
    private TableColumn<AtendimentoCidadeRep, String> clmClasse;
    @FXML
    private TableColumn<AtendimentoCidadeRep, Integer> clmStatus;
    @FXML
    public TextField tboxCodigoMarca;
    //</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tblRepresentantes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData(oldValue);
            try {
                bindData(newValue);
            } catch (SQLException ex) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension scrnsize = toolkit.getScreenSize();
        larguraTela = scrnsize.width;
        splitWindow.setDividerPositions(1.0);
        try {
            String[] colecoes = DAOFactory.getColecaoDAO().getTresUltimasColecoes();
            lbPrimeiraColecao.setText(colecoes[0]);
            lbSegundaColecao.setText(colecoes[1]);
            lbTerceiraColecao.setText(colecoes[2]);
            lbQuartaColecao.setText(colecoes[3]);
            
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        
        this.factoryColumns();
        btnShowBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        splBtnImprimirBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        btnEnviarBid.disableProperty().bind(tblRepresentantes.getSelectionModel().selectedItemProperty().isNull());
        TextFieldUtils.inSql(tboxCodColecao);
        TextFieldUtils.inSql(tboxCodRepresentante);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos Locais">
    private void factoryColumns() {
        
        clmClasse.setCellFactory((TableColumn<AtendimentoCidadeRep, String> param) -> {
            TableCell cell = new TableCell<AtendimentoCidadeRep, String>() {
                
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    AtendimentoCidadeRep currentCidade = currentRow == null ? null : (AtendimentoCidadeRep) currentRow.getItem();
                    if (currentCidade != null) {
                        String status = currentCidade.getClasse();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(String priority) {
                    switch(priority) {
                        case "VV":
                            setStyle("-fx-background-color: #00FFFF; ");
                            break;
                        case "V":
                            setStyle("-fx-background-color: #00FFFF;");
                            break;
                        case "P":
                            setStyle("-fx-background-color: #FF8C00;");
                            break;
                        case "S":
                            setStyle("-fx-background-color: #9932CC;");
                            break;
                        case "E":
                            setStyle("-fx-background-color: #A9A9A9;");
                            break;
                    }
                }
                
                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });
        
        clmStatus.setCellFactory((TableColumn<AtendimentoCidadeRep, Integer> param) -> {
            TableCell cell = new TableCell<AtendimentoCidadeRep, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString() + "");
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    AtendimentoCidadeRep currentCidade = currentRow == null ? null : (AtendimentoCidadeRep) currentRow.getItem();
                    if (currentCidade != null) {
                        Integer status = currentCidade.getStatus_comp_col();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Integer priority) {
                    switch(priority) {
                        case 1:
                            setStyle("-fx-background-color: #00FF00; -fx-text-fill: #00FF00;");
                            break;
                        case 2:
                            setStyle("-fx-background-color: #00BFFF; -fx-text-fill: #00BFFF;");
                            break;
                        case 3:
                            setStyle("-fx-background-color: #FFFF00; -fx-text-fill: #FFFF00;");
                            break;
                        case 4:
                            setStyle("-fx-background-color: #FFA500; -fx-text-fill: #FFA500;");
                            break;
                        case 5:
                            setStyle("-fx-background-color: #FF0000; -fx-text-fill: #FF0000;");
                            break;
                    }
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmIndicadorObrigatorio.setCellFactory((TableColumn<BidPremiado, String> param) -> {
            TableCell cell = new TableCell<BidPremiado, String>() {
                
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidPremiado currentReserva = currentRow == null ? null : (BidPremiado) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getObrigatorio();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusLido");
                    
                }
                
                private void setPriorityStyle(String priority) {
                    switch(priority) {
                        case "X":
                            getStyleClass().add("statusLido");
                            break;
                    }
                }
                
                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });
        
        clmRealMeta.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    BidPremiado currentCidade = currentRow == null ? null : (BidPremiado) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getRel_meta();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority <= 79.999) {
                        setStyle("-fx-background-color: #FF0000; -fx-font-weight: bold;");
                    } else if (priority > 79.999 && priority < 99.999) {
                        setStyle("-fx-background-color: #FFFF00; -fx-font-weight: bold;");
                    } else {
                        setStyle("-fx-background-color: #32CD32; -fx-font-weight: bold;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmRealizadoRep.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmPercPremio.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmPremio.setCellFactory((TableColumn<BidPremiado, Double> param) -> {
            TableCell cell = new TableCell<BidPremiado, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidMtaTp.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidRealTp.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidMtaCli.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidRealCli.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidMtaCln.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidRealCln.setCellFactory((TableColumn<Bid, Integer> param) -> {
            TableCell cell = new TableCell<Bid, Integer>() {
                
                @Override
                public void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getIntegerInstance(BRAZIL);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Integer getString() {
                    return getItem() == null ? 0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidAttTp.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getTp_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidAttTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getTf_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidAttPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getPm_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidAttCln.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getCln_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidAttCli.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getPercentInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString() / 100));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Bid currentCidade = currentRow == null ? null : (Bid) currentRow.getItem();
                    if (currentCidade != null) {
                        Double realmeta = currentCidade.getPv_per();
                        if (!isHover() && !isSelected() && !isFocused() && (realmeta != null)) {
                            setPriorityStyle(realmeta);
                        }
                    }
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private void setPriorityStyle(Double priority) {
                    if (priority >= 100.0) {
                        setStyle("-fx-background-color: #FFD700;");
                    }
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidMtaPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidRealPm.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidMtaTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        
        clmBidRealTf.setCellFactory((TableColumn<Bid, Double> param) -> {
            TableCell cell = new TableCell<Bid, Double>() {
                
                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }
                
                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }
                
                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
    }
    
    private void bindData(Bid selectedBid) throws SQLException {
        if (selectedBid != null) {
            NumberFormat z = NumberFormat.getIntegerInstance(BRAZIL);
            NumberFormat y = NumberFormat.getNumberInstance(BRAZIL);
            NumberFormat w = NumberFormat.getCurrencyInstance(BRAZIL);
            
            // <editor-fold defaultstate="collapsed" desc="BID">
            pmRcm.textProperty().bind(selectedBid.pm_rcmProperty().asString("%12.2f"));
            pmRpm.textProperty().bind(selectedBid.pm_rpmProperty().asString("%12.2f"));
            pmMta.textProperty().bind(selectedBid.pm_mtaProperty().asString("%12.2f"));
            pmLy.textProperty().bind(selectedBid.pm_lyProperty().asString("%12.2f"));
            pmAttMeta.textProperty().bind(selectedBid.pm_perProperty().asString());
            cdRcm.textProperty().bind(selectedBid.cd_rcmProperty().asString());
            cdRpm.textProperty().bind(selectedBid.cd_rpmProperty().asString());
            cdMta.textProperty().bind(selectedBid.cd_mtaProperty().asString());
            cdLy.textProperty().bind(selectedBid.cd_lyProperty().asString());
            cdAttMeta.textProperty().bind(selectedBid.cd_perProperty().asString());
            pvRcm.textProperty().bind(selectedBid.pv_rcmProperty().asString());
            pvRpm.textProperty().bind(selectedBid.pv_rpmProperty().asString());
            pvMta.textProperty().bind(selectedBid.pv_mtaProperty().asString());
            pvLy.textProperty().bind(selectedBid.pv_lyProperty().asString());
            pvAttMeta.textProperty().bind(selectedBid.pv_perProperty().asString());
            kaRcm.textProperty().bind(selectedBid.ka_rcmProperty().asString());
            kaRpm.textProperty().bind(selectedBid.ka_rpmProperty().asString());
            kaMta.textProperty().bind(selectedBid.ka_mtaProperty().asString());
            kaLy.textProperty().bind(selectedBid.ka_lyProperty().asString());
            kaAttMeta.textProperty().bind(selectedBid.ka_perProperty().asString());
            ppRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_rcmProperty().get())));
            ppRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_rpmProperty().get())));
            ppMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_mtaProperty().get())));
            ppLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.pp_lyProperty().get())));
            ppAttMeta.textProperty().bind(selectedBid.pp_perProperty().asString());
            vpRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_rcmProperty().get())));
            vpRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_rpmProperty().get())));
            vpMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_mtaProperty().get())));
            vpLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.vp_lyProperty().get())));
            vpAttMeta.textProperty().bind(selectedBid.vp_perProperty().asString());
            tpRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_rcmProperty().get())));
            tpRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_rpmProperty().get())));
            tpMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_mtaProperty().get())));
            tpLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tp_lyProperty().get())));
            tpAttMeta.textProperty().bind(selectedBid.tp_perProperty().asString());
            tfRcm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_rcmProperty().get())));
            tfRpm.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_rpmProperty().get())));
            tfMta.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_mtaProperty().get())));
            tfLy.textProperty().bind(new SimpleStringProperty(z.format(selectedBid.tf_lyProperty().get())));
            tfAttMeta.textProperty().bind(selectedBid.tf_perProperty().asString());
            pgRcm.textProperty().bind(selectedBid.pg_rcmProperty().asString("%12.2f"));
            pgRpm.textProperty().bind(selectedBid.pg_rpmProperty().asString("%12.2f"));
            pgMta.textProperty().bind(selectedBid.pg_mtaProperty().asString("%12.2f"));
            pgLy.textProperty().bind(selectedBid.pg_lyProperty().asString("%12.2f"));
            pgAttMeta.textProperty().bind(selectedBid.pg_perProperty().asString());
            prRcm.textProperty().bind(selectedBid.pr_rcmProperty().asString("%12.2f"));
            prRpm.textProperty().bind(selectedBid.pr_rpmProperty().multiply(100).asString("%12.2f"));
            prMta.textProperty().bind(selectedBid.pr_mtaProperty().multiply(100).asString("%12.2f"));
            prLy.textProperty().bind(selectedBid.pr_lyProperty().asString("%12.2f"));
            prAttMeta.textProperty().bind(selectedBid.pr_perProperty().asString());
            clmReal.textProperty().bind(selectedBid.cln_rcmProperty().asString());
            clnMta.textProperty().bind(selectedBid.cln_mtaProperty().asString());
            clmAttMeta.textProperty().bind(selectedBid.cln_perProperty().asString("%4.2f"));
            lyClnAtt.textProperty().bind(selectedBid.cln_att_lyProperty().asString());
            mostrRep.textProperty().bind(selectedBid.mostrProperty().asString());
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LINHAS">
            setValuesTextFieldLinhas(z, y, w, selectedBid);
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="DADOS REP">
            lbCodRep.textProperty().bind(selectedBid.codrepProperty());
            lbRep.textProperty().bind(selectedBid.representProperty());
            lbRegiaoRep.textProperty().bind(selectedBid.regiaoProperty());
            lbSemanaBid.textProperty().bind(selectedBid.sem_hojeProperty().asString().concat("/").concat(selectedBid.sem_totalProperty().asString()));
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="CIDADES">
            tblSica.setItems(DAOFactory.getBidDAO().getCidadesRep(selectedBid.codrepProperty().get(), selectedBid.codmarProperty().get()));
            TableFilter filter = new TableFilter(tblSica);
            lbCidadesAtendidasPrimeiraColecao.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getStatus_comp_col() == 1).count()
                            + ")"
            );
            
            lbCidadesAtendidasSegundaColecao.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getStatus_comp_col() == 2).count()
                            + ")"
            );
            
            lbCidadesAtendidasTerceiraColecao.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getStatus_comp_col() == 3).count()
                            + ")"
            );
            
            lbCidadesAtendidasQuartaColecao.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getStatus_comp_col() == 4).count()
                            + ")"
            );
            
            lbCidadesAtendidasClientesInativos.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getStatus_comp_col() == 5).count()
                            + ")"
            );
            
            lbCidadesAtendidasClasseVV.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getClasse().equals("VV")).count()
                            + ")"
            );
            
            lbCidadesAtendidasClasseV.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getClasse().equals("V")).count()
                            + ")"
            );
            
            lbCidadesAtendidasClasseP.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getClasse().equals("P")).count()
                            + ")"
            );
            
            lbCidadesAtendidasClasseS.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getClasse().equals("S")).count()
                            + ")"
            );
            
            lbCidadesAtendidasClasseE.setText(
                    "("
                            + tblSica.getItems().stream().filter(t -> t.getClasse().equals("E")).count()
                            + ")"
            );
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PREMIADO">
            if (selectedBid.getCodrep().equals("000") || selectedBid.getCodrep().equals("001")) {
                tblBidPremiado.getItems().clear();
            } else {
                tblBidPremiado.setItems(DAOFactory.getBidDAO().getBidPremiadoRep(selectedBid.codrepProperty().get(), selectedBid.codmarProperty().get(), selectedBid.codcolProperty().get()));
            }
            //</editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="STYLE CSS">
            gridJornada.getStyleClass().clear();
            gridCd.getStyleClass().clear();
            gridPg.getStyleClass().clear();
            gridPm.getStyleClass().clear();
            gridPr.getStyleClass().clear();
            gridPv.getStyleClass().clear();
            lbJornada.getStyleClass().clear();
            
            gridPerformance.getStyleClass().clear();
            gridKa.getStyleClass().clear();
            gridPp.getStyleClass().clear();
            gridTf.getStyleClass().clear();
            gridTp.getStyleClass().clear();
            gridVp.getStyleClass().clear();
            gridBasico.getStyleClass().clear();
            gridDenim.getStyleClass().clear();
            gridCollection.getStyleClass().clear();
            gridCasual.getStyleClass().clear();
            gridMostruario.getStyleClass().clear();
            
            this.factoryLabelAttMeta();
            if(Globals.getEmpresaLogada().getCodigo().equals("2000")){
                imgLogoMarca.setImage(imgUpWave);

                gridJornada.getStyleClass().add("gridBidJornadaDlz");
                lbJornada.getStyleClass().add("gridBidJornadaDlz");
                gridCd.getStyleClass().add("gridBidJornadaDlz");
                gridPg.getStyleClass().add("gridBidJornadaDlz");
                gridPm.getStyleClass().add("gridBidJornadaDlz");
                gridPr.getStyleClass().add("gridBidJornadaDlz");
                gridPv.getStyleClass().add("gridBidJornadaDlz");

                gridPerformance.getStyleClass().add("gridBidPerformanceDlz");
                gridKa.getStyleClass().add("gridBidPerformanceDlz");
                gridPp.getStyleClass().add("gridBidPerformanceDlz");
                gridTf.getStyleClass().add("gridBidPerformanceDlz");
                gridTp.getStyleClass().add("gridBidPerformanceDlz");
                gridVp.getStyleClass().add("gridBidPerformanceDlz");
                gridBasico.getStyleClass().add("gridBidPerformanceDlz");
                gridDenim.getStyleClass().add("gridBidPerformanceDlz");
                gridCollection.getStyleClass().add("gridBidPerformanceDlz");
                gridCasual.getStyleClass().add("gridBidPerformanceDlz");
                gridMostruario.getStyleClass().add("gridBidPerformanceDlz");

            }else {
                if (selectedBid.getCodmar().equals("D")) {
                    imgLogoMarca.setImage(imgDlz);
                    gridJornada.getStyleClass().add("gridBidJornadaDlz");
                    lbJornada.getStyleClass().add("gridBidJornadaDlz");
                    gridCd.getStyleClass().add("gridBidJornadaDlz");
                    gridPg.getStyleClass().add("gridBidJornadaDlz");
                    gridPm.getStyleClass().add("gridBidJornadaDlz");
                    gridPr.getStyleClass().add("gridBidJornadaDlz");
                    gridPv.getStyleClass().add("gridBidJornadaDlz");

                    gridPerformance.getStyleClass().add("gridBidPerformanceDlz");
                    gridKa.getStyleClass().add("gridBidPerformanceDlz");
                    gridPp.getStyleClass().add("gridBidPerformanceDlz");
                    gridTf.getStyleClass().add("gridBidPerformanceDlz");
                    gridTp.getStyleClass().add("gridBidPerformanceDlz");
                    gridVp.getStyleClass().add("gridBidPerformanceDlz");
                    gridBasico.getStyleClass().add("gridBidPerformanceDlz");
                    gridDenim.getStyleClass().add("gridBidPerformanceDlz");
                    gridCollection.getStyleClass().add("gridBidPerformanceDlz");
                    gridCasual.getStyleClass().add("gridBidPerformanceDlz");
                    gridMostruario.getStyleClass().add("gridBidPerformanceDlz");

                } else {
                    imgLogoMarca.setImage(imgFlorDeLis);
                    gridJornada.getStyleClass().add("gridBidJornadaFlor");
                    gridCd.getStyleClass().add("gridBidJornadaFlor");
                    gridPg.getStyleClass().add("gridBidJornadaFlor");
                    gridPm.getStyleClass().add("gridBidJornadaFlor");
                    gridPr.getStyleClass().add("gridBidJornadaFlor");
                    gridPv.getStyleClass().add("gridBidJornadaFlor");
                    lbJornada.getStyleClass().add("gridBidJornadaFlor");

                    gridPerformance.getStyleClass().add("gridBidPerformanceFlor");
                    gridKa.getStyleClass().add("gridBidPerformanceFlor");
                    gridPp.getStyleClass().add("gridBidPerformanceFlor");
                    gridTf.getStyleClass().add("gridBidPerformanceFlor");
                    gridTp.getStyleClass().add("gridBidPerformanceFlor");
                    gridVp.getStyleClass().add("gridBidPerformanceFlor");
                    gridBasico.getStyleClass().add("gridBidPerformanceFlor");
                    gridDenim.getStyleClass().add("gridBidPerformanceFlor");
                    gridCollection.getStyleClass().add("gridBidPerformanceFlor");
                    gridCasual.getStyleClass().add("gridBidPerformanceFlor");
                    gridMostruario.getStyleClass().add("gridBidPerformanceFlor");
                }
            }
            //</editor-fold>
        }
    }

    private void setValuesTextFieldLinhas(NumberFormat z, NumberFormat y, NumberFormat w, Bid selectedBid) throws SQLException {
        Map<String, IndicadoresBidLinha> indicadorLinha = DAOFactory.getBidDAO().getBidLinha(selectedBid.getCodrep(), selectedBid.getCodcol(), selectedBid.getCodmar());
        
        if (indicadorLinha.get("ESSENTIAL") != null) {
            tpRcmBas.setText(z.format(indicadorLinha.get("ESSENTIAL").getQtde_tp()));
            tpMtaBas.setText(z.format(indicadorLinha.get("ESSENTIAL").getMeta_tp()));
            perTpBas.setText(y.format(indicadorLinha.get("ESSENTIAL").getAtt_tp()));
            tfRcmBas.setText(w.format(indicadorLinha.get("ESSENTIAL").getQtde_tf()));
            tfMtaBas.setText(w.format(indicadorLinha.get("ESSENTIAL").getMeta_tf()));
            perTfBas.setText(y.format(indicadorLinha.get("ESSENTIAL").getAtt_tf()));
            lyTpBas.setText(z.format(indicadorLinha.get("ESSENTIAL").getLy_tp()));
            lyTfBas.setText(w.format(indicadorLinha.get("ESSENTIAL").getLy_tf()));
            rcmPgBas.setText(y.format(indicadorLinha.get("ESSENTIAL").getPg()));
            rcmPrBas.setText(y.format(indicadorLinha.get("ESSENTIAL").getPr()));
        } else {
            tpRcmBas.setText("0");
            tpMtaBas.setText("0");
            perTpBas.setText("0");
            tfRcmBas.setText("0");
            tfMtaBas.setText("0");
            perTpBas.setText("0");
            lyTpBas.setText("0");
            lyTfBas.setText("0");
            rcmPgBas.setText("0");
            rcmPrBas.setText("0");
        }
        
        if (indicadorLinha.get("DENIM") != null) {
            tpRcmDen.setText(z.format(indicadorLinha.get("DENIM").getQtde_tp()));
            tpMtaDen.setText(z.format(indicadorLinha.get("DENIM").getMeta_tp()));
            perTpDen.setText(y.format(indicadorLinha.get("DENIM").getAtt_tp()));
            tfRcmDen.setText(w.format(indicadorLinha.get("DENIM").getQtde_tf()));
            tfMtaDen.setText(w.format(indicadorLinha.get("DENIM").getMeta_tf()));
            perTfDen.setText(y.format(indicadorLinha.get("DENIM").getAtt_tf()));
            lyTpDen.setText(z.format(indicadorLinha.get("DENIM").getLy_tp()));
            lyTfDen.setText(w.format(indicadorLinha.get("DENIM").getLy_tf()));
            rcmPgDen.setText(y.format(indicadorLinha.get("DENIM").getPg()));
            rcmPrDen.setText(y.format(indicadorLinha.get("DENIM").getPr()));
        } else {
            tpRcmDen.setText("0");
            tpMtaDen.setText("0");
            perTpDen.setText("0");
            tfRcmDen.setText("0");
            tfMtaDen.setText("0");
            perTfDen.setText("0");
            lyTpDen.setText("0");
            lyTfDen.setText("0");
            rcmPgDen.setText("0");
            rcmPrDen.setText("0");
        }
        
        if (selectedBid.getCodmar().equals("D") || selectedBid.getCodmar().equals("U") ) {
            lbLinhaPrincipal.setText("CASUAL");
            
            if (indicadorLinha.get("CASUAL") != null) {
                tpRcmLinPri.setText(z.format(indicadorLinha.get("CASUAL").getQtde_tp()));
                tpMtaLinPri.setText(z.format(indicadorLinha.get("CASUAL").getMeta_tp()));
                perTpLinPri.setText(y.format(indicadorLinha.get("CASUAL").getAtt_tp()));
                tfRcmLinPri.setText(w.format(indicadorLinha.get("CASUAL").getQtde_tf()));
                tfMtaLinPri.setText(w.format(indicadorLinha.get("CASUAL").getMeta_tf()));
                perTfLinPri.setText(y.format(indicadorLinha.get("CASUAL").getAtt_tf()));
                lyTpLinPri.setText(z.format(indicadorLinha.get("CASUAL").getLy_tp()));
                lyTfLinPri.setText(w.format(indicadorLinha.get("CASUAL").getLy_tf()));
                rcmPgLinPri.setText(y.format(indicadorLinha.get("CASUAL").getPg()));
                rcmPrLinPri.setText(y.format(indicadorLinha.get("CASUAL").getPr()));
            } else {
                tpRcmLinPri.setText("0");
                tpMtaLinPri.setText("0");
                perTpLinPri.setText("0");
                tfRcmLinPri.setText("0");
                tfMtaLinPri.setText("0");
                perTfLinPri.setText("0");
                lyTpLinPri.setText("0");
                lyTfLinPri.setText("0");
                rcmPgLinPri.setText("0");
                rcmPrLinPri.setText("0");
            }
        } else {
            lbLinhaPrincipal.setText("COLLECTION");
            
            if (indicadorLinha.get("COLLECTION") != null) {
                tpRcmLinPri.setText(z.format(indicadorLinha.get("COLLECTION").getQtde_tp()));
                tpMtaLinPri.setText(z.format(indicadorLinha.get("COLLECTION").getMeta_tp()));
                perTpLinPri.setText(y.format(indicadorLinha.get("COLLECTION").getAtt_tp()));
                tfRcmLinPri.setText(w.format(indicadorLinha.get("COLLECTION").getQtde_tf()));
                tfMtaLinPri.setText(w.format(indicadorLinha.get("COLLECTION").getMeta_tf()));
                perTfLinPri.setText(y.format(indicadorLinha.get("COLLECTION").getAtt_tf()));
                lyTpLinPri.setText(z.format(indicadorLinha.get("COLLECTION").getLy_tp()));
                lyTfLinPri.setText(w.format(indicadorLinha.get("COLLECTION").getLy_tf()));
                rcmPgLinPri.setText(y.format(indicadorLinha.get("COLLECTION").getPg()));
                rcmPrLinPri.setText(y.format(indicadorLinha.get("COLLECTION").getPr()));
            } else {
                tpRcmLinPri.setText("0");
                tpMtaLinPri.setText("0");
                perTpLinPri.setText("0");
                tfRcmLinPri.setText("0");
                tfMtaLinPri.setText("0");
                perTfLinPri.setText("0");
                lyTpLinPri.setText("0");
                lyTfLinPri.setText("0");
                rcmPgLinPri.setText("0");
                rcmPrLinPri.setText("0");
            }
        }
        
        if (selectedBid.getCodmar().equals("D") || selectedBid.getCodmar().equals("U")) {
            lbLinhaSecundaria.setText("CAMISA");
            
            if (indicadorLinha.get("CAMISA") != null) {
                tpRcmLinSec.setText(z.format(indicadorLinha.get("CAMISA").getQtde_tp()));
                tpMtaLinSec.setText(z.format(indicadorLinha.get("CAMISA").getMeta_tp()));
                perTpLinSec.setText(y.format(indicadorLinha.get("CAMISA").getAtt_tp()));
                tfRcmLinSec.setText(w.format(indicadorLinha.get("CAMISA").getQtde_tf()));
                tfMtaLinSec.setText(w.format(indicadorLinha.get("CAMISA").getMeta_tf()));
                perTfLinSec.setText(y.format(indicadorLinha.get("CAMISA").getAtt_tf()));
                lyTpLinSec.setText(z.format(indicadorLinha.get("CAMISA").getLy_tp()));
                lyTfLinSec.setText(w.format(indicadorLinha.get("CAMISA").getLy_tf()));
                rcmPgLinSec.setText(y.format(indicadorLinha.get("CAMISA").getPg()));
                rcmPrLinSec.setText(y.format(indicadorLinha.get("CAMISA").getPr()));
            } else {
                tpRcmLinSec.setText("0");
                tpMtaLinSec.setText("0");
                perTpLinSec.setText("0");
                tfRcmLinSec.setText("0");
                tfMtaLinSec.setText("0");
                perTfLinSec.setText("0");
                lyTpLinSec.setText("0");
                lyTfLinSec.setText("0");
                rcmPgLinSec.setText("0");
                rcmPrLinSec.setText("0");
            }
        } else {
            lbLinhaSecundaria.setText("WISHES");
            
            if (indicadorLinha.get("WISHES") != null) {
                tpRcmLinSec.setText(z.format(indicadorLinha.get("WISHES").getQtde_tp()));
                tpMtaLinSec.setText(z.format(indicadorLinha.get("WISHES").getMeta_tp()));
                perTpLinSec.setText(y.format(indicadorLinha.get("WISHES").getAtt_tp()));
                tfRcmLinSec.setText(w.format(indicadorLinha.get("WISHES").getQtde_tf()));
                tfMtaLinSec.setText(w.format(indicadorLinha.get("WISHES").getMeta_tf()));
                perTfLinSec.setText(y.format(indicadorLinha.get("WISHES").getAtt_tf()));
                lyTpLinSec.setText(z.format(indicadorLinha.get("WISHES").getLy_tp()));
                lyTfLinSec.setText(w.format(indicadorLinha.get("WISHES").getLy_tf()));
                rcmPgLinSec.setText(y.format(indicadorLinha.get("WISHES").getPg()));
                rcmPrLinSec.setText(y.format(indicadorLinha.get("WISHES").getPr()));
            } else {
                tpRcmLinSec.setText("0");
                tpMtaLinSec.setText("0");
                perTpLinSec.setText("0");
                tfRcmLinSec.setText("0");
                tfMtaLinSec.setText("0");
                perTfLinSec.setText("0");
                lyTpLinSec.setText("0");
                lyTfLinSec.setText("0");
                rcmPgLinSec.setText("0");
                rcmPrLinSec.setText("0");
            }
        }
    }
    
    private void unbindData(Bid selectedBid) {
        if (selectedBid != null) {
            pmRcm.textProperty().unbind();
            pmRpm.textProperty().unbind();
            pmMta.textProperty().unbind();
            pmLy.textProperty().unbind();
            pmAttMeta.textProperty().unbind();
            cdRcm.textProperty().unbind();
            cdRpm.textProperty().unbind();
            cdMta.textProperty().unbind();
            cdLy.textProperty().unbind();
            cdAttMeta.textProperty().unbind();
            pvRcm.textProperty().unbind();
            pvRpm.textProperty().unbind();
            pvMta.textProperty().unbind();
            pvLy.textProperty().unbind();
            pvAttMeta.textProperty().unbind();
            kaRcm.textProperty().unbind();
            kaRpm.textProperty().unbind();
            kaMta.textProperty().unbind();
            kaLy.textProperty().unbind();
            kaAttMeta.textProperty().unbind();
            ppRcm.textProperty().unbind();
            ppRpm.textProperty().unbind();
            ppMta.textProperty().unbind();
            ppLy.textProperty().unbind();
            ppAttMeta.textProperty().unbind();
            vpRcm.textProperty().unbind();
            vpRpm.textProperty().unbind();
            vpMta.textProperty().unbind();
            vpLy.textProperty().unbind();
            vpAttMeta.textProperty().unbind();
            tpRcm.textProperty().unbind();
            tpRpm.textProperty().unbind();
            tpMta.textProperty().unbind();
            tpLy.textProperty().unbind();
            tpAttMeta.textProperty().unbind();
            tfRcm.textProperty().unbind();
            tfRpm.textProperty().unbind();
            tfMta.textProperty().unbind();
            tfLy.textProperty().unbind();
            tfAttMeta.textProperty().unbind();
            pgRcm.textProperty().unbind();
            pgRpm.textProperty().unbind();
            pgMta.textProperty().unbind();
            pgLy.textProperty().unbind();
            pgAttMeta.textProperty().unbind();
            prRcm.textProperty().unbind();
            prRpm.textProperty().unbind();
            prMta.textProperty().unbind();
            prLy.textProperty().unbind();
            prAttMeta.textProperty().unbind();
            clmReal.textProperty().unbind();
            clnMta.textProperty().unbind();
            clmAttMeta.textProperty().unbind();
            mostrRep.textProperty().unbind();

//            tpRcmBas.textProperty().unbind();
//            tpMtaBas.textProperty().unbind();
//            perTpBas.textProperty().unbind();
//
//            tpRcmDen.textProperty().unbind();
//            tpMtaDen.textProperty().unbind();
//            perTpDen.textProperty().unbind();
//
//            tpRcmLinPri.textProperty().unbind();
//            tpMtaLinPri.textProperty().unbind();
//            perTpLinPri.textProperty().unbind();
//            tfRcmLinPri.textProperty().unbind();
//            lyTpLinPri.textProperty().unbind();
//            lyTfLinPri.textProperty().unbind();
//
//            tpRcmLinSec.textProperty().unbind();
//            tpMtaLinSec.textProperty().unbind();
//            perTpLinSec.textProperty().unbind();
//            tfRcmLinSec.textProperty().unbind();
//            lyTpLinSec.textProperty().unbind();
//            lyTfLinSec.textProperty().unbind();
            lbCodRep.textProperty().unbind();
            lbRep.textProperty().unbind();
            lbRegiaoRep.textProperty().unbind();
            lbSemanaBid.textProperty().unbind();
            
            tblSica.setItems(FXCollections.observableArrayList());
            tblBidPremiado.setItems(FXCollections.observableArrayList());
            
            gridJornada.getStyleClass().clear();
            gridCd.getStyleClass().clear();
            gridPg.getStyleClass().clear();
            gridPm.getStyleClass().clear();
            gridPr.getStyleClass().clear();
            gridPv.getStyleClass().clear();
            lbJornada.getStyleClass().clear();
            
            gridPerformance.getStyleClass().clear();
            gridKa.getStyleClass().clear();
            gridPp.getStyleClass().clear();
            gridTf.getStyleClass().clear();
            gridTp.getStyleClass().clear();
            gridVp.getStyleClass().clear();
            gridBasico.getStyleClass().clear();
            gridDenim.getStyleClass().clear();
            gridCollection.getStyleClass().clear();
            gridCasual.getStyleClass().clear();
            gridMostruario.getStyleClass().clear();
            
            gridJornada.getStyleClass().add("gridBidJornada");
            gridCd.getStyleClass().add("gridBidJornada");
            gridPg.getStyleClass().add("gridBidJornada");
            gridPm.getStyleClass().add("gridBidJornada");
            gridPr.getStyleClass().add("gridBidJornada");
            gridPv.getStyleClass().add("gridBidJornada");
            lbJornada.getStyleClass().add("gridBidJornada");
            
            gridPerformance.getStyleClass().add("gridBidPerformance");
            gridKa.getStyleClass().add("gridBidPerformance");
            gridPp.getStyleClass().add("gridBidPerformance");
            gridTf.getStyleClass().add("gridBidPerformance");
            gridTp.getStyleClass().add("gridBidPerformance");
            gridVp.getStyleClass().add("gridBidPerformance");
            gridBasico.getStyleClass().add("gridBidPerformance");
            gridDenim.getStyleClass().add("gridBidPerformance");
            gridCollection.getStyleClass().add("gridBidPerformance");
            gridCasual.getStyleClass().add("gridBidPerformance");
            gridMostruario.getStyleClass().add("gridBidPerformance");
            
        }
    }
    
    private void factoryLabelAttMeta() {
        backgroundLabel(pmAttMeta);
        backgroundLabel(cdAttMeta);
        backgroundLabel(pvAttMeta);
        backgroundLabel(kaAttMeta);
        backgroundLabel(ppAttMeta);
        backgroundLabel(vpAttMeta);
        backgroundLabel(tpAttMeta);
        backgroundLabel(tfAttMeta);
        backgroundLabel(pgAttMeta);
        backgroundLabel(prAttMeta);
    }
    
    private void backgroundLabel(Label attMeta) {
        Double metaAtt = Double.parseDouble(attMeta.getText());
        if (metaAtt <= 79.999) {
            attMeta.setStyle("-fx-background-color: #FF0000; -fx-text-fill: #000;");
        } else if (metaAtt > 79.999 && metaAtt < 99.999) {
            attMeta.setStyle("-fx-background-color: #FFFF00; -fx-text-fill: #000;");
        } else {
            attMeta.setStyle("-fx-background-color: #32CD32; -fx-text-fill: #000;");
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos FXML">

    @FXML
    private void tboxCodigoMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        try {
            FilterMarcaController controlFilterMarca = new FilterMarcaController(Modals.FXMLWindow.FilterMarca, tboxCodigoMarca.getText());
            if (!controlFilterMarca.returnValue.isEmpty()) {
                tboxCodigoMarca.setText(controlFilterMarca.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (event != null) {
            event.consume();
        }
    }

    @FXML
    private void tboxCodRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarPresentanteOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarPresentanteOnAction(ActionEvent event) {
        try {
            FilterRepresentanteController ctrolFilter = new FilterRepresentanteController(Modals.FXMLWindow.FilterRepresentante, tboxCodRepresentante.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxCodRepresentante.setText(ctrolFilter.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void tboxCodColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarColecaoOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        FilterColecoesView filterColecoesView = new FilterColecoesView();
        filterColecoesView.setValorFiltroInicial(tboxCodColecao.getText());
        filterColecoesView.show(false);
        
        if (filterColecoesView.itensSelecionados.size() > 0) {
            tboxCodColecao.setText(filterColecoesView.getResultAsString(true));
        }
    }
    
    @FXML
    private void tboxCodFamiliaOnKeyReleased(KeyEvent event) {
    }
    
    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
    }
    
    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblRepresentantes.setItems(DAOFactory.getBidDAO().loadBid(tboxCodRepresentante.getText(),
                    tboxCodColecao.getText(), tboxCodigoMarca.getText().replaceAll("'",""), tboxCodFamilia.getText()));
            abrirCaixaEmail = Boolean.TRUE;
            abrirNovamente = Boolean.TRUE;
            
            lbUltimaAtualizacao.setText(DAOFactory.getLogDAO().getLastLogJobOracle().toUpperCase());
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnAtualizarDadosOnAction(ActionEvent event) {
    }
    
    @FXML
    private void btnShowBidOnAction(ActionEvent event) {
        Double larguraBid = 1.0 - (1300.0 / larguraTela.doubleValue());
        
        if (displayedBid) {
            displayedBid = Boolean.FALSE;
            splitWindow.setDividerPositions(1.0);
            btnShowBid.setText("Exibir BID");
        } else {
            displayedBid = Boolean.TRUE;
            splitWindow.setDividerPositions(larguraBid);
            btnShowBid.setText("Esconder BID");
        }
    }
    
    @FXML
    private void btnEnviarBidOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");
        WritableImage imagePremio = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File filePremio = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imagePremio, null), "png", filePremio);
            
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());
            
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCompleto.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdfNoOpen(romaneiosParaImpressao, "BidCompleto");
            
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String body_text = "";
        try {
            String line = null;
            String filePath = "c:/SysDelizLocal/body.mail";
            byte[] buffer = new byte[1000];
            FileInputStream inputStream = new FileInputStream(filePath);
            int total = 0;
            int nRead = 0;
            while ((nRead = inputStream.read(buffer)) != -1) {
                total += nRead;
                line += new String(buffer);
            }
            inputStream.close();
            
            if (abrirCaixaEmail || abrirNovamente) {
                String text_file = "";
                String[] dadosReturn = GUIUtils.showTextAreaDialogWithRemember("Conteúdo e-mail:", line);
                String[] lines = dadosReturn[0].split("\n");
                abrirNovamente = !Boolean.parseBoolean(dadosReturn[1]);
                FileWriter fileWriter = new FileWriter(filePath);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                for (String lineText : lines) {
                    body_text += lineText + "<br/>";
                    text_file += lineText + "\n";
                }
                bufferedWriter.write(text_file);
                bufferedWriter.close();
                abrirCaixaEmail = Boolean.FALSE;
            }
        } catch (FileNotFoundException ex) {
            String text_file = "";
            String filePath = "c:/SysDelizLocal/body.mail";
            String[] dadosReturn = GUIUtils.showTextAreaDialogWithRemember("Conteúdo e-mail:", null);
            String[] lines = dadosReturn[0].split("\n");
            abrirNovamente = !Boolean.parseBoolean(dadosReturn[1]);
            try {
                FileWriter fileWriter = new FileWriter(filePath);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                for (String lineText : lines) {
                    body_text += lineText + "<br/>";
                    text_file += lineText + "\n";
                }
                bufferedWriter.write(text_file);
                bufferedWriter.close();
            } catch (IOException ex1) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex1);
                GUIUtils.showException(ex1);
            }
            abrirCaixaEmail = Boolean.FALSE;
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        
        List<SdGerenteRepresentante> gerentes = (List<SdGerenteRepresentante>) new FluentDao()
                .selectFrom(SdGerenteRepresentante.class)
                .where(it -> it
                        .equal("id.codrep.codRep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep())
                        .equal("id.marca.codigo", tblRepresentantes.getSelectionModel().getSelectedItem().getCodmar()))
                .resultList();
        
        String emailRepGer = tblRepresentantes.getSelectionModel().getSelectedItem().getEmail_rep();
        //: ",elio@deliz.com.br"
        if(tblRepresentantes.getSelectionModel().getSelectedItem().getCodmar().equals("F")){
            emailRepGer += ",luis@deliz.com.br";
        }else if(tblRepresentantes.getSelectionModel().getSelectedItem().getCodmar().equals("D")){
            emailRepGer += ",luis@deliz.com.br";
        } else {
            emailRepGer += ",ricardo@upwave.com.br";
        }
        emailRepGer += gerentes.size() > 0 ? "," + gerentes.stream().map(it -> it.getId().getCodcli().getEmail()).collect(Collectors.joining(",")) : "";
        MailUtils.sendMailBid(MailUtils.AttachMail.FULL_BID,
                emailRepGer,
                body_text,
                tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent());
        try {
            DAOFactory.getLogDAO().saveLog(new Log(null, SceneMainController.getAcesso().getUsuario(), "Enviar", "BID",
                    tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep(), "Envio de BID por e-mail. Rep.: " + tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent()));
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnImprimirBidOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");
        
        WritableImage imageP = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File fileP = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imageP, null), "png", fileP);
            
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBid.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);
            
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void btnImprimirBidCompletoOnAction(ActionEvent event) {
        WritableImage image = vboxBid.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/printBid.png");
        WritableImage imagePremio = tblBidPremiado.snapshot(new SnapshotParameters(), null);
        File filePremio = new File("c:/SysDelizLocal/printBidPremiado.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            ImageIO.write(SwingFXUtils.fromFXImage(imagePremio, null), "png", filePremio);
            
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());
            
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCompleto.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);
            
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void btnImprimirCidadesOnAction(ActionEvent event) {
        
        String statusParaImpressao = tblSica.getItems().stream().map(it -> String.valueOf(it.getStatus_comp_col())).distinct().collect(Collectors.joining("|"));
        String classesParaImpressao = tblSica.getItems().stream().map(AtendimentoCidadeRep::getClasse).distinct().collect(Collectors.joining("|"));
        
        try {
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            Map parametros = new HashMap();
            parametros.put("codrep", tblRepresentantes.getSelectionModel().getSelectedItem().getCodrep());
            parametros.put("marca", tblRepresentantes.getSelectionModel().getSelectedItem().getMarca());
            parametros.put("represent", tblRepresentantes.getSelectionModel().getSelectedItem().getRepresent());
            parametros.put("col_pri", lbPrimeiraColecao.getText());
            parametros.put("col_seg", lbSegundaColecao.getText());
            parametros.put("col_ter", lbTerceiraColecao.getText());
            parametros.put("col_qua", lbQuartaColecao.getText());
            parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

            parametros.put("statusParaImpressao", statusParaImpressao.substring(0, statusParaImpressao.length() - 1));
            parametros.put("classesParaImpressao", classesParaImpressao.substring(0, classesParaImpressao.length() - 1));
            
            JasperPrint jasperPrint = null;
            JasperReport jr;
            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioBidCidadesRep.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPdf(romaneiosParaImpressao);
            
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (JRException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //</editor-fold>
}
