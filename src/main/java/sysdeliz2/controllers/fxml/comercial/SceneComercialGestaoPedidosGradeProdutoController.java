/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.comercial;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.table.TableProdutoGradeProduto;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComercialGestaoPedidosGradeProdutoController extends Modals implements Initializable {

    private String referencia;

    @FXML
    private TableView<TableProdutoGradeProduto> tblGradeProduto;
    @FXML
    private TableColumn<TableProdutoGradeProduto, Integer> clnSaldoProduto;

    /**
     * Initializes the controller class.
     */
    public SceneComercialGestaoPedidosGradeProdutoController(FXMLWindow file, String referencia) throws IOException {
        super(file);
        this.referencia = referencia;
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblGradeProduto.setItems(DAOFactory.getProdutoDAO().getGradeProduto(referencia));
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialGestaoPedidosGradeProdutoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

}
