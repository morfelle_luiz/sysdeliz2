package sysdeliz2.controllers.fxml.comercial;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterClienteController;
import sysdeliz2.controllers.fxml.procura.FilterPeriodoController;
import sysdeliz2.controllers.fxml.procura.FilterProdutoController;
import sysdeliz2.controllers.fxml.procura.FilterRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.Colecao;
import sysdeliz2.models.Log;
import sysdeliz2.models.Produto;
import sysdeliz2.models.sysdeliz.SdStatusProduto;
import sysdeliz2.models.sysdeliz.comercial.SdTransfProdMostruario;
import sysdeliz2.models.table.TableManutencaoPedidosCoresReferencia;
import sysdeliz2.models.table.TableManutencaoPedidosPedido;
import sysdeliz2.models.table.TableProdutoGradeProduto;
import sysdeliz2.models.ti.Mensagem;
import sysdeliz2.models.view.pcp.VSdStatusGradeProd;
import sysdeliz2.models.view.pcp.VStatusProdutoCor;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SceneComercialGestaoPedidosManutencaoPedidoController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Local vars">
    private String mensagemPadrao = "08";
    private int columnPosition = 10;
    private int columnCoresPosition = 1;
    private ListProperty<TableManutencaoPedidosPedido> pedidosRef = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ObservableList<Map<String, String>> produtosEsgotados = null;
    private ObservableList<Map<String, String>> produtosBloqueados = null;
    private Locale BRAZIL = new Locale("pt", "BR");
    private final Image imgBtnTrocar = new Image(getClass().getResource("/images/icons/buttons/change product (1).png").toExternalForm());
    private final Image imgBtnReservar = new Image(getClass().getResource("/images/icons/buttons/booking order (1).png").toExternalForm());
    private final Image imgBtnCancelar = new Image(getClass().getResource("/images/icons/buttons/delete product (1).png").toExternalForm());
    private String coresIn;
    private Tooltip tipTFieldProcura = new Tooltip("F4 - abre tela de filtro\nDEL - Limpa filtro");
    private String novoCodigo = "";
    private String referencia = "";
    private boolean showGrade = true;

    private ToggleSwitch tsSelectorGrupoEconomico = new ToggleSwitch(20, 50);
    private ToggleSwitch tsSelectorPagtoAntecipado = new ToggleSwitch(20, 50);
    private ToggleSwitch tsSelectorClienteG4G5 = new ToggleSwitch(20, 50);

    GenericDao<Mensagem> daoMensagem = new GenericDaoImpl<>(Mensagem.class);

    private List<SdStatusProduto> statusProdutos = (List<SdStatusProduto>) new FluentDao().selectFrom(SdStatusProduto.class).get().resultList();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML components">
    FormFieldComboBox<String> cboxBloqueioComercial = FormFieldComboBox.create(String.class, box -> {
        box.title("Bloq. Comercial");
        box.width(110.0);
        box.items(FXCollections.observableList(Arrays.asList("Ambos", "Bloqueado", "Liberado")));
        box.select(0);
    });
    FormFieldComboBox<String> cboxBloqueioFinanceiro = FormFieldComboBox.create(String.class, box -> {
        box.title("Bloq. Financeiro");
        box.width(110.0);
        box.items(FXCollections.observableList(Arrays.asList("Ambos", "Bloqueado", "Liberado")));
        box.select(0);
    });
    FormFieldSegmentedButton<String> fieldRevisao = FormFieldSegmentedButton.create(field -> {
        field.title("Revisão");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                loadCodigos();
            }
        });
    });
    FormTableView<VStatusProdutoCor> tblStatusProduto = FormTableView.create(VStatusProdutoCor.class, table -> {
        table.title("Status Produto");
        table.expanded();
        table.withoutCounter();
        table.tableProperties().getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(57.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VStatusProdutoCor, VStatusProdutoCor>, ObservableValue<VStatusProdutoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VStatusProdutoCor, VStatusProdutoCor>() {
                            @Override
                            protected void updateItem(VStatusProdutoCor item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                getStyleClass().removeAll("table-row-success", "table-row-primary", "table-row-info", "table-row-warning", "table-row-dark", "table-row-danger");
                                if (item != null && !empty) {
                                    setGraphic(FormButton.create(btn -> {
                                        btn.title(item.getStatusComercial().getStatus());
                                        btn.addStyle("es").addStyle(item.getStatusComercial().getStyle());
                                        btn.setStyle(btn.getStyle() + "");
                                        btn.setAction(evtAtualizar -> {
                                            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                message.message("Deseja realmente atualizar o status do produto?");
                                                message.showAndWait();
                                            }).value.get())) {
                                                new Fragment().show(fragment -> {
                                                    List<VSdStatusGradeProd> gradeCor = item.getStatusGrade().stream()
                                                            .filter(grdCor -> grdCor.getId().getProdutoCor().getId().getCor().getCor().equals(item.getId().getCor().getCor()))
                                                            .collect(Collectors.toList());

                                                    fragment.title("Alterar Status " + item.getId().getCor().toString());
                                                    fragment.size(300.0, 150.0);

                                                    FormFieldText fieldStatusAtual = FormFieldText.create(field -> {
                                                        field.title("Status Atual");
                                                        field.editable.set(false);
                                                        field.value.set(item.getStatusComercial().toString());
                                                    });
                                                    FormFieldComboBox<SdStatusProduto> fieldStatusProduto = FormFieldComboBox.create(SdTransfProdMostruario.class, field -> {
                                                        field.title("Novo Status");
                                                        field.width(290.0);
                                                        field.items.set(FXCollections.observableList(statusProdutos));
                                                        field.select(item.getStatusPcp());
                                                    });
                                                    fragment.box.getChildren().add(FormBox.create(boxStatus -> {
                                                        boxStatus.vertical();
                                                        boxStatus.add(fieldStatusAtual.build());
                                                        boxStatus.add(fieldStatusProduto.build());
                                                    }));
                                                    fragment.buttonsBox.getChildren().add(FormButton.create(btnAtualizarStatus -> {
                                                        btnAtualizarStatus.title("Atualizar Status");
                                                        btnAtualizarStatus.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                        btnAtualizarStatus.addStyle("success");
                                                        btnAtualizarStatus.setAction(evtAtualizarStatus -> {

                                                            if (gradeCor.stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0).anyMatch(cor -> cor.getStatusComercial().getCodigo() != fieldStatusProduto.value.get().getCodigo()
                                                                    && cor.getStatusComercial().getCodigo() != item.getStatusComercial().getCodigo())) {
                                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                                    message.message("Existem status nos tamanhos diferentes dos que você selecionou, deseja aplicar o novo status para todos os tamanhos?");
                                                                    message.showAndWait();
                                                                }).value.get())) {
                                                                    gradeCor.stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0).forEach(grade -> {
                                                                        grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                    });
                                                                } else {
                                                                    gradeCor.stream().filter(grade -> grade.getStatusComercial().getCodigo() == fieldStatusProduto.value.get().getCodigo()
                                                                            || grade.getStatusComercial().getCodigo() == item.getStatusComercial().getCodigo()).forEach(grade -> {
                                                                        grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                    });
                                                                }
                                                            } else {
                                                                gradeCor.stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0).forEach(grade -> {
                                                                    grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                });
                                                            }
                                                            tblStatusProduto.refresh();
                                                            fragment.close();

                                                            MessageBox.create(message -> {
                                                                message.message("Atualizado status comercial da cor " + item.getId().getCor().toString());
                                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                message.position(Pos.TOP_RIGHT);
                                                                message.notification();
                                                            });
                                                        });
                                                    }));
                                                });
                                            }
                                        });
                                    }));
                                    setStyle(getStyle() + "-fx-alignment: CENTER; -fx-padding: -4;");
                                    getStyleClass().addAll("es", "table-row-" + item.getStatusPcp().getStyle());
                                }
                            }
                        };
                    });
                    ;
                }).build() /*Cor*/
        );
    });
    @FXML
    private SplitPane splitTables;
    @FXML
    private HBox boxFiltroBloqueio;
    @FXML
    private TableView<TableProdutoGradeProduto> tblGradeProduto;
    @FXML
    private TableColumn<TableProdutoGradeProduto, Integer> clnSaldoProduto;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, Boolean> clnSelected;
    @FXML
    private MenuItem menuSelectAll;
    @FXML
    private Pane pnToggleGrupoEconomico;
    @FXML
    private Pane pnTogglePagtoAntecipado;
    @FXML
    private Pane pnToggleClienteG4G5;
    @FXML
    private Label lbToggleGrupoEconomico;
    @FXML
    private Label lbTogglePagtoAntecipado;
    @FXML
    private Label lbToggleClienteG4G5;
    @FXML
    private TextField tboxPeriodo;
    @FXML
    private TextField tboxGrupoCliente;
    @FXML
    private Button btnProcurarGrupoCliente;
    @FXML
    private Button btnProcurarPeriodo;
    @FXML
    private TextField tboxRepresentante;
    @FXML
    private Button btnProcurarRepresentante;
    @FXML
    private ListView<Produto> listReferencias;
    @FXML
    private Button btnCarregarPedidos;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnCarregarEstoque;
    @FXML
    private Button btnConfirmarRevisaoComercial;
    @FXML
    private TableView<TableManutencaoPedidosPedido> tblPedidos;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido> clnAcoes;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, String> clnFinanceiro;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, String> clnComercial;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, String> clnNpa;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, String> clnPagtoAntecipado;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, Integer> clnTotalPecas;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, BigDecimal> clnValorBruto;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, BigDecimal> clnDesconto;
    @FXML
    private TableColumn<TableManutencaoPedidosPedido, BigDecimal> clnPercCancelados;
    @FXML
    private TextField tboxProduto;
    @FXML
    private TableView<TableManutencaoPedidosCoresReferencia> tblCoresRef;
    @FXML
    private TableColumn<TableManutencaoPedidosCoresReferencia, String> clnCorCoresRef;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TitledPane tpaneConfiguracoes;
    @FXML
    private ComboBox<Mensagem> cboxMotivoCancelamento;
    @FXML
    private Label lbDescProduto;

    @FXML
    private VBox vboxStatusProduto;
    @FXML
    private HBox boxBtnStatusCodigo;
    //</editor-fold>

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pnToggleGrupoEconomico.getChildren().add(tsSelectorGrupoEconomico);
        lbToggleGrupoEconomico.textProperty().bind(Bindings.when(tsSelectorGrupoEconomico.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        tsSelectorGrupoEconomico.switchedOnProperty().addListener((obs, oldState, newState) -> {
            this.loadCodigos();
        });

        pnTogglePagtoAntecipado.getChildren().add(tsSelectorPagtoAntecipado);
        lbTogglePagtoAntecipado.textProperty().bind(Bindings.when(tsSelectorPagtoAntecipado.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        tsSelectorPagtoAntecipado.switchedOnProperty().addListener((obs, oldState, newState) -> {
            this.loadCodigos();
        });

        pnToggleClienteG4G5.getChildren().add(tsSelectorClienteG4G5);
        lbToggleClienteG4G5.textProperty().bind(Bindings.when(tsSelectorClienteG4G5.switchedOnProperty()).then("SIM").otherwise("NÃO"));

        listReferencias.setCellFactory(new Callback<ListView<Produto>, ListCell<Produto>>() {
            @Override
            public ListCell<Produto> call(ListView<Produto> param) {
                return new ProdutoCell();
            }
        });
        listReferencias.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Produto>() {
            @Override
            public void changed(ObservableValue<? extends Produto> observable, Produto oldValue, Produto newValue) {
                if (newValue != null) {
                    try {
                        lbDescProduto.setText("[" + newValue.getCodigo() + "] " + newValue.getDescricao());
                        tblCoresRef.setItems(DAOFactory.getProdutoDAO().getCoresProduto(newValue.getCodigo()));

//                        produtosEsgotados = DAOFactory.getFirebirdSQLDAO().getProdutosEsgotados(newValue.getCodigo());
//                        produtosBloqueados = DAOFactory.getFirebirdSQLDAO().getProdutosBloqueados(newValue.getCodigo());
                        makeColumnsTableCores();

                        List<VStatusProdutoCor> statusProdutoCor = (List<VStatusProdutoCor>) new FluentDao()
                                .selectFrom(VStatusProdutoCor.class)
                                .where(eb -> eb.equal("id.produto.codigo", newValue.getCodigo()))
                                .orderBy("id.cor.cor", OrderType.ASC)
                                .resultList();
                        if (tblStatusProduto.tableProperties().getColumns().size() > 1)
                            while (tblStatusProduto.tableProperties().getColumns().size() > 1) {
                                tblStatusProduto.tableProperties().getColumns().remove(1);
                            }
                        if (statusProdutoCor.size() > 0) {
                            statusProdutoCor.get(0).getStatusGrade().forEach(grade -> {
                                tblStatusProduto.addColumn(FormTableColumn.create(cln -> {
                                    cln.title(grade.getId().getTam());
                                    cln.width(40.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<VStatusProdutoCor, VStatusProdutoCor>, ObservableValue<VStatusProdutoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                    cln.format(param -> {
                                        return new TableCell<VStatusProdutoCor, VStatusProdutoCor>() {
                                            @Override
                                            protected void updateItem(VStatusProdutoCor item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                setGraphic(null);
                                                getStyleClass().removeAll("table-row-success", "table-row-warning", "table-row-dark", "table-row-danger");
                                                if (item != null && !empty) {
                                                    JPAUtils.getEntityManager().refresh(item);
                                                    item.getStatusGrade().stream()
                                                            .filter(grd -> grd.getId().getTam().equals(grade.getId().getTam()))
                                                            .findFirst().ifPresent(grd -> {
                                                                setGraphic(FormButton.create(btn -> {
                                                                    btn.title(grd.getStatusComercial().getStatus());
                                                                    btn.addStyle("es").addStyle(grd.getStatusComercial().getStyle());
                                                                    btn.setAction(evtAtualizar -> {
                                                                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                                            message.message("Deseja realmente atualizar o status do produto?");
                                                                            message.showAndWait();
                                                                        }).value.get())) {
                                                                            new Fragment().show(fragment -> {
                                                                                VSdStatusGradeProd gradeCor = item.getStatusGrade().stream()
                                                                                        .filter(grdCor -> grdCor.getId().getTam().equals(grade.getId().getTam()))
                                                                                        .findFirst().get();

                                                                                fragment.title("Alterar Status " + gradeCor.getId().getProdutoCor().getId().getCor().getCor() + "/" + gradeCor.getId().getTam());
                                                                                fragment.size(300.0, 150.0);

                                                                                FormFieldText fieldStatusAtual = FormFieldText.create(field -> {
                                                                                    field.title("Status Atual");
                                                                                    field.editable.set(false);
                                                                                    field.value.set(gradeCor.getStatusComercial().toString());
                                                                                });
                                                                                FormFieldComboBox<SdStatusProduto> fieldStatusProduto = FormFieldComboBox.create(SdTransfProdMostruario.class, field -> {
                                                                                    field.title("Novo Status");
                                                                                    field.width(290.0);
                                                                                    field.items.set(FXCollections.observableList(statusProdutos));
                                                                                    field.select(gradeCor.getStatusPcp());
                                                                                });
                                                                                fragment.box.getChildren().add(FormBox.create(boxStatus -> {
                                                                                    boxStatus.vertical();
                                                                                    boxStatus.add(fieldStatusAtual.build());
                                                                                    boxStatus.add(fieldStatusProduto.build());
                                                                                }));
                                                                                fragment.buttonsBox.getChildren().add(FormButton.create(btnAtualizarStatus -> {
                                                                                    btnAtualizarStatus.title("Atualizar Status");
                                                                                    btnAtualizarStatus.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                                                    btnAtualizarStatus.addStyle("success");
                                                                                    btnAtualizarStatus.setAction(evtAtualizarStatus -> {

//                                                                                        btn.removeStyle(gradeCor.getStatusComercial().getStyle());
                                                                                        System.out.println("update status da grade: " + gradeCor.getId().getProdutoCor().getId().getCor().getCor() + "/" + gradeCor.getId().getTam());
                                                                                        gradeCor.setStatusComercial(fieldStatusProduto.value.get());
//                                                                                        btn.addStyle(fieldStatusProduto.value.get().getStyle());
//                                                                                        btn.title(fieldStatusProduto.value.get().getStatus());

                                                                                        fragment.close();
                                                                                        tblStatusProduto.refresh();

                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Atualizado status comercial da grade " + gradeCor.getId().getProdutoCor().getId().getCor().getCor() + "/" + gradeCor.getId().getTam());
                                                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                                            message.position(Pos.TOP_RIGHT);
                                                                                            message.notification();
                                                                                        });
                                                                                    });
                                                                                }));
                                                                            });
                                                                        }
                                                                    });
                                                                }));
                                                                setStyle(getStyle() + "-fx-alignment: CENTER; -fx-padding: -4;");
                                                                getStyleClass().addAll("", "es", "table-row-" + grd.getStatusPcp().getStyle());
                                                            });
                                                }
                                            }
                                        };
                                    });
                                }).build() /**/);
                            });
                            statusProdutoCor.forEach(cor -> {
                                cor.getStatusGrade().forEach(grade -> {
                                    grade.statusComercialProperty().addListener((observable1, oldValue1, newValue1) -> {
                                        System.out.println("update status grade: " + grade.getId().getProdutoCor().getId().getCor() + "/" + grade.getId().getTam());
                                        try {
                                            new NativeDAO().runNativeQueryUpdate("update sd_status_grade_prod_001 set status_comercial = '%s', dt_status_comercial = sysdate where codigo = '%s' and cor = '%s' and tam = '%s'",
                                                    newValue1.getCodigo(),
                                                    grade.getId().getProdutoCor().getId().getProduto().getCodigo(),
                                                    grade.getId().getProdutoCor().getId().getCor().getCor(),
                                                    grade.getId().getTam());
                                            JPAUtils.getEntityManager().refresh(grade);
                                            SysLogger.addSysDelizLog("Manutenção de Pedido", TipoAcao.EDITAR, grade.getId().getProdutoCor().getId().getProduto().getCodigo(),
                                                    "Alterado status comercial do produto " + grade.getId().getProdutoCor().getId().getProduto().getCodigo() + "/" + grade.getId().getProdutoCor().getId().getCor().getCor() + "/" + grade.getId().getTam() +
                                                            " de " + oldValue1.toString() + " para " + newValue1.toString());
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                });
                            });
                        }
                        tblStatusProduto.setItems(FXCollections.observableList(statusProdutoCor));

                        List<SdStatusProduto> statusPcp = tblStatusProduto.items.stream()
                                .map(grade -> grade.getStatusPcp())
                                .distinct()
                                .collect(Collectors.toList());
                        List<SdStatusProduto> statusComercial = tblStatusProduto.items.stream()
                                .map(grade -> grade.getStatusComercial())
                                .distinct()
                                .collect(Collectors.toList());
                        SdStatusProduto _STATUS_PRODUTO_LIMITADO = new FluentDao().selectFrom(SdStatusProduto.class).where(eb -> eb.equal("codigo", "2")).singleResult();
                        AtomicReference<SdStatusProduto> stsComercial = new AtomicReference<>(statusComercial.stream().filter(grade -> grade.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO :
                                statusComercial.stream().allMatch(item -> item.getOrdem() == 0) ? statusComercial.get(0) : statusComercial.stream().filter(status -> status.getOrdem() != 0).findFirst().get());
                        SdStatusProduto stsPcp = statusPcp.stream().filter(grade -> grade.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO :
                                statusPcp.stream().allMatch(item -> item.getOrdem() == 0) ? statusPcp.get(0) : statusPcp.stream().filter(status -> status.getOrdem() != 0).findFirst().get();

                        boxBtnStatusCodigo.getStyleClass().removeAll("danger", "info", "primary", "dark", "warning", "success");
                        boxBtnStatusCodigo.getStyleClass().add(stsPcp.getStyle());
                        boxBtnStatusCodigo.getChildren().clear();
                        boxBtnStatusCodigo.getChildren().add(FormButton.create(btn -> {
                            btn.title(stsComercial.get().getStatus());
                            btn.addStyle("xs").addStyle(stsComercial.get().getStyle());
                            btn.setAction(evt -> {
                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Deseja realmente atualizar o status do produto?");
                                    message.showAndWait();
                                }).value.get())) {
                                    new Fragment().show(fragment -> {
                                        fragment.title("Alterar Status do Produto");
                                        fragment.size(300.0, 150.0);

                                        FormFieldText fieldStatusAtual = FormFieldText.create(field -> {
                                            field.title("Status Atual");
                                            field.editable.set(false);
                                            field.value.set(stsComercial.toString());
                                        });
                                        FormFieldComboBox<SdStatusProduto> fieldStatusProduto = FormFieldComboBox.create(SdTransfProdMostruario.class, field -> {
                                            field.title("Novo Status");
                                            field.width(290.0);
                                            field.items.set(FXCollections.observableList(statusProdutos));
                                            field.select(stsPcp);
                                        });
                                        fragment.box.getChildren().add(FormBox.create(boxStatus -> {
                                            boxStatus.vertical();
                                            boxStatus.add(fieldStatusAtual.build());
                                            boxStatus.add(fieldStatusProduto.build());
                                        }));
                                        fragment.buttonsBox.getChildren().add(FormButton.create(btnAtualizarStatus -> {
                                            btnAtualizarStatus.title("Atualizar Status");
                                            btnAtualizarStatus.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                            btnAtualizarStatus.addStyle("success");
                                            btnAtualizarStatus.setAction(evtAtualizar -> {
                                                btn.removeStyle(stsComercial.get().getStyle());

                                                if (statusProdutoCor.stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0).anyMatch(cor -> cor.getStatusComercial().getCodigo() != fieldStatusProduto.value.get().getCodigo()
                                                        && cor.getStatusComercial().getCodigo() != stsComercial.get().getCodigo())) {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Existem status nos tamanhos diferentes dos que você selecionou, deseja aplicar o novo status para todos os tamanhos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        statusProdutoCor.forEach(cor -> {
                                                            cor.getStatusGrade().stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0)
                                                                    .forEach(grade -> {
                                                                        grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                    });
                                                        });
                                                    } else {
                                                        statusProdutoCor.forEach(cor -> {
                                                            cor.getStatusGrade().stream().filter(grade -> grade.getStatusComercial().getCodigo() == fieldStatusProduto.value.get().getCodigo()
                                                                            || cor.getStatusComercial().getCodigo() != stsComercial.get().getCodigo())
                                                                    .forEach(grade -> {
                                                                        grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                    });
                                                        });
                                                    }
                                                } else {
                                                    statusProdutoCor.forEach(cor -> {
                                                        cor.getStatusGrade().stream().filter(grade -> grade.getStatusComercial().getOrdem() != 0)
                                                                .forEach(grade -> {
                                                                    grade.setStatusComercial(fieldStatusProduto.value.get());
                                                                });
                                                    });
                                                }

                                                btn.addStyle(fieldStatusProduto.value.get().getStyle());
                                                btn.title(fieldStatusProduto.value.get().getStatus());
                                                stsComercial.set(fieldStatusProduto.value.get());

                                                tblStatusProduto.refresh();
                                                fragment.close();
                                                MessageBox.create(message -> {
                                                    message.message("Atualizado status comercial do produto.");
                                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                    message.position(Pos.TOP_RIGHT);
                                                    message.notification();
                                                });
                                            });
                                        }));
                                    });
                                }
                            });
                        }));

                        ScrollBar verticalBarCores = (ScrollBar) tblCoresRef.lookup(".scroll-bar:vertical");
                        ScrollBar verticalBarStatus = (ScrollBar) tblStatusProduto.tableProperties().lookup(".scroll-bar:vertical");
                        if (verticalBarCores != null)
                            verticalBarCores.valueProperty().bindBidirectional(verticalBarStatus.valueProperty());

                    } catch (SQLException ex) {
                        Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                }
            }
        });
        tblCoresRef.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblCoresRef.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tblStatusProduto.tableProperties().getSelectionModel().clearSelection();
                tblStatusProduto.tableProperties().getSelectionModel().select(tblCoresRef.getSelectionModel().getSelectedIndex());
            }
        });
        try {
            Colecao colecaoAtual = DAOFactory.getColecaoDAO().getColecaoAtual();
            if (colecaoAtual != null) {
                List<Produto> produtos = DAOFactory.getProdutoDAO().getProdutosColecao(colecaoAtual.getStrCodigo());
                listReferencias.setItems(FXCollections.observableArrayList(produtos));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tboxGrupoCliente.setTooltip(tipTFieldProcura);
        tboxPeriodo.setTooltip(tipTFieldProcura);
        tboxProduto.setTooltip(tipTFieldProcura);
        tboxRepresentante.setTooltip(tipTFieldProcura);

        tboxPeriodo.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!tboxPeriodo.getText().isEmpty()) {
                tboxPeriodo.setText(tboxPeriodo.getText().replace("'", ""));
                if (!newValue) {
                    tboxPeriodo.setText("'" + tboxPeriodo.getText().replace(",", "','") + "'");
                    loadCodigos();
                }
            }
        });
        tboxGrupoCliente.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!tboxGrupoCliente.getText().isEmpty()) {
                tboxGrupoCliente.setText(tboxGrupoCliente.getText().replace("'", ""));
                if (!newValue) {
                    tboxGrupoCliente.setText("'" + tboxGrupoCliente.getText().replace(",", "','") + "'");
                    loadCodigos();
                }
            }
        });
        tboxProduto.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!tboxProduto.getText().isEmpty()) {
                tboxProduto.setText(tboxProduto.getText().replace("'", ""));
                if (!newValue) {
                    tboxProduto.setText("'" + tboxProduto.getText().replace(",", "','") + "'");
                    loadCodigos();
                }
            }
        });
        tboxRepresentante.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!tboxRepresentante.getText().isEmpty()) {
                tboxRepresentante.setText(tboxRepresentante.getText().replace("'", ""));
                if (!newValue) {
                    tboxRepresentante.setText("'" + tboxRepresentante.getText().replace(",", "','") + "'");
                    loadCodigos();
                }
            }
        });

        tpaneConfiguracoes.disableProperty().bind(pedidosRef.emptyProperty());
        cboxMotivoCancelamento.setItems(FXCollections.observableList((List<Mensagem>) new FluentDao().selectFrom(Mensagem.class).get().orderBy("descricao", OrderType.ASC).resultList()));
        cboxMotivoCancelamento.getSelectionModel().select(getMensagemPadrao());

        boxFiltroBloqueio.getChildren().add(cboxBloqueioComercial.build());
        boxFiltroBloqueio.getChildren().add(cboxBloqueioFinanceiro.build());
        boxFiltroBloqueio.getChildren().add(fieldRevisao.build());

        vboxStatusProduto.getChildren().add(tblStatusProduto.build());

        btnConfirmarRevisaoComercial.disableProperty().bind(tblStatusProduto.items.emptyProperty());

        TextFieldUtils.upperCase(tboxPeriodo);
        TextFieldUtils.upperCase(tboxGrupoCliente);
        TextFieldUtils.upperCase(tboxProduto);
        TextFieldUtils.upperCase(tboxRepresentante);
    }

    //<editor-fold defaultstate="collapsed" desc="Rules methods">
    private List<String> breakInValues(String[] inValues) {
        List<String> newInValues = new ArrayList<>();

        int divInValues = (inValues.length / 1000) + 1;
        int valueInitial = inValues.length;
        int valueFinal = 0;

        for (int i = 1; i <= divInValues; i++) {
            valueFinal = valueInitial - 1000;
            int j = 0;
            String inValue = "";
            for (j = valueInitial; (j > valueFinal && j > 0); j--) {
                inValue += inValues[j - 1] + ",";
            }
            newInValues.add(inValue.substring(0, inValue.length() - 1));
            valueInitial = j;
        }

        return newInValues;
    }

    private Mensagem getMensagemPadrao() {
        return cboxMotivoCancelamento.getItems().stream()
                .filter(mensagem -> mensagem.getCodmen().equals(mensagemPadrao))
                .collect(Collectors.maxBy((o1, o2) -> o1.getCodmen().compareTo(o2.getCodmen())))
                .get();
    }

    private String createWhereFilter() {
        String whereFilter = "";
        if (!tboxGrupoCliente.getText().isEmpty()) {
            String[] inValues = tboxGrupoCliente.getText().split(",");
            if (inValues.length < 1000) {
                whereFilter += "and cli.codcli in (" + tboxGrupoCliente.getText() + ")\n";
            } else {
                whereFilter += "and (1 <> 1\n";
                for (String inValue : breakInValues(inValues)) {
                    whereFilter += " or cli.codcli in (" + inValue + ")\n";
                }
                whereFilter += ")\n";
            }
        }
        if (!tboxPeriodo.getText().isEmpty()) {
            String[] inValues = tboxPeriodo.getText().split(",");
            if (inValues.length < 1000) {
                whereFilter += "and ped.periodo in (" + tboxPeriodo.getText() + ")\n";
            } else {
                whereFilter += "and (1 <> 1\n";
                for (String inValue : breakInValues(inValues)) {
                    whereFilter += " or ped.periodo in (" + inValue + ")\n";
                }
                whereFilter += ")\n";
            }
        }
        if (!tboxProduto.getText().isEmpty()) {
            String[] inValues = tboxProduto.getText().split(",");
            if (inValues.length < 1000) {
                whereFilter += "and prd.codigo in (" + tboxProduto.getText() + ")\n";
            } else {
                whereFilter += "and (1 <> 1\n";
                for (String inValue : breakInValues(inValues)) {
                    whereFilter += " or prd.codigo in (" + inValue + ")\n";
                }
                whereFilter += ")\n";
            }
        }
        if (!tboxRepresentante.getText().isEmpty()) {
            String[] inValues = tboxRepresentante.getText().split(",");
            if (inValues.length < 1000) {
                whereFilter += "and rep.codrep in (" + tboxRepresentante.getText() + ")\n";
            } else {
                whereFilter += "and (1 <> 1\n";
                for (String inValue : breakInValues(inValues)) {
                    whereFilter += " or rep.codrep in (" + inValue + ")\n";
                }
                whereFilter += ")\n";
            }
        }
        if (tsSelectorGrupoEconomico.switchedOnProperty().get()) {
            whereFilter += "and cli.grupo is null\n";
        }
        if (tsSelectorPagtoAntecipado.switchedOnProperty().get()) {
            whereFilter += "and trim(ped.pgto) <> '1'\n";
        }
        if (tsSelectorClienteG4G5.switchedOnProperty().get()) {
            whereFilter += "and cli.sit_cli not in ('G4','G5')\n";
        }
        if (cboxBloqueioComercial.value.get().equals("Bloqueado")) {
            whereFilter += "and ped.bloqueio = '0'\n";
        }
        if (cboxBloqueioComercial.value.get().startsWith("Liberado")) {
            whereFilter += "and ped.bloqueio = '1'\n";
        }
        if (cboxBloqueioFinanceiro.value.get().equals("Bloqueado")) {
            whereFilter += "and ped.financeiro = '0'\n";
        }
        if (cboxBloqueioFinanceiro.value.get().startsWith("Liberado")) {
            whereFilter += "and ped.financeiro = '1'\n";
        }
        if (!fieldRevisao.value.get().equals("A")) {
            whereFilter += "and sdprd.revisao_comercial = '" + fieldRevisao.value.get() + "'\n";
        }

        return whereFilter;
    }

    private void loadCodigos() {
        try {
            tblCoresRef.getItems().clear();
            tblPedidos.getItems().clear();
            tblGradeProduto.getItems().clear();
            splitTables.setDividerPositions(.8);
            listReferencias.getItems().clear();
            tblStatusProduto.items.clear();
            if (tblStatusProduto.tableProperties().getColumns().size() > 1)
                while (tblStatusProduto.tableProperties().getColumns().size() > 1) {
                    tblStatusProduto.tableProperties().getColumns().remove(1);
                }
            List<Produto> produtos = DAOFactory.getProdutoDAO().getProdutosPedidos(this.createWhereFilter());
            if (fieldRevisao.value.get().equals("S")) {
                produtos.addAll(DAOFactory.getProdutoDAO().getProdutosWithoutPedidos(tboxProduto.getText()));
                produtos = produtos.stream().distinct().collect(Collectors.toList());
            }
            listReferencias.setItems(FXCollections.observableArrayList(produtos));
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private TableManutencaoPedidosCoresReferencia getCorTable(String cor) {
        for (TableManutencaoPedidosCoresReferencia cores : tblCoresRef.getItems()) {
            if (cores.getCor().equals(cor)) {
                return cores;
            }
        }

        return null;
    }

    private void makeColumnsTable() {

        if (tblPedidos.getColumns().size() > 17) {
            int totalColumns = tblPedidos.getColumns().size();
            for (int i = 0; i < totalColumns - 17; i++) {
                tblPedidos.getColumns().remove(10);
            }
            columnPosition = 10;
        }

        if (!pedidosRef.isEmpty()) {
            tblPedidos.setEditable(true);

            TableManutencaoPedidosPedido pedido = pedidosRef.get(0);
            pedido.getHeadersTamanhos().forEach((keyColumn, valueColumn) -> {
                TableColumn<TableManutencaoPedidosPedido, Integer> columnTamanho = new TableColumn<>(valueColumn);
                columnTamanho.prefWidthProperty().set(40.0);
                columnTamanho.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TableManutencaoPedidosPedido, Integer>, ObservableValue<Integer>>() {
                    @Override
                    public ObservableValue<Integer> call(TableColumn.CellDataFeatures<TableManutencaoPedidosPedido, Integer> features) {
                        return new ReadOnlyObjectWrapper(((TableManutencaoPedidosPedido) features.getValue()).getTamanhos().get(valueColumn));
                    }
                });
                Callback<TableColumn<TableManutencaoPedidosPedido, Integer>, TableCell<TableManutencaoPedidosPedido, Integer>> cellFactoryQtde
                        = new Callback<TableColumn<TableManutencaoPedidosPedido, Integer>, TableCell<TableManutencaoPedidosPedido, Integer>>() {
                    public TableCell call(TableColumn p) {
                        return new SceneComercialGestaoPedidosManutencaoPedidoController.EditingCellClnTamanho();
                    }
                };
                columnTamanho.setCellFactory(cellFactoryQtde);
                columnTamanho.setOnEditCommit(action -> {
                    alterarGradePedido(action, valueColumn);
                });

                tblPedidos.getColumns().add(columnPosition++, columnTamanho);
            });

            clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
            clnSelected.setCellValueFactory((cellData) -> {
                TableManutencaoPedidosPedido cellValue = cellData.getValue();
                BooleanProperty property = cellValue.selectedProperty();
                property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
                return property;
            });

            clnAcoes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido>, ObservableValue<TableManutencaoPedidosPedido>>() {
                @Override
                public ObservableValue<TableManutencaoPedidosPedido> call(TableColumn.CellDataFeatures<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido> features) {
                    return new ReadOnlyObjectWrapper(features.getValue());
                }
            });
            clnAcoes.setComparator(new Comparator<TableManutencaoPedidosPedido>() {
                @Override
                public int compare(TableManutencaoPedidosPedido p1, TableManutencaoPedidosPedido p2) {
                    return p1.getNumero().compareTo(p2.getNumero());
                }
            });
            clnAcoes.setCellFactory(new Callback<TableColumn<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido>, TableCell<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido>>() {
                @Override
                public TableCell<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido> call(TableColumn<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido> btnCol) {
                    return new TableCell<TableManutencaoPedidosPedido, TableManutencaoPedidosPedido>() {
                        final ImageView buttonGraphicExcluir = new ImageView();
                        final ImageView buttonGraphicReservar = new ImageView();
                        final ImageView buttonGraphicTrocar = new ImageView();
                        final Button buttonExcluir = new Button();
                        final Button buttonReservar = new Button();
                        final Button buttonTrocar = new Button();
                        final HBox boxButtons = new HBox(3);

                        final Tooltip tipBtnExcluir = new Tooltip("Cancelar referência do pedido");
                        final Tooltip tipBtnReservar = new Tooltip("Reservar referência para expedição");
                        final Tooltip tipBtnTrocar = new Tooltip("Trocar referência do pedido");

                        {
                            buttonExcluir.setGraphic(buttonGraphicExcluir);
                            buttonExcluir.setTooltip(tipBtnExcluir);
                            buttonReservar.setGraphic(buttonGraphicReservar);
                            buttonReservar.setTooltip(tipBtnReservar);
                            buttonTrocar.setGraphic(buttonGraphicTrocar);
                            buttonTrocar.setTooltip(tipBtnTrocar);
                        }

                        @Override
                        public void updateItem(TableManutencaoPedidosPedido pedido, boolean empty) {
                            super.updateItem(pedido, empty);
                            if (pedido != null) {
                                buttonReservar.setText("Reservar Pedido");
                                buttonGraphicReservar.setImage(imgBtnReservar);
                                buttonReservar.getStyleClass().add("info");
                                buttonReservar.getStyleClass().add("xs");
                                buttonReservar.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                                buttonTrocar.setText("Trocar Produto");
                                buttonGraphicTrocar.setImage(imgBtnTrocar);
                                buttonTrocar.getStyleClass().add("warning");
                                buttonTrocar.getStyleClass().add("xs");
                                buttonTrocar.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                                buttonExcluir.setText("Excluir Produto");
                                buttonGraphicExcluir.setImage(imgBtnCancelar);
                                buttonExcluir.getStyleClass().add("danger");
                                buttonExcluir.getStyleClass().add("xs");
                                buttonExcluir.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                                boxButtons.getChildren().clear();
                                boxButtons.getChildren().addAll(buttonExcluir, buttonReservar, buttonTrocar);
                                setGraphic(boxButtons);
                                buttonReservar.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        for (Map.Entry<Integer, String> entry : pedido.getHeadersTamanhos().entrySet()) {
                                            try {
                                                if (DAOFactory.getProdutoDAO().getEstoqueSku(pedido.getCodigo(), pedido.getCor(), entry.getValue(), "0005") < pedido.getTamanhos().get(entry.getValue())) {
                                                    if (DAOFactory.getProdutoDAO().getEstoqueSku(pedido.getCodigo(), pedido.getCor(), entry.getValue(), "0001") <= 0) {
                                                        String gradeEstoque = DAOFactory.getProdutoDAO().getEstoqueSkuRelatorio(pedido.getCodigo(), pedido.getCor(), entry.getValue(), "0005");
                                                        GUIUtils.showMessageWithGrade("Referência sem estoque.\n\nGrade de Tamanhos em Estoque:\n",
                                                                gradeEstoque);
                                                        return;
                                                    } else {
                                                        String gradeEstoque = DAOFactory.getProdutoDAO().getEstoqueSkuRelatorio(pedido.getCodigo(), pedido.getCor(), entry.getValue(), "0005");
                                                        GUIUtils.showMessageWithGrade("Referência sem estoque de expedição.\n"
                                                                        + "É necessário a transferência do estoque de recebimento para expedição.\n\n",
                                                                gradeEstoque);
                                                        return;
                                                    }
                                                }
                                            } catch (SQLException ex) {
                                                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                GUIUtils.showException(ex);
                                                return;
                                            }
                                        }
                                        pedido.getTamanhos().forEach((key, value) -> {
                                            if (value > 0) {
                                                try {
                                                    DAOFactory.getPedidoDAO().reservarRefPedido(pedido.getNumero(), pedido.getCodigo(), pedido.getCor(), key, value);

                                                } catch (SQLException ex) {
                                                    Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                    GUIUtils.showException(ex);
                                                    return;
                                                }
                                            }
                                        });
                                        Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                                                "Reservar",
                                                "SD Manutenção de Pedido",
                                                pedido.getNumero(),
                                                "Reservado referência " + pedido.getCodigo() + " na cor " + pedido.getCor() + " do pedido " + pedido.getNumero());
                                        try {
                                            DAOFactory.getLogDAO().saveLog(logAction);
                                            DAOFactory.getLogDAO().saveLogTI(logAction);
                                        } catch (SQLException ex) {
                                            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                            GUIUtils.showException(ex);
                                        }

                                        pedidosRef.remove(pedido);
                                        tblPedidos.getItems().remove(pedido);
                                        tblPedidos.refresh();
                                        buttonReservar.setDisable(true);
                                        buttonTrocar.setDisable(true);
                                        buttonExcluir.setDisable(true);
                                        GUIUtils.showMessageNotification("Manutenção de Pedidos", "Referência reservada com sucesso.");
                                    }
                                });
                                buttonTrocar.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (GUIUtils.showQuestionMessageDefaultNao("Tem certeza que deseja trocar a referência?")) {
                                            try {
                                                if (tblPedidos.getItems().stream().filter(pedido -> pedido.isSelected()).count() <= 0) {
                                                    String reservaRef = DAOFactory.getPedidoDAO().getReservaReferencia(pedido.getNumero(), pedido.getCodigo(), pedido.getCor());
                                                    if (reservaRef != "X") {
                                                        String messageError = "Esta referência contém reservas ou faturamento no pedido.\n\nReserva/Nota Fiscal nº: "
                                                                + (reservaRef == null ? "Aguardando separação" : reservaRef);
                                                        GUIUtils.showMessage(messageError,
                                                                Alert.AlertType.WARNING);
                                                        return;
                                                    }
                                                    novoCodigo = GUIUtils.showTextFieldDialog("Digite o código da referência que irá substituir o código: " + pedido.getCodigo());

                                                    Produto produtoSubstituto = DAOFactory.getProdutoDAO().getByCodigo(novoCodigo);
                                                    if (produtoSubstituto == null) {
                                                        GUIUtils.showMessage("O código digitado não existe.",
                                                                Alert.AlertType.ERROR);
                                                        return;
                                                    }
                                                    DAOFactory.getPedidoDAO().trocarRefPedido(pedido.getNumero(), pedido.getCodigo(), pedido.getCor(), novoCodigo, pedido.getTamanhos());
                                                    TableManutencaoPedidosCoresReferencia cores = getCorTable(pedido.getCor());
                                                    pedido.getTamanhos().forEach((t, u) -> {
                                                        cores.getTamanhos().replace(t, cores.getTamanhos().get(t) + u);
                                                    });
                                                    tblCoresRef.refresh();
                                                    Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                                                            "Alterar",
                                                            "SD Manutenção de Pedido",
                                                            pedido.getNumero(),
                                                            "Alterado referência " + pedido.getCodigo() + " para " + novoCodigo + " na cor " + pedido.getCor() + " do pedido " + pedido.getNumero());
                                                    try {
                                                        DAOFactory.getLogDAO().saveLog(logAction);
                                                        DAOFactory.getLogDAO().saveLogTI(logAction);
                                                    } catch (SQLException ex) {
                                                        Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                        GUIUtils.showException(ex);
                                                    }
                                                    pedido.setCodigo(novoCodigo);
                                                    pedidosRef.remove(pedido);
                                                    tblPedidos.getItems().remove(pedido);
                                                } else {

                                                    novoCodigo = GUIUtils.showTextFieldDialog("Digite o código da referência que irá substituir o código: " + pedido.getCodigo());
                                                    List<TableManutencaoPedidosPedido> pedidosParaAlteracao = new ArrayList<>();
                                                    tblPedidos.getItems().stream().filter(pedido -> pedido.isSelected()).forEach(pedidoSelected -> {
                                                        pedidosParaAlteracao.add(pedidoSelected);
                                                    });
                                                    pedidosParaAlteracao.forEach(pedidoAll -> {
                                                        try {
                                                            String reservaRef = DAOFactory.getPedidoDAO().getReservaReferencia(pedidoAll.getNumero(), pedidoAll.getCodigo(), pedidoAll.getCor());
                                                            if (reservaRef != "X") {
                                                                String messageError = "Esta referência contém reservas ou faturamentos no pedido.\n\nReserva/Nota Fiscal nº: "
                                                                        + (reservaRef == null ? "Aguardando separação" : reservaRef);
                                                                GUIUtils.showMessage(messageError,
                                                                        Alert.AlertType.WARNING);
                                                                return;
                                                            }
                                                            DAOFactory.getPedidoDAO().trocarRefPedido(pedidoAll.getNumero(), pedidoAll.getCodigo(), pedidoAll.getCor(), novoCodigo, pedidoAll.getTamanhos());
                                                            TableManutencaoPedidosCoresReferencia cores = getCorTable(pedidoAll.getCor());
                                                            pedidoAll.getTamanhos().forEach((t, u) -> {
                                                                cores.getTamanhos().replace(t, cores.getTamanhos().get(t) + u);
                                                            });
                                                            tblCoresRef.refresh();
                                                            Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                                                                    "Alterar",
                                                                    "SD Manutenção de Pedido",
                                                                    pedidoAll.getNumero(),
                                                                    "Alterado referência " + pedidoAll.getCodigo() + " para " + novoCodigo + " na cor " + pedidoAll.getCor() + " do pedido " + pedidoAll.getNumero());
                                                            try {
                                                                DAOFactory.getLogDAO().saveLog(logAction);
                                                                DAOFactory.getLogDAO().saveLogTI(logAction);
                                                            } catch (SQLException ex) {
                                                                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                                GUIUtils.showException(ex);
                                                            }
                                                            pedidoAll.setCodigo(novoCodigo);
                                                            pedidosRef.remove(pedidoAll);
                                                            tblPedidos.getItems().remove(pedidoAll);
                                                        } catch (SQLException ex) {
                                                            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                            GUIUtils.showException(ex);
                                                            return;
                                                        }
                                                    });
                                                }
                                                tblPedidos.refresh();
                                                GUIUtils.showMessageNotification("Manutenção de Pedidos", "Troca de referências realizada com sucesso.");
                                            } catch (SQLException ex) {
                                                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                GUIUtils.showException(ex);
                                                return;
                                            }
                                        }
                                    }
                                });
                                buttonExcluir.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente cancelar a referência dos pedidos selecionados?")) {
                                            String numero = pedido.getNumero();
                                            String codigo = pedido.getCodigo();
                                            String cor = pedido.getCor();
                                            try {
                                                if (tblPedidos.getItems().stream().filter(pedido -> pedido.isSelected()).count() <= 0) {
                                                    DAOFactory.getPedidoDAO().cancelRefPedido(numero, codigo, cor);
                                                    TableManutencaoPedidosCoresReferencia cores = getCorTable(cor);
                                                    pedido.getTamanhos().forEach((t, u) -> {
                                                        cores.getTamanhos().replace(t, cores.getTamanhos().get(t) + u);
                                                    });
                                                    tblCoresRef.refresh();

                                                    Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                                                            "Alterar",
                                                            "SD Manutenção de Pedido",
                                                            pedido.getNumero(),
                                                            "Cancelada referência " + pedido.getCodigo() + " na cor " + pedido.getCor() + " do pedido " + pedido.getNumero());
                                                    try {
                                                        DAOFactory.getLogDAO().saveLog(logAction);
                                                        DAOFactory.getLogDAO().saveLogTI(logAction);
                                                    } catch (SQLException ex) {
                                                        Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                        GUIUtils.showException(ex);
                                                    }
                                                    pedidosRef.remove(pedido);
                                                    tblPedidos.getItems().remove(pedido);
                                                } else {
                                                    List<TableManutencaoPedidosPedido> pedidosParaAlteracao = new ArrayList<>();
                                                    tblPedidos.getItems().stream().filter(pedido -> pedido.isSelected()).forEach(pedidoSelected -> {
                                                        pedidosParaAlteracao.add(pedidoSelected);
                                                    });
                                                    pedidosParaAlteracao.forEach(pedidoAll -> {
                                                        try {
                                                            DAOFactory.getPedidoDAO().cancelRefPedido(pedidoAll.getNumero(), pedidoAll.getCodigo(), pedidoAll.getCor());
                                                            TableManutencaoPedidosCoresReferencia cores = getCorTable(cor);
                                                            pedido.getTamanhos().forEach((t, u) -> {
                                                                cores.getTamanhos().replace(t, cores.getTamanhos().get(t) + u);
                                                            });
                                                            tblCoresRef.refresh();

                                                            Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                                                                    "Alterar",
                                                                    "SD Manutenção de Pedido",
                                                                    pedidoAll.getNumero(),
                                                                    "Cancelada referência " + pedidoAll.getCodigo() + " na cor " + pedidoAll.getCor() + " do pedido " + pedidoAll.getNumero());
                                                            try {
                                                                DAOFactory.getLogDAO().saveLog(logAction);
                                                                DAOFactory.getLogDAO().saveLogTI(logAction);
                                                            } catch (SQLException ex) {
                                                                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                                GUIUtils.showException(ex);
                                                            }
                                                            pedidosRef.remove(pedido);
                                                            tblPedidos.getItems().remove(pedido);

                                                        } catch (SQLException ex) {
                                                            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                            GUIUtils.showException(ex);
                                                            return;
                                                        }
                                                    });
                                                }
                                                tblPedidos.refresh();

                                                GUIUtils.showMessageNotification("Manutenção de Pedidos", "Referência cancelada no pedido.");
                                            } catch (SQLException ex) {
                                                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                                                GUIUtils.showException(ex);
                                                return;
                                            }
                                        }
                                    }
                                });
                            } else {
                                setText(null);
                                setGraphic(null);
                            }
                        }
                    };
                }

            });

            clnTotalPecas.setCellFactory((TableColumn<TableManutencaoPedidosPedido, Integer> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, Integer>() {

                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : item + "");
                        setAlignment(Pos.CENTER);
                        setStyle("-fx-text-fill: blue; -fx-font-weight: bold; -fx-text-alignment: center;");
                        setGraphic(null);
                    }

                };
                return cell;
            });

            clnFinanceiro.setCellFactory((TableColumn<TableManutencaoPedidosPedido, String> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        TableManutencaoPedidosPedido currentPedido = currentRow == null ? null : (TableManutencaoPedidosPedido) currentRow.getItem();
                        if (currentPedido != null) {
                            String status = currentPedido.getFinanceiro();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused() && status != null) {
                                setPriorityStyle(status);
                            }
                        }
                    }

                    @Override
                    public void updateSelected(boolean upd) {
                        super.updateSelected(upd);
                    }

                    private void clearPriorityStyle() {
                        ObservableList<String> styleClasses = getStyleClass();
                        styleClasses.remove("danger");
                        styleClasses.remove("success");

                    }

                    private void setPriorityStyle(String priority) {
                        switch (priority) {
                            case "BLOQUEADO":
                                getStyleClass().add("danger");
                                break;
                            case "LIBERADO":
                                getStyleClass().add("success");
                                break;
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem();
                    }
                };
                return cell;
            });

            clnComercial.setCellFactory((TableColumn<TableManutencaoPedidosPedido, String> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        TableManutencaoPedidosPedido currentPedido = currentRow == null ? null : (TableManutencaoPedidosPedido) currentRow.getItem();
                        if (currentPedido != null) {
                            String status = currentPedido.getComercial();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused() && status != null) {
                                setPriorityStyle(status);
                            }
                        }
                    }

                    @Override
                    public void updateSelected(boolean upd) {
                        super.updateSelected(upd);
                    }

                    private void clearPriorityStyle() {
                        ObservableList<String> styleClasses = getStyleClass();
                        styleClasses.remove("danger");
                        styleClasses.remove("success");

                    }

                    private void setPriorityStyle(String priority) {
                        switch (priority) {
                            case "BLOQUEADO":
                                getStyleClass().add("danger");
                                break;
                            case "LIBERADO":
                                getStyleClass().add("success");
                                break;
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem();
                    }
                };
                return cell;
            });

            clnNpa.setCellFactory((TableColumn<TableManutencaoPedidosPedido, String> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        TableManutencaoPedidosPedido currentPedido = currentRow == null ? null : (TableManutencaoPedidosPedido) currentRow.getItem();
                        if (currentPedido != null) {
                            String status = currentPedido.getNpa();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused() && status != null) {
                                setPriorityStyle(status);
                            }
                        }
                    }

                    @Override
                    public void updateSelected(boolean upd) {
                        super.updateSelected(upd);
                    }

                    private void clearPriorityStyle() {
                        ObservableList<String> styleClasses = getStyleClass();
                        styleClasses.remove("statusNpa");

                    }

                    private void setPriorityStyle(String priority) {
                        switch (priority) {
                            case "NÃO":
                                getStyleClass().add("statusNpa");
                                break;
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem();
                    }
                };
                return cell;
            });

            tblPedidos.setRowFactory(tv -> new TableRow<TableManutencaoPedidosPedido>() {
                @Override
                protected void updateItem(TableManutencaoPedidosPedido item, boolean empty) {
                    super.updateItem(item, empty);
                    clearPriorityStyle();

                    if (item != null) {
                        String status = item.getPagtoAntecipado();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusNpa");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "SIM":
                            getStyleClass().add("statusNpa");
                            break;
                    }
                }

            });

            clnPagtoAntecipado.setCellFactory((TableColumn<TableManutencaoPedidosPedido, String> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                        TableRow currentRow = getTableRow();
                        TableManutencaoPedidosPedido currentPedido = currentRow == null ? null : (TableManutencaoPedidosPedido) currentRow.getItem();
                        if (currentPedido != null) {
                            String status = currentPedido.getPagtoAntecipado();
                            clearPriorityStyle();
                            if (!isHover() && !isSelected() && !isFocused() && status != null) {
                                setPriorityStyle(status);
                            }
                        }
                    }

                    @Override
                    public void updateSelected(boolean upd) {
                        super.updateSelected(upd);
                    }

                    private void clearPriorityStyle() {
                        ObservableList<String> styleClasses = getStyleClass();
                        styleClasses.remove("statusNpa");

                    }

                    private void setPriorityStyle(String priority) {
                        switch (priority) {
                            case "SIM":
                                getStyleClass().add("statusNpa");
                                break;
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem();
                    }
                };
                return cell;
            });

            clnValorBruto.setCellFactory((TableColumn<TableManutencaoPedidosPedido, BigDecimal> param) -> {
                TableCell cell = new TableCell<TableManutencaoPedidosPedido, BigDecimal>() {

                    @Override
                    public void updateItem(BigDecimal item, boolean empty) {
                        super.updateItem(item, empty);
                        NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                        formatValueCell.setMaximumFractionDigits(2);
                        setText(empty ? null : item == null ? null : formatValueCell.format(item.doubleValue()));
                        setGraphic(null);
                    }

                    @Override
                    public void updateSelected(boolean upd) {
                        super.updateSelected(upd);
                    }
                };
                return cell;
            });

            clnPercCancelados.setCellFactory(param -> {
                return new TableCell<TableManutencaoPedidosPedido, BigDecimal>() {
                    @Override
                    protected void updateItem(BigDecimal item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        clearStyle();

                        if (item != null && !empty) {
                            setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                            if (item.doubleValue() >= .1) {
                                getStyleClass().add("table-row-danger");
                            }
                        }
                    }

                    private void clearStyle() {
                        getStyleClass().remove("table-row-danger");
                    }
                };
            });
        }
    }

    private void alterarGradePedido(TableColumn.CellEditEvent<TableManutencaoPedidosPedido, Integer> action, String valueColumn) {
        TableManutencaoPedidosPedido pedidoTable = action.getRowValue();
        int difAlteracao = action.getNewValue() - action.getOldValue();

        try {
            DAOFactory.getPedidoDAO().alterarGradePedido(pedidoTable.getNumero(),
                    pedidoTable.getCodigo(),
                    pedidoTable.getCor(),
                    valueColumn,
                    difAlteracao,
                    cboxMotivoCancelamento.getSelectionModel().getSelectedItem().getCodmen());
            pedidoTable.setQtdePecas(pedidoTable.getQtdePecas() + difAlteracao);
            pedidoTable.setValorBruto(new BigDecimal(pedidoTable.getValorUnit().doubleValue() * pedidoTable.getQtdePecas()));
            pedidoTable.getTamanhos().replace(valueColumn, action.getNewValue());
            pedidoTable.setEdited(true);

            TableManutencaoPedidosCoresReferencia cores = getCorTable(pedidoTable.getCor());
            cores.getTamanhos().replace(valueColumn, cores.getTamanhos().get(valueColumn) - difAlteracao);
            Log logAction = new Log(SceneMainController.getAcesso().getUsuario(),
                    "Alterar",
                    "SD Manutenção de Pedido",
                    pedidoTable.getNumero(),
                    "Alterado quantidade referência " + pedidoTable.getCodigo() + " para na cor " + pedidoTable.getCor() + " tamanho " + valueColumn
                            + " do pedido " + pedidoTable.getNumero() + " de " + action.getOldValue() + " para " + action.getNewValue());
            try {
                DAOFactory.getLogDAO().saveLog(logAction);
                DAOFactory.getLogDAO().saveLogTI(logAction);
            } catch (SQLException ex) {
                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
            tblCoresRef.refresh();
            GUIUtils.showMessageNotification("Manutenção de Pedidos", "Grade no tamanho: " + valueColumn + " no pedido: " + pedidoTable.getNumero() + " alterada com sucesso!");
        } catch (SQLException ex) {
            Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

    }

    private String corEsgotada(String cor) {
        String tamanhosCor = "";
        tamanhosCor = produtosEsgotados.stream()
                .filter(stringStringMap -> stringStringMap.get("cor").equals(cor))
                .map(stringStringMap -> stringStringMap.get("tam"))
                .collect(Collectors.joining(" , "));

        return tamanhosCor;
    }

    private String corBloqueada(String cor) {
        String tamanhosCor = "";
        tamanhosCor = produtosBloqueados.stream()
                .filter(stringStringMap -> stringStringMap.get("cor").equals(cor))
                .map(stringStringMap -> stringStringMap.get("tam"))
                .collect(Collectors.joining(" , "));

        return tamanhosCor;
    }

    private void makeColumnsTableCores() {

        if (!tblCoresRef.getItems().isEmpty()) {
            tblCoresRef.getColumns().clear();
            TableManutencaoPedidosCoresReferencia cores = tblCoresRef.getItems().get(0);
            columnCoresPosition = 1;
            TableColumn<TableManutencaoPedidosCoresReferencia, String> columnCor = new TableColumn<>("Cor");
            columnCor.prefWidthProperty().set(50.0);
            columnCor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TableManutencaoPedidosCoresReferencia, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<TableManutencaoPedidosCoresReferencia, String> features) {
                    return new ReadOnlyObjectWrapper(((TableManutencaoPedidosCoresReferencia) features.getValue()).getCor());
                }
            });
            columnCor.setCellFactory(paramTableManutencaoPedidosCoresReferenciaIntegerTableColumn -> {
                return new TableCell<TableManutencaoPedidosCoresReferencia, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);

                        if (item != null && !empty) {
                            setText(item);
                        }
                    }

                    private void clearStyle() {
                        getStyleClass().remove("table-row-warning");
                        getStyleClass().remove("table-row-danger");
                    }
                };
            });
            tblCoresRef.getColumns().add(0, columnCor);
            cores.getHeadersTamanhos().forEach((keyColumn, valueColumn) -> {
                TableColumn<TableManutencaoPedidosCoresReferencia, Integer> columnTamanho = new TableColumn<>(valueColumn);
                columnTamanho.prefWidthProperty().set(40.0);
                columnTamanho.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TableManutencaoPedidosCoresReferencia, Integer>, ObservableValue<Integer>>() {
                    @Override
                    public ObservableValue<Integer> call(TableColumn.CellDataFeatures<TableManutencaoPedidosCoresReferencia, Integer> features) {
                        return new ReadOnlyObjectWrapper(((TableManutencaoPedidosCoresReferencia) features.getValue()).getTamanhos().get(valueColumn));
                    }
                });
                columnTamanho.setCellFactory((TableColumn<TableManutencaoPedidosCoresReferencia, Integer> param) -> {
                    TableCell cell = new TableCell<TableManutencaoPedidosCoresReferencia, Integer>() {

                        @Override
                        public void updateItem(Integer item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(empty ? null : getString().toString());
                            setGraphic(null);
                            TableRow currentRow = getTableRow();
                            TableManutencaoPedidosCoresReferencia currentPedido = currentRow == null ? null : (TableManutencaoPedidosCoresReferencia) currentRow.getItem();
                            if (currentPedido != null) {
                                Integer status = currentPedido.getTamanhos().get(valueColumn);
                                clearPriorityStyle();
                                if (!isHover() && !isSelected() && !isFocused() && status != null) {
                                    setPriorityStyle(status);
                                }
                            }
                        }

                        @Override
                        public void updateSelected(boolean upd) {
                            super.updateSelected(upd);
                        }

                        private void clearPriorityStyle() {
                            ObservableList<String> styleClasses = getStyleClass();
                            styleClasses.remove("statusBoletoVencido");

                        }

                        private void setPriorityStyle(Integer priority) {
                            if (priority < 0) {
                                getStyleClass().add("statusBoletoVencido");
                            }
                        }

                        private Integer getString() {
                            return getItem() == null ? 0 : getItem();
                        }
                    };
                    return cell;
                });
                tblCoresRef.getColumns().add(columnCoresPosition++, columnTamanho);
            });
        }
    }

    private Runnable runnableLoadEstoque = new Runnable() {
        @Override
        public void run() {
            try {
                tblGradeProduto.setItems(DAOFactory.getProdutoDAO().getGradeProduto(referencia));
            } catch (SQLException ex) {
                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML actions">
    @FXML
    private void tboxPeriodoOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarPeriodoOnAction(null);
        } else if (event.getCode() == KeyCode.DELETE) {
            ((TextField) event.getSource()).clear();
        }
    }

    @FXML
    private void tboxGrupoClienteOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarGrupoCliente(null);
        } else if (event.getCode() == KeyCode.DELETE) {
            ((TextField) event.getSource()).clear();
        }
    }

    @FXML
    private void tboxRepresentanteOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarRepresentanteOnAction(null);
        } else if (event.getCode() == KeyCode.DELETE) {
            ((TextField) event.getSource()).clear();
        }
    }

    @FXML
    private void tboxProdutoOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        } else if (event.getCode() == KeyCode.DELETE) {
            ((TextField) event.getSource()).clear();
        }
    }

    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxProduto.setText(ctrolFilterProduto.returnValue);
                this.loadCodigos();
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarGrupoCliente(ActionEvent event) {
        try {
            FilterClienteController ctrolFilter = new FilterClienteController(Modals.FXMLWindow.FilterCliente, tboxGrupoCliente.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxGrupoCliente.setText(ctrolFilter.returnValue);
                this.loadCodigos();
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarPeriodoOnAction(ActionEvent event) {
        try {
            FilterPeriodoController ctrolFilter = new FilterPeriodoController(Modals.FXMLWindow.FilterPeriodo, tboxPeriodo.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxPeriodo.setText(ctrolFilter.returnValue);
                this.loadCodigos();
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarRepresentanteOnAction(ActionEvent event) {
        try {
            FilterRepresentanteController ctrolFilter = new FilterRepresentanteController(Modals.FXMLWindow.FilterRepresentante, tboxRepresentante.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxRepresentante.setText(ctrolFilter.returnValue);
                this.loadCodigos();
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnConfirmarRevisaoComercialOnAction(ActionEvent event) {
        try {
            new NativeDAO().runNativeQueryUpdate("update sd_produto_001 set revisao_comercial = 'N' where codigo = '%s'", listReferencias.getSelectionModel().getSelectedItem().getCodigo());
            SysLogger.addSysDelizLog("Manutenção de Pedido", TipoAcao.EDITAR, listReferencias.getSelectionModel().getSelectedItem().getCodigo(),
                    "Marcado produto como revisado status comercial.");
            MessageBox.create(message -> {
                message.message("Marcado produto " + listReferencias.getSelectionModel().getSelectedItem().getCodigo() + " como revisado");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }

    }

    @FXML
    private void btnCarregarPedidosOnAction(ActionEvent event) {
        if (tblCoresRef.getSelectionModel().getSelectedItems().size() > 0) {
            try {
                coresIn = "";
                tblCoresRef.getSelectionModel().getSelectedItems().forEach(cor -> {
                    coresIn += "'" + cor.getCor() + "',";
                });
                coresIn = coresIn.substring(0, coresIn.length() - 1);
                pedidosRef.set(FXCollections.observableList(DAOFactory.getPedidoDAO()
                        .getPedidosManutencao(listReferencias.getSelectionModel().getSelectedItem().getCodigo(), coresIn, this.createWhereFilter())));
                this.makeColumnsTable();
                tblPedidos.setItems(pedidosRef.get());

                referencia = listReferencias.getSelectionModel().getSelectedItem().getCodigo();
                cboxMotivoCancelamento.getSelectionModel().select(getMensagemPadrao());
                new Thread(runnableLoadEstoque).start();
            } catch (SQLException ex) {
                Logger.getLogger(SceneComercialGestaoPedidosManutencaoPedidoController.class
                        .getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        } else {
            GUIUtils.showMessage("Você deve selecionar ao menos uma cor.", Alert.AlertType.INFORMATION);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tblCoresRef.getItems().clear();
        tblPedidos.getItems().clear();
        lbDescProduto.setText("");
        listReferencias.getItems().clear();
        tboxGrupoCliente.clear();
        tboxPeriodo.clear();
        tboxProduto.clear();
        tboxRepresentante.clear();
        tsSelectorGrupoEconomico.switchedOnProperty().set(false);
        tsSelectorPagtoAntecipado.switchedOnProperty().set(false);
    }

    @FXML
    private void btnCarregarEstoqueOnAction(ActionEvent event) {
        if (tblPedidos.getItems().size() <= 0) {
            GUIUtils.showMessage("Você deve carregar uma referência.", Alert.AlertType.INFORMATION);
            return;
        }

        if (showGrade) {
            splitTables.setDividerPositions(1);
            btnCarregarEstoque.setText("Exibir Estoque");
        } else {
            splitTables.setDividerPositions(0.80);
            btnCarregarEstoque.setText("Ocultar Estoque");
        }
        showGrade = !showGrade;
    }

    //<editor-fold defaultstate="collapsed" desc="Disabled method">
    private void btnSalvarAlteracoesOnAction(ActionEvent event) {
        tblPedidos.getItems().forEach(pedido -> {
            if (pedido.isEdited()) {
                System.out.println(pedido.toString());
                pedido.getTamanhos().forEach((t, u) -> {

                    System.out.println(t + ">" + u);
                });
            }
        });
    }
    //</editor-fold>

    @FXML
    private void menuSelectAllOnAction(ActionEvent event) {
        tblPedidos.getItems().forEach(pedido -> {
            pedido.setSelected(!pedido.isSelected());
        });
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Class private">
    private class EditingCellClnTamanho extends TableCell<TableManutencaoPedidosPedido, Integer> {

        private TextField textField;

        public EditingCellClnTamanho() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    private class ProdutoCell extends ListCell<Produto> {

        @Override
        public void updateItem(Produto item, boolean empty) {
            super.updateItem(item, empty);

            int index = this.getIndex();
            String codigo = null;

            if (item != null || !empty) {
                codigo = item.getCodigo();
            }

            this.setText(codigo);
            setGraphic(null);
        }
    }
    //</editor-fold>
}
