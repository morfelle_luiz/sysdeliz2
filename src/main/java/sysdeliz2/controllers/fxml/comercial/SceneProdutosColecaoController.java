/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.comercial;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdProdutoColecao;
import sysdeliz2.models.sysdeliz.SdProdutoColecaoPK;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.sys.LocalLogger;

import javax.persistence.NoResultException;
import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProdutosColecaoController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc="Declaração: Variáveis Controle">
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty novoCadastro = new SimpleBooleanProperty(false);
    private final ObjectProperty<Colecao> colecaoSelecionada = new SimpleObjectProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    private final ListProperty<SdProdutoColecao> registros = new SimpleListProperty<>();
    private final ListProperty<VSdDadosProduto> produtosDisponiveis = new SimpleListProperty<>();
    private final ListProperty<SdProdutoColecao> produtosSelecionados = new SimpleListProperty<>();
    private final ObservableList<VSdDadosProduto> produtosDisponiveisAll = FXCollections.observableArrayList();
    private final ObservableList<SdProdutoColecao> produtosSelecionadosAll = FXCollections.observableArrayList();
    private final ObservableList<SdProdutoColecao> produtosSelecionadosParaDelete = FXCollections.observableArrayList();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    private GenericDao<SdProdutoColecao> daoProdutoColecao = new GenericDaoImpl<>(SdProdutoColecao.class);
    private GenericDao<VSdDadosProduto> daoViewDadosProduto = new GenericDaoImpl<>(VSdDadosProduto.class);
    private GenericDao<Colecao> daoColecao = new GenericDaoImpl<>(Colecao.class);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Componentes">
    @FXML
    private TabPane tabPanePrincipal;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroColecao;
    @FXML
    private Button btnProcurarColecao;
    @FXML
    private TextField tboxFiltroMarca;
    @FXML
    private Button btnProcurarMarca;
    @FXML
    private Button btnCarregar;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private TableView<SdProdutoColecao> tblRegistros;
    @FXML
    private TableColumn<SdProdutoColecao, SdProdutoColecaoPK> clnMarca;
    @FXML
    private TableColumn<SdProdutoColecao, SdProdutoColecaoPK> clnColecao;
    @FXML
    private TableColumn<SdProdutoColecao, SdProdutoColecaoPK> clnProduto;
    @FXML
    private TableColumn<SdProdutoColecao, Boolean> clnContaPr;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbContadorRegistros;
    @FXML
    private Button btnAddRegister;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField tboxColecao;
    @FXML
    private Button btnColecao;
    @FXML
    private TextField tboxFiltroColecaoCadastro;
    @FXML
    private Button btnProcurarColecaoCadastro;
    @FXML
    private TextField tboxFiltroMarcaCadastro;
    @FXML
    private Button btnProcurarMarcaCadastro;
    @FXML
    private ListView<VSdDadosProduto> lviewProdutosDisponiveis;
    @FXML
    private MenuItem requestMenuSelecionarTodos;
    @FXML
    private TextField tboxBuscarProdutosDisponiveis;
    @FXML
    private Button btnAddSelected;
    @FXML
    private Button btnAddAll;
    @FXML
    private Button btnDeleteSelected;
    @FXML
    private Button btnDeleteAll;
    @FXML
    private Label lbContadorProdutos;
    @FXML
    private TableView<SdProdutoColecao> tblProdutosSelecionados;
    @FXML
    private TableColumn<SdProdutoColecao, SdProdutoColecaoPK> clnProdutoSelecionado;
    @FXML
    private TableColumn<SdProdutoColecao, Boolean> clnContaPrSelecionado;
    @FXML
    private TextField tboxProcuraProdutoSelecionado;
    @FXML
    private MenuItem requestMenuSelecionarTodosSelecionados;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponentes();

        try {
            registros.set(daoProdutoColecao.list());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void initializeComponentes() {
        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbContadorRegistros.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registros.sizeProperty()).concat(" registros"));
        lbContadorProdutos.textProperty().bind(new SimpleStringProperty("Selecionado(s) ").concat(produtosSelecionados.sizeProperty()).concat(" produto(s)"));
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TextField CADASTRO">
        TextFieldUtils.upperCase(tboxFiltroMarca);
        TextFieldUtils.upperCase(tboxFiltroColecao);
        //tboxColecao.editableProperty().bind(emEdicao.and(novoCadastro));
        tboxFiltroMarcaCadastro.editableProperty().bind(emEdicao);
        TextFieldUtils.upperCase(tboxFiltroMarcaCadastro);
        tboxFiltroColecaoCadastro.editableProperty().bind(emEdicao);
        TextFieldUtils.upperCase(tboxFiltroColecaoCadastro);
        tboxBuscarProdutosDisponiveis.editableProperty().bind(emEdicao);
        tboxBuscarProdutosDisponiveis.textProperty()
                .addListener((ChangeListener) (observable, oldValue, newValue) -> searchAvailable((String) oldValue, (String) newValue));
        tboxProcuraProdutoSelecionado.editableProperty().bind(emEdicao);
        tboxProcuraProdutoSelecionado.textProperty()
                .addListener((ChangeListener) (observable, oldValue, newValue) -> searchSelected((String) oldValue, (String) newValue));
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Buttons CADASTRO">
        //btnColecao.disableProperty().bind(emEdicao.not().or(novoCadastro.not()));
        btnProcurarColecaoCadastro.disableProperty().bind(emEdicao.not());
        btnProcurarMarcaCadastro.disableProperty().bind(emEdicao.not());
        btnCancel.disableProperty().bind(emEdicao.not());
        btnSave.disableProperty().bind(emEdicao.not());
        btnAddAll.disableProperty().bind(emEdicao.not());
        btnAddSelected.disableProperty().bind(emEdicao.not());
        btnDeleteAll.disableProperty().bind(emEdicao.not());
        btnDeleteSelected.disableProperty().bind(emEdicao.not());
        btnAddRegister.disableProperty().bind(emEdicao);
        btnEditRegister.disableProperty().bind(emEdicao);
        btnDeleteRegister.disableProperty().bind(emEdicao);
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ListView PRODUTOS DISPONIVEIS">
        lviewProdutosDisponiveis.itemsProperty().bind(produtosDisponiveis);
        lviewProdutosDisponiveis.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Table REGISTROS">
        tblRegistros.itemsProperty().bind(registros);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Table PRODUTOS SELECIONADOS">
        tblProdutosSelecionados.itemsProperty().bind(produtosSelecionados);
        tblProdutosSelecionados.editableProperty().bind(emEdicao);
        tblProdutosSelecionados.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblProdutosSelecionados.setRowFactory(param -> {
            return new TableRow<SdProdutoColecao>() {

                @Override
                protected void updateItem(SdProdutoColecao item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();

                    if (item != null && !empty) {
                        if (!item.getId().getCodigo().isAtivo()) {
                            getStyleClass().add("table-row-danger");
                        }
                    }
                }

                private void clearStyle() {
                    getStyleClass().remove("table-row-danger");
                }
            };
        });
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TableColumn REGISTROS">
        clnMarca.setCellFactory(param -> {
            return new TableCell<SdProdutoColecao, SdProdutoColecaoPK>() {
                @Override
                protected void updateItem(SdProdutoColecaoPK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText("[" + item.getCodigo().getCodMarca() + "] " + item.getCodigo().getMarca());
                    }
                }
            };
        });
        clnColecao.setCellFactory(param -> {
            return new TableCell<SdProdutoColecao, SdProdutoColecaoPK>() {
                @Override
                protected void updateItem(SdProdutoColecaoPK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty && item.getColecao() != null) {
                        setText("[" + item.getColecao().getCodigo() + "] " + item.getColecao().getDescricao());
                    }
                }
            };
        });
        clnProduto.setCellFactory(param -> {
            return new TableCell<SdProdutoColecao, SdProdutoColecaoPK>() {
                @Override
                protected void updateItem(SdProdutoColecaoPK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText("[" + item.getCodigo().getCodigo() + "] " + item.getCodigo().getDescricao());
                    }
                }
            };
        });
        clnContaPr.setCellFactory(param -> {
            return new TableCell<SdProdutoColecao, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText(item ? "SIM" : "NÃO");
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUTOS SELECIONADOS">
        clnProdutoSelecionado.setCellFactory(param -> {
            return new TableCell<SdProdutoColecao, SdProdutoColecaoPK>() {
                @Override
                protected void updateItem(SdProdutoColecaoPK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);

                    if (item != null && !empty) {
                        setText("[" + item.getCodigo().getCodigo() + "] " + item.getCodigo().getDescricao());
                    }
                }
            };
        });
        clnProdutoSelecionado.setComparator((p1, p2) -> p1.getCodigo().getCodigo().compareTo(p2.getCodigo().getCodigo()));
        clnContaPrSelecionado.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnContaPrSelecionado.setCellValueFactory((cellData) -> {
            SdProdutoColecao cellValue = cellData.getValue();
            BooleanProperty property = ((SdProdutoColecao) cellValue).pcProperty();
            property.addListener((observable, oldValue, newValue) -> ((SdProdutoColecao) cellValue).setPc(newValue));
            return property;
        });
        // </editor-fold>
    }

    private void carregaProdutosColecao(String colecao) {
        try {
            colecaoSelecionada.set((Colecao) daoColecao.initCriteria().addPredicate("codigo", colecao, PredicateType.EQUAL).loadEntityByPredicate());
            produtosSelecionados.set(daoProdutoColecao.initCriteria()
                    .addPredicate("id.colecao.codigo", colecao, PredicateType.EQUAL)
                    .loadListByPredicate());
            produtosSelecionadosAll.addAll(produtosSelecionados);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void searchAvailable(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            produtosDisponiveis.set(produtosDisponiveisAll);
        }

        String value = newVal.toUpperCase();
        ObservableList<VSdDadosProduto> subentries = FXCollections.observableArrayList();
        for (VSdDadosProduto entry : produtosDisponiveis) {
            if (entry.getCodigo().toUpperCase().contains(value)) {
                subentries.add(entry);
            }
        }
        produtosDisponiveis.set(subentries);
    }

    private void searchSelected(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            produtosSelecionados.set(produtosSelecionadosAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdProdutoColecao> subentries = FXCollections.observableArrayList();
        for (SdProdutoColecao entry : produtosSelecionados) {
            if (entry.getId().getCodigo().getCodigo().toUpperCase().contains(value)) {
                subentries.add(entry);
            }
        }
        produtosSelecionados.set(subentries);

    }

    private void removeSelecionados() {
        List<VSdDadosProduto> produtosJaSelecionado = new ArrayList<>();
        produtosSelecionadosAll.forEach(sdProdutoColecao -> {
            for (VSdDadosProduto vSdDadosProduto : produtosDisponiveisAll) {
                if (vSdDadosProduto.getCodigo().equals(sdProdutoColecao.getId().getCodigo().getCodigo())) {
                    produtosJaSelecionado.add(vSdDadosProduto);
                }
            }
        });
        for (VSdDadosProduto vSdDadosProduto : produtosJaSelecionado) {
            produtosDisponiveisAll.remove(vSdDadosProduto);
            produtosDisponiveis.remove(vSdDadosProduto);
        }
    }

    private void getProdutosDisponiveis(GenericDao daoConsulta) {
        ObservableList<VSdDadosProduto> disponiveis = FXCollections.observableArrayList();
        try {
            disponiveis = daoConsulta.addOrderByAsc("codigo").loadListByPredicate();
            produtosDisponiveis.set(disponiveis);
            produtosDisponiveisAll.addAll(disponiveis);
            this.removeSelecionados();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void getProdutosDisponiveisFiltros() {
        daoViewDadosProduto.clearPredicate();
        daoViewDadosProduto = daoViewDadosProduto.initCriteria().addPredicate("ativo", true, PredicateType.EQUAL);
        if (!tboxFiltroMarcaCadastro.getText().isEmpty()) {
            daoViewDadosProduto = daoViewDadosProduto.addPredicate("codMarca", tboxFiltroMarcaCadastro.getText().split(","), PredicateType.IN);
        }
        if (!tboxFiltroColecaoCadastro.getText().isEmpty()) {
            daoViewDadosProduto = daoViewDadosProduto.addPredicate("codCol", tboxFiltroColecaoCadastro.getText().split(","), PredicateType.IN);
        }
        getProdutosDisponiveis(daoViewDadosProduto);
    }

    private void produtosColecaoNovo() {
        carregaProdutosColecao(tboxColecao.getText());
        if (colecaoSelecionada.get() == null) {
            GUIUtils.showMessage("Coleção não encontrada, verifique se a coleção está cadastrada no TI.");
            tboxColecao.requestFocus();
            return;
        }
        if (emEdicao.get()) {
            daoViewDadosProduto.clearPredicate();
            daoViewDadosProduto = daoViewDadosProduto.initCriteria()
                    .addPredicate("ativo", true, PredicateType.EQUAL);
            getProdutosDisponiveis(daoViewDadosProduto);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="TAB Listagem">
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarProdutoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(produto -> produto.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarColecaoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraColecao = null;
        try {
            procuraColecao = new GenericFilter<Colecao>() {
            };
            procuraColecao.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColecao.setText(procuraColecao.selectedsReturn.stream().map(colecao -> colecao.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        GenericFilter<Marca> procurarMarca = null;
        try {
            procurarMarca = new GenericFilter<Marca>() {
            };
            procurarMarca.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMarca.setText(procurarMarca.selectedsReturn.stream().map(marca -> marca.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tblRegistrosOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() >= 2) {
            SdProdutoColecao produtoSelecionado = tblRegistros.getSelectionModel().getSelectedItem();

            if (produtoSelecionado != null) {
                carregaProdutosColecao(produtoSelecionado.getId().getColecao().getCodigo());
                tboxColecao.setText(produtoSelecionado.getId().getColecao().getCodigo());
                tabPanePrincipal.getSelectionModel().select(1);
            }
        }
    }

    // buttons
    @FXML
    private void btnCarregarOnAction(ActionEvent event) {
        daoProdutoColecao = daoProdutoColecao.initCriteria();
        if (!tboxFiltroMarca.getText().isEmpty()) {
            daoProdutoColecao = daoProdutoColecao.addPredicate("id.codigo.codMarca", tboxFiltroMarca.getText().split(","), PredicateType.IN);
        }
        if (!tboxFiltroColecao.getText().isEmpty()) {
            daoProdutoColecao = daoProdutoColecao.addPredicate("id.colecao.codigo", tboxFiltroColecao.getText().split(","), PredicateType.IN);
        }
        if (!tboxFiltroProduto.getText().isEmpty()) {
            daoProdutoColecao = daoProdutoColecao.addPredicate("id.codigo.codigo", tboxFiltroProduto.getText().split(","), PredicateType.IN);
        }
        try {
            registros.set(daoProdutoColecao.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroColecao.clear();
        tboxFiltroMarca.clear();
        tboxFiltroProduto.clear();
    }

    // request menu
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel", "*.xlsx"))));
        try {
            new ExportExcel<SdProdutoColecao>(tblRegistros).exportTableViewToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TAB Manutenção">
    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        emEdicao.set(true);
        novoCadastro.set(true);
        produtosSelecionados.clear();
        produtosSelecionadosAll.clear();
        produtosDisponiveis.clear();
        produtosDisponiveisAll.clear();
        colecaoSelecionada.set(null);
        tboxColecao.clear();
        tboxColecao.requestFocus();
    }

    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        daoViewDadosProduto.clearPredicate();
        daoViewDadosProduto = daoViewDadosProduto.initCriteria()
                .addPredicate("ativo", true, PredicateType.EQUAL);
        getProdutosDisponiveis(daoViewDadosProduto);
        emEdicao.set(true);
    }

    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (produtosSelecionados.isEmpty()) {
            GUIUtils.showMessage("Selecione uma coleção cadastrada.");
            tboxColecao.requestFocus();
            return;
        }

        if (!GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja excluir os produtos da coleção " + tboxColecao.getText())) {
            return;
        }

        try {
            for (SdProdutoColecao sdProdutoColecao : produtosSelecionadosAll) {
                try {
                    daoProdutoColecao.delete(sdProdutoColecao);
                } catch (NoResultException ex) {
                    ex.printStackTrace();
                }
            }
            tboxColecao.clear();
            btnCancelOnAction(null);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) {
        try {
            for (SdProdutoColecao sdProdutoColecao : produtosSelecionadosParaDelete) {
                try {
                    daoProdutoColecao.delete(sdProdutoColecao.getId());
                } catch (NoResultException ex) {
                    ex.printStackTrace();
                }
            }

            for (SdProdutoColecao sdProdutoColecao : produtosSelecionadosAll) {
                daoProdutoColecao.update(sdProdutoColecao);
            }

            this.btnCancelOnAction(null);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        produtosDisponiveisAll.clear();
        produtosDisponiveis.clear();
        produtosSelecionadosAll.clear();
        produtosSelecionadosParaDelete.clear();
        if (tboxColecao.getText().isEmpty()) {
            produtosSelecionados.clear();
        } else {
            this.carregaProdutosColecao(tboxColecao.getText());
        }
        tboxFiltroColecaoCadastro.clear();
        tboxFiltroMarcaCadastro.clear();
        emEdicao.set(false);
        novoCadastro.set(false);
    }

    @FXML
    private void tboxColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnColecaoOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            this.produtosColecaoNovo();
        }
    }

    @FXML
    private void btnColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraColecao = null;
        try {
            procuraColecao = new GenericFilter<Colecao>() {
            };
            procuraColecao.show(ResultTypeFilter.SINGLE_RESULT);
            tboxColecao.setText(procuraColecao.selectedsReturn.stream().map(colecao -> colecao.getCodigo()).collect(Collectors.joining(",")));

            this.produtosColecaoNovo();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColecaoCadastroOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarColecaoCadastroOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            this.getProdutosDisponiveisFiltros();
        }
    }

    @FXML
    private void btnProcurarColecaoCadastroOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraColecao = null;
        try {
            procuraColecao = new GenericFilter<Colecao>() {
            };
            procuraColecao.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColecaoCadastro.setText(procuraColecao.selectedsReturn.stream().map(colecao -> colecao.getCodigo()).collect(Collectors.joining(",")));
            this.getProdutosDisponiveisFiltros();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroMarcaCadastroOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarMarcaCadastroOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            this.getProdutosDisponiveisFiltros();
        }
    }

    @FXML
    private void btnProcurarMarcaCadastroOnAction(ActionEvent event) {
        GenericFilter<Marca> procurarMarca = null;
        try {
            procurarMarca = new GenericFilter<Marca>() {
            };
            procurarMarca.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMarcaCadastro.setText(procurarMarca.selectedsReturn.stream().map(marca -> marca.getCodigo()).collect(Collectors.joining(",")));
            this.getProdutosDisponiveisFiltros();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnAddSelectedOnAction(ActionEvent event) {
        ObservableList<VSdDadosProduto> produtosDisponiveisSelecionados = FXCollections.observableArrayList();
        produtosDisponiveisSelecionados.addAll(lviewProdutosDisponiveis.getSelectionModel().getSelectedItems());
        for (VSdDadosProduto selectedItem : produtosDisponiveisSelecionados) {
            SdProdutoColecao novoProduto = new SdProdutoColecao(selectedItem, colecaoSelecionada.get());
            produtosSelecionados.add(novoProduto);
            produtosSelecionadosAll.add(novoProduto);
            produtosDisponiveis.remove(selectedItem);
            produtosDisponiveisAll.remove(selectedItem);
        }
    }

    @FXML
    private void btnAddAllOnAction(ActionEvent event) {
        this.requestMenuSelecionarTodosOnAction(null);
        this.btnAddSelectedOnAction(null);
    }

    @FXML
    private void btnDeleteSelectedOnAction(ActionEvent event) {
        ObservableList<SdProdutoColecao> produtosDisponiveisSelecionados = FXCollections.observableArrayList();
        produtosDisponiveisSelecionados.addAll(tblProdutosSelecionados.getSelectionModel().getSelectedItems());
        for (SdProdutoColecao selectedItem : produtosDisponiveisSelecionados) {
            produtosDisponiveis.add(selectedItem.getId().getCodigo());
            produtosDisponiveisAll.add(selectedItem.getId().getCodigo());
            produtosSelecionadosParaDelete.add(selectedItem);
            produtosSelecionados.remove(selectedItem);
            produtosSelecionadosAll.remove(selectedItem);
        }
    }

    @FXML
    private void btnDeleteAllOnAction(ActionEvent event) {
        this.requestMenuSelecionarTodosSelecionadosOnAction(null);
        this.btnDeleteSelectedOnAction(null);
    }

    @FXML
    private void requestMenuSelecionarTodosOnAction(ActionEvent event) {
        lviewProdutosDisponiveis.getSelectionModel().selectAll();
    }

    @FXML
    private void requestMenuSelecionarTodosSelecionadosOnAction(ActionEvent event) {
        tblProdutosSelecionados.getSelectionModel().selectAll();
    }
    //</editor-fold>
}
