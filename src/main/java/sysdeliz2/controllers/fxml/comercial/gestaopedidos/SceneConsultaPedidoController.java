/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.comercial.gestaopedidos;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosPedido;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneConsultaPedidoController implements Initializable {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    private final ListProperty<VSdDadosPedido> pedidos = new SimpleListProperty<>();
    private final ListProperty<PedIten> itensPedido = new SimpleListProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Componentes">
    @FXML
    private TabPane tabsPrincipal;
    @FXML
    private DatePicker tboxDtInicioDigitacao;
    @FXML
    private DatePicker tboxDtFimDigitacao;
    @FXML
    private DatePicker tboxDtInicioEmissao;
    @FXML
    private DatePicker tboxDtFimEmissao;
    @FXML
    private DatePicker tboxDtInicioFatura;
    @FXML
    private DatePicker tboxDtFimFatura;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroCliente;
    @FXML
    private Button btnProcurarCliente;
    @FXML
    private TextField tboxFiltroRepresentante;
    @FXML
    private Button btnProcurarRepresentante;
    @FXML
    private TextField tboxFiltroColecao;
    @FXML
    private Button btnProcurarColecao;
    @FXML
    private TextField tboxFiltroNumero;
    @FXML
    private TextField tboxFiltroMarca;
    @FXML
    private Button btnProcurarMarca;
    @FXML
    private Button btnCarregarProducao;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private TableView<VSdDadosPedido> tblPedidos;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnNumero;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnCliente;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnRepresentante;
    @FXML
    private TableColumn<VSdDadosPedido, String> clnStatus;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnEntrega;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnDtDigitacao;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnDtEmissao;
    @FXML
    private TableColumn<VSdDadosPedido, Pedido> clnDtFatura;
    @FXML
    private TableColumn<VSdDadosPedido, Integer> clnQtdePedido;
    @FXML
    private TableColumn<VSdDadosPedido, BigDecimal> clnValorPedido;
    @FXML
    private TableColumn<VSdDadosPedido, Integer> clnQtdePendente;
    @FXML
    private TableColumn<VSdDadosPedido, BigDecimal> clnValorPendente;
    @FXML
    private TableColumn<VSdDadosPedido, Integer> clnQtdeFaturado;
    @FXML
    private TableColumn<VSdDadosPedido, BigDecimal> clnValorFaturado;
    @FXML
    private MenuItem requestMenuImprimirEspelho;
    @FXML
    private MenuItem requestMenuEnviarEspelho;
    @FXML
    private Label lbContadorPedidos;
    @FXML
    private HBox boxManutencao;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    GenericDao<VSdDadosPedido> daoPedido = new GenericDaoImpl<>(VSdDadosPedido.class);
    GenericDao<SdColecaoMarca001> daoColecaoMarca = new GenericDaoImpl<>(SdColecaoMarca001.class);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Componentes Manutenção">
    private final FormFieldText fieldNumero = FormFieldText.create(field -> {
        field.title("Número");
        field.addStyle("info");
        field.keyReleased(evt -> {
            if (evt.getCode() == KeyCode.ENTER) {
                VSdDadosPedido pedidoCarregado = (VSdDadosPedido) new FluentDao().selectFrom(VSdDadosPedido.class)
                        .where(it -> it
                                .equal("numero.numero", field.value.get()))
                        .singleResult();
                if (pedidoCarregado != null) {
                    unbindPedido();
                    bindPedido(pedidoCarregado);
                } else {
                    MessageBox.create(message -> {
                        message.message("Número de pedido não encontrado.");
                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                        message.showAndWait();
                    });
                }
            }
        });
    });
    private final FormFieldText fieldCliente = FormFieldText.create(field -> {
        field.title("Cliente");
        field.editable(false);
        field.width(500.0);
    });
    private final FormFieldText fieldRepresentante = FormFieldText.create(field -> {
        field.title("Representante");
        field.editable(false);
        field.width(500.0);
    });
    private final FormFieldText fieldDtEmissao = FormFieldText.create(field -> {
        field.title("Emissão");
        field.editable(false);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldText fieldTabela = FormFieldText.create(field -> {
        field.title("Tabela");
        field.editable(false);
        field.width(70.0);
    });
    private final FormFieldText fieldDesconto = FormFieldText.create(field -> {
        field.title("Desconto");
        field.editable(false);
        field.width(120.0);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldText fieldCondicao = FormFieldText.create(field -> {
        field.title("Pagto");
        field.editable(false);
        field.width(150.0);
    });
    private final FormFieldText fieldMarca = FormFieldText.create(field -> {
        field.title("Marca");
        field.editable(false);
        field.width(150.0);
    });
    private final FormFieldText fieldColecao = FormFieldText.create(field -> {
        field.title("Coleção");
        field.editable(false);
        field.width(200.0);
    });
    private final FormFieldText fieldQtde = FormFieldText.create(field -> {
        field.title("Qtde. Pend.");
        field.editable(false);
        field.width(80.0);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldText fieldQtdeFatura = FormFieldText.create(field -> {
        field.title("Qtde. Fat.");
        field.editable(false);
        field.addStyle("lg");
        field.width(100.0);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldText fieldQtdeCancelado = FormFieldText.create(field -> {
        field.title("Qtde. Canc.");
        field.editable(false);
        field.width(80.0);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldText fieldValor = FormFieldText.create(field -> {
        field.title("Pendente");
        field.editable(false);
        field.width(100.0);
        field.addStyle("warning");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldValorFatura = FormFieldText.create(field -> {
        field.title("Faturado");
        field.editable(false);
        field.addStyle("lg").addStyle("success");
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldValorCancelado = FormFieldText.create(field -> {
        field.title("Cancelado");
        field.editable(false);
        field.width(100.0);
        field.addStyle("danger");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldValorDesconto = FormFieldText.create(field -> {
        field.title("Desconto");
        field.editable(false);
        field.width(100.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldStatus = FormFieldText.create(field -> {
        field.title("Status");
        field.editable(false);
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.CENTER);
    });
    private final FormFieldTextArea fieldObservacao = FormFieldTextArea.create(field -> {
        field.title("Observação");
        field.editable(false);
        field.width(500.0);
        field.height(100.0);
    });
    // </editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        this.initializeComponentes();
    }
    
    public void initializeComponentes() {
        
        tboxDtInicioFatura.setValue(LocalDate.of(1900,1,1));
        tboxDtInicioEmissao.setValue(LocalDate.of(1900,1,1));
        tboxDtInicioDigitacao.setValue(LocalDate.of(1900,1,1));
        tboxDtFimFatura.setValue(LocalDate.of(2050,12,31));
        tboxDtFimEmissao.setValue(LocalDate.of(2050,12,31));
        tboxDtFimDigitacao.setValue(LocalDate.of(2050,12,31));
        
        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbContadorPedidos.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(pedidos.sizeProperty()).concat(" registros"));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField FILTROS">
        TextFieldUtils.upperCase(tboxFiltroCliente);
        TextFieldUtils.upperCase(tboxFiltroColecao);
        TextFieldUtils.upperCase(tboxFiltroMarca);
        TextFieldUtils.upperCase(tboxFiltroProduto);
        TextFieldUtils.upperCase(tboxFiltroRepresentante);
        TextFieldUtils.upperCase(tboxFiltroNumero);
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView PEDIDOS">
        tblPedidos.itemsProperty().bind(pedidos);
        tblPedidos.setRowFactory(tv -> {
            TableRow<VSdDadosPedido> row = new TableRow<VSdDadosPedido>() {
                @Override
                protected void updateItem(VSdDadosPedido item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    
                    if (item == null) {
                        clearStyle();
                    } else if (item.getStatus().equals("PENDENTE")) {
                        clearStyle();
                        getStyleClass().add("table-row-warning");
                    } else if (item.getStatus().equals("FATURADO")) {
                        clearStyle();
                        getStyleClass().add("table-row-success");
                    } else if (item.getStatus().equals("CANCELADO")) {
                        clearStyle();
                        getStyleClass().add("table-row-danger");
                    }
                }
                
                private void clearStyle() {
                    getStyleClass().remove("table-row-danger");
                    getStyleClass().remove("table-row-success");
                    getStyleClass().remove("table-row-warning");
                }
                
            };
            row.setOnMouseClicked(evt -> {
                if (evt.getClickCount() > 1) {
                    unbindPedido();
                    bindPedido(tblPedidos.getSelectionModel().getSelectedItem());
                    tabsPrincipal.getSelectionModel().select(1);
                }
            });
            
            return row;
        });
        tblPedidos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PEDIDOS">
        clnNumero.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(item.getNumero());
                }
            };
        });
        clnCliente.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText("[" + item.getCodcli().getCodcli() + "] " + item.getCodcli().getNome());
                }
            };
        });
        clnRepresentante.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText("[" + item.getCodrep().getCodRep() + "] " + item.getCodrep().getNome());
                }
            };
        });
        clnEntrega.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText("[" + item.getPeriodo().getPrazo() + "] " + item.getPeriodo().getDescricao());
                }
            };
        });
        clnDtDigitacao.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toDateFormat(item.getDtdigita()));
                }
            };
        });
        clnDtEmissao.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toDateFormat(item.getDt_emissao()));
                }
            };
        });
        clnDtFatura.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toDateFormat(item.getDt_fatura()));
                }
            };
        });
        clnQtdePedido.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toIntegerFormat(item));
                }
            };
        });
        clnValorPedido.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toMonetaryFormat(item.doubleValue(), 2));
                }
            };
        });
        clnQtdePendente.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toIntegerFormat(item));
                }
            };
        });
        clnValorPendente.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toMonetaryFormat(item.doubleValue(), 2));
                }
            };
        });
        clnQtdeFaturado.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toIntegerFormat(item));
                }
            };
        });
        clnValorFaturado.setCellFactory(param -> {
            return new TableCell<VSdDadosPedido, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty)
                        setText(StringUtils.toMonetaryFormat(item.doubleValue(), 2));
                }
            };
        });
        // </editor-fold>
        
        initManutencaoTab();
    }
    
    private void initManutencaoTab() {
        boxManutencao.getChildren().add(FormBox.create(manutencao -> {
            manutencao.vertical();
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldNumero.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldCliente.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldRepresentante.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldDtEmissao.build());
                boxFields.add(fieldColecao.build());
                boxFields.add(fieldMarca.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldTabela.build());
                boxFields.add(fieldDesconto.build());
                boxFields.add(fieldCondicao.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldQtdeFatura.build());
                boxFields.add(fieldValorFatura.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldQtde.build());
                boxFields.add(fieldQtdeCancelado.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldValor.build());
                boxFields.add(fieldValorCancelado.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldStatus.build());
                boxFields.add(fieldValorDesconto.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldStatus.build());
            }));
            manutencao.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldObservacao.build());
            }));
        }));
        boxManutencao.getChildren().add(FormBox.create(tblItensBox -> {
            tblItensBox.vertical();
            tblItensBox.expanded();
            tblItensBox.add(FormTableView.create(PedIten.class, table -> {
                table.title("Itens");
                table.expanded();
                table.items.bind(itensPedido);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getCodigo()));
                        }).build() /*Código*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Descrição");
                            cln.width(300.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getDescricao()));
                        }).build() /*Descrição*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cód. Cor");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getCor()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /*Cód. Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getDescricao()));
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Tam.");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /*Tam.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Valor");
                            cln.width(100.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPreco()));
                            cln.alignment(FormTableColumn.Alignment.RIGHT);
                            cln.format(param -> {
                                return new TableCell<PedIten, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toMonetaryFormat(item, 2));
                                        }
                                    }
                                };
                            });
                        }).build() /*Valor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /*Qtde*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Fat.");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde_f()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /*Fat.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Canc.");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde_canc()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /*Canc.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Total");
                            cln.width(130.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.alignment(FormTableColumn.Alignment.RIGHT);
                            cln.format(param -> {
                                return new TableCell<PedIten, PedIten>() {
                                    @Override
                                    protected void updateItem(PedIten item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toMonetaryFormat(item.getPreco().multiply(new BigDecimal(item.getQtde() + item.getQtde_f())), 2));
                                        }
                                    }
                                };
                            });
                        }).build() /*Total*/);
            }).build());
        }));
    }
    
    private void bindPedido(VSdDadosPedido pedido) {
        itensPedido.set(FXCollections.observableList(pedido.getNumero().getItens()));
        fieldNumero.value.set(pedido.getNumero().getNumero());
        fieldCliente.value.set(pedido.getNumero().getCodcli().toString());
        fieldRepresentante.value.set(pedido.getNumero().getCodrep().getNome());
        fieldDtEmissao.value.set(StringUtils.toDateFormat(pedido.getNumero().getDt_emissao()));
        fieldTabela.value.set(pedido.getNumero().getTabPre().getRegiao());
        fieldDesconto.value.set(pedido.getNumero().getDesconto());
        fieldCondicao.value.set(pedido.getNumero().getPagto());
        fieldMarca.value.set(pedido.getMarca().getDescricao());
        fieldColecao.value.set(pedido.getColecao().getDescricao());
        fieldQtde.value.set(StringUtils.toIntegerFormat(pedido.getQtde()));
        fieldQtdeCancelado.value.set(StringUtils.toIntegerFormat(pedido.getQtdeCanc()));
        fieldQtdeFatura.value.set(StringUtils.toIntegerFormat(pedido.getQtdeF()));
        fieldValorCancelado.value.set(StringUtils.toMonetaryFormat(pedido.getValorCanc(), 2));
        fieldValor.value.set(StringUtils.toMonetaryFormat(pedido.getValorPend(), 2));
        fieldValorDesconto.value.set(StringUtils.toMonetaryFormat(pedido.getValorDesc(), 2));
        fieldValorFatura.value.set(StringUtils.toMonetaryFormat(pedido.getValorFat(), 2));
        fieldObservacao.value.set(pedido.getNumero().getObs());
        fieldStatus.value.set(pedido.getStatus());
        fieldStatus.addStyle(pedido.getStatus().equals("PENDENTE") ? "warning" : pedido.getStatus().equals("FATURADO") ? "success" : "danger");
    }
    
    private void unbindPedido() {
        itensPedido.clear();
        fieldNumero.clear();
        fieldCliente.clear();
        fieldRepresentante.clear();
        fieldDtEmissao.clear();
        fieldTabela.clear();
        fieldDesconto.clear();
        fieldCondicao.clear();
        fieldMarca.clear();
        fieldColecao.clear();
        fieldQtde.clear();
        fieldQtdeCancelado.clear();
        fieldQtdeFatura.clear();
        fieldValorCancelado.clear();
        fieldValor.clear();
        fieldValorDesconto.clear();
        fieldValorFatura.clear();
        fieldObservacao.clear();
        fieldStatus.clear();
        fieldStatus.removeStyle("success").removeStyle("danger").removeStyle("warning");
    }
    
    // <editor-fold defaultstate="collapsed" desc="Actions FXML: Listagem">
    // <editor-fold defaultstate="collapsed" desc="DataPickers">
    @FXML
    private void tboxDtInicioDigitacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioDigitacao.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimDigitacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimDigitacao.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimDigitacao.setValue(tboxDtInicioDigitacao.getValue());
        }
    }
    
    @FXML
    private void tboxDtInicioEmissaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioEmissao.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimEmissaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimEmissao.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimEmissao.setValue(tboxDtInicioEmissao.getValue());
        }
    }
    
    @FXML
    private void tboxDtInicioFaturaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioFatura.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimFaturaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimFatura.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimFatura.setValue(tboxDtInicioFatura.getValue());
        }
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Produto">
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4)
            this.btnProcurarProdutoOnAction(null);
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(value -> value.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Cliente">
    @FXML
    private void tboxFiltroClienteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4)
            this.btnProcurarClienteOnAction(null);
    }
    
    @FXML
    private void btnProcurarClienteOnAction(ActionEvent event) {
        GenericFilter<Entidade> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Entidade>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(value -> value.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Representante">
    @FXML
    private void tboxFiltroRepresentanteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4)
            this.btnProcurarRepresentanteOnAction(null);
    }
    
    @FXML
    private void btnProcurarRepresentanteOnAction(ActionEvent event) {
        GenericFilter<Represen> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Represen>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(value -> value.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Coleção">
    @FXML
    private void tboxFiltroColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4)
            this.btnProcurarColecaoOnAction(null);
    }
    
    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Colecao>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColecao.setText(procuraProduto.selectedsReturn.stream().map(value -> value.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Marca">
    @FXML
    private void tboxFiltroMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4)
            this.btnProcurarMarcaOnAction(null);
    }
    
    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        GenericFilter<Marca> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Marca>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMarca.setText(procuraProduto.selectedsReturn.stream().map(value -> value.getCodigoFilter()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Buttons">
    @FXML
    private void btnCarregarProducaoOnAction(ActionEvent event) {
        if (!tboxFiltroColecao.getText().isEmpty() && tboxFiltroMarca.getText().isEmpty()) {
            GUIUtils.showMessage("Para consultar a coleção, você deve consultar a marca também.");
            return;
        }
        if (tboxDtFimDigitacao.getValue() != null && tboxDtInicioDigitacao.getValue() == null) {
            GUIUtils.showMessage("Você deve inserir uma data de início para a data de digitação.");
            return;
        } else if (tboxDtFimDigitacao.getValue() != null && tboxDtInicioDigitacao.getValue() != null &&
                tboxDtFimDigitacao.getValue().isBefore(tboxDtInicioDigitacao.getValue())) {
            GUIUtils.showMessage("A data DE deve ser menor que a data ATÉ para a data de digitação.");
            return;
        }
        if (tboxDtFimEmissao.getValue() != null && tboxDtInicioEmissao.getValue() == null) {
            GUIUtils.showMessage("Você deve inserir uma data de início para a data de emissão.");
            return;
        } else if (tboxDtFimEmissao.getValue() != null && tboxDtInicioEmissao.getValue() != null &&
                tboxDtFimEmissao.getValue().isBefore(tboxDtInicioEmissao.getValue())) {
            GUIUtils.showMessage("A data DE deve ser menor que a data ATÉ para a data de emissão.");
            return;
        }
        if (tboxDtFimFatura.getValue() != null && tboxDtInicioFatura.getValue() == null) {
            GUIUtils.showMessage("Você deve inserir uma data de início para a data de fatura.");
            return;
        } else if (tboxDtFimFatura.getValue() != null && tboxDtInicioFatura.getValue() != null &&
                tboxDtFimFatura.getValue().isBefore(tboxDtInicioFatura.getValue())) {
            GUIUtils.showMessage("A data DE deve ser menor que a data ATÉ para a data de fatura.");
            return;
        }
        
        LocalDate dtFimDigitacao = tboxDtFimDigitacao.getValue() == null ? tboxDtInicioDigitacao.getValue() : tboxDtFimDigitacao.getValue();
        LocalDate dtFimEmissao = tboxDtFimEmissao.getValue() == null ? tboxDtInicioEmissao.getValue() : tboxDtFimEmissao.getValue();
        LocalDate dtFimFatura = tboxDtFimFatura.getValue() == null ? tboxDtInicioFatura.getValue() : tboxDtFimFatura.getValue();
        
        JPAUtils.clearEntitys(pedidos);
        daoPedido.clearPredicate();
        daoPedido = daoPedido.initCriteria();
        
        if (tboxDtInicioDigitacao.getValue() != null)
            daoPedido = daoPedido.addPredicateBetween("numero.dtdigita", tboxDtInicioDigitacao.getValue(), dtFimDigitacao);
        if (tboxDtInicioEmissao.getValue() != null)
            daoPedido = daoPedido.addPredicateBetween("numero.dt_emissao", tboxDtInicioEmissao.getValue(), dtFimEmissao);
        if (tboxDtInicioFatura.getValue() != null)
            daoPedido = daoPedido.addPredicateBetween("numero.dt_fatura", tboxDtInicioFatura.getValue(), dtFimFatura);
        if (!tboxFiltroColecao.getText().isEmpty())
            daoPedido = daoPedido.addPredicate("colecao.codigo", tboxFiltroColecao.getText().split(","), PredicateType.IN);
        if (!tboxFiltroMarca.getText().isEmpty())
            daoPedido = daoPedido.addPredicate("marca.codigo", tboxFiltroMarca.getText().split(","), PredicateType.IN);
        if (!tboxFiltroRepresentante.getText().isEmpty())
            daoPedido = daoPedido.addPredicate("numero.codrep.codrep", tboxFiltroRepresentante.getText().split(","), PredicateType.IN);
        if (!tboxFiltroNumero.getText().isEmpty())
            daoPedido = daoPedido.addPredicate("numero.numero", tboxFiltroNumero.getText().split(","), PredicateType.IN);
        if (!tboxFiltroCliente.getText().isEmpty())
            daoPedido = daoPedido.addPredicate("numero.codcli.codcli", tboxFiltroCliente.getText().split(","), PredicateType.IN);
        
        try {
            pedidos.set(daoPedido.addDistinctValues().addOrderByAsc("numero").loadListByPredicate());
            
            if (!tboxFiltroProduto.getText().isEmpty())
                pedidos.set(FXCollections.observableList(pedidos.stream()
                        .filter(pedido ->
                                pedido.getNumero().getItens().stream()
                                        .map(pedIten -> pedIten.getId().getCodigo().getCodigo())
                                        .distinct()
                                        .collect(Collectors.toList()).contains(tboxFiltroProduto.getText()))
                        .collect(Collectors.toList())));
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxDtInicioFatura.setValue(LocalDate.of(1900,1,1));
        tboxDtInicioEmissao.setValue(LocalDate.of(1900,1,1));
        tboxDtInicioDigitacao.setValue(LocalDate.of(1900,1,1));
        tboxDtFimFatura.setValue(LocalDate.of(2050,12,31));
        tboxDtFimEmissao.setValue(LocalDate.of(2050,12,31));
        tboxDtFimDigitacao.setValue(LocalDate.of(2050,12,31));
        tboxFiltroCliente.clear();
        tboxFiltroColecao.clear();
        tboxFiltroMarca.clear();
        tboxFiltroNumero.clear();
        tboxFiltroProduto.clear();
        tboxFiltroRepresentante.clear();
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="RequestMenu">
    @FXML
    private void requestMenuImprimirEspelhoOnAction(ActionEvent event) {
        tblPedidos.getSelectionModel().getSelectedItems().forEach(pedido -> {
            try {
                new ReportUtils().config()
                        .addReport(ReportUtils.ReportFile.ESPELHO_PEDIDO_ERP,
                                new ReportUtils.ParameterReport("p_of", pedido.getNumero().getNumero()),
                                new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                        .view().toView();
            } catch (JRException | SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    }
    
    @FXML
    private void requestMenuEnviarEspelhoOnAction(ActionEvent event) {
        tblPedidos.getSelectionModel().getSelectedItems().forEach(pedido -> {
            List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
            InputStream inputStream = null;
            inputStream = SceneConsultaPedidoController.class.getResourceAsStream("/relatorios/EspelhoPedidoErp.jasper");
            Map<String,Object> parametros = new HashMap<>();
            parametros.put("p_of", pedido.getNumero().getNumero());
            parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());
            try {
                /**
                 * Prepare PDF file report with order sale
                 */
                jasperPrintList.add(JasperFillManager.fillReport(inputStream, parametros, ConnectionFactory.getEmpresaConnection()));
                ReportUtils.jasperToPdfNoOpen(jasperPrintList, "EspelhoPedido_" + pedido.getNumero().getNumero());
                SimpleMail.INSTANCE
                        .addDestinatario(pedido.getNumero().getCodcli().getEmail())
                        .comAssunto("Pedido " + pedido.getMarca().getDescricao() + " " + pedido.getNumero().getNumero() + " - Espelho de Pedido")
                        .addReplyTo("comercial@deliz.com.br")
                        .comCorpo("<html><head>"
                                + "<title>Pedido " + pedido.getNumero().getNumero() + " Deliz realizado.</title>"
                                + "<meta charset=\"utf-8\">"
                                + "</head>\n"
                                + "<body><div><h2>Pedido realizado!</h2>"
                                + "<span>Olá, " + pedido.getNumero().getCodcli().getNome() + "!</br>"
                                + "Seu pedido, número " + pedido.getNumero().getNumero() + ", foi realizado com sucesso em "
                                + StringUtils.toDateFormat(pedido.getNumero().getDt_emissao()) + ".<br/>"
                                + "Anexo espelho de pedido.<br/><br/>"
                                + "Qualquer dúvida ou necessidade estamos a disposição para atendê-lo.<br/><br/>"
                                + "Atenciosamente,<br/>"
                                + "Fone: (48) 3641-1900<br/>"
                                + "Whatsapp: (47) 9 9187-1164<br/>"
                                + "E-mail: comercial@deliz.com.br<br/>"
                                + "www.deliz.com.br</span>"
                                + "</div>"
                                + "</body></html>")
                        .addAttachment("C:\\SysDelizLocal\\local_files\\EspelhoPedido_" + pedido.getNumero().getNumero() + ".pdf")
                        .send();
                File someFileEspelho = new File("C:\\SysDelizLocal\\local_files\\EspelhoPedido_" + pedido.getNumero().getNumero() + ".pdf");
                someFileEspelho.delete();
            } catch (SQLException | JRException | IOException | NullPointerException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    }
    //</editor-fold>
    //</editor-fold>
    
}
