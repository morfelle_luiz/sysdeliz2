/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.compras;

import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdAgendaServico001;
import sysdeliz2.models.properties.GestaoCompra00X;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.MaskTextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneAdicionarAgendaOcController extends Modals implements Initializable {

    @FXML
    private Button btnFechar;
    @FXML
    private Button btnNovaObservacao;
    @FXML
    private Button btnBack;
    @FXML
    private Label lbRegister;
    @FXML
    private Button btnNext;
    @FXML
    private TextField tboxNumero;
    @FXML
    private TextField tboxUsuario;
    @FXML
    private Button btnSalvar;
    @FXML
    private TextField tboxDtCadastro;
    @FXML
    private TextField tboxTipo;
    @FXML
    private DatePicker tboxDtAgenda;
    @FXML
    private TextField tboxHrAgenda;
    @FXML
    private ComboBox<String> cboxRepeticao;
    @FXML
    private Spinner<Integer> numericRepeticoes;
    @FXML
    private Button btnExcluir;
    @FXML
    private TextField tboxCodCli;
    @FXML
    private TextField tboxEmail;

    private final ListProperty<SdAgendaServico001> agendasOc = new SimpleListProperty<>();
    private final IntegerProperty indexSelected = new SimpleIntegerProperty(-1);
    private final BooleanProperty editionMode = new SimpleBooleanProperty(false);
    private GestaoCompra00X selectedOc = null;
    private SdAgendaServico001 newAgendaOc = null;

    public SceneAdicionarAgendaOcController(FXMLWindow file, GestaoCompra00X selectedOc) throws IOException {
        super(file);
        this.selectedOc = selectedOc;

        super.show(this);
    }

    private void bindData(SdAgendaServico001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().bind(selected.campo1Property());
            tboxDtCadastro.textProperty().bind(selected.dtCadastroPropertyAsString());
            tboxTipo.textProperty().bind(selected.tipoProperty());
            tboxUsuario.textProperty().bind(selected.usuarioProperty());
            tboxDtAgenda.valueProperty().bind(selected.dtAgendaProperty());
            tboxHrAgenda.textProperty().bind(selected.hrAgendaProperty());
            tboxEmail.textProperty().bind(selected.campo3Property());
            tboxCodCli.textProperty().bind(selected.campo2Property());
        }
    }

    private void unbindData() {
        tboxNumero.textProperty().unbind();
        tboxDtCadastro.textProperty().unbind();
        tboxTipo.textProperty().unbind();
        tboxUsuario.textProperty().unbind();
        tboxDtAgenda.valueProperty().unbind();
        tboxHrAgenda.textProperty().unbind();
        tboxEmail.textProperty().unbind();
        tboxCodCli.textProperty().unbind();
    }

    private void bindBirectionalData(SdAgendaServico001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().bind(selected.campo1Property());
            tboxDtCadastro.textProperty().bind(selected.dtCadastroPropertyAsString());
            tboxTipo.textProperty().bind(selected.tipoProperty());
            tboxUsuario.textProperty().bind(selected.usuarioProperty());
            tboxEmail.textProperty().bind(selected.campo3Property());
            tboxCodCli.textProperty().bind(selected.campo2Property());
            tboxDtAgenda.valueProperty().bindBidirectional(selected.dtAgendaProperty());
            tboxHrAgenda.textProperty().bindBidirectional(selected.hrAgendaProperty());
        }
    }

    private void unbindBirectionalData(SdAgendaServico001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().unbind();
            tboxDtCadastro.textProperty().unbind();
            tboxTipo.textProperty().unbind();
            tboxUsuario.textProperty().unbind();
            tboxEmail.textProperty().unbind();
            tboxCodCli.textProperty().unbind();
            tboxDtAgenda.valueProperty().unbindBidirectional(selected.dtAgendaProperty());
            tboxHrAgenda.textProperty().unbindBidirectional(selected.hrAgendaProperty());
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskTextField.timeField(tboxHrAgenda);
        lbRegister.textProperty().bind(indexSelected.add(1).asString());
        btnExcluir.disableProperty().bind(agendasOc.emptyProperty().or(editionMode));
        btnBack.disableProperty().bind(indexSelected.lessThanOrEqualTo(0).or(agendasOc.emptyProperty()).or(editionMode));
        btnNext.disableProperty().bind(indexSelected.greaterThanOrEqualTo(agendasOc.sizeProperty().subtract(1)).or(agendasOc.emptyProperty()).or(editionMode));
        btnSalvar.disableProperty().bind(editionMode.not());
        numericRepeticoes.disableProperty().bind(cboxRepeticao.valueProperty().isEqualTo("Não repetir"));
        btnNovaObservacao.disableProperty().bind(editionMode);

        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 10, 0);
        numericRepeticoes.setValueFactory(valueFactory);
        numericRepeticoes.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);

        try {
            agendasOc.set(DAOFactory.getSdAgendaServico001DAO().getCrmOrdemCompraMaterial(selectedOc));
            if (agendasOc.getSize() > 0) {
                indexSelected.set(indexSelected.add(1).get());
                unbindData();
                bindData(agendasOc.get(indexSelected.get()));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdicionarAgendaOcController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnNovaObservacaoOnAction(ActionEvent event) {
        unbindData();
        editionMode.set(true);
        newAgendaOc = new SdAgendaServico001(
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")),
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")),
                selectedOc.getTipo().equals("MAT") ? "OC1" : "OT1",
                selectedOc.getNumero(),
                selectedOc.getCodcli(),
                selectedOc.getEmail(),
                SceneMainController.getAcesso().getEmail(),
                SceneMainController.getAcesso().getDisplayName(),
                SceneMainController.getAcesso().getUsuario(),
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        bindBirectionalData(newAgendaOc);
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        try {
            LocalDateTime dataHoraAgora = LocalDateTime.now();
            LocalDateTime dataHoraAgenda = LocalDateTime.parse(
                    tboxDtAgenda.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + tboxHrAgenda.getText() + ":00",
                    DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
            if (dataHoraAgenda.getDayOfMonth() <= dataHoraAgora.getDayOfMonth()
                    && dataHoraAgenda.getMonthValue() <= dataHoraAgora.getMonthValue()
                    && dataHoraAgenda.getYear() <= dataHoraAgora.getYear()
                    && dataHoraAgenda.getHour() <= dataHoraAgora.getHour()) {
                GUIUtils.showMessage("A data e hora de agenda não pode ser igual ou menor a data e hora do momento, revise a data inicial da agenda.", Alert.AlertType.WARNING);
                return;
            }

            unbindBirectionalData(newAgendaOc);
            for (int i = 0; i <= numericRepeticoes.getValue(); i++) {
                SdAgendaServico001 toSave = new SdAgendaServico001(newAgendaOc);
                if (cboxRepeticao.getValue().equals("Mensalmente")) {
                    toSave.setDtAgenda(newAgendaOc.getDtAgenda().plusMonths(i));
                } else if (cboxRepeticao.getValue().equals("Semanalmente")) {
                    toSave.setDtAgenda(newAgendaOc.getDtAgenda().plusWeeks(i));
                } else {
                    toSave.setDtAgenda(newAgendaOc.getDtAgenda().plusDays(i));
                }
                DAOFactory.getSdAgendaServico001DAO().save(toSave);
                agendasOc.add(toSave);
            }
            newAgendaOc = null;
            indexSelected.set(agendasOc.getSize() - 1);
            bindData(agendasOc.get(indexSelected.get()));
            editionMode.set(false);
            GUIUtils.showMessageNotification("Agenda Ordem de Compra", "Agenda de reenvio de O.C. cadastrada.");
        } catch (SQLException ex) {
            Logger.getLogger(SceneAdicionarAgendaOcController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnBackOnAction(ActionEvent event) {
        indexSelected.set(indexSelected.subtract(1).get());
        unbindData();
        bindData(agendasOc.get(indexSelected.get()));
    }

    @FXML
    private void btnNextOnAction(ActionEvent event) {
        indexSelected.set(indexSelected.add(1).get());
        unbindData();
        bindData(agendasOc.get(indexSelected.get()));
    }

    @FXML
    private void btnExcluirOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Você realmente quer excluir o registro?")) {
            try {
                DAOFactory.getSdAgendaServico001DAO().delete(agendasOc.get(indexSelected.get()));
                agendasOc.remove(indexSelected.get());
                indexSelected.set(0);
                unbindData();
                bindData(agendasOc.get(indexSelected.get()));
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        }
    }

}
