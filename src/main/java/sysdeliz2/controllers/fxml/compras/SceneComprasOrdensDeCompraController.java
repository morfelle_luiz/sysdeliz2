/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.compras;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterFornecedorController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.*;
import sysdeliz2.models.properties.GestaoCompra00X;
import sysdeliz2.models.properties.LogEmailOc00X;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.models.view.VSdMrpOc;
import sysdeliz2.utils.ImageUtils;
import sysdeliz2.utils.*;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneComprasOrdensDeCompraController implements Initializable {

    Locale BRAZIL = new Locale("pt", "BR");
    private final ListProperty<GestaoCompra00X> registerFind = new SimpleListProperty<>();
    private final ToggleSwitch toggleRed = new ToggleSwitch(16, 40, "#f5c6cb");
    private final ToggleSwitch toggleGreen = new ToggleSwitch(16, 40, "#c3e6cb");
    private final ToggleSwitch toggleBlue = new ToggleSwitch(16, 40, "#b8daff");
    private final ToggleSwitch toggleYellow = new ToggleSwitch(16, 40, "#ffeeba");
    private final ToggleSwitch toggleGrey = new ToggleSwitch(16, 40, "#d6d8db");
    private final ToggleSwitch toggleLight = new ToggleSwitch(16, 40, "#bee5eb");
    private final ToggleSwitch toggleOrange = new ToggleSwitch(16, 40, "#FC9323");
    private final List<GestaoCompra00X> itensAConfirmarToSend = new ArrayList<>();
    private final List<GestaoCompra00X> itensAtrasadosToSend = new ArrayList<>();
    private List<String> to_emails = new ArrayList<>();

    //<editor-fold defaultstate="collapsed" desc="FXML components">
    @FXML
    private StackPane stackPane;
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<GestaoCompra00X> tblOrdens;
    @FXML
    private TableColumn<GestaoCompra00X, GestaoCompra00X> clnAcoes;
    @FXML
    private TableColumn<GestaoCompra00X, Boolean> clnSelected;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnEmail;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnPedFornecedor;
    @FXML
    private TableColumn<GestaoCompra00X, Double> clnQtde;
    @FXML
    private TableColumn<GestaoCompra00X, Double> clnCusto;
    @FXML
    private TableColumn<GestaoCompra00X, Double> clnValor;
    @FXML
    private TableColumn<GestaoCompra00X, LocalDate> clnDtEntrega;
    @FXML
    private TableColumn<GestaoCompra00X, LocalDate> clnDtFatura;
    @FXML
    private TableColumn<GestaoCompra00X, LocalDate> clnDtEmissao;
    @FXML
    private TableColumn<GestaoCompra00X, LocalDate> clnDias;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnFatura;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnCondicao;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnCodcli;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnStatusEnvio;
    @FXML
    private TableColumn<GestaoCompra00X, String> clnDeposito;
    @FXML
    private TableView<LogEmailOc00X> tblLogEmails;
    @FXML
    private Button btnEnviarTodos;
    @FXML
    private Label lbCountRegister;
    @FXML
    private Label lbFaturamentoAtrasado;
    @FXML
    private Label lbFaturamentoAConfirmar;
    @FXML
    private Label lbFaturamentoComReenvio;
    @FXML
    private Label lbFaturado;
    @FXML
    private Label lbSemDataFaturamento;
    @FXML
    private Label lblLoteAtrasado;
    @FXML
    private TextField tboxFornecedor;
    @FXML
    private TextField tboxMaterial;
    @FXML
    private TextField tboxGrupoComprador;
    @FXML
    private TextField tboxOrdemCompra;
    @FXML
    private TextField tboxCor;
    @FXML
    private TextField tboxNotaFiscal;
    @FXML
    public TextField tboxLote;
    @FXML
    private Button btnCarregarOcs;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnExportarExcel;
    @FXML
    private Button btnForcarAttLote;
    @FXML
    private CheckBox chboxMarcarTodos;
    @FXML
    private Label lbOrdemCompraEnviada;
    @FXML
    private DatePicker dpickerInicio;
    @FXML
    private DatePicker dpickerFim;
    @FXML
    private TextField tboxDeposito;
//</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeFieldsFilter();

        AcessoSistema usuario = SceneMainController.getAcesso();

        tblOrdens.itemsProperty().bind(registerFind);
        lbCountRegister.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registerFind.sizeProperty()).concat(" registros"));

        makeLabelsToggleFilter();

        chboxMarcarTodos.selectedProperty().addListener((observable, oldValue, newValue) -> {
            registerFind.forEach(ordem -> ordem.setSelected(newValue));
            chboxMarcarTodos.setText(newValue ? "Desmarcar Todos" : "Marcar Todos");
        });

        try {
            registerFind.set(DAOFactory.getGestaoCompra00XDAO().getAll());
            makeColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblOrdens.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                try {
                    tblLogEmails.setItems(DAOFactory.getLogEmailOc00XDAO().getLogByOc(newValue.getNumero()));
                } catch (SQLException ex) {
                    Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        });
    }

    private void makeLabelsToggleFilter() {
        lbFaturamentoAtrasado.setGraphic(toggleRed);
        lbFaturamentoAConfirmar.setGraphic(toggleYellow);
        lbFaturamentoComReenvio.setGraphic(toggleBlue);
        lblLoteAtrasado.setGraphic(toggleOrange);
        lbFaturado.setGraphic(toggleGreen);
        lbSemDataFaturamento.setGraphic(toggleGrey);
        lbOrdemCompraEnviada.setGraphic(toggleLight);
        toggleRed.switchedOnProperty().set(false);
        toggleYellow.switchedOnProperty().set(false);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(false);
        toggleGrey.switchedOnProperty().set(false);
        toggleLight.switchedOnProperty().set(false);
        toggleOrange.switchedOnProperty().set(false);
    }

    private void makeFieldsFilter() {
        TextFieldUtils.upperCase(tboxCor);
        TextFieldUtils.upperCase(tboxNotaFiscal);
        TextFieldUtils.upperCase(tboxGrupoComprador);
        TextFieldUtils.upperCase(tboxMaterial);
        TextFieldUtils.upperCase(tboxFornecedor);
        TextFieldUtils.upperCase(tboxOrdemCompra);
        TextFieldUtils.inSql(tboxOrdemCompra);
        TextFieldUtils.inSql(tboxCor);
        TextFieldUtils.inSql(tboxGrupoComprador);
    }

    private void marcarItensOc(GestaoCompra00X ordemCompra, boolean state) {
        ordemCompra.setSelected(state);
        registerFind.stream().
                filter(oc -> Boolean.logicalAnd(
                        Boolean.logicalAnd(
                                oc.getNumero().equals(ordemCompra.getNumero()),
                                oc.getCodcli().equals(ordemCompra.getCodcli())),
                        Boolean.logicalAnd(
                                !oc.getStatus().equals("S"),
                                !oc.getStatus().equals("G")))).
                forEach(ocs -> ocs.setSelected(state));
    }

    private void sendSelectedOrdemCompra(GestaoCompra00X ordemCompra) {

        itensAConfirmarToSend.clear();
        itensAtrasadosToSend.clear();
        to_emails.clear();

        registerFind.stream().
                filter(oc -> Boolean.logicalAnd(
                        Boolean.logicalAnd(
                                oc.getNumero().equals(ordemCompra.getNumero()),
                                oc.getCodcli().equals(ordemCompra.getCodcli())),
                        oc.getStatus().equals("Y"))).
                forEach(ordemCompraStream -> {
                    itensAConfirmarToSend.add(ordemCompraStream);
                    if (!to_emails.contains(ordemCompraStream.getEmail())) {
                        to_emails.add(ordemCompraStream.getEmail());
                    }
                });

        registerFind.stream().
                filter(oc -> Boolean.logicalAnd(
                        Boolean.logicalAnd(
                                oc.getNumero().equals(ordemCompra.getNumero()),
                                oc.getCodcli().equals(ordemCompra.getCodcli())),
                        oc.getStatus().equals("R"))).
                forEach(ordemCompraStream -> {
                    itensAtrasadosToSend.add(ordemCompraStream);
                    if (!to_emails.contains(ordemCompraStream.getEmail())) {
                        to_emails.add(ordemCompraStream.getEmail());
                    }
                });
        try {
            if (itensAConfirmarToSend.size() > 0) {
                MailUtils.sendOcAConfirmar(to_emails, itensAConfirmarToSend);
            }
            if (itensAtrasadosToSend.size() > 0) {
                MailUtils.sendOcAtrasada(to_emails, itensAtrasadosToSend);
            }

            itensAConfirmarToSend.forEach(ordemMaterial -> {
                try {
                    DAOFactory.getSdLogEmailOc001DAO().save(new SdLogEmailOc001(
                            ordemMaterial.getCodcli(),
                            ordemMaterial.getNumero(),
                            ordemMaterial.getCodigo(),
                            ordemMaterial.getCor(),
                            null,
                            ordemMaterial.getDtFaturaAsString(),
                            ordemMaterial.getEmail()));
                    ordemMaterial.setStatus("E");
                } catch (SQLException ex) {
                    SysLogger.addFileLog("Não pode salvar o log de envio de e-mail para o fornecedor " + ordemMaterial.getCodcli() +
                            " com o número " + ordemMaterial.getNumero(), "Compras::Envio de E-mail ao Fornecedor");
                    ex.printStackTrace();
                    GUIUtils.showException(ex);
                }
            });

            itensAtrasadosToSend.forEach(ordemMaterial -> {
                try {
                    DAOFactory.getSdLogEmailOc001DAO().save(new SdLogEmailOc001(
                            ordemMaterial.getCodcli(),
                            ordemMaterial.getNumero(),
                            ordemMaterial.getCodigo(),
                            ordemMaterial.getCor(),
                            null,
                            ordemMaterial.getDtFaturaAsString(),
                            ordemMaterial.getEmail()));
                    ordemMaterial.setStatus("E");
                } catch (SQLException ex) {
                    SysLogger.addFileLog("Não pode salvar o log de envio de e-mail para o fornecedor " + ordemMaterial.getCodcli() +
                            " com o número " + ordemMaterial.getNumero(), "Compras::Envio de E-mail ao Fornecedor");
                    ex.printStackTrace();
                    GUIUtils.showException(ex);
                }
            });

            GUIUtils.showMessageNotification("Envio de O.C.", "O.C. enviada ao fornecedor.");
            tblOrdens.refresh();
        } catch (UnsupportedEncodingException ex) {
            SysLogger.addFileLog("Erro no envio do e-mail para " + ordemCompra.getCodcli() + "/" + ordemCompra.getEmail() +
                    " com o número " + ordemCompra.getNumero() + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (MessagingException ex) {
            SysLogger.addFileLog("Erro no envio do e-mail para " + ordemCompra.getCodcli() + "/" + ordemCompra.getEmail() +
                    " com o número " + ordemCompra.getNumero() + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (ParseException ex) {
            SysLogger.addFileLog("Erro no envio do e-mail para " + ordemCompra.getCodcli() + "/" + ordemCompra.getEmail() +
                    " com o número " + ordemCompra.getNumero() + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void agendarReenvioOrdemCompra(GestaoCompra00X ordemCompra) {
        try {
            SceneAdicionarAgendaOcController controller = new SceneAdicionarAgendaOcController(Modals.FXMLWindow.ComprasAgendaReenvioOc, ordemCompra);

            registerFind.stream().
                    filter(oc -> Boolean.logicalAnd(
                            oc.getNumero().equals(ordemCompra.getNumero()),
                            oc.getCodcli().equals(ordemCompra.getCodcli()))).
                    forEach(ordemCompraStream -> ordemCompraStream.setStatus("B"));
            tblOrdens.refresh();
        } catch (IOException ex) {
            SysLogger.addFileLog("Erro ao agendar o envio do e-mail para " + ordemCompra.getCodcli() + "/" + ordemCompra.getEmail() +
                    " com o número " + ordemCompra.getNumero() + " - ERROR: \n" + ex.getMessage(), "Compras::Agendamento Envio de E-mail ao Fornecedor");
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void observacaoOrdemCompra(GestaoCompra00X ordemCompra) {
        try {
            SceneObservacaoOcController controller = new SceneObservacaoOcController(Modals.FXMLWindow.ComprasObservacaoOc, ordemCompra);
        } catch (IOException ex) {
            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void observacaoRecebimentoOrdemCompra(GestaoCompra00X ordemCompra) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText("Observações de Recebimento da OC");
        alert.getButtonTypes().clear();
        alert.getButtonTypes().add(new ButtonType("Salvar"));

        TextArea textArea = new TextArea(ordemCompra.getObsRecebimento());
        textArea.setEditable(true);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        if (alert.showAndWait().get() == alert.getButtonTypes().get(0)) {
            try {
                registerFind.stream()
                        .filter(oc -> oc.getNumero().equals(ordemCompra.getNumero()))
                        .forEach(oc -> oc.setObsRecebimento(textArea.getText()));
                if (ordemCompra.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateObsRecebimentoCompra(ordemCompra);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateObsRecebimentoTecelagem(ordemCompra);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        }
    }

    private void imprimirEspelhoOrdemCompra(GestaoCompra00X ordemCompra) {
        try {
            // First, compile jrxml file.
            ImageUtils.buildQRCode(ordemCompra.getNumero(), 300, 300, "C:\\SysDelizLocal\\local_files\\qcode_oc.png");
            JasperReport jasperReport = null;

            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();

            Map<String, Integer> opcoes = new HashMap<>();
            opcoes.put("Espelho O.C.", 1);
            opcoes.put("O.C. agrup. NF", 2);
            opcoes.put("NF agrup. O.C.", 3);
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Opção de layout:", opcoes);
            parameters.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());
            if (opcaoEscolhida == 1) {
                parameters.put("ordem_compra", ordemCompra.getNumero());
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/EspelhoOrdemCompra.jasper"));
            } else if (opcaoEscolhida == 2) {
                if (ordemCompra.getFatura() == null || ordemCompra.getFatura().isEmpty()) {
                    GUIUtils.showMessage("O item selecionado ainda não foi faturado, para utilizar este layout de espelho, somente com itens já faturados.");
                    return;
                }
                parameters.put("ordem_compra", ordemCompra.getNumero());
                parameters.put("nota_fiscal", ordemCompra.getFatura());
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/EspelhoOrdemCompraGrpNf.jasper"));
            } else if (opcaoEscolhida == 3) {
                if (ordemCompra.getFatura() == null || ordemCompra.getFatura().isEmpty()) {
                    GUIUtils.showMessage("O item selecionado ainda não foi faturado, para utilizar este layout de espelho, somente com itens já faturados.");
                    return;
                }
                ImageUtils.buildQRCode(ordemCompra.getFatura(), 300, 300, "C:\\SysDelizLocal\\local_files\\qrCode_" + ordemCompra.getCodcli().concat(ordemCompra.getFatura()) + ".png");
                parameters.put("nota_fiscal", ordemCompra.getFatura());
                parameters.put("codcli", ordemCompra.getCodcli());
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/EspelhoOrdemCompraGrpOcByNf.jasper"));
            }

            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            if (opcaoEscolhida == 3)
                ImageUtils.deleteImage("C:\\SysDelizLocal\\local_files\\qrCode_" + ordemCompra.getCodcli().concat(ordemCompra.getFatura()) + ".png");
            new PrintReportPreview().showReport(print);

        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");

        } catch (SQLException ex) {
            ex.printStackTrace();
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }

    private void makeColumns() {

        tblOrdens.setEditable(true);

        // <editor-fold defaultstate="collapsed" desc="Coluna ações">
        clnAcoes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GestaoCompra00X, GestaoCompra00X>, ObservableValue<GestaoCompra00X>>() {
            @Override
            public ObservableValue<GestaoCompra00X> call(TableColumn.CellDataFeatures<GestaoCompra00X, GestaoCompra00X> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnAcoes.setComparator(new Comparator<GestaoCompra00X>() {
            @Override
            public int compare(GestaoCompra00X p1, GestaoCompra00X p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnAcoes.setCellFactory(new Callback<TableColumn<GestaoCompra00X, GestaoCompra00X>, TableCell<GestaoCompra00X, GestaoCompra00X>>() {
            @Override
            public TableCell<GestaoCompra00X, GestaoCompra00X> call(TableColumn<GestaoCompra00X, GestaoCompra00X> btnCol) {
                return new TableCell<GestaoCompra00X, GestaoCompra00X>() {
                    final ImageView btnProgReenvioRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/alarm (1).png").toExternalForm()));
                    final ImageView btnEnviarRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/send (1).png").toExternalForm()));
                    final ImageView btnObservacaoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/info register (1).png").toExternalForm()));
                    final ImageView btnRecebimentoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/water transport (1).png").toExternalForm()));
                    final ImageView btnImprimirRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/print (1).png").toExternalForm()));
                    final Button btnProgReenvioRow = new Button();
                    final Button btnEnviarRow = new Button();
                    final Button btnObservacaoRow = new Button();
                    final Button btnRecebimentoRow = new Button();
                    final Button btnImprimirRow = new Button();
                    final Button btnCadastrarLote = FormButton.create(btn -> {
                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.addStyle("xs").addStyle("warning");
                        btn.tooltip("Alterar Nº Lote Original");
                        btn.icon(sysdeliz2.utils.sys.ImageUtils.getIcon(sysdeliz2.utils.sys.ImageUtils.Icon.EDIT, sysdeliz2.utils.sys.ImageUtils.IconSize._16));
                    });
                    final Button btnVizualizarMrp = FormButton.create(btn -> {
                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.addStyle("xs").addStyle("success");
                        btn.tooltip("Vizualizar MRP");
                        btn.icon(sysdeliz2.utils.sys.ImageUtils.getIcon(sysdeliz2.utils.sys.ImageUtils.Icon.ANALISE, sysdeliz2.utils.sys.ImageUtils.IconSize._16));
                    });
                    final HBox boxButtonsRow = new HBox(3);

                    final Tooltip tipBtnProgReenvioRow = new Tooltip("Agendar Reenvio de Ordem de Compra");
                    final Tooltip tipBtnEnviarRow = new Tooltip("Enviar Ordem de Compra");
                    final Tooltip tipBtnObservacaoRow = new Tooltip("Observações Ordem de Compra");
                    final Tooltip tipBtnRecebimentoRow = new Tooltip("Observações de Recebimento para Ordem de Compra");
                    final Tooltip tipBtnImprimirRow = new Tooltip("Imprimir Espelho de Ordem de Compra");

                    {
                        btnProgReenvioRow.setGraphic(btnProgReenvioRowIcon);
                        btnProgReenvioRow.setTooltip(tipBtnProgReenvioRow);
                        btnProgReenvioRow.setText("Agendar Reenvio");
                        btnProgReenvioRow.getStyleClass().add("primary");
                        btnProgReenvioRow.getStyleClass().add("xs");
                        btnProgReenvioRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnEnviarRow.setGraphic(btnEnviarRowIcon);
                        btnEnviarRow.setTooltip(tipBtnEnviarRow);
                        btnEnviarRow.setText("Enviar Ordem de Compra");
                        btnEnviarRow.getStyleClass().add("dark");
                        btnEnviarRow.getStyleClass().add("xs");
                        btnEnviarRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnObservacaoRow.setGraphic(btnObservacaoRowIcon);
                        btnObservacaoRow.setTooltip(tipBtnObservacaoRow);
                        btnObservacaoRow.setText("Observações Ordem de Compra");
                        btnObservacaoRow.getStyleClass().add("info");
                        btnObservacaoRow.getStyleClass().add("xs");
                        btnObservacaoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnRecebimentoRow.setGraphic(btnRecebimentoRowIcon);
                        btnRecebimentoRow.setTooltip(tipBtnRecebimentoRow);
                        btnRecebimentoRow.setText("Observações Recebimento Ordem de Compra");
                        btnRecebimentoRow.getStyleClass().add("warning");
                        btnRecebimentoRow.getStyleClass().add("xs");
                        btnRecebimentoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);

                        btnImprimirRow.setGraphic(btnImprimirRowIcon);
                        btnImprimirRow.setTooltip(tipBtnImprimirRow);
                        btnImprimirRow.setText("Imprimir Espelho Ordem de Compra");
                        btnImprimirRow.getStyleClass().add("danger");
                        btnImprimirRow.getStyleClass().add("xs");
                        btnImprimirRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }

                    @Override
                    public void updateItem(GestaoCompra00X selectedRow, boolean empty) {
                        super.updateItem(selectedRow, empty);
                        if (selectedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnRecebimentoRow, btnObservacaoRow, btnProgReenvioRow, btnEnviarRow, btnImprimirRow, btnCadastrarLote, btnVizualizarMrp);
                            setGraphic(boxButtonsRow);

                            btnProgReenvioRow.setOnAction(event -> agendarReenvioOrdemCompra(selectedRow));
                            btnEnviarRow.setOnAction(event -> sendSelectedOrdemCompra(selectedRow));
                            btnObservacaoRow.setOnAction(event -> observacaoOrdemCompra(selectedRow));
                            btnRecebimentoRow.setOnAction(event -> observacaoRecebimentoOrdemCompra(selectedRow));
                            btnImprimirRow.setOnAction(event -> imprimirEspelhoOrdemCompra(selectedRow));
                            btnCadastrarLote.setOnAction(event -> janelaCadastroNumeroLote(selectedRow));
                            btnVizualizarMrp.setOnAction(event -> janelaVizualizacaoMrp(selectedRow));
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }

                };
            }

        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna select">
        clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnSelected.setCellValueFactory((cellData) -> {
            GestaoCompra00X cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> {
                marcarItensOc(cellValue, newValue);
            });
            return property;
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pintura das linhas">
        tblOrdens.setRowFactory(tv -> new TableRow<GestaoCompra00X>() {
            @Override
            protected void updateItem(GestaoCompra00X item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null) {
                    clearStyle();
                } else if (item.getStatus().equals("Y")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatus().equals("G")) {
                    clearStyle();
                    getStyleClass().add("table-row-success");
                } else if (item.getStatus().equals("R")) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else {
                    clearStyle();
                    getStyleClass().add("table-row-secundary");
                }
            }

            private void clearStyle() {
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-secundary");
                getStyleClass().remove("table-row-success");
            }

        });

        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Pintura Coluna Status Envio">
        clnStatusEnvio.setCellFactory((TableColumn<GestaoCompra00X, String> param) -> {
            TableCell cell = new TableCell<GestaoCompra00X, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    if (item != null && !empty) {
                        setText(getString());
                        setGraphic(null);
                        if (item.equals("B")) {
                            getStyleClass().add("table-row-primary");
                        } else if (item.equals("E")) {
                            getStyleClass().add("table-row-info");
                        }
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }

                private void clearStyle() {
                    getStyleClass().remove("table-row-primary");
                    getStyleClass().remove("table-row-info");
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Data Entrega">
        Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>> cellFactoryDtEntrega
                = new Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnDate();
            }
        };
        clnDtEntrega.setCellFactory(cellFactoryDtEntrega);
        clnDtEntrega.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            LocalDate dataEntregaAnterior = selectObject.getDtEntrega();
            LocalDate dataEntregaNova = event.getNewValue();
            selectObject.setDtEntrega(event.getNewValue());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateDatasCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateDatasTecelagem(selectObject);
                }
                DAOFactory.getSdAlteracaoOc001DAO().save(new SdAlteracaoOc001(
                        selectObject.getNumero(),
                        selectObject.getCodigo(),
                        selectObject.getCor(),
                        "DE",
                        "NR",
                        selectObject.getDeposito(),
                        selectObject.getOrdem(),
                        LocalDateTime.now(),
                        dataEntregaAnterior,
                        dataEntregaNova));
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Data de entrega alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Data de entrega alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Data Emissao">
        Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>> cellFactoryDtEmissao
                = new Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnDate();
            }
        };
        clnDtEmissao.setCellFactory(cellFactoryDtEmissao);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Data Fatura">
        Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>> cellFactoryDtFatura
                = new Callback<TableColumn<GestaoCompra00X, LocalDate>, TableCell<GestaoCompra00X, LocalDate>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnDate();
            }
        };
        clnDtFatura.setCellFactory(cellFactoryDtFatura);
        clnDtFatura.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setDtFatura(event.getNewValue());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateDatasCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateDatasTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Data de faturamento alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Data de faturamento alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna E-mail">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryEmail
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnEmail.setCellFactory(cellFactoryEmail);
        clnEmail.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setEmail(event.getNewValue());
            registerFind.stream().
                    filter(oc -> Boolean.logicalAnd(
                            oc.getNumero().equals(selectObject.getNumero()),
                            oc.getCodcli().equals(selectObject.getCodcli()))).
                    forEach(ocs -> ocs.setEmail(event.getNewValue()));
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateEmailCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateEmailTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Alterado email de recebimento do fornecedor no gestáo de compras para  " + selectObject.getEmail());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Email do fornecedor registrado com sucesso!");
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Pedido Fornecedor">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryPedFornecedor
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnPedFornecedor.setCellFactory(cellFactoryPedFornecedor);
        clnPedFornecedor.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setPedFornecedor(event.getNewValue());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updatePedidoCompra(selectObject);
                    if (!event.getNewValue().isEmpty()) {
                        DAOFactory.getGestaoCompra00XDAO().updatePedidoCompraTi(selectObject);
                    }
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updatePedidoTecelagem(selectObject);
                    if (!event.getNewValue().isEmpty()) {
                        DAOFactory.getGestaoCompra00XDAO().updatePedidoTecelagemTi(selectObject);
                    }
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Pedido do fornecedor alterado na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Pedido do fornecedor alterado na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Codigo Fornecedor">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryCodcli
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnStringCodcli();
            }
        };
        clnCodcli.setCellFactory(cellFactoryCodcli);
        clnCodcli.setOnEditCommit((event) -> {
            try {
                GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();

                Cliente fornecedor = DAOFactory.getClienteDAO().getByCodcli(event.getNewValue());
                if (fornecedor == null) {
                    GUIUtils.showMessage("Código de fornecedor não encontrado ou inativo, verifique o cadastro no TI.", Alert.AlertType.WARNING);
                } else {
                    selectObject.setCodcli(fornecedor.getCodcli());
                    selectObject.setNome(fornecedor.getNome());
                    selectObject.setEmail(fornecedor.getEmail());
                    registerFind.stream().
                            filter(oc -> Boolean.logicalAnd(
                                    oc.getNumero().equals(selectObject.getNumero()),
                                    oc.getCodcli().equals(selectObject.getCodcli()))).
                            forEach(ocs -> {
                                ocs.setCodcli(fornecedor.getCodcli());
                                ocs.setNome(fornecedor.getNome());
                                ocs.setEmail(fornecedor.getEmail());
                            });

                    DAOFactory.getGestaoCompra00XDAO().updateCodforCompraTi(selectObject);
                    Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                            Log.TipoAcao.EDITAR, "Gestão Compras",
                            selectObject.getNumero(),
                            "Alterado fornecedor na OC: " + selectObject.getNumero() + " para o código " + selectObject.getCodcli());
                    DAOFactory.getLogDAO().saveLog(logAlteracao);
                    DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                    GUIUtils.showMessageNotification("Alteração de O.C.", "Alterado fornecedor na OC: " + selectObject.getNumero() + " para o código " + selectObject.getCodcli());
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }

        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Fatura">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryFatura
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnFatura.setCellFactory(cellFactoryFatura);
        clnFatura.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setFatura(event.getNewValue());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateNfCompra(selectObject);
                    if (!event.getNewValue().isEmpty()) {
                        DAOFactory.getGestaoCompra00XDAO().updateNfCompraTi(selectObject);
                    }
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateNfTecelagem(selectObject);
                    if (!event.getNewValue().isEmpty()) {
                        DAOFactory.getGestaoCompra00XDAO().updateNfTecelagemTi(selectObject);
                    }
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Nota fiscal alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Nota fiscal alterada na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Deposito">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryDeposito
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnDeposito.setCellFactory(cellFactoryDeposito);
        clnDeposito.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setDeposito(event.getNewValue());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateDepositoItemCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateDepositoTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Depósito alterado na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);
                GUIUtils.showMessageNotification("Alteração de O.C.", "Depósito alterado na OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Qtde">
        Callback<TableColumn<GestaoCompra00X, Double>, TableCell<GestaoCompra00X, Double>> cellFactoryQtde
                = new Callback<TableColumn<GestaoCompra00X, Double>, TableCell<GestaoCompra00X, Double>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnQtde();
            }
        };
        clnQtde.setCellFactory(cellFactoryQtde);
        clnQtde.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setQtde(event.getNewValue());
            selectObject.setValor(selectObject.getQtde() * selectObject.getCusto());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateQtdeItemCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateQtdeItemTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Alterada a quantidade da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);

                GUIUtils.showMessageNotification("Gestão de Compras",
                        "Alterada a quantidade da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });

        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Custo">
        Callback<TableColumn<GestaoCompra00X, Double>, TableCell<GestaoCompra00X, Double>> cellFactoryCusto
                = new Callback<TableColumn<GestaoCompra00X, Double>, TableCell<GestaoCompra00X, Double>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnCusto();
            }
        };
        clnCusto.setCellFactory(cellFactoryCusto);
        clnCusto.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setCusto(event.getNewValue());
            selectObject.setValor(selectObject.getQtde() * selectObject.getCusto());
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateCustoItemCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateCustoItemTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Alterado o custo da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);

                GUIUtils.showMessageNotification("Gestão de Compras",
                        "Alterado o custo da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Valor Total">
        clnValor.setCellFactory((TableColumn<GestaoCompra00X, Double> param) -> {
            TableCell cell = new TableCell<GestaoCompra00X, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Coluna Condição">
        Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>> cellFactoryCondicao
                = new Callback<TableColumn<GestaoCompra00X, String>, TableCell<GestaoCompra00X, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnCondicao.setCellFactory(cellFactoryCondicao);
        clnCondicao.setOnEditCommit((event) -> {
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            selectObject.setCondicao(event.getNewValue());
            registerFind.stream().
                    filter(oc -> Boolean.logicalAnd(
                            oc.getNumero().equals(selectObject.getNumero()),
                            oc.getCodcli().equals(selectObject.getCodcli()))).
                    forEach(ocs -> ocs.setCondicao(event.getNewValue()));
            try {
                if (selectObject.getTipo().equals("MAT")) {
                    DAOFactory.getGestaoCompra00XDAO().updateCondicaoCompra(selectObject);
                } else {
                    DAOFactory.getGestaoCompra00XDAO().updateCondicaoTecelagem(selectObject);
                }
                Log logAlteracao = new Log(SceneMainController.getAcesso().getUsuario(),
                        Log.TipoAcao.EDITAR, "Gestão Compras",
                        selectObject.getNumero(),
                        "Alterada a condição de pagamento da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
                DAOFactory.getLogDAO().saveLog(logAlteracao);
                DAOFactory.getLogDAO().saveLogTI(logAlteracao);

                GUIUtils.showMessageNotification("Gestão de Compras",
                        "Alterada a condição de pagamento da OC: " + selectObject.getNumero() + " material: " + selectObject.getCodigo() + " na cor: " + selectObject.getCor() + " de " + event.getOldValue() + " para " + event.getNewValue());
            } catch (SQLException ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
            }
        });

        //</editor-fold>
    }

    private void janelaVizualizacaoMrp(GestaoCompra00X selectedRow) {
        new Fragment().show(frag -> {
            frag.size(770.0, 750.0);
            frag.title.setText("Vizualização MRP");

            final FormFieldText ocField = FormFieldText.create(field -> {
                field.title("O.C.");
                field.value.setValue(selectedRow.getNumero());
                field.editable.set(false);
            });
            final FormFieldText cnpjField = FormFieldText.create(field -> {
                field.title("CNPJ");
                field.value.setValue(selectedRow.getCnpj());
                field.editable.set(false);
                field.width(200);
            });
            final FormFieldText fornField = FormFieldText.create(field -> {
                field.title("Nome Fornecedor");
                field.value.setValue(selectedRow.getNome());
                field.width(350);
                field.editable.set(false);
            });

            final FormTableView<VSdMrpOc> tblMrp = FormTableView.create(VSdMrpOc.class, table -> {
                table.title("MRP");
                table.expanded();
                List<VSdMrpOc> listMrp = (List<VSdMrpOc>) new FluentDao().selectFrom(VSdMrpOc.class).where(it -> it.equal("ordcompra", selectedRow.getNumero())).resultList();
                if (listMrp != null && listMrp.size() > 0) {
                    table.items.setValue(FXCollections.observableArrayList(listMrp.stream().sorted(Comparator.comparing(VSdMrpOc::getNumero).thenComparing(VSdMrpOc::getPeriodo)).collect(Collectors.toList())));
                }
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Número");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Lote");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Dt. Lote");
                            cln.width(110);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtlote())));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Consumo");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumo()));
                        }).build(), /*Número*/
                        FormTableColumn.create(cln -> {
                            cln.title("Un");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpOc, VSdMrpOc>, ObservableValue<VSdMrpOc>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                        }).build() /*Número*/
                );
            });

            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.add(FormBox.create(boxOc -> {
                    boxOc.horizontal();
                    boxOc.add(ocField.build(), cnpjField.build(), fornField.build());
                }));
                principal.add(tblMrp.build());
            }));
        });
    }

    private void janelaCadastroNumeroLote(GestaoCompra00X selectedRow) {
        new Fragment().show(frag -> {
            frag.size(250.0, 100.0);
            frag.title.setText("Cadastro Nº Lote");

            FormButton confirmBtn = FormButton.create(btn -> {
                btn.addStyle("success");
                btn.icon(sysdeliz2.utils.sys.ImageUtils.getIcon(sysdeliz2.utils.sys.ImageUtils.Icon.CONFIRMAR, sysdeliz2.utils.sys.ImageUtils.IconSize._24));
                btn.title("Confirmar");

            });
            final FormFieldSingleFind<TabPrz> periodoFilter = FormFieldSingleFind.create(TabPrz.class, field -> {
                field.title("Lote");
                field.defaults.add(new DefaultFilter("P", "tipo"));
                if (selectedRow.getPeriodo() != null) {
                    field.value.set(new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", selectedRow.getPeriodo())).singleResult());
                }
            });
            frag.setOnKeyReleased(event -> {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    confirmBtn.requestFocus();
                    confirmBtn.fire();
                }
            });
            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.horizontal();
                principal.add(periodoFilter.build());
                confirmBtn.setOnAction(evt -> {
                    try {
                        selectedRow.setPeriodoOrig(periodoFilter.value.get() == null ? "" : periodoFilter.value.getValue().getPrazo());
                        selectedRow.setDtLoteOrig(periodoFilter.value.get() == null ? null : StringUtils.toShortDateFormat(periodoFilter.value.getValue().getDtInicio()));
                        DAOFactory.getGestaoCompra00XDAO().updateNumeroLote(selectedRow);
                        MessageBox.create(message -> {
                            message.message("Lote Atualizado com Sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        tblOrdens.refresh();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    frag.close();
                });
            }));
            frag.buttonsBox.getChildren().add(confirmBtn);
        });
    }

    @FXML
    private void btnEnviarTodosOnAction(ActionEvent event) {

        registerFind.stream()
                .filter(ordemCompraStream -> ordemCompraStream.isSelected())
                .map(ordemCompraStream -> ordemCompraStream.getNumero())
                .distinct()
                .forEach(numeroOrdem -> {
                    itensAConfirmarToSend.clear();
                    itensAtrasadosToSend.clear();
                    to_emails.clear();

                    registerFind.stream().
                            filter(oc -> Boolean.logicalAnd(
                                    Boolean.logicalAnd(
                                            oc.getNumero().equals(numeroOrdem),
                                            oc.getCodcli().equals(numeroOrdem)),
                                    oc.getStatus().equals("Y"))).
                            forEach(ordemCompraStream -> {
                                itensAConfirmarToSend.add(ordemCompraStream);
                                if (!to_emails.contains(ordemCompraStream.getEmail())) {
                                    to_emails.add(ordemCompraStream.getEmail());
                                }
                            });

                    registerFind.stream().
                            filter(oc -> Boolean.logicalAnd(
                                    Boolean.logicalAnd(
                                            oc.getNumero().equals(numeroOrdem),
                                            oc.getCodcli().equals(numeroOrdem)),
                                    oc.getStatus().equals("R"))).
                            forEach(ordemCompraStream -> {
                                itensAtrasadosToSend.add(ordemCompraStream);
                                if (!to_emails.contains(ordemCompraStream.getEmail())) {
                                    to_emails.add(ordemCompraStream.getEmail());
                                }
                            });
                    try {
                        if (itensAConfirmarToSend.size() > 0) {
                            MailUtils.sendOcAConfirmar(to_emails, itensAConfirmarToSend);
                        }
                        if (itensAtrasadosToSend.size() > 0) {
                            MailUtils.sendOcAtrasada(to_emails, itensAtrasadosToSend);
                        }

                        itensAConfirmarToSend.forEach(ordemMaterial -> {
                            try {
                                DAOFactory.getSdLogEmailOc001DAO().save(new SdLogEmailOc001(
                                        ordemMaterial.getCodcli(),
                                        ordemMaterial.getNumero(),
                                        ordemMaterial.getCodigo(),
                                        ordemMaterial.getCor(),
                                        null,
                                        ordemMaterial.getDtFaturaAsString(),
                                        ordemMaterial.getEmail()));
                                ordemMaterial.setStatus("E");
                            } catch (SQLException ex) {
                                SysLogger.addFileLog("Não pode salvar o log de envio de e-mail para o fornecedor " + ordemMaterial.getCodcli() +
                                        " com o número " + ordemMaterial.getNumero(), "Compras::Envio de E-mail ao Fornecedor");
                                ex.printStackTrace();
                                GUIUtils.showException(ex);
                            }
                        });

                        itensAtrasadosToSend.forEach(ordemMaterial -> {
                            try {
                                DAOFactory.getSdLogEmailOc001DAO().save(new SdLogEmailOc001(
                                        ordemMaterial.getCodcli(),
                                        ordemMaterial.getNumero(),
                                        ordemMaterial.getCodigo(),
                                        ordemMaterial.getCor(),
                                        null,
                                        ordemMaterial.getDtFaturaAsString(),
                                        ordemMaterial.getEmail()));
                                ordemMaterial.setStatus("E");
                            } catch (SQLException ex) {
                                SysLogger.addFileLog("Não pode salvar o log de envio de e-mail para o fornecedor " + ordemMaterial.getCodcli() +
                                        " com o número " + ordemMaterial.getNumero(), "Compras::Envio de E-mail ao Fornecedor");
                                ex.printStackTrace();
                                GUIUtils.showException(ex);
                            }
                        });

                    } catch (UnsupportedEncodingException ex) {
                        SysLogger.addFileLog("Erro no envio do e-mail para " + numeroOrdem + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
                        Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    } catch (MessagingException ex) {
                        SysLogger.addFileLog("Erro no envio do e-mail para " + numeroOrdem + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
                        Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    } catch (ParseException ex) {
                        SysLogger.addFileLog("Erro no envio do e-mail para " + numeroOrdem + " - ERROR: \n" + ex.getMessage(), "Compras::Envio de E-mail ao Fornecedor");
                        Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }

                });
        tblOrdens.refresh();
        GUIUtils.showMessageNotification("Envio de O.C.", "O.C. enviada ao fornecedor.");
    }

    @FXML
    private void btnCarregarOcsOnAction(ActionEvent event) {

        String whereData = "";
        if (dpickerInicio.getValue() != null || dpickerFim.getValue() != null) {
            if ((dpickerInicio.getValue() != null && dpickerFim.getValue() == null)
                    || (dpickerInicio.getValue() == null && dpickerFim.getValue() != null)) {
                GUIUtils.showMessage("Você deve selecionar uma data de início e fim para a consulta.", Alert.AlertType.INFORMATION);
                return;
            } else if (dpickerInicio.getValue().isAfter(dpickerFim.getValue())) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereData += "and dt_fatura between to_date('" + dpickerInicio.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dpickerFim.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }

        String wheresValues = "";
        String filterStatusValue = ""
                .concat(toggleGreen.switchedOnProperty().get() ? "'G'," : "")
                .concat(toggleGrey.switchedOnProperty().get() ? "'S'," : "")
                .concat(toggleRed.switchedOnProperty().get() ? "'R'," : "")
                .concat(toggleYellow.switchedOnProperty().get() ? "'Y'," : "");
        filterStatusValue = filterStatusValue.length() > 0 ? "and status in (" + filterStatusValue.substring(0, filterStatusValue.length() - 1) + ")\n" : "";
        String filterStatusEnvioValue = ""
                .concat(toggleBlue.switchedOnProperty().get() ? "'B'," : "")
                .concat(toggleLight.switchedOnProperty().get() ? "'E'," : "");
        filterStatusEnvioValue = filterStatusEnvioValue.length() > 0 ? "and status_envio in (" + filterStatusEnvioValue.substring(0, filterStatusEnvioValue.length() - 1) + ")\n" : "";
        wheresValues += filterStatusValue;
        wheresValues += filterStatusEnvioValue;
        wheresValues += tboxFornecedor.getLength() > 0 ? "and (codcli = '" + tboxFornecedor.getText() + "' or nome like '%" + tboxFornecedor.getText() + "%')\n" : "";
        wheresValues += tboxCor.getLength() > 0 ? "and cor in (" + tboxCor.getText() + ")\n" : "";
        wheresValues += tboxGrupoComprador.getLength() > 0 ? "and grupo in (" + tboxGrupoComprador.getText() + ")\n" : "";
        wheresValues += tboxMaterial.getLength() > 0 ? "and (codigo = '" + tboxMaterial.getText() + "' or descricao like '%" + tboxMaterial.getText() + "%')\n" : "";
        wheresValues += tboxOrdemCompra.getLength() > 0 ? "and numero in (" + tboxOrdemCompra.getText() + ")\n" : "";
        wheresValues += tboxNotaFiscal.getLength() > 0 ? "and nf_fornecedor = '" + tboxNotaFiscal.getText() + "'\n" : "";
        wheresValues += tboxLote.getLength() > 0 ? "and periodo = '" + tboxLote.getText() + "'\n" : "";
        wheresValues += whereData.length() > 0 ? whereData : "";

        try {
            ObservableList<GestaoCompra00X> fromFilter = DAOFactory.getGestaoCompra00XDAO().getFromFilter(wheresValues);
            if (tboxDeposito.getLength() > 0) {
                registerFind.set(FXCollections.observableArrayList(fromFilter.stream().filter(it -> it.getDeposito().equals(tboxDeposito.getText())).collect(Collectors.toList())));
            } else
                registerFind.set(fromFilter);
            if (toggleOrange.switchedOnProperty().get()) {
                filtrarLotesAtrasados();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            GUIUtils.showException(ex);
        }
    }

    private void filtrarLotesAtrasados() {
        registerFind.removeIf(oc -> {
            TabPrz lote = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", oc.getPeriodo())).singleResult();
            return lote == null || lote.getDtInicio().minusDays(3L).isAfter(oc.getDtEntrega());
        });
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxCor.clear();
        tboxNotaFiscal.clear();
        tboxGrupoComprador.clear();
        tboxMaterial.clear();
        tboxFornecedor.clear();
        tboxOrdemCompra.clear();
        dpickerFim.setValue(null);
        dpickerInicio.setValue(null);
        tboxDeposito.clear();
        tboxLote.clear();

        toggleRed.switchedOnProperty().set(false);
        toggleYellow.switchedOnProperty().set(false);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(false);
        toggleGrey.switchedOnProperty().set(false);

        registerFind.clear();

    }

    @FXML
    public void btnExportarExcelOnAction(ActionEvent actionEvent) {

        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        if (filePath == null || filePath.equals(""))
            return;
        ExportUtils.ExcelExportMode excelExportMode = new ExportUtils().excel(filePath);
        try {
            excelExportMode.addSheet("Ordens de Compra", builder -> {
                builder.loadColunasClasse(GestaoCompra00X.class);
                builder.createHeader(builder.workbook, 0, true);
                int rowIndex = 1;
                for (GestaoCompra00X item : tblOrdens.getItems()) {
                    XSSFCellStyle style = (XSSFCellStyle) builder.workbook.createCellStyle();
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    style.setBorderBottom(BorderStyle.THIN);
                    style.setBorderTop(BorderStyle.THIN);
                    style.setBorderRight(BorderStyle.THIN);
                    style.setBorderLeft(BorderStyle.THIN);
                    if (item.getStatus().equals("Y"))
                        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
                    else if (item.getStatus().equals("G"))
                        style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
                    else if (item.getStatus().equals("R"))
                        style.setFillForegroundColor(IndexedColors.CORAL.getIndex());
                    else
                        style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
                    builder.drawRow(style, item, rowIndex++);
                }
                builder.andThen();
            });
            excelExportMode.addSheet("MRP", builder -> {
                Sheet sheet = builder.sheet;
                builder.loadColunasClasse(VSdMrpOc.class);
                XSSFCellStyle style = (XSSFCellStyle) builder.workbook.createCellStyle();
                style.setBorderBottom(BorderStyle.THIN);
                style.setBorderTop(BorderStyle.THIN);
                style.setBorderRight(BorderStyle.THIN);
                style.setBorderLeft(BorderStyle.THIN);
                int rowIndex = 0;
                builder.createHeader(builder.workbook, rowIndex++, true);
                for (String numero : tblOrdens.getItems().stream().map(GestaoCompra00X::getNumero).distinct().sorted(String::compareTo).collect(Collectors.toList())) {
                    List<VSdMrpOc> listMrp = (List<VSdMrpOc>) new FluentDao().selectFrom(VSdMrpOc.class).where(it -> it.equal("ordcompra", numero))
                            .orderBy(Arrays.asList(new Ordenacao("numero"), new Ordenacao("ordcompra"), new Ordenacao("periodo"))).resultList();
                    if (listMrp == null || listMrp.size() == 0) {
                        continue;
                    }
                    for (VSdMrpOc mrp : listMrp) {
                        Row row = sheet.createRow(rowIndex++);
                        builder.cellFormatValue(row, 0, mrp.getNumero(), "string");
                        builder.cellFormatValue(row, 1, mrp.getOrdcompra(), "string");
                        builder.cellFormatValue(row, 2, mrp.getCodigo(), "string");
                        builder.cellFormatValue(row, 3, mrp.getCor(), "string");
                        builder.cellFormatValue(row, 4, mrp.getPeriodo(), "string");
                        builder.cellFormatValue(row, 5, StringUtils.toShortDateFormat(mrp.getDtlote()), "string");
                        builder.cellFormatValue(row, 6, mrp.getConsumo().toString(), "double");
                        builder.cellFormatValue(row, 7, mrp.getUnidade(), "string");
                    }
                }
                builder.andThen();
            });
            excelExportMode.export();
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }

        MessageBox.create(message -> {
            message.message("Excel exportado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });

    }

    @FXML
    public void btnForcarAttLoteOnAction(ActionEvent actionEvent) {
        if (tblOrdens.getItems().size() > 0) {
            try {
                new NativeDAO().runNativeQueryUpdate("" +
                        "BEGIN\n" +
                        "  DBMS_SCHEDULER.RUN_JOB(\n" +
                        "    JOB_NAME            => 'J_ATUALIZA_OC_PERIODO',\n" +
                        "    USE_CURRENT_SESSION => FALSE);\n" +
                        "END;");

            } catch (SQLException e) {
                e.printStackTrace();
                MessageBox.create(message -> {
                    message.message("Houve um problema ao atualizar as OC's. Entre em contato com a TI.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showAndWait();
                });
                return;
            }

            new RunAsyncWithOverlay(this.stackPane).exec(task -> {
                while (true) {
                    try {
                        if (!getStatusJob()) break;
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    btnCarregarOcsOnAction(actionEvent);
                }
            });

        }

    }

    private boolean getStatusJob() throws SQLException {
        List<Object> objects = new NativeDAO().runNativeQuery("select decode(state,'SCHEDULED','P','RUNNING','E', 'SUCCEEDED', 'A') estado\n" +
                "                  from user_scheduler_jobs\n" +
                "                 where job_name = 'J_ATUALIZA_OC_PERIODO'");

        if (objects.size() > 0) {
            return ((Map) objects.get(0)).get("ESTADO").equals("E");
        }
        return false;
    }

    private class EditingCellClnString extends TableCell<GestaoCompra00X, String> {

        private TextField textField;

        public EditingCellClnString() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    private class EditingCellClnStringCodcli extends TableCell<GestaoCompra00X, String> {

        private TextField textField;
        private Tooltip toolTextField = new Tooltip("Pressione F4 para consultar fornecedores.");

        public EditingCellClnStringCodcli() {
        }

        @Override
        public void startEdit() {
            super.startEdit();
            GestaoCompra00X selectObject = tblOrdens.getSelectionModel().getSelectedItem();
            if (selectObject.getTipo().equals("MAT")) {
                if (textField == null) {

                    createTextField();
                }

                setGraphic(textField);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                textField.selectAll();

            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setTooltip(toolTextField);
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.F4) {
                        FilterFornecedorController ctrolFilter;
                        try {
                            ctrolFilter = new FilterFornecedorController(
                                    Modals.FXMLWindow.FilterFornecedor,
                                    textField.getText(),
                                    EnumsController.ResultTypeFilter.SINGLE_RESULT);

                            if (!ctrolFilter.returnValue.isEmpty()) {
                                textField.setText(ctrolFilter.returnValue.replace("'", ""));
                                commitEdit(textField.getText());
                            } else {
                                cancelEdit();
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(SceneComprasOrdensDeCompraController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                            cancelEdit();
                        }
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    private class EditingCellClnDate extends TableCell<GestaoCompra00X, LocalDate> {

        private DatePicker datePicker;

        private EditingCellClnDate() {
        }

        @Override
        public void startEdit() {
            if (!isEmpty()) {
                super.startEdit();
                createDatePicker();
                setText(null);
                setGraphic(datePicker);
            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(getDate().toString());
            setGraphic(null);
        }

        @Override
        public void updateItem(LocalDate item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (datePicker != null) {
                        datePicker.setValue(getDate());
                    }
                    setText(null);
                    setGraphic(datePicker);
                } else {
                    setText(getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                    setGraphic(null);
                }
            }
        }

        private void createDatePicker() {
            datePicker = new DatePicker(getDate());
            datePicker.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            datePicker.setOnAction((e) -> {
                System.out.println("Committed: " + datePicker.getValue().toString());
                commitEdit(datePicker.getValue());
            });
//            datePicker.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//                if (!newValue) {
//                    commitEdit(Date.from(datePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
//                }
//            });
        }

        private LocalDate getDate() {
            return getItem() == null ? LocalDate.now() : getItem();
        }
    }

    private class EditingCellClnInteger extends TableCell<GestaoCompra00X, Integer> {

        private TextField textField;
        NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);

        public EditingCellClnInteger() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(formatValueCell.format(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(formatValueCell.format(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(formatValueCell.format(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private Integer getString() {
            return getItem() == null ? 0 : getItem();
        }
    }

    private class EditingCellClnDouble extends TableCell<GestaoCompra00X, Double> {

        private TextField textField;
        NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);

        public EditingCellClnDouble() {
            formatValueCell.setMaximumFractionDigits(2);
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(formatValueCell.format(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(formatValueCell.format(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(formatValueCell.format(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText().replace(",", ".").replace("R$ ", "")));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private Double getString() {
            return getItem() == null ? 0.0 : getItem();
        }
    }

    private class EditingCellClnCusto extends TableCell<GestaoCompra00X, Double> {

        private TextField textField;
        NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);

        public EditingCellClnCusto() {
            formatValueCell.setMaximumFractionDigits(5);
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(formatValueCell.format(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(formatValueCell.format(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(formatValueCell.format(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText().replace(",", ".").replace("R$ ", "")));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private Double getString() {
            return getItem() == null ? 0.0 : getItem();
        }
    }

    private class EditingCellClnQtde extends TableCell<GestaoCompra00X, Double> {

        private TextField textField;
        NumberFormat formatValueCell = NumberFormat.getNumberInstance(BRAZIL);

        public EditingCellClnQtde() {
            formatValueCell.setMaximumFractionDigits(3);
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(formatValueCell.format(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(formatValueCell.format(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(formatValueCell.format(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText().replace(",", ".")));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private Double getString() {
            return getItem() == null ? 0.0 : getItem();
        }
    }
}
