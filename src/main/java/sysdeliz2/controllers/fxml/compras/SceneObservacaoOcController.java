/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.compras;

import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdObservacaoOc001;
import sysdeliz2.models.properties.GestaoCompra00X;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneObservacaoOcController extends Modals implements Initializable {

    @FXML
    private Button btnFechar;
    @FXML
    private Button btnNovaObservacao;
    @FXML
    private Button btnBack;
    @FXML
    private Label lbRegister;
    @FXML
    private Button btnNext;
    @FXML
    private TextField tboxNumero;
    @FXML
    private TextField tboxMaterial;
    @FXML
    private TextField tboxCor;
    @FXML
    private TextField tboxUsuario;
    @FXML
    private TextField tboxDtObservacao;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private Button btnSalvar;

    private final ListProperty<SdObservacaoOc001> observacoesOc = new SimpleListProperty<>();
    private final IntegerProperty indexSelected = new SimpleIntegerProperty(-1);
    private final BooleanProperty editionMode = new SimpleBooleanProperty(false);
    private GestaoCompra00X selectedOc = null;
    private SdObservacaoOc001 newObservacao = null;

    public SceneObservacaoOcController(FXMLWindow file, GestaoCompra00X selectedOc) throws IOException {
        super(file);
        this.selectedOc = selectedOc;

        super.show(this);
    }

    private void bindData(SdObservacaoOc001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().bind(selected.numeroProperty());
            tboxMaterial.textProperty().bind(selected.codigoProperty());
            tboxCor.textProperty().bind(selected.corProperty());
            tboxUsuario.textProperty().bind(selected.usuarioProperty());
            tboxDtObservacao.textProperty().bind(selected.dtObservacaoProperty());
            tboxObservacao.textProperty().bind(selected.observacaoProperty());
        }
    }

    private void unbindData() {
        tboxNumero.textProperty().unbind();
        tboxMaterial.textProperty().unbind();
        tboxCor.textProperty().unbind();
        tboxUsuario.textProperty().unbind();
        tboxDtObservacao.textProperty().unbind();
        tboxObservacao.textProperty().unbind();
    }

    private void bindBirectionalData(SdObservacaoOc001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().bind(selected.numeroProperty());
            tboxMaterial.textProperty().bind(selected.codigoProperty());
            tboxCor.textProperty().bind(selected.corProperty());
            tboxUsuario.textProperty().bind(selected.usuarioProperty());
            tboxDtObservacao.textProperty().bind(selected.dtObservacaoProperty());
            tboxObservacao.textProperty().bindBidirectional(selected.observacaoProperty());
        }
    }

    private void unbindBirectionalData(SdObservacaoOc001 selected) {
        if (selected != null) {
            tboxNumero.textProperty().unbindBidirectional(selected.numeroProperty());
            tboxMaterial.textProperty().unbindBidirectional(selected.codigoProperty());
            tboxCor.textProperty().unbindBidirectional(selected.corProperty());
            tboxUsuario.textProperty().unbindBidirectional(selected.usuarioProperty());
            tboxDtObservacao.textProperty().unbindBidirectional(selected.dtObservacaoProperty());
            tboxObservacao.textProperty().unbindBidirectional(selected.observacaoProperty());
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbRegister.textProperty().bind(indexSelected.add(1).asString());
        btnBack.disableProperty().bind(indexSelected.lessThanOrEqualTo(0).or(observacoesOc.emptyProperty()).or(editionMode));
        btnNext.disableProperty().bind(indexSelected.greaterThanOrEqualTo(observacoesOc.sizeProperty().subtract(1)).or(observacoesOc.emptyProperty()).or(editionMode));
        btnSalvar.disableProperty().bind(editionMode.not());

        try {
            observacoesOc.set(DAOFactory.getSdObservacaoOc001DAO().getCrmOrdemCompraMaterial(selectedOc));
            if (observacoesOc.getSize() > 0) {
                indexSelected.set(indexSelected.add(1).get());
                unbindData();
                bindData(observacoesOc.get(indexSelected.get()));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SceneObservacaoOcController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnNovaObservacaoOnAction(ActionEvent event) {
        unbindData();
        editionMode.set(true);
        newObservacao = new SdObservacaoOc001(
                selectedOc.getNumero(),
                selectedOc.getCodigo(),
                selectedOc.getCor(),
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss")),
                SceneMainController.getAcesso().getUsuario(), "");
        bindBirectionalData(newObservacao);
        observacoesOc.add(newObservacao);
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        try {
            unbindBirectionalData(newObservacao);
            DAOFactory.getSdObservacaoOc001DAO().save(newObservacao);
            newObservacao = null;
            indexSelected.set(observacoesOc.getSize() - 1);
            bindData(observacoesOc.get(indexSelected.get()));
            editionMode.set(false);
            GUIUtils.showMessageNotification("Observação Ordem de Compra", "Observação ao material da O.C. cadastrada.");
        } catch (SQLException ex) {
            Logger.getLogger(SceneObservacaoOcController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnBackOnAction(ActionEvent event) {
        indexSelected.set(indexSelected.subtract(1).get());
        unbindData();
        bindData(observacoesOc.get(indexSelected.get()));
    }

    @FXML
    private void btnNextOnAction(ActionEvent event) {
        indexSelected.set(indexSelected.add(1).get());
        unbindData();
        bindData(observacoesOc.get(indexSelected.get()));
    }

}
