package sysdeliz2.controllers.fxml.expedicao;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.util.Callback;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.LocalLogger;

import javax.print.*;
import java.sql.SQLException;

public class PrintTagProduto extends WindowBase {
    
    private final FormFieldMultipleFind<Marca> marcaField = FormFieldMultipleFind.create(Marca.class).title("Marca").width(80.0).toUpper();
    private final FormFieldMultipleFind<Produto> produtoField = FormFieldMultipleFind.create(Produto.class).title("Produto").width(200.0).toUpper();
    private final FormFieldMultipleFind<Colecao> colecaoField = FormFieldMultipleFind.create(Colecao.class).title("Coleção").toUpper();
    private final FormTableView<VSdDadosProduto> produtosTable = FormTableView.create(VSdDadosProduto.class, formTableView -> {
        formTableView.expanded();
        formTableView.multipleSelection();
        formTableView.title("Produtos");
        formTableView.addColumn(FormTableColumn.create(formTableColumn -> {
            formTableColumn.title("Código");
            formTableColumn.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) features -> new ReadOnlyObjectWrapper(features.getValue().getCodigo()));
        }));
        formTableView.addColumn(FormTableColumn.create(formTableColumn -> {
            formTableColumn.title("Descrição");
            formTableColumn.width(300.0);
            formTableColumn.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDescricao()));
        }));
        formTableView.addColumn(FormTableColumn.create(formTableColumn -> {
            formTableColumn.title("Marca");
            formTableColumn.alignment(FormTableColumn.Alignment.CENTER);
            formTableColumn.width(100.0);
            formTableColumn.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) features -> new ReadOnlyObjectWrapper(features.getValue().getMarca()));
        }));
        formTableView.addColumn(FormTableColumn.create(formTableColumn -> {
            formTableColumn.title("Coleção");
            formTableColumn.width(150.0);
            formTableColumn.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) features -> new ReadOnlyObjectWrapper(features.getValue().getColecao()));
        }));
    });
    private final Button imprimirEtiquetaButton = FormButton.create(formButton -> {
        formButton.addStyle("warning");
        formButton.addStyle("lg");
        formButton.title("Imprimir Tag Mostruário");
        formButton.icon(new Image(getClass().getResource("/images/icons/buttons/print label (2).png").toExternalForm()));
        formButton.setAction(event -> {
            try {
                InputBox<Integer> inputMessage = InputBox.build(Integer.class, inputBox -> {
                    inputBox.type(MessageBox.TypeMessageBox.INPUT);
                    inputBox.message("Quantidade de etiquetas por produto:");
                });
                inputMessage.showAndWait();
                Integer qtdeEtiquetasProduto = inputMessage.value.get() == null ? 0 : inputMessage.value.getValue();
    
                String zplCodeTag = generateZplCode(qtdeEtiquetasProduto);
    
                if (zplCodeTag.length() > 0)
                    printZplString(zplCodeTag);
            }catch (NumberFormatException e){
                GUIUtils.showMessage("Digite um número válido.\nVocê digitou um valor não númerico no campo solicitado.", Alert.AlertType.WARNING);
            }
            
        });
    });
    
    private GenericDao<VSdDadosProduto> daoDadosProdutos = new GenericDaoImpl<>(VSdDadosProduto.class);
    private final ListProperty<VSdDadosProduto> produtos = new SimpleListProperty<>();
    
    public PrintTagProduto() {
        super("Imprimir Tags Produto", new Image(PrintTagProduto.class.getResource("/images/icons/imprimir tags (4).png").toExternalForm()));
        
        produtosTable.items.bind(produtos);
        
        imprimirEtiquetaButton.disableProperty().bind(Bindings.lessThanOrEqual(produtos.sizeProperty(), 0));
        
        super.box.getChildren().add(FormBox.create(formBoxFilter -> {
            formBoxFilter.horizontal();
            formBoxFilter.add(FormTitledPane.create(formTitledPane -> {
                formTitledPane.filter();
                formTitledPane.add(FormBox.create(formBox -> {
                    formBox.horizontal();
                    formBox.add(formBox.create(formBoxC1 -> {
                        formBoxC1.vertical();
                        formBoxC1.add(produtoField.build());
                        formBoxC1.add(marcaField.build());
                    }));
                    formBox.add(formBox.create(formBoxC2 -> {
                        formBoxC2.vertical();
                        formBoxC2.add(colecaoField.build());
                    }));
                }));
                formTitledPane.find.setOnAction(event -> {
                    daoDadosProdutos = daoDadosProdutos.initCriteria();
                    if (marcaField.textValue.get()!= null && !marcaField.textValue.get().isEmpty())
                        daoDadosProdutos.addPredicate("codMarca", marcaField.textValue.getValue().split(","), PredicateType.IN);
                    if (colecaoField.textValue.get()!= null && !colecaoField.textValue.get().isEmpty())
                        daoDadosProdutos.addPredicate("codCol", colecaoField.textValue.getValue().split(","), PredicateType.IN);
                    if (produtoField.textValue.get()!= null && !produtoField.textValue.get().isEmpty())
                        daoDadosProdutos.addPredicate("codigo", produtoField.textValue.getValue().split(","), PredicateType.IN);
    
                    try {
                        produtos.set(daoDadosProdutos.loadListByPredicate());
                    } catch (SQLException e) {
                        e.printStackTrace();
                        LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                        GUIUtils.showException(e);
                    }
                });
                formTitledPane.clean.setOnAction(event -> {
                    marcaField.clear();
                    produtoField.clear();
                    colecaoField.clear();
                });
            }));
        }));
        super.box.getChildren().add(produtosTable.build());
        super.box.getChildren().add(FormBox.create(formBox -> {
            formBox.horizontal();
            formBox.add(imprimirEtiquetaButton);
        }));
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    private void printZplString(String zplCode){
        PrinterJob jobPrinterSelect = PrinterJob.createPrinterJob();
        if (!jobPrinterSelect.showPrintDialog(Globals.getMainStage().getScene().getWindow())) {
            return;
        }
    
        String printerName = jobPrinterSelect.getPrinter().getName();
        PrintService ps = null;
        try {
        
            byte[] by = zplCode.getBytes();
            DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
            PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);
        
            if (pss.length == 0) {
                throw new RuntimeException("No printer services available.");
            }
        
            for (PrintService psRead : pss) {
                if (psRead.getName().contains(printerName)) {
                    ps = psRead;
                    break;
                }
            }
        
            if (ps == null) {
                throw new RuntimeException("No printer mapped in TS.");
            }
        
            DocPrintJob job = ps.createPrintJob();
            Doc doc = new SimpleDoc(by, flavor, null);
        
            job.print(doc, null);
        } catch (PrintException e) {
            GUIUtils.showMessage("Cannot print label on this printer : " + ps.getName(), Alert.AlertType.ERROR);
        }
    }
    
    @NotNull
    private String generateZplCode(Integer qtdeEtiquetasProduto) {
        String zplCodeTag = "";
        int paginadorTags = 1;
        for (VSdDadosProduto selectedRegister : produtosTable.selectedRegisters()) {
            for (int i = 0; i < qtdeEtiquetasProduto; i++) {
                if (paginadorTags == 1) {
                    zplCodeTag += ""
                            + "^XA\n"
                            + "^MMT\n"
                            + "^PW799\n"
                            + "^LL0440\n"
                            + "^LS0\n";
                    zplCodeTag += "" +
                            "^FT716,381^A0I,28,28^FH\\^FD"+ selectedRegister.getCodigo() +"^FS\n" +
                            "^FT734,412^A0I,23,24^FH\\^FDTAG DIGITAL^FS\n" +
                            "^FT740,371^A0B,17,16^FH\\^FDFor\\87a de Vendas^FS\n" +
                            "^BY3,3,44^FT791,370^BCB,,N,N\n" +
                            "^FD>9361817162524^FS\n" +
                            "^FT592,170^BQN,2,4\n" +
                            "^FH\\^FDMA,https://imagens.deliz.com.br/carteirinha/"+ selectedRegister.getCodigo() +"_ct.png^FS\n" +
                            "^FT625,364^A0B,17,16^FB168,1,0,C^FH\\^FDLeia este QRCODE,^FS\n" +
                            "^FT646,364^A0B,17,16^FB168,1,0,C^FH\\^FDcom seu Smartphone^FS\n" +
                            "^FT667,364^A0B,17,16^FB168,1,0,C^FH\\^FDTablet, para acessar as ^FS\n" +
                            "^FT688,364^A0B,17,16^FB168,1,0,C^FH\\^FDinforma\\87\\E4es e variantes ^FS\n" +
                            "^FT709,364^A0B,17,16^FB168,1,0,C^FH\\^FDde cor deste produto.^FS\n" +
                            "^FT593,322^A0B,17,16^FH\\^FD"+ selectedRegister.getGrade() +"^FS\n" +
                            "^FT593,371^A0B,17,16^FH\\^FDGrade:^FS\n" +
                            "^FT570,372^A0B,17,16^FH\\^FD"+ selectedRegister.getDescricao() +"^FS\n" +
                            "^LRY^FO543,377^GB256,0,29^FS^LRN";
                } else if (paginadorTags == 2) {
                    zplCodeTag += "" +
                            "^FT460,381^A0I,28,28^FH\\^FD"+selectedRegister.getCodigo()+"^FS\n" +
                            "^FT478,412^A0I,23,24^FH\\^FDTAG DIGITAL^FS\n" +
                            "^FT484,371^A0B,17,16^FH\\^FDFor\\87a de Vendas^FS\n" +
                            "^BY3,3,44^FT535,370^BCB,,N,N\n" +
                            "^FD>9361817162524^FS\n" +
                            "^FT336,170^BQN,2,4\n" +
                            "^FH\\^FDMA,https://imagens.deliz.com.br/carteirinha/" + selectedRegister.getCodigo() + "_ct.png^FS\n" +
                            "^FT369,364^A0B,17,16^FB168,1,0,C^FH\\^FDLeia este QRCODE,^FS\n" +
                            "^FT390,364^A0B,17,16^FB168,1,0,C^FH\\^FDcom seu Smartphone^FS\n" +
                            "^FT411,364^A0B,17,16^FB168,1,0,C^FH\\^FDTablet, para acessar as ^FS\n" +
                            "^FT432,364^A0B,17,16^FB168,1,0,C^FH\\^FDinforma\\87\\E4es e variantes ^FS\n" +
                            "^FT453,364^A0B,17,16^FB168,1,0,C^FH\\^FDde cor deste produto.^FS\n" +
                            "^FT337,322^A0B,17,16^FH\\^FD" + selectedRegister.getGrade() + "^FS\n" +
                            "^FT337,371^A0B,17,16^FH\\^FDGrade:^FS\n" +
                            "^FT314,372^A0B,17,16^FH\\^FD" + selectedRegister.getDescricao() + "^FS\n" +
                            "^LRY^FO287,377^GB256,0,29^FS^LRN";
                } else {
                    zplCodeTag += "" +
                            "^FT205,381^A0I,28,28^FH\\^FD" + selectedRegister.getCodigo() + "^FS\n" +
                            "^FT223,412^A0I,23,24^FH\\^FDTAG DIGITAL^FS\n" +
                            "^FT229,371^A0B,17,16^FH\\^FDFor\\87a de Vendas^FS\n" +
                            "^BY3,3,44^FT280,370^BCB,,N,N\n" +
                            "^FD>9361817162524^FS\n" +
                            "^FT81,170^BQN,2,4\n" +
                            "^FH\\^FDMA,https://imagens.deliz.com.br/carteirinha/" + selectedRegister.getCodigo() + "_ct.png^FS\n" +
                            "^FT114,364^A0B,17,16^FB168,1,0,C^FH\\^FDLeia este QRCODE,^FS\n" +
                            "^FT135,364^A0B,17,16^FB168,1,0,C^FH\\^FDcom seu Smartphone^FS\n" +
                            "^FT156,364^A0B,17,16^FB168,1,0,C^FH\\^FDTablet, para acessar as ^FS\n" +
                            "^FT177,364^A0B,17,16^FB168,1,0,C^FH\\^FDinforma\\87\\E4es e variantes ^FS\n" +
                            "^FT198,364^A0B,17,16^FB168,1,0,C^FH\\^FDde cor deste produto.^FS\n" +
                            "^FT82,322^A0B,17,16^FH\\^FD" + selectedRegister.getGrade() + "^FS\n" +
                            "^FT82,371^A0B,17,16^FH\\^FDGrade:^FS\n" +
                            "^FT59,372^A0B,17,16^FH\\^FD" + selectedRegister.getDescricao() + "^FS\n" +
                            "^LRY^FO32,377^GB256,0,29^FS^LRN";
                    zplCodeTag += ""
                            + "^PQ1,0,1,Y\n"
                            + "^XZ\n";
                    paginadorTags = 0;
                }
                paginadorTags++;
            }
        }
        if ((produtosTable.selectedRegisters().size() * qtdeEtiquetasProduto) % 3 != 0) {
            zplCodeTag += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
        return zplCodeTag;
    }
}
