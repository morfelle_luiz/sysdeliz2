/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.expedicao;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.*;
import sysdeliz2.models.*;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.AcessoSistema;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SceneExpedicaoReservasController implements Initializable {
    
    /**
     * Variáveis do FXML View
     */
    @FXML
    private ComboBox<String[]> cboxStatusReserva;
    @FXML
    private TextField tboxNumeroPedido;
    @FXML
    private TextField tboxNumeroReserva;
    @FXML
    private TableView<ReservaPedido> tblReservas;
    @FXML
    private TableView<ProdutosReservaPedido> tblProdutosReserva;
    @FXML
    private Button btnImprimir;
    @FXML
    private Button btnImprimirVarios;
    @FXML
    private TextField tboxCadNumeroPedido;
    @FXML
    private TextField tboxCadNumeroReserva;
    @FXML
    private TextField tboxCadDeposito;
    @FXML
    private TextField tboxCliente;
    @FXML
    private TextField tboxCadQtdeTotal;
    @FXML
    private TextField tboxCadStatusReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmStatusReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmPedidoReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmCondicao;
    @FXML
    private TableColumn<ReservaPedido, String> clmMarcaReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmClienteReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmNumeroReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmDepositoReserva;
    @FXML
    private TableColumn<ReservaPedido, Integer> clmQtdeReserva;
    @FXML
    private TableColumn<ReservaPedido, String> clmDescPedido;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private Button btnExcluirReserva;
    @FXML
    private Button btnJoinReserva;
    @FXML
    private Button btnJoinCliente;
    
    /**
     * Variáveis do Controller
     */
    private ReservaPedidoDAO daoReservaPedido;
    private MarketingPedidoDAO daoMarketingPedido;
    private ProdutoDAO daoProduto;
    private CaixaReservaPedidoDAO daoCaixaReserva;
    private ReservaPedido rpCurrentReservaPedidoSelected;
    private List<ReservaPedido> loadData;
    private AcessoSistema usuarioLogado;
    private BooleanProperty hasItensTblReservas = new SimpleBooleanProperty(false);
    
    private void hasItensTableReserva() {
        if (tblReservas.getItems().isEmpty()) {
            hasItensTblReservas.set(false);
        } else {
            hasItensTblReservas.set(true);
        }
    }
    
    private ReservaPedido getReservaSelected(List<ReservaPedido> reservas, String status) {
        for (ReservaPedido reserva : reservas) {
            if (status.contains(reserva.getStrStatus())) {
                return reserva;
            }
        }
        return null;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuarioLogado = SceneMainController.getAcesso();
        
        daoReservaPedido = DAOFactory.getReservaPedidoDAO();
        
        daoMarketingPedido = DAOFactory.getMarketingPedidoDAO();
        daoProduto = DAOFactory.getProdutoDAO();
        daoCaixaReserva = DAOFactory.getCaixaReservaPedidoDAO();
        hasItensTableReserva();
        
        GUIUtils.autoFitTable(tblReservas, GUIUtils.COLUNAS_TBL_RESERVAS);
        pnStatusLoad.setVisible(true);
//        new Thread(() -> {
//            try {
//                loadData = daoReservaPedido.load();
//                tblReservas.setItems(FXCollections.observableArrayList(loadData));
//                printTable();
//                pnStatusLoad.setVisible(false);
//                hasItensTableReserva();
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneExpedicaoReservasController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }).start();
        
        tblReservas.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        btnExcluirReserva.disableProperty().bind(tblReservas.getSelectionModel().selectedItemProperty().isNull());
        btnJoinReserva.disableProperty().bind(tblReservas.getSelectionModel().selectedItemProperty().isNull());
        btnJoinCliente.disableProperty().bind(tblReservas.getSelectionModel().selectedItemProperty().isNull());
        btnImprimir.disableProperty().bind(tblReservas.getSelectionModel().selectedItemProperty().isNull());
        btnImprimirVarios.disableProperty().bind(hasItensTblReservas.not());
        
        tblReservas.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
            unbindData(oldValue);
            bindData(newValue);
        });
        
        cboxStatusReserva.setItems(FXCollections.observableArrayList(new String[]{" ", "Status"},
                new String[]{"A", "Aberto"}, new String[]{"I", "Impresso"}, new String[]{"P", "Lido"},
                new String[]{"C", "Em Caixa"}, new String[]{"E", "Expedido"}, new String[]{"F", "Faturado"}));
        cboxStatusReserva.setConverter(new StringConverter<String[]>() {
            @Override
            public String toString(String[] object) {
                return object[1];
            }
            
            @Override
            public String[] fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
        cboxStatusReserva.getSelectionModel().select(0);
    }
    
    private TableCell<ReservaPedido, String> getStringTableCell() {
        return new TableCell<ReservaPedido, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ReservaPedido currentReserva = currentRow == null ? null : (ReservaPedido) currentRow.getItem();
                if (currentReserva != null) {
                    String status = currentReserva.getStrStatus();
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }
            
            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }
            
            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusImpresso");
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
                styleClasses.remove("statusFaturado");
            }
            
            private void setPriorityStyle(String priority) {
                switch(priority) {
                    case "I":
                        getStyleClass().add("statusImpresso");
                        break;
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "E":
                        getStyleClass().add("statusExpedido");
                        break;
                    case "F":
                        getStyleClass().add("statusFaturado");
                        break;
                }
            }
            
            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        };
    }
    
    private TableCell<ReservaPedido, Integer> getIntegerTableCell() {
        return new TableCell<ReservaPedido, Integer>() {
            @Override
            public void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : "" + getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ReservaPedido currentReserva = currentRow == null ? null : (ReservaPedido) currentRow.getItem();
                if (currentReserva != null) {
                    String status = currentReserva.getStrStatus();
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }
            
            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }
            
            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusImpresso");
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
                styleClasses.remove("statusFaturado");
                
            }
            
            private void setPriorityStyle(String priority) {
                switch(priority) {
                    case "I":
                        getStyleClass().add("statusImpresso");
                        break;
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "E":
                        getStyleClass().add("statusExpedido");
                        break;
                    case "F":
                        getStyleClass().add("statusFaturado");
                        break;
                }
            }
            
            private Integer getString() {
                return getItem() == null ? 0 : getItem();
            }
        };
    }
    
    private void printTable() {
        clmStatusReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmPedidoReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmCondicao.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmMarcaReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmDescPedido.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmClienteReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmNumeroReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmDepositoReserva.setCellFactory((TableColumn<ReservaPedido, String> param) -> getStringTableCell());
        clmQtdeReserva.setCellFactory((TableColumn<ReservaPedido, Integer> param) -> getIntegerTableCell());
    }
    
    private void bindData(ReservaPedido rpReserva) {
        if (rpReserva != null) {
            tboxCadNumeroReserva.textProperty().bind(rpReserva.strNumeroReservaProperty());
            tboxCadNumeroPedido.textProperty().bind(rpReserva.strNumeroPedidoProperty());
            tboxCadDeposito.textProperty().bind(rpReserva.strDepositoProperty());
            tboxCadQtdeTotal.textProperty().bind(rpReserva.intQtdeProperty().asString());
            tboxCadStatusReserva.textProperty().bind(rpReserva.strDescStatusProperty());
            
            tblProdutosReserva.setItems(FXCollections.observableArrayList(rpReserva.listProdutosProperty()));
        }
    }
    
    private void unbindData(ReservaPedido rpReserva) {
        if (rpReserva != null) {
            tboxCadNumeroReserva.textProperty().unbind();
            tboxCadNumeroPedido.textProperty().unbind();
            tboxCadDeposito.textProperty().unbind();
            tboxCadQtdeTotal.textProperty().unbind();
            tboxCadStatusReserva.textProperty().unbind();
            
            tblProdutosReserva.setItems(FXCollections.observableArrayList());
            
            clearForm();
        }
    }
    
    private void clearForm() {
        tboxCadNumeroReserva.clear();
        tboxCadNumeroPedido.clear();
        tboxCadDeposito.clear();
        tboxCadQtdeTotal.clear();
    }
    
    private void printReserva(ReservaPedido reserva) {
        rpCurrentReservaPedidoSelected = reserva;
        
        List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
        List<ProdutosReservaPedido> mktsParaRomaneio = new ArrayList<>();
        Map<String, Object> parametros = new HashMap<>();
        String grupoCliente = "";
        String colecaoPedido;
        
        JasperPrint jasperPrint;
        JasperReport jr;
        try {
            
            if (rpCurrentReservaPedidoSelected.strNumeroReservaProperty().get().equals("0")) {
                rpCurrentReservaPedidoSelected.strStatusProperty().set("I");
                rpCurrentReservaPedidoSelected.strDescStatusProperty().set(GUIUtils.getStatusReserva(rpCurrentReservaPedidoSelected.strStatusProperty().get()));
                
                String strAIReserva = daoReservaPedido.updByPedidoCodigoCorTam(rpCurrentReservaPedidoSelected, usuarioLogado);
                
                rpCurrentReservaPedidoSelected.strNumeroReservaProperty().set(strAIReserva);
                
                // verificar as marcas da reserva
                List<String> marcasReserva = daoReservaPedido.getMarcasReserva(rpCurrentReservaPedidoSelected);
                grupoCliente = marcasReserva.get(0); // obtendo o SIT_CLI do cliente
                marcasReserva.remove(0);
                colecaoPedido = marcasReserva.get(0); // obtendo a coleção do pedido
                marcasReserva.remove(0);
                
                Pedido pedJpa = new FluentDao().selectFrom(Pedido.class).where(it -> it.equal("numero", rpCurrentReservaPedidoSelected.getStrNumeroPedido())).singleResult();
                
                if (!colecaoPedido.equals("MKT") && !pedJpa.getTabPre().getRegiao().startsWith("B2B")) {
                    for (String marca : marcasReserva) {
                        // verificar se o cliente é novo
                        List<String> dadosCliente = daoMarketingPedido.isClienteNovo(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), marca);
                        
                        if (dadosCliente.contains("SIM")) {
                            // se for novo ver se já foi entregue marketing de cliente novo (está na consulta)
                            // verificar materiais de cliente novo
                            mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsClienteNovo(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), marca));
                            grupoCliente = "CN";
                        } else {
                            // se não for verificar se já foram entregues marketings unico no cliente (na consulta)
                            // se não listar marketings unico no cliente
                            // colocar em um arraylist para o romaneio
                            mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoCliente(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), marca, grupoCliente));
                        }
                        
                        // verificar se foram entregues materiais unico no pedido (na consulta)
                        // se não listar marketings unico no pedido
                        // colocar em um arraylist para o romaneio
                        mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoPedido(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), marca, grupoCliente));
                    }
                }
                // Verificar se tem cadastro de marketing no pedido
                List<ProdutosReservaPedido> mktsCadastradosPedido = daoMarketingPedido.loadMktsCadastradoNoPedido(rpCurrentReservaPedidoSelected.getStrNumeroPedido());
                // se tiver colocar no array para o romaneio
                mktsParaRomaneio.addAll(mktsCadastradosPedido);
                // atualizar o cadastro do marketing do pedido
                daoMarketingPedido.updByMktPedido(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), strAIReserva, mktsCadastradosPedido);
                
                // salvar materias do array no banco na tabela SD_RESERVA_PEDIDO_001
                daoReservaPedido.saveMktReservaPedido(rpCurrentReservaPedidoSelected, mktsParaRomaneio, usuarioLogado);
                
                bindData(rpCurrentReservaPedidoSelected);
            }
            
            List<ProdutosMktReservaPedido> mktsRomaneio = new ArrayList<>();
            int qtdeMkts = 0;
            for (ProdutosReservaPedido produtosReservaPedido : mktsParaRomaneio) {
                String descMkt = (daoProduto.getProdutosMarketingByCodigo(produtosReservaPedido.getStrCodigo())).getStrDescricao();
                String obsMkt = (daoMarketingPedido.getProdutoMktPedido(rpCurrentReservaPedidoSelected.getStrNumeroPedido(), produtosReservaPedido.getStrCodigo())).getStrObs();
                
                qtdeMkts += produtosReservaPedido.getIntQtde();
                
                mktsRomaneio.add(
                        new ProdutosMktReservaPedido(
                                produtosReservaPedido.getStrLocal(),
                                produtosReservaPedido.getStrCodigo(),
                                produtosReservaPedido.getIntQtde(),
                                descMkt,
                                obsMkt
                        )
                );
            }
            JRBeanCollectionDataSource jrbeanListMkts = new JRBeanCollectionDataSource(mktsRomaneio);
            rpCurrentReservaPedidoSelected.intQtdeProperty().set(rpCurrentReservaPedidoSelected.intQtdeProperty().get() + qtdeMkts);
            
            parametros.put("PEDIDO", rpCurrentReservaPedidoSelected.getStrNumeroPedido());
            parametros.put("RESERVA", rpCurrentReservaPedidoSelected.getStrNumeroReserva());
            parametros.put("TOTALRESERVA", rpCurrentReservaPedidoSelected.getIntQtde());
            parametros.put("PRODUTOSMKT", jrbeanListMkts);
            parametros.put("GRUPOCLIENTE", grupoCliente);
            parametros.put("LOGODELIZ", " ");
            parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RomaneioReservaPedido.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPrinter(romaneiosParaImpressao);
            
        } catch (JRException | SQLException | IOException ex) {
            Logger.getLogger(SceneExpedicaoReservasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        
        printTable();
    }
    
    @FXML
    public void btnProcuraReservaOnAction(ActionEvent evt) {
        GUIUtils.autoFitTable(tblReservas, GUIUtils.COLUNAS_TBL_RESERVAS);
        pnStatusLoad.setVisible(true);
        Thread consulta = new Thread(() -> {
            try {
                String statusReserva = cboxStatusReserva.getSelectionModel().getSelectedItem()[0];
                loadData = daoReservaPedido.loadByFormReservaPedido(tboxNumeroPedido.getText() + " ",
                        tboxNumeroReserva.getText() + " ", tboxCliente.getText() + " ", statusReserva);
                tblReservas.setItems(FXCollections.observableArrayList(loadData));
                printTable();
                pnStatusLoad.setVisible(false);
                
                hasItensTableReserva();
                
            } catch (SQLException ex) {
                Logger.getLogger(SceneExpedicaoReservasController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        try {
            consulta.start();
            evt.consume();
        } catch (Exception ex) {
            consulta.destroy();
        }
    }
    
    @FXML
    public void btnImprimirOnAction(ActionEvent evt) {
        tblReservas.getSelectionModel().getSelectedItems().forEach(this::printReserva);
        evt.consume();
    }
    
    @FXML
    public void btnImprimirVariosOnAction(ActionEvent evt) {
        String[] pedidosParaImpressao = GUIUtils.showTextAreaDialog("Cole os números dos PEDIDOS para impressão:");
        for (String pedido : pedidosParaImpressao) {
            for (ReservaPedido reserva : tblReservas.getItems()) {
                if (reserva.getStrNumeroPedido().equals(pedido) && reserva.getStrNumeroReserva().equals("0")) {
                    printReserva(reserva);
                    break;
                }
            }
        }
        evt.consume();
    }
    
    @FXML
    private void ctxMenuExcluirReferenciaOnAction(ActionEvent event) {
        try {
            rpCurrentReservaPedidoSelected = tblReservas.getSelectionModel().getSelectedItem();
            ProdutosReservaPedido prpCurrentReservaPedidoSelected = tblProdutosReserva.getSelectionModel().getSelectedItem();
            if (rpCurrentReservaPedidoSelected.strStatusProperty().isEqualTo("F")
                    .or(rpCurrentReservaPedidoSelected.strStatusProperty().isEqualTo("E")).get()) {
                GUIUtils.showMessage("Você não pode excluir uma referência de uma reserva já faturada ou pronta para faturamento.",
                        AlertType.INFORMATION);
                return;
            }
            CaixaReservaPedido caixaProduto = daoCaixaReserva.getCaixaByProduto(rpCurrentReservaPedidoSelected,
                    prpCurrentReservaPedidoSelected);
            if (caixaProduto != null) {
                GUIUtils.showMessage("Você não pode excluir uma referência que já se encontra em uma caixa.\n"
                                + "Remova as barras da referência na caixa nº.: " + caixaProduto.getStrCaixa() + " e após faça a remoção nesta tela.",
                        AlertType.INFORMATION);
                return;
            }
            
            if (!GUIUtils.showQuestionMessageDefaultNao("Tem certeza que deseja excluir a referência?")) {
                return;
            }
            
            daoReservaPedido.deleteReferenciaReserva(rpCurrentReservaPedidoSelected, prpCurrentReservaPedidoSelected);
            daoReservaPedido.deleteReferenciaReserva_PED_RESERVA(rpCurrentReservaPedidoSelected, prpCurrentReservaPedidoSelected);
            rpCurrentReservaPedidoSelected.listProdutosProperty().get().remove(prpCurrentReservaPedidoSelected);
            bindData(rpCurrentReservaPedidoSelected);
            printTable();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        event.consume();
    }
    
    @FXML
    private void btnExcluirReservaOnAction(ActionEvent event) {
        rpCurrentReservaPedidoSelected = tblReservas.getSelectionModel().getSelectedItem();
        if (rpCurrentReservaPedidoSelected.strStatusProperty().isEqualTo("P")
                .or(rpCurrentReservaPedidoSelected.strStatusProperty().isEqualTo("E")
                        .or(rpCurrentReservaPedidoSelected.strStatusProperty().isEqualTo("F")))
                .get()) {
            GUIUtils.showMessage("Você pode excluir somente reservas com o STATUS = IMPRESSO", AlertType.INFORMATION);
            return;
        }
        
        if (GUIUtils.showQuestionMessageDefaultNao("Tem certeza que deseja excluir a reserva?")) {
            try {
                daoReservaPedido.deleteReservaPedido(rpCurrentReservaPedidoSelected);
                daoReservaPedido.deleteReservaPedido_PED_RESERVA(rpCurrentReservaPedidoSelected);
                daoMarketingPedido.updReservaByReservaPedido(rpCurrentReservaPedidoSelected);
                tblReservas.getItems().remove(rpCurrentReservaPedidoSelected);
                unbindData(rpCurrentReservaPedidoSelected);
                tblReservas.getSelectionModel().select(0);
                printTable();
            } catch (SQLException e) {
                e.printStackTrace();
                GUIUtils.showException(e);
            }
        }
        event.consume();
    }
    
    @FXML
    private void btnJoinReservaOnAction(ActionEvent event) {
        if (tblReservas.getSelectionModel().getSelectedItems().size() != 2) {
            GUIUtils.showMessage(
                    "Você deve selecionar somente duas reservas para fazer a junção.\nUtilize o CTRL do teclado para selecionar diferentes linhas da tabela.",
                    AlertType.INFORMATION);
            return;
        }
        
        List<ReservaPedido> selectReservas = tblReservas.getSelectionModel().getSelectedItems();
        rpCurrentReservaPedidoSelected = this.getReservaSelected(selectReservas, "IPE");
        ReservaPedido seqReservaSelected = this.getReservaSelected(selectReservas, "A");
        
        if (rpCurrentReservaPedidoSelected == null || seqReservaSelected == null) {
            GUIUtils.showMessage(
                    "Você deve selecionar uma reserva com STATUS igual IMPRESSO, PARCIAL ou PRONTO PARA FATURAR e uma reserva com STATUS igual ABERTO",
                    AlertType.WARNING);
            return;
        }
        if (!rpCurrentReservaPedidoSelected.getStrNumeroPedido().equals(seqReservaSelected.getStrNumeroPedido())) {
            GUIUtils.showMessage("Para junção, as reservas devem ser do mesmo PEDIDO.", AlertType.WARNING);
            return;
        }
        try {
            if (rpCurrentReservaPedidoSelected.getStrStatus().equals("E")) {
                rpCurrentReservaPedidoSelected.strStatusProperty().set("P");
                daoReservaPedido.updStatusByReservaPedido(rpCurrentReservaPedidoSelected);
            }
            
            rpCurrentReservaPedidoSelected.listProdutosProperty()
                    .addAll(seqReservaSelected.listProdutosProperty().get());
            rpCurrentReservaPedidoSelected.intQtdeProperty().set(rpCurrentReservaPedidoSelected.intQtdeProperty()
                    .add(seqReservaSelected.intQtdeProperty().get()).get());
            daoReservaPedido.updByPedidoCodigoCorTam(rpCurrentReservaPedidoSelected, usuarioLogado);
            tblReservas.getItems().remove(seqReservaSelected);
            bindData(rpCurrentReservaPedidoSelected);
            printTable();
        } catch (SQLException e) {
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        event.consume();
    }
    
    @FXML
    private void btnJoinClienteOnAction(ActionEvent event) {
        
        List<ReservaPedido> selectReservas = new ArrayList<>(tblReservas.getSelectionModel().getSelectedItems());
        Object[] clientes = selectReservas.stream().map(reservaPedido -> reservaPedido.getStrCliente()).distinct().toArray();
        Object[] marcas = selectReservas.stream().map(reservaPedido -> reservaPedido.getStrMarca()).distinct().toArray();
        Object[] descontos = selectReservas.stream().map(reservaPedido -> reservaPedido.getStrPercenteDesc()).distinct().toArray();
        Object[] status = selectReservas.stream().filter(reservaPedido -> !reservaPedido.getStrStatus().equals("A")).distinct().toArray();
        Object[] reps = selectReservas.stream().map(reservaPedido -> reservaPedido.getCodrep().trim()).distinct().toArray();
        Object[] pgtos = selectReservas.stream().map(reservaPedido -> reservaPedido.getPgto().trim()).distinct().toArray();
        if (clientes.length != 1 || selectReservas.size() <= 1) {
            GUIUtils.showMessage(
                    "Você pode selecionar somente pedidos do mesmo cliente para esse agrupamento.",
                    AlertType.INFORMATION);
            return;
        }
        if (marcas.length != 1) {
            GUIUtils.showMessage(
                    "Você pode agrupar somente pedidos da mesma marca de um cliente.",
                    AlertType.INFORMATION);
            return;
        }
        if (descontos.length != 1) {
            GUIUtils.showMessage(
                    "Você pode agrupar somente pedidos da mesma marca de um cliente com o mesmo desconto.",
                    AlertType.INFORMATION);
            return;
        }
        if (status.length > 0) {
            GUIUtils.showMessage(
                    "Existem pedidos no agrupamento que não estão com o status 'Aberto', verifique os pedidos selecionados para o agrupamento.",
                    AlertType.INFORMATION);
            return;
        }
        if (reps.length > 1) {
            GUIUtils.showMessage(
                    "Você pode agrupar somente pedidos do mesmo representante.",
                    AlertType.INFORMATION);
            return;
        }
        if (pgtos.length > 1) {
            GUIUtils.showMessage(
                    "Você pode agrupar somente pedidos com a mesma condição de pagamento.",
                    AlertType.INFORMATION);
            return;
        }
        
        String pedidosAgrupados = selectReservas.stream().map(pedido -> pedido.getStrNumeroPedido()).collect(Collectors.joining("','"));
        
        try {
            selectReservas.sort(Comparator.comparing(ReservaPedido::getStrNumeroPedido));
            ReservaPedido reservaPrincipal = selectReservas.get(0);
            reservaPrincipal.strStatusProperty().set("I");
            String numeroReserva = daoReservaPedido.updByPedidoCodigoCorTam(reservaPrincipal, usuarioLogado);
            reservaPrincipal.setStrNumeroReserva(numeroReserva);
            selectReservas.remove(0);
            for (ReservaPedido selectReserva : selectReservas) {
                selectReserva.setStrStatus("I");
                selectReserva.setStrPedAgrupador(reservaPrincipal.getStrNumeroPedido());
                selectReserva.setStrNumeroReserva(numeroReserva);
                daoReservaPedido.updByPedidoCodigoCorTam(selectReserva, usuarioLogado);
            }
    
            List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
            List<ProdutosReservaPedido> mktsParaRomaneio = new ArrayList<>();
            Map<String, Object> parametros = new HashMap<>();
            String grupoCliente = "";
            String colecaoPedido;

            JasperPrint jasperPrint;
            JasperReport jr;
            // verificar as marcas da reserva
            List<String> marcasReserva = daoReservaPedido.getMarcasReserva(reservaPrincipal);
            grupoCliente = marcasReserva.get(0); // obtendo o SIT_CLI do cliente
            marcasReserva.remove(0);
            colecaoPedido = marcasReserva.get(0); // obtendo a coleção do pedido
            marcasReserva.remove(0);

            Pedido pedJpa = new FluentDao().selectFrom(Pedido.class).where(it -> it.equal("numero", reservaPrincipal.getStrNumeroPedido())).singleResult();

            if (!colecaoPedido.equals("MKT") && !pedJpa.getTabPre().getRegiao().startsWith("B2B")) {
                for (String marca : marcasReserva) {
                    // verificar se o cliente é novo
                    List<String> dadosCliente = daoMarketingPedido.isClienteNovo(reservaPrincipal.getStrNumeroPedido(), marca);

                    if (dadosCliente.contains("SIM")) {
                        // se for novo ver se já foi entregue marketing de cliente novo (está na consulta)
                        // verificar materiais de cliente novo
                        mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsClienteNovo(reservaPrincipal.getStrNumeroPedido(), marca));
                        grupoCliente = "CN";
                    } else {
                        // se não for verificar se já foram entregues marketings unico no cliente (na consulta)
                        // se não listar marketings unico no cliente
                        // colocar em um arraylist para o romaneio
                        mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoCliente(reservaPrincipal.getStrNumeroPedido(), marca, grupoCliente));
                    }

                    // verificar se foram entregues materiais unico no pedido (na consulta)
                    // se não listar marketings unico no pedido
                    // colocar em um arraylist para o romaneio
                    mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoPedido(reservaPrincipal.getStrNumeroPedido(), marca, grupoCliente));
                }
            }
            // Verificar se tem cadastro de marketing no pedido
            List<ProdutosReservaPedido> mktsCadastradosPedido = daoMarketingPedido.loadMktsCadastradoNoPedido(pedidosAgrupados);
            // se tiver colocar no array para o romaneio
            mktsParaRomaneio.addAll(mktsCadastradosPedido);
            // atualizar o cadastro do marketing do pedido
            daoMarketingPedido.updByMktPedido(reservaPrincipal.getStrNumeroPedido(), reservaPrincipal.getStrNumeroReserva(), mktsCadastradosPedido);

            // salvar materias do array no banco na tabela SD_RESERVA_PEDIDO_001
            daoReservaPedido.saveMktReservaPedido(reservaPrincipal, mktsParaRomaneio, usuarioLogado);

            List<ProdutosMktReservaPedido> mktsRomaneio = new ArrayList<>();
            int qtdeMkts = 0;
            for (ProdutosReservaPedido produtosReservaPedido : mktsParaRomaneio) {
                String descMkt = (daoProduto.getProdutosMarketingByCodigo(produtosReservaPedido.getStrCodigo())).getStrDescricao();
                String obsMkt = (daoMarketingPedido.getProdutoMktPedido(pedidosAgrupados, produtosReservaPedido.getStrCodigo())).getStrObs();

                qtdeMkts += produtosReservaPedido.getIntQtde();

                mktsRomaneio.add(
                        new ProdutosMktReservaPedido(
                                produtosReservaPedido.getStrLocal(),
                                produtosReservaPedido.getStrCodigo(),
                                produtosReservaPedido.getIntQtde(),
                                descMkt,
                                obsMkt
                        )
                );
            }
            JRBeanCollectionDataSource jrbeanListMkts = new JRBeanCollectionDataSource(mktsRomaneio);
            reservaPrincipal.intQtdeProperty().set(reservaPrincipal.intQtdeProperty().get() + qtdeMkts + selectReservas.stream().mapToInt(value -> value.getIntQtde()).sum());

            parametros.put("PEDIDO", reservaPrincipal.getStrNumeroPedido());
            parametros.put("RESERVA", reservaPrincipal.getStrNumeroReserva());
            parametros.put("TOTALRESERVA", reservaPrincipal.getIntQtde());
            parametros.put("PRODUTOSMKT", jrbeanListMkts);
            parametros.put("GRUPOCLIENTE", grupoCliente);
            parametros.put("LOGODELIZ", " ");
            parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

            jr = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/RomaneioReservaPedido.jasper"));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);
            ReportUtils.jasperToPrinter(romaneiosParaImpressao);
            
            
            bindData(reservaPrincipal);
            printTable();
            
        } catch (JRException | SQLException | IOException e) {
//        } catch (SQLException e) {
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        event.consume();
    }
    
}
