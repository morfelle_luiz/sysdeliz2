/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.expedicao;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.*;
import sysdeliz2.models.*;
import sysdeliz2.utils.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneLeituraExpedicaoController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML COMPONENTES">

    @FXML
    private Label lbObsExp;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private TextField tboxCarregaReserva;
    @FXML
    private Label lbPedido;
    @FXML
    private Label lbReserva;
    @FXML
    private Label lbCliente;
    @FXML
    private Label lbQtdeTotal;
    @FXML
    private TextField tboxBarraProduto;
    @FXML
    private TextField tboxDescProduto;
    @FXML
    private TableView<ProdutosReservaPedido> tblItensReserva;
    @FXML
    private Button btnFecharCaixa;
    @FXML
    private Button btnFecharReserva;
    @FXML
    private TextField tboxCheckBarra;
    @FXML
    private TextField tboxCaixaAtual;
    @FXML
    private TableView<ProdutoCaixaReservaPedido> tblItensCaixa;
    @FXML
    private TableView<CaixaReservaPedido> tblCaixasReserva;
    @FXML
    private Label lbTransportadora;
    @FXML
    private TableColumn<ProdutosReservaPedido, String> clmIRCodigo;
    @FXML
    private TableColumn<ProdutosReservaPedido, String> clmIRCor;
    @FXML
    private TableColumn<ProdutosReservaPedido, String> clmIRTam;
    @FXML
    private TableColumn<ProdutosReservaPedido, Integer> clmIRQtde;
    @FXML
    private TableColumn<ProdutosReservaPedido, Integer> clmIRSaldo;
    @FXML
    private TableColumn<?, ?> clmICBarra;
    @FXML
    private TableColumn<?, ?> clmICCodigo;
    @FXML
    private TableColumn<?, ?> clmICDescricao;
    @FXML
    private TableColumn<?, ?> clmICCor;
    @FXML
    private TableColumn<?, ?> clmICTam;
    @FXML
    private TableColumn<CaixaReservaPedido, String> clmCRCaixa;
    @FXML
    private TableColumn<CaixaReservaPedido, Integer> clmCRQtde;
    @FXML
    private Button btnExcluirCaixa;
    @FXML
    private TextField tboxQtdeItensEmCaixa;
    @FXML
    private Pane pnToggleExcluirCaixa;
    @FXML
    private Label lbToggleExcluirCaixa;
    @FXML
    private Label lbQtdeTotalCaixas;
    @FXML
    private Label lbSaldoTotal;
//</editor-fold>

    private ToggleSwitch tsExcluirBarra = new ToggleSwitch(20, 50);

    private ReservaPedido currentReserva;
    private CaixaReservaPedido caixaAberta;
    private ProdutoCaixaReservaPedido currentProdutoCaixa;
    private ReservaPedidoDAO daoReservaPedido;
    private ProdutoDAO daoProduto;
    private CaixaReservaPedidoDAO daoCaixaReservaPedido;
    private ProdutoLancamentoReserva prodBarra;
    private ProdutosReservaPedido produtoPedido;
    private BooleanProperty podeFecharCaixa = new SimpleBooleanProperty(false);
    private BooleanProperty podeFecharReserva = new SimpleBooleanProperty(false);
    private IntegerProperty qtdeTotalCaixas = new SimpleIntegerProperty(0);
    private IntegerProperty qtdeTotalSaldo = new SimpleIntegerProperty(0);
    private AcessoSistema usuarioLogado;

    private void printTableCR() {
        clmCRCaixa.setCellFactory((TableColumn<CaixaReservaPedido, String> param) -> new TableCell<CaixaReservaPedido, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                CaixaReservaPedido currentProduto = currentRow == null ? null
                        : (CaixaReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getStrStatus();
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusImpresso");
            }

            private void setPriorityStyle(String priority) {
                if ("A".equals(priority)) {
                    getStyleClass().add("statusImpresso");
                }
            }

            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        });

        clmCRQtde.setCellFactory((TableColumn<CaixaReservaPedido, Integer> param) -> new TableCell<CaixaReservaPedido, Integer>() {
            @Override
            public void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : "" + getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                CaixaReservaPedido currentProduto = currentRow == null ? null
                        : (CaixaReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getStrStatus();
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusImpresso");
            }

            private void setPriorityStyle(String priority) {
                if ("A".equals(priority)) {
                    getStyleClass().add("statusImpresso");
                }
            }

            private Integer getString() {
                return getItem() == null ? 0 : getItem();
            }
        });
    }

    private void printTableIR() {

        clmIRCodigo.setCellFactory((TableColumn<ProdutosReservaPedido, String> param) -> new TableCell<ProdutosReservaPedido, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ProdutosReservaPedido currentProduto = currentRow == null ? null
                        : (ProdutosReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getIntSaldo() == 0 ? "F"
                            : currentProduto.getIntSaldo() != currentProduto.getIntQtde() ? "P" : "N";
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "F":
                        getStyleClass().add("statusExpedido");
                        break;
                }
            }

            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        });

        clmIRCor.setCellFactory((TableColumn<ProdutosReservaPedido, String> param) -> new TableCell<ProdutosReservaPedido, String>() {

            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ProdutosReservaPedido currentProduto = currentRow == null ? null
                        : (ProdutosReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getIntSaldo() == 0 ? "F"
                            : currentProduto.getIntSaldo() != currentProduto.getIntQtde() ? "P" : "N";
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "F":
                        getStyleClass().add("statusExpedido");
                        break;
                }
            }

            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        });

        clmIRTam.setCellFactory((TableColumn<ProdutosReservaPedido, String> param) -> new TableCell<ProdutosReservaPedido, String>() {

            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ProdutosReservaPedido currentProduto = currentRow == null ? null
                        : (ProdutosReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getIntSaldo() == 0 ? "F"
                            : currentProduto.getIntSaldo() != currentProduto.getIntQtde() ? "P" : "N";
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "F":
                        getStyleClass().add("statusExpedido");
                        break;
                }
            }

            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        });

        clmIRQtde.setCellFactory((TableColumn<ProdutosReservaPedido, Integer> param) -> new TableCell<ProdutosReservaPedido, Integer>() {

            @Override
            public void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : "" + getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ProdutosReservaPedido currentProduto = currentRow == null ? null
                        : (ProdutosReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getIntSaldo() == 0 ? "F"
                            : currentProduto.getIntSaldo() != currentProduto.getIntQtde() ? "P" : "N";
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "F":
                        getStyleClass().add("statusExpedido");
                        break;
                }
            }

            private Integer getString() {
                return getItem() == null ? 0 : getItem();
            }
        });

        clmIRSaldo.setCellFactory((TableColumn<ProdutosReservaPedido, Integer> param) -> new TableCell<ProdutosReservaPedido, Integer>() {

            @Override
            public void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : "" + getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ProdutosReservaPedido currentProduto = currentRow == null ? null
                        : (ProdutosReservaPedido) currentRow.getItem();
                if (currentProduto != null) {
                    String status = currentProduto.getIntSaldo() == 0 ? "F"
                            : currentProduto.getIntSaldo() != currentProduto.getIntQtde() ? "P" : "N";
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "P":
                        getStyleClass().add("statusLido");
                        break;
                    case "F":
                        getStyleClass().add("statusExpedido");
                        break;
                }
            }

            private Integer getString() {
                return getItem() == null ? 0 : getItem();
            }
        });
    }

    private void bindData(CaixaReservaPedido caixa) {
        if (caixa != null) {
            tboxCaixaAtual.textProperty().bind(caixa.strCaixaProperty());
            tboxQtdeItensEmCaixa.textProperty().bind(caixa.intQtdeProperty().asString());
            tblItensCaixa.setItems(FXCollections.observableList(caixa.getListProdutosCaixa()));
            if (caixa.getStrStatus().equals("F")) {
                tboxCaixaAtual.setStyle("-fx-background-color: #90ee90;");
            } else {
                tboxCaixaAtual.setStyle("-fx-background-color: #faff00;");
            }
            this.checkFecharCaixaEReserva();
            podeFecharCaixa.set(caixa.getStrStatus().equals("A") && caixa.getIntQtde() > 0);
        }
    }

    private void unbindData(CaixaReservaPedido caixa) {
        tboxCaixaAtual.textProperty().unbind();
        tboxQtdeItensEmCaixa.textProperty().unbind();
        tblItensCaixa.setItems(FXCollections.observableArrayList());
        tboxCaixaAtual.setStyle("-fx-background-color: #faff00;");
    }

    private void bindDataReserva(ReservaPedido reserva) {
        lbPedido.textProperty().bind(reserva.strNumeroPedidoProperty());
        lbReserva.textProperty().bind(reserva.strNumeroReservaProperty());
        lbCliente.textProperty().bind(reserva.strClienteProperty());
        lbTransportadora.textProperty().bind(reserva.strTransportadoraProperty());
        lbQtdeTotal.textProperty().bind(reserva.intQtdeProperty().asString());
        lbObsExp.textProperty().bind(reserva.obsExpedicaoProperty());
        lbObsExp.visibleProperty().bind(reserva.obsExpedicaoProperty().isNotNull().or(reserva.obsExpedicaoProperty().isNotEmpty()));
    }

    private void unbindDataReserva() {
        lbPedido.textProperty().unbind();
        lbReserva.textProperty().unbind();
        lbCliente.textProperty().unbind();
        lbTransportadora.textProperty().unbind();
        lbQtdeTotal.textProperty().unbind();
        lbObsExp.textProperty().unbind();
    }

    private void barraOk() {
        tboxCheckBarra.setText("SIM");
        tboxCheckBarra.setStyle("-fx-background-color: lawngreen;");
    }

    private void barraNok() {
        tboxCheckBarra.setText("NÃO");
        tboxCheckBarra.setStyle("-fx-background-color: red;");
    }

    private void checkFecharCaixaEReserva() {
        podeFecharCaixa.set(false);
        podeFecharReserva.set(false);
        if (tblCaixasReserva.getItems().size() <= 0) {
            return;
        }
        for (CaixaReservaPedido caixa : tblCaixasReserva.getItems()) {
            if (caixa.getStrStatus().equals("A") && caixa.getIntQtde() > 0) {
                podeFecharCaixa.set(true);
                return;
            }
        }
        for (CaixaReservaPedido caixa : tblCaixasReserva.getItems()) {
            if (caixa.getStrStatus().equals("E")) {
                return;
            }
        }
        if (qtdeTotalSaldo.greaterThan(0).get()) {
            return;
        }
        podeFecharReserva.set(true);
    }

    private void printEtiquetaCaixaNova() throws JRException, IOException, SQLException {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("PEDIDO", currentReserva.getStrNumeroPedido());
        parametros.put("RESERVA", currentReserva.getStrNumeroReserva());
        parametros.put("CAIXA", caixaAberta.getStrCaixa());
        parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

        JasperReport jr = (JasperReport) JRLoader
                .loadObject(getClass().getResourceAsStream("/relatorios/EtqCaixaReservaPedido.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
        ReportUtils.jasperToPrinter(jasperPrint);
    }

    private void printEtiquetaCaixaNova(String numeroCaixa) throws JRException, IOException, SQLException {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("PEDIDO", currentReserva.getStrNumeroPedido());
        parametros.put("RESERVA", currentReserva.getStrNumeroReserva());
        parametros.put("CAIXA", numeroCaixa);
        parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

        JasperReport jr = (JasperReport) JRLoader
                .loadObject(getClass().getResourceAsStream("/relatorios/EtqCaixaReservaPedido.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
        ReportUtils.jasperToPrinter(jasperPrint);
    }

    private void criarNovaCaixa() throws SQLException, JRException, IOException {
        // Busca no banco a numeração trazendo um objeto novo do tipo
        // CaixaReservaPedido
        caixaAberta = daoCaixaReservaPedido.newNovaCaixa();
        tblCaixasReserva.getItems().add(caixaAberta);
        tblCaixasReserva.getItems()
                .sort((CaixaReservaPedido t, CaixaReservaPedido t1) -> t.getStrStatus().compareTo(t1.getStrStatus()));
        unbindData(null);
        bindData(caixaAberta);
        // Mudar status da reserva para "P" se for a primeira caixa e lock no pedido do
        // TI
        if (tblCaixasReserva.getItems().size() == 1) {
            currentReserva.strStatusProperty().set("P");
            daoReservaPedido.updStatusByReservaPedido(currentReserva);
            daoReservaPedido.lockPedido(currentReserva);
            daoCaixaReservaPedido.adicionaLogPrimeiraCaixa(currentReserva);
        }
        // Imprime etiqueta da caixa
        this.printEtiquetaCaixaNova();
    }

    private Boolean isCaixaAberta() {
        return tblCaixasReserva.getItems().stream().anyMatch((caixa) -> (caixa.getStrStatus().equals("A")));
    }

    private ProdutosReservaPedido produtoPedido(String pedido, String reserva, String codigo, String cor, String tam) {
        for (ProdutosReservaPedido produto : tblItensReserva.getItems()) {
            if (produto.getStrCodigo().equals(codigo) && produto.getStrCor().equals(cor)
                    && produto.getStrTam().equals(tam) && produto.getStrPedido().equals(pedido)
                    && produto.getStrReserva().equals(reserva)) {
                return produto;
            }
        }
        return null;
    }

    private ProdutoCaixaReservaPedido produtoCaixa(String barra) {
        for (ProdutoCaixaReservaPedido produto : tblItensCaixa.getItems()) {
            if (produto.getStrBarra().equals(barra)) {
                return produto;
            }
        }
        return null;
    }

    private void adicionarBarra() throws SQLException, JRException, IOException {
        // executar
        ButtonType retornoErro;
        prodBarra = daoProduto.getProdutoLancamentoByPedidoAndReservaAndBarra(currentReserva.getStrNumeroPedido(),
                currentReserva.getStrNumeroReserva(), tboxBarraProduto.getText());
        if (prodBarra == null) {
            barraNok();
            GUIUtils.showMessageFullScreen("Este produto não foi encontrado no depósito 0001!\nInforme a TI sobre o erro.", Alert.AlertType.ERROR).showAndWait();

            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        }
        tboxDescProduto.setText(prodBarra.getStrCodigo() + " : " + prodBarra.getStrDescricao() + " (" + prodBarra.getStrCor() + "/" + prodBarra.getStrTam() + ")");
        if (!prodBarra.isBoolDoPedido()) {
            barraNok();
            GUIUtils.showMessageFullScreen("Este produto não pertence a esta reserva.\n"
                    + tboxBarraProduto.getText() + "\n"
                    + tboxDescProduto.getText(),
                    Alert.AlertType.ERROR
            ).showAndWait();
            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        } else if (prodBarra.isBoolExiste()) {
            barraNok();
            List<String> rastreio = daoCaixaReservaPedido.getRastreioBarra(tboxBarraProduto.getText());
            if (rastreio.isEmpty()) {
                GUIUtils.showMessageFullScreen("Esta barra já se encontra em uma caixa.\nFavor informar a TI a barra para rastreio ou abra novamente a tela de lançamento.",
                        Alert.AlertType.WARNING).showAndWait();
            } else {
                GUIUtils.showMessageFullScreen("Esta barra já se encontra em uma caixa.\nPedido: " + rastreio.get(1) + " - Reserva: "
                        + rastreio.get(2) + " - Caixa: " + rastreio.get(0) + "\n"
                        + "Lançado por: " + rastreio.get(4) + " em " + rastreio.get(3) + "\n"
                        + "Faturado: " + rastreio.get(5),
                        Alert.AlertType.INFORMATION).showAndWait();
            }
            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        } else if (prodBarra.getIntSaldo() <= 0) {
            barraNok();
            GUIUtils.showMessageFullScreen("Já foram lidos todos os produtos desta barra.\n"
                            + tboxBarraProduto.getText() + "\n"
                            + tboxDescProduto.getText(),
                    Alert.AlertType.WARNING).showAndWait();
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        }else if (prodBarra.getInfoExped() != null) {
            barraNok();
            GUIUtils.showMessageFullScreen(prodBarra.getInfoExped(),
                    Alert.AlertType.ERROR).showAndWait();
        }
        barraOk();
        if (!isCaixaAberta()) {
            this.criarNovaCaixa();
        }
        // CRIAR O OBJETO DO currentProdutoCaixa
        currentProdutoCaixa = new ProdutoCaixaReservaPedido(prodBarra.getStrPedido(), prodBarra.getStrReserva(), prodBarra.getStrCodigo(), prodBarra.getStrCor(),
                prodBarra.getStrTam(), tboxBarraProduto.getText(), prodBarra.getStrDescricao(), prodBarra.getStrTipo());
        int qtdeParaSalvar = 1;
        // verifica se é um produto de KIT
        if (prodBarra.getStrTipo().equals("KIT")) {
            qtdeParaSalvar = prodBarra.getIntQtde();
        }

        for (int i = 0; i < qtdeParaSalvar; i++) {
            // SALVAR NO BANCO O currentProdutoCaixa
            daoCaixaReservaPedido.saveBarra(prodBarra.getStrPedido(), prodBarra.getStrReserva(), caixaAberta, currentProdutoCaixa, usuarioLogado);
            // Salvar no banco a movimentação da PEDIDO3, PED_RESERVA, PA_ITEM e PA_MOV
            // Os produtos de MKTs não são adicionados na PEDIDO3 e nem removido da
            // PED_RESERVA (não existe alocação)
            if (!currentProdutoCaixa.getStrTipo().equals("MKT")) {
                daoCaixaReservaPedido.adicionaBarra_PEDIDO3(prodBarra.getStrPedido(), caixaAberta, currentProdutoCaixa, prodBarra,
                        usuarioLogado);
                // Removido regra de excluir da PED_RESERVA por regra do TI
                // daoCaixaReservaPedido.removeItem_PED_RESERVA(currentReserva,
                // currentProdutoCaixa);
            }
            daoCaixaReservaPedido.removeItem_PA_ITEM(currentProdutoCaixa, prodBarra);
            daoCaixaReservaPedido.adicionaMov_PA_MOV(prodBarra.getStrPedido(), caixaAberta, currentProdutoCaixa, prodBarra, "S",
                    usuarioLogado);
            // ADICIONAR O currentProdutoCaixa NO LIST DO OBJETO caixaAberta
            caixaAberta.getListProdutosCaixa().add(currentProdutoCaixa);
            // ADICIONAR O currentProdutoCaixa NO TABLEVIEW tblItensCaixa
            tblItensCaixa.refresh();
            // ADICIONAR +1 NO caixaAberta NA QTDE TOTAL
            caixaAberta.intQtdeProperty().set(caixaAberta.intQtdeProperty().get() + 1);
            qtdeTotalCaixas.set(qtdeTotalCaixas.add(1).get());
            qtdeTotalSaldo.set(qtdeTotalSaldo.subtract(1).get());
            // DIMINUIR -1 DO OBJETO DA LINHA SELECIONADO COM O CODIGO LIDO NO
            // tblItensReserva
            produtoPedido = this.produtoPedido(prodBarra.getStrPedido(), prodBarra.getStrReserva(), prodBarra.getStrCodigo(), prodBarra.getStrCor(), prodBarra.getStrTam());
            produtoPedido.intSaldoProperty().set(produtoPedido.intSaldoProperty().get() - 1);
        }
        tblItensReserva.getSelectionModel().select(produtoPedido);
    }

    private CaixaReservaPedido caixaProduto(String barra) {
        for (CaixaReservaPedido caixa : tblCaixasReserva.getItems()) {
            for (ProdutoCaixaReservaPedido produto : caixa.getListProdutosCaixa()) {
                if (produto.getStrBarra().equals(barra)) {
                    return caixa;
                }
            }
        }
        return null;
    }

    private void excluirBarra() throws SQLException {
        // executar
        ButtonType retornoErro;
        prodBarra = daoProduto.getProdutoLancamentoByPedidoAndReservaAndBarra(currentReserva.getStrNumeroPedido(),
                currentReserva.getStrNumeroReserva(), tboxBarraProduto.getText());
        tboxDescProduto.setText(prodBarra.getStrCodigo() + " : " + prodBarra.getStrDescricao() + " ("
                + prodBarra.getStrCor() + "/" + prodBarra.getStrTam() + ")");
        if (!prodBarra.isBoolDoPedido()) {
            barraNok();
            GUIUtils.showMessageFullScreen("Este produto não pertence a esta reserva.",
                    Alert.AlertType.ERROR).showAndWait();
//                    retornoErro = GUIUtils.showMessageNoButtonFocus("Este produto não pertence a esta reserva.",
//                            Alert.AlertType.ERROR);
            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        } else if (!prodBarra.isBoolExiste()) {
            barraNok();
            GUIUtils.showMessageFullScreen("Esta barra não se encontra em uma caixa.",
                    Alert.AlertType.WARNING).showAndWait();
//                    retornoErro = GUIUtils.showMessageNoButtonFocus("Esta barra não se encontra em uma caixa.",
//                            Alert.AlertType.WARNING);
            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();
            return;
        } else if (!caixaAberta.getStrStatus().equals("A")) {
            if (isCaixaAberta()) {
                barraNok();
                GUIUtils.showMessageFullScreen("Esta reserva está com uma caixa aberta.\n" + "Feche a caixa para excluir a barra.",
                        Alert.AlertType.WARNING).showAndWait();
//                retornoErro = GUIUtils.showMessageNoButtonFocus(
//                        "Esta reserva está com uma caixa aberta.\n" + "Feche a caixa para excluir a barra.",
//                        Alert.AlertType.WARNING);
                // Limpeza de tela
                tboxBarraProduto.clear();
                tboxBarraProduto.requestFocus();
                return;
            } else {
                caixaAberta = caixaProduto(tboxBarraProduto.getText());
            }
        }
        barraOk();

        // CRIAR O OBJETO DO currentProdutoCaixa
        currentProdutoCaixa = new ProdutoCaixaReservaPedido(prodBarra.getStrPedido(), prodBarra.getStrReserva(), prodBarra.getStrCodigo(), prodBarra.getStrCor(),
                prodBarra.getStrTam(), tboxBarraProduto.getText(), prodBarra.getStrDescricao(), prodBarra.getStrTipo());
        int qtdeParaSalvar = 1;
        // verifica se é um produto de KIT
        if (prodBarra.getStrTipo().equals("KIT")) {
            qtdeParaSalvar = prodBarra.getIntQtde();
        }
        for (int i = 0; i < qtdeParaSalvar; i++) {
            // DELETAR NO BANCO O currentProdutoCaixa
            // caixaAberta = tblCaixasReserva.getSelectionModel().getSelectedItem();
            daoCaixaReservaPedido.deleteBarra(prodBarra.getStrPedido(), prodBarra.getStrReserva(), caixaAberta, currentProdutoCaixa);
            // Fazer a movimentação no TI
            if (!prodBarra.getStrTipo().equals("MKT")) {
                daoCaixaReservaPedido.removeBarra_PEDIDO3(prodBarra.getStrPedido(), caixaAberta, currentProdutoCaixa);
                // Removido regra de excluir da PED_RESERVA por regra do TI
                // daoCaixaReservaPedido.adicionaItem_PED_RESERVA(currentReserva,
                // currentProdutoCaixa, prodBarra);
            }
            daoCaixaReservaPedido.adicionaItem_PA_ITEM(currentProdutoCaixa, prodBarra);
            daoCaixaReservaPedido.adicionaMov_PA_MOV(prodBarra.getStrPedido(), caixaAberta, currentProdutoCaixa, prodBarra, "E",
                    usuarioLogado);
            daoCaixaReservaPedido.adicionaLogRemocaoBarra(currentReserva, caixaAberta, currentProdutoCaixa);

            // ABRE a caixa e deixa ela aberta agora
            if (caixaAberta.getStrStatus().equals("F")) {
                caixaAberta.strStatusProperty().set("A");
                daoCaixaReservaPedido.updStatusCaixaReserva(prodBarra.getStrPedido(), prodBarra.getStrReserva(), caixaAberta);
            }
            bindData(caixaAberta);

            // REMOVER O currentProdutoCaixa NO LIST DO OBJETO caixaAberta
            caixaAberta.getListProdutosCaixa().remove(produtoCaixa(currentProdutoCaixa.getStrBarra()));
            // ADICIONAR -1 NO caixaAberta NA QTDE TOTAL
            caixaAberta.intQtdeProperty().set(caixaAberta.intQtdeProperty().get() - 1);
            qtdeTotalCaixas.set(qtdeTotalCaixas.subtract(1).get());
            qtdeTotalSaldo.set(qtdeTotalSaldo.add(1).get());
            // ADICIONAR +1 DO OBJETO DA LINHA SELECIONADO COM O CODIGO LIDO NO
            // tblItensReserva
            produtoPedido = this.produtoPedido(prodBarra.getStrPedido(), prodBarra.getStrReserva(), prodBarra.getStrCodigo(), prodBarra.getStrCor(), prodBarra.getStrTam());
            produtoPedido.intSaldoProperty().set(produtoPedido.intSaldoProperty().get() + 1);
        }
        // Atualizar os TABLEVIEWs para exibição dos novos valores dos objetos
        tblItensReserva.getSelectionModel().select(produtoPedido);
        tblCaixasReserva.refresh();
        tblItensReserva.refresh();
        tblItensCaixa.refresh();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        usuarioLogado = SceneMainController.getAcesso();
        daoReservaPedido = DAOFactory.getReservaPedidoDAO();
        MarketingPedidoDAO daoMarketingPedido = DAOFactory.getMarketingPedidoDAO();
        daoProduto = DAOFactory.getProdutoDAO();
        daoCaixaReservaPedido = DAOFactory.getCaixaReservaPedidoDAO();

        pnToggleExcluirCaixa.getChildren().add(tsExcluirBarra);
        tsExcluirBarra.disableProperty().bind(tboxBarraProduto.focusedProperty().not());
        lbToggleExcluirCaixa.textProperty()
                .bind(Bindings.when(tsExcluirBarra.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        lbQtdeTotalCaixas.textProperty().bind(qtdeTotalCaixas.asString());
        lbSaldoTotal.textProperty().bind(qtdeTotalSaldo.asString());

        tboxBarraProduto.editableProperty().set(false);
        btnExcluirCaixa.disableProperty().bind(tblCaixasReserva.getSelectionModel().selectedItemProperty().isNull());
        btnFecharCaixa.disableProperty().bind(podeFecharCaixa.not());
        btnFecharReserva.disableProperty().bind(podeFecharReserva.not());

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tboxCarregaReserva.requestFocus();
            }
        });

        tblCaixasReserva.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
            unbindData(oldValue);
            bindData(newValue);
        });

    }

    @FXML
    private void tboxCarregaReservaKeyPressed(KeyEvent evt) {
        if (evt.getCode().equals(KeyCode.ENTER)) {
            String[] barcodeReserva = tboxCarregaReserva.getText().toUpperCase().split("-");
            if (barcodeReserva.length < 2) {
                GUIUtils.showMessage("Confira o número do PEDIDO e RESERVA para consulta.\nEx.: 0000012345-0000000123",
                        Alert.AlertType.ERROR);
                return;
            }
            try {
                currentReserva = daoReservaPedido.getFromLancByPedidoAndReserva(barcodeReserva[0], barcodeReserva[1]);

                if (currentReserva != null) {
                    bindDataReserva(currentReserva);

                    tblItensReserva.setItems(FXCollections.observableArrayList(currentReserva.listProdutosProperty()));
                    tblItensReserva.getItems().forEach(produto -> {
                        qtdeTotalSaldo.set(qtdeTotalSaldo.add(produto.getIntSaldo()).get());
                    });
                    printTableIR();

                    tblCaixasReserva.setItems(FXCollections.observableList(
                            daoCaixaReservaPedido.loadByPedidoAndReserva(currentReserva.strNumeroPedidoProperty().get(),
                                    currentReserva.strNumeroReservaProperty().get())));
                    caixaAberta = tblCaixasReserva.getItems().size() > 0 ? tblCaixasReserva.getItems().get(0) : null;
                    if (caixaAberta != null) {
                        tblCaixasReserva.getItems().forEach(caixa -> {
                            qtdeTotalCaixas.set(qtdeTotalCaixas.add(caixa.getIntQtde()).get());
                        });
                        bindData(caixaAberta);
                        GUIUtils.autoFitTable(tblItensCaixa, GUIUtils.COLUNAS_TBL_ITENS_CAIXA_LANCAMENTO_RESERVA);
                        printTableCR();
                    }

                    this.checkFecharCaixaEReserva();
                    if (!currentReserva.getStrStatus().equals("E")) {
                        tboxCarregaReserva.setDisable(true);
                        tboxBarraProduto.editableProperty().set(true);
                        tboxBarraProduto.requestFocus();
                    } else {
                        tboxCarregaReserva.clear();
                        tboxCarregaReserva.requestFocus();
                    }
                } else {
                    GUIUtils.showMessage("O PEDIDO e a RESERVA não foram encontrados, verifique o romaneio.",
                            Alert.AlertType.INFORMATION);
                    lbReserva.setText(barcodeReserva[1]);
                    lbPedido.setText(barcodeReserva[0]);
                    tboxCarregaReserva.clear();
                    tboxCarregaReserva.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnFecharCaixaOnAction(ActionEvent event) {
        try {
            caixaAberta.strStatusProperty().set("F");
            caixaAberta.getListProdutosCaixa().forEach(produtoCaixa -> {
                try {
                    daoCaixaReservaPedido.updStatusCaixaReserva(produtoCaixa.getStrPedido(), produtoCaixa.getStrReserva(), caixaAberta);
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            });
            daoCaixaReservaPedido.adicionaLogEncerramentoCaixa(currentReserva, caixaAberta);

            // Desativado opção de abrir caixa depois de fechar a caixa
            // O sistema irá abrir sempre
            // if (GUIUtils.showQuestionMessageDefaultNao("Deseja abrir uma NOVA CAIXA?")) {
            // this.criarNovaCaixa();
            // }
            this.checkFecharCaixaEReserva();
        } catch (SQLException ex) {
            Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        printTableIR();
        printTableCR();

        // Limpeza de tela
        tboxBarraProduto.clear();
        tboxBarraProduto.requestFocus();
    }

    @FXML
    private void btnFecharReservaOnAction(ActionEvent event) {
        try {
            // Desalocando o pedido
            daoReservaPedido.unlockPedido(currentReserva);
            // Adicionar log do encerramento da reserva
            daoCaixaReservaPedido.adicionaLogEncerramentoReserva(currentReserva);
            // Adicionar o peso PESO nas caixas
            // Alterar o status das caixas da reserva para "E" de expedido
            tblCaixasReserva.getItems().forEach(caixa -> {
                    caixa.strStatusProperty().set("E");
                    caixa.getListProdutosCaixa().forEach(produtoCaixa -> {
                        try {
                        daoCaixaReservaPedido.alteraPesoCaixas_PEDIDO3(produtoCaixa.getStrPedido(), caixa);
                        daoCaixaReservaPedido.updStatusCaixaReserva(produtoCaixa.getStrPedido(), produtoCaixa.getStrReserva(), caixa);
                        } catch (SQLException ex) {
                            Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        }
                    });
            });
            // Alterar o status da reserva para "E" Expedido (liberado para faturar)
            currentReserva.strStatusProperty().set("E");
            daoReservaPedido.updStatusByReservaPedido(currentReserva);

            // Limpeza de tela
            currentReserva = new ReservaPedido();
            caixaAberta = new CaixaReservaPedido();
            currentProdutoCaixa = new ProdutoCaixaReservaPedido();
            unbindData(caixaAberta);
            unbindDataReserva();

            lbCliente.setText(" ");
            lbPedido.setText(" ");
            lbQtdeTotal.setText(" ");
            lbReserva.setText(" ");
            lbTransportadora.setText(" ");
            tboxDescProduto.clear();
            qtdeTotalCaixas.set(0);
            qtdeTotalSaldo.set(0);

            tblCaixasReserva.getItems().clear();
            tblItensCaixa.getItems().clear();
            tblItensReserva.getItems().clear();
            tboxCaixaAtual.clear();
            tboxQtdeItensEmCaixa.clear();

            printTableIR();
            printTableCR();
            this.checkFecharCaixaEReserva();

            tboxBarraProduto.clear();
            tboxBarraProduto.editableProperty().set(false);

            tboxCarregaReserva.disableProperty().set(false);
            tboxCarregaReserva.clear();
            tboxCarregaReserva.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnExcluirCaixaOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir a caixa?")) {
            try {
                caixaAberta = tblCaixasReserva.getSelectionModel().getSelectedItem();
                // Excluir a caixa no TI
                caixaAberta.getListProdutosCaixa().forEach(produtoCaixa -> {
                    try {
                        prodBarra = daoProduto.getProdutoLancamentoByPedidoAndReservaAndBarra(
                                currentReserva.getStrNumeroPedido(), currentReserva.getStrNumeroReserva(),
                                produtoCaixa.getStrBarra());
                        if (!prodBarra.getStrTipo().equals("MKT")) {
                            daoCaixaReservaPedido.removeBarra_PEDIDO3(prodBarra.getStrPedido(), caixaAberta, produtoCaixa);
                            // Removido regra de excluir da PED_RESERVA por regra do TI
                            // daoCaixaReservaPedido.adicionaItem_PED_RESERVA(currentReserva, produtoCaixa,
                            // prodBarra);
                        }
                        daoCaixaReservaPedido.adicionaItem_PA_ITEM(produtoCaixa, prodBarra);
                        daoCaixaReservaPedido.adicionaMov_PA_MOV(prodBarra.getStrPedido(), caixaAberta, produtoCaixa, prodBarra,
                                "E", usuarioLogado);
                        daoCaixaReservaPedido.adicionaLogRemocaoBarra(currentReserva, caixaAberta, produtoCaixa);
                        // Excluir caixa no banco do SysDeliz
                        daoCaixaReservaPedido.deleteCaixa(prodBarra.getStrPedido(), prodBarra.getStrReserva(), caixaAberta);
                    } catch (SQLException ex) {
                        Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                });
                currentReserva.strStatusProperty().set("P");
                daoReservaPedido.updStatusByReservaPedido(currentReserva);

                // ATUALIZAR o saldo do produto no objeto tblItensReserva.getItems() com +1 para
                // cada item excluido
                caixaAberta.getListProdutosCaixa().forEach(cnsmr -> {
                    produtoPedido = this.produtoPedido(cnsmr.getStrPedido(), cnsmr.getStrReserva(), cnsmr.getStrCodigo(), cnsmr.getStrCor(), cnsmr.getStrTam());
                    produtoPedido.intSaldoProperty().set(produtoPedido.intSaldoProperty().get() + 1);
                    qtdeTotalCaixas.set(qtdeTotalCaixas.subtract(1).get());
                    qtdeTotalSaldo.set(qtdeTotalSaldo.add(1).get());
                });
                // REMOVER da tblCaixasReserva a caixa
                tblCaixasReserva.getItems().removeIf(caixaAberta::equals);

                // ATUALIZAR o status da Reserva caso não fique mais nenhuma caixa. Passa para
                // "I"
                if (tblCaixasReserva.getItems().size() <= 0) {
                    currentReserva.strStatusProperty().set("I");
                    daoReservaPedido.updStatusByReservaPedido(currentReserva);
                }

                // Atualizar os TABLEVIEWs para exibição dos novos valores dos objetos
                tblCaixasReserva.refresh();
                tblItensReserva.refresh();
                tblItensCaixa.refresh();
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }

        printTableIR();
        printTableCR();
        this.checkFecharCaixaEReserva();
        // Limpeza de tela
        tboxBarraProduto.editableProperty().set(true);
        tboxBarraProduto.clear();
        tboxBarraProduto.requestFocus();
    }

    @FXML
    private void tboxBarraProdutoOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            // Adicionando barras a caixa
            bindData(caixaAberta);
            try {
                if (!tsExcluirBarra.switchedOnProperty().get()) {
                    this.adicionarBarra();
                    // Limpeza de tela
                    tboxBarraProduto.clear();
                    tboxBarraProduto.requestFocus();
                    if (qtdeTotalSaldo.isEqualTo(0).get()) {
                        // fechar caixa
                        this.btnFecharCaixaOnAction(null);
                        // fechar reserva
                        this.btnFecharReservaOnAction(null);
                    }
                } else {
                    this.excluirBarra();
                    // Limpeza de tela
                    tboxBarraProduto.clear();
                    tboxBarraProduto.requestFocus();
                }

                this.checkFecharCaixaEReserva();
                printTableIR();
                printTableCR();
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (JRException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (IOException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        } else if (event.isControlDown() && event.getCode() == KeyCode.N) {
            try {
                // Forçar criar uma nova caixa
                this.criarNovaCaixa();
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (JRException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (IOException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
            // Limpeza de tela
            tboxBarraProduto.clear();
            tboxBarraProduto.requestFocus();

            this.checkFecharCaixaEReserva();
            printTableIR();
            printTableCR();
        } else if (event.getCode() == KeyCode.F2) {
            // Altera para modo excluir barra
            tsExcluirBarra.switchedOnProperty().set(tsExcluirBarra.switchedOnProperty().not().get());
            if (currentReserva.getStrStatus().equals("E")) {
                // Limpeza de tela
                tboxBarraProduto.editableProperty().set(true);
                tboxBarraProduto.clear();
                tboxBarraProduto.requestFocus();
            }
        } else if (event.getCode() == KeyCode.F4) {
            // Excluir caixa
            if (tblCaixasReserva.getSelectionModel().selectedItemProperty().isNull().get()) {
                GUIUtils.showMessage("Você deve selecionar uma caixa para excluir.", Alert.AlertType.ERROR);
                return;
            }
            this.btnExcluirCaixaOnAction(null);

            this.checkFecharCaixaEReserva();
            printTableIR();
            printTableCR();
        } else if (event.getCode() == KeyCode.F8) {
            // Fechar caixa
            if (btnFecharCaixa.disableProperty().get()) {
                GUIUtils.showMessage("Você não pode fechar essa caixa.", Alert.AlertType.ERROR);
                return;
            }
            this.btnFecharCaixaOnAction(null);

            this.checkFecharCaixaEReserva();
            printTableIR();
            printTableCR();
        } else if (event.getCode() == KeyCode.F10) {
            // Fechar reserva
            if (btnFecharReserva.disableProperty().get()) {
                GUIUtils.showMessage("Verifique as caixas desta reserva para após fazer o fechamento.",
                        Alert.AlertType.WARNING);
                return;
            }
            this.btnFecharReservaOnAction(null);

            this.checkFecharCaixaEReserva();
            printTableIR();
            printTableCR();
        }
    }

    @FXML
    private void tblCaixasReservaOnKeyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.F5)) {
            bindDataReserva(currentReserva);

            tblCaixasReserva.getItems().clear();
            try {
                tblCaixasReserva.setItems(FXCollections.observableList(
                        daoCaixaReservaPedido.loadByPedidoAndReserva(currentReserva.strNumeroPedidoProperty().get(),
                                currentReserva.strNumeroReservaProperty().get())));
                caixaAberta = tblCaixasReserva.getItems().size() > 0 ? tblCaixasReserva.getItems().get(0) : null;
                if (caixaAberta != null) {
                    bindData(caixaAberta);
                    GUIUtils.autoFitTable(tblItensCaixa, GUIUtils.COLUNAS_TBL_ITENS_CAIXA_LANCAMENTO_RESERVA);
                    printTableCR();
                }

                this.checkFecharCaixaEReserva();
                tboxBarraProduto.requestFocus();
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        } else if (event.isControlDown() && event.getCode().equals(KeyCode.P)) {
            try {
                this.printEtiquetaCaixaNova(tblCaixasReserva.getSelectionModel().getSelectedItem().getStrCaixa());
            } catch (JRException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (IOException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (SQLException ex) {
                Logger.getLogger(SceneLeituraExpedicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
}
