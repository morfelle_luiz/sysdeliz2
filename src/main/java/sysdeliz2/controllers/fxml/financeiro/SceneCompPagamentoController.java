/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.financeiro;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sysdeliz2.models.ReceberCartao;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.sys.MailUtils;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCompPagamentoController extends Modals implements Initializable {

    @FXML
    private AnchorPane apCompPagto;
    @FXML
    private Label lbAtiCode;
    @FXML
    private Label lbDataAut;
    @FXML
    private Label lbPedido;
    @FXML
    private Label lbAutCode;
    @FXML
    private Label lbTipoCredito;
    @FXML
    private Label lbValorTotal;
    @FXML
    private Label lbDescQtdeParcelas;
    @FXML
    private Label lbQtdeParcelas;
    @FXML
    private Label lbDescValorParcela;
    @FXML
    private Label lbValorParcelaComp;
    @FXML
    private Label lbBrand;
    @FXML
    private Label lbCardNumber;
    @FXML
    private Button btnEnviarComprovante;

    private ReceberCartao selectedPayment;
    private String emailEnvio;

    public SceneCompPagamentoController(FXMLWindow file, ReceberCartao selectedPayment, String emailEnvio) throws IOException {
        super(file);

        this.selectedPayment = selectedPayment;
        this.emailEnvio = emailEnvio;

        super.styleWindow = StageStyle.UTILITY;
        super.show(this);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (selectedPayment != null) {
            lbPedido.setText(selectedPayment.getPedido());
            lbAutCode.setText(selectedPayment.getAuthorization_code());
            lbBrand.setText(selectedPayment.getBrand().toUpperCase());
            lbCardNumber.setText(selectedPayment.getCard_number());
            lbTipoCredito.setText(selectedPayment.getParcelas() == 1 ? "CREDITO A VISTA" : "CREDITO PARCELADO LOJISTA");
            lbValorTotal.setText(selectedPayment.getValor() + "");
            lbDataAut.setText(selectedPayment.getData_receber());
            if (selectedPayment.getParcelas() != 1) {
                lbDescQtdeParcelas.visibleProperty().set(true);
                lbDescValorParcela.visibleProperty().set(true);
                lbQtdeParcelas.visibleProperty().set(true);
                lbValorParcelaComp.visibleProperty().set(true);
                lbQtdeParcelas.setText("0" + selectedPayment.getParcelas());
                lbValorParcelaComp.setText(((Double) (selectedPayment.getValor() / ((Integer) selectedPayment.getParcelas()).doubleValue())) + "");
            }
        }
    }

    @FXML
    private void btnEnviarComprovanteOnAction(ActionEvent event) {
        WritableImage image = apCompPagto.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/local_files/comp_pag.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            MailUtils.sendMailCompPag(emailEnvio, selectedPayment);
        } catch (IOException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        Stage main = (Stage) btnEnviarComprovante.getScene().getWindow();
        main.close();
    }

}
