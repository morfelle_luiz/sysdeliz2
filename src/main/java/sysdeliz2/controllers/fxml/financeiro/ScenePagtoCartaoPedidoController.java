/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.financeiro;

import com.google.gson.Gson;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Cliente;
import sysdeliz2.models.Pedido;
import sysdeliz2.models.ReceberCartao;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.TableUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.enums.CardType;
import sysdeliz2.utils.sys.MailUtils;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePagtoCartaoPedidoController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Local vars">
//    private final String API_HOMOLOGACAO = "https://api-homologacao.getnet.com.br"; //HOMOLOGACAO
    private final String API_PRODUCAO = "https://api.getnet.com.br";
    private final String API_HOMOLOGACAO = API_PRODUCAO;
    private final String MOD_AUTH = "/auth/oauth/v2/token";
    private final String MOD_TOKEN_CARD = "/v1/tokens/card";
    private final String MOD_CARD_VERIFICATION = "/v1/cards/verification";
    private final String MOD_CARD_PAYMENT = "/v1/payments/credit";
    private final String MOD_CANCEL_REQUEST = "/v1/payments/cancel/request";
    private final String USER = "ed3917ab-a8e8-4545-8698-3cf98ff1e5e4";
    private final String PASSWORD = "80d404a1-516b-468c-9cc2-71cb1520e6af";
    private final String SELLER_ID = "63c22f0c-ebcc-40c1-b2b8-6317a5e58e2e";
//    private final String USER = "2a4c6eee-6d5e-4f8c-92e0-bc025e074ade"; //HOMOLOGACAO
//    private final String PASSWORD = "363ae0c8-513a-413d-8743-37c06d349556"; //HOMOLOGACAO
//    private final String SELLER_ID = "b04a96a1-3065-48e0-935d-bce558ce38d2"; //HOMOLOGACAO
    private ApiErrorReturn message = null;
    private OAuthToken authToken = null;
    private CardToken cardToken = null;
    private CardVerification cardVerification = null;
    private ReturnPayment returnPayment = null;
    private PaymentCancel cancelPay = null;
    private RequestCancelReturn returnCancelRequest = null;
    private RequestCancelStatus statusCancelRequest = null;
    private ReceberCartao pay_billing = null;
    private Integer contador = 0;
    private Locale BRAZIL = new Locale("pt", "BR");
    private NumberFormat currValue = NumberFormat.getCurrencyInstance(BRAZIL);
    private Pedido pedidoPagamento = null;
    private Double valorOriginal = 0.0;
    private Double descontoOriginal = 0.0;
    private Double totalOriginal = 0.0;

    private ToggleSwitch tsValorParcial = new ToggleSwitch(22, 40);
    private ToggleSwitch tsTipoReceber = new ToggleSwitch(22, 50);

    private Image imgElo = new Image(getClass().getResource("/images/b-elo-50.png").toExternalForm());
    private Image imgVisa = new Image(getClass().getResource("/images/b-visa-50.png").toExternalForm());
    private Image imgMaster = new Image(getClass().getResource("/images/b-mastercard-50.png").toExternalForm());
    private Image imgHiper = new Image(getClass().getResource("/images/b-hipercard-50.png").toExternalForm());
    private Image imgAmex = new Image(getClass().getResource("/images/b-amex-50.png").toExternalForm());
    private Image imgUnknown = new Image(getClass().getResource("/images/b-unknown-50.png").toExternalForm());
    private final Image btnImage = new Image(getClass().getResource("/images/icons8-card-payment-cancel-20.png").toExternalForm());
    private final Image btnViewImage = new Image(getClass().getResource("/images/icons8-card-payment-check-cancel-20.png").toExternalForm());
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML components">
    @FXML
    private AnchorPane anchorPagamento;
    @FXML
    private TextField tboxPedido_C;
    @FXML
    private TextField tboxCliente_C;
    @FXML
    private DatePicker tboxDataRec_C;
    @FXML
    private Button btnProcurarReceber;
    @FXML
    private TableView<ReceberCartao> tblRecebimentos;
    @FXML
    private TableColumn<ReceberCartao, String> clnCodCli;
    @FXML
    private TableColumn<ReceberCartao, String> clnNomeCli;
    @FXML
    private TableColumn<ReceberCartao, String> clnPedido;
    @FXML
    private TableColumn<ReceberCartao, Double> clnValor;
    @FXML
    private TableColumn<ReceberCartao, String> clnCartao;
    @FXML
    private TableColumn<ReceberCartao, ReceberCartao> clnStatus;
    @FXML
    private Pane pnToggleRecParcial;
    @FXML
    private Label lbToggleRecParcial;
    @FXML
    private TextField tboxNumero;
    @FXML
    private TextField tboxCliente;
    @FXML
    private TextField tboxDocto;
    @FXML
    private TextField tboxGrupo;
    @FXML
    private TextField tboxEndereco;
    @FXML
    private TextField tboxBairro;
    @FXML
    private TextField tboxCidade;
    @FXML
    private TextField tboxUf;
    @FXML
    private TextField tboxTelefone;
    @FXML
    private TextField tboxEmail;
    @FXML
    private TextField tboxValor;
    @FXML
    private TextField tboxTotal;
    @FXML
    private TextField tboxDesconto;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private TextField tboxNumeroEnd;
    @FXML
    private TextField tboxCvv;
    @FXML
    private TextField tboxNomeCartao;
    @FXML
    private ComboBox<String> tboxMes;
    @FXML
    private ComboBox<String> tboxAno;
    @FXML
    private ComboBox<String> tboxParcelas;
    @FXML
    private Label lbValorParcela;
    @FXML
    private Button btnPagar;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField tboxNumeroCartaoQuad1;
    @FXML
    private TextField tboxNumeroCartaoQuad2;
    @FXML
    private TextField tboxNumeroCartaoQuad3;
    @FXML
    private TextField tboxNumeroCartaoQuad4;
    @FXML
    private ImageView imgCardType;
    @FXML
    private TextField tboxNumeroAgrupado;
    @FXML
    private TextField tboxValorParcial;
    @FXML
    private MenuItem cmMenuEnviarComprovante;
    @FXML
    private MenuItem cmMenuCancelarPagamento;
    @FXML
    private Label lbTituloTboxNumero;
    @FXML
    private Pane pnToggleTipoReceber;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            tblRecebimentos.setItems(DAOFactory.getReceberCartaoDAO().get_all());
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        TableUtils.autoFitTable(tblRecebimentos, TableUtils.COLUNAS_TBL_FINANCEIRO_RECEBER_CARTAO_RECEBIMENTOS);
        this.factoryColumns();

        pnToggleRecParcial.getChildren().add(tsValorParcial);
        lbToggleRecParcial.textProperty().bind(Bindings.when(tsValorParcial.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        tsValorParcial.switchedOnProperty().addListener((obs, oldState, newState) -> {
            if (newState.booleanValue()) {
                tboxValorParcial.setEditable(true);
                tboxValorParcial.clear();
                tboxValorParcial.requestFocus();
            } else {
                tboxValorParcial.setEditable(false);
                tboxValorParcial.clear();
            }
        });

        pnToggleTipoReceber.getChildren().add(tsTipoReceber);
        tsTipoReceber.switchedOnProperty().addListener((obs, oldState, newState) -> {
            if (newState.booleanValue()) {
                lbTituloTboxNumero.setText(" Título");
                tboxNumero.setPromptText("Nº Título");
                tboxNumeroAgrupado.setPromptText("Nº Título");
            } else {
                lbTituloTboxNumero.setText(" Pedido");
                tboxNumero.setPromptText("Nº Pedido");
                tboxNumeroAgrupado.setPromptText("Nº Pedido");
            }
        });

        tboxNomeCartao.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));

        tboxMes.setItems(FXCollections.observableArrayList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"));
        for (int i = 0; i <= 20; i++) {
            tboxAno.getItems().add((LocalDate.now().getYear() - 2000 + i) + "");
        }
        tboxParcelas.setItems(FXCollections.observableArrayList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"));
        tboxParcelas.getSelectionModel().select(0);
        tboxParcelas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal() / Integer.parseInt(tboxParcelas.getValue())));
        });

        //this.connectApi();
    }


    //<editor-fold defaultstate="collapsed" desc="API methods">
    void executeHttpRequestTester(String jsonData, HttpPost httpPost) throws UnsupportedEncodingException, IOException {
        String line = "";
        StringBuffer result = new StringBuffer();

        HttpResponse response = null;
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();

        response = client.execute(httpPost);

        System.out.println("Post parameters : " + jsonData);
        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        System.out.println(result.toString());
    }

    HttpResponse executeHttpRequest(String jsonData, HttpPost httpPost) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpPost);
    }

    HttpResponse executeHttpRequest(HttpGet httpGet) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpGet);
    }

    void executeOAuth(String restUrl, String username, String password, String jsonData) {
        authToken = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        String auth = new StringBuffer(username).append(":").append(password).toString();
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        httpPost.setHeader("authorization", authHeader);
        httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
        try {
            HttpResponse response = executeHttpRequest(jsonData, httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                authToken = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), OAuthToken.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeTokenCard(String restUrl, OAuthToken tokenAuth, String jsonData) {
        cardToken = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpPost.setHeader("authorization", auth);
        httpPost.setHeader("content-type", "application/json");
        try {
            HttpResponse response = executeHttpRequest(jsonData, httpPost);
            if (response.getStatusLine().getStatusCode() != 201) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                cardToken = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), CardToken.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeCardVerification(String restUrl, OAuthToken tokenAuth, CardPayment card) {
        cardVerification = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpPost.setHeader("authorization", auth);
        httpPost.setHeader("content-type", "application/json");
        httpPost.setHeader("seller_id", SELLER_ID);
        try {
            HttpResponse response = executeHttpRequest(new Gson().toJson(card), httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                cardVerification = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), CardVerification.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executePayment(String restUrl, OAuthToken tokenAuth, Payment billing) {
        returnPayment = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpPost.setHeader("authorization", auth);
        httpPost.setHeader("content-type", "application/json");
        try {
            HttpResponse response = executeHttpRequest(new Gson().toJson(billing), httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                returnPayment = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ReturnPayment.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeCancelZeroDay(String restUrl, OAuthToken tokenAuth, String payment_id) {
        cancelPay = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl + "/v1/payments/credit/" + payment_id + "/cancel");
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpPost.setHeader("authorization", auth);
        httpPost.setHeader("cache-control", "no-cache");
        httpPost.setHeader("content-type", "application/json");
        try {
            HttpResponse response = executeHttpRequest("", httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                cancelPay = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), PaymentCancel.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeRequestCancel(String restUrl, OAuthToken tokenAuth, RequestCancel requestCancel) {
        returnCancelRequest = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpPost.setHeader("authorization", auth);
        httpPost.setHeader("cache-control", "no-cache");
        httpPost.setHeader("content-type", "application/json");
        httpPost.setHeader("seller_id", SELLER_ID);
        try {
            HttpResponse response = executeHttpRequest(new Gson().toJson(requestCancel), httpPost);
            if (response.getStatusLine().getStatusCode() != 202) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                returnCancelRequest = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), RequestCancelReturn.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeCancelStatus(String restUrl, OAuthToken tokenAuth, ReceberCartao recibo) {
        statusCancelRequest = null;
        message = null;
        HttpGet httpGet = new HttpGet(restUrl + "/v1/payments/cancel/request?cancel_custom_key=" + recibo.getNumero());
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpGet.setHeader("authorization", auth);
        httpGet.setHeader("seller_id", SELLER_ID);
        try {
            HttpResponse response = executeHttpRequest(httpGet);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                statusCancelRequest = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), RequestCancelStatus.class);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("error while encoding api url : " + e);
        } catch (IOException e) {
            System.out.println("ioException occured while sending http request : " + e);
        } catch (Exception e) {
            System.out.println("exception occured while sending http request : " + e);
        } finally {
            httpGet.releaseConnection();
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Rules methods">
    private void factoryColumns() {
//        clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
//        clnSelected.setCellValueFactory((cellData) -> {
//            ReceberCartao cellValue = cellData.getValue();
//            BooleanProperty property = cellValue.selectedProperty();
//            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
//            return property;
//        });
        clnStatus.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReceberCartao, ReceberCartao>, ObservableValue<ReceberCartao>>() {
            @Override
            public ObservableValue<ReceberCartao> call(TableColumn.CellDataFeatures<ReceberCartao, ReceberCartao> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnStatus.setComparator(new Comparator<ReceberCartao>() {
            @Override
            public int compare(ReceberCartao p1, ReceberCartao p2) {
                return p1.getData_receber().compareTo(p2.getData_receber());
            }
        });
        clnStatus.setCellFactory(new Callback<TableColumn<ReceberCartao, ReceberCartao>, TableCell<ReceberCartao, ReceberCartao>>() {
            @Override
            public TableCell<ReceberCartao, ReceberCartao> call(TableColumn<ReceberCartao, ReceberCartao> btnCol) {
                return new TableCell<ReceberCartao, ReceberCartao>() {
                    final ImageView buttonGraphic = new ImageView();
                    final Button button = new Button();

                    {
                        button.setGraphic(buttonGraphic);
                        button.setMinWidth(130);
                    }

                    @Override
                    public void updateItem(ReceberCartao recibo, boolean empty) {
                        super.updateItem(recibo, empty);
                        if (recibo != null) {
                            if (recibo.getStatus().equals("APPROVED")) {
                                button.setText("Cancelar");
                                buttonGraphic.setImage(btnImage);
                                button.getStyleClass().add("danger");

                                getStyleClass().remove("statusBoletoVencido");
                                getStyleClass().remove("statusLigarCliente");
                                setGraphic(button);
                                button.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        if (!GUIUtils.showQuestionMessageDefaultNao("Esse procedimento é irreversível.\n\nDeseja realmente cancelar o recebimento?")) {
                                            return;
                                        }

                                        button.disableProperty().set(true);
                                        recibo.setStatus(getCancelPayment(recibo));
                                        button.disableProperty().set(false);
                                        tblRecebimentos.refresh();
                                    }
                                });
                            } else if (recibo.getStatus().equals("CANCELING")) {
                                button.setText("Ver Status");
                                buttonGraphic.setImage(btnViewImage);
                                button.getStyleClass().add("info");

                                getStyleClass().remove("statusBoletoVencido");
                                getStyleClass().remove("statusLigarCliente");
                                setGraphic(button);
                                button.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        button.disableProperty().set(true);
                                        if (getCancelStatus(recibo)) {
                                            recibo.setStatus("CANCELED");
                                        }
                                        button.disableProperty().set(false);
                                        tblRecebimentos.refresh();
                                    }
                                });
                            } else {
                                getStyleClass().add("statusBoletoVencido");
                                setText(recibo.getStatus());
                                setGraphic(null);
                            }
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });

        clnCodCli.setCellFactory((TableColumn<ReceberCartao, String> param) -> {
            TableCell cell = new TableCell<ReceberCartao, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ReceberCartao currentReserva = currentRow == null ? null : (ReceberCartao) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "CANCELED":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "CANCELING":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clnCartao.setCellFactory((TableColumn<ReceberCartao, String> param) -> {
            TableCell cell = new TableCell<ReceberCartao, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ReceberCartao currentReserva = currentRow == null ? null : (ReceberCartao) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "CANCELED":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "CANCELING":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clnNomeCli.setCellFactory((TableColumn<ReceberCartao, String> param) -> {
            TableCell cell = new TableCell<ReceberCartao, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ReceberCartao currentReserva = currentRow == null ? null : (ReceberCartao) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "CANCELED":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "CANCELING":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clnPedido.setCellFactory((TableColumn<ReceberCartao, String> param) -> {
            TableCell cell = new TableCell<ReceberCartao, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ReceberCartao currentReserva = currentRow == null ? null : (ReceberCartao) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "CANCELED":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "CANCELING":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };
            return cell;
        });

        clnValor.setCellFactory((TableColumn<ReceberCartao, Double> param) -> {
            TableCell cell = new TableCell<ReceberCartao, Double>() {

                @Override
                public void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    NumberFormat formatValueCell = NumberFormat.getCurrencyInstance(BRAZIL);
                    formatValueCell.setMaximumFractionDigits(2);
                    setText(empty ? null : formatValueCell.format(getString()));
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    ReceberCartao currentReserva = currentRow == null ? null : (ReceberCartao) currentRow.getItem();
                    if (currentReserva != null) {
                        String status = currentReserva.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused() && status != null) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");

                }

                private void setPriorityStyle(String priority) {
                    switch (priority) {
                        case "CANCELED":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "CANCELING":
                            getStyleClass().add("statusLigarCliente");
                            break;
                    }
                }

                private Double getString() {
                    return getItem() == null ? 0.0 : getItem();
                }
            };
            return cell;
        });

    }

    private Double getParcialValue() {
        Double partialValue = 0.0;
        Boolean acceptValue = false;
        do {
            String strNewValue = GUIUtils.showTextFieldDialog("Valor parcial a receber:");
            try {
                partialValue = Double.parseDouble(strNewValue);
                acceptValue = true;
            } catch (NumberFormatException ex) {
                GUIUtils.showMessage("Digite um valor válido para recebimento parcial. 0000,00", Alert.AlertType.WARNING);
                acceptValue = false;
            }
        } while (!acceptValue);
        return partialValue;
    }

    private Boolean connectApi() {
        String restUrl = API_HOMOLOGACAO + MOD_AUTH;
        String username = USER;
        String password = PASSWORD;

        executeOAuth(restUrl, username, password, "scope=oob&grant_type=client_credentials");
        if (message != null) {
            GUIUtils.showMessage("Erro durante a conexão com o banco."
                    + "\nERRO: " + message.getError()
                    + "\nMENSAGEM: " + message.getError_description(), Alert.AlertType.ERROR);
            return false;
        }
        authToken.setTime_init(LocalTime.now());
        return true;
    }

    private Boolean getTokenCard(String card_number) {
        String restUrl = API_HOMOLOGACAO + MOD_TOKEN_CARD;

        executeTokenCard(restUrl, authToken, "{ \"card_number\": \"" + card_number + "\"}");
        if (message != null) {
            GUIUtils.showMessage("Erro durante a tokenização do cartão."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private Boolean getCardVerification(CardPayment card) {
        String restUrl = API_HOMOLOGACAO + MOD_CARD_VERIFICATION;

        executeCardVerification(restUrl, authToken, card);
        if (message != null) {
            GUIUtils.showMessage("Erro durante a verificação do cartão."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private Boolean getPayment(Payment payment) {
        String restUrl = API_HOMOLOGACAO + MOD_CARD_PAYMENT;

        executePayment(restUrl, authToken, payment);
        if (message != null) {
            GUIUtils.showMessage("Erro durante o pagamento."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private String getCancelPayment(ReceberCartao recibo) {
        String returnApi = recibo.getStatus();

        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date_pay = dateFormat.parse(recibo.getData_receber());
            Date date_now = dateFormat.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            Boolean differDates = date_now.after(date_pay);

            if (!differDates) {
                if (getCancelPayZeroDay(recibo)) {
                    returnApi = cancelPay.status;
                }
            } else {
                if (getCancelRequest(recibo)) {
                    returnApi = "CANCELING";
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            return returnApi;
        }
        return returnApi;
    }

    private Boolean getCancelPayZeroDay(ReceberCartao recibo) {
        String restUrl = API_HOMOLOGACAO;

        if (authToken == null) {
            if (!this.connectApi()) {
                return false;
            }
        }

        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            if (!this.connectApi()) {
                return false;
            }
        }

        executeCancelZeroDay(restUrl, authToken, recibo.getPayment_id());
        if (message != null) {
            GUIUtils.showMessage("Erro durante a verificação do cartão."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        try {
            String data_cancel = cancelPay.credit_cancel.canceled_at.substring(0, 10) + " "
                    + cancelPay.credit_cancel.canceled_at.substring(11, 19);
            DAOFactory.getReceberCartaoDAO().cancel_transaction(recibo.getNumero(), data_cancel, cancelPay.payment_id, "", cancelPay.status);
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showMessage("Erro ao registrar cancelamento no banco de dados.\n"
                    + "Recibo: " + recibo.getNumero()
                    + "\nPayment ID: " + cancelPay.payment_id
                    + "\n\nEncaminhe as informações para o TI.",
                    Alert.AlertType.ERROR);
            GUIUtils.showException(ex);
        }
        return true;
    }

    private Boolean getCancelRequest(ReceberCartao recibo) {
        String restUrl = API_HOMOLOGACAO + MOD_CANCEL_REQUEST;

        if (authToken == null) {
            if (!this.connectApi()) {
                return false;
            }
        }

        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            if (!this.connectApi()) {
                return false;
            }
        }

        RequestCancel requestCancel = new RequestCancel(recibo.getPayment_id(), ((Double) (recibo.getValor() * 100.0)).intValue(), recibo.getNumero());
        executeRequestCancel(restUrl, authToken, requestCancel);
        if (message != null) {
            GUIUtils.showMessage("Erro durante a verificação do cartão."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        if (!returnCancelRequest.status.equals("ACCEPTED")) {
            GUIUtils.showMessage("Cancelamento de recebimento não autorizado."
                    + "\nConsulte o financeiro/operadora do cartão (GETNET) com as informações:"
                    + "\nPayment ID: " + returnCancelRequest.payment_id
                    + "\nRequest ID: " + returnCancelRequest.cancel_request_id
                    + "\nCustom Key: " + returnCancelRequest.cancel_custom_key
                    + "\nStatus: " + returnCancelRequest.status,
                    Alert.AlertType.ERROR);
            return false;
        }
        try {
            String data_cancel = returnCancelRequest.cancel_request_at.substring(0, 10) + " "
                    + returnCancelRequest.cancel_request_at.substring(11, 19);
            DAOFactory.getReceberCartaoDAO().cancel_transaction(recibo.getNumero(), data_cancel, returnCancelRequest.payment_id,
                    returnCancelRequest.cancel_request_id, "CANCELING");
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showMessage("Erro ao registrar cancelamento no banco de dados.\n"
                    + "Recibo: " + recibo.getNumero()
                    + "\nPayment ID: " + returnCancelRequest.payment_id
                    + "\nRequest ID: " + returnCancelRequest.cancel_request_id
                    + "\nStatus: " + returnCancelRequest.status
                    + "\n\nEncaminhe as informações para o TI.",
                    Alert.AlertType.ERROR);
            GUIUtils.showException(ex);
        }
        return true;
    }

    private Boolean getCancelStatus(ReceberCartao recibo) {
        String restUrl = API_HOMOLOGACAO;

        if (authToken == null) {
            if (!this.connectApi()) {
                return false;
            }
        }

        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            if (!this.connectApi()) {
                return false;
            }
        }

        executeCancelStatus(restUrl, authToken, recibo);
        if (message != null) {
            GUIUtils.showMessage("Erro durante a verificação do cartão."
                    + "\nERRO: " + message.getMessage()
                    + "\nALERTA: " + message.getName()
                    + "\nDETALHE: " + message.getDetails().get(0).status + ":" + message.getDetails().get(0).error_code
                    + "\nMENSAGEM: " + message.getDetails().get(0).description_detail, Alert.AlertType.ERROR);
            return false;
        }
        if (!statusCancelRequest.status_processing_cancel_code.equals("000")) {
            GUIUtils.showMessage("Status do cancelamento solicitado."
                    + "\nConsulte o financeiro/operadora do cartão (GETNET) para mais informações:"
                    + "\nStatus CODE: " + statusCancelRequest.status_processing_cancel_code
                    + "\nStatus MESSAGE: " + statusCancelRequest.status_processing_cancel_message
                    + "\nCancel Request ID: " + statusCancelRequest.cancel_request_id,
                    Alert.AlertType.INFORMATION);
            return false;
        }
        try {
            DAOFactory.getReceberCartaoDAO().status_transaction(recibo.getNumero(), "CANCELED");
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showMessage("Erro ao registrar cancelamento no banco de dados.\n"
                    + "Recibo: " + recibo.getNumero()
                    + "\nPayment ID: " + statusCancelRequest.payment_id
                    + "\nRequest ID: " + statusCancelRequest.cancel_request_id
                    + "\nStatus CODE: " + statusCancelRequest.status_processing_cancel_code
                    + "\n\nEncaminhe as informações para o TI.",
                    Alert.AlertType.ERROR);
            GUIUtils.showException(ex);
        }
        return true;
    }

    private String getJsonStr(InputStream contentWsReturn) throws IOException {
        StringBuffer jsonStr = new StringBuffer();
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(contentWsReturn));
        while ((line = reader.readLine()) != null) {
            jsonStr.append(line);
        }
        return jsonStr.toString();
    }

    private void unbindDataPagamento() {
        tboxCliente.clear();
        tboxDocto.clear();
        tboxGrupo.clear();
        tboxEndereco.clear();
        tboxNumeroEnd.clear();
        tboxBairro.clear();
        tboxCidade.clear();
        tboxUf.clear();
        tboxTelefone.clear();
        tboxEmail.clear();
        tboxObservacao.clear();
        tboxValor.clear();
        tboxDesconto.clear();
        tboxTotal.clear();
        tboxValorParcial.clear();
        tsValorParcial.switchedOnProperty().set(false);

        tboxNumeroCartaoQuad1.clear();
        tboxNumeroCartaoQuad2.clear();
        tboxNumeroCartaoQuad3.clear();
        tboxNumeroCartaoQuad4.clear();
        tboxNomeCartao.clear();
        tboxCvv.clear();
        tboxMes.getSelectionModel().select(0);
        tboxAno.getSelectionModel().select(0);
        tboxParcelas.getSelectionModel().select(0);
    }

    private void bindDataPagamento(Pedido pedido) {
        if (pedido != null) {
            tboxCliente.setText(pedido.getCliente());
            tboxDocto.setText(pedido.getDocumento());
            tboxGrupo.setText(pedido.getGrupo());
            tboxEndereco.setText(pedido.getEndereco());
            tboxNumeroEnd.setText(pedido.getNumero_end());
            tboxBairro.setText(pedido.getBairro());
            tboxCidade.setText(pedido.getCidade());
            tboxUf.setText(pedido.getUf());
            tboxTelefone.setText(pedido.getTelefone());
            tboxEmail.setText(pedido.getEmail());
            tboxObservacao.setText(pedido.getObservacao());
            if (pedido.getValor_recebi() > 0.0) {
                tboxObservacao.setText((pedido.getObservacao() != null ? pedido.getObservacao() : "").concat("\nJá foi recebido com cartão o valor de: " + currValue.format(pedido.getValor_recebi())));
            }

            tboxValor.setText(currValue.format(pedido.getValor()));
            tboxDesconto.setText(currValue.format(pedido.getDesconto()));
            tboxTotal.setText(currValue.format(pedido.getTotal()));
        }
    }

    private Boolean isValidCard(String cardNumber) {
        int sum = 0;
        boolean alternate = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            int n = Integer.parseInt(cardNumber.substring(i, i + 1));
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML actions">
    @FXML
    private void tboxNumeroOnKeyRelease(KeyEvent event) {
        unbindDataPagamento();
        if (!tsTipoReceber.switchedOnProperty().get()) {
            if (tboxNumero.getText().length() == 10) {
                try {
                    pedidoPagamento = DAOFactory.getPedidoDAO().getPedido(tboxNumero.getText());
                    if (pedidoPagamento != null) {
                        bindDataPagamento(pedidoPagamento);
                        lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal()));
                        tboxNumeroAgrupado.clear();
                        tboxNumeroAgrupado.requestFocus();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        } else {
            if (tboxNumero.getText().length() >= 6) {
                try {
                    pedidoPagamento = DAOFactory.getReceberDAO().getReceber(tboxNumero.getText());
                    if (pedidoPagamento != null) {
                        bindDataPagamento(pedidoPagamento);
                        lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal()));
                        tboxNumeroAgrupado.clear();
                        tboxNumeroAgrupado.requestFocus();
                        valorOriginal = pedidoPagamento.getValor();
                        descontoOriginal = pedidoPagamento.getDesconto();
                        totalOriginal = pedidoPagamento.getTotal();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        }
    }

    @FXML
    private void tboxNumeroAgrupadoOnKeyRelease(KeyEvent event) {
        if (!tsTipoReceber.switchedOnProperty().get()) {
            if (tboxNumeroAgrupado.getText().length() == 10) {
                try {
                    Pedido pedidoAgrupadoPagamento = DAOFactory.getPedidoDAO().getPedido(tboxNumeroAgrupado.getText());
                    if (pedidoAgrupadoPagamento != null) {
                        if (!pedidoAgrupadoPagamento.getCodcli().equals(pedidoPagamento.getCodcli()) && pedidoAgrupadoPagamento.getGrupo_eco().equals(pedidoPagamento.getGrupo_eco())) {
                            GUIUtils.showMessage("O cliente do pedido agrupado é diferente do pedido principal.", Alert.AlertType.WARNING);
                            tboxNumeroAgrupado.clear();
                            tboxNumeroAgrupado.requestFocus();
                            return;
                        }
                        unbindDataPagamento();
                        pedidoPagamento.setValor(pedidoPagamento.getValor() + pedidoAgrupadoPagamento.getValor());
                        pedidoPagamento.setDesconto(pedidoPagamento.getDesconto() + pedidoAgrupadoPagamento.getDesconto());
                        pedidoPagamento.setTotal(pedidoPagamento.getTotal() + pedidoAgrupadoPagamento.getTotal());
                        bindDataPagamento(pedidoPagamento);
                        lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal()));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        } else {
            if (tboxNumeroAgrupado.getText().length() >= 6) {
                String[] titulosAgrupados = tboxNumeroAgrupado.getText().split(",");
                Double valorAgrupado = 0.0;
                Double descontoAgrupado = 0.0;
                Double totalAgrupado = 0.0;

                for (String titulo : titulosAgrupados) {
                    try {
                        Pedido pedidoAgrupadoPagamento = DAOFactory.getReceberDAO().getReceber(titulo);
                        if (pedidoAgrupadoPagamento != null) {
                            if (!pedidoAgrupadoPagamento.getCodcli().equals(pedidoPagamento.getCodcli()) && !pedidoAgrupadoPagamento.getGrupo_eco().equals(pedidoPagamento.getGrupo_eco())) {
                                GUIUtils.showMessage("O cliente do titulo agrupado é diferente do título principal.", Alert.AlertType.WARNING);
                                unbindDataPagamento();
                                pedidoPagamento.setValor(valorOriginal);
                                pedidoPagamento.setDesconto(descontoOriginal);
                                pedidoPagamento.setTotal(totalOriginal);
                                bindDataPagamento(pedidoPagamento);
                                lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal()));

                                tboxNumeroAgrupado.clear();
                                tboxNumeroAgrupado.requestFocus();
                                return;
                            }
                            valorAgrupado += pedidoAgrupadoPagamento.getValor();
                            descontoAgrupado += pedidoAgrupadoPagamento.getDesconto();
                            totalAgrupado += pedidoAgrupadoPagamento.getTotal();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                }
                unbindDataPagamento();
                pedidoPagamento.setValor(valorOriginal + valorAgrupado);
                pedidoPagamento.setDesconto(descontoOriginal + descontoAgrupado);
                pedidoPagamento.setTotal(totalOriginal + totalAgrupado);
                bindDataPagamento(pedidoPagamento);
                lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal()));
            }
        }
    }

    @FXML
    private void tboxValorParcialOnKeyRelease(KeyEvent event) {
        try {
            String textValue = tboxValorParcial.getText().replace(",", ".");
            pedidoPagamento.setTotal(Double.parseDouble(textValue));
            lbValorParcela.setText(currValue.format(pedidoPagamento.getTotal() / Integer.parseInt(tboxParcelas.getValue())));
        } catch (NumberFormatException ex) {
            GUIUtils.showMessage("Digite um valor numérico válido, utilize vírgula (,) para separar as casas decimais.", Alert.AlertType.INFORMATION);
            tboxValorParcial.clear();
            tboxValorParcial.requestFocus();
        }
    }

    @FXML
    private void btnPagarOnAction(ActionEvent event) {

        btnCancelar.disableProperty().set(true);
        String cardNumber = tboxNumeroCartaoQuad1.getText().concat(tboxNumeroCartaoQuad2.getText().concat(tboxNumeroCartaoQuad3.getText().concat(tboxNumeroCartaoQuad4.getText())));
        CardType typeCard = CardType.detect(cardNumber);

        //<editor-fold defaultstate="collapsed" desc="Form validation">
        if (tboxNumero.getLength() != 10 && !tsTipoReceber.switchedOnProperty().get()) {
            GUIUtils.showMessage("Digite um número de pedido válido. (10 dígitos)", Alert.AlertType.INFORMATION);
            tboxNumero.requestFocus();
            return;
        }
        if (tboxNumero.getLength() < 6 && tsTipoReceber.switchedOnProperty().get()) {
            GUIUtils.showMessage("Digite um número de título válido. (min. 6 dígitos)", Alert.AlertType.INFORMATION);
            tboxNumero.requestFocus();
            return;
        }
        if (tboxCliente.getText().isEmpty()) {
            GUIUtils.showMessage("Pedido não existente na base de dados.", Alert.AlertType.INFORMATION);
            tboxNumero.requestFocus();
            return;
        }
        if (tboxNomeCartao.getText().isEmpty()) {
            GUIUtils.showMessage("Digite o nome impresso no seu cartão.", Alert.AlertType.INFORMATION);
            tboxNomeCartao.requestFocus();
            return;
        }
        if (tboxCvv.getText().isEmpty()) {
            GUIUtils.showMessage("Digite o código de segurança do cartão.", Alert.AlertType.INFORMATION);
            tboxCvv.requestFocus();
            return;
        }
        if (tboxMes.getValue().isEmpty()) {
            GUIUtils.showMessage("Selecione o mês de validade do cartão.", Alert.AlertType.INFORMATION);
            return;
        }
        if (tboxAno.getValue().isEmpty()) {
            GUIUtils.showMessage("Selecione o mês de validade do cartão.", Alert.AlertType.INFORMATION);
            return;
        }
        if (!this.isValidCard(cardNumber)) {
            GUIUtils.showMessage("Digite um número de cartão válido.", Alert.AlertType.INFORMATION);
            tboxNumeroCartaoQuad1.requestFocus();
            return;
        }
        if (typeCard == CardType.UNKNOWN) {
            GUIUtils.showMessage("Você pode utilizar somente as bandeiras: Mastercard, Visa, Amex, Elo ou Hipercard.", Alert.AlertType.INFORMATION);
            tboxNumeroCartaoQuad1.requestFocus();
            return;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="oAuth validation">
        if (authToken == null) {
            if (!this.connectApi()) {
                return;
            }
        }

        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            if (!this.connectApi()) {
                return;
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Tokenização and Verification card">
        if (!this.getTokenCard(cardNumber)) {
            return;
        }

        String brandCard = typeCard.name();
        CardPayment card = new CardPayment(cardToken.number_token, brandCard, tboxNomeCartao.getText(), tboxMes.getValue(), tboxAno.getValue(), tboxCvv.getText());

        //A verificação do cartão é feito somente para MASTER ou VISA, conforme documentação API
        if (typeCard == CardType.mastercard || typeCard == CardType.visa) {
            if (!this.getCardVerification(card)) {
                return;
            }

            if (!this.cardVerification.status.equals("VERIFIED")) {
                GUIUtils.showMessage("O cartão não pode ser verificado pela operadora. Cartão Inválido/Vencido ou Estabelecimento Inválido!", Alert.AlertType.INFORMATION);
                return;
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Payment billing">
        Order order = new Order(tboxNumero.getText().replace("/", "."));
        BillingAddress billing_address = new BillingAddress(pedidoPagamento.getEndereco(), pedidoPagamento.getNumero_end(), "", pedidoPagamento.getBairro(),
                pedidoPagamento.getCidade(), pedidoPagamento.getUf(), "Brasil", pedidoPagamento.getCep());
        String[] nomeSeparado = pedidoPagamento.getCliente().split(" ");
        String last_name = nomeSeparado[nomeSeparado.length - 1];
        nomeSeparado[nomeSeparado.length - 1] = " ";
        String first_name = Arrays.stream(nomeSeparado).collect(Collectors.joining(" ")).trim();
        Customer customer = new Customer(pedidoPagamento.getCodcli(), pedidoPagamento.getCliente(), first_name, last_name, pedidoPagamento.getDoc_tipo(), pedidoPagamento.getDocumento(), billing_address);
        Credit credit = new Credit(Boolean.FALSE, Boolean.FALSE, tboxParcelas.getValue().equals("1") ? "FULL" : "INSTALL_NO_INTEREST", Integer.parseInt(tboxParcelas.getValue()), card);

        if (!GUIUtils.showQuestionMessageDefaultNao("Confirme os valores de pagamento:\n\n"
                + "VALOR TOTAL: " + currValue.format(pedidoPagamento.getTotal()) + "\n"
                + "QTDE PARCELAS: " + tboxParcelas.getValue() + "\n"
                + "VALOR PARCELA: " + lbValorParcela.getText())) {
            return;
        }

        Integer amount = 0;
        amount = ((Double) (pedidoPagamento.getTotal() * 100.0)).intValue();
        Payment billing = new Payment(SELLER_ID, amount, "BRL", order, customer, credit);
        if (!this.getPayment(billing)) {
            return;
        }
        if (returnPayment.status.equals("CANCELED")) {
            GUIUtils.showMessage("Compra com cartão de crédito cancelada!", Alert.AlertType.INFORMATION);
            return;
        } else if (returnPayment.status.equals("DENIED")) {
            GUIUtils.showMessage("Compra com cartão de crédito negada!", Alert.AlertType.WARNING);
            return;
        } else if (returnPayment.status.equals("UNPROCESSED")) {
            GUIUtils.showMessage("Compra com cartão de crédito não processada!", Alert.AlertType.INFORMATION);
            return;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Persist payment database">
        String data_rec = returnPayment.credit.authorized_at.substring(0, 10) + " " + returnPayment.credit.authorized_at.substring(12, 19);
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormat.parse(data_rec);
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        } catch (ParseException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String numero_pagto = DAOFactory.getReceberCartaoDAO().save_transaction(returnPayment.payment_id, returnPayment.credit.authorization_code, returnPayment.credit.terminal_nsu,
                    returnPayment.credit.acquirer_transaction_id, (cardVerification != null ? cardVerification.verification_id : "CARTAO NAO VERIFICADO"), amount.doubleValue() / 100.0,
                    returnPayment.status, pedidoPagamento.getNumero(), Integer.parseInt(tboxParcelas.getValue()), brandCard, cardToken.number_token, authToken.access_token, tboxCvv.getText(),
                    tboxMes.getValue() + "/" + tboxAno.getValue(), tboxNumeroCartaoQuad1.getText() + "********" + tboxNumeroCartaoQuad4.getText(), data_rec, tboxNumeroAgrupado.getText());
            pay_billing = new ReceberCartao(numero_pagto, returnPayment.payment_id, returnPayment.credit.authorization_code, pedidoPagamento.getNumero(), amount.doubleValue() / 100.0,
                    tboxNumeroCartaoQuad1.getText() + "********" + tboxNumeroCartaoQuad4.getText(), brandCard, Integer.parseInt(tboxParcelas.getValue()), returnPayment.status,
                    data_rec, pedidoPagamento.getCodcli(), pedidoPagamento.getCliente(), tboxNumeroAgrupado.getText());
            tblRecebimentos.getItems().add(pay_billing);
            tblRecebimentos.refresh();
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            WritableImage image = anchorPagamento.snapshot(new SnapshotParameters(), null);
            File file = new File("C:/SysDelizLocal/local_files/printPagamentoView.png");
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                MailUtils.sendMailErroPersistencia(returnPayment.credit.authorization_code, returnPayment.payment_id, returnPayment.credit.terminal_nsu,
                        returnPayment.credit.acquirer_transaction_id, (cardVerification == null ? "CARTAO NAO VERIFICADO" : cardVerification.verification_id),
                        returnPayment.status, cardToken.number_token, authToken.access_token);
            } catch (IOException exce) {
                Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, exce);
                GUIUtils.showException(exce);
            }
            GUIUtils.showMessage("Erro ao registrar pagamento no banco de dados.\n"
                    + "Authorization CODE: " + returnPayment.credit.authorization_code
                    + "\nPayment ID: " + returnPayment.payment_id
                    + "\nTerminal NSU: " + returnPayment.credit.terminal_nsu
                    + "\nAcquirer Transaction ID: " + returnPayment.credit.acquirer_transaction_id
                    + "\nVerification ID: " + (cardVerification == null ? "CARTAO NAO VERIFICADO" : cardVerification.verification_id)
                    + "\nReturn Payment STATUS: " + returnPayment.status
                    + "\nCard Number TOKEN: " + cardToken.number_token
                    + "\nOAuth Access TOKEN: " + authToken.access_token
                    + "\n\nEncaminhe as informações para o TI.",
                    Alert.AlertType.ERROR);
            GUIUtils.showException(ex);
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Certificate of payment">
        try {
            new SceneCompPagamentoController(Modals.FXMLWindow.FinanceiroSceneCompPagamento, pay_billing, pedidoPagamento.getEmail());
        } catch (IOException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        //</editor-fold>

        this.unbindDataPagamento();
        tboxNumeroAgrupado.clear();
        tboxNumero.clear();
        tboxNumero.requestFocus();
        btnCancelar.disableProperty().set(false);
    }

    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        this.unbindDataPagamento();
        tboxNumeroAgrupado.clear();
        tboxNumero.clear();
        tboxNumero.requestFocus();
        btnCancelar.disableProperty().set(false);
    }

    @FXML
    private void btnProcurarReceberOnAction(ActionEvent event) {
        try {
            tblRecebimentos.setItems(DAOFactory.getReceberCartaoDAO().get_form(tboxPedido_C.getText(), tboxCliente_C.getText(),
                    tboxDataRec_C.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        } catch (SQLException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void cmMenuEnviarComprovanteOnAction(ActionEvent event) {
        ReceberCartao selectedPayment = tblRecebimentos.getSelectionModel().getSelectedItem();
        if (selectedPayment != null) {
            try {
                String strPedidoPago = selectedPayment.getPedido();
                String strCodCli = selectedPayment.getCodcli();
                if (selectedPayment.getPedido().contains("AGRUP")) {
                    strPedidoPago = strPedidoPago.replace("AGRUP [", "").replace("]", "").split(",")[0];
                }
                Cliente clientPayment = DAOFactory.getClienteDAO().getByCodcli(strCodCli);
                new SceneCompPagamentoController(Modals.FXMLWindow.FinanceiroSceneCompPagamento, selectedPayment, clientPayment.getEmail());
            } catch (IOException ex) {
                Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            } catch (SQLException ex) {
                Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void cmMenuCancelarPagamentoOnAction(ActionEvent event) {
        ReceberCartao selectedPayment = tblRecebimentos.getSelectionModel().getSelectedItem();
        if (selectedPayment != null) {
            if (!GUIUtils.showQuestionMessageDefaultNao("Esse procedimento é irreversível.\n\nDeseja realmente cancelar o recebimento?")) {
                return;
            }

            selectedPayment.setStatus(getCancelPayment(selectedPayment));
            tblRecebimentos.refresh();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="TextField NUMERO CARTÃO">
    @FXML
    private void tboxNumeroCartaoQuad1OnKeyRelease(KeyEvent event) {
        if (tboxNumeroCartaoQuad1.getLength() == 4) {
            tboxNumeroCartaoQuad2.requestFocus();
        }
    }

    @FXML
    private void tboxNumeroCartaoQuad2OnKeyRelease(KeyEvent event) {
        if (tboxNumeroCartaoQuad2.getLength() == 4) {
            tboxNumeroCartaoQuad3.requestFocus();
        }
    }

    @FXML
    private void tboxNumeroCartaoQuad3OnKeyRelease(KeyEvent event) {
        if (tboxNumeroCartaoQuad3.getLength() == 4) {
            tboxNumeroCartaoQuad4.requestFocus();
        }
    }

    @FXML
    private void tboxNumeroCartaoQuad4OnKeyRelease(KeyEvent event) {
        String cardNumber = tboxNumeroCartaoQuad1.getText().concat(tboxNumeroCartaoQuad2.getText().concat(tboxNumeroCartaoQuad3.getText().concat(tboxNumeroCartaoQuad4.getText())));
        CardType typeCard = CardType.detect(cardNumber);

        if (typeCard == CardType.UNKNOWN) {
            imgCardType.setImage(imgUnknown);
        } else if (typeCard == CardType.amex) {
            imgCardType.setImage(imgAmex);
        } else if (typeCard == CardType.elo) {
            imgCardType.setImage(imgElo);
        } else if (typeCard == CardType.hipercard) {
            imgCardType.setImage(imgHiper);
        } else if (typeCard == CardType.mastercard) {
            imgCardType.setImage(imgMaster);
        } else if (typeCard == CardType.visa) {
            imgCardType.setImage(imgVisa);
        }

        if (tboxNumeroCartaoQuad4.getLength() == 4) {
            tboxNomeCartao.requestFocus();
        }
    }

    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="API JSON class return">
    private class ApiErrorReturn {

        String error;
        String error_description;
        List<DetailsError> details;
        String message;
        String name;
        Integer status_code;

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public String getError_description() {
            return error_description;
        }

        public void setError_description(String error_description) {
            this.error_description = error_description;
        }

        public List<DetailsError> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsError> details) {
            this.details = details;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getStatus_code() {
            return status_code;
        }

        public void setStatus_code(Integer status_code) {
            this.status_code = status_code;
        }

    }

    private class DetailsError {

        String status;
        String error_code;
        String description;
        String description_detail;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getError_code() {
            return error_code;
        }

        public void setError_code(String error_code) {
            this.error_code = error_code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription_detail() {
            return description_detail;
        }

        public void setDescription_detail(String description_detail) {
            this.description_detail = description_detail;
        }

    }

    private class OAuthToken {

        private String access_token;
        private String token_type;
        private Integer expires_in;
        private LocalTime time_init;
        private String scope;

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public Integer getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(Integer expires_in) {
            this.expires_in = expires_in;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public LocalTime getTime_init() {
            return time_init;
        }

        public void setTime_init(LocalTime time_init) {
            this.time_init = time_init;
        }

    }

    private class CardToken {

        private String number_token;

        public String getNumber_token() {
            return number_token;
        }

        public void setNumber_token(String number_token) {
            this.number_token = number_token;
        }

    }

    private class CardPayment {

        String number_token;
        String brand;
        String cardholder_name;
        String expiration_month;
        String expiration_year;
        String security_code;

        public CardPayment(String number_token, String brand, String cardholder_name, String expiration_month, String expiration_year, String security_code) {
            this.number_token = number_token;
            this.brand = brand;
            this.cardholder_name = cardholder_name;
            this.expiration_month = expiration_month;
            this.expiration_year = expiration_year;
            this.security_code = security_code;
        }

        public String getNumber_token() {
            return number_token;
        }

        public void setNumber_token(String number_token) {
            this.number_token = number_token;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCardholder_name() {
            return cardholder_name;
        }

        public void setCardholder_name(String cardholder_name) {
            this.cardholder_name = cardholder_name;
        }

        public String getExpiration_month() {
            return expiration_month;
        }

        public void setExpiration_month(String expiration_month) {
            this.expiration_month = expiration_month;
        }

        public String getExpiration_year() {
            return expiration_year;
        }

        public void setExpiration_year(String expiration_year) {
            this.expiration_year = expiration_year;
        }

        public String getSecurity_code() {
            return security_code;
        }

        public void setSecurity_code(String security_code) {
            this.security_code = security_code;
        }

    }

    private class CardVerification {

        String status;
        String verification_id;
        String authorization_code;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVerification_id() {
            return verification_id;
        }

        public void setVerification_id(String verification_id) {
            this.verification_id = verification_id;
        }

        public String getAuthorization_code() {
            return authorization_code;
        }

        public void setAuthorization_code(String authorization_code) {
            this.authorization_code = authorization_code;
        }

    }

    private class Payment {

        String seller_id;
        Integer amount;
        String currency;
        Order order;
        Customer customer;
        Credit credit;

        public Payment(String seller_id, Integer amount, String currency, Order order, Customer customer, Credit credit) {
            this.seller_id = seller_id;
            this.amount = amount;
            this.currency = currency;
            this.order = order;
            this.customer = customer;
            this.credit = credit;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Order getOrder() {
            return order;
        }

        public void setOrder(Order order) {
            this.order = order;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Credit getCredit() {
            return credit;
        }

        public void setCredit(Credit credit) {
            this.credit = credit;
        }

    }

    private class Order {

        String order_id;
        String product_type;

        public Order(String order_id) {
            this.order_id = order_id;
            this.product_type = "physical_goods";
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getProduct_type() {
            return product_type;
        }

        public void setProduct_type(String product_type) {
            this.product_type = product_type;
        }

    }

    private class Customer {

        String customer_id;
        String name;
        String first_name;
        String last_name;
        String document_type;
        String document_number;
        BillingAddress billing_address;

        public Customer(String customer_id, String name, String first_name, String last_name, String document_type, String document_number, BillingAddress billing_address) {
            this.customer_id = customer_id;
            this.name = name;
            this.first_name = first_name;
            this.last_name = last_name;
            this.document_type = document_type;
            this.document_number = document_number.replaceAll("[^0-9]", "");
            this.billing_address = billing_address;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    
        public String getFirst_name() {
            return first_name;
        }
    
        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }
    
        public String getLast_name() {
            return last_name;
        }
    
        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }
    
        public String getDocument_type() {
            return document_type;
        }

        public void setDocument_type(String document_type) {
            this.document_type = document_type;
        }

        public String getDocument_number() {
            return document_number;
        }

        public void setDocument_number(String document_number) {
            this.document_number = document_number;
        }

        public BillingAddress getBilling_address() {
            return billing_address;
        }

        public void setBilling_address(BillingAddress billing_address) {
            this.billing_address = billing_address;
        }

    }

    private class BillingAddress {

        String street;
        String number;
        String complement;
        String district;
        String city;
        String state;
        String country;
        String postal_code;

        public BillingAddress(String street, String number, String complement, String district, String city, String state, String country, String postal_code) {
            this.street = street;
            this.number = number;
            this.complement = complement;
            this.district = district;
            this.city = city;
            this.state = state;
            this.country = country;
            this.postal_code = postal_code;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getComplement() {
            return complement;
        }

        public void setComplement(String complement) {
            this.complement = complement;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostal_code() {
            return postal_code;
        }

        public void setPostal_code(String postal_code) {
            this.postal_code = postal_code;
        }

    }

    private class Credit {

        Boolean delayed;
        Boolean save_card_data;
        String transaction_type;
        Integer number_installments;
        CardPayment card;

        public Credit(Boolean delayed, Boolean save_card_data, String transaction_type, Integer number_installments, CardPayment card) {
            this.delayed = delayed;
            this.save_card_data = save_card_data;
            this.transaction_type = transaction_type;
            this.number_installments = number_installments;
            this.card = card;
        }

        public Boolean getDelayed() {
            return delayed;
        }

        public void setDelayed(Boolean delayed) {
            this.delayed = delayed;
        }

        public Boolean getSave_card_data() {
            return save_card_data;
        }

        public void setSave_card_data(Boolean save_card_data) {
            this.save_card_data = save_card_data;
        }

        public String getTransaction_type() {
            return transaction_type;
        }

        public void setTransaction_type(String transaction_type) {
            this.transaction_type = transaction_type;
        }

        public Integer getNumber_installments() {
            return number_installments;
        }

        public void setNumber_installments(Integer number_installments) {
            this.number_installments = number_installments;
        }

        public CardPayment getCard() {
            return card;
        }

        public void setCard(CardPayment card) {
            this.card = card;
        }

    }

    private class CreditCardReturn {

        String delayed;
        String authorization_code;
        String authorized_at;
        String reason_code;
        String reason_message;
        String acquirer;
        String soft_descriptor;
        String brand;
        String terminal_nsu;
        String acquirer_transaction_id;
        String transaction_id;

        public CreditCardReturn(String delayed, String authorization_code, String authorized_at, String reason_code, String reason_message, String acquirer, String soft_descriptor, String brand, String terminal_nsu, String acquirer_transaction_id, String transaction_id) {
            this.delayed = delayed;
            this.authorization_code = authorization_code;
            this.authorized_at = authorized_at;
            this.reason_code = reason_code;
            this.reason_message = reason_message;
            this.acquirer = acquirer;
            this.soft_descriptor = soft_descriptor;
            this.brand = brand;
            this.terminal_nsu = terminal_nsu;
            this.acquirer_transaction_id = acquirer_transaction_id;
            this.transaction_id = transaction_id;
        }

        public String getDelayed() {
            return delayed;
        }

        public void setDelayed(String delayed) {
            this.delayed = delayed;
        }

        public String getAuthorization_code() {
            return authorization_code;
        }

        public void setAuthorization_code(String authorization_code) {
            this.authorization_code = authorization_code;
        }

        public String getAuthorized_at() {
            return authorized_at;
        }

        public void setAuthorized_at(String authorized_at) {
            this.authorized_at = authorized_at;
        }

        public String getReason_code() {
            return reason_code;
        }

        public void setReason_code(String reason_code) {
            this.reason_code = reason_code;
        }

        public String getReason_message() {
            return reason_message;
        }

        public void setReason_message(String reason_message) {
            this.reason_message = reason_message;
        }

        public String getAcquirer() {
            return acquirer;
        }

        public void setAcquirer(String acquirer) {
            this.acquirer = acquirer;
        }

        public String getSoft_descriptor() {
            return soft_descriptor;
        }

        public void setSoft_descriptor(String soft_descriptor) {
            this.soft_descriptor = soft_descriptor;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getTerminal_nsu() {
            return terminal_nsu;
        }

        public void setTerminal_nsu(String terminal_nsu) {
            this.terminal_nsu = terminal_nsu;
        }

        public String getAcquirer_transaction_id() {
            return acquirer_transaction_id;
        }

        public void setAcquirer_transaction_id(String acquirer_transaction_id) {
            this.acquirer_transaction_id = acquirer_transaction_id;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

    }

    private class ReturnPayment {

        String payment_id;
        String seller_id;
        Integer amount;
        String currency;
        String order_id;
        String status;
        String received_at;
        CreditCardReturn credit;

        public ReturnPayment(String payment_id, String seller_id, Integer amount, String currency, String order_id, String status, String received_at, CreditCardReturn credit) {
            this.payment_id = payment_id;
            this.seller_id = seller_id;
            this.amount = amount;
            this.currency = currency;
            this.order_id = order_id;
            this.status = status;
            this.received_at = received_at;
            this.credit = credit;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReceived_at() {
            return received_at;
        }

        public void setReceived_at(String received_at) {
            this.received_at = received_at;
        }

        public CreditCardReturn getCredit() {
            return credit;
        }

        public void setCredit(CreditCardReturn credit) {
            this.credit = credit;
        }

    }

    private class RequestCancel {

        String payment_id;
        Integer cancel_amount;
        String cancel_custom_key;

        public RequestCancel(String payment_id, Integer amount, String cancel_custom_key) {
            this.payment_id = payment_id;
            this.cancel_amount = amount;
            this.cancel_custom_key = cancel_custom_key;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public Integer getCancel_amount() {
            return cancel_amount;
        }

        public void setCancel_amount(Integer cancel_amount) {
            this.cancel_amount = cancel_amount;
        }

        public String getCancel_custom_key() {
            return cancel_custom_key;
        }

        public void setCancel_custom_key(String cancel_custom_key) {
            this.cancel_custom_key = cancel_custom_key;
        }

    }

    private class RequestCancelReturn {

        String seller_id;
        String payment_id;
        String cancel_request_at;
        String cancel_request_id;
        String cancel_custom_key;
        String status;

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getCancel_request_at() {
            return cancel_request_at;
        }

        public void setCancel_request_at(String cancel_request_at) {
            this.cancel_request_at = cancel_request_at;
        }

        public String getCancel_request_id() {
            return cancel_request_id;
        }

        public void setCancel_request_id(String cancel_request_id) {
            this.cancel_request_id = cancel_request_id;
        }

        public String getCancel_custom_key() {
            return cancel_custom_key;
        }

        public void setCancel_custom_key(String cancel_custom_key) {
            this.cancel_custom_key = cancel_custom_key;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    private class RequestCancelStatus {

        String seller_id;
        String payment_id;
        String cancel_request_at;
        String cancel_request_id;
        String cancel_custom_key;
        String terminal_nsu;
        String acquirer_transaction_id;
        String transaction_date;
        String currency;
        String transaction_type;
        String number_installments;
        Integer amount;
        Integer cancel_amount;
        String status_processing_cancel_code;
        String status_processing_cancel_message;

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getCancel_request_at() {
            return cancel_request_at;
        }

        public void setCancel_request_at(String cancel_request_at) {
            this.cancel_request_at = cancel_request_at;
        }

        public String getCancel_request_id() {
            return cancel_request_id;
        }

        public void setCancel_request_id(String cancel_request_id) {
            this.cancel_request_id = cancel_request_id;
        }

        public String getCancel_custom_key() {
            return cancel_custom_key;
        }

        public void setCancel_custom_key(String cancel_custom_key) {
            this.cancel_custom_key = cancel_custom_key;
        }

        public String getTerminal_nsu() {
            return terminal_nsu;
        }

        public void setTerminal_nsu(String terminal_nsu) {
            this.terminal_nsu = terminal_nsu;
        }

        public String getAcquirer_transaction_id() {
            return acquirer_transaction_id;
        }

        public void setAcquirer_transaction_id(String acquirer_transaction_id) {
            this.acquirer_transaction_id = acquirer_transaction_id;
        }

        public String getTransaction_date() {
            return transaction_date;
        }

        public void setTransaction_date(String transaction_date) {
            this.transaction_date = transaction_date;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getTransaction_type() {
            return transaction_type;
        }

        public void setTransaction_type(String transaction_type) {
            this.transaction_type = transaction_type;
        }

        public String getNumber_installments() {
            return number_installments;
        }

        public void setNumber_installments(String number_installments) {
            this.number_installments = number_installments;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public Integer getCancel_amount() {
            return cancel_amount;
        }

        public void setCancel_amount(Integer cancel_amount) {
            this.cancel_amount = cancel_amount;
        }

        public String getStatus_processing_cancel_code() {
            return status_processing_cancel_code;
        }

        public void setStatus_processing_cancel_code(String status_processing_cancel_code) {
            this.status_processing_cancel_code = status_processing_cancel_code;
        }

        public String getStatus_processing_cancel_message() {
            return status_processing_cancel_message;
        }

        public void setStatus_processing_cancel_message(String status_processing_cancel_message) {
            this.status_processing_cancel_message = status_processing_cancel_message;
        }

    }

    private class CreditCancel {

        String canceled_at;
        String message;

        public String getCanceled_at() {
            return canceled_at;
        }

        public void setCanceled_at(String canceled_at) {
            this.canceled_at = canceled_at;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    private class PaymentCancel {

        String payment_id;
        String seller_id;
        Integer amount;
        String currency;
        String order_id;
        String status;
        CreditCancel credit_cancel;

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public CreditCancel getCredit_cancel() {
            return credit_cancel;
        }

        public void setCredit_cancel(CreditCancel credit_cancel) {
            this.credit_cancel = credit_cancel;
        }

    }
//</editor-fold>
}
