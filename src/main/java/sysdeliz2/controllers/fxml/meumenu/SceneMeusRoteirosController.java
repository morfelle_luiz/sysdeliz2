/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.meumenu;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.controllers.fxml.procura.SceneProcurarFornecedorController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Cliente;
import sysdeliz2.models.properties.Roteiros;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.TableUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneMeusRoteirosController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Local vars">
    String selectTiposConsulta = "";
    Roteiros currentSelectedRoteiro = null;
    IntegerProperty registroSelected = new SimpleIntegerProperty(0);
    IntegerProperty qtdeRegistros = new SimpleIntegerProperty(0);
    BooleanProperty isEditMode = new SimpleBooleanProperty(false);
    BooleanProperty isNewMode = new SimpleBooleanProperty(false);
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="FXML properties">
    @FXML
    private Button btnVizualizar;
    @FXML
    private DatePicker tboxData_C;
    @FXML
    private CheckBox chboxRetirar_C;
    @FXML
    private CheckBox chboxEntregar_C;
    @FXML
    private CheckBox chboxAmbos_C;
    @FXML
    private TextField tboxFornecedor_C;
    @FXML
    private TextField tboxCidade_C;
    @FXML
    private TextField tboxMercadoria_C;
    @FXML
    private Button btnProcurar;
    @FXML
    private TabPane tabPanes;
    @FXML
    private TableView<Roteiros> tblRoteiros;
    @FXML
    private TableColumn<Roteiros, String> clnStatus;
    @FXML
    private Button btnNovo_L;
    @FXML
    private Button btnEditar_L;
    @FXML
    private Button btnExcluir_L;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxSolicitante;
    @FXML
    private TextField tboxDataSolicitacao;
    @FXML
    private ComboBox<String> cboxTipo;
    @FXML
    private ComboBox<String> cboxStatusRoteiro;
    @FXML
    private DatePicker tboxData;
    @FXML
    private TextArea tboxMercadoria;
    @FXML
    private TextArea tboxObservacao;
    @FXML
    private TextField tboxCodFor;
    @FXML
    private Button btnProcurarCodFor;
    @FXML
    private TextField tboxDocFor;
    @FXML
    private TextField tboxNomeFor;
    @FXML
    private TextField tboxEndFor;
    @FXML
    private TextField tboxBairroFor;
    @FXML
    private TextField tboxCidade;
    @FXML
    private TextField tboxUF;
    @FXML
    private TextField tboxReferencia;
    @FXML
    private TextField tboxTelefone;
    @FXML
    private Button btnSalvar;
    @FXML
    private Button btnAnterior;
    @FXML
    private TextField tboxRegistro;
    @FXML
    private Button btnPosterior;
    @FXML
    private Button btnNovo_E;
    @FXML
    private Button btnEditar_E;
    @FXML
    private Button btnExcluir_E;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblRoteiros.setItems(DAOFactory.getRoteirosDAO().getUser(Globals.getUsuarioLogado().getNome().replace("name: ", "")));
            TableUtils.autoFitTable(tblRoteiros, TableUtils.COLUNAS_TBL_MEU_MENU_ROTEIROS);
            qtdeRegistros.set(tblRoteiros.getItems().size());
            if (qtdeRegistros.greaterThan(0).get()) {
                currentSelectedRoteiro = tblRoteiros.getItems().get(0);
                tblRoteiros.getSelectionModel().select(0);
                registroSelected.set(1);
            }

            cboxTipo.setItems(FXCollections.observableArrayList("Retirar", "Entregar", "Ambos"));
            cboxStatusRoteiro.setItems(FXCollections.observableArrayList("Todos", "Finalizado", "Aberto"));
            cboxStatusRoteiro.getSelectionModel().select(0);

            tblRoteiros.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                try {
                    currentSelectedRoteiro = newValue;
                    this.unbindData(oldValue);
                    this.bindData(currentSelectedRoteiro);
                } catch (SQLException ex) {
                    Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            });

            chboxRetirar_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Retirar|");
                    } else {
                        selectTiposConsulta.replace("Retirar|", "");
                    }
                }
            });
            chboxEntregar_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Entregar|");
                    } else {
                        selectTiposConsulta.replace("Entregar|", "");
                    }
                }
            });
            chboxAmbos_C.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        selectTiposConsulta = selectTiposConsulta.concat("Ambos|");
                    } else {
                        selectTiposConsulta.replace("Ambos|", "");
                    }
                }
            });

            tboxRegistro.textProperty().bind(registroSelected.asString());

            btnProcurarCodFor.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
            btnSalvar.disableProperty().bind(isEditMode.not().and(isNewMode.not()));

            btnVizualizar.disableProperty().bind(tblRoteiros.getSelectionModel().selectedItemProperty().isNull());
            btnEditar_L.disableProperty().bind(tblRoteiros.getSelectionModel().selectedItemProperty().isNull().or(isNewMode));
            btnExcluir_L.disableProperty().bind(tblRoteiros.getSelectionModel().selectedItemProperty().isNull().or(isEditMode));
            btnEditar_E.disableProperty().bind(tblRoteiros.getSelectionModel().selectedItemProperty().isNull().or(isNewMode));
            btnExcluir_E.disableProperty().bind(tblRoteiros.getSelectionModel().selectedItemProperty().isNull().or(isEditMode));
            btnNovo_L.disableProperty().bind(isEditMode);
            btnNovo_E.disableProperty().bind(isEditMode);

            btnAnterior.disableProperty().bind(registroSelected.lessThanOrEqualTo(1));
            btnPosterior.disableProperty().bind(qtdeRegistros.isEqualTo(0).or(registroSelected.isEqualTo(qtdeRegistros)));

            tboxMercadoria.setTooltip(new Tooltip("Informe as expecificações da mercadoria como peso, rolo, etc."));

            this.factoryColumns();

        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Local functions">
    private void bindData(Roteiros roteiro) throws SQLException {
        if (roteiro != null) {
            Cliente fornecedor = DAOFactory.getFornecedorDAO().getByCodigo(roteiro.codforProperty().get());

            tboxCodigo.editableProperty().set(false);
            tboxSolicitante.editableProperty().set(false);
            tboxDataSolicitacao.editableProperty().set(false);
            cboxTipo.editableProperty().set(false);
            tboxData.editableProperty().set(false);
            tboxMercadoria.editableProperty().set(false);
            tboxObservacao.editableProperty().set(false);
            tboxCodFor.editableProperty().set(false);
            tboxDocFor.editableProperty().set(false);
            tboxNomeFor.editableProperty().set(false);
            tboxEndFor.editableProperty().set(false);
            tboxBairroFor.editableProperty().set(false);
            tboxCidade.editableProperty().set(false);
            tboxUF.editableProperty().set(false);
            tboxReferencia.editableProperty().set(false);
            tboxTelefone.editableProperty().set(false);

            tboxCodigo.textProperty().set(roteiro.codigoProperty().asString().get());
            tboxSolicitante.textProperty().set(roteiro.solicitanteProperty().get());
            tboxDataSolicitacao.textProperty().set(roteiro.dt_solicitacaoProperty().get());
            cboxTipo.valueProperty().set(roteiro.tipoProperty().get());
            tboxData.valueProperty().set(roteiro.dt_roteiroProperty().get());
            tboxMercadoria.textProperty().set(roteiro.mercadoriaProperty().get());
            tboxObservacao.textProperty().set(roteiro.observacaoProperty().get());
            tboxCodFor.textProperty().set(roteiro.codforProperty().get());
            tboxDocFor.textProperty().set(fornecedor == null ? "" : fornecedor.cnpjProperty().get());
            tboxNomeFor.textProperty().set(fornecedor == null ? "" : fornecedor.nomeProperty().get());
            tboxEndFor.textProperty().set(roteiro.enderecoProperty().get());
            tboxBairroFor.textProperty().set(roteiro.bairroProperty().get());
            tboxCidade.textProperty().set(roteiro.cidadeProperty().get());
            tboxUF.textProperty().set(roteiro.ufProperty().get());
            tboxReferencia.textProperty().set(roteiro.referenciaProperty().get());
            tboxTelefone.textProperty().set(roteiro.telefoneProperty().get());
        }
    }

    private void bindBidirectionalData(Roteiros roteiro) throws SQLException {
        if (roteiro != null) {
            Cliente fornecedor = DAOFactory.getFornecedorDAO().getByCodigo(roteiro.codforProperty().get());

            tboxCodigo.editableProperty().set(false);
            tboxSolicitante.editableProperty().set(false);
            tboxDataSolicitacao.editableProperty().set(false);
            cboxTipo.editableProperty().set(true);
            tboxData.editableProperty().set(true);
            tboxMercadoria.editableProperty().set(true);
            tboxObservacao.editableProperty().set(true);
            tboxCodFor.editableProperty().set(true);
            tboxDocFor.editableProperty().set(false);
            tboxNomeFor.editableProperty().set(false);
            tboxEndFor.editableProperty().set(true);
            tboxBairroFor.editableProperty().set(true);
            tboxCidade.editableProperty().set(true);
            tboxUF.editableProperty().set(true);
            tboxReferencia.editableProperty().set(true);
            tboxTelefone.editableProperty().set(true);

            tboxCodigo.textProperty().set(roteiro.codigoProperty().asString().get());
            tboxSolicitante.textProperty().set(roteiro.solicitanteProperty().get());
            tboxDataSolicitacao.textProperty().set(roteiro.dt_solicitacaoProperty().get());
            cboxTipo.valueProperty().set(roteiro.tipoProperty().get());
            tboxData.valueProperty().set(roteiro.dt_roteiroProperty().get());
            tboxMercadoria.textProperty().set(roteiro.mercadoriaProperty().get());
            tboxObservacao.textProperty().set(roteiro.observacaoProperty().get());
            tboxCodFor.textProperty().set(roteiro.codforProperty().get());
            tboxDocFor.textProperty().set(fornecedor == null ? "" : fornecedor.cnpjProperty().get());
            tboxNomeFor.textProperty().set(fornecedor == null ? "" : fornecedor.nomeProperty().get());
            tboxEndFor.textProperty().set(roteiro.enderecoProperty().get());
            tboxBairroFor.textProperty().set(roteiro.bairroProperty().get());
            tboxCidade.textProperty().set(roteiro.cidadeProperty().get());
            tboxUF.textProperty().set(roteiro.ufProperty().get());
            tboxReferencia.textProperty().set(roteiro.referenciaProperty().get());
            tboxTelefone.textProperty().set(roteiro.telefoneProperty().get());
        }
    }

    private void unbindData(Roteiros roteiro) {
        if (roteiro != null) {
            tboxCodigo.clear();
            tboxSolicitante.clear();
            tboxDataSolicitacao.clear();
            tboxBairroFor.clear();
            tboxCidade.clear();
            tboxCodFor.clear();
            tboxCodigo.clear();
            tboxEndFor.clear();
            tboxMercadoria.clear();
            tboxObservacao.clear();
            tboxTelefone.clear();
            tboxUF.clear();
            tboxDocFor.clear();
            tboxNomeFor.clear();
        }
    }

    private void factoryColumns() {
        clnStatus.setCellFactory((TableColumn<Roteiros, String> param) -> {
            TableCell cell = new TableCell<Roteiros, String>() {

                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                    TableRow currentRow = getTableRow();
                    Roteiros currentFatura = currentRow == null ? null : (Roteiros) currentRow.getItem();
                    if (currentFatura != null) {
                        String status = currentFatura.getStatus();
                        clearPriorityStyle();
                        if (!isHover() && !isSelected() && !isFocused()) {
                            setPriorityStyle(status);
                        }
                    }
                }

                @Override
                public void updateSelected(boolean upd) {
                    super.updateSelected(upd);
                }

                private void clearPriorityStyle() {
                    ObservableList<String> styleClasses = getStyleClass();
                    styleClasses.remove("statusBoletoVencido");
                    styleClasses.remove("statusLigarCliente");
                    styleClasses.remove("statusBoletoAVencer");
                }

                private void setPriorityStyle(String priority) {
                    switch (priority.toUpperCase()) {
                        case "ABERTO":
                            getStyleClass().add("statusBoletoVencido");
                            break;
                        case "PROGRAMADO":
                            getStyleClass().add("statusLigarCliente");
                            break;
                        case "FINALIZADO":
                            getStyleClass().add("statusBoletoAVencer");
                            break;
                    }
                }

                private String getString() {
                    return getItem() == null ? null : getItem();
                }
            };
            return cell;
        });
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="FXML events">
    @FXML
    private void btnVisualizarOnAction(ActionEvent event) {
        try {
            bindData(currentSelectedRoteiro);
            tabPanes.getSelectionModel().select(1);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblRoteiros.setItems(DAOFactory.getRoteirosDAO().getByFormUser(tboxData_C.getValue() == null ? "" : tboxData_C.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    selectTiposConsulta, tboxFornecedor_C.getText(), tboxCidade_C.getText(), tboxMercadoria_C.getText(),
                    Globals.getUsuarioLogado().getNome().replace("name: ", ""),
                    cboxStatusRoteiro.getValue()
            ));
            tabPanes.getSelectionModel().select(0);
            qtdeRegistros.set(tblRoteiros.getItems().size());
            if (qtdeRegistros.greaterThan(0).get()) {
                currentSelectedRoteiro = tblRoteiros.getItems().get(0);
                tblRoteiros.getSelectionModel().select(0);
            }
            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnNovoOnAction(ActionEvent event) {
        try {
            tabPanes.getSelectionModel().select(1);
            unbindData(currentSelectedRoteiro);
            currentSelectedRoteiro = new Roteiros();
            currentSelectedRoteiro.setSolicitante(Globals.getUsuarioLogado().getNome().replace("name: ", ""));
            currentSelectedRoteiro.setDt_solicitacao(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            currentSelectedRoteiro.setTipo("Ambos");
            currentSelectedRoteiro.setStatus("Aberto");
            currentSelectedRoteiro.setDt_roteiro(LocalDate.now());
            tboxBairroFor.clear();
            tboxCidade.clear();
            tboxCodFor.clear();
            tboxCodigo.clear();
            tboxEndFor.clear();
            tboxMercadoria.clear();
            tboxObservacao.clear();
            tboxTelefone.clear();
            tboxUF.clear();
            tboxDocFor.clear();
            tboxNomeFor.clear();

            tblRoteiros.getItems().add(currentSelectedRoteiro);
            tblRoteiros.getSelectionModel().select(currentSelectedRoteiro);
            qtdeRegistros.set(qtdeRegistros.add(1).get());
            registroSelected.set(qtdeRegistros.get());

            isNewMode.set(true);
            bindBidirectionalData(currentSelectedRoteiro);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnEditarOnAction(ActionEvent event) {
        try {
            tabPanes.getSelectionModel().select(1);
            isEditMode.set(true);
            bindBidirectionalData(currentSelectedRoteiro);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnExcluirOnAction(ActionEvent event) {
        if (!isNewMode.get()) {
            if (currentSelectedRoteiro.getStatus().equals("Programado")) {
                GUIUtils.showMessage("Este roteiro está programado para o destino.\nNão pode ser excluído.", Alert.AlertType.INFORMATION);
                return;
            }
            if (GUIUtils.showQuestionMessageDefaultSim("Deseja realmente excluir o roteiro?")) {
                try {
                    DAOFactory.getRoteirosDAO().delete(currentSelectedRoteiro);
                    tblRoteiros.getItems().remove(currentSelectedRoteiro);
                    qtdeRegistros.set(qtdeRegistros.subtract(1).get());
                    if (qtdeRegistros.greaterThan(0).get()) {
                        currentSelectedRoteiro = tblRoteiros.getItems().get(0);
                        registroSelected.set(1);
                        bindData(currentSelectedRoteiro);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        } else {
            tblRoteiros.getItems().remove(currentSelectedRoteiro);
            qtdeRegistros.set(qtdeRegistros.subtract(1).get());
            if (qtdeRegistros.greaterThan(0).get()) {
                try {
                    currentSelectedRoteiro = tblRoteiros.getItems().get(0);
                    registroSelected.set(1);
                    bindData(currentSelectedRoteiro);
                } catch (SQLException ex) {
                    Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
            isNewMode.set(false);
        }
    }

    @FXML
    private void tboxCodForOnKeyRelease(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarCodForOnAction(null);
        }
    }

    @FXML
    private void btnProcurarCodForOnAction(ActionEvent event) {
        try {
            SceneProcurarFornecedorController ctrlSceneFornecedor = new SceneProcurarFornecedorController(Modals.FXMLWindow.SceneProcurarFornecedor);
            if (!ctrlSceneFornecedor.cods_retorno.isEmpty()) {
                String[] codigos = ctrlSceneFornecedor.cods_retorno.replace("|", "-").split("-");
                if (codigos.length > 1) {
                    GUIUtils.showMessage("Você pode escolher somente um fornecedor.", Alert.AlertType.ERROR);
                    return;
                }

                Cliente fornecedor = DAOFactory.getFornecedorDAO().getByCodigo(ctrlSceneFornecedor.cods_retorno);
                tboxCodFor.setText(ctrlSceneFornecedor.cods_retorno);
                tboxDocFor.textProperty().set(fornecedor.cnpjProperty().get());
                tboxNomeFor.textProperty().set(fornecedor.nomeProperty().get());
                tboxCidade.textProperty().set(fornecedor.getCidade());
                tboxUF.textProperty().set(fornecedor.getEstado());
                tboxEndFor.textProperty().set(fornecedor.getEndereco());
                tboxBairroFor.textProperty().set(fornecedor.getBairro());
                tboxReferencia.textProperty().set(fornecedor.getReferencia());
                tboxTelefone.textProperty().set(fornecedor.getTelefone());
                currentSelectedRoteiro.setFornecedor(fornecedor.getNome());
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        try {
            if (tboxCodFor.getText().length() == 0) {
                GUIUtils.showMessage("Você deve inserir o código do fornecedor.", Alert.AlertType.WARNING);
                return;
            }

            currentSelectedRoteiro.bairroProperty().set(tboxBairroFor.textProperty().get());
            currentSelectedRoteiro.cidadeProperty().set(tboxCidade.textProperty().get());
            currentSelectedRoteiro.codforProperty().set(tboxCodFor.textProperty().get());
            currentSelectedRoteiro.codigoProperty().set(Integer.parseInt(tboxCodigo.textProperty().get()));
            currentSelectedRoteiro.dt_roteiroProperty().set(tboxData.valueProperty().get());
            currentSelectedRoteiro.dt_solicitacaoProperty().set(tboxDataSolicitacao.textProperty().get());
            currentSelectedRoteiro.enderecoProperty().set(tboxEndFor.textProperty().get());
            currentSelectedRoteiro.fornecedorProperty().set(tboxNomeFor.textProperty().get());
            currentSelectedRoteiro.mercadoriaProperty().set(tboxMercadoria.textProperty().get());
            currentSelectedRoteiro.observacaoProperty().set(tboxObservacao.textProperty().get());
            currentSelectedRoteiro.referenciaProperty().set(tboxReferencia.textProperty().get());
            currentSelectedRoteiro.solicitanteProperty().set(tboxSolicitante.textProperty().get());
            currentSelectedRoteiro.telefoneProperty().set(tboxTelefone.textProperty().get());
            currentSelectedRoteiro.tipoProperty().set(cboxTipo.valueProperty().get());
            currentSelectedRoteiro.ufProperty().set(tboxUF.textProperty().get());

            if (isNewMode.get()) {
                currentSelectedRoteiro.setCodigo(Integer.parseInt(DAOFactory.getRoteirosDAO().save(currentSelectedRoteiro)));
            } else {
                DAOFactory.getRoteirosDAO().update(currentSelectedRoteiro);
            }

            unbindData(currentSelectedRoteiro);
            bindData(currentSelectedRoteiro);
            Notifications alertFinnaly = Notifications.create()
                    .title("Meus Roteiros")
                    .text("Dados salvos com sucesso!")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            alertFinnaly.showInformation();

            isEditMode.set(false);
            isNewMode.set(false);

            this.factoryColumns();
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnAnteriorOnAction(ActionEvent event) {
        try {
            registroSelected.set(registroSelected.subtract(1).get());
            currentSelectedRoteiro = tblRoteiros.getItems().get(registroSelected.subtract(1).get());
            bindData(currentSelectedRoteiro);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnPosteriorOnAction(ActionEvent event) {
        try {
            registroSelected.set(registroSelected.add(1).get());
            currentSelectedRoteiro = tblRoteiros.getItems().get(registroSelected.subtract(1).get());
            bindData(currentSelectedRoteiro);
        } catch (SQLException ex) {
            Logger.getLogger(SceneMeusRoteirosController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    //</editor-fold>

}
