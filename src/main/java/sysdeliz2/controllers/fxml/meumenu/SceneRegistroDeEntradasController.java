/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.meumenu;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.procura.FilterColaboradorSeniorController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdColaboradorSenior001;
import sysdeliz2.models.SdRegistroEntrada001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.MascarasFX;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.DateUtils;
import sysdeliz2.utils.validator.SimpleMessage;
import sysdeliz2.utils.validator.Validator;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author lima.joao
 */
public class SceneRegistroDeEntradasController implements Initializable {

    @FXML
    private TextArea tboxObservacao;
    @FXML
    private ChoiceBox<String> cboxStatus;
    @FXML
    private ChoiceBox<String> cboxFilterLanche;
    @FXML
    private Button btnCancelar;

    @FXML
    private DatePicker tboxFilterDataEntrada;
    @FXML
    private TextField tboxFilterNomeFuncionario;
    @FXML
    private TextField tboxFilterSetor;
    @FXML
    @SuppressWarnings("unused")
    private Button btnProcurar;
    @FXML
    private TabPane tabPanes;
    @FXML
    private TableView<SdRegistroEntrada001> tblRegistroEntradas;
    @FXML
    private Button btnVizualizar;
    @FXML
    private Button btnNovoRegsitroEntrada;
    @FXML
    private Button btnEditarRegistroEntrada;
    @FXML
    private Button btnExcluirEntrada;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxSolicitante;
    @FXML
    private TextField tboxDataEmissao;
    @FXML
    private Button btnProcurarFuncionario;
    @FXML
    private TextField tboxNomeFuncionario;
    @FXML
    private TextField tboxNomeSetor;
    @FXML
    private DatePicker tboxDataEntrada;
    @FXML
    private DatePicker tboxDataSaida;
    @FXML
    private TextField tboxHoraEntrada;
    @FXML
    private TextField tboxHoraSaida;
    @FXML
    private ComboBox<String> cboxLanche;
    @FXML
    public ComboBox<String> cboxCafe;
    @FXML
    private Button btnSalvar;
    @FXML
    private Button btnAnterior;
    @FXML
    private TextField tboxRegistro;
    @FXML
    private Button btnPosterior;
    @FXML
    private Button btnNovo_E;
    @FXML
    private Button btnEditar_E;
    @FXML
    private Button btnExcluir_E;

    private ObservableList<SdRegistroEntrada001> listEntradas = FXCollections.observableArrayList();

    private BooleanProperty isEditMode = new SimpleBooleanProperty(false);
    private BooleanProperty isNewMode = new SimpleBooleanProperty(false);
    private IntegerProperty registroSelected = new SimpleIntegerProperty(0);
    private IntegerProperty qtdeRegistros = new SimpleIntegerProperty(0);

    private SdRegistroEntrada001 registroSelecionado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            listEntradas.clear();

            // TODO Regra para quando for member RH exibir todos os registros
            List<ObservableList<String>> whereClauses = FXCollections.observableArrayList();
            whereClauses.add(FXCollections.observableArrayList(" email_responsavel = '" + SceneMainController.getAcesso().getEmail() + "'"));

            listEntradas.addAll(
                    Objects.requireNonNull(
                            DAOFactory.getSdRegistroEntradas001DAO()
                    ).getBy(whereClauses)
            );
            tblRegistroEntradas.setItems(listEntradas);
            qtdeRegistros.set(tblRegistroEntradas.getItems().size());

            if (qtdeRegistros.greaterThan(0).get()) {
                this.registroSelecionado = tblRegistroEntradas.getItems().get(0);
                tblRegistroEntradas.getSelectionModel().select(0);
                registroSelected.set(1);
            }

            cboxLanche.setItems(FXCollections.observableArrayList("Sem Lanche", "1 Salgado 1 Doce"));
            cboxCafe.setItems(FXCollections.observableArrayList("", "Não", "Sim"));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        cboxFilterLanche.setItems(FXCollections.observableArrayList("Ambos", "Sem Lanche", "Com Lanche"));
        cboxFilterLanche.getSelectionModel().select(0);
        cboxStatus.setItems(FXCollections.observableArrayList("Ambos", "Abertos", "Fechados"));
        cboxStatus.getSelectionModel().select(0);

        btnVizualizar
                .disableProperty()
                .bind(
                        tblRegistroEntradas.getSelectionModel()
                                .selectedItemProperty()
                                .isNull()
                                .or(isEditMode)
                                .or(isNewMode)
                );

        btnNovoRegsitroEntrada.disableProperty().bind(isEditMode.or(isNewMode));
        btnNovo_E.disableProperty().bind(isEditMode.or(isNewMode));

        btnEditarRegistroEntrada.disableProperty().bind(tblRegistroEntradas.getSelectionModel().selectedItemProperty().isNull().or(isNewMode).or(isEditMode));
        btnEditar_E.disableProperty().bind(tblRegistroEntradas.getSelectionModel().selectedItemProperty().isNull().or(isNewMode).or(isEditMode));

        btnExcluirEntrada.disableProperty().bind(tblRegistroEntradas.getSelectionModel().selectedItemProperty().isNull().or(isEditMode).or(isNewMode));
        btnExcluir_E.disableProperty().bind(tblRegistroEntradas.getSelectionModel().selectedItemProperty().isNull().or(isEditMode).or(isNewMode));

        btnSalvar.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
        btnCancelar.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
        btnProcurarFuncionario.disableProperty().bind((isNewMode.not()));
        cboxLanche.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
        cboxCafe.setDisable(true);

        cboxLanche.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Sem Lanche")) {
                cboxCafe.getSelectionModel().select(0);
                cboxCafe.setDisable(true);
            } else {
                cboxCafe.setDisable(false);
            }
        });

        MascarasFX.mascaraTime(tboxHoraEntrada);
        MascarasFX.mascaraTime(tboxHoraSaida);

        tboxNomeFuncionario.disableProperty().bind(isEditMode.not().and(isNewMode.not()));
        tboxNomeSetor.disableProperty().bind(isEditMode.not().and(isNewMode.not()));

        TextFieldUtils.upperCase(tboxFilterNomeFuncionario);
        TextFieldUtils.upperCase(tboxFilterSetor);

        TextFieldUtils.upperCase(tboxNomeFuncionario);
        TextFieldUtils.upperCase(tboxNomeSetor);

        tboxRegistro.textProperty().bind(registroSelected.asString());

        btnAnterior.disableProperty().bind(registroSelected.lessThanOrEqualTo(1).or(isNewMode).or(isEditMode));
        btnPosterior.disableProperty().bind(qtdeRegistros.isEqualTo(0).or(registroSelected.isEqualTo(qtdeRegistros).or(isNewMode).or(isEditMode)));
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnProcurarOnAction(ActionEvent event) {

        List<ObservableList<String>> whereClauses = FXCollections.observableArrayList();

        whereClauses.add(FXCollections.observableArrayList("1 = 1"));
        // Fixo sempre pega apenas os registros do usuario que abriu
        whereClauses.add(FXCollections.observableArrayList(" and email_responsavel = '" + SceneMainController.getAcesso().getEmail() + "'"));
        if (tboxFilterDataEntrada.getValue() != null) {
            LocalDate dtEntrada = tboxFilterDataEntrada.getValue();
            String dt_formatted = DateTimeFormatter.ofPattern("dd/MM/yyy").format(dtEntrada);
            whereClauses.add(
                    FXCollections
                            .observableArrayList(" and dh_entrada BETWEEN to_date('" +
                                    dt_formatted +
                                    " 00:00:00', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" +
                                    dt_formatted
                                    + " 23:59:59', 'dd/MM/yyyy HH24:MI:SS') "));
        }

        switch (cboxFilterLanche.getSelectionModel().getSelectedIndex()) {
            case 0:
                break;
            case 1:
                whereClauses.add(FXCollections.observableArrayList(" and pedir_lanche = 'N' "));
                break;
            case 2:
                whereClauses.add(FXCollections.observableArrayList(" and pedir_lanche = 'S' "));
                break;
        }

        switch (cboxStatus.getSelectionModel().getSelectedIndex()) {
            case 0:
                break;
            case 1:
                whereClauses.add(FXCollections.observableArrayList(" and situacao = 'Aberto' "));
                break;
            case 2:
                whereClauses.add(FXCollections.observableArrayList(" and situacao = 'Fechado' "));
                break;
        }

        if (!tboxFilterNomeFuncionario.getText().equals("")) {
            whereClauses.add(FXCollections
                    .observableArrayList(" AND NOME_FUNCIONARIO LIKE '%" + tboxFilterNomeFuncionario.getText() + "%' "));
        }

        if (!tboxFilterSetor.getText().equals("")) {
            whereClauses.add(FXCollections
                    .observableArrayList(" AND SETOR LIKE '%" + tboxFilterSetor.getText() + "%' "));
        }

        try {
            listEntradas.clear();
            listEntradas.addAll(
                    Objects.requireNonNull(
                            DAOFactory.getSdRegistroEntradas001DAO()
                    ).getBy(whereClauses)
            );
            tblRegistroEntradas.setItems(listEntradas);

            qtdeRegistros.set(tblRegistroEntradas.getItems().size());
            if (qtdeRegistros.greaterThan(0).get()) {
                this.registroSelecionado = tblRegistroEntradas.getItems().get(0);
                tblRegistroEntradas.getSelectionModel().select(0);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);

        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnVisualizarOnAction(ActionEvent event) {
        this.registroSelecionado = tblRegistroEntradas.getSelectionModel().getSelectedItem();
        if (bindData(true)) {
            tabPanes.getSelectionModel().select(1);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnNovoOnAction(ActionEvent event) {
        MessageBox.create(message -> {
            message.message("Lembre-se de selecionar se deseja café ou não!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.showAndWait();
            message.position(Pos.CENTER);
        });
        registroSelecionado = new SdRegistroEntrada001();
        registroSelecionado.setNomeResponsavel(SceneMainController.getAcesso().getNome());
        registroSelecionado.setEmailResponsavel(SceneMainController.getAcesso().getEmail());
        registroSelecionado.setDataEmissao(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(LocalDate.now()));
        registroSelecionado.setPedirLanche("N");
        registroSelecionado.setTipoLanche("0");
        registroSelecionado.setSituacao("Aberto");
        registroSelecionado.setImpresso("N");

        isNewMode.set(true);

        if (bindData(false)) {
            tabPanes.getSelectionModel().select(1);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnEditarOnAction(ActionEvent event) {

        if (tblRegistroEntradas.getSelectionModel().getSelectedItem().getSituacao().equals("Fechado")) {
            GUIUtils.showMessage("Somente registros abertos podem ser alterados.", Alert.AlertType.INFORMATION);
            return;
        }

        this.registroSelecionado = tblRegistroEntradas.getSelectionModel().getSelectedItem();

        isEditMode.set(true);

        if (bindData(false)) {
            tabPanes.getSelectionModel().select(1);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnExcluirOnAction(ActionEvent event) {

        Validator validator = new Validator();

        LocalTime horaLimite1 = LocalTime.of(17, 30);
        LocalTime horaLimite2 = LocalTime.of(17, 30);

        // Quando tiver lanche e não tiver sido fechada ainda
        boolean registroFechado = !tblRegistroEntradas.getSelectionModel().getSelectedItem().getSituacao().equals("Aberto");

        boolean regraTempoLimiteMesmoDia = false;
        boolean regraTempoLimiteDiaSegunte = false;

        if (!registroFechado) {
            LocalDate dataEntrada = DateUtils.getInstance().getDate(tblRegistroEntradas.getSelectionModel().getSelectedItem().getDataEntrada()).toLocalDate();
            // Hora limite 1
            regraTempoLimiteMesmoDia = (dataEntrada.equals(LocalDate.now()) && LocalTime.now().isAfter(horaLimite1));
            // Hora limite 2
            regraTempoLimiteDiaSegunte = (dataEntrada.equals(LocalDate.now().plusDays(1)) && LocalTime.now().isAfter(horaLimite2));
        }

        validator
                // Somente abertos podem ser removidos.
                .addIf(
                        registroFechado,
                        new SimpleMessage("ERRO", "Somente registros com situação aberta podem ser removidos. Converse com o RH")
                )
                .addIf(
                        regraTempoLimiteMesmoDia,
                        new SimpleMessage("ERRO", "Horario limite (16:00) para exclusão atingido. Converse com o RH")
                )
                .addIf(
                        regraTempoLimiteDiaSegunte,
                        new SimpleMessage("ERRO", "Horario limite (17:30) para exclusão atingido. Converse com o RH")
                )
                .addIf(
                        regraTempoLimiteDiaSegunte,
                        new SimpleMessage("ERRO", "Temporariamente proibido fazer hora extra no período noturno. Converse com o RH")
                );

        if (validator.hasError()) {
            validator.showAlertErrors();
            return;
        }

        if (GUIUtils.showQuestionMessageDefaultNao("Deseja remover o registro selecionado? Esta operação não poderá ser desfeita.")) {
            try {
                String codigo = tblRegistroEntradas.getSelectionModel().getSelectedItem().getCodigo();
                Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO())
                        .delete(Integer.parseInt(codigo));

                SdRegistroEntrada001 remover = null;
                for (SdRegistroEntrada001 registro : listEntradas) {
                    if (registro.getCodigo().equals(codigo)) {
                        remover = registro;
                        break;
                    }
                }
                if (remover != null) {
                    listEntradas.remove(remover);
                }

                tblRegistroEntradas.setItems(listEntradas);
                tblRegistroEntradas.refresh();

                qtdeRegistros.set(qtdeRegistros.subtract(1).get());
                if (qtdeRegistros.greaterThan(0).get()) {
                    this.registroSelecionado = tblRegistroEntradas.getItems().get(0);
                    registroSelected.set(1);
                    bindData(true);
                }
            } catch (SQLException ex) {
                Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnProcurarFuncionarioOnAction(ActionEvent event) {
        try {
            FilterColaboradorSeniorController filter = new FilterColaboradorSeniorController(Modals.FXMLWindow.FilterColaboradorSenior);
            if (filter.colaborador != null) {
                SdColaboradorSenior001 colaborador = filter.colaborador;

                this.registroSelecionado.setNomeFuncionario(colaborador.getNomeFuncionario());
                this.registroSelecionado.setSetor(colaborador.getNomeSetor());

                bindData(false);
            }
        } catch (IOException ex) {
            Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnSalvarOnAction(ActionEvent event) {
        if (cboxCafe.getSelectionModel().isSelected(0) && !cboxLanche.getSelectionModel().isSelected(0)) {
            MessageBox.create(message -> {
                message.message("Por favor, selecione se deseja café ou não.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.show();
                message.position(Pos.CENTER);
            });
        } else {
            // Carrega os dados da tela;
            if (!tboxCodigo.getText().equals("")) {
                this.registroSelecionado.setCodigo(tboxCodigo.getText().trim());
            }

            this.registroSelecionado.setNomeResponsavel(tboxSolicitante.getText());
            this.registroSelecionado.setDataEmissao(tboxDataEmissao.getText());
            this.registroSelecionado.setNomeFuncionario(tboxNomeFuncionario.getText());
            this.registroSelecionado.setSetor(tboxNomeSetor.getText());

            this.registroSelecionado.setDataEntrada(tboxDataEntrada.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + tboxHoraEntrada.getText());
            this.registroSelecionado.setDataSaida(tboxDataSaida.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + tboxHoraSaida.getText());
            this.registroSelecionado.setObservacao(tboxObservacao.getText());

            // Lanche
            switch (cboxLanche.getSelectionModel().getSelectedIndex()) {
                case 0:
                    this.registroSelecionado.setPedirLanche("N");
                    break;
                case 1:
                    this.registroSelecionado.setPedirLanche("S");
                    this.registroSelecionado.setTipoLanche("0");
                    break;
                case 2:
                    this.registroSelecionado.setPedirLanche("S");
                    this.registroSelecionado.setTipoLanche("1");
                    break;
            }
            //Café
            this.registroSelecionado.setComCafe(cboxCafe.getSelectionModel().getSelectedIndex() == 2 ? "S" : "N");
            this.registroSelecionado.setComCafeString(cboxCafe.getSelectionModel().getSelectedIndex() == 2 ? "Sim" : "Não");

            if (isNewMode.getValue()) {
                try {
                    if (Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).existRegistroAberto(this.registroSelecionado)) {
                        GUIUtils.showMessage("Já existe um registro aberto para o funcionário " + this.registroSelecionado.getNomeFuncionario());
                        return;
                    }
                } catch (SQLException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    GUIUtils.showException(e);
                }
            }

            // Validações
            Validator validator = new Validator();

            LocalDateTime dataEntrada = DateUtils.getInstance().getDate(this.registroSelecionado.getDataEntrada());
            LocalDateTime dataSaida = DateUtils.getInstance().getDate(this.registroSelecionado.getDataSaida());
            LocalDateTime dataAtual = LocalDateTime.now();
            LocalDateTime dataLimite = LocalDateTime.of(LocalDate.now().plusDays(3), LocalTime.MAX);

            LocalTime horaAtual = LocalTime.now();
            LocalTime horaLimiteHoje = LocalTime.of(16, 0);
            LocalTime horaLimiteDiaSeguinte = LocalTime.of(17, 30);
            LocalTime horaExtra = LocalTime.of(8, 1);

            validator
                    // Data de entrada não deve ser menor que a data atual;
                    .addIf(dataEntrada.isBefore(dataAtual), new SimpleMessage("Erro", "A data/hora de entrada não pode ser menor que a data/hora atual."))
                    // Data de entrada não pode ser maior que a data de saida;
                    .addIf(dataEntrada.isAfter(dataSaida), new SimpleMessage("Erro", "Foi informado uma data/hora de entrada maior que a data/hora de saida."))
                    // Data de entrada não pode ser maior que o dia de hoje + 3;
                    //.addIf(dataEntrada.isAfter(dataLimite), new SimpleMessage("Erro", "A data de entrada não pode exceder o limite de 3 dias a contar de hoje."))
                    // Hora Extra não
//                    .addIf(LocalTime.parse(tboxHoraEntrada.getText()).isAfter(horaExtra), new SimpleMessage("Erro", "Hora extra pemitida apenas no horário da manhã"))
                    // Lanches para o mesmo dia devem ser lançados até as 16 horas
                    //    Quando a data de entrada for igual a hoje só permitir pedir lanche quando o horario atual for menor ou igual a 16 horas.
                    .addIf(
                            dataEntrada.toLocalDate().equals(dataAtual.toLocalDate()) &&
                                    this.registroSelecionado.getPedirLancheUpd().equals("S") &&
                                    horaAtual.isAfter(horaLimiteHoje),
                            new SimpleMessage("Erro", "Horário limite para solicitar o lanche excedido.")
                    )
                    // Lanches para o dia seguinte devem ser lançados até as 17:30 horas
                    //    Quando a data de entrada for igual a hoje + 3 só permitir pedir lanche quando o horario atual for menor ou igual a 17:30 horas.
                    .addIf(
                            dataEntrada.toLocalDate().equals(dataLimite.toLocalDate()) &&
                                    this.registroSelecionado.getPedirLancheUpd().equals("S") &&
                                    horaAtual.isAfter(horaLimiteDiaSeguinte),
                            new SimpleMessage("Erro", "Horário limite para solicitar o lanche excedido.")
                    )
                    // Restrição solicitada pela Fran. do RH.
                    .addIf(
                            horaAtual.isAfter(horaLimiteDiaSeguinte),
                            new SimpleMessage("Erro", "Horario limite para registrar entrada/lanche. Verifique com o RH.")
                    );

            if (validator.hasError()) {
                validator.showAlertErrors();
                return;
            }

            if (this.registroSelecionado.getSetor() == null || this.registroSelecionado.getSetor().equals("")) {
                this.registroSelecionado.setSetor("OUTROS");
            }

            // Se inserindo
            try {
                if (this.registroSelecionado.getCodigo().equals("")) {
                    Integer codigo = Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).add(this.registroSelecionado);
                    this.registroSelecionado.setCodigo(codigo.toString());
                    tboxCodigo.setText(codigo.toString());
                } else {
                    Objects.requireNonNull(DAOFactory.getSdRegistroEntradas001DAO()).update(this.registroSelecionado);
                }

                if (isEditMode.get()) {
                    listEntradas.stream().filter(registro -> registro.getCodigo().equals(this.registroSelecionado.getCodigo())).forEach(registro -> {
                        registro.setDataEntrada(this.registroSelecionado.getDataEntrada());
                        registro.setDataSaida(this.registroSelecionado.getDataSaida());
                        registro.setPedirLanche(this.registroSelecionado.getPedirLancheUpd());
                        registro.setTipoLanche(this.registroSelecionado.getTipoLanche());
                        registro.setComCafe(this.registroSelecionado.getComCafe());
                    });
                    tblRegistroEntradas.setItems(listEntradas);
                    tblRegistroEntradas.refresh();

                    isEditMode.set(false);
                    isNewMode.set(false);

                    if (bindData(true)) {
                        tabPanes.getSelectionModel().select(1);
                    }
                }

                if (isNewMode.get()) {
                    listEntradas.add(this.registroSelecionado);
                    tblRegistroEntradas.setItems(listEntradas);
                    tblRegistroEntradas.refresh();

                    tblRegistroEntradas.getSelectionModel().select(this.registroSelecionado);
                    qtdeRegistros.set(qtdeRegistros.add(1).get());
                    registroSelected.set(qtdeRegistros.get());
                    tblRegistroEntradas.refresh();

                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja manter os dados?");
                        message.showAndWait();
                    }).value.get())) {

                        String dtEntrada = this.registroSelecionado.getDataEntrada();
                        String dtSaida = this.registroSelecionado.getDataSaida();
                        String hrEntrada = this.registroSelecionado.getDataSaida();
                        String hrSaida = this.registroSelecionado.getDataSaida();
                        String tipoLanche = this.registroSelecionado.getTipoLanche();
                        String situacao = this.registroSelecionado.getSituacao();
                        String impresso = this.registroSelecionado.getImpresso();
                        String pedirLanche = this.registroSelecionado.getPedirLanche();

                        registroSelecionado = new SdRegistroEntrada001();
                        registroSelecionado.setNomeResponsavel(SceneMainController.getAcesso().getNome());
                        registroSelecionado.setEmailResponsavel(SceneMainController.getAcesso().getEmail());
                        registroSelecionado.setDataEmissao(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(LocalDate.now()));
                        registroSelecionado.setPedirLanche(pedirLanche);
                        registroSelecionado.setTipoLanche(tipoLanche);
                        registroSelecionado.setSituacao(situacao);
                        registroSelecionado.setImpresso(impresso);
                        registroSelecionado.setDataEntrada(dtEntrada);
                        registroSelecionado.setDataSaida(dtSaida);
                        registroSelecionado.setHoraEntrada(hrEntrada);
                        registroSelecionado.setHoraSaida(hrSaida);

                        isNewMode.set(true);

                        if (bindData(false)) {
                            tabPanes.getSelectionModel().select(1);
                        }
                    } else {
                        clearBindData();
                        isEditMode.set(false);
                        isNewMode.set(false);
                    }
                }
            } catch (SQLException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                GUIUtils.showException(e);
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnAnteriorOnAction(ActionEvent event) {
        registroSelected.set(registroSelected.subtract(1).get());
        this.registroSelecionado = tblRegistroEntradas.getItems().get(registroSelected.subtract(1).get());
        tblRegistroEntradas.getSelectionModel().select(this.registroSelecionado);
        bindData(true);
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnPosteriorOnAction(ActionEvent event) {
        registroSelected.set(registroSelected.add(1).get());
        this.registroSelecionado = tblRegistroEntradas.getItems().get(registroSelected.subtract(1).get());
        tblRegistroEntradas.getSelectionModel().select(this.registroSelecionado);
        bindData(true);
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnCancelarOnAction(ActionEvent event) {
        isNewMode.set(false);
        isEditMode.set(false);

        clearBindData();

        if (tblRegistroEntradas.getItems().size() > 0) {
            this.registroSelecionado = tblRegistroEntradas.getItems().get(registroSelected.subtract(1).get());
            bindData(true);
        }
    }

    @FXML
    private void onNomeFuncionarioKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4 && tboxNomeFuncionario.editableProperty().get()) {
            this.btnProcurarFuncionarioOnAction(null);
        }
    }

    private boolean bindData(boolean readOnly) {
        if (registroSelecionado == null) {
            clearBindData();
            return false;
        }
        // Não permite alterar
        tboxCodigo.editableProperty().set(false);
        tboxSolicitante.editableProperty().set(false);
        tboxDataEmissao.editableProperty().set(false);

        //tboxNomeFuncionario.editableProperty().set(false);
        //tboxNomeSetor.editableProperty().set(false);

        tboxDataEntrada.setDisable(readOnly);
        tboxHoraEntrada.editableProperty().set(!readOnly);

        cboxLanche.editableProperty().set(false);

        tboxDataSaida.setDisable(readOnly);
        tboxHoraSaida.editableProperty().set(!readOnly);

        tboxCodigo.textProperty().set(registroSelecionado.getCodigo());
        tboxSolicitante.textProperty().set(registroSelecionado.getNomeResponsavel());
        tboxDataEmissao.textProperty().set(registroSelecionado.getDataEmissao());
        tboxNomeFuncionario.textProperty().set(registroSelecionado.getNomeFuncionario());
        tboxNomeSetor.textProperty().set(registroSelecionado.getSetor());

        tboxDataEntrada.valueProperty().set(DateUtils.getInstance().getDate(registroSelecionado.getDataEntrada()).toLocalDate());
        tboxHoraEntrada.textProperty().set(DateTimeFormatter.ofPattern("HH:mm").format(DateUtils.getInstance().getDate(registroSelecionado.getDataEntrada()).toLocalTime()));

        cboxLanche.setVisibleRowCount(3);

        if (registroSelecionado.getPedirLancheUpd().equals("N")) {
            cboxLanche.setValue("Sem Lanche");
        } else {
            if (registroSelecionado.getTipoLanche().equals("0")) {
                cboxLanche.setValue("1 Salgado 1 Doce");
            } else {
                cboxLanche.setValue("2 Doces");
            }
        }

        tboxDataSaida.valueProperty().set(DateUtils.getInstance().getDate(registroSelecionado.getDataSaida()).toLocalDate());
        tboxHoraSaida.textProperty().set(DateTimeFormatter.ofPattern("HH:mm").format(DateUtils.getInstance().getDate(registroSelecionado.getDataSaida())));

        return true;
    }

    private void clearBindData() {
        this.registroSelecionado = null;

        // Não permite alterar
        tboxCodigo.editableProperty().set(false);
        tboxSolicitante.editableProperty().set(false);
        tboxDataEmissao.editableProperty().set(false);

        //tboxNomeFuncionario.editableProperty().set(false);
        //tboxNomeSetor.editableProperty().set(false);

        tboxDataEntrada.setDisable(true);
        tboxHoraEntrada.editableProperty().set(false);

        cboxLanche.getSelectionModel().select(0);
        cboxLanche.editableProperty().set(false);

        tboxDataSaida.setDisable(true);
        tboxHoraSaida.editableProperty().set(false);

        tboxCodigo.textProperty().set("");
        tboxSolicitante.textProperty().set("");
        tboxDataEmissao.textProperty().set("");
        tboxNomeFuncionario.textProperty().set("");
        tboxNomeSetor.textProperty().set("");

        tboxDataEntrada.valueProperty().set(LocalDate.now());
        tboxHoraEntrada.textProperty().set("00:00");
        tboxHoraEntrada.editableProperty().set(false);

        cboxLanche.setVisibleRowCount(3);

        tboxDataSaida.valueProperty().set(LocalDate.now());
        tboxHoraSaida.textProperty().set("00:00");
        tboxHoraSaida.editableProperty().set(false);
    }

}
