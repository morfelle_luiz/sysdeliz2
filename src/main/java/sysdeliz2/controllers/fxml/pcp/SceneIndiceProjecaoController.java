/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.cadastros.SceneCadastroFuncaoController;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdIndiceProjecao001;
import sysdeliz2.models.sysdeliz.SdIndiceProjecao001PK;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneIndiceProjecaoController implements Initializable {
    
    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final ListProperty<SdIndiceProjecao001> listRegisters = new SimpleListProperty();
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private SdIndiceProjecao001 selectedItem = null;
    
    private final ObjectProperty<Marca> selectedMarca = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> selectedColecao = new SimpleObjectProperty<>();
    
    private ToggleSwitch tsByMarca = new ToggleSwitch(20, 50);
    private ToggleSwitch tsByLinha = new ToggleSwitch(20, 50);
    private ToggleSwitch tsByFixo = new ToggleSwitch(20, 50);
    private ToggleSwitch tsByMeta = new ToggleSwitch(20, 50);
    
    private GenericDao<SdIndiceProjecao001> daoSdIndiceProjecao001 = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdIndiceProjecao001.class);
    private GenericDao<Colecao> daoColecaoTi001 = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Colecao.class);
    private GenericDao<Marca> daoMarca001 = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Marca.class);
    
    // <editor-fold defaultstate="collapsed" desc="Componentes FXML">
    @FXML
    private TabPane tabMainPane;
    @FXML
    private ComboBox<String> cboxLinhaMeta;
    @FXML
    private TextField tboxIndice;
    @FXML
    private TextField tboxMeta;
    @FXML
    private Pane pnToggleMarca;
    @FXML
    private Pane pnToggleLinha;
    @FXML
    private Pane pnToggleFixo;
    @FXML
    private Pane pnToggleMeta;
    @FXML
    private Label lbToggleMarca;
    @FXML
    private Label lbToggleLinha;
    @FXML
    private Label lbToggleFixo;
    @FXML
    private Label lbToggleMeta;
    @FXML
    private Label lbTagLinhaMeta;
    @FXML
    private Button btnAddRegister;
    @FXML
    private TextField tboxDefaultFilter;
    @FXML
    private Button btnFindDefault;
    @FXML
    private TableView<SdIndiceProjecao001> tblRegisters;
    @FXML
    private TableColumn<SdIndiceProjecao001, SdIndiceProjecao001> clnActions;
    @FXML
    private TableColumn<SdIndiceProjecao001, SdIndiceProjecao001PK> clnColecao;
    @FXML
    private TableColumn<SdIndiceProjecao001, SdIndiceProjecao001PK> clnDescColecao;
    @FXML
    private TableColumn<SdIndiceProjecao001, SdIndiceProjecao001PK> clnMarca;
    @FXML
    private TableColumn<SdIndiceProjecao001, Boolean> clnByMarca;
    @FXML
    private TableColumn<SdIndiceProjecao001, Boolean> clnByLinha;
    @FXML
    private TableColumn<SdIndiceProjecao001, BigDecimal> clnByFixo;
    @FXML
    private TableColumn<SdIndiceProjecao001, BigDecimal> clnByMeta;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private Button btnFirstRegister;
    @FXML
    private Button btnPreviusRegister;
    @FXML
    private Button btnNextRegister;
    @FXML
    private Button btnLastRegister;
    @FXML
    private Button btnAddRegister2;
    @FXML
    private Button btnEditRegister;
    @FXML
    private Button btnDeleteRegister;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnReturn;
    @FXML
    private TextField tboxColecao;
    @FXML
    private Button btnProcuraColecao;
    @FXML
    private TextField tboxMarca;
    @FXML
    private Button btnProcuraMarca;
    //</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
        initializeComponents();
        makeButtonsActions();
        
        try {
            listRegisters.set(daoSdIndiceProjecao001.list());
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    private void initializeComponents(){
        TextFieldUtils.upperCase(tboxDefaultFilter);
        MaskTextField.numericDotField(tboxIndice);
        MaskTextField.numericDotField(tboxMeta);
    
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        tblRegisters.itemsProperty().bind(listRegisters);
        btnCancel.disableProperty().bind(disableBtnCancelRegister);
        btnSave.disableProperty().bind(disableBtnSaveRegister);
        btnEditRegister.disableProperty().bind(disableBtnEditRegister);
        btnDeleteRegister.disableProperty().bind(disableBtnDeleteRegister);
    
        btnReturn.disableProperty().bind(inEdition);
        tabMainPane.getTabs().get(0).disableProperty().bind(inEdition);
        btnAddRegister2.disableProperty().bind(inEdition);
    
        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnProcuraColecao.disableProperty().bind(inEdition.not());
        btnProcuraMarca.disableProperty().bind(inEdition.not());
    
        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegisters.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
    
        disableButtonNavegation.bind(tblRegisters.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));
    
        pnToggleMarca.getChildren().add(tsByMarca);
        lbToggleMarca.textProperty().bind(Bindings.when(tsByMarca.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        pnToggleLinha.getChildren().add(tsByLinha);
        lbToggleLinha.textProperty().bind(Bindings.when(tsByLinha.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        pnToggleFixo.getChildren().add(tsByFixo);
        lbToggleFixo.textProperty().bind(Bindings.when(tsByFixo.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        pnToggleMeta.getChildren().add(tsByMeta);
        lbToggleMeta.textProperty().bind(Bindings.when(tsByMeta.switchedOnProperty()).then("SIM").otherwise("NÃO"));
    
        tboxIndice.disableProperty().bind(tsByFixo.switchedOnProperty().not());
        tboxMeta.disableProperty().bind(tsByMeta.switchedOnProperty().not());
        tsByMarca.disableProperty().bind(tsByLinha.switchedOnProperty().or(tsByFixo.switchedOnProperty()));
        tsByLinha.disableProperty().bind(tsByMarca.switchedOnProperty().or(tsByFixo.switchedOnProperty()));
        tsByFixo.disableProperty().bind(tsByLinha.switchedOnProperty().or(tsByMarca.switchedOnProperty()).or(tsByMeta.switchedOnProperty()));
    
        lbTagLinhaMeta.visibleProperty().bind(tsByMeta.switchedOnProperty().and(tsByLinha.switchedOnProperty()));
        cboxLinhaMeta.visibleProperty().bind(tsByMeta.switchedOnProperty().and(tsByLinha.switchedOnProperty()));
        
        tblRegisters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegisters.getItems().indexOf(newValue));
        });
        tblRegisters.getSelectionModel().selectFirst();
    }
    
    private void makeButtonsActions() {
        clnActions.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>() {
            @Override
            public ObservableValue<SdIndiceProjecao001> call(TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnActions.setComparator(new Comparator<SdIndiceProjecao001>() {
            @Override
            public int compare(SdIndiceProjecao001 p1, SdIndiceProjecao001 p2) {
                return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnActions.setCellFactory(new Callback<TableColumn<SdIndiceProjecao001, SdIndiceProjecao001>, TableCell<SdIndiceProjecao001, SdIndiceProjecao001>>() {
            @Override
            public TableCell<SdIndiceProjecao001, SdIndiceProjecao001> call(TableColumn<SdIndiceProjecao001, SdIndiceProjecao001> btnCol) {
                return new TableCell<SdIndiceProjecao001, SdIndiceProjecao001>() {
                    final ImageView btnInfoRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
                    final ImageView btnEditRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final ImageView btnDeleteRowIcon = new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm()));
                    final Button btnInfoRow = new Button();
                    final Button btnEditRow = new Button();
                    final Button btnDeleteRow = new Button();
                    final HBox boxButtonsRow = new HBox(3);
                    
                    final Tooltip tipBtnInfoRow = new Tooltip("Visualizar informações");
                    final Tooltip tipBtnEditRow = new Tooltip("Editar registro selecionado");
                    final Tooltip tipBtnDeleteRow = new Tooltip("Excluir registro selecionado");
                    
                    {
                        btnInfoRow.setGraphic(btnInfoRowIcon);
                        btnInfoRow.setTooltip(tipBtnInfoRow);
                        btnInfoRow.setText("Informações do Registro");
                        btnInfoRow.getStyleClass().add("info");
                        btnInfoRow.getStyleClass().add("xs");
                        btnInfoRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnEditRow.setGraphic(btnEditRowIcon);
                        btnEditRow.setTooltip(tipBtnEditRow);
                        btnEditRow.setText("Alteração do Registro");
                        btnEditRow.getStyleClass().add("warning");
                        btnEditRow.getStyleClass().add("xs");
                        btnEditRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnDeleteRow.setGraphic(btnDeleteRowIcon);
                        btnDeleteRow.setTooltip(tipBtnDeleteRow);
                        btnDeleteRow.setText("Exclusão do Registro");
                        btnDeleteRow.getStyleClass().add("danger");
                        btnDeleteRow.getStyleClass().add("xs");
                        btnDeleteRow.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }
                    
                    @Override
                    public void updateItem(SdIndiceProjecao001 seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);
                            
                            btnInfoRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    bindData(selectedItem);
                                    tabMainPane.getSelectionModel().select(1);
                                }
                            });
                            btnEditRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    tabMainPane.getSelectionModel().select(1);
                                    btnEditRegisterOnAction(null);
                                }
                            });
                            btnDeleteRow.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    selectedItem = seletedRow;
                                    btnDeleteRegisterOnAction(null);
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        
    }
    
    private void clearForm() {
        tboxColecao.clear();
        tboxMarca.clear();
        
        tboxIndice.clear();
        tboxMeta.clear();
        tsByMarca.switchedOnProperty().set(false);
        tsByFixo.switchedOnProperty().set(false);
        tsByLinha.switchedOnProperty().set(false);
        tsByMeta.switchedOnProperty().set(false);
    }
    
    private void bindData(SdIndiceProjecao001 selectItem) {
        if (selectItem != null){
            tboxColecao.textProperty().bind(selectItem.idProperty().get().colecaoProperty().get().descricaoProperty());
            tboxMarca.textProperty().bind(selectItem.idProperty().get().marcaProperty().get().descricaoProperty());
//            cboxMarca.valueProperty().bindBidirectional(indice.strMarcaProperty());
//
//            cboxLinhaMeta.valueProperty().bindBidirectional(indice.strLinhaProperty());
//            cboxColecao.valueProperty().bindBidirectional(indice.objColecaoProperty());
//            StringConverter<Number> converter = new NumberStringConverter();
//            Bindings.bindBidirectional(tboxIndice.textProperty(), indice.douIndiceProperty(), converter);
//            Bindings.bindBidirectional(tboxMeta.textProperty(), indice.douMetaProperty(), converter);
//            tsByMarca.switchedOnProperty().bindBidirectional(indice.isByMarcaProperty());
//            tsByLinha.switchedOnProperty().bindBidirectional(indice.isByLinhaProperty());
//            tsByFixo.switchedOnProperty().bindBidirectional(indice.isByFixoProperty());
//            tsByMeta.switchedOnProperty().bindBidirectional(indice.isByMetaProperty());
        }
    }
    
    private void unbindData() {
//        cboxMarca.valueProperty().unbindBidirectional(indice.strMarcaProperty());
//        cboxLinhaMeta.valueProperty().unbindBidirectional(indice.strLinhaProperty());
//        cboxColecao.valueProperty().unbindBidirectional(indice.objColecaoProperty());
//        tboxIndice.textProperty().unbindBidirectional(indice.douIndiceProperty());
//        tboxMeta.textProperty().unbindBidirectional(indice.douMetaProperty());
//        tsByMarca.switchedOnProperty().unbindBidirectional(indice.isByMarcaProperty());
//        tsByLinha.switchedOnProperty().unbindBidirectional(indice.isByLinhaProperty());
//        tsByFixo.switchedOnProperty().unbindBidirectional(indice.isByFixoProperty());
//        tsByMeta.switchedOnProperty().unbindBidirectional(indice.isByMetaProperty());
//        clearForm();
    }
    
    private void bindBidirectionalData(SdIndiceProjecao001 selectItem) {
        if (selectItem != null) {
            tboxColecao.textProperty().bind(selectItem.idProperty().get().colecaoProperty().get().descricaoProperty());
            tboxMarca.textProperty().bind(selectItem.idProperty().get().marcaProperty().get().descricaoProperty());
//            tboxRegisterCode.textProperty().bind(selectItem.codigoProperty().asString());
//            tboxDescriptionCode.textProperty().bindBidirectional(selectItem.descricaoProperty());
        }
    }
    
    private void unbindBidirectionalData(SdIndiceProjecao001 selectItem) {
//        if (selectItem != null) {
//            tboxRegisterCode.textProperty().unbindBidirectional(selectItem.codigoProperty());
//            tboxDescriptionCode.textProperty().unbindBidirectional(selectItem.descricaoProperty());
//        }
    }
    
    private String validateForm() {
        StringBuilder errorMessage = new StringBuilder();

//        if (cboxMarca.getSelectionModel().isEmpty()) {
//            errorMessage.append("Selecione uma MARCA para a regra.").append(StringUtils.newLine());
//        }
//        if (cboxColecao.getSelectionModel().isEmpty()) {
//            errorMessage.append("Selecione uma COLEÇÃO para a regra.").append(StringUtils.newLine());
//        }
//        if (currentSelected.isByFixoProperty().not().and(currentSelected.isByLinhaProperty().not().and(currentSelected.isByMarcaProperty().not())).get()) {
//            errorMessage.append("Você deve seelecionar uma regra: POR MARCA ou POR LINHA ou POR FIXO.").append(StringUtils.newLine());
//        }
//        if (currentSelected.isByFixoProperty().and(tboxIndice.textProperty().isEmpty().or(tboxIndice.textProperty().isEqualTo("0"))).get()) {
//            errorMessage.append("Digite o valor do ÍNDICE. Ex.: 0.8452").append(StringUtils.newLine());
//        }
//        for (IndiceProjecao indice : tblRegraIndices.getItems()) {
//            if (indice.equals(currentSelected)) {
//                errorMessage.append("Já existe uma regra com essa configuração.").append(StringUtils.newLine());
//                break;
//            }
//        }
        return errorMessage.toString();
    }
    
    private void btnSalvarOnAction(ActionEvent event) {
        
//        currentSelected.douIndiceProperty().set(Bindings.when(tsByLinha.switchedOnProperty().or(tsByMarca.switchedOnProperty()).or(tsByMeta.switchedOnProperty())).then(0).otherwise(currentSelected.douIndiceProperty().get()).get());
//        currentSelected.douMetaProperty().set(Bindings.when(tsByMeta.switchedOnProperty().not().and(tsByLinha.switchedOnProperty().or(tsByMarca.switchedOnProperty()))).then(0).otherwise(currentSelected.douMetaProperty().get()).get());
//        currentSelected.strLinhaProperty().set(Bindings.when(tsByMeta.switchedOnProperty().not().and(tsByLinha.switchedOnProperty().or(tsByMarca.switchedOnProperty()))).then("-").otherwise(currentSelected.strLinhaProperty().get()).get());
//
//        if (isNewIndice) {
//            String validation = this.validateForm();
//            if (!validation.isEmpty()) {
//                GUIUtils.showMessage(validation, Alert.AlertType.WARNING);
//                return;
//            }
//            tblRegraIndices.getItems().add(currentSelected);
//            try {
//                daoIndice.insertRegraIndice(currentSelected);
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneIndiceProjecaoController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        } else {
//            if (currentSelected.isByFixoProperty().and(tboxIndice.textProperty().isEmpty().or(tboxIndice.textProperty().isEqualTo("0"))).get()) {
//                GUIUtils.showMessage("Digite o valor do ÍNDICE. Ex.: 0.8452", Alert.AlertType.WARNING);
//                tboxIndice.requestFocus();
//                return;
//            }
//
//            if (currentSelected.isByMetaProperty().and(tboxMeta.textProperty().isEmpty().or(tboxMeta.textProperty().isEqualTo("0"))).get()) {
//                GUIUtils.showMessage("Digite o valor da META. Ex.: 10000", Alert.AlertType.WARNING);
//                tboxMeta.requestFocus();
//                return;
//            }
//
//            try {
//                daoIndice.updateRegraIndice(currentSelected);
//            } catch (SQLException ex) {
//                Logger.getLogger(SceneIndiceProjecaoController.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        }
//        isNewIndice = false;
//        isEditMode.set(false);
//        unbindData(currentSelected);
//        tblRegraIndices.refresh();
//        tblRegraIndices.getSelectionModel().select(currentSelected);
    }
    
    @FXML
    private void btnAddRegisterOnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdIndiceProjecao001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
        
        tabMainPane.getSelectionModel().select(1);
    }
    
    @FXML
    private void btnFindDefaultOnAction(ActionEvent event) {
        try {
            listRegisters.set(daoSdIndiceProjecao001.initCriteria()
                    .addPredicateEqPkEmbedded("id", "colecao", tboxDefaultFilter.getText(), false)
                    .loadListByPredicate());
            tboxDefaultFilter.clear();
        } catch (SQLException ex) {
            Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnFirstRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectFirst();
    }
    
    @FXML
    private void btnPreviusRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectPrevious();
    }
    
    @FXML
    private void btnNextRegisterOnAction(ActionEvent event) {
        tblRegisters.getSelectionModel().selectNext();
    }
    
    @FXML
    private void btnLastRegisterOnAtion(ActionEvent event) {
        tblRegisters.getSelectionModel().selectLast();
    }
    
    @FXML
    private void btnAddRegister2OnAction(ActionEvent event) {
        unbindData();
        selectedItem = new SdIndiceProjecao001();
        bindBidirectionalData(selectedItem);
        inEdition.set(true);
    }
    
    @FXML
    private void btnEditRegisterOnAction(ActionEvent event) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }
    
    @FXML
    private void btnDeleteRegisterOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                daoSdIndiceProjecao001.delete(selectedItem);
                SysLogger.addFullLog(
                        "Cadastro Índices Projeção",
                        TipoAcao.EXCLUIR,
                        selectedItem.getId().toString(),
                        "Excluído o índice da marca " +
                                selectedItem.getId().getMarca().getDescricao() +
                                " da coleção " +
                                selectedItem.getId().getColecao().getDescricao()
                );
                listRegisters.remove(selectedItem);
                tblRegisters.getSelectionModel().selectFirst();
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroFuncaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
    
    @FXML
    private void btnSaveOnAction(ActionEvent event) {
    }
    
    @FXML
    private void btnCancelOnAction(ActionEvent event) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        inEdition.set(false);
        tblRegisters.getSelectionModel().selectFirst();
    }
    
    @FXML
    private void btnReturnOnAction(ActionEvent event) {
        tabMainPane.getSelectionModel().select(0);
    }
    
    @FXML
    private void btnProcuraColecaoOnAction(ActionEvent event) {
        GenericFilter<Colecao> filterColecao = null;
        try {
            filterColecao = new GenericFilter<Colecao>(){};
            filterColecao.show(ResultTypeFilter.SINGLE_RESULT);
            selectedColecao.set(filterColecao.selectedReturn);
            selectedItem.getId().setColecao(selectedColecao.get());
            tboxColecao.textProperty().bind(selectedItem.idProperty().get().colecaoProperty().get().descricaoProperty());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
        
    }
    
    @FXML
    private void btnProcuraMarcaOnAction(ActionEvent event) {
        GenericFilter<Marca> filterMarca = null;
        try {
            filterMarca = new GenericFilter<Marca>(){};
            filterMarca.show(ResultTypeFilter.SINGLE_RESULT);
            selectedMarca.set(filterMarca.selectedReturn);
            selectedItem.getId().setMarca(selectedMarca.get());
            tboxMarca.textProperty().bind(selectedItem.idProperty().get().marcaProperty().get().descricaoProperty());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
}
