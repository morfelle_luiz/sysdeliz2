/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.procura.FilterMarcaController;
import sysdeliz2.controllers.fxml.procura.FilterProdutoController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.controllers.views.procura.FilterEtiqProdView;
import sysdeliz2.controllers.views.procura.FilterLinhaView;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.Cor;
import sysdeliz2.models.ProdutoOrdem;
import sysdeliz2.models.Tamanho;
import sysdeliz2.models.properties.AnaliseGrade;
import sysdeliz2.models.properties.AnaliseGradeDetalhe;
import sysdeliz2.models.sysdeliz.SdAnaliseGrade;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.LoadingModalWorker;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.GenericCellFactory;
import sysdeliz2.utils.gui.ICellFactoryListener;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author lima.joao
 */
public class ScenePcpCadastrarDemandaController implements Initializable {

    //<editor-fold desc="Variáveis FXML">
    @FXML
    public TextField tboxLinha;
    @FXML
    public Button btnFind;
    @FXML
    public Button btnRodarJob;
    @FXML
    public Button btnAttJob;
    @FXML
    public TextField tboxModelagem;
    @FXML
    public TextField tboxClasse;
    //@FXML
    //public TableColumn<Produto, ImageView> clnStatus;
    @FXML
    public TableColumn<Produto, String> clnColecao;
    @FXML
    public TableColumn<Produto, String> clnLinha;
    @FXML
    public TableColumn<Produto, String> clnModelagem;
    @FXML
    public TableColumn<Produto, String> clnClasse;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise01;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise02;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise03;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise04;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise05;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise06;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise07;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise08;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise09;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise10;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise11;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise12;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise13;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise14;
    @FXML
    public TableColumn<AnaliseGrade, String> clnAnalise15;
    @FXML
    public TableView<AnaliseGrade> tblAnaliseGrade;
    @FXML
    public Button btnLoadMedia;
    @FXML
    public TextField tFieldTempoUltimaRodada;
    @FXML
    public TextField tfieldEstadoJob;
    @FXML
    public TextField tFieldInicioUltimaRodada;
    @FXML
    public TextField tFieldDataProximaRodada;
    @FXML
    public TextField tboxCodigoMarca;
    @FXML
    private TextField tboxCodigoColecaoAnalise;
    @FXML
    private TextField tboxCodigoProduto;
    @FXML
    private TextField tboxCodigoColecao;
    @FXML
    private TableView<Produto> tblProdutoSelecionado;
    @FXML
    private TableView<Cor> tblCores;
    @FXML
    private TextField tboxProcurar;
    @FXML
    private TableView<Tamanho> tblTamanhos;
    @FXML
    private Button btnSalvar;
    @FXML
    private TextField tboxPercentualCor;
    @FXML
    private TableColumn<Cor, String> clnPercentualCor;
    @FXML
    private TableColumn<Tamanho, String> clnPercentualTam;
    @FXML
    private TextField tboxProcurarTam;
    @FXML
    private TextField tboxPercentualTam;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private Label lblStatusProgresso;
    @FXML
    @SuppressWarnings("unused")
    private ProgressIndicator pbProgress;
    //</editor-fold>

    private final ListProperty<Produto> listDisponiveis = new SimpleListProperty<>();
    private final ListProperty<Cor> listCores = new SimpleListProperty<>();
    private final ListProperty<Tamanho> listTamanhos = new SimpleListProperty<>();

    protected final StringProperty dataUltimoLog = new SimpleStringProperty();
    protected final StringProperty infoUltimoLog = new SimpleStringProperty();

    protected final StringProperty jobInicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty jobTempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty statusJobLog = new SimpleStringProperty();
    protected final StringProperty jobDataProximoLog = new SimpleStringProperty();

    private final ObservableList<Produto> listDisponiveisAll = FXCollections.observableArrayList();
    private final ObservableList<Tamanho> listTamanhosAll = FXCollections.observableArrayList();

    private final ObservableList<SdAnaliseGrade> analiseGradeList = FXCollections.observableArrayList();
    private final ObservableList<SdAnaliseGrade> analiseGradeListFiltered = FXCollections.observableArrayList();

    private GenericDao<Produto> produtoDao;
    private GenericDao<SdAnaliseGrade> analiseGradeDao;

    private Produto produtoSelecionado = null;

    private Double perc = 0.0;
    private Double _percCor = 0.0;
    private Double _percTam = 0.0;

    // Teste para inserir um listener no GenericCellFactory -- Esta com um problema, na primeira iteração não esta executando commit;
    private final ICellFactoryListener<Cor, String> colPercentualCorListener = new ICellFactoryListener<Cor, String>() {

        @Override
        public void onCancelEdit(TableColumn.CellEditEvent<Cor, String> event) {
            // do nothing 
        }

        @Override
        public void onCommitEdit(TableColumn.CellEditEvent<Cor, String> event) {
            perc = 0.0;

            tblCores.getItems().forEach(cor -> {
                if ((cor.getCodigo().equals(event.getRowValue().getCodigo())) && (cor.getCor().equals(event.getRowValue().getCor()))) {
                    perc += Double.parseDouble(event.getNewValue());
                    cor.setPerccor(event.getNewValue());
                } else {
                    perc += Double.parseDouble(cor.getPerccor());
                }
            });

            tblProdutoSelecionado.getSelectionModel().getSelectedItem().setAlterou(true);
            tblProdutoSelecionado.refresh();

            tboxPercentualCor.setText(perc.toString() + "%");

            if (perc > 100) {
                tboxPercentualCor.setStyle("-fx-text-fill: red;");
            } else {
                tboxPercentualCor.setStyle("-fx-text-fill: #000;");
            }
        }

        @Override
        public void onStartEdit(TableColumn.CellEditEvent<Cor, String> event) {
            // do nothing 
        }
    };

    // Teste para inserir um listener no GenericCellFactory -- Esta com um problema, na primeira iteração não esta executando commit;
    private final ICellFactoryListener<Tamanho, String> colPercentualTamListener = new ICellFactoryListener<Tamanho, String>() {

        @Override
        public void onCancelEdit(TableColumn.CellEditEvent<Tamanho, String> event) {
            // do nothing 
        }

        @Override
        public void onCommitEdit(TableColumn.CellEditEvent<Tamanho, String> event) {
            perc = 0.0;

            listTamanhosAll.forEach(tam -> {
                if ((tam.getCodigo().equals(event.getRowValue().getCodigo()) && (tam.getTam().equals(event.getRowValue().getTam())))) {
                    perc += Double.parseDouble(event.getNewValue());
                    tam.setPerctam(event.getNewValue());
                } else {
                    perc += Double.parseDouble(tam.getPerctam());
                }
            });

            tblProdutoSelecionado.getSelectionModel().getSelectedItem().setAlterou(true);
            tblProdutoSelecionado.refresh();

            tboxPercentualTam.setText(perc.toString() + "%");
            if (perc > 100) {
                tboxPercentualTam.setStyle("-fx-text-fill: red;");
            } else {
                tboxPercentualTam.setStyle("-fx-text-fill: #000;");
            }
        }

        @Override
        public void onStartEdit(TableColumn.CellEditEvent<Tamanho, String> event) {
            // do nothing 
        }
    };

    private void searchAvailable(String oldVal, String newVal) {
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listDisponiveis.set(listDisponiveisAll);
        }

        String value = newVal.toUpperCase();
        ObservableList<Produto> subentries = FXCollections.observableArrayList();

        listDisponiveis.forEach(entry -> {
            boolean match = true;
            if (
                    (!entry.getCodigo().toUpperCase().contains(value)) &&
                            (!entry.getDescricao().toUpperCase().contains(value))
            ) {
                match = false;
            }
            if (match) {
                subentries.add(entry);
            }
        });
        listDisponiveis.set(subentries);
    }

    private void searchTamanhosAvailable(String oldVal, String newVal) {
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listTamanhos.clear();
            listTamanhos.set(listTamanhosAll);
        }

        String value = newVal.toUpperCase();
        ObservableList<Tamanho> subentries = FXCollections.observableArrayList();

        listTamanhos.forEach(entry -> {
            boolean match = true;
            if (!entry.getTam().toUpperCase().contains(value)) {
                match = false;
            }
            if (match) {
                subentries.add(entry);
            }
        });
        listTamanhos.set(subentries);
    }

    private void carregaPercentuais(Produto produto, Boolean calcular) {

        try {
            if (produto.getCoresDemanda() == null || produto.getCoresDemanda().size() <= 0) {
                ObservableList<Cor> cores = Objects.requireNonNull(DAOFactory.getProdutoDAO()).getCoresPorProduto(produto.getCodigo());

                if (cores.size() == 1 && cores.get(0).getCor().equals("UN")) {
                    if (cores.get(0).getPerccor().equals("0")) {
                        cores.get(0).setPerccor("100");
                    }
                }

                produto.setCoresDemanda(cores);
            }

            if (produto.getTamanhosDemanda() == null || produto.getTamanhosDemanda().size() <= 0) {
                ObservableList<Tamanho> tamanhos = Objects.requireNonNull(DAOFactory.getProdutoDAO()).getTamanhosPorProduto(produto.getCodigo());
                // Insere a lista de cores dentro de cada produto parregado
                produto.setTamanhosDemanda(tamanhos);
            }

            produto.setAlterou(true);

        } catch (SQLException ex) {
            Logger.getLogger(ScenePcpCadastrarDemandaController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (calcular) {
            perc = 0.0;

            listCores.set(produto.getCoresDemanda());

            listCores.forEach(cor -> perc += Double.parseDouble(cor.getPerccor()));

            tboxPercentualCor.setText(perc.toString() + "%");

            if (perc > 100) {
                tboxPercentualCor.setStyle("-fx-text-fill: red;");
            } else {
                tboxPercentualCor.setStyle("-fx-text-fill: #000;");
            }

            perc = 0.0;
            listTamanhos.set(produto.getTamanhosDemanda());
            listTamanhos.forEach(tamanho -> perc += Double.parseDouble(tamanho.getPerctam()));

            // Carrega os tamanhos do produto informado
            listTamanhosAll.clear();
            listTamanhosAll.addAll(produto.getTamanhosDemanda());

            perc = Math.floor(perc);
            tboxPercentualTam.setText(perc.toString() + "%");
            // Exibe o percentual
            if (perc > 100) {

                tboxPercentualTam.setStyle("-fx-text-fill: red;");
            } else {
                tboxPercentualTam.setStyle("-fx-text-fill: #000;");
            }
        }
    }

    private void escondeColunas() {
        clnAnalise01.setVisible(false);
        clnAnalise02.setVisible(false);
        clnAnalise03.setVisible(false);
        clnAnalise04.setVisible(false);
        clnAnalise05.setVisible(false);
        clnAnalise06.setVisible(false);
        clnAnalise07.setVisible(false);
        clnAnalise08.setVisible(false);
        clnAnalise09.setVisible(false);
        clnAnalise10.setVisible(false);
        clnAnalise11.setVisible(false);
        clnAnalise12.setVisible(false);
        clnAnalise13.setVisible(false);
        clnAnalise14.setVisible(false);
        clnAnalise15.setVisible(false);
    }

    private void montaColumnaAnalise(TableColumn<AnaliseGrade, String> coluna, int posicao) {
        coluna.setVisible(true);
        coluna.setCellValueFactory(param -> {
            coluna.setText(param.getValue().getDetalhe().get(posicao).getTamanho());
            return new SimpleStringProperty(param.getValue().getDetalhe().get(posicao).getPercTamanho().toString());
        });
    }

    private void montaGridAnalise(Produto produto) {
        btnLoadMedia.setDisable(true);
        escondeColunas();

        String[] colecoes = tboxCodigoColecaoAnalise.getText().replace("'", "").split(",");

//        analiseGradeListFiltered.setAll(analiseGradeList.filtered(sdAnaliseGrade ->
//                tboxCodigoColecaoAnalise.getText().contains(sdAnaliseGrade.getColecao()) &&
//                        sdAnaliseGrade.getModelagem().equals(produto.getEtiqueta().getModelagem()) &&
//                        sdAnaliseGrade.getClasse().equals(produto.getEtiqueta().getGrupo()) &&
//                        sdAnaliseGrade.getFaixa().equals(produto.getFaixa().getCodigo()) &&
//                        sdAnaliseGrade.getLinha().equals(produto.getLinha().getSdLinhaDesc())
//        ));

        if (analiseGradeDao == null) {
            analiseGradeDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdAnaliseGrade.class);
        }

        @SuppressWarnings("unchecked")
        ObservableList<SdAnaliseGrade> tmp = null;
        String modelagem = produto.getEtiqueta().getModelagem();
        String grupo = produto.getEtiqueta().getGrupo();
        String faixa = produto.getFaixa().getCodigo();
        String linha = produto.getLinha().getSdLinhaDesc();
        String marca = produto.getMarca().getSdGrupo();
        try {
            tmp = analiseGradeDao.initCriteria()
                    .addPredicateIn("colecao", colecoes) //tboxCodigoColecaoAnalise.getText().replaceAll("'", "").split(",")
                    .addPredicateEq("modelagem", modelagem)
                    .addPredicateEq("classe", grupo)
                    .addPredicateEq("faixa", faixa)
                    .addPredicateEq("linha", linha)
                    .addPredicateEq("marca", marca)
                    .addOrderByDesc("colecao")
                    .loadListByPredicate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        analiseGradeList.setAll(tmp);

        // Filtra
        ObservableList<AnaliseGrade> list = null;
        try {
            list = AnaliseGrade.buildBySdAnaliseGrade(analiseGradeList);
            tblAnaliseGrade.setItems(list);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (list != null && list.size() > 0) {
            btnLoadMedia.setDisable(false);
            clnAnalise01.setVisible(true);
            clnAnalise01.setText("Coleção");

            clnAnalise01.setCellValueFactory(param -> param.getValue().colecaoProperty());

            int count = list.get(0).getDetalhe().size();
            for (int i = 0; i < count; i++) {
                switch (i) {
                    case 0:
                        montaColumnaAnalise(clnAnalise02, 0);
                        break;
                    case 1:
                        montaColumnaAnalise(clnAnalise03, 1);
                        break;
                    case 2:
                        montaColumnaAnalise(clnAnalise04, 2);
                        break;
                    case 3:
                        montaColumnaAnalise(clnAnalise05, 3);
                        break;
                    case 4:
                        montaColumnaAnalise(clnAnalise06, 4);
                        break;
                    case 5:
                        montaColumnaAnalise(clnAnalise07, 5);
                        break;
                    case 6:
                        montaColumnaAnalise(clnAnalise08, 6);
                        break;
                    case 7:
                        montaColumnaAnalise(clnAnalise09, 7);
                        break;
                    case 8:
                        montaColumnaAnalise(clnAnalise10, 8);
                        break;
                    case 9:
                        montaColumnaAnalise(clnAnalise11, 9);
                        break;
                    case 10:
                        montaColumnaAnalise(clnAnalise12, 10);
                        break;
                    case 11:
                        montaColumnaAnalise(clnAnalise13, 11);
                        break;
                    case 12:
                        montaColumnaAnalise(clnAnalise14, 12);
                        break;
                    case 13:
                        montaColumnaAnalise(clnAnalise15, 13);
                        break;
                }
            }
        }
    }

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb  rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // Inicializa o GenericDao
        produtoDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Produto.class);

        TextFieldUtils.upperCase(tboxCodigoProduto);
        TextFieldUtils.inSql(tboxCodigoProduto);
        TextFieldUtils.upperCase(tboxCodigoColecao);
        TextFieldUtils.inSql(tboxCodigoColecao);
        TextFieldUtils.upperCase(tboxClasse);
        TextFieldUtils.upperCase(tboxModelagem);
        TextFieldUtils.upperCase(tboxLinha);

        btnLoadMedia.setDisable(true);
        //<editor-fold defaultstate="collapsed" desc="Carga dos produtos">
        try {
            produtoDao
                    .initCriteria()
                    .addPredicateEq("codigo", "0"); // 0

            listDisponiveis.set(produtoDao.loadListByPredicate());

            listDisponiveisAll.addAll(listDisponiveis);

            tblProdutoSelecionado.itemsProperty().bind(listDisponiveis);
            tblProdutoSelecionado.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            tblProdutoSelecionado.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    carregaPercentuais(newValue, true);

                    montaGridAnalise(newValue);
                } else {
                    carregaPercentuais(oldValue, true);

                    montaGridAnalise(oldValue);
                }
            });

            tblProdutoSelecionado.setRowFactory(selected -> new TableRow<Produto>() {
                @Override
                public void updateItem(Produto item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null) {
                        setStyle("");
                    } else {
                        double percCor = 0.0;
                        double percTam = 0.0;
                        if (item.getCoresDemanda() != null) {
                            for (Cor cor : item.getCoresDemanda()) {
                                percCor += cor.getPercCorAsDouble();
                            }
                        }

                        if (item.getTamanhosDemanda() != null) {
                            for (Tamanho tamanho : item.getTamanhosDemanda()) {
                                percTam += tamanho.getPercTamAsDouble();
                            }
                        }

                        percCor = Math.round(percCor);
                        percTam = Math.round(percTam);

                        if (percCor == 100.0 && percTam == 100.0) {
                            setStyle("-fx-background-color:  #81cf81");
                        } else if (percCor == 100.0 || percTam == 100.0) {
                            setStyle("-fx-background-color:  #fbb34e");
                        } else {
                            setStyle("-fx-background-color:  #c9302c");
                        }
                    }
                }
            });

            // Listas dependentes
            listDisponiveis.forEach(produto -> {
                try {
                    // Cores
                    produto.setCoresDemanda(Objects.requireNonNull(DAOFactory.getProdutoDAO()).getCoresPorProduto("0"));
                    produto.setTamanhosDemanda(DAOFactory.getProdutoDAO().getTamanhosPorProduto("0"));
                } catch (SQLException ex) {
                    Logger.getLogger(ScenePcpCadastrarDemandaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            //clnStatus.setCellValueFactory(param -> {
//                if (param.getValue().getAlterou()) {
//                    return new SimpleObjectProperty<>(Constantes.newImageViewActive());
//                } else {
//                    return new SimpleObjectProperty<>(Constantes.newImageViewInactive());
//                }
//            });

            clnColecao.setCellValueFactory(param -> param.getValue().getColecao().descricaoProperty());

            clnLinha.setCellValueFactory(param -> param.getValue().getLinha().sdLinhaDescProperty());
            clnClasse.setCellValueFactory(param -> param.getValue().getEtiqueta().grupoProperty());
            clnModelagem.setCellValueFactory(param -> param.getValue().getEtiqueta().modelagemProperty());

            listCores.set(Objects.requireNonNull(DAOFactory.getProdutoDAO()).getCoresPorProduto("0"));
            tblCores.itemsProperty().bind(listCores);
            tblCores.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            listTamanhos.set(DAOFactory.getProdutoDAO().getTamanhosPorProduto("0"));
            tblTamanhos.itemsProperty().bind(listTamanhos);
            tblTamanhos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        } catch (SQLException ex) {
            Logger.getLogger(ScenePcpCadastrarDemandaController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        TextFieldUtils.upperCase(tboxProcurar);

        tboxProcurar.textProperty().addListener((observable, oldValue, newValue) -> searchAvailable(oldValue, newValue));
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Carga das Cores">
        tblCores.setEditable(true);

        @SuppressWarnings("unchecked")
        Callback<TableColumn<Cor, String>, TableCell<Cor, String>> genericStringCellCallback = new GenericCellFactory().buildCallback(colPercentualCorListener);

        clnPercentualCor.setCellValueFactory(new PropertyValueFactory<>("perccor"));
        clnPercentualCor.setCellFactory(genericStringCellCallback);
        clnPercentualCor.setStyle("-fx-alignment: CENTER-RIGHT;");
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Carga dos Tamanhos">
        tblTamanhos.setEditable(true);

        @SuppressWarnings("unchecked")
        Callback<TableColumn<Tamanho, String>, TableCell<Tamanho, String>> percTamanhoCellCallback = new GenericCellFactory().buildCallback(colPercentualTamListener);

        clnPercentualTam.setCellValueFactory(new PropertyValueFactory<>("perctam"));
        clnPercentualTam.setCellFactory(percTamanhoCellCallback);
        clnPercentualTam.setStyle("-fx-alignment: CENTER-RIGHT;");

        TextFieldUtils.upperCase(tboxProcurarTam);

        tboxProcurarTam.textProperty().addListener((observable, oldValue, newValue) -> searchTamanhosAvailable(oldValue, newValue));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Carga Job">
        getLogJob();
        getStatusJob();
        tfieldEstadoJob.textProperty().bind(statusJobLog);
        tFieldInicioUltimaRodada.textProperty().bind(jobInicioUltimoLog);
        tFieldDataProximaRodada.textProperty().bind(jobDataProximoLog);
        tFieldTempoUltimaRodada.textProperty().bind(jobTempoUltimoLog);
        // </editor-fold>
    }

    @FXML
    private void tboxCodigoProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
//        FilterProdutosView view = new FilterProdutosView();
//        view.show();
//        tboxCodigoProduto.setText(view.getResultAsString(false));
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxCodigoProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxCodigoProduto.setText(ctrolFilterProduto.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        event.consume();
    }

    @FXML
    private void tboxCodigoColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColecaoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        FilterColecoesView view = new FilterColecoesView();
        view.show(false);
        tboxCodigoColecao.setText(view.getResultAsString(false));

        if (event != null) {
            event.consume();
        }
    }

    @FXML
    private void tboxCodigoMarcaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarMarcaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMarcaOnAction(ActionEvent event) {
        try {
            FilterMarcaController controlFilterMarca = new FilterMarcaController(Modals.FXMLWindow.FilterMarca, tboxCodigoMarca.getText());
            if (!controlFilterMarca.returnValue.isEmpty()) {
                tboxCodigoMarca.setText(controlFilterMarca.returnValue);
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (event != null) {
            event.consume();
        }
    }

    @FXML
    private void tblProdutoSelecionadoOnMouseClicked(MouseEvent event) {
        carregaPercentuais(tblProdutoSelecionado.getSelectionModel().getSelectedItem(), true);
        produtoSelecionado = tblProdutoSelecionado.getSelectionModel().getSelectedItem();
        event.consume();
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnSalvarOnAction(ActionEvent event) {
        List<String> erros = new ArrayList<>();

        // Desabilita para evitar duplo clique.
        btnSalvar.setDisable(true);

        pnStatusLoad.setVisible(true);
        new Thread(() -> {
            Platform.runLater(() -> lblStatusProgresso.setText("Executando validações..."));

            if (produtoSelecionado == null) {
                Platform.runLater(() -> GUIUtils.showMessage("Nenhum produto foi selecionado!"));
                btnSalvar.setDisable(false);
                pnStatusLoad.setVisible(false);
                Platform.runLater(() -> lblStatusProgresso.setText(""));
                return;
            }
            if (produtoSelecionado.getAlterou()) {
                // verifica se os percentuais das cores não excede 100 %
                _percCor = 0.0;
                _percTam = 0.0;

                if (produtoSelecionado.getCoresDemanda() != null) {
                    produtoSelecionado.getCoresDemanda().forEach(cor -> _percCor += cor.getPercCorAsDouble());
                } else {
                    carregaPercentuais(produtoSelecionado, false);
                }

                if (_percCor > 100.0) {
                    erros.add("Produto " + produtoSelecionado.getCodigo() + " Coleção: " + produtoSelecionado.getColecao() + " - O Percentual das cores excede 100%\n");
                }

                if (produtoSelecionado.getTamanhosDemanda() != null) {
                    produtoSelecionado.getTamanhosDemanda().forEach(tamanho -> _percTam += tamanho.getPercTamAsDouble());
                } else {
                    carregaPercentuais(produtoSelecionado, false);
                }

                if (_percTam > 100.0) {
                    erros.add("Produto " + produtoSelecionado.getCodigo() + " Coleção: " + produtoSelecionado.getColecao() + " - O Percentual dos tamanhos excede 100%\n");
                }

                produtoSelecionado.setGravar(_percTam == 100.0 && _percCor == 100.0);
            }

            if (erros.size() > 0) {
                btnSalvar.setDisable(false);
                pnStatusLoad.setVisible(false);

                Platform.runLater(() -> GUIUtils.showMessageWithGrade("Alguns percentuais excedem o limite de 100%.", erros.toString()));
            } else {
                Platform.runLater(() -> lblStatusProgresso.setText("Preparando para gravar dados..."));
                // Monta lista de objetos para serem inseridos
                List<ProdutoOrdem> list = new ArrayList<>();
                if (produtoSelecionado.getGravar() && produtoSelecionado.getAlterou()) {
                    produtoSelecionado.getCoresDemanda().forEach(cor ->
                            produtoSelecionado.getTamanhosDemanda().forEach(tamanho ->
                                    list.add(new ProdutoOrdem(tamanho.getCodigo(), cor.getCor(), tamanho.getTam(),
                                            tamanho.getPercTamAsDouble() * (cor.getPercCorAsDouble() / 100.0)))
                            )
                    );
                }

                try {

                    if (list.isEmpty()) {
                        Platform.runLater(() -> lblStatusProgresso.setText("Sem registros para alterar"));
                        Platform.runLater(() -> GUIUtils.showMessage("Sem registro para atualizar.", Alert.AlertType.INFORMATION));
                    } else {
                        Platform.runLater(() -> lblStatusProgresso.setText("Gravando dados..."));
                        Objects.requireNonNull(DAOFactory.getProdutoDAO()).updateProdutoOrdem(list);
                        Platform.runLater(() -> GUIUtils.showMessage("Registros atualizados com sucesso.", Alert.AlertType.INFORMATION));
                    }

                    if (produtoSelecionado.getGravar() && produtoSelecionado.getAlterou()) {
                        produtoSelecionado.setAlterou(false);
                    }

                    produtoSelecionado = null;
                    tblProdutoSelecionado.refresh();
                    btnSalvar.setDisable(false);
                    pnStatusLoad.setVisible(false);
                } catch (Exception ex) {
                    btnSalvar.setDisable(false);
                    pnStatusLoad.setVisible(false);
                    Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);

                    Platform.runLater(() -> GUIUtils.showException(ex));
                }
            }
        }).start();

        event.consume();
    }

    @FXML
    public void btnProcurarColecaoAnaliseOnAction(ActionEvent actionEvent) {
        FilterColecoesView view = new FilterColecoesView();
        view.show(false);
        tboxCodigoColecaoAnalise.setText(view.getResultAsString(false));
        actionEvent.consume();
    }

    private Task loadCoresTamanhos(ObservableList<Produto> lista) {
        return new Task() {
            @Override
            protected Object call() {
                this.updateMessage("Preparando dados...");
                try {
                    for (Produto produto : lista) {
                        if (produto.getCoresDemanda() == null || produto.getCoresDemanda().size() <= 0) {
                            ObservableList<Cor> cores = Objects.requireNonNull(DAOFactory.getProdutoDAO()).getCoresPorProduto(produto.getCodigo());
                            if (cores.size() == 1 && cores.get(0).getCor().equals("UN")) {
                                if (cores.get(0).getPerccor().equals("0")) {
                                    cores.get(0).setPerccor("100");
                                }
                            }
                            produto.setCoresDemanda(cores);
                        }

                        if (produto.getTamanhosDemanda() == null || produto.getTamanhosDemanda().size() <= 0) {
                            ObservableList<Tamanho> tamanhos = Objects.requireNonNull(DAOFactory.getProdutoDAO()).getTamanhosPorProduto(produto.getCodigo());
                            // Insere a lista de cores dentro de cada produto parregado
                            produto.setTamanhosDemanda(tamanhos);
                        }
                    }
                } catch (SQLException e) {
                    Logger.getLogger(ScenePcpCadastrarDemandaController.class.getName()).log(Level.SEVERE, null, e);
                    GUIUtils.showException(e);
                }
                return true;
            }
        };
    }

    @FXML
    public void btnFiltrarOnAction(ActionEvent actionEvent) {

        try {
            // Inicializa o critéria
            produtoDao.initCriteria();

            produtoSelecionado = null;

            if (!tboxCodigoProduto.getText().trim().equals("")) {
                List<String> list = new ArrayList<>(Arrays.asList(tboxCodigoProduto.getText().trim().replace("'", "").split(",")));
                produtoDao.addPredicateIn("codigo", list.toArray());
            }

            if (!tboxCodigoColecao.getText().trim().equals("")) {
                GenericDao<Colecao> colecaoDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Colecao.class);
                colecaoDao
                        .initCriteria()
                        .addPredicateIn("codigo", new ArrayList<>(Arrays.asList(tboxCodigoColecao.getText().trim().replace("'", "").split(","))).toArray());

                ObservableList list = colecaoDao.loadListByPredicate();
                if (list.size() > 0) {
                    produtoDao.addPredicateIn("colecao", list.toArray());
                }
            }

            if (!tboxClasse.getText().trim().equals("") || !tboxModelagem.getText().trim().equals("")) {
                GenericDao<EtqProd> etqProdDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), EtqProd.class);

                etqProdDao.initCriteria();

                if (!tboxClasse.getText().trim().equals("")) {
                    etqProdDao.addPredicateEq("grupo", tboxClasse.getText().replace("'", ""));
                }

                if (!tboxModelagem.getText().trim().equals("")) {
                    etqProdDao.addPredicateEq("modelagem", tboxModelagem.getText().replace("'", ""));
                }

                ObservableList list = etqProdDao.loadListByPredicate();
                if (list.size() > 0) {
                    produtoDao.addPredicateIn("etiqueta", list.toArray());
                }
            }

            Marca marca = new Marca();
            if (!tboxCodigoMarca.getText().isEmpty()) {
                GenericDao<Marca> marcaDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Marca.class);

                produtoDao.addPredicateIn("marca", marcaDao
                        .initCriteria()
                        .addPredicateEq("sdGrupo", tboxCodigoMarca.getText().replaceAll("'", ""))
                        .loadListByPredicate().toArray());

            }

            if (!tboxLinha.getText().trim().equals("")) {
                GenericDao<Linha> linhaDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Linha.class);
                ObservableList list = linhaDao
                        .initCriteria()
                        .addPredicateLike("sdLinhaDesc", tboxLinha.getText().replace("'", ""))
                        .loadListByPredicate();
                produtoDao.addPredicateIn("linha", list.toArray());
            }

            produtoDao.addPredicateEq("ativo", "S");

            ObservableList<Produto> lista = produtoDao.loadListByPredicate();
            new LoadingModalWorker(loadCoresTamanhos(lista), true);
            listDisponiveis.clear();
            listDisponiveis.set(lista);

            listTamanhos.clear();
            listTamanhosAll.clear();
            listCores.clear();
            listDisponiveisAll.clear();
            listDisponiveisAll.addAll(listDisponiveis);

            tboxModelagem.setText("");
            tboxClasse.setText("");
            tboxLinha.setText("");

            tblProdutoSelecionado.getSelectionModel().selectFirst();

        } catch (SQLException ex) {
            Logger.getLogger(ScenePcpCadastrarDemandaController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        actionEvent.consume();
    }

    @FXML
    public void btnLoadMediaOnAction(ActionEvent actionEvent) {

        boolean continuar = true;
        if (Double.parseDouble(tboxPercentualTam.getText().replace("%", "")) > 0) {
            if (!GUIUtils.showQuestionMessageDefaultNao("Já existe um percentual informado deseja alterar?")) {
                continuar = false;
            }
        }

        if (continuar) {
            tblAnaliseGrade.getSelectionModel().selectLast();
            perc = 0d;
            for (AnaliseGradeDetalhe analiseGradeDetalhe : tblAnaliseGrade.getSelectionModel().getSelectedItem().getDetalhe()) {
                listTamanhosAll.forEach(tam -> {
                    if (tam.getTam().equals(analiseGradeDetalhe.getTamanho())) {
                        tam.setPerctam(analiseGradeDetalhe.getPercTamanho().toString());
                        perc += analiseGradeDetalhe.getPercTamanho().doubleValue();
                    }
                });
            }

            perc = Math.floor(perc);

            tboxPercentualTam.setText(perc.toString() + "%");
            if (perc > 100) {
                tboxPercentualTam.setStyle("-fx-text-fill: red;");
            } else {
                tboxPercentualTam.setStyle("-fx-text-fill: #000;");
            }
        }
    }

    public void btnProcurarLinhaOnAction(ActionEvent actionEvent) {
        FilterLinhaView view = new FilterLinhaView();
        view.show(true);

        tboxLinha.setText(view.itensSelecionados.get(0).getSdLinhaDesc());

        if (actionEvent != null) {
            actionEvent.consume();
        }
    }

    public void btnProcurarClasseModelagemOnAction(ActionEvent actionEvent) {
        FilterEtiqProdView filterEtiqProdView = new FilterEtiqProdView();
        filterEtiqProdView.show(true);

        tboxClasse.setText(filterEtiqProdView.itensSelecionados.get(0).getGrupo());
        tboxModelagem.setText(filterEtiqProdView.itensSelecionados.get(0).getModelagem());
    }

    @FXML
    public void btnAtualizarStatusOnAction(ActionEvent actionEvent) {
        getStatusJob();
    }

    @FXML
    public void btnRodarJobOnAction(ActionEvent actionEvent) {
        try {
            new NativeDAO().runNativeQueryUpdate("" +
                    "BEGIN\n" +
                    "  DBMS_SCHEDULER.RUN_JOB(\n" +
                    "    JOB_NAME            => 'J_SD_ANALISE_GRADE',\n" +
                    "    USE_CURRENT_SESSION => FALSE);\n" +
                    "END;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        getStatusJob();
    }

    private void getStatusJob() {
        try {
            List<Object> dadosLog = new NativeDAO().runNativeQuery("" +
                    "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO') estado,\n" +
                    "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                    "       last_run_duration tempo,\n" +
                    "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                    "  from user_scheduler_jobs\n" +
                    " where job_name = 'J_SD_ANALISE_GRADE'");

            if (dadosLog.size() > 0) {
                Map infosLog = (Map) dadosLog.get(0);
                statusJobLog.set(infosLog.get("ESTADO").toString());
                jobInicioUltimoLog.set(infosLog.get("ULT_RODADA").toString());
                jobTempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
                jobDataProximoLog.set(infosLog.get("PROX_RODADA").toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void getLogJob() {
        List<Object> dadosLog = new FluentDao().runNativeQuery("" +
                "select TO_CHAR(log.LOG_DATE, 'dd/MM/yyyy HH24:MI:SS') dt_log,\n" +
                "       decode(to_char(additional_info),\n" +
                "              'REASON=\"manual slave run\"',\n" +
                "              'Execução Manual',\n" +
                "              null,\n" +
                "              'Execução Automática',\n" +
                "              to_char(additional_info)) info\n" +
                "  from USER_SCHEDULER_JOB_LOG log\n" +
                " where log.log_id = (select max(log_id)\n" +
                "                       from USER_SCHEDULER_JOB_LOG\n" +
                "                      where job_name = 'J_SD_ANALISE_GRADE')\n");

        if (dadosLog.size() > 0) {
            Object[] infosLog = (Object[]) dadosLog.get(0);
            dataUltimoLog.set(infosLog[0].toString());
            infoUltimoLog.set(infosLog[1].toString());
        }
    }
}
