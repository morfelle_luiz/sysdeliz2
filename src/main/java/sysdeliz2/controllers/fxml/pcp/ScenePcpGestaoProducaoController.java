/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.control.CheckListView;
import org.controlsfx.control.MaskerPane;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.spreadsheet.*;
import sysdeliz2.controllers.fxml.SceneHistLiberacaoPcpController;
import sysdeliz2.controllers.fxml.SceneHistRevisaoPcpController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.SceneSaveLiberacaoPcpController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.GestaoProducaoDAO;
import sysdeliz2.daos.IndiceProjecaoDAO;
import sysdeliz2.models.Colecao;
import sysdeliz2.models.Log;
import sysdeliz2.models.Periodo;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePcpGestaoProducaoController implements Initializable {
    
    // Local variables
    CheckComboBox<Colecao> chkColecao;
    CheckComboBox<Periodo> chkPeriodo;
    GestaoProducaoDAO daoGestao;
    IndiceProjecaoDAO daoIndiceProjecao;
    Integer calculaTotalVenda = 0;
    Integer calculaTotalMeta = 0;
    String regraIndice = "";
    ObservableList<Map> dadosProducao;
    Double[] larguras = new Double[]{/*País*/40.0, /*Col*/ 67.0, /*Desc Col*/ 140.0, /*Catal*/ 40.0, /*Refer*/ 70.0, /*Desc Prod*/ 255.0, /*Dt Entrega*/ 75.0, /*Venda*/ 65.0,
            /*Marc*/ 77.0, /*Linha*/ 70.0, /*Class Mod*/ 135.0, /*Familia*/ 85.0, /*Familia*/ 67.0, /*Cod Tec*/ 75.0, /*Tec*/ 195.0, /*Consumo*/ 75.0, /*Preco*/ 72.0};
    SpreadsheetView tabelao = null;
    MaskerPane processPane = new MaskerPane();
    Boolean exibirSetores = new Boolean(false);
    
    // FXML variables
    @FXML
    private Button btnFecharJanela;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private VBox pnStatusLoad;
    @FXML
    private Button btnCarregar;
    @FXML
    private Button btnExibirSetores;
    @FXML
    private DatePicker tboxDataInicial;
    @FXML
    private DatePicker tboxDataFinal;
    @FXML
    private TextField tboxLotes;
    @FXML
    private TextField tboxColecoes;
    @FXML
    private GridPane grdFormFiltros;
    @FXML
    private VBox pnDockSpreadSheet;
    @FXML
    private TextField tboxReferencia;
    @FXML
    private Button btnZerarAnalista;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        daoGestao = DAOFactory.getGestaoProducaoDAO();
        daoIndiceProjecao = DAOFactory.getIndiceProjecaoDAO();
        
        tboxDataInicial.setValue(LocalDate.parse("1500-01-01"));
        tboxDataFinal.setValue(LocalDate.parse("2050-12-31"));
        
        processPane.setVisible(false);
        
        TextFieldUtils.upperCase(tboxReferencia);
    }
    
    // Local methods
    private void makeHeaderWrappable(TableColumn col, Pos posCol, TextAlignment tAlign, String cor) {
        Label label = new Label(col.getText());
        label.setStyle("-fx-padding: 2px; -fx-text-fill:#" + cor);
        label.setWrapText(true);
        label.setAlignment(posCol);
        label.setTextAlignment(tAlign);
        
        StackPane stack = new StackPane();
        stack.getChildren().add(label);
        stack.prefWidthProperty().bind(col.widthProperty().subtract(5));
        label.prefWidthProperty().bind(stack.prefWidthProperty());
        col.setText(null);
        col.setGraphic(stack);
    }
    
    private Integer totalVendaMarca(ObservableList<Map> dados, String colecao, String marca) {
        calculaTotalVenda = 0;
        
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca)) {
                calculaTotalVenda += (Integer.parseInt(mapData.get("clm18").toString()) - Integer.parseInt(mapData.get("clm20").toString()));
            }
        });
        
        return calculaTotalVenda;
    }
    
    private Integer totalMetaMarca(ObservableList<Map> dados, String colecao, String marca) {
        calculaTotalMeta = 0;
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca)) {
                calculaTotalMeta += (Integer.parseInt(mapData.get("clm22").toString()) - Integer.parseInt(mapData.get("clm20").toString()));
            }
        });
        
        return calculaTotalMeta;
    }
    
    private Integer totalConcentradoMarca(ObservableList<Map> dados, String colecao, String marca) {
        calculaTotalMeta = 0;
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca)) {
                calculaTotalMeta += Integer.parseInt(mapData.get("clm20").toString());
            }
        });
        
        return calculaTotalMeta;
    }
    
    private Integer totalVendaLinha(ObservableList<Map> dados, String colecao, String marca, String linha) {
        calculaTotalVenda = 0;
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca) && mapData.get("clm" + mapData.keySet().size()).equals(linha)) {
                calculaTotalVenda += (Integer.parseInt(mapData.get("clm18").toString()) - Integer.parseInt(mapData.get("clm20").toString()));
            }
        });
        
        return calculaTotalVenda;
    }
    
    private Integer totalMetaLinha(ObservableList<Map> dados, String colecao, String marca, String linha) {
        calculaTotalMeta = 0;
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca) && mapData.get("clm" + mapData.keySet().size()).equals(linha)) {
                calculaTotalMeta += (Integer.parseInt(mapData.get("clm22").toString()) - Integer.parseInt(mapData.get("clm20").toString()));
            }
        });
        
        return calculaTotalMeta;
    }
    
    private Integer totalConcentradoLinha(ObservableList<Map> dados, String colecao, String marca, String linha) {
        calculaTotalMeta = 0;
        dados.forEach((Map mapData) -> {
            if (mapData.get("clm2").equals(colecao) && mapData.get("clm9").equals(marca) && mapData.get("clm" + mapData.keySet().size()).equals(linha)) {
                calculaTotalMeta += Integer.parseInt(mapData.get("clm20").toString());
            }
        });
        
        return calculaTotalMeta;
    }
    
    private String getRegra(String colecao, String marca) throws SQLException {
        return daoIndiceProjecao.getRegraIndice(colecao, marca);
    }
    
    private SpreadsheetCell createCell(String textCell, Integer row, Integer column, Integer totalColumns) {
        SpreadsheetCell cell = null;
        String[] clnAnalista = null;
        String hasObs = "0";
        if (column.equals(21)) {
            clnAnalista = textCell.split("-");
            if (clnAnalista.length == 0) {
                textCell = "0";
            } else {
                textCell = clnAnalista[0];
                hasObs = clnAnalista[1];
            }
        }
        if (column < 18 && column != 4) {
            cell = SpreadsheetCellType.STRING.createCell(row, column - 1, 1, 1, textCell);
        } else {
            if (textCell.equals("-")) {
                cell = SpreadsheetCellType.STRING.createCell(row, column - 1, 1, 1, textCell);
            } else {
                cell = SpreadsheetCellType.DOUBLE.createCell(row, column - 1, 1, 1, Double.parseDouble(textCell.replace(',', '.')));
            }
        }
        cell.setEditable(true);
        
        if (column == 1) {
            if (textCell.equals("PY")) {
                cell.setGraphic(new ImageView("/images/paraguay-flag-round-icon-16.png"));
            } else {
                cell.setGraphic(new ImageView("/images/brazil-flag-round-icon-16.png"));
            }
        } else if (column == 8) {
//            if (textCell.contains("*")) {
//                textCell = textCell.replace('*', ' ').trim();
//                cell = SpreadsheetCellType.STRING.createCell(row, column - 1, 1, 1, textCell);
//                cell.activateCorner(SpreadsheetCell.CornerPosition.TOP_RIGHT);
//            }
            cell.setEditable(true);
            if (textCell.equals("LIVRE")) {
                cell.getStyleClass().remove("spreadsheet-cell-status-limit");
                cell.getStyleClass().remove("spreadsheet-cell-status-livre-com");
                cell.getStyleClass().remove("spreadsheet-cell-status-encerrado");
                cell.getStyleClass().add("spreadsheet-cell-status-livre");
            } else if (textCell.equals("*LIMITADO")) {
                cell.getStyleClass().remove("spreadsheet-cell-status-livre");
                cell.getStyleClass().remove("spreadsheet-cell-status-livre-com");
                cell.getStyleClass().remove("spreadsheet-cell-status-encerrado");
                cell.getStyleClass().add("spreadsheet-cell-status-limit");
            } else if (textCell.equals("*LIVRE")) {
                cell.getStyleClass().remove("spreadsheet-cell-status-limit");
                cell.getStyleClass().remove("spreadsheet-cell-status-livre");
                cell.getStyleClass().remove("spreadsheet-cell-status-encerrado");
                cell.getStyleClass().add("spreadsheet-cell-status-livre-com");
            } else if (textCell.equals("*ENCERRADA")) {
                cell.getStyleClass().remove("spreadsheet-cell-status-limit");
                cell.getStyleClass().remove("spreadsheet-cell-status-livre");
                cell.getStyleClass().remove("spreadsheet-cell-status-livre-com");
                cell.getStyleClass().add("spreadsheet-cell-status-encerrado");
            }
        } else if (column == 6) {
            if (textCell.endsWith("*")) {
                cell.activateCorner(SpreadsheetCell.CornerPosition.TOP_LEFT);
                cell.getStyleClass().add("spreadsheet-cell-producao");
            }
        }
        
        if (column == 18) {
            cell.getStyleClass().add("spreadsheet-cell-status-livre");
        } else if (column == 19) {
            cell.getStyleClass().add("spreadsheet-cell-vend-pend");
        } else if (column == 20) {
            cell.getStyleClass().add("spreadsheet-cell-concentrado");
        } else if (column == 21) {
            cell.setEditable(true);
            cell.getStyleClass().remove("spreadsheet-cell-status-limit");
            if (!hasObs.equals("0")) {
                cell.getStyleClass().add("spreadsheet-cell-status-limit");
            }
        } else if (column == 22) {
            cell.getStyleClass().add("spreadsheet-cell-meta");
        } else if (column == 23) {
            cell.getStyleClass().add("spreadsheet-cell-indice");
        } else if (column == 24) {
            cell.getStyleClass().add("spreadsheet-cell-projet");
        } else if (column == 25) {
            cell.getStyleClass().add("spreadsheet-cell-pr-meta");
        } else if (column == 26) {
            cell.getStyleClass().add("spreadsheet-cell-producao");
        } else if (column == 27 || column == 28) {
            cell.getStyleClass().add("spreadsheet-cell-estoq-most");
        } else if (column == 29 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-pos");
        } else if (column == 30 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-neg");
        } else if (column == 31 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-pos");
        } else if (column == 32 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-neg");
        } else if (column == 33 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-pos");
        } else if (column == 34 && !textCell.equals("-")) {
            cell.getStyleClass().add("spreadsheet-cell-sld-neg");
        } else if (column == 41) {
            cell.getStyleClass().add("spreadsheet-cell-confirm");
        } else if (column == 42) {
            cell.getStyleClass().add("spreadsheet-cell-estoq-most");
        } else if (column == 43) {
            cell.getStyleClass().add("spreadsheet-cell-totals");
        } else if (column == totalColumns - 1) {
            cell.getStyleClass().add("spreadsheet-cell-total-lote");
        }
        
        return cell;
    }
    
    // FXML methods
    @FXML
    private void btnCarregarOnAction(ActionEvent event) {
        
        // <editor-fold defaultstate="collapsed" desc="Validação dos dados de filtro noformulário de pesquisa.">
        // Listas para consulta em modo IN ()
        List<String> selectedColecoes = new ArrayList<>();
        List<String> selectedPeriodos = new ArrayList<>();
        List<String> selectedReferencias = new ArrayList<>();
        
        Integer periodoInicial = 0;
        Integer periodoFinal = 0;
        String[] lotes = tboxLotes.getText().split("-");
        if (lotes.length == 2) {
            try {
                periodoInicial = Integer.parseInt(lotes[0]);
                periodoFinal = Integer.parseInt(lotes[1]);
            } catch (NumberFormatException ex) {
                Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showMessage("Verifique os números de lotes informados.", AlertType.ERROR);
                return;
            }
            if (periodoInicial > periodoFinal) {
                GUIUtils.showMessage("O valor do lote inicial deve ser menor que o final.", AlertType.INFORMATION);
                return;
            }
    
            Integer finalPeriodoInicial = periodoInicial;
            Integer finalPeriodoFinal = periodoFinal;
            List<TabPrz> periodos = (List<TabPrz>) new FluentDao().selectFrom(TabPrz.class)
                    .where(it -> it
                            .equal("tipo", "P")
                            .equal("ativo", true)
                            .greaterThanOrEqualTo("prazo", String.valueOf(finalPeriodoInicial))
                            .lessThanOrEqualTo("prazo", String.valueOf(finalPeriodoFinal)))
                    .resultList();
    
            periodos.forEach(periodo -> selectedPeriodos.add(periodo.getPrazo()));
//            for (int i = periodoInicial; i <= periodoFinal; i++) {
//                selectedPeriodos.add(String.valueOf(i));
//            }
        } else {
            for (String periodo : tboxLotes.getText().split(";")) {
                selectedPeriodos.add(periodo);
            }
        }
        
        for (String colecao : tboxColecoes.getText().split(";")) {
            selectedColecoes.add(colecao);
        }
        
        for (String codigo : tboxReferencia.getText().split(";")) {
            selectedReferencias.add(codigo);
        }
        
        // Verificação de preenchimento de formulário para filtros
        if (selectedColecoes.isEmpty()) {
            GUIUtils.showMessage("Você deve selecionar uma coleção.", AlertType.INFORMATION);
            return;
        }
        if (selectedPeriodos.isEmpty()) {
            GUIUtils.showMessage("Você deve selecionar um período.", AlertType.INFORMATION);
            return;
        }
        if (tboxDataInicial.getValue() == null) {
            GUIUtils.showMessage("Você deve inserir uma data inicial.", AlertType.INFORMATION);
            return;
        }
        if (tboxDataFinal.getValue() == null) {
            GUIUtils.showMessage("Você deve inserir uma data final.", AlertType.INFORMATION);
            return;
        }
        if (tboxDataInicial.getValue().isAfter(tboxDataFinal.getValue())) {
            GUIUtils.showMessage("Você deve inserir uma data inicial menor que a data final.", AlertType.INFORMATION);
            return;
        }
        // </editor-fold>

        selectedPeriodos.sort((o1, o2) -> o1.compareTo(o2));

        processPane.setText("Carregando...");
        processPane.setVisible(true);
        try {
            ObservableList<Map> allData = null;
            allData = daoGestao.getProducaoByDataAndColecaoAndPeriodo(
                    tboxDataInicial.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    tboxDataFinal.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), selectedColecoes,
                    selectedPeriodos, selectedReferencias);
            
            //Carrega o MAP com o primeiro objeto do array de retorno da consulta
            Map<String, String> dataHeader = allData.get(0);
            //Remove a linha do cabeçalho do array principal de MAPs
            allData.remove(0);
            
            // <editor-fold defaultstate="collapsed" desc="Criação das células de totais - A1:(Última)1">
            Map<String, String> totalsHeader = new HashMap<String, String>();
            for (int j = 1; j <= dataHeader.keySet().size() - 1; j++) {
                totalsHeader.put("clm" + j, " ");
                //Colunas de totais a partir da coluna 18 (VENDA TOTAL)
                if (j == 17) {
                    totalsHeader.put("clm" + j, "Totais:");
                }
                if (j >= 18 && j <= dataHeader.keySet().size() - 1) {
                    if (j == 23) {
                        continue;
                    }
                    totalsHeader.put("clm" + j, "0");
                }
            }
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="Loop para cálculo do indice de projeção e totais">
            for (int i = 0; i < allData.size(); i++) {
                //Passando linha a linha (produto) calcula o indice a partir da regra cadastrada
                Map<String, String> mapRow = allData.get(i);
                try {
                    // <editor-fold defaultstate="collapsed" desc="Cálculo do indice de projeção">
                    //Obtem a regra cadastrada para o PRODUTO
                    regraIndice = getRegra(mapRow.get("clm2").toString(), mapRow.get("clm9").toString());
                    Double indice = 0.0;
                    //Se a regra for cadastrada para o indice na marca
                    if (regraIndice.equals("MARCA")) {
                        indice = totalVendaMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue()
                                / totalMetaMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue();
                        //Se a regra for para o cadastrado de indice por linha
                    } else if (regraIndice.equals("LINHA")) {
                        indice = totalVendaLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue()
                                / totalMetaLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue();
                        //Se for definido uma meta global para a marca
                    } else if (regraIndice.equals("METALINHA")) {
                        indice = Double.parseDouble(daoIndiceProjecao.getRegraIndiceByMeta(mapRow.get("clm2").toString(), mapRow.get("clm9").toString()));
                        if (indice == 0) {
                            indice = totalVendaLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue()
                                    / totalMetaLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue();
                        } else {
                            indice = totalVendaLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue()
                                    / (indice - totalConcentradoLinha(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()).doubleValue());
                        }
                        //Se foi definido uma meta global para a linha da marca
                    } else if (regraIndice.equals("METAMARCA")) {
                        indice = Double.parseDouble(daoIndiceProjecao.getRegraIndiceByMeta(mapRow.get("clm2").toString(), mapRow.get("clm9").toString(), mapRow.get("clm" + mapRow.keySet().size()).toString()));
                        if (indice == 0) {
                            indice = totalVendaMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue()
                                    / totalMetaMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue();
                        } else {
                            indice = totalVendaMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue()
                                    / (indice - totalConcentradoMarca(allData, mapRow.get("clm2").toString(), mapRow.get("clm9").toString()).doubleValue());
                        }
                    } else {
                        //Se o indice for definido específico para o produto/marca/linha
                        indice = Double.parseDouble(regraIndice);
                    }
                    //</editor-fold>
                    
                    //Calcula a projeção -> PROJEÇÃO = ((VENDA TOTAL - CONCENTRADO) / INDICE) + CONCENTRADO
                    Double projetado = ((Double.parseDouble(mapRow.get("clm18").toString()) - Double.parseDouble(mapRow.get("clm20").toString())) / indice)
                            + Double.parseDouble(mapRow.get("clm20").toString());
                    //Cálculo de diferencial da projeção sobre a meta -> PR_META = PROJEÇÃO - META
                    Double pr_meta = projetado - Double.parseDouble(mapRow.get("clm22").toString());
                    //Atribui ao MAP do produto os valores cálculados.
                    mapRow.put("clm23", String.format("%.4f", indice));
                    mapRow.put("clm24", projetado.intValue() + "");
                    mapRow.put("clm25", pr_meta.intValue() + "");
                    
                    // <editor-fold defaultstate="collapsed" desc="Cálculo de totais">
                    //Cálculo dos totais da linha 1
                    for (int j = 18; j <= dataHeader.keySet().size() - 1; j++) {
                        if (j == 23) {
                            continue;
                        }
                        // o cálculo é feito por somatório acumulativo de linha a linha (produto) do loop
                        int totalValue = Integer.parseInt(totalsHeader.get("clm" + j).toString()); //O que está na celula de total
                        int rowValue = Integer.parseInt(Boolean.logicalOr(mapRow.get("clm" + j).toString().equals("-"),
                                mapRow.get("clm" + j).toString().equals(" ")) ? "0" : j == 21 ? mapRow.get("clm" + j).toString().split("-")[0] : mapRow.get("clm" + j).toString());// + o valor da célula do produto
                        totalsHeader.put("clm" + j, (totalValue + rowValue) + ""); //salva no MAP de totais
                    }
                    //</editor-fold>
                } catch (SQLException ex) {
                    Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
            //</editor-fold>
            
            dadosProducao = allData; //ObservableList de MAPs de produtos
            // <editor-fold defaultstate="collapsed" desc="Criação do objeto do SPREADSHEET">
            GridBase grid = new GridBase(dadosProducao.size() + 2, dataHeader.keySet().size() - 1); //Cria o GRID do spreadsheet
            tabelao = new SpreadsheetView(grid); //Cria o spreadsheet
            pnDockSpreadSheet.getChildren().clear(); //Limpa o PANEL onde será colocado o spreadsheet (feito para re-consultas)
            pnDockSpreadSheet.getChildren().add(tabelao); //Adiciona o spreadsheet no painel
            VBox.setVgrow(tabelao, Priority.ALWAYS); //Alinhado o spreadsheet com a jenela
            tabelao.setShowRowHeader(true);
            tabelao.setShowColumnHeader(true);
            tabelao.setEditable(true); //Definindo o spreadsheet com editável para alteração da coluna ANALISTA, VENDA e LOTES
            tabelao.getStyleClass().add("spreadsheet");
            btnExibirSetores.setVisible(true);
            btnLimparFiltros.setVisible(true);
            
            tabelao.setOnKeyPressed((eventKeyPressed) -> {
                if (tabelao.getSelectionModel().getFocusedCell().getColumn() == 20) {
                    if (eventKeyPressed.isControlDown() && eventKeyPressed.getCode() == KeyCode.R) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellsRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        SpreadsheetCell cellRow = cellsRow.get(20);
                        cellRow.getStyleClass().add("spreadsheet-cell-status-livre");
                    }
                }
            });
            // </editor-fold>
            
            //Array que irá guardar as listas de células (linhas) do spreadsheet
            ArrayList<ObservableList<SpreadsheetCell>> rowsGrid = new ArrayList<>(dadosProducao.size() + 2);
            
            // <editor-fold defaultstate="collapsed" desc="Criação da linha(1) de totais">
            ObservableList<SpreadsheetCell> totalsCell = FXCollections.observableArrayList(); //Lista de células da linha de totais
            for (int i = 1; i <= dataHeader.keySet().size() - 1; i++) {
                //Cria uma célula por coluna e adiciona na lista para o array de linhas
                SpreadsheetCell cell = SpreadsheetCellType.STRING.createCell(0, i - 1, 1, 1, totalsHeader.get("clm" + i).toString());
                cell.setEditable(true);
                if (i >= 18 && i <= dataHeader.keySet().size() - 1) {
                    if (i != 23) {
                        cell.getStyleClass().add("spreadsheet-cell-totals");
                    }
                }
                totalsCell.add(cell);
            }
            // </editor-fold>
            rowsGrid.add(totalsCell);
            
            // <editor-fold defaultstate="collapsed" desc="Criação da linha(2) de cebeçalho da tabela">
            ObservableList<SpreadsheetCell> columnsCell = FXCollections.observableArrayList();
            for (int i = 1; i <= dataHeader.keySet().size() - 1; i++) {
                //Cria uma célula por coluna com a descrição da coluna
                SpreadsheetCell cell = SpreadsheetCellType.STRING.createCell(1, i - 1, 1, 1, dataHeader.get("clm" + i).toString());
                tabelao.getColumns().get(i - 1).setPrefWidth(70.0); //tamanho padrão para todas as colunas
                cell.setEditable(false);
                cell.setWrapText(true);
                // <editor-fold defaultstate="collapsed" desc="Estilizando as células de HEADER da tabela">
                cell.getStyleClass().add("spreadsheet-cell-header-table");
                if (i == 1 || i == 2 || i == 5 || i == 7 || i == 8) { //define como fixa as colunas PAÍS, COLEÇÃO, REFER., DT ENTREGA, VENDA
                    tabelao.getColumns().get(i - 1).setFixed(true);
                }
                
                if (i > 1 && i < 18) {
                    tabelao.getColumns().get(i - 1).setPrefWidth(larguras[i - 1]); //Altera a largura das colunas expecíficas para o definido no array(double) de larguras
//                } else if (i >= 35 && i <= 72) {
//                    tabelao.hideColumn(tabelao.getColumns().get(i - 1)); //Oculta as colunas dos setores menos ACAB., CONFIRMAÇÃO,  PCP
                }
                
                if (i == 18) {
                    cell.getStyleClass().add("spreadsheet-cell-status-livre");
                } else if (i == 19) {
                    cell.getStyleClass().add("spreadsheet-cell-vend-pend");
                } else if (i == 20) {
                    cell.getStyleClass().add("spreadsheet-cell-concentrado");
                } else if (i == 22) {
                    cell.getStyleClass().add("spreadsheet-cell-meta");
                } else if (i == 23) {
                    cell.getStyleClass().add("spreadsheet-cell-indice");
                } else if (i == 24) {
                    cell.getStyleClass().add("spreadsheet-cell-projet");
                } else if (i == 25) {
                    cell.getStyleClass().add("spreadsheet-cell-pr-meta");
                } else if (i == 26) {
                    cell.getStyleClass().add("spreadsheet-cell-producao");
                } else if (i == 27 || i == 28) {
                    cell.getStyleClass().add("spreadsheet-cell-estoq-most");
                }
                //</editor-fold>
                columnsCell.add(cell);
            }
            //</editor-fold>
            rowsGrid.add(columnsCell);
            
            // <editor-fold defaultstate="collapsed" desc="Criação das células das linhas(.) com os valores da tabela">
            for (int i = 0; i < dadosProducao.size(); i++) {
                ObservableList<SpreadsheetCell> cellRow = FXCollections.observableArrayList(); //Lista de células da linha (por produto)
                Map<String, String> rowMap = dadosProducao.get(i); //Cria o objeto MAP com o produto da linha "i". Esse objeto é a fonte de informações do produto para a linha da tabela
                for (int j = 1; j <= rowMap.keySet().size() - 1; j++) {
                    cellRow.add(createCell(rowMap.get("clm" + j).toString(), i + 2, j, rowMap.keySet().size()));//Cria as células com os valores das colunas do produto "i"
                }
                
                // <editor-fold defaultstate="collapsed" desc="Criação da ACTION de alteração na coluna VENDA">
                cellRow.get(7).textProperty().addListener((obs, oldvalue, newvalue) -> {
                    if (Boolean.logicalOr(
                            Boolean.logicalOr(
                                    obs.getValue().toUpperCase().equals("LIMITADO"),
                                    obs.getValue().toUpperCase().equals("LIVRE")),
                            obs.getValue().toUpperCase().equals("ENCERRADA"))) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellsRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        
                        try {
                            //Estilo da céluna com condicional
                            if (newvalue.toUpperCase().equals("LIVRE")) {
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-limit");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-livre");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-encerrado");
                                cellsRow.get(7).getStyleClass().add("spreadsheet-cell-status-livre-com");
                            } else if (newvalue.toUpperCase().equals("LIMITADO")) {
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-livre");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-livre-com");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-encerrado");
                                cellsRow.get(7).getStyleClass().add("spreadsheet-cell-status-limit");
                            } else if (newvalue.toUpperCase().equals("ENCERRADA")) {
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-livre");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-livre-com");
                                cellsRow.get(7).getStyleClass().remove("spreadsheet-cell-status-limit");
                                cellsRow.get(7).getStyleClass().add("spreadsheet-cell-status-encerrado");
                            }
                            //Abre a janela para adicionar comentário a alteração da coluna VENDA
                            SceneSaveLiberacaoPcpController liberaProducao = new SceneSaveLiberacaoPcpController(Modals.FXMLWindow.SceneSaveLiberacaoPcp, cellsRow.get(1).getText(), cellsRow.get(4).getText(),
                                    cellsRow.get(5).getText(), newvalue.toUpperCase(), oldvalue.toUpperCase());
                            liberaProducao.styleWindow = StageStyle.UNDECORATED;
                            liberaProducao.show();
                            //<<log>>
                            DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Alterar", "Gestão de Produção", cellsRow.get(4).getText(),
                                    "Alteração da venda do produto: " + cellsRow.get(5).getText() + " da coleção: " + cellsRow.get(1).getText() + " para " + newvalue.toUpperCase()));
                        } catch (IOException ex) {
                            Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        }
                    }
                });
                //</editor-fold>
                // <editor-fold defaultstate="collapsed" desc="Criação da ACTION de edição da coluna ANALISTA">
                cellRow.get(20).textProperty().addListener((obs, oldvalue, newvalue) -> {
                    Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                    ObservableList<SpreadsheetCell> cellsRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                    try {
                        Integer novaMeta = Integer.parseInt(newvalue.replace(".", ""));
                        Integer metaAnterior = Integer.parseInt(oldvalue.replace(".", ""));
                        //Salva o histórico de revisão da meta
                        daoGestao.saveHistoricoRev(cellsRow.get(4).getText(), cellsRow.get(1).getText(), metaAnterior, novaMeta, SceneMainController.getAcesso().getUsuario());
                        DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Alterar", "Gestão de Produção", cellsRow.get(4).getText(),
                                "Alteração da revisão meta do produto: " + cellsRow.get(5).getText() + " da coleção: " + cellsRow.get(1).getText() + " de " + oldvalue.replace(".", "")
                                        + " para " + newvalue.replace(".", "")));//<<log>>
                    } catch (NumberFormatException ex) {
                        GUIUtils.showMessage("Insira um número no campo analista!", AlertType.INFORMATION);
                    } catch (SQLException ex) {
                        Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                        GUIUtils.showException(ex);
                    }
                });
                //</editor-fold>
                rowsGrid.add(cellRow);
            }
            //</editor-fold>
            //rowsGrid.add(rowsCell); >> Dentro do LOOP
            
            grid.setRows(rowsGrid); //Insere as linhas no spreadsheet
            tabelao.getFixedRows().add(0); //Fixa as linhas 1 e 2 do spreadsheet
            tabelao.getFixedRows().add(1);
            tabelao.setContextMenu(null); //Desabilita a exibição do ContextMenu do spreadsheet
            
            ButtonMenu menuFiltro = new ButtonMenu();
            for (int i = 0; i < tabelao.getColumns().size(); i++) {
                Filter filtro = new FilterBase(tabelao, i);
                tabelao.setFilteredRow(1);
                tabelao.getColumns().get(i).setFilter(filtro);
                menuFiltro.createMenu(filtro, i, tabelao);
            }
            
            //Ação de botão direto para abrir a tela de planejamento de produção de referência
            tabelao.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                @Override
                public void handle(ContextMenuEvent event) {
                    // <editor-fold defaultstate="collapsed" desc="ContextMenu Planejamento">
                    if (tabelao.getSelectionModel().getFocusedCell().getColumn() == 4) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellsRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        //Cria o MAP do produto para envio na tela de planejamento
                        Map<String, String> produto = new HashMap<String, String>();
                        for (int i = 1; i <= cellsRow.size(); i++) {
                            produto.put("clm" + i, cellsRow.get(i - 1).getText());
                        }
                        try {
                            // <editor-fold defaultstate="collapsed" desc="Abrir a janela de planejamento de produto">
                            FXMLLoader root = new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/pcp/ScenePlanejamentoLote.fxml"));
                            ScenePlanejamentoLoteController cltrPlanejamento = new ScenePlanejamentoLoteController(produto,
                                    tboxDataInicial.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy", new Locale("pt", "BR"))),
                                    tboxDataFinal.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy", new Locale("pt", "BR"))),
                                    selectedColecoes,
                                    selectedPeriodos);
                            root.setController(cltrPlanejamento);
                            Stage mainStage = new Stage();
                            Scene scene;
                            scene = new Scene(root.load());
                            scene.getStylesheets().add("/styles/bootstrap2.css");
                            scene.getStylesheets().add("/styles/bootstrap3.css");
                            scene.getStylesheets().add("/styles/styles.css");
                            scene.getStylesheets().add("/styles/stylePadrao.css");
                            scene.getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css" );
                            
                            Image applicationIcon = new Image(getClass().getResourceAsStream(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/images/logo deliz (2).png" : "/images/icon-upwave.png"));
                            mainStage.getIcons().add(applicationIcon);
                            mainStage.setTitle("SysDeliz 2");
                            mainStage.setScene(scene);
                            mainStage.setMaximized(true);
//                            mainStage.initStyle(StageStyle.DECORATED);
//                            mainStage.initModality(Modality.APPLICATION_MODAL);
//                            mainStage.setResizable(false);
                            mainStage.setOnCloseRequest((WindowEvent arg0) -> {
                                mainStage.close();
                            });
                            mainStage.showAndWait();
                            //</editor-fold>
                            
                            if (cltrPlanejamento.changedPart) { //Verifica se houve alteração no planejamento do produto
                                //atualiza a linha do produto no spreadsheet com os dados alterados no planejamento
                                Map updateValueMap = dadosProducao.get(selectedRow - 2); //MAP com os dados OLD do produto
                                Integer metaAnterior = Integer.parseInt(updateValueMap.get("clm22").toString()); //Meta anterior
                                //MAP atualizado com os dados do produto modificado
                                Map updateDatabase = daoGestao.getProducaoByDataAndReferenciaAndPeriodo(
                                        tboxDataInicial.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                                        tboxDataFinal.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                                        cellsRow.get(4).getText(), selectedPeriodos);
                                Integer metaNova = Integer.parseInt(updateDatabase.get("clm22").toString()); //Nova meta planejada
                                //Atualização dos dados do planejamento alterado na linha do produto no spreadsheet
                                for (int j = 1; j <= updateDatabase.keySet().size() - 1; j++) {
                                    cellsRow.set(j - 1, createCell(updateDatabase.get("clm" + j).toString(), selectedRow, j, updateDatabase.keySet().size()));
                                    updateValueMap.put("clm" + j, updateDatabase.get("clm" + j).toString());
                                }
                                
                                // <editor-fold defaultstate="collapsed" desc="Cálculo do indice com os dados atualizados (idem início)">
                                regraIndice = getRegra(updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString());
                                Double indiceUpd = 0.0;
                                if (regraIndice.equals("MARCA")) {
                                    indiceUpd = totalVendaMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue()
                                            / totalMetaMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue();
                                } else if (regraIndice.equals("LINHA")) {
                                    indiceUpd = totalVendaLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue()
                                            / totalMetaLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue();
                                } else if (regraIndice.equals("METALINHA")) {
                                    indiceUpd = Double.parseDouble(daoIndiceProjecao.getRegraIndiceByMeta(updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()));
                                    if (indiceUpd <= 0) {
                                        indiceUpd = totalVendaLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue()
                                                / totalMetaLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue();
                                    } else {
                                        indiceUpd = totalVendaLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue()
                                                / indiceUpd - totalConcentradoLinha(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()).doubleValue();
                                    }
                                } else if (regraIndice.equals("METAMARCA")) {
                                    indiceUpd = Double.parseDouble(daoIndiceProjecao.getRegraIndiceByMeta(updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString(), updateDatabase.get("clm10").toString()));
                                    if (indiceUpd <= 0) {
                                        indiceUpd = totalVendaMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue()
                                                / totalMetaMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue();
                                    } else {
                                        indiceUpd = totalVendaMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue()
                                                / indiceUpd - totalConcentradoMarca(dadosProducao, updateDatabase.get("clm2").toString(), updateDatabase.get("clm9").toString()).doubleValue();
                                    }
                                } else {
                                    indiceUpd = Double.parseDouble(regraIndice);
                                }
                                Double projetado = ((Double.parseDouble(updateDatabase.get("clm18").toString()) - Double.parseDouble(updateDatabase.get("clm20").toString())) / indiceUpd) + Double.parseDouble(updateDatabase.get("clm20").toString());
                                Double pr_meta = projetado - Double.parseDouble(updateDatabase.get("clm22").toString());
                                cellsRow.set(22, createCell(String.format("%.4f", indiceUpd), selectedRow, 23, 23));
                                cellsRow.set(23, createCell(projetado.intValue() + "", selectedRow, 24, 24));
                                cellsRow.set(24, createCell(pr_meta.intValue() + "", selectedRow, 25, 25));
                                //</editor-fold>
                                
                                daoGestao.saveHistoricoRev(cellsRow.get(4).getText(), cellsRow.get(1).getText(), metaAnterior, metaNova, SceneMainController.getAcesso().getUsuario());
                                DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Alterar", "Gestão de Produção", cellsRow.get(4).getText(),
                                        "Alteração da meta do produto: " + cellsRow.get(5).getText() + " da coleção: " + cellsRow.get(1).getText() + " de " + metaAnterior + " para " + metaNova));//<<log>>
                                Notifications alertFinnaly = Notifications.create()
                                        .title("Gestão de Produção")
                                        .text("Atualizado produção e planejamento do produto.")
                                        .graphic(null)
                                        .hideAfter(Duration.seconds(5))
                                        .position(Pos.TOP_RIGHT);
                                alertFinnaly.showInformation();
                            }
                            
                        } catch (IOException ex) {
                            Logger.getLogger(SceneMainController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(SceneMainController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        }
                    }
                    //</editor-fold>
                    // <editor-fold defaultstate="collapsed" desc="ContextMenu Observação PCP">
                    if (tabelao.getSelectionModel().getFocusedCell().getColumn() == 5) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        try {
                            SceneProdutoObservacaoPcpController observacaoPcp = new SceneProdutoObservacaoPcpController(Modals.FXMLWindow.ObservacaoPcpProduto, cellRow);

                            if (observacaoPcp.produto.getObservacaoPcp() != null && !observacaoPcp.produto.getObservacaoPcp().isEmpty()) {
                                cellRow.get(5).activateCorner(SpreadsheetCell.CornerPosition.TOP_LEFT);
                                cellRow.get(5).getStyleClass().add("spreadsheet-cell-producao");
                            } else {
                                cellRow.get(5).deactivateCorner(SpreadsheetCell.CornerPosition.TOP_LEFT);
                                cellRow.get(5).getStyleClass().remove("spreadsheet-cell-producao");
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        }
                    }
                    //</editor-fold>
                    // <editor-fold defaultstate="collapsed" desc="ContextMenu Histórico de Venda">
                    if (tabelao.getSelectionModel().getFocusedCell().getColumn() == 7) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        if (cellRow.get(7).getText().startsWith("*")) {
                            try {
                                SceneHistLiberacaoPcpController histLiberacao = new SceneHistLiberacaoPcpController(Modals.FXMLWindow.SceneHistLiberacaoPcp, cellRow.get(4).getText(), cellRow.get(1).getText());
                                histLiberacao.styleWindow = StageStyle.UNDECORATED;
                                histLiberacao.show();
                            } catch (IOException ex) {
                                Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                                GUIUtils.showException(ex);
                            }
                        }
                    }
                    //</editor-fold>
                    // <editor-fold defaultstate="collapsed" desc="ContextMenu Histórico de Revisão">
                    if (tabelao.getSelectionModel().getFocusedCell().getColumn() == 20) {
                        Integer selectedRow = tabelao.getSelectionModel().getFocusedCell().getRow(); //Pega a linha selecionada
                        ObservableList<SpreadsheetCell> cellRow = tabelao.getItems().get(selectedRow); //Pega a lista de células da linha selecionada
                        try {
                            SceneHistRevisaoPcpController histRev = new SceneHistRevisaoPcpController(Modals.FXMLWindow.SceneHistRevMetaPcp, cellRow.get(4).getText(),
                                    cellRow.get(1).getText(), SceneMainController.getAcesso().getUsuario());
                            histRev.styleWindow = StageStyle.UNDECORATED;
                            histRev.show();
                        } catch (IOException ex) {
                            Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                            GUIUtils.showException(ex);
                        }
                    }
                    //</editor-fold>
                }
            });
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        processPane.setVisible(false);
    }
    
    @FXML
    private void btnExibirSetoresOnAction(ActionEvent event) {
        if (tboxColecoes.getLength() != 0) {
            // <editor-fold defaultstate="collapsed" desc="Abrir a janela de setores de produto">
            FXMLLoader root = new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/pcp/ScenePcpGestaoProducaoSetores.fxml"));
            ScenePcpGestaoProducaoSetoresController cltrPlanejamento = new ScenePcpGestaoProducaoSetoresController(tboxColecoes.getText());
            root.setController(cltrPlanejamento);
            Stage mainStage = new Stage();
            Scene scene;
            try {
                scene = new Scene(root.load());
                scene.getStylesheets().add("/styles/bootstrap2.css");
                scene.getStylesheets().add("/styles/bootstrap3.css");
                scene.getStylesheets().add("/styles/styles.css");
                scene.getStylesheets().add("/styles/stylePadrao.css");
                scene.getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css" );

                Image applicationIcon = new Image(getClass().getResourceAsStream(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/images/logo deliz (2).png" : "/images/icon-upwave.png"));
                mainStage.getIcons().add(applicationIcon);
                mainStage.setTitle("SysDeliz 2");
                mainStage.setScene(scene);
                //mainStage.initStyle(StageStyle.DECORATED);
                //mainStage.initModality(Modality.WINDOW_MODAL);
                mainStage.setResizable(true);
                mainStage.setOnCloseRequest((WindowEvent arg0) -> {
                    mainStage.close();
                });
                mainStage.show();
            } catch (IOException ex) {
                Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
            //</editor-fold>
        }
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
    
    }
    
    @FXML
    private void btnZerarAnalistaOnAction(ActionEvent event) {
        String colecaoProdutos = GUIUtils.showTextFieldDialog("Digite a coleção que deseja zerar a coluna analista:");
        if (colecaoProdutos != null && !colecaoProdutos.isEmpty()) {
            try {
                DAOFactory.getGestaoProducaoDAO().zerarAnalista(colecaoProdutos, Globals.getUsuarioLogado().getUsuario());
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpGestaoProducaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }
}

class ButtonMenu {
    
    private SpreadsheetView spv = null;
    private Integer columnGlobal = 0;
    private Boolean checkAll = true;
    private ObservableList<MenuItem> itensButtonMenu = FXCollections.observableArrayList();
    private ListView<String> listView;
    private List<String> colunsFiltered = new ArrayList<>();
    private List<String> columnOrder = new ArrayList<>();
    private List<CheckListView<String>> listsFilters = new ArrayList<>();
    ObservableList<SpreadsheetCell> cellsLine;
    
    private BitSet hiddenRows;
    private Set<String> stringSet = new HashSet<>();
    private Set<String> copySet = new HashSet<>();
    private Set<String> stringOld = new HashSet<>();
    
    public void updateLists(Filter filtro) {
        for (int i = 0; i < listsFilters.size(); i++) {
            if (!colunsFiltered.contains(i + "")) {
                final int itColumn = i;
                CheckListView<String> listaItens = listsFilters.get(i);
                stringSet = new HashSet<>();
                stringSet.clear();
                for (int j = spv.getFilteredRow() + 1; j < spv.getGrid().getRowCount(); ++j) {
                    if (!hiddenRows.get(j)) {
                        stringSet.add(spv.getGrid().getRows().get(j).get(i).getText());
                    }
                }
                listaItens.getItems().clear();
                listaItens.setItems(FXCollections.observableArrayList(stringSet));
                listaItens.getCheckModel().checkAll();
                listaItens.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
                    public void onChanged(ListChangeListener.Change<? extends String> clnm) {
                        while (clnm.next()) {
                            if (clnm.wasAdded()) {
                                for (String itenChecked : clnm.getAddedSubList()) {
                                    copySet.add(itenChecked);
                                }
                            } else {
                                for (String itenChecked : clnm.getRemoved()) {
                                    copySet.remove(itenChecked);
                                }
                            }
                            if (!colunsFiltered.contains(itColumn + "")) {
                                colunsFiltered.add(itColumn + "");
                                filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
                            }
                        }
                    }
                });
            }
        }
    }
    
    public ButtonMenu(/*Integer column, SpreadsheetView spv*/) {
//        this.column = column;
//        this.spv = spv;
//
//        stringSet.clear();
//        for (int i = spv.getFilteredRow() + 1; i < spv.getGrid().getRowCount(); ++i) {
//            stringSet.add(spv.getGrid().getRows().get(i).get(column).getText());
//        }
    }
    
    public void createMenu(Filter filtro, Integer column, SpreadsheetView spv) {
        Integer localColumn = column;
        this.columnGlobal = column;
        this.spv = spv;
        listView = new ListView<>();
        stringSet = new HashSet<>();
        stringSet.clear();
        //copySet.clear();
        for (int i = spv.getFilteredRow() + 1; i < spv.getGrid().getRowCount(); ++i) {
            stringSet.add(spv.getGrid().getRows().get(i).get(column).getText());
            copySet.add(spv.getGrid().getRows().get(i).get(column).getText());
        }
        
        itensButtonMenu.clear();
        //<editor-fold>
        MenuItem orderColumn = new MenuItem("Ordem Crescente", new ImageView("/images/icons8-alphabetical-sorting-16.png"));
        orderColumn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                columnGlobal = localColumn;
                
                if (spv.getComparator() == ascendingComp && columnOrder.contains(localColumn + "")) {
                    spv.setComparator(descendingComp);
                    orderColumn.setText("Remover Ordenação");
                } else if (spv.getComparator() == descendingComp && columnOrder.contains(localColumn + "")) {
                    spv.setComparator(null);
                    orderColumn.setText("Ordem Crescente");
                    columnOrder.remove(localColumn + "");
                } else if (!columnOrder.contains(localColumn + "")) {
                    spv.setComparator(ascendingComp);
                    orderColumn.setText("Ordem Decrescente");
                    columnOrder.add(localColumn + "");
                    filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
                }
            }
        });
        itensButtonMenu.add(orderColumn);
        
        SeparatorMenuItem menuSeparator = new SeparatorMenuItem();
        itensButtonMenu.add(menuSeparator);
        //</editor-fold>
        
        //<editor-fold>
        CheckListView<String> listaItens = new CheckListView<>(FXCollections.observableArrayList(stringSet));
        listaItens.getCheckModel().checkAll();
        listaItens.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                while (c.next()) {
                    if (c.wasAdded()) {
                        for (String itenChecked : c.getAddedSubList()) {
                            copySet.add(itenChecked);
                        }
                    } else {
                        for (String itenChecked : c.getRemoved()) {
                            copySet.remove(itenChecked);
                        }
                    }
                    if (!colunsFiltered.contains(column + "")) {
                        colunsFiltered.add(column + "");
                        filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
                    }
                }
            }
        });
        listsFilters.add(listaItens);
        
        TextField textFilterValue = new TextField();
        textFilterValue.setPromptText("Filtrar");
        textFilterValue.setOnKeyReleased(inserValue -> {
            if (textFilterValue.getText().length() >= 1) {
                stringOld.clear();
                stringOld.addAll(listaItens.getItems());
                listaItens.getItems().clear();
                copySet.removeAll(stringOld);
                for (String itemLista : stringOld) {
                    if (itemLista.contains(textFilterValue.getText().toUpperCase())) {
                        copySet.add(itemLista);
                        listaItens.getItems().add(itemLista);
                    }
                }
            } else if (textFilterValue.getText().length() == 0) {
                stringSet = new HashSet<>();
                stringSet.clear();
                for (int j = spv.getFilteredRow() + 1; j < spv.getGrid().getRowCount(); ++j) {
                    if (!hiddenRows.get(j)) {
                        stringSet.add(spv.getGrid().getRows().get(j).get(column).getText());
                    }
                }
                listaItens.getItems().clear();
                listaItens.setItems(FXCollections.observableArrayList(stringSet));
                
                for (int i = spv.getFilteredRow() + 1; i < spv.getGrid().getRowCount(); ++i) {
                    stringOld.add(spv.getGrid().getRows().get(i).get(column).getText());
                    copySet.add(spv.getGrid().getRows().get(i).get(column).getText());
                }
                listaItens.setItems(FXCollections.observableArrayList(stringOld));
            }
            if (!colunsFiltered.contains(column + "")) {
                colunsFiltered.add(column + "");
                filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
            }
            listaItens.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
                public void onChanged(ListChangeListener.Change<? extends String> c) {
                    while (c.next()) {
                        if (c.wasAdded()) {
                            for (String itenChecked : c.getAddedSubList()) {
                                copySet.add(itenChecked);
                            }
                        } else {
                            for (String itenChecked : c.getRemoved()) {
                                copySet.remove(itenChecked);
                            }
                        }
                        if (!colunsFiltered.contains(column + "")) {
                            colunsFiltered.add(column + "");
                            filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
                        }
                    }
                }
            });
            listaItens.getCheckModel().checkAll();
            listaItens.refresh();
        });
        CustomMenuItem menuFilterValue = new CustomMenuItem(textFilterValue);
        menuFilterValue.setHideOnClick(false);
        itensButtonMenu.add(menuFilterValue);
        
        listView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        if (item != null) {
                            CheckBox checkBox = new CheckBox();
                            checkBox.setSelected(copySet.contains(item));
                            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                                @Override
                                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                                    if (newValue) {
                                        copySet.add(item);
                                    } else {
                                        copySet.remove(item);
                                    }
                                    if (!colunsFiltered.contains(column + "")) {
                                        colunsFiltered.add(column + "");
                                    }
                                }
                            });
                            setGraphic(checkBox);
                        }
                    }
                };
            }
        });
        listView.setItems(FXCollections.observableArrayList(stringSet));
        
        CustomMenuItem itensColuna = new CustomMenuItem(listaItens);
        //CustomMenuItem itensColuna = new CustomMenuItem(listView);
        itensColuna.setHideOnClick(false);
        itensButtonMenu.add(itensColuna);
        //</editor-fold>
        
        //<editor-fold>
        Label lbCheckAll = new Label("Marcar/Desmarcar Todos", new ImageView("/images/icons8-double-tick-16.png"));
        CustomMenuItem checkAllItens = new CustomMenuItem(lbCheckAll);
        checkAllItens.setHideOnClick(false);
        checkAllItens.setOnAction(((event) -> {
            if (checkAll) {
                for (String item : listaItens.getItems()) {
                    listaItens.getCheckModel().clearCheck(item);
                    copySet.remove(item);
                }
                checkAll = Boolean.FALSE;
            } else {
                for (String item : listaItens.getItems()) {
                    listaItens.getCheckModel().check(item);
                    copySet.add(item);
                }
                checkAll = Boolean.TRUE;
            }
            if (!colunsFiltered.contains(column + "")) {
                colunsFiltered.add(column + "");
                filtro.getMenuButton().setGraphic(new ImageView("/images/icons8-conversion-16.png"));
            }
        }));
        itensButtonMenu.add(checkAllItens);
        
        SeparatorMenuItem menuSeparator2 = new SeparatorMenuItem();
        itensButtonMenu.add(menuSeparator2);
        
        Label lbClearFilter = new Label("Limpar Filtro", new ImageView("/images/icons8-clear-filters-16.png"));
        CustomMenuItem clearFilterItens = new CustomMenuItem(lbClearFilter);
        clearFilterItens.setHideOnClick(true);
        clearFilterItens.setOnAction(((event) -> {
            if (colunsFiltered.contains(column + "")) {
                colunsFiltered.remove(column + "");
                stringOld.clear();
                for (int i = spv.getFilteredRow() + 1; i < spv.getGrid().getRowCount(); ++i) {
                    stringOld.add(spv.getGrid().getRows().get(i).get(column).getText());
                    copySet.add(spv.getGrid().getRows().get(i).get(column).getText());
                }
                listaItens.setItems(FXCollections.observableArrayList(stringOld));
                listaItens.getCheckModel().checkAll();
                listaItens.refresh();
                filtro.getMenuButton().setGraphic(null);
                textFilterValue.clear();
            }
        }));
        itensButtonMenu.add(clearFilterItens);
        //</editor-fold>
        
        filtro.getMenuButton().getItems().addAll(itensButtonMenu);
        filtro.getMenuButton().showingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    hiddenRows.clear();
                    List<Integer> rowsHidden = new ArrayList<>();
                    for (String clm : colunsFiltered) {
                        List<String> columnItens = listsFilters.get(Integer.parseInt(clm)).getCheckModel().getCheckedItems();
                        for (int i = spv.getFilteredRow() + 1; i < spv.getGrid().getRowCount(); ++i) {
                            if (!columnItens.contains(spv.getGrid().getRows().get(i).get(Integer.parseInt(clm)).getText())) {
                                rowsHidden.add(i);
                            }
                        }
                    }
                    for (Integer row : rowsHidden) {
                        hiddenRows.set(row, true);
                    }
                    spv.setHiddenRows(hiddenRows);
                    atualizaSomatorio(spv);
                    
                    updateLists(filtro);
                    
                } else {
                    hiddenRows = new BitSet(spv.getHiddenRows().size());
                    hiddenRows.or(spv.getHiddenRows());
                }
            }
        });
    }
    
    private final Comparator ascendingComp = new Comparator<ObservableList<SpreadsheetCell>>() {
        @Override
        public int compare(ObservableList<SpreadsheetCell> o1, ObservableList<SpreadsheetCell> o2) {
            SpreadsheetCell cell1 = o1.get(columnGlobal);
            SpreadsheetCell cell2 = o2.get(columnGlobal);
            if (cell1.getRow() <= spv.getFilteredRow()) {
                return Integer.compare(cell1.getRow(), cell2.getRow());
            } else if (cell2.getRow() <= spv.getFilteredRow()) {
                return Integer.compare(cell1.getRow(), cell2.getRow());
            } else if (cell1.getCellType() == SpreadsheetCellType.INTEGER && cell2.getCellType() == SpreadsheetCellType.INTEGER) {
                return Integer.compare((Integer) cell1.getItem(), (Integer) cell2.getItem());
            } else if (cell1.getCellType() == SpreadsheetCellType.DOUBLE && cell2.getCellType() == SpreadsheetCellType.DOUBLE) {
                return Double.compare((Double) cell1.getItem(), (Double) cell2.getItem());
            } else {
                return cell1.getText().compareToIgnoreCase(cell2.getText());
            }
        }
    };
    
    private final Comparator descendingComp = new Comparator<ObservableList<SpreadsheetCell>>() {
        @Override
        public int compare(ObservableList<SpreadsheetCell> o1, ObservableList<SpreadsheetCell> o2) {
            SpreadsheetCell cell1 = o1.get(columnGlobal);
            SpreadsheetCell cell2 = o2.get(columnGlobal);
            if (cell1.getRow() <= spv.getFilteredRow()) {
                return Integer.compare(cell1.getRow(), cell2.getRow());
            } else if (cell2.getRow() <= spv.getFilteredRow()) {
                return Integer.compare(cell1.getRow(), cell2.getRow());
            } else if (cell1.getCellType() == SpreadsheetCellType.INTEGER && cell2.getCellType() == SpreadsheetCellType.INTEGER) {
                return Integer.compare((Integer) cell2.getItem(), (Integer) cell1.getItem());
            } else if (cell1.getCellType() == SpreadsheetCellType.DOUBLE && cell2.getCellType() == SpreadsheetCellType.DOUBLE) {
                return Double.compare((Double) cell2.getItem(), (Double) cell1.getItem());
            } else {
                return cell2.getText().compareToIgnoreCase(cell1.getText());
            }
        }
    };
    
    private void atualizaSomatorio(SpreadsheetView spv) {
        
        Map<String, String> totalsHeaderFiltro = new HashMap<String, String>();
        ObservableList<SpreadsheetCell> loadTotals = spv.getItems().get(0);
        for (int j = 1; j <= loadTotals.size(); j++) {
            totalsHeaderFiltro.put("clm" + j, " ");
            if (j == 17) {
                totalsHeaderFiltro.put("clm" + j, "Totais:");
            }
            if (j >= 18 && j <= totalsHeaderFiltro.keySet().size()) {
                if (j == 23) {
                    continue;
                }
                totalsHeaderFiltro.put("clm" + j, "0");
            }
        }
        int totalValue;
        int rowValue;
        GridBase gridBase = (GridBase) spv.getGrid();
        for (int i = 2; i < spv.getItems().size(); i++) {
            cellsLine = spv.getItems().get(i);
            for (int j = 18; j <= cellsLine.size(); j++) {
                if (j != 23) {
                    totalValue = Integer.parseInt(totalsHeaderFiltro.get("clm" + j).toString());
                    rowValue = Integer.parseInt(cellsLine.get(j - 1).getText().equals("-") ? "0" : cellsLine.get(j - 1).getText().replace(".", ""));
                    totalsHeaderFiltro.put("clm" + j, (totalValue + rowValue) + "");
                    gridBase.setCellValue(0, j - 1, (totalValue + rowValue) + "");
                }
            }
        }
    }
}
