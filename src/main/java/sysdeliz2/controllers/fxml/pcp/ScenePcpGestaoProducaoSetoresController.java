/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.controlsfx.control.table.TableFilter;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.LockedTableCell;

import java.net.URL;
import java.sql.SQLException;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePcpGestaoProducaoSetoresController implements Initializable {

    @FXML
    private TableView<Map> tblProducaoSetores;
    String colecao;

    public ScenePcpGestaoProducaoSetoresController(String colecao) {
        this.colecao = colecao;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            List<String> selectedColecoes = new ArrayList<>();
            selectedColecoes.addAll(Arrays.asList(this.colecao.split(";")));
            ObservableList<Map> allData = null;
            List<String[]> setoresProducao = DAOFactory.getGestaoProducaoDAO().getSetoresProducao();
            allData = DAOFactory.getGestaoProducaoDAO().getProducaoSetores(selectedColecoes, setoresProducao);
            Map<String, String> dataHeader = allData.get(0);
            allData.remove(0);
            tblProducaoSetores.setItems(allData);
            tblProducaoSetores.setEditable(true);

            Callback<TableColumn<Map, String>, TableCell<Map, String>> cellFactoryForMap = new Callback<TableColumn<Map, String>, TableCell<Map, String>>() {
                @Override
                public TableCell call(TableColumn p) {
                    return new TextFieldTableCell(new StringConverter() {
                        @Override
                        public String toString(Object t) {
                            return t != null ? t.toString() : " ";
                        }

                        @Override
                        public Object fromString(String string) {
                            return string;
                        }
                    });
                }
            };

            for (int i = 1; i <= dataHeader.keySet().size(); i++) {
                TableColumn column = new TableColumn(dataHeader.get("clm" + i));
                column.setPrefWidth(70);
                column.setEditable(false);

                if (i == 2) {
                    column.setPrefWidth(160);
                }

                column.setCellValueFactory(new MapValueFactory("clm" + i));

                tblProducaoSetores.getColumns().add(column);
                column.setCellFactory(cellFactoryForMap);
                if (i >= 1 && i <= 2) {
                    makeHeaderWrappable(column, Pos.CENTER_LEFT, TextAlignment.LEFT);
                    column.setCellFactory(coluna -> {
                        return new LockedTableCell() {
                            @Override
                            protected void updateItem(Object item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(empty ? " " : "" + getItem());
                                setStyle("-fx-background-color:  #BBBBBB");
                            }

                        };
                    });
                } else if (i == dataHeader.keySet().size()) {
                    makeHeaderWrappable(column, Pos.CENTER_LEFT, TextAlignment.LEFT);
                    column.setCellFactory(new Callback<TableColumn<Map, String>, TableCell<Map, String>>() {
                        @Override
                        public TableCell<Map, String> call(TableColumn<Map, String> param) {
                            TableCell cell = new TableCell<Map, String>() {
                                @Override
                                public void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(empty ? " " : "" + getItem());
                                    setGraphic(null);
                                    if (getItem() != null) {
                                        if (!getItem().equals("-")) {
                                            setStyle("-fx-background-color:  #5cc7f2");
                                        }
                                    }
                                }
                            };
                            return cell;
                        }
                    });
                } else {
                    makeHeaderWrappable(column, Pos.CENTER_LEFT, TextAlignment.CENTER);
                }

                column.setId("clm" + i);
                column.setText(dataHeader.get("clm" + i));
            }

            tblProducaoSetores.setRowFactory((TableView<Map> tv) -> {
                TableRow<Map> row = new TableRow<>();
                row.getStyleClass().add("table-row");

                return row;
            });
            //TableUtils.addCustomTableMenu(tblProducaoSetores);
            TableFilter filter = new TableFilter(tblProducaoSetores);
        } catch (SQLException ex) {
            ex.printStackTrace();
            GUIUtils.showException(ex);
        }
    }

    private void makeHeaderWrappable(TableColumn col, Pos posCol, TextAlignment tAlign) {
        Label label = new Label(col.getText());
        label.setWrapText(true);
        label.setAlignment(posCol);
        label.setTextAlignment(tAlign);

        StackPane stack = new StackPane();
        stack.getChildren().add(label);
        stack.prefWidthProperty().bind(col.widthProperty().subtract(5));
        label.prefWidthProperty().bind(stack.prefWidthProperty());
        col.setText(null);
        col.setGraphic(stack);
    }

}
