/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import com.google.gson.Gson;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import sysdeliz2.utils.GUIUtils;

import java.io.*;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePcpImportApiOrganizeController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Local vars">
    private final String API_PRODUCAO = "http://api.organizedobrasil.com.br";
    private final String MOD_AUTH = API_PRODUCAO + "/token";
    private final String MOD_GET_ROTEIRO = API_PRODUCAO + "/api/Roteiro";
    private final String MOD_GET_OPERACAO = API_PRODUCAO + "/api/Operacao?codigoRoteiro=";
    private final String USER_API = "Deliz.api";
    private final String PASS_API = "i8DrLd6iEJr4";

    private OAuthToken authToken = null;
    private ApiErrorReturn message = null;
    private Roteiro[] roteiros = null;
    private Operacao[] operacoes = null;

    private List<Maquina_001> maquinas_001 = new ArrayList<>();
    private List<Operacao_001> operacoes_001 = new ArrayList<>();
    private List<Pcpseqproc_001> pcpseqprocs_001 = new ArrayList<>();

    private Integer codigoMaquina = 1;
    private Integer codigoOperacao = 1;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="FXML components">
    @FXML
    private Button btnGetTokenOAuth;
    @FXML
    private ListView<String> lviewLogImport;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    //<editor-fold defaultstate="collapsed" desc="API methods">
    HttpResponse executeHttpRequest(String jsonData, HttpPost httpPost) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpPost);
    }

    HttpResponse executeHttpRequest(HttpGet httpGet) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpGet);
    }

    void executeOAuth(String restUrl, String username, String password) {
        authToken = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
        String entityValue = "grant_type=password&username=" + username + "&password=" + password;
        try {
            HttpResponse response = executeHttpRequest(entityValue, httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                authToken = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), OAuthToken.class);
                authToken.setTime_init(LocalTime.now());
            }
        } catch (UnsupportedEncodingException e) {
            GUIUtils.showException(e);
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpPost.releaseConnection();
        }
    }

    void executeGetRoteiros(String restUrl, OAuthToken tokenAuth) {
        roteiros = null;
        message = null;
        HttpGet httpGet = new HttpGet(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpGet.setHeader("authorization", auth);
        try {
            HttpResponse response = executeHttpRequest(httpGet);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                roteiros = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), Roteiro[].class);
            }
        } catch (UnsupportedEncodingException e) {
            GUIUtils.showException(e);
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpGet.releaseConnection();
        }
    }

    void executeGetOperacoesRoteiro(String restUrl, OAuthToken tokenAuth, String codigoRoteiro) {
        operacoes = null;
        message = null;
        HttpGet httpGet = new HttpGet(restUrl + codigoRoteiro);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpGet.setHeader("authorization", auth);
        try {
            HttpResponse response = executeHttpRequest(httpGet);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                operacoes = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), Operacao[].class);
            }
        } catch (UnsupportedEncodingException e) {
            GUIUtils.showException(e);
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpGet.releaseConnection();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Rules methods">
    private String getJsonStr(InputStream contentWsReturn) throws IOException {
        StringBuffer jsonStr = new StringBuffer();
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(contentWsReturn));
        while ((line = reader.readLine()) != null) {
            jsonStr.append(line);
        }
        return jsonStr.toString();
    }

    private Boolean connectApi() {
        executeOAuth(MOD_AUTH, USER_API, PASS_API);
        if (message != null) {
            GUIUtils.showMessage("Erro durante a conexão com o Organize."
                    + "\nERRO: " + message.getError()
                    + "\nMENSAGEM: " + message.getError_description(), Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private Boolean apiGetRoteiros(OAuthToken token) {
        executeGetRoteiros(MOD_GET_ROTEIRO, authToken);
        if (message != null) {
            GUIUtils.showMessage("Erro ao obter os roteiros do Organize."
                    + "\nERRO: " + message.getError()
                    + "\nMENSAGEM: " + message.getMessage(), Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private Boolean apiGetOperacoesRoteiro(OAuthToken token, String codigoRoteiro) {
        executeGetOperacoesRoteiro(MOD_GET_OPERACAO, authToken, codigoRoteiro);
        if (message != null) {
            GUIUtils.showMessage("Erro ao obter as operações do roteiro:#" + codigoRoteiro + " do Organize."
                    + "\nERRO: " + message.getError()
                    + "\nMENSAGEM: " + message.getMessage(), Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }

    private Boolean hasMaquina(Maquina_001 maquina) {
        for (Maquina_001 lMaquina_001 : maquinas_001) {
            if (lMaquina_001.equals(maquina)) {
                return true;
            }
        }
        return false;
    }

    private Boolean hasOperacao(Operacao_001 operacao) {
        for (Operacao_001 lOperacao_001 : operacoes_001) {
            if (lOperacao_001.equals(operacao)) {
                return true;
            }
        }
        return false;
    }

    private Maquina_001 getMaquina(Maquina_001 maquina) {
        for (Maquina_001 lMaquina_001 : maquinas_001) {
            if (lMaquina_001.equals(maquina)) {
                return lMaquina_001;
            }
        }
        return null;
    }

    private Operacao_001 getOperacao(Operacao_001 operacao) {
        for (Operacao_001 lOperacao_001 : operacoes_001) {
            if (lOperacao_001.equals(operacao)) {
                return lOperacao_001;
            }
        }
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="FXML actions">
    @FXML
    private void btnGetTokenOAuthOnAction(ActionEvent event) {

        lviewLogImport.getItems().clear();

        //<editor-fold defaultstate="collapsed" desc="oAuth validation">
        lviewLogImport.getItems().add("Conectando com a API Organize...");
        lviewLogImport.refresh();
        if (authToken == null) {
            if (!this.connectApi()) {
                return;
            }
        }

        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            if (!this.connectApi()) {
                return;
            }
        }
        lviewLogImport.getItems().add("Conectado com a API.");
        lviewLogImport.refresh();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Get roteiros API">
        lviewLogImport.getItems().add("Obtendo roteiros da API Organize...");
        lviewLogImport.refresh();
        if (!apiGetRoteiros(authToken)) {
            return;
        }
        lviewLogImport.getItems().add("Roteiros carregados.");
        lviewLogImport.refresh();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Get operacoes roteiro API">
        lviewLogImport.getItems().add("Obtendo operações da API Organize...");
        lviewLogImport.refresh();
        for (Roteiro roteiro : roteiros) {
            if (apiGetOperacoesRoteiro(authToken, roteiro.codigo.toString())) {
                roteiro.setOperacoes(operacoes);
            } else {
                roteiro.setOperacoes(null);
            }
        }
        lviewLogImport.getItems().add("Operações carregadas.");
        lviewLogImport.refresh();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Create excel objects list">
        lviewLogImport.getItems().add("Preparando objetos Maquinas...");
        lviewLogImport.refresh();
        for (Roteiro roteiro : roteiros) {
            for (Operacao operacao : roteiro.getOperacoes()) {
                if (!hasMaquina(new Maquina_001(operacao.getEquipamento().getCodigo() + " - " + operacao.getEquipamento().getDescricao()))) {
                    maquinas_001.add(new Maquina_001(
                            String.format("%03d", codigoMaquina++),
                            operacao.getEquipamento().getCodigo() + " - " + operacao.getEquipamento().getDescricao(),
                            operacao.getEquipamento().getDescricao(),
                            operacao.getEquipamento().getInterferencia()));
                }
            }
        }
        lviewLogImport.getItems().add("Objetos Maquinas prontos.");
        lviewLogImport.refresh();

        lviewLogImport.getItems().add("Preparando objetos Operações...");
        lviewLogImport.refresh();
        for (Roteiro roteiro : roteiros) {
            for (Operacao operacao : roteiro.getOperacoes()) {
                if (!hasOperacao(new Operacao_001(operacao.getCodigo() + " - " + operacao.getDescricao()))) {
                    operacoes_001.add(new Operacao_001(
                            String.format("%03d", codigoOperacao++),
                            operacao.getCodigo() + " - " + operacao.getDescricao(),
                            operacao.getDescricao(),
                            getMaquina(new Maquina_001(operacao.getEquipamento().getCodigo() + " - " + operacao.getEquipamento().getDescricao())),
                            operacao.getTempo(),
                            operacao.getTempo(),
                            operacao.getCusto(),
                            operacao.getCompCostura(),
                            operacao.concessao.doubleValue()));
                }
            }
        }
        lviewLogImport.getItems().add("Objetos Operações prontos.");
        lviewLogImport.refresh();

        lviewLogImport.getItems().add("Preparando objetos PcpSeqProcs...");
        lviewLogImport.refresh();
        for (Roteiro roteiro : roteiros) {
            for (Operacao operacao : roteiro.getOperacoes()) {
                pcpseqprocs_001.add(new Pcpseqproc_001(
                        operacao.ordem,
                        roteiro.getSetor().getCodigo() == 118 ? "109" : roteiro.getSetor().getCodigo() == 123 ? "111" : "000",
                        getOperacao(new Operacao_001(operacao.getCodigo() + " - " + operacao.getDescricao())),
                        roteiro.getReferencia(),
                        " ",
                        roteiro.getPercentual(),
                        roteiro.getCusto1(),
                        roteiro.getCusto2(),
                        roteiro.getCusto3()
                ));
            }
        }
        lviewLogImport.getItems().add("objetos PcpSeqProcs prontos.");
        lviewLogImport.refresh();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Create excel file">
        lviewLogImport.getItems().add("Iniciando exportação Excel...");
        lviewLogImport.refresh();
        Workbook workbook = new org.apache.poi.xssf.usermodel.XSSFWorkbook();

        //<editor-fold defaultstate="collapsed" desc="Sheet maquinas">
        lviewLogImport.getItems().add("Preparando planilha de Maquinas...");
        lviewLogImport.refresh();
        Sheet sheet = workbook.createSheet("Organize - Maquinas");
        int rowCount = 0;
        Row rowHead = sheet.createRow(rowCount++);
        Cell cellHeadA = rowHead.createCell(0);
        cellHeadA.setCellValue("CODIGO");
        Cell cellHeadB = rowHead.createCell(1);
        cellHeadB.setCellValue("CODIGO ORGANIZE");
        Cell cellHeadC = rowHead.createCell(2);
        cellHeadC.setCellValue("DESCRIÇÃO");
        Cell cellHeadD = rowHead.createCell(3);
        cellHeadD.setCellValue("INTERFERÊNCIA");
        for (Maquina_001 maquina : maquinas_001) {
            Row row = sheet.createRow(rowCount++);
            Cell cellA = row.createCell(0);
            cellA.setCellValue((String) maquina.getCodigo());
            Cell cellB = row.createCell(1);
            cellB.setCellValue((String) maquina.getCodigo_organize());
            Cell cellC = row.createCell(2);
            cellC.setCellValue((String) maquina.getDescricao());
            Cell cellD = row.createCell(3);
            cellD.setCellValue(maquina.getInterferencia());
        }
        lviewLogImport.getItems().add("Planilha de Maquinas pronta.");
        lviewLogImport.refresh();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Sheet operações">
        lviewLogImport.getItems().add("Preparando planilha de Operações...");
        lviewLogImport.refresh();
        Sheet sheetOp = workbook.createSheet("Organize - Operacoes");
        rowCount = 0;
        Row rowHeadOp = sheetOp.createRow(rowCount++);
        Cell cellHeadOpA = rowHeadOp.createCell(0);
        cellHeadOpA.setCellValue("CODIGO");
        Cell cellHeadOpB = rowHeadOp.createCell(1);
        cellHeadOpB.setCellValue("CODIGO ORGANIZE");
        Cell cellHeadOpC = rowHeadOp.createCell(2);
        cellHeadOpC.setCellValue("DESCRICAO");
        Cell cellHeadOpD = rowHeadOp.createCell(3);
        cellHeadOpD.setCellValue("MAQUINA");
        Cell cellHeadOpE = rowHeadOp.createCell(4);
        cellHeadOpE.setCellValue("TEMPO");
        Cell cellHeadOpF = rowHeadOp.createCell(5);
        cellHeadOpF.setCellValue("TEMPO TOTAL");
        Cell cellHeadOpG = rowHeadOp.createCell(6);
        cellHeadOpG.setCellValue("CUSTO");
        Cell cellHeadOpH = rowHeadOp.createCell(7);
        cellHeadOpH.setCellValue("COMP CUSTURA");
        Cell cellHeadOpI = rowHeadOp.createCell(8);
        cellHeadOpI.setCellValue("CONCESSAO");
        for (Operacao_001 operacao : operacoes_001) {
            Row row = sheetOp.createRow(rowCount++);
            Cell cellOpA = row.createCell(0);
            cellOpA.setCellValue((String) operacao.getCodigo());
            Cell cellOpB = row.createCell(1);
            cellOpB.setCellValue((String) operacao.getCodigo_organize());
            Cell cellOpC = row.createCell(2);
            cellOpC.setCellValue((String) operacao.getDescricao());
            Cell cellOpD = row.createCell(3);
            cellOpD.setCellValue((String) operacao.getMaquina().getCodigo());
            Cell cellOpE = row.createCell(4);
            cellOpE.setCellValue(operacao.getTempo());
            Cell cellOpF = row.createCell(5);
            cellOpF.setCellValue(operacao.getTempoTotal());
            Cell cellOpG = row.createCell(6);
            cellOpG.setCellValue(operacao.getCusto());
            Cell cellOpH = row.createCell(7);
            cellOpH.setCellValue(operacao.getCompCustura());
            Cell cellOpI = row.createCell(8);
            cellOpI.setCellValue(operacao.getConcessao());
        }
        lviewLogImport.getItems().add("Planilha de Operações pronta.");
        lviewLogImport.refresh();
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Sheet roteiros">
        lviewLogImport.getItems().add("Preparando planilha de Roteiros...");
        lviewLogImport.refresh();
        Sheet sheetRot = workbook.createSheet("Organize - Roteiros");
        rowCount = 0;
        Row rowRot = sheetRot.createRow(rowCount++);
        Cell cellHeadRotA = rowRot.createCell(0);
        cellHeadRotA.setCellValue("ORDEM");
        Cell cellHeadRotB = rowRot.createCell(1);
        cellHeadRotB.setCellValue("SETOR");
        Cell cellHeadRotC = rowRot.createCell(2);
        cellHeadRotC.setCellValue("OPERACAO");
        Cell cellHeadRotD = rowRot.createCell(3);
        cellHeadRotD.setCellValue("PRODUTO");
        Cell cellHeadRotE = rowRot.createCell(4);
        cellHeadRotE.setCellValue("DESCRICAO");
        Cell cellHeadRotF = rowRot.createCell(5);
        cellHeadRotF.setCellValue("PERCENTUAL");
        Cell cellHeadRotG = rowRot.createCell(6);
        cellHeadRotG.setCellValue("CUSTO 1");
        Cell cellHeadRotH = rowRot.createCell(7);
        cellHeadRotH.setCellValue("CUSTO 2");
        Cell cellHeadRotI = rowRot.createCell(8);
        cellHeadRotI.setCellValue("CUSTO 3");
        for (Pcpseqproc_001 roteiro : pcpseqprocs_001) {
            Row row = sheetRot.createRow(rowCount++);
            Cell cellRotA = row.createCell(0);
            cellRotA.setCellValue(roteiro.getOrdem());
            Cell cellRotB = row.createCell(1);
            cellRotB.setCellValue((String) roteiro.getSetor());
            Cell cellRotC = row.createCell(2);
            cellRotC.setCellValue((String) roteiro.getOperacao().getCodigo());
            Cell cellRotD = row.createCell(3);
            cellRotD.setCellValue((String) roteiro.getProduto());
            Cell cellRotE = row.createCell(4);
            cellRotE.setCellValue((String) roteiro.getDescricao());
            Cell cellRotF = row.createCell(5);
            cellRotF.setCellValue(roteiro.getPercentual());
            Cell cellRotG = row.createCell(6);
            cellRotG.setCellValue(roteiro.getCusto1());
            Cell cellRotH = row.createCell(7);
            cellRotH.setCellValue(roteiro.getCusto2());
            Cell cellRotI = row.createCell(8);
            cellRotI.setCellValue(roteiro.getCusto3());
        }
        lviewLogImport.getItems().add("Planilha de Roteiros pronta.");
        lviewLogImport.refresh();
        //</editor-fold>

        try (FileOutputStream outputStream = new FileOutputStream("C:/SysDelizLocal/imports_organize.xlsx")) {
            workbook.write(outputStream);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScenePcpImportApiOrganizeController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpImportApiOrganizeController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        lviewLogImport.getItems().add("Exportação Excel finalizada.");
        lviewLogImport.refresh();
        //</editor-fold>

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="API JSON class return">
    private class ApiErrorReturn {

        private String error;
        private String error_description;
        private String message;

        public ApiErrorReturn() {
        }

        public ApiErrorReturn(String error, String error_description, String message) {
            this.error = error;
            this.error_description = error_description;
            this.message = message;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public String getError_description() {
            return error_description;
        }

        public void setError_description(String error_description) {
            this.error_description = error_description;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    private class OAuthToken {

        private String access_token;
        private String token_type;
        private Integer expires_in;
        private LocalTime time_init;

        public OAuthToken() {
        }

        public OAuthToken(String access_token, String token_type, Integer expires_in, LocalTime time_init) {
            this.access_token = access_token;
            this.token_type = token_type;
            this.expires_in = expires_in;
            this.time_init = time_init;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public Integer getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(Integer expires_in) {
            this.expires_in = expires_in;
        }

        public LocalTime getTime_init() {
            return time_init;
        }

        public void setTime_init(LocalTime time_init) {
            this.time_init = time_init;
        }

    }

    private class ParteRoteiro {

        private Integer codigo;
        private String descricao;

        public ParteRoteiro() {
        }

        public ParteRoteiro(Integer codigo, String descricao) {
            this.codigo = codigo;
            this.descricao = descricao;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
    }

    private class SetorRoteiro {

        private Integer codigo;
        private String descricao;

        public SetorRoteiro() {
        }

        public SetorRoteiro(Integer codigo, String descricao) {
            this.codigo = codigo;
            this.descricao = descricao;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

    }

    private class Equipamento {

        private Integer codigo;
        private String descricao;
        private Double interferencia;

        public Equipamento() {
        }

        public Equipamento(Integer codigo, String descricao, Double interferencia) {
            this.codigo = codigo;
            this.descricao = descricao;
            this.interferencia = interferencia;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Double getInterferencia() {
            return interferencia;
        }

        public void setInterferencia(Double interferencia) {
            this.interferencia = interferencia;
        }

    }

    private class Operacao {

        private Integer codigo;
        private String descricao;
        private Integer ordem;
        private Double tempo;
        private Double tempoHora;
        private Double custo;
        private Integer concessao;
        private Equipamento equipamento;
        private String dataInclusao;
        private String dataAlteracao;
        private Double compCostura;

        public Operacao() {
        }

        public Operacao(Integer codigo, String descricao, Integer ordem, Double tempo, Double tempoHora, Double custo, Integer concessao,
                Equipamento equipamento, String dataInclusao, String dataAlteracao, Double compCostura) {
            this.codigo = codigo;
            this.descricao = descricao;
            this.ordem = ordem;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo = custo;
            this.concessao = concessao;
            this.equipamento = equipamento;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = dataAlteracao;
            this.compCostura = compCostura;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Integer getOrdem() {
            return ordem;
        }

        public void setOrdem(Integer ordem) {
            this.ordem = ordem;
        }

        public Double getTempo() {
            return tempo;
        }

        public void setTempo(Double tempo) {
            this.tempo = tempo;
        }

        public Double getTempoHora() {
            return tempoHora;
        }

        public void setTempoHora(Double tempoHora) {
            this.tempoHora = tempoHora;
        }

        public Double getCusto() {
            return custo;
        }

        public void setCusto(Double custo) {
            this.custo = custo;
        }

        public Integer getConcessao() {
            return concessao;
        }

        public void setConcessao(Integer concessao) {
            this.concessao = concessao;
        }

        public Equipamento getEquipamento() {
            return equipamento;
        }

        public void setEquipamento(Equipamento equipamento) {
            this.equipamento = equipamento;
        }

        public String getDataInclusao() {
            return dataInclusao;
        }

        public void setDataInclusao(String dataInclusao) {
            this.dataInclusao = dataInclusao;
        }

        public String getDataAlteracao() {
            return dataAlteracao;
        }

        public void setDataAlteracao(String dataAlteracao) {
            this.dataAlteracao = dataAlteracao;
        }

        public Double getCompCostura() {
            return compCostura;
        }

        public void setCompCostura(Double compCostura) {
            this.compCostura = compCostura;
        }

    }

    private class Roteiro {

        private Integer codigo;
        private String referencia;
        private String descricao;
        private ParteRoteiro parte;
        private Double percentual;
        private SetorRoteiro setor;
        private Double tempo;
        private Double tempoHora;
        private Double custo1;
        private Double custo2;
        private Double custo3;
        private String dataInclusao;
        private String dataAlteracao;
        private Operacao[] operacoes;

        public Roteiro() {
        }

        public Roteiro(Integer codigo, String referencia, String descricao, ParteRoteiro parte, Double percentual, SetorRoteiro setor, Double tempo, Double tempoHora,
                Double custo1, Double custo2, Double custo3, String dataInclusao, String dataAlteracao) {
            this.codigo = codigo;
            this.referencia = referencia;
            this.descricao = descricao;
            this.parte = parte;
            this.percentual = percentual;
            this.setor = setor;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo1 = custo1;
            this.custo2 = custo2;
            this.custo3 = custo3;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = dataAlteracao;
        }

        public Roteiro(Integer codigo, String referencia, String descricao, ParteRoteiro parte, Double percentual, SetorRoteiro setor, Double tempo, Double tempoHora,
                Double custo1, Double custo2, Double custo3, String dataInclusao, String dataAlteracao, Operacao[] operacoes) {
            this.codigo = codigo;
            this.referencia = referencia;
            this.descricao = descricao;
            this.parte = parte;
            this.percentual = percentual;
            this.setor = setor;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo1 = custo1;
            this.custo2 = custo2;
            this.custo3 = custo3;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = dataAlteracao;
            this.operacoes = operacoes;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        public String getReferencia() {
            return referencia;
        }

        public void setReferencia(String referencia) {
            this.referencia = referencia;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public ParteRoteiro getParte() {
            return parte;
        }

        public void setParte(ParteRoteiro parte) {
            this.parte = parte;
        }

        public Double getPercentual() {
            return percentual;
        }

        public void setPercentual(Double percentual) {
            this.percentual = percentual;
        }

        public SetorRoteiro getSetor() {
            return setor;
        }

        public void setSetor(SetorRoteiro setor) {
            this.setor = setor;
        }

        public Double getTempo() {
            return tempo;
        }

        public void setTempo(Double tempo) {
            this.tempo = tempo;
        }

        public Double getTempoHora() {
            return tempoHora;
        }

        public void setTempoHora(Double tempoHora) {
            this.tempoHora = tempoHora;
        }

        public Double getCusto1() {
            return custo1;
        }

        public void setCusto1(Double custo1) {
            this.custo1 = custo1;
        }

        public Double getCusto2() {
            return custo2;
        }

        public void setCusto2(Double custo2) {
            this.custo2 = custo2;
        }

        public Double getCusto3() {
            return custo3;
        }

        public void setCusto3(Double custo3) {
            this.custo3 = custo3;
        }

        public String getDataInclusao() {
            return dataInclusao;
        }

        public void setDataInclusao(String dataInclusao) {
            this.dataInclusao = dataInclusao;
        }

        public String getDataAlteracao() {
            return dataAlteracao;
        }

        public void setDataAlteracao(String dataAlteracao) {
            this.dataAlteracao = dataAlteracao;
        }

        public Operacao[] getOperacoes() {
            return operacoes;
        }

        public void setOperacoes(Operacao[] operacoes) {
            this.operacoes = operacoes;
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Excel class sheet">
    private class Maquina_001 {

        private String codigo;
        private String codigo_organize;
        private String descricao;
        private Double interferencia;

        public Maquina_001() {
        }

        public Maquina_001(String codigo_organize) {
            this.codigo_organize = codigo_organize;
        }

        public Maquina_001(String codigo, String codigo_organize, String descricao, Double interferencia) {
            this.codigo = codigo;
            this.codigo_organize = codigo_organize;
            this.descricao = descricao;
            this.interferencia = interferencia;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getCodigo_organize() {
            return codigo_organize;
        }

        public void setCodigo_organize(String codigo_organize) {
            this.codigo_organize = codigo_organize;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Double getInterferencia() {
            return interferencia;
        }

        public void setInterferencia(Double interferencia) {
            this.interferencia = interferencia;
        }

        @Override
        public boolean equals(Object o) {

            if (o == null) {
                return false;
            }
            if (this == o) {
                return true;
            }
            if (getClass() != o.getClass()) {
                return false;
            }
            if (super.equals(o)) {
                return true;
            }

            final Maquina_001 other = (Maquina_001) o;
            if (this.codigo_organize.equals(other.getCodigo_organize())) {
                return true;
            }

            return false;
        }

    }

    private class Operacao_001 {

        private String codigo;
        private String codigo_organize;
        private String descricao;
        private Maquina_001 maquina;
        private Double tempo;
        private Double tempoTotal;
        private Double custo;
        private Double compCustura;
        private Double concessao;

        public Operacao_001() {
        }

        public Operacao_001(String codigo_organize) {
            this.codigo_organize = codigo_organize;
        }

        public Operacao_001(String codigo, String codigo_organize, String descricao, Maquina_001 maquina, Double tempo,
                Double tempoTotal, Double custo, Double compCustura, Double concessao) {
            this.codigo = codigo;
            this.codigo_organize = codigo_organize;
            this.descricao = descricao;
            this.maquina = maquina;
            this.tempo = tempo;
            this.tempoTotal = tempoTotal;
            this.custo = custo;
            this.compCustura = compCustura;
            this.concessao = concessao;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getCodigo_organize() {
            return codigo_organize;
        }

        public void setCodigo_organize(String codigo_organize) {
            this.codigo_organize = codigo_organize;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Maquina_001 getMaquina() {
            return maquina;
        }

        public void setMaquina(Maquina_001 maquina) {
            this.maquina = maquina;
        }

        public Double getTempo() {
            return tempo;
        }

        public void setTempo(Double tempo) {
            this.tempo = tempo;
        }

        public Double getTempoTotal() {
            return tempoTotal;
        }

        public void setTempoTotal(Double tempoTotal) {
            this.tempoTotal = tempoTotal;
        }

        public Double getCusto() {
            return custo;
        }

        public void setCusto(Double custo) {
            this.custo = custo;
        }

        public Double getCompCustura() {
            return compCustura;
        }

        public void setCompCustura(Double compCustura) {
            this.compCustura = compCustura;
        }

        public Double getConcessao() {
            return concessao;
        }

        public void setConcessao(Double concessao) {
            this.concessao = concessao;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            if (this == o) {
                return true;
            }
            if (getClass() != o.getClass()) {
                return false;
            }
            if (super.equals(o)) {
                return true;
            }

            final Operacao_001 other = (Operacao_001) o;
            if (this.codigo_organize.equals(other.getCodigo_organize())) {
                return true;
            }

            return false;
        }

    }

    private class Pcpseqproc_001 {

        private Integer ordem;
        private String setor;
        private Operacao_001 operacao;
        private String produto;
        private String descricao;
        private Double percentual;
        private Double custo1;
        private Double custo2;
        private Double custo3;

        public Pcpseqproc_001() {
        }

        public Pcpseqproc_001(Integer ordem, String setor, Operacao_001 operacao, String produto, String descricao,
                Double percentual, Double custo1, Double custo2, Double custo3) {
            this.ordem = ordem;
            this.setor = setor;
            this.operacao = operacao;
            this.produto = produto;
            this.descricao = descricao;
            this.percentual = percentual;
            this.custo1 = custo1;
            this.custo2 = custo2;
            this.custo3 = custo3;
        }

        public Integer getOrdem() {
            return ordem;
        }

        public void setOrdem(Integer ordem) {
            this.ordem = ordem;
        }

        public String getSetor() {
            return setor;
        }

        public void setSetor(String setor) {
            this.setor = setor;
        }

        public Operacao_001 getOperacao() {
            return operacao;
        }

        public void setOperacao(Operacao_001 operacao) {
            this.operacao = operacao;
        }

        public String getProduto() {
            return produto;
        }

        public void setProduto(String produto) {
            this.produto = produto;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public Double getPercentual() {
            return percentual;
        }

        public void setPercentual(Double percentual) {
            this.percentual = percentual;
        }

        public Double getCusto1() {
            return custo1;
        }

        public void setCusto1(Double custo1) {
            this.custo1 = custo1;
        }

        public Double getCusto2() {
            return custo2;
        }

        public void setCusto2(Double custo2) {
            this.custo2 = custo2;
        }

        public Double getCusto3() {
            return custo3;
        }

        public void setCusto3(Double custo3) {
            this.custo3 = custo3;
        }

    }
    //</editor-fold>
}
