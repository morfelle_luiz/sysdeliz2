/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import org.glassfish.jersey.client.ClientProperties;
import sysdeliz2.controllers.fxml.procura.FilterProdutoController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Of1001;
import sysdeliz2.models.OfIten001;
import sysdeliz2.models.properties.Composicao00X;
import sysdeliz2.models.properties.TagComposicao00X;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.dialog.FileChoice;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.StringUtils;

import javax.imageio.ImageIO;
import javax.print.*;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePcpImpressaoEtiquetaComposicaoController implements Initializable {
    
    private Of1001 ofSelected = null;
    private int columnCoresPosition = 1;
    private Tooltip tipTFieldProcura = new Tooltip("F4 - abre tela de filtro\nDEL - Limpa filtro");
    private int blackLimit = 380;
    private int total;
    private int widthBytes;
    private boolean compressHex = false;
    private static Map<Integer, String> mapCode = new HashMap<Integer, String>();
    private List<TagComposicao00X> tagsOf = new ArrayList();
    private String zplCodeTag = "";
    private String zplPreview = "";
    private int pos1 = 160;
    private int pos2 = 140;
    private int paginadorTags = 1;
    private final BooleanProperty disabledImprimir = new SimpleBooleanProperty(true);
    private final BooleanProperty disabledLimparGrade = new SimpleBooleanProperty(true);
    
    @FXML
    private TextField tboxNumeroOf;
    @FXML
    private TextField tboxProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TableView<OfIten001> tblGradeImpressao;
    @FXML
    private TableColumn<OfIten001, String> clnCorGrade;
    @FXML
    private TableColumn<OfIten001, String> clnComposicaoCorGrade;
    @FXML
    private Label lbDescProduto;
    @FXML
    private ImageView imgPreviewTag;
    @FXML
    private Button btnCriarTagComposicao;
    @FXML
    private Button btnCriarTagProduto;
    @FXML
    private Button btnImprimir;
    @FXML
    private Button btnLimparGrade;
    @FXML
    private CheckBox chboxGerarArquivo;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tboxNumeroOf.setTooltip(tipTFieldProcura);
        tboxProduto.setTooltip(tipTFieldProcura);
        
        btnImprimir.disableProperty().bind(disabledImprimir);
        btnLimparGrade.disableProperty().bind(disabledLimparGrade);
    }
    
    {
        mapCode.put(1, "G");
        mapCode.put(2, "H");
        mapCode.put(3, "I");
        mapCode.put(4, "J");
        mapCode.put(5, "K");
        mapCode.put(6, "L");
        mapCode.put(7, "M");
        mapCode.put(8, "N");
        mapCode.put(9, "O");
        mapCode.put(10, "P");
        mapCode.put(11, "Q");
        mapCode.put(12, "R");
        mapCode.put(13, "S");
        mapCode.put(14, "T");
        mapCode.put(15, "U");
        mapCode.put(16, "V");
        mapCode.put(17, "W");
        mapCode.put(18, "X");
        mapCode.put(19, "Y");
        mapCode.put(20, "g");
        mapCode.put(40, "h");
        mapCode.put(60, "i");
        mapCode.put(80, "j");
        mapCode.put(100, "k");
        mapCode.put(120, "l");
        mapCode.put(140, "m");
        mapCode.put(160, "n");
        mapCode.put(180, "o");
        mapCode.put(200, "p");
        mapCode.put(220, "q");
        mapCode.put(240, "r");
        mapCode.put(260, "s");
        mapCode.put(280, "t");
        mapCode.put(300, "u");
        mapCode.put(320, "v");
        mapCode.put(340, "w");
        mapCode.put(360, "x");
        mapCode.put(380, "y");
        mapCode.put(400, "z");
    }
    
    public String convertfromImg(BufferedImage image) throws IOException {
        String cuerpo = createBody(image);
        if (compressHex) {
            cuerpo = encodeHexAscii(cuerpo);
        }
        return headDoc() + cuerpo + footDoc();
    }
    
    private String createBody(BufferedImage orginalImage) throws IOException {
        StringBuffer sb = new StringBuffer();
        Graphics2D graphics = orginalImage.createGraphics();
        graphics.drawImage(orginalImage, 0, 0, null);
        int height = orginalImage.getHeight();
        int width = orginalImage.getWidth();
        int rgb, red, green, blue, index = 0;
        char auxBinaryChar[] = {'0', '0', '0', '0', '0', '0', '0', '0'};
        widthBytes = width / 8;
        if (width % 8 > 0) {
            widthBytes = (((int) (width / 8)) + 1);
        } else {
            widthBytes = width / 8;
        }
        this.total = widthBytes * height;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                rgb = orginalImage.getRGB(w, h);
                red = (rgb >> 16) & 0x000000FF;
                green = (rgb >> 8) & 0x000000FF;
                blue = (rgb) & 0x000000FF;
                char auxChar = '1';
                int totalColor = red + green + blue;
                if (totalColor > blackLimit) {
                    auxChar = '0';
                }
                auxBinaryChar[index] = auxChar;
                index++;
                if (index == 8 || w == (width - 1)) {
                    sb.append(fourByteBinary(new String(auxBinaryChar)));
                    auxBinaryChar = new char[]{'0', '0', '0', '0', '0', '0', '0', '0'};
                    index = 0;
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    
    private String fourByteBinary(String binaryStr) {
        int decimal = Integer.parseInt(binaryStr, 2);
        if (decimal > 15) {
            return Integer.toString(decimal, 16).toUpperCase();
        } else {
            return "0" + Integer.toString(decimal, 16).toUpperCase();
        }
    }
    
    private String encodeHexAscii(String code) {
        int maxlinea = widthBytes * 2;
        StringBuffer sbCode = new StringBuffer();
        StringBuffer sbLinea = new StringBuffer();
        String previousLine = null;
        int counter = 1;
        char aux = code.charAt(0);
        boolean firstChar = false;
        for (int i = 1; i < code.length(); i++) {
            if (firstChar) {
                aux = code.charAt(i);
                firstChar = false;
                continue;
            }
            if (code.charAt(i) == '\n') {
                if (counter >= maxlinea && aux == '0') {
                    sbLinea.append(",");
                } else if (counter >= maxlinea && aux == 'F') {
                    sbLinea.append("!");
                } else if (counter > 20) {
                    int multi20 = (counter / 20) * 20;
                    int resto20 = (counter % 20);
                    sbLinea.append(mapCode.get(multi20));
                    if (resto20 != 0) {
                        sbLinea.append(mapCode.get(resto20) + aux);
                    } else {
                        sbLinea.append(aux);
                    }
                } else {
                    sbLinea.append(mapCode.get(counter) + aux);
                    if (mapCode.get(counter) == null) {
                    }
                }
                counter = 1;
                firstChar = true;
                if (sbLinea.toString().equals(previousLine)) {
                    sbCode.append(":");
                } else {
                    sbCode.append(sbLinea.toString());
                }
                previousLine = sbLinea.toString();
                sbLinea.setLength(0);
                continue;
            }
            if (aux == code.charAt(i)) {
                counter++;
            } else {
                if (counter > 20) {
                    int multi20 = (counter / 20) * 20;
                    int resto20 = (counter % 20);
                    sbLinea.append(mapCode.get(multi20));
                    if (resto20 != 0) {
                        sbLinea.append(mapCode.get(resto20) + aux);
                    } else {
                        sbLinea.append(aux);
                    }
                } else {
                    sbLinea.append(mapCode.get(counter) + aux);
                }
                counter = 1;
                aux = code.charAt(i);
            }
        }
        return sbCode.toString();
    }
    
    private String headDoc() {
        String str = "^XA "
                + "^FO0,0^GFA," + total + "," + total + "," + widthBytes + ", ";
        return str;
    }
    
    private String footDoc() {
        String str = "^FS"
                + "^XZ";
        return str;
    }
    
    private void setCompressHex(boolean compressHex) {
        this.compressHex = compressHex;
    }
    
    private void setBlacknessLimitPercentage(int percentage) {
        blackLimit = (percentage * 768 / 100);
    }
    
    private String generateHexaImage(String imageName) throws IOException {
        BufferedImage orginalImage = ImageIO.read(new File("C:\\SysDelizLocal\\local_files\\" + imageName));
        setCompressHex(true);
        setBlacknessLimitPercentage(50);
        return convertfromImg(orginalImage);
    }
    
    private void alterarGradeTamanho(TableColumn.CellEditEvent<OfIten001, Integer> action, String valueColumn) {
        OfIten001 ofItenCorTam = action.getRowValue();
        ofItenCorTam.getTamanhos().replace(valueColumn, action.getNewValue());
    }
    
    private void makeColumnsTableCores() {
        
        if (!tblGradeImpressao.getItems().isEmpty()) {
            tblGradeImpressao.setEditable(true);
            tblGradeImpressao.getColumns().clear();
            OfIten001 cores = tblGradeImpressao.getItems().get(0);
            columnCoresPosition = 2;
            TableColumn<OfIten001, Integer> columnCor = new TableColumn<>("Cor");
            columnCor.prefWidthProperty().set(50.0);
            columnCor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OfIten001, Integer>, ObservableValue<Integer>>() {
                @Override
                public ObservableValue<Integer> call(TableColumn.CellDataFeatures<OfIten001, Integer> features) {
                    return new ReadOnlyObjectWrapper(((OfIten001) features.getValue()).getCor());
                }
            });
            tblGradeImpressao.getColumns().add(0, columnCor);
            TableColumn<OfIten001, Integer> columnComposicaoCor = new TableColumn<>("Composição Cor");
            columnComposicaoCor.prefWidthProperty().set(220.0);
            columnComposicaoCor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OfIten001, Integer>, ObservableValue<Integer>>() {
                @Override
                public ObservableValue<Integer> call(TableColumn.CellDataFeatures<OfIten001, Integer> features) {
                    return new ReadOnlyObjectWrapper(((OfIten001) features.getValue()).getComposicao());
                }
            });
            tblGradeImpressao.getColumns().add(1, columnComposicaoCor);
            cores.getHeadersTamanhos().forEach((keyColumn, valueColumn) -> {
                TableColumn<OfIten001, Integer> columnTamanho = new TableColumn<>(valueColumn);
                columnTamanho.prefWidthProperty().set(40.0);
                columnTamanho.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OfIten001, Integer>, ObservableValue<Integer>>() {
                    @Override
                    public ObservableValue<Integer> call(TableColumn.CellDataFeatures<OfIten001, Integer> features) {
                        return new ReadOnlyObjectWrapper(((OfIten001) features.getValue()).getTamanhos().get(valueColumn));
                    }
                });
                Callback<TableColumn<OfIten001, Integer>, TableCell<OfIten001, Integer>> cellFactoryQtde
                        = new Callback<TableColumn<OfIten001, Integer>, TableCell<OfIten001, Integer>>() {
                    public TableCell call(TableColumn p) {
                        return new ScenePcpImpressaoEtiquetaComposicaoController.EditingCellClnTamanho();
                    }
                };
                columnTamanho.setCellFactory(cellFactoryQtde);
                columnTamanho.setOnEditCommit(action -> {
                    alterarGradeTamanho(action, valueColumn);
                });
                tblGradeImpressao.getColumns().add(columnCoresPosition++, columnTamanho);
            });
            
        }
    }
    
    private Image getPreviewEtiqueta(String zpl) throws IOException {
        Client client = ClientBuilder.newBuilder().register(ScenePcpImpressaoEtiquetaComposicaoController.class).build();
        client.property(ClientProperties.CONNECT_TIMEOUT, 5000);
        client.property(ClientProperties.READ_TIMEOUT, 60000);
        // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
        WebTarget target = client.target("http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/");
        Invocation.Builder request = target.request();
        request.accept("image/png"); // omit this line to get PNG images back
        request.header("X-Rotation", "270");
        request.header("Content-Type", "application/x-www-form-urlencoded");
        
        try {
            Response response = response = request.post(Entity.entity(zpl, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus() == 200) {
                byte[] body = response.readEntity(byte[].class
                );
                File file = new File("C:\\SysDelizLocal\\local_files\\labelComposicao.png"); // change file name for PNG images
                Files.write(file.toPath(), body);
                String imaPath = file.toURI().toString();
                return new Image(imaPath);
            } else {
                String body = response.readEntity(String.class);
                GUIUtils.showMessage("Não foi possível gerar a imagem da etiqueta. Erro: " + body);
                return new Image(this.getClass().getResource("/images/erroGeracaoImagemTag.png").toExternalForm());
                //throw new SocketTimeoutException(body);
            }
        } catch (Exception ex) {
            return new Image(this.getClass().getResource("/images/erroGeracaoImagemTag.png").toExternalForm());
        }
    }
    
    private void criarPreviewComposicao() throws IOException {
        paginadorTags = 1;
        tagsOf.stream().limit(3).forEach(tagComposicao -> {
            if (paginadorTags == 1) {
                zplPreview += ""
                        + "^XA\n"
                        + "^MMT\n"
                        + "^PW791\n"
                        + "^LL0679\n"
                        + "^LS0\n";
                zplPreview += "" +
                        "^FO704,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO544,544^GFA,02560,02560,00020,:Z64:\n" +
                        "eJzt1MFu00AQBuDZLvL2kMYcg3DrPgKoCFxhxa9SngBLvQQ1ahb10BuXXpF4CF5goxx67JUDUhdxyI0acSBIls0/s3GaVBUPgLpRrOTzer27M7NED22jKcvXwYZpsWSzn+Nrjm+xEOA+Ff/gv0fbq7FGtLyxNLd8TPmVaba0RXPcIZjvzPIEgpVrFlGPbUvMTXwK69Ejtihjm7ZVYfkNER0R9Xn6uzdte2JpfxAs4RGSiU2/Lw3vHPTZTsmsWWbYxqRhz79QNLi0VML8st+rBUX5zFOpsarkLcVLO50uyCsyLpnbvWNLw0Mbzd2YnLKp3Z25/MBSfmjNzKUzp6YN7U796LWl7ImNlNW/LM091lZ62ECMWqIJ1mbfBbt4idgURKdshcN6k1QsdmrBxh9KYhiRqXQwfKmnxPp1/PGuJcOiJ+Pxs3118QJLzeK6z3bii2AR0TOzMLz3nucSBSuLU4ywO3VlsBxP+SZVsJnLDlY2q8wUE7u0OfbFqIv9HqlLp/7AbmhP7PMR7BMRkiKZyJ5q+u16pBvknO9ipG2CsJsFL49N+iEVtinm7Ku7+CqxIadHgXdUzXFnNRvHo60xZ87PgW6l+dRJDin8XphrpsbzvsCIbasQO+pycr3dZ5Nljq+3qquFtea7G7ct1Fu5YaEus3tss36VXB/TfU1W8C/bokmlVoadwbwMtTVelJGxZmVnbWvYYtweiDlz5dIrnuEejAswU05fSwQz/Ddw/NJOIQZiI1iMlQ6QeQjpmO2nM6QrKUqOVc2jfINxgvdqUg3x45lm4yd2KlJ4xw+uWzEEHWPqK2t4LvEbNiRHjJmfu/gctieGIwelpM9cygmfdIZ6M8oPeb0jMWpUy3tQjdmqYKk54zNpVMEOfTBzzRVGuUfwXrtgulVsKVthg6kamdjnmoXRsl8wY2HDYFoSSvrh3AhmJKF4PImHWCpmaFzdWl6rhYeV2a2deBqXSBif83qDHWMPYO/d8Nbm2CsYTqn+yhqpN5Rs/KEzjgLHBPv8NWRliBFHrMV6OjOc2BOLM4vvBpNaRczHa5Zy9iFw68+K7Th149Kms6dSg07P2jbt8lQKuMHJ29bxHYtt4Y3dTHjZ8DtFsHVvuTy0/6v9Be9JwdU=:238A\n" +
                        "^FT733,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT717,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT728,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT713,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT737,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT737,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT721,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT721,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT754,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT754,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 687;
                pos2 = 667;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplPreview += "" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
            } else if (paginadorTags == 2) {
                zplPreview += "" +
                        "^FO440,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO256,544^GFA,03072,03072,00024,:Z64:\n" +
                        "eJzt1T9v00AUAPB3PZTrkMaMQThxPwKoCFLVir9K+ARY6hLUKDnUoRtLVyQ+BF/gogwduzIg9RBDNmrEgJEsm/fuj2MnrRhg7KuUOL9c7+x3714AHuJ/BJP02t9xbjzcHa/oNXafkty80diMLnLnk/167in4Lzeu3BRMt5yTRxWG8otZ196lvznr6ZZ3oEtve8bVQkfOu/DIfD0iX1ZZIv2qHZgA9OgxB7dVdWb9sG89pNnChYy+NRzvpd8jn4PY8pEgnwF3/vwzdPpXElJ03Rj/KodOvNKQcsxA+AaChs+XOWgGQoVrOTy1Pj6WnbWagWIykoOVio+sx8dSrFS0UmxZwmCppyfWR09kh0n+U8JaYx5S7bxvHCqABeZBvt345Uvc7wRgTp4ol58wMh4olpPTn/UAHUBk3Do47zLjvSL4cJeH46Rr5vfz9NjlC0zNKCh65Gc62XgH4JnIBe2j9vfZsZ4mc5xtsFTpxmOcQZcRQ1+p0VHLV5lYSgivZOzyKdjlYRfYlWK/0W9hWPunCfpHACzAcFHvC4dfqgu8xHrXzX3nMsTSEjmlgrwej2/7ENCHolk/zPiYLhNcNytPm17QJe1vVbjnonPS55UJHam6bhl+zsUNcakpn86BfC8xPmmei2bc54vGuWsGHd36nDZC+y/bYftDuuO2n4zu8d3+w8zr4911G2Ge9G++B4uMtRwzivctoCrs4nhXQoraz6tKeA9wWN+4EtcqugbnQ3RqGiOm+I2pDOsh+pCuuGK4n7VP0QOFz4mVj+Uy8/5DCeCZaSa0/4Vz9hWdDl23AFYC887J6b8PMmC47nfnwjgWFq7Br6Xw9xm8JsdCDPDpLlRw4XxoHNsqHnl+riLhPPS+oMOgxz4/U+NQsoryls28Z9YjcU69d5o5P9bWxQ11A4i1LQo4UdZ5xcgj74m0zgo8CT3qN87BjbcupPOxdW4K2Yy368bWhSlkmr/eX+ORcQGzrO1xwXKNno7afqZhlmKB6tjnx/op5g39nRq3fY15RseO3Gt5afoDtpvgfdNpV2mPcb++eKf0klElVPjcTRd02BYSezTLm276DNbVbMsjusSC2J7H+IFityoqm/7U9A3FV1UVWbfnxTSgEn+RqiK4wwOZaHdeWmE2bpfxPD7EQ/xz/AFsKcHV:E6E3\n" +
                        "^FT469,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT453,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT463,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT448,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT473,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT473,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT457,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT457,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT490,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT490,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 420;
                pos2 = 400;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplPreview += "\n" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                
            } else {
                zplPreview += "" +
                        "^FO176,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO0,544^GFA,03072,03072,00024,:Z64:\n" +
                        "eJzt1D9vGzcUAPDHvEDsoBwzNohy9xW89QwoUj+KgwxdD/AQAS10LAykYzu3QD9DxixFaGQovDRzNgodvAQ1O1kFBF3feyQVykoQBMhoGj6dfnd6/PPIB3DbvkRzfBkfsBKvDt/3fKnTl2YjH9/S/4Jvfkh+dHcXewr54Xv3OUTYc8VuBmo+dxY9ZHd5cNFf3nCEkXjL7ufBJB/BHXk8ER/Wjcu9IhwBaJ5mdT0Mf0S/P45ecbRq7sy/hXe0Wloc8IZP0JLPQCX/+h3g+KmDlrwr3n+4AayfBGiVhVB9Y3Xh85MNhbLoq1NnjqM3E4enfgYn4IyrVr5+EL0mX3mz8uAHqHyYPoo+GTsEp/5zcBpovm2XfCwOvYW5BeOmYecXv1G+G3JaH9f4tD6VEdceNuz8F12TW8C1im6Tj0BcL/VfH/KqaZ5L/BxHw8WvtDQTvdXsj0NTOC0fbuip8V0eP0Zvm1cczfv2vf9OEbreWPKVnzzY8ydr9A6qp65O64n24hdK7d80L/Jn1uz87QvyNxZ6T/nd5UXZ7/0I1ECZCGXelatobkiLgGv23fv0cRc0f1mW+wfEG0mprZ6t++PSl3zH+R22aV58TsZqkBaM7/O+Bfq+wSvxjtcze0+uGvGj8lyU7WM+L89d0eTohkPvdg/3m4RoDzjWk8lH/LD+gOXLV4deNJmp/YQrmK9jsOy0omtOdr+NndOo0OHOz4fhLLu2aMfiJ3jpzaVNbujnldx5dWV5Z0SvyA3fUSnbUj6yT8m1p3nSzqcaOcu+8GjVWooJ53+ZPZDzoRuRUPBtciVOvx7Rb6jf6+QoThuL+lCXDvM49Qt22oiaZvfa69fJzRE7lVU68urcm7PkVXaqD2hDk9dnKk7TGdgXs+yL6ObsnBymi+whOl79SL1D3cVNAY98dDVYdhOSNy46zZOGypNIbtP70dFlhzhO2cjyfuy3jo6ykTn+Lr/iRhztbLHv9RI2HXk72ffHAWYtbdCuRlv6Ma3bS3Lf7PspDYacKvJPUHov9YHKjf5zzzmrlOPvHP6TnbclG++E3qpt6RwD5o5r9KZ0qTO0r2Y33PDgaG1uxhEfebj2Zij9ntSNE7Uahp/jvOJ5iQUI0Pdb/QHXrgkY122vSeLsoatDum237bPb/8Z052s=:1FA9\n" +
                        "^FT206,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT190,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT200,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT184,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT210,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT210,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT194,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT194,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT227,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT227,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 160;
                pos2 = 140;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplPreview += "\n" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",96^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplPreview += ""
                        + "^PQ1,0,1,Y\n"
                        + "^XZ\n";
                paginadorTags = 0;
            }
            paginadorTags++;
        });
        if (tagsOf.size() % 3 != 0) {
            zplPreview += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
        
    }
    
    private void criarPreviewProduto() throws IOException {
        paginadorTags = 1;
        tagsOf.stream().limit(3).forEach(tagComposicao -> {
            if (paginadorTags == 1) {
                zplPreview += ""
                        + "^XA\n"
                        + "^MMT\n"
                        + "^PW830\n"
                        + "^LL0719\n"
                        + "^LS0\n";
                zplPreview += ""
                        + "^FT185,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO40,520^IMR:COMP.GRF^FS\n"
                        + "^FT213,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT195,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT230,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT230,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT202,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT202,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT176,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT176,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 160;
                pos2 = 140;
                tagComposicao.getComposicao().forEach(composicaoTag -> {
                    zplPreview += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicaoTag.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicaoTag.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplPreview += ""
                        + "^FT130,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
            } else if (paginadorTags == 2) {
                zplPreview += ""
                        + "^FT455,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO300,520^IMR:COMP.GRF^FS\n"
                        + "^FT470,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT452,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT487,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT487,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT459,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT459,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT433,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT433,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 420;
                pos2 = 400;
                tagComposicao.getComposicao().forEach(composicaoTag -> {
                    zplPreview += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicaoTag.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicaoTag.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplPreview += ""
                        + "^FT390,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                
            } else {
                zplPreview += ""
                        + "^FT700,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO540,520^IMR:COMP.GRF^FS\n"
                        + "^FT716,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT698,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT733,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT733,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT705,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT705,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT679,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT679,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 667;
                pos2 = 647;
                tagComposicao.getComposicao().forEach(composicaoTag -> {
                    zplPreview += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicaoTag.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicaoTag.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplPreview += ""
                        + "^FT630,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                zplPreview += ""
                        + "^PQ1,0,1,Y\n"
                        + "^XZ\n";
                paginadorTags = 0;
            }
            paginadorTags++;
        });
        if (tagsOf.size() % 3 != 0) {
            zplPreview += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
        
    }
    
    @FXML
    private void tboxNumeroOfOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            try {
                ofSelected = DAOFactory.getOf1001DAO().getOfByNumero(tboxNumeroOf.getText());
                if (ofSelected == null) {
                    GUIUtils.showMessage("Ordem de Produção não encontrada!", Alert.AlertType.INFORMATION);
                    return;
                }
                
                tblGradeImpressao.setItems(ofSelected.getGrade());
                makeColumnsTableCores();
                
                tboxProduto.setText(ofSelected.getCodigo());
                tboxProduto.setDisable(true);
                btnProcurarProduto.setDisable(true);
                lbDescProduto.setText("Referência: " + ofSelected.getCodigo());
                disabledLimparGrade.set(false);
                
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpImpressaoEtiquetaComposicaoController.class
                        .getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        } else if (event.getCode() == KeyCode.DELETE) {
            tboxNumeroOf.clear();
            
            tboxProduto.clear();
            tboxProduto.setDisable(false);
            btnProcurarProduto.setDisable(false);
            
            tblGradeImpressao.getItems().clear();
            disabledLimparGrade.set(true);
            disabledImprimir.set(true);
        }
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxProduto.setText(ctrolFilterProduto.returnValue);
                ofSelected = DAOFactory.getOf1001DAO().getOfByCodigoWithoutDb(ctrolFilterProduto.returnValue.replace("'", ""));
                tblGradeImpressao.setItems(ofSelected.getGrade());
                makeColumnsTableCores();
                
                tboxProduto.setText(ofSelected.getCodigo());
                tboxProduto.setDisable(true);
                btnProcurarProduto.setDisable(true);
                
            }
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ScenePcpImpressaoEtiquetaComposicaoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void tboxProdutoOnKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        } else if (event.getCode() == KeyCode.DELETE) {
            ((TextField) event.getSource()).clear();
            tboxProduto.setDisable(false);
            btnProcurarProduto.setDisable(false);
            
            tblGradeImpressao.getItems().clear();
        }
    }
    
    @FXML
    private void btnCriarTagComposicaoOnAction(ActionEvent event) throws IOException {
        tagsOf.clear();
        zplCodeTag = "";
        zplPreview = "";
        
        ofSelected.getGrade().forEach(gradeOf -> {
            try {
                ObservableList<Composicao00X> composicaoProduto = DAOFactory.getPropertiesDAO().getComposicaoProduto(ofSelected.getNumero(), gradeOf.getCor(), "103");
                gradeOf.getHeadersTamanhos().forEach((pos, tam) -> {
                    for (int i = 0; i < gradeOf.getTamanhos().get(tam); i++) {
                        tagsOf.add(new TagComposicao00X(ofSelected.getCodigo(), ofSelected.getNumero(), gradeOf.getCor(), tam, composicaoProduto));
                    }
                });
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpImpressaoEtiquetaComposicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        criarPreviewComposicao();
        
        paginadorTags = 1;
        tagsOf.forEach(tagComposicao -> {
            if (paginadorTags == 1) {
                zplCodeTag += ""
                        + "^XA\n"
                        + "^MMT\n"
                        + "^PW791\n"
                        + "^LL0679\n"
                        + "^LS0\n";
                zplCodeTag += "" +
                        "^FO704,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO544,544^GFA,02560,02560,00020,:Z64:\n" +
                        "eJzt1MFu00AQBuDZLvL2kMYcg3DrPgKoCFxhxa9SngBLvQQ1ahb10BuXXpF4CF5goxx67JUDUhdxyI0acSBIls0/s3GaVBUPgLpRrOTzer27M7NED22jKcvXwYZpsWSzn+Nrjm+xEOA+Ff/gv0fbq7FGtLyxNLd8TPmVaba0RXPcIZjvzPIEgpVrFlGPbUvMTXwK69Ejtihjm7ZVYfkNER0R9Xn6uzdte2JpfxAs4RGSiU2/Lw3vHPTZTsmsWWbYxqRhz79QNLi0VML8st+rBUX5zFOpsarkLcVLO50uyCsyLpnbvWNLw0Mbzd2YnLKp3Z25/MBSfmjNzKUzp6YN7U796LWl7ImNlNW/LM091lZ62ECMWqIJ1mbfBbt4idgURKdshcN6k1QsdmrBxh9KYhiRqXQwfKmnxPp1/PGuJcOiJ+Pxs3118QJLzeK6z3bii2AR0TOzMLz3nucSBSuLU4ywO3VlsBxP+SZVsJnLDlY2q8wUE7u0OfbFqIv9HqlLp/7AbmhP7PMR7BMRkiKZyJ5q+u16pBvknO9ipG2CsJsFL49N+iEVtinm7Ku7+CqxIadHgXdUzXFnNRvHo60xZ87PgW6l+dRJDin8XphrpsbzvsCIbasQO+pycr3dZ5Nljq+3qquFtea7G7ct1Fu5YaEus3tss36VXB/TfU1W8C/bokmlVoadwbwMtTVelJGxZmVnbWvYYtweiDlz5dIrnuEejAswU05fSwQz/Ddw/NJOIQZiI1iMlQ6QeQjpmO2nM6QrKUqOVc2jfINxgvdqUg3x45lm4yd2KlJ4xw+uWzEEHWPqK2t4LvEbNiRHjJmfu/gctieGIwelpM9cygmfdIZ6M8oPeb0jMWpUy3tQjdmqYKk54zNpVMEOfTBzzRVGuUfwXrtgulVsKVthg6kamdjnmoXRsl8wY2HDYFoSSvrh3AhmJKF4PImHWCpmaFzdWl6rhYeV2a2deBqXSBif83qDHWMPYO/d8Nbm2CsYTqn+yhqpN5Rs/KEzjgLHBPv8NWRliBFHrMV6OjOc2BOLM4vvBpNaRczHa5Zy9iFw68+K7Th149Kms6dSg07P2jbt8lQKuMHJ29bxHYtt4Y3dTHjZ8DtFsHVvuTy0/6v9Be9JwdU=:238A\n" +
                        "^FT733,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT717,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT728,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT713,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT737,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT737,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT721,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT721,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT754,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT754,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 687;
                pos2 = 667;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += "" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
            } else if (paginadorTags == 2) {
                zplCodeTag += "" +
                        "^FO440,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO256,544^GFA,03072,03072,00024,:Z64:\n" +
                        "eJzt1T9v00AUAPB3PZTrkMaMQThxPwKoCFLVir9K+ARY6hLUKDnUoRtLVyQ+BF/gogwduzIg9RBDNmrEgJEsm/fuj2MnrRhg7KuUOL9c7+x3714AHuJ/BJP02t9xbjzcHa/oNXafkty80diMLnLnk/167in4Lzeu3BRMt5yTRxWG8otZ196lvznr6ZZ3oEtve8bVQkfOu/DIfD0iX1ZZIv2qHZgA9OgxB7dVdWb9sG89pNnChYy+NRzvpd8jn4PY8pEgnwF3/vwzdPpXElJ03Rj/KodOvNKQcsxA+AaChs+XOWgGQoVrOTy1Pj6WnbWagWIykoOVio+sx8dSrFS0UmxZwmCppyfWR09kh0n+U8JaYx5S7bxvHCqABeZBvt345Uvc7wRgTp4ol58wMh4olpPTn/UAHUBk3Do47zLjvSL4cJeH46Rr5vfz9NjlC0zNKCh65Gc62XgH4JnIBe2j9vfZsZ4mc5xtsFTpxmOcQZcRQ1+p0VHLV5lYSgivZOzyKdjlYRfYlWK/0W9hWPunCfpHACzAcFHvC4dfqgu8xHrXzX3nMsTSEjmlgrwej2/7ENCHolk/zPiYLhNcNytPm17QJe1vVbjnonPS55UJHam6bhl+zsUNcakpn86BfC8xPmmei2bc54vGuWsGHd36nDZC+y/bYftDuuO2n4zu8d3+w8zr4911G2Ge9G++B4uMtRwzivctoCrs4nhXQoraz6tKeA9wWN+4EtcqugbnQ3RqGiOm+I2pDOsh+pCuuGK4n7VP0QOFz4mVj+Uy8/5DCeCZaSa0/4Vz9hWdDl23AFYC887J6b8PMmC47nfnwjgWFq7Br6Xw9xm8JsdCDPDpLlRw4XxoHNsqHnl+riLhPPS+oMOgxz4/U+NQsoryls28Z9YjcU69d5o5P9bWxQ11A4i1LQo4UdZ5xcgj74m0zgo8CT3qN87BjbcupPOxdW4K2Yy368bWhSlkmr/eX+ORcQGzrO1xwXKNno7afqZhlmKB6tjnx/op5g39nRq3fY15RseO3Gt5afoDtpvgfdNpV2mPcb++eKf0klElVPjcTRd02BYSezTLm276DNbVbMsjusSC2J7H+IFityoqm/7U9A3FV1UVWbfnxTSgEn+RqiK4wwOZaHdeWmE2bpfxPD7EQ/xz/AFsKcHV:E6E3\n" +
                        "^FT469,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT453,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT463,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT448,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT473,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT473,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT457,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT457,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT490,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT490,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 420;
                pos2 = 400;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += "\n" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
            } else {
                zplCodeTag += "" +
                        "^FO176,96^GFA,\n" +
                        (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                        "^FO0,544^GFA,03072,03072,00024,:Z64:\n" +
                        "eJzt1D9vGzcUAPDHvEDsoBwzNohy9xW89QwoUj+KgwxdD/AQAS10LAykYzu3QD9DxixFaGQovDRzNgodvAQ1O1kFBF3feyQVykoQBMhoGj6dfnd6/PPIB3DbvkRzfBkfsBKvDt/3fKnTl2YjH9/S/4Jvfkh+dHcXewr54Xv3OUTYc8VuBmo+dxY9ZHd5cNFf3nCEkXjL7ufBJB/BHXk8ER/Wjcu9IhwBaJ5mdT0Mf0S/P45ecbRq7sy/hXe0Wloc8IZP0JLPQCX/+h3g+KmDlrwr3n+4AayfBGiVhVB9Y3Xh85MNhbLoq1NnjqM3E4enfgYn4IyrVr5+EL0mX3mz8uAHqHyYPoo+GTsEp/5zcBpovm2XfCwOvYW5BeOmYecXv1G+G3JaH9f4tD6VEdceNuz8F12TW8C1im6Tj0BcL/VfH/KqaZ5L/BxHw8WvtDQTvdXsj0NTOC0fbuip8V0eP0Zvm1cczfv2vf9OEbreWPKVnzzY8ydr9A6qp65O64n24hdK7d80L/Jn1uz87QvyNxZ6T/nd5UXZ7/0I1ECZCGXelatobkiLgGv23fv0cRc0f1mW+wfEG0mprZ6t++PSl3zH+R22aV58TsZqkBaM7/O+Bfq+wSvxjtcze0+uGvGj8lyU7WM+L89d0eTohkPvdg/3m4RoDzjWk8lH/LD+gOXLV4deNJmp/YQrmK9jsOy0omtOdr+NndOo0OHOz4fhLLu2aMfiJ3jpzaVNbujnldx5dWV5Z0SvyA3fUSnbUj6yT8m1p3nSzqcaOcu+8GjVWooJ53+ZPZDzoRuRUPBtciVOvx7Rb6jf6+QoThuL+lCXDvM49Qt22oiaZvfa69fJzRE7lVU68urcm7PkVXaqD2hDk9dnKk7TGdgXs+yL6ObsnBymi+whOl79SL1D3cVNAY98dDVYdhOSNy46zZOGypNIbtP70dFlhzhO2cjyfuy3jo6ykTn+Lr/iRhztbLHv9RI2HXk72ffHAWYtbdCuRlv6Ma3bS3Lf7PspDYacKvJPUHov9YHKjf5zzzmrlOPvHP6TnbclG++E3qpt6RwD5o5r9KZ0qTO0r2Y33PDgaG1uxhEfebj2Zij9ntSNE7Uahp/jvOJ5iQUI0Pdb/QHXrgkY122vSeLsoatDum237bPb/8Z052s=:1FA9\n" +
                        "^FT206,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                        "^FT190,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                        "^FT200,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                        "^FT184,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + tagComposicao.getTam() + "^FS\n" +
                        "^FT210,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                        "^FT210,493^A0R,16,16^FH\\^FD" + tagComposicao.getNumero() + "^FS\n" +
                        "^FT194,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                        "^FT194,504^A0R,16,16^FH\\^FD" + tagComposicao.getCor() + "^FS\n" +
                        "^FT227,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                        "^FT227,505^A0R,16,16^FH\\^FD" + tagComposicao.getCodigo() + "^FS\n";
                pos1 = 160;
                pos2 = 140;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += "\n" +
                            "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicao.getAplicacao() + "^FS\n" +
                            "^FT" + pos2 + ",96^A0R,20,19^FH\\^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^PQ1,0,1,Y\n"
                        + "^XZ\n";
                paginadorTags = 0;
            }
            paginadorTags++;
        });
        if (tagsOf.size() % 3 != 0) {
            zplCodeTag += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
//        System.out.println(zplPreview);
//        System.out.println("------------------------------------------------------------");
//        System.out.println(zplCodeTag);
        imgPreviewTag.setImage(getPreviewEtiqueta(zplPreview));
        disabledImprimir.set(false);
    }
    
    @Deprecated
    private void btnCriarTagComposicaoOnActionBKP(ActionEvent event) throws IOException {
        tagsOf.clear();
        zplCodeTag = "";
        zplPreview = "";
        zplCodeTag += StaticsLabel.imagemLogoDeliz;
        zplPreview += StaticsLabel.imagemLogoDeliz;
        zplCodeTag += Boolean.logicalOr(
                ofSelected.getCodigo().equals("F60266"),
                Boolean.logicalOr(
                        ofSelected.getCodigo().equals("F60261"),
                        ofSelected.getCodigo().equals("F60299"))) ? StaticsLabel.imagemInstrucaoF60266 : StaticsLabel.imagemInstrucaoGeral;
        zplPreview += Boolean.logicalOr(
                ofSelected.getCodigo().equals("F60266"),
                Boolean.logicalOr(
                        ofSelected.getCodigo().equals("F60261"),
                        ofSelected.getCodigo().equals("F60299"))) ? StaticsLabel.imagemInstrucaoF60266 : StaticsLabel.imagemInstrucaoGeral;
        
        ofSelected.getGrade().forEach(gradeOf -> {
            try {
                ObservableList<Composicao00X> composicaoProduto = DAOFactory.getPropertiesDAO().getComposicaoProduto(ofSelected.getNumero(), gradeOf.getCor(), "103");
                gradeOf.getHeadersTamanhos().forEach((pos, tam) -> {
                    for (int i = 0; i < gradeOf.getTamanhos().get(tam); i++) {
                        tagsOf.add(new TagComposicao00X(ofSelected.getCodigo(), ofSelected.getNumero(), gradeOf.getCor(), tam, composicaoProduto));
                    }
                });
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpImpressaoEtiquetaComposicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        
        criarPreviewComposicao();
        
        paginadorTags = 1;
        tagsOf.forEach(tagComposicao -> {
            if (paginadorTags == 1) {
                zplCodeTag += ""
                        + "^XA\n"
                        + "^MMT\n"
                        + "^PW791\n"
                        + "^LL0679\n"
                        + "^LS0\n";
                zplCodeTag += ""
                        + "^FT185,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO40,520^IMR:COMP.GRF^FS\n"
                        + "^FT213,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT195,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT230,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT230,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT202,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT202,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT176,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT176,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 160;
                pos2 = 140;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT130,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
            } else if (paginadorTags == 2) {
                zplCodeTag += ""
                        + "^FT455,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO300,520^IMR:COMP.GRF^FS\n"
                        + "^FT470,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT452,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT487,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT487,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT459,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT459,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT433,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT433,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 420;
                pos2 = 400;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT390,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                
            } else {
                zplCodeTag += ""
                        + "^FT700,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO540,520^IMR:COMP.GRF^FS\n"
                        + "^FT716,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT698,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT733,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT733,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT705,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT705,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT679,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT679,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 667;
                pos2 = 647;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT630,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                zplCodeTag += ""
                        + "^PQ1,0,1,Y\n"
                        + "^XZ\n";
                paginadorTags = 0;
            }
            paginadorTags++;
        });
        if (tagsOf.size() % 3 != 0) {
            zplCodeTag += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
//        System.out.println(zplPreview);
//        System.out.println("------------------------------------------------------------");
//        System.out.println(zplCodeTag);
        imgPreviewTag.setImage(getPreviewEtiqueta(zplPreview));
        disabledImprimir.set(false);
    }
    
    @FXML
    private void btnCriarTagProdutoOnAction(ActionEvent event) throws IOException {
        tagsOf.clear();
        zplCodeTag = "";
        zplPreview = "";
        zplCodeTag += StaticsLabel.imagemLogoDeliz;
        zplPreview += StaticsLabel.imagemLogoDeliz;
        zplCodeTag += ofSelected.getCodigo().equals("F60266") ? StaticsLabel.imagemInstrucaoF60266 : StaticsLabel.imagemInstrucaoGeral;
        zplPreview += ofSelected.getCodigo().equals("F60266") ? StaticsLabel.imagemInstrucaoF60266 : StaticsLabel.imagemInstrucaoGeral;
        
        ofSelected.getGrade().forEach(gradeOf -> {
            try {
                ObservableList<Composicao00X> composicaoProduto = DAOFactory.getPropertiesDAO().getComposicaoProduto(ofSelected.getNumero(), gradeOf.getCor(), "103");
                gradeOf.getHeadersTamanhos().forEach((pos, tam) -> {
                    for (int i = 0; i < gradeOf.getTamanhos().get(tam); i++) {
                        tagsOf.add(new TagComposicao00X(ofSelected.getCodigo(), ofSelected.getNumero(), gradeOf.getCor(), tam, composicaoProduto));
                    }
                });
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpImpressaoEtiquetaComposicaoController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        
        criarPreviewComposicao();
        
        paginadorTags = 1;
        tagsOf.forEach(tagComposicao -> {
            if (paginadorTags == 1) {
                zplCodeTag += ""
                        + "^XA\n"
                        + "^MMT\n"
                        + "^PW830\n"
                        + "^LL0719\n"
                        + "^LS0\n";
                zplCodeTag += ""
                        + "^FT185,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO40,520^IMR:COMP.GRF^FS\n"
                        + "^FT213,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT195,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT230,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT230,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT202,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT202,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT176,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT176,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 160;
                pos2 = 140;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT130,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
            } else if (paginadorTags == 2) {
                zplCodeTag += ""
                        + "^FT455,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO300,520^IMR:COMP.GRF^FS\n"
                        + "^FT470,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT452,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT487,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT487,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT459,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT459,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT433,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT433,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 420;
                pos2 = 400;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT390,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                
            } else {
                zplCodeTag += ""
                        + "^FT700,200^XGR:LOGO.GRF,1,1^FS\n"
                        + "^FO540,520^IMR:COMP.GRF^FS\n"
                        + "^FT716,203^A0R,17,16^FB142,1,0,C^FH^FD03.306.680/0001-64^FS\n"
                        + "^FT698,210^A0R,14,14^FB143,1,0,C^FH^FDFABRICADO NO BRASIL^FS\n"
                        + "^FT733,417^A0R,20,19^FH^FDREF:^FS\n"
                        + "^FT733,458^A0R,20,19^FH^FD" + tagComposicao.getCodigo() + "^FS\n"
                        + "^FT705,415^A0R,20,19^FH^FDCOR:^FS\n"
                        + "^FT705,458^A0R,20,19^FH^FD" + tagComposicao.getCor() + "^FS\n"
                        + "^FT679,418^A0R,20,19^FH^FDOF:^FS\n"
                        + "^FT679,458^A0R,20,19^FH^FD" + tagComposicao.getNumero() + "^FS\n";
                pos1 = 667;
                pos2 = 647;
                tagComposicao.getComposicao().forEach(composicao -> {
                    zplCodeTag += ""
                            + "^FT" + pos1 + ",100^A0R,20,19^FB56,1,0,C^FH^FD" + composicao.getAplicacao() + "^FS\n"
                            + "^FT" + pos2 + ",100^A0R,20,19^FH^FD" + composicao.getComposicao() + "^FS\n";
                    pos1 -= 45;
                    pos2 -= 45;
                });
                zplCodeTag += ""
                        + "^FT630,640^A0N,39,45^FH28,1,0,C^FH^FD" + tagComposicao.getTam() + "^FS\n";
                zplCodeTag += ""
                        + "^PQ1,0,1,Y\n"
                        + "^XZ\n";
                paginadorTags = 0;
            }
            paginadorTags++;
        });
        if (tagsOf.size() % 3 != 0) {
            zplCodeTag += ""
                    + "^PQ1,0,1,Y\n"
                    + "^XZ\n";
        }
//        System.out.println(zplPreview);
//        System.out.println("------------------------------------------------------------");
//        System.out.println(zplCodeTag);
        imgPreviewTag.setImage(getPreviewEtiqueta(zplPreview));
        disabledImprimir.set(false);
    }
    
    @FXML
    private void btnImprimirOnAction(ActionEvent event) {
        if (chboxGerarArquivo.isSelected()) {
            String pathFile = FileChoice.show(box -> {
                box.addExtension(new FileChooser.ExtensionFilter("Arquivo de Texto", "*.txt"));
            }).stringSavePath();
            
            FileWriter myWriter = null;
            try {
                myWriter = new FileWriter(pathFile);
                myWriter.write(zplCodeTag);
                myWriter.close();
                MessageBox.create(message -> {
                    message.message("Arquivo TXT de etiqueta criado com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
            
            return;
        }
        
        PrinterJob jobPrinterSelect = PrinterJob.createPrinterJob();
        if (jobPrinterSelect == null) {
            MessageBox.create(message -> {
                message.message("Impossível criar o job de impressão com as impressoras instaladas neste usuário, tente reiniciar o computador para atualização da lista de impressoras.");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
            return;
        }
        if (!jobPrinterSelect.showPrintDialog(btnImprimir.getScene().getWindow())) {
            return;
        }
        
        String printerName = jobPrinterSelect.getPrinter().getName();
        PrintService ps = null;
        try {
            
            byte[] by = zplCodeTag.getBytes();
            DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
            PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);
            
            if (pss.length == 0) {
                throw new RuntimeException("No printer services available.");
            }
            
            for (PrintService psRead : pss) {
                if (psRead.getName().contains(printerName)) {
                    ps = psRead;
                    break;
                }
            }
            
            if (ps == null) {
                throw new RuntimeException("No printer mapped in TS.");
            }
            
            DocPrintJob job = ps.createPrintJob();
            Doc doc = new SimpleDoc(by, flavor, null);
            
            job.print(doc, null);
            
            tboxNumeroOf.clear();
            
            tblGradeImpressao.getItems().clear();
            disabledLimparGrade.set(true);
            disabledImprimir.set(true);
            
        } catch (PrintException e) {
            GUIUtils.showMessage("Cannot print label on this printer : " + ps.getName(), Alert.AlertType.ERROR);
        }
    }
    
    @FXML
    private void btnLimparGradeOnAction(ActionEvent event) {
        ofSelected.getGrade().forEach(grade -> {
            grade.getTamanhos().forEach((key, value) -> {
                grade.getTamanhos().replace(key, 0);
            });
        });
        tblGradeImpressao.refresh();
    }
    
    private class EditingCellClnTamanho extends TableCell<OfIten001, Integer> {
        
        private TextField textField;
        
        public EditingCellClnTamanho() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void commitEdit(Integer newValue) {
            super.commitEdit(newValue);
        }
        
        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
    
    private static class StaticsLabel {
        
        public static String imgCompressedDeliz = "01024,01024,00008,:Z64:eJyt0U1OhTAQAOAhNc5OLmDkIoZeyxX0xYVLj+BNfN15DY7QJYum4/wReTEvmBcnIR/QmWFaAEmiwqEJOJ7dl+tmM6jlx4XpCnSbheWXm7CyUlzdie25aWQHvuJ2z2tN1riH5vJsUhsoz9ILeFz5BsxEOtNAki4Jugxx8ceq9kT2mih7mSbQ11nyu4Zv2n7FkxSEgkn64YJpckcdGZOMiem3/R+9qON+0R133wts3c3TVZsPGuq8rM0/ou3nEW1/clBmhl0c/fdbQ+rbdOy1uHPvt37pcnrkTeq5rG51m8s/Xz1n86OY76v5Ws0TmWm2c04xm0MxcTVDMzsyYf40YzN7F93gwubkPrkPN57tP8Y3xyLbQg==:5AEB";
        public static String imgCompressedUpWave = "938,938,7, ,::N0G3IFH0N0G7IFH0::N0G7GCJ0N0G7G8J0::N0G3GCJ0N0G7IFH0:::,M0KFH0::::N0G7GCG1GFH0N0G7G8G0GFH0:::N0G7GCG1GFH0N0G7IFH0N0G3HFGEG0GCN0G3HFGEG1GCO0HFG8G3GCO0G1GCG0G7GCR0GFG0R0G3GCP0G3HFGCO0G1IFGCP0G7HFGCP0G3HFGCQ0G7GFGCQ0G1GFGCR0G7GCR0G1GC,:::P0G1GFH0P0G7GFH0O0G7HFH0N0G3IFH0N0G7HFGCH0N0G7GFGEI0N0G7GEJ0N0G7GFGCI0N0G7HFGEH0O0IFH0P0HFH0:O0IFH0N0G7HFGEH0N0G7GFGCI0N0G7GEJ0N0G7GFGEI0N0G7HFGCH0N0G3IFH0O0G7HFH0P0G7GFH0P0G1GFH0N0G1GFG0G1H0N0G3GFG9GCH0N0G7GFGDGEH0N0G7GFGDGFH0N0G7GBGDGFH0N0G7G1GCG7H0N0G7G1GCG7G8G0:N0G7G0GEG7G8G0N0G7G8GEG7G8G0N0G3GEHFH0N0G7IFH0:N0G7HFGEH0N0G7HFG8H0,Q0GFH0P0G3GFH0O0G1HFH0O0G7HFH0N0G7HFGCH0N0G7HFI0N0G7GFJ0N0G7GFG8I0N0G7HFI0N0G7IFH0O0G7HFH0O0G1HFH0P0G3GFH0P0G4GFH0O0HFG8H0N0G1HFGEH0N0G3HFGEH0N0G7IFH0N0G7HDGFH0N0G7G9GCGFH0N0G7G1GCG7G8G0::N0G7G9GCGFG8G0N0G7GDGCGFH0N0G7GDHFH0N0G3GDGFGEH0N0G1GDGFGCH0O0GDGFG8H0,:::::::::::::::::::";
        
        public static String imagemInstrucaoGeral = ""
                + "~DGR:COMP.GRF,2158,26," +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000FF0000000000000000000000000000000000000000000000\n" +
                "0000BD8000000000000000000000000000000000000000000000\n" +
                "00018080000000000001800000300000000000000000003F0000\n" +
                "00010080000000000001800000700000000000000000807F8040\n" +
                "00010080000000400000C0000060000000000000000181FFE040\n" +
                "00030080000000400000C00000E00000000000000001C3FFF0E0\n" +
                "00040080000000E00000600000C1FFFFFF801FFFC000E78079C0\n" +
                "00080040000000E000003FFFFF81FFFFFF801FFFC000FE001F80\n" +
                "00080040000001E000003FFFFF01800C01801FFFC0007C000F80\n" +
                "181000406000033000003C3F87018018018E000060E038000F00\n" +
                "181000406000031000003CFFCF018030018F000061E07C000F80\n" +
                "1810004060000318000036FFCD018030018F000061C07C000F80\n" +
                "09D000466000060C00003380390180E60181C0003700C70039C0\n" +
                "09D0004F6000040C00003380390180E60180E0003E00C70038C0\n" +
                "0FF0004F43000C0C00303380390181E6018070003C00C38070C0\n" +
                "0F720059C1C00C0600F036C06D0183E601803800780183C0E0C0\n" +
                "0E320070C1E0180601E036E0CD0183E601801C00F00181C0E060\n" +
                "0E320070C0F0180201E03460CD0186E601801E00F00181E1E060\n" +
                "0C120070C078180303C03C61CD01866601800F01F80180E1C060\n" +
                "04120040C00F30011E003C1B07018CE6018003C7180180738060\n" +
                "0412DB40C007A001F8003C1E070198E6018001EE0801803F0060\n" +
                "0616DB40C003E001F800381E070198E6018000FE0801803F0060\n" +
                "060ADA408000F003C000380E0701B0E60180007C0C01801E0060\n" +
                "0602DA4080007807C000381E0701E0E6018008FC1C01801E0060\n" +
                "0602DA4180007C0FC000381E0701E0E601801FFFFC01801E0060\n" +
                "0202DA41800087F860003C31870180E6018073C7C601807F0060\n" +
                "0202DA41800183F020003C618D0180E60180E783C60180738060\n" +
                "0202DA41800181F030003460CD0180E60180CF01E60180E38060\n" +
                "0302DA41000101F0300034E0CD0180E60180CE00E60180E1C0C0\n" +
                "0302DBC100030F3E10003780790180E60181B8007A00C1C0E0C0\n" +
                "0302DB8300030F1E18003780790180E60181B8003F00C1C0E0C0\n" +
                "0302DA8300023C0F18003380390180E60181F0001F00C38071C0\n" +
                "0103DC030007E001EC0037E0F90180E60183C000078067003B80\n" +
                "01017403000FC000FC0036F3ED0180000187800003C07E003F80\n" +
                "01800002000F00003E003C3F87018000018F000001E03C001F00\n" +
                "01800002003C00000F003C1F07018000019E000001F03C000E00\n" +
                "018000060078000007C03FFFFF018000019E000001B03E001F00\n" +
                "0180000601F0000003E07000038180000183FFFFFF807F007F80\n" +
                "00FFFFFE03BFFFFFFFB0E00000C1FFFFFF83FFFFFF80E3FFF1C0\n" +
                "00FFFFFE033FFFFFFFB0C00000C1FFFFFF0000000000E1FFE1C0\n" +
                "00FFFFFC000000000001C00000E00000000000000001C0FFC1E0\n" +
                "00000000000000000001800000600000000000000001C03F00E0\n" +
                "0000000000000000000100000020000000000000000080000040\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000";
        
        public static String imagemInstrucaoF60266 = ""
                + "~DGR:COMP.GRF,2158,26, "
                + "0000000008000000000001000000070000000000000000000000\n"
                + "080000000E000000000003800000078000000000000000000000\n"
                + "280000000F000002000003E000000F8000000000000000000000\n"
                + "380000001F000007000001FFFFFFFE0000FFFFE0000003F80000\n"
                + "7DE01E01FF000007000000FFFFFFF80000FFFFF000403C078040\n"
                + "7F383F873F00600F0030007CFFF0700000FFFFF800607803C1C0\n"
                + "3E1FE1FC3E00781D80E0007DF87EF00008FFFFFD0038E000E380\n"
                + "0F8780703C001C18C3C0006F801FF00008001FFF001CC0006700\n"
                + "07C00000F8001E18C38000630003B0000C0000FF000F80003E00\n"
                + "07E00001F8000730EF0000630003B0000E00001E000380007800\n"
                + "07F00007F80001E07C000065C01FB0000300000C0007E000FC00\n"
                + "07FC000FF80000E07000006CF01EF0000380003C0006F001CC00\n"
                + "073E003FB00000F8F000006CF83C700000E0007E000638078C00\n"
                + "071F003E300000DF9800007C7870700000E000FE000E1C060E00\n"
                + "070FC07C3000018F1C00007C3CE07000007FFFFE000C03380600\n"
                + "0387C1F03000019F9C0000780EE0700001FFFFFE000C03F00600\n"
                + "0381F3E020000378E600007807C0700003FFFFFE000C01E00600\n"
                + "0381FFC0200003E07E00007807C0700007C79C07000C03F80600\n"
                + "03807F80200007E03F0000783EE070000F03FC07000C03380600\n"
                + "01C03F80600007801F00007C7CE070001800E00700061C070C00\n"
                + "01C07F8060001E000380006CF87070003801F003000678038C00\n"
                + "01C1FFC060001C0001E0006CF83C70007003F8030003F001D800\n"
                + "01C1F3E06000780000E00065F01EF00070039C030003C000F800\n"
                + "00E7C0F86001F00000F80067C01FB000600F1E03800380003800\n"
                + "00FFC0FCE003F00000FE0063800FB000E03C0383800EC0006E00\n"
                + "00FFFFFFC007E00000670067000FB000FFFFFFFF803CF001E700\n"
                + "00FFFFFFC00E60000037806FC01FF000FFFFFFFFC0303C078380\n"
                + "00FFFFFFC018E0000038C07CF8FCF001FFFFFFFFC0601FFF00C0\n"
                + "01F00007E010FFFFFFF8407C7FE070000300001C004007FC0040\n"
                + "03E00001E001FFFFFFF8007FFFFFF8000700000E000000000000\n"
                + "03C0000060000000000001FBFFFFFE000E00000E000000000000\n"
                + "0180000020000000000001E000001F000C000003000000000000\n"
                + "0000000000000000000003800000078008000003000000000000\n"
                + "0000000000000000000000000000070000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000007C00000\n"
                + "000000000000000000000000000000000000000000000FF00000\n"
                + "00000000000000000000000000000000000000000001F83E0000\n"
                + "0000000000000000000003FFFFFFFC00000000000003E00F8000\n"
                + "0000000000000000000003FFFFFFFC000000000000078007C000\n"
                + "0000000000000000000003FFFFFFFC0000000000000E0000F000\n"
                + "0000000000000000000003807000040000000000000800007000\n"
                + "000000000000000000000381E0000C0000000000001800003000\n"
                + "000000000000000000000383C000040000000000003000001000\n"
                + "0000000000000000000003878000040000000000003000000800\n"
                + "00000000000000000000038F0000040000000000007000000800\n"
                + "00000000000000000000039E0000040000000000007387830C00\n"
                + "0000000000000000000003BC0000040000000000006186C70C00\n"
                + "0000000000000000000003B8000004000000000000E184C60C00\n"
                + "0000000000000000000003F0000004000000000000E1CCC60C00\n"
                + "0000000000000000000003E0000004000000000000E0CC6C0C00\n"
                + "0000000000000000000003C0000004000000000000E0C86C0C00\n"
                + "000000000000000000000380000004000000000000E0687C0C00\n"
                + "000000000000000000000381FFFF040000000000007060380C00\n"
                + "000000000000000000000381FFFF040000000000007030180800\n"
                + "000000000000000000000381FFFF040000000000003030100800\n"
                + "0000000000000000000003800002040000000000001000001000\n"
                + "0000000000000000000003800000040000000000001000003000\n"
                + "0000000000000000000003800000040000000000000800003000\n"
                + "0000000000000000000003800000040000000000000E0001E000\n"
                + "0000000000000000000003800000040000000000000F8003C000\n"
                + "00000000000000000000038000000400000000000007C00F8000\n"
                + "00000000000000000000038000000400000000000001F00F0000\n"
                + "000000000000000000000380000004000000000000003FF00000\n"
                + "000000000000000000000380000004000000000000000FC00000\n"
                + "0000000000000000000003800000040000000000000000000000\n"
                + "0000000000000000000003800000040000000000000FFFFFF000\n"
                + "0000000000000000000003FFFFFFFC0000000000000FFFFFF000\n"
                + "0000000000000000000003FFFFFFFC0000000000000000000000\n"
                + "0000000000000000000000000000000000000000000FFFFFF000\n"
                + "0000000000000000000000000000000000000000000FFFFFF000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000";
        
        public static String imagemLogoDeliz = ""
                + "~DGR:LOGO,792,8, "
                + "3FFFFFFFFFFFF800                                                    \n"
                + "7FFFFFFFFFFFF800                                                                     \n"
                + "3FFFFFFFFFFFF800                                                                     \n"
                + "389FFFFFFFF53800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3C00000000007000                                                                     \n"
                + "1C00000000007000                                                                     \n"
                + "1C00000000007000                                                                     \n"
                + "1E0000000000F000                                                                     \n"
                + "0E0000000000E000                                                                     \n"
                + "0F0000000001E000                                                                     \n"
                + "070000000001C000                                                                     \n"
                + "078000000003C000                                                                     \n"
                + "03C0000000078000                                                                     \n"
                + "03C00000000F0000                                                                     \n"
                + "01F00000001F0000                                                                     \n"
                + "00F80000003E0000                                                                     \n"
                + "007C0000007C0000                                                                     \n"
                + "003F000001F80000                                                                     \n"
                + "000FC00007E00000                                                                     \n"
                + "0007FC007FC00000                                                                     \n"
                + "0001FFFFFF000000                                                                     \n"
                + "00003FFFF8000000                                                                     \n"
                + "000007FF80000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "001FFE0000000000                                                                     \n"
                + "007FFF8000000000                                                                     \n"
                + "01FFFFE000000000                                                                     \n"
                + "03F8E7F000000000                                                                     \n"
                + "07C0E0F800000000                                                                     \n"
                + "0F00E07C00000000                                                                     \n"
                + "1E00E03C00000000                                                                     \n"
                + "1C00E01E00000000                                                                     \n"
                + "3C00E00E00000000                                                                     \n"
                + "3800E00F00000000                                                                     \n"
                + "3800E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "7000E00700000000                                                                     \n"
                + "7000E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "3800E00700000000                                                                     \n"
                + "3800E00F00000000                                                                     \n"
                + "3800E00E00000000                                                                     \n"
                + "3C00E01E00000000                                                                     \n"
                + "1E00E03C00000000                                                                     \n"
                + "1F00E07C00000000                                                                     \n"
                + "0F80E1F800000000                                                                     \n"
                + "07C0E7F000000000                                                                     \n"
                + "03C0FFC000000000                                                                     \n"
                + "0180FF8000000000                                                                     \n"
                + "0000FC0000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "3FFFFFFFFFFFFF00                                                                     \n"
                + "7FFFFFFFFFFFFF80                                                                     \n"
                + "7FFFFFFFFFFFFF80                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "7FFFFFFE3E000000                                                                     \n"
                + "3FFFFFFE3E000000                                                                     \n"
                + "7FFFFFFE3E000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "2000000000000000                                                                     \n"
                + "7000000000000000                                                                     \n"
                + "7800000000000000                                                                     \n"
                + "7E00000E00000000                                                                     \n"
                + "7F00000E00000000                                                                     \n"
                + "7F80000E00000000                                                                     \n"
                + "7FE0000E00000000                                                                     \n"
                + "79F0000E00000000                                                                     \n"
                + "70FC000E00000000                                                                     \n"
                + "707E000E00000000                                                                     \n"
                + "781F000E00000000                                                                     \n"
                + "700FC00E00000000                                                                     \n"
                + "7803E00E00000000                                                                     \n"
                + "7801F80E00000000                                                                     \n"
                + "7800FC0E00000000                                                                     \n"
                + "78003E0E00000000                                                                     \n"
                + "78001F8E00000000                                                                     \n"
                + "78000FCE00000000                                                                     \n"
                + "780003FE00000000                                                                     \n"
                + "780001FE00000000                                                                     \n"
                + "7800007E00000000                                                                     \n"
                + "7800003E00000000                                                                     \n"
                + "7800001E00000000                                                                     \n"
                + "7800000600000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000 \n";
    }
    
}
