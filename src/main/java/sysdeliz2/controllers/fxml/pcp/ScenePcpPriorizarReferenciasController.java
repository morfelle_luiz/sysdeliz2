/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.cadastros.SceneCadastroColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterFamiliaController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Familia;
import sysdeliz2.models.SdProdutoPrioridadeMrp001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePcpPriorizarReferenciasController implements Initializable {

    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final ListProperty<SdProdutoPrioridadeMrp001> listDisponiveis = new SimpleListProperty();
    private final ListProperty<SdProdutoPrioridadeMrp001> listSelecionados = new SimpleListProperty();
    private final ObservableList<SdProdutoPrioridadeMrp001> listDisponiveisAll = FXCollections.observableArrayList();
    private final ObservableList<SdProdutoPrioridadeMrp001> listSelecionadosAll = FXCollections.observableArrayList();
    private Familia familia = new Familia();

    @FXML
    private TextField tboxCodigoFamilia;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private ListView<SdProdutoPrioridadeMrp001> listReferenciasDisponiveis;
    @FXML
    private TextField tboxProcurarReferenciaDisponivel;
    @FXML
    private Button btnAddSelected;
    @FXML
    private Button btnAddAll;
    @FXML
    private Button btnDeleteSelected;
    @FXML
    private Button btnDeleteAll;
    @FXML
    private ComboBox<String> cboxPrioridade;
    @FXML
    private TableView<SdProdutoPrioridadeMrp001> tblProdutoSelecionado;
    @FXML
    private TextField tboxProcurarProdutoSelecionado;
    @FXML
    private Label lbPrioritiesCount;
    @FXML
    private Button btnSalvar;
    @FXML
    private TextField tboxCodigoColecao;
    @FXML
    private Button btnProcurarColecao;

    public void searchAvailable(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listDisponiveis.set(listDisponiveisAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdProdutoPrioridadeMrp001> subentries = FXCollections.observableArrayList();
        for (SdProdutoPrioridadeMrp001 entry : listDisponiveis) {
            boolean match = true;
            if (!entry.getCodigo().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listDisponiveis.set(subentries);

    }

    public void searchSelected(String oldVal, String newVal) {

        if (oldVal != null && (newVal.length() < oldVal.length())) {
            listSelecionados.set(listSelecionadosAll);
        }
        String value = newVal.toUpperCase();
        ObservableList<SdProdutoPrioridadeMrp001> subentries = FXCollections.observableArrayList();
        for (SdProdutoPrioridadeMrp001 entry : listSelecionados) {
            boolean match = true;
            if (!entry.getCodigo().toUpperCase().contains(value)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        listSelecionados.set(subentries);

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxCodigoFamilia);
        TextFieldUtils.inSql(tboxCodigoFamilia);
        TextFieldUtils.upperCase(tboxCodigoColecao);
        TextFieldUtils.inSql(tboxCodigoColecao);
        //btnSalvar.disableProperty().bind(disableBtnSaveRegister);

        try {
            listDisponiveis.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getAvailableCollectionActual());
            listSelecionados.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getSelectedPriorities());
            listDisponiveisAll.addAll(listDisponiveis);
            listSelecionadosAll.addAll(listSelecionados);
        } catch (SQLException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        listReferenciasDisponiveis.itemsProperty().bind(listDisponiveis);
        listReferenciasDisponiveis.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listReferenciasDisponiveis.setCellFactory(new Callback<ListView<SdProdutoPrioridadeMrp001>, ListCell<SdProdutoPrioridadeMrp001>>() {
            @Override
            public ListCell<SdProdutoPrioridadeMrp001> call(ListView<SdProdutoPrioridadeMrp001> param) {
                return new ListOperationsCell();
            }
        });
        TextFieldUtils.upperCase(tboxProcurarReferenciaDisponivel);
        //tboxProcurarReferenciaDisponivel.disableProperty().bind(listDisponiveis.emptyProperty());
        tboxProcurarReferenciaDisponivel.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchAvailable((String) oldValue, (String) newValue);
            }
        });

        tblProdutoSelecionado.itemsProperty().bind(listSelecionados);
        tblProdutoSelecionado.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        lbPrioritiesCount.textProperty().bind(new SimpleStringProperty("").concat(listSelecionados.sizeProperty()).concat(" produto(s) priorizado(s)"));
        TextFieldUtils.upperCase(tboxProcurarProdutoSelecionado);
        //tboxProcurarProdutoSelecionado.disableProperty().bind(listSelecionados.emptyProperty());
        tboxProcurarProdutoSelecionado.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                searchSelected((String) oldValue, (String) newValue);
            }
        });

        btnAddSelected.disableProperty().bind(listReferenciasDisponiveis.getSelectionModel().selectedItemProperty().isNull());
        btnAddAll.disableProperty().bind(listDisponiveis.emptyProperty());
        btnDeleteSelected.disableProperty().bind(tblProdutoSelecionado.getSelectionModel().selectedItemProperty().isNull());
        btnDeleteAll.disableProperty().bind(listSelecionados.emptyProperty());

        disableBtnSaveRegister.bind(listSelecionados.emptyProperty());
    }

    @FXML
    private void tboxCodigoFamiliaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarFamiliaOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                String filterEdited = "";
                for (String codigo : tboxCodigoFamilia.getText().split(",")) {
                    filterEdited += "'" + codigo + "',";
                }
                listDisponiveis.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getAvailableByFilter(filterEdited.substring(0, filterEdited.length() - 1), tboxCodigoColecao.getText()));
                listDisponiveisAll.clear();
                listDisponiveisAll.addAll(listDisponiveis);
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void tboxCodigoColecaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarFamiliaOnAction(null);
        } else if (event.getCode() == KeyCode.ENTER) {
            try {
                String filterEdited = "";
                for (String codigo : tboxCodigoColecao.getText().split(",")) {
                    filterEdited += "'" + codigo + "',";
                }
                listDisponiveis.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getAvailableByFilter(tboxCodigoFamilia.getText(), filterEdited.substring(0, filterEdited.length() - 1)));
                listDisponiveisAll.clear();
                listDisponiveisAll.addAll(listDisponiveis);
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnProcurarColecaoOnAction(ActionEvent event) {
        FilterColecoesView filterColecoesView = new FilterColecoesView();
        filterColecoesView.setValorFiltroInicial(tboxCodigoColecao.getText());
        filterColecoesView.show(false);
        if(filterColecoesView.itensSelecionados.size() > 0){
            tboxCodigoColecao.setText(filterColecoesView.getResultAsString(true));

            try {
                listDisponiveis.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getAvailableByFilter(tboxCodigoFamilia.getText(), tboxCodigoColecao.getText()));
                listDisponiveisAll.clear();
                listDisponiveisAll.addAll(listDisponiveis);
            } catch (SQLException ex) {
                Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }
    }

    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
        try {
            FilterFamiliaController ctrolFilterColecao = new FilterFamiliaController(Modals.FXMLWindow.FilterFamilia, tboxCodigoFamilia.getText());
            if (!ctrolFilterColecao.returnValue.isEmpty()) {
                tboxCodigoFamilia.setText(ctrolFilterColecao.returnValue);
                try {
                    listDisponiveis.set(DAOFactory.getSdProdutoPrioridadeMrp001DAO().getAvailableByFilter(tboxCodigoFamilia.getText(), tboxCodigoColecao.getText()));
                    listDisponiveisAll.clear();
                    listDisponiveisAll.addAll(listDisponiveis);
                } catch (SQLException ex) {
                    Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void listReferenciasDisponiveisOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            btnAddSelectedOnAction(null);
        }
    }

    @FXML
    private void btnAddSelectedOnAction(ActionEvent event) {
        ObservableList<SdProdutoPrioridadeMrp001> listToAdd = listReferenciasDisponiveis.getSelectionModel().getSelectedItems();
        listToAdd.forEach(produto -> produto.setPrioridade(Integer.parseInt(cboxPrioridade.getValue())));
        listSelecionados.addAll(listToAdd);
        listSelecionadosAll.addAll(listToAdd);
        listDisponiveis.removeAll(listToAdd);
        listDisponiveisAll.removeAll(listToAdd);

    }

    @FXML
    private void btnAddAllOnAction(ActionEvent event) {
        ObservableList<SdProdutoPrioridadeMrp001> listToAdd = listReferenciasDisponiveis.getItems();
        listToAdd.forEach(produto -> produto.setPrioridade(Integer.parseInt(cboxPrioridade.getValue())));
        listSelecionados.addAll(listToAdd);
        listSelecionadosAll.addAll(listToAdd);
        listDisponiveis.removeAll(listToAdd);
        listDisponiveisAll.removeAll(listToAdd);
    }

    @FXML
    private void btnDeleteSelectedOnAction(ActionEvent event) {
        ObservableList<SdProdutoPrioridadeMrp001> listToDelete = tblProdutoSelecionado.getSelectionModel().getSelectedItems();
        listDisponiveis.addAll(listToDelete);
        listDisponiveisAll.addAll(listToDelete);
        listSelecionados.removeAll(listToDelete);
        listSelecionadosAll.removeAll(listToDelete);
    }

    @FXML
    private void btnDeleteAllOnAction(ActionEvent event) {
        ObservableList<SdProdutoPrioridadeMrp001> listToDelete = tblProdutoSelecionado.getItems();

        listDisponiveis.addAll(listToDelete);
        listDisponiveisAll.addAll(listToDelete);
        listSelecionados.removeAll(listToDelete);
        listSelecionadosAll.removeAll(listToDelete);
    }

    @FXML
    private void tblProdutoSelecionadoOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            btnDeleteSelectedOnAction(null);
        }

    }

    @FXML
    private void btnSalvarOnAtion(ActionEvent event) {
        try {
            DAOFactory.getSdProdutoPrioridadeMrp001DAO().deleteAll();
        } catch (SQLException ex) {
            Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        listSelecionados.forEach(produto -> {
            try {
                DAOFactory.getSdProdutoPrioridadeMrp001DAO().save(produto);
            } catch (SQLException ex) {
                Logger.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        GUIUtils.showMessageNotification("Cadastro de Prioridades", "Produtos priorizados para o próximo MRP.");
    }

    private class ListOperationsCell extends ListCell<SdProdutoPrioridadeMrp001> {

        @Override
        public void updateItem(SdProdutoPrioridadeMrp001 item, boolean empty) {
            super.updateItem(item, empty);
            this.setText(null);
            this.setGraphic(null);
            if (item != null || !empty) {
                int index = this.getIndex();
                this.setText("[" + item.getCodigo() + "] " + item.getDescricao());
            }
        }
    }
}
