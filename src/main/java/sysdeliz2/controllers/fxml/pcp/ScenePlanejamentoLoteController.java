/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.controllers.fxml.SceneCreateNewPartController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.GestaoProducaoDAO;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.Log;
import sysdeliz2.models.SdProduto001;
import sysdeliz2.models.SomatorioCoresGestao;
import sysdeliz2.models.TabPrz001;
import sysdeliz2.models.properties.GestaoDeLote;
import sysdeliz2.models.properties.GestaoDeLoteGrade;
import sysdeliz2.models.properties.GestaoDeProduto;
import sysdeliz2.models.sysdeliz.SdStatusProduto;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.TableUtils;
import sysdeliz2.utils.ToggleSwitch;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.FormFieldComboBox;
import sysdeliz2.utils.gui.components.FormFieldDate;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ScenePlanejamentoLoteController implements Initializable {

    /**
     * Local VAR
     */
    private Map dadosProduto;
    private String dataInicioVendas, dataFimVendas, codigoProduto;
    private GestaoProducaoDAO daoGestao;
    private ToggleSwitch tsPvenda = new ToggleSwitch(20, 48);
    private ToggleSwitch tsPvendaCor = new ToggleSwitch(20, 48);
    private ToggleSwitch tsCalculoGlobal = new ToggleSwitch(20, 48);
    private ToggleSwitch tsCalculoComMost = new ToggleSwitch(20, 48);
    private ToggleSwitch tsZerarCor = new ToggleSwitch(20, 48);
    private Integer metaGlobal, emProducaoGlobal, produzidoGlobal,
            concentradoGlobal, planejadoGlobal, confirmadoGlobal, mostruarioGlobal, saldoLotear = 0,
            vendaPendenteGlobal, estoqueGlobal, saldoProducao, projetadoRevisao;
    int totalNegativos = 0;
    int multiploPadrao = 0;
    private String setorAlterado = "";
    private Integer novoPlanejado = 0;
    private Image imgLOCKED = new Image(getClass().getResource("/images/icons/buttons/lock (1).png").toExternalForm());
    private Image imgUNLOCKED = new Image(getClass().getResource("/images/icons/buttons/unlock (1).png").toExternalForm());
    private Image imgLOCKED16 = new Image(getClass().getResource("/images/icons/buttons/lock (1).png").toExternalForm());
    private Image imgUNLOCKED16 = new Image(getClass().getResource("/images/icons/buttons/unlock (1).png").toExternalForm());
    private List<String> colecoesConsulta = new ArrayList();
    private List<String> periodosConsulta = new ArrayList();
    private Boolean continuarCalculando = false;
    private List<SdStatusProduto> statusProdutos = new ArrayList<>();

    private final SdStatusProduto _STATUS_PRODUTO_LIMITADO = new FluentDao().selectFrom(SdStatusProduto.class).where(eb -> eb.equal("codigo", 2)).singleResult();

    public Boolean changedPart = false;

    // <editor-fold defaultstate="collapsed" desc="FXML Configurations">
    @FXML
    private Label lbMeta;
    @FXML
    private Label lbIndice;
    @FXML
    private Label lbProjecao;
    @FXML
    private Label lbProjetadoRevisao;
    @FXML
    private Label lbSaldoLotear;
    @FXML
    private Label lbGPlanejado;
    @FXML
    private Label lbGSaldoProducao;
    @FXML
    private Label lbPrMeta;
    @FXML
    private Button btnCancelar;
    @FXML
    private Label lbGProducao;
    @FXML
    private Label lbGProduzido;
    @FXML
    private Label lbGMostruario;
    @FXML
    private Label lbTogglePvenda;
    @FXML
    private Label lbTogglePvendaCor;
    @FXML
    private Label lbToggleZerarCor;
    @FXML
    private Label lbGVendTotal;
    @FXML
    private Label lbGVendPend;
    @FXML
    private Label lbGConcentrado;
    @FXML
    private Label lbGVendFat;
    @FXML
    private Label lbTagGSaldoProducao;
    @FXML
    private Label lbGSaldoPlanejado;
    @FXML
    private Label lbGSaldoEstoque;
    @FXML
    private Label lbGEstoque;
    @FXML
    private Pane pnTogglePvenda;
    @FXML
    private Pane pnTogglePvendaCor;
    @FXML
    private Pane pnToggleZerarCor;
    @FXML
    private Button btnAddLote;
    @FXML
    private MenuItem menuSelectAll;
    @FXML
    private TableView<SomatorioCoresGestao> tblSomatoriosCor;
    @FXML
    private TableView<GestaoDeLote> tblPlanejaLotes;
    @FXML
    private TableView<GestaoDeLoteGrade> tblGradeLote;
    @FXML
    private TableView<GestaoDeProduto> tblGradeGlobal;
    @FXML
    private TableColumn<GestaoDeLote, Integer> clmPlanejado;
    @FXML
    private TableColumn<GestaoDeLote, String> clnOfPlanejamento;
    @FXML
    private TableColumn<GestaoDeLote, String> clnSetorOf;
    @FXML
    private TableColumn<GestaoDeLoteGrade, Integer> clmQtdePlanejadoLote;
    @FXML
    private TableColumn<SomatorioCoresGestao, Integer> clnMultiplo;
    @FXML
    private TableColumn<SomatorioCoresGestao, Boolean> clnSelected;
    @FXML
    private Pane pnToggleCalculoGlobal;
    @FXML
    private Label lbToggleCalculoGlobal;
    @FXML
    private Pane pnToggleComMost;
    @FXML
    private Label lbToggleComMost;
    @FXML
    private TextField tboxSimulacaoPlanejado;
    @FXML
    private Button btnEditarGradeLote;
    @FXML
    private Button btnAtualizaLotes;
    @FXML
    private Button btnConfirmarPlano;
    @FXML
    private Button btnProgNegativos;
    @FXML
    private ImageView imgLock;
    @FXML
    private TableColumn<GestaoDeLote, Boolean> clmLockedLote;
    @FXML
    private TextField tboxProduto;
    @FXML
    private TextField tboxMarca;
    @FXML
    private TextField tboxColecao;
    @FXML
    private TextArea tboxObservacaoPcp;

    @FXML
    private HBox boxDtProduto;
    @FXML
    private TextField tboxStatusComercialProduto;
    @FXML
    private ComboBox<SdStatusProduto> cboxStatusPcpProduto;
    @FXML
    private TableColumn<SomatorioCoresGestao, SdStatusProduto> clnStatusComercialCor;
    @FXML
    private TableColumn<SomatorioCoresGestao, SomatorioCoresGestao> clnStatusPcpCor;
    @FXML
    private TableColumn<GestaoDeProduto, SdStatusProduto> clnStatusComercialTam;
    @FXML
    private TableColumn<GestaoDeProduto, GestaoDeProduto> clnStatusPcpTam;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        daoGestao = DAOFactory.getGestaoProducaoDAO();
        statusProdutos = (List<SdStatusProduto>) new FluentDao().selectFrom(SdStatusProduto.class).get().resultList();
        cboxStatusPcpProduto.setItems(FXCollections.observableList(statusProdutos));
        try {
            // <editor-fold defaultstate="collapsed" desc="Get observação PCP do produto">
            SdProduto001 sdProduto = DAOFactory.getSdProduto001DAO().getByCodigo(codigoProduto);
            tboxObservacaoPcp.setText(sdProduto != null ? sdProduto.getObservacaoPcp() : "");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TableView GRADE GLOBAL">
            List<GestaoDeProduto> gradeProduto = new ArrayList<>(daoGestao.getAnaliseProduto(codigoProduto, dataInicioVendas, dataFimVendas, periodosConsulta, colecoesConsulta));
            tblGradeGlobal.setItems(FXCollections.observableList(gradeProduto));
            this.calculaPercSaldoPlanejado();
            this.calculeSomatoriosCores();
            Callback<TableColumn<SomatorioCoresGestao, Integer>, TableCell<SomatorioCoresGestao, Integer>> cellFactoryMultiplo
                    = new Callback<TableColumn<SomatorioCoresGestao, Integer>, TableCell<SomatorioCoresGestao, Integer>>() {
                public TableCell call(TableColumn p) {
                    return new EditingCellTblSomatorio();
                }
            };
            clnMultiplo.setCellFactory(cellFactoryMultiplo);
            clnMultiplo.setEditable(true);

            clnSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
            clnSelected.setCellValueFactory((cellData) -> {
                SomatorioCoresGestao cellValue = cellData.getValue();
                BooleanProperty property = cellValue.selectedProperty();
                property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
                return property;
            });

            //TableUtils.autoFitTable(tblGradeGlobal, TableUtils.COLUNAS_TBL_GESTAO_PRODUCAO_GRADE_GLOBAL);
            tblGradeGlobal.getColumns().forEach(coluna -> {
                TableUtils.makeHeaderWrappable(coluna, Pos.BOTTOM_LEFT, TextAlignment.LEFT);
            });
            tblGradeGlobal.setOnMousePressed((event) -> {
                this.calculeSomatoriosCores();
                this.calculeSomatoriosCoresGlobal();
            });

            // dados status produto
            List<SdStatusProduto> statusPcp = tblSomatoriosCor.getItems().stream()
                    .map(gradeGlobal -> gradeGlobal.getStatusPcp())
                    .distinct()
                    .collect(Collectors.toList());
            List<SdStatusProduto> statusComercial = tblSomatoriosCor.getItems().stream()
                    .map(gradeGlobal -> gradeGlobal.getStatusComercial())
                    .distinct()
                    .collect(Collectors.toList());
            tboxStatusComercialProduto.setText((statusComercial.stream().filter(status -> status.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO : statusComercial.stream().max(Comparator.comparingInt(SdStatusProduto::getOrdem)).get()).toString());
            cboxStatusPcpProduto.getSelectionModel().select(statusPcp.stream().filter(status -> status.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO : statusPcp.stream().max(Comparator.comparingInt(SdStatusProduto::getOrdem)).get());
            cboxStatusPcpProduto.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    if (tblSomatoriosCor.getItems().stream().anyMatch(cor -> cor.getStatusPcp().getCodigo() != newValue.getCodigo()
                            && cor.getStatusPcp().getCodigo() != oldValue.getCodigo()
                            && cor.getStatusPcp().getOrdem() != 0)) {
                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Existem status em cores diferentes dos que você selecionou, deseja aplicar o novo status para todas as cores?");
                            message.showAndWait();
                        }).value.get())) {
                            tblSomatoriosCor.getItems().stream().filter(cor -> cor.getStatusPcp().getOrdem() != 0).forEach(cor -> {
                                cor.setStatusPcp(newValue);
                            });
                        } else {
                            tblSomatoriosCor.getItems().stream().filter(cor -> cor.getStatusPcp().getCodigo() == newValue.getCodigo()
                                    || cor.getStatusPcp().getCodigo() == oldValue.getCodigo()).forEach(cor -> {
                                cor.setStatusPcp(newValue);
                            });
                        }
                    } else {
                        tblSomatoriosCor.getItems().stream().filter(cor -> cor.getStatusPcp().getOrdem() != 0).forEach(cor -> {
                            cor.setStatusPcp(newValue);
                        });
                    }
                    tblSomatoriosCor.refresh();
                    MessageBox.create(message -> {
                        message.message("Alterado status do produto de " + oldValue.toString() + " para " + newValue.toString());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            });

            tblSomatoriosCor.setEditable(true);
            clnStatusPcpCor.setEditable(true);
            clnStatusPcpCor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SomatorioCoresGestao, SomatorioCoresGestao>, ObservableValue<SomatorioCoresGestao>>() {
                @Override
                public ObservableValue<SomatorioCoresGestao> call(TableColumn.CellDataFeatures<SomatorioCoresGestao, SomatorioCoresGestao> features) {
                    return new ReadOnlyObjectWrapper(features.getValue());
                }
            });
            clnStatusPcpCor.setComparator(new Comparator<SomatorioCoresGestao>() {
                @Override
                public int compare(SomatorioCoresGestao p1, SomatorioCoresGestao p2) {
                    return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
                }
            });
            clnStatusPcpCor.setCellFactory(param -> {
//                ComboBoxTableCell<SomatorioCoresGestao, SdStatusProduto> cboxCell = new ComboBoxTableCell<SomatorioCoresGestao, SdStatusProduto>(FXCollections.observableList(statusProdutos)) {
//                    @Override
//                    public void updateItem(SdStatusProduto item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (item != null && !empty && tblSomatoriosCor.getSelectionModel().getSelectedItem() != null) {
//                            if (item.getCodigo() != tblSomatoriosCor.getSelectionModel().getSelectedItem().getStatusPcp().getCodigo()) {
//                                tblSomatoriosCor.getSelectionModel().getSelectedItem().setStatusPcp(item);
//                                MessageBox.create(message -> {
//                                    message.message("Alterado status da cor " + tblSomatoriosCor.getSelectionModel().getSelectedItem().getCod_cor() + " para " + item.toString());
//                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                    message.position(Pos.TOP_RIGHT);
//                                    message.notification();
//                                });
//                            }
//                        }
//                    }
//                };
//                return cboxCell;
                return new TableCell<SomatorioCoresGestao, SomatorioCoresGestao>() {
                    @Override
                    protected void updateItem(SomatorioCoresGestao item, boolean empty) {
                        super.updateItem(item, empty);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setGraphic(FormFieldComboBox.create(SdStatusProduto.class, field -> {
                                field.withoutTitle();
                                field.addStyle("xs");
                                field.items.set(FXCollections.observableList(statusProdutos));
                                field.select(item.getStatusPcp());
                                field.addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null) {
                                        item.setStatusPcp((SdStatusProduto) newValue);
                                        MessageBox.create(message -> {
                                            message.message("Alterando o status da cor " + item.getCod_cor() + " de " + oldValue.toString() + " para " + newValue.toString());
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                });
                            }).build());
                        }
                    }
                };
            });
            tblSomatoriosCor.getItems().forEach(item -> {
                item.statusPcpProperty().addListener(new ChangeListener<SdStatusProduto>() {
                    @Override
                    public void changed(ObservableValue<? extends SdStatusProduto> observable, SdStatusProduto oldValue, SdStatusProduto newValue) {
                        if (newValue != null && oldValue.getCodigo() != newValue.getCodigo()) {
                            if (tblGradeGlobal.getItems().stream().filter(grade -> grade.getCor().equals(item.getCod_cor()) && grade.getStatusPcp().getOrdem() != 0)
                                    .anyMatch(grade -> grade.getStatusPcp().getCodigo() != newValue.getCodigo()
                                            && grade.getStatusPcp().getCodigo() != oldValue.getCodigo())) {
                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Existem status em tamanhos diferentes dos que você selecionou na cor " + item.getCod_cor() + ", deseja aplicar o novo status para todos os tamanhos?");
                                    message.showAndWait();
                                }).value.get())) {
                                    tblGradeGlobal.getItems().stream().filter(grade -> grade.getCor().equals(item.getCod_cor()) && grade.getStatusPcp().getOrdem() != 0).forEach(grade -> {
                                        grade.setStatusPcp(((SdStatusProduto) newValue));
                                    });
                                } else {
                                    tblGradeGlobal.getItems().stream().filter(grade -> grade.getCor().equals(item.getCod_cor())
                                                    && (grade.getStatusPcp().getCodigo() == newValue.getCodigo() || grade.getStatusPcp().getCodigo() == oldValue.getCodigo()))
                                            .forEach(grade -> {
                                                grade.setStatusPcp(newValue);
                                            });
                                }
                            } else {
                                tblGradeGlobal.getItems().stream().filter(grade -> grade.getCor().equals(item.getCod_cor()) && grade.getStatusPcp().getOrdem() != 0).forEach(grade -> {
                                    grade.setStatusPcp(newValue);
                                });
                            }
                            tblGradeGlobal.refresh();
                        }
                    }
                });
            });

            tblGradeGlobal.setEditable(true);
            clnStatusPcpTam.setEditable(true);
            clnStatusPcpTam.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GestaoDeProduto, GestaoDeProduto>, ObservableValue<GestaoDeProduto>>() {
                @Override
                public ObservableValue<GestaoDeProduto> call(TableColumn.CellDataFeatures<GestaoDeProduto, GestaoDeProduto> features) {
                    return new ReadOnlyObjectWrapper(features.getValue());
                }
            });
            clnStatusPcpTam.setComparator(new Comparator<GestaoDeProduto>() {
                @Override
                public int compare(GestaoDeProduto p1, GestaoDeProduto p2) {
                    return compare(p1, p2);//p1.getCodigo().compareTo(p2.getCodigo());
                }
            });
            clnStatusPcpTam.setCellFactory(param -> {
//                ComboBoxTableCell<GestaoDeProduto, SdStatusProduto> cboxCell = new ComboBoxTableCell<GestaoDeProduto, SdStatusProduto>(FXCollections.observableList(statusProdutos)) {
//                    @Override
//                    public void updateItem(SdStatusProduto item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (item != null && !empty && tblGradeGlobal.getSelectionModel().getSelectedItem() != null) {
//                            if (item.getCodigo() != tblGradeGlobal.getSelectionModel().getSelectedItem().getStatusPcp().getCodigo()) {
//                                tblGradeGlobal.getSelectionModel().getSelectedItem().setStatusPcp(item);
//                                MessageBox.create(message -> {
//                                    message.message("Alterado status da cor/tam " + tblGradeGlobal.getSelectionModel().getSelectedItem().getCor()+"/"+
//                                            tblGradeGlobal.getSelectionModel().getSelectedItem().getTam() + " para " + item.toString());
//                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                    message.position(Pos.TOP_RIGHT);
//                                    message.notification();
//                                });
//                            }
//                        }
//                    }
//                };
//                return cboxCell;
                return new TableCell<GestaoDeProduto, GestaoDeProduto>() {
                    @Override
                    protected void updateItem(GestaoDeProduto item, boolean empty) {
                        super.updateItem(item, empty);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setGraphic(FormFieldComboBox.create(SdStatusProduto.class, field -> {
                                field.withoutTitle();
                                field.addStyle("xs");
                                field.items.set(FXCollections.observableList(statusProdutos));
                                field.select(item.getStatusPcp());
                                field.addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null) {
                                        item.setStatusPcp((SdStatusProduto) newValue);
                                        MessageBox.create(message -> {
                                            message.message("Alterando o status da cor/tam " + item.getCor()+"/"+item.getTam() + " de " + oldValue.toString() + " para " + newValue.toString());
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                });
                            }).build());
                        }
                    }
                };
            });
            tblGradeGlobal.getItems().forEach(item -> {
                item.statusPcpProperty().addListener(new ChangeListener<SdStatusProduto>() {
                    @Override
                    public void changed(ObservableValue<? extends SdStatusProduto> observable, SdStatusProduto oldValue, SdStatusProduto newValue) {
                        if (newValue != null && oldValue.getCodigo() != newValue.getCodigo()) {
                            try {
                                new NativeDAO().runNativeQueryUpdate("update sd_status_grade_prod_001 set status_pcp = '%s', dt_status_pcp = sysdate where codigo = '%s' and cor = '%s' and tam = '%s'",
                                        newValue.getCodigo(), codigoProduto, item.getCor(), item.getTam());
                                new NativeDAO().runNativeQueryUpdate("update sd_produto_001 set revisao_comercial = 'S' where codigo = '%s'", codigoProduto);
                                SysLogger.addSysDelizLog("Gestão de Lote", TipoAcao.EDITAR, codigoProduto, "Alterado status do produto na grade "+ item.getCor() + "/" + item.getTam()
                                        + " de " + oldValue.toString() + " para " + newValue.toString());
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        }
                    }
                });
            });

            // data entrega produto
            Produto produtoGestao = new FluentDao().selectFrom(Produto.class).where(eb -> eb.equal("codigo", codigoProduto)).singleResult();
            boxDtProduto.getChildren().add(FormFieldDate.create(field -> {
                field.withoutTitle();
                field.defaultValue(produtoGestao.getDtEntrega());
                field.value.addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        LocalDate dataAnterior = produtoGestao.getDtEntrega();
                        produtoGestao.setDtEntrega(newValue);
                        new FluentDao().merge(produtoGestao);
                        SysLogger.addSysDelizLog("Gestão de Produção", TipoAcao.EDITAR, codigoProduto, "Data de Entrega do produto " + codigoProduto + " ");
                        MessageBox.create(message -> {
                            message.message("Alterada data de entrega do produto de " + StringUtils.toDateFormat(dataAnterior) + " para " + StringUtils.toDateFormat(newValue));
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    }
                });
            }).build());
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TableView OFS">
            tblPlanejaLotes.setItems((daoGestao.getLotesProduto(codigoProduto)));
            clmLockedLote.setCellFactory(new Callback<TableColumn<GestaoDeLote, Boolean>, TableCell<GestaoDeLote, Boolean>>() {
                @Override
                public TableCell<GestaoDeLote, Boolean> call(TableColumn<GestaoDeLote, Boolean> param) {
                    TableCell cell = new TableCell<GestaoDeLote, Boolean>() {
                        @Override
                        public void updateItem(Boolean item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(empty ? " " : "" + getItem());
                            setGraphic(null);
                            if (getItem() != null) {
                                if (getItem()) {
                                    setGraphic(new ImageView(imgLOCKED16));
                                } else {
                                    setGraphic(new ImageView(imgUNLOCKED16));
                                }
                            }
                        }
                    };
                    return cell;
                }
            });
            //Desetivado até que solicitem fazer a alteração direto na célula
            //tem que implementar o OnCommit com a mesma lógica do OnKeyPressed do TextBox
            Callback<TableColumn<GestaoDeLote, String>, TableCell<GestaoDeLote, String>> cellFactoryQtdeLote
                    = new Callback<TableColumn<GestaoDeLote, String>, TableCell<GestaoDeLote, String>>() {
                public TableCell call(TableColumn p) {
                    return new EditingCellTblLote();
                }
            };
            clnOfPlanejamento.setCellFactory(cellFactoryQtdeLote);
            Callback<TableColumn<GestaoDeLote, String>, TableCell<GestaoDeLote, String>> cellFactorySetorOf
                    = new Callback<TableColumn<GestaoDeLote, String>, TableCell<GestaoDeLote, String>>() {
                public TableCell call(TableColumn p) {
                    return new EditingCellTblSetorOf();
                }
            };
            clnSetorOf.setCellFactory(cellFactorySetorOf);
            tblPlanejaLotes.getSelectionModel().selectedItemProperty().addListener((event, oldValue, newValue) -> {
                unbindData(oldValue);
                try {
                    bindData(newValue);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            });

            Callback<TableColumn<GestaoDeLoteGrade, Integer>, TableCell<GestaoDeLoteGrade, Integer>> cellFactoryQtde
                    = new Callback<TableColumn<GestaoDeLoteGrade, Integer>, TableCell<GestaoDeLoteGrade, Integer>>() {
                public TableCell call(TableColumn p) {
                    return new EditingCellTblGradeLote();
                }
            };
            clmQtdePlanejadoLote.setCellFactory(cellFactoryQtde);
            clmPlanejado.setEditable(true);

            tblPlanejaLotes.setOnMousePressed((event) -> {
                this.calculeSomatoriosCoresOf();
            });
            //</editor-fold>

        } catch (SQLException ex) {
            Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
        // <editor-fold defaultstate="collapsed" desc="Criação dos objects toggle de escolha">
        pnTogglePvenda.getChildren().add(tsPvenda);
        lbTogglePvenda.textProperty().bind(Bindings.when(tsPvenda.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        pnTogglePvendaCor.getChildren().add(tsPvendaCor);
        lbTogglePvendaCor.textProperty().bind(Bindings.when(tsPvendaCor.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        tsZerarCor.switchedOnProperty().set(true);
        pnToggleZerarCor.getChildren().add(tsZerarCor);
        lbToggleZerarCor.textProperty().bind(Bindings.when(tsZerarCor.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        pnToggleComMost.getChildren().add(tsCalculoComMost);
        lbToggleComMost.textProperty().bind(Bindings.when(tsCalculoComMost.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        //-----------------
        pnToggleCalculoGlobal.getChildren().add(tsCalculoGlobal);
        lbToggleCalculoGlobal.textProperty().bind(Bindings.when(tsCalculoGlobal.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        tsCalculoGlobal.switchedOnProperty().addListener((obs, oldState, newState) -> {
            if (tblPlanejaLotes.getSelectionModel().getSelectedItem() != null) {
                if (newState.booleanValue()) {
                    tboxSimulacaoPlanejado.textProperty().unbindBidirectional(tblPlanejaLotes.getSelectionModel().getSelectedItem().planejadoProperty());
                } else {
                    StringConverter<Number> converter = new NumberStringConverter();
                    Bindings.bindBidirectional(tboxSimulacaoPlanejado.textProperty(), tblPlanejaLotes.getSelectionModel().getSelectedItem().planejadoProperty(), converter);
                }
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Definição dos valores do grid de dados globais do produto">
        //Informações da referência, marca e coleção
        tboxProduto.setText("[" + dadosProduto.get("clm5").toString() + "] - " + dadosProduto.get("clm6").toString());
        tboxMarca.setText(dadosProduto.get("clm9").toString());
        tboxColecao.setText("[" + dadosProduto.get("clm2").toString() + "] - " + dadosProduto.get("clm3").toString());
        //Dados planejado ---------------------
        planejadoGlobal = !dadosProduto.get("clm42").toString().equals("-") ? Integer.parseInt(dadosProduto.get("clm42").toString().replace(".", "")) : 0;
        confirmadoGlobal = !dadosProduto.get("clm41").toString().equals("-") ? Integer.parseInt(dadosProduto.get("clm41").toString().replace(".", "")) : 0;
        planejadoGlobal += confirmadoGlobal;
        lbGPlanejado.setText("" + planejadoGlobal);
        //-------------------------------------
        //Dados em produção e produzido -------
        emProducaoGlobal = dadosProduto.get("clm43").toString().equals("-") ? 0 - planejadoGlobal : Integer.parseInt(dadosProduto.get("clm43").toString().replace(".", "")) - planejadoGlobal;
        lbGProducao.setText("" + emProducaoGlobal);
        produzidoGlobal = Integer.parseInt(dadosProduto.get("clm26").toString().replace(".", "")) - emProducaoGlobal;
        lbGProduzido.setText("" + produzidoGlobal);
        //-------------------------------------
        //Dados concentrado -------------------
        concentradoGlobal = Integer.parseInt(dadosProduto.get("clm20").toString().replace(".", ""));
        lbGConcentrado.setText("" + concentradoGlobal);
        //-------------------------------------
        //Dados de vendas ---------------------
        lbGVendTotal.setText(dadosProduto.get("clm18").toString());
        vendaPendenteGlobal = Integer.parseInt(dadosProduto.get("clm19").toString().replace(".", ""));
        lbGVendPend.setText("" + vendaPendenteGlobal);
        lbGVendFat.setText("" + (Integer.parseInt(dadosProduto.get("clm18").toString().replace(".", "")) - Integer.parseInt(dadosProduto.get("clm19").toString().replace(".", ""))));
        //-------------------------------------
        //Dados de estoque e mostruário -------
        estoqueGlobal = Integer.parseInt(dadosProduto.get("clm27").toString().replace(".", ""));
        lbGEstoque.setText("" + estoqueGlobal);
        lbGSaldoEstoque.setText("" + (estoqueGlobal - vendaPendenteGlobal));
        mostruarioGlobal = Integer.parseInt(dadosProduto.get("clm28").toString().replace(".", ""));
        lbGMostruario.setText("" + mostruarioGlobal);
        //-------------------------------------
        //Dados de metas e saldos -------------
        metaGlobal = Integer.parseInt(dadosProduto.get("clm22").toString().replace(".", ""));
        lbMeta.setText("" + metaGlobal);
        saldoProducao = estoqueGlobal + mostruarioGlobal + emProducaoGlobal - vendaPendenteGlobal;
        lbGSaldoProducao.setText("" + saldoProducao);
        if (saldoProducao < 0) {
            lbTagGSaldoProducao.setStyle("-fx-background-color: #FFC0CB");
        } else {
            lbTagGSaldoProducao.setStyle("-fx-background-color: #98FB98");
        }
        lbGSaldoPlanejado.setText("" + (saldoProducao + planejadoGlobal));
        projetadoRevisao = Integer.parseInt(dadosProduto.get("clm21").toString().replace(".", ""));
        lbProjetadoRevisao.setText("" + projetadoRevisao);
        if (projetadoRevisao > 0) {
            saldoLotear = projetadoRevisao - metaGlobal;
        }
        lbSaldoLotear.setText("" + saldoLotear);
        //-------------------------------------
        //Dados de projeção e indice ----------
        lbIndice.setText(dadosProduto.get("clm23").toString());
        lbProjecao.setText(dadosProduto.get("clm24").toString().replace(".", ""));
        lbPrMeta.setText(dadosProduto.get("clm25").toString().replace(".", ""));
        //-------------------------------------
        //</editor-fold>

        btnEditarGradeLote.disableProperty().bind(tblPlanejaLotes.getSelectionModel().selectedItemProperty().isNull());

        // merge status do produto
        tblGradeGlobal.getItems().forEach(itemGlobal -> {
            try {
                List<Object> countStatusProduto = new NativeDAO().runNativeQuery("select count(*) existStatus from sd_status_grade_prod_001 where codigo = '%s' and cor = '%s' and tam = '%s'",
                        codigoProduto, itemGlobal.getCor(), itemGlobal.getTam());
                Map<String, Object> mapExistStatusGrade = (Map<String, Object>) countStatusProduto.get(0);
                if (((BigDecimal) mapExistStatusGrade.get("EXISTSTATUS")).compareTo(BigDecimal.ZERO) == 0) {
                    new NativeDAO().runNativeQueryUpdate("insert into sd_status_grade_prod_001 (codigo, cor, tam, status_pcp, status_comercial, dt_status_pcp, dt_status_comercial) values ('%s','%s','%s','%s','%s',sysdate, sysdate)",
                            codigoProduto, itemGlobal.getCor(), itemGlobal.getTam(), itemGlobal.getStatusPcp().getCodigo(), itemGlobal.getStatusComercial().getCodigo());
                }
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });

    }

    /**
     * Local METHODS
     */
    public ScenePlanejamentoLoteController(Map produto, String data_inicio, String data_fim, List<String> colecoes, List<String> periodos) {
        colecoesConsulta = colecoes;
        periodosConsulta = periodos;
        dataInicioVendas = data_inicio;
        dataFimVendas = data_fim;
        dadosProduto = produto;
        codigoProduto = produto.get("clm5").toString();
        multiploPadrao = produto.get("clm10").toString().equals("DENIM") ? 2 : 1;
    }

    private void bindData(GestaoDeLote lote) throws FileNotFoundException {
        if (lote != null) {
            if (!tsCalculoGlobal.switchedOnProperty().get()) {
                StringConverter<Number> converter = new NumberStringConverter();
                Bindings.bindBidirectional(tboxSimulacaoPlanejado.textProperty(), lote.planejadoProperty(), converter);
            }
            if (lote.lockedProperty().get()) {
                tboxSimulacaoPlanejado.disableProperty().set(true);
            } else {
                tboxSimulacaoPlanejado.disableProperty().set(false);
            }
            imgLock.setImage(validationImage(lote));
            tblGradeLote.setItems(lote.getGrade());
            this.calculeSomatoriosCoresOf();
            tboxSimulacaoPlanejado.requestFocus();
        }
    }

    private void unbindData(GestaoDeLote lote) {
        if (lote != null) {
            if (!tsCalculoGlobal.switchedOnProperty().get()) {
                tboxSimulacaoPlanejado.textProperty().unbindBidirectional(lote.planejadoProperty());
            }
            tblGradeLote.setItems(FXCollections.observableArrayList());
        }
    }

    private GestaoDeProduto getAnaliseGlobalProduto(GestaoDeLoteGrade gradeLote) {

        for (GestaoDeProduto gradeGlobal : tblGradeGlobal.getItems()) {
            String corGlobal = gradeGlobal.getCor();
            String tamGlobal = gradeGlobal.getTam();
            if (gradeLote.getCod_cor().equals(corGlobal) && gradeLote.getTam().equals(tamGlobal)) {
                return gradeGlobal;
            }
        }

        return null;
    }

    private void somaNovoPlanejado(Integer valor) {
        novoPlanejado += valor;
    }

    private void calculaPercSaldoPlanejado() {
        final Integer totalPlanejado = tblGradeGlobal.getItems().stream()
                .mapToInt(sku -> sku.getSaldo_planej())
                .sum();
        tblGradeGlobal.getItems().forEach(sku -> {
            Double percSaldoPlanej = totalPlanejado > 0
                    ? BigDecimal.valueOf(sku.saldo_planejProperty().getValue().doubleValue() / totalPlanejado.doubleValue() * 100.0).setScale(2, RoundingMode.HALF_UP).doubleValue()
                    : 0.0;
            sku.setP_saldo_planej(percSaldoPlanej);
        });
        tblGradeGlobal.refresh();
    }

    private Integer somaPlanejadoOrigLotes() {
        Integer planejado = 0;
        for (GestaoDeProduto gradeGlobal : tblGradeGlobal.getItems()) {
            planejado += gradeGlobal.getPlanej_of();
        }
        return planejado;
    }

    private Integer somaPlanejadoLotes() {
        Integer planejado = 0;
        for (GestaoDeLote lote : tblPlanejaLotes.getItems()) {
            for (GestaoDeLoteGrade grade : lote.getGrade()) {
                planejado += grade.getPlanejado();
            }
        }
        return planejado;
    }

    private Boolean getCalculaCor(String cod_cor) {
        for (SomatorioCoresGestao cores : tblSomatoriosCor.getItems()) {
            if (cores.getCod_cor().equals(cod_cor)) {
                return cores.isSelected();
            }
        }
        return false;
    }

    private Integer somaSaldoProducaoGlobal() {
        Integer saldo = 0;
        for (GestaoDeProduto gradeGlobal : tblGradeGlobal.getItems()) {
            if (this.getCalculaCor(gradeGlobal.getCor())) {
                saldo += gradeGlobal.getSaldo_producao_calc();
            }
        }
        return saldo;
    }

    private void limpaSimuladoGlobal() {
        for (GestaoDeProduto gradeGlobal : tblGradeGlobal.getItems()) {
            gradeGlobal.setSimulacao(0);
        }
    }

    private void calculeSomatoriosCores() {

        ObservableList<SomatorioCoresGestao> cores = tblSomatoriosCor.getItems();//FXCollections.observableArrayList();
        for (GestaoDeProduto grade : tblGradeGlobal.getItems()) {
            SomatorioCoresGestao somatorioCores = null;
            for (SomatorioCoresGestao somatorio : cores) {
                if (somatorio.getCod_cor().equals(grade.getCor())) {
                    somatorioCores = somatorio;
                    break;
                }
            }
            if (somatorioCores == null) {
                List<GestaoDeProduto> gradeProduto = tblGradeGlobal.getItems();
                List<SdStatusProduto> statusPcp = gradeProduto.stream()
                        .filter(gradeGlobal -> gradeGlobal.getCor().equals(grade.getCor()))
                        .map(gradeGlobal -> gradeGlobal.getStatusPcp())
                        .distinct()
                        .collect(Collectors.toList());
                List<SdStatusProduto> statusComercial = gradeProduto.stream()
                        .filter(gradeGlobal -> gradeGlobal.getCor().equals(grade.getCor()))
                        .map(gradeGlobal -> gradeGlobal.getStatusComercial())
                        .distinct()
                        .collect(Collectors.toList());

                somatorioCores = new SomatorioCoresGestao(grade.getCor(), 0, 0, 0, 0, multiploPadrao,
                        statusComercial.stream().filter(status -> status.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO : statusComercial.stream().max(Comparator.comparingInt(SdStatusProduto::getOrdem)).get(),
                        statusPcp.stream().filter(status -> status.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO : statusPcp.stream().max(Comparator.comparingInt(SdStatusProduto::getOrdem)).get());
                cores.add(somatorioCores);
            }
            somatorioCores.setVenda_total(grade.venda_totalProperty().intValue());
            somatorioCores.setEm_producao(grade.em_producaoProperty().intValue());
            somatorioCores.setProduzido(grade.produzidoProperty().intValue());
            somatorioCores.setPlanej_of(grade.planej_ofProperty().intValue());
        }


        tblSomatoriosCor.setItems(cores);
    }

    private void calculeSomatoriosCoresOf() {
        for (SomatorioCoresGestao somatorioCor : tblSomatoriosCor.getItems()) {
            somatorioCor.setVenda_total(0);
            somatorioCor.setEm_producao(0);
            somatorioCor.setProduzido(0);
            somatorioCor.setPlanej_of(tblGradeLote.getItems().stream()
                    .filter(grade -> grade.getCod_cor().equals(somatorioCor.getCod_cor()))
                    .mapToInt(cor -> cor.getPlanejado())
                    .sum());
        }
    }

    private void calculeSomatoriosCoresGlobal() {
        for (SomatorioCoresGestao somatorioCor : tblSomatoriosCor.getItems()) {
            somatorioCor.setVenda_total(0);
            somatorioCor.setVenda_total(tblGradeGlobal.getItems().stream()
                    .filter(grade -> grade.getCor().equals(somatorioCor.getCod_cor()))
                    .mapToInt(cor -> cor.getVenda_total())
                    .sum());
            somatorioCor.setEm_producao(0);
            somatorioCor.setEm_producao(tblGradeGlobal.getItems().stream()
                    .filter(grade -> grade.getCor().equals(somatorioCor.getCod_cor()))
                    .mapToInt(cor -> cor.getEm_producao())
                    .sum());
            somatorioCor.setProduzido(0);
            somatorioCor.setProduzido(tblGradeGlobal.getItems().stream()
                    .filter(grade -> grade.getCor().equals(somatorioCor.getCod_cor()))
                    .mapToInt(cor -> cor.getProduzido())
                    .sum());
            somatorioCor.setPlanej_of(0);
            somatorioCor.setPlanej_of(tblGradeGlobal.getItems().stream()
                    .filter(grade -> grade.getCor().equals(somatorioCor.getCod_cor()))
                    .mapToInt(cor -> cor.getPlanej_of())
                    .sum());
        }
    }

    private Integer getMultiploCor(String cod_cor) {
        for (SomatorioCoresGestao cores : tblSomatoriosCor.getItems()) {
            if (cores.getCod_cor().equals(cod_cor)) {
                return cores.getMultiplo();
            }
        }
        return 0;
    }

    private Double getPerVendaCor(String cod_cor) {
        Integer totalVendas = 0;
        Integer totalVendasCor = 0;
        Double perVendaCor = 0.0;
        for (SomatorioCoresGestao cores : tblSomatoriosCor.getItems()) {
            if (cores.isSelected()) {
                totalVendas += tblGradeGlobal.getItems().stream()
                        .filter(gradeGlobal -> gradeGlobal.getCor().equals(cores.getCod_cor()))
                        .mapToInt(gradeGlobal -> gradeGlobal.getCarteira())
                        .sum();
            }
        }

        totalVendasCor = tblGradeGlobal.getItems().stream()
                .filter(gradeGlobal -> gradeGlobal.getCor().equals(cod_cor))
                .mapToInt(gradeGlobal -> gradeGlobal.getCarteira())
                .sum();

        perVendaCor = totalVendasCor.doubleValue() / totalVendas.doubleValue() * 100.0;
        return perVendaCor;
    }

    private Double getPerProdutoCor(String cod_cor, String tam) {
        Double totalVendas = 0.0;
        Double totalVendasCor = 0.0;
        Double perVendaCor = 0.0;
        for (SomatorioCoresGestao cores : tblSomatoriosCor.getItems()) {
            if (cores.isSelected()) {
                totalVendas += tblGradeGlobal.getItems().stream()
                        .filter(gradeGlobal -> gradeGlobal.getCor().equals(cores.getCod_cor()))
                        .mapToDouble(gradeGlobal -> gradeGlobal.getP_prod_tam())
                        .sum();
            }
        }

        totalVendasCor = tblGradeGlobal.getItems().stream()
                .filter(gradeGlobal -> Boolean.logicalAnd(gradeGlobal.getCor().equals(cod_cor), gradeGlobal.getTam().equals(tam)))
                .mapToDouble(gradeGlobal -> gradeGlobal.getP_prod_tam())
                .sum();

        perVendaCor = totalVendasCor / totalVendas * 100.0;
        return perVendaCor;
    }

    public Image validationImage(GestaoDeLote lote) {
        btnEditarGradeLote.setText(lote.lockedProperty().get() ? "Liberar O.F." : "Bloquear O.F.");
        return lote.lockedProperty().get() ? imgUNLOCKED : imgLOCKED;
    }

    private void calculaGrade(Integer qtdeLote, Integer saldoProducao, Float percentVenda, GestaoDeLote lote) {
        //Preparando variáveis para cálculo
        Integer simulacaoPlanejado = 0;
        Float percentNegativos = ((Integer) 1).floatValue();
        Integer planejadosNegativos = 0; //Variável que irá salvar se tiver planejados negativos para a grade.
        Integer baseGrade = qtdeLote + saldoProducao; //Variável que irá guardar o cálculo base para a formula da simulação.
        Float basePorcentagem = ((Integer) 1).floatValue(); //Variável que irá guardar a base da porcentagem para cálculo da simulação, sendo que quando for planejado zerado, será desconsiderado do cálculo subtraindo a porcentagem de venda.
        Integer planejadoLote = 0; //Variável que irá salvar o valor do planejado no lote
        Integer saldoProducaoCalc = saldoProducao;

        //dentro do lote, é calculado tamanho por cor do produto.
        for (GestaoDeLoteGrade gradeLocal : lote.getGrade()) {
            //@gradeGlobal irá receber o objeto da grade global para pegar o percentual de venda/cor/produto do produto para o cálculo
            GestaoDeProduto gradeGlobal = getAnaliseGlobalProduto(gradeLocal);

            if (this.getCalculaCor(gradeLocal.getCod_cor())) {
                //verifica se a cor está marcada para calular, se sim faz o calculo
                //senão atribui zero para o planejado
                if (gradeLocal.isCalculaGrade()) {
                    //@percentCalculo terá o valor da porcentagem que será utilizada no cálculo
                    Float percentCalculo = !tsPvenda.switchedOnProperty().get() //teste se não é por % venda
                            ? (this.getPerProdutoCor(gradeLocal.getCod_cor(), gradeLocal.getTam())).floatValue() / 100 //utiliza a % produto
                            : !tsPvendaCor.switchedOnProperty().get() //teste se não é por % de cor
                            ? ((Double) gradeGlobal.p_venda_tamProperty().get()).floatValue() / 100 //utiliza a % de venda
                            : ((this.getPerVendaCor(gradeLocal.getCod_cor())).floatValue() / 100) * (((Double) gradeGlobal.p_venda_tamcorProperty().get()).floatValue() / 100); //utiliza a % da cor
                    //calcular o percentual de venda da grade global sobre o % total de venda para a nova grade (Excel Fabrício)
                    percentCalculo = percentCalculo / percentVenda;

                    //calcula o simulado conforme opção de escolha
                    //simulado = ((((lote + saldoTotal) * "% selecionadaTam") - concentradoTam) + concentradoTam) - saldoTam
                    //na multiplicação do (lote + saldoTotal) * "% selecionadaTam" é feito um round() para pegar somente a parte inteira da multiplicação dos decimais
                    Float loteQtde = baseGrade.floatValue();
                    Float pencentCalculoBase = percentCalculo / basePorcentagem;
                    Integer simulacaoPlanejadoTam = Math.round(loteQtde * pencentCalculoBase);
                    Integer concentrado = gradeGlobal.concentradoProperty().get();
                    Integer saldoProducaoSimulacao = gradeGlobal.saldo_producao_calcProperty().get();
                    simulacaoPlanejado = ((simulacaoPlanejadoTam - concentrado)
                            + concentrado) - saldoProducaoSimulacao;
                    //verifica se a simulação resultou um planejamento negativo
                    //se for o caso, é atribuido o valor 0(zero) para a simulação e somado o negativo (transformando em positivo) a variável planejadosNegativos
                    //caso não for negativo, o valor do planejado do tamnho da grade no lote será o simulado.
                    if (simulacaoPlanejado < 0) {
                        gradeLocal.setCalculaGrade(false);
                        gradeLocal.planejadoProperty().set(0);
                        planejadosNegativos += simulacaoPlanejado * (-1);
                        percentNegativos -= percentCalculo;
                        saldoProducaoCalc -= gradeGlobal.getSaldo_producao_calc();
                    } else {
                        gradeLocal.planejadoProperty().set(simulacaoPlanejado);
                    }
                }
            } else {
                if (tsZerarCor.switchedOnProperty().get()) {
                    gradeLocal.planejadoProperty().set(0);
                }
            }
            //recalcula o saldo de produção para o cálculo incrementando o planejado na simulação para utilização no próximo lote. [inserido no calculo do multiplo]
            //gradeGlobal.saldo_producao_calcProperty().set(gradeGlobal.saldo_producao_calcProperty().get() + gradeLocal.planejadoProperty().get());
            //gradeGlobal.simulacaoProperty().set(gradeGlobal.simulacaoProperty().add(gradeLocal.planejadoProperty().get()).get());
            //incrementa a variável planejadoLote para ver o tamanho total do lote
            planejadoLote += gradeLocal.planejadoProperty().get();

        }
        if (planejadosNegativos > 0) {
            calculaGrade(qtdeLote, saldoProducaoCalc, percentNegativos, lote);
        }
    }

    /**
     * FXML METHODS
     */
    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        Stage main = (Stage) btnCancelar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void btnAddLoteOnAction(ActionEvent event) {
        try {
            SceneCreateNewPartController novoLote = new SceneCreateNewPartController(Modals.FXMLWindow.SceneCreateNewPart,
                    tblPlanejaLotes.getItems());
            if (novoLote.newParts != null) {
                novoLote.newParts.forEach(lote -> {
                    ObservableList<GestaoDeLoteGrade> gradeNovoLote = FXCollections.observableArrayList();
                    tblGradeGlobal.getItems().forEach(grade -> {
                        try {
                            gradeNovoLote.add(new GestaoDeLoteGrade(lote.getLote(), grade.getCor(), DAOFactory.getProdutoDAO().getDescCorProduto(grade.getCor()), grade.getTam(),
                                    Math.round(((Integer) lote.getPlanejado()).floatValue() * ((Double) grade.getP_prod_tam()).floatValue() / 100)));
                        } catch (SQLException ex) {
                            Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                    lote.gradeProperty().set(gradeNovoLote);
                    tblPlanejaLotes.getItems().add(lote);
                });
                Collections.sort(tblPlanejaLotes.getItems(), Comparator.comparing(GestaoDeLote::getLote));

                for (GestaoDeLote lote
                        : tblPlanejaLotes.getItems()) {
                    System.out.println(lote.getOf() + " = " + lote.getLote());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void tboxSimulacaoPlanejadoOnKeyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            GestaoDeLote selectedLote = tblPlanejaLotes.getSelectionModel().getSelectedItem();
            if (selectedLote == null) {
                GUIUtils.showMessage("Você deve selecionar o lote que deseja alterar o planejado.", Alert.AlertType.WARNING);
                return;
            }
            Integer novoPlanejadoLote = selectedLote.getPlanejado();
            selectedLote.planejado_origProperty().set(novoPlanejadoLote);
            Integer planejadoAnteriorLote = 0;
            for (GestaoDeLoteGrade grade : selectedLote.getGrade()) {
                if (this.getCalculaCor(grade.getCod_cor())) {
                    planejadoAnteriorLote += grade.getPlanejado();
                }
            }
            for (GestaoDeLoteGrade grade : selectedLote.getGrade()) {
                if (this.getCalculaCor(grade.getCod_cor())) {
                    grade.setPlanejado(Math.round(((Integer) grade.getPlanejado()).floatValue() / planejadoAnteriorLote.floatValue() * novoPlanejadoLote.floatValue()));
                } else if (!continuarCalculando) {
                    grade.setPlanejado(grade.getPlanejado_orig());
                }
            }

            this.btnAtualizaLotesOnAction(null);
        }
    }

    @FXML
    private void btnEditarGradeLoteOnAction(ActionEvent event) {
        if (tblPlanejaLotes.getSelectionModel().getSelectedItem().lockedProperty().get()) {
            tblPlanejaLotes.getSelectionModel().getSelectedItem().lockedProperty().set(false);
            tboxSimulacaoPlanejado.disableProperty().set(false);
            tsCalculoGlobal.disableProperty().set(false);
        } else {
            tblPlanejaLotes.getSelectionModel().getSelectedItem().lockedProperty().set(true);
            tboxSimulacaoPlanejado.disableProperty().set(true);
            tsCalculoGlobal.disableProperty().set(true);
            tsCalculoGlobal.switchedOnProperty().set(false);
        }
        imgLock.setImage(validationImage(tblPlanejaLotes.getSelectionModel().getSelectedItem()));
        tblPlanejaLotes.refresh();
    }

    private void btnZerarSimulacaoOnAction(ActionEvent event) {
        /**
         * Zerar o saldo de produção com a qtd_orig caso teste novamente um
         * outro cenário
         */
        tblGradeGlobal.getItems().forEach(gradeGlobal -> {
            gradeGlobal.saldo_planejProperty().set(gradeGlobal.saldo_planejProperty().get() + gradeGlobal.planej_ofProperty().get() - gradeGlobal.simulacaoProperty().get());
            gradeGlobal.saldo_producaoProperty().set(gradeGlobal.saldo_producao_origProperty().get());
            gradeGlobal.simulacaoProperty().set(0);
        });
        saldoProducao = somaSaldoProducaoGlobal();
    }

    @FXML
    private void btnAtualizaLotesOnAction(ActionEvent event) {
        this.calculeSomatoriosCoresGlobal();
        if (!continuarCalculando) {
            btnZerarSimulacaoOnAction(null);
        }
        this.limpaSimuladoGlobal();
        novoPlanejado = 0;
        Integer novaMeta = tsCalculoGlobal.switchedOnProperty().get()
                ? Integer.parseInt(tboxSimulacaoPlanejado.getText())
                : somaPlanejadoOrigLotes() + emProducaoGlobal + concentradoGlobal + produzidoGlobal + mostruarioGlobal;

        Integer baseCalculo = novaMeta - emProducaoGlobal - concentradoGlobal - produzidoGlobal - mostruarioGlobal;
        /*Acho que tem que tirar o mostruário também.*/

        /**
         * Zerar o saldo de produção de calculo com a qtd_orig caso teste
         * novamente um outro cenário - Zera a simulação do global para o novo
         * planejamento
         *
         * O valor do Saldo de Produção é calculado conforme configuração de
         * cálculo com mostruário ou não. Caso não é para contabilizar o
         * mostruário no cálculo, é subtraído o valor do saldo de produção
         */
        if (!continuarCalculando) {
            tblGradeGlobal.getItems().forEach(gradeGlobal -> {
                gradeGlobal.saldo_planejProperty().set(gradeGlobal.saldo_producao_origProperty().get() + gradeGlobal.planej_ofProperty().get());
                gradeGlobal.saldo_producao_calcProperty().set(tsCalculoComMost.switchedOnProperty().get()
                        ? gradeGlobal.saldo_producao_origProperty().get()
                        : gradeGlobal.saldo_producao_origProperty().get() - gradeGlobal.mostruarioProperty().get());
                gradeGlobal.simulacaoProperty().set(0);
            });
        }
        saldoProducao = somaSaldoProducaoGlobal();

        /**
         * Caso for global, disDtribuir entre os lotes o proporcional de cada
         * lote sobre o novo montante depois calcular lote a lote
         * individualmente.
         */
        if (tsCalculoGlobal.switchedOnProperty().get()) {
            tblPlanejaLotes.getItems().forEach(plano -> {
                Float porcentLoteNaMeta = (((Integer) plano.getPlanejado_orig()).floatValue() / planejadoGlobal.floatValue());
                plano.setPlanejado(Math.round(porcentLoteNaMeta * baseCalculo));
                plano.setPlanejado_orig(Math.round(porcentLoteNaMeta * baseCalculo));
            });
        }

        //Preparando variáveis para cálculo
        Float percentVendas = 1.0f;
        Integer planejadoLote = 0; //Variável que irá salvar o valor do planejado no lote
        for (GestaoDeLote lote : tblPlanejaLotes.getItems()) {
            //preparando a grade para o calculo da simulação
            lote.getGrade().forEach(grade -> grade.setCalculaGrade(true));
            //base de cálculo será o quanto foi planejado pelo lote mais saldo de produção.
            //no caso do primeiro lote será o saldo atual da produção, para os demais será o atual mais o planejado do lote anterior
            //no final do ciclo de todos os lotes deve ficar igual ao saldo planejado
            //baseGrade = lote.getPlanejado_orig() + saldoProducao;
            if (!lote.isLocked()) {
                calculaGrade(lote.getPlanejado(), saldoProducao, percentVendas, lote);
            }

            //Calcula a grade pelo multiplo configurado na tabela de somatórios
            int qtdePlanejadoMultiplo = 0;
            for (GestaoDeLoteGrade gradeLocal : lote.getGrade()) {
                GestaoDeProduto gradeGlobal = getAnaliseGlobalProduto(gradeLocal);

                //verificar se o lote não está com a opção LOCK true
                //caso o esteja locked não deve ser calculado o planejado para esse produto e o simulado irá ficar o planejado OF.
                if (!lote.lockedProperty().get()) {
                    Integer multiplo = this.getMultiploCor(gradeLocal.getCod_cor());
                    Integer planejMultiplo = Math.round(gradeLocal.planejadoProperty().floatValue() / multiplo.floatValue()) * multiplo;
                    gradeLocal.planejadoProperty().set(planejMultiplo);

                }
                qtdePlanejadoMultiplo += gradeLocal.planejadoProperty().get();

                //recalcula o saldo de produção para o cálculo incrementando o planejado na simulação para utilização no próximo lote.
                gradeGlobal.saldo_producao_calcProperty().set(gradeGlobal.saldo_producao_calcProperty().get() + gradeLocal.planejadoProperty().get());
                gradeGlobal.simulacaoProperty().set(gradeGlobal.simulacaoProperty().add(gradeLocal.planejadoProperty().get()).get());
            }
            planejadoLote = qtdePlanejadoMultiplo;

            lote.planejadoProperty().set(planejadoLote); //atualiza o total pelo planejado da grade na simulação
            saldoProducao = somaSaldoProducaoGlobal(); //recalcula o saldo total de produção para o cálculo.
            if (saldoProducao < 0) {
                //saldoProducao = saldoProducao * (-1); //passa o saldo para positivo caso seja negativo.
            }

            novoPlanejado += planejadoLote;
        }

        //Calcular o novo planejado global
        Integer saldoPlanejadoGlobal = 0;
        for (GestaoDeProduto gradeGlobal : tblGradeGlobal.getItems()) {
            gradeGlobal.saldo_planejProperty().set(gradeGlobal.getEstoque() + gradeGlobal.getMostruario() + gradeGlobal.getEm_producao() + gradeGlobal.getSimulacao()
                    - gradeGlobal.getCarteira() - gradeGlobal.getConcentrado_pend());
            saldoPlanejadoGlobal += gradeGlobal.saldo_planejProperty().get();
        }
        this.calculaPercSaldoPlanejado();

        tblGradeGlobal.refresh();
        tblPlanejaLotes.refresh();
        tblGradeLote.refresh();

        metaGlobal = novoPlanejado + mostruarioGlobal + produzidoGlobal + emProducaoGlobal;
        lbMeta.setText("" + metaGlobal);
        lbPrMeta.setText("" + (metaGlobal - Integer.parseInt(lbProjecao.getText())));
        lbGPlanejado.setText("" + (metaGlobal - mostruarioGlobal - produzidoGlobal - emProducaoGlobal));
        lbGSaldoPlanejado.setText("" + saldoPlanejadoGlobal);
        saldoLotear = projetadoRevisao - metaGlobal;
        lbSaldoLotear.setText("" + saldoLotear);

        if (tblSomatoriosCor.getItems().stream().filter(cor -> !cor.isSelected()).count() > 0) {
            continuarCalculando = GUIUtils.showQuestionMessageDefaultNao("Continuar calculando cores separadas?\n\n"
                    + "[NÃO] Próximo cálculo sem dados atualizados da tela.\n"
                    + "[SIM] Próximo cálculo irá utilizar os dados desta tela.");
        }
    }

    @FXML
    private void btnProgNegativosOnAction(ActionEvent event) {
        if (tblPlanejaLotes.getItems().size() == 0) {
            GUIUtils.showMessage("Você deve criar um lote para atender os negativos.", Alert.AlertType.INFORMATION);
            return;
        }

        GestaoDeLote lote = tblPlanejaLotes.getItems().get(0);
        totalNegativos = 0;
        lote.getGrade().forEach(grade -> {
            GestaoDeProduto gradeGlobal = this.getAnaliseGlobalProduto(grade);
            if (gradeGlobal.getSaldo_producao() < 0) {
                //Verificar aqui com o PCP um GAP de peças para atendenter o negativo
                //Confirmei com o Vanilson e ele passou que o negativo irá atender somente o negativo, sem nenhuma peça de gap.
                grade.setPlanejado(gradeGlobal.getSaldo_producao() * (-1));
                totalNegativos += grade.getPlanejado();
            } else {
                grade.setPlanejado(0);
            }
        });
        lote.setPlanejado(totalNegativos);
        for (int i = 1; i < tblPlanejaLotes.getItems().size(); i++) {
            GestaoDeLote loteZero = tblPlanejaLotes.getItems().get(i);
            loteZero.setPlanejado(0);
        }

    }

    @FXML
    private void btnConfirmarPlanoOnAction(ActionEvent event) {
        if (tblPlanejaLotes.getItems().size() <= 0) {
            return;
        }

        for (GestaoDeLote lote : tblPlanejaLotes.getItems()) {
            try {

                if (lote.getOf().equals("0")) {
                    ObservableList<GestaoDeLoteGrade> gradeSemCoresInativas = FXCollections.observableArrayList();
                    for (GestaoDeLoteGrade gradeLote : lote.getGrade()) {
                        if (this.getCalculaCor(gradeLote.getCod_cor())) {
                            gradeSemCoresInativas.add(gradeLote);
                        }
                    }

                    lote.setGrade(gradeSemCoresInativas);
                }

                String concatGrade = "";
                concatGrade = lote.getGrade().stream().map((grade) -> grade.getTam() + "," + grade.getCod_cor() + "," + grade.getPlanejado() + ";").reduce(concatGrade, String::concat);
//                final StringProperty concatGrade = new SimpleStringProperty("");
//                lote.getGrade().stream().collect(Collectors.groupingBy(GestaoDeLoteGrade::getCod_cor,Collectors.summingInt(GestaoDeLoteGrade::getPlanejado))).forEach((cor, qtde) -> {
////                    System.out.println(cor+" - "+qtde);
//                    if(qtde > 0){
//                       concatGrade.set(lote.getGrade().stream().filter(gradeLocal -> gradeLocal.getCod_cor().equals(cor)).map((grade) -> grade.getTam() + "," + grade.getCod_cor() + "," + grade.getPlanejado() + ";").reduce(concatGrade.get(), String::concat));
////                       System.out.println(concatGrade.get());
////                       lote.getGrade().stream().filter(gradeLocal -> gradeLocal.getCod_cor().equals(cor)).forEach(grade -> System.out.println(grade.getCod_cor() +":"+grade.getCor()+"-"+grade.getTam()+"-"+grade.getPlanejado()));
//                    }
//                });

                String codProduto = dadosProduto.get("clm5").toString();
                String numeroOf = lote.getOf();
                String numeroLote = lote.getLote();
                Integer planejadoLote = lote.getPlanejado();

                if (lote.isMovimentarSetor()) {
                    lote.setSetor("100");
                }
                if (planejadoLote > 0) {
                    if (numeroOf.equals("0")) {
                        numeroOf = daoGestao.callFunctionCreateOf(numeroLote, codProduto, concatGrade, SceneMainController.getAcesso().getUsuario());
                        DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Incluir", "Gestão de Produção", numeroOf,
                                "Criado OF: " + numeroOf + " no LOTE: " + numeroLote + " para a REFERÊNCIA: " + codProduto + " com " + planejadoLote + " peças.")); //<<log>>
                        lote.setOf(numeroOf);
                    } else {
                        daoGestao.callFunctionUpdateOf(numeroOf, codProduto, concatGrade, lote.getSetor(), SceneMainController.getAcesso().getUsuario());
                        DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Alterar", "Gestão de Produção", numeroOf,
                                "Alterado OF: " + numeroOf + " no LOTE: " + numeroLote + " da REFERÊNCIA: " + codProduto + " de " + lote.getPlanejado_orig() + " para " + planejadoLote + " peças.")); //<<log>>
                    }
                } else {
                    if (DAOFactory.getMatReserva001DAO().isReservaOf(numeroOf)) {
                        GUIUtils.showMessage("A OF " + numeroOf + " não pode ser excluída pois contém reserva de materiais.", Alert.AlertType.INFORMATION);
                    } else {

                        String retornoDelete = daoGestao.callFunctionDeleteOf(numeroOf, SceneMainController.getAcesso().getUsuario());
                        if (retornoDelete.equals("nok")) {
                            daoGestao.callFunctionUpdateOf(numeroOf, codProduto, concatGrade, lote.getSetor(), SceneMainController.getAcesso().getUsuario());
                            DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Alterar", "Gestão de Produção", numeroOf,
                                    "Alterado OF: " + numeroOf + " no LOTE: " + numeroLote + " da REFERÊNCIA: " + codProduto + " de " + lote.getPlanejado_orig() + " para " + planejadoLote + " peças.")); //<<log>>
                            continue;
                        }
                        DAOFactory.getLogDAO().saveLog(new Log(SceneMainController.getAcesso().getUsuario(), "Excluir", "Gestão de Produção", numeroOf,
                                "Excluido OF: " + numeroOf + " no LOTE: " + numeroLote + " da REFERÊNCIA: " + codProduto + " com " + lote.getPlanejado_orig() + " peças.")); //<<log>>
                    }
                }
                if (lote.isMovimentarSetor()) {
                    DAOFactory.getGestaoProducaoDAO().movimentaSetor99(lote, codigoProduto);
                    lote.setSetor("99");
                }


            } catch (SQLException ex) {
                Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
                return;
            }
        }

        changedPart = true;
        Stage main = (Stage) btnCancelar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void menuSelectAllOnAction(ActionEvent event) {
        tblSomatoriosCor.getItems().forEach(cor -> {
            cor.setSelected(!cor.isSelected());
        });
    }

    class EditingCellTblGradeLote extends TableCell<GestaoDeLoteGrade, Integer> {

        private TextField textField;

        public EditingCellTblGradeLote() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            GestaoDeLoteGrade gradeSelecionada = tblGradeLote.getSelectionModel().getSelectedItem();
            if (gradeSelecionada != null && getPerProdutoCor(gradeSelecionada.getCod_cor(), gradeSelecionada.getTam()) == 0) {
                if (!GUIUtils.showQuestionMessageDefaultNao("Você está colocando uma quantidade para uma grade de cor e tamanho que está com o percentual zerado no cadastro de produto.\n\n" +
                        "Deseja incluir mesmo assim a grade?")) {
                    gradeSelecionada.setPlanejado(0);
                    setText("0");
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                    return;
                }
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    class EditingCellTblLote extends TableCell<GestaoDeLote, String> {

        private TextField textField;

        public EditingCellTblLote() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void commitEdit(String newValue) {
            super.commitEdit(newValue);
            String novoLote = newValue;
            String numeroOf = tblPlanejaLotes.getSelectionModel().getSelectedItem().getOf();
            try {
                //System.out.println("Lote: " + novoLote + " OF: " + numeroOf);
                ObservableList<TabPrz001> lotes = DAOFactory.getTabPrz001DAO().getByTipoPrazo(novoLote);
                if (lotes.size() == 0) {
                    GUIUtils.showMessage("O lote " + novoLote + " não é um lote válido, digite um lote válido.", Alert.AlertType.ERROR);
                } else {
                    TabPrz001 loteSelecionado = lotes.stream().findAny().get();
                    DAOFactory.getGestaoProducaoDAO().updateLoteOf(numeroOf,
                            loteSelecionado.getPrazo(),
                            loteSelecionado.getDtInicio(),
                            loteSelecionado.getDtFim());
                    GUIUtils.showMessageNotification("Planejamento de OF", "Lote da OF " + numeroOf + " atualizado para " + novoLote + " com sucesso.");
                }
            } catch (SQLException ex) {
                Logger.getLogger(ScenePlanejamentoLoteController.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    class EditingCellTblSetorOf extends TableCell<GestaoDeLote, String> {

        private TextField textField;

        public EditingCellTblSetorOf() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            if (tblPlanejaLotes.getSelectionModel().getSelectedItem().getSetor().equals("99")) {
                textField.setDisable(true);
            } else {
                textField.setDisable(false);
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void commitEdit(String newValue) {
            super.commitEdit(newValue);
            String novoSetor = newValue;
            if (novoSetor.equals("99")) {
                GestaoDeLote gestaoOf = tblPlanejaLotes.getSelectionModel().getSelectedItem();
                //System.out.println("Lote: " + novoLote + " OF: " + numeroOf);
                //Movimento da OF no faccao e faccao3
                gestaoOf.setMovimentarSetor(true);
            }
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    class EditingCellTblSomatorio extends TableCell<SomatorioCoresGestao, Integer> {

        private TextField textField;

        public EditingCellTblSomatorio() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

}
