/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Log;
import sysdeliz2.models.SdProduto001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProdutoObservacaoPcpController extends Modals implements Initializable {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    public final SdProduto001 produto = new SdProduto001();

    @FXML
    private Button btnSalvar;
    @FXML
    private Button btnFechar;
    @FXML
    private TextField tboxCodigo;
    @FXML
    private TextField tboxDescricao;
    @FXML
    private TextField tboxColecao;
    @FXML
    private TextArea tboxObservacaoPcp;

    public SceneProdutoObservacaoPcpController(FXMLWindow file, ObservableList<SpreadsheetCell> row) throws IOException {
        super(file);
        this.codigo.set(row.get(4).getText());
        this.descricao.set(row.get(5).getText());
        this.colecao.set(row.get(1).getText());
        super.show(this);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TextFieldUtils.upperCase(tboxObservacaoPcp);
        try {
            tboxCodigo.textProperty().bind(codigo);
            tboxDescricao.textProperty().bind(descricao);
            tboxColecao.textProperty().bind(colecao);

            produto.copy(DAOFactory.getSdProduto001DAO().getByCodigo(codigo.get()));
            tboxObservacaoPcp.textProperty().bindBidirectional(produto.observacaoPcpProperty());
        } catch (SQLException ex) {
            Logger.getLogger(SceneProdutoObservacaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        try {
            DAOFactory.getSdProduto001DAO().update(produto);
            DAOFactory.getLogDAO().saveLog(new Log(
                    SceneMainController.getAcesso().getUsuario(),
                    Log.TipoAcao.EDITAR,
                    "Gestão Produto",
                    produto.getCodigo(),
                    "Alterado a observação do PCP para o produto " + produto.getCodigo() + " para " + produto.getObservacaoPcp()));
            GUIUtils.showMessageNotification("Gestão de Produção", "Observação ao produto salva com sucesso!");
        } catch (SQLException ex) {
            Logger.getLogger(SceneProdutoObservacaoPcpController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnFecharOnAction(ActionEvent event) {
        tboxCodigo.textProperty().unbind();
        tboxDescricao.textProperty().unbind();
        tboxColecao.textProperty().unbind();
        tboxObservacaoPcp.textProperty().unbindBidirectional(produto.observacaoPcpProperty());

        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }
}
