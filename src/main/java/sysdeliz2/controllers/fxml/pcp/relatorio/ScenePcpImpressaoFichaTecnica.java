package sysdeliz2.controllers.fxml.pcp.relatorio;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.controlsfx.control.ToggleSwitch;
import sysdeliz2.controllers.views.procura.FilterOrdemProducaoView;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.properties.OfGrade;
import sysdeliz2.models.properties.OfGradeItem;
import sysdeliz2.models.sysdeliz.SdRoteiroOrganize001;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.ti.PcpFtOf;
import sysdeliz2.models.view.VSdBidCidadesRep;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ImageUtils;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author lima.joao
 * @since 09/09/2019 15:09
 */
public class ScenePcpImpressaoFichaTecnica implements Initializable {

    //<editor-fold desc="Variáveis FXML">
    @FXML
    public ImageView imgFichaTecnica;
    @FXML
    public ToggleSwitch cboxImprimirOF;
    @FXML
    public ToggleSwitch cboxImprimirFichaTecnica;
    @FXML
    public ToggleSwitch cboxImprimirTabelaMedida;
    @FXML
    public ToggleSwitch cboxImprimirOutrosArquivos;
    @FXML
    public ToggleSwitch cboxImprimirRoteiro;
    @FXML
    public VBox boxImpRoteiro;
    @FXML
    public TableColumn<OfGrade, String> cl1;
    @FXML
    public TableColumn<OfGrade, String> cl2;
    @FXML
    public TableColumn<OfGrade, String> cl3;
    @FXML
    public TableColumn<OfGrade, String> cl4;
    @FXML
    public TableColumn<OfGrade, String> cl5;
    @FXML
    public TableColumn<OfGrade, String> cl6;
    @FXML
    public TableColumn<OfGrade, String> cl7;
    @FXML
    public TableColumn<OfGrade, String> cl8;
    @FXML
    public TableColumn<OfGrade, String> cl9;
    @FXML
    public TableColumn<OfGrade, String> cl10;
    @FXML
    public TableColumn<OfGrade, String> cl11;
    @FXML
    public TableColumn<OfGrade, String> cl12;
    @FXML
    public TableColumn<OfGrade, String> cl13;
    @FXML
    public TableView<OfGrade> tblGrade;
    @FXML
    public TableColumn<String, String> clnAcoes;
    @FXML
    public TableColumn<Of1, String> clnOF;
    @FXML
    public TableColumn<Of1, String> clnPeriodo;
    @FXML
    public TableColumn<Of1, String> clnProdutoCodigo;
    @FXML
    public TableColumn<Of1, String> clnProdutoNome;
    @FXML
    public TableColumn<Of1, String> clnColecao;
    @FXML
    public TableColumn<Of1, String> clnMarca;
    @FXML
    public TableView<Of1> tblImprimir;
    @FXML
    public ComboBox<String> forcarColecao;
    @FXML
    public VBox boxForcarColecao;
    //</editor-fold>

    private ObservableList<Of1> listaOF = FXCollections.observableArrayList();

    private Of1 selectedOF1;
    private String colecaoUsar = "";

    private String pathFichaTecnica = "";

    private List<String> filesFichaTecnica = null;
    private List<String> filesExtras = null;
    private List<String> filesTabelaMedida = null;

    private List<String> janelaBuscaSetores() {
        AtomicReference<List<String>> setores = new AtomicReference<>(new ArrayList<>());

        new Fragment().show(frag -> {
            frag.size(400.0, 250.0);
            frag.title.setText("Escolha de setor");
            List<SdRoteiroOrganize001> roteiros = (List<SdRoteiroOrganize001>) new FluentDao().selectFrom(SdRoteiroOrganize001.class).where(it -> it.equal("produto.codigo", selectedOF1.getProduto().getCodigo())).resultList();
            final FormTableView<SdRoteiroOrganize001> tblRoteiros = FormTableView.create(SdRoteiroOrganize001.class, table -> {
                table.expanded();
                table.title("SETORES");
                table.editable.set(true);

                table.items.set(FXCollections.observableArrayList(roteiros));
                table.selectColumn();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Setor");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdSetor()));
                        }).build(), /*Setor*/
                        FormTableColumn.create(cln -> {
                            cln.title("Descrição");
                            cln.width(200);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescSetor()));
                        }).build()/*Setor*/
                );
            });
            Button btnConfirmar = FormButton.create(btn -> {
                btn.setText("Confirmar");
                btn.addStyle("success");
                btn.icon(sysdeliz2.utils.sys.ImageUtils.getIcon(sysdeliz2.utils.sys.ImageUtils.Icon.CONFIRMAR, sysdeliz2.utils.sys.ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    List<SdRoteiroOrganize001> list = tblRoteiros.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                    if (list.size() == 0) {
                        return;
                    }
                    setores.set(list.stream().map(SdRoteiroOrganize001::getSdSetor).collect(Collectors.toList()));
                    frag.close();
                });
            });
            frag.buttonsBox.getChildren().add(btnConfirmar);
            frag.box.getChildren().add(tblRoteiros.build());
        });
        return setores.get();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblImprimir.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectedOF1 = newValue;
                setDadosnaTela();
            }
        });

        clnOF.setCellValueFactory(param -> param.getValue().numeroProperty());
        clnPeriodo.setCellValueFactory(param -> param.getValue().periodoProperty());
        clnProdutoCodigo.setCellValueFactory(param -> param.getValue().getProduto().codigoProperty());
        clnProdutoNome.setCellValueFactory(param -> param.getValue().getProduto().descricaoProperty());
        clnColecao.setCellValueFactory(param -> param.getValue().getProduto().getColecao().codigoProperty());
        clnMarca.setCellValueFactory(param -> param.getValue().getProduto().getMarca().descricaoProperty());

        final FormFieldSingleFind<Colecao> forcarColecaoField = FormFieldSingleFind.create(Colecao.class, field -> {
            field.title("Forçar Coleção");
            field.setDefaultCode("17CO");
        });
        boxForcarColecao.getChildren().add(forcarColecaoField.build());
        if (forcarColecaoField.value.get() != null) {
            colecaoUsar = forcarColecaoField.value.get().getCodigo();
            forcarColecaoField.postSelected((observable, oldValue, newValue) -> {
                colecaoUsar = forcarColecaoField.value.get().getCodigo();
                setDadosnaTela();
            });
        }

//        forcarColecao.setItems(FXCollections.observableArrayList("17CO - Continua", "1906 - SS20", "1909 - S20", "2002 - W20", "2006 - SS21", "2009 - S21", "2102 - W21", "2106 - SS22", "2109 - S22"));
//        colecaoUsar = "";
//        forcarColecao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//            if (tblImprimir.getItems().size() == 0)
//                MessageBox.create(message -> {
//                    message.message("Você deve primeiro adicionar as OFs para impressão.");
//                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                    message.showAndWait();
//                });
//            else {
//                colecaoUsar = newValue.substring(0, 4);
//                setDadosnaTela();
//            }
//
//        });
    }

    @FXML
    @SuppressWarnings("unused")
    public void btnProcurarOrdemProducaoOnAction(ActionEvent actionEvent) {
        FilterOrdemProducaoView filterOrdemProducaoView = new FilterOrdemProducaoView();
        filterOrdemProducaoView.show(true);
        selectedOF1 = filterOrdemProducaoView.itensSelecionados.get(0);

        setDadosnaTela();
    }

    private void ocultaColunas() {
        cl2.setVisible(false);
        cl3.setVisible(false);
        cl4.setVisible(false);
        cl5.setVisible(false);
        cl6.setVisible(false);
        cl7.setVisible(false);
        cl8.setVisible(false);
        cl9.setVisible(false);
        cl10.setVisible(false);
        cl11.setVisible(false);
        cl12.setVisible(false);
        cl13.setVisible(false);
    }

    private void montaColunaGrade(TableColumn<OfGrade, String> coluna, int posicao) {
        coluna.setVisible(true);
        coluna.setCellValueFactory(param -> {
            coluna.setText(param.getValue().getOfGradeItens().get(posicao).getTamanho());

            double qtd = param.getValue().getOfGradeItens().get(posicao).getQtde().doubleValue() + param.getValue().getOfGradeItens().get(posicao).getQtdeB().doubleValue();

            return new SimpleStringProperty(String.valueOf(qtd));
        });
    }

    private void setDadosnaTela() {
        // Procura a Ficha Técnica
        try {
            buildPath(true);
            buildPath(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!pathFichaTecnica.equals("") && new File(pathFichaTecnica).exists()) {
            imgFichaTecnica.setImage(new Image("file://///" + pathFichaTecnica));
        } else {
            imgFichaTecnica.setImage(null);
        }

        // Remove os sem quantidade
        ObservableList<OfGrade> list = FXCollections.observableArrayList();

        boolean usar = false;
        for (OfGrade ofGrade : selectedOF1.getGrade()) {
            for (OfGradeItem ofGradeIten : ofGrade.getOfGradeItens()) {
                if ((ofGradeIten.getQtde().doubleValue() + ofGradeIten.getQtdeB().doubleValue() + ofGradeIten.getQtdeC().doubleValue()) > 0) {
                    usar = true;
                }
            }
            if (usar) {
                list.add(ofGrade);
            }
        }

        tblGrade.getItems().clear();
        tblGrade.setItems(list);

        // Exibe informacoes no grid;
        cl1.setText("Cor");
        cl1.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCor() + " - " + param.getValue().getNomeCor()));

        ocultaColunas();

        int count = selectedOF1.getGrade().get(0).getOfGradeItens().size();
        for (int i = 0; i < count; i++) {
            switch (i) {
                case 0:
                    montaColunaGrade(cl2, i);
                    break;
                case 1:
                    montaColunaGrade(cl3, i);
                    break;
                case 2:
                    montaColunaGrade(cl4, i);
                    break;
                case 3:
                    montaColunaGrade(cl5, i);
                    break;
                case 4:
                    montaColunaGrade(cl6, i);
                    break;
                case 5:
                    montaColunaGrade(cl7, i);
                    break;
                case 6:
                    montaColunaGrade(cl8, i);
                    break;
                case 7:
                    montaColunaGrade(cl9, i);
                    break;
                case 8:
                    montaColunaGrade(cl10, i);
                    break;
                case 9:
                    montaColunaGrade(cl11, i);
                    break;
                case 10:
                    montaColunaGrade(cl12, i);
                    break;
                case 11:
                    montaColunaGrade(cl13, i);
                    break;
            }
        }
    }

    private void buildPath(boolean isFichaTecnica) throws SQLException {
        String pathBase = "K:/";
        String pathBaseFichaTecnica = "FichaTecnica/";
        String pathBaseTabelaMedida = "Tabela de Medidas/";

        if (isFichaTecnica) {
            pathFichaTecnica = "";
            String base = pathBase + pathBaseFichaTecnica;
            base += ImageUtils.getFolderMarca(selectedOF1.getProduto().getMarca());

            File folder = new File(base);
            String[] folders = folder.list();

            if (folders != null) {
                for (String s : folders) {
                    if (!selectedOF1.getProduto().getColecao().getCodigo().equals("17CO")) {
                        colecaoUsar = selectedOF1.getProduto().getColecao().getCodigo();
                    }

                    if (s.startsWith(colecaoUsar)) {
                        base += s + "/";
                        break;
                    }
                }

                File folderColecao = new File(base);
                String[] filesColecao = folderColecao.list();
                if (filesFichaTecnica == null) {
                    filesFichaTecnica = new ArrayList<>();
                } else {
                    filesFichaTecnica.clear();
                }
                if (filesExtras == null) {
                    filesExtras = new ArrayList<>();
                } else {
                    filesExtras.clear();
                }
                assert filesColecao != null;
                for (String s : filesColecao) {
                    String tmp = s.toUpperCase();
                    String[] arquivo = tmp.split("_");

                    if (arquivo[0].equals(selectedOF1.getProduto().getCodigo())) {
                        if (arquivo[arquivo.length - 1].toUpperCase().equals("FT.JPG") || arquivo[arquivo.length - 1].toUpperCase().equals("FT.JPEG")) {
                            if (arquivo.length > 2) {
                                if (selectedOF1.getItens().stream().anyMatch(it -> it.getId().getCor().getCor().equals(arquivo[1]))) {
                                    filesFichaTecnica.add(base + tmp);
                                    if (new File(base + tmp).exists()) {
                                        pathFichaTecnica = base + tmp;
                                    }
                                }
                            } else {
                                filesFichaTecnica.add(base + tmp);
                                if (new File(base + tmp).exists()) {
                                    pathFichaTecnica = base + tmp;
                                }
                            }
                        } else {
                            filesExtras.add(base + tmp);
                        }
                    }

//                    if ((tmp.startsWith(selectedOF1.getProduto().getCodigo() + ".") || tmp.startsWith(selectedOF1.getProduto().getCodigo() + "_")) &&
//                            (!tmp.equals(selectedOF1.getProduto().getCodigo() + "_FT.JPG"))) {
//                        filesExtras.add(base + tmp);
//                    }
                }

//                base += selectedOF1.getProduto().getCodigo() + "_FT.JPG";
//                if (new File(base).exists()) {
//                    pathFichaTecnica = base;
//                }
            }
        } else {
            if (filesTabelaMedida == null) {
                filesTabelaMedida = new ArrayList<>();
            } else {
                filesTabelaMedida.clear();
            }

            String base = pathBase + pathBaseTabelaMedida;
            base += ImageUtils.getFolderMarca(selectedOF1.getProduto().getMarca());

            File folder = new File(base);
            String[] folders = folder.list();

            if (folders != null) {

                if (!selectedOF1.getProduto().getColecao().getCodigo().equals("17CO")) {
                    colecaoUsar = selectedOF1.getProduto().getColecao().getCodigo();
                }

                for (String s : folders) {
                    if (s.startsWith(colecaoUsar)) {
                        base += s + "/";
                        break;
                    }
                }

                File folderColecao = new File(base);
                String[] filesColecao = folderColecao.list();
                String sufixo = "P";
                String sufixo_jeans = "3";

                if (filesColecao != null) {
                    GenericDao<PcpFtOf> pcpFtOfGenericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), PcpFtOf.class);

                    if (selectedOF1.getPeriodo().equals("M")) {
                        sufixo = "M";
                        sufixo_jeans = "2";
                    }

                    for (String s : filesColecao) {
                        if (s.startsWith(selectedOF1.getProduto().getCodigo() + ".") ||
                                s.startsWith(selectedOF1.getProduto().getCodigo() + "_" + sufixo) ||
                                s.startsWith(selectedOF1.getProduto().getCodigo() + "_" + sufixo_jeans)
                        ) {
                            String[] frag = s.split("_");
                            if (frag.length <= 2) {
//                                if(sufixo.equals("") && (!s.contains("_M") && !s.contains("_2"))) {
                                filesTabelaMedida.add(base + s);
//                                } else {
//                                    if(!sufixo.equals("") && ((s.contains("_M") || s.contains("_2")))){
//                                        filesTabelaMedida.add(base + s);
//                                    }
//                                }
                            } else if (frag.length > 3) {
                                ObservableList pcpFtOfList = pcpFtOfGenericDao
                                        .initCriteria()
                                        .addPredicateEqPkEmbedded("pcpFtOfID", "numero", selectedOF1.getNumero(), false)
                                        .loadListByPredicate();

                                boolean tabMedInsumo = false;
                                for (Object pcpFtOf : pcpFtOfList) {
                                    if (s.contains(((PcpFtOf) pcpFtOf).getPcpFtOfID().getInsumo())) {
                                        boolean existe = false;
                                        for (String s1 : filesTabelaMedida) {
                                            existe = s1.equals(base + s);
                                        }

                                        if (!existe) {
                                            filesTabelaMedida.add(base + s);
                                            tabMedInsumo = true;
                                        }
                                    }
                                }
                            } else {
                                /**
                                 * Incluído este trecho pois a modelagem precisa salvar uma tabela de medida para Antes Lavagem e Depois Lavagem
                                 */
                                if ((s.endsWith("_AL.jpg") || s.endsWith("_PL.jpg")))
                                    filesTabelaMedida.add(base + s);
                            }
                        }
                    }
                }
            }
        }
    }

    @FXML
    @SuppressWarnings("unused")
    public void btnImprimirOnAction(ActionEvent actionEvent) {
        try {
            if (listaOF.size() <= 0 || selectedOF1.getNumero().equals("")) {
                GUIUtils.showMessage("Nenhuma OF Selecionada.", Alert.AlertType.INFORMATION);
                return;
            }

            List<JasperPrint> layoutsParaImprimir = new ArrayList<>();

            tblImprimir.getSelectionModel().selectFirst();
            for (Of1 of1 : tblImprimir.getItems()) {
                selectedOF1 = of1;
                setDadosnaTela();
                try {
                    if (cboxImprimirOF.isSelected()) {
                        Map<String, Object> parametros = new HashMap<>();

                        String qrCode = "C:\\SysDelizLocal\\local_files\\" + of1.getNumero() + ".JPG";
                        ImageUtils.buildQRCode(of1.getNumero(), 300, 300, qrCode);

                        JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/ImpressaoOF.jasper"));

                        parametros.put("NUMERO_OF1", of1.getNumero());
                        parametros.put("QR_CODE", qrCode);

                        Connection connection = ConnectionFactory.getEmpresaConnection();
                        layoutsParaImprimir.add(JasperFillManager.fillReport(jr, parametros, connection));
                        connection.close();
                    }

                    if (cboxImprimirFichaTecnica.isSelected()) {
                        for (String pathFt : filesFichaTecnica) {
                            Map<String, Object> parametros = new HashMap<>();

                            JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/ImpressaoFT.jasper"));

                            parametros.put("P_REFERENCIA", of1.getProduto().getCodigo());
                            parametros.put("MARCA", of1.getProduto().getMarca().getDescricao());
                            parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());

                            if (!pathFichaTecnica.equals("") && new File(pathFt).exists()) {
                                parametros.put("P_IMAGEM", pathFt);
                            } else {
                                parametros.put("P_IMAGEM", "");
                            }
                            Connection connection = ConnectionFactory.getEmpresaConnection();
                            layoutsParaImprimir.add(JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection()));
                        }

                        Map<String, Object> parametros2 = new HashMap<>();

                        JasperReport jr2 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/ImpressaoConsumoFT.jasper"));

                        parametros2.put("P_NUMERO_OF", of1.getNumero());

                        Connection connection = ConnectionFactory.getEmpresaConnection();
                        layoutsParaImprimir.add(JasperFillManager.fillReport(jr2, parametros2, connection));
                        connection.close();
                    }

                    if (cboxImprimirTabelaMedida.isSelected()) {
                        for (String s : filesTabelaMedida) {
                            Image img = new Image("file://////" + s);
                            BufferedImage bmf = SwingFXUtils.fromFXImage(img, null);

                            if (bmf != null) {
                                if (bmf.getType() != BufferedImage.TYPE_BYTE_GRAY) {
                                    bmf = ImageUtils.image2GrayScale(bmf);
                                    ImageIO.write(bmf, "jpg", new File(s));
                                }

                                Map<String, Object> parametros = new HashMap<>();
                                JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/ImpressaoTabelaMedida.jasper"));
                                parametros.put("P_IMAGEM", s);
                                Connection connection = ConnectionFactory.getEmpresaConnection();
                                layoutsParaImprimir.add(JasperFillManager.fillReport(jr, parametros, connection));
                                connection.close();
                            }
                        }
                    }

                    if (cboxImprimirOutrosArquivos.isSelected()) {
                        if (filesExtras.size() > 0) {
                            for (String s : filesExtras) {
                                if (new File(s).exists()) {
                                    Map<String, Object> parametros = new HashMap<>();
                                    JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/ImpressaoTabelaMedida.jasper"));
                                    parametros.put("P_IMAGEM", s);
                                    Connection connection = ConnectionFactory.getEmpresaConnection();
                                    layoutsParaImprimir.add(JasperFillManager.fillReport(jr, parametros, connection));
                                    connection.close();
                                }
                            }
                        }
                    }

                    if (cboxImprimirRoteiro.isSelected()) {

                        Map<String, Object> parametros = new HashMap<>();
                        JasperReport jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/relatorios/RoteiroProducaoProduto.jasper"));
                        List<SdRoteiroOrganize001> roteiros = (List<SdRoteiroOrganize001>) new FluentDao().selectFrom(SdRoteiroOrganize001.class)
                                .where(it -> it
                                        .equal("produto.codigo", of1.getProduto().getCodigo()))
                                .resultList();

                        ListProperty<SdRoteiroOrganize001> roteirosBean = new SimpleListProperty(FXCollections.observableArrayList(roteiros));
                        AtomicReference<List<SdRoteiroOrganize001>> roteirosSelecionados = new AtomicReference<>(new ArrayList<>());

                        if (roteiros.size() == 0) continue;

                        if (roteiros.size() == 1) {
                            roteirosSelecionados.get().add(roteiros.get(0));
                        } else {
                            new Fragment().show(fg -> {

                                final FormButton btnConfirm = FormButton.create(btn -> {
                                    btn.icon(sysdeliz2.utils.sys.ImageUtils.getIcon(sysdeliz2.utils.sys.ImageUtils.Icon.CONFIRMAR, sysdeliz2.utils.sys.ImageUtils.IconSize._24));
                                    btn.addStyle("success");
                                    btn.title("Confirmar");
                                });

                                final FormTableView<SdRoteiroOrganize001> tblRoteiros = FormTableView.create(SdRoteiroOrganize001.class, table -> {
                                    table.title("Roteiros");
                                    table.expanded();
                                    table.items.bind(roteirosBean);
                                    table.selectColumn();
                                    table.editable.set(true);
                                    table.columns(
                                            FormTableColumn.create(cln -> {
                                                cln.title("Código");
                                                cln.width(90);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodorg()));
                                            }).build(),
                                            FormTableColumn.create(cln -> {
                                                cln.title("Referência");
                                                cln.width(90);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReferencia()));
                                            }).build(),
                                            FormTableColumn.create(cln -> {
                                                cln.title("Descrição");
                                                cln.width(550);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                                            }).build(),
                                            FormTableColumn.create(cln -> {
                                                cln.title("Percentual");
                                                cln.width(100);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPercentual()));
                                            }).build(),
                                            FormTableColumn.create(cln -> {
                                                cln.title("Setor");
                                                cln.width(200);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescSetor()));
                                            }).build(),
                                            FormTableColumn.create(cln -> {
                                                cln.title("Data Inclusão");
                                                cln.width(130);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdRoteiroOrganize001, SdRoteiroOrganize001>, ObservableValue<SdRoteiroOrganize001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtInclusao()));
                                            }).build()
                                    );
                                });
                                fg.size(1500.0, 700.0);
                                fg.title("Selecione o roteiro para impressão");
                                fg.box.getChildren().add(tblRoteiros.build());

                                btnConfirm.setOnAction(evt -> {
                                    List<SdRoteiroOrganize001> selecionados = tblRoteiros.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                                    if (selecionados.size() == 0) return;
                                    roteirosSelecionados.get().addAll(selecionados);
                                    fg.close();
                                });
                                fg.buttonsBox.getChildren().add(btnConfirm);
                            });
                        }

                        if (roteirosSelecionados.get().size() > 0) {
                            for (SdRoteiroOrganize001 roteiro : roteirosSelecionados.get()) {
                                parametros.put("ROTEIRO", roteiro.getCodorg());
                                parametros.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());
                                Connection connection = ConnectionFactory.getEmpresaConnection();
                                layoutsParaImprimir.add(JasperFillManager.fillReport(jr, parametros, connection));
                                connection.close();
                            }

                        }
                    }
                } catch (JRException | SQLException | IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }

            if (layoutsParaImprimir.size() > 0) {
                //layoutsParaImprimir.forEach(it -> new PrintReportPreview().showReport(it));

                ReportUtils.jasperToPdf(layoutsParaImprimir, "TempImpOF");
            }
        } catch (JRException | IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    public void btnAdicionarOnAction(ActionEvent actionEvent) {
        FilterOrdemProducaoView filterOrdemProducaoView = new FilterOrdemProducaoView();
        filterOrdemProducaoView.show(false);
        listaOF.setAll(filterOrdemProducaoView.itensSelecionados);
        tblImprimir.setItems(listaOF);
        tblImprimir.getSelectionModel().selectFirst();
    }

    @FXML
    public void btnLimparOnAction(ActionEvent actionEvent) {
        listaOF.clear();
    }
}
