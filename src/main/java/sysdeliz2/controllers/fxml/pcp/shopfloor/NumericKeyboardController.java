/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.MaskTextField;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class NumericKeyboardController extends Modals implements Initializable {
    
    public enum KeyboardType {
        TIME,
        INTEGER,
        DECIMAL
    }
    
    @FXML
    private TextField tboxViewNumber;
    private KeyboardType tipoTeclado = null;
    private String valorInicial = null;
    
    public final ObjectProperty<LocalTime> timeKeyboard = new SimpleObjectProperty<>();
    public final DoubleProperty doubleKeyboard = new SimpleDoubleProperty();
    public final IntegerProperty integerKeyboard = new SimpleIntegerProperty();
    
    public NumericKeyboardController(FXMLWindow file, KeyboardType tipoTeclado) throws IOException {
        super(file);
        this.tipoTeclado = tipoTeclado;
        
        super.show(this);
    }
    
    public NumericKeyboardController(FXMLWindow file, KeyboardType tipoTeclado, String valorInicial) throws IOException {
        super(file);
        this.tipoTeclado = tipoTeclado;
        this.valorInicial = valorInicial;
        
        super.show(this);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(this.valorInicial == null) {
            tboxViewNumber.clear();
        }else{
            tboxViewNumber.setText(valorInicial);
        }
        if (this.tipoTeclado == KeyboardType.TIME) {
            MaskTextField.timeField(tboxViewNumber);
        } else if (this.tipoTeclado == KeyboardType.DECIMAL) {
            MaskTextField.numericDotField(tboxViewNumber);
        } else {
            MaskTextField.numericField(tboxViewNumber);
        }
    }
    
    @FXML
    private void btnOneOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("1"));
    }
    
    @FXML
    private void btnTwoOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("2"));
    }
    
    @FXML
    private void btnThreeOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("3"));
    }
    
    @FXML
    private void btnFourOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("4"));
    }
    
    @FXML
    private void btnFiveOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("5"));
    }
    
    @FXML
    private void btnSixOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("6"));
    }
    
    @FXML
    private void btnSevenOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("7"));
    }
    
    @FXML
    private void btnEightOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("8"));
    }
    
    @FXML
    private void btnNineOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("9"));
    }
    
    @FXML
    private void btnDotOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat(","));
    }
    
    @FXML
    private void btnZeroOnAction(ActionEvent event) {
        tboxViewNumber.setText(tboxViewNumber.getText().concat("0"));
    }
    
    @FXML
    private void btnOkOnAction(ActionEvent event) {
        try {
            
            if (this.tipoTeclado == KeyboardType.TIME) {
                timeKeyboard.set(LocalTime.parse(tboxViewNumber.getText(), DateTimeFormatter.ofPattern("HH:mm")));
            } else if (this.tipoTeclado == KeyboardType.DECIMAL) {
                doubleKeyboard.set(Double.parseDouble(tboxViewNumber.getText().replace(",", ".")));
            } else {
                integerKeyboard.set(Integer.parseInt(tboxViewNumber.getText()));
            }
            
            Stage main = (Stage) tboxViewNumber.getScene().getWindow();
            main.close();
        } catch (NumberFormatException | DateTimeParseException ex) {
            GUIUtils.showMessage("O valor digitado não está no padrão do campo desejado, verifique o valor digitado!");
        }
    }
    
    @FXML
    private void btnBackspaceOnAction(ActionEvent event) {
        if(tboxViewNumber.getLength() > 0) {
            tboxViewNumber.setText(tboxViewNumber.getText().substring(0, tboxViewNumber.getText().length() - 1));
        }
    }
    
}
