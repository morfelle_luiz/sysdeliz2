/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.application.Platform;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdTiposParada001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneCadastraHorarioLancamentoController extends Modals implements Initializable {
    
    public enum TipoCadastro {
        PARADA,
        RETORNO,
        PRODUCAO_MANUAL,
        QTDE_ONLY,
        PRIMEIRO_LANCAMENTO
    }
    
    public final MapProperty<String, Object> returnValues = new SimpleMapProperty<>(FXCollections.observableHashMap());
    private GenericDao<SdTiposParada001> daoTiposParada = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdTiposParada001.class);
    
    private TipoCadastro tipoCadastro = null;
    
    // <editor-fold defaultstate="collapsed" desc="FXML Components">
    @FXML
    private Button btnCancelar;
    @FXML
    private Button btnSalvar;
    @FXML
    private TextField tboxHoraInicio;
    @FXML
    private Button btnTecladoHoraInicio;
    @FXML
    private TextField tboxHoraFim;
    @FXML
    private Button btnTecladoHoraFim;
    @FXML
    private TextField tboxQtde;
    @FXML
    private Button btnTecladoQuantidade;
    @FXML
    private TextField tboxMotivo;
    @FXML
    private Button btnTecladoMotivo;
    //</editor-fold>
    
    public SceneCadastraHorarioLancamentoController(FXMLWindow file, TipoCadastro tipoCadastro) throws IOException {
        super(file);
        this.tipoCadastro = tipoCadastro;
        super.show(this);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (tipoCadastro == TipoCadastro.PARADA) {
            btnTecladoQuantidade.setDisable(true);
            btnTecladoHoraFim.setDisable(true);
            btnTecladoHoraInicio.setDisable(true);
            tboxHoraInicio.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
            tboxMotivo.clear();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tboxMotivo.requestFocus();
                }
            });
        } else if (tipoCadastro == TipoCadastro.RETORNO) {
            btnTecladoQuantidade.setDisable(true);
            btnTecladoMotivo.setDisable(true);
            btnTecladoHoraInicio.setDisable(true);
            tboxHoraFim.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
            tboxMotivo.setEditable(false);
        } else if(tipoCadastro == TipoCadastro.QTDE_ONLY){
            btnTecladoMotivo.setDisable(true);
            btnTecladoHoraFim.setDisable(true);
            btnTecladoHoraInicio.setDisable(true);
        } else if(tipoCadastro == TipoCadastro.PRIMEIRO_LANCAMENTO) {
            btnTecladoQuantidade.setDisable(true);
            btnTecladoHoraFim.setDisable(true);
            btnTecladoHoraInicio.setDisable(false);
            tboxHoraInicio.setText(LocalTime.of(8,20).format(DateTimeFormatter.ofPattern("HH:mm")));
            tboxMotivo.setDisable(false);
        }else{
            btnTecladoMotivo.setDisable(true);
        }
    }
    
    @FXML
    private void btnCancelarOnAction(ActionEvent event) {
        Stage main = (Stage) btnCancelar.getScene().getWindow();
        main.close();
    }
    
    @FXML
    private void btnSalvarOnAction(ActionEvent event) {
        if(tipoCadastro == TipoCadastro.PRODUCAO_MANUAL && tboxHoraInicio.getText().isEmpty()){
            GUIUtils.showMessage("Você deve informar a hora de início do lançamento!");
            return;
        }
        if(tipoCadastro == TipoCadastro.PRODUCAO_MANUAL && tboxHoraFim.getText().isEmpty()){
            GUIUtils.showMessage("Você deve informar a hora de fim do lançamento!");
            return;
        }
        if(tipoCadastro == TipoCadastro.PARADA && tboxMotivo.getText().isEmpty()){
            GUIUtils.showMessage("Você deve informar o motivo da parada!");
            return;
        }
        if(tipoCadastro == TipoCadastro.PRODUCAO_MANUAL && tboxQtde.getText().isEmpty()){
            GUIUtils.showMessage("Você deve informar a quantidade de peças produzidas!");
            return;
        }
        if(tipoCadastro == TipoCadastro.PRODUCAO_MANUAL && LocalTime.parse(tboxHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")).isAfter(LocalTime.parse(tboxHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")))){
            GUIUtils.showMessage("A hora fim deve ser menor que a hora início!");
            return;
        }
    
        try {
            if(tipoCadastro == TipoCadastro.PARADA &&
                    (SdTiposParada001) daoTiposParada.initCriteria().addPredicateEq("codigo", tboxMotivo.getText()).loadEntityByPredicate() == null){
                GUIUtils.showMessage("Código digitado não é um código existente!");
                return;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
            return;
        }
    
        returnValues.put("horaInicio", tboxHoraInicio.getText().isEmpty() ? null : LocalTime.parse(tboxHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
        returnValues.put("horaFim", tboxHoraFim.getText().isEmpty() ? null : LocalTime.parse(tboxHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
        returnValues.put("motivo", Integer.parseInt(tboxMotivo.getText()));
        returnValues.put("qtde", Integer.parseInt(tboxQtde.getText()));
    
        Stage main = (Stage) btnSalvar.getScene().getWindow();
        main.close();
    }
    
    @FXML
    private void btnTecladoHoraInicioOnAction(ActionEvent event) {
        try {
            tboxHoraInicio.setText(
            new NumericKeyboardController(FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.TIME)
                    .timeKeyboard
                    .get()
                    .format(DateTimeFormatter.ofPattern("HH:mm"))
            );
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnTecladoHoraFimOnAction(ActionEvent event) {
        try {
            tboxHoraFim.setText(
                    new NumericKeyboardController(FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.TIME)
                            .timeKeyboard
                            .get()
                            .format(DateTimeFormatter.ofPattern("HH:mm"))
            );
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnTecladoQuantidadeOnAction(ActionEvent event) {
        try {
            tboxQtde.setText(
                    new NumericKeyboardController(FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.INTEGER)
                            .integerKeyboard
                            .getValue()
                            .toString()
            );
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnTecladoMotivoOnAction(ActionEvent event) {
        try {
            tboxMotivo.setText(
                    new NumericKeyboardController(FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.INTEGER)
                            .integerKeyboard
                            .getValue()
                            .toString()
            );
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
            GUIUtils.showException(e);
        }
    }
    
}
