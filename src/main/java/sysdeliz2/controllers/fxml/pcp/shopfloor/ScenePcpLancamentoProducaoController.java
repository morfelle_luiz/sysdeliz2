package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.view.VSdAprovDiariaColab;
import sysdeliz2.models.view.VSdPacotesLancOperacao;
import sysdeliz2.models.view.VSdProdHoraColab;
import sysdeliz2.models.view.VSdProgramacaoPendente;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ScenePcpLancamentoProducaoController implements Initializable {
    
    private final ObjectProperty<SdPacote001> sdPacoteSelecionado = new SimpleObjectProperty<>();
    private final BooleanProperty operadorParado = new SimpleBooleanProperty(false);
    private SdLancamentoParada001 paradaAberta = null;
    private LocalDateTime dhInicioLancadoManual = LocalDateTime.now();
    private AnimationTimer timer;
    private long lastTimerCall;
    // <editor-fold defaultstate="collapsed" desc="Declaração: Operações Colaborador">
    private final ListProperty<VSdProgramacaoPendente> operacoesPendenteOperador = new SimpleListProperty<>();
    private final ObjectProperty<VSdProgramacaoPendente> operacaoPendenteColaboradorSelecionada = new SimpleObjectProperty<>();
    private final ListProperty<VSdPacotesLancOperacao> pacotesLancamentoOperacao = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Map<String, String>> coresLancamentoAgrupadas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdPacotesLancOperacao> pacotesLancamentoOperacaoCorSelecionada = new SimpleListProperty<>(FXCollections.observableArrayList());
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Setor Operação">
    private final ListProperty<SdSetorOp001> setoresOperacao = new SimpleListProperty<>();
    private final ObjectProperty<SdSetorOp001> setorSelecionado = new SimpleObjectProperty<>();
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs JPA">
    private GenericDao<VSdProgramacaoPendente> daoOperacoesColaborador = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdProgramacaoPendente.class);
    private GenericDao<SdSetorOp001> daoSetorOp = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdSetorOp001.class);
    private GenericDao<SdPacote001> daoPacote = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdPacote001.class);
    private GenericDao<VSdPacotesLancOperacao> daoPacotesLancOperacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdPacotesLancOperacao.class);
    private GenericDao<SdLancamentoProducao001> daoLancamentosProducao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLancamentoProducao001.class);
    private GenericDao<SdLancamentoParada001> daoLancamentosParada = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLancamentoParada001.class);
    private GenericDao<SdColaborador> daoColaborador = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdColaborador.class);
    private GenericDao<SdProgramacaoPacote002> daoProgramacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdProgramacaoPacote002.class);
    private GenericDao<VSdAprovDiariaColab> daoViewAprovDiariaColab = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdAprovDiariaColab.class);
    private GenericDao<VSdProdHoraColab> daoProdHoraColab = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdProdHoraColab.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Components">
    @FXML
    private ComboBox<SdSetorOp001> cboxSetor;
    @FXML
    private Button btnFechar;
    @FXML
    private Button btnLimpar;
    @FXML
    private Label lbRelogio;
    @FXML
    private Label lbData;
    @FXML
    private TextField tboxOrdemProgramada;
    @FXML
    private TextField tboxProdutoProgramado;
    @FXML
    private TextField tboxProgramacaoCarregada;
    @FXML
    private TextField tboxCodigoOperador;
    @FXML
    private TableView<VSdProgramacaoPendente> tblOperacoes;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnAgrupamento;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnSeqColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnCodigoOperacao;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnDescOperacao;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnTempoOperacao;
    @FXML
    private TableView<VSdPacotesLancOperacao> tblPacotesLancamento;
    @FXML
    private TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao> clnSelectPacote;
    @FXML
    private TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao> clnQtdePacote;
    @FXML
    private TableView<Map<String, String>> tblCoresPacotesLancamento;
    @FXML
    private TableColumn<Map<String, String>, Map<String, String>> clnCorLancamento;
    @FXML
    private TableColumn<Map<String, String>, Map<String, String>> clnDescCorLancamento;
    @FXML
    private Button btnLancarParada;
    @FXML
    private Button btnLancarHora;
    @FXML
    private ImageView imgProduto;
    
    private final Image imgActive = new Image(getClass().getResource("/images/icons/buttons/active (3).png").toExternalForm());
    private final Image imgInactive = new Image(getClass().getResource("/images/icons/buttons/inactive (3).png").toExternalForm());
    // </editor-fold>
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        try {
//            setoresOperacao.set(daoSetorOp.list());
//            cboxSetor.requestFocus();
        
        this.initializeComponentes();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tboxProgramacaoCarregada.requestFocus();
            }
        });
        SysLogger.addFileLogApontamento("Iniciando tela de apontamento ",
                "APONTAMENTO DE PRODUÇÃO");
    
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override
            public void handle(final long now) {
                if (now > lastTimerCall + 1_000_000_000l) {
                    lbRelogio.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                    lbData.setText(LocalDate.now().format(DateTimeFormatter.ofPattern("E, dd/MM/yyyy")));
                    lastTimerCall = now;
                }
            }
        };
        timer.start();
//        } catch (SQLException e) {
//            SysLogger.addFileLogApontamento("Erro no initialize, tela de apontamento "
//                            + "EXCEPTION: " + e.getMessage(),
//                    "APONTAMENTO DE PRODUÇÃO");
//            e.printStackTrace();
//            GUIUtils.showException(e);
//        }
    }
    
    private void initializeComponentes() {
        // <editor-fold defaultstate="collapsed" desc="ComboBox SETOR OPERACAO">
        cboxSetor.itemsProperty().bind(setoresOperacao);
        cboxSetor.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                SysLogger.addFileLogApontamento("Selecionado setor " + newValue.getCodigo() + "para apontamentos de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                tboxProgramacaoCarregada.requestFocus();
            }
        });
        // </editor-fold
        // <editor-fold defaultstate="collapsed" desc="TableView OPERACOES">
        tblOperacoes.itemsProperty().bind(operacoesPendenteOperador);
        tblOperacoes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                SysLogger.addFileLogApontamento("Selecionando a operação " + newValue.getProgramacao().getId().getOperacao()
                                + " pelo colaborador " + newValue.getColaborador()
                                + " da programação " + newValue.getProgramacao().getId().getPacote()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                operacaoPendenteColaboradorSelecionada.set(newValue);
                try {
                    pacotesLancamentoOperacao.set(daoPacotesLancOperacao.initCriteria()
                            .addPredicateEq("programacao", sdPacoteSelecionado.get().getCodigo())
                            .addPredicateEq("colaborador", tboxCodigoOperador.getText())
                            .addPredicateEq("operacao", newValue.getProgramacao().getId().getOperacao().getCodorg())
                            .addPredicateEq("ordem", newValue.getProgramacao().getId().getOrdem())
                            .addPredicateEq("quebra", newValue.getProgramacao().getId().getQuebra())
                            .loadListByPredicate()
                    );
                    SysLogger.addFileLogApontamento("Carregando pacotes de lançamento para a operação " + newValue.getProgramacao().getId().getOperacao()
                                    + " pelo colaborador " + newValue.getColaborador()
                                    + " da programação " + newValue.getProgramacao().getId().getPacote()
                                    + " para apontamento de produção.",
                            "APONTAMENTO DE PRODUÇÃO");
                    coresLancamentoAgrupadas.clear();
                    Set<Map<String, String>> coresLancamento = new HashSet<>();
                    for (VSdPacotesLancOperacao pacote : pacotesLancamentoOperacao) {
                        Map<String, String> pacoteLancMap = new HashMap<>();
                        pacoteLancMap.put("qtde", String.valueOf(pacotesLancamentoOperacao.stream()
                                .filter(pacotesLanc -> pacotesLanc.getCor().equals(pacote.getCor()))
                                .mapToLong(pctLanc -> pctLanc.getQtde())
                                .sum()));
                        pacoteLancMap.put("codigo", pacote.getCor());
                        pacoteLancMap.put("descricao", pacote.getDescCor());
                        coresLancamento.add(pacoteLancMap);
                    }
                    ObservableList<Map<String, String>> coresParaLancamento = FXCollections.observableList(coresLancamento.stream().collect(Collectors.toList()));
                    coresParaLancamento = FXCollections.observableList(coresParaLancamento.stream()
                            .sorted(Comparator.comparing(o -> ((Map<String, String>) o).get("qtde")).reversed())
                            .collect(Collectors.toList()));
                    coresLancamentoAgrupadas.set(coresParaLancamento);
                    tblCoresPacotesLancamento.getSelectionModel().select(0);
                } catch (SQLException e) {
                    SysLogger.addFileLogApontamento("Erro na carga dos pacotes de lançamento para "
                                    + "a operação " + newValue.getProgramacao().getId().getOperacao()
                                    + " pelo colaborador " + newValue.getColaborador()
                                    + " da programação " + newValue.getProgramacao().getId().getPacote()
                                    + "EXCEPTION: " + e.getMessage(),
                            "APONTAMENTO DE PRODUÇÃO");
                    e.printStackTrace();
                    GUIUtils.showException(e);
                }
            }
        });
        clnAgrupamento.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(item.getOperacoesAgrupadas().size() > 0 ? "SIM" : "NÃO");
                    }
                }
            };
        });
        clnSeqColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toIntegerFormat(item.getSeqColaborador()));
                    }
                }
            };
        });
        clnCodigoOperacao.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(String.valueOf(item.getId().getOperacao().getCodorg()));
                    }
                }
            };
        });
        clnDescOperacao.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(item.getId().getOperacao().getDescricao());
                    }
                }
            };
        });
        clnTempoOperacao.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.getOperacoesAgrupadas().size() > 0 ? item.getTempoOpAgrupada().doubleValue() : item.getTempoOp().doubleValue(), 2));
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView CORES PACOTES LANCAMENTO">
        tblCoresPacotesLancamento.itemsProperty().bind(coresLancamentoAgrupadas);
        tblCoresPacotesLancamento.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                SysLogger.addFileLogApontamento("Selecionando a cor " + newValue.get("codigo")
                                + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                pacotesLancamentoOperacao.forEach(pacote -> pacote.setSelected(false));
                pacotesLancamentoOperacaoCorSelecionada.clear();
                pacotesLancamentoOperacaoCorSelecionada.set(
                        FXCollections.observableList(pacotesLancamentoOperacao.stream()
                                .filter(pacote -> pacote.getCor().equals(newValue.get("codigo")))
                                .collect(Collectors.toList())));
                if (pacotesLancamentoOperacaoCorSelecionada.size() > 0) {
                    pacotesLancamentoOperacaoCorSelecionada.get().get(0).setSelected(true);
                }
            }
        });
        clnCorLancamento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map<String, String>, Map<String, String>>, ObservableValue<Map<String, String>>>() {
            @Override
            public ObservableValue<Map<String, String>> call(TableColumn.CellDataFeatures<Map<String, String>, Map<String, String>> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnCorLancamento.setCellFactory(new Callback<TableColumn<Map<String, String>, Map<String, String>>, TableCell<Map<String, String>, Map<String, String>>>() {
            @Override
            public TableCell<Map<String, String>, Map<String, String>> call(TableColumn<Map<String, String>, Map<String, String>> param) {
                return new TableCell<Map<String, String>, Map<String, String>>() {
                    @Override
                    protected void updateItem(Map<String, String> item, boolean empty) {
                        super.updateItem(item, empty);
                        setGraphic(null);
                        setText(null);
                        if (item != null && !empty) {
                            setText(item.get("codigo"));
                        }
                    }
                };
            }
        });
        clnDescCorLancamento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map<String, String>, Map<String, String>>, ObservableValue<Map<String, String>>>() {
            @Override
            public ObservableValue<Map<String, String>> call(TableColumn.CellDataFeatures<Map<String, String>, Map<String, String>> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnDescCorLancamento.setCellFactory(new Callback<TableColumn<Map<String, String>, Map<String, String>>, TableCell<Map<String, String>, Map<String, String>>>() {
            @Override
            public TableCell<Map<String, String>, Map<String, String>> call(TableColumn<Map<String, String>, Map<String, String>> param) {
                return new TableCell<Map<String, String>, Map<String, String>>() {
                    @Override
                    protected void updateItem(Map<String, String> item, boolean empty) {
                        super.updateItem(item, empty);
                        setGraphic(null);
                        setText(null);
                        if (item != null && !empty) {
                            setText(item.get("descricao"));
                        }
                    }
                };
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PACOTES LANCAMENTO">
        tblPacotesLancamento.getSelectionModel().setCellSelectionEnabled(true);
        tblPacotesLancamento.itemsProperty().bind(pacotesLancamentoOperacaoCorSelecionada);
        tblPacotesLancamento.setEditable(true);
        tblPacotesLancamento.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (oldValue != null) {
                    oldValue.setSelected(false);
                    
                    SysLogger.addFileLogApontamento("Desmarcando o pacote de lançamento " + oldValue.getPacote()
                                    + " na cor " + oldValue.getCor()
                                    + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                    + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                    + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                    + " para apontamento de produção.",
                            "APONTAMENTO DE PRODUÇÃO");
                }
                newValue.setSelected(true);
                SysLogger.addFileLogApontamento("Selecionando o pacote de lançamento " + newValue.getPacote()
                                + " na cor " + newValue.getCor()
                                + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
            }
        });
        clnSelectPacote.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdPacotesLancOperacao, VSdPacotesLancOperacao>, ObservableValue<VSdPacotesLancOperacao>>() {
            @Override
            public ObservableValue<VSdPacotesLancOperacao> call(TableColumn.CellDataFeatures<VSdPacotesLancOperacao, VSdPacotesLancOperacao> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnSelectPacote.setComparator(new Comparator<VSdPacotesLancOperacao>() {
            @Override
            public int compare(VSdPacotesLancOperacao p1, VSdPacotesLancOperacao p2) {
                return p1.getIdJpa().compareTo(p2.getIdJpa());
            }
        });
        clnSelectPacote.setCellFactory(new Callback<TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao>, TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao>>() {
            @Override
            public TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao> call(TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao> btnCol) {
                return new TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao>() {
                    final ImageView imgSelectPacote = new ImageView(imgActive);
                    final Button btnSelectPacote = new Button();
                    final Tooltip tipBtnSelectPacote = new Tooltip("Selecionar/Deselecionar pacote");
                    
                    {
                        btnSelectPacote.setGraphic(imgSelectPacote);
                        btnSelectPacote.setTooltip(tipBtnSelectPacote);
                    }
                    
                    @Override
                    public void updateItem(VSdPacotesLancOperacao pacote, boolean empty) {
                        super.updateItem(pacote, empty);
                        if (pacote != null) {
                            imgSelectPacote.setImage(pacote.isSelected() ? imgActive : imgInactive);
                            btnSelectPacote.setText("Selecionar/Deselecionar pacote");
                            btnSelectPacote.setGraphic(imgSelectPacote);
                            btnSelectPacote.setPrefWidth(50.0);
                            btnSelectPacote.getStyleClass().add("lg");
                            btnSelectPacote.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                            setGraphic(btnSelectPacote);
                            btnSelectPacote.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    pacote.setSelected(!pacote.isSelected());
                                    SysLogger.addFileLogApontamento((!pacote.isSelected() ? "Selecionando" : "Desmarcando") + " o pacote de lançamento " + pacote.getPacote()
                                                    + " na cor " + pacote.getCor()
                                                    + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                                    + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                                    + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                                    + " para apontamento de produção.",
                                            "APONTAMENTO DE PRODUÇÃO");
                                    tblPacotesLancamento.refresh();
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        clnQtdePacote.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdPacotesLancOperacao, VSdPacotesLancOperacao>, ObservableValue<VSdPacotesLancOperacao>>() {
            @Override
            public ObservableValue<VSdPacotesLancOperacao> call(TableColumn.CellDataFeatures<VSdPacotesLancOperacao, VSdPacotesLancOperacao> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnQtdePacote.setComparator(new Comparator<VSdPacotesLancOperacao>() {
            @Override
            public int compare(VSdPacotesLancOperacao p1, VSdPacotesLancOperacao p2) {
                return p1.getIdJpa().compareTo(p2.getIdJpa());
            }
        });
        clnQtdePacote.setCellFactory(new Callback<TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao>, TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao>>() {
            @Override
            public TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao> call(TableColumn<VSdPacotesLancOperacao, VSdPacotesLancOperacao> btnCol) {
                return new TableCell<VSdPacotesLancOperacao, VSdPacotesLancOperacao>() {
                    final Button btnAlterarQtdePacote = new Button();
                    final Tooltip tipBtnAlterarQtdePacote = new Tooltip("Alterar quantidade de peças do pacote.");
                    
                    {
                        btnAlterarQtdePacote.setTooltip(tipBtnAlterarQtdePacote);
                    }
                    
                    @Override
                    public void updateItem(VSdPacotesLancOperacao pacote, boolean empty) {
                        super.updateItem(pacote, empty);
                        if (pacote != null) {
                            btnAlterarQtdePacote.setText(pacote.getQtde() + "");
                            btnAlterarQtdePacote.setPrefWidth(80.0);
                            btnAlterarQtdePacote.getStyleClass().add("lg");
                            btnAlterarQtdePacote.contentDisplayProperty().set(ContentDisplay.TEXT_ONLY);
                            setGraphic(btnAlterarQtdePacote);
                            btnAlterarQtdePacote.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    try {
                                        pacote.setQtde(
                                                new NumericKeyboardController(Modals.FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.INTEGER)
                                                        .integerKeyboard
                                                        .getValue()
                                        );
                                        SysLogger.addFileLogApontamento("Alterando a qtde do pacote de lançamento " + pacote.getPacote()
                                                        + " para " + pacote.getQtde()
                                                        + " na cor " + pacote.getCor()
                                                        + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                                        + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                                        + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                                        + " para apontamento de produção.",
                                                "APONTAMENTO DE PRODUÇÃO");
                                        tblPacotesLancamento.refresh();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
                                        GUIUtils.showException(e);
                                    }
//                                    try {
//                                        pacote.setQtde(
//                                                (Integer) new SceneCadastraHorarioLancamentoController(Modals.FXMLWindow.CadastroHorarioLancamento,
//                                                        SceneCadastraHorarioLancamentoController.TipoCadastro.QTDE_ONLY)
//                                                        .returnValues.get().get("qtde")
//                                        );
//                                        tblPacotesLancamento.refresh();
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                        LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
//                                        GUIUtils.showException(e);
//                                    }
                                }
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button LANCAR PARADA">
        btnLancarParada.textProperty().bind(Bindings.when(operadorParado).then("RETORNAR PARADA").otherwise("INICIAR PARADA"));
        // </editor-fold>
    }
    
    private void exibirDadosProgramacao(SdPacote001 pacoteSelecionado) {
        if (pacoteSelecionado != null) {
            tboxOrdemProgramada.setText(pacoteSelecionado.getSdProgramacaoOf().getOrdemProd());
            tboxProdutoProgramado.setText(pacoteSelecionado.getProduto().getCodigo());
            try {
                File loadImgProduto = new File("\\\\divad01\\Sistema\\TI_ERP\\Arquivos\\imagens\\produto\\" + pacoteSelecionado.getProduto().getCodigo() + ".jpg");
                imgProduto.setImage(new Image(new FileInputStream(loadImgProduto)));
            } catch (FileNotFoundException ex) {
                File loadNoImage = new File("C:\\SysDelizApontamento\\no-photo.png");
                try {
                    imgProduto.setImage(new Image(new FileInputStream(loadNoImage)));
                } catch (FileNotFoundException ex1) {
                    Logger.getLogger(ScenePcpLancamentoProducaoController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            SysLogger.addFileLogApontamento("Carregando informações do pacote de programação " + tboxOrdemProgramada.getText()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
        }
    }
    
    private void limparSetup() {
        JPAUtils.clearEntity(sdPacoteSelecionado);
        operacoesPendenteOperador.clear();
        sdPacoteSelecionado.set(null);
        operacaoPendenteColaboradorSelecionada.set(null);
        pacotesLancamentoOperacaoCorSelecionada.clear();
        coresLancamentoAgrupadas.clear();
        pacotesLancamentoOperacao.clear();
        operadorParado.set(false);
        imgProduto.setImage(null);
        tboxCodigoOperador.clear();
        tboxProgramacaoCarregada.clear();
        tboxProdutoProgramado.clear();
        tboxOrdemProgramada.clear();
        tboxProgramacaoCarregada.requestFocus();
        
        SysLogger.addFileLogApontamento("Limpando tela de apontamento de produção.",
                "APONTAMENTO DE PRODUÇÃO");
    }
    
    private long verificaParadaEmIntervalo(Integer codigoColaborador, LocalDateTime dhInicio, LocalDateTime dhFim) throws SQLException {
        // Verifica se dh fim está em um intervalo
        SdColaborador colaborador = DAOFactory.getSdColaborador001DAO().getByCode(codigoColaborador);
        Long tempoIntervalos = 0L;
        if (colaborador != null) {
            ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                    .getIntervalosTurno(colaborador.getCodigoTurno() + "");
            
            for (Map<String, Object> intervalo : intervalos) {
                if (intervalo.get("tipo").equals("inter"))
                    continue;
                
                LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
                LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
                LocalDateTime horarioInicioLancamento = LocalDateTime.of(
                        inicioIntervalo.getYear(),
                        inicioIntervalo.getMonth(),
                        inicioIntervalo.getDayOfMonth(),
                        dhInicio.getHour(),
                        dhInicio.getMinute());
                LocalDateTime horarioFimLancamento = LocalDateTime.of(
                        inicioIntervalo.getYear(),
                        inicioIntervalo.getMonth(),
                        dhFim.getDayOfMonth() != dhInicio.getDayOfMonth() ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                        dhFim.getHour(),
                        dhFim.getMinute());
                Long tempoIntervalo = (Long) intervalo.get("intervalo");
                
                if (horarioInicioLancamento.isBefore(inicioIntervalo) && horarioFimLancamento.isAfter(fimIntervalo)) {
                    tempoIntervalos += tempoIntervalo;
                } else if ((horarioInicioLancamento.isBefore(inicioIntervalo) && horarioFimLancamento.isAfter(inicioIntervalo)) && horarioFimLancamento.isBefore(fimIntervalo)) {
                    tempoIntervalos += ChronoUnit.MINUTES.between(inicioIntervalo, horarioFimLancamento);
                } else if ((horarioInicioLancamento.isAfter(inicioIntervalo) && horarioInicioLancamento.isBefore(fimIntervalo)) && horarioFimLancamento.isAfter(fimIntervalo)) {
                    tempoIntervalos += ChronoUnit.MINUTES.between(horarioInicioLancamento, fimIntervalo);
                } else if (horarioFimLancamento.isAfter(inicioIntervalo) && horarioFimLancamento.isBefore(fimIntervalo)) {
                    tempoIntervalos += 0L;
                }
            }
        }
        return tempoIntervalos;
    }
    
    /**
     * verifica se a data fim enté dentro de um período de intrajornada.
     * verifica também se uma programação inicia antes do intervalo e termina
     * após incrementando o tempo de intervalo no tempo da operação.
     *
     * @param codigoColaborador
     * @param dhInicio
     * @param dhFim
     * @return
     * @throws SQLException
     */
    private long verificaHorarioEmIntervalo(Integer codigoColaborador, LocalDateTime dhInicio, LocalDateTime dhFim) throws SQLException {
        // Verifica se dh fim está em um intervalo
        SdColaborador colaborador = DAOFactory.getSdColaborador001DAO().getByCode(codigoColaborador);
        Long tempoIntervalos = 0L;
        if (colaborador != null) {
            ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                    .getIntervalosTurno(colaborador.getCodigoTurno() + "");
            
            LocalDateTime dhFimComIntervalo = dhFim;
            for (Map<String, Object> intervalo : intervalos) {
                LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
                LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
                LocalDateTime horarioInicioLancamento = LocalDateTime.of(
                        inicioIntervalo.getYear(),
                        inicioIntervalo.getMonth(),
                        inicioIntervalo.getDayOfMonth(),
                        dhInicio.getHour(),
                        dhInicio.getMinute());
                LocalDateTime horarioFimLancamento = LocalDateTime.of(
                        inicioIntervalo.getYear(),
                        inicioIntervalo.getMonth(),
                        dhInicio.getDayOfMonth() != dhFim.getDayOfMonth() ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                        dhFim.getHour(),
                        dhFim.getMinute());
                Long tempoIntervalo = (Long) intervalo.get("intervalo");
                
                if (((String) intervalo.get("tipo")).equals("intra")) {
                    if ((horarioInicioLancamento.isBefore(inicioIntervalo) || horarioInicioLancamento.isEqual(inicioIntervalo))
                            && (horarioFimLancamento.isAfter(fimIntervalo) || horarioFimLancamento.isEqual(fimIntervalo))) {
                        tempoIntervalos += tempoIntervalo;
                    } else if (horarioInicioLancamento.isAfter(inicioIntervalo) && horarioInicioLancamento.isBefore(fimIntervalo)) {
                        tempoIntervalos += ChronoUnit.MINUTES.between(horarioInicioLancamento, fimIntervalo);
                    } else if (horarioFimLancamento.isAfter(inicioIntervalo) && horarioFimLancamento.isBefore(fimIntervalo)) {
                        tempoIntervalos += ChronoUnit.MINUTES.between(inicioIntervalo, horarioFimLancamento);
                    }
                }
            }
        }
        return tempoIntervalos;
    }
    
    private Long minutosParadaDedutivelDia(Integer codigoColaborador, LocalDateTime dhInicio, LocalDateTime dhFim) {
        long tempoParadas = 0L;
        
        try {
            ObservableList<SdLancamentoParada001> paradasColaborador = daoLancamentosParada.initCriteria()
                    .addPredicateEqPkEmbedded("id", "colaborador", codigoColaborador.toString(), false)
                    .addPredicateBetween("dhFim",
                            LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("00:00:00", DateTimeFormatter.ofPattern("HH:mm:ss"))),
                            LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("23:59:59", DateTimeFormatter.ofPattern("HH:mm:ss"))))
                    .loadListByPredicate();
            
            if (paradasColaborador.size() > 0) {
                LocalDateTime dhInicioParada = null;
                LocalDateTime dhFimParada = null;
                for (SdLancamentoParada001 parada : paradasColaborador) {
                    if (parada.getId().getMotivo().isDedutivel()) {
                        if ((parada.getId().getDhInicio().isBefore(dhInicio) || parada.getId().getDhInicio().isEqual(dhInicio))
                                && (parada.getDhFim().isAfter(dhFim) || parada.getDhFim().isEqual(dhFim))) {
                            dhInicioParada = dhInicio;
                            dhFimParada = dhFim;
                            tempoParadas += ChronoUnit.MINUTES.between(dhInicioParada, dhFimParada);
                        } else if ((parada.getId().getDhInicio().isAfter(dhInicio) || parada.getId().getDhInicio().isEqual(dhInicio))
                                && (parada.getDhFim().isBefore(dhFim) || parada.getDhFim().isEqual(dhFim))) {
                            tempoParadas += parada.getTempo().longValue();
                        } else if ((parada.getId().getDhInicio().isBefore(dhInicio))
                                && ((parada.getDhFim().isBefore(dhFim) || parada.getDhFim().isEqual(dhFim)) && parada.getDhFim().isAfter(dhInicio))) {
                            dhInicioParada = dhInicio;
                            dhFimParada = parada.getDhFim();
                            tempoParadas += ChronoUnit.MINUTES.between(dhInicioParada, dhFimParada);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        return tempoParadas;
    }
    
    public void setDhInicioLancamento(LocalDateTime dhLancado){
        dhInicioLancadoManual = dhLancado;
    }
    
    //------------------------------- FXML Actions -------------------------------------------------
    
    @FXML
    void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        timer.stop();
        main.close();
    }
    
    @FXML
    void btnLimparOnAction(ActionEvent event) {
        limparSetup();
    }
    
    @FXML
    void tboxProgramacaoCarregadaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            try {
                sdPacoteSelecionado.set((SdPacote001) daoPacote.initCriteria()
                        .addPredicateEq("codigo", tboxProgramacaoCarregada.getText())
                        .loadEntityByPredicate());
                
                if (sdPacoteSelecionado.get() == null) {
                    GUIUtils.showMessageFullScreenWithButtonClose("PROGRAMAÇÃO NÃO ENCONTRADA COM O NÚMERO " + tboxProgramacaoCarregada.getText() + "!",
                            Alert.AlertType.ERROR).showAndWait();
                    tboxProgramacaoCarregada.clear();
                    tboxProgramacaoCarregada.requestFocus();
                    return;
                }
                
                SysLogger.addFileLogApontamento("Carregando pacote de programação " + tboxOrdemProgramada.getText()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                
                exibirDadosProgramacao(sdPacoteSelecionado.get());
                tboxCodigoOperador.requestFocus();
            } catch (SQLException | NullPointerException e) {
                SysLogger.addFileLogApontamento("Erro para carregar pacote de programação " + tboxOrdemProgramada.getText()
                                + "EXCEPTION: " + e.getMessage(),
                        "APONTAMENTO DE PRODUÇÃO");
                e.printStackTrace();
                GUIUtils.showException(e);
            }
        }
    }
    
    @FXML
    void tboxCodigoOperadorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            try {
                JPAUtils.clearEntitys(operacoesPendenteOperador);
                JPAUtils.clearEntity(paradaAberta);
                //Carrega operações pendentes
                operacoesPendenteOperador.set(
                        daoOperacoesColaborador.initCriteria()
                                .addPredicateEq("colaborador", tboxCodigoOperador.getText())
                                .addPredicateEqCascateField("programacao.id.pacote", sdPacoteSelecionado.get().getCodigo(), false)
                                .loadListByPredicate()
                );
                
                SysLogger.addFileLogApontamento("Carregado " + operacoesPendenteOperador.size() + " operações pendentes para o colaborador " + tboxCodigoOperador.getText()
                                + " na programação " + tboxOrdemProgramada.getText()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                
                // Verifica se existe alguma parada em aberto para o colaborador
                paradaAberta = (SdLancamentoParada001) daoLancamentosParada.initCriteria()
                        .addPredicateEqPkEmbedded("id", "colaborador", tboxCodigoOperador.getText(), false)
                        .addPredicateIsNull("dhFim")
                        .loadEntityByPredicate();
                operadorParado.set(paradaAberta != null);
                
                SysLogger.addFileLogApontamento((operadorParado.get() ? "Encontrada" : "Não encontrada")
                                + " uma parada aberta para o colaborador " + tboxCodigoOperador.getText()
                                + " na programação " + tboxOrdemProgramada.getText()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                
                // Mensagem de sem operações pendentes para o colaborador
                if (operacoesPendenteOperador.size() == 0) {
                    GUIUtils.showMessageFullScreenWithButtonClose("NENHUMA OPERAÇÃO PENDENTE PARA O COLABORADOR NO PACOTE " + tboxProgramacaoCarregada.getText() + "!",
                            Alert.AlertType.ERROR).showAndWait();
                    if (paradaAberta == null) {
                        tboxCodigoOperador.clear();
                        tboxProgramacaoCarregada.clear();
                        tboxProgramacaoCarregada.requestFocus();
                    }
                    return;
                }
                
                // Atualiza a tabela de operações pendentes e seleciona a primeira operação
                tblOperacoes.refresh();
                tblOperacoes.getSelectionModel().select(0);
                
                //Seleciona a cor do produto para lançamento a partir do último apontamento feito pelo colaborador
                ObservableList<SdLancamentoProducao001> lancamentosDoDia = daoLancamentosProducao.initCriteria()
                        .addPredicateBetween(
                                "dhFim",
                                LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("00:00:00", DateTimeFormatter.ofPattern("HH:mm:ss"))),
                                LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("23:59:59", DateTimeFormatter.ofPattern("HH:mm:ss"))))
                        .addPredicateEq("colaborador", tboxCodigoOperador.getText())
                        .loadListByPredicate();
                SdLancamentoProducao001 maxLancamentoDia = null;
                if (lancamentosDoDia.size() > 0) {
                    maxLancamentoDia = lancamentosDoDia.stream()
                            .collect(Collectors.maxBy(Comparator.comparing(SdLancamentoProducao001::getDhFim))).get();
                }
                if (maxLancamentoDia != null) {
                    SdPacote001 ultimoPacoteLancado = DAOFactory.getSdPacoteLancamento001DAO().getByPacoteProgOrdem(maxLancamentoDia.getPacoteProg(), maxLancamentoDia.getPacoteLan());
                    if (ultimoPacoteLancado != null) {
                        for (Map<String, String> item : tblCoresPacotesLancamento.getItems()) {
                            if (item.get("codigo").equals(ultimoPacoteLancado.getCor())) {
                                tblCoresPacotesLancamento.getSelectionModel().select(item);
                                break;
                            }
                        }
                    }
                }
                
                //Atualiza a tabela de pacotes de lançamento
                tblPacotesLancamento.refresh();
            } catch (SQLException | NullPointerException e) {
                SysLogger.addFileLogApontamento("Erro para carregar operações pendentes do colaborador " + tboxCodigoOperador.getText()
                                + " para o pacote de programação " + tboxOrdemProgramada.getText()
                                + "EXCEPTION: " + e.getMessage(),
                        "APONTAMENTO DE PRODUÇÃO");
                e.printStackTrace();
                GUIUtils.showException(e);
            }
        }
    }
    
    @FXML
    void btnLancarParadaOnMouseClicked(MouseEvent event) {
        try {
            if (operadorParado.get()) {
                paradaAberta.setDhFim(LocalDateTime.now());
                if (event.isShiftDown()) {
                    ObservableMap<String, Object> returnoEntradaManual = new SceneCadastraHorarioLancamentoController(
                            Modals.FXMLWindow.CadastroHorarioLancamento,
                            SceneCadastraHorarioLancamentoController.TipoCadastro.RETORNO)
                            .returnValues
                            .get();
                    
                    paradaAberta.setDhFim(paradaAberta.getDhFim().with((LocalTime) returnoEntradaManual.get("horaFim")));
                }
                paradaAberta.setTempo(new BigDecimal((ChronoUnit.SECONDS.between(paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()) / 60.0)
                        - (verificaParadaEmIntervalo(paradaAberta.getId().getColaborador(), paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()))));
                
                daoLancamentosParada.update(paradaAberta);
                operadorParado.set(false);
                SysLogger.addFileLogApontamento("Retornando parada aberta para o colaborador " + tboxCodigoOperador.getText()
                                + " na programação " + tboxOrdemProgramada.getText()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
            } else {
                SysLogger.addFileLogApontamento("Abrindo tela de lançamento de parada para o colaborador " + tboxCodigoOperador.getText()
                                + " na programação " + tboxOrdemProgramada.getText()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                new ScenePcpLancamentoParadaController(Modals.FXMLWindow.LancamentoParada, tboxCodigoOperador.getText());
            }
        } catch (IOException | SQLException e) {
            SysLogger.addFileLogApontamento("Erro no lançamento do retorno ou lançamento de parada "
                            + "EXCEPTION: " + e.getMessage(),
                    "APONTAMENTO DE PRODUÇÃO");
            e.printStackTrace();
            GUIUtils.showException(e);
        }
        this.btnLimparOnAction(null);
    }
    
    @FXML
    void btnLancarHoraOnMouseClicked(MouseEvent event) {
        
        try {
            //Carrega o objeto do operador que irá fazer o lançamento
            SdColaborador colaboradorLancamento = (SdColaborador) daoColaborador.initCriteria()
                    .addPredicateEq("codigo", tboxCodigoOperador.getText())
                    .loadEntityByPredicate();
            
            //Verifica se o operador está com uma parada em aberto
            if (operadorParado.get()) {
                paradaAberta.setDhFim(LocalDateTime.now());
                paradaAberta.setTempo(new BigDecimal((ChronoUnit.SECONDS.between(paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()) / 60.0)
                        - (verificaParadaEmIntervalo(colaboradorLancamento.getCodigo(), paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()))));
                
                SysLogger.addFileLogApontamento("Fechando parada aberta no apontamento de produção "
                                + " na operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                
                daoLancamentosParada.update(paradaAberta);
                operadorParado.set(false);
            }
            
            //Guarda o objeto da operação selecionada
            VSdProgramacaoPendente operacaoSelecionada = tblOperacoes.getSelectionModel().getSelectedItem();
            ObservableList<SdLancamentoProducao001> lancamentosDoDia = daoLancamentosProducao.initCriteria()
                    .addPredicateBetween(
                            "dhFim",
                            LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("00:00:00", DateTimeFormatter.ofPattern("HH:mm:ss"))),
                            LocalDateTime.now().with(LocalDate.now()).with(LocalTime.parse("23:59:59", DateTimeFormatter.ofPattern("HH:mm:ss"))))
                    .addPredicateEq("colaborador", colaboradorLancamento.getCodigo())
                    .loadListByPredicate();
            SysLogger.addFileLogApontamento("Carregando " + lancamentosDoDia.size() + " lançamentos no dia atual "
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            
            //Set da hora fim e qtde de peças lancadas
            LocalDateTime dhFimLancamento = LocalDateTime.now(ZoneId.systemDefault());
            Integer qtdePecasLancamento = pacotesLancamentoOperacao.stream()
                    .filter(pacote -> pacote.isSelected())
                    .mapToInt(pacote -> pacote.getQtde())
                    .sum();
            //Set da dh inicio do lançamento para o início do turno 08:20, caso tenha um lançamento no dia, pega a dh inicio a dh fim do último lançamento do dia
            //Se não tem lançamento no dia, consulta no SENIOR se tem o ponto, se tiver, pega o horário do ponto, senão pede para o colaborador digitar.
            LocalDateTime dhInicioLancamento = LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 20));
            SdLancamentoProducao001 maxLancamentoDia = null;
            if (lancamentosDoDia.stream().count() > 0L) {
                maxLancamentoDia = lancamentosDoDia.stream()
                        .collect(Collectors.maxBy(Comparator.comparing(SdLancamentoProducao001::getDhFim))).get();
    
                SysLogger.addFileLogApontamento("Atribuindo dh fim do ultimo lançamento "
                                + " do colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                + " na programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                + " com a DH: " + maxLancamentoDia.getDhFim()
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                dhInicioLancamento = maxLancamentoDia.getDhFim();
            }
            else if (!event.isShiftDown()) {
                //verifica se tem ponto
                dhInicioLancamento = DAOFactory.getSdColaborador001DAO().getHoraPontoCostureira(String.valueOf(colaboradorLancamento.getCodigoRH()));
                if(dhInicioLancamento == null) {
                    //se não tem ponto, pede para o usuário digitar a hora com 08:20 como default
                    Button btnMessage = new Button("REGISTRAR HORA");
                    btnMessage.setGraphic(new ImageView(new Image(this.getClass().getResource("/images/icons/buttons/keyboard (4).png").toExternalForm())));
                    btnMessage.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            try {
                                LocalTime horaLancada = new NumericKeyboardController(Modals.FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.TIME, "08:20")
                                        .timeKeyboard
                                        .get();
                                if (horaLancada != null) {
                                    setDhInicioLancamento(LocalDateTime.now().with(horaLancada));
                                    ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                                } else {
                                    GUIUtils.showMessageFullScreenWithButtonClose("VOCÊ PRECISA INFORMAR A HORA DE INÍCIO DA OPERAÇÃO!",
                                            Alert.AlertType.INFORMATION).showAndWait();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                SysLogger.addFileLogApontamento("Erro no lançamento de produção para o colaborador " + tboxCodigoOperador.getText()
                                                + " na programação " + tboxOrdemProgramada.getText()
                                                + " para a operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                                                + "EXCEPTION: " + e.getMessage(),
                                        "APONTAMENTO DE PRODUÇÃO");
                                GUIUtils.showException(e);
                            }
                        }
                    });
                    do {
                        GUIUtils.showMessageFullScreenWithButtonClose("ESSE É O SEU PRIMEIRO LANÇAMENTO DO DIA, FAVOR, INFORMAR A HORA DE INÍCIO DESTE LANÇAMENTO!",
                                Alert.AlertType.INFORMATION, btnMessage).showAndWait();
                        dhInicioLancamento = dhInicioLancadoManual;
                        if(dhFimLancamento.isBefore(dhInicioLancamento))
                            GUIUtils.showMessageFullScreenWithButtonClose("VOCÊ INSERIU UMA HORA DE INÍCIO MAIOR QUE A HORA AGORA, O HORÁRIO DE INÍCIO DEVE SER MENOR QUE A HORA ATUAL!\n\n" +
                                            "HORÁRIO DIGITADO: "+dhInicioLancamento.format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " " +
                                            "HORÁRIO ATUAL: "+dhFimLancamento.format(DateTimeFormatter.ofPattern("HH:mm:ss")),
                                    Alert.AlertType.WARNING).showAndWait();
                    }while(dhFimLancamento.isBefore(dhInicioLancamento));
                    SysLogger.addFileLogApontamento("Atribuindo dh inicio lançamento como valor manual "
                                    + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                    + " na programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                    + " com a DH: " + dhInicioLancamento
                                    + " para apontamento de produção.",
                            "APONTAMENTO DE PRODUÇÃO");
                }else{
                    SysLogger.addFileLogApontamento("Atribuindo dh inicio lançamento como valor do ponto registrado "
                                    + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                    + " na programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                    + " com a DH: " + dhInicioLancamento
                                    + " para apontamento de produção.",
                            "APONTAMENTO DE PRODUÇÃO");
                }
            }
            
            //Verifica se está com o SHIFT apertado para entrada manual de informação
            //Tem que setar os valores para DH INICIO / DH FIM / QTDE
            if (event.isShiftDown()) {
                ObservableMap<String, Object> returnoEntradaManual = new SceneCadastraHorarioLancamentoController(
                        Modals.FXMLWindow.CadastroHorarioLancamento,
                        SceneCadastraHorarioLancamentoController.TipoCadastro.PRODUCAO_MANUAL)
                        .returnValues
                        .get();
                
                dhInicioLancamento = LocalDateTime.now().with((LocalTime) returnoEntradaManual.get("horaInicio"));
                dhFimLancamento = LocalDateTime.now().with((LocalTime) returnoEntradaManual.get("horaFim"));
                qtdePecasLancamento = (Integer) returnoEntradaManual.get("qtde");
                SysLogger.addFileLogApontamento("Lançamento manual de horário de lançamento "
                                + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                                + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                                + " início " + dhInicioLancamento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))
                                + " fim " + dhFimLancamento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))
                                + " qtde " + qtdePecasLancamento
                                + " para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
            }
            
            ObservableList<SdLancamentoProducao001> pacotesParaLancamento = FXCollections.observableArrayList();
            // set dos tempos para calculo da produção e horários de lançamentos dos pacotes
            Long tempoParadoIntervalo = verificaHorarioEmIntervalo(colaboradorLancamento.getCodigo(), dhInicioLancamento, dhFimLancamento); // em minutos
            SysLogger.addFileLogApontamento("Encontrados " + tempoParadoIntervalo + " minutos de parada em intervalo "
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            Long tempoParadaDedutivel = minutosParadaDedutivelDia(colaboradorLancamento.getCodigo(), dhInicioLancamento, dhFimLancamento); // em minutos
            SysLogger.addFileLogApontamento("Encontrados " + tempoParadaDedutivel + " minutos de parada dedutível "
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            Long tempoTotalProducaoEmSegundos = ChronoUnit.SECONDS.between(dhInicioLancamento, dhFimLancamento); // em segundos
            SysLogger.addFileLogApontamento("Tempo de " + tempoTotalProducaoEmSegundos + " segundos de produção (das " + dhInicioLancamento + " às " + dhFimLancamento + ")"
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            // tempoRealOperacao é a variável que tem o tempo realizado na operação, esse valor é para cálculo da eficiência
            Long tempoRealOperacao = (tempoTotalProducaoEmSegundos - (tempoParadoIntervalo * 60) - (tempoParadaDedutivel * 60)) / qtdePecasLancamento; // tem da operação descontando paradas
            SysLogger.addFileLogApontamento("Tempo de " + tempoRealOperacao + " minutos de produção real (não contabiliza paradas dedutiveis e intervalos)"
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            // tempoOperacaoDhFim é a variável com o tempo da operação em minutos corridos, para determinar a hora fim do lançamento do pacote
            // o intuito da variável é para quando selecionar mais de um pacote de lançamento e ele determinar uma hora fim para cada pacote e não agrupado.
            Long tempoOperacaoDhFim = tempoTotalProducaoEmSegundos / qtdePecasLancamento; // tempo da operação em hora corrida
            SysLogger.addFileLogApontamento("Tempo de " + tempoOperacaoDhFim + " minutos de produção corrigo (das " + dhInicioLancamento + " às " + dhFimLancamento + ") por peça"
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            // cria o array com os pacotes de lançamento selecionados
            List<VSdPacotesLancOperacao> pacotesSelecionados = pacotesLancamentoOperacao.stream()
                    .filter(pacote -> pacote.isSelected())
                    .collect(Collectors.toList());
            SysLogger.addFileLogApontamento("Encontrados " + pacotesSelecionados.size() + " pacotes de lançamentos selecionados "
                            + " pelo colaborador " + operacaoPendenteColaboradorSelecionada.get().getColaborador()
                            + " da programação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote()
                            + " para apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
            
            //criando a lista de lançamentos para salvar no banco de dados.
            Integer qtdePecasOperacao = qtdePecasLancamento;
            LocalDateTime dhInicioLancamentoOperacao = dhInicioLancamento;
            String lancAgrup = pacotesSelecionados.get(0).getProgramacao()
                    .concat(String.valueOf(pacotesSelecionados.get(0).getOperacao()))
                    .concat(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(pacotesSelecionados.get(0).getOrdem()),2,'0'))
                    .concat(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(pacotesSelecionados.get(0).getQuebra()),2,'0'))
                    .concat(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(pacotesSelecionados.get(0).getQuebra()),2,'0'))
                    .concat(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(pacotesSelecionados.get(0).getColaborador()),3,'0'))
                    .concat(dhInicioLancamento.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
            for (VSdPacotesLancOperacao pacote : pacotesSelecionados) {
                if (qtdePecasOperacao <= 0) {
                    break;
                }
                
                // cáculo da eficiencia utilizando o tempo de operação sem paradas de turno e paradas dedutíveis
                Double eficienciaPacote = operacaoSelecionada.getProgramacao().getTempoOpAgrupada().doubleValue() / (tempoRealOperacao.doubleValue() / 60.0);
                // AQUI TEM QUE CRIAR UM ALERTA PARA O RODRIGO VERIFICAR, INDICANDO O LANÇAMENTO E AS PARADAS VINCULADAS NO LANÇAMENTO.
                LocalDateTime horaFimLancamento = dhInicioLancamentoOperacao
                        .plusSeconds((pacote.getQtde() < qtdePecasOperacao ? pacote.getQtde() : qtdePecasOperacao) * ((long) tempoOperacaoDhFim));
                pacotesParaLancamento.add(
                        new SdLancamentoProducao001(
                                sdPacoteSelecionado.get().getCodigo(),
                                operacaoSelecionada.getProgramacao().getId().getOperacao().getCodorg(),
                                operacaoSelecionada.getProgramacao().getId().getOrdem(),
                                operacaoSelecionada.getProgramacao().getId().getQuebra(),
                                colaboradorLancamento.getCodigo(),
                                pacote.getPacote(),
                                operacaoSelecionada.getProgramacao().getTempoOpAgrupada(),
                                pacote.getQtde() < qtdePecasOperacao ? pacote.getQtde() : qtdePecasOperacao,
                                dhInicioLancamentoOperacao,
                                horaFimLancamento,
                                new BigDecimal(tempoRealOperacao.doubleValue() / 60.0),
                                operacaoSelecionada.getProgramacao().getCelula().getCodigo(),
                                new BigDecimal(eficienciaPacote > 2.0 ? 2.0 : eficienciaPacote),
                                lancAgrup
                        )
                );
                SysLogger.addFileLogApontamento("Praparando lançamento ["
                                + " pacote de programacao " + sdPacoteSelecionado.get().getCodigo()
                                + ", operacao " + operacaoSelecionada.getProgramacao().getId().getOperacao().getCodorg()
                                + ", ordem " + operacaoSelecionada.getProgramacao().getId().getOrdem()
                                + ", quebra " + operacaoSelecionada.getProgramacao().getId().getQuebra()
                                + ", colaborador " + colaboradorLancamento.getCodigo()
                                + ", pacote lançamento " + pacote.getPacote()
                                + ", tempo operacao " + operacaoSelecionada.getProgramacao().getTempoOpAgrupada()
                                + ", qtde peças " + (pacote.getQtde() < qtdePecasOperacao ? pacote.getQtde() : qtdePecasOperacao)
                                + ", dh inicio lançamento " + dhInicioLancamentoOperacao
                                + ", dh fim lançamento " + horaFimLancamento
                                + ", tempo realizado " + (tempoRealOperacao.doubleValue() / 60.0)
                                + ", celula " + operacaoSelecionada.getProgramacao().getCelula().getCodigo()
                                + ", eficiencia calculada " + eficienciaPacote
                                + "] para apontamento de produção.",
                        "APONTAMENTO DE PRODUÇÃO");
                dhInicioLancamentoOperacao = horaFimLancamento;
                qtdePecasOperacao = qtdePecasOperacao - pacote.getQtde();
            }
            //Save para os lançamentos
            daoLancamentosProducao.saveList(pacotesParaLancamento);
            
            //Check e set de lançado ou finalizado para a operação.
            if (operacaoSelecionada.getQtdePend() - qtdePecasLancamento > 0)
                operacaoPendenteColaboradorSelecionada.get().getProgramacao().setStatusProg(1);
            else {
                operacaoPendenteColaboradorSelecionada.get().getProgramacao().setStatusProg(2);
                ObservableList<SdProgramacaoPacote002> operacoesQuebradas = daoProgramacao.initCriteria()
                        .addPredicate("id.pacote", operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getPacote(), PredicateType.EQUAL)
                        .addPredicate("id.operacao.codorg", operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao().getCodorg(), PredicateType.EQUAL)
                        .addPredicate("id.ordem", operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOrdem(), PredicateType.EQUAL)
                        .addPredicate("id.setorOp.codigo", operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getSetorOp().getCodigo(), PredicateType.EQUAL)
                        .loadListByPredicate();
                for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesQuebradas) {
                    sdProgramacaoPacote002.setStatusProg(2);
                    daoProgramacao.update(sdProgramacaoPacote002);
                }
            }
            //Update da operação pendente
            daoProgramacao.update(operacaoPendenteColaboradorSelecionada.get().getProgramacao());
            operacaoPendenteColaboradorSelecionada.get().getProgramacao().getOperacoesAgrupadas().forEach(sdProgramacaoPacote002 -> {
                sdProgramacaoPacote002.setStatusProg(operacaoPendenteColaboradorSelecionada.get().getProgramacao().getStatusProg());
                try {
                    daoProgramacao.update(sdProgramacaoPacote002);
                } catch (SQLException e) {
                    e.printStackTrace();
                    LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                    GUIUtils.showException(e);
                }
            });
            
            //Calculando eficência da operação para exibição em tela
            BigDecimal eficienciaOperacao = new BigDecimal(pacotesParaLancamento.stream().collect(Collectors.averagingDouble(lancamento -> lancamento.getEficiencia().doubleValue())));
            //Calculando a produtividade do dia até a hora de lançamento
            VSdProdHoraColab vSdProdHoraColab = (VSdProdHoraColab) daoProdHoraColab.initCriteria().addPredicate("colaborador.codigo", colaboradorLancamento.getCodigo(), PredicateType.EQUAL).loadEntityByPredicate();
            Double eficienciaColab = vSdProdHoraColab.getTmpOperacao().doubleValue() /
                    (vSdProdHoraColab.getTmpDisponivel().doubleValue() - vSdProdHoraColab.getTmpIntervalo().doubleValue() - vSdProdHoraColab.getTmpDeduzEfic().doubleValue());
            Double produtividadeColab = vSdProdHoraColab.getTmpOperacao().doubleValue() /
                    (vSdProdHoraColab.getTmpDisponivel().doubleValue() - vSdProdHoraColab.getTmpIntervalo().doubleValue() - vSdProdHoraColab.getTmpDeduzProd().doubleValue());
            //Exibindo eficiências e produtividade para costureira
            GUIUtils.showMessageEficiencia(eficienciaOperacao.doubleValue(), eficienciaColab, produtividadeColab);
            
            
            //Removendo os pacotes lançados da lista de pacotes
            pacotesLancamentoOperacao.removeIf(pacote -> pacote.isSelected());
            //limpando tela
            pacotesParaLancamento.clear();
            lancamentosDoDia.clear();
            sdPacoteSelecionado.set(null); // alterado para leitura de of
            pacotesLancamentoOperacao.clear();
            operacoesPendenteOperador.clear();
            operacaoPendenteColaboradorSelecionada.set(null);
            limparSetup();
            tboxCodigoOperador.clear();
            tboxProgramacaoCarregada.clear();
            tboxProgramacaoCarregada.requestFocus();
            
            SysLogger.addFileLogApontamento("Finalizando apontamento de produção.",
                    "APONTAMENTO DE PRODUÇÃO");
        } catch (SQLException | IOException e) {
            SysLogger.addFileLogApontamento("Erro no lançamento de produção para o colaborador " + tboxCodigoOperador.getText()
                            + " na programação " + tboxOrdemProgramada.getText()
                            + " para a operação " + operacaoPendenteColaboradorSelecionada.get().getProgramacao().getId().getOperacao()
                            + "EXCEPTION: " + e.getMessage(),
                    "APONTAMENTO DE PRODUÇÃO");
            e.printStackTrace();
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    void tblOperacoesOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() >= 2) {
            if (tblOperacoes.getSelectionModel().getSelectedItem().getProgramacao().getOperacoesAgrupadas().size() == 0)
                return;
            
            try {
                new ScenePcpOperacoesAgrupadasController(Modals.FXMLWindow.ConsultaApontamentoOperacoesAgrupadas, tblOperacoes.getSelectionModel().getSelectedItem().getProgramacao());
            } catch (IOException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }
    
    /**
     * FXML Controller class
     *
     * @author cristiano.diego
     */
    public static class ScenePcpLancamentoParadaController extends Modals implements Initializable {
        
        
        private String codigoDigitado = null;
        // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
        private GenericDao<SdColaborador> daoColaborador = new GenericDaoImpl<>(SdColaborador.class);
        private GenericDao<SdTiposParada001> daoTiposParada = new GenericDaoImpl<>(SdTiposParada001.class);
        private GenericDao<SdParadasTurno001> daoParadasTurno = new GenericDaoImpl<>(SdParadasTurno001.class);
        private GenericDao<SdLancamentoParada001> daoLancamentosParada = new GenericDaoImpl<>(SdLancamentoParada001.class);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Components">
        @FXML
        private Button btnCancelar;
        @FXML
        private Button btnParar;
        @FXML
        private TextField tboxCodigoOperador;
        @FXML
        private TextArea tboxDescricaoMotivo;
        @FXML
        private TextField tboxMotivo;
        @FXML
        private Button btnTecladoMotivo;
        @FXML
        private Button btnTempoParada;
        //</editor-fold>
        
        private final ObjectProperty<SdColaborador> colaboradorParado = new SimpleObjectProperty<>();
        private final ObjectProperty<SdTiposParada001> tipoParada = new SimpleObjectProperty<>();
        private final BooleanProperty operadorParado = new SimpleBooleanProperty(false);
        private SdLancamentoParada001 paradaAberta = null;
        private Integer tempoParadaDefinida = 0;
        
        
        
        public ScenePcpLancamentoParadaController(FXMLWindow file, String codigoDigitado) throws IOException {
            super(file);
            this.codigoDigitado = codigoDigitado;
            super.show(this);
        }
        
        /**
         * Initializes the controller class.
         */
        @Override
        public void initialize(URL url, ResourceBundle rb) {
            // <editor-fold defaultstate="collapsed" desc="Inicialização dos componentes FXML">
            btnParar.disableProperty().bind(colaboradorParado.isNull().or(tipoParada.isNull().and(operadorParado.not())));
            btnTempoParada.disableProperty().bind(colaboradorParado.isNull().or(tipoParada.isNull().and(operadorParado.not())));
            btnParar.textProperty().bind(Bindings.when(operadorParado).then("RETORNAR").otherwise("PARAR"));
            tboxMotivo.disableProperty().bind(operadorParado);
            btnTecladoMotivo.disableProperty().bind(operadorParado);
            //</editor-fold>
            
            if (!codigoDigitado.isEmpty()) {
                tboxCodigoOperador.setText(codigoDigitado);
                try {
                    colaboradorParado.set((SdColaborador) daoColaborador.initCriteria()
                            .addPredicateEq("codigo", tboxCodigoOperador.getText())
                            .loadEntityByPredicate());
                    if (colaboradorParado.isNull().get()) {
                        GUIUtils.showMessageFullScreenWithButtonClose("NÃO FOI ENCONTRADO UM OPERADOR COM O CÓDIGO DIGITADO!",
                                Alert.AlertType.ERROR).showAndWait();
                        tboxCodigoOperador.clear();
                        tboxCodigoOperador.requestFocus();
                        return;
                    }
                    
                    paradaAberta = (SdLancamentoParada001) daoLancamentosParada.initCriteria()
                            .addPredicateEqPkEmbedded("id", "colaborador", colaboradorParado.get().getCodigo(), false)
                            .addPredicateIsNull("dhFim")
                            .loadEntityByPredicate();
                    operadorParado.set(paradaAberta != null);
                    tboxDescricaoMotivo.setText(operadorParado.get() ? paradaAberta.getId().getMotivo().getDescricao() : "MOTIVO PARADA");
                } catch (SQLException e) {
                    e.printStackTrace();
                    GUIUtils.showException(e);
                }
            }
            
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    if (colaboradorParado.isNull().get()) {
                        tboxCodigoOperador.requestFocus();
                    } else {
                        tboxMotivo.requestFocus();
                    }
                }
            });
        }
        
        private long verificaParadaEmIntervalo(Integer codigoColaborador, LocalDateTime dhInicio, LocalDateTime dhFim) throws SQLException {
            // Verifica se dh fim está em um intervalo
            Long tempoIntervalos = 0L;
            if (colaboradorParado.get() != null) {
                ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                        .getIntervalosTurno(String.valueOf(colaboradorParado.get().getCodigoTurno()));
                
                for (Map<String, Object> intervalo : intervalos) {
                    if (intervalo.get("tipo").equals("inter"))
                        continue;
                    
                    LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
                    LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
                    LocalDateTime horarioInicioLancamento = LocalDateTime.of(
                            inicioIntervalo.getYear(),
                            inicioIntervalo.getMonth(),
                            inicioIntervalo.getDayOfMonth(),
                            dhInicio.getHour(),
                            dhInicio.getMinute());
                    LocalDateTime horarioFimLancamento = LocalDateTime.of(
                            inicioIntervalo.getYear(),
                            inicioIntervalo.getMonth(),
                            dhFim.getDayOfMonth() != dhInicio.getDayOfMonth() ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                            dhFim.getHour(),
                            dhFim.getMinute());
                    Long tempoIntervalo = (Long) intervalo.get("intervalo");
                    
                    if (horarioInicioLancamento.isBefore(inicioIntervalo) && horarioFimLancamento.isAfter(fimIntervalo)) {
                        tempoIntervalos += tempoIntervalo;
                    } else if ((horarioInicioLancamento.isBefore(inicioIntervalo) && horarioFimLancamento.isAfter(inicioIntervalo)) && horarioFimLancamento.isBefore(fimIntervalo)) {
                        tempoIntervalos += ChronoUnit.MINUTES.between(inicioIntervalo, horarioFimLancamento);
                    } else if ((horarioInicioLancamento.isAfter(inicioIntervalo) && horarioInicioLancamento.isBefore(fimIntervalo)) && horarioFimLancamento.isAfter(fimIntervalo)) {
                        tempoIntervalos += ChronoUnit.MINUTES.between(horarioInicioLancamento, fimIntervalo);
                    } else if (horarioFimLancamento.isAfter(inicioIntervalo) && horarioFimLancamento.isBefore(fimIntervalo)) {
                        tempoIntervalos += 0L;
                    }
                }
            }
            return tempoIntervalos;
        }
        
        @FXML
        private void btnCancelarOnAction(ActionEvent event) {
            Stage main = (Stage) btnCancelar.getScene().getWindow();
            main.close();
        }
        
        @FXML
        private void btnPararOnAction(ActionEvent event) {
            try {
                if (operadorParado.get()) {
                    paradaAberta.setDhFim(LocalDateTime.now());
                    paradaAberta.setTempo(new BigDecimal((ChronoUnit.SECONDS.between(paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()) / 60.0)
                            - (verificaParadaEmIntervalo(paradaAberta.getId().getColaborador(), paradaAberta.getId().getDhInicio(), paradaAberta.getDhFim()))));
                    
                    daoLancamentosParada.update(paradaAberta);
                    operadorParado.set(false);
                } else {
                    LocalDateTime dhInicioParada = LocalDateTime.now();
                    SdLancamentoParada001 paradaOperador = new SdLancamentoParada001(
                            new SdLancamentoParada001PK(Integer.parseInt(tboxCodigoOperador.getText()), tipoParada.get(), dhInicioParada),
                            null,
                            null
                    );
                    if(tempoParadaDefinida != 0) {
                        paradaOperador.setTempo(BigDecimal.valueOf(tempoParadaDefinida));
                        ObservableList<SdParadasTurno001> paradasTurno = daoParadasTurno.initCriteria()
                                .addPredicateEq("turno", 21)
                                .loadListByPredicate();
                        int tempoEmIntervalo = paradasTurno.stream()
                                .filter(sdParadasTurno001 -> (sdParadasTurno001.getInicio().toLocalTime().isAfter(paradaOperador.getId().getDhInicio().toLocalTime())
                                        || sdParadasTurno001.getInicio().toLocalTime().equals(paradaOperador.getId().getDhInicio().toLocalTime()))
                                        && (sdParadasTurno001.getFim().toLocalTime().isBefore(paradaOperador.getId().getDhInicio().plusMinutes(tempoParadaDefinida).toLocalTime())
                                        || sdParadasTurno001.getFim().toLocalTime().equals(paradaOperador.getId().getDhInicio().plusMinutes(tempoParadaDefinida).toLocalTime())))
                                .mapToInt(SdParadasTurno001::getTempoIntervalo)
                                .sum();
                        paradaOperador.setDhFim(paradaOperador.getId().getDhInicio().plusMinutes(tempoParadaDefinida + tempoEmIntervalo));
                    }
                    daoLancamentosParada.save(paradaOperador);
                }
                
                GUIUtils.showMessageNotification("Lançamento de Produção", "LANÇAMENTO DE PARADA REALIZADO COM SUCESSO!", Pos.CENTER, 5);
                
                Stage main = (Stage) btnParar.getScene().getWindow();
                main.close();
            } catch (SQLException e) {
                e.printStackTrace();
                GUIUtils.showException(e);
            }
            
        }
        
        @FXML
        private void btnTecladoMotivoOnAction(ActionEvent event) {
            try {
                tipoParada.set((SdTiposParada001) daoTiposParada.initCriteria()
                        .addPredicateEq("codigo", tboxMotivo.getText())
                        .loadEntityByPredicate());
            
                if (tipoParada.isNull().get()) {
                    GUIUtils.showMessageFullScreenWithButtonClose("MOTIVO DE PARADA NÃO ENCONTRADO NA BASE DE DADOS!",
                            Alert.AlertType.ERROR).showAndWait();
                    tboxMotivo.clear();
                    tboxMotivo.requestFocus();
                    return;
                } else if (!tipoParada.get().isAtivo()) {
                    GUIUtils.showMessageFullScreenWithButtonClose("MOTIVO "+tipoParada.get().getDescricao()+" ESTÁ INATIVO, INFORME UM MOTIVO ATIVO!",
                            Alert.AlertType.ERROR).showAndWait();
                    tboxMotivo.clear();
                    tboxMotivo.requestFocus();
                    return;
                }
            
                tboxDescricaoMotivo.setText(tipoParada.get().getDescricao());
            } catch (SQLException e) {
                e.printStackTrace();
                GUIUtils.showException(e);
            }
        }
        
        @FXML
        private void btnTempoParadaOnAction(ActionEvent event) {
            if (tipoParada.get() == null){
                GUIUtils.showMessageFullScreenWithButtonClose("VOCÊ DEVE INFORMAR PRIMEIRO O MOTIVO DA PARADA!",
                        Alert.AlertType.WARNING).showAndWait();
                return;
            }
            
            if(!tipoParada.get().isComTempo()){
                GUIUtils.showMessageFullScreenWithButtonClose("A PARADA SELECIONADA NÃO PODE SER DEFINIDA TEMPO DE FINALIZAÇÃO!",
                        Alert.AlertType.WARNING).showAndWait();
                return;
            }
            try {
                tempoParadaDefinida = new NumericKeyboardController(FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.INTEGER)
                                .integerKeyboard
                                .getValue();
            } catch (Exception e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "${CLASS_NAME}:${METHOD_NAME}");
                GUIUtils.showException(e);
            }
        }
        
        @FXML
        void tboxCodigoOperadorOnKeyReleased(KeyEvent event) {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    colaboradorParado.set((SdColaborador) daoColaborador.initCriteria()
                            .addPredicateEq("codigo", tboxCodigoOperador.getText())
                            .loadEntityByPredicate());
                    
                    if (colaboradorParado.isNull().get()) {
                        GUIUtils.showMessageFullScreenWithButtonClose("NÃO FOI ENCONTRADO UM OPERADOR COM O CÓDIGO DIGITADO!",
                                Alert.AlertType.ERROR).showAndWait();
                        tboxCodigoOperador.clear();
                        tboxCodigoOperador.requestFocus();
                        return;
                    }
                    
                    paradaAberta = (SdLancamentoParada001) daoLancamentosParada.initCriteria()
                            .addPredicateEqPkEmbedded("id", "colaborador", colaboradorParado.get().getCodigo(), false)
                            .addPredicateIsNull("dhFim")
                            .loadEntityByPredicate();
                    operadorParado.set(paradaAberta != null);
                    tboxDescricaoMotivo.setText(operadorParado.get() ? paradaAberta.getId().getMotivo().getDescricao() : "MOTIVO PARADA");
                    tboxMotivo.requestFocus();
                } catch (SQLException e) {
                    e.printStackTrace();
                    GUIUtils.showException(e);
                }
            }
        }
        
        @FXML
        void tboxMotivoOnKeyReleased(KeyEvent event) {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    tipoParada.set((SdTiposParada001) daoTiposParada.initCriteria()
                            .addPredicateEq("codigo", tboxMotivo.getText())
                            .loadEntityByPredicate());
            
                    if (tipoParada.isNull().get()) {
                        GUIUtils.showMessageFullScreenWithButtonClose("MOTIVO DE PARADA NÃO ENCONTRADO NA BASE DE DADOS!",
                                Alert.AlertType.ERROR).showAndWait();
                        tboxMotivo.clear();
                        tboxMotivo.requestFocus();
                        return;
                    } else if (!tipoParada.get().isAtivo()) {
                        GUIUtils.showMessageFullScreenWithButtonClose("MOTIVO "+tipoParada.get().getDescricao()+" ESTÁ INATIVO, INFORME UM MOTIVO ATIVO!",
                                Alert.AlertType.ERROR).showAndWait();
                        tboxMotivo.clear();
                        tboxMotivo.requestFocus();
                        return;
                    }
            
                    tboxDescricaoMotivo.setText(tipoParada.get().getDescricao());
                } catch (SQLException e) {
                    e.printStackTrace();
                    GUIUtils.showException(e);
                }
            }
        }
        
    }
}

