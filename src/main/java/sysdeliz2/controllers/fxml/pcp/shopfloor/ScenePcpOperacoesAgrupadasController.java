package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdProgramacaoPacote002;
import sysdeliz2.models.sysdeliz.SdProgramacaoPacote002PK;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ScenePcpOperacoesAgrupadasController extends Modals implements Initializable {
    
    private final SdProgramacaoPacote002 operacaoMae;
    private final GenericDao<SdProgramacaoPacote002> daoProgramacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdProgramacaoPacote002.class);
    private final ListProperty<SdProgramacaoPacote002> operacoesAgrupas = new SimpleListProperty<>();
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Components">
    @FXML
    private Button btnFechar;
    @FXML
    private TextField tboxOrdemOperacaoMae;
    @FXML
    private TextField tboxOperacaoMae;
    @FXML
    private TextField tboxTempoOperacaoMae;
    @FXML
    private TableView<SdProgramacaoPacote002> tblOperacoesAgrupadas;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnSeqColaborador;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnCodigoOperacao;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnDescOperacao;
    @FXML
    private TableColumn<SdProgramacaoPacote002, BigDecimal> clnTempoOperacao;
    // </editor-fold>
    
    
    public ScenePcpOperacoesAgrupadasController(FXMLWindow file, SdProgramacaoPacote002 operacaoMae) throws IOException {
        super(file);
        this.operacaoMae = operacaoMae;
        super.show(this);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.initializeComponentes();
        try {
            exibirDadosProgramacao(this.operacaoMae);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    private void initializeComponentes() {
        tblOperacoesAgrupadas.itemsProperty().bind(operacoesAgrupas);
        clnSeqColaborador.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toIntegerFormat(item.getOrdem()));
                    }
                }
            };
        });
        clnCodigoOperacao.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(String.valueOf(item.getOperacao().getCodorg()));
                    }
                }
            };
        });
        clnDescOperacao.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(item.getOperacao().getDescricao());
                    }
                }
            };
        });
        clnTempoOperacao.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                    }
                }
            };
        });
    }
    
    private void exibirDadosProgramacao(SdProgramacaoPacote002 operacaoMaeConsulta) throws SQLException {
        if (operacaoMaeConsulta != null) {
            tboxOrdemOperacaoMae.setText(String.valueOf(operacaoMaeConsulta.getId().getOrdem()));
            tboxOperacaoMae.setText(operacaoMaeConsulta.getId().getOperacao().toString());
            tboxTempoOperacaoMae.setText(StringUtils.toDecimalFormat(operacaoMaeConsulta.getTempoOp().doubleValue(),4));
    
            operacoesAgrupas.set(daoProgramacao.initCriteria()
                    .addPredicateEqPkEmbedded("id","pacote", operacaoMaeConsulta.getId().getPacote(), false)
                    .addPredicateEqPkEmbedded("id","setorOp", operacaoMaeConsulta.getId().getSetorOp().getCodigo(), false)
                    .addPredicateEq("agrupador", operacaoMaeConsulta.getId().getOperacao().getCodorg())
                    .loadListByPredicate()
            );
            tblOperacoesAgrupadas.refresh();
        }
    }
    
    //------------------------------- FXML Actions -------------------------------------------------
    
    @FXML
    void btnFecharOnAction(ActionEvent event) {
        Stage main = (Stage) btnFechar.getScene().getWindow();
        main.close();
    }
}

