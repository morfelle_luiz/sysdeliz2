/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.cadastros.SceneCadastroColaboradorController;
import sysdeliz2.controllers.fxml.procura.*;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Of1001;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.view.VSdOfsParaProgramar;
import sysdeliz2.models.view.ViewOcupacaoColaborador;
import sysdeliz2.utils.*;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.sys.EnumsController;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
@Deprecated
public class SceneProgramaProducaoOfController implements Initializable {
    
    private final Image imgAlertaProgramacao = new Image(getClass().getResource("/images/icons/buttons/warning (1).png").toExternalForm());
    private ToggleSwitch toggleRed = new ToggleSwitch(16, 40, "#f5c6cb");
    private ToggleSwitch toggleGreen = new ToggleSwitch(16, 40, "#c3e6cb");
    private ToggleSwitch toggleBlue = new ToggleSwitch(16, 40, "#b8daff");
    private ToggleSwitch toggleYellow = new ToggleSwitch(16, 40, "#ffeeba");
    
    private final ObjectProperty<SdProgramacaoOf001> programacaoProducaoOf = new SimpleObjectProperty<>();
    private Of1001 ofParaProgramacao = null;
    private final ListProperty<VSdOfsParaProgramar> ofsParaProgramar = new SimpleListProperty<VSdOfsParaProgramar>();
    private final ObjectProperty<VSdOfsParaProgramar> ofParaProgramarSelecionada = new SimpleObjectProperty<VSdOfsParaProgramar>();
    
    private final IntegerProperty totalPacotesOf = new SimpleIntegerProperty();
    private final IntegerProperty totalPacotesLancamentoOf = new SimpleIntegerProperty();
    private final ListProperty<SdPacote001> listaDePacotes = new SimpleListProperty<SdPacote001>(FXCollections.observableArrayList());
    private final ListProperty<SdPacote001> listaDePacotesLancamento = new SimpleListProperty<SdPacote001>(FXCollections.observableArrayList());
    private final ObjectProperty<SdPacote001> pacoteSelecionado = new SimpleObjectProperty<SdPacote001>();
    private final BooleanProperty emVisualizacao = new SimpleBooleanProperty(false);
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty emProgramacao = new SimpleBooleanProperty(false);
    private final BooleanProperty programacaoComLancametos = new SimpleBooleanProperty(false);
    private List<Integer> setoresComLancamento = new ArrayList<>();
    private final BooleanProperty btnIniciarInativo = new SimpleBooleanProperty(false);
    private Double ibPadraoProgramacao = 0.33;
    
    private List<SdOperRoteiroOrganize001> roteiroOperacoesProduto = null;
    private List<SdProgramacaoPacote001> operacoesParaProgramacao = new ArrayList<>();
    private List<SdProgramacaoPacote001> operacoesBase = new ArrayList<>();
    private final ListProperty<SdSetorOp001> setoresOperacaoRoteiroProduto = new SimpleListProperty<>();
    private List<SdProgramacaoPacote001> operacoesSetorSelecionado = new ArrayList<>();
    private final ListProperty<SdProgramacaoPacote001> operacoesSetorSelecionadoParaProgramacao = new SimpleListProperty<>(FXCollections.observableArrayList()); //lista para exibição em tableview
    
    private SdSetorOp001 setorOperacaoSelecionado = null;
    private final SdCelula celulaSelecionada = new SdCelula();
    private SdCelula celulaSetorOperacaoSelecionada = null;
    private SdColaborador colaboradorSemPolivalencia = null;
    private final ListProperty<ViewOcupacaoColaborador> colaboradoresParaProgramacao = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdProgramacaoPacote001> ocupacaoColaborador = new SimpleListProperty<SdProgramacaoPacote001>(FXCollections.observableArrayList());
    
    // <editor-fold defaultstate="collapsed" desc="FXML components">
    @FXML
    private Button btnExcluirProgramacao;
    @FXML
    private TabPane tabsWindow;
    @FXML
    private Tab tabListagem;
    @FXML
    private Tab tabProgramacao;
    @FXML
    private TableView<SdPacote001> tblPacotesLancamento;
    @FXML
    private TableView<VSdOfsParaProgramar> tblOfParaProgramacao;
    @FXML
    private TableColumn<VSdOfsParaProgramar, String> clnStatusMaterial;
    @FXML
    private TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar> clnAcoes;
    @FXML
    private TableView<SdProgramacaoPacote001> tblProgramacao;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Boolean> clnAlerta;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Double> clnTempoOperacao;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Double> clnPercIb;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Integer> clnOrdem;
    @FXML
    private TableColumn<SdProgramacaoPacote001, String> clnIndependente;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Integer> clnGrupo;
    @FXML
    private TableColumn<SdProgramacaoPacote001, LocalDateTime> clnInicio;
    @FXML
    private TableColumn<SdProgramacaoPacote001, LocalDateTime> clnIb;
    @FXML
    private TableColumn<SdProgramacaoPacote001, LocalDateTime> clnFim;
    @FXML
    private TableColumn<SdProgramacaoPacote001, SdColaborador> clnCostureira;
    @FXML
    private TableColumn<SdProgramacaoPacote001, SdEquipamentosOrganize001> clnEquipamento;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Integer> clnQtdePecas;
    @FXML
    private TableColumn<SdProgramacaoPacote001, Double> clnTempoTotal;
    @FXML
    private TableColumn<SdProgramacaoPacote001, SdOperacaoOrganize001> clnOperacao;
    @FXML
    private TableView<ViewOcupacaoColaborador> tblColaboradoresOperacao;
    @FXML
    private TableView<SdProgramacaoPacote001> tblOcupacaoColaborador;
    @FXML
    private TableColumn<SdProgramacaoPacote001, SdCelula> clnCelula;
    @FXML
    private TableColumn<SdProgramacaoPacote001, SdOperacaoOrganize001> clnOperacaoOcupacao;
    @FXML
    private TableColumn<SdProgramacaoPacote001, LocalDateTime> clnHorarioInicioOcupacao;
    @FXML
    private TableColumn<SdProgramacaoPacote001, LocalDateTime> clnHorarioFimOcupacao;
    @FXML
    private Label lbRegistersCount;
    @FXML
    private TextField tboxInfosPacote;
    @FXML
    private Button btnGuardarProgramacao;
    @FXML
    private TextField tboxOperacaoSelecionada;
    @FXML
    private TextField tboxQtdeOperacaoSelecionada;
    @FXML
    private TextField tboxTempoOperacaoSelecionada;
    @FXML
    private TextField tboxTempoTotalOperacaoSelecionada;
    @FXML
    private TextField tboxEquipamentoOperacaoSelecionada;
    @FXML
    private Accordion pagsInformacoes;
    @FXML
    private TitledPane paneInformacoesCostureira;
    @FXML
    private TitledPane paneInformacoesCelula;
    @FXML
    private TitledPane paneConfiguracaoProgramacao;
    @FXML
    private TextField tboxNumeroOf;
    @FXML
    private TextField tboxQtdeOf;
    @FXML
    private TextField tboxFamiliaOf;
    @FXML
    private TextField tboxProdutoOf;
    @FXML
    private TextField tboxDescricaoProduto;
    @FXML
    private RadioButton rbtnReferencia;
    @FXML
    private ToggleGroup tipoPacote;
    @FXML
    private RadioButton rbtnCor;
    @FXML
    private RadioButton rbtnTamanho;
    @FXML
    private TextField tboxQtdePacotes;
    @FXML
    private TextField tboxIbPadraoProgramacao;
    @FXML
    private TextField tboxNumeroOfCopia;
    @FXML
    private Button btnProcurarPacotesOf;
    @FXML
    private ComboBox<SdPacote001> cboxCopiarPacote;
    @FXML
    private ComboBox<SdPacote001> cboxPacote;
    @FXML
    private TextField tboxPecasPacote;
    @FXML
    private Button btnIniciarProgramacao;
    @FXML
    private Button btnConfirmarProgramacao;
    @FXML
    private Button btnCancelarProgramacao;
    @FXML
    private ComboBox<SdSetorOp001> cboxSetoresOperacao;
    @FXML
    private TextField tboxCelula;
    @FXML
    private Label lbDescricaoCelula;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private ContextMenu contextMenuPreparacao1;
    @FXML
    private DatePicker tboxDtInicioEnvio;
    @FXML
    private DatePicker tboxDtFimEnvio;
    @FXML
    private DatePicker tboxDtInicioRetorno;
    @FXML
    private DatePicker tboxDtFimRetorno;
    @FXML
    private DatePicker tboxDtInicioProgramacao;
    @FXML
    private DatePicker tboxDtFimProgramacao;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroPeriodo;
    @FXML
    private Button btnProcurarPeriodo;
    @FXML
    private TextField tboxFiltroFamilia;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroSetor;
    @FXML
    private Label lbFiltroOfAtrasada;
    @FXML
    private Label lbFiltroOfAAtrasar;
    @FXML
    private Label lbFiltroOfEmDia;
    @FXML
    private Label lbFiltroOfFinalizada;
    @FXML
    private Button btnCarregarOfs;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnImprimirProgramacao;
    @FXML
    private VBox paneConfigPacote;
    @FXML
    private RadioButton rbtnReferenciaLancamento;
    @FXML
    private ToggleGroup pacoteLancamento;
    @FXML
    private RadioButton rbtnCorLancamento;
    @FXML
    private RadioButton rbtnTamanhoLancamento;
    @FXML
    private TextField tboxQtdePacotesLancamento;
    @FXML
    private VBox paneConfigPacoteLancamento;
    //</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ofsParaProgramar.set(DAOFactory.getVSdOfsParaProgramarDAO().getOfsParaProgramar("PLANALTO"));
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::initialize");
            GUIUtils.showException(ex);
        }
        
        // <editor-fold defaultstate="collapsed" desc="Inicialização dos componentes FXML">
        initializeComponentesFxml();
        celulaSelecionada.clear();
        celulaSetorOperacaoSelecionada = null;
        // </editor-fold>
    }
    
    /**
     * Inicialização dos componentes de tela FXML
     */
    private void initializeComponentesFxml() {
        // <editor-fold defaultstate="collapsed" desc="Pane CONFIG PACOTE">
        paneConfigPacote.disableProperty().bind(totalPacotesOf.lessThan(listaDePacotes.getSize()).or(emVisualizacao).or(emEdicao));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pane CONFIG PACOTE LANCAMENTO">
        paneConfigPacoteLancamento.disableProperty().bind(totalPacotesLancamentoOf.lessThan(listaDePacotesLancamento.getSize()).or(emVisualizacao).or(pacoteSelecionado.isNull()));
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ToggleSwitch FILTRO">
        lbFiltroOfAtrasada.setGraphic(toggleRed);
        lbFiltroOfAAtrasar.setGraphic(toggleYellow);
        lbFiltroOfFinalizada.setGraphic(toggleBlue);
        lbFiltroOfEmDia.setGraphic(toggleGreen);
        toggleRed.switchedOnProperty().set(true);
        toggleYellow.switchedOnProperty().set(true);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(true);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TextField QTDE_PACOTES">
        tboxQtdePacotes.textProperty().bind(totalPacotesOf.asString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField QTDE_PACOTES LANCAMENTO">
        tboxQtdePacotesLancamento.textProperty().bind(totalPacotesLancamentoOf.asString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField IB PADRAO">
        MascarasFX.mascaraNumero(tboxIbPadraoProgramacao);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField CELULA">
        tboxCelula.disableProperty().bind(emVisualizacao);
        btnProcurarCelula.disableProperty().bind(emVisualizacao);
//        bindCelulaSelecionada(this.celulaSetorOperacaoSelecionada);
        tboxCelula.textProperty().bind(celulaSelecionada.codigoProperty().asString());
        lbDescricaoCelula.textProperty().bind(celulaSelecionada.descricaoProperty());
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField FILTRO">
        TextFieldUtils.upperCase(tboxFiltroFamilia);
        TextFieldUtils.upperCase(tboxFiltroOf);
        TextFieldUtils.upperCase(tboxFiltroPeriodo);
        TextFieldUtils.upperCase(tboxFiltroProduto);
        TextFieldUtils.upperCase(tboxFiltroSetor);
        TextFieldUtils.inSql(tboxFiltroFamilia);
        TextFieldUtils.inSql(tboxFiltroOf);
        TextFieldUtils.inSql(tboxFiltroPeriodo);
        TextFieldUtils.inSql(tboxFiltroProduto);
        TextFieldUtils.inSql(tboxFiltroSetor);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ComboBox PACOTE">
        cboxPacote.itemsProperty().bind(listaDePacotes);
        cboxPacote.setCellFactory(new Callback<ListView<SdPacote001>, ListCell<SdPacote001>>() {
            @Override
            public ListCell<SdPacote001> call(ListView<SdPacote001> param) {
                final ListCell<SdPacote001> cell = new ListCell<SdPacote001>() {
                    @Override
                    public void updateItem(SdPacote001 item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getDescricao());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboxPacote.setConverter(new StringConverter<SdPacote001>() {
            @Override
            public String toString(SdPacote001 object) {
                return object.getDescricao();
            }
            
            @Override
            public SdPacote001 fromString(String string) {
                return null;
            }
        });
        cboxPacote.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tboxPecasPacote.setText(newValue.qtdeProperty().getValue().toString());
                tboxInfosPacote.setText(newValue.getDescricao());
                pacoteSelecionado.set(newValue);
                if (emVisualizacao.or(emEdicao.and(pacoteSelecionado.get().pacoteProgramadoProperty())).get()) {
                    btnIniciarInativo.set(true);
                    carregaSetoresParaLeitura();
                    return;
                }
                rbtnReferenciaLancamento.setSelected(true);
                
                btnIniciarInativo.set(false);
                
                roteiroOperacoesProduto = null;
                operacoesParaProgramacao.clear();
                operacoesBase.clear();
                setoresOperacaoRoteiroProduto.clear();
                operacoesSetorSelecionadoParaProgramacao.clear();
                setorOperacaoSelecionado = null;
                celulaSelecionada.clear();
                celulaSetorOperacaoSelecionada = null;
                colaboradorSemPolivalencia = null;
                colaboradoresParaProgramacao.clear();
                ocupacaoColaborador.clear();
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ComboBox SETOR OPERACAO">
        cboxSetoresOperacao.itemsProperty().bind(setoresOperacaoRoteiroProduto);
        cboxSetoresOperacao.setCellFactory(new Callback<ListView<SdSetorOp001>, ListCell<SdSetorOp001>>() {
            @Override
            public ListCell<SdSetorOp001> call(ListView<SdSetorOp001> param) {
                final ListCell<SdSetorOp001> cell = new ListCell<SdSetorOp001>() {
                    @Override
                    public void updateItem(SdSetorOp001 item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getDescricao());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboxSetoresOperacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
//                if (!operacoesSetorSelecionado.isEmpty()
//                        && hasNullOperador(operacoesSetorSelecionado)
//                        && cboxSetoresOperacao.getItems().indexOf(newValue) > cboxSetoresOperacao.getItems().indexOf(oldValue)) {
//                    GUIUtils.showMessage("Você precisa definir os operadores em todas as operações do setor.", Alert.AlertType.WARNING);
//                    if (setorOperacaoSelecionado == oldValue) {
//                        cboxSetoresOperacao.getSelectionModel().select(oldValue);
//                    }
//                    return;
//                } else {
                this.selectSetorOperacao(newValue);
                this.setorOperacaoSelecionado = newValue;
//                }
            }
        });
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TitlePane PAINEL CONFIGURACAO">
        paneConfiguracaoProgramacao.disableProperty().bind(emProgramacao);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Label REGISTERS COUNT">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(ofsParaProgramar.sizeProperty()).concat(" registros"));
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="RadioButtons TIPO_PACOTE">
        rbtnReferencia.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteReferencia();
                programacaoProducaoOf.get().setTipoPacote("R");
                rbtnReferenciaLancamento.setDisable(false);
                rbtnCorLancamento.setDisable(false);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        rbtnCor.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteCor();
                programacaoProducaoOf.get().setTipoPacote("C");
                rbtnReferenciaLancamento.setDisable(true);
                rbtnCorLancamento.setDisable(false);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        rbtnTamanho.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteTamanho();
                programacaoProducaoOf.get().setTipoPacote("T");
                rbtnReferenciaLancamento.setDisable(true);
                rbtnCorLancamento.setDisable(true);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="RadioButtons TIPO PACOTE LANCAMENTO">
        rbtnReferenciaLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoReferencia();
                pacoteSelecionado.get().setTipoLancamento("R");
            }
        });
        rbtnCorLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoCor();
                pacoteSelecionado.get().setTipoLancamento("C");
            }
        });
        rbtnTamanhoLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoTamanho();
                pacoteSelecionado.get().setTipoLancamento("T");
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button INICIAR_PROGRAMACAO">
        btnIniciarProgramacao.disableProperty().bind(emProgramacao
                .or(programacaoProducaoOf.isNull())
                .or(listaDePacotes.emptyProperty())
                .or(btnIniciarInativo)
                .or(emVisualizacao));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button CONFIRMA_PROGRAMACAO">
        btnConfirmarProgramacao.disableProperty().bind(emProgramacao.not().and(emEdicao.not()));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button PROCURAR CÉLULA">
        btnProcurarCelula.disableProperty().bind(emVisualizacao);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button EXCLUIR PROGRAMACAO">
        btnExcluirProgramacao.disableProperty().bind(emEdicao.not());
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView OFS PARA PROGRAMACAO">
        tblOfParaProgramacao.itemsProperty().bind(ofsParaProgramar);
        tblOfParaProgramacao.setRowFactory(tv -> new TableRow<VSdOfsParaProgramar>() {
            @Override
            protected void updateItem(VSdOfsParaProgramar item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (item.getStatusOf().equals("R")) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else if (item.getStatusOf().equals("Y")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatusOf().equals("G")) {
                    clearStyle();
                    getStyleClass().add("table-row-success");
                } else if (item.getStatusOf().equals("B")) {
                    clearStyle();
                    getStyleClass().add("table-row-info");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-success");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-info");
            }
            
        });
        tblOfParaProgramacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                ofParaProgramarSelecionada.set(newValue);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PROGRAMACAO">
        tblProgramacao.itemsProperty().bind(operacoesSetorSelecionadoParaProgramacao);
        tblProgramacao.editableProperty().bind(emVisualizacao.not().or(emProgramacao).or(emEdicao));
        tblProgramacao.setRowFactory(tv -> new TableRow<SdProgramacaoPacote001>() {
            @Override
            protected void updateItem(SdProgramacaoPacote001 item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null || empty) {
                    clearStyle();
                } else if (item.getProgramacaoApontada()) {
                    clearStyle();
                    getStyleClass().add("table-row-secundary");
                } else if (item.isHasConflito() && item.getAgrupador() == 0) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else if (item.getQuebra() > 1 && item.isQuebraManual()) {
                    clearStyle();
                    getStyleClass().add("table-row-info");
                } else if (item.getQuebra() > 1) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-info");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-secundary");
            }
            
        });
        tblProgramacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.bindProgramacao(newValue);
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::initializeComponentesFxml");
                GUIUtils.showException(ex);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView COLABORADOR">
        tblColaboradoresOperacao.itemsProperty().bind(colaboradoresParaProgramacao);
        tblColaboradoresOperacao.setRowFactory(tv -> new TableRow<ViewOcupacaoColaborador>() {
            @Override
            protected void updateItem(ViewOcupacaoColaborador item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    clearStyle();
                } else if (item.getStatusOcupacao().equals("Y")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatusOcupacao().equals("G")) {
                    clearStyle();
                    getStyleClass().add("table-row-success");
                } else if (item.getStatusOcupacao().equals("R")) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else if (item.getStatusOcupacao().equals("B")) {
                    clearStyle();
                    getStyleClass().add("table-row-primary");
                } else if (item.getStatusOcupacao().equals("D")) {
                    clearStyle();
                    getStyleClass().add("table-row-secundary");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-primary");
                getStyleClass().remove("table-row-success");
                getStyleClass().remove("table-row-secundary");
            }
            
        });
        tblColaboradoresOperacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                bindColaboradorSelecionado(newValue);
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::initializeComponentesFxml");
                GUIUtils.showException(ex);
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView OCUPACAO COLABORADOR">
        tblOcupacaoColaborador.itemsProperty().bind(ocupacaoColaborador);
        // <editor-fold defaultstate="collapsed" desc="ColumnTableView COLUNAS">
        // <editor-fold defaultstate="collapsed" desc="Column INICIO">
        clnHorarioInicioOcupacao.setCellFactory(
                new Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>>() {
                    @Override
                    public TableCell<SdProgramacaoPacote001, LocalDateTime> call(
                            TableColumn<SdProgramacaoPacote001, LocalDateTime> param) {
                        return new TableCell<SdProgramacaoPacote001, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                
                                if (item != null && !empty) {
                                    setText(item.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column FIM">
        clnHorarioFimOcupacao.setCellFactory(
                new Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>>() {
                    @Override
                    public TableCell<SdProgramacaoPacote001, LocalDateTime> call(
                            TableColumn<SdProgramacaoPacote001, LocalDateTime> param) {
                        return new TableCell<SdProgramacaoPacote001, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                
                                if (item != null && !empty) {
                                    setText(item.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PACOTES LANCAMENTO">
        tblPacotesLancamento.itemsProperty().bind(listaDePacotesLancamento);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE PROGRAMACAO">
        // <editor-fold defaultstate="collapsed" desc="Column ALERTA">
        clnAlerta.setCellFactory(new Callback<TableColumn<SdProgramacaoPacote001, Boolean>, TableCell<SdProgramacaoPacote001, Boolean>>() {
            @Override
            public TableCell<SdProgramacaoPacote001, Boolean> call(TableColumn<SdProgramacaoPacote001, Boolean> btnCol) {
                return new TableCell<SdProgramacaoPacote001, Boolean>() {
                    
                    @Override
                    public void updateItem(Boolean hasConflito, boolean empty) {
                        super.updateItem(hasConflito, empty);
                        setText(null);
                        setGraphic(null);
                        if (hasConflito != null && !empty) {
                            setGraphic(hasConflito ? new ImageView(imgAlertaProgramacao) : null);
                        }
                    }
                };
            }
            
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO_OPERACAO">
        clnTempoOperacao.setCellFactory(
                new Callback<TableColumn<SdProgramacaoPacote001, Double>, TableCell<SdProgramacaoPacote001, Double>>() {
                    @Override
                    public TableCell<SdProgramacaoPacote001, Double> call(
                            TableColumn<SdProgramacaoPacote001, Double> param) {
                        return new TableCell<SdProgramacaoPacote001, Double>() {
                            @Override
                            protected void updateItem(Double item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                } else {
                                    setText(null);
                                }
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO_TOTAL">
        clnTempoTotal.setCellFactory(
                new Callback<TableColumn<SdProgramacaoPacote001, Double>, TableCell<SdProgramacaoPacote001, Double>>() {
                    @Override
                    public TableCell<SdProgramacaoPacote001, Double> call(
                            TableColumn<SdProgramacaoPacote001, Double> btnCol) {
                        return new TableCell<SdProgramacaoPacote001, Double>() {
                            
                            @Override
                            public void updateItem(Double item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item, 4));
                                    setGraphic(null);
                                } else {
                                    setText(null);
                                    setGraphic(null);
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column ORDEM">
        Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>> cellFactoryOrdem = new Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnInteger();
            }
        };
        clnOrdem.setCellFactory(cellFactoryOrdem);
        clnOrdem.setOnEditCommit((event) -> {
            this.updateOrdemOperacao(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column INDEPENDENCIA">
        Callback<TableColumn<SdProgramacaoPacote001, String>, TableCell<SdProgramacaoPacote001, String>> cellFactoryIndependente = new Callback<TableColumn<SdProgramacaoPacote001, String>, TableCell<SdProgramacaoPacote001, String>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnString();
            }
        };
        clnIndependente.setCellFactory(cellFactoryIndependente);
        clnIndependente.setOnEditCommit((event) -> {
            this.updateIndependenciaOperacao(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AGRUPADOR">
        Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>> cellFactoryGrupo = new Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnInteger();
            }
        };
        clnGrupo.setCellFactory(cellFactoryGrupo);
        clnGrupo.setOnEditCommit((event) -> {
            this.updateAgrupadorOperacoes(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column DH_INICIO">
        Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>> cellFactoryDhInicio = new Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnDateTime();
            }
        };
        clnInicio.setCellFactory(cellFactoryDhInicio);
        clnInicio.setOnEditCommit((event) -> {
            this.updateDhInicioOperacao(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column DH_IB">
        clnIb.setCellFactory(new Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>>() {
            @Override
            public TableCell<SdProgramacaoPacote001, LocalDateTime> call(TableColumn<SdProgramacaoPacote001, LocalDateTime> param) {
                return new TableCell<SdProgramacaoPacote001, LocalDateTime>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText(item.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                        }
                    }
                };
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column DH_FIM">
        clnFim.setCellFactory(new Callback<TableColumn<SdProgramacaoPacote001, LocalDateTime>, TableCell<SdProgramacaoPacote001, LocalDateTime>>() {
            @Override
            public TableCell<SdProgramacaoPacote001, LocalDateTime> call(TableColumn<SdProgramacaoPacote001, LocalDateTime> param) {
                return new TableCell<SdProgramacaoPacote001, LocalDateTime>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText(item.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                        }
                    }
                };
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column QTDE">
        Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>> cellFactoryQtde = new Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnInteger();
            }
        };
        clnQtdePecas.setCellFactory(cellFactoryQtde);
        clnQtdePecas.setOnEditCommit((event) -> {
            this.updateQtdePecasOperacao(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PERC_IB">
        Callback<TableColumn<SdProgramacaoPacote001, Double>, TableCell<SdProgramacaoPacote001, Double>> cellFactoryIb = new Callback<TableColumn<SdProgramacaoPacote001, Double>, TableCell<SdProgramacaoPacote001, Double>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnPercent();
            }
        };
        clnPercIb.setCellFactory(cellFactoryIb);
        clnPercIb.setOnEditCommit((event) -> {
            this.updateIbOperacao(event);
        });
        // </editor-fold>
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE OFS PARA PROGRAMAR">
        // <editor-fold defaultstate="collapsed" desc="Column STATUS MATERIAL">
        clnStatusMaterial.setCellFactory(
                new Callback<TableColumn<VSdOfsParaProgramar, String>, TableCell<VSdOfsParaProgramar, String>>() {
                    @Override
                    public TableCell<VSdOfsParaProgramar, String> call(
                            TableColumn<VSdOfsParaProgramar, String> param) {
                        return new TableCell<VSdOfsParaProgramar, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                clearStyle();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("REQUISITADO")) {
                                        getStyleClass().add("table-row-success");
                                    } else if (item.equals("REQ. PARCIAL")) {
                                        getStyleClass().add("table-row-primary");
                                    } else {
                                        getStyleClass().add("table-row-danger");
                                        
                                    }
                                } else {
                                    setText(null);
                                }
                            }
                            
                            private void clearStyle() {
                                getStyleClass().remove("table-row-primary");
                                getStyleClass().remove("table-row-success");
                                getStyleClass().remove("table-row-danger");
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AÇÕES">
        clnAcoes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdOfsParaProgramar, VSdOfsParaProgramar>, ObservableValue<VSdOfsParaProgramar>>() {
            @Override
            public ObservableValue<VSdOfsParaProgramar> call(TableColumn.CellDataFeatures<VSdOfsParaProgramar, VSdOfsParaProgramar> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnAcoes.setComparator(new Comparator<VSdOfsParaProgramar>() {
            @Override
            public int compare(VSdOfsParaProgramar p1, VSdOfsParaProgramar p2) {
                return p1.getOp().compareTo(p2.getOp());
            }
        });
        clnAcoes.setCellFactory(new Callback<TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar>, TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar>>() {
            @Override
            public TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar> call(TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar> btnCol) {
                return new TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar>() {
                    final ImageView imgBtnProgramarOf = new ImageView(new Image(getClass().getResource("/images/icons/buttons/alarm (1).png").toExternalForm()));
                    final ImageView imgBtnMovimentarOf = new ImageView(new Image(getClass().getResource("/images/icons/buttons/send (1).png").toExternalForm()));
                    final ImageView imgBtnAbrirProgramacao = new ImageView(new Image(getClass().getResource("/images/icons/buttons/view (1).png").toExternalForm()));
                    final ImageView imgBtnEditarProgramacao = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final Button btnProgramarOf = new Button();
                    final Button btnMovimentarOf = new Button();
                    final Button btnAbrirProgramacao = new Button();
                    final Button btnEditarProgramacao = new Button();
                    final HBox boxButtonsRow = new HBox(3);
                    
                    final Tooltip tipBtnProgramarOf = new Tooltip("Programar Produção OF");
                    final Tooltip tipBtnMovimentarOf = new Tooltip("Movimentar OF no TI");
                    final Tooltip tipBtnAbrirProgramacao = new Tooltip("Visualizar Programação OF");
                    final Tooltip tipBtnEditarProgramacao = new Tooltip("Editar Programação de Produção OF");
                    
                    {
                        btnProgramarOf.setGraphic(imgBtnProgramarOf);
                        btnProgramarOf.setTooltip(tipBtnProgramarOf);
                        btnProgramarOf.setText("Programar");
                        btnProgramarOf.getStyleClass().add("success");
                        btnProgramarOf.getStyleClass().add("xs");
                        btnProgramarOf.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnMovimentarOf.setGraphic(imgBtnMovimentarOf);
                        btnMovimentarOf.setTooltip(tipBtnMovimentarOf);
                        btnMovimentarOf.setText("Movimentar");
                        btnMovimentarOf.getStyleClass().add("warning");
                        btnMovimentarOf.getStyleClass().add("xs");
                        btnMovimentarOf.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnAbrirProgramacao.setGraphic(imgBtnAbrirProgramacao);
                        btnAbrirProgramacao.setTooltip(tipBtnAbrirProgramacao);
                        btnAbrirProgramacao.setText("Visualizar");
                        btnAbrirProgramacao.getStyleClass().add("primary");
                        btnAbrirProgramacao.getStyleClass().add("xs");
                        btnAbrirProgramacao.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnEditarProgramacao.setGraphic(imgBtnEditarProgramacao);
                        btnEditarProgramacao.setTooltip(tipBtnEditarProgramacao);
                        btnEditarProgramacao.setText("Editar");
                        btnEditarProgramacao.getStyleClass().add("secundary");
                        btnEditarProgramacao.getStyleClass().add("xs");
                        btnEditarProgramacao.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }
                    
                    @Override
                    public void updateItem(VSdOfsParaProgramar seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            if (seletedRow.isProgramado()) {
                                btnProgramarOf.setDisable(true);
                                btnMovimentarOf.setDisable(true);
                                btnAbrirProgramacao.setDisable(false);
                                btnEditarProgramacao.setDisable(false);
                                
                                btnAbrirProgramacao.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnVisualizarProgramacao(seletedRow);
                                    }
                                });
                                btnEditarProgramacao.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnEditarProgramacao(seletedRow);
                                    }
                                });
                            } else {
                                btnProgramarOf.setDisable(false);
                                btnMovimentarOf.setDisable(false);
                                btnAbrirProgramacao.setDisable(true);
                                btnEditarProgramacao.setDisable(true);
                                
                                btnProgramarOf.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnProgramarOf(seletedRow);
                                    }
                                });
                                btnMovimentarOf.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnMovimentarOf(seletedRow);
                                    }
                                });
                            }
                            
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnProgramarOf, btnMovimentarOf, btnAbrirProgramacao, btnEditarProgramacao);
                            setGraphic(boxButtonsRow);
                            
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        //</editor-fold>
        //</editor-fold>
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos de COMPONENTS EVENTS">
    
    /**
     * Método para OnCommit na coluna de IB da operação
     *
     * @param event
     */
    private void updateOrdemOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote001, Integer> event) {
        if (event.getNewValue() == event.getOldValue()) {
            return;
        }
        
        // Ordem e objeto primeiro da lista
        // Se a nova ordem é menor que a ordem antiga, é necessário buscar o objeto que está nesta ordem
        // esse objeto serve para pegar a posição onde deve ser feito a reprogramacao das operacoes
        Integer menorOrdem = event.getOldValue() < event.getNewValue() ? event.getOldValue() : event.getNewValue();
        SdProgramacaoPacote001 menorProgramacao = event.getOldValue() < event.getNewValue()
                ? event.getRowValue()
                : operacoesParaProgramacao.stream()
                .filter(programacaoFull -> programacaoFull.getOrdem() == event.getNewValue() && programacaoFull.getQuebra() == 1)
                .findFirst()
                .get();
        int indiceMenorProgamacao = operacoesParaProgramacao.indexOf(menorProgramacao);
        SdProgramacaoPacote001 operacaoAnteriorMenorOrdem = menorProgramacao.getOperacaoAnterior();
        
        //Guarda o horário da menor ordem para que este seja o primeiro para o recálculo dos horários
        LocalDateTime dhInicioMenorOrdem = menorProgramacao.getDhInicio();
        
        // Manter as referências dos objetos que serão trocados
        // Programação ou agrupamento que será movimentado
        SdProgramacaoPacote001 programacaoOldValue = event.getRowValue();
        // Programação que estava na posição que está sendo assumida
        SdProgramacaoPacote001 programacaoNewValue = operacoesParaProgramacao.stream()
                .filter(programacaoFull -> programacaoFull.getOrdem() == event.getNewValue() && programacaoFull.getQuebra() == 1)
                .findFirst()
                .get();
        
        if (programacaoNewValue.getSetorOp() != programacaoOldValue.getSetorOp()) {
            GUIUtils.showMessage("Você está colocando esta operação em uma posição de outro setor de operação. Verifique a ordem e inclua a operação em um ordem que esteja em seu setor de operação.");
            tblProgramacao.refresh();
            return;
        }
        
        if (setoresComLancamento.contains(programacaoNewValue.getSetorOp())) {
            GUIUtils.showMessage("Existem lançamentos de produção neste setor, não é permitido esta alteração em caso de já estar em produção.");
            tblProgramacao.refresh();
            return;
        }
        
        // Exclui as operacões que foram quebradas automaticamente na seleção da célula.
        removeOperacoesQuebradas(indiceMenorProgamacao);
        
        // Indice para obter a posição que a nova ordem vai assumir no vetor.
        int indiceProgamacaoNewValue = operacoesParaProgramacao.indexOf(programacaoNewValue);
        final IntegerProperty posicaoProgramacao = new SimpleIntegerProperty(indiceProgamacaoNewValue); // quarda em um property para utilizar no forEach()
        // Guarda no vetor todas as operações agrupadas que serão movimentadas.
        List<SdProgramacaoPacote001> programacaoAgrupadaParaMovimento = operacoesParaProgramacao.stream()
                .filter(programacaoProcura -> (programacaoProcura.getOperacao() == programacaoOldValue.getOperacao() || programacaoProcura.getAgrupador() == programacaoOldValue.getOperacao())
                        && programacaoProcura.getSetorOp() == programacaoOldValue.getSetorOp())
                .collect(Collectors.toList());
        // Percorre a lista de operações agrupadas (caso não tenha agrupamento, vem só a operação que foi movimentada)
        // Primeiro é excluída as operações agrupadas no vetor principal.
        // Após é adicionado as operações na posição da nova ordem.
        programacaoAgrupadaParaMovimento.forEach(programacaoParaMovimento -> operacoesParaProgramacao.remove(programacaoParaMovimento));
        programacaoAgrupadaParaMovimento.forEach(programacaoParaMovimento -> {
            operacoesParaProgramacao.add(posicaoProgramacao.get(), programacaoParaMovimento);
            posicaoProgramacao.set(posicaoProgramacao.add(1).get());
        });
        
        // pegando o objeto da menor posição
        SdProgramacaoPacote001 programacaoNovaOrdem = operacoesParaProgramacao.get(indiceMenorProgamacao);
        programacaoNovaOrdem.setDhInicio(dhInicioMenorOrdem);
        
        //Reordenando as programações com a ordem movimentada
        int ordemInicial = menorOrdem;
        for (int i = operacoesParaProgramacao.indexOf(programacaoNovaOrdem); i < operacoesParaProgramacao.size(); i++) {
            SdProgramacaoPacote001 operacaoProgramada = operacoesParaProgramacao.get(i);
            if (operacaoProgramada.getQuebra() > 1) {
                ordemInicial -= 1;
            }
            operacaoProgramada.setOrdem(ordemInicial);
            ordemInicial += 1;
        }
        reordenarEncadeamentoObjetosProgramacao(operacaoAnteriorMenorOrdem);
        
        try {
            this.programarOperacoes(programacaoNovaOrdem);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateOrdemOperacao");
            GUIUtils.showException(ex);
        }
        
    }
    
    /**
     * Método para ações após alteração da coluna Independência
     *
     * @param event
     */
    private void updateIndependenciaOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote001, String> event) {
        SdProgramacaoPacote001 programacaoSelecionada = event.getRowValue();
        
        if (event.getNewValue().length() == 0) {
            programacaoSelecionada.setIndependencia(event.getNewValue().toUpperCase());
            programacaoSelecionada.setDhInicio(programacaoSelecionada.getOperacaoAnterior().getDhIb());
            try {
                programarOperacoes(programacaoSelecionada, true);
            } catch (Exception e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
            return;
        }
        
        if (!event.getNewValue().toUpperCase().startsWith("I")) {
            GUIUtils.showMessage("Para tornar a operação independente, você deve informar o valor 'I'(letra i) nesta coluna.", Alert.AlertType.WARNING);
            return;
        }
        
        try {
            programacaoSelecionada.setDhInicio(getDhOperacaoIndependente(programacaoSelecionada, event.getNewValue().toUpperCase()));
            programacaoSelecionada.setIndependencia(event.getNewValue().toUpperCase());
            
            programarOperacoes(programacaoSelecionada, true);
        } catch (Exception e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            //GUIUtils.showException(e);
        }
    }
    
    /**
     * Método para OnCommit na coluna de agrupador de operacões
     *
     * @param event
     */
    private void updateAgrupadorOperacoes(TableColumn.CellEditEvent<SdProgramacaoPacote001, Integer> event) {
        SdProgramacaoPacote001 programacaoSelecionada = event.getRowValue();
        
        if (event.getNewValue() == 0) {
            programacaoSelecionada.setAgrupador(event.getNewValue());
            try {
                programarOperacoes(programacaoSelecionada.getOperacaoAnterior());
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateAgrupadorOperacoes");
            }
            return;
        }
        SdProgramacaoPacote001 programacaoMaeAgrupamento = null;
        try {
            programacaoMaeAgrupamento = operacoesParaProgramacao.stream()
                    .filter(programacaoFull -> programacaoFull.getOperacao() == event.getNewValue()
                            && programacaoFull.getSetorOp() == programacaoSelecionada.getSetorOp())
                    .findFirst()
                    .get();
        } catch (NoSuchElementException ex) {
            GUIUtils.showMessage("O código de operação digitado não existe neste roteiro.", Alert.AlertType.WARNING);
        }
        
        if (programacaoMaeAgrupamento.getProgramacaoApontada()) {
            GUIUtils.showMessage("Existem lançamentos de produção para esta operação, não é permitido agrupamento em operações com produção.");
            tblProgramacao.refresh();
            return;
        }
        
        if (programacaoMaeAgrupamento != null) {
            programacaoSelecionada.setAgrupador(event.getNewValue());
            try {
                if (programacaoMaeAgrupamento.getColaboradorObject() != null) {
                    programacaoSelecionada.setColaborador(programacaoMaeAgrupamento.getColaborador());
                    programacaoSelecionada.setColaboradorObject(programacaoMaeAgrupamento.getColaboradorObject());
                }
                programacaoSelecionada.setIb(programacaoMaeAgrupamento.getIb());
                programacaoSelecionada.setQtde(programacaoMaeAgrupamento.getQtde());
                
                programarOperacoes(programacaoMaeAgrupamento.getOperacaoAnterior());
                carregaConflitos();
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateAgrupadorOperacoes");
            }
        }
    }
    
    /**
     * Método para OnCommit na coluna de dh inicio da operação
     *
     * @param event
     */
    private void updateDhInicioOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote001, LocalDateTime> event) {
        SdProgramacaoPacote001 programacaoSelecionada = event.getRowValue();
        programacaoSelecionada.setDhInicio(event.getNewValue());
        try {
            programarOperacoes(programacaoSelecionada, true);
            carregaConflitos();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateDhInicioOperacao");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para OnCommit na coluna de qtde de peças da opoeração
     *
     * @param event
     */
    private void updateQtdePecasOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote001, Integer> event) {
        SdProgramacaoPacote001 programacaoSelecionada = event.getRowValue();
        if (!programacaoSelecionada.isQuebraManual()) {
            GUIUtils.showMessage("Você pode alterar a quantidade somente de programações quebradas.", Alert.AlertType.INFORMATION);
            tblProgramacao.refresh();
            return;
        }
        
        if (programacaoSelecionada.getProgramacaoApontada()) {
            GUIUtils.showMessage("Existem lançamentos de produção para esta operação, não é permitido alteração de quantidade em operações com produção.");
            tblProgramacao.refresh();
            return;
        }
        
        SdProgramacaoPacote001 programacaoMaeQuebra = operacoesParaProgramacao.stream()
                .filter(programacaoMae -> programacaoMae.getOperacao() == programacaoSelecionada.getOperacao()
                        && programacaoMae.getSetorOp() == programacaoSelecionada.getSetorOp()
                        && programacaoMae.getQuebra() == 1)
                .findFirst()
                .get();
        int qtdeTotalPecas = programacaoMaeQuebra.getQtde() + programacaoSelecionada.getQtde();
        programacaoMaeQuebra.setQtde(qtdeTotalPecas - event.getNewValue());
        programacaoMaeQuebra.setTempoTotal(programacaoMaeQuebra.getQtde() * programacaoMaeQuebra.getOperacaoObject().getTempo().doubleValue());
        programacaoSelecionada.setQtde(event.getNewValue());
        programacaoSelecionada.setTempoTotal(programacaoSelecionada.getQtde() * programacaoSelecionada.getOperacaoObject().getTempo().doubleValue());
        
        try {
            this.programarOperacoes(programacaoMaeQuebra);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateQtdePecasOperacao");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para OnCommit na coluna de IB da operação
     *
     * @param event
     */
    private void updateIbOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote001, Double> event) {
        SdProgramacaoPacote001 programacaoSelecionada = event.getRowValue();
        
        if (programacaoSelecionada.getProgramacaoApontada()) {
            GUIUtils.showMessage("Existem lançamentos de produção para esta operação, não é permitido alteração de quantidade em operações com produção.");
            tblProgramacao.refresh();
            return;
        }
        
        programacaoSelecionada.setIb(event.getNewValue() / 100.0);
        operacoesParaProgramacao.stream()
                .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == programacaoSelecionada.getOperacao()
                        && programacaoAgrupada.getSetorOp() == programacaoSelecionada.getSetorOp())
                .forEach(programacaoAgrupada -> programacaoAgrupada.setIb(event.getNewValue() / 100.0));
        try {
            this.programarOperacoes(programacaoSelecionada);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::updateIbOperacao");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para executar a ação do click do botão programar of
     *
     * @param selectedRow
     */
    private void actionBtnProgramarOf(VSdOfsParaProgramar selectedRow) {
        try {
            programacaoProducaoOf.set(new SdProgramacaoOf001(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")),
                    41,
                    selectedRow.getSetor(),
                    selectedRow.getOp(),
                    "R",
                    selectedRow.getQtde(),
                    "N"
            ));
            rbtnReferencia.setSelected(true);
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            criarPacoteReferencia();
            
            ofParaProgramarSelecionada.set(selectedRow);
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnProgramarOf");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para executar a ação do click do botão movimentar of
     *
     * @param selectedRow
     */
    private void actionBtnMovimentarOf(VSdOfsParaProgramar selectedRow) {
    
    }
    
    /**
     * Método para executar a ação do click do botão abrir programacao
     *
     * @param selectedRow
     */
    private void actionBtnVisualizarProgramacao(VSdOfsParaProgramar selectedRow) {
        
        try {
            emVisualizacao.set(true);
            programacaoProducaoOf.set(DAOFactory.getSdProgramacaoOf001DAO().getByOf(selectedRow.getOp()));
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote inicial">
            switch(programacaoProducaoOf.get().getTipoPacote()) {
                case "R":
                    rbtnReferencia.setSelected(true);
                    break;
                case "C":
                    rbtnCor.setSelected(true);
                    break;
                case "T":
                    rbtnTamanho.setSelected(true);
                    break;
                default:
                    rbtnReferencia.setSelected(true);
                    break;
            }
            listaDePacotes.clear();
            listaDePacotes.set(DAOFactory.getSdPacote001DAO().getByNumero(ofParaProgramacao.getNumero()));
            totalPacotesOf.set(listaDePacotes.getSize());
            cboxPacote.getSelectionModel().select(0);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote lancamento inicial">
            switch(pacoteSelecionado.get().getTipoLancamento()) {
                case "R":
                    rbtnReferenciaLancamento.setSelected(true);
                    break;
                case "C":
                    rbtnCorLancamento.setSelected(true);
                    break;
                case "T":
                    rbtnTamanhoLancamento.setSelected(true);
                    break;
                default:
                    rbtnReferenciaLancamento.setSelected(true);
                    break;
            }
            // </editor-fold>
            
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnVisualizarProgramacao");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para executar a ação do click do botão editar programacao
     *
     * @param selectedRow
     */
    private void actionBtnEditarProgramacao(VSdOfsParaProgramar selectedRow) {
        
        try {
            emEdicao.set(true);
            btnIniciarInativo.set(true);
            programacaoProducaoOf.set(DAOFactory.getSdProgramacaoOf001DAO().getByOf(selectedRow.getOp()));
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote inicial">
            switch(programacaoProducaoOf.get().getTipoPacote()) {
                case "R":
                    rbtnReferencia.setSelected(true);
                    criarPacoteReferencia();
                    break;
                case "C":
                    rbtnCor.setSelected(true);
                    criarPacoteCor();
                    break;
                case "T":
                    rbtnTamanho.setSelected(true);
                    criarPacoteTamanho();
                    break;
                default:
                    rbtnReferencia.setSelected(true);
                    criarPacoteReferencia();
                    break;
            }
            //listaDePacotes.clear();
            //listaDePacotes.set(DAOFactory.getSdPacote001DAO().getByNumero(ofParaProgramacao.getNumero()));
            //totalPacotesOf.set(listaDePacotes.getSize());
            //cboxPacote.getSelectionModel().select(0);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote lancamento inicial">
            if (pacoteSelecionado.get().getTipoLancamento() != null) {
                switch(pacoteSelecionado.get().getTipoLancamento()) {
                    case "R":
                        rbtnReferenciaLancamento.setSelected(true);
                        criarPacoteLancamentoReferencia();
                        break;
                    case "C":
                        rbtnCorLancamento.setSelected(true);
                        criarPacoteLancamentoCor();
                        break;
                    case "T":
                        rbtnTamanhoLancamento.setSelected(true);
                        criarPacoteLancamentoTamanho();
                        break;
                    default:
                        rbtnReferenciaLancamento.setSelected(true);
                        criarPacoteLancamentoReferencia();
                        break;
                }
            } else {
                rbtnReferenciaLancamento.setSelected(true);
                criarPacoteLancamentoReferencia();
            }
            // </editor-fold>
            
            ofParaProgramarSelecionada.set(selectedRow);
            
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnEditarProgramacao");
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Método para bind da celula selecionada com os elementos que mostram os dados
     *
     * @param celula
     */
    private void bindCelulaSelecionada(SdCelula celula) {
        if (celula != null) {
            tboxCelula.textProperty().bind(celula.codigoProperty().asString());
            lbDescricaoCelula.textProperty().bind(celula.descricaoProperty());
        }
        tboxCelula.textProperty().unbind();
        lbDescricaoCelula.textProperty().unbind();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos de BIND e UNBIND">
    
    /**
     * Ligação dos dados da OF selecionada com os componentes do cabeçalho da
     * tela.
     *
     * @param of
     * @throws SQLException
     */
    private void bindOfProgramacao(Of1001 of) throws Exception {
        if (of != null) {
            tboxNumeroOf.textProperty().bind(of.numeroProperty());
            tboxQtdeOf.textProperty().bind(of.ativosProperty().asString());
            tboxFamiliaOf.textProperty().bind(of.getRelCodigo().getRelFamilia().descricaoProperty());
            tboxProdutoOf.textProperty().bind(of.codigoProperty());
            tboxDescricaoProduto.textProperty().bind(of.getRelCodigo().descricaoProperty());
        }
    }
    
    /**
     * Método de ligação de dados quando selecionada uma operação na table de
     * programacao
     *
     * @param programacaoSelecionada
     * @throws SQLException
     */
    private void bindProgramacao(SdProgramacaoPacote001 programacaoSelecionada) throws Exception {
        if (programacaoSelecionada != null) {
            tboxOperacaoSelecionada.setText(programacaoSelecionada.getOperacaoObject().toString());
            tboxQtdeOperacaoSelecionada.setText(programacaoSelecionada.qtdeProperty().asString().get());
            tboxTempoOperacaoSelecionada.setText(StringUtils.toDecimalFormat(programacaoSelecionada.getTempoOperacao(), 4) + " min.");
            tboxTempoTotalOperacaoSelecionada.setText(StringUtils.toDecimalFormat(programacaoSelecionada.tempoTotalProperty().get(), 4) + " min.");
            tboxEquipamentoOperacaoSelecionada.setText(programacaoSelecionada.getMaquinaObject().toString());
            
            ObservableList<ViewOcupacaoColaborador> listPolivalenciaCelulaLivre = DAOFactory.getSdColaborador001DAO()
                    .getPolivalenteCelulaLivreProgramacao(programacaoSelecionada);
//            listPolivalenciaCelulaLivre.forEach(colaborador -> colaborador.setStatusOcupacao("G"));
            ObservableList<ViewOcupacaoColaborador> listCelulaLivre = DAOFactory.getSdColaborador001DAO()
                    .getCelulaLivreProgramacao(programacaoSelecionada);
//            listCelulaLivre.forEach(colaborador -> colaborador.setStatusOcupacao("B"));
            ObservableList<ViewOcupacaoColaborador> listPolivalenciaLivre = DAOFactory.getSdColaborador001DAO()
                    .getPolivalenteLivreProgramacao(programacaoSelecionada);
//            listPolivalenciaLivre.forEach(colaborador -> colaborador.setStatusOcupacao("Y"));
            ObservableList<ViewOcupacaoColaborador> listPolivalenciaCelulaOcupado = DAOFactory.getSdColaborador001DAO()
                    .getPolivalenteCelulaOcupadaProgramacao(programacaoSelecionada);
//            listPolivalenciaCelulaOcupado.forEach(colaborador -> colaborador.setStatusOcupacao("R"));
//            ObservableList<ViewOcupacaoColaborador> listPolivalenciaOcupado = DAOFactory.getSdColaborador001DAO()
//                    .getPolivalenteOcupadaProgramacao(programacaoSelecionada);
//            listPolivalenciaOcupado.forEach(colaborador -> colaborador.setStatusOcupacao("D"));
            
            colaboradoresParaProgramacao.clear();
            colaboradoresParaProgramacao.addAll(listPolivalenciaCelulaLivre);
            colaboradoresParaProgramacao.addAll(listCelulaLivre);
            colaboradoresParaProgramacao.addAll(listPolivalenciaCelulaOcupado);
            colaboradoresParaProgramacao.addAll(listPolivalenciaLivre);
//            colaboradoresParaProgramacao.addAll(listPolivalenciaOcupado);
            
            colaboradoresParaProgramacao.forEach(colaborador -> {
                long tempoAlocadoProgramacao = getMinutosAlocadoOperador(colaborador, programacaoSelecionada);
                colaborador.setTempoOcupado((Double.parseDouble(colaborador.getTempoOcupado()) + tempoAlocadoProgramacao) + "");
                colaborador.setTempoLivre((Double.parseDouble(colaborador.getTempoLivre()) - tempoAlocadoProgramacao) + "");
            });
        }
    }
    
    /**
     * Binding dados do colaborador após selecionar uma opção no combobox
     */
    private void bindColaboradorSelecionado(ViewOcupacaoColaborador colaborador) throws Exception {
        if (emVisualizacao.get()) {
            return;
        }
        if (colaborador != null) {
            SdProgramacaoPacote001 programacaoOperacao = tblProgramacao.getSelectionModel().getSelectedItem();
            
            if (programacaoOperacao != null) {
                final int codigoOperacao = programacaoOperacao.getOperacao();
                final IntegerProperty tempoOcupadoOperacao = new SimpleIntegerProperty(0);
                // Set do colaborador na programacao
                // Para excluir o operador, é preciso excluir pelo botão direito do mouse sobre a linha da operação/programação
                programacaoOperacao.setColaborador(colaborador.getColaborador().getCodigo());
                programacaoOperacao.setColaboradorObject(colaborador.getColaborador());
                tempoOcupadoOperacao.set((int) programacaoOperacao.getTempoTotal());
                operacoesParaProgramacao.stream()
                        .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == codigoOperacao)
                        .forEach(programacaoAgrupada -> {
                            programacaoAgrupada.setColaborador(colaborador.getColaborador().getCodigo());
                            programacaoAgrupada.setColaboradorObject(colaborador.getColaborador());
                            tempoOcupadoOperacao.set(tempoOcupadoOperacao.add(programacaoAgrupada.getTempoTotal()).intValue());
                        });
                
                // Set na tabela de ocupação do colaborador das operações já programadas para
                // ele no dia da programação da operação selecionada
                colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacaoOperacao.getDhInicio()));
                colaborador.setTempoOcupado((Double.parseDouble(colaborador.getTempoOcupado()) + tempoOcupadoOperacao.get()) + "");
                colaborador.setTempoLivre((Double.parseDouble(colaborador.getTempoLivre()) - tempoOcupadoOperacao.get()) + "");
                ocupacaoColaborador.set(colaborador.getOcupacao());
            }
            carregaConflitos();
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Criar Pacotes Programação">
    private void criarPacoteTamanho() {
        listaDePacotes.clear();
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            gradeCor.getHeadersTamanhos().entrySet().stream()
                    .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                    .forEach((entry) -> {
                        if (gradeCor.getTamanhos().get(entry.getValue()) > 0) {
                            ordemPacote.set(ordemPacote.add(1).get());
                            SdPacote001 pacoteInicial = new SdPacote001(
                                    ordemPacote.get(),
                                    ofParaProgramacao.getNumero(),
                                    ofParaProgramacao.getCodigo(),
                                    gradeCor.getCor(),
                                    entry.getValue(),
                                    gradeCor.getTamanhos().get(entry.getValue()),
                                    "T",
                                    null,
                                    ofParaProgramarSelecionada.get().getSetor()
                            );
                            listaDePacotes.add(pacoteInicial);
                        }
                    });
        });
        if (emEdicao.get()) {
            listaDePacotes.forEach(pacote -> {
                try {
                    SdPacote001 pacoteProgramado = DAOFactory.getSdPacote001DAO().getByPacote(pacote.getCodigo());
                    if (pacoteProgramado != null) {
                        pacote.setTipoLancamento(pacoteProgramado.getTipoLancamento());
                    }
                    pacote.setPacoteProgramado(pacoteProgramado != null);
                } catch (SQLException e) {
                    e.printStackTrace();
                    LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                    GUIUtils.showException(e);
                }
            });
            
            cboxCopiarPacote.getItems().clear();
            cboxCopiarPacote.getItems().addAll(listaDePacotes.stream().filter(SdPacote001::isPacoteProgramado).collect(Collectors.toList()));
            //listaDePacotes.removeIf(SdPacote001::isPacoteProgramado);
        }
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
    }
    
    /**
     * Criação dos objetos dos pacotes por cor e inserção no array de pacotes
     */
    private void criarPacoteCor() {
        listaDePacotes.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            qtdeTotalPacoteInicial.set(0);
            gradeCor.getTamanhos().forEach((key, value) -> {
                qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
            });
            if (qtdeTotalPacoteInicial.get() > 0) {
                ordemPacote.set(ordemPacote.add(1).get());
                SdPacote001 pacoteInicial = new SdPacote001(
                        ordemPacote.get(),
                        ofParaProgramacao.getNumero(),
                        ofParaProgramacao.getCodigo(),
                        gradeCor.getCor(),
                        null,
                        qtdeTotalPacoteInicial.get(),
                        "C",
                        null,
                        ofParaProgramarSelecionada.get().getSetor()
                );
                listaDePacotes.add(pacoteInicial);
            }
        });
        if (emEdicao.get()) {
            listaDePacotes.forEach(pacote -> {
                try {
                    SdPacote001 pacoteProgramado = DAOFactory.getSdPacote001DAO().getByPacote(pacote.getCodigo());
                    if (pacoteProgramado != null) {
                        pacote.setTipoLancamento(pacoteProgramado.getTipoLancamento());
                    }
                    pacote.setPacoteProgramado(pacoteProgramado != null);
                } catch (SQLException e) {
                    e.printStackTrace();
                    LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                    GUIUtils.showException(e);
                }
            });
            
            cboxCopiarPacote.getItems().clear();
            cboxCopiarPacote.getItems().addAll(listaDePacotes.stream().filter(SdPacote001::isPacoteProgramado).collect(Collectors.toList()));
            //listaDePacotes.removeIf(SdPacote001::isPacoteProgramado);
        }
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
    }
    
    /**
     * Criação do objeto do pacote por referência e inserção no array de pacotes
     */
    private void criarPacoteReferencia() {
        listaDePacotes.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            gradeCor.getTamanhos().forEach((key, value) -> {
                qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
            });
        });
        SdPacote001 pacotePorReferencia = new SdPacote001(
                1,
                ofParaProgramacao.getNumero(),
                ofParaProgramacao.getCodigo(),
                null,
                null,
                qtdeTotalPacoteInicial.get(),
                "R",
                null,
                ofParaProgramarSelecionada.get().getSetor()
        );
        listaDePacotes.add(pacotePorReferencia);
        if (emEdicao.get()) {
            listaDePacotes.forEach(pacote -> {
                try {
                    SdPacote001 pacoteProgramado = DAOFactory.getSdPacote001DAO().getByPacote(pacote.getCodigo());
                    if (pacoteProgramado != null) {
                        pacote.setTipoLancamento(pacoteProgramado.getTipoLancamento());
                    }
                    pacote.setPacoteProgramado(pacoteProgramado != null);
                } catch (SQLException e) {
                    e.printStackTrace();
                    LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                    GUIUtils.showException(e);
                }
            });
            
            cboxCopiarPacote.getItems().clear();
            cboxCopiarPacote.getItems().addAll(listaDePacotes.stream().filter(SdPacote001::isPacoteProgramado).collect(Collectors.toList()));
            //listaDePacotes.removeIf(SdPacote001::isPacoteProgramado);
        }
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Criar Pacotes Lançamento">
    
    /**
     * Criação dos objetos dos pacotes de lançamento por cor e tamanho e
     * inserção no array de pacotes de lançamento
     */
    private void criarPacoteLancamentoTamanho() {
        listaDePacotesLancamento.clear();
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            if (pacoteSelecionado.get().getCor() == null || pacoteSelecionado.get().getCor().equals(gradeCor.getCor())) {
                gradeCor.getHeadersTamanhos().entrySet().stream()
                        .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                        .forEach((entry) -> {
                            if (pacoteSelecionado.get().getTam() == null || pacoteSelecionado.get().getTam().equals(entry.getValue())) {
                                if (gradeCor.getTamanhos().get(entry.getValue()) > 0) {
                                    ordemPacote.set(ordemPacote.add(1).get());
                                    SdPacote001 pacoteInicial = new SdPacote001(
                                            pacoteSelecionado.get().getCodigo(),
                                            ordemPacote.get(),
                                            ofParaProgramacao.getNumero(),
                                            ofParaProgramacao.getCodigo(),
                                            gradeCor.getCor(),
                                            entry.getValue(),
                                            gradeCor.getTamanhos().get(entry.getValue()),
                                            "T",
                                            null);
                                    listaDePacotesLancamento.add(pacoteInicial);
                                }
                            }
                        });
            }
        });
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
    }
    
    /**
     * Criação dos objetos dos pacotes por cor e inserção no array de pacotes
     */
    private void criarPacoteLancamentoCor() {
        listaDePacotesLancamento.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            if (pacoteSelecionado.get().getCor() == null || pacoteSelecionado.get().getCor().equals(gradeCor.getCor())) {
                qtdeTotalPacoteInicial.set(0);
                gradeCor.getTamanhos().forEach((key, value) -> {
                    qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
                });
                if (qtdeTotalPacoteInicial.get() > 0) {
                    ordemPacote.set(ordemPacote.add(1).get());
                    SdPacote001 pacoteInicial = new SdPacote001(
                            pacoteSelecionado.get().getCodigo(),
                            ordemPacote.get(),
                            ofParaProgramacao.getNumero(),
                            ofParaProgramacao.getCodigo(),
                            gradeCor.getCor(),
                            null,
                            qtdeTotalPacoteInicial.get(),
                            "C",
                            null);
                    listaDePacotesLancamento.add(pacoteInicial);
                }
            }
        });
        
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
    }
    
    /**
     * Criação do objeto do pacote por referência e inserção no array de pacotes
     */
    private void criarPacoteLancamentoReferencia() {
        listaDePacotesLancamento.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            gradeCor.getTamanhos().forEach((key, value) -> {
                qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
            });
        });
        SdPacote001 pacotePorReferencia = new SdPacote001(
                pacoteSelecionado.get().getCodigo(),
                1,
                ofParaProgramacao.getNumero(),
                ofParaProgramacao.getCodigo(),
                null,
                null,
                qtdeTotalPacoteInicial.get(),
                "R",
                null);
        listaDePacotesLancamento.add(pacotePorReferencia);
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
    }
    //</editor-fold>
    
    /**
     * Rotina que irá pegar a data inicial para uma operação conforme a operação de independência.
     * Caso o programador interiu somente o I na coluna, irá pegar o primeiro horário do setor de operação, caso ele indique qual a operação junto com o I
     * a data e hora desta operação será o inicio da operação selecionada.
     *
     * @param programacaoSelecionada
     * @param independencia
     * @return
     */
    private LocalDateTime getDhOperacaoIndependente(SdProgramacaoPacote001 programacaoSelecionada, String independencia) throws Exception {
        String codigoOperacao = independencia.length() > 1 ? independencia.substring(1) : null;
        
        LocalDateTime dhInicioOperacaoIndependente = operacoesParaProgramacao.stream()
                .filter(programacao -> programacao.getSetorOp() == programacaoSelecionada.getSetorOp())
                .findFirst()
                .get().getDhInicio();
        if (codigoOperacao != null) {
            List<SdProgramacaoPacote001> operacoesIndependentes = operacoesParaProgramacao.stream()
                    .filter(programacao -> programacao.getSetorOp() == programacaoSelecionada.getSetorOp()
                            && programacao.getOperacao() == Integer.parseInt(codigoOperacao)
                            && programacao.getOrdem() < programacaoSelecionada.getOrdem()
                    ).collect(Collectors.toList());
            if (operacoesIndependentes.size() == 0) {
                GUIUtils.showMessage("Você indicou uma operação paralela para esta operação não existente neste roteiro.", Alert.AlertType.WARNING);
                throw new Exception("Operação não encontrada no roteiro");
            }
            dhInicioOperacaoIndependente = operacoesIndependentes.get(operacoesIndependentes.size() - 1).getDhInicio();
        }
        return dhInicioOperacaoIndependente;
    }
    
    /**
     * Verifica se na lista de operações do setor ficou alguma operacação sem o
     * operador
     *
     * @param operacoesSetor
     * @return Boolean
     */
    private Boolean hasNullOperador(List<SdProgramacaoPacote001> operacoesSetor) {
        return operacoesSetor.stream().filter(programacao -> programacao.getColaboradorObject() == null).count() > 0;
    }
    
    /**
     * Verifica se na lista de operações ficou alguma operacação com conflito de
     * operador
     *
     * @param operacoes
     * @return Boolean
     */
    private Boolean hasConflitoOperador(List<SdProgramacaoPacote001> operacoes) {
        return operacoes.stream().filter(programacao -> programacao.isHasConflito()).count() > 0;
    }
    
    /**
     * Verifica se na lista de operações ficou alguma operacação sem célula definida
     *
     * @param operacoes
     * @return Boolean
     */
    private Boolean hasCelulaNull(List<SdProgramacaoPacote001> operacoes) {
        return operacoes.stream().filter(programacao -> programacao.getCelulaObject() == null).count() > 0;
    }
    
    /**
     * Rotina que verifica se um colaborador está ocupado para uma programacao.
     * A base para verificação é o array de operacoesParaProgramar
     *
     * @param operador
     * @param programacaoOperacao
     * @return
     */
    private Boolean operadorOcupadoEmProgramacao(SdColaborador operador, SdProgramacaoPacote001 programacaoOperacao) {
        
        for (SdProgramacaoPacote001 operacaoProgramada : operacoesParaProgramacao) {
            if (programacaoOperacao != operacaoProgramada
                    && operacaoProgramada.getColaborador() == operador.getCodigo()
                    && ((programacaoOperacao.getDhInicio().isAfter(operacaoProgramada.getDhInicio()) && programacaoOperacao.getDhInicio().isBefore(operacaoProgramada.getDhFim()))
                    || (programacaoOperacao.getDhFim().isAfter(operacaoProgramada.getDhInicio()) && programacaoOperacao.getDhFim().isBefore(operacaoProgramada.getDhFim()))
                    || (programacaoOperacao.getDhInicio().isAfter(operacaoProgramada.getDhInicio()) && programacaoOperacao.getDhFim().isBefore(operacaoProgramada.getDhFim()))
                    || (programacaoOperacao.getDhInicio().isBefore(operacaoProgramada.getDhInicio()) && programacaoOperacao.getDhFim().isAfter(operacaoProgramada.getDhFim())))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Rotina que verifica se um colaborador está ocupado para uma programacao.
     * A base para verificação é a tabela SD_PROGRAMACAO_PACOTE_001::ORACLE
     *
     * @param operador
     * @param programacaoOperacao
     * @return
     * @throws SQLException
     */
    private Boolean operadorOcupadoProgramado(SdColaborador operador, SdProgramacaoPacote001 programacaoOperacao) throws SQLException {
        
        try {
            SdColaborador programacaoOcupacao = DAOFactory.getSdColaborador001DAO()
                    .getOcupadoProgramacao(operador, programacaoOperacao);
            if (programacaoOcupacao != null) {
                return true;
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::operadorOcupadoProgramado");
            return false;
        }
        return false;
    }
    
    /**
     * Rotina para unificar as consultas de operador ocupado no banco e no array
     * de operações programadas.
     *
     * @param operador
     * @param programacaoOperacao
     * @return
     * @throws SQLException
     */
    private Boolean operadorOcupado(SdColaborador operador, SdProgramacaoPacote001 programacaoOperacao) throws SQLException {
        return operadorOcupadoEmProgramacao(operador, programacaoOperacao) || operadorOcupadoProgramado(operador, programacaoOperacao);
    }
    
    /**
     * Rotina que percorre a programação para verificar os conflitos com os
     * operadores
     *
     * @throws SQLException
     */
    private void carregaConflitos() throws SQLException {
        for (SdProgramacaoPacote001 programacaoOperacao : operacoesParaProgramacao) {
            if (programacaoOperacao.getColaboradorObject() != null) {
                programacaoOperacao.setHasConflito(operadorOcupado(programacaoOperacao.getColaboradorObject(), programacaoOperacao));
//                System.out.println("Teste :: Ordem: " + programacaoOperacao.getOrdem() + " Conflito? " + (programacaoOperacao.isHasConflito() ? "sim" : "não"));
            }
        }
        tblProgramacao.refresh();
    }
    
    /**
     * Método para procurda de operadores para as operações programadas no setor
     *
     * @param operacoesProgramadas
     */
    private void procurarOperadores(List<SdProgramacaoPacote001> operacoesProgramadas) {
        operacoesProgramadas.forEach(operacaoProgramada -> {
            if (operacaoProgramada.getColaboradorObject() == null) {
                try {
                    SdColaborador colaboradorLivre = DAOFactory.getSdColaborador001DAO()
                            .getLivreProgramacao(operacaoProgramada);
                    if (colaboradorLivre != null && !operadorOcupadoEmProgramacao(colaboradorLivre, operacaoProgramada)) {
                        operacaoProgramada.setColaborador(colaboradorLivre.getCodigo());
                        operacaoProgramada.setColaboradorObject(colaboradorLivre);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(SceneProgramaProducaoOfController.class
                            .getName()).log(Level.SEVERE, null, ex);
                    LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::procurarOperadores");
                }
            }
        });
        /**
         * caso não encontre operador para alguma operação, abre o pane de
         * operadores para o programador selecionar um operador manualmente.
         */
        paneInformacoesCostureira.setExpanded(hasNullOperador(operacoesProgramadas));
    }
    
    /**
     * Função para retornar os minutos que estão sendo alocados na programação
     * que está sendo criada. O tempo deve ser contabilizado como ocupação do
     * colaborador.
     *
     * @param colaborador
     * @return
     */
    private long getMinutosAlocadoOperador(ViewOcupacaoColaborador colaborador,
                                           SdProgramacaoPacote001 programacaoParaColaborador) {
        return operacoesParaProgramacao.stream()
                .filter(programacaoOperacao
                        -> programacaoOperacao.getColaborador() == Integer.parseInt(colaborador.getCodigo())
                        && programacaoOperacao.getDhInicio().toLocalDate() == programacaoParaColaborador.getDhInicio().toLocalDate())
                .mapToLong(programacaoOperacao -> Math.round(((double) programacaoOperacao.getQtde()) * programacaoOperacao.getTempoOperacao()))
                .sum();
    }
    
    /**
     * Rotina que reordena o encadeamento dos objetos das operações da
     * programação. Passar como parâmetro o objeto anterior ao que precisa
     * iniciar a reordenação.
     *
     * @param posicaoAnterior
     */
    private void reordenarEncadeamentoObjetosProgramacao(SdProgramacaoPacote001 posicaoAnterior) {
        int indicePrimeiraOperacao = posicaoAnterior != null ? operacoesParaProgramacao.indexOf(posicaoAnterior) : -1;
        SdProgramacaoPacote001 operacaoAnterior = posicaoAnterior;
        for (int i = indicePrimeiraOperacao + 1; i < operacoesParaProgramacao.size(); i++) {
            SdProgramacaoPacote001 operacaoAtual = operacoesParaProgramacao.get(i);
            if (operacaoAtual.getQuebra() > 1) {
                operacaoAtual.setOperacaoAnterior(operacaoAnterior.getOperacaoAnterior());
                operacaoAtual.setOperacaoPosterior(operacaoAnterior.getOperacaoPosterior());
            } else {
                operacaoAtual.setOperacaoAnterior(operacaoAnterior);
                operacaoAtual.setOperacaoPosterior(null);
                if (operacaoAnterior != null) {
                    operacaoAnterior.setOperacaoPosterior(operacaoAtual);
                }
                operacaoAnterior = operacaoAtual;
            }
        }

//        operacoesParaProgramacao.forEach(programacao -> {
//            System.out.println("-----------------");
//            System.out.println("Ant: " + (programacao.getOperacaoAnterior() == null ? "inicio" : programacao.getOperacaoAnterior().getOperacaoObject() + " Ordem: " + programacao.getOperacaoAnterior().getOrdem()));
//            System.out.println("Atu: " + programacao.getOperacaoObject() + " Ordem: " + programacao.getOrdem());
//            System.out.println("Pos: " + (programacao.getOperacaoPosterior() == null ? "final" : programacao.getOperacaoPosterior().getOperacaoObject() + " Ordem: " + programacao.getOperacaoPosterior().getOrdem()));
//        });
    }
    
    /**
     * Reordena as operações de um setor pela ordem e quebra
     *
     * @return List<SdProgramacaoPacote001>
     */
    private List<SdProgramacaoPacote001> carregarOperacoesSetor() {
        //reordenando as operações do setor para exibição na tabela
        List<SdProgramacaoPacote001> operacoesProgramadasSetor = operacoesParaProgramacao
                .stream()
                .filter(operacaoParaProgramacao -> operacaoParaProgramacao.getSetorOp() == celulaSetorOperacaoSelecionada.getCodigoSetorOp())
                .collect(Collectors.toList());
//        Comparator<SdProgramacaoPacote001> comparator = Comparator.comparing(programacao -> programacao.getOrdem());
//        comparator = comparator.thenComparing(Comparator.comparing(programacao -> programacao.getQuebra()));
//        operacoesProgramadasSetor.sort(comparator);
        
        return operacoesProgramadasSetor;
    }
    
    /**
     * Rotina para selecionar as operações do setor selecionado na ComboBox.
     *
     * @param setorSelecionado
     */
    private void selectSetorOperacao(SdSetorOp001 setorSelecionado) {
        operacoesSetorSelecionadoParaProgramacao.clear();
        celulaSelecionada.clear();
        celulaSetorOperacaoSelecionada = null;
        operacoesSetorSelecionado.clear();
        operacoesSetorSelecionado = operacoesParaProgramacao
                .stream()
                .filter(operacaoParaProgramacao -> operacaoParaProgramacao.getSetorOp() == setorSelecionado.getCodigo())
                .collect(Collectors.toList());
        if (emVisualizacao.or(emEdicao).get()) {
            operacoesSetorSelecionadoParaProgramacao.set(FXCollections.observableList(operacoesSetorSelecionado));
            tblProgramacao.refresh();
        }
        
        if (operacoesSetorSelecionado.get(0).getCelulaObject() != null) {
            celulaSelecionada.copy(operacoesSetorSelecionado.get(0).getCelulaObject());
            celulaSetorOperacaoSelecionada = new SdCelula(celulaSelecionada);
            operacoesSetorSelecionadoParaProgramacao.set(FXCollections.observableList(operacoesSetorSelecionado));
            tblProgramacao.refresh();
        }
    }
    
    /**
     * Rotina que cria o objeto da operacao para ser programada.
     *
     * @param operacaoRoteiro
     * @return SdProgramacaoPacote001
     * @throws NumberFormatException
     * @throws SQLException
     */
    private SdProgramacaoPacote001 criaObjetoOperacaoRoteiro(SdOperRoteiroOrganize001 operacaoRoteiro)
            throws NumberFormatException, SQLException {
        SdProgramacaoPacote001 operacaoParaProgramacao = new SdProgramacaoPacote001(
                pacoteSelecionado.get().getCodigo(),
                operacaoRoteiro.getId().getOperacao().getCodorg(),
                operacaoRoteiro.getId().getOrdem(),
                LocalDateTime.now(),
                LocalDateTime.now(),
                LocalDateTime.now(),
                0,
                Integer.parseInt(operacaoRoteiro.getSetorOperacao()),
                operacaoRoteiro.getId().getOperacao().getEquipamentoOrganize().getCodorg(),
                0,
                pacoteSelecionado.get().getQtde(),
                operacaoRoteiro.getIndependencia(),
                0,
                ibPadraoProgramacao,
                1,
                0,
                operacaoRoteiro.getTempoOp().doubleValue());
        
        return operacaoParaProgramacao;
    }
    
    /**
     * Rotina que copia as programações de outro pacote.
     * As operações já vem com toda a configuração salva.
     *
     * @throws SQLException
     */
    private void carregaRoteiroOperacoesProduto() throws SQLException, NumberFormatException {
        roteiroOperacoesProduto = DAOFactory.getSdOperRoteiroOrganize001DAO().getByReferenciaAndSetor(ofParaProgramacao.getCodigo(), programacaoProducaoOf.get().getSetor());
        
        operacoesBase.clear();
        operacoesParaProgramacao.clear();
        SdProgramacaoPacote001 operacaoAnterior = null;
        for (SdOperRoteiroOrganize001 operacaoProduto : roteiroOperacoesProduto) {
            SdProgramacaoPacote001 operacaoAtual = criaObjetoOperacaoRoteiro(operacaoProduto);
            operacaoAtual.setOperacaoAnterior(operacaoAnterior);
            if (operacaoAnterior != null) {
                operacaoAnterior.setOperacaoPosterior(operacaoAtual);
            }
            operacaoAnterior = operacaoAtual;
            operacoesBase.add(operacaoAtual);
            operacoesParaProgramacao.add(operacaoAtual);
        }
        
        setoresOperacaoRoteiroProduto.set(DAOFactory.getSdOperRoteiroOrganize001DAO().getSetoresOperacaoByReferenciaAndSetor(ofParaProgramacao.getCodigo(), programacaoProducaoOf.get().getSetor()));
    }
    
    /**
     * Rotina que carrega o roteiro do produto e cria a lista de operações para
     * programacao de produção. Cria a lista dos setores de operação que existem
     * no roteiro para alimentação do combobox.
     *
     * @throws SQLException
     */
    private void carregaRoteiroOperacoesProduto(Boolean copiarPacote) throws SQLException, NumberFormatException {
        if (copiarPacote) {
            ObservableList<SdProgramacaoPacote001> operacoesCopiadas = DAOFactory.getSdProgramacaoPacote001DAO().getByPacote(cboxCopiarPacote.getSelectionModel().getSelectedItem().getCodigo());
            
            operacoesBase.clear();
            operacoesParaProgramacao.clear();
            SdProgramacaoPacote001 operacaoAnterior = null;
            for (SdProgramacaoPacote001 operacaoProgramada : operacoesCopiadas) {
                SdProgramacaoPacote001 operacaoAtual = operacaoProgramada;
                operacaoAtual.setQtde(pacoteSelecionado.get().getQtde());
                operacaoAtual.setCelula(0);
                operacaoAtual.setCelulaObject(null);
                operacaoAtual.setPacote(pacoteSelecionado.get().getCodigo());
                operacaoAtual.setOperacaoAnterior(operacaoAnterior);
                if (operacaoAnterior != null) {
                    operacaoAnterior.setOperacaoPosterior(operacaoAtual);
                }
                operacaoAnterior = operacaoAtual;
                operacoesBase.add(operacaoAtual);
                operacoesParaProgramacao.add(operacaoAtual);
            }
            
            setoresOperacaoRoteiroProduto.set(DAOFactory.getSdOperRoteiroOrganize001DAO().getSetoresOperacaoByReferenciaAndSetor(ofParaProgramacao.getCodigo(), programacaoProducaoOf.get().getSetor()));
        } else {
            carregaRoteiroOperacoesProduto();
        }
    }
    
    /**
     * Rotina que carrega a programação do pacote para exibição ou edicao
     *
     * @throws SQLException
     */
    private void carregaProgramacaoPacote(SdPacote001 pacote) throws SQLException, NumberFormatException {
        ObservableList<SdProgramacaoPacote001> roteiroOperacoesPacote = DAOFactory.getSdProgramacaoPacote001DAO().getByPacote(pacote.getCodigo());
        
        operacoesBase.clear();
        operacoesParaProgramacao.clear();
        SdProgramacaoPacote001 operacaoAnterior = null;
        for (SdProgramacaoPacote001 operacaoProduto : roteiroOperacoesPacote) {
            SdProgramacaoPacote001 operacaoAtual = operacaoProduto;
            operacaoAtual.setOperacaoAnterior(operacaoAnterior);
            if (operacaoAnterior != null) {
                operacaoAnterior.setOperacaoPosterior(operacaoAtual);
            }
            operacaoAnterior = operacaoAtual;
            operacoesBase.add(operacaoAtual);
            operacoesParaProgramacao.add(operacaoAtual);
        }
        programacaoComLancametos.set(operacoesParaProgramacao.stream().filter(SdProgramacaoPacote001::getProgramacaoApontada).count() > 0);
        setoresComLancamento.addAll(operacoesParaProgramacao.stream().filter(SdProgramacaoPacote001::getProgramacaoApontada).map(SdProgramacaoPacote001::getSetorOp).distinct().collect(Collectors.toList()));
        carregaConflitos();
        setoresOperacaoRoteiroProduto.set(DAOFactory.getSdOperRoteiroOrganize001DAO().getSetoresOperacaoByReferenciaAndSetor(ofParaProgramacao.getCodigo(), programacaoProducaoOf.get().getSetor()));
    }
    
    /**
     * Rotina que carrega a programacao de um pacote.
     */
    private void carregaSetoresParaLeitura() {
        if (pacoteSelecionado.get() == null) {
            GUIUtils.showMessage("Você deve selecionar um pacote para iniciar a programação.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        
        try {
            carregaProgramacaoPacote(pacoteSelecionado.get());
            cboxSetoresOperacao.getSelectionModel().select(0);
            
        } catch (SQLException | NumberFormatException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    /**
     * Verifica se o dia é um dia útil. O dia útil é cadastrado no TI na tabela
     * ano_001
     *
     * @param dhProrgamacaoOperacao
     * @return
     * @throws SQLException
     */
    private LocalDateTime verificaDiaUtil(LocalDateTime dhProrgamacaoOperacao) throws SQLException {
        
        while (DAOFactory.getSdTurnoDAO().getDateNotUtil(dhProrgamacaoOperacao).size() > 0) {
            dhProrgamacaoOperacao = dhProrgamacaoOperacao.plusDays(1);
        }
        
        return dhProrgamacaoOperacao;
    }
    
    /**
     * Rotina que verifica o horário de inicio da programacao e retorna uma nova
     * data para o início, caso a operacao inicie fora de um turno da célula. É
     * verificado também se é um dia útil.
     *
     * @param operacaoProgramada
     * @return
     */
    private void verificaDhInicio(SdProgramacaoPacote001 operacaoProgramada)
            throws SQLException {
        ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                .getIntervalosTurno(operacaoProgramada.getCelulaObject().getTurnos()
                        .stream()
                        .map(turno -> turno.getCodigoTurno() + "")
                        .collect(Collectors.toList()));
        
        LocalDateTime dhInicio = operacaoProgramada.getDhInicio();
        for (Map<String, Object> intervalo : intervalos) {
            LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
            LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
            LocalDateTime horarioProgramacao = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    dhInicio.getHour() >= 0 && dhInicio.getHour() < 8 ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                    dhInicio.getHour(),
                    dhInicio.getMinute());
            Long tempoIntervalo = (Long) intervalo.get("intervalo");
            
            if (horarioProgramacao.isAfter(inicioIntervalo) && horarioProgramacao.isBefore(fimIntervalo)) {
                if (((String) intervalo.get("tipo")).equals("intra")) {
                    dhInicio = dhInicio.plusMinutes(tempoIntervalo);
                } else {
                    dhInicio.plusDays(dhInicio.getHour() > 0 && dhInicio.getHour() < fimIntervalo.getHour() ? 0L : 1L);
                    dhInicio = LocalDateTime.of(
                            dhInicio.getYear(),
                            dhInicio.getMonth(),
                            dhInicio.getDayOfMonth(),
                            fimIntervalo.getHour(),
                            fimIntervalo.getMinute());
                }
                break;
            }
        }
        dhInicio = verificaDiaUtil(dhInicio);
        
        operacaoProgramada.setDhInicio(dhInicio);
    }
    
    /**
     * Rotina que verifica o horário de ib da programacao e retorna uma nova
     * data para o ib, caso a operacao tem ib fora de um turno da célula. É
     * verificado também se é um dia útil.
     *
     * @param operacaoProgramada
     * @return
     */
    private void verificaDhIb(SdProgramacaoPacote001 operacaoProgramada)
            throws SQLException {
        ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                .getIntervalosTurno(operacaoProgramada.getCelulaObject().getTurnos()
                        .stream()
                        .map(turno -> turno.getCodigoTurno() + "")
                        .collect(Collectors.toList()));
        
        LocalDateTime dhIb = operacaoProgramada.getDhIb();
        for (Map<String, Object> intervalo : intervalos) {
            LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
            LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
            LocalDateTime horarioProgramacao = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    dhIb.getHour() >= 0 && dhIb.getHour() < 8 ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                    dhIb.getHour(),
                    dhIb.getMinute());
            Long tempoIntervalo = (Long) intervalo.get("intervalo");
            
            if (horarioProgramacao.isAfter(inicioIntervalo) && horarioProgramacao.isBefore(fimIntervalo)) {
                dhIb = dhIb.plusMinutes(tempoIntervalo);
            }
        }
        dhIb = verificaDiaUtil(dhIb);
        
        operacaoProgramada.setDhIb(dhIb);
    }
    
    private Double getTempoOperacaoAgrupada(SdProgramacaoPacote001 operacao) {
        final DoubleProperty tempoOperacao = new SimpleDoubleProperty(operacao.getTempoOperacao());
        
        operacoesParaProgramacao.stream()
                .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == operacao.getOperacao()
                        && programacaoAgrupada.getSetorOp() == operacao.getSetorOp())
                .forEach(programacaoAgrupada -> tempoOperacao.set(tempoOperacao.add(programacaoAgrupada.getTempoOperacao()).get()));
        
        return tempoOperacao.get();
    }
    
    /**
     * Rotina que verifica o horário de fim da programacao e retorna uma nova
     * data para o fim, caso a operacao finalize fora de um turno da célula. É
     * verificado também se é um dia útil. Caso o fim fique fora do turno, é
     * quebrada a programação e gerado uma nova com o início do próximo turno.
     *
     * @param operacaoProgramada
     * @return
     */
    private void verificaDhFim(SdProgramacaoPacote001 operacaoProgramada)
            throws SQLException {
        
        //Transforma a data para um dia 01/01/1900 ou 02/01/1900 para testes com os horários dos turnos.
        LocalDateTime dhInicioParaBuscarTurno = LocalDateTime.of(
                1900,
                Month.JANUARY,
                1,
                operacaoProgramada.getDhInicio().getHour(),
                operacaoProgramada.getDhInicio().getMinute());
        SdTurno turnoAtual = DAOFactory.getSdTurnoDAO().getByDateTime(dhInicioParaBuscarTurno,
                operacaoProgramada.getCelulaObject().getTurnos().stream().map(turno -> turno.getCodigoTurno() + "").collect(Collectors.toList()));
        if (turnoAtual == null) {
            GUIUtils.showMessage("Ocorreu um erro na verificação da data fim para a programação. Programação da operação: " + operacaoProgramada.toErrorPrint());
        }
        Map<String, Object> proximoTurno = DAOFactory.getSdTurnoDAO().getProximoTurno(turnoAtual.getCodigo());
        
        //Transforma a data para um dia 01/01/1900 ou 02/01/1900 para testes com os horários dos turnos.
        LocalDateTime dhVerificacaoTurno = LocalDateTime.of(
                turnoAtual.getDhInicioTurno().getYear(),
                turnoAtual.getDhInicioTurno().getMonth(),
                turnoAtual.getDhInicioTurno().getDayOfMonth() +
                        (operacaoProgramada.getDhFim().getHour() >= 0 && operacaoProgramada.getDhFim().getHour() < turnoAtual.getDhInicioTurno().getHour()
//                        ? turnoAtual.getDhInicioTurno().getDayOfMonth() + operacaoProgramada.getDhInicio().getDayOfMonth() != operacaoProgramada.getDhFim().getDayOfMonth() ? 1 : 0
                                ? 1
//                        : turnoAtual.getDhInicioTurno().getDayOfMonth() + 1 + operacaoProgramada.getDhInicio().getDayOfMonth() != operacaoProgramada.getDhFim().getDayOfMonth() ? 1 : 0),
                                : 0),
                operacaoProgramada.getDhFim().getHour(),
                operacaoProgramada.getDhFim().getMinute());
        
        // Se a data de verificação for maior que a data fim do turno atual, tem que ser quebrada a operação
        if (dhVerificacaoTurno.isAfter(turnoAtual.getDhFimTurno())) {
            SdProgramacaoPacote001 operacaoQuebrada = new SdProgramacaoPacote001(operacaoProgramada);
            
            // Informações para atualização na programação atual após quebra
            LocalDateTime novaDhFimOperacaoProgramada = LocalDateTime.of(
                    operacaoProgramada.getDhInicio().getYear(),
                    operacaoProgramada.getDhInicio().getMonth(),
                    operacaoProgramada.getDhInicio().getDayOfMonth(),
                    turnoAtual.getDhFimTurno().getHour(),
                    turnoAtual.getDhFimTurno().getMinute());
            if (dhVerificacaoTurno.getDayOfWeek() == DayOfWeek.SATURDAY) {
                novaDhFimOperacaoProgramada = LocalDateTime.of(
                        operacaoProgramada.getDhInicio().getYear(),
                        operacaoProgramada.getDhInicio().getMonth(),
                        operacaoProgramada.getDhInicio().getDayOfMonth(),
                        12,
                        35);
            }
            double tempoTotalProgramacaoAtual = ChronoUnit.MINUTES.between(operacaoProgramada.getDhInicio(), novaDhFimOperacaoProgramada);
            tempoTotalProgramacaoAtual -= removeIntervaloDhFim(operacaoProgramada);
            long qtdePecasProgramacaoAtual = (long) (tempoTotalProgramacaoAtual / getTempoOperacaoAgrupada(operacaoProgramada));
            long qtdePecasProgramacaoQuebrada = operacaoProgramada.getQtde() - qtdePecasProgramacaoAtual;
            double tempoTotalProgramacaoQuebrada = qtdePecasProgramacaoQuebrada * getTempoOperacaoAgrupada(operacaoProgramada);
            LocalDateTime novaDhInicioOperacaoQuebrada
                    = novaDhFimOperacaoProgramada.plusDays(turnoAtual.getCodigo() == (int) proximoTurno.get("codigo") ? 1 : 0).
                    withHour(((LocalDateTime) proximoTurno.get("inicio")).getHour()).
                    withMinute(((LocalDateTime) proximoTurno.get("inicio")).getMinute());
            novaDhInicioOperacaoQuebrada = verificaDiaUtil(novaDhInicioOperacaoQuebrada);
            LocalDateTime novaDhFimOperacaoQuebrada = novaDhInicioOperacaoQuebrada.plusMinutes((long) tempoTotalProgramacaoQuebrada);
            
            operacaoProgramada.setDhFim(novaDhFimOperacaoProgramada);
            operacaoProgramada.setTempoTotal(tempoTotalProgramacaoAtual);
            operacaoProgramada.setQtde((int) qtdePecasProgramacaoAtual);
            
            operacaoQuebrada.setDhInicio(novaDhInicioOperacaoQuebrada);
            operacaoQuebrada.setDhIb(novaDhInicioOperacaoQuebrada);
            operacaoQuebrada.setDhFim(novaDhFimOperacaoQuebrada);
            verificaDhFimEmIntervalo(operacaoQuebrada);
            operacaoQuebrada.setTempoTotal(tempoTotalProgramacaoQuebrada);
            operacaoQuebrada.setQtde((int) qtdePecasProgramacaoQuebrada);
            operacaoQuebrada.setQuebra(operacaoQuebrada.getQuebra() + 1);
            
            operacoesParaProgramacao.add(operacoesParaProgramacao.indexOf(operacaoProgramada) + 1, operacaoQuebrada);
        }
        
    }
    
    /**
     * verifica se a data fim enté dentro de um período de intrajornada.
     * verifica também se uma programação inicia antes do intervalo e termina
     * após incrementando o tempo de intervalo no tempo da operação.
     *
     * @param operacaoProgramada
     * @throws SQLException
     */
    private void verificaDhFimEmIntervalo(SdProgramacaoPacote001 operacaoProgramada) throws SQLException {
        // Verifica se dh fim está em um intervalo
        ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                .getIntervalosTurno(operacaoProgramada.getCelulaObject().getTurnos()
                        .stream()
                        .map(turno -> turno.getCodigoTurno() + "")
                        .collect(Collectors.toList()));
        
        LocalDateTime dhFimComIntervalo = operacaoProgramada.getDhFim();
        for (Map<String, Object> intervalo : intervalos) {
            LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
            LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
            LocalDateTime horarioProgramacaoInicio = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    inicioIntervalo.getDayOfMonth(),
                    operacaoProgramada.getDhInicio().getHour(),
                    operacaoProgramada.getDhInicio().getMinute());
            LocalDateTime horarioProgramacaoFim = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    dhFimComIntervalo.getHour() >= 0 && dhFimComIntervalo.getHour() < 8 ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                    dhFimComIntervalo.getHour(),
                    dhFimComIntervalo.getMinute());
            Long tempoIntervalo = (Long) intervalo.get("intervalo");
            
            if (((String) intervalo.get("tipo")).equals("intra")) {
                if (horarioProgramacaoFim.isAfter(inicioIntervalo) && horarioProgramacaoFim.isBefore(fimIntervalo)) {
                    dhFimComIntervalo = dhFimComIntervalo.plusMinutes(tempoIntervalo);
                } else if (horarioProgramacaoInicio.isBefore(inicioIntervalo) && horarioProgramacaoFim.isAfter(fimIntervalo)) {
                    dhFimComIntervalo = dhFimComIntervalo.plusMinutes(tempoIntervalo);
                }
                break;
            }
        }
        operacaoProgramada.setDhFim(dhFimComIntervalo);
    }
    
    /**
     * verifica se a data fim enté dentro de um período de intrajornada.
     * verifica também se uma programação inicia antes do intervalo e termina
     * após incrementando o tempo de intervalo no tempo da operação.
     *
     * @param operacaoProgramada
     * @throws SQLException
     */
    private Long removeIntervaloDhFim(SdProgramacaoPacote001 operacaoProgramada) throws SQLException {
        // Verifica se dh fim está em um intervalo
        ObservableList<Map<String, Object>> intervalos = DAOFactory.getSdTurnoDAO()
                .getIntervalosTurno(operacaoProgramada.getCelulaObject().getTurnos()
                        .stream()
                        .map(turno -> turno.getCodigoTurno() + "")
                        .collect(Collectors.toList()));
        
        LocalDateTime dhFimComIntervalo = operacaoProgramada.getDhFim();
        for (Map<String, Object> intervalo : intervalos) {
            LocalDateTime inicioIntervalo = ((LocalDateTime) intervalo.get("inicio"));
            LocalDateTime fimIntervalo = ((LocalDateTime) intervalo.get("fim"));
            LocalDateTime horarioProgramacaoInicio = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    inicioIntervalo.getDayOfMonth(),
                    operacaoProgramada.getDhInicio().getHour(),
                    operacaoProgramada.getDhInicio().getMinute());
            LocalDateTime horarioProgramacaoFim = LocalDateTime.of(
                    inicioIntervalo.getYear(),
                    inicioIntervalo.getMonth(),
                    dhFimComIntervalo.getHour() >= 0 && dhFimComIntervalo.getHour() < 8 ? inicioIntervalo.getDayOfMonth() + 1 : inicioIntervalo.getDayOfMonth(),
                    dhFimComIntervalo.getHour(),
                    dhFimComIntervalo.getMinute());
            Long tempoIntervalo = (Long) intervalo.get("intervalo");
            
            if (((String) intervalo.get("tipo")).equals("intra")) {
                if (horarioProgramacaoFim.isAfter(inicioIntervalo) && horarioProgramacaoFim.isBefore(fimIntervalo)) {
                    return tempoIntervalo;
                } else if (horarioProgramacaoInicio.isBefore(inicioIntervalo) && horarioProgramacaoFim.isAfter(fimIntervalo)) {
                    return tempoIntervalo;
                }
                break;
            }
        }
        return 0L;
    }
    
    /**
     * Rotina para retornar o tempo total de uma operação somado ao seu
     * agrupamento.
     *
     * @param operacao
     * @return Long
     */
    private Double getTempoTotalOperacao(SdProgramacaoPacote001 operacao) {
        final DoubleProperty tempoTotalOperacao = new SimpleDoubleProperty(operacao.getTempoTotal());
        
        operacoesParaProgramacao.stream()
                .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == operacao.getOperacao()
                        && programacaoAgrupada.getSetorOp() == operacao.getSetorOp())
                .forEach(programacaoAgrupada -> tempoTotalOperacao.set(tempoTotalOperacao.add(programacaoAgrupada.getTempoTotal()).get()));
        
        return tempoTotalOperacao.get();
    }
    
    /**
     * Metódo é executado após a seleção do setor e célula. No início da rotina
     * é verificado a última ocupação da célula e se a primeira operacao a ser
     * analisada não é dependente de uma final de outro setor.
     *
     * @throws SQLException
     */
    private void programarOperacoes(SdProgramacaoPacote001 programacaoSelecionada) throws Exception {
        programarOperacoes(programacaoSelecionada, false);
    }
    
    private void programarOperacoes(SdProgramacaoPacote001 programacaoSelecionada, Boolean alteracaoManual) throws Exception {
        // GET da última operação programada para a célula
        SdProgramacaoPacote001 ultimaOperacaoProgramadaCelula = DAOFactory.getSdProgramacaoPacote001DAO().getUltimaOcupacaoCelula(celulaSetorOperacaoSelecionada.getCodigo());
        // GET da primeira operação que será programada no setor/célula
        int posicaoProgramacaoSelecionada = programacaoSelecionada != null
                ? operacoesParaProgramacao.indexOf(programacaoSelecionada)
                : 0;
        SdProgramacaoPacote001 primeiraOperacaoDoSetor = operacoesParaProgramacao.get(posicaoProgramacaoSelecionada);
        removeOperacoesQuebradas(posicaoProgramacaoSelecionada);
        // GET da hora de fim da última operação do roteiro para programar.
        // É feito um teste para saber se a primeira operação do setor selecionado não tem uma dependencia em outro setor,
        // assim não poderá iniciar antes deste, caso não tenha dependência é "setado" a hora atual do relógio.
        // Após é definido o horário de início da programação do setor.
        // Se a dh de início for maior que a dh ib, assume a dh início a configurada
        // Se ouve uma intervenção de dh pelo programador, essa deve assumir a data do programacar
        LocalDateTime dhInicioProgramacaoOperacao = primeiraOperacaoDoSetor.getDhInicio();
        if (!alteracaoManual) {
            LocalDateTime dhInicioOperacaoAnterior = primeiraOperacaoDoSetor.getOperacaoAnterior() != null
                    && primeiraOperacaoDoSetor.getOperacaoAnterior().getDhIb().isAfter(primeiraOperacaoDoSetor.getDhInicio())
                    ? primeiraOperacaoDoSetor.getOperacaoAnterior().getDhIb() : primeiraOperacaoDoSetor.getDhInicio();
            
            dhInicioProgramacaoOperacao = ultimaOperacaoProgramadaCelula != null && ultimaOperacaoProgramadaCelula.getDhFim().isAfter(dhInicioOperacaoAnterior)
                    ? ultimaOperacaoProgramadaCelula.getDhFim().plusMinutes(1)
                    : dhInicioOperacaoAnterior;
        }
        
        // A operação de programação é realizada para o setor selecionado
        for (int i = posicaoProgramacaoSelecionada; i < operacoesParaProgramacao.size(); i++) {
            SdProgramacaoPacote001 operacaoParaProgramacao = operacoesParaProgramacao.get(i);
            if (operacaoParaProgramacao.getProgramacaoApontada()) {
                continue;
            }
            // condição para que a análise da programação seja realizada somente nas operacções do setor selecionado na célula.
            if (operacaoParaProgramacao.getCelulaObject() != null
                    && operacaoParaProgramacao.getSetorOp() != celulaSetorOperacaoSelecionada.getCodigoSetorOp()
                    && operacaoParaProgramacao.getOperacaoAnterior().getDhIb().isBefore(operacaoParaProgramacao.getDhInicio())) {
                celulaSetorOperacaoSelecionada = operacaoParaProgramacao.getCelulaObject();
            } else if (operacaoParaProgramacao.getCelulaObject() == null && operacaoParaProgramacao.getSetorOp() != celulaSetorOperacaoSelecionada.getCodigoSetorOp()) {
                continue;
            }
            
            operacaoParaProgramacao.setCelula(celulaSetorOperacaoSelecionada.getCodigo());
            operacaoParaProgramacao.setCelulaObject(celulaSetorOperacaoSelecionada);
            
            // Caso a operação que está sendo programada é um agrupamento, assume os valores da operação mãe
            if (operacaoParaProgramacao.getAgrupador() != 0) {
                operacaoParaProgramacao.setDhInicio(operacaoParaProgramacao.getOperacaoAnterior().getDhInicio());
                operacaoParaProgramacao.setDhIb(operacaoParaProgramacao.getOperacaoAnterior().getDhIb());
                operacaoParaProgramacao.setDhFim(operacaoParaProgramacao.getOperacaoAnterior().getDhFim());
                continue;
            }
            
            // Caso a operação seja de quebra, não é realizada a definição das data aqui.
            // As datas s"ao definidas no momento da quebra da operacao.
            if (operacaoParaProgramacao.getQuebra() == 1 || operacaoParaProgramacao.isQuebraManual()) {
                // Define a DH inicial da operação (IB operação anterior).
                if (operacaoParaProgramacao.getIndependencia() != null && operacaoParaProgramacao.getIndependencia().length() > 0) {
                    dhInicioProgramacaoOperacao = getDhOperacaoIndependente(operacaoParaProgramacao, operacaoParaProgramacao.getIndependencia());
                }
                
                if (operacaoParaProgramacao.getQuebra() == 1) {
                    operacaoParaProgramacao.setDhInicio(dhInicioProgramacaoOperacao);
                    verificaDhInicio(operacaoParaProgramacao);
                }
                
                // Define a DH final da operação (DH Inicial + TEMPO TOTAL operação (em minutos))
                operacaoParaProgramacao.setDhFim(operacaoParaProgramacao.getDhInicio()
                        .plusMinutes(getTempoTotalOperacao(operacaoParaProgramacao).longValue()));
                
                verificaDhFimEmIntervalo(operacaoParaProgramacao);
                
                // Define a DH do IB da operação (DH Final - (TEMPO TOTAL operação posterior (em minutos) * IB da operação))
                // é feito um teste para verificar se o IB não é menor que a data de início, caso for seta como IB a DH Inicial
                operacaoParaProgramacao.setDhIb(operacaoParaProgramacao.getDhFim()
                        .minusMinutes((long) (operacaoParaProgramacao.getOperacaoPosterior() == null
                                ? 0
                                : operacaoParaProgramacao.isQuebraManual()
                                ? getTempoTotalOperacao(operacaoParaProgramacao.getOperacaoPosterior()) / 2 * (1 - operacaoParaProgramacao.getIb())
                                : getTempoTotalOperacao(operacaoParaProgramacao.getOperacaoPosterior()) * (1 - operacaoParaProgramacao.getIb()))));
                operacaoParaProgramacao.setDhIb(operacaoParaProgramacao.getDhIb().isBefore(operacaoParaProgramacao.getDhInicio())
                        ? operacaoParaProgramacao.getDhInicio()
                        : operacaoParaProgramacao.getDhIb());
                
                Double pecasIb = getPecasIb(operacaoParaProgramacao);
                operacaoParaProgramacao.setQtdeIb(pecasIb.intValue() > 0 ? pecasIb.intValue() : 1);
                verificaDhIb(operacaoParaProgramacao);
                
            }
            
            // verificação da dh final da operação para verificar se não está fora do turno
            // essa verificação também acontece para as operações que foram quebradas
            verificaDhFim(operacaoParaProgramacao);
            //if (operacaoParaProgramacao.getDhIb().isAfter(operacaoParaProgramacao.getDhFim())) {
            //    operacaoParaProgramacao.setDhIb(operacaoParaProgramacao.getDhFim());
            //}
            
            //verifica se a operação é a ultima do setor e atribui o IB para 100%
            if (operacaoParaProgramacao.getOperacaoPosterior() == null || operacaoParaProgramacao.getOperacaoPosterior().getSetorOp() != operacaoParaProgramacao.getSetorOp()) {
                operacaoParaProgramacao.setDhIb(operacaoParaProgramacao.getDhFim());
            }
            
            // Define a variável com o IB da operacao atual para ser o DH Inicial da próxima operacao
            if (operacaoParaProgramacao.getQuebra() > 1 && operacaoParaProgramacao.isQuebraManual()) {
                dhInicioProgramacaoOperacao = dhInicioProgramacaoOperacao.isBefore(operacaoParaProgramacao.getDhIb()) ? dhInicioProgramacaoOperacao : operacaoParaProgramacao.getDhIb();
            } else if (operacaoParaProgramacao.getQuebra() == 1) {
                dhInicioProgramacaoOperacao = operacaoParaProgramacao.getDhIb();
            }
            operacaoParaProgramacao.setProgramacaoAnalisada(true);
        }
        
        operacoesSetorSelecionado = carregarOperacoesSetor();
        procurarOperadores(operacoesSetorSelecionado);
        carregaConflitos();
        
        // Atribui a programação para o array de exibição da tabela.
        operacoesSetorSelecionadoParaProgramacao.set(FXCollections.observableList(operacoesSetorSelecionado));
        tblProgramacao.refresh();
        
    }
    
    /**
     * Rotina para calculo da quantidade de peças que deverão ser entregues no IB
     *
     * @param operacaoParaProgramacao
     * @return
     */
    private Double getPecasIb(SdProgramacaoPacote001 operacaoParaProgramacao) {
        LocalDateTime dhFimCalculoPecasIb = operacaoParaProgramacao.getDhInicio().plusMinutes((int) (operacaoParaProgramacao.getQtde() * getTempoOperacaoAgrupada(operacaoParaProgramacao)));
        LocalDateTime dhIbCalculoPecasIb = dhFimCalculoPecasIb.minusMinutes((long) (operacaoParaProgramacao.getOperacaoPosterior() == null
                ? 0
                : operacaoParaProgramacao.isQuebraManual()
                ? getTempoTotalOperacao(operacaoParaProgramacao.getOperacaoPosterior()) / 2 * (1 - operacaoParaProgramacao.getIb())
                : getTempoTotalOperacao(operacaoParaProgramacao.getOperacaoPosterior()) * (1 - operacaoParaProgramacao.getIb())));
        dhIbCalculoPecasIb = dhIbCalculoPecasIb.isBefore(operacaoParaProgramacao.getDhInicio())
                ? operacaoParaProgramacao.getDhInicio()
                : dhIbCalculoPecasIb;
        Long minutosIb = Duration.between(operacaoParaProgramacao.getDhInicio(), dhIbCalculoPecasIb).toMinutes();
        return (Double) (minutosIb / getTempoOperacaoAgrupada(operacaoParaProgramacao));
    }
    
    /**
     * Rotina que remove as operações que foram quebradas automaticamente.
     *
     * @param posicaoOperacaoInicial
     */
    private void removeOperacoesQuebradas(Integer posicaoOperacaoInicial) {
        List<SdProgramacaoPacote001> listaDeVerificacao = new ArrayList<>();
        listaDeVerificacao.addAll(operacoesParaProgramacao);
        for (int i = posicaoOperacaoInicial; i < listaDeVerificacao.size(); i++) {
            SdProgramacaoPacote001 operacaoProgramada = listaDeVerificacao.get(i);
            if (operacaoProgramada.getQuebra() > 1 && !operacaoProgramada.isQuebraManual()) {
                if (!operacaoProgramada.getProgramacaoApontada()) {
                    operacoesParaProgramacao.stream()
                            .filter(programacaoOperacao
                                    -> programacaoOperacao.getOperacao() == operacaoProgramada.getOperacao()
                                    && programacaoOperacao.getOrdem() == operacaoProgramada.getOrdem()
                                    && programacaoOperacao.getQuebra() == 1)
                            .forEach(programacaoMaeQuebra
                                    -> {
                                programacaoMaeQuebra.setQtde(programacaoMaeQuebra.getQtde() + operacaoProgramada.getQtde());
                                programacaoMaeQuebra.setTempoTotal(programacaoMaeQuebra.getQtde() * programacaoMaeQuebra.getOperacaoObject().getTempo().doubleValue());
                                programacaoMaeQuebra.setDhFim(programacaoMaeQuebra.getDhFim()
                                        .plusMinutes((long) programacaoMaeQuebra.getTempoTotal()));
                            });
                    operacoesParaProgramacao.remove(operacaoProgramada);
                }
            }
        }
    }
    
    @FXML
    private void btnIniciarProgramacaoOnAction(ActionEvent event) {
        if (((RadioButton) pacoteLancamento.getSelectedToggle()).disableProperty().get()) {
            GUIUtils.showMessage("Você deve selecionar como será o pacote de lançamento da produção.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        
        if (pacoteSelecionado == null) {
            GUIUtils.showMessage("Você deve selecionar um pacote para iniciar a programação.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        this.ibPadraoProgramacao = (tboxIbPadraoProgramacao.getText().isEmpty()) ? 0.33 : (Double.parseDouble(tboxIbPadraoProgramacao.getText()) / 100.0);
        
        emProgramacao.set(true);
//        cboxCopiarPacote.getItems().add(pacoteSelecionado);
        cboxPacote.getItems().remove(pacoteSelecionado);
        
        try {
            carregaRoteiroOperacoesProduto(cboxCopiarPacote.getSelectionModel().getSelectedItem() != null);
            cboxSetoresOperacao.getSelectionModel().select(0);
            
        } catch (SQLException | NumberFormatException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        if (setorOperacaoSelecionado == null) {
            GUIUtils.showMessage("Você deve selecionar o SETOR DE OPERAÇÃO", Alert.AlertType.INFORMATION);
            return;
        }
        
        if (setoresComLancamento.contains(setorOperacaoSelecionado.getCodigo())) {
            GUIUtils.showMessage("Esta programação já existe lançamentos para esse setor, não poderá ser alterado a célula.");
            return;
        }
        
        try {
            ObservableList<SdCelula> sendSelected = FXCollections.observableArrayList();
            if (celulaSelecionada.getCodigo() != 0) {
                sendSelected.add(celulaSelecionada);
            }
            FilterCelulaController ctrlFilter = new FilterCelulaController(Modals.FXMLWindow.FilterCelula, sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT,
                    setorOperacaoSelecionado.getCodigo()
            );
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                celulaSelecionada.copy(ctrlFilter.selectedObjects.get(0));
                celulaSelecionada.setSelected(true);
                celulaSetorOperacaoSelecionada = new SdCelula(celulaSelecionada);
//                bindCelulaSelecionada(celulaSetorOperacaoSelecionada);
                this.programarOperacoes(operacoesSetorSelecionado.get(0));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarCelulaOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void tboxCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarCelulaOnAction(null);
        }
    }
    
    @FXML
    private void btnConfirmarProgramacaoOnAction(ActionEvent event) {
        
        //<editor-fold defaultstate="collapsed" desc="Validações">
        if (hasConflitoOperador(operacoesParaProgramacao)) {
            if (!GUIUtils.showQuestionMessageDefaultSim("Existem operações com conflitos de colaborador, você deseja salvar a programação com os conflitos?")) {
                return;
            }
        }

//        if (hasNullOperador(operacoesParaProgramacao)) {
//            GUIUtils.showMessage("Existem operações sem colaborador, você deve inserir um operador para todas as operações do setor!", Alert.AlertType.WARNING);
//            return;
//        }

//        if (hasCelulaNull(operacoesParaProgramacao)) {
//            GUIUtils.showMessage("Existem operações sem célula definida, você deve inserir uma célula para todas as operações e setores!", Alert.AlertType.WARNING);
//            return;
//        }
        //</editor-fold>
        
        try {
            
            if (emEdicao.not().get()) {
                DAOFactory.getSdProgramacaoOf001DAO().save(programacaoProducaoOf.get());
                DAOFactory.getSdPacote001DAO().save(pacoteSelecionado.get());
                for (SdPacote001 pacoteLancamento : listaDePacotesLancamento) {
                    DAOFactory.getSdPacoteLancamento001DAO().save(pacoteLancamento);
                }
            } else {
                //DAOFactory.getSdProgramacaoPacote001DAO().deleteByPacote(pacoteSelecionado.get());
                if (programacaoComLancametos.not().get()) {
                    DAOFactory.getSdPacoteLancamento001DAO().deleteByPacoteProg(pacoteSelecionado.get());
                    for (SdPacote001 pacoteLancamento : listaDePacotesLancamento) {
                        DAOFactory.getSdPacoteLancamento001DAO().save(pacoteLancamento);
                    }
                }
                DAOFactory.getSdPacote001DAO().update(pacoteSelecionado.get());
                btnIniciarInativo.set(true);
            }
            for (SdProgramacaoPacote001 programacaoOperacao : operacoesParaProgramacao) {
                
                if (programacaoOperacao.getProgramacaoApontada()) {
                    DAOFactory.getSdProgramacaoPacote001DAO().update(programacaoOperacao);
                } else {
                    DAOFactory.getSdProgramacaoPacote001DAO().delete(programacaoOperacao);
                    DAOFactory.getSdProgramacaoPacote001DAO().save(programacaoOperacao);
                }
                if (programacaoOperacao.getColaboradorObject() != null &&
                        DAOFactory.getSdPolivalencia001DAO().getByPolivalencia(programacaoOperacao.getColaboradorObject(), programacaoOperacao.getOperacaoObject()).size() == 0) {
                    SdPolivalencia001 polivalencia = new SdPolivalencia001(programacaoOperacao.getColaborador(), programacaoOperacao.getOperacao(), 0, "S");
                    DAOFactory.getSdPolivalencia001DAO().forcarPolivalencia(polivalencia);
                }
            }
            operacoesParaProgramacao.stream().filter(programacao -> programacao.getColaboradorObject() != null)
                    .collect(Collectors.groupingBy(SdProgramacaoPacote001::getColaborador))
                    .forEach((agrupador, programacoesAgrupadas) -> {
                        programacoesAgrupadas.stream()
                                .collect(Collectors.groupingBy(datasAgrupadas -> datasAgrupadas.getDhInicio().toLocalDate()))
                                .forEach((agrupadorData, operacoesAgrupadas) -> {
                                    double tempoOcupado = operacoesAgrupadas.stream().mapToDouble(SdProgramacaoPacote001::getTempoTotal).sum();
                                    LocalDate dataDiario = agrupadorData;
                                    Integer codigoColaborador = agrupador;
                                    try {
                                        DAOFactory.getSdColaborador001DAO().mergeToOcupados(codigoColaborador, dataDiario, tempoOcupado);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                                        GUIUtils.showException(e);
                                    }
                                });
                    });
            
            if (GUIUtils.showQuestionMessageDefaultSim("Deseja imprimir o romaneio da programação realizada?")) {
                btnImprimirProgramacaoOnAction(null);
            }
            
            listaDePacotesLancamento.clear();
            setoresOperacaoRoteiroProduto.clear();
            operacoesSetorSelecionadoParaProgramacao.clear();
            setoresComLancamento.clear();
            programacaoComLancametos.set(false);
            setorOperacaoSelecionado = null;
            celulaSelecionada.clear();
            celulaSetorOperacaoSelecionada = null;
            colaboradorSemPolivalencia = null;
            colaboradoresParaProgramacao.clear();
            ocupacaoColaborador.clear();
            emProgramacao.set(false);
            tboxNumeroOfCopia.clear();
            cboxCopiarPacote.getItems().clear();
            listaDePacotes.remove(pacoteSelecionado.get());
            
            if (listaDePacotes.size() > 0) {
                if (GUIUtils.showQuestionMessageDefaultSim("Deseja manter a configuração do último pacote programado?")) {
                    tboxNumeroOfCopia.setText(ofParaProgramacao.getNumero());
                    cboxCopiarPacote.getItems().add(pacoteSelecionado.get());
                    cboxCopiarPacote.getSelectionModel().select(0);
                }
                cboxPacote.requestFocus();
            } else {
                pacoteSelecionado.set(null);
                
                ofParaProgramarSelecionada.get().setProgramado(true);
                ofParaProgramarSelecionada.get().setStatusOf("G");
                //ofParaProgramarSelecionada.get().setDtProg(programacaoProducaoOf.get().getDataCad());
                
                emEdicao.set(false);
                tblOfParaProgramacao.refresh();
                tabsWindow.getSelectionModel().select(0);
                tabListagem.setDisable(false);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnConfirmarProgramacaoOnAction");
        }
    }
    
    @FXML
    private void btnCancelarProgramacaoOnAction(ActionEvent event) {
        
        rbtnReferencia.setSelected(true);
        rbtnReferenciaLancamento.setSelected(true);
        
        programacaoProducaoOf.set(null);
        ofParaProgramacao = null;
        totalPacotesOf.set(0);
        listaDePacotes.clear();
        listaDePacotesLancamento.clear();
        pacoteSelecionado.set(null);
        programacaoComLancametos.set(false);
        emProgramacao.set(false);
        emVisualizacao.set(false);
        emEdicao.set(false);
        roteiroOperacoesProduto = null;
        operacoesParaProgramacao.clear();
        operacoesBase.clear();
        setoresOperacaoRoteiroProduto.clear();
        operacoesSetorSelecionado.clear();
        operacoesSetorSelecionadoParaProgramacao.clear();
        setorOperacaoSelecionado = null;
        celulaSelecionada.clear();
        celulaSetorOperacaoSelecionada = null;
        colaboradorSemPolivalencia = null;
        colaboradoresParaProgramacao.clear();
        ocupacaoColaborador.clear();
        tboxNumeroOfCopia.clear();
        cboxCopiarPacote.getItems().clear();
        setoresComLancamento.clear();
        tblProgramacao.refresh();
        
        tabsWindow.getSelectionModel().select(0);
        tabListagem.setDisable(false);
    }
    
    @FXML
    private void requestMenuQuebrarOperacaoOnAction(ActionEvent event) {
        SdProgramacaoPacote001 programacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
        
        
        if (programacaoSelecionada != null) {
            try {
                if (programacaoSelecionada.getCelulaObject() == null) {
                    GUIUtils.showMessage("Para você quebrar uma operação, você deve antes selecionar a célula.", Alert.AlertType.WARNING);
                    return;
                }
                
                // Cria a programação de quebra de dia da operacao
                SdProgramacaoPacote001 operacaoQuebrada = new SdProgramacaoPacote001(programacaoSelecionada);
                if (programacaoSelecionada.getQtde() % 2 == 0) {
                    programacaoSelecionada.setQtde(programacaoSelecionada.getQtde() / 2);
                    operacaoQuebrada.setQtde(operacaoQuebrada.getQtde() / 2);
                } else {
                    programacaoSelecionada.setQtde((programacaoSelecionada.getQtde() / 2) + 1);
                    operacaoQuebrada.setQtde(operacaoQuebrada.getQtde() / 2);
                }
                
                // Atualiza a data hora fim da operacao que foi quebrada para o fim da nova qtde
                programacaoSelecionada.setTempoTotal(programacaoSelecionada.getQtde() * programacaoSelecionada.getOperacaoObject().getTempo().doubleValue());
                programacaoSelecionada.setDhFim(programacaoSelecionada.getDhInicio().plusMinutes((long) (programacaoSelecionada.getTempoTotal())));
                programacaoSelecionada.setDhIb(programacaoSelecionada.getDhFim()
                        .minusMinutes((long) (programacaoSelecionada.getOperacaoPosterior() == null
                                ? 0
                                : (getTempoTotalOperacao(programacaoSelecionada.getOperacaoPosterior()) / 2) * programacaoSelecionada.getIb())));
                programacaoSelecionada.setDhIb(programacaoSelecionada.getDhIb().isBefore(programacaoSelecionada.getDhInicio())
                        ? programacaoSelecionada.getDhInicio()
                        : programacaoSelecionada.getDhIb());
                verificaDhIb(programacaoSelecionada);
                programacaoSelecionada.setQuebraManual(true);
                
                // Atualiza a data hora fim da operacao gerada com a quebra para o fim da nova qtde
                operacaoQuebrada.setTempoTotal(operacaoQuebrada.getQtde() * operacaoQuebrada.getOperacaoObject().getTempo().doubleValue());
                operacaoQuebrada.setDhFim(operacaoQuebrada.getDhInicio().plusMinutes((long) (operacaoQuebrada.getTempoTotal())));
                operacaoQuebrada.setDhIb(programacaoSelecionada.getDhIb());
                operacaoQuebrada.setQuebraManual(true);
                operacaoQuebrada.setQuebra(operacaoQuebrada.getQuebra() + 1);
                //Inclui a operação gerada com a quebra na lista de programacoes
                int posicaoOperacaoQuebrada = operacoesParaProgramacao.indexOf(programacaoSelecionada) + 1;
                operacoesParaProgramacao.add(posicaoOperacaoQuebrada, operacaoQuebrada);
                
                // Atualiza a lista das operações do produto com a operação quebrada fazendo a análise a partir da operação depois da quebra gerada.
                if (programacaoSelecionada.getOperacaoPosterior() != null) {
                    programarOperacoes(programacaoSelecionada.getOperacaoPosterior());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::requestMenuQuebrarOperacaoOnAction");
            }
            tblProgramacao.refresh();
        }
    }
    
    @FXML
    private void requestMenuExcluirQuebrarOperacaoOnAction(ActionEvent event) {
        SdProgramacaoPacote001 programacaoSelecionadaQuebrada = tblProgramacao.getSelectionModel().getSelectedItem();
        if (programacaoSelecionadaQuebrada.getQuebra() == 1) {
            GUIUtils.showMessage("Somente operações quebradas que poderão ser excluídas.", Alert.AlertType.WARNING);
            return;
        }
        
        final ObjectProperty<SdProgramacaoPacote001> programacaoMaeQuebrada = new SimpleObjectProperty<>();
        if (programacaoSelecionadaQuebrada != null) {
            try {
                operacoesParaProgramacao.stream()
                        .filter(programacaoOperacao
                                -> programacaoOperacao.getOperacao() == programacaoSelecionadaQuebrada.getOperacao()
                                && programacaoOperacao.getOrdem() == programacaoSelecionadaQuebrada.getOrdem()
                                && programacaoOperacao.getQuebra() == 1)
                        .forEach(programacaoMaeQuebra
                                -> {
                            programacaoMaeQuebra.setQtde(programacaoMaeQuebra.getQtde() + programacaoSelecionadaQuebrada.getQtde());
                            programacaoMaeQuebra.setTempoTotal(programacaoMaeQuebra.getQtde() * programacaoMaeQuebra.getOperacaoObject().getTempo().doubleValue());
                            programacaoMaeQuebrada.set(programacaoMaeQuebra);
                        });
                operacoesParaProgramacao.remove(programacaoSelecionadaQuebrada);
                
                programarOperacoes(programacaoMaeQuebrada.get());
            } catch (Exception ex) {
                ex.printStackTrace();
                GUIUtils.showException(ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::requestMenuExcluirQuebrarOperacaoOnAction");
            }
            tblProgramacao.refresh();
        }
        
    }
    
    @FXML
    private void requestMenuExcluirColaboradorOnAction(ActionEvent event) {
        SdProgramacaoPacote001 programacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
        
        if (programacaoSelecionada.getProgramacaoApontada()) {
            GUIUtils.showMessage("Existem lançamentos de produção para essa operação. Não será possível excluir o colaborador das operações com produção.");
            return;
        }
        
        programacaoSelecionada.setColaborador(0);
        programacaoSelecionada.setColaboradorObject(null);
        programacaoSelecionada.setHasConflito(false);
        
        operacoesParaProgramacao.stream()
                .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == programacaoSelecionada.getOperacao()
                        && programacaoAgrupada.getSetorOp() == programacaoSelecionada.getSetorOp())
                .forEach(programacaoAgrupada -> {
                    programacaoAgrupada.setColaborador(0);
                    programacaoAgrupada.setColaboradorObject(null);
                    programacaoAgrupada.setHasConflito(false);
                });
        try {
            carregaConflitos();
            tblProgramacao.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
        }
    }
    
    @Deprecated
    @FXML
    private void btnGuardarProgramacaoOnAction(ActionEvent event) {
    }
    
    @Deprecated
    @FXML
    private void tboxNumeroOfCopiaOnKeyReleased(KeyEvent event) {
    }
    
    @FXML
    private void btnProcurarPacotesOfOnAction(ActionEvent event) {
        try {
            ObservableList<SdPacote001> pacotesParaCopiar = DAOFactory.getSdPacote001DAO().getByNumeroAndCodigo(tboxNumeroOfCopia.getText(), ofParaProgramarSelecionada.get().getReferencia());
            if (pacotesParaCopiar.size() > 0) {
                cboxCopiarPacote.setItems(pacotesParaCopiar);
            } else {
                GUIUtils.showMessage("Não encontrado nenhum pacote para essa OF e produto carregado.");
                tboxNumeroOfCopia.clear();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnProcurarPacotesOfOnAction");
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void requestMenuForcarColaboradorOnAction(ActionEvent event) {
        SdProgramacaoPacote001 programacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
        if (programacaoSelecionada.getProgramacaoApontada()) {
            GUIUtils.showMessage("Existem lançamentos de produção para essa operação. Não será possível forçar outro colaborador das operações com produção.");
            return;
        }
        
        try {
            ObservableList<SdColaborador> sendSelected = FXCollections.observableArrayList();
            if (colaboradorSemPolivalencia != null) {
                sendSelected.add(colaboradorSemPolivalencia);
            }
            sendSelected.clear();
            FilterColaboradorController ctrlFilter = new FilterColaboradorController(Modals.FXMLWindow.FilterColaborador,
                    sendSelected,
                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
            if (!ctrlFilter.selectedObjects.isEmpty()) {
                colaboradorSemPolivalencia = ctrlFilter.selectedObjects.get(0);
                SdColaborador colaboradorAlocado = new SdColaborador();
                colaboradorAlocado.copy(colaboradorSemPolivalencia);
                programacaoSelecionada.setColaborador(colaboradorAlocado.getCodigo());
                programacaoSelecionada.setColaboradorObject(colaboradorAlocado);
                operacoesParaProgramacao.stream()
                        .filter(programacaoAgrupada -> programacaoAgrupada.getAgrupador() == programacaoSelecionada.getOperacao()
                                && programacaoAgrupada.getSetorOp() == programacaoSelecionada.getSetorOp())
                        .forEach(programacaoAgrupada -> {
                            programacaoAgrupada.setColaborador(colaboradorAlocado.getCodigo());
                            programacaoAgrupada.setColaboradorObject(colaboradorAlocado);
                        });
            }
            carregaConflitos();
            tblProgramacao.refresh();
            
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::requestMenuForcarColaboradorOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxFiltroProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxFiltroProduto.setText(ctrolFilterProduto.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarProdutoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarPeriodoOnAction(ActionEvent event) {
        try {
            FilterPeriodoController ctrolFilter = new FilterPeriodoController(Modals.FXMLWindow.FilterPeriodo, tboxFiltroPeriodo.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxFiltroPeriodo.setText(ctrolFilter.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarPeriodoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
        try {
            FilterFamiliaController ctrolFilterColecao = new FilterFamiliaController(Modals.FXMLWindow.FilterFamilia, tboxFiltroFamilia.getText());
            if (!ctrolFilterColecao.returnValue.isEmpty()) {
                tboxFiltroFamilia.setText(ctrolFilterColecao.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarFamiliaOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnCarregarOfsOnAction(ActionEvent event) {
        
        String whereDataEnvio = "";
        if (tboxDtInicioEnvio.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioEnvio.getValue();
            LocalDate dtFim = tboxDtFimEnvio.getValue() == null ? tboxDtInicioEnvio.getValue() : tboxDtFimEnvio.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataEnvio += "and dt_env between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String whereDataRetorno = "";
        if (tboxDtInicioRetorno.getValue() != null || tboxDtFimRetorno.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioRetorno.getValue();
            LocalDate dtFim = tboxDtFimRetorno.getValue() == null ? tboxDtInicioRetorno.getValue() : tboxDtFimRetorno.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataRetorno += "and dt_ret between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String whereDataProgramacao = "";
        if (tboxDtInicioProgramacao.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioProgramacao.getValue();
            LocalDate dtFim = tboxDtFimProgramacao.getValue() == null ? tboxDtInicioProgramacao.getValue() : tboxDtFimProgramacao.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataProgramacao += "and dt_prog between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String filterStatusValue = ""
                .concat(toggleGreen.switchedOnProperty().get() ? "'G'," : "")
                .concat(toggleRed.switchedOnProperty().get() ? "'R'," : "")
                .concat(toggleYellow.switchedOnProperty().get() ? "'Y'," : "")
                .concat(toggleBlue.switchedOnProperty().get() ? "'B'," : "");
        filterStatusValue = filterStatusValue.length() > 0 ? "and status_of in (" + filterStatusValue.substring(0, filterStatusValue.length() - 1) + ")\n" : "";
        
        String wheresValues = "";
        wheresValues += filterStatusValue;
        wheresValues += tboxFiltroProduto.getLength() > 0 ? "and referencia in (" + tboxFiltroProduto.getText() + ")\n" : "";
        wheresValues += tboxFiltroFamilia.getLength() > 0 ? "and codfam in (" + tboxFiltroFamilia.getText() + ")\n" : "";
        wheresValues += tboxFiltroOf.getLength() > 0 ? "and op in (" + tboxFiltroOf.getText() + ")\n" : "";
        wheresValues += tboxFiltroPeriodo.getLength() > 0 ? "and periodo in (" + tboxFiltroPeriodo.getText() + ")\n" : "";
        wheresValues += tboxFiltroSetor.getLength() > 0 ? "and setor in (" + tboxFiltroSetor.getText() + ")\n" : "";
        wheresValues += whereDataEnvio.length() > 0 ? whereDataEnvio : "";
        wheresValues += whereDataRetorno.length() > 0 ? whereDataRetorno : "";
        wheresValues += whereDataProgramacao.length() > 0 ? whereDataProgramacao : "";
        
        try {
            ofsParaProgramar.set(DAOFactory.getVSdOfsParaProgramarDAO().getOfsParaProgramarFromFilter("PLANALTO", wheresValues));
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnCarregarOfsOnAction");
            GUIUtils.showException(ex);
        }
        
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroFamilia.clear();
        tboxFiltroOf.clear();
        tboxFiltroPeriodo.clear();
        tboxFiltroProduto.clear();
        tboxFiltroSetor.clear();
        
        tboxDtFimEnvio.setValue(null);
        tboxDtInicioEnvio.setValue(null);
        tboxDtFimRetorno.setValue(null);
        tboxDtInicioRetorno.setValue(null);
        tboxDtFimProgramacao.setValue(null);
        tboxDtInicioProgramacao.setValue(null);
        
        toggleRed.switchedOnProperty().set(true);
        toggleYellow.switchedOnProperty().set(true);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(true);
    }
    
    @FXML
    private void btnImprimirProgramacaoOnAction(ActionEvent event) {
        try {
            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoProducaoOf.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            
            parameters.put("pacote", pacoteSelecionado.get().getCodigo());
            parameters.put("setor", programacaoProducaoOf.get().getSetor());
            
            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);
            
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    
    @FXML
    private void btnExcluirProgramacaoOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir a programação?")) {
            if (pacoteSelecionado != null) {
                try {
                    DAOFactory.getSdPacoteLancamento001DAO().deleteByPacoteProg(pacoteSelecionado.get());
                    DAOFactory.getSdPacote001DAO().delete(pacoteSelecionado.get());
                    DAOFactory.getSdProgramacaoPacote001DAO().deleteByPacote(pacoteSelecionado.get());
                    if (listaDePacotes.size() == 1) {
                        DAOFactory.getSdProgramacaoOf001DAO().deleteBySetor(programacaoProducaoOf.get());
                        pacoteSelecionado.set(null);
                        setoresOperacaoRoteiroProduto.clear();
                        operacoesSetorSelecionadoParaProgramacao.clear();
                        setorOperacaoSelecionado = null;
                        celulaSelecionada.clear();
                        celulaSetorOperacaoSelecionada = null;
                        colaboradorSemPolivalencia = null;
                        colaboradoresParaProgramacao.clear();
                        ocupacaoColaborador.clear();
                        emProgramacao.set(false);
                        emEdicao.set(false);
                        
                        tblOfParaProgramacao.getItems().remove(ofParaProgramarSelecionada);
                        tblOfParaProgramacao.refresh();
                        tabsWindow.getSelectionModel().select(0);
                        tabListagem.setDisable(false);
                    } else {
                        listaDePacotes.remove(pacoteSelecionado.get());
                        pacoteSelecionado.set(null);
                        setoresOperacaoRoteiroProduto.clear();
                        operacoesSetorSelecionadoParaProgramacao.clear();
                        setorOperacaoSelecionado = null;
                        celulaSelecionada.clear();
                        celulaSetorOperacaoSelecionada = null;
                        colaboradorSemPolivalencia = null;
                        colaboradoresParaProgramacao.clear();
                        ocupacaoColaborador.clear();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    Logger.getLogger(SceneProgramaProducaoOfController.class.getName()).log(Level.SEVERE, null, ex);
                    LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnExcluirProgramacaoOnAction");
                    GUIUtils.showException(ex);
                }
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Class TABLECELL EDICAO COLUNA">
    private class EditingCellClnString extends TableCell<SdProgramacaoPacote001, String> {
        
        private TextField textField;
        
        public EditingCellClnString() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
    
    private class EditingCellClnColaborador extends TableCell<SdProgramacaoPacote001, SdColaborador> {
        
        private TextField textField;
        
        public EditingCellClnColaborador() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(getString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(SdColaborador item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setEditable(false);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.F4) {
                        try {
                            ObservableList<SdColaborador> sendSelected = FXCollections.observableArrayList();
                            if (getItem() != null) {
                                sendSelected.add(getItem());
                            }
                            FilterColaboradorController ctrlFilter = new FilterColaboradorController(
                                    Modals.FXMLWindow.FilterColaborador, sendSelected,
                                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
                            if (!ctrlFilter.selectedObjects.isEmpty()) {
                                setItem(ctrlFilter.selectedObjects.get(0));
                                
                                commitEdit(getItem());
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null,
                                    ex);
                            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "ProgramacaoOF:EditingCellClnColaborador");
                            GUIUtils.showException(ex);
                        }
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private String getString() {
            return getItem() == null ? "" : "[" + getItem().getCodigo() + "] " + getItem().getNome();
        }
    }
    
    private class EditingCellClnInteger extends TableCell<SdProgramacaoPacote001, Integer> {
        
        private TextField textField;
        
        public EditingCellClnInteger() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(StringUtils.toIntegerFormat(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(StringUtils.toIntegerFormat(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(StringUtils.toIntegerFormat(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(StringUtils.toIntegerFormat(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private Integer getString() {
            return getItem() == null ? 0 : getItem();
        }
    }
    
    private class EditingCellClnPercent extends TableCell<SdProgramacaoPacote001, Double> {
        
        private TextField textField;
        
        public EditingCellClnPercent() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(StringUtils.toPercentualFormat(getItem(), 2));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(StringUtils.toPercentualFormat(getString(), 2));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(StringUtils.toPercentualFormat(getString(), 2));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(StringUtils.toPercentualFormat(getString(), 2));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private Double getString() {
            return getItem() == null ? 0.0 : getItem();
        }
    }
    
    private class EditingCellClnDateTime extends TableCell<SdProgramacaoPacote001, LocalDateTime> {
        
        private TextField textField;
        
        public EditingCellClnDateTime() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(getString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(LocalDateTime item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            MaskTextField.dateTimeField(textField);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(setDateTime(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private LocalDateTime setDateTime(String dateTime) {
            return LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        }
        
        private String getString() {
            return getItem() == null ? "" : getItem().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        }
    }
//</editor-fold>
}
