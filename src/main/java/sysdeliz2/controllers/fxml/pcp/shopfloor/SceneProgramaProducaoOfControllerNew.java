/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.fxml.cadastros.SceneCadastroColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterFamiliaController;
import sysdeliz2.controllers.fxml.procura.FilterPeriodoController;
import sysdeliz2.controllers.fxml.procura.FilterProdutoController;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Of1001;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.view.VSdColabCelProg;
import sysdeliz2.models.view.VSdOfsParaProgramar;
import sysdeliz2.models.view.VSdProgramacaoPendente;
import sysdeliz2.utils.*;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.enums.StatusProgramacao;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.EnumsController;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProgramaProducaoOfControllerNew implements Initializable {
    
    private Integer positionInitial = 0;
    private Integer positionEnd = 0;
    private Integer ordemDestino = 0;
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs persistências">
    GenericDao<SdSetorOp001> daoSetorOp = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdSetorOp001.class);
    GenericDao<SdOperacaoOrganize001> daoOperacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdOperacaoOrganize001.class);
    GenericDao<SdCelula> daoCelula = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCelula.class);
    GenericDao<SdEquipamentosOrganize001> daoEquipamento = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdEquipamentosOrganize001.class);
    GenericDao<VSdColabCelProg> daoColabsCelula = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdColabCelProg.class);
    GenericDao<SdProgramacaoPacote002> daoProgramacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdProgramacaoPacote002.class);
    GenericDao<SdPacote001> daoPacote = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdPacote001.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Listagem das OFs para programar">
    private final ListProperty<VSdOfsParaProgramar> ofsParaProgramar = new SimpleListProperty<VSdOfsParaProgramar>();
    private final ObjectProperty<VSdOfsParaProgramar> ofParaProgramarSelecionada = new SimpleObjectProperty<VSdOfsParaProgramar>();
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Modos de programação">
    private final BooleanProperty emVisualizacao = new SimpleBooleanProperty(false);
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty emProgramacao = new SimpleBooleanProperty(false);
    private final BooleanProperty btnIniciarInativo = new SimpleBooleanProperty(false);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: OF para programar">
    private final ObjectProperty<SdProgramacaoOf001> programacaoProducaoOf = new SimpleObjectProperty<>();
    private Of1001 ofParaProgramacao = null;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Pacotes para programação">
    private final IntegerProperty totalPacotesOf = new SimpleIntegerProperty();
    private final IntegerProperty totalPacotesLancamentoOf = new SimpleIntegerProperty();
    private final ListProperty<SdPacote001> listaDePacotes = new SimpleListProperty<SdPacote001>(FXCollections.observableArrayList());
    private final ListProperty<SdPacote001> listaDePacotesProgramados = new SimpleListProperty<SdPacote001>(FXCollections.observableArrayList());
    private final ListProperty<SdPacote001> listaDePacotesLancamento = new SimpleListProperty<SdPacote001>(FXCollections.observableArrayList());
    private final ObjectProperty<SdPacote001> pacoteSelecionado = new SimpleObjectProperty<SdPacote001>();
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Setores para programação">
    private final ListProperty<SdSetorOp001> setoresOperacao = new SimpleListProperty<>();
    private List<SdProgramacaoPacote002> operacoesSetorSelecionado = new ArrayList<>();
    private List<Integer> setoresComLancamento = new ArrayList<>();
    private SdSetorOp001 setorOperacaoSelecionado = null;
    private final SdCelula celulaSetorSelecionada = new SdCelula();
    private SdCelula celulaSetorOperacaoSelecionada = null;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Operações para programação">
    private List<SdOperRoteiroOrganize001> roteiroOperacoesProduto = null;
    private List<SdProgramacaoPacote002> operacoesParaProgramacao = new ArrayList<>();
    private final ListProperty<SdProgramacaoPacote002> operacoesSetorSelecionadoParaProgramacao = new SimpleListProperty<>(FXCollections.observableArrayList()); //lista para exibição em tableview
    private final BooleanProperty programacaoComLancametos = new SimpleBooleanProperty(false);
    private ArrayList<SdProgramacaoPacote002> operacoesSelecionadas = new ArrayList<>();
    private final ListProperty<SdProgramacaoPacote002> operacoesParaExcluir = new SimpleListProperty<>(FXCollections.observableArrayList());
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Células e Colaboradores para programação">
    private final SdCelula celulaSelecionada = new SdCelula();
    private SdColaborador colaboradorSemPolivalencia = null;
    private final ListProperty<VSdColabCelProg> colaboradoresParaProgramacao = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdColabCelProg> colaboradoresCelulaSelecionada = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdProgramacaoPendente> ocupacaoColaborador = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdProgramacaoPacote002> operacoesAgrupadas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ArrayList<VSdProgramacaoPendente> operacoesPendentesSelecionadas = new ArrayList<>();
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Variáveis para controles">
    public static final DataFormat SERIALIZED_MIME_TYPE = Globals.SERIALIZED_MIME_TYPE;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML components">
    @FXML
    private TabPane tabsWindow;
    @FXML
    private Tab tabListagem;
    // <editor-fold defaultstate="collapsed" desc="Filtro OFs para programar">
    @FXML
    private DatePicker tboxDtInicioEnvio;
    @FXML
    private DatePicker tboxDtFimEnvio;
    @FXML
    private DatePicker tboxDtInicioRetorno;
    @FXML
    private DatePicker tboxDtFimRetorno;
    @FXML
    private DatePicker tboxDtInicioProgramacao;
    @FXML
    private DatePicker tboxDtFimProgramacao;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroPeriodo;
    @FXML
    private Button btnProcurarPeriodo;
    @FXML
    private TextField tboxFiltroFamilia;
    @FXML
    private TextField tboxFiltroFaccao;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroSetor;
    @FXML
    private HBox hboxFiltroOfs;
    FormToggleField toggleSomenteNaoProgramadas = new FormToggleField("Som. Não Prog.", false, false);
    @FXML
    private Label lbFiltroOfAtrasada;
    private ToggleSwitch toggleRed = new ToggleSwitch(16, 40, "#f5c6cb");
    @FXML
    private Label lbFiltroOfAAtrasar;
    private ToggleSwitch toggleYellow = new ToggleSwitch(16, 40, "#ffeeba");
    @FXML
    private Label lbFiltroOfEmDia;
    private ToggleSwitch toggleBlue = new ToggleSwitch(16, 40, "#b8daff");
    @FXML
    private Label lbFiltroOfFinalizada;
    private ToggleSwitch toggleGreen = new ToggleSwitch(16, 40, "#c3e6cb");
    @FXML
    private Button btnCarregarOfs;
    @FXML
    private Button btnLimparFiltros;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tabela de OFs para programar">
    @FXML
    private TableView<VSdOfsParaProgramar> tblOfParaProgramacao;
    @FXML
    private TableColumn<VSdOfsParaProgramar, String> clnStatusMaterial;
    @FXML
    private TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar> clnAcoes;
    @FXML
    private TableColumn<VSdOfsParaProgramar, LocalDate> clnDtEnvio;
    @FXML
    private TableColumn<VSdOfsParaProgramar, LocalDate> clnDtRetorno;
    @FXML
    private TableColumn<VSdOfsParaProgramar, LocalDateTime> clnDtProgramacao;
    @FXML
    private Label lbRegistersCount;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Infos OFs para programar">
    @FXML
    private TextField tboxNumeroOf;
    @FXML
    private TextField tboxQtdeOf;
    @FXML
    private TextField tboxFamiliaOf;
    @FXML
    private TextField tboxProdutoOf;
    @FXML
    private TextField tboxDescricaoProduto;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Pacote Programacao">
    @FXML
    private TextField tboxInfosPacote;
    @FXML
    private RadioButton rbtnReferencia;
    @FXML
    private ToggleGroup tipoPacote;
    @FXML
    private RadioButton rbtnCor;
    @FXML
    private RadioButton rbtnTamanho;
    @FXML
    private TextField tboxQtdePacotes;
    @FXML
    private ComboBox<SdPacote001> cboxPacote;
    @FXML
    private TextField tboxPecasPacote;
    @FXML
    private VBox paneConfigPacoteProgramacao;
    @FXML
    private VBox panePacotesProgramados;
    @FXML
    private ComboBox<SdPacote001> cboxPacoteProgramado;
    @FXML
    private TextField tboxOrdemProdPacoteProgramado;
    @FXML
    private Button btnLimparPacoteProgramado;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Pacote Lançamento">
    @FXML
    private TableView<SdPacote001> tblPacotesLancamento;
    @FXML
    private RadioButton rbtnReferenciaLancamento;
    @FXML
    private ToggleGroup pacoteLancamento;
    @FXML
    private RadioButton rbtnCorLancamento;
    @FXML
    private RadioButton rbtnTamanhoLancamento;
    @FXML
    private TextField tboxQtdePacotesLancamento;
    @FXML
    private VBox paneConfigPacoteLancamento;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tabela de operações para programar">
    @FXML
    private ComboBox<SdSetorOp001> cboxSetoresOperacao;
    @FXML
    private TextField tboxCelulaSetor;
    @FXML
    private Label lbDescricaoCelulaSetor;
    @FXML
    private Button btnProcurarCelulaSetor;
    @FXML
    private Label lbTempoSetor;
    @FXML
    private Label lbOperacoesSetor;
    @FXML
    private TableView<SdProgramacaoPacote002> tblProgramacao;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOrdem;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOperacoes;
    @FXML
    private TableColumn<SdProgramacaoPacote002, BigDecimal> clnTempoOperacao;
    @FXML
    private TableColumn<SdProgramacaoPacote002, Integer> clnQtde;
    @FXML
    private TableColumn<SdProgramacaoPacote002, BigDecimal> clnTempoTotal;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Botões Prorgamação">
    @FXML
    private Button btnExcluirProgramacao;
    @FXML
    private Button btnIniciarProgramacao;
    @FXML
    private Button btnConfirmarProgramacao;
    @FXML
    private Button btnCancelarProgramacao;
    @FXML
    private Button btnImprimirProgramacao;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Informações do Colaborador">
    @FXML
    private TitledPane tpaneColaborador;
    @FXML
    private TextField tboxCelula;
    @FXML
    private Label lbDescricaoCelula;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private TableView<VSdColabCelProg> tblColaboradoresOperacao;
    @FXML
    private TableColumn<VSdColabCelProg, BigDecimal> clnOperMesColaborador;
    @FXML
    private TableColumn<VSdColabCelProg, BigDecimal> clnEficMesColaborador;
    @FXML
    private TableColumn<VSdColabCelProg, BigDecimal> clnProdMesColaborador;
    @FXML
    private Label lbAprovTempo30;
    @FXML
    private Label lbAprovTempo1;
    // <editor-fold defaultstate="collapsed" desc="Tabela de Operações pendentes Colaborador">
    @FXML
    private TableView<VSdProgramacaoPendente> tblOperacoesColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnSeqOperacaoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnPacoteColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnOperacaoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnEquipamentoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnTempoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnQtdeColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnAgrupColaborador;
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tabela de operações agrupadas">
    @FXML
    private TitledPane tpaneOperacoesAgrupadas;
    @FXML
    private TableView<SdProgramacaoPacote002> tblOperacoesAgrupadas;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOrdemAgrupada;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOperacaoAgrupada;
    @FXML
    private TableColumn<SdProgramacaoPacote002, BigDecimal> clnTempoOpAgrupado;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdEquipamentosOrganize001> clnEquipamentoAgrupado;
    // </editor-fold>
    //</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ofsParaProgramar.set(DAOFactory.getVSdOfsParaProgramarDAO().getOfsParaProgramar("PLANALTO"));
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::initialize");
            GUIUtils.showException(ex);
            
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro no initialize, carga das ofs para programar ou programadas "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
        
        // <editor-fold defaultstate="collapsed" desc="Inicialização dos componentes FXML">
        initializeComponentesFxml();
        celulaSelecionada.clear();
        celulaSetorSelecionada.clear();
        celulaSetorOperacaoSelecionada = null;
        // </editor-fold>
    }
    
    /**
     * Inicialização dos componentes de tela FXML
     */
    private void initializeComponentesFxml() {
        // <editor-fold defaultstate="collapsed" desc="Pane PACOTES PROGRAMADOS">
        panePacotesProgramados.disableProperty().bind(listaDePacotesProgramados.emptyProperty().or(emVisualizacao).or(emEdicao).or(emProgramacao));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pane CONFIG PACOTE">
        paneConfigPacoteProgramacao.disableProperty().bind(totalPacotesOf.lessThan(listaDePacotes.getSize()).or(emVisualizacao).or(emEdicao).or(emProgramacao));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pane CONFIG PACOTE LANCAMENTO">
        paneConfigPacoteLancamento.disableProperty().bind(totalPacotesLancamentoOf.lessThan(listaDePacotesLancamento.getSize()).or(emVisualizacao).or(pacoteSelecionado.isNull()).or(emProgramacao));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HBox FILTRO OFS">
        hboxFiltroOfs.getChildren().add(toggleSomenteNaoProgramadas);
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ToggleSwitch FILTRO">
        lbFiltroOfAtrasada.setGraphic(toggleRed);
        lbFiltroOfAAtrasar.setGraphic(toggleYellow);
        lbFiltroOfFinalizada.setGraphic(toggleBlue);
        lbFiltroOfEmDia.setGraphic(toggleGreen);
        toggleRed.switchedOnProperty().set(true);
        toggleYellow.switchedOnProperty().set(true);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(true);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TextField QTDE_PACOTES">
        tboxQtdePacotes.textProperty().bind(totalPacotesOf.asString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField QTDE_PACOTES LANCAMENTO">
        tboxQtdePacotesLancamento.textProperty().bind(totalPacotesLancamentoOf.asString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField CELULA">
        tboxCelula.disableProperty().bind(emVisualizacao);
        btnProcurarCelula.disableProperty().bind(emVisualizacao);
        tboxCelula.textProperty().bind(celulaSelecionada.codigoProperty().asString());
        lbDescricaoCelula.textProperty().bind(celulaSelecionada.descricaoProperty());
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField CELULA SETOR">
        tboxCelulaSetor.disableProperty().bind(emVisualizacao);
        btnProcurarCelulaSetor.disableProperty().bind(emVisualizacao);
        tboxCelulaSetor.textProperty().bind(celulaSetorSelecionada.codigoProperty().asString());
        lbDescricaoCelulaSetor.textProperty().bind(celulaSetorSelecionada.descricaoProperty());
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField FILTRO">
        TextFieldUtils.upperCase(tboxFiltroFamilia);
        TextFieldUtils.upperCase(tboxFiltroOf);
        TextFieldUtils.upperCase(tboxFiltroPeriodo);
        TextFieldUtils.upperCase(tboxFiltroProduto);
        TextFieldUtils.upperCase(tboxFiltroSetor);
        TextFieldUtils.inSql(tboxFiltroFamilia);
        TextFieldUtils.inSql(tboxFiltroOf);
        TextFieldUtils.inSql(tboxFiltroPeriodo);
        TextFieldUtils.inSql(tboxFiltroProduto);
        TextFieldUtils.inSql(tboxFiltroSetor);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ComboBox PACOTES PROGRAMADOS">
        cboxPacoteProgramado.itemsProperty().bind(listaDePacotesProgramados);
        cboxPacoteProgramado.setCellFactory(new Callback<ListView<SdPacote001>, ListCell<SdPacote001>>() {
            @Override
            public ListCell<SdPacote001> call(ListView<SdPacote001> param) {
                final ListCell<SdPacote001> cell = new ListCell<SdPacote001>() {
                    @Override
                    public void updateItem(SdPacote001 item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getDescricao() + " - " + item.getSdProgramacaoOf().getOrdemProd());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboxPacoteProgramado.setConverter(new StringConverter<SdPacote001>() {
            @Override
            public String toString(SdPacote001 object) {
                return object.getDescricao() + " - " + object.getSdProgramacaoOf().getOrdemProd();
            }
            
            @Override
            public SdPacote001 fromString(String string) {
                return null;
            }
        });
        cboxPacoteProgramado.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                switch(newValue.getTipo()) {
                    case "C":
                        rbtnCor.setSelected(true);
                        break;
                    case "T":
                        rbtnTamanho.setSelected(true);
                        break;
                    default:
                        rbtnReferencia.setSelected(true);
                        break;
                }
                cboxPacote.getSelectionModel().select(0);
                switch(newValue.getTipoLancamento()) {
                    case "C":
                        rbtnCorLancamento.setSelected(true);
                        break;
                    case "T":
                        rbtnTamanhoLancamento.setSelected(true);
                        break;
                    default:
                        rbtnReferenciaLancamento.setSelected(true);
                        break;
                }
                
                tboxOrdemProdPacoteProgramado.clear();
                tboxOrdemProdPacoteProgramado.setText(newValue.getSdProgramacaoOf().getOrdemProd());
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ComboBox PACOTE">
        cboxPacote.itemsProperty().bind(listaDePacotes);
        cboxPacote.setCellFactory(new Callback<ListView<SdPacote001>, ListCell<SdPacote001>>() {
            @Override
            public ListCell<SdPacote001> call(ListView<SdPacote001> param) {
                final ListCell<SdPacote001> cell = new ListCell<SdPacote001>() {
                    @Override
                    public void updateItem(SdPacote001 item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getDescricao());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboxPacote.setConverter(new StringConverter<SdPacote001>() {
            @Override
            public String toString(SdPacote001 object) {
                return object.getDescricao();
            }
            
            @Override
            public SdPacote001 fromString(String string) {
                return null;
            }
        });
        cboxPacote.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tboxPecasPacote.setText(newValue.qtdeProperty().getValue().toString());
                tboxInfosPacote.setText(newValue.getDescricao());
                pacoteSelecionado.set(newValue);
                if (emVisualizacao.or(emEdicao.and(pacoteSelecionado.get().pacoteProgramadoProperty())).get()) {
                    btnIniciarInativo.set(true);
                    carregaSetoresParaLeitura();
                    return;
                }
                rbtnReferenciaLancamento.setSelected(true);
                
                btnIniciarInativo.set(false);
                
                roteiroOperacoesProduto = null;
                operacoesParaProgramacao.clear();
                setoresOperacao.clear();
                operacoesSetorSelecionadoParaProgramacao.clear();
                setorOperacaoSelecionado = null;
                celulaSelecionada.clear();
                celulaSetorSelecionada.clear();
                celulaSetorOperacaoSelecionada = null;
                colaboradorSemPolivalencia = null;
                colaboradoresParaProgramacao.clear();
                ocupacaoColaborador.clear();
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ComboBox SETOR OPERACAO">
        cboxSetoresOperacao.itemsProperty().bind(setoresOperacao);
        cboxSetoresOperacao.setCellFactory(new Callback<ListView<SdSetorOp001>, ListCell<SdSetorOp001>>() {
            @Override
            public ListCell<SdSetorOp001> call(ListView<SdSetorOp001> param) {
                final ListCell<SdSetorOp001> cell = new ListCell<SdSetorOp001>() {
                    @Override
                    public void updateItem(SdSetorOp001 item, boolean empty) {
                        super.updateItem(item, empty);
                        getStyleClass().remove("success");
                        if (item != null) {
                            setText(item.getDescricao() + " [" + operacoesParaProgramacao.stream().filter(operacao -> operacao.getId().getSetorOp().getCodigo() == item.getCodigo()).count() + "]");
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboxSetoresOperacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                this.setorOperacaoSelecionado = newValue;
                this.selectSetorOperacao(newValue);
                lbOperacoesSetor.setText(StringUtils.toIntegerFormat(operacoesSetorSelecionadoParaProgramacao.stream().filter(operacao -> operacao.getId().getSetorOp().getCodigo() == newValue.getCodigo()).count()));
                lbTempoSetor.setText(StringUtils.toDecimalFormat(operacoesSetorSelecionadoParaProgramacao.stream().filter(operacao -> operacao.getId().getSetorOp().getCodigo() == newValue.getCodigo()).mapToDouble(operacao -> operacao.getTempoTotal().doubleValue()).sum() / 528, 2));
            }
        });
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Label REGISTERS COUNT">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(ofsParaProgramar.sizeProperty()).concat(" registros"));
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="RadioButtons TIPO_PACOTE">
        rbtnReferencia.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteReferencia();
                programacaoProducaoOf.get().setTipoPacote("R");
                rbtnReferenciaLancamento.setDisable(false);
                rbtnCorLancamento.setDisable(false);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        rbtnCor.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteCor();
                programacaoProducaoOf.get().setTipoPacote("C");
                rbtnReferenciaLancamento.setDisable(true);
                rbtnCorLancamento.setDisable(false);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        rbtnTamanho.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteTamanho();
                programacaoProducaoOf.get().setTipoPacote("T");
                rbtnReferenciaLancamento.setDisable(true);
                rbtnCorLancamento.setDisable(true);
                rbtnTamanhoLancamento.setDisable(false);
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="RadioButtons TIPO PACOTE LANCAMENTO">
        rbtnReferenciaLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoReferencia();
                pacoteSelecionado.get().setTipoLancamento("R");
            }
        });
        rbtnCorLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoCor();
                pacoteSelecionado.get().setTipoLancamento("C");
            }
        });
        rbtnTamanhoLancamento.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                criarPacoteLancamentoTamanho();
                pacoteSelecionado.get().setTipoLancamento("T");
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button INICIAR_PROGRAMACAO">
        btnIniciarProgramacao.disableProperty().bind(emProgramacao
                .or(programacaoProducaoOf.isNull())
                .or(listaDePacotes.emptyProperty())
                .or(btnIniciarInativo)
                .or(emVisualizacao));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button CONFIRMA_PROGRAMACAO">
        btnConfirmarProgramacao.disableProperty().bind(emProgramacao.not().and(emEdicao.not()));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button PROCURAR CÉLULA">
        btnProcurarCelula.disableProperty().bind(emVisualizacao);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button EXCLUIR PROGRAMACAO">
        btnExcluirProgramacao.disableProperty().bind(emEdicao.not());
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Button LIMPAR PACOTE PROGRAMADO">
        btnLimparPacoteProgramado.disableProperty().bind(listaDePacotesProgramados.emptyProperty().or(emVisualizacao).or(emEdicao).or(emProgramacao));
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView OFS PARA PROGRAMACAO">
        tblOfParaProgramacao.itemsProperty().bind(ofsParaProgramar);
        tblOfParaProgramacao.setRowFactory(tv -> new TableRow<VSdOfsParaProgramar>() {
            @Override
            protected void updateItem(VSdOfsParaProgramar item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (item.getStatusOf().equals("R")) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else if (item.getStatusOf().equals("Y")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatusOf().equals("G")) {
                    clearStyle();
                    getStyleClass().add("table-row-success");
                } else if (item.getStatusOf().equals("B")) {
                    clearStyle();
                    getStyleClass().add("table-row-info");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-success");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-info");
            }
            
        });
        tblOfParaProgramacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                ofParaProgramarSelecionada.set(newValue);
            }
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PROGRAMACAO">
        tblProgramacao.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblProgramacao.itemsProperty().bind(operacoesSetorSelecionadoParaProgramacao);
        tblProgramacao.editableProperty().bind(emVisualizacao.not().or(emProgramacao).or(emEdicao));
        tblProgramacao.setRowFactory(tv -> {
            TableRow<SdProgramacaoPacote002> row = new TableRow<SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    if (item != null) {
                        if (item.getStatusProgramacao() == StatusProgramacao.LANCADO) {
                            getStyleClass().add("table-row-danger");
                        } else if (item.getStatusProgramacao() == StatusProgramacao.QUEBRADA) {
                            getStyleClass().add("table-row-warning");
                        } else if (item.getStatusProgramacao() == StatusProgramacao.AGRUPADA) {
                            getStyleClass().add("table-row-secundary");
                        } else if (item.getStatusProgramacao() == StatusProgramacao.PROGRAMADO) {
                            getStyleClass().add("table-row-primary");
                        } else if (item.getStatusProgramacao() == StatusProgramacao.FINALIZADO) {
                            getStyleClass().add("table-row-success");
                        }
                    }
                }
                
                private void clearStyle() {
                    getStyleClass().remove("table-row-primary");
                    getStyleClass().remove("table-row-secundary");
                    getStyleClass().remove("table-row-success");
                    getStyleClass().remove("table-row-warning");
                    getStyleClass().remove("table-row-danger");
                }
            };
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();
                    
                    operacoesSelecionadas.clear();//important...
                    ObservableList<SdProgramacaoPacote002> items = tblProgramacao.getSelectionModel().getSelectedItems();
                    for (SdProgramacaoPacote002 iI : items) {
                        if (iI.getAgrupador() != 0) {
                            GUIUtils.showMessage("Você não pode reordenar uma operação agrupada, para isso utilize a primeira operação do agrupamento.", Alert.AlertType.WARNING);
                            continue;
                        } else if (iI.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || iI.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
                            GUIUtils.showMessage("Esta operação já está em produção e não pode ser alterada.", Alert.AlertType.WARNING);
                            continue;
                        }
                        
                        operacoesSelecionadas.add(iI);
                        for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesParaProgramacao) {
                            if (sdProgramacaoPacote002.getAgrupador() == iI.getId().getOperacao().getCodorg() &&
                                    sdProgramacaoPacote002.getId().getSetorOp().getCodigo() == iI.getId().getSetorOp().getCodigo()) {
                                operacoesSelecionadas.add(sdProgramacaoPacote002);
                            }
                        }
                    }
                    positionInitial = operacoesSelecionadas.stream().mapToInt(value -> value.getId().getOrdem()).min().getAsInt();
                    
                    Comparator<SdProgramacaoPacote002> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
                    comparator = comparator.thenComparing(Comparator.comparing(programacao -> programacao.getId().getQuebra()));
                    operacoesSelecionadas.sort(comparator);
                    
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer) db.getContent(SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    int dropIndex;
                    SdProgramacaoPacote002 dI = null;
                    
                    if (row.isEmpty()) {
                        dropIndex = tblProgramacao.getItems().size();
                    } else {
                        dropIndex = row.getIndex();
                        dI = tblProgramacao.getItems().get(dropIndex);
                    }
                    int delta = 0;
                    if (dI != null)
                        while (operacoesSelecionadas.contains(dI)) {
                            delta = 1;
                            --dropIndex;
                            if (dropIndex < 0) {
                                dI = null;
                                dropIndex = 0;
                                break;
                            }
                            dI = tblProgramacao.getItems().get(dropIndex);
                        }
                    if (dI.getAgrupador() != 0) {
                        GUIUtils.showMessage("Você não pode reordenar uma operação para uma posição que tenha agrupamento.", Alert.AlertType.WARNING);
                        return;
                    }
                    positionEnd = dI.getId().getOrdem();
                    ordemDestino = dI.getId().getOrdem();
                    if (positionEnd < positionInitial) {
                        positionEnd = positionInitial;
                        positionInitial = dI.getId().getOrdem();
                    }
                    for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesSetorSelecionadoParaProgramacao) {
                        if (sdProgramacaoPacote002.getId().getOrdem() >= positionInitial && sdProgramacaoPacote002.getId().getOrdem() <= positionEnd) {
                            if (sdProgramacaoPacote002.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || sdProgramacaoPacote002.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
                                GUIUtils.showMessage("Você está tentando reordernar operações porém existem operações lançadas entre as novas ordens, esse processo não pode ser realizado.");
                                event.setDropCompleted(true);
                                operacoesSelecionadas.clear();
                                event.consume();
                                return;
                            }
                        }
                    }
                    for (SdProgramacaoPacote002 sI : operacoesSelecionadas) {
                        tblProgramacao.getItems().remove(sI);
                    }
                    
                    if (dI != null)
                        dropIndex = tblProgramacao.getItems().indexOf(dI) + delta;
                    else if (dropIndex != 0)
                        dropIndex = tblProgramacao.getItems().size();
                    
                    tblProgramacao.getSelectionModel().clearSelection();
                    
                    for (SdProgramacaoPacote002 sI : operacoesSelecionadas) {
                        //draggedIndex = selections.get(i);
                        sI.getId().setOrdem(ordemDestino);
                        sI.setPosicaoOriginal(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(sI.getId().getOrdem()), 3,'0')+"0");
                        tblProgramacao.getItems().add(dropIndex, sI);
                        tblProgramacao.getSelectionModel().select(dropIndex);
                        dropIndex++;
                    }
                    
                    event.setDropCompleted(true);
                    operacoesSelecionadas.clear();
                    event.consume();
                    this.updateOrdemOperacao();
                }
            });
            
            return row;
        });
        tblProgramacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.bindProgramacao(newValue);
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView COLABORADOR">
        tblColaboradoresOperacao.itemsProperty().bind(colaboradoresCelulaSelecionada);
        tblColaboradoresOperacao.setRowFactory(tv -> new TableRow<VSdColabCelProg>() {
            @Override
            protected void updateItem(VSdColabCelProg item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item != null && !empty) {
                    if (item.getStatusColaborador().equals(StatusProgramacao.PROGRAMADO)) {
                        getStyleClass().add("table-row-primary");
                    } else if (item.getStatusColaborador().equals(StatusProgramacao.POLIVALENTE)) {
                        getStyleClass().add("table-row-success");
                    }
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-primary");
                getStyleClass().remove("table-row-success");
            }
            
        });
        tblColaboradoresOperacao.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                unbindColaboradorSelecionado(oldValue);
                bindColaboradorSelecionado(newValue);
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
                LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::initializeComponentesFxml");
                GUIUtils.showException(ex);
                
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                                + "Erro no bind do colaborador selecionado da tabela de colaboradores da operação "
                                + "EXCEPTION: " + ex.getMessage()
                                + " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PROGRAMACAO COLABORADOR">
        tblOperacoesColaborador.itemsProperty().bind(ocupacaoColaborador);
        tblOperacoesColaborador.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.bindOperacaoColaborador(newValue);
        });
        tblOperacoesColaborador.setRowFactory(tv -> {
            TableRow<VSdProgramacaoPendente> row = new TableRow<VSdProgramacaoPendente>() {
            };
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();
                    
                    operacoesSelecionadas.clear();//important...
                    ObservableList<VSdProgramacaoPendente> items = tblOperacoesColaborador.getSelectionModel().getSelectedItems();
                    for (VSdProgramacaoPendente iI : items) {
                        operacoesPendentesSelecionadas.add(iI);
                    }
                    
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer) db.getContent(SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    
                    if (operacoesSelecionadas.size() > 0) {
                        // veio do operacoes para incluir
                        int dropIndex;
                        dropIndex = tblOperacoesColaborador.getItems().size();
                        
                        tblOperacoesColaborador.getSelectionModel().clearSelection();
                        this.requestMenuExcluirColaboradorOnAction(null);
                        for (SdProgramacaoPacote002 sI : operacoesSelecionadas) {
                            //draggedIndex = selections.get(i);
                            if (sI.getAgrupador() != 0)
                                continue;
                            tblColaboradoresOperacao.getSelectionModel().getSelectedItem()
                                    .setDiasProg(tblColaboradoresOperacao.getSelectionModel().getSelectedItem().getDiasProg()
                                            .add(BigDecimal.valueOf((sI.getQtde() * sI.getTempoOpAgrupada().doubleValue()) / 528.0)));
                            
                            sI.setColaboradorCelula(tblColaboradoresOperacao.getSelectionModel().getSelectedItem());
                            sI.setColaborador(tblColaboradoresOperacao.getSelectionModel().getSelectedItem().getColaborador());
                            if (sI.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA)) {
                                for (SdProgramacaoPacote002 operacoesAgrupada : sI.getOperacoesAgrupadas()) {
                                    operacoesAgrupada.setColaboradorCelula(tblColaboradoresOperacao.getSelectionModel().getSelectedItem());
                                    operacoesAgrupada.setColaborador(tblColaboradoresOperacao.getSelectionModel().getSelectedItem().getColaborador());
                                    operacoesAgrupada.setCelula(celulaSetorOperacaoSelecionada);
                                }
                            }
                            sI.setSeqColaborador(dropIndex + 1);
                            if (!sI.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA) &&
                                    !sI.getStatusProgramacao().equals(StatusProgramacao.QUEBRADA))
                                sI.setStatusProgramacao(StatusProgramacao.PROGRAMADO);
                            VSdProgramacaoPendente sNI = new VSdProgramacaoPendente(ofParaProgramacao.getCodigo(), sI.getColaborador().getCodigo(), sI);
                            tblOperacoesColaborador.getItems().add(dropIndex, sNI);
                            tblOperacoesColaborador.getSelectionModel().select(dropIndex);
                            dropIndex++;
                        }
                    } else if (operacoesPendentesSelecionadas.size() > 0) {
                        // veio do operacoes colaborador para reordenar
                        int dropIndex;
                        VSdProgramacaoPendente dI = null;
                        
                        if (row.isEmpty()) {
                            dropIndex = tblOperacoesColaborador.getItems().size();
                        } else {
                            dropIndex = row.getIndex();
                            dI = tblOperacoesColaborador.getItems().get(dropIndex);
                        }
                        int delta = 0;
                        if (dI != null)
                            while (operacoesPendentesSelecionadas.contains(dI)) {
                                delta = 1;
                                --dropIndex;
                                if (dropIndex < 0) {
                                    dI = null;
                                    dropIndex = 0;
                                    break;
                                }
                                dI = tblOperacoesColaborador.getItems().get(dropIndex);
                            }
                        
                        for (VSdProgramacaoPendente sI : operacoesPendentesSelecionadas) {
                            tblOperacoesColaborador.getItems().remove(sI);
                        }
                        
                        if (dI != null)
                            dropIndex = tblOperacoesColaborador.getItems().indexOf(dI) + delta;
                        else if (dropIndex != 0)
                            dropIndex = tblOperacoesColaborador.getItems().size();
                        
                        tblOperacoesColaborador.getSelectionModel().clearSelection();
                        
                        for (VSdProgramacaoPendente sI : operacoesPendentesSelecionadas) {
                            //draggedIndex = selections.get(i);
                            tblOperacoesColaborador.getItems().add(dropIndex, sI);
                            tblOperacoesColaborador.getSelectionModel().select(dropIndex);
                            dropIndex++;
                        }
                        this.updateOrdemOperacaoPendente();
                    } else {
                        return;
                    }
                    
                    event.setDropCompleted(true);
                    operacoesSelecionadas.clear();
                    operacoesPendentesSelecionadas.clear();
                    event.consume();
                }
            });
            
            return row;
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView OPERACOES AGRUPADAS">
        tblOperacoesAgrupadas.itemsProperty().bind(operacoesAgrupadas);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView PACOTES LANCAMENTO">
        tblPacotesLancamento.itemsProperty().bind(listaDePacotesLancamento);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE PROGRAMACAO">
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO_OPERACAO">
        Callback<TableColumn<SdProgramacaoPacote002, BigDecimal>, TableCell<SdProgramacaoPacote002, BigDecimal>> cellFactoryTempoOper = new Callback<TableColumn<SdProgramacaoPacote002, BigDecimal>, TableCell<SdProgramacaoPacote002, BigDecimal>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnBigDecimal();
            }
        };
        clnTempoOperacao.setCellFactory(cellFactoryTempoOper);
        clnTempoOperacao.setOnEditCommit((event) -> {
            this.updateTempoOperacao(event);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO_TOTAL">
        clnTempoTotal.setCellFactory(
                new Callback<TableColumn<SdProgramacaoPacote002, BigDecimal>, TableCell<SdProgramacaoPacote002, BigDecimal>>() {
                    @Override
                    public TableCell<SdProgramacaoPacote002, BigDecimal> call(
                            TableColumn<SdProgramacaoPacote002, BigDecimal> param) {
                        return new TableCell<SdProgramacaoPacote002, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column ORDEM">
        clnOrdem.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText("" + item.getOrdem());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AGRUPADOR">
//        Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>> cellFactoryGrupo = new Callback<TableColumn<SdProgramacaoPacote001, Integer>, TableCell<SdProgramacaoPacote001, Integer>>() {
//            public TableCell call(TableColumn p) {
//                return new EditingCellClnInteger();
//            }
//        };
//        clnGrupo.setCellFactory(cellFactoryGrupo);
//        clnGrupo.setOnEditCommit((event) -> {
//            this.updateAgrupadorOperacoes(event);
//        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column OPERACAO">
        clnOperacoes.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOperacao().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column QTDE">
        Callback<TableColumn<SdProgramacaoPacote002, Integer>, TableCell<SdProgramacaoPacote002, Integer>> cellFactoryQtde = new Callback<TableColumn<SdProgramacaoPacote002, Integer>, TableCell<SdProgramacaoPacote002, Integer>>() {
            public TableCell call(TableColumn p) {
                return new EditingCellClnInteger();
            }
        };
        clnQtde.setCellFactory(cellFactoryQtde);
        clnQtde.setOnEditCommit((event) -> {
            this.updateQtdePecasOperacao(event);
        });
        // </editor-fold>
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE OFS PARA PROGRAMAR">
        // <editor-fold defaultstate="collapsed" desc="Column STATUS MATERIAL">
        clnStatusMaterial.setCellFactory(
                new Callback<TableColumn<VSdOfsParaProgramar, String>, TableCell<VSdOfsParaProgramar, String>>() {
                    @Override
                    public TableCell<VSdOfsParaProgramar, String> call(
                            TableColumn<VSdOfsParaProgramar, String> param) {
                        return new TableCell<VSdOfsParaProgramar, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                clearStyle();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("REQUISITADO")) {
                                        getStyleClass().add("table-row-success");
                                    } else if (item.equals("REQ. PARCIAL")) {
                                        getStyleClass().add("table-row-primary");
                                    } else {
                                        getStyleClass().add("table-row-danger");
                                        
                                    }
                                } else {
                                    setText(null);
                                }
                            }
                            
                            private void clearStyle() {
                                getStyleClass().remove("table-row-primary");
                                getStyleClass().remove("table-row-success");
                                getStyleClass().remove("table-row-danger");
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Columns DATAS">
        clnDtEnvio.setCellFactory(param -> new TableCell<VSdOfsParaProgramar, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateFormat(item));
            }
        });
        clnDtRetorno.setCellFactory(param -> new TableCell<VSdOfsParaProgramar, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateFormat(item));
            }
        });
        clnDtProgramacao.setCellFactory(param -> new TableCell<VSdOfsParaProgramar, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateTimeFormat(item));
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AÇÕES">
        clnAcoes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdOfsParaProgramar, VSdOfsParaProgramar>, ObservableValue<VSdOfsParaProgramar>>() {
            @Override
            public ObservableValue<VSdOfsParaProgramar> call(TableColumn.CellDataFeatures<VSdOfsParaProgramar, VSdOfsParaProgramar> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnAcoes.setComparator(new Comparator<VSdOfsParaProgramar>() {
            @Override
            public int compare(VSdOfsParaProgramar p1, VSdOfsParaProgramar p2) {
                return p1.getOp().compareTo(p2.getOp());
            }
        });
        clnAcoes.setCellFactory(new Callback<TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar>, TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar>>() {
            @Override
            public TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar> call(TableColumn<VSdOfsParaProgramar, VSdOfsParaProgramar> btnCol) {
                return new TableCell<VSdOfsParaProgramar, VSdOfsParaProgramar>() {
                    final ImageView imgBtnProgramarOf = new ImageView(new Image(getClass().getResource("/images/icons/buttons/alarm (1).png").toExternalForm()));
                    final ImageView imgBtnMovimentarOf = new ImageView(new Image(getClass().getResource("/images/icons/buttons/send (1).png").toExternalForm()));
                    final ImageView imgBtnAbrirProgramacao = new ImageView(new Image(getClass().getResource("/images/icons/buttons/view (1).png").toExternalForm()));
                    final ImageView imgBtnEditarProgramacao = new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm()));
                    final Button btnProgramarOf = new Button();
                    final Button btnMovimentarOf = new Button();
                    final Button btnAbrirProgramacao = new Button();
                    final Button btnEditarProgramacao = new Button();
                    final HBox boxButtonsRow = new HBox(3);
                    
                    final Tooltip tipBtnProgramarOf = new Tooltip("Programar Produção OF");
                    final Tooltip tipBtnMovimentarOf = new Tooltip("Movimentar OF no TI");
                    final Tooltip tipBtnAbrirProgramacao = new Tooltip("Visualizar Programação OF");
                    final Tooltip tipBtnEditarProgramacao = new Tooltip("Editar Programação de Produção OF");
                    
                    {
                        btnProgramarOf.setGraphic(imgBtnProgramarOf);
                        btnProgramarOf.setTooltip(tipBtnProgramarOf);
                        btnProgramarOf.setText("Programar");
                        btnProgramarOf.getStyleClass().add("success");
                        btnProgramarOf.getStyleClass().add("xs");
                        btnProgramarOf.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnMovimentarOf.setGraphic(imgBtnMovimentarOf);
                        btnMovimentarOf.setTooltip(tipBtnMovimentarOf);
                        btnMovimentarOf.setText("Movimentar");
                        btnMovimentarOf.getStyleClass().add("warning");
                        btnMovimentarOf.getStyleClass().add("xs");
                        btnMovimentarOf.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnAbrirProgramacao.setGraphic(imgBtnAbrirProgramacao);
                        btnAbrirProgramacao.setTooltip(tipBtnAbrirProgramacao);
                        btnAbrirProgramacao.setText("Visualizar");
                        btnAbrirProgramacao.getStyleClass().add("primary");
                        btnAbrirProgramacao.getStyleClass().add("xs");
                        btnAbrirProgramacao.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                        
                        btnEditarProgramacao.setGraphic(imgBtnEditarProgramacao);
                        btnEditarProgramacao.setTooltip(tipBtnEditarProgramacao);
                        btnEditarProgramacao.setText("Editar");
                        btnEditarProgramacao.getStyleClass().add("secundary");
                        btnEditarProgramacao.getStyleClass().add("xs");
                        btnEditarProgramacao.contentDisplayProperty().set(ContentDisplay.GRAPHIC_ONLY);
                    }
                    
                    @Override
                    public void updateItem(VSdOfsParaProgramar seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            if (seletedRow.isProgramado()) {
                                btnProgramarOf.setDisable(true);
                                btnMovimentarOf.setDisable(true);
                                btnAbrirProgramacao.setDisable(false);
                                btnEditarProgramacao.setDisable(false);
                                
                                btnAbrirProgramacao.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnVisualizarProgramacao(seletedRow);
                                    }
                                });
                                btnEditarProgramacao.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnEditarProgramacao(seletedRow);
                                    }
                                });
                            } else {
                                btnProgramarOf.setDisable(false);
                                btnMovimentarOf.setDisable(false);
                                btnAbrirProgramacao.setDisable(true);
                                btnEditarProgramacao.setDisable(true);
                                
                                btnProgramarOf.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnProgramarOf(seletedRow);
                                    }
                                });
                                btnMovimentarOf.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        actionBtnMovimentarOf(seletedRow);
                                    }
                                });
                            }
                            
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnProgramarOf, btnMovimentarOf, btnAbrirProgramacao, btnEditarProgramacao);
                            setGraphic(boxButtonsRow);
                            
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
            
        });
        //</editor-fold>
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE PROGRAMACAO COLABORADOR">
        // <editor-fold defaultstate="collapsed" desc="Column SEQ COLABORADOR">
        clnSeqOperacaoColaborador.setCellFactory(
                new Callback<TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002>, TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>>() {
                    @Override
                    public TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002> call(
                            TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> param) {
                        return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                            @Override
                            protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toIntegerFormat(item.getSeqColaborador()));
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PACOTE COLABORADOR">
        clnPacoteColaborador.setCellFactory(
                new Callback<TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002>, TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>>() {
                    @Override
                    public TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002> call(
                            TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> btnCol) {
                        return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                            
                            @Override
                            public void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.getId().getPacote());
                                    setGraphic(null);
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column OPERACAO COLABORADOR">
        clnOperacaoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getId().getOperacao().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column EQUIPAMENTO COLABORADOR">
        clnEquipamentoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getMaquina().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO COLABORADOR">
        clnTempoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.getTempoTotal().doubleValue(), 4));
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column QTDE COLABORADOR">
        clnQtdeColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toIntegerFormat(item.getQtde()));
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AGRUP COLABORADOR">
        clnAgrupColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA) ? "SIM" : "NÃO");
                    }
                }
            };
        });
        // </editor-fold>
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE OPERACOES AGRUPADAS">
        // <editor-fold defaultstate="collapsed" desc="Column ORDEM OPERACAO">
        clnOrdemAgrupada.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOrdem() + "");
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PACOTE COLABORADOR">
        clnOperacaoAgrupada.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOperacao().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column EQUIPAMENTO AGRUPADO">
        clnEquipamentoAgrupado.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdEquipamentosOrganize001>() {
                @Override
                protected void updateItem(SdEquipamentosOrganize001 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO COLABORADOR">
        clnTempoOpAgrupado.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                    }
                }
            };
        });
        // </editor-fold>
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE COLABORADOR">
        clnOperMesColaborador.setCellFactory(param -> {
            return new TableCell<VSdColabCelProg, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toPercentualFormat(item.doubleValue(), 0));
                    }
                }
            };
        });
        clnEficMesColaborador.setCellFactory(param -> {
            return new TableCell<VSdColabCelProg, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toPercentualFormat(item.doubleValue(), 0));
                    }
                }
            };
        });
        clnProdMesColaborador.setCellFactory(param -> {
            return new TableCell<VSdColabCelProg, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toPercentualFormat(item.doubleValue(), 0));
                    }
                }
            };
        });
        // </editor-fold>
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos de COMPONENTS EVENTS">
    
    /**
     * Método para alteração da ordem de operação no objeto.
     */
    private void updateOrdemOperacao() {
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                        "Alterando ordem das operações do setor " + setorOperacaoSelecionado.toString() +
                        " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        Integer menorOrdemSetorOperacao = operacoesSetorSelecionado.stream().mapToInt(operacao -> operacao.getOrdemOriginal()).min().getAsInt();
        Comparator<SdProgramacaoPacote002> comparatorReorder = Comparator.comparing(programacao -> programacao.getPosicaoOriginal());
        comparatorReorder = comparatorReorder.thenComparing(Comparator.comparing(programacao -> programacao.getId().getOrdem()));
        comparatorReorder = comparatorReorder.thenComparing(Comparator.comparing(programacao -> ((SdProgramacaoPacote002) programacao).getId().getQuebra()).reversed());
        operacoesSetorSelecionado.sort(comparatorReorder);
        for (SdProgramacaoPacote002 operacaoProgramacao : operacoesSetorSelecionado) {
            operacaoProgramacao.setPosicaoOriginal(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(operacaoProgramacao.getId().getOrdem()), 3,'0')+"1");
            if (operacaoProgramacao.getId().getQuebra() > 1)
                operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao);
            else
                operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao++);
        }
        
        Comparator<SdProgramacaoPacote002> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
        comparator = comparator.thenComparing(Comparator.comparing(programacao -> programacao.getId().getQuebra()));
        operacoesParaProgramacao.sort(comparator);
        reordenarEncadeamentoObjetosProgramacao(null);
        selectSetorOperacao(setorOperacaoSelecionado);
//        for (SdProgramacaoPacote002 operacao : operacoesParaProgramacao) {
//            System.out.println(operacao != null ?
//                    "\nprogramacao=" + operacao.toString() +
//                            "\noperacaoAnterior=" + (operacao.getOperacaoAnterior() != null ? operacao.getOperacaoAnterior().toString() : "null") +
//                            "\noperacaoPosterior=" + (operacao.getOperacaoPosterior() != null ? operacao.getOperacaoPosterior().toString() : "null") +
//                            "\n------"
//                    : "null");
//        }
    }
    
    /**
     * Método para alteração da ordem de operação no objeto.
     */
    private void updateOrdemOperacaoPendente() {
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                        "Alterando ordem das operações pendentes do colaborador " + tblColaboradoresOperacao.getSelectionModel().getSelectedItem().getColaborador() +
                        " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        
        Integer seqOperacaoColaborador = 1;
        for (VSdProgramacaoPendente operacaoProgramada : ocupacaoColaborador) {
            operacaoProgramada.getProgramacao().setSeqColaborador(seqOperacaoColaborador++);
        }
        
        Comparator<VSdProgramacaoPendente> comparator = Comparator.comparing(programacao -> programacao.getProgramacao().getSeqColaborador());
        ocupacaoColaborador.sort(comparator);
    }
    
    /**
     * Método para OnCommit na coluna de tempo da opoeração
     *
     * @param event
     */
    private void updateTempoOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote002, BigDecimal> event) {
        SdProgramacaoPacote002 programacaoSelecionada = event.getRowValue();
        if (programacaoSelecionada.getStatusProgramacao() == StatusProgramacao.FINALIZADO) {
            GUIUtils.showMessage("Você não pode trocar o tempo de uma operação já finalizada.");
            tblProgramacao.refresh();
            return;
        }
        
        BigDecimal tempoAgrupadoOld = BigDecimal.ZERO;
        if (programacaoSelecionada.getAgrupador() == 0)
            tempoAgrupadoOld = programacaoSelecionada.getTempoOpAgrupada();
        else
            tempoAgrupadoOld = programacaoSelecionada.getOperacaoAgrupador().getTempoOpAgrupada();
        
        BigDecimal oldValue = programacaoSelecionada.getTempoOp();
        
        programacaoSelecionada.setTempoOp(event.getNewValue());
        if (programacaoSelecionada.getAgrupador() == 0)
            programacaoSelecionada.setTempoOpAgrupada((tempoAgrupadoOld.subtract(oldValue)).add(event.getNewValue()));
        else {
            programacaoSelecionada.getOperacaoAgrupador().setTempoOpAgrupada((tempoAgrupadoOld.subtract(oldValue)).add(event.getNewValue()));
            programacaoSelecionada.getOperacaoAgrupador().setTempoTotal(BigDecimal.valueOf(programacaoSelecionada.getOperacaoAgrupador().getQtde() * programacaoSelecionada.getOperacaoAgrupador().getTempoOpAgrupada().doubleValue()));
        }
        programacaoSelecionada.setTempoTotal(BigDecimal.valueOf(programacaoSelecionada.getQtde() * programacaoSelecionada.getTempoOpAgrupada().doubleValue()));
        
        try {
            DAOFactory.getSdProgramacaoPacote001DAO().update(programacaoSelecionada);
            tblProgramacao.refresh();
            
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Alterado o tempo da operação " + programacaoSelecionada.getId().getOperacao().getCodorg() + " setor " + programacaoSelecionada.getId().getSetorOp() + " na ordem " + programacaoSelecionada.getId().getOrdem() + " quebra " + programacaoSelecionada.getId().getQuebra()
                            + " de " + oldValue + " para " + event.getNewValue()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    /**
     * Método para OnCommit na coluna de qtde de peças da opoeração
     *
     * @param event
     */
    private void updateQtdePecasOperacao(TableColumn.CellEditEvent<SdProgramacaoPacote002, Integer> event) {
        SdProgramacaoPacote002 programacaoSelecionada = event.getRowValue();
        if (!(programacaoSelecionada.getStatusProgramacao() == StatusProgramacao.QUEBRADA)) {
            GUIUtils.showMessage("Você pode alterar a quantidade somente de operações quebradas.", Alert.AlertType.INFORMATION);
            tblProgramacao.refresh();
            return;
        }
        
        if (programacaoSelecionada.getStatusProgramacao() == StatusProgramacao.LANCADO) {
            GUIUtils.showMessage("Existem lançamentos de produção para esta operação, não é permitido alteração de quantidade em operações com produção.");
            tblProgramacao.refresh();
            return;
        }
        
        int oldValue = programacaoSelecionada.getQtde();
        programacaoSelecionada.setQtde(event.getNewValue());
        programacaoSelecionada.setQtdeAlterada(true);
        
        //Define a quantidade de cada programacao
        final IntegerProperty totalPecas = new SimpleIntegerProperty(pacoteSelecionado.get().getQtde()
                - operacoesSetorSelecionado.stream()
                .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                        operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem() &&
                        operacao.getId().getQuebra() != programacaoSelecionada.getId().getQuebra() &&
                        operacao.isQtdeAlterada())
                .mapToInt(SdProgramacaoPacote002::getQtde)
                .sum()
                - programacaoSelecionada.getQtde());
        final IntegerProperty totalQuebras = new SimpleIntegerProperty((int) operacoesSetorSelecionado.stream()
                .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                        operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem() &&
                        operacao.getId().getQuebra() != programacaoSelecionada.getId().getQuebra() &&
                        !operacao.isQtdeAlterada())
                .count());
        
        if (totalPecas.get() <= 0) {
            GUIUtils.showMessage("Você está colocando uma quantidade para a quebra que na sua soma será maior que a quantidade do pacote.", Alert.AlertType.WARNING);
            programacaoSelecionada.setQtde(oldValue);
            programacaoSelecionada.setQtdeAlterada(false);
            return;
        } else if (totalQuebras.get() == 0) {
            GUIUtils.showMessage("Essa é a última quebra da operação, a quantidade não pode ser alterada.", Alert.AlertType.WARNING);
            programacaoSelecionada.setQtde(oldValue);
            programacaoSelecionada.setQtdeAlterada(false);
            return;
        }
        
        final IntegerProperty qtdeQuebra = new SimpleIntegerProperty(((int) (totalPecas.get() / totalQuebras.get())) + (totalPecas.get() % totalQuebras.get() == 0 ? 0 : 1));
        for (SdProgramacaoPacote002 selectedItem : operacoesSetorSelecionado.stream()
                .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                        operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem() &&
                        operacao.getId().getQuebra() != programacaoSelecionada.getId().getQuebra() &&
                        !operacao.isQtdeAlterada())
                .collect(Collectors.toList())) {
            selectedItem.setQtde(qtdeQuebra.get());
            selectedItem.setTempoTotal(BigDecimal.valueOf(qtdeQuebra.multiply(selectedItem.getTempoOp().doubleValue()).get()));
            totalPecas.set(totalPecas.subtract(qtdeQuebra.get()).get());
            if (totalPecas.lessThan(qtdeQuebra).get())
                qtdeQuebra.set(totalPecas.get());
        }
        
        programacaoSelecionada.setTempoTotal(BigDecimal.valueOf(programacaoSelecionada.getQtde() * programacaoSelecionada.getTempoOpAgrupada().doubleValue()));
        tblProgramacao.refresh();
        
        // <editor-fold defaultstate="collapsed" desc="[log register]">
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                        + "Alterado a qtde da operação " + programacaoSelecionada.getId().getOperacao().getCodorg() + " setor " + programacaoSelecionada.getId().getSetorOp() + " na ordem " + programacaoSelecionada.getId().getOrdem() + " quebra " + programacaoSelecionada.getId().getQuebra()
                        + " de " + event.getOldValue() + " para " + event.getNewValue()
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        //</editor-fold>
    }
    
    /**
     * Método para executar a ação do click do botão programar of
     *
     * @param selectedRow
     */
    private void actionBtnProgramarOf(VSdOfsParaProgramar selectedRow) {
        try {
            programacaoProducaoOf.set(new SdProgramacaoOf001(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")),
                    41,
                    selectedRow.getSetor(),
                    selectedRow.getOp(),
                    "R",
                    selectedRow.getQtde(),
                    "N"
            ));
            ofParaProgramarSelecionada.set(selectedRow);
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
            
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            rbtnReferencia.setSelected(true);
            criarPacoteReferencia();
            
            listaDePacotesProgramados.set(daoPacote.initCriteria()
                    .addPredicateEqCascateField("produto.codigo", ofParaProgramacao.getCodigo(), false)
                    .loadListByPredicate());
            
            SysLogger.addFileLogProgramacaoOf("Iniciado programação da OF " + programacaoProducaoOf.get().getOrdemProd()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnProgramarOf");
            GUIUtils.showException(ex);
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro ao pressionar o botão de programar OF "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    
    /**
     * Método para executar a ação do click do botão movimentar of
     *
     * @param selectedRow
     */
    @Deprecated
    private void actionBtnMovimentarOf(VSdOfsParaProgramar selectedRow) {
    
    }
    
    /**
     * Método para executar a ação do click do botão abrir programacao
     *
     * @param selectedRow
     */
    private void actionBtnVisualizarProgramacao(VSdOfsParaProgramar selectedRow) {
        
        try {
            emVisualizacao.set(true);
            programacaoProducaoOf.set(DAOFactory.getSdProgramacaoOf001DAO().getByOf(selectedRow.getOp()));
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            SysLogger.addFileLogProgramacaoOf("Abrindo programação da OF " + programacaoProducaoOf.get().getOrdemProd() + " para visualização "
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote inicial">
            switch(programacaoProducaoOf.get().getTipoPacote()) {
                case "R":
                    rbtnReferencia.setSelected(true);
                    break;
                case "C":
                    rbtnCor.setSelected(true);
                    break;
                case "T":
                    rbtnTamanho.setSelected(true);
                    break;
                default:
                    rbtnReferencia.setSelected(true);
                    break;
            }
            listaDePacotes.clear();
            listaDePacotes.set(DAOFactory.getSdPacote001DAO().getByNumero(ofParaProgramacao.getNumero()));
            totalPacotesOf.set(listaDePacotes.getSize());
            cboxPacote.getSelectionModel().select(0);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote lancamento inicial">
            switch(pacoteSelecionado.get().getTipoLancamento()) {
                case "R":
                    rbtnReferenciaLancamento.setSelected(true);
                    break;
                case "C":
                    rbtnCorLancamento.setSelected(true);
                    break;
                case "T":
                    rbtnTamanhoLancamento.setSelected(true);
                    break;
                default:
                    rbtnReferenciaLancamento.setSelected(true);
                    break;
            }
            // </editor-fold>
            
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnVisualizarProgramacao");
            GUIUtils.showException(ex);
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro ao pressionar o botão de visualizar OF "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    
    /**
     * Método para executar a ação do click do botão editar programacao
     *
     * @param selectedRow
     */
    private void actionBtnEditarProgramacao(VSdOfsParaProgramar selectedRow) {
        
        try {
            emEdicao.set(true);
            btnIniciarInativo.set(true);
            programacaoProducaoOf.set(DAOFactory.getSdProgramacaoOf001DAO().getByOf(selectedRow.getOp()));
            ofParaProgramacao = programacaoProducaoOf.get().getOf1();
            bindOfProgramacao(ofParaProgramacao);
            SysLogger.addFileLogProgramacaoOf("Abrindo programação da OF " + programacaoProducaoOf.get().getOrdemProd() + " para edição "
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            
            ofParaProgramarSelecionada.set(selectedRow);
            
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote inicial">
            switch(programacaoProducaoOf.get().getTipoPacote()) {
                case "C":
                    rbtnCor.setSelected(true);
                    criarPacoteCor();
                    break;
                case "T":
                    rbtnTamanho.setSelected(true);
                    criarPacoteTamanho();
                    break;
                default:
                    rbtnReferencia.setSelected(true);
                    criarPacoteReferencia();
                    break;
            }
            //listaDePacotes.clear();
            //listaDePacotes.set(DAOFactory.getSdPacote001DAO().getByNumero(ofParaProgramacao.getNumero()));
            //totalPacotesOf.set(listaDePacotes.getSize());
            //cboxPacote.getSelectionModel().select(0);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Criação do pacote lancamento inicial">
            if (pacoteSelecionado.get().getTipoLancamento() != null) {
                switch(pacoteSelecionado.get().getTipoLancamento()) {
                    case "C":
                        criarPacoteLancamentoCor();
                        rbtnCorLancamento.setSelected(true);
                        break;
                    case "T":
                        criarPacoteLancamentoTamanho();
                        rbtnTamanhoLancamento.setSelected(true);
                        break;
                    default:
                        criarPacoteLancamentoReferencia();
                        rbtnReferenciaLancamento.setSelected(true);
                        break;
                }
            } else {
                rbtnReferenciaLancamento.setSelected(true);
                criarPacoteLancamentoReferencia();
            }
            // </editor-fold>
            
            tabsWindow.getSelectionModel().select(1);
            tabListagem.setDisable(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::actionBtnEditarProgramacao");
            GUIUtils.showException(ex);
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro ao pressionar o botão de editar OF "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos de BIND e UNBIND">
    
    /**
     * Método para bind da celula selecionada com os elementos que mostram os
     * dados
     *
     * @param celula
     */
    private void bindCelulaSelecionada(SdCelula celula) {
        if (celula != null) {
            tboxCelula.textProperty().bind(celula.codigoProperty().asString());
            lbDescricaoCelula.textProperty().bind(celula.descricaoProperty());
        }
        tboxCelula.textProperty().unbind();
        lbDescricaoCelula.textProperty().unbind();
    }
    
    /**
     * Ligação dos dados da OF selecionada com os componentes do cabeçalho da
     * tela.
     *
     * @param of
     * @throws SQLException
     */
    private void bindOfProgramacao(Of1001 of) throws Exception {
        if (of != null) {
            tboxNumeroOf.textProperty().bind(of.numeroProperty());
            tboxQtdeOf.textProperty().bind(of.ativosProperty().asString());
            tboxFamiliaOf.textProperty().bind(of.getRelCodigo().getRelFamilia().descricaoProperty());
            tboxProdutoOf.textProperty().bind(of.codigoProperty());
            tboxDescricaoProduto.textProperty().bind(of.getRelCodigo().descricaoProperty());
        }
    }
    
    /**
     * Método de ligação de dados quando selecionada uma operação na table de
     * programacao
     *
     * @param programacaoSelecionada
     * @throws SQLException
     */
    private void bindProgramacao(SdProgramacaoPacote002 programacaoSelecionada) {
        if (programacaoSelecionada != null) {
            verificaPolivalenciaProgramacao(programacaoSelecionada);
        }
    }
    
    /**
     * Binding dados do colaborador após selecionar uma opção no combobox
     */
    private void bindColaboradorSelecionado(VSdColabCelProg colaborador) {
        tpaneColaborador.setText("");
        if (colaborador != null) {
            ocupacaoColaborador.bindBidirectional(colaborador.programacaoProperty());
            tpaneColaborador.setText(colaborador.getColaborador().toString());
        }
    }
    
    private void unbindColaboradorSelecionado(VSdColabCelProg colaborador) {
        tpaneColaborador.setText("");
        if (colaborador != null) {
            ocupacaoColaborador.unbindBidirectional(colaborador.programacaoProperty());
        }
    }
    
    /**
     * Método de ligação de dados quando selecionada uma operação na table de
     * programacao
     *
     * @param operacaoSelecionada
     * @throws SQLException
     */
    private void bindOperacaoColaborador(VSdProgramacaoPendente operacaoSelecionada) {
        tpaneOperacoesAgrupadas.setExpanded(false);
        if (operacaoSelecionada != null) {
            if (operacaoSelecionada.getProgramacao().getStatusProgramacao().equals(StatusProgramacao.AGRUPADA)) {
                operacoesAgrupadas.clear();
                try {
                    operacoesAgrupadas.set(operacaoSelecionada.getProgramacao().getOperacoesAgrupadas());
                    if (operacoesAgrupadas.size() == 0) {
                        operacoesAgrupadas.set(daoProgramacao
                                .initCriteria()
                                .addPredicateEq("agrupador", operacaoSelecionada.getProgramacao().getId().getOperacao().getCodorg())
                                .addPredicateEqPkEmbedded("id", "pacote", operacaoSelecionada.getProgramacao().getId().getPacote(), false)
                                .addPredicateEqPkEmbedded("id", "setorOp", operacaoSelecionada.getProgramacao().getId().getSetorOp().getCodigo(), false)
                                .loadListByPredicate());
                    }
                    tpaneOperacoesAgrupadas.setExpanded(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                    LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                    GUIUtils.showException(e);
                }
            }
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Criar Pacotes Programação">
    private void criarPacoteTamanho() {
        listaDePacotes.clear();
        if (emEdicao.get()) {
            try {
                listaDePacotes.set(DAOFactory.getSdPacote001DAO().getListByPacote(ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor())));
                if (listaDePacotes.size() > 0) {
                    listaDePacotes.forEach(sdPacote001 -> {
                        sdPacote001.setPacoteProgramado(true);
                    });
                } else {
                    listaDePacotes.set(DAOFactory.getSdPacote001DAO().getListByPacote(ofParaProgramarSelecionada.get().getOp()));
                    if (listaDePacotes.size() > 0) {
                        listaDePacotes.forEach(sdPacote001 -> {
                            sdPacote001.setPacoteProgramado(true);
                        });
                    } else {
                        GUIUtils.showMessage("Não foi possível encontrar o pacote para a programação " + ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor()));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                                + "Erro ao carregar um pacote já programado "
                                + "EXCEPTION: " + e.getMessage()
                                + " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
            }
        } else {
            final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
            ofParaProgramacao.getGrade().forEach(gradeCor -> {
                gradeCor.getHeadersTamanhos().entrySet().stream()
                        .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                        .forEach((entry) -> {
                            if (gradeCor.getTamanhos().get(entry.getValue()) > 0) {
                                ordemPacote.set(ordemPacote.add(1).get());
                                SdPacote001 pacoteInicial = new SdPacote001(
                                        ordemPacote.get(),
                                        ofParaProgramacao.getNumero(),
                                        ofParaProgramacao.getCodigo(),
                                        gradeCor.getCor(),
                                        entry.getValue(),
                                        gradeCor.getTamanhos().get(entry.getValue()),
                                        "T",
                                        null,
                                        ofParaProgramarSelecionada.get().getSetor()
                                );
                                listaDePacotes.add(pacoteInicial);
                            }
                        });
            });
        }
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
        
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote por tamanho no total de " + listaDePacotes.getSize()
                + " pacote(s) por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    
    /**
     * Criação dos objetos dos pacotes por cor e inserção no array de pacotes
     */
    private void criarPacoteCor() {
        listaDePacotes.clear();
        if (emEdicao.get()) {
            try {
                listaDePacotes.set(DAOFactory.getSdPacote001DAO().getListByPacote(ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor())));
                if (listaDePacotes.size() > 0) {
                    listaDePacotes.forEach(sdPacote001 -> {
                        sdPacote001.setPacoteProgramado(true);
                    });
                } else {
                    listaDePacotes.set(DAOFactory.getSdPacote001DAO().getListByPacote(ofParaProgramarSelecionada.get().getOp()));
                    if (listaDePacotes.size() > 0) {
                        listaDePacotes.forEach(sdPacote001 -> {
                            sdPacote001.setPacoteProgramado(true);
                        });
                    } else {
                        GUIUtils.showMessage("Não foi possível encontrar o pacote para a programação " + ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor()));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                                + "Erro ao carregar um pacote já programado "
                                + "EXCEPTION: " + e.getMessage()
                                + " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
            }
        } else {
            final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
            final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
            ofParaProgramacao.getGrade().forEach(gradeCor -> {
                qtdeTotalPacoteInicial.set(0);
                gradeCor.getTamanhos().forEach((key, value) -> {
                    qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
                });
                if (qtdeTotalPacoteInicial.get() > 0) {
                    ordemPacote.set(ordemPacote.add(1).get());
                    SdPacote001 pacoteInicial = new SdPacote001(
                            ordemPacote.get(),
                            ofParaProgramacao.getNumero(),
                            ofParaProgramacao.getCodigo(),
                            gradeCor.getCor(),
                            null,
                            qtdeTotalPacoteInicial.get(),
                            "C",
                            null,
                            ofParaProgramarSelecionada.get().getSetor()
                    );
                    listaDePacotes.add(pacoteInicial);
                }
            });
        }
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
        
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote por cor com total de " + listaDePacotes.getSize()
                + " pacote(s) por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    
    /**
     * Criação do objeto do pacote por referência e inserção no array de pacotes
     */
    private void criarPacoteReferencia() {
        listaDePacotes.clear();
        SdPacote001 pacotePorReferencia = null;
        
        if (emEdicao.get()) {
            try {
                pacotePorReferencia = DAOFactory.getSdPacote001DAO().getByPacote(ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor()));
                if (pacotePorReferencia != null) {
                    pacotePorReferencia.setPacoteProgramado(true);
                } else {
                    pacotePorReferencia = DAOFactory.getSdPacote001DAO().getByPacote(ofParaProgramarSelecionada.get().getOp());
                    if (pacotePorReferencia != null) {
                        pacotePorReferencia.setPacoteProgramado(true);
                    } else {
                        GUIUtils.showMessage("Não foi possível encontrar o pacote para a programação " + ofParaProgramarSelecionada.get().getOp().concat(ofParaProgramarSelecionada.get().getSetor()));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                                + "Erro ao carregar um pacote já programado "
                                + "EXCEPTION: " + e.getMessage()
                                + " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
            }
        } else {
            final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
            ofParaProgramacao.getGrade().forEach(gradeCor -> {
                gradeCor.getTamanhos().forEach((key, value) -> {
                    qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
                });
            });
            pacotePorReferencia = new SdPacote001(
                    1,
                    ofParaProgramacao.getNumero(),
                    ofParaProgramacao.getCodigo(),
                    null,
                    null,
                    qtdeTotalPacoteInicial.get(),
                    "R",
                    null,
                    ofParaProgramarSelecionada.get().getSetor()
            );
        }
        listaDePacotes.add(pacotePorReferencia);
        totalPacotesOf.set(listaDePacotes.getSize());
        cboxPacote.getSelectionModel().select(0);
        
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote para referência " + pacotePorReferencia.getCodigo()
                + " por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Criar Pacotes Lançamento">
    
    /**
     * Criação dos objetos dos pacotes de lançamento por cor e tamanho e
     * inserção no array de pacotes de lançamento
     */
    private void criarPacoteLancamentoTamanho() {
        listaDePacotesLancamento.clear();
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            if (pacoteSelecionado.get().getCor() == null || pacoteSelecionado.get().getCor().equals(gradeCor.getCor())) {
                gradeCor.getHeadersTamanhos().entrySet().stream()
                        .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                        .forEach((entry) -> {
                            if (pacoteSelecionado.get().getTam() == null || pacoteSelecionado.get().getTam().equals(entry.getValue())) {
                                if (gradeCor.getTamanhos().get(entry.getValue()) > 0) {
                                    ordemPacote.set(ordemPacote.add(1).get());
                                    SdPacote001 pacoteInicial = new SdPacote001(
                                            pacoteSelecionado.get().getCodigo(),
                                            ordemPacote.get(),
                                            ofParaProgramacao.getNumero(),
                                            ofParaProgramacao.getCodigo(),
                                            gradeCor.getCor(),
                                            entry.getValue(),
                                            gradeCor.getTamanhos().get(entry.getValue()),
                                            "T",
                                            null);
                                    listaDePacotesLancamento.add(pacoteInicial);
                                }
                            }
                        });
            }
        });
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote de lançamento por tamanho no total de " + listaDePacotes.getSize()
                + " pacote(s) por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    
    /**
     * Criação dos objetos dos pacotes por cor e inserção no array de pacotes
     */
    private void criarPacoteLancamentoCor() {
        listaDePacotesLancamento.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        final IntegerProperty ordemPacote = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            if (pacoteSelecionado.get().getCor() == null || pacoteSelecionado.get().getCor().equals(gradeCor.getCor())) {
                qtdeTotalPacoteInicial.set(0);
                gradeCor.getTamanhos().forEach((key, value) -> {
                    qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
                });
                if (qtdeTotalPacoteInicial.get() > 0) {
                    ordemPacote.set(ordemPacote.add(1).get());
                    SdPacote001 pacoteInicial = new SdPacote001(
                            pacoteSelecionado.get().getCodigo(),
                            ordemPacote.get(),
                            ofParaProgramacao.getNumero(),
                            ofParaProgramacao.getCodigo(),
                            gradeCor.getCor(),
                            null,
                            qtdeTotalPacoteInicial.get(),
                            "C",
                            null);
                    listaDePacotesLancamento.add(pacoteInicial);
                }
            }
        });
        
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote de lançamento por cor no total de " + listaDePacotes.getSize()
                + " pacote(s) por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    
    /**
     * Criação do objeto do pacote por referência e inserção no array de pacotes
     */
    private void criarPacoteLancamentoReferencia() {
        listaDePacotesLancamento.clear();
        final IntegerProperty qtdeTotalPacoteInicial = new SimpleIntegerProperty(0);
        ofParaProgramacao.getGrade().forEach(gradeCor -> {
            gradeCor.getTamanhos().forEach((key, value) -> {
                qtdeTotalPacoteInicial.set(qtdeTotalPacoteInicial.add(value).get());
            });
        });
        SdPacote001 pacotePorReferencia = new SdPacote001(
                pacoteSelecionado.get().getCodigo(),
                1,
                ofParaProgramacao.getNumero(),
                ofParaProgramacao.getCodigo(),
                null,
                null,
                qtdeTotalPacoteInicial.get(),
                "R",
                null);
        listaDePacotesLancamento.add(pacotePorReferencia);
        totalPacotesLancamentoOf.set(listaDePacotesLancamento.getSize());
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd()
                + " - Criado pacote de lançamento para a referência " + ofParaProgramacao.getCodigo()
                + " por " + SceneMainController.getAcesso().getUsuario(), "PROGRAMAÇÃO OF");
    }
    //</editor-fold>
    
    /**
     * Rotina que verifica se o colaborador tem a polivalencia na operacao selecionada ou se é ele quem está programado para realizar a mesma.
     *
     * @param programacaoSelecionada
     */
    private void verificaPolivalenciaProgramacao(SdProgramacaoPacote002 programacaoSelecionada) {
        colaboradoresParaProgramacao.forEach(colaborador -> {
            if (programacaoSelecionada.getColaborador() != null && colaborador.getColaborador().getCodigo() == programacaoSelecionada.getColaborador().getCodigo()) {
                colaborador.setStatusColaborador(StatusProgramacao.PROGRAMADO);
                programacaoSelecionada.setColaboradorCelula(colaborador);
            } else if (colaborador.getPolivalencia().stream().filter(polivalencia -> polivalencia.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg()).count() > 0) {
                colaborador.setStatusColaborador(StatusProgramacao.POLIVALENTE);
            } else {
                colaborador.setStatusColaborador(StatusProgramacao.LIVRE);
            }
        });
        tblColaboradoresOperacao.refresh();
    }
    
    /**
     * Rotina que reordena o encadeamento dos objetos das operações da
     * programação. Passar como parâmetro o objeto anterior ao que precisa
     * iniciar a reordenação.
     *
     * @param posicaoAnterior
     */
    private void reordenarEncadeamentoObjetosProgramacao(SdProgramacaoPacote002 posicaoAnterior) {
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                        "Reordenando o encadeamento das operações " +
                        " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        int indicePrimeiraOperacao = posicaoAnterior != null ? operacoesParaProgramacao.indexOf(posicaoAnterior) : -1;
        SdProgramacaoPacote002 operacaoAnterior = posicaoAnterior;
        for (int i = indicePrimeiraOperacao + 1; i < operacoesParaProgramacao.size(); i++) {
            SdProgramacaoPacote002 operacaoAtual = operacoesParaProgramacao.get(i);
            if (operacaoAtual.getId().getQuebra() > 1) {
                operacaoAtual.setOperacaoAnterior(operacaoAnterior.getOperacaoAnterior());
                operacaoAtual.setOperacaoPosterior(operacaoAnterior.getOperacaoPosterior());
            } else {
                operacaoAtual.setOperacaoAnterior(operacaoAnterior);
                operacaoAtual.setOperacaoPosterior(null);
                if (operacaoAnterior != null) {
                    operacaoAnterior.setOperacaoPosterior(operacaoAtual);
                }
                operacaoAnterior = operacaoAtual;
            }
        }
    }
    
    /**
     * Rotina para selecionar as operações do setor selecionado na ComboBox.
     *
     * @param setorSelecionado
     */
    private void selectSetorOperacao(SdSetorOp001 setorSelecionado) {
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                        + "Selecionado o setor de operação " + setorSelecionado.getDescricao()
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        operacoesSetorSelecionadoParaProgramacao.clear();
        operacoesSetorSelecionado.clear();
        operacoesSetorSelecionado = operacoesParaProgramacao
                .stream()
                .filter(operacaoParaProgramacao -> operacaoParaProgramacao.getId().getSetorOp().getCodigo() == setorSelecionado.getCodigo())
                .collect(Collectors.toList());
        operacoesSetorSelecionadoParaProgramacao.set(FXCollections.observableList(operacoesSetorSelecionado));
        if (emVisualizacao.or(emEdicao).and(operacoesSetorSelecionadoParaProgramacao.emptyProperty().not()).get()) {
            celulaSetorSelecionada.copy(operacoesSetorSelecionado.get(0).getCelula());
            celulaSetorOperacaoSelecionada = new SdCelula(celulaSetorSelecionada);
            
            celulaSelecionada.copy(operacoesSetorSelecionado.get(0).getCelula());
            //celulaSelecionada.setSelected(true);
            
            colaboradoresCelulaSelecionada.clear();
            colaboradoresCelulaSelecionada.set(FXCollections.observableList(colaboradoresParaProgramacao.get()
                    .stream()
                    .filter(colaborador -> colaborador.getCelula().getCodigo() == celulaSelecionada.getCodigo())
                    .collect(Collectors.toList())));
            
            if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 1) {
                SdProgramacaoPacote002 operacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
                verificaPolivalenciaProgramacao(operacaoSelecionada);
            }
        }
        
        tblProgramacao.refresh();
        
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                        + "Carregado as operações do setor " + setorSelecionado.getDescricao()
                        + " no total de  " + operacoesSetorSelecionado.size() + " operações "
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
    }
    
    /**
     * Rotina que cria o objeto da operacao para ser programada.
     *
     * @param operacaoRoteiro
     * @return SdProgramacaoPacote001
     * @throws NumberFormatException
     * @throws SQLException
     */
    private SdProgramacaoPacote002 criaObjetoOperacaoRoteiro(SdOperRoteiroOrganize001 operacaoRoteiro)
            throws NumberFormatException, SQLException {
        SdProgramacaoPacote002 operacaoParaProgramacao = new SdProgramacaoPacote002(
                pacoteSelecionado.get().getCodigo(),
                (SdSetorOp001) daoSetorOp.initCriteria().addPredicateEq("codigo", Integer.parseInt(operacaoRoteiro.getSetorOperacao())).loadEntityByPredicate(),
                operacaoRoteiro.getId().getOperacao(),
                operacaoRoteiro.getId().getOrdem(),
                1,
                operacaoRoteiro.getId().getOperacao().getEquipamentoOrganize(),
                BigDecimal.valueOf(operacaoRoteiro.getTempoOp().doubleValue()),
                pacoteSelecionado.get().getQtde(),
                0,
                LocalDateTime.now(),
                999,
                BigDecimal.valueOf(operacaoRoteiro.getTempoOp().doubleValue() * pacoteSelecionado.get().getQtde()),
                StatusProgramacao.LIVRE);
        
        return operacaoParaProgramacao;
    }
    
    /**
     * Rotina que copia as programações de outro pacote. As operações já vem com
     * toda a configuração salva.
     *
     * @throws SQLException
     */
    private void carregaRoteiroOperacoesProduto() throws SQLException, NumberFormatException {
        roteiroOperacoesProduto = DAOFactory.getSdOperRoteiroOrganize001DAO().getByReferenciaAndSetor(ofParaProgramacao.getCodigo(), programacaoProducaoOf.get().getSetor());
        
        operacoesParaProgramacao.clear();
        SdProgramacaoPacote002 operacaoAnterior = null;
        for (SdOperRoteiroOrganize001 operacaoProduto : roteiroOperacoesProduto) {
            SdProgramacaoPacote002 operacaoAtual = criaObjetoOperacaoRoteiro(operacaoProduto);
            operacaoAtual.setOperacaoAnterior(operacaoAnterior);
            if (operacaoAnterior != null) {
                operacaoAnterior.setOperacaoPosterior(operacaoAtual);
            }
            operacaoAnterior = operacaoAtual;
            operacoesParaProgramacao.add(operacaoAtual);
        }
        
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                        + "Carregando operações do roteiro do produto " + ofParaProgramacao.getCodigo()
                        + " no total de  " + operacoesParaProgramacao.size() + " operações "
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
    }
    
    /**
     * Rotina para remover uma operação da ocupação de um colaborador.
     *
     * @param programacao
     * @param colaborador
     */
    private void removeProgramacaoOcupacaoColaborador(SdProgramacaoPacote002 programacao, SdColaborador colaborador) {
        colaboradoresParaProgramacao.forEach(colaboradores -> {
            if (colaboradores.getColaborador().getCodigo() == colaborador.getCodigo()) {
                colaboradores.getProgramacao().removeIf(vSdProgramacaoPendente -> vSdProgramacaoPendente.getProgramacao().equals(programacao));
                return;
            }
        });
    }
    
    /**
     * Rotina que carrega a programacao de um pacote.
     */
    private void carregaSetoresParaLeitura() {
        if (pacoteSelecionado.get() == null) {
            GUIUtils.showMessage("Você deve selecionar um pacote para iniciar a programação.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        
        try {
            this.carregaProgramacaoPacote(pacoteSelecionado.get());
            if (emVisualizacao.not().get())
                this.verificaAgrupamento();
            setoresOperacao.set(daoSetorOp.list());
            cboxSetoresOperacao.getSelectionModel().select(0);
            
        } catch (SQLException | NumberFormatException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro ao carregar os setores de operação para leitura "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    
    /**
     * Rotina que carrega a programação do pacote para exibição ou edicao
     *
     * @throws SQLException
     */
    private void carregaProgramacaoPacote(SdPacote001 pacote) throws SQLException, NumberFormatException {
        ObservableList<SdProgramacaoPacote002> roteiroOperacoesPacote = daoProgramacao
                .initCriteria()
                .addPredicateEqPkEmbedded("id", "pacote", pacote.getCodigo(), false)
                .addOrderByAsc("id", "ordem")
                .loadListByPredicate();
        Comparator<SdProgramacaoPacote002> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
        comparator = comparator.thenComparing(Comparator.comparing(programacao -> programacao.getId().getQuebra()));
        roteiroOperacoesPacote.sort(comparator);
        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                        + "Carregando operações da programacao do pacote  " + pacote.getCodigo()
                        + " no total de  " + roteiroOperacoesPacote.size() + " operações "
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        
        if (emProgramacao.get()) {
            ObservableList<SdProgramacaoPacote002> roteiroOperacoesPacoteToCopy = FXCollections.observableArrayList();
            roteiroOperacoesPacoteToCopy.addAll(roteiroOperacoesPacote);
            roteiroOperacoesPacote.clear();
            roteiroOperacoesPacoteToCopy.forEach(sdProgramacaoPacote002 -> {
                SdProgramacaoPacote002 programacaoCopiada = new SdProgramacaoPacote002(sdProgramacaoPacote002);
                programacaoCopiada.getId().setPacote(pacoteSelecionado.get().getCodigo());
                programacaoCopiada.setStatusProg(0);
                programacaoCopiada.setStatusProgramacao(StatusProgramacao.LIVRE);
                programacaoCopiada.setQtde(pacoteSelecionado.get().getQtde());
                if (programacaoCopiada.getOperacoesAgrupadas().size() > 0 || programacaoCopiada.getAgrupador() != 0) {
                    programacaoCopiada.setStatusProgramacao(StatusProgramacao.AGRUPADA);
                }
                roteiroOperacoesPacote.add(programacaoCopiada);
            });
        }
        
        operacoesParaProgramacao.clear();
        operacoesParaProgramacao.addAll(roteiroOperacoesPacote);
        operacoesParaProgramacao.forEach(sdProgramacaoPacote002 -> {
            if (sdProgramacaoPacote002.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || sdProgramacaoPacote002.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
                programacaoComLancametos.set(true);
                return;
            }
        });
        
        if (emEdicao.or(emProgramacao).get()) {
            reordenarEncadeamentoObjetosProgramacao(null);
            colaboradoresParaProgramacao.clear();
            colaboradoresParaProgramacao.set(daoColabsCelula
                    .initCriteria()
                    .addPredicateEq("ativo", true, false)
                    .loadListByPredicate());
            
            operacoesParaProgramacao.stream()
                    .filter(sdProgramacaoPacote002 -> sdProgramacaoPacote002.getAgrupador() != 0)
                    .forEach(sdProgramacaoPacote002 -> {
                        sdProgramacaoPacote002.setOperacaoAgrupador(
                                operacoesParaProgramacao.stream()
                                        .filter(sdProgramacaoPacote0021 -> sdProgramacaoPacote0021.getId().getOperacao().getCodorg() == sdProgramacaoPacote002.getId().getOperacao().getCodorg()
                                                && sdProgramacaoPacote0021.getId().getSetorOp().getCodigo() == sdProgramacaoPacote002.getId().getSetorOp().getCodigo())
                                        .findFirst().get());
                    });
        }
        
    }
    
    /**
     * Verificando os agrupamentos do roteiro.
     */
    private void verificaAgrupamento() {
        for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesParaProgramacao) {
            List<SdProgramacaoPacote002> operacoesAgrupadas = operacoesParaProgramacao.stream()
                    .filter(sdProgramacaoPacote -> sdProgramacaoPacote.getId().getSetorOp().getCodigo() == sdProgramacaoPacote002.getId().getSetorOp().getCodigo() &&
                            sdProgramacaoPacote.getAgrupador() == sdProgramacaoPacote002.getId().getOperacao().getCodorg())
                    .collect(Collectors.toList());
            if (operacoesAgrupadas.size() > 0) {
                sdProgramacaoPacote002.setOperacoesAgrupadas(FXCollections.observableList(operacoesAgrupadas));
            } else if (sdProgramacaoPacote002.getAgrupador() != 0) {
                SdProgramacaoPacote002 operacaoAgrupada = sdProgramacaoPacote002.getOperacaoAnterior();
                if (sdProgramacaoPacote002.getOperacaoAnterior().getAgrupador() != 0) {
                    operacaoAgrupada = sdProgramacaoPacote002.getOperacaoAnterior().getOperacaoAgrupador();
                }
                sdProgramacaoPacote002.setOperacaoAgrupador(operacaoAgrupada);
            }
        }
    }
    
    // ------------------------------- FXML ACTIONS -----------------------------------------------
    
    @FXML
    private void btnIniciarProgramacaoOnAction(ActionEvent event) {
        SysLogger.addFileLogProgramacaoOf("Iniciando carga do roteiro para programação da OF " + programacaoProducaoOf.get().getOrdemProd()
                        + " por " + SceneMainController.getAcesso().getUsuario(),
                "PROGRAMAÇÃO OF");
        if (((RadioButton) pacoteLancamento.getSelectedToggle()).disableProperty().get()) {
            GUIUtils.showMessage("Você deve selecionar como será o pacote de lançamento da produção.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        if (pacoteSelecionado == null) {
            GUIUtils.showMessage("Você deve selecionar um pacote para iniciar a programação.",
                    Alert.AlertType.INFORMATION);
            return;
        }
        emProgramacao.set(true);
        
        cboxPacote.getItems().remove(pacoteSelecionado);
        
        try {
            if (cboxPacoteProgramado.getSelectionModel().selectedItemProperty().isNotNull().get()) {
                carregaProgramacaoPacote(cboxPacoteProgramado.getSelectionModel().getSelectedItem());
            } else {
                carregaRoteiroOperacoesProduto();
                
                colaboradoresParaProgramacao.clear();
                colaboradoresParaProgramacao.set(daoColabsCelula
                        .initCriteria()
                        .addPredicateEq("ativo", true, false)
                        .loadListByPredicate());
            }
            setoresOperacao.set(daoSetorOp.list());
            cboxSetoresOperacao.getSelectionModel().select(0);
            
        } catch (SQLException | NumberFormatException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - "
                            + "Erro ao iniciar a carga das operações para iniciar programação "
                            + "EXCEPTION: " + ex.getMessage()
                            + " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    
    @FXML
    private void requestMenuAgruparOperacaoOnAction(ActionEvent event) {
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        SdProgramacaoPacote002 operacaoParaProgramacao = tblProgramacao.getSelectionModel().getSelectedItem();
        
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() > 1) {
            GUIUtils.showMessage("Para agrupar uma operação, você deve selecionar somente a operação que deseja agrupar.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getOperacaoAnterior() == null || operacaoParaProgramacao.equals(operacoesSetorSelecionado.stream().findFirst().get())) {
            GUIUtils.showMessage("Esta é a primeira operação do setor, ela não pode ser agrupada. Você precisa primeiro alterar a ordem da operação para após a operação que ela deverá agrupar.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || operacaoParaProgramacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Esta operação já está em produção ou finalizada e não pode ser alterada.", Alert.AlertType.WARNING);
            return;
        }
        //</editor-fold>
        
        SdProgramacaoPacote002 operacaoAgrupada = operacaoParaProgramacao.getOperacaoAnterior();
        if (operacaoParaProgramacao.getOperacaoAnterior().getAgrupador() != 0) {
            operacaoAgrupada = operacaoParaProgramacao.getOperacaoAnterior().getOperacaoAgrupador();
        }
        if (operacaoAgrupada.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || operacaoAgrupada.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Você está tentando agrupar em uma operação que já está em produção ou finalizada, esse procedimento não é permitido.", Alert.AlertType.WARNING);
            return;
        }
        
        this.requestMenuExcluirColaboradorOnAction(null);
        operacaoParaProgramacao.setAgrupador(operacaoAgrupada.getId().getOperacao().getCodorg());
        operacaoParaProgramacao.setOperacaoAgrupador(operacaoAgrupada);
        operacaoParaProgramacao.setColaborador(operacaoAgrupada.getColaborador());
        operacaoParaProgramacao.setColaboradorCelula(operacaoAgrupada.getColaboradorCelula());
        operacaoParaProgramacao.setSeqColaborador(999);
        operacaoParaProgramacao.setCelula(operacaoAgrupada.getCelula());
        operacaoParaProgramacao.setStatusProgramacao(StatusProgramacao.AGRUPADA);
        operacaoAgrupada.getOperacoesAgrupadas().add(operacaoParaProgramacao);
        operacaoAgrupada.setStatusProgramacao(StatusProgramacao.AGRUPADA);
        operacaoAgrupada.setTempoOpAgrupada(operacaoAgrupada.getTempoOpAgrupada().add(operacaoParaProgramacao.getTempoOp()));
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuDesagruparOperacaoOnAction(ActionEvent event) {
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        SdProgramacaoPacote002 operacaoParaProgramacao = tblProgramacao.getSelectionModel().getSelectedItem();
        
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() > 1) {
            GUIUtils.showMessage("Para desagrupar uma operação, você deve selecionar somente a operação que deseja ser desagrupada.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getAgrupador() == 0) {
            GUIUtils.showMessage("A operação selecionada não está agrupada.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getOperacaoPosterior() != null && operacaoParaProgramacao.getOperacaoPosterior().getAgrupador() != 0) {
            GUIUtils.showMessage("A operação posterior a esta selecionada também está agrupada e deve ser desagrupada primeiro.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || operacaoParaProgramacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Esta operação já está em produção ou finalizada e não pode ser alterada.", Alert.AlertType.WARNING);
            return;
        } else if (operacaoParaProgramacao.getOperacaoAgrupador().getStatusProgramacao().equals(StatusProgramacao.LANCADO) || operacaoParaProgramacao.getOperacaoAgrupador().getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Esta operação já está em produção ou finalizada e não pode ser alterada.", Alert.AlertType.WARNING);
            return;
        }
        //</editor-fold>
        
        operacaoParaProgramacao.getOperacaoAgrupador().setTempoOpAgrupada(operacaoParaProgramacao.getOperacaoAgrupador().getTempoOpAgrupada().subtract(operacaoParaProgramacao.getTempoOp()));
        operacaoParaProgramacao.getOperacaoAgrupador().getOperacoesAgrupadas().remove(operacaoParaProgramacao.getOperacaoAgrupador().getOperacoesAgrupadas().indexOf(operacaoParaProgramacao));
        if (operacaoParaProgramacao.getOperacaoAgrupador().getOperacoesAgrupadas().size() == 0) {
            operacaoParaProgramacao.getOperacaoAgrupador().setStatusProgramacao(operacaoParaProgramacao.getOperacaoAgrupador().getColaborador() != null ? StatusProgramacao.PROGRAMADO : StatusProgramacao.LIVRE);
        }
        operacaoParaProgramacao.setOperacaoAgrupador(null);
        operacaoParaProgramacao.setAgrupador(0);
        operacaoParaProgramacao.setStatusProgramacao(operacaoParaProgramacao.getColaborador() != null ? StatusProgramacao.PROGRAMADO : StatusProgramacao.LIVRE);
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuQuebrarOperacaoOnAction(ActionEvent event) {
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        for (SdProgramacaoPacote002 selectedItem : tblProgramacao.getSelectionModel().getSelectedItems()) {
            SdProgramacaoPacote002 programacaoSelecionada = selectedItem;
            // <editor-fold defaultstate="collapsed" desc="Validações">
            if (programacaoSelecionada.getAgrupador() != 0) {
                GUIUtils.showMessage("Você não pode quebrar operações agrupadas, quebre somente a primeira operação do agrupamento.", Alert.AlertType.WARNING);
                continue;
            } else if (programacaoSelecionada.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
                GUIUtils.showMessage("Esta operação já está finalizada e não pode ser alterada.", Alert.AlertType.WARNING);
                continue;
            }
            // </editor-fold>
            
            if (programacaoSelecionada != null) {
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                "Quebra da operação " + programacaoSelecionada.getId().getOperacao() + " setor " + programacaoSelecionada.getId().getSetorOp() + " na ordem " + programacaoSelecionada.getId().getOrdem() + " quebra " + programacaoSelecionada.getId().getQuebra() +
                                " manualmente " +
                                " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
                // Cria a programação de quebra da operacao
                SdProgramacaoPacote002 programacaoQuebrada = new SdProgramacaoPacote002(programacaoSelecionada);
                
                //Ajusta os objetos para quebrado e altera a quebra da programação quebrada
                programacaoSelecionada.setStatusProgramacao(StatusProgramacao.QUEBRADA);
                programacaoQuebrada.setColaborador(null);
                programacaoQuebrada.getId().setQuebra((int) operacoesSetorSelecionado.stream()
                        .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoQuebrada.getId().getOperacao().getCodorg() &&
                                operacao.getId().getOrdem() == programacaoQuebrada.getId().getOrdem()).count() + 1);
                programacaoQuebrada.setStatusProgramacao(StatusProgramacao.QUEBRADA);
                
                //Inclui a operação gerada com a quebra na lista de programacoes
                int posicaoOperacaoQuebrada = operacoesParaProgramacao.indexOf(programacaoSelecionada) + 1;
                operacoesParaProgramacao.add(posicaoOperacaoQuebrada, programacaoQuebrada);
                
                //Inclui a operação gerada com a quebra na lista de programacoes do setor
                //int posicaoOperacaoQuebradaSetor = operacoesSetorSelecionado.indexOf(programacaoSelecionada) + 1;
                //operacoesSetorSelecionado.add(posicaoOperacaoQuebradaSetor, programacaoQuebrada);
                selectSetorOperacao(setorOperacaoSelecionado);
                
                //Define a quantidade de cada programacao
                final IntegerProperty totalPecas = new SimpleIntegerProperty(pacoteSelecionado.get().getQtde());
                final IntegerProperty totalQuebras = new SimpleIntegerProperty((int) operacoesSetorSelecionado.stream()
                        .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoQuebrada.getId().getOperacao().getCodorg() &&
                                operacao.getId().getOrdem() == programacaoQuebrada.getId().getOrdem())
                        .count());
                final IntegerProperty qtdeQuebra = new SimpleIntegerProperty(((int) (totalPecas.get() / totalQuebras.get())) + (totalPecas.get() % totalQuebras.get() == 0 ? 0 : 1));
                
                operacoesSetorSelecionado.stream()
                        .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoQuebrada.getId().getOperacao().getCodorg() &&
                                operacao.getId().getOrdem() == programacaoQuebrada.getId().getOrdem()).forEach(programacao -> {
                    programacao.setQtde(qtdeQuebra.get());
                    programacao.setTempoTotal(BigDecimal.valueOf(qtdeQuebra.multiply(programacao.getTempoOp().doubleValue()).get()));
                    totalPecas.set(totalPecas.subtract(qtdeQuebra.get()).get());
                    if (totalPecas.lessThan(qtdeQuebra).get())
                        qtdeQuebra.set(totalPecas.get());
                });
                
            }
        }
        
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuExcluirQuebrarOperacaoOnAction(ActionEvent event) {
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        SdProgramacaoPacote002 programacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
        
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() > 1) {
            GUIUtils.showMessage("Você pode remover quebra somente de uma programação, selecione somente uma linha.", Alert.AlertType.WARNING);
            return;
        } else if (programacaoSelecionada.getId().getQuebra() == 1) {
            GUIUtils.showMessage("Você pode remover somente de operações que foram quebradas, elas estão identificadas na cor \"amarelo\". Selecione uma programação quebrada que não seja a primeira programação da quebra.", Alert.AlertType.WARNING);
            return;
        } else if (programacaoSelecionada.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || programacaoSelecionada.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Esta operação já está finalizada ou em produção e não pode ser alterada.", Alert.AlertType.WARNING);
            return;
        }
        // </editor-fold>
        
        if (programacaoSelecionada != null) {
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                            "Removendo quebra da operação " + programacaoSelecionada.getId().getOperacao() + " setor " + programacaoSelecionada.getId().getSetorOp() + " na ordem " + programacaoSelecionada.getId().getOrdem() + " quebra " + programacaoSelecionada.getId().getQuebra() +
                            " manualmente " +
                            " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
            
            //Inserindo no array para exclusão quando salvar
            operacoesParaExcluir.add(programacaoSelecionada);
            //Remove a operação gerada com a quebra na lista de programacoes
            operacoesParaProgramacao.remove(programacaoSelecionada);
            //Remove a operação gerada com a quebra na lista de programacoes do setor
            operacoesSetorSelecionado.remove(programacaoSelecionada);
            
            //Define a quantidade de cada programacao
            final IntegerProperty totalPecas = new SimpleIntegerProperty(pacoteSelecionado.get().getQtde());
            final IntegerProperty totalQuebras = new SimpleIntegerProperty((int) operacoesSetorSelecionado.stream()
                    .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                            operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem())
                    .count());
            final IntegerProperty qtdeQuebra = new SimpleIntegerProperty(((int) (totalPecas.get() / totalQuebras.get())) + (totalPecas.get() % totalQuebras.get() == 0 ? 0 : 1));
            
            operacoesSetorSelecionado.stream()
                    .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                            operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem()).forEach(programacao -> {
                programacao.setQtde(qtdeQuebra.get());
                programacao.setTempoTotal(BigDecimal.valueOf(qtdeQuebra.multiply(programacao.getTempoOp().doubleValue()).get()));
                totalPecas.set(totalPecas.subtract(qtdeQuebra.get()).get());
                if (totalPecas.lessThan(qtdeQuebra).get())
                    qtdeQuebra.set(totalPecas.get());
            });
            
            if (totalQuebras.get() == 1) {
                operacoesSetorSelecionado.stream()
                        .filter(operacao -> operacao.getId().getOperacao().getCodorg() == programacaoSelecionada.getId().getOperacao().getCodorg() &&
                                operacao.getId().getOrdem() == programacaoSelecionada.getId().getOrdem()).forEach(programacao -> programacao.setStatusProgramacao(StatusProgramacao.LIVRE));
            }
        }
        
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuExcluirColaboradorOnAction(ActionEvent event) {
        tblProgramacao.getSelectionModel().getSelectedItems().forEach(programacao -> {
            if ((!programacao.getStatusProgramacao().equals(StatusProgramacao.LANCADO) && !programacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) && programacao.getColaborador() != null) {
                removeProgramacaoOcupacaoColaborador(programacao, programacao.getColaborador());
                programacao.getColaboradorCelula().setDiasProg(programacao.getColaboradorCelula().getDiasProg().subtract(BigDecimal.valueOf((programacao.getQtde() * programacao.getTempoOpAgrupada().doubleValue()) / 528.0)));
                programacao.setColaboradorCelula(null);
                programacao.setColaborador(null);
                programacao.setSeqColaborador(999);
                if (programacao.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA)) {
                    for (SdProgramacaoPacote002 operacoesAgrupada : programacao.getOperacoesAgrupadas()) {
                        removeProgramacaoOcupacaoColaborador(operacoesAgrupada, operacoesAgrupada.getColaborador());
                        operacoesAgrupada.setColaboradorCelula(null);
                        operacoesAgrupada.setColaborador(null);
                        operacoesAgrupada.setCelula(null);
                        operacoesAgrupada.setSeqColaborador(999);
                    }
                }
                
                if (!programacao.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA) &&
                        !programacao.getStatusProgramacao().equals(StatusProgramacao.QUEBRADA))
                    programacao.setStatusProgramacao(StatusProgramacao.LIVRE);
            }
        });
        tblProgramacao.refresh();
        tblColaboradoresOperacao.refresh();
    }
    
    @FXML
    private void requestMenuLimparColaboradoresOnAction(ActionEvent event) {
        operacoesSetorSelecionado.forEach(programacao -> {
            if ((!programacao.getStatusProgramacao().equals(StatusProgramacao.LANCADO) && !programacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) && programacao.getColaborador() != null) {
                removeProgramacaoOcupacaoColaborador(programacao, programacao.getColaborador());
                programacao.getColaboradorCelula().setDiasProg(programacao.getColaboradorCelula().getDiasProg().subtract(BigDecimal.valueOf((programacao.getQtde() * programacao.getTempoOpAgrupada().doubleValue()) / 528.0)));
                programacao.setColaboradorCelula(null);
                programacao.setColaborador(null);
                programacao.setSeqColaborador(999);
                if (programacao.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA)) {
                    for (SdProgramacaoPacote002 operacoesAgrupada : programacao.getOperacoesAgrupadas()) {
                        removeProgramacaoOcupacaoColaborador(operacoesAgrupada, operacoesAgrupada.getColaborador());
                        operacoesAgrupada.setColaboradorCelula(null);
                        operacoesAgrupada.setColaborador(null);
                        operacoesAgrupada.setSeqColaborador(999);
                    }
                }
                if (!programacao.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA) &&
                        !programacao.getStatusProgramacao().equals(StatusProgramacao.QUEBRADA))
                    programacao.setStatusProgramacao(StatusProgramacao.LIVRE);
            }
        });
        tblProgramacao.refresh();
        tblColaboradoresOperacao.refresh();
    }
    
    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        if (setorOperacaoSelecionado == null || operacoesSetorSelecionado.size() <= 0) {
            GUIUtils.showMessage("Você deve selecionar o SETOR DE OPERAÇÃO", Alert.AlertType.INFORMATION);
            return;
        }
        
        celulaSelecionada.setSelected(false);
        GenericFilter<SdCelula> filterCelula = null;
        try {
            filterCelula = new GenericFilter<SdCelula>() {
            };
            filterCelula.show(ResultTypeFilter.SINGLE_RESULT);
            celulaSelecionada.copy(filterCelula.selectedReturn);
            //celulaSelecionada.setSelected(true);
            
            colaboradoresCelulaSelecionada.clear();
            colaboradoresCelulaSelecionada.set(FXCollections.observableList(colaboradoresParaProgramacao.get()
                    .stream()
                    .filter(colaborador -> colaborador.getCelula().getCodigo() == celulaSelecionada.getCodigo())
                    .collect(Collectors.toList())));
            
            if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 1) {
                SdProgramacaoPacote002 operacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
                verificaPolivalenciaProgramacao(operacaoSelecionada);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnProcurarCelulaSetorOnAction(ActionEvent event) {
        if (setorOperacaoSelecionado == null || operacoesSetorSelecionado.size() <= 0) {
            GUIUtils.showMessage("Você deve selecionar o SETOR DE OPERAÇÃO", Alert.AlertType.INFORMATION);
            return;
        }
        
        celulaSelecionada.setSelected(false);
        GenericFilter<SdCelula> filterCelula = null;
        try {
            filterCelula = new GenericFilter<SdCelula>() {
            };
            filterCelula.show(ResultTypeFilter.SINGLE_RESULT);
            celulaSetorSelecionada.copy(filterCelula.selectedReturn);
            //celulaSetorSelecionada.setSelected(true);
            celulaSetorOperacaoSelecionada = new SdCelula(celulaSetorSelecionada);
            operacoesSetorSelecionado.forEach(sdProgramacaoPacote002 -> sdProgramacaoPacote002.setCelula(celulaSetorOperacaoSelecionada));
            
            celulaSelecionada.copy(celulaSetorSelecionada);
            celulaSelecionada.setSelected(true);
            colaboradoresCelulaSelecionada.clear();
            colaboradoresCelulaSelecionada.set(FXCollections.observableList(colaboradoresParaProgramacao.get()
                    .stream()
                    .filter(colaborador -> colaborador.getCelula().getCodigo() == celulaSelecionada.getCodigo())
                    .collect(Collectors.toList())));
            
            if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 1) {
                SdProgramacaoPacote002 operacaoSelecionada = tblProgramacao.getSelectionModel().getSelectedItem();
                verificaPolivalenciaProgramacao(operacaoSelecionada);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarCelulaOnAction(null);
        }
    }
    
    @FXML
    private void tboxCelulaSetorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarCelulaSetorOnAction(null);
        }
    }
    
    @FXML
    private void requestMenuUsarEsteOnAction(ActionEvent event) {
        for (SdProgramacaoPacote002 selectedItem : tblProgramacao.getSelectionModel().getSelectedItems()) {
            VSdColabCelProg colaboradorSelecionado = tblColaboradoresOperacao.getSelectionModel().getSelectedItem();
            
            if (selectedItem.getColaborador() != null) {
                removeProgramacaoOcupacaoColaborador(selectedItem, selectedItem.getColaborador());
            }
            
            colaboradorSelecionado.setDiasProg(colaboradorSelecionado.getDiasProg().add(BigDecimal.valueOf((selectedItem.getQtde() * selectedItem.getTempoOpAgrupada().doubleValue()) / 528.0)));
            selectedItem.setColaboradorCelula(colaboradorSelecionado);
            selectedItem.setColaborador(colaboradorSelecionado.getColaborador());
            selectedItem.setSeqColaborador(ocupacaoColaborador.size() + 1);
            
            VSdProgramacaoPendente programacaoPendente = new VSdProgramacaoPendente(
                    ofParaProgramarSelecionada.get().getReferencia(),
                    colaboradorSelecionado.getColaborador().getCodigo(),
                    selectedItem);
            ocupacaoColaborador.add(programacaoPendente);
            
            if (selectedItem.getOperacoesAgrupadas().size() > 0) {
                for (SdProgramacaoPacote002 operacoesAgrupada : selectedItem.getOperacoesAgrupadas()) {
                    operacoesAgrupada.setCelula(celulaSetorOperacaoSelecionada);
                    operacoesAgrupada.setColaboradorCelula(colaboradorSelecionado);
                    operacoesAgrupada.setColaborador(colaboradorSelecionado.getColaborador());
                }
            } else if (selectedItem.getAgrupador() != 0) {
                for (SdProgramacaoPacote002 operacoesAgrupada : selectedItem.getOperacaoAgrupador().getOperacoesAgrupadas()) {
                    operacoesAgrupada.setCelula(celulaSetorOperacaoSelecionada);
                    operacoesAgrupada.setColaboradorCelula(colaboradorSelecionado);
                    operacoesAgrupada.setColaborador(colaboradorSelecionado.getColaborador());
                }
            }
            
            if (!selectedItem.getStatusProgramacao().equals(StatusProgramacao.AGRUPADA) &&
                    !selectedItem.getStatusProgramacao().equals(StatusProgramacao.QUEBRADA))
                selectedItem.setStatusProgramacao(StatusProgramacao.PROGRAMADO);
        }
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuAdicionarOperacaoOnAction(ActionEvent event) {
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        GenericFilter<SdOperacaoOrganize001> filterOperacao = null;
        try {
            filterOperacao = new GenericFilter<SdOperacaoOrganize001>() {
            };
            filterOperacao.show(ResultTypeFilter.SINGLE_RESULT);
            SdOperacaoOrganize001 operacaoSelecionada = filterOperacao.selectedReturn;
            SdProgramacaoPacote002 novaProgramacao = new SdProgramacaoPacote002(
                    pacoteSelecionado.get().getCodigo(),
                    setorOperacaoSelecionado,
                    operacaoSelecionada,
                    0,
                    1,
                    operacaoSelecionada.getEquipamentoOrganize(),
                    operacaoSelecionada.getTempo(),
                    pacoteSelecionado.get().getQtde(),
                    0,
                    tblProgramacao.getSelectionModel().getSelectedItem().getDtProgramacao(),
                    999,
                    BigDecimal.valueOf(pacoteSelecionado.get().getQtde() * operacaoSelecionada.getTempo().doubleValue()),
                    StatusProgramacao.LIVRE
            );
            
            int posicaoUltimaOperacao = operacoesSetorSelecionado.size() - 1;
            SdProgramacaoPacote002 ultimaProgramacaoSetorSelecionado = operacoesSetorSelecionado.get(posicaoUltimaOperacao);
//            do {
//                ultimaProgramacaoSetorSelecionado = operacoesSetorSelecionado.get(posicaoUltimaOperacao);
//                posicaoUltimaOperacao--;
//            } while (ultimaProgramacaoSetorSelecionado.getAgrupador() > 0);
            novaProgramacao.setCelula(ultimaProgramacaoSetorSelecionado.getCelula());
            novaProgramacao.getId().setOrdem(ultimaProgramacaoSetorSelecionado.getId().getOrdem()+1);
            novaProgramacao.setOrdemOriginal(novaProgramacao.getId().getOrdem());
            int posicaoUltimaOperacaoSetorSelecionado = operacoesParaProgramacao.indexOf(ultimaProgramacaoSetorSelecionado);
            operacoesParaProgramacao.add(posicaoUltimaOperacaoSetorSelecionado + 1,
                    novaProgramacao);
            Comparator<SdProgramacaoPacote002> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
            comparator = comparator.thenComparing(Comparator.comparing(programacao -> ((SdProgramacaoPacote002) programacao).getId().getQuebra()).reversed());
            operacoesParaProgramacao.sort(comparator);
            operacoesSetorSelecionado.sort(comparator);
            Integer menorOrdemSetorOperacao = operacoesSetorSelecionado.stream().mapToInt(operacao -> operacao.getOrdemOriginal()).min().getAsInt();
            for (int i = operacoesParaProgramacao.indexOf(operacoesSetorSelecionado.stream().findFirst().get()); i < operacoesParaProgramacao.size(); i++) {
                SdProgramacaoPacote002 operacaoProgramacao = operacoesParaProgramacao.get(i);
                if (operacaoProgramacao.getId().getQuebra() > 1) {
                    operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao);
                } else {
                    operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao++);
                }
            }
            Comparator<SdProgramacaoPacote002> comparatorReorder = Comparator.comparing(programacao -> programacao.getId().getOrdem());
            comparatorReorder = comparatorReorder.thenComparing(Comparator.comparing(programacao -> ((SdProgramacaoPacote002) programacao).getId().getQuebra()));
            operacoesParaProgramacao.sort(comparatorReorder);
            reordenarEncadeamentoObjetosProgramacao(null);
            
            selectSetorOperacao(setorOperacaoSelecionado);
            tblProgramacao.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void requestMenuExcluirOperacaoOnAction(ActionEvent event) {
        SdProgramacaoPacote002 selectProgramacao = tblProgramacao.getSelectionModel().getSelectedItem();
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() > 1) {
            GUIUtils.showMessage("Você pode remover somente uma operação, selecione somente uma linha.", Alert.AlertType.WARNING);
            return;
        }
        
        if (selectProgramacao.getStatusProgramacao().equals(StatusProgramacao.LANCADO) || selectProgramacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Você não pode excluir uma operações que já foi produzida.", Alert.AlertType.WARNING);
            return;
        }
        //</editor-fold>
        operacoesParaExcluir.add(selectProgramacao);
        operacoesParaProgramacao.remove(selectProgramacao);
        operacoesSetorSelecionado.remove(selectProgramacao);
        operacoesSetorSelecionadoParaProgramacao.remove(selectProgramacao);
        
        Integer menorOrdemSetorOperacao = operacoesParaProgramacao.stream().mapToInt(operacao -> operacao.getId().getOrdem()).min().getAsInt();
        for (SdProgramacaoPacote002 operacaoProgramacao : operacoesParaProgramacao) {
            operacaoProgramacao.getId().setOrdem(menorOrdemSetorOperacao++);
        }
        
        Comparator<SdProgramacaoPacote002> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
        comparator = comparator.thenComparing(Comparator.comparing(programacao -> programacao.getId().getQuebra()));
        operacoesParaProgramacao.sort(comparator);
        reordenarEncadeamentoObjetosProgramacao(null);
        
        tblProgramacao.refresh();
    }
    
    @FXML
    private void requestMenuLiberarOperacaoOnAction(ActionEvent event) {
        SdProgramacaoPacote002 selectProgramacao = tblProgramacao.getSelectionModel().getSelectedItem();
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() == 0)
            return;
        
        if (tblProgramacao.getSelectionModel().getSelectedItems().size() > 1) {
            GUIUtils.showMessage("Você pode liberar uma programação por vez.", Alert.AlertType.WARNING);
            return;
        }
        
        if (!selectProgramacao.getStatusProgramacao().equals(StatusProgramacao.FINALIZADO)) {
            GUIUtils.showMessage("Você pode liberar somente programações já finalizadas.", Alert.AlertType.WARNING);
            return;
        }
        
        if (selectProgramacao.getAgrupador() != 0) {
            GUIUtils.showMessage("Libere para produção somente a operação agrupadora.", Alert.AlertType.WARNING);
            return;
        }
        
        if (!GUIUtils.showQuestionMessageDefaultSim("Deseja realmente liberar a programação selecionada?\nEsse procedimento irá liberar a operação para ser realizada novamente."))
            return;
        //</editor-fold>
        
        selectProgramacao.setStatusProgramacao(StatusProgramacao.PROGRAMADO);
        selectProgramacao.setStatusProg(0);
        selectProgramacao.setOperacaoLiberada(true);
        selectProgramacao.setQtde(selectProgramacao.getQtde() * 2);
        tblProgramacao.refresh();
    }
    
    @FXML
    private void btnCancelarProgramacaoOnAction(ActionEvent event) {
        
        tboxOrdemProdPacoteProgramado.clear();
        listaDePacotesProgramados.clear();
        programacaoProducaoOf.set(null);
        ofParaProgramacao = null;
        totalPacotesOf.set(0);
        listaDePacotes.clear();
        listaDePacotesLancamento.clear();
        pacoteSelecionado.set(null);
        programacaoComLancametos.set(false);
        emProgramacao.set(false);
        emVisualizacao.set(false);
        emEdicao.set(false);
        roteiroOperacoesProduto = null;
        operacoesParaProgramacao.clear();
        setoresOperacao.clear();
        operacoesSetorSelecionado.clear();
        operacoesSetorSelecionadoParaProgramacao.clear();
        operacoesParaExcluir.clear();
        setorOperacaoSelecionado = null;
        celulaSelecionada.clear();
        celulaSetorSelecionada.clear();
        celulaSetorOperacaoSelecionada = null;
        colaboradorSemPolivalencia = null;
        colaboradoresParaProgramacao.clear();
        ocupacaoColaborador.clear();
        setoresComLancamento.clear();
        tblProgramacao.refresh();
        colaboradoresCelulaSelecionada.clear();
        operacoesAgrupadas.clear();
        tabsWindow.getSelectionModel().select(0);
        tabListagem.setDisable(false);
        tblOfParaProgramacao.refresh();
    }
    
    @FXML
    private void btnConfirmarProgramacaoOnAction(ActionEvent event) {
        
        //<editor-fold defaultstate="collapsed" desc="Validações">
//        if (hasNullOperador(operacoesParaProgramacao)) {
//            GUIUtils.showMessage("Existem operações sem colaborador, você deve inserir um operador para todas as operações do setor!", Alert.AlertType.WARNING);
//            return;
//        }
//        if (hasCelulaNull(operacoesParaProgramacao)) {
//            GUIUtils.showMessage("Existem operações sem célula definida, você deve inserir uma célula para todas as operações e setores!", Alert.AlertType.WARNING);
//            return;
//        }
        if (celulaSetorOperacaoSelecionada == null) {
            GUIUtils.showMessage("Você deve definir a célula responsável pelo setor de operação!", Alert.AlertType.WARNING);
            return;
        }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                            " Confirmando programação da OF " +
                            " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
            
            if (emEdicao.not().get()) {
                DAOFactory.getSdProgramacaoOf001DAO().save(programacaoProducaoOf.get());
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                "Persistindo OF programada (sd_programacao_of_001)" +
                                " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
                DAOFactory.getSdPacote001DAO().save(pacoteSelecionado.get());
                // <editor-fold defaultstate="collapsed" desc="[log register]">
                SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                "Persistindo pacote programado (sd_pacote_001) " + pacoteSelecionado.get().toString() +
                                " por " + SceneMainController.getAcesso().getUsuario(),
                        "PROGRAMAÇÃO OF");
                //</editor-fold>
                for (SdPacote001 pacoteLancamento : listaDePacotesLancamento) {
                    DAOFactory.getSdPacoteLancamento001DAO().save(pacoteLancamento);
                    // <editor-fold defaultstate="collapsed" desc="[log register]">
                    SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                    "Persistindo pacote de lançamento programado (sd_pacote_lancamento_001) " + pacoteLancamento.toString() +
                                    " por " + SceneMainController.getAcesso().getUsuario(),
                            "PROGRAMAÇÃO OF");
                    //</editor-fold>
                }
            } else {
                if (programacaoComLancametos.not().get()) {
                    DAOFactory.getSdPacoteLancamento001DAO().deleteByPacoteProg(pacoteSelecionado.get());
                    // <editor-fold defaultstate="collapsed" desc="[log register]">
                    SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                    "Deletanto pacote de lançamento (sd_pacote_lancamento_001) do pacote " + pacoteSelecionado.get().toString() +
                                    " por " + SceneMainController.getAcesso().getUsuario(),
                            "PROGRAMAÇÃO OF");
                    //</editor-fold>
                    for (SdPacote001 pacoteLancamento : listaDePacotesLancamento) {
                        DAOFactory.getSdPacoteLancamento001DAO().save(pacoteLancamento);
                        // <editor-fold defaultstate="collapsed" desc="[log register]">
                        SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                        "Persistindo pacote de lançamento programado (sd_pacote_lancamento_001) " + pacoteLancamento.toString() +
                                        " por " + SceneMainController.getAcesso().getUsuario(),
                                "PROGRAMAÇÃO OF");
                        //</editor-fold>
                    }
                }
                DAOFactory.getSdPacote001DAO().update(pacoteSelecionado.get());
                
                btnIniciarInativo.set(true);
            }
            
            for (VSdColabCelProg vSdColabCelProg : colaboradoresParaProgramacao) {
                if (vSdColabCelProg.getProgramacao().size() == 0)
                    continue;
                for (VSdProgramacaoPendente vSdProgramacaoPendente : vSdColabCelProg.getProgramacao()) {
                    DAOFactory.getSdProgramacaoPacote001DAO().update(vSdProgramacaoPendente.getProgramacao());
                }
            }
            
            for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesParaExcluir) {
                DAOFactory.getSdProgramacaoPacote001DAO().delete(sdProgramacaoPacote002);
            }
            
            for (SdProgramacaoPacote002 programacaoOperacao : operacoesParaProgramacao) {
                
                if (programacaoOperacao.getStatusProgramacao() == StatusProgramacao.LANCADO ||
                        programacaoOperacao.getStatusProgramacao() == StatusProgramacao.FINALIZADO ||
                        programacaoOperacao.isOperacaoLiberada()) {
                    DAOFactory.getSdProgramacaoPacote001DAO().update(programacaoOperacao);
                    // <editor-fold defaultstate="collapsed" desc="[log register]">
                    SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                    "Operação  " + programacaoOperacao.getId().getOperacao() + " setor " + programacaoOperacao.getId().getSetorOp() +
                                    " na ordem " + programacaoOperacao.getId().getOrdem() + " quebra " + programacaoOperacao.getId().getQuebra() +
                                    " colaborador " + programacaoOperacao.getColaborador() +
                                    " tempo total " + programacaoOperacao.getTempoTotal() + " tempo da operacao " + programacaoOperacao.getTempoOp() + " célula " + programacaoOperacao.getCelula() +
                                    " agrupador com " + programacaoOperacao.getAgrupador() + " pacote " + programacaoOperacao.getId().getPacote() + " - " +
                                    "Alterando operação já lançada " +
                                    " por " + SceneMainController.getAcesso().getUsuario(),
                            "PROGRAMAÇÃO OF");
                    //</editor-fold>
                } else {
                    if (emEdicao.get())
                        DAOFactory.getSdProgramacaoPacote001DAO().delete(programacaoOperacao);
                    DAOFactory.getSdProgramacaoPacote001DAO().save(programacaoOperacao);
                    // <editor-fold defaultstate="collapsed" desc="[log register]">
                    SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                                    "Operação  " + programacaoOperacao.getId().getOperacao() + " setor " + programacaoOperacao.getId().getSetorOp() +
                                    " na ordem " + programacaoOperacao.getId().getOrdem() + " quebra " + programacaoOperacao.getId().getQuebra() +
                                    " colaborador " + programacaoOperacao.getColaborador() +
                                    " tempo total " + programacaoOperacao.getTempoTotal() + " tempo da operacao " + programacaoOperacao.getTempoOp() + " célula " + programacaoOperacao.getCelula() +
                                    " agrupador com " + programacaoOperacao.getAgrupador() + " pacote " + programacaoOperacao.getId().getPacote() + " - " +
                                    "Persistindo operação (sd_programacao_pacote_001) programada " +
                                    " por " + SceneMainController.getAcesso().getUsuario(),
                            "PROGRAMAÇÃO OF");
                    //</editor-fold>
                }
                if (programacaoOperacao.getColaborador() != null &&
                        DAOFactory.getSdPolivalencia001DAO().getByPolivalencia(programacaoOperacao.getColaborador(), programacaoOperacao.getId().getOperacao()).size() == 0) {
                    SdPolivalencia001 polivalencia = new SdPolivalencia001(programacaoOperacao.getColaborador().getCodigo(), programacaoOperacao.getId().getOperacao().getCodorg(), 0, "S");
                    DAOFactory.getSdPolivalencia001DAO().forcarPolivalencia(polivalencia);
                }
            }
            
            
            if (GUIUtils.showQuestionMessageDefaultSim("Deseja imprimir o romaneio da programação realizada?")) {
                btnImprimirProgramacaoOnAction(null);
            }
            
            listaDePacotesLancamento.clear();
            listaDePacotes.remove(pacoteSelecionado.get());
            pacoteSelecionado.set(null);
            programacaoComLancametos.set(false);
            emProgramacao.set(false);
            roteiroOperacoesProduto = null;
            operacoesParaProgramacao.clear();
            setoresOperacao.clear();
            operacoesSetorSelecionado.clear();
            operacoesSetorSelecionadoParaProgramacao.clear();
            operacoesParaExcluir.clear();
            setorOperacaoSelecionado = null;
            celulaSelecionada.clear();
            celulaSetorSelecionada.clear();
            celulaSetorOperacaoSelecionada = null;
            colaboradorSemPolivalencia = null;
            colaboradoresParaProgramacao.clear();
            ocupacaoColaborador.clear();
            setoresComLancamento.clear();
            tblProgramacao.refresh();
            colaboradoresCelulaSelecionada.clear();
            operacoesAgrupadas.clear();
            
            if (listaDePacotes.size() > 0) {
                cboxPacote.requestFocus();
            } else {
                ofParaProgramarSelecionada.get().setProgramado(true);
                ofParaProgramarSelecionada.get().setStatusOf("G");
                ofParaProgramarSelecionada.get().setDtProg(LocalDateTime.parse(programacaoProducaoOf.get().getDataCad(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                this.btnCancelarProgramacaoOnAction(null);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnConfirmarProgramacaoOnAction");
            // <editor-fold defaultstate="collapsed" desc="[log register]">
            SysLogger.addFileLogProgramacaoOf("OF " + programacaoProducaoOf.get().getOrdemProd() + " - " +
                            "Erro ao persistir programação no banco " +
                            "EXCEPTION: " + ex.getMessage() +
                            " por " + SceneMainController.getAcesso().getUsuario(),
                    "PROGRAMAÇÃO OF");
            //</editor-fold>
        }
    }
    
    @FXML
    private void btnExcluirProgramacaoOnAction(ActionEvent event) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir a programação?")) {
            if (pacoteSelecionado != null) {
                try {
                    DAOFactory.getSdPacoteLancamento001DAO().deleteByPacoteProg(pacoteSelecionado.get());
                    DAOFactory.getSdPacote001DAO().delete(pacoteSelecionado.get());
                    
                    for (SdProgramacaoPacote002 sdProgramacaoPacote002 : operacoesParaProgramacao) {
                        DAOFactory.getSdProgramacaoPacote001DAO().delete(sdProgramacaoPacote002);
                    }
                    if (listaDePacotes.size() == 1) {
                        DAOFactory.getSdProgramacaoOf001DAO().deleteBySetor(programacaoProducaoOf.get());
                        
                        ofParaProgramarSelecionada.get().setProgramado(false);
                        ofParaProgramarSelecionada.get().setStatusOf("R");
                        ofParaProgramarSelecionada.get().setDtProg(null);
                        tblOfParaProgramacao.refresh();
                        this.btnCancelarProgramacaoOnAction(null);
                    } else {
                        listaDePacotes.remove(pacoteSelecionado.get());
                        pacoteSelecionado.set(null);
                        programacaoComLancametos.set(false);
                        emProgramacao.set(false);
                        emEdicao.set(false);
                        roteiroOperacoesProduto = null;
                        operacoesParaProgramacao.clear();
                        setoresOperacao.clear();
                        operacoesSetorSelecionado.clear();
                        operacoesSetorSelecionadoParaProgramacao.clear();
                        setorOperacaoSelecionado = null;
                        celulaSelecionada.clear();
                        celulaSetorSelecionada.clear();
                        celulaSetorOperacaoSelecionada = null;
                        colaboradorSemPolivalencia = null;
                        colaboradoresParaProgramacao.clear();
                        ocupacaoColaborador.clear();
                        setoresComLancamento.clear();
                        tblProgramacao.refresh();
                        colaboradoresCelulaSelecionada.clear();
                        operacoesAgrupadas.clear();
                        tabsWindow.getSelectionModel().select(0);
                        tabListagem.setDisable(false);
                        tblOfParaProgramacao.refresh();
                        cboxPacote.requestFocus();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    Logger.getLogger(SceneProgramaProducaoOfControllerNew.class.getName()).log(Level.SEVERE, null, ex);
                    LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnExcluirProgramacaoOnAction");
                    GUIUtils.showException(ex);
                }
            }
        }
    }
    
    @FXML
    private void btnImprimirProgramacaoOnAction(ActionEvent event) {
        try {
            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoProducaoOf.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            
            parameters.put("pacote", pacoteSelecionado.get().getCodigo());
            parameters.put("setor", programacaoProducaoOf.get().getSetor());
            
            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);
            
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    
    @FXML
    private void btnLimparPacoteProgramadoOnAction(ActionEvent event) {
        cboxPacoteProgramado.getSelectionModel().clearSelection();
        tboxOrdemProdPacoteProgramado.clear();
    }
    
    // -------------------------------------  FILTROS ---------------------------------------------
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxFiltroProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxFiltroProduto.setText(ctrolFilterProduto.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarProdutoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarPeriodoOnAction(ActionEvent event) {
        try {
            FilterPeriodoController ctrolFilter = new FilterPeriodoController(Modals.FXMLWindow.FilterPeriodo, tboxFiltroPeriodo.getText());
            if (!ctrolFilter.returnValue.isEmpty()) {
                tboxFiltroPeriodo.setText(ctrolFilter.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarPeriodoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
        try {
            FilterFamiliaController ctrolFilterColecao = new FilterFamiliaController(Modals.FXMLWindow.FilterFamilia, tboxFiltroFamilia.getText());
            if (!ctrolFilterColecao.returnValue.isEmpty()) {
                tboxFiltroFamilia.setText(ctrolFilterColecao.returnValue);
                
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarFamiliaOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML
    private void btnCarregarOfsOnAction(ActionEvent event) {
        
        String whereDataEnvio = "";
        if (tboxDtInicioEnvio.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioEnvio.getValue();
            LocalDate dtFim = tboxDtFimEnvio.getValue() == null ? tboxDtInicioEnvio.getValue() : tboxDtFimEnvio.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataEnvio += "and dt_env between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String whereDataRetorno = "";
        if (tboxDtInicioRetorno.getValue() != null || tboxDtFimRetorno.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioRetorno.getValue();
            LocalDate dtFim = tboxDtFimRetorno.getValue() == null ? tboxDtInicioRetorno.getValue() : tboxDtFimRetorno.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataRetorno += "and dt_ret between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String whereDataProgramacao = "";
        if (tboxDtInicioProgramacao.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioProgramacao.getValue();
            LocalDate dtFim = tboxDtFimProgramacao.getValue() == null ? tboxDtInicioProgramacao.getValue() : tboxDtFimProgramacao.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataProgramacao += "and dt_prog between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        String filterSomenteNaoProgramadas = toggleSomenteNaoProgramadas.isSwitchedOn.get() ? "and dt_prog is null\n" : "";
        
        String filterStatusValue = ""
                .concat(toggleGreen.switchedOnProperty().get() ? "'G'," : "")
                .concat(toggleRed.switchedOnProperty().get() ? "'R'," : "")
                .concat(toggleYellow.switchedOnProperty().get() ? "'Y'," : "")
                .concat(toggleBlue.switchedOnProperty().get() ? "'B'," : "");
        filterStatusValue = filterStatusValue.length() > 0 ? "and status_of in (" + filterStatusValue.substring(0, filterStatusValue.length() - 1) + ")\n" : "";
        
        String wheresValues = "";
        wheresValues += filterStatusValue;
        wheresValues += filterSomenteNaoProgramadas;
        wheresValues += tboxFiltroProduto.getLength() > 0 ? "and referencia in (" + tboxFiltroProduto.getText() + ")\n" : "";
        wheresValues += tboxFiltroFaccao.getLength() > 0 ? "and faccao like '%" + tboxFiltroFaccao.getText().toUpperCase() + "%'\n" : "";
        wheresValues += tboxFiltroFamilia.getLength() > 0 ? "and codfam in (" + tboxFiltroFamilia.getText() + ")\n" : "";
        wheresValues += tboxFiltroOf.getLength() > 0 ? "and op in (" + tboxFiltroOf.getText() + ")\n" : "";
        wheresValues += tboxFiltroPeriodo.getLength() > 0 ? "and periodo in (" + tboxFiltroPeriodo.getText() + ")\n" : "";
        wheresValues += tboxFiltroSetor.getLength() > 0 ? "and setor in (" + tboxFiltroSetor.getText() + ")\n" : "";
        wheresValues += whereDataEnvio.length() > 0 ? whereDataEnvio : "";
        wheresValues += whereDataRetorno.length() > 0 ? whereDataRetorno : "";
        wheresValues += whereDataProgramacao.length() > 0 ? whereDataProgramacao : "";
        
        try {
            ofsParaProgramar.set(DAOFactory.getVSdOfsParaProgramarDAO().getOfsParaProgramarFromFilter("PLANALTO", wheresValues));
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfControllerNew.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnCarregarOfsOnAction");
            GUIUtils.showException(ex);
        }
        
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroFamilia.clear();
        tboxFiltroOf.clear();
        tboxFiltroPeriodo.clear();
        tboxFiltroProduto.clear();
        tboxFiltroSetor.clear();
        tboxFiltroFaccao.clear();
        
        tboxDtFimEnvio.setValue(null);
        tboxDtInicioEnvio.setValue(null);
        tboxDtFimRetorno.setValue(null);
        tboxDtInicioRetorno.setValue(null);
        tboxDtFimProgramacao.setValue(null);
        tboxDtInicioProgramacao.setValue(null);
        
        toggleRed.switchedOnProperty().set(true);
        toggleYellow.switchedOnProperty().set(true);
        toggleBlue.switchedOnProperty().set(false);
        toggleGreen.switchedOnProperty().set(true);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Class TABLECELL EDICAO COLUNA">
    @Deprecated
    private class EditingCellClnString extends TableCell<SdProgramacaoPacote001, String> {
        
        private TextField textField;
        
        public EditingCellClnString() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
    
    @Deprecated
    private class EditingCellClnColaborador extends TableCell<SdProgramacaoPacote001, SdColaborador> {
        
        private TextField textField;
        
        public EditingCellClnColaborador() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(getString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(SdColaborador item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setEditable(false);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.F4) {
                        try {
                            ObservableList<SdColaborador> sendSelected = FXCollections.observableArrayList();
                            if (getItem() != null) {
                                sendSelected.add(getItem());
                            }
                            FilterColaboradorController ctrlFilter = new FilterColaboradorController(
                                    Modals.FXMLWindow.FilterColaborador, sendSelected,
                                    EnumsController.ResultTypeFilter.SINGLE_RESULT);
                            if (!ctrlFilter.selectedObjects.isEmpty()) {
                                setItem(ctrlFilter.selectedObjects.get(0));
                                
                                commitEdit(getItem());
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            Logger.getLogger(SceneCadastroColaboradorController.class.getName()).log(Level.SEVERE, null,
                                    ex);
                            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "ProgramacaoOF:EditingCellClnColaborador");
                            GUIUtils.showException(ex);
                        }
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private String getString() {
            return getItem() == null ? "" : "[" + getItem().getCodigo() + "] " + getItem().getNome();
        }
    }
    
    private class EditingCellClnInteger extends TableCell<SdProgramacaoPacote002, Integer> {
        
        private TextField textField;
        
        public EditingCellClnInteger() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(StringUtils.toIntegerFormat(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(StringUtils.toIntegerFormat(getString()));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(StringUtils.toIntegerFormat(getString()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(StringUtils.toIntegerFormat(getString()));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Integer.parseInt(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private Integer getString() {
            return getItem() == null ? 0 : getItem();
        }
    }
    
    private class EditingCellClnBigDecimal extends TableCell<SdProgramacaoPacote002, BigDecimal> {
        
        private TextField textField;
        
        public EditingCellClnBigDecimal() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(StringUtils.toDecimalFormat(getItem().doubleValue(), 4));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(BigDecimal item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(StringUtils.toDecimalFormat(getString().doubleValue(), 4));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(StringUtils.toDecimalFormat(getString().doubleValue(), 4));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(StringUtils.toDecimalFormat(getString().doubleValue(), 4));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(BigDecimal.valueOf(Double.parseDouble(textField.getText().replace(",", "."))));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private BigDecimal getString() {
            return getItem() == null ? BigDecimal.ZERO : getItem();
        }
    }
    
    @Deprecated
    private class EditingCellClnPercent extends TableCell<SdProgramacaoPacote001, Double> {
        
        private TextField textField;
        
        public EditingCellClnPercent() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(StringUtils.toPercentualFormat(getItem(), 2));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(StringUtils.toPercentualFormat(getString(), 2));
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(StringUtils.toPercentualFormat(getString(), 2));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(StringUtils.toPercentualFormat(getString(), 2));
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private Double getString() {
            return getItem() == null ? 0.0 : getItem();
        }
    }
    
    @Deprecated
    private class EditingCellClnDateTime extends TableCell<SdProgramacaoPacote001, LocalDateTime> {
        
        private TextField textField;
        
        public EditingCellClnDateTime() {
        }
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            if (textField == null) {
                createTextField();
            }
            
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            
            setText(getString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        @Override
        public void updateItem(LocalDateTime item, boolean empty) {
            super.updateItem(item, empty);
            
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
        
        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            MaskTextField.dateTimeField(textField);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                
                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(setDateTime(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }
        
        private LocalDateTime setDateTime(String dateTime) {
            return LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        }
        
        private String getString() {
            return getItem() == null ? "" : getItem().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        }
    }
//</editor-fold>
}
