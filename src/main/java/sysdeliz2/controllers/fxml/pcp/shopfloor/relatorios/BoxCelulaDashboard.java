package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import eu.hansolo.medusa.*;
import javafx.animation.AnimationTimer;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.sys.LocalLogger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BoxCelulaDashboard {
    
    private final AnchorPane boxAnchorCelula = new AnchorPane();
    private final VBox vboxCelula = new VBox();
    
    private final HBox hboxDadosCelula = new HBox();
    private final Label lbTitleCelula = new Label();
    private final Label lbDescricaoCelula = new Label();
    
    private final VBox vboxEficiencia = new VBox();
    private final Label lbTitleEficiencia = new Label();
    private final HBox boxEficiencia = new HBox();
    
    private final VBox vboxAproveitamento = new VBox();
    private final Label lbTitleAproveitamento = new Label();
    private final HBox boxAproveitamento = new HBox();
    
    private final HBox hboxBottonBoxCelula = new HBox();
    
    private final VBox vboxTempoParado = new VBox();
    private final Label lbTitleTempoParado = new Label();
    private final HBox hboxTempoParado = new HBox();
    private final Pane pnLcdTempoParado = new Pane();
    private final Label lbTitleComParada = new Label();
    private final Label lbParadoAgora = new Label();
    private final Label lbTitleTotalParadas = new Label();
    private final Label lbTotalParadas = new Label();
    
    private final VBox vboxTempoProdExtra = new VBox();
    private final Label lbTitleTempoProdExtra = new Label();
    private final Pane pnLcdTempoProdExtra = new Pane();
    private final HBox hboxTempoProdExtra = new HBox();
    
    private final VBox vboxGaugeEficDia = new VBox();
    private final Label lbValuesEficDia = new Label();
    private Gauge eficienciaDia;
    private Gauge eficienciaMes;
    private Gauge eficienciaAno;
    private final VBox vboxGaugeAprovDia = new VBox();
    private final Label lbValuesAprovDia = new Label();
    private Gauge aproveitamentoDia;
    private Gauge aproveitamentoMes;
    private Gauge aproveitamentoAno;
    private Gauge tempoParado;
    private Gauge qtdeColabsCelula;
    private long lastTimerCall;
    private AnimationTimer timer;
    
    private static final Random RND = new Random();
    
    private void initComponentesFxml() {
        
        this.initGauges();
        
        boxAnchorCelula.setPrefHeight(352.0);
        boxAnchorCelula.setPrefWidth(350.0);
        boxAnchorCelula.setStyle("-fx-border-color: #C0C0C0; -fx-border-radius: 3;");
        
        AnchorPane.setBottomAnchor(vboxCelula, 0.0);
        AnchorPane.setLeftAnchor(vboxCelula, 0.0);
        AnchorPane.setRightAnchor(vboxCelula, 0.0);
        AnchorPane.setTopAnchor(vboxCelula, 0.0);
        vboxCelula.setLayoutX(4.0);
        vboxCelula.setLayoutY(4.0);
        vboxCelula.setPrefHeight(343.0);
        vboxCelula.setPrefWidth(350.0);
        vboxCelula.setSpacing(5.0);
        
        hboxDadosCelula.setPrefHeight(0.0);
        hboxDadosCelula.setPrefWidth(350.0);
        hboxDadosCelula.setSpacing(3.0);
        hboxDadosCelula.setStyle("-fx-background-color: white;");
        
        lbTitleCelula.setText("Célula:");
        lbTitleCelula.setFont(new Font(14.0));
        
        HBox.setHgrow(lbDescricaoCelula, javafx.scene.layout.Priority.ALWAYS);
        lbDescricaoCelula.setPrefHeight(20.0);
        lbDescricaoCelula.setPrefWidth(290.0);
        lbDescricaoCelula.setFont(new Font("System Bold", 14.0));
        hboxDadosCelula.setPadding(new Insets(0.0, 0.0, 0.0, 2.0));
        
        vboxEficiencia.setPrefHeight(123.0);
        vboxEficiencia.setPrefWidth(364.0);
        
        lbTitleEficiencia.getStyleClass().add("form-field");
        lbTitleEficiencia.setText("Eficiência");
        
        boxEficiencia.setPrefHeight(117.0);
        boxEficiencia.setPrefWidth(359.0);
        boxEficiencia.setSpacing(3.0);
        boxEficiencia.setStyle("-fx-background-color: white;");
        boxEficiencia.getStyleClass().add("corner-up-left");
        
        vboxAproveitamento.setLayoutX(15.0);
        vboxAproveitamento.setLayoutY(40.0);
        vboxAproveitamento.setPrefHeight(123.0);
        vboxAproveitamento.setPrefWidth(173.0);
        
        lbTitleAproveitamento.getStyleClass().add("form-field");
        lbTitleAproveitamento.setText("Produtividade");
        
        boxAproveitamento.setPrefHeight(100.0);
        boxAproveitamento.setPrefWidth(200.0);
        boxAproveitamento.setSpacing(3.0);
        boxAproveitamento.setStyle("-fx-background-color: white;");
        boxAproveitamento.getStyleClass().add("corner-up-left");
        
        hboxBottonBoxCelula.setPrefHeight(56.0);
        hboxBottonBoxCelula.setPrefWidth(359.0);
        hboxBottonBoxCelula.setSpacing(5.0);
        
        vboxTempoParado.setPrefHeight(61.0);
        vboxTempoParado.setPrefWidth(255.0);
        
        lbTitleTempoParado.getStyleClass().add("form-field");
        lbTitleTempoParado.setText("Paradas do Dia");
        
        hboxTempoParado.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        hboxTempoParado.setSpacing(3.0);
        hboxTempoParado.setPrefHeight(100.0);
        hboxTempoParado.setPrefWidth(255.0);
        hboxTempoParado.setStyle("-fx-background-color: white;");
        hboxTempoParado.getStyleClass().add("corner-up-left");
        
        pnLcdTempoParado.setPrefHeight(30.0);
        pnLcdTempoParado.setPrefWidth(70.0);
        pnLcdTempoParado.setPadding(new Insets(3.0));
        
        lbTitleTotalParadas.setPrefHeight(30.0);
        lbTitleTotalParadas.setPrefWidth(35.0);
        lbTitleTotalParadas.setText("Total:");
        lbTitleTotalParadas.setWrapText(true);
        
        lbTotalParadas.setAlignment(javafx.geometry.Pos.CENTER);
        lbTotalParadas.setPrefHeight(52.0);
        lbTotalParadas.setPrefWidth(37.0);
        lbTotalParadas.setStyle("-fx-text-fill: white;");
        lbTotalParadas.getStyleClass().add("secundary");
        lbTotalParadas.setFont(new Font("System Bold", 14.0));
        
        lbTitleComParada.setPrefHeight(30.0);
        lbTitleComParada.setPrefWidth(59.0);
        lbTitleComParada.setText("C/ Parada:");
        lbTitleComParada.setWrapText(true);
        
        lbParadoAgora.setAlignment(javafx.geometry.Pos.CENTER);
        lbParadoAgora.setPrefHeight(73.0);
        lbParadoAgora.setPrefWidth(42.0);
        lbParadoAgora.setStyle("-fx-text-fill: white;");
        lbParadoAgora.setFont(new Font("System Bold", 14.0));
        hboxTempoParado.setPadding(new Insets(3.0));
        
        vboxTempoProdExtra.setPrefHeight(61.0);
        vboxTempoProdExtra.setPrefWidth(80.0);
        
        lbTitleTempoProdExtra.getStyleClass().add("form-field");
        lbTitleTempoProdExtra.setText("Operação");
        
        hboxTempoProdExtra.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        hboxTempoProdExtra.setPrefHeight(100.0);
        hboxTempoProdExtra.setPrefWidth(100.0);
        hboxTempoProdExtra.setStyle("-fx-background-color: white;");
        hboxTempoProdExtra.getStyleClass().add("corner-up-left");
        
        HBox.setHgrow(pnLcdTempoProdExtra, javafx.scene.layout.Priority.ALWAYS);
        pnLcdTempoProdExtra.setPrefHeight(25.0);
        pnLcdTempoProdExtra.setPrefWidth(70.0);
        pnLcdTempoProdExtra.setPadding(new Insets(3.0));
        hboxTempoProdExtra.setPadding(new Insets(3.0));
        
        vboxCelula.setPadding(new Insets(5.0));
        
        vboxGaugeEficDia.setAlignment(javafx.geometry.Pos.CENTER);
        lbValuesEficDia.setFont(new Font("System", 10.0));
        lbValuesEficDia.textProperty().bind(Bindings.format("Min.: %,.1f Max.: %,.1f", eficienciaDia.minMeasuredValueProperty(), eficienciaDia.maxMeasuredValueProperty()));
        vboxGaugeEficDia.getChildren().add(eficienciaDia);
        vboxGaugeEficDia.getChildren().add(lbValuesEficDia);
        boxEficiencia.getChildren().add(vboxGaugeEficDia);
        boxEficiencia.getChildren().add(eficienciaMes);
        boxEficiencia.getChildren().add(eficienciaAno);
        
        vboxGaugeAprovDia.setAlignment(javafx.geometry.Pos.CENTER);
        lbValuesAprovDia.setFont(new Font("System", 10.0));
        lbValuesAprovDia.textProperty().bind(Bindings.format("Min.: %,.1f Max.: %,.1f", aproveitamentoDia.minMeasuredValueProperty(), aproveitamentoDia.maxMeasuredValueProperty()));
        vboxGaugeAprovDia.getChildren().add(aproveitamentoDia);
        vboxGaugeAprovDia.getChildren().add(lbValuesAprovDia);
        boxAproveitamento.getChildren().add(vboxGaugeAprovDia);
        boxAproveitamento.getChildren().add(aproveitamentoMes);
        boxAproveitamento.getChildren().add(aproveitamentoAno);
        
        pnLcdTempoParado.getChildren().add(tempoParado);
        pnLcdTempoProdExtra.getChildren().add(qtdeColabsCelula);
        
        hboxDadosCelula.getChildren().add(lbTitleCelula);
        hboxDadosCelula.getChildren().add(lbDescricaoCelula);
        vboxCelula.getChildren().add(hboxDadosCelula);
        vboxEficiencia.getChildren().add(lbTitleEficiencia);
        vboxEficiencia.getChildren().add(boxEficiencia);
        vboxCelula.getChildren().add(vboxEficiencia);
        vboxAproveitamento.getChildren().add(lbTitleAproveitamento);
        vboxAproveitamento.getChildren().add(boxAproveitamento);
        vboxCelula.getChildren().add(vboxAproveitamento);
        vboxTempoParado.getChildren().add(lbTitleTempoParado);
        hboxTempoParado.getChildren().add(pnLcdTempoParado);
        hboxTempoParado.getChildren().add(lbTitleTotalParadas);
        hboxTempoParado.getChildren().add(lbTotalParadas);
        hboxTempoParado.getChildren().add(lbTitleComParada);
        hboxTempoParado.getChildren().add(lbParadoAgora);
        vboxTempoParado.getChildren().add(hboxTempoParado);
        hboxBottonBoxCelula.getChildren().add(vboxTempoParado);
        vboxTempoProdExtra.getChildren().add(lbTitleTempoProdExtra);
        hboxTempoProdExtra.getChildren().add(pnLcdTempoProdExtra);
        vboxTempoProdExtra.getChildren().add(hboxTempoProdExtra);
        hboxBottonBoxCelula.getChildren().add(vboxTempoProdExtra);
        vboxCelula.getChildren().add(hboxBottonBoxCelula);
        boxAnchorCelula.getChildren().add(vboxCelula);
    }
    
    private void initGauges() {
        eficienciaDia = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        eficienciaMes = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        eficienciaAno = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        aproveitamentoDia = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        aproveitamentoMes = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        aproveitamentoAno = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
        
        tempoParado = GaugeBuilder.create()
                .skinType(Gauge.SkinType.LCD)
                .maxMeasuredValueVisible(false)
                .majorTickMarksVisible(false)
                .mediumTickMarksVisible(false)
                .minMeasuredValueVisible(false)
                .minorTickMarksVisible(false)
                .oldValueVisible(false)
                .onlyFirstAndLastTickLabelVisible(false)
                .oldValueVisible(false)
                .maxHeight(30.0)
                .minHeight(25.0)
                .maxWidth(70.0)
                .minWidth(70.0)
                .decimals(0)
                .unit("min")
                .lcdDesign(LcdDesign.YELLOW_BLACK)
                .lcdFont(LcdFont.STANDARD)
                .animated(true)
                .build();
        
        qtdeColabsCelula = GaugeBuilder.create()
                .skinType(Gauge.SkinType.LCD)
                .maxMeasuredValueVisible(false)
                .majorTickMarksVisible(false)
                .mediumTickMarksVisible(false)
                .minMeasuredValueVisible(false)
                .minorTickMarksVisible(false)
                .oldValueVisible(false)
                .onlyFirstAndLastTickLabelVisible(false)
                .oldValueVisible(false)
                .maxHeight(30.0)
                .minHeight(25.0)
                .maxWidth(70.0)
                .minWidth(70.0)
                .decimals(0)
                .unit("colabs")
                .lcdDesign(LcdDesign.GREEN_BLACK)
                .lcdFont(LcdFont.STANDARD)
                .animated(true)
                .build();
        
    }
    
    private void atualizaDados(int celula) {
        
        try {
            
            Map<String, Object> dadosDia = DAOFactory.getDashboardProducaoDAO().getProducaoDia(celula);
            Map<String, Object> dadosMes = DAOFactory.getDashboardProducaoDAO().getProducaoMes(celula);
            Map<String, Object> dadosAno = DAOFactory.getDashboardProducaoDAO().getProducaoAno(celula);
            
            if(!dadosDia.isEmpty()) {
                eficienciaDia.setValue((double) dadosDia.get("efic"));
                eficienciaMes.setValue(dadosMes.size() > 0 ? (double) dadosMes.get("efic") : (double) dadosDia.get("efic"));
                eficienciaAno.setValue(dadosAno.size() > 0 ? (double) dadosAno.get("efic") : dadosMes.size() > 0 ? (double) dadosMes.get("efic") : (double) dadosDia.get("efic"));
                aproveitamentoDia.setValue((double) dadosDia.get("prod"));
                aproveitamentoMes.setValue(dadosMes.size() > 0 ? (double) dadosMes.get("prod") : (double) dadosDia.get("prod"));
                aproveitamentoAno.setValue(dadosAno.size() > 0 ? (double) dadosAno.get("prod") : dadosMes.size() > 0 ? (double) dadosMes.get("prod") : (double) dadosDia.get("prod"));
                tempoParado.setValue((double) dadosDia.get("tmp_parado"));
                qtdeColabsCelula.setValue((int) dadosDia.get("qtde_colab"));
                lbTotalParadas.setText(String.valueOf((int) dadosDia.get("tot_paradas")));
    
                eficienciaDia.setTitle((String) dadosDia.get("dia"));
                eficienciaMes.setTitle((String) dadosMes.get("mes"));
                eficienciaAno.setTitle((String) dadosAno.get("ano"));
                aproveitamentoDia.setTitle((String) dadosDia.get("dia"));
                aproveitamentoMes.setTitle((String) dadosMes.get("mes"));
                aproveitamentoAno.setTitle((String) dadosAno.get("ano"));
    
                if ((int) dadosDia.get("par_agora") == 0) {
                    lbParadoAgora.setText("NÃO");
                    lbParadoAgora.getStyleClass().remove("danger");
                    lbParadoAgora.getStyleClass().remove("warning");
                    lbParadoAgora.getStyleClass().add("success");
                } else if ((int) dadosDia.get("par_agora") == 3) {
                    lbParadoAgora.setText("MEC");
                    lbParadoAgora.getStyleClass().remove("warning");
                    lbParadoAgora.getStyleClass().remove("success");
                    lbParadoAgora.getStyleClass().add("danger");
                } else {
                    lbParadoAgora.setText("SIM");
                    lbParadoAgora.getStyleClass().remove("danger");
                    lbParadoAgora.getStyleClass().remove("success");
                    lbParadoAgora.getStyleClass().add("warning");
                }
    
                if (eficienciaDia.getValue() < 80) {
                    boxAnchorCelula.getStyleClass().remove("success");
                    boxAnchorCelula.getStyleClass().remove("warning");
                    boxAnchorCelula.getStyleClass().add("danger");
                } else if (eficienciaDia.getValue() >= 80 && eficienciaDia.getValue() < 90) {
                    boxAnchorCelula.getStyleClass().remove("success");
                    boxAnchorCelula.getStyleClass().add("warning");
                    boxAnchorCelula.getStyleClass().remove("danger");
                } else {
                    boxAnchorCelula.getStyleClass().add("success");
                    boxAnchorCelula.getStyleClass().remove("warning");
                    boxAnchorCelula.getStyleClass().remove("danger");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    private void atualizaDadosColaborador(int colaborador) {
        
        try {
            
            Map<String, Object> dadosDia = DAOFactory.getDashboardProducaoDAO().getProducaoColaboradorDia(colaborador);
            Map<String, Object> dadosMes = DAOFactory.getDashboardProducaoDAO().getProducaoColaboradorMes(colaborador);
            Map<String, Object> dadosAno = DAOFactory.getDashboardProducaoDAO().getProducaoColaboradorAno(colaborador);
            
            eficienciaDia.setValue((double) dadosDia.get("efic"));
            eficienciaMes.setValue(dadosMes.size() > 0 ? (double) dadosMes.get("efic") : (double) dadosDia.get("efic"));
            eficienciaAno.setValue(dadosAno.size() > 0 ? (double) dadosAno.get("efic") : dadosMes.size() > 0 ? (double) dadosMes.get("efic") : (double) dadosDia.get("efic"));
            aproveitamentoDia.setValue((double) dadosDia.get("prod"));
            aproveitamentoMes.setValue(dadosMes.size() > 0 ? (double) dadosMes.get("prod") : (double) dadosDia.get("prod"));
            aproveitamentoAno.setValue(dadosAno.size() > 0 ? (double) dadosAno.get("prod") : dadosMes.size() > 0 ? (double) dadosMes.get("prod") : (double) dadosDia.get("prod"));
            tempoParado.setValue((double) dadosDia.get("tmp_parado"));
            qtdeColabsCelula.setValue((int) dadosDia.get("med_opers"));
            qtdeColabsCelula.setUnit("opers");
            lbTotalParadas.setText(String.valueOf((int) dadosDia.get("tot_paradas")));
            
            eficienciaDia.setTitle((String) dadosDia.get("dia"));
            eficienciaMes.setTitle((String) dadosMes.get("mes"));
            eficienciaAno.setTitle((String) dadosAno.get("ano"));
            aproveitamentoDia.setTitle((String) dadosDia.get("dia"));
            aproveitamentoMes.setTitle((String) dadosMes.get("mes"));
            aproveitamentoAno.setTitle((String) dadosAno.get("ano"));
            
            if ((int) dadosDia.get("par_agora") == 0) {
                lbParadoAgora.setText("NÃO");
                lbParadoAgora.getStyleClass().remove("danger");
                lbParadoAgora.getStyleClass().remove("warning");
                lbParadoAgora.getStyleClass().add("success");
            } else if ((int) dadosDia.get("par_agora") == 3) {
                lbParadoAgora.setText("MEC");
                lbParadoAgora.getStyleClass().remove("warning");
                lbParadoAgora.getStyleClass().remove("success");
                lbParadoAgora.getStyleClass().add("danger");
            } else {
                lbParadoAgora.setText("SIM");
                lbParadoAgora.getStyleClass().remove("danger");
                lbParadoAgora.getStyleClass().remove("success");
                lbParadoAgora.getStyleClass().add("warning");
            }
            
            if (eficienciaDia.getValue() < 80) {
                boxAnchorCelula.getStyleClass().remove("success");
                boxAnchorCelula.getStyleClass().remove("warning");
                boxAnchorCelula.getStyleClass().add("danger");
            } else if (eficienciaDia.getValue() >= 80 && eficienciaDia.getValue() < 90) {
                boxAnchorCelula.getStyleClass().remove("success");
                boxAnchorCelula.getStyleClass().add("warning");
                boxAnchorCelula.getStyleClass().remove("danger");
            } else {
                boxAnchorCelula.getStyleClass().add("success");
                boxAnchorCelula.getStyleClass().remove("warning");
                boxAnchorCelula.getStyleClass().remove("danger");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    public BoxCelulaDashboard(SdCelula celula) {
        
        this.initComponentesFxml();
        
        lbDescricaoCelula.setText(celula.getDescricao() + "/" + celula.getLider().getNome());
        lbDescricaoCelula.setOnMouseClicked(event -> {
            if(event.getClickCount() >= 2){
                try {
                    FXMLLoader root = new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/pcp/shopfloor/relatorios/SceneDashboardColaboradores.fxml"));
                    root.setController(new SceneDashboardColaboradoresController(celula.getCodigo()));
                    Stage dashboardStage = new Stage();
                    Scene scene;
                    scene = new Scene(root.load());
                    scene.getStylesheets().add("/styles/bootstrap2.css");
                    scene.getStylesheets().add("/styles/bootstrap3.css");
                    scene.getStylesheets().add("/styles/styles.css");
    
                    Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
                    dashboardStage.getIcons().add(applicationIcon);
                    dashboardStage.setTitle("SysDeliz 2");
                    dashboardStage.setScene(scene);
                    dashboardStage.initStyle(StageStyle.DECORATED);
                    dashboardStage.setOnCloseRequest((WindowEvent arg0) -> {
                        dashboardStage.close();
                    });
                    dashboardStage.show();
                } catch (IOException ex) {
                    Logger.getLogger(SceneMainController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            }
        });
        atualizaDados(celula.getCodigo());
        
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override
            public void handle(final long now) {
                if (now > lastTimerCall + 120_000_000_000l) {
                    atualizaDados(celula.getCodigo());
                    lastTimerCall = now;
                }
            }
        };
        timer.start();
    }
    
    public BoxCelulaDashboard(SdColaborador colaborador) {
        
        this.initComponentesFxml();
        lbTitleCelula.setText("Colab.:");
        
        lbDescricaoCelula.setText("[" + colaborador.getCodigo() + "] " + colaborador.getNome());
        atualizaDadosColaborador(colaborador.getCodigo());
        
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override
            public void handle(final long now) {
                if (now > lastTimerCall + 120_000_000_000l) {
                    atualizaDadosColaborador(colaborador.getCodigo());
                    lastTimerCall = now;
                }
            }
        };
        timer.start();
    }
    
    public AnchorPane show() {
        return boxAnchorCelula;
    }
}
