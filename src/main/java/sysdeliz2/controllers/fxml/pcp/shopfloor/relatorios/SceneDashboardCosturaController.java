/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneDashboardCosturaController implements Initializable {
    
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private Button btnCarregar;
    @FXML
    private Button btnLimparFiltros;
    // </editor-fold>
    @FXML
    private FlowPane containerCelulas;
    
    private GenericDao<SdCelula> daoCelula = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCelula.class);
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ObservableList<SdCelula> celulas = daoCelula.list();
            containerCelulas.getChildren().clear();
            celulas.forEach(celula001 -> {
                containerCelulas.getChildren().add(new BoxCelulaDashboard(celula001).show());
            });
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> filterCelula = null;
        try {
            filterCelula = new GenericFilter<SdCelula>() {};
            filterCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(filterCelula.selectedsReturn.stream().map(celula001 -> String.valueOf(celula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }
    
    @FXML
    private void btnCarregarOnAction(ActionEvent event) {
        try {
            ObservableList<SdCelula> celulas = daoCelula.initCriteria().
                    addPredicateIn("codigo", tboxFiltroCelula.getText().split(","))
                    .loadListByPredicate();
            containerCelulas.getChildren().clear();
            celulas.forEach(celula001 -> {
                containerCelulas.getChildren().add(new BoxCelulaDashboard(celula001).show());
            });
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        try {
            tboxFiltroCelula.clear();
            ObservableList<SdCelula> celulas = daoCelula.list();
            containerCelulas.getChildren().clear();
            celulas.forEach(celula001 -> {
                containerCelulas.getChildren().add(new BoxCelulaDashboard(celula001).show());
            });
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    
}
