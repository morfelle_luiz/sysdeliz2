/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.models.view.VSdProducaoOf;
import sysdeliz2.utils.*;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneMonitorOfController implements Initializable {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    private final ListProperty<VSdProducaoOf> ofsProgramadas = new SimpleListProperty<VSdProducaoOf>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML Components">
    @FXML
    private TitledPane titledPaneFiltros;
    @FXML
    private DatePicker tboxDtInicioEnvio;
    @FXML
    private DatePicker tboxDtFimEnvio;
    @FXML
    private DatePicker tboxDtInicioRetorno;
    @FXML
    private DatePicker tboxDtFimRetorno;
    @FXML
    private DatePicker tboxDtInicioProgramacao;
    @FXML
    private DatePicker tboxDtFimProgramacao;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroSetor;
    @FXML
    private TextField tboxFiltroPeriodo;
    @FXML
    private TextField tboxFiltroFamilia;
    @FXML
    private TextField tboxFiltroMaterial;
    @FXML
    private HBox hboxFiltroOfs;
    @FXML
    private Label lbFiltroOfAtrasada;
    @FXML
    private Label lbFiltroOfAAtrasar;
    @FXML
    private Label lbFiltroOfEmDia;
    @FXML
    private Label lbFiltroOfFinalizada;
    @FXML
    private Button btnProcurarPeriodo;
    @FXML
    private Button btnProcurarFamilia;
    @FXML
    private Button btnProcurarMaterial;
    @FXML
    private Button btnCarregarProducao;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private TableView<VSdProducaoOf> tblProducao;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private TableColumn<VSdProducaoOf, LocalDate> clnDtEnvio;
    @FXML
    private TableColumn<VSdProducaoOf, LocalDate> clnDtRetorno;
    @FXML
    private TableColumn<VSdProducaoOf, LocalDateTime> clnDtProgramacao;
    @FXML
    private TableColumn<VSdProducaoOf, VSdProducaoOf> clnProduto;
    @FXML
    private TableColumn<VSdProducaoOf, String> clnStatusMaterial;
    @FXML
    private TableColumn<VSdProducaoOf, VSdProducaoOf> clnMaterial;
    @FXML
    private TableColumn<VSdProducaoOf, BigDecimal> clnPercProducao;
    @FXML
    private TableColumn<VSdProducaoOf, BigDecimal> clnPercOperacao;
    @FXML
    private Label lbRegistersCount;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Componentes Extra">
    private FormToggleField toggleSomenteProgramadas = new FormToggleField("Somente Prog.", false, false);
    private ToggleSwitch toggleRed = new ToggleSwitch(16, 40, "#f5c6cb");
    private ToggleSwitch toggleYellow = new ToggleSwitch(16, 40, "#ffeeba");
    private ToggleSwitch toggleBlue = new ToggleSwitch(16, 40, "#b8daff");
    private ToggleSwitch toggleGreen = new ToggleSwitch(16, 40, "#c3e6cb");
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    GenericDao<VSdProducaoOf> daoProducao = new GenericDaoImpl<>(VSdProducaoOf.class);
    // </editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ofsProgramadas.set(daoProducao.list());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
        
        this.initializeComponentesFxml();
    }
    
    private void initializeComponentesFxml() {
        // <editor-fold defaultstate="collapsed" desc="HBox FILTRO OFS">
        hboxFiltroOfs.getChildren().add(toggleSomenteProgramadas);
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ToggleSwitch FILTRO">
        lbFiltroOfAtrasada.setGraphic(toggleRed);
        lbFiltroOfAAtrasar.setGraphic(toggleYellow);
        lbFiltroOfFinalizada.setGraphic(toggleBlue);
        lbFiltroOfEmDia.setGraphic(toggleGreen);
        toggleRed.switchedOnProperty().set(true);
        toggleYellow.switchedOnProperty().set(true);
        toggleGreen.switchedOnProperty().set(true);
        toggleBlue.switchedOnProperty().set(false);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Label REGISTERS COUNT">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(ofsProgramadas.sizeProperty()).concat(" registros"));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextField FILTRO">
        TextFieldUtils.upperCase(tboxFiltroFamilia);
        TextFieldUtils.upperCase(tboxFiltroMaterial);
        TextFieldUtils.upperCase(tboxFiltroOf);
        TextFieldUtils.upperCase(tboxFiltroPeriodo);
        TextFieldUtils.upperCase(tboxFiltroProduto);
        TextFieldUtils.upperCase(tboxFiltroSetor);
        TextFieldUtils.inSql(tboxFiltroFamilia);
        TextFieldUtils.inSql(tboxFiltroMaterial);
        TextFieldUtils.inSql(tboxFiltroOf);
        TextFieldUtils.inSql(tboxFiltroPeriodo);
        TextFieldUtils.inSql(tboxFiltroProduto);
        TextFieldUtils.inSql(tboxFiltroSetor);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView OFS PROGRAMADOS">
        tblProducao.itemsProperty().bind(ofsProgramadas);
        tblProducao.setRowFactory(tv -> new TableRow<VSdProducaoOf>() {
            @Override
            protected void updateItem(VSdProducaoOf item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (item.getStatusOf().equals("R")) {
                    clearStyle();
                    getStyleClass().add("table-row-danger");
                } else if (item.getStatusOf().equals("Y")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatusOf().equals("G")) {
                    clearStyle();
                    getStyleClass().add("table-row-success");
                } else if (item.getStatusOf().equals("B")) {
                    clearStyle();
                    getStyleClass().add("table-row-info");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-success");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-info");
            }
            
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn COLUMNS TABLE OFS PROGRAMADOS">
        // <editor-fold defaultstate="collapsed" desc="Column STATUS MATERIAL">
        clnStatusMaterial.setCellFactory(
                new Callback<TableColumn<VSdProducaoOf, String>, TableCell<VSdProducaoOf, String>>() {
                    @Override
                    public TableCell<VSdProducaoOf, String> call(
                            TableColumn<VSdProducaoOf, String> param) {
                        return new TableCell<VSdProducaoOf, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                clearStyle();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("REQUISITADO")) {
                                        getStyleClass().add("table-row-success");
                                    } else if (item.equals("REQ. PARCIAL")) {
                                        getStyleClass().add("table-row-primary");
                                    } else {
                                        getStyleClass().add("table-row-danger");
                                        
                                    }
                                } else {
                                    setText(null);
                                }
                            }
                            
                            private void clearStyle() {
                                getStyleClass().remove("table-row-primary");
                                getStyleClass().remove("table-row-success");
                                getStyleClass().remove("table-row-danger");
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Columns DATAS">
        clnDtEnvio.setCellFactory(param -> new TableCell<VSdProducaoOf, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateFormat(item));
            }
        });
        clnDtRetorno.setCellFactory(param -> new TableCell<VSdProducaoOf, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateFormat(item));
            }
        });
        clnDtProgramacao.setCellFactory(param -> new TableCell<VSdProducaoOf, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty)
                    setText(StringUtils.toDateTimeFormat(item));
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PRODUTO">
        clnProduto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdProducaoOf, VSdProducaoOf>, ObservableValue<VSdProducaoOf>>() {
            @Override
            public ObservableValue<VSdProducaoOf> call(TableColumn.CellDataFeatures<VSdProducaoOf, VSdProducaoOf> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnProduto.setComparator(new Comparator<VSdProducaoOf>() {
            @Override
            public int compare(VSdProducaoOf p1, VSdProducaoOf p2) {
                return p1.getReferencia().compareTo(p2.getReferencia());
            }
        });
        clnProduto.setCellFactory(new Callback<TableColumn<VSdProducaoOf, VSdProducaoOf>, TableCell<VSdProducaoOf, VSdProducaoOf>>() {
            @Override
            public TableCell<VSdProducaoOf, VSdProducaoOf> call(TableColumn<VSdProducaoOf, VSdProducaoOf> btnCol) {
                return new TableCell<VSdProducaoOf, VSdProducaoOf>() {
                    @Override
                    public void updateItem(VSdProducaoOf item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        
                        if (item != null && !empty) {
                            setText("[" + item.getReferencia() + "] " + item.getProduto());
                        }
                    }
                };
            }
            
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column MATERIAL">
        clnMaterial.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<VSdProducaoOf, VSdProducaoOf>, ObservableValue<VSdProducaoOf>>() {
            @Override
            public ObservableValue<VSdProducaoOf> call(TableColumn.CellDataFeatures<VSdProducaoOf, VSdProducaoOf> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnMaterial.setComparator(new Comparator<VSdProducaoOf>() {
            @Override
            public int compare(VSdProducaoOf p1, VSdProducaoOf p2) {
                return p1.getMaterial().compareTo(p2.getMaterial());
            }
        });
        clnMaterial.setCellFactory(new Callback<TableColumn<VSdProducaoOf, VSdProducaoOf>, TableCell<VSdProducaoOf, VSdProducaoOf>>() {
            @Override
            public TableCell<VSdProducaoOf, VSdProducaoOf> call(TableColumn<VSdProducaoOf, VSdProducaoOf> btnCol) {
                return new TableCell<VSdProducaoOf, VSdProducaoOf>() {
                    @Override
                    public void updateItem(VSdProducaoOf item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        
                        if (item != null && !empty) {
                            setText("[" + item.getCodMat() + "] " + item.getMaterial());
                        }
                    }
                };
            }
            
        });
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column % PRODUÇÃO">
        clnPercProducao.setCellFactory(
                new Callback<TableColumn<VSdProducaoOf, BigDecimal>, TableCell<VSdProducaoOf, BigDecimal>>() {
                    @Override
                    public TableCell<VSdProducaoOf, BigDecimal> call(
                            TableColumn<VSdProducaoOf, BigDecimal> param) {
                        return new TableCell<VSdProducaoOf, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 1));
                                }
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column % OPERAÇÃO">
        clnPercOperacao.setCellFactory(
                new Callback<TableColumn<VSdProducaoOf, BigDecimal>, TableCell<VSdProducaoOf, BigDecimal>>() {
                    @Override
                    public TableCell<VSdProducaoOf, BigDecimal> call(
                            TableColumn<VSdProducaoOf, BigDecimal> param) {
                        return new TableCell<VSdProducaoOf, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 1));
                                }
                            }
                            
                        };
                    }
                });
        // </editor-fold>
        //</editor-fold>
    }
    
    // <editor-fold defaultstate="collapsed" desc="Componentes DatePicker KeyEvent">
    @FXML
    private void tboxDtInicioEnvioOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioEnvio.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimEnvioOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimEnvio.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimEnvio.setValue(tboxDtInicioEnvio.getValue());
        }
    }
    
    @FXML
    private void tboxDtInicioRetornoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioRetorno.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimRetornoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimRetorno.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimRetorno.setValue(tboxDtInicioRetorno.getValue());
        }
    }
    
    @FXML
    private void tboxDtInicioProgramacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioProgramacao.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimProgramacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimProgramacao.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimProgramacao.setValue(tboxDtInicioProgramacao.getValue());
        }
    }
    //</editor-fold>
    
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarProdutoOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> filterProduto = null;
        try {
            filterProduto = new GenericFilter<Produto>() {
            };
            filterProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(filterProduto.selectedsReturn.stream().map(produto -> produto.getCodigoFilter()).collect(Collectors.joining(",")));
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    
    @FXML
    private void btnProcurarPeriodoOnAction(ActionEvent event) {
        GenericFilter<TabPrz> filterPeriodo = null;
        try {
            filterPeriodo = new GenericFilter<TabPrz>() {
            };
            filterPeriodo.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroPeriodo.setText(filterPeriodo.selectedsReturn.stream().map(tabPrz -> tabPrz.getCodigoFilter()).collect(Collectors.joining(",")));
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroPeriodoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarPeriodoOnAction(null);
        }
    }
    
    
    @FXML
    private void tboxFiltroFamiliaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarFamiliaOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarFamiliaOnAction(ActionEvent event) {
        GenericFilter<Familia> filterPeriodo = null;
        try {
            filterPeriodo = new GenericFilter<Familia>() {
            };
            filterPeriodo.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroFamilia.setText(filterPeriodo.selectedsReturn.stream().map(familia -> familia.getCodigoFilter()).collect(Collectors.joining(",")));
            
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    
    @FXML
    private void tboxFiltroMaterialOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            this.btnProcurarMaterialOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarMaterialOnAction(ActionEvent event) {
    }
    
    //Buttons Consulta
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxDtFimEnvio.setValue(null);
        tboxDtFimProgramacao.setValue(null);
        tboxDtFimRetorno.setValue(null);
        tboxDtInicioEnvio.setValue(null);
        tboxDtInicioProgramacao.setValue(null);
        tboxDtInicioRetorno.setValue(null);
        
        tboxFiltroProduto.clear();
        tboxFiltroFamilia.clear();
        tboxFiltroMaterial.clear();
        tboxFiltroOf.clear();
        tboxFiltroPeriodo.clear();
        tboxFiltroSetor.clear();
    }
    
    @FXML
    private void btnCarregarProducaoOnAction(ActionEvent event) {
        
        // <editor-fold defaultstate="collapsed" desc="Validações">
        if (tboxDtInicioEnvio.getValue() == null && tboxDtFimEnvio.getValue() != null) {
            GUIUtils.showMessage("Você deve preencher os dois campos de datas para a data de envio ou somente a data de início do período de envio.");
            return;
        }
        if (tboxDtFimEnvio.getValue() != null && tboxDtInicioEnvio.getValue() != null && tboxDtFimEnvio.getValue().isBefore(tboxDtInicioEnvio.getValue())) {
            GUIUtils.showMessage("A data fim do período de envio não pode sem menor que a data de início do período.");
            return;
        }
        if (tboxDtInicioRetorno.getValue() == null && tboxDtFimRetorno.getValue() != null) {
            GUIUtils.showMessage("Você deve preencher os dois campos de datas para a data de retorno ou somente a data de início do período de retorno.");
            return;
        }
        if (tboxDtFimRetorno.getValue() != null && tboxDtInicioRetorno.getValue() != null && tboxDtFimRetorno.getValue().isBefore(tboxDtInicioRetorno.getValue())) {
            GUIUtils.showMessage("A data fim do período de retorno não pode sem menor que a data de início do período.");
            return;
        }
        if (tboxDtInicioProgramacao.getValue() == null && tboxDtFimProgramacao.getValue() != null) {
            GUIUtils.showMessage("Você deve preencher os dois campos de datas para a data de programação ou somente a data de início do período de programação.");
            return;
        }
        if (tboxDtFimProgramacao.getValue() != null && tboxDtInicioProgramacao.getValue() != null && tboxDtFimProgramacao.getValue().isBefore(tboxDtInicioProgramacao.getValue())) {
            GUIUtils.showMessage("A data fim do período de programação não pode sem menor que a data de início do período.");
            return;
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dt Envio">
        LocalDate dtInicioEnvio = tboxDtInicioEnvio.getValue();
        LocalDate dtFimEnvio = dtInicioEnvio;
        if (tboxDtFimEnvio.getValue() != null)
            dtFimEnvio = tboxDtFimEnvio.getValue();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dt Retorno">
        LocalDate dtInicioRetorno = tboxDtInicioRetorno.getValue();
        LocalDate dtFimRetorno = dtInicioRetorno;
        if (tboxDtFimRetorno.getValue() != null)
            dtFimRetorno = tboxDtFimRetorno.getValue();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Dt Programação">
        LocalDate dtInicioProgramacao = tboxDtInicioProgramacao.getValue();
        LocalDate dtFimProgramacao = dtInicioProgramacao;
        if (tboxDtFimProgramacao.getValue() != null)
            dtFimProgramacao = tboxDtFimProgramacao.getValue();
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Status OF">
        String filterStatusValue = ""
                .concat(toggleGreen.switchedOnProperty().get() ? "G," : "")
                .concat(toggleRed.switchedOnProperty().get() ? "R," : "")
                .concat(toggleYellow.switchedOnProperty().get() ? "Y," : "")
                .concat(toggleBlue.switchedOnProperty().get() ? "B," : "");
        filterStatusValue = filterStatusValue.length() > 0 ? filterStatusValue.substring(0, filterStatusValue.length() - 1) : "";
        // </editor-fold>
        
        daoProducao = daoProducao.initCriteria();
        if (dtInicioEnvio != null)
            daoProducao = daoProducao.addPredicateBetween("dtEnv", dtInicioEnvio, dtFimEnvio);
        if (dtInicioRetorno != null)
            daoProducao = daoProducao.addPredicateBetween("dtRet", dtInicioRetorno, dtFimRetorno);
        if (dtInicioProgramacao != null)
            daoProducao = daoProducao.addPredicateBetween("dtProg", dtInicioProgramacao, dtFimProgramacao);
        if (filterStatusValue.length() > 0)
            daoProducao = daoProducao.addPredicate("statusOf", filterStatusValue.split(","), PredicateType.IN);
        if (toggleSomenteProgramadas.isSwitchedOn.get())
            daoProducao = daoProducao.addPredicate("codigo", null, PredicateType.IS_NOT_NULL);
        if (!tboxFiltroProduto.getText().isEmpty())
            daoProducao = daoProducao.addPredicate("referencia", tboxFiltroProduto.getText().split(","), PredicateType.IN);
        if (!tboxFiltroFamilia.getText().isEmpty())
            daoProducao = daoProducao.addPredicate("familia", tboxFiltroFamilia.getText().split(","), PredicateType.IN);
        if (!tboxFiltroPeriodo.getText().isEmpty())
            daoProducao = daoProducao.addPredicate("periodo", tboxFiltroPeriodo.getText().split(","), PredicateType.IN);
        if (!tboxFiltroSetor.getText().isEmpty())
            daoProducao = daoProducao.addPredicate("setor", tboxFiltroSetor.getText().split(","), PredicateType.IN);
        if (!tboxFiltroOf.getText().isEmpty())
            daoProducao = daoProducao.addPredicate("op", tboxFiltroOf.getText().split(","), PredicateType.IN);
        
        try {
            ofsProgramadas.set(daoProducao.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    //Request Menus
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        try {
            new ExportExcel<VSdProducaoOf>(ofsProgramadas, VSdProducaoOf.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
        try {
            VSdProducaoOf ofSelecionada = tblProducao.getSelectionModel().getSelectedItem();
            
            if (ofSelecionada.getCodigo() == null) {
                GUIUtils.showMessage("A O.F. selecionada ainda não foi programada.");
                return;
            }
            
            // First, compile jrxml file.
            JasperReport jasperReport = null;
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/MonitorPacoteOf.jasper"));
            
            parameters.put("pacote", ofSelecionada.getCodigo());
            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);
            parameters.put("codEmpresa", Globals.getEmpresaLogada().getCodigo());
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
            
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    
}
