/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdTiposParada001;
import sysdeliz2.models.view.VSdParadaHora;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneParadasController implements Initializable {

    private final ListProperty<VSdParadaHora> registroConsulta = new SimpleListProperty<>();
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    private final GenericDao<VSdParadaHora> daoProducao = new GenericDaoImpl<>(VSdParadaHora.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML">
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private TextField tboxFiltroColaborador;
    @FXML
    private Button btnProcurarColaborador;
    @FXML
    private TextField tboxFiltroMotivo;
    @FXML
    private Button btnProcurarMotivo;
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private TextField tboxFiltroHoraInicio;
    @FXML
    private TextField tboxFiltroHoraFim;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private DatePicker tboxDtInicioProd;
    @FXML
    private DatePicker tboxDtFimProd;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnCarregarProducao;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView PARADAS">
    @FXML
    private TableView<VSdParadaHora> tblParadas;
    @FXML
    private TableColumn<VSdParadaHora, LocalDateTime> clnData;
    @FXML
    private TableColumn<VSdParadaHora, SdColaborador> clnColaborador;
    @FXML
    private TableColumn<VSdParadaHora, LocalDateTime> clnHoraInicio;
    @FXML
    private TableColumn<VSdParadaHora, LocalDateTime> clnHoraFim;
    @FXML
    private TableColumn<VSdParadaHora, SdTiposParada001> clnMotivo;
    @FXML
    private TableColumn<VSdParadaHora, BigDecimal> clnTmpParado;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbRegistersCount;
    //</editor-fold>
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponents();
    }

    private void initializeComponents() {

        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registroConsulta.sizeProperty()).concat(" registros"));
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TableView PRODUCAO">
        tblParadas.itemsProperty().bind(registroConsulta);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUCAO">
        clnData.setCellFactory(param -> new TableCell<VSdParadaHora, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(StringUtils.toDateFormat(item.toLocalDate()));
                }
            }
        });
        clnHoraInicio.setCellFactory(param -> new TableCell<VSdParadaHora, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toTimeFormat(item.toLocalTime()));
                }
            }
        });
        clnHoraFim.setCellFactory(param -> new TableCell<VSdParadaHora, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toTimeFormat(item.toLocalTime()));
                }
            }
        });
        clnTmpParado.setCellFactory(param -> new TableCell<VSdParadaHora, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnMotivo.setComparator(new Comparator<SdTiposParada001>() {
            @Override
            public int compare(SdTiposParada001 p1, SdTiposParada001 p2) {
                return p1.getDescricao().compareTo(p2.getDescricao());
            }
        });
        clnColaborador.setComparator(new Comparator<SdColaborador>() {
            @Override
            public int compare(SdColaborador p1, SdColaborador p2) {
                return p1.getNome().compareTo(p2.getNome());
            }
        });
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TextBox DATA PICKERS">
        tboxDtInicioProd.setValue(LocalDate.now());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox HR INICIO/FIM">
        MaskTextField.timeField(tboxFiltroHoraInicio);
        MaskTextField.timeField(tboxFiltroHoraFim);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="MenuRequest EXPORT e PRINT DADOS">
        requestMenuExportarExcel.disableProperty().bind(registroConsulta.emptyProperty());
        requestMenuImprimirRelatorio.disableProperty().bind(registroConsulta.emptyProperty());
        // </editor-fold>
    }

    // <editor-fold defaultstate="collapsed" desc="Actions Filtro">
    @FXML
    private void tboxFiltroMotivoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarMotivoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarMotivoOnAction(ActionEvent event) {
        GenericFilter<SdTiposParada001> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<SdTiposParada001>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroMotivo.setText(procuraProduto.selectedsReturn.stream().map(sdTiposParada001 -> String.valueOf(sdTiposParada001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> procuraCelula = null;
        try {
            procuraCelula = new GenericFilter<SdCelula>() {
            };
            procuraCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(procuraCelula.selectedsReturn.stream().map(sdCelula001 -> String.valueOf(sdCelula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColaboradorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }

    @FXML
    private void tboxDtInicioProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioProd.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxDtFimProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimProd.setValue(LocalDate.now());
        } else if (event.getCode() == KeyCode.C) {
            tboxDtFimProd.setValue(tboxDtInicioProd.getValue());
        }
    }

    @FXML
    private void tboxFiltroHoraInicioOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraInicio.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        }
    }

    @FXML
    private void tboxFiltroHoraFimOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraFim.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        } else if (event.getCode() == KeyCode.C) {
            tboxFiltroHoraFim.setText(tboxFiltroHoraInicio.getText());
        }
    }

    @FXML
    private void btnProcurarColaboradorOnAction(ActionEvent event) {
        GenericFilter<SdColaborador> procurarColaborador = null;
        try {
            procurarColaborador = new GenericFilter<SdColaborador>() {
            };
            procurarColaborador.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procurarColaborador.selectedsReturn.stream().map(sdColaborador001 -> String.valueOf(sdColaborador001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnCarregarParadasOnAction(ActionEvent event) {
        try {
            GenericDao consulta = daoProducao.initCriteria();
            if(!tboxFiltroMotivo.getText().isEmpty())
                consulta = consulta.addPredicate("motivo.codigo", tboxFiltroMotivo.getText().split(","), PredicateType.IN);
            if(!tboxFiltroColaborador.getText().isEmpty())
                consulta = consulta.addPredicate("colaborador.codigo", tboxFiltroColaborador.getText().split(","), PredicateType.IN);
            if(!tboxFiltroCelula.getText().isEmpty())
                consulta = consulta.addPredicate("celula", tboxFiltroCelula.getText().split(","), PredicateType.IN);
    
            LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
            LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
            if(tboxDtFimProd.getValue() != null)
                dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
            else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
            }
            consulta = consulta.addPredicateBetween("dhInicio", dhInicio, dhFim);
    
            JPAUtils.clearEntitys(registroConsulta);
            registroConsulta.set(consulta.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        registroConsulta.clear();
        tboxFiltroColaborador.clear();
        tboxFiltroCelula.clear();
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(null);
        tboxFiltroHoraInicio.clear();
        tboxFiltroHoraFim.clear();
        tboxFiltroMotivo.clear();
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Actions MenuRequest">
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        try {
            new ExportExcel<VSdParadaHora>(registroConsulta, VSdParadaHora.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
//        try {
//            Map<String, Integer> opcoes = new HashMap<>();
//            opcoes.put("De Colaborador/Lançamentos", 1);
//            opcoes.put("De Colaborador/Operações", 2);
//            opcoes.put("De Operação/Colaborador", 3);
//            opcoes.put("De Operação/Família/Colaborador", 4);
//            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);
//
//            // First, compile jrxml file.
//            JasperReport jasperReport = null;
//            HashMap<String, Object> parameters = new HashMap<String, Object>();
//            if (opcaoEscolhida == 1) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoLancamentos.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroMotivo.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 2) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacao.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroMotivo.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 3) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupado.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroMotivo.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 4) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupadoFamilia.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroMotivo.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            }
//
//            if (opcaoEscolhida != null) {
//                JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getDelizConnection());
//                new PrintReportPreview().showReport(print);
//            }else{
//                GUIUtils.showMessage("Escolha uma opção de layout de relatório.");
//                return;
//            }
//
//        } catch (JRException | NullPointerException ex) {
//            ex.printStackTrace();
//            Logger.getLogger(ImpressaoFichaCtps.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
//
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            Logger.getLogger(ImpressaoFichaCtps.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
//        }
    }
    //</editor-fold>

}
