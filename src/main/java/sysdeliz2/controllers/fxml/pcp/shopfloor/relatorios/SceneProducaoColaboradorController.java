/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.Section;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;
import javafx.util.StringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.controllers.fxml.pcp.shopfloor.SceneProgramaProducaoOfControllerNew;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdProdDiariaColaborador;
import sysdeliz2.models.view.VSdProgramacaoPendente;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.PrintReportPreview;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProducaoColaboradorController implements Initializable {
    
    private final ListProperty<VSdProdDiariaColaborador> registroConsulta = new SimpleListProperty<>();
    private final ListProperty<SdLancamentoProducao001> lancamentosColaborador = new SimpleListProperty<>();
    private final ListProperty<SdLancamentoParada001> paradasColaborador = new SimpleListProperty<>();
    private final ListProperty<VSdProgramacaoPendente> programacaoPendenteColaborador = new SimpleListProperty<>();
    private final ListProperty<SdProgramacaoPacote002> operacoesAgrupadasProgramacaoPendente = new SimpleListProperty<>(FXCollections.observableArrayList());
    
    private ArrayList<VSdProgramacaoPendente> operacoesPendentesSelecionadas = new ArrayList<>();
    
    private final BooleanProperty editandoLancamento = new SimpleBooleanProperty(false);
    private final BooleanProperty editandoParada = new SimpleBooleanProperty(false);
    
    private Gauge eficienciaDia;
    
    private LocalDateTime dhInicioLancamentoSelecionado = LocalDateTime.now();
    private LocalDateTime dhFimLancamentoSelecionado = LocalDateTime.now();
    
    private final ObjectProperty<SdLancamentoParada001> paradaAbertaColaborador = new SimpleObjectProperty<>(null);
    private final ObjectProperty<SdLancamentoProducao001> lancamentoColaboradorSelecionado = new SimpleObjectProperty<>();
    private final ObjectProperty<SdLancamentoParada001> paradaColaboradorSelecionada = new SimpleObjectProperty<>();
    
    // <editor-fold defaultstate="collapsed" desc="Desclaração: Converters">
    private final StringConverter<LocalDateTime> convertDateTime = new StringConverter<LocalDateTime>() {
        @Override
        public String toString(LocalDateTime object) {
            return object.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        }
        
        @Override
        public LocalDateTime fromString(String string) {
            return LocalDateTime.parse(string, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        }
    };
    private final StringConverter<BigDecimal> convertBigDecimalPercent = new StringConverter<BigDecimal>() {
        @Override
        public String toString(BigDecimal object) {
            return StringUtils.toPercentualFormat(object.doubleValue(), 1);
        }
        
        @Override
        public BigDecimal fromString(String string) {
            return BigDecimal.valueOf(Double.parseDouble(string.replace(",", ".").replace("%", "")));
        }
    };
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    private final GenericDao<SdTiposParada001> daoParadas = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdTiposParada001.class);
    private final GenericDao<SdLancamentoParada001> daoLancamentoParadas = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLancamentoParada001.class);
    private final GenericDao<SdLancamentoProducao001> daoLancamentoProducao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLancamentoProducao001.class);
    private final GenericDao<SdParadasTurno001> daoParadasTurno = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdParadasTurno001.class);
    private final GenericDao<SdColaborador> daoColaborador = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdColaborador.class);
    private final GenericDao<SdColabCelula001> daoCelula = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdColabCelula001.class);
    private final GenericDao<VSdProgramacaoPendente> daoProgramacoesPendentes = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdProgramacaoPendente.class);
    private final GenericDao<SdProgramacaoPacote002> daoProgramacaoPacote = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdProgramacaoPacote002.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML">
    @FXML
    private TabPane tabsWindow;
    @FXML
    private Tab tabListagem;
    @FXML
    private Button btnVoltarPainel;
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private HBox hboxFiltro;
    @FXML
    private TextField tboxFiltroColaborador;
    @FXML
    private Button btnProcurarColaborador;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private DatePicker tboxDtInicioProd;
    @FXML
    private DatePicker tboxDtFimProd;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnCarregarProducao;
    private FormToggleField toggleSomenteDiaUtil = new FormToggleField("Som. Dia Útil", true, false);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML Gestão">
    @FXML
    private Button btnImprimirProgramacao;
    @FXML
    private Button btnLancarParada;
    @FXML
    private Button btnRetornarParada;
    @FXML
    private Button btnAbrirPainelColaborador;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView Produção">
    @FXML
    private TableView<VSdProdDiariaColaborador> tblProducao;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, LocalDate> clnData;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnOperacao;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnEficiencia;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnProdutividade;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpProduzido;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpProgramado;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpProdutivo;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpProducao;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpParado;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnTmpDeduzEficiencia;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnEficienciaMes;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnProdutividadeMes;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnEficienciaAno;
    @FXML
    private TableColumn<VSdProdDiariaColaborador, Double> clnProdutividadeAno;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbRegistersCount;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML Infos Colaborador">
    @FXML
    private TextField tboxColaborador;
    @FXML
    private TextField tboxCelula;
    @FXML
    private TextField tboxParadoAgora;
    @FXML
    private HBox boxGaugesColaborador;
    @FXML
    private Button btnRetornarParadaColaborador;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView Lançamento Colaborador">
    @FXML
    private TableView<SdLancamentoProducao001> tblLancamentosColaborador;
    @FXML
    private TableColumn<SdLancamentoProducao001, SdPacoteLancamento001> clnProdutoLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, SdPacoteLancamento001> clnCorLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, SdPacoteLancamento001> clnTamanhoLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, LocalDateTime> clnDhInicioLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, LocalDateTime> clnDhFimLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, BigDecimal> clnTempoProdLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, BigDecimal> clnTempoOrgLancamento;
    @FXML
    private TableColumn<SdLancamentoProducao001, SdLancamentoProducao001> clnEficienciaLancamento;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView Paradas Colaborador">
    @FXML
    private TableView<SdLancamentoParada001> tblParadasColaborador;
    @FXML
    private TableColumn<SdLancamentoParada001, SdLancamentoParada001PK> clnMotivoParada;
    @FXML
    private TableColumn<SdLancamentoParada001, SdLancamentoParada001PK> clnDhInicioParada;
    @FXML
    private TableColumn<SdLancamentoParada001, LocalDateTime> clnFimParada;
    @FXML
    private TableColumn<SdLancamentoParada001, BigDecimal> clnMinutosParados;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML Ocupação Colaborador">
    @FXML
    private TableView<VSdProgramacaoPendente> tblOperacoesColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnSeqOperacaoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnPacoteColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnOperacaoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnEquipamentoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnTempoColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnQtdeColaborador;
    @FXML
    private TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> clnAgrupColaborador;
    @FXML
    private TitledPane tpaneOperacoesAgrupadas;
    @FXML
    private TableView<SdProgramacaoPacote002> tblOperacoesAgrupadas;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOrdemAgrupada;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdProgramacaoPacote002PK> clnOperacaoAgrupada;
    @FXML
    private TableColumn<SdProgramacaoPacote002, BigDecimal> clnTempoOpAgrupado;
    @FXML
    private TableColumn<SdProgramacaoPacote002, SdEquipamentosOrganize001> clnEquipamentoAgrupado;
    @FXML
    private Button btnConfirmarAlteracao;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML Formulário Lançamento">
    @FXML
    private TitledPane tpaneApontamentoProducao;
    @FXML
    private TextField tboxSeqLancamento;
    @FXML
    private TextField tboxPacoteProgLancamento;
    @FXML
    private TextField tboxOperacaoLancamento;
    @FXML
    private TextField tboxOrdemLancamento;
    @FXML
    private TextField tboxQuebraLancamento;
    @FXML
    private TextField tboxPacoteLancamento;
    @FXML
    private TextField tboxProdutoLancamento;
    @FXML
    private TextField tboxCorLancamento;
    @FXML
    private TextField tboxTamLancamento;
    @FXML
    private TextField tboxQtdeLancamento;
    @FXML
    private TextField tboxTmpOrganize;
    @FXML
    private TextField tboxDhInicioLancamento;
    @FXML
    private TextField tboxDhFimLancamento;
    @FXML
    private TextField tboxTmpProducaoLancamento;
    @FXML
    private TextField tboxEficiencieLancamento;
    @FXML
    private Button btnRecalcularEficiencia;
    @FXML
    private Button btnLiberarEdicaoLancamento;
    @FXML
    private Button btnExcluirLancamento;
    @FXML
    private Button btnSalvaLancamento;
    @FXML
    private Button btnCancelarLancamento;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML Formulário Paradas">
    @FXML
    private TitledPane tpaneApontamentoParada;
    @FXML
    private TextField tboxMotivoParada;
    @FXML
    private TextField tboxDhInicioParada;
    @FXML
    private TextField tboxDhFimParada;
    @FXML
    private TextField tboxMinutosParado;
    @FXML
    private Button btnLiberarEdicaoParada;
    @FXML
    private Button btnExcluirParada;
    @FXML
    private Button btnSalvaParada;
    @FXML
    private Button btnCancelarParada;
    //</editor-fold>
    //</editor-fold>
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponents();
    }
    
    private void initializeComponents() {
        // <editor-fold defaultstate="collapsed" desc="HBox FILTROS">
        hboxFiltro.getChildren().add(0, toggleSomenteDiaUtil);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TitlePane Lançamentos/Programacao">
        tpaneOperacoesAgrupadas.disableProperty().bind(tblOperacoesColaborador.getSelectionModel().selectedItemProperty().isNull());
        tpaneApontamentoParada.disableProperty().bind(tblParadasColaborador.getSelectionModel().selectedItemProperty().isNull());
        tpaneApontamentoProducao.disableProperty().bind(tblLancamentosColaborador.getSelectionModel().selectedItemProperty().isNull());
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registroConsulta.sizeProperty()).concat(" registros"));
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Buttons GESTÃO">
        btnImprimirProgramacao.disableProperty().bind(registroConsulta.emptyProperty().or(tblProducao.getSelectionModel().selectedItemProperty().isNull()));
        btnLancarParada.disableProperty().bind(registroConsulta.emptyProperty().or(tblProducao.getSelectionModel().selectedItemProperty().isNull()));
        btnRetornarParada.disableProperty().bind(registroConsulta.emptyProperty().or(tblProducao.getSelectionModel().selectedItemProperty().isNull()));
        btnAbrirPainelColaborador.disableProperty().bind(registroConsulta.emptyProperty().or(tblProducao.getSelectionModel().selectedItemProperty().isNull()));
        btnRetornarParadaColaborador.disableProperty().bind(paradaAbertaColaborador.isNull());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Buttons Lancamento/Parada">
        btnRecalcularEficiencia.disableProperty().bind(editandoLancamento);
        btnLiberarEdicaoLancamento.disableProperty().bind(editandoLancamento);
        btnExcluirLancamento.disableProperty().bind(editandoLancamento);
        btnSalvaLancamento.disableProperty().bind(editandoLancamento.not());
        
        btnLiberarEdicaoParada.disableProperty().bind(editandoParada);
        btnExcluirParada.disableProperty().bind(editandoParada);
        btnSalvaParada.disableProperty().bind(editandoParada.not());
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView PRODUCAO">
        tblProducao.itemsProperty().bind(registroConsulta);
        tblProducao.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblProducao.setRowFactory(tv -> new TableRow<VSdProdDiariaColaborador>() {
            @Override
            protected void updateItem(VSdProdDiariaColaborador item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (Integer.parseInt(item.getDia()) % 2 == 0) {
                    clearStyle();
                    getStyleClass().add("table-row-alternate-on");
                } else {
                    clearStyle();
                    getStyleClass().add("table-row-alternate-off");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-alternate-on");
                getStyleClass().remove("table-row-alternate-off");
            }
            
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView Apontamentos Produção">
        tblLancamentosColaborador.itemsProperty().bind(lancamentosColaborador);
        tblLancamentosColaborador.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            lancamentoColaboradorSelecionado.set(newValue);
            this.unbindLancamentoProducaoColaborador(oldValue);
            this.bindLancamentoProducaoColaborador(newValue);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView Apontamentos Paradas">
        tblParadasColaborador.itemsProperty().bind(paradasColaborador);
        tblParadasColaborador.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            paradaColaboradorSelecionada.set(newValue);
            unbindLancamentoParadaColaborador(oldValue);
            bindLancamentoParadaColaborador(newValue);
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView Programação Pendente">
        tblOperacoesColaborador.itemsProperty().bind(programacaoPendenteColaborador);
        tblOperacoesColaborador.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblOperacoesColaborador.setRowFactory(tv -> {
            TableRow<VSdProgramacaoPendente> row = new TableRow<VSdProgramacaoPendente>() {
            };
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();
                    
                    ObservableList<VSdProgramacaoPendente> items = tblOperacoesColaborador.getSelectionModel().getSelectedItems();
                    for (VSdProgramacaoPendente iI : items) {
                        operacoesPendentesSelecionadas.add(iI);
                    }
                    
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SceneProgramaProducaoOfControllerNew.SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SceneProgramaProducaoOfControllerNew.SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer) db.getContent(SceneProgramaProducaoOfControllerNew.SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                
                if (db.hasContent(SceneProgramaProducaoOfControllerNew.SERIALIZED_MIME_TYPE)) {
                    
                    if (operacoesPendentesSelecionadas.size() > 0) {
                        // veio do operacoes colaborador para reordenar
                        int dropIndex;
                        VSdProgramacaoPendente dI = null;
                        
                        if (row.isEmpty()) {
                            dropIndex = tblOperacoesColaborador.getItems().size();
                        } else {
                            dropIndex = row.getIndex();
                            dI = tblOperacoesColaborador.getItems().get(dropIndex);
                        }
                        int delta = 0;
                        if (dI != null)
                            while (operacoesPendentesSelecionadas.contains(dI)) {
                                delta = 1;
                                --dropIndex;
                                if (dropIndex < 0) {
                                    dI = null;
                                    dropIndex = 0;
                                    break;
                                }
                                dI = tblOperacoesColaborador.getItems().get(dropIndex);
                            }
                        
                        for (VSdProgramacaoPendente sI : operacoesPendentesSelecionadas) {
                            tblOperacoesColaborador.getItems().remove(sI);
                        }
                        
                        if (dI != null)
                            dropIndex = tblOperacoesColaborador.getItems().indexOf(dI) + delta;
                        else if (dropIndex != 0)
                            dropIndex = tblOperacoesColaborador.getItems().size();
                        
                        tblOperacoesColaborador.getSelectionModel().clearSelection();
                        
                        for (VSdProgramacaoPendente sI : operacoesPendentesSelecionadas) {
                            //draggedIndex = selections.get(i);
                            tblOperacoesColaborador.getItems().add(dropIndex, sI);
                            tblOperacoesColaborador.getSelectionModel().select(dropIndex);
                            dropIndex++;
                        }
                        this.updateOrdemOperacaoPendente();
                    } else {
                        return;
                    }
                    
                    event.setDropCompleted(true);
                    operacoesPendentesSelecionadas.clear();
                    event.consume();
                }
            });
            
            return row;
        });
        tblOperacoesColaborador.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            tpaneOperacoesAgrupadas.setExpanded(false);
            operacoesAgrupadasProgramacaoPendente.clear();
            if (newValue != null && newValue.getProgramacao().getOperacoesAgrupadas().size() > 0) {
                operacoesAgrupadasProgramacaoPendente.get().addAll(newValue.getProgramacao().getOperacoesAgrupadas());
                tpaneOperacoesAgrupadas.setExpanded(true);
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableView Operações Agrupadas Programação Pendente">
        tblOperacoesAgrupadas.itemsProperty().bind(operacoesAgrupadasProgramacaoPendente);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUCAO">
        clnData.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(StringUtils.toDateFormat(item));
                }
            }
        });
        clnEficiencia.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnOperacao.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividade.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficienciaMes.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividadeMes.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficienciaAno.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividadeAno.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnTmpParado.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpDeduzEficiencia.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProducao.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProdutivo.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProduzido.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProgramado.setCellFactory(param -> new TableCell<VSdProdDiariaColaborador, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn Apontamentos Produção">
        clnProdutoLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, SdPacoteLancamento001>() {
            @Override
            protected void updateItem(SdPacoteLancamento001 item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(item.getReferencia());
                }
            }
        });
        clnCorLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, SdPacoteLancamento001>() {
            @Override
            protected void updateItem(SdPacoteLancamento001 item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(item.getCor() != null ? item.getCor().getCor() : "TOT");
                }
            }
        });
        clnTamanhoLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, SdPacoteLancamento001>() {
            @Override
            protected void updateItem(SdPacoteLancamento001 item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(item.getTam());
                }
            }
        });
        clnDhInicioLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDateTimeFormat(item));
                }
            }
        });
        clnDhFimLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDateTimeFormat(item));
                }
            }
        });
        clnTempoProdLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTempoOrgLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnEficienciaLancamento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SdLancamentoProducao001, SdLancamentoProducao001>, ObservableValue<SdLancamentoProducao001>>() {
            @Override
            public ObservableValue<SdLancamentoProducao001> call(TableColumn.CellDataFeatures<SdLancamentoProducao001, SdLancamentoProducao001> features) {
                return new ReadOnlyObjectWrapper(features.getValue());
            }
        });
        clnEficienciaLancamento.setComparator(new Comparator<SdLancamentoProducao001>() {
            @Override
            public int compare(SdLancamentoProducao001 p1, SdLancamentoProducao001 p2) {
                String value1 = String.valueOf(p1.getTempoOper().doubleValue() / p1.getTempoProd().doubleValue());
                String value2 = String.valueOf(p2.getTempoOper().doubleValue() / p2.getTempoProd().doubleValue());
                return value1.compareTo(value2);
            }
        });
        clnEficienciaLancamento.setCellFactory(param -> new TableCell<SdLancamentoProducao001, SdLancamentoProducao001>() {
            @Override
            protected void updateItem(SdLancamentoProducao001 item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toPercentualFormat((item.getTempoOper().doubleValue() / item.getTempoProd().doubleValue()), 2));
                }
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn Apontamentos Paradas">
        clnMotivoParada.setCellFactory(param -> new TableCell<SdLancamentoParada001, SdLancamentoParada001PK>() {
            @Override
            protected void updateItem(SdLancamentoParada001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(item.getMotivo().toString());
                }
            }
        });
        clnDhInicioParada.setCellFactory(param -> new TableCell<SdLancamentoParada001, SdLancamentoParada001PK>() {
            @Override
            protected void updateItem(SdLancamentoParada001PK item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(StringUtils.toDateTimeFormat(item.getDhInicio()));
                }
            }
        });
        clnFimParada.setCellFactory(param -> new TableCell<SdLancamentoParada001, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDateTimeFormat(item));
                }
            }
        });
        clnMinutosParados.setCellFactory(param -> new TableCell<SdLancamentoParada001, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn Programação Pendente">
        // <editor-fold defaultstate="collapsed" desc="Column SEQ COLABORADOR">
        clnSeqOperacaoColaborador.setCellFactory(
                new Callback<TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002>, TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>>() {
                    @Override
                    public TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002> call(
                            TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> param) {
                        return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                            @Override
                            protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toIntegerFormat(item.getSeqColaborador()));
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PACOTE COLABORADOR">
        clnPacoteColaborador.setCellFactory(
                new Callback<TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002>, TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>>() {
                    @Override
                    public TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002> call(
                            TableColumn<VSdProgramacaoPendente, SdProgramacaoPacote002> btnCol) {
                        return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                            
                            @Override
                            public void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.getId().getPacote());
                                    setGraphic(null);
                                }
                            }
                        };
                    }
                });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column OPERACAO COLABORADOR">
        clnOperacaoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getId().getOperacao().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column EQUIPAMENTO COLABORADOR">
        clnEquipamentoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getMaquina().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO COLABORADOR">
        clnTempoColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.getTempoTotal().doubleValue(), 4));
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column QTDE COLABORADOR">
        clnQtdeColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toIntegerFormat(item.getQtde()));
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column AGRUP COLABORADOR">
        clnAgrupColaborador.setCellFactory(param -> {
            return new TableCell<VSdProgramacaoPendente, SdProgramacaoPacote002>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOperacoesAgrupadas().size() > 0 ? "SIM" : "NÃO");
                    }
                }
            };
        });
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn Operações Agrupadas">
        // <editor-fold defaultstate="collapsed" desc="Column ORDEM OPERACAO">
        clnOrdemAgrupada.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOrdem() + "");
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column PACOTE COLABORADOR">
        clnOperacaoAgrupada.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdProgramacaoPacote002PK>() {
                @Override
                protected void updateItem(SdProgramacaoPacote002PK item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.getOperacao().toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column EQUIPAMENTO AGRUPADO">
        clnEquipamentoAgrupado.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, SdEquipamentosOrganize001>() {
                @Override
                protected void updateItem(SdEquipamentosOrganize001 item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(item.toString());
                    }
                }
            };
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Column TEMPO COLABORADOR">
        clnTempoOpAgrupado.setCellFactory(param -> {
            return new TableCell<SdProgramacaoPacote002, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                    }
                }
            };
        });
        // </editor-fold>
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TextBox FILTROS">
        TextFieldUtils.upperCase(tboxFiltroProduto);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DATA PICKERS">
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(LocalDate.now());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DH INICIO/FIM LANCAMENTO">
        //MaskTextField.dateTimeField(tboxDhInicioLancamento);
        //MaskTextField.dateTimeField(tboxDhFimLancamento);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DH INICIO/FIM PARADA">
        //MaskTextField.dateTimeField(tboxDhInicioParada);
        //MaskTextField.dateTimeField(tboxDhFimParada);
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="MenuRequest EXPORT e PRINT DADOS">
        requestMenuExportarExcel.disableProperty().bind(registroConsulta.emptyProperty());
        requestMenuImprimirRelatorio.disableProperty().bind(registroConsulta.emptyProperty());
        // </editor-fold>
    }
    
    private void bindLancamentoProducaoColaborador(SdLancamentoProducao001 sdLancamentoProducao001) {
        if (sdLancamentoProducao001 != null) {
            tboxSeqLancamento.textProperty().bind(sdLancamentoProducao001.idProperty().asString());
            tboxPacoteProgLancamento.textProperty().bind(sdLancamentoProducao001.pacoteProgProperty());
            tboxOperacaoLancamento.textProperty().bind(sdLancamentoProducao001.operacaoOrganizeProperty().asString());
            tboxOrdemLancamento.textProperty().bind(sdLancamentoProducao001.ordemProperty().asString());
            tboxQuebraLancamento.textProperty().bind(sdLancamentoProducao001.quebraProperty().asString());
            tboxPacoteLancamento.textProperty().bind(sdLancamentoProducao001.pacoteLanProperty().asString());
            tboxProdutoLancamento.textProperty().bind(sdLancamentoProducao001.pctLancamentoProperty().get().referenciaProperty());
            tboxCorLancamento.textProperty().bind(sdLancamentoProducao001.pctLancamentoProperty().get().corProperty().get().descricaoProperty());
            tboxTamLancamento.textProperty().bind(sdLancamentoProducao001.pctLancamentoProperty().get().tamProperty());
            tboxQtdeLancamento.textProperty().bind(sdLancamentoProducao001.qtdeProperty().asString());
            tboxTmpOrganize.textProperty().bind(sdLancamentoProducao001.tempoOperProperty().asString());
            tboxDhInicioLancamento.textProperty().bindBidirectional(sdLancamentoProducao001.dhInicioProperty(), convertDateTime);
            tboxDhFimLancamento.textProperty().bindBidirectional(sdLancamentoProducao001.dhFimProperty(), convertDateTime);
            tboxTmpProducaoLancamento.textProperty().bind(sdLancamentoProducao001.tempoProdProperty().asString());
            tboxEficiencieLancamento.textProperty().bindBidirectional(sdLancamentoProducao001.eficienciaProperty(), convertBigDecimalPercent);
        }
    }
    
    private void unbindLancamentoProducaoColaborador(SdLancamentoProducao001 sdLancamentoProducao001) {
        if (sdLancamentoProducao001 != null) {
            tboxSeqLancamento.textProperty().unbind();
            tboxPacoteProgLancamento.textProperty().unbind();
            tboxOperacaoLancamento.textProperty().unbind();
            tboxOrdemLancamento.textProperty().unbind();
            tboxQuebraLancamento.textProperty().unbind();
            tboxPacoteLancamento.textProperty().unbind();
            tboxProdutoLancamento.textProperty().unbind();
            tboxCorLancamento.textProperty().unbind();
            tboxTamLancamento.textProperty().unbind();
            tboxQtdeLancamento.textProperty().unbind();
            tboxTmpOrganize.textProperty().unbind();
            tboxDhInicioLancamento.textProperty().unbindBidirectional(sdLancamentoProducao001.dhInicioProperty());
            tboxDhFimLancamento.textProperty().unbindBidirectional(sdLancamentoProducao001.dhFimProperty());
            tboxTmpProducaoLancamento.textProperty().unbind();
            tboxEficiencieLancamento.textProperty().unbindBidirectional(sdLancamentoProducao001.eficienciaProperty());
        }
    }
    
    private void bindLancamentoParadaColaborador(SdLancamentoParada001 sdLancamentoParada001) {
        if (sdLancamentoParada001 != null) {
            tboxMotivoParada.textProperty().bind(sdLancamentoParada001.getId().motivoProperty().asString());
            tboxDhInicioParada.textProperty().bindBidirectional(sdLancamentoParada001.idProperty().get().dhInicioProperty(), convertDateTime);
            tboxDhFimParada.textProperty().bindBidirectional(sdLancamentoParada001.dhFimProperty(), convertDateTime);
            tboxMinutosParado.textProperty().bind(sdLancamentoParada001.tempoProperty().asString());
        }
    }
    
    private void unbindLancamentoParadaColaborador(SdLancamentoParada001 sdLancamentoParada001) {
        if (sdLancamentoParada001 != null) {
            tboxMotivoParada.textProperty().unbind();
            tboxDhInicioParada.textProperty().unbindBidirectional(sdLancamentoParada001.idProperty().get().dhInicioProperty());
            tboxDhFimParada.textProperty().unbindBidirectional(sdLancamentoParada001.dhFimProperty());
            tboxMinutosParado.textProperty().unbind();
        }
    }
    
    private void updateOrdemOperacaoPendente() {
        Integer seqOperacaoColaborador = 1;
        for (VSdProgramacaoPendente operacaoProgramada : programacaoPendenteColaborador) {
            operacaoProgramada.getProgramacao().setSeqColaborador(seqOperacaoColaborador++);
        }
        
        Comparator<VSdProgramacaoPendente> comparator = Comparator.comparing(programacao -> programacao.getProgramacao().getSeqColaborador());
        programacaoPendenteColaborador.sort(comparator);
    }
    
    private Gauge initializeGauge(int value, String title) {
        return GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE_SECTION)
                .unit("%")
                .title(title)
                .value(value)
                .minHeight(125.0)
                .prefHeight(125.0)
                .maxHeight(125.0)
                .decimals(0)
                .animated(true)
                .minValue(0)
                .maxValue(150)
                .sections(new Section(0.0, 80.0, Color.RED), new Section(80.0, 90.0, Color.YELLOW), new Section(90.0, 150.0, Color.GREEN))
                .build();
    }
    
    private void limparPainelColaborador() {
        tboxColaborador.clear();
        tboxCelula.clear();
        tboxParadoAgora.clear();
        tboxParadoAgora.getStyleClass().remove("danger");
        tboxParadoAgora.getStyleClass().remove("success");
        paradaAbertaColaborador.set(null);
        boxGaugesColaborador.getChildren().clear();
        lancamentosColaborador.clear();
        paradasColaborador.clear();
        programacaoPendenteColaborador.clear();
    }
    
    private void abrirPainelColaborado(int codigoColaborador) throws SQLException {
        SdColaborador colaborador001 = (SdColaborador) daoColaborador.initCriteria().addPredicateEq("codigo", codigoColaborador).loadEntityByPredicate();
        SdCelula celula001 = ((SdColabCelula001) daoCelula.initCriteria().addPredicateEq("colaborador", codigoColaborador).loadEntityByPredicate()).getCelula();
        paradaAbertaColaborador.set((SdLancamentoParada001) daoLancamentoParadas.initCriteria()
                .addPredicateEqPkEmbedded("id", "colaborador", codigoColaborador, false)
                .addPredicateIsNull("dhFim")
                .loadEntityByPredicate());
        //Exibir infor
        tboxColaborador.setText(colaborador001.toString());
        tboxCelula.setText(celula001.toString());
        tboxParadoAgora.setText((paradaAbertaColaborador.get() != null ? paradaAbertaColaborador.get().getId().getMotivo().toString() : "TRABALHANDO"));
        tboxParadoAgora.getStyleClass().add((paradaAbertaColaborador.get() != null ? "danger" : "success"));
        
        //Exibir lancamentos
        exibirLancamentosColaborador(codigoColaborador);
        
        //Exibir gauges
        boxGaugesColaborador.getChildren().clear();
        Map<String, Object> producaoPeriodoColaborador = DAOFactory.getDashboardProducaoDAO().getProducaoColaboradorPeriodo(
                codigoColaborador,
                tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        );
        
        
        if (lancamentosColaborador.size() > 0) {
            SdLancamentoProducao001 ultimoLancamento = (SdLancamentoProducao001) lancamentosColaborador.get(lancamentosColaborador.getSize() - 1);
            boxGaugesColaborador.getChildren().add(initializeGauge(((Double) (ultimoLancamento.getEficiencia().doubleValue() * 100)).intValue(), "Ult. Oper. Lanç."));
        } else {
            boxGaugesColaborador.getChildren().add(initializeGauge(0, "Ult. Oper. Lanç."));
        }
        boxGaugesColaborador.getChildren().add(initializeGauge((Integer) producaoPeriodoColaborador.get("oper"), "Operacional"));
        boxGaugesColaborador.getChildren().add(initializeGauge((Integer) producaoPeriodoColaborador.get("efic"), "Eficiência"));
        boxGaugesColaborador.getChildren().add(initializeGauge((Integer) producaoPeriodoColaborador.get("prod"), "Produtividade"));
        
        //Exibir operações pendentes
        getProgramacaoPendente(codigoColaborador);
        
        tabsWindow.getSelectionModel().select(1);
    }
    
    private void exibirLancamentosColaborador(int codigoColaborador) throws SQLException {
        LocalDateTime inicioLancamentos = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(1);
        LocalDateTime fimLancamentos = LocalDateTime.now().with(tboxDtFimProd.getValue()).withHour(23);
        lancamentosColaborador.set(daoLancamentoProducao.initCriteria()
                .addPredicateEq("colaborador", codigoColaborador)
                .addPredicateBetween("dhInicio", inicioLancamentos, fimLancamentos)
                .addOrderByAsc("dhInicio")
                .loadListByPredicate());
        paradasColaborador.set(daoLancamentoParadas.initCriteria()
                .addPredicateEqPkEmbedded("id", "colaborador", codigoColaborador, false)
                .addPredicateBetween("dhFim", inicioLancamentos, fimLancamentos)
                .addOrderByAsc("dhFim")
                .loadListByPredicate());
    }
    
    private void getProgramacaoPendente(int codigoColaborador) throws SQLException {
        programacaoPendenteColaborador.set(daoProgramacoesPendentes
                .initCriteria()
                .addPredicateEq("colaborador", codigoColaborador)
                .addOrderByAsc("programacao", "seqColaborador")
                .loadListByPredicate());
        int indexOper = 1;
        for (VSdProgramacaoPendente vSdProgramacaoPendente : programacaoPendenteColaborador) {
            vSdProgramacaoPendente.getProgramacao().setSeqColaborador(indexOper);
            indexOper++;
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Actions Filtro">
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(produto -> produto.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }
    
    @FXML
    private void tboxDtInicioProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioProd.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimProd.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> procuraCelula = null;
        try {
            procuraCelula = new GenericFilter<SdCelula>() {
            };
            procuraCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(procuraCelula.selectedsReturn.stream().map(sdCelula001 -> String.valueOf(sdCelula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroColaboradorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarColaboradorOnAction(ActionEvent event) {
        GenericFilter<SdColaborador> procurarColaborador = null;
        try {
            procurarColaborador = new GenericFilter<SdColaborador>() {
            };
            procurarColaborador.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procurarColaborador.selectedsReturn.stream().map(sdColaborador001 -> String.valueOf(sdColaborador001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnCarregarProducaoOnAction(ActionEvent event) {
        try {
            JPAUtils.clearEntitys(registroConsulta);
            registroConsulta.set(DAOFactory.getVSdProdDiariaCelulaDAO().getProducaoColaborador(
                    tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"),
                    tboxFiltroCelula.getText(),
                    tboxFiltroOf.getText(),
                    tboxFiltroProduto.getText(),
                    tboxFiltroColaborador.getText()
            ));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        this.limparPainelColaborador();
        registroConsulta.clear();
        tboxFiltroProduto.clear();
        tboxFiltroCelula.clear();
        tboxFiltroOf.clear();
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(LocalDate.now());
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Actions MenuRequest">
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        try {
            new ExportExcel<VSdProdDiariaColaborador>(registroConsulta, VSdProdDiariaColaborador.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
        try {
            Map<String, Integer> opcoes = new HashMap<>();
            opcoes.put("De Colaborador/Lançamentos (Ano/Mês/Dia)", 1);
            opcoes.put("De Colaborador/Lançamentos (Resumido Dia)", 2);
            opcoes.put("De Cartão Resumo Dia Anterior", 3);
            //opcoes.put("Relatório de Paradas", 3);
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);
            
            // First, compile jrxml file.
            JasperReport jasperReport = null;
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            if (opcaoEscolhida == 1) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoColaborador.jasper"));
                
                parameters.put("_pDiaUtil", (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"));
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pDataFim", tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInNumeroOf", tboxFiltroOf.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 2) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoColaboradorSimples.jasper"));
                
                parameters.put("_pDiaUtil", (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"));
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pDataFim", tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInNumeroOf", tboxFiltroOf.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoColaboradorCartao.jasper"));
                
                parameters.put("_pData", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            }
            
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Actions Gestão">
    @FXML
    private void btnImprimirProgramacaoOnAction(ActionEvent event) {
        String colaboradoresSelecionados = tblProducao.getSelectionModel().getSelectedItems().stream()
                .map(vSdProdDiariaColaborador -> vSdProdDiariaColaborador.getCodColaborador())
                .distinct()
                .collect(Collectors.joining(","));
        
        try {
            Map<String, Integer> opcoes = new HashMap<>();
            opcoes.put("De Programação Pendente Colaborador", 1);
            opcoes.put("De Cartão de Programação", 2);
            //opcoes.put("Relatório de Paradas", 3);
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);
            
            // First, compile jrxml file.
            JasperReport jasperReport = null;
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            if (opcaoEscolhida == 1) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoColaborador.jasper"));
                parameters.put("colaborador", colaboradoresSelecionados);
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
                
            } else {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoColaboradorCartao.jasper"));
                
                parameters.put("colaborador", tblProducao.getItems().stream()
                        .map(vSdProdDiariaColaborador -> vSdProdDiariaColaborador.getCodColaborador())
                        .distinct()
                        .collect(Collectors.joining(",")));
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            }
            
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
        } catch (JRException | SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLancarParadaOnAction(ActionEvent event) {
        if (tboxFiltroCelula.getText().isEmpty() || tboxFiltroCelula.getText().split(",").length > 1) {
            GUIUtils.showMessage("Você deve filtrar uma célula somente, para realizar a operação.\nVocê deve também selecionar os colaboradores que serão cadastrado a parada.");
            return;
        }
        
        if (!GUIUtils.showQuestionMessageDefaultSim("Deseja realmente lançar uma parada em massa para os colaboradores selecionados?")) {
            return;
        }
        
        try {
            int colaboradoresSelecionados[] = tblProducao.getSelectionModel().getSelectedItems().stream()
                    .mapToInt(vSdProdDiariaColaborador -> Integer.parseInt(vSdProdDiariaColaborador.getCodColaborador()))
                    .distinct()
                    .toArray();
            
            Map<String, Integer> motivos = new HashMap<>();
            ObservableList<SdTiposParada001> motivosParadas = daoParadas.initCriteria().addPredicateEq("ativo", true).addOrderByAsc("descricao").loadListByPredicate();
            for (SdTiposParada001 motivoParada : motivosParadas) {
                motivos.put("[" + motivoParada.getCodigo() + "] " + motivoParada.getDescricao(), motivoParada.getCodigo());
            }
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Motivo da parada!", motivos);
//            LocalTime horaLancada = new NumericKeyboardController(Modals.FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.TIME, LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))).timeKeyboard
//                    .get();
            String operadoresNaoAbertoParada = "";
//            LocalDateTime horaInicioParada = LocalDateTime.now().with(horaLancada);
            LocalDateTime horaInicioParada = LocalDateTime.now();
            for (int colaboradoresSelecionado : colaboradoresSelecionados) {
                //Verifica se o colaborador está com uma parada aberta, se sim, não abre a parada em massa
                SdLancamentoParada001 paradaAberta = (SdLancamentoParada001) daoLancamentoParadas.initCriteria()
                        .addPredicate("id.colaborador", colaboradoresSelecionado, PredicateType.EQUAL)
                        .addPredicate("dhFim", null, PredicateType.IS_NULL)
                        .loadEntityByPredicate();
                if (paradaAberta != null) {
                    operadoresNaoAbertoParada += "Colab: " + colaboradoresSelecionado + " Parada: " + paradaAberta.getId().getMotivo().getCodigo() + "\n";
                    continue;
                }
                SdLancamentoParada001 sdLancamentoParada = new SdLancamentoParada001(
                        new SdLancamentoParada001PK(colaboradoresSelecionado, new SdTiposParada001(opcaoEscolhida), horaInicioParada)
                );
                daoLancamentoParadas.save(sdLancamentoParada);
            }
            if (operadoresNaoAbertoParada.isEmpty()) {
                GUIUtils.showMessageNotification("Inclusão em Massa", "Parada lançada para os colaboradores selecionados.");
            } else {
                GUIUtils.showMessage("Parada lançada para os colaboradores selecionados." +
                        "\n\nColaboradores com paradas em aberto: \n" + operadoresNaoAbertoParada);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnRetornarParadaOnAction(ActionEvent event) {
        if (tboxFiltroCelula.getText().isEmpty() || tboxFiltroCelula.getText().split(",").length > 1) {
            GUIUtils.showMessage("Você deve filtrar uma célula somente, para realizar a operação.\nVocê deve também selecionar os colaboradores que serão retornadas as paradas.");
            return;
        }
        
        if (!GUIUtils.showQuestionMessageDefaultSim("Deseja realmente retornar as paradas dos colaboradores selecionados?")) {
            return;
        }
        
        try {
            int colaboradoresSelecionados[] = tblProducao.getSelectionModel().getSelectedItems().stream()
                    .mapToInt(vSdProdDiariaColaborador -> Integer.parseInt(vSdProdDiariaColaborador.getCodColaborador()))
                    .distinct()
                    .toArray();
            
            Map<String, Integer> motivos = new HashMap<>();
            ObservableList<SdTiposParada001> motivosParadas = daoParadas.initCriteria().addPredicateEq("ativo", true).addOrderByAsc("descricao").loadListByPredicate();
            for (SdTiposParada001 motivoParada : motivosParadas) {
                motivos.put("[" + motivoParada.getCodigo() + "] " + motivoParada.getDescricao(), motivoParada.getCodigo());
            }
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Motivo da parada!", motivos);
//            LocalTime horaLida = new NumericKeyboardController(Modals.FXMLWindow.Keyboard, NumericKeyboardController.KeyboardType.TIME, LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))).timeKeyboard
//                    .get();
//
//            LocalDateTime horaFimParada = LocalDateTime.now().with(horaLida);
            String colaboradorNaoEncerradoParada = "";
            LocalDateTime horaFimParada = LocalDateTime.now();
            for (int colaboradoresSelecionado : colaboradoresSelecionados) {
                SdLancamentoParada001 sdLancamentoParada = (SdLancamentoParada001) daoLancamentoParadas.initCriteria()
                        .addPredicate("id.colaborador", colaboradoresSelecionado, PredicateType.EQUAL)
                        .addPredicate("id.motivo.codigo", opcaoEscolhida, PredicateType.EQUAL)
                        .addPredicate("dhFim", null, PredicateType.IS_NULL)
                        .loadEntityByPredicate();
                
                if (sdLancamentoParada != null) {
                    sdLancamentoParada.setDhFim(horaFimParada);
                    
                    SdLancamentoParada001 sdLancamentoParadaPeriodo = (SdLancamentoParada001) daoLancamentoParadas.initCriteria()
                            .addPredicate("id.colaborador", colaboradoresSelecionado, PredicateType.EQUAL)
                            .addPredicate("id.dhInicio", sdLancamentoParada.getId().getDhInicio(), PredicateType.GREATER_THAN_OR_EQUAL)
                            .addPredicate("dhFim", horaFimParada, PredicateType.LESS_THAN_OR_EQUAL)
                            .loadEntityByPredicate();
                    
                    if (sdLancamentoParadaPeriodo != null) {
                        colaboradorNaoEncerradoParada += "Colab: " + colaboradoresSelecionado + " com parada lançada no período. Parada: "
                                + sdLancamentoParadaPeriodo.getId().getMotivo().getCodigo()
                                + " de " + sdLancamentoParadaPeriodo.getId().getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + " à "
                                + sdLancamentoParadaPeriodo.getDhFim().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + ".\n";
                        continue;
                    }
                    
                    ObservableList<SdParadasTurno001> paradasTurno = daoParadasTurno.initCriteria()
                            .addPredicateEq("turno", 21)
                            .loadListByPredicate();
                    int tempoEmIntervalo = paradasTurno.stream()
                            .filter(sdParadasTurno001 -> (sdParadasTurno001.getInicio().toLocalTime().isAfter(sdLancamentoParada.getId().getDhInicio().toLocalTime())
                                    || sdParadasTurno001.getInicio().toLocalTime().equals(sdLancamentoParada.getId().getDhInicio().toLocalTime()))
                                    && (sdParadasTurno001.getFim().toLocalTime().isBefore(sdLancamentoParada.getDhFim().toLocalTime())
                                    || sdParadasTurno001.getFim().toLocalTime().equals(sdLancamentoParada.getDhFim().toLocalTime())))
                            .mapToInt(SdParadasTurno001::getTempoIntervalo)
                            .sum();
                    sdLancamentoParada.setTempo(new BigDecimal(ChronoUnit.MINUTES.between(sdLancamentoParada.getId().getDhInicio(), horaFimParada) - tempoEmIntervalo));
                    daoLancamentoParadas.update(sdLancamentoParada);
                } else {
                    colaboradorNaoEncerradoParada += "Colab: " + colaboradoresSelecionado + " sem parada aberta com motivo: " + opcaoEscolhida + ".\n";
                }
            }
            if (colaboradorNaoEncerradoParada.isEmpty()) {
                GUIUtils.showMessageNotification("Inclusão em Massa", "Retorno de parada lançada para os colaboradores selecionados.");
            } else {
                GUIUtils.showMessage("Retorno de parada lançada para os colaboradores selecionados." +
                        "\n\nColaboradores com parada não encerrada: \n" + colaboradorNaoEncerradoParada);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnAbrirPainelColaboradorOnAction(ActionEvent event) {
        try {
            int codigoColaborador = Integer.parseInt(tblProducao.getSelectionModel().getSelectedItem().getCodColaborador());
            this.abrirPainelColaborado(codigoColaborador);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Actions Painel Colaborador">
    @FXML
    private void btnConfirmarAlteracaoOnAction(ActionEvent event) {
        if (!GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja alterar a ordem das operações pendentes do colaborador?"))
            return;
        
        programacaoPendenteColaborador.forEach(vSdProgramacaoPendente -> {
            try {
                daoProgramacaoPacote.update(vSdProgramacaoPendente.getProgramacao());
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        });
    }
    
    @FXML
    private void btnVoltarPainelOnAction(ActionEvent event) {
        this.limparPainelColaborador();
        tabsWindow.getSelectionModel().select(0);
    }
    
    @FXML
    private void btnRetornarParadaColaboradorOnAction(ActionEvent event) {
        if (!GUIUtils.showQuestionMessageDefaultSim("Deseja realmente retornar a parada do colaborador?")) {
            return;
        }
        
        try {
            LocalDateTime horaFimParada = LocalDateTime.now();
            
            if (paradaAbertaColaborador != null) {
                paradaAbertaColaborador.get().setDhFim(horaFimParada);
                
                ObservableList<SdParadasTurno001> paradasTurno = daoParadasTurno.initCriteria()
                        .addPredicateEq("turno", 21)
                        .loadListByPredicate();
                int tempoEmIntervalo = paradasTurno.stream()
                        .filter(sdParadasTurno001 -> (sdParadasTurno001.getInicio().toLocalTime().isAfter(paradaAbertaColaborador.get().getId().getDhInicio().toLocalTime())
                                || sdParadasTurno001.getInicio().toLocalTime().equals(paradaAbertaColaborador.get().getId().getDhInicio().toLocalTime()))
                                && (sdParadasTurno001.getFim().toLocalTime().isBefore(paradaAbertaColaborador.get().getDhFim().toLocalTime())
                                || sdParadasTurno001.getFim().toLocalTime().equals(paradaAbertaColaborador.get().getDhFim().toLocalTime())))
                        .mapToInt(SdParadasTurno001::getTempoIntervalo)
                        .sum();
                paradaAbertaColaborador.get().setTempo(new BigDecimal(ChronoUnit.MINUTES.between(paradaAbertaColaborador.get().getId().getDhInicio(), horaFimParada) - tempoEmIntervalo));
                daoLancamentoParadas.update(paradaAbertaColaborador.get());
            }
            GUIUtils.showMessageNotification("Retorno de Parada", "Retorno de parada lançada para os colaboradores selecionados.");
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Actions Lançamento">
    @FXML
    private void btnRecalcularEficienciaOnAction(ActionEvent event) {
        if (lancamentoColaboradorSelecionado.get().getDhInicio().isBefore(LocalDateTime.parse("05/03/2020 00:00:00", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")))) {
            GUIUtils.showMessage("Lançamentos anteriores à 05/03/2020 não podem ser alterados ou recalculados.");
            lancamentoColaboradorSelecionado.get().setDhInicio(dhInicioLancamentoSelecionado);
            lancamentoColaboradorSelecionado.get().setDhFim(dhFimLancamentoSelecionado);
            
            return;
        }
        
        if (lancamentoColaboradorSelecionado.get() == null) {
            GUIUtils.showMessage("Você precisa selecionar um lançamento para recalcular a eficiência!");
            return;
        }
        
        if (lancamentoColaboradorSelecionado.get().getDhFim() == null || lancamentoColaboradorSelecionado.get().getDhInicio() == null) {
            GUIUtils.showMessage("Verifique as datas de início e fim do lançamento se estão no padrão \"DD/MM/AAAA HH:MM:SS\"");
            
            lancamentoColaboradorSelecionado.get().setDhInicio(dhInicioLancamentoSelecionado);
            lancamentoColaboradorSelecionado.get().setDhFim(dhFimLancamentoSelecionado);
            return;
        }
        
        try {
            ObservableList<SdLancamentoProducao001> lancamentosAgrupados = daoLancamentoProducao.initCriteria()
                    .addPredicate("lancAgrup", lancamentoColaboradorSelecionado.get().getLancAgrup(), PredicateType.EQUAL)
                    .loadListByPredicate();
            
            final LocalDateTime dhInicioLancamento = lancamentosAgrupados.stream()
                    .map(SdLancamentoProducao001::getDhInicio)
                    .min(LocalDateTime::compareTo)
                    .get();
            final LocalDateTime dhFimLancamento = lancamentosAgrupados.stream()
                    .map(SdLancamentoProducao001::getDhFim)
                    .max(LocalDateTime::compareTo)
                    .get();
            final Integer totalPecas = lancamentosAgrupados.stream()
                    .mapToInt(SdLancamentoProducao001::getQtde)
                    .sum();
            
            
            ObservableList<SdParadasTurno001> paradasTurno = daoParadasTurno.initCriteria()
                    .addPredicateEq("turno", 21)
                    .loadListByPredicate();
            int tempoEmIntervalo = paradasTurno.stream()
                    .filter(sdParadasTurno001 -> (sdParadasTurno001.getInicio().toLocalTime().isAfter(dhInicioLancamento.toLocalTime())
                            || sdParadasTurno001.getInicio().toLocalTime().equals(dhInicioLancamento.toLocalTime()))
                            && (sdParadasTurno001.getFim().toLocalTime().isBefore(dhFimLancamento.toLocalTime())
                            || sdParadasTurno001.getFim().toLocalTime().equals(dhFimLancamento.toLocalTime())))
                    .mapToInt(SdParadasTurno001::getTempoIntervalo)
                    .sum();
            ObservableList<SdLancamentoParada001> paradasPeriodo = daoLancamentoParadas.initCriteria()
                    .addPredicate("id.colaborador", lancamentoColaboradorSelecionado.get().getColaborador(), PredicateType.EQUAL)
                    .addPredicate("id.dhInicio", dhInicioLancamento, PredicateType.GREATER_THAN_OR_EQUAL)
                    .addPredicate("dhFim", dhFimLancamento, PredicateType.LESS_THAN_OR_EQUAL)
                    .loadListByPredicate();
            int tempoParadas = paradasPeriodo.stream()
                    .mapToInt(sdLancamentoParada001 -> sdLancamentoParada001.getTempo().intValue())
                    .sum();
            
            final long segundosIntervalo = ChronoUnit.SECONDS.between(dhInicioLancamento, dhFimLancamento);
            final double tempoRealizado = ((segundosIntervalo - (tempoEmIntervalo * 60) - (tempoParadas * 60)) / totalPecas.doubleValue()) / 60.0;
            
            for (SdLancamentoProducao001 sdLancamentoProducao001 : lancamentosAgrupados) {
                double eficCalculada = sdLancamentoProducao001.getTempoOper().doubleValue() / tempoRealizado;
                sdLancamentoProducao001.setEficiencia(BigDecimal.valueOf(eficCalculada > 2 ? 2 : eficCalculada));
                sdLancamentoProducao001.setTempoProd(BigDecimal.valueOf(tempoRealizado));
                daoLancamentoProducao.update(sdLancamentoProducao001);
            }
            
            GUIUtils.showMessageNotification("Recalculando Eficiência", "Recalculando a eficiência do lançamento e persistido no banco de dados.");
            tblLancamentosColaborador.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLiberarEdicaoLancamentoOnAction(ActionEvent event) {
        tboxDhInicioLancamento.setEditable(true);
        tboxDhFimLancamento.setEditable(true);
        editandoLancamento.set(true);
        dhInicioLancamentoSelecionado = lancamentoColaboradorSelecionado.get().getDhInicio();
        dhFimLancamentoSelecionado = lancamentoColaboradorSelecionado.get().getDhFim();
    }
    
    @FXML
    private void btnExcluirLancamentoOnAction(ActionEvent event) {
        SdLancamentoProducao001 lancamentoSelecionado = tblLancamentosColaborador.getSelectionModel().getSelectedItem();
        if (lancamentoSelecionado == null)
            return;
        
        if (!GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja excluir o lançamento selecionado?"))
            return;
        
        try {
            daoLancamentoProducao.delete(lancamentoSelecionado);
            lancamentosColaborador.remove(lancamentoSelecionado);
            JPAUtils.clearEntity(lancamentoSelecionado);
            
            VSdProgramacaoPendente programacaoPendente = (VSdProgramacaoPendente) daoProgramacoesPendentes.initCriteria()
                    .addPredicate("programacao.id.pacote", lancamentoSelecionado.getPacoteProg(), PredicateType.EQUAL)
                    .addPredicate("programacao.id.operacao.codorg", lancamentoSelecionado.getOperacao(), PredicateType.EQUAL)
                    .addPredicate("programacao.id.ordem", lancamentoSelecionado.getOrdem(), PredicateType.EQUAL)
                    .addPredicate("programacao.id.quebra", lancamentoSelecionado.getQuebra(), PredicateType.EQUAL)
                    .addPredicate("colaborador", lancamentoSelecionado.getColaborador(), PredicateType.EQUAL)
                    .loadEntityByPredicate();
            SdProgramacaoPacote002 programacaoPacote002 = (SdProgramacaoPacote002) daoProgramacaoPacote.initCriteria()
                    .addPredicate("id.pacote", lancamentoSelecionado.getPacoteProg(), PredicateType.EQUAL)
                    .addPredicate("id.operacao.codorg", lancamentoSelecionado.getOperacao(), PredicateType.EQUAL)
                    .addPredicate("id.ordem", lancamentoSelecionado.getOrdem(), PredicateType.EQUAL)
                    .addPredicate("id.quebra", lancamentoSelecionado.getQuebra(), PredicateType.EQUAL)
                    .addPredicate("celula", lancamentoSelecionado.getCelula(), PredicateType.EQUAL)
                    .loadEntityByPredicate();
            if (programacaoPacote002 != null && programacaoPendente != null) {
                if (programacaoPendente.getQtdePend() < programacaoPacote002.getQtde())
                    programacaoPacote002.setStatusProg(1);
                else
                    programacaoPacote002.setStatusProg(0);
                daoProgramacaoPacote.update(programacaoPacote002);
            }
            
            exibirLancamentosColaborador(lancamentoSelecionado.getColaborador());
            getProgramacaoPendente(lancamentoSelecionado.getColaborador());
            tblLancamentosColaborador.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnSalvaLancamentoOnAction(ActionEvent event) {
        
        try {
            ObservableList<SdLancamentoProducao001> lancamentosAgrupados = daoLancamentoProducao.initCriteria()
                    .addPredicate("lancAgrup", lancamentoColaboradorSelecionado.get().getLancAgrup(), PredicateType.EQUAL)
                    .addPredicate("id",lancamentoColaboradorSelecionado.get().getId(), PredicateType.NOT_EQUAL)
                    .loadListByPredicate();
            
            for (SdLancamentoProducao001 lancamentosAgrupado : lancamentosAgrupados) {
                lancamentosAgrupado.setDhInicio(LocalDateTime.now().with(lancamentoColaboradorSelecionado.get().getDhInicio().toLocalDate()).with(lancamentosAgrupado.getDhInicio().toLocalTime()));
                lancamentosAgrupado.setDhFim(LocalDateTime.now().with(lancamentoColaboradorSelecionado.get().getDhFim().toLocalDate()).with(lancamentosAgrupado.getDhFim().toLocalTime()));
                daoLancamentoProducao.update(lancamentosAgrupado);
            }
            daoLancamentoProducao.update(lancamentoColaboradorSelecionado.get());
            
            btnRecalcularEficienciaOnAction(null);
            tpaneApontamentoProducao.setExpanded(false);
            editandoLancamento.set(false);
            exibirLancamentosColaborador(lancamentoColaboradorSelecionado.get().getColaborador());
        }catch (SQLException e){
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnCancelarLancamentoOnAction(ActionEvent event) {
        tboxDhInicioLancamento.setEditable(false);
        tboxDhFimLancamento.setEditable(false);
        tpaneApontamentoProducao.setExpanded(false);
        editandoLancamento.set(false);
    }
    
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Actions Parada">
    @FXML
    private void btnLiberarEdicaoParadaOnAction(ActionEvent event) {
        tboxDhFimParada.setEditable(true);
        editandoParada.set(true);
        dhInicioLancamentoSelecionado = paradaColaboradorSelecionada.get().getId().getDhInicio();
        dhFimLancamentoSelecionado = paradaColaboradorSelecionada.get().getDhFim();
    }
    
    @FXML
    private void btnExcluirParadaOnAction(ActionEvent event) {
        SdLancamentoParada001 lancamentoSelecionado = tblParadasColaborador.getSelectionModel().getSelectedItem();
        if (lancamentoSelecionado == null)
            return;
        
        if (!GUIUtils.showQuestionMessageDefaultSim("Tem certeza que deseja excluir o lançamento selecionado?"))
            return;
        
        try {
            daoLancamentoParadas.delete(lancamentoSelecionado);
            paradasColaborador.remove(lancamentoSelecionado);
            tblParadasColaborador.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnSalvaParadaOnAction(ActionEvent event) {
        if (paradaColaboradorSelecionada.get() == null) {
            GUIUtils.showMessage("Você precisa selecionar um lançamento para recalcular a eficiência!");
            return;
        }
        
        if (paradaColaboradorSelecionada.get().getDhFim() == null || paradaColaboradorSelecionada.get().getId().getDhInicio() == null) {
            GUIUtils.showMessage("Verifique as datas de início e fim da parada se estão no padrão \"DD/MM/AAAA HH:MM:SS\"");
            paradaColaboradorSelecionada.get().getId().setDhInicio(dhInicioLancamentoSelecionado);
            paradaColaboradorSelecionada.get().setDhFim(dhFimLancamentoSelecionado);
            return;
        }
        
        try {
            SdLancamentoParada001 paradaToUpdate = (SdLancamentoParada001) daoLancamentoParadas.initCriteria()
                    .addPredicate("id.colaborador", paradaColaboradorSelecionada.get().getId().getColaborador(), PredicateType.EQUAL)
                    .addPredicate("id.motivo.codigo", paradaColaboradorSelecionada.get().getId().getMotivo().getCodigo(), PredicateType.EQUAL)
                    .addPredicate("id.dhInicio", dhInicioLancamentoSelecionado, PredicateType.EQUAL)
                    .loadEntityByPredicate();
            
            ObservableList<SdParadasTurno001> paradasTurno = daoParadasTurno.initCriteria()
                    .addPredicateEq("turno", 21)
                    .loadListByPredicate();
            int tempoEmIntervalo = paradasTurno.stream()
                    .filter(sdParadasTurno001 -> (sdParadasTurno001.getInicio().toLocalTime().isAfter(paradaColaboradorSelecionada.get().getId().getDhInicio().toLocalTime())
                            || sdParadasTurno001.getInicio().toLocalTime().equals(paradaColaboradorSelecionada.get().getId().getDhInicio().toLocalTime()))
                            && (sdParadasTurno001.getFim().toLocalTime().isBefore(paradaColaboradorSelecionada.get().getDhFim().toLocalTime())
                            || sdParadasTurno001.getFim().toLocalTime().equals(paradaColaboradorSelecionada.get().getDhFim().toLocalTime())))
                    .mapToInt(SdParadasTurno001::getTempoIntervalo)
                    .sum();
            paradaToUpdate.getId().setDhInicio(paradaColaboradorSelecionada.get().getId().getDhInicio());
            paradaToUpdate.setDhFim(paradaColaboradorSelecionada.get().getDhFim());
            paradaToUpdate.setTempo(new BigDecimal(ChronoUnit.MINUTES.between(paradaColaboradorSelecionada.get().getId().getDhInicio(), paradaColaboradorSelecionada.get().getDhFim()) - tempoEmIntervalo));
            daoLancamentoParadas.update(paradaToUpdate);
            
            GUIUtils.showMessageNotification("Lançamento de Parada", "Dados do lançamento da parada atualizado com sucesso.");
            tpaneApontamentoParada.setExpanded(false);
            editandoParada.set(false);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnCancelarParadaOnAction(ActionEvent event) {
        tboxDhInicioParada.setEditable(false);
        tboxDhFimParada.setEditable(false);
        tpaneApontamentoParada.setExpanded(false);
        editandoParada.set(false);
    }
    //</editor-fold>
    //</editor-fold>
    
}
