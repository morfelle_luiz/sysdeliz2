/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser.ExtensionFilter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.sysdeliz.SdPacoteLancamento001;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdProdHoraOperacao;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.PrintReportPreview;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProducaoOperacaoController implements Initializable {

    private final ListProperty<VSdProdHoraOperacao> registroConsulta = new SimpleListProperty<>();
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    private final GenericDao<VSdProdHoraOperacao> daoProducao = new GenericDaoImpl<>(VSdProdHoraOperacao.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML">
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private HBox hboxFiltro;
    @FXML
    private TextField tboxFiltroColaborador;
    @FXML
    private Button btnProcurarColaborador;
    @FXML
    private TextField tboxFiltroOperacao;
    @FXML
    private Button btnProcurarOperacao;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private TextField tboxFiltroHoraInicio;
    @FXML
    private TextField tboxFiltroHoraFim;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private DatePicker tboxDtInicioProd;
    @FXML
    private DatePicker tboxDtFimProd;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnCarregarProducao;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView Produção">
    @FXML
    private TableView<VSdProdHoraOperacao> tblProducao;
    @FXML
    private TableColumn<VSdProdHoraOperacao, LocalDate> clnData;
    @FXML
    private TableColumn<VSdProdHoraOperacao, SdCelula> clnCelula;
    @FXML
    private TableColumn<VSdProdHoraOperacao, SdColaborador> clnColaborador;
    @FXML
    private TableColumn<VSdProdHoraOperacao, LocalTime> clnHoraInicio;
    @FXML
    private TableColumn<VSdProdHoraOperacao, LocalTime> clnHoraFim;
    @FXML
    private TableColumn<VSdProdHoraOperacao, VSdDadosProduto> clnProduto;
    @FXML
    private TableColumn<VSdProdHoraOperacao, SdPacoteLancamento001> clnCor;
    @FXML
    private TableColumn<VSdProdHoraOperacao, SdOperacaoOrganize001> clnOperacao;
    @FXML
    private TableColumn<VSdProdHoraOperacao, BigDecimal> clnTmpOperacao;
    @FXML
    private TableColumn<VSdProdHoraOperacao, BigDecimal> clnTmpRealizado;
    @FXML
    private TableColumn<VSdProdHoraOperacao, BigDecimal> clnEficiencia;
    @FXML
    private TableColumn<VSdProdHoraOperacao, BigDecimal> clnTmpTotalOperacao;
    @FXML
    private TableColumn<VSdProdHoraOperacao, BigDecimal> clnTmpTotalRealizado;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbRegistersCount;
    //</editor-fold>
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponents();
    }

    private void initializeComponents() {

        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registroConsulta.sizeProperty()).concat(" registros"));
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TableView PRODUCAO">
        tblProducao.itemsProperty().bind(registroConsulta);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUCAO">
        clnData.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(StringUtils.toDateFormat(item));
                }
            }
        });
        clnEficiencia.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();

                if (item != null && !empty) {
                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                    if (item.doubleValue() < .8) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= .8 && item.doubleValue() < .9) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }

            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnHoraInicio.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, LocalTime>() {
            @Override
            protected void updateItem(LocalTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toTimeFormat(item));
                }
            }
        });
        clnHoraFim.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, LocalTime>() {
            @Override
            protected void updateItem(LocalTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toTimeFormat(item));
                }
            }
        });
        clnTmpOperacao.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpRealizado.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
        
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpTotalOperacao.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
        
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpTotalRealizado.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
        
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnCor.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, SdPacoteLancamento001>() {
            @Override
            protected void updateItem(SdPacoteLancamento001 item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
        
                if (item != null && !empty) {
                    if(item.getCor() != null)
                        setText("["+item.getCor().getCor()+"] "+item.getCor().getDescricao());
                }
            }
        });
        clnCor.setComparator(new Comparator<SdPacoteLancamento001>() {
            @Override
            public int compare(SdPacoteLancamento001 p1, SdPacoteLancamento001 p2) {
                return p1.getCor().getCor().compareTo(p2.getCor().getCor());
            }
        });
        clnProduto.setCellFactory(param -> new TableCell<VSdProdHoraOperacao, VSdDadosProduto>() {
            @Override
            protected void updateItem(VSdDadosProduto item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
            
                if (item != null && !empty) {
                    setText("["+item.getCodigo()+"] "+item.getDescricao());
                }
            }
        });
        clnProduto.setComparator(new Comparator<VSdDadosProduto>() {
            @Override
            public int compare(VSdDadosProduto p1, VSdDadosProduto p2) {
                return p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        clnCelula.setComparator(new Comparator<SdCelula>() {
            @Override
            public int compare(SdCelula p1, SdCelula p2) {
                return p1.getDescricao().compareTo(p2.getDescricao());
            }
        });
        clnColaborador.setComparator(new Comparator<SdColaborador>() {
            @Override
            public int compare(SdColaborador p1, SdColaborador p2) {
                return p1.getNome().compareTo(p2.getNome());
            }
        });
        clnOperacao.setComparator(new Comparator<SdOperacaoOrganize001>() {
            @Override
            public int compare(SdOperacaoOrganize001 p1, SdOperacaoOrganize001 p2) {
                return p1.getCodigo().compareTo(p2.getCodigo());
            }
        });
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TextBox FILTROS">
        TextFieldUtils.upperCase(tboxFiltroProduto);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DATA PICKERS">
        tboxDtInicioProd.setValue(LocalDate.now());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox HR INICIO/FIM">
        MaskTextField.timeField(tboxFiltroHoraInicio);
        MaskTextField.timeField(tboxFiltroHoraFim);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="MenuRequest EXPORT e PRINT DADOS">
        requestMenuExportarExcel.disableProperty().bind(registroConsulta.emptyProperty());
        requestMenuImprimirRelatorio.disableProperty().bind(registroConsulta.emptyProperty());
        // </editor-fold>
    }

    // <editor-fold defaultstate="collapsed" desc="Actions Filtro">
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(produto -> produto.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> procuraCelula = null;
        try {
            procuraCelula = new GenericFilter<SdCelula>() {
            };
            procuraCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(procuraCelula.selectedsReturn.stream().map(sdCelula001 -> String.valueOf(sdCelula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColaboradorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }

    @FXML
    private void tboxDtInicioProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioProd.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxDtFimProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimProd.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxFiltroHoraInicioOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraInicio.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        }
    }

    @FXML
    private void tboxFiltroHoraFimOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraFim.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        }
    }

    @FXML
    private void btnProcurarColaboradorOnAction(ActionEvent event) {
        GenericFilter<SdColaborador> procurarColaborador = null;
        try {
            procurarColaborador = new GenericFilter<SdColaborador>() {
            };
            procurarColaborador.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procurarColaborador.selectedsReturn.stream().map(sdColaborador001 -> String.valueOf(sdColaborador001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroOperacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarOperacaoOnAction(ActionEvent event) {
        GenericFilter<SdOperacaoOrganize001> procuraOperacao = null;
        try {
            procuraOperacao = new GenericFilter<SdOperacaoOrganize001>() {
            };
            procuraOperacao.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procuraOperacao.selectedsReturn.stream().map(sdOperacaoOrganize001 -> String.valueOf(sdOperacaoOrganize001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnCarregarProducaoOnAction(ActionEvent event) {
        try {
            GenericDao consulta = daoProducao.initCriteria();
            if(!tboxFiltroProduto.getText().isEmpty())
                consulta = consulta.addPredicate("referencia.codigo", tboxFiltroProduto.getText().split(","), PredicateType.IN);
            if(!tboxFiltroOperacao.getText().isEmpty())
                consulta = consulta.addPredicate("operacao.codigo", tboxFiltroOperacao.getText().split(","), PredicateType.IN);
            if(!tboxFiltroColaborador.getText().isEmpty())
                consulta = consulta.addPredicate("colaborador.codigo", tboxFiltroColaborador.getText().split(","), PredicateType.IN);
            if(!tboxFiltroCelula.getText().isEmpty())
                consulta = consulta.addPredicate("celula.codigo", tboxFiltroCelula.getText().split(","), PredicateType.IN);
            if(!tboxFiltroOf.getText().isEmpty())
                consulta = consulta.addPredicate("pacoteLanc.id.codigo", tboxFiltroOf.getText(), PredicateType.LIKE);
    
            LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
            LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
            if(tboxDtFimProd.getValue() != null)
                dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
            else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
            }
            consulta = consulta.addPredicateBetween("dhFim", dhInicio, dhFim);
    
            JPAUtils.clearEntitys(registroConsulta);
            registroConsulta.set(consulta.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        registroConsulta.clear();
        tboxFiltroProduto.clear();
        tboxFiltroColaborador.clear();
        tboxFiltroCelula.clear();
        tboxFiltroOf.clear();
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(null);
        tboxFiltroHoraInicio.clear();
        tboxFiltroHoraFim.clear();
        tboxFiltroOperacao.clear();
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Actions MenuRequest">
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        try {
            new ExportExcel<VSdProdHoraOperacao>(registroConsulta, VSdProdHoraOperacao.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
        try {
            Map<String, Integer> opcoes = new HashMap<>();
            opcoes.put("De Colaborador/Lançamentos", 1);
            opcoes.put("De Colaborador/Operações", 2);
            opcoes.put("De Operação/Colaborador", 3);
            opcoes.put("De Operação/Família/Colaborador", 4);
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);

            // First, compile jrxml file.
            JasperReport jasperReport = null;
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            if (opcaoEscolhida == 1) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoLancamentos.jasper"));
    
                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                if(tboxDtFimProd.getValue() != null)
                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 2) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacao.jasper"));
    
                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                if(tboxDtFimProd.getValue() != null)
                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 3) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupado.jasper"));
    
                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                if(tboxDtFimProd.getValue() != null)
                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 4) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupadoFamilia.jasper"));
    
                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                if(tboxDtFimProd.getValue() != null)
                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            }
            
            if (opcaoEscolhida != null) {
                JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
                new PrintReportPreview().showReport(print);
            }else{
                GUIUtils.showMessage("Escolha uma opção de layout de relatório.");
                return;
            }

        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");

        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    //</editor-fold>

}
