/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser.ExtensionFilter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdProdDiariaCelula;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.PrintReportPreview;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormToggleField;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProducaoPlantaController implements Initializable {
    
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private HBox hboxFiltro;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private DatePicker tboxDtInicioProd;
    @FXML
    private DatePicker tboxDtFimProd;
    @FXML
    private Button btnCarregar;
    @FXML
    private Button btnLimparFiltros;
    private FormToggleField toggleSomenteDiaUtil = new FormToggleField("Som. Dia Útil", true, false);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView">
    @FXML
    private TableView<VSdProdDiariaCelula> tblProducao;
    @FXML
    private TableColumn<VSdProdDiariaCelula, LocalDate> clnData;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnOperacao;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnEficiencia;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnProdutividade;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpDeduzEfic;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpProduzido;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpProgramado;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpProdutivo;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpProducao;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnTmpParado;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnEficienciaDia;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnProdutividadeDia;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnEficienciaMes;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnProdutividadeMes;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnEficienciaAno;
    @FXML
    private TableColumn<VSdProdDiariaCelula, Double> clnProdutividadeAno;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbCountRegistros;
    //</editor-fold>
    private final ListProperty<VSdProdDiariaCelula> registroConsulta = new SimpleListProperty<>();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponents();
    }
    
    private void initializeComponents() {
        
        // <editor-fold defaultstate="collapsed" desc="HBox FILTROS">
        hboxFiltro.getChildren().add(1, toggleSomenteDiaUtil);
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbCountRegistros.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registroConsulta.sizeProperty()).concat(" registros"));
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView PRODUCAO">
        tblProducao.itemsProperty().bind(registroConsulta);
        tblProducao.setRowFactory(tv -> new TableRow<VSdProdDiariaCelula>() {
            @Override
            protected void updateItem(VSdProdDiariaCelula item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (Integer.parseInt(item.getDia()) % 2 == 0) {
                    clearStyle();
                    getStyleClass().add("table-row-alternate-on");
                } else {
                    clearStyle();
                    getStyleClass().add("table-row-alternate-off");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-alternate-on");
                getStyleClass().remove("table-row-alternate-off");
            }
            
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUCAO">
        clnData.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, LocalDate>() {
            @Override
            protected void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (item != null && !empty) {
                    setText(StringUtils.toDateFormat(item));
                }
            }
        });
        clnOperacao.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficiencia.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividade.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item >= 80 && item < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficienciaDia.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividadeDia.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficienciaMes.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividadeMes.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnEficienciaAno.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnProdutividadeAno.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                
                if (item != null && !empty) {
                    setText(String.valueOf(item));
                    if (item.doubleValue() < 80.0) {
                        getStyleClass().add("danger");
                    } else if (item.doubleValue() >= 80 && item.doubleValue() < 90) {
                        getStyleClass().add("warning");
                    } else {
                        getStyleClass().add("success");
                    }
                }
            }
            
            void clear() {
                getStyleClass().remove("danger");
                getStyleClass().remove("warning");
                getStyleClass().remove("success");
            }
        });
        clnTmpDeduzEfic.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpParado.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProducao.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProdutivo.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProduzido.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnTmpProgramado.setCellFactory(param -> new TableCell<VSdProdDiariaCelula, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TextBox FILTROS">
        TextFieldUtils.upperCase(tboxFiltroProduto);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DATA PICKERS">
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(LocalDate.now());
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="MenuRequest EXPORT e PRINT DADOS">
        requestMenuExportarExcel.disableProperty().bind(registroConsulta.emptyProperty());
        requestMenuImprimirRelatorio.disableProperty().bind(registroConsulta.emptyProperty());
        // </editor-fold>
    }
    
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        }
    }
    
    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }
    
    @FXML
    private void tboxDtInicioProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioProd.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void tboxDtFimProdOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimProd.setValue(LocalDate.now());
        }
    }
    
    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(produto -> produto.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> procuraCelula = null;
        try {
            procuraCelula = new GenericFilter<SdCelula>() {
            };
            procuraCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(procuraCelula.selectedsReturn.stream().map(sdCelula001 -> String.valueOf(sdCelula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnCarregarOnAction(ActionEvent event) {
        try {
            JPAUtils.clearEntitys(registroConsulta);
            registroConsulta.set(DAOFactory.getVSdProdDiariaCelulaDAO().getProducaoCelula(
                    tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"),
                    tboxFiltroCelula.getText(),
                    tboxFiltroOf.getText(),
                    tboxFiltroProduto.getText()
            ));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        registroConsulta.clear();
        tboxFiltroProduto.clear();
        tboxFiltroCelula.clear();
        tboxFiltroOf.clear();
        tboxDtInicioProd.setValue(LocalDate.now());
        tboxDtFimProd.setValue(LocalDate.now());
    }
    
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        try {
            new ExportExcel<VSdProdDiariaCelula>(registroConsulta, VSdProdDiariaCelula.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
        try {
            Map<String, Integer> opcoes = new HashMap<>();
            opcoes.put("De Produção Ano/Mês/Dia/Célula", 1);
            opcoes.put("De Produção Célula/Dia", 2);
            opcoes.put("De Produção Célula/Periodo", 3);
            opcoes.put("De Produção Célula - Cartão", 4);
            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);
            
            // First, compile jrxml file.
            JasperReport jasperReport = null;
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            if (opcaoEscolhida == 1) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoPlanta.jasper"));
                
                parameters.put("_pDiaUtil", (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"));
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pDataFim", tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInNumeroOf", tboxFiltroOf.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 2) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoPlantaSimples.jasper"));
    
                parameters.put("_pDiaUtil", (toggleSomenteDiaUtil.isSwitchedOn.get() ? "S" : "S,N"));
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pDataFim", tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                parameters.put("_pInNumeroOf", tboxFiltroOf.getText());
                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 3) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoPlantaPeriodo.jasper"));
        
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pDataFim", tboxDtFimProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            } else if (opcaoEscolhida == 4) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoPlantaCartao.jasper"));
    
                parameters.put("_pDataIni", tboxDtInicioProd.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            }
            
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            new PrintReportPreview().showReport(print);
            
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
}