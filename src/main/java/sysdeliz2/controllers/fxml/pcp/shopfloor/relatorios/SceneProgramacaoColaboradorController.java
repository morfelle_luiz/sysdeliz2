package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.controllers.fxml.pcp.shopfloor.SceneProgramaProducaoOfController;
import sysdeliz2.controllers.fxml.procura.FilterCelulaController;
import sysdeliz2.controllers.fxml.procura.FilterColaboradorController;
import sysdeliz2.controllers.fxml.procura.FilterProdutoController;
import sysdeliz2.controllers.fxml.rh.ImpressaoFichaCtps;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.view.VSdImpProgColaborador;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.PrintReportPreview;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.sys.EnumsController;
import sysdeliz2.utils.sys.LocalLogger;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SceneProgramacaoColaboradorController implements Initializable {
    
    private final BooleanProperty impressaoPendente = new SimpleBooleanProperty(false);
    private final ListProperty<VSdImpProgColaborador> colaboradoresOcupados = new SimpleListProperty<>();
    private String wheresValues = "";
    
    // <editor-fold defaultstate="collapsed" desc="FXML components">
    @FXML
    private DatePicker tboxDtInicioProgramacao;
    
    @FXML
    private DatePicker tboxDtFimProgramacao;
    
    @FXML
    private TextField tboxFiltroOf;
    
    @FXML
    private TextField tboxFiltroSetor;
    
    @FXML
    private TextField tboxFiltroProduto;
    
    @FXML
    private Button btnProcurarProduto;
    
    @FXML
    private TextField tboxFiltroColaborador;
    
    @FXML
    private Button btnProcurarColaborador;
    
    @FXML
    private TextField tboxFiltroCelula;
    
    @FXML
    private Button btnProcurarCelula;
    
    @FXML
    private Button btnCarregar;
    
    @FXML
    private Button btnLimparFiltros;
    
    @FXML
    private Button btnImprimirAgenda;
    
    @FXML
    private Button btnReimprimirProgramacao;
    
    @FXML
    private TableView<VSdImpProgColaborador> tblColaborador;
    
    @FXML
    private Label lbCountColaboradores;
    
    // </editor-fold>
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            colaboradoresOcupados.set(DAOFactory.getVSdImpProgColaboradorDAO().getOfsParaProgramar(wheresValues));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
        }
        this.initializeComponents();
    }
    
    private void initializeComponents() {
        // <editor-fold defaultstate="collapsed" desc="TextField FILTRO">
        TextFieldUtils.upperCase(tboxFiltroCelula);
        TextFieldUtils.upperCase(tboxFiltroOf);
        TextFieldUtils.upperCase(tboxFiltroColaborador);
        TextFieldUtils.upperCase(tboxFiltroProduto);
        TextFieldUtils.upperCase(tboxFiltroSetor);
        TextFieldUtils.inSql(tboxFiltroCelula);
        TextFieldUtils.inSql(tboxFiltroOf);
        TextFieldUtils.inSql(tboxFiltroColaborador);
        TextFieldUtils.inSql(tboxFiltroProduto);
        TextFieldUtils.inSql(tboxFiltroSetor);
        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Button IMPRIMIR/REIMPRIMIR">
        btnImprimirAgenda.disableProperty().bind(tblColaborador.getSelectionModel().selectedItemProperty().isNull().or(impressaoPendente.not()));
        btnReimprimirProgramacao.disableProperty().bind(tblColaborador.getSelectionModel().selectedItemProperty().isNull());
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="TableView COLABORADORES">
        tblColaborador.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tblColaborador.itemsProperty().bind(colaboradoresOcupados);
        tblColaborador.setRowFactory(tv -> new TableRow<VSdImpProgColaborador>() {
            @Override
            protected void updateItem(VSdImpProgColaborador item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item == null) {
                    clearStyle();
                } else if (item.getStatus().equals("PENDENTE")) {
                    clearStyle();
                    getStyleClass().add("table-row-warning");
                } else if (item.getStatus().equals("PARCIAL")) {
                    clearStyle();
                    getStyleClass().add("table-row-info");
                }
            }
            
            private void clearStyle() {
                getStyleClass().remove("table-row-success");
                getStyleClass().remove("table-row-warning");
                getStyleClass().remove("table-row-danger");
                getStyleClass().remove("table-row-info");
            }
            
        });
        tblColaborador.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectItemTblColaborador(newValue);
            }
        });
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Label COUNTS COLABORADORES/PROGRAMACAO">
        lbCountColaboradores.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(colaboradoresOcupados.sizeProperty()).concat(" registros"));
        // </editor-fold>
    }
    
    private void selectItemTblColaborador(VSdImpProgColaborador linhaSelecionada) {
        impressaoPendente.set(false);
        if (linhaSelecionada.getStatus().equals("PENDENTE") || linhaSelecionada.getStatus().equals("PARCIAL")) {
            impressaoPendente.set(true);
        }
    }
    
    @FXML //Feito
    void btnCarregarOnAction(ActionEvent event) {
        String whereDataProgramacao = "";
        if (tboxDtInicioProgramacao.getValue() != null) {
            LocalDate dtInicio = tboxDtInicioProgramacao.getValue();
            LocalDate dtFim = tboxDtFimProgramacao.getValue() == null ? tboxDtInicioProgramacao.getValue() : tboxDtFimProgramacao.getValue();
            if (dtInicio.isAfter(dtFim)) {
                GUIUtils.showMessage("Verifique as datas, a data de início deve ser menor que a data .", Alert.AlertType.INFORMATION);
                return;
            } else {
                whereDataProgramacao += "and to_date(to_char(prog.dh_inicio,'DD/MM/YYYY'),'DD/MM/YYYY') between to_date('" + dtInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') and to_date('" + dtFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','DD/MM/YYYY') \n";
            }
        }
        
        wheresValues = "";
        wheresValues += tboxFiltroProduto.getLength() > 0 ? "and pct.referencia in (" + tboxFiltroProduto.getText() + ")\n" : "";
        wheresValues += tboxFiltroColaborador.getLength() > 0 ? "and col.codigo in (" + tboxFiltroColaborador.getText() + ")\n" : "";
        wheresValues += tboxFiltroOf.getLength() > 0 ? "and pof.ordem_prod in (" + tboxFiltroOf.getText() + ")\n" : "";
        wheresValues += tboxFiltroCelula.getLength() > 0 ? "and prog.celula in (" + tboxFiltroCelula.getText() + ")\n" : "";
        wheresValues += tboxFiltroSetor.getLength() > 0 ? "and pof.setor in (" + tboxFiltroSetor.getText() + ")\n" : "";
        wheresValues += whereDataProgramacao.length() > 0 ? whereDataProgramacao : "";
    
        try {
            colaboradoresOcupados.set(DAOFactory.getVSdImpProgColaboradorDAO().getOfsParaProgramar(wheresValues));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
        }
    }
    
    @FXML //Feito
    void btnImprimirAgendaOnAction(ActionEvent event) {
        try {
            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoColaborador.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();
        
            for(VSdImpProgColaborador colabImp : tblColaborador.getSelectionModel().getSelectedItems()) {
                if(colabImp.getStatus().equals("SEM PROGRAMACAO")){
                    continue;
                }
            
                parameters.clear();
                parameters.put("colaborador", colabImp.getCodigo());
                parameters.put("dh_inicio", tboxDtInicioProgramacao.getValue() != null ? tboxDtInicioProgramacao.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("dh_fim", tboxDtFimProgramacao.getValue() != null ? tboxDtFimProgramacao.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
            
                JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
                if(print.getPages().size() > 0) {
                    new PrintReportPreview().showReport(print);
                }else{
                    GUIUtils.showMessage("Operações para o período selecionado já foram impressos para o colaborador!");
                }
            }
    
            colaboradoresOcupados.set(DAOFactory.getVSdImpProgColaboradorDAO().getOfsParaProgramar(wheresValues));
        
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    
    @FXML //Feito
    void btnLimparFiltrosOnAction(ActionEvent event) {
        tboxFiltroCelula.clear();
        tboxFiltroOf.clear();
        tboxFiltroColaborador.clear();
        tboxFiltroSetor.clear();
        tboxFiltroProduto.clear();
        
        colaboradoresOcupados.clear();
    }
    
    @FXML //Feito
    void btnProcurarCelulaOnAction(ActionEvent event) {
        try {
            FilterCelulaController ctrlFilter = new FilterCelulaController(Modals.FXMLWindow.FilterCelula, FXCollections.observableArrayList(), EnumsController.ResultTypeFilter.MULTIPLE_RESULT);
            if (!ctrlFilter.returnValue.isEmpty()) {
                tboxFiltroCelula.setText(ctrlFilter.returnValue);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarProdutoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML //Feito
    void btnProcurarColaboradorOnAction(ActionEvent event) {
        try {
            FilterColaboradorController ctrlFilter = new FilterColaboradorController(Modals.FXMLWindow.FilterColaborador, FXCollections.observableArrayList(), EnumsController.ResultTypeFilter.MULTIPLE_RESULT);
            if (!ctrlFilter.returnValue.isEmpty()) {
                tboxFiltroColaborador.setText(ctrlFilter.returnValue);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarProdutoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML //Feito
    void btnProcurarProdutoOnAction(ActionEvent event) {
        try {
            FilterProdutoController ctrolFilterProduto = new FilterProdutoController(Modals.FXMLWindow.FilterProduto, tboxFiltroProduto.getText());
            if (!ctrolFilterProduto.returnValue.isEmpty()) {
                tboxFiltroProduto.setText(ctrolFilterProduto.returnValue);
            
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(SceneProgramaProducaoOfController.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController::btnProcurarProdutoOnAction");
            GUIUtils.showException(ex);
        }
    }
    
    @FXML //Feito
    void btnReimprimirProgramacaoOnAction(ActionEvent event) {
        try {
            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/ProgramacaoColaboradorReimpresso.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();
        
            for(VSdImpProgColaborador colabImp : tblColaborador.getSelectionModel().getSelectedItems()) {
                if(colabImp.getStatus().equals("SEM PROGRAMACAO")){
                    continue;
                }
                
                parameters.clear();
                parameters.put("colaborador", colabImp.getCodigo());
                parameters.put("dh_inicio", tboxDtInicioProgramacao.getValue() != null ? tboxDtInicioProgramacao.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                parameters.put("dh_fim", tboxDtFimProgramacao.getValue() != null ? tboxDtFimProgramacao.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    
                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(parameters);
    
                JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
                if(print.getPages().size() > 0) {
                    new PrintReportPreview().showReport(print);
                }else{
                    GUIUtils.showMessage("Operações para o período selecionado já foram re-impressos para o colaborador!");
                }
            }
        
        } catch (JRException | NullPointerException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(ImpressaoFichaCtps.class
                    .getName()).log(Level.SEVERE, null, ex);
            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
        }
    }
    
}
