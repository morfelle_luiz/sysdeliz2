/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.pcp.shopfloor.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdHorarioProgColab;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProgramacaoOperacaoController implements Initializable {

    private final ListProperty<VSdHorarioProgColab> registroConsulta = new SimpleListProperty<>();
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: DAOs">
    private final GenericDao<VSdHorarioProgColab> daoProducao = new GenericDaoImpl<>(VSdHorarioProgColab.class);
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: FXML">
    // <editor-fold defaultstate="collapsed" desc="FXML Filtro">
    @FXML
    private HBox hboxFiltro;
    @FXML
    private TextField tboxFiltroColaborador;
    @FXML
    private Button btnProcurarColaborador;
    @FXML
    private TextField tboxFiltroOperacao;
    @FXML
    private Button btnProcurarOperacao;
    @FXML
    private TextField tboxFiltroOf;
    @FXML
    private TextField tboxFiltroProduto;
    @FXML
    private Button btnProcurarProduto;
    @FXML
    private TextField tboxFiltroCelula;
    @FXML
    private TextField tboxFiltroHoraInicio;
    @FXML
    private TextField tboxFiltroHoraFim;
    @FXML
    private Button btnProcurarCelula;
    @FXML
    private DatePicker tboxDtInicioDe;
    @FXML
    private DatePicker tboxDtInicioAte;
    @FXML
    private DatePicker tboxDtFimDe;
    @FXML
    private DatePicker tboxDtFimAte;
    @FXML
    private Button btnLimparFiltros;
    @FXML
    private Button btnCarregarProducao;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FXML TableView Produção">
    @FXML
    private TableView<VSdHorarioProgColab> tblProducao;
    @FXML
    private TableColumn<VSdHorarioProgColab, SdCelula> clnCelula;
    @FXML
    private TableColumn<VSdHorarioProgColab, SdColaborador> clnColaborador;
    @FXML
    private TableColumn<VSdHorarioProgColab, LocalDateTime> clnHoraInicio;
    @FXML
    private TableColumn<VSdHorarioProgColab, LocalDateTime> clnHoraFim;
    @FXML
    private TableColumn<VSdHorarioProgColab, VSdDadosProduto> clnProduto;
    @FXML
    private TableColumn<VSdHorarioProgColab, VSdDadosProduto> clnFamilia;
    @FXML
    private TableColumn<VSdHorarioProgColab, BigDecimal> clnTempoTotal;
    @FXML
    private TableColumn<VSdHorarioProgColab, SdOperacaoOrganize001> clnOperacao;
    @FXML
    private TableColumn<VSdHorarioProgColab, Boolean> clnStatus;
    @FXML
    private MenuItem requestMenuExportarExcel;
    @FXML
    private MenuItem requestMenuImprimirRelatorio;
    @FXML
    private Label lbRegistersCount;
    //</editor-fold>
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initializeComponents();
    }

    private void initializeComponents() {

        // <editor-fold defaultstate="collapsed" desc="Label COUNT REGISTROS">
        lbRegistersCount.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(registroConsulta.sizeProperty()).concat(" registros"));
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TableView PRODUCAO">
        tblProducao.itemsProperty().bind(registroConsulta);
        tblProducao.setRowFactory(tv -> new TableRow<VSdHorarioProgColab>() {
            @Override
            protected void updateItem(VSdHorarioProgColab item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item != null && !empty && item.isInWork())
                    getStyleClass().add("table-row-success");
            }
        
            private void clearStyle() {
                getStyleClass().remove("table-row-success");
            }
        
        });
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TableColumn PRODUCAO">
        clnHoraInicio.setCellFactory(param -> new TableCell<VSdHorarioProgColab, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toShortDateTimeFormat(item));
                }
            }
        });
        clnHoraFim.setCellFactory(param -> new TableCell<VSdHorarioProgColab, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);

                if (item != null && !empty) {
                    setText(StringUtils.toShortDateTimeFormat(item));
                }
            }
        });
        clnTempoTotal.setCellFactory(param -> new TableCell<VSdHorarioProgColab, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
        
                if (item != null && !empty) {
                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                }
            }
        });
        clnProduto.setCellFactory(param -> new TableCell<VSdHorarioProgColab, VSdDadosProduto>() {
            @Override
            protected void updateItem(VSdDadosProduto item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
            
                if (item != null && !empty) {
                    setText("["+item.getCodigo()+"] "+item.getDescricao());
                }
            }
        });
        clnFamilia.setCellFactory(param -> new TableCell<VSdHorarioProgColab, VSdDadosProduto>() {
            @Override
            protected void updateItem(VSdDadosProduto item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
            
                if (item != null && !empty) {
                    setText("["+item.getCodFam()+"] "+item.getFamilia());
                }
            }
        });
        clnStatus.setCellFactory(param -> new TableCell<VSdHorarioProgColab, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                setGraphic(null);
                
                if (item != null && !empty) {
                    if (item)
                        setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/active (1).png").toExternalForm())));
                    else
                        setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/inactive (1).png").toExternalForm())));
                }
            }
        });
        clnProduto.setComparator((p1, p2) -> p1.getCodigo().compareTo(p2.getCodigo()));
        clnFamilia.setComparator((p1, p2) -> p1.getFamilia().compareTo(p2.getFamilia()));
        clnCelula.setComparator((p1, p2) -> p1.getDescricao().compareTo(p2.getDescricao()));
        clnColaborador.setComparator((p1, p2) -> p1.getNome().compareTo(p2.getNome()));
        clnOperacao.setComparator((p1, p2) -> p1.getCodigo().compareTo(p2.getCodigo()));
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="TextBox FILTROS">
        TextFieldUtils.upperCase(tboxFiltroProduto);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox DATA PICKERS">
        tboxDtInicioDe.setValue(LocalDate.now());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TextBox HR INICIO/FIM">
        MaskTextField.timeField(tboxFiltroHoraInicio);
        MaskTextField.timeField(tboxFiltroHoraFim);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="MenuRequest EXPORT e PRINT DADOS">
        requestMenuExportarExcel.disableProperty().bind(registroConsulta.emptyProperty());
        requestMenuImprimirRelatorio.disableProperty().bind(registroConsulta.emptyProperty());
        // </editor-fold>
    }

    // <editor-fold defaultstate="collapsed" desc="Actions Filtro">
    @FXML
    private void tboxFiltroProdutoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarProdutoOnAction(null);
        }
    }

    @FXML
    private void btnProcurarProdutoOnAction(ActionEvent event) {
        GenericFilter<Produto> procuraProduto = null;
        try {
            procuraProduto = new GenericFilter<Produto>() {
            };
            procuraProduto.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroProduto.setText(procuraProduto.selectedsReturn.stream().map(produto -> produto.getCodigo()).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroCelulaOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarCelulaOnAction(null);
        }
    }

    @FXML
    private void btnProcurarCelulaOnAction(ActionEvent event) {
        GenericFilter<SdCelula> procuraCelula = null;
        try {
            procuraCelula = new GenericFilter<SdCelula>() {
            };
            procuraCelula.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroCelula.setText(procuraCelula.selectedsReturn.stream().map(sdCelula001 -> String.valueOf(sdCelula001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void tboxFiltroColaboradorOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }

    @FXML
    private void tboxDtInicioDeOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioDe.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxDtInicioAteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtInicioAte.setValue(LocalDate.now());
        }
    }

    @FXML
    private void tboxDtFimDeOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimDe.setValue(LocalDate.now());
        } else if(event.getCode() == KeyCode.C) {
            tboxDtFimDe.setValue(tboxDtInicioDe.getValue());
        }
    }

    @FXML
    private void tboxDtFimAteOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxDtFimAte.setValue(LocalDate.now());
        } else if(event.getCode() == KeyCode.C) {
            tboxDtFimAte.setValue(tboxDtInicioAte.getValue());
        }
    }

    @FXML
    private void tboxFiltroHoraInicioOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraInicio.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        }
    }

    @FXML
    private void tboxFiltroHoraFimOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.T) {
            tboxFiltroHoraFim.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        }
    }

    @FXML
    private void btnProcurarColaboradorOnAction(ActionEvent event) {
        GenericFilter<SdColaborador> procurarColaborador = null;
        try {
            procurarColaborador = new GenericFilter<SdColaborador>() {
            };
            procurarColaborador.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procurarColaborador.selectedsReturn.stream().map(sdColaborador001 -> String.valueOf(sdColaborador001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    @FXML
    private void tboxFiltroOperacaoOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.F4) {
            btnProcurarColaboradorOnAction(null);
        }
    }
    
    @FXML
    private void btnProcurarOperacaoOnAction(ActionEvent event) {
        GenericFilter<SdOperacaoOrganize001> procuraOperacao = null;
        try {
            procuraOperacao = new GenericFilter<SdOperacaoOrganize001>() {
            };
            procuraOperacao.show(ResultTypeFilter.MULTIPLE_RESULT);
            tboxFiltroColaborador.setText(procuraOperacao.selectedsReturn.stream().map(sdOperacaoOrganize001 -> String.valueOf(sdOperacaoOrganize001.getCodigo())).collect(Collectors.joining(",")));
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnCarregarProducaoOnAction(ActionEvent event) {
        try {
            GenericDao consulta = daoProducao.initCriteria();
            if(!tboxFiltroProduto.getText().isEmpty())
                consulta = consulta.addPredicate("referencia.codigo", tboxFiltroProduto.getText().split(","), PredicateType.IN);
            if(!tboxFiltroOperacao.getText().isEmpty())
                consulta = consulta.addPredicate("operacao.codigo", tboxFiltroOperacao.getText().split(","), PredicateType.IN);
            if(!tboxFiltroColaborador.getText().isEmpty())
                consulta = consulta.addPredicate("colaborador.codigo", tboxFiltroColaborador.getText().split(","), PredicateType.IN);
            if(!tboxFiltroCelula.getText().isEmpty())
                consulta = consulta.addPredicate("celula.codigo", tboxFiltroCelula.getText().split(","), PredicateType.IN);
            if(!tboxFiltroOf.getText().isEmpty())
                consulta = consulta.addPredicate("pacote", tboxFiltroOf.getText(), PredicateType.LIKE);
            if(tboxDtInicioDe != null) {
                LocalDateTime dhInicioDe = LocalDateTime.now().with(tboxDtInicioDe.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhInicioAte = LocalDateTime.now().with(tboxDtInicioDe.getValue()).withHour(23).withMinute(59).withSecond(59);
                if (tboxDtInicioAte.getValue() != null)
                    dhInicioAte = dhInicioAte.with(tboxDtInicioAte.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhInicioDe = dhInicioDe.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhInicioAte = dhInicioAte.with(tboxDtInicioDe.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                consulta = consulta.addPredicateBetween("dhInicio", dhInicioDe, dhInicioAte);
            }
            if(tboxDtFimDe != null){
                LocalDateTime dhFimDe = LocalDateTime.now().with(tboxDtInicioDe.getValue()).withHour(0).withMinute(0).withSecond(0);
                LocalDateTime dhFimAte = LocalDateTime.now().with(tboxDtInicioDe.getValue()).withHour(23).withMinute(59).withSecond(59);
                if(tboxDtInicioAte.getValue() != null)
                    dhFimAte = dhFimAte.with(tboxDtInicioAte.getValue()).withHour(23).withMinute(59).withSecond(59);
                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
                    dhFimDe = dhFimDe.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                    dhFimAte = dhFimAte.with(tboxDtInicioDe.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                }
                consulta = consulta.addPredicateBetween("dhFim", dhFimDe, dhFimAte);
            }
            
            consulta = consulta.addOrderByAsc("dhFim");
    
            JPAUtils.clearEntitys(registroConsulta);
            registroConsulta.set(consulta.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void btnLimparFiltrosOnAction(ActionEvent event) {
        registroConsulta.clear();
        tboxFiltroProduto.clear();
        tboxFiltroColaborador.clear();
        tboxFiltroCelula.clear();
        tboxFiltroOf.clear();
        tboxDtInicioDe.setValue(LocalDate.now());
        tboxDtInicioAte.setValue(null);
        tboxDtFimDe.setValue(null);
        tboxDtFimAte.setValue(null);
        tboxFiltroHoraInicio.clear();
        tboxFiltroHoraFim.clear();
        tboxFiltroOperacao.clear();
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Actions MenuRequest">
    @FXML
    private void requestMenuExportarExcelOnAction(ActionEvent event) {
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new ExtensionFilter("Arquivos do Excel", "*.xlsx"))));
        try {
            new ExportExcel<VSdHorarioProgColab>(registroConsulta, VSdHorarioProgColab.class).exportArrayToExcel(filePath);
            GUIUtils.showMessage("Exportação realizada com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    @FXML
    private void requestMenuImprimirRelatorioOnAction(ActionEvent event) {
//        try {
//            Map<String, Integer> opcoes = new HashMap<>();
//            opcoes.put("De Colaborador/Lançamentos", 1);
//            opcoes.put("De Colaborador/Operações", 2);
//            opcoes.put("De Operação/Colaborador", 3);
//            opcoes.put("De Operação/Família/Colaborador", 4);
//            Integer opcaoEscolhida = GUIUtils.showComboBoxDialog("Relatório:", opcoes);
//
//            // First, compile jrxml file.
//            JasperReport jasperReport = null;
//            HashMap<String, Object> parameters = new HashMap<String, Object>();
//            if (opcaoEscolhida == 1) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoLancamentos.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
//                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 2) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacao.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
//                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 3) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupado.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
//                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            } else if (opcaoEscolhida == 4) {
//                jasperReport = (JasperReport) JRLoader
//                        .loadObject(getClass().getResourceAsStream("/relatorios/RelatorioProducaoOperacaoAgrupadoFamilia.jasper"));
//
//                LocalDateTime dhInicio = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(0).withMinute(0).withSecond(0);
//                LocalDateTime dhFim = LocalDateTime.now().with(tboxDtInicioProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                if(tboxDtFimProd.getValue() != null)
//                    dhFim = dhFim.with(tboxDtFimProd.getValue()).withHour(23).withMinute(59).withSecond(59);
//                else if (!tboxFiltroHoraInicio.getText().isEmpty() && !tboxFiltroHoraFim.getText().isEmpty()) {
//                    dhInicio = dhInicio.with(LocalTime.parse(tboxFiltroHoraInicio.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                    dhFim = dhFim.with(tboxDtInicioProd.getValue()).with(LocalTime.parse(tboxFiltroHoraFim.getText(), DateTimeFormatter.ofPattern("HH:mm")));
//                }
//                parameters.put("_pDataIni", dhInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pDataFim", dhFim.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//                parameters.put("_pInCodigoCelula", tboxFiltroCelula.getText());
//                parameters.put("_pInCodigoOperacao", tboxFiltroOperacao.getText());
//                parameters.put("_pInReferencia", tboxFiltroProduto.getText());
//                parameters.put("_pInColaborador", tboxFiltroColaborador.getText());
//                ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
//                list.add(parameters);
//            }
//
//            if (opcaoEscolhida != null) {
//                JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getDelizConnection());
//                new PrintReportPreview().showReport(print);
//            }else{
//                GUIUtils.showMessage("Escolha uma opção de layout de relatório.");
//                return;
//            }
//
//        } catch (JRException | NullPointerException ex) {
//            ex.printStackTrace();
//            Logger.getLogger(ImpressaoFichaCtps.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            LocalLogger.addLog(ex.getMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
//
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            Logger.getLogger(ImpressaoFichaCtps.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "SceneProgramaProducaoOfController:btnImprimirProgramacaoOnAction");
//        }
    }
    //</editor-fold>

}
