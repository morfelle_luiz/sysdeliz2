package sysdeliz2.controllers.fxml.procura;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdColaboradorSenior001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilterColaboradorSeniorController extends Modals implements Initializable {

    public SdColaboradorSenior001 colaborador;

    private String whereClause = "";

    @FXML
    private ComboBox<String> cboxTipoFiltro;
    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<SdColaboradorSenior001> tblFilters;
    @FXML
    private Button btnLoad;

    /**
     * Initializes the controller class.
     */
    public FilterColaboradorSeniorController(Modals.FXMLWindow file) throws IOException {
        super(file);
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            tblFilters.setItems(FXCollections.observableList(DAOFactory.getSdColaboradorSenior001DAO().getAll()));
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(false);
        cboxTipoFiltro.setItems(FXCollections.observableArrayList("Nome Func", "Empresa", "Setor"));
        cboxTipoFiltro.getSelectionModel().selectFirst();
        tboxValorFiltro.requestFocus();

        TextFieldUtils.upperCase(tboxValorFiltro);
    }

    private void createWhereClause() {
        if(!tboxValorFiltro.getText().equals("")){
            switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
                case "Nome Func":
                    whereClause = "f.NOMFUN like '%" + tboxValorFiltro.getText().trim() + "%'";
                    break;
                case "Empresa":
                    whereClause = "(SELECT e.nomemp FROM SENIOR_IND.r030emp e WHERE e.NUMEMP = f.NUMEMP) like '%" + tboxValorFiltro.getText().trim() + "%'";
                    break;
                case "Setor":
                    whereClause = "(select NOMLOC from senior_ind.r016orn WHERE NUMLOC = f.NUMLOC) like '%" + tboxValorFiltro.getText().trim() + "%'";
                    break;
            }
        } else {
            whereClause = "";
        }
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {}

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            if (tboxValorFiltro.getText().isEmpty()) {
                return;
            }
            this.createWhereClause();

            tblFilters.setItems(DAOFactory.getSdColaboradorSenior001DAO().getBy(whereClause));
            tboxValorFiltro.clear();
            cboxTipoFiltro.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        colaborador = tblFilters.getSelectionModel().getSelectedItem();
        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            this.btnUseThisOnAction(null);
        }
    }

}
