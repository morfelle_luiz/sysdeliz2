package sysdeliz2.controllers.fxml.procura;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilterEstadosController extends Modals implements Initializable {

    public TabUf tabUf;
    private String whereClause = "";
    private static GenericDao<TabUf> dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), TabUf.class);

    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<TabUf> tblFilters;
    @FXML
    private Button btnLoad;
    @FXML
    private TableColumn<TabUf, String> clnUfSigla;
    @FXML
    private TableColumn<TabUf, String> clnUfCodigo;

    /**
     * Initializes the controller class.
     */
    public FilterEstadosController(FXMLWindow file) throws IOException {
        super(file);
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            tblFilters.setItems(dao.list());
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(false);
        tboxValorFiltro.requestFocus();

        TextFieldUtils.upperCase(tboxValorFiltro);

        clnUfCodigo.setCellValueFactory(param -> param.getValue().getId().codigoProperty());
        clnUfSigla.setCellValueFactory(param -> param.getValue().getId().siglaEstProperty());
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {}

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            dao.clearPredicate();
            if (tboxValorFiltro.getText().isEmpty()) {
                tblFilters.setItems(dao.list());
            } else {
                dao.addPredicateLike("descricao", tboxValorFiltro.getText());
                tblFilters.setItems(dao.loadListByPredicate());
            }

            tboxValorFiltro.clear();
            tboxValorFiltro.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        tabUf = tblFilters.getSelectionModel().getSelectedItem();
        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            this.btnUseThisOnAction(null);
        }
    }

}
