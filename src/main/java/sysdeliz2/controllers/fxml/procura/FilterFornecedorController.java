/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.controlsfx.control.table.TableFilter;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Cliente;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.sys.EnumsController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterFornecedorController extends Modals implements Initializable {

    public String returnValue = "";
    private String selecteds = "";
    private List<ObservableList<String>> whereClause = new ArrayList<>();
    private final BooleanProperty singleResult = new SimpleBooleanProperty(true);

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private Button btnWhereAnd;
    @FXML
    private Button btnWhereOr;
    @FXML
    private ComboBox<String> cboxRelacaoColuna;
    @FXML
    private ComboBox<String> cboxTipoFiltro;
    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private ListView<String> listWheres;
    @FXML
    private Button btnCheckAll;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<Cliente> tblFilters;
    @FXML
    private TableColumn<Cliente, Boolean> clmSelected;
    @FXML
    private Button btnFindFilter;
    @FXML
    private Button btnLoad;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    public FilterFornecedorController(FXMLWindow file, String selectedClients, EnumsController.ResultTypeFilter resultType) throws IOException {
        super(file);
        this.selecteds = resultType == EnumsController.ResultTypeFilter.MULTIPLE_RESULT ? selectedClients : "";
        this.singleResult.set(resultType == EnumsController.ResultTypeFilter.SINGLE_RESULT);
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnCheckAll.disableProperty().bind(singleResult);
        tblFilters.setEditable(true);
        clmSelected.editableProperty().bind(singleResult.not());
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setCellValueFactory((cellData) -> {
            Cliente cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
            return property;
        });

        cboxRelacaoColuna.setItems(FXCollections.observableArrayList("Igual", "Diferente"));
        cboxRelacaoColuna.getSelectionModel().selectFirst();

        cboxTipoFiltro.setItems(FXCollections.observableArrayList("Código", "Nome", "CNPJ", "Cidade", "Região"));
        cboxTipoFiltro.valueProperty().addListener((observable, oldValue, newValue) -> {
            btnFindFilter.setVisible(false);
            tboxValorFiltro.setEditable(true);
            if (newValue.equals("Cidade")) {
                btnFindFilter.setVisible(true);
                tboxValorFiltro.setEditable(false);
            }
        });
        cboxTipoFiltro.getSelectionModel().selectFirst();
        tboxValorFiltro.requestFocus();
        try {
            tblFilters.setItems(DAOFactory.getClienteDAO().getAllFornecedor());
        } catch (SQLException ex) {
            Logger.getLogger(FilterFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        if (singleResult.get()) {
            tblFilters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (oldValue != null) {
                    oldValue.setSelected(false);
                }
                if (newValue != null) {
                    newValue.setSelected(true);
                }
            });
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Controller methods">
    private void createWhereClause(String relationClause) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Código":
                whereClause.add(0, FXCollections.observableArrayList(relationClause,
                        "cli.codcli",
                        cboxRelacaoColuna.getSelectionModel().selectedItemProperty().get().equals("Igual") ? "=" : "<>",
                        tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou")
                        + " código "
                        + Bindings.when(cboxRelacaoColuna.valueProperty().isEqualTo("Igual")).then("igual ").otherwise("não igual ").get()
                        + tboxValorFiltro.getText());
                break;
            case "Nome":
                whereClause.add(0, FXCollections.observableArrayList(relationClause,
                        "cli.nome",
                        cboxRelacaoColuna.getSelectionModel().selectedItemProperty().get().equals("Igual") ? "like" : "not like",
                        tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou")
                        + " nome "
                        + Bindings.when(cboxRelacaoColuna.valueProperty().isEqualTo("Igual")).then("contém ").otherwise("não contém ").get()
                        + tboxValorFiltro.getText());
                break;
            case "Documento":
                whereClause.add(0, FXCollections.observableArrayList(relationClause,
                        "cli.cnpj",
                        cboxRelacaoColuna.getSelectionModel().selectedItemProperty().get().equals("Igual") ? "=" : "<>",
                        tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou")
                        + " documento "
                        + Bindings.when(cboxRelacaoColuna.valueProperty().isEqualTo("Igual")).then("igual ").otherwise("não igual ").get()
                        + tboxValorFiltro.getText());
                break;
            case "Cidade":
                whereClause.add(0, FXCollections.observableArrayList(relationClause,
                        "cid.cod_cid",
                        cboxRelacaoColuna.getSelectionModel().selectedItemProperty().get().equals("Igual") ? "in" : "not in",
                        tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou")
                        + " código cidade "
                        + Bindings.when(cboxRelacaoColuna.valueProperty().isEqualTo("Igual")).then("igual ").otherwise("não igual ").get()
                        + tboxValorFiltro.getText().replace(",", " ou "));
                break;
            case "Região":
                whereClause.add(0, FXCollections.observableArrayList(relationClause,
                        "uf.regiao",
                        cboxRelacaoColuna.getSelectionModel().selectedItemProperty().get().equals("Igual") ? "like" : "not like",
                        tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou")
                        + " região "
                        + Bindings.when(cboxRelacaoColuna.valueProperty().isEqualTo("Igual")).then("contém ").otherwise("não contém ").get()
                        + tboxValorFiltro.getText().replace(",", " ou "));
                break;

        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    private void btnWhereAndOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("and");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnWhereOrOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("or");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Cidade":
                try {
                    FilterCidadeController ctrolFilterColecao = new FilterCidadeController(Modals.FXMLWindow.FilterCidade, tboxValorFiltro.getText());
                    if (!ctrolFilterColecao.returnValue.isEmpty()) {
                        tboxValorFiltro.setText(ctrolFilterColecao.returnValue);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
                break;

        }
    }

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            if (tboxValorFiltro.getText().isEmpty()) {
                return;
            }
            this.createWhereClause("");
            tblFilters.setItems(DAOFactory.getClienteDAO().getFornecedoresFromFilter(whereClause));

            listWheres.getItems().clear();
            whereClause.clear();
            tboxValorFiltro.clear();
            cboxTipoFiltro.requestFocus();

            TableFilter filter = new TableFilter(tblFilters);
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            rowTable.selectedProperty().set(!rowTable.selectedProperty().get());
        });
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            if (rowTable.selectedProperty().get()) {
                returnValue += "'" + rowTable.getCodcli() + "',";
            }
        });
        if (!selecteds.isEmpty()) {
            returnValue = selecteds + "," + returnValue;
        }
        if (returnValue.length() > 0) {
            returnValue = returnValue.trim().substring(0, returnValue.length() - 1);
        }

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    @FXML
    private void listWheresOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            int positionWhereClause = listWheres.getSelectionModel().getSelectedIndex();
            listWheres.getItems().remove(positionWhereClause);
            whereClause.remove(positionWhereClause);
        }
    }
    //</editor-fold>

}
