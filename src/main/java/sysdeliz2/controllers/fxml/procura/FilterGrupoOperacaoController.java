/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.SdGrupoOperacao001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.sys.EnumsController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterGrupoOperacaoController extends Modals implements Initializable {

    public String returnValue = "";
    public ObservableList<SdGrupoOperacao001> selectedObjects = FXCollections.observableArrayList();
    private String selecteds = "";
    private List<ObservableList<String>> whereClause = new ArrayList<>();
    private final BooleanProperty singleResult = new SimpleBooleanProperty(true);

    @FXML
    private Button btnWhereAnd;
    @FXML
    private Button btnWhereOr;
    @FXML
    private ComboBox<String> cboxTipoFiltro;
    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private ListView<String> listWheres;
    @FXML
    private Button btnCheckAll;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<SdGrupoOperacao001> tblFilters;
    @FXML
    private TableColumn<SdGrupoOperacao001, Boolean> clmSelected;
    @FXML
    private TableColumn<SdGrupoOperacao001, Boolean> clnAtivo;
    @FXML
    private Button btnFindFilter;
    @FXML
    private Button btnLoad;

    /**
     * Initializes the controller class.
     */
    public FilterGrupoOperacaoController(FXMLWindow file, ObservableList<SdGrupoOperacao001> selecteds, EnumsController.ResultTypeFilter resultType) throws IOException {
        super(file);
        this.selectedObjects.clear();
        this.selectedObjects.addAll(selecteds);
        this.singleResult.set(resultType == EnumsController.ResultTypeFilter.SINGLE_RESULT);
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            tblFilters.setItems(FXCollections.observableList(DAOFactory.getSdGrupoOperacao001DAO().getAll()));
            selectObjectsOld();
            tblFilters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (oldValue != null) {
                    oldValue.setSelected(false);
                    selectedObjects.remove(oldValue);
                }
                if (newValue != null) {
                    newValue.setSelected(true);
                    if (!selectedObjects.contains(newValue)) {
                        selectedObjects.add(newValue);
                    }
                }
            });
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        btnCheckAll.disableProperty().bind(singleResult);
        tblFilters.setEditable(true);
        clmSelected.editableProperty().bind(singleResult.not());
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setCellValueFactory((cellData) -> {
            SdGrupoOperacao001 cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> {
                cellValue.setSelected(newValue);
                if (newValue && !selectedObjects.contains(cellValue)) {
                    selectedObjects.add(cellValue);
                } else if (!newValue) {
                    selectedObjects.remove(cellValue);
                }
            });
            return property;
        });
        clnAtivo.setCellFactory(column -> {
            TableCell<SdGrupoOperacao001, Boolean> cell = new TableCell<SdGrupoOperacao001, Boolean>() {

                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        this.setText(item ? "S" : "N");
                    }
                }
            };
            return cell;
        });

        cboxTipoFiltro.setItems(FXCollections.observableArrayList("Código", "Descrição"));
        cboxTipoFiltro.getSelectionModel().selectFirst();
        tboxValorFiltro.requestFocus();
    }

    private void selectObjectsOld() {
        selectedObjects.forEach(register -> {
            tblFilters.getItems().forEach(registerTable -> {
                if (registerTable.equals(register)) {
                    tblFilters.getSelectionModel().select(registerTable);
                    registerTable.copy(register);
                }
            });
        });
    }

    private void createWhereClause(String relationClause) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Código":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "codigo", "like", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " código contém " + tboxValorFiltro.getText());
                break;
            case "Descrição":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "descricao", "like", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " descrição contém " + tboxValorFiltro.getText());
                break;
        }
    }

    @FXML
    private void btnWhereAndOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("and");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnWhereOrOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("or");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {

    }

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            if (tboxValorFiltro.getText().isEmpty()) {
                return;
            }
            this.createWhereClause("");
            tblFilters.setItems(DAOFactory.getSdGrupoOperacao001DAO().getFromFilter(whereClause));

            listWheres.getItems().clear();
            whereClause.clear();
            tboxValorFiltro.clear();
            cboxTipoFiltro.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            rowTable.selectedProperty().set(!rowTable.selectedProperty().get());
        });
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            if (rowTable.selectedProperty().get()) {
                returnValue += "'" + rowTable.getCodigo() + "',";
            }
        });
        if (returnValue.length() > 0) {
            returnValue = returnValue.substring(0, returnValue.length() - 1);
        }

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    @FXML
    private void listWheresOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            int positionWhereClause = listWheres.getSelectionModel().getSelectedIndex();
            listWheres.getItems().remove(positionWhereClause);
            whereClause.remove(positionWhereClause);
        }
    }

}
