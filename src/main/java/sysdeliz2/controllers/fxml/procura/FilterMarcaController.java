/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Marca;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterMarcaController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public String returnValue = "";
    private String selecteds = "";
    private List<ObservableList<String>> whereClause = new ArrayList<>();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private Button btnWhereAnd;
    @FXML
    private Button btnWhereOr;
    @FXML
    private ComboBox<String> cboxTipoFiltro;
    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private ListView<String> listWheres;
    @FXML
    private Button btnCheckAll;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<Marca> tblFilters;
    @FXML
    private TableColumn<Marca, Boolean> clmSelected;
    @FXML
    private Button btnFindFilter;
    @FXML
    private Button btnLoad;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    public FilterMarcaController(FXMLWindow file, String selecteds) throws IOException {
        super(file);
        this.selecteds = selecteds;
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            tblFilters.setItems(FXCollections.observableList(DAOFactory.getMarcaDAO().getAll()));
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(true);
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setCellValueFactory((cellData) -> {
            Marca cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
            return property;
        });
        cboxTipoFiltro.setItems(FXCollections.observableArrayList("Código", "Descrição"));
        cboxTipoFiltro.getSelectionModel().selectFirst();
        tboxValorFiltro.requestFocus();
    }

    //<editor-fold defaultstate="collapsed" desc="Controller methods">
    private void createWhereClause(String relationClause) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Código":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "codigo", "like", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " código contém " + tboxValorFiltro.getText());
                break;
            case "Descrição":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "descricao", "like", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " descrição contém " + tboxValorFiltro.getText());
                break;

        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    private void btnWhereAndOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("and");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnWhereOrOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("or");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {

    }

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            if (tboxValorFiltro.getText().isEmpty()) {
                return;
            }
            this.createWhereClause("");
            tblFilters.setItems(DAOFactory.getMarcaDAO().getMarcasFromFilter(whereClause));

            listWheres.getItems().clear();
            whereClause.clear();
            tboxValorFiltro.clear();
            cboxTipoFiltro.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            rowTable.selectedProperty().set(!rowTable.selectedProperty().get());
        });
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            if (rowTable.selectedProperty().get()) {
                returnValue += "'" + rowTable.getCodigo() + "',";
            }
        });
        if (!selecteds.isEmpty()) {
            returnValue = selecteds + "," + returnValue;
        }
        if (returnValue.length() > 0) {
            returnValue = returnValue.substring(0, returnValue.length() - 1);
        }

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    @FXML
    private void listWheresOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            int positionWhereClause = listWheres.getSelectionModel().getSelectedIndex();
            listWheres.getItems().remove(positionWhereClause);
            whereClause.remove(positionWhereClause);
        }
    }
    //</editor-fold>

}
