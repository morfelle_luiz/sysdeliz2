/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.controllers.fxml.SceneBidController;
import sysdeliz2.controllers.views.procura.FilterColecoesView;
import sysdeliz2.controllers.views.procura.FilterLinhaView;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Produto;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterProdutoController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public String returnValue = "";
    private String selecteds = "";
    private List<ObservableList<String>> whereClause = new ArrayList<>();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private Button btnWhereAnd;
    @FXML
    private Button btnWhereOr;
    @FXML
    private ComboBox<String> cboxTipoFiltro;
    @FXML
    private TextField tboxValorFiltro;
    @FXML
    private ListView<String> listWheres;
    @FXML
    private Button btnCheckAll;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<Produto> tblFilters;
    @FXML
    private TableColumn<Produto, Boolean> clmSelected;
    @FXML
    private Button btnFindFilter;
    @FXML
    private Button btnLoad;
    //</editor-fold>

    /**
     * Initializes the controller class.
     */
    public FilterProdutoController(FXMLWindow file, String selecteds) throws IOException {
        super(file);
        this.selecteds = selecteds;
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tblFilters.setEditable(true);
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setCellValueFactory((cellData) -> {
            Produto cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
            return property;
        });

        cboxTipoFiltro.setItems(FXCollections.observableArrayList("Código", "Descrição", "Marca", "Linha", "Coleção", "Estampa", "Família"));
        cboxTipoFiltro.valueProperty().addListener((observable, oldValue, newValue) -> {
            btnFindFilter.setVisible(false);
            tboxValorFiltro.setEditable(true);
            if (newValue.equals("Marca") || newValue.equals("Linha") || newValue.equals("Coleção") || newValue.equals("Estampa") || newValue.equals("Família")) {
                btnFindFilter.setVisible(true);
                tboxValorFiltro.setEditable(false);
            }
        });
        cboxTipoFiltro.getSelectionModel().selectFirst();
        tboxValorFiltro.requestFocus();
    }

    //<editor-fold defaultstate="collapsed" desc="Controller methods">
    private void createWhereClause(String relationClause) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Código":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.codigo", "=", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " código igual " + tboxValorFiltro.getText());
                break;
            case "Descrição":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.descricao", "like", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " descrição contém " + tboxValorFiltro.getText());
                break;
            case "Marca":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.marca", "in", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " marca igual " + tboxValorFiltro.getText().replace(",", " ou "));
                break;
            case "Linha":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.linha", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " linha igual " + tboxValorFiltro.getText().replace(",", " ou "));
                break;
            case "Coleção":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.colecao", "in", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " coleção igual " + tboxValorFiltro.getText().replace(",", " ou "));
                break;
            case "Estampa":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.estampa", "in", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " estampa igual " + tboxValorFiltro.getText().replace(",", " ou "));
                break;
            case "Família":
                whereClause.add(0, FXCollections.observableArrayList(relationClause, "prod.familia", "in", tboxValorFiltro.getText()));
                listWheres.getItems().add(0, (relationClause.equals("and") ? "e" : "ou") + " família igual " + tboxValorFiltro.getText().replace(",", " ou "));
                break;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    private void btnWhereAndOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("and");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnWhereOrOnAction(ActionEvent event) {
        if (tboxValorFiltro.getText().isEmpty()) {
            return;
        }

        this.createWhereClause("or");
        tboxValorFiltro.clear();
        cboxTipoFiltro.requestFocus();
    }

    @FXML
    private void btnFindFilterOnAction(ActionEvent event) {
        switch (cboxTipoFiltro.getSelectionModel().getSelectedItem()) {
            case "Marca":
                try {
                    FilterMarcaController ctrolFilterColecao = new FilterMarcaController(Modals.FXMLWindow.FilterMarca, tboxValorFiltro.getText());
                    if (!ctrolFilterColecao.returnValue.isEmpty()) {
                        tboxValorFiltro.setText(ctrolFilterColecao.returnValue);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
                break;
            case "Linha":
                FilterLinhaView filterLinhaView = new FilterLinhaView();
                filterLinhaView.setValorFiltroInicial(tboxValorFiltro.getText());
                filterLinhaView.show();

                if (filterLinhaView.itensSelecionados.size() > 0){
                    tboxValorFiltro.setText(filterLinhaView.getResultAsString(true));
                }
                break;
            case "Coleção":
                FilterColecoesView filterColecoesView = new FilterColecoesView();
                filterColecoesView.setValorFiltroInicial(tboxValorFiltro.getText());
                if (filterColecoesView.itensSelecionados.size() > 0) {
                    tboxValorFiltro.setText(filterColecoesView.getResultAsString(true));
                }
                break;
            case "Estampa":
                try {
                    FilterEstampaController ctrolFilterColecao = new FilterEstampaController(Modals.FXMLWindow.FilterEstampa, tboxValorFiltro.getText());
                    if (!ctrolFilterColecao.returnValue.isEmpty()) {
                        tboxValorFiltro.setText(ctrolFilterColecao.returnValue);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
                break;
            case "Família":
                try {
                    FilterFamiliaController ctrolFilterColecao = new FilterFamiliaController(Modals.FXMLWindow.FilterFamilia, tboxValorFiltro.getText());
                    if (!ctrolFilterColecao.returnValue.isEmpty()) {
                        tboxValorFiltro.setText(ctrolFilterColecao.returnValue);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(SceneBidController.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
                break;
        }
    }

    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {
            if (tboxValorFiltro.getText().isEmpty()) {
                return;
            }
            this.createWhereClause("");
            tblFilters.setItems(DAOFactory.getProdutoDAO().getProdutosFromFilter(whereClause));

            listWheres.getItems().clear();
            whereClause.clear();
            tboxValorFiltro.clear();
            cboxTipoFiltro.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            rowTable.selectedProperty().set(!rowTable.selectedProperty().get());
        });
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> {
            if (rowTable.selectedProperty().get()) {
                returnValue += "'" + rowTable.getCodigo() + "',";
            }
        });
        if (!selecteds.isEmpty()) {
            returnValue = selecteds + "," + returnValue;
        }
        if (returnValue.length() > 0) {
            returnValue = returnValue.substring(0, returnValue.length() - 1);
        }

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    @FXML
    private void listWheresOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            int positionWhereClause = listWheres.getSelectionModel().getSelectedIndex();
            listWheres.getItems().remove(positionWhereClause);
            whereClause.remove(positionWhereClause);
        }
    }
    //</editor-fold>

}
