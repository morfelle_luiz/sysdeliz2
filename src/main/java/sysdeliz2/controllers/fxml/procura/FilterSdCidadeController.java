/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterSdCidadeController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public List<SdCidade> returnValue;
    private String uf;
    private Object[] ufs;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    @SuppressWarnings("unused")
    private TextField tboxEstadoFiltro;
    @FXML
    @SuppressWarnings("unused")
    private TextField tboxNomeFiltro;
    @FXML
    @SuppressWarnings("unused")
    private Button btnCheckAll;
    @FXML
    @SuppressWarnings("unused")
    private Button btnUseThis;
    @FXML
    @SuppressWarnings("unused")
    private TableView<SdCidade> tblFilters;
    @FXML
    @SuppressWarnings("unused")
    private TableColumn<SdCidade, Boolean> clmSelected;
    @FXML
    @SuppressWarnings("unused")
    private Button btnFindFilter;
    @FXML
    @SuppressWarnings("unused")
    private Button btnLoad;
    //</editor-fold>


    private GenericDao<SdCidade> dao;

    /**
     * Initializes the controller class.
     */
    public FilterSdCidadeController(FXMLWindow file, String uf) throws IOException {
        super(file);
        this.uf = uf;
        this.ufs = null;
        super.show(this);
    }

    @SuppressWarnings("unused")
    public FilterSdCidadeController(FXMLWindow file, Object[] ufs) throws IOException {
        super(file);
        this.uf = "";
        this.ufs = ufs;
        super.show(this);
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {

        dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade.class);

        try {
            dao.initCriteria();

            if(uf != null) {
                dao.addPredicateEq("siglaUf", uf);
            }

            if (ufs != null){
                dao.addPredicateIn("siglaUf", ufs);
            }

            tblFilters.setItems(dao.loadListByPredicate());
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(true);
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setCellValueFactory((cellData) -> {
            SdCidade cellValue = cellData.getValue();
            BooleanProperty property = cellValue.selectedProperty();
            property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
            return property;
        });

        if(ufs != null){
            tboxEstadoFiltro.setText(Arrays.toString(ufs));
        } else {
            tboxEstadoFiltro.setText(uf);
        }

        tboxEstadoFiltro.disableProperty();

        tboxNomeFiltro.requestFocus();
    }

    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    @SuppressWarnings("unused")
    private void btnLoadOnAction(ActionEvent event) {
        try {

            dao.initCriteria();
            dao.clearPredicate();

            if(ufs != null){
                dao.addPredicateIn("siglaUf", ufs);
            } else {
                dao.addPredicateEq("siglaUf", uf);
            }

            if(!tboxNomeFiltro.getText().isEmpty()){
                dao.addPredicateLike("nome", tboxNomeFiltro.getText());
            }

            tblFilters.setItems(dao.loadListByPredicate());
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    @SuppressWarnings("unused")
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> rowTable.selectedProperty().set(!rowTable.selectedProperty().get()));
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        returnValue = new ArrayList<>();
        returnValue.addAll(tblFilters.getItems().filtered(SdCidade::isSelected));

        if(event != null) {
            event.consume();
        }

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();

    }

    @FXML
    @SuppressWarnings("unused")
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    //</editor-fold>

}
