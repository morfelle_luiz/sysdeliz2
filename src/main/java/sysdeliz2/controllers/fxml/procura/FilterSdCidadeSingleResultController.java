/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterSdCidadeSingleResultController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public SdCidade returnValue;
    private String uf;

    private List<ObservableList<String>> whereClause = new ArrayList<>();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxEstadoFiltro;
    @FXML
    private TextField tboxNomeFiltro;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<SdCidade> tblFilters;
    @FXML
    private Button btnFindFilter;
    @FXML
    private Button btnLoad;
    //</editor-fold>


    private GenericDao<SdCidade> dao;

    /**
     * Initializes the controller class.
     */
    public FilterSdCidadeSingleResultController(FXMLWindow file, String uf) throws IOException {
        super(file);
        this.uf = uf;
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade.class);

        TextFieldUtils.upperCase(tboxEstadoFiltro);
        TextFieldUtils.upperCase(tboxNomeFiltro);

        try {

            if(!uf.equals("")) {
                dao.initCriteria();
                dao.addPredicateEq("siglaUf", uf);
                tblFilters.setItems(dao.loadListByPredicate());
            } else {
                tblFilters.setItems(dao.list());
            }
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(true);
        tboxEstadoFiltro.setText(uf);
        tboxNomeFiltro.requestFocus();
    }

    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {

            dao.initCriteria();
            dao.clearPredicate();
            if(!tboxEstadoFiltro.getText().equals("")) {
                dao.addPredicateEq("siglaUf", tboxEstadoFiltro.getText());
            }

            if(!tboxNomeFiltro.getText().isEmpty()){
                dao.addPredicateLike("nome", tboxNomeFiltro.getText());
            }

            tblFilters.setItems(dao.loadListByPredicate());
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnCheckAllOnAction(ActionEvent event) {
        tblFilters.getItems().forEach(rowTable -> rowTable.selectedProperty().set(!rowTable.selectedProperty().get()));
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        returnValue = tblFilters.getSelectionModel().getSelectedItem();
        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblFilters.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnUseThisOnAction(null);
        }
    }

    //</editor-fold>

}
