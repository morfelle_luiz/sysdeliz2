/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdRegiao;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class FilterSdRegiaoController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public SdRegiao returnValue;
    private List<ObservableList<String>> whereClause = new ArrayList<>();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxNomeFiltro;
    @FXML
    private Button btnUseThis;
    @FXML
    private TableView<SdRegiao> tblFilters;
    @FXML
    private Button btnLoad;
    //</editor-fold>


    private GenericDao<SdRegiao> dao;

    /**
     * Initializes the controller class.
     */
    public FilterSdRegiaoController(FXMLWindow file) throws IOException {
        super(file);
        super.show(this);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRegiao.class);

        try {
            tblFilters.setItems(dao.list());
        } catch (SQLException ex) {
            Logger.getLogger(FilterPeriodoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tblFilters.setEditable(true);
        tboxNomeFiltro.requestFocus();
    }

    //<editor-fold defaultstate="collapsed" desc="FXML action methods">
    @FXML
    private void btnLoadOnAction(ActionEvent event) {
        try {

            dao.initCriteria();
            dao.clearPredicate();

            if(!tboxNomeFiltro.getText().isEmpty()){
                dao.addPredicateLike("descricao", tboxNomeFiltro.getText());
            }

            tblFilters.setItems(dao.loadListByPredicate());
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnUseThisOnAction(ActionEvent event) {
        returnValue = tblFilters.getSelectionModel().getSelectedItem();
        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblFiltersOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            this.btnUseThisOnAction(null);
        }
    }

    //</editor-fold>

}
