/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Cliente;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProcurarClienteController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public String cods_retorno = "";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxCliente;
    @FXML
    private TextField tboxInformacoes;
    @FXML
    private Button btnProcurar;
    @FXML
    private Button btnMarcarTudo;
    @FXML
    private Button btnFinalizar;
    @FXML
    private TableView<Cliente> tblConsulta;
    @FXML
    private TableColumn<Cliente, Boolean> clmSelectedCliente;
    //</editor-fold>

    public SceneProcurarClienteController(FXMLWindow file) throws IOException {
        super(file);
        super.show(this);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblConsulta.setItems(DAOFactory.getClienteDAO().getAll());
            tblConsulta.setEditable(true);
            clmSelectedCliente.setCellFactory((col) -> new CheckBoxTableCell<>());
            clmSelectedCliente.setCellValueFactory((cellData) -> {
                Cliente cellValue = cellData.getValue();
                BooleanProperty property = cellValue.selectedProperty();
                property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
                return property;
            });
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos Locais">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Métodos FXML">
    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblConsulta.setItems(DAOFactory.getClienteDAO().getByForm(tboxCliente.getText(), tboxInformacoes.getText()));
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnMarcarTudoOnAction(ActionEvent event) {
        tblConsulta.getItems().forEach(repr -> {
            repr.selectedProperty().set(!repr.selectedProperty().get());
        });
    }

    @FXML
    private void btnFinalizarOnAction(ActionEvent event) {
        tblConsulta.getItems().forEach(repr -> {
            if (repr.selectedProperty().get()) {
                cods_retorno += repr.getCodcli() + "|";
            }
        });
        if (cods_retorno.length() > 0) {
            cods_retorno = cods_retorno.substring(0, cods_retorno.length() - 1);
        }

        Stage main = (Stage) btnFinalizar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblConsultaOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblConsulta.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnFinalizarOnAction(null);
        }
    }
    //</editor-fold>

}
