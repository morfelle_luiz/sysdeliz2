/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.procura;

import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Familia;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Modals;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class SceneProcurarFamiliaController extends Modals implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Variáveis Locais">
    public String cods_retorno = "";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variáveis FXML">
    @FXML
    private TextField tboxFamilia;
    @FXML
    private Button btnProcurar;
    @FXML
    private TextField tboxGrupo;
    @FXML
    private Button btnMarcarTudo;
    @FXML
    private Button btnFinalizar;
    @FXML
    private TableView<Familia> tblConsulta;
    @FXML
    private TableColumn<Familia, Boolean> clmSelected;
    //</editor-fold>

    public SceneProcurarFamiliaController(FXMLWindow file) throws IOException {
        super(file);
        super.show(this);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            tblConsulta.setItems(DAOFactory.getFamiliaDAO().getAll());
            tblConsulta.setEditable(true);
            clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
            clmSelected.setCellValueFactory((cellData) -> {
                Familia cellValue = cellData.getValue();
                BooleanProperty property = cellValue.selectedProperty();
                property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
                return property;
            });
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }

        tboxFamilia.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        tboxGrupo.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos Locais">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Métodos FXML">
    @FXML
    private void btnProcurarOnAction(ActionEvent event) {
        try {
            tblConsulta.setItems(DAOFactory.getFamiliaDAO().getByWindowsForm(tboxFamilia.getText(), tboxGrupo.getText()));
            tblConsulta.setEditable(true);
            clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
            clmSelected.setCellValueFactory((cellData) -> {
                Familia cellValue = cellData.getValue();
                BooleanProperty property = cellValue.selectedProperty();
                property.addListener((observable, oldValue, newValue) -> cellValue.setSelected(newValue));
                return property;
            });
        } catch (SQLException ex) {
            Logger.getLogger(SceneProcurarRepresentanteController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    @FXML
    private void btnMarcarTudoOnAction(ActionEvent event) {
        tblConsulta.getItems().forEach(repr -> {
            repr.selectedProperty().set(!repr.selectedProperty().get());
        });
    }

    @FXML
    private void btnFinalizarOnAction(ActionEvent event) {
        tblConsulta.getItems().forEach(repr -> {
            if (repr.selectedProperty().get()) {
                cods_retorno += repr.getCodigo() + "|";
            }
        });
        if (cods_retorno.length() > 0) {
            cods_retorno = cods_retorno.substring(0, cods_retorno.length() - 1);
        }

        Stage main = (Stage) btnFinalizar.getScene().getWindow();
        main.close();
    }

    @FXML
    private void tblConsultaOnMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            tblConsulta.getSelectionModel().getSelectedItem().selectedProperty().set(true);
            this.btnFinalizarOnAction(null);
        }
    }
    //</editor-fold>

}
