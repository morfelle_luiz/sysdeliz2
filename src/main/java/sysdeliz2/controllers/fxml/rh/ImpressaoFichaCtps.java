/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.controllers.fxml.rh;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.PrintReportPreview;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author cristiano.diego
 */
public class ImpressaoFichaCtps implements Initializable {

    @FXML
    private TextField tboxRegistroFunc;
    @FXML
    private TextField tboxCodigoEmp;
    @FXML
    private Button btnCriarCartaoFunc;
    @FXML
    private HBox previewCartao;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void btnCriarCartaoFuncOnAction(ActionEvent event) {
        try {
            if (tboxRegistroFunc.getText().isEmpty() || tboxCodigoEmp.getText().isEmpty()) {
                GUIUtils.showMessage("Vcê precisa informar o número de registro e código de empresa para gerar o cartão.", Alert.AlertType.WARNING);
                return;
            }

            // First, compile jrxml file.
            JasperReport jasperReport = (JasperReport) JRLoader
                    .loadObject(getClass().getResourceAsStream("/relatorios/TagCtps.jasper"));
            // Fields for report
            HashMap<String, Object> parameters = new HashMap<String, Object>();

            parameters.put("codfun", tboxRegistroFunc.getText());
            parameters.put("codemp", tboxCodigoEmp.getText());

            ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(parameters);

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, ConnectionFactory.getEmpresaConnection());
            previewCartao.getChildren().add(new PrintReportPreview().getSwingNode(print));
            previewCartao.requestLayout();
        } catch (JRException ex) {
            Logger.getLogger(ImpressaoFichaCtps.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpressaoFichaCtps.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
