package sysdeliz2.controllers.views.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaFornecedor;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.window.base.CadastroBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CadastroBarcasFornecedorController extends CadastroBase<SdBarcaFornecedor> {

    private final FormFieldSingleFind<Entidade> fieldFornecedor = FormFieldSingleFind.create(Entidade.class, field -> {
        field.title("Fornecedor");
        field.width(500.0);
    });
    private final FormFieldText fieldMaquina = FormFieldText.create(field -> {
        field.title("Máquina");
        field.width(160.0);
    });
    private final FormFieldRange fieldCapacidadeMaquina = FormFieldRange.create(field -> {
        field.title("Capacidade");
        field.width(200.0);
    });
    private final FormFieldText fieldCusto = FormFieldText.create(field -> {
        field.title("Excesso");
        field.postLabel("%");
        field.width(100.0);
    });
    private final FormFieldText fiedlPrazo = FormFieldText.create(field -> {
        field.title("Prazo");
        field.postLabel("dias");
        field.width(100.0);
    });
    private final FormFieldToggle fieldAtivo = FormFieldToggle.create(field -> {
        field.title("Ativo");
    });
    private final FormFieldTextArea fieldObservacao = FormFieldTextArea.create(field -> {
        field.title("Observações");
        field.height(150.0);
        field.width(300.0);
    });

    public CadastroBarcasFornecedorController() throws SQLException {
        super("Cadastro de Barcas Fornecedor", ImageUtils.getImage(ImageUtils.Icon.LAVANDERIA));
        super.tboxFiltroPadrao.placeHolder("BUSCA POR MÁQUINA");
        super.cpaneFormulario.add(FormBox.create(boxFieldsCadastro -> {
            boxFieldsCadastro.vertical();
            boxFieldsCadastro.expanded();
            boxFieldsCadastro.add(FormBox.create(boxDadosFornecedor -> {
                boxDadosFornecedor.horizontal();
                boxDadosFornecedor.add(fieldFornecedor.build());
            }));
            boxFieldsCadastro.add(FormBox.create(boxDadosMaquina -> {
                boxDadosMaquina.horizontal();
                boxDadosMaquina.add(fieldAtivo.build(), fieldMaquina.build(), fieldCapacidadeMaquina.build());
            }));
            boxFieldsCadastro.add(FormBox.create(boxObservacaoMaquina -> {
                boxObservacaoMaquina.horizontal();
                boxObservacaoMaquina.add(fieldObservacao.build());
            }));
            boxFieldsCadastro.add(FormBox.create(boxDadosExtras -> {
                boxDadosExtras.horizontal();
                boxDadosExtras.add(fieldCusto.build(), fiedlPrazo.build());
            }));
        }));
    }

    @Override
    protected void initializeComponents() {
        fieldFornecedor.editable.bind(navegation.inEdition);
        fieldMaquina.editable.bind(navegation.inEdition);
        fieldCusto.editable.bind(navegation.inEdition);
        fiedlPrazo.editable.bind(navegation.inEdition);
        fieldAtivo.editable.bind(navegation.inEdition);
        fieldObservacao.editable.bind(navegation.inEdition);
        fieldCapacidadeMaquina.editable.bind(navegation.inEdition);
    }

    @Override
    protected FormTableColumn[] setColumnsTable() {
        return new FormTableColumn[]{
                FormTableColumn.create(cln -> {
                    cln.title("Fornecedor");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFornecedor().toString()));

                }).build() /*Fornecedor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Máquina");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMaquina()));
                }).build() /*Máquina*/,
                FormTableColumn.create(cln -> {
                    cln.title("Min.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMinimo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Min.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Máx.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaximo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Máx.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Prazo");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrazo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Prazo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtivo()));
                    cln.format(param -> {
                        return new TableCell<SdBarcaFornecedor, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Ativo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Observação");
                    cln.width(450.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getObs()));
                }).build() /*Observação*/
        };
    }

    @Override
    protected void btnFindDefaultOnAction(String defaultValue) {
        JPAUtils.clearEntity(super.listRegisters.getValue());
        AtomicReference<List<SdBarcaFornecedor>> barcasFornecedor = new AtomicReference<>(Collections.emptyList());
        new RunAsyncWithOverlay(this).exec(task -> {
            barcasFornecedor.set((List<SdBarcaFornecedor>) new FluentDao().selectFrom(SdBarcaFornecedor.class)
                    .where(eb -> eb.like("id.maquina", super.tboxFiltroPadrao.value.getValue()))
                    .resultList());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                super.listRegisters.set(FXCollections.observableList(barcasFornecedor.get()));
            }
        });
    }

    @Override
    protected void btnFindAdvanced() {

    }

    @Override
    protected void bindData(SdBarcaFornecedor selectItem) {
        fieldFornecedor.value.bind(selectItem.getId().fornecedorProperty());
        fieldMaquina.value.bind(selectItem.getId().maquinaProperty());
        fieldCapacidadeMaquina.valueBegin.bind(selectItem.minimoProperty());
        fieldCapacidadeMaquina.valueEnd.bind(selectItem.maximoProperty());
        fieldCusto.value.bind(selectItem.custoProperty().asString());
        fiedlPrazo.value.bind(selectItem.prazoProperty().asString());
        fieldAtivo.value.bind(selectItem.ativoProperty());
        fieldObservacao.value.bind(selectItem.obsProperty());
    }

    @Override
    protected void unbindData() {
        fieldFornecedor.value.unbind();
        fieldMaquina.value.unbind();
        fieldCapacidadeMaquina.valueBegin.unbind();
        fieldCapacidadeMaquina.valueEnd.unbind();
        fieldCusto.value.unbind();
        fiedlPrazo.value.unbind();
        fieldAtivo.value.unbind();
        fieldObservacao.value.unbind();
    }

    @Override
    protected void bindBidirectionalData(SdBarcaFornecedor selectItem) {
        if (selectItem != null) {
            Bindings.bindBidirectional(fieldFornecedor.value, selectItem.getId().fornecedorProperty());
            Bindings.bindBidirectional(fieldMaquina.value, selectItem.getId().maquinaProperty());
            Bindings.bindBidirectional(fieldCapacidadeMaquina.valueBegin, selectItem.minimoProperty());
            Bindings.bindBidirectional(fieldCapacidadeMaquina.valueEnd, selectItem.maximoProperty());
            Bindings.bindBidirectional(fieldCusto.value, selectItem.custoProperty(), new BigDecimalStringConverter());
            Bindings.bindBidirectional(fiedlPrazo.value, selectItem.prazoProperty(), new NumberStringConverter());
            Bindings.bindBidirectional(fieldAtivo.value, selectItem.ativoProperty());
            Bindings.bindBidirectional(fieldObservacao.value, selectItem.obsProperty());
//            fieldFornecedor.value.bindBidirectional(selectItem.getId().fornecedorProperty());
//            fieldMaquina.value.bindBidirectional(selectItem.getId().maquinaProperty());
//            fieldCapacidadeMaquina.valueBegin.bindBidirectional(selectItem.minimoProperty());
//            fieldCapacidadeMaquina.valueEnd.bindBidirectional(selectItem.maximoProperty());
//            fieldCusto.value.bindBidirectional(selectItem.custoProperty(), new BigDecimalStringConverter());
//            fiedlPrazo.value.bindBidirectional(selectItem.prazoProperty(), new NumberStringConverter());
//            fieldAtivo.value.bindBidirectional(selectItem.ativoProperty());
//            fieldObservacao.value.bindBidirectional(selectItem.obsProperty());
        }
    }

    @Override
    protected void unbindBidirectionalData(SdBarcaFornecedor selectItem) {
        fieldFornecedor.value.unbindBidirectional(selectItem.getId().fornecedorProperty());
        fieldMaquina.value.unbindBidirectional(selectItem.getId().maquinaProperty());
        fieldCapacidadeMaquina.valueBegin.unbindBidirectional(selectItem.minimoProperty());
        fieldCapacidadeMaquina.valueEnd.unbindBidirectional(selectItem.maximoProperty());
        fieldCusto.value.unbindBidirectional(selectItem.custoProperty());
        fiedlPrazo.value.unbindBidirectional(selectItem.prazoProperty());
        fieldAtivo.value.unbindBidirectional(selectItem.ativoProperty());
        fieldObservacao.value.unbindBidirectional(selectItem.obsProperty());
    }

    @Override
    protected void clearForm() {
//        fieldFornecedor.clear();
//        fieldMaquina.clear();
//        fieldCapacidadeMaquina.clear();
//        fieldCusto.clear();
//        fiedlPrazo.clear();
//        fieldAtivo.value.set(true);
//        fieldObservacao.clear();
    }

    @Override
    protected boolean validationForm() {
        return true;
    }

    @Override
    public void closeWindow() {

    }
}
