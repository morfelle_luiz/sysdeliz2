package sysdeliz2.controllers.views.cadastros;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.CadCep;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.window.base.CadastroBase;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CadastroCepController extends CadastroBase<CadCep> {

    private final FormFieldSingleFind<Cidade> singleFindCidade = FormFieldSingleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.dividedWidth(400);
        field.toUpper();
    });

    private final FormFieldText textCep = FormFieldText.create(field -> {
        field.title("CEP");
        field.decimalField(0);
    });

    private final FormFieldText textBairro = FormFieldText.create(field -> {
        field.title("Bairro");
        field.width(400);
        field.toUpper();
    });

    public CadastroCepController() {
        super("Cadastro de CEP", ImageUtils.getImage(ImageUtils.Icon.CIDADE), 10);
        tboxFiltroPadrao.toUpper();
        super.cpaneFormulario.add(FormBox.create(boxFiltro -> {
            boxFiltro.horizontal();
            boxFiltro.add(textCep.build(), singleFindCidade.build(), textBairro.build());
        }));
    }

    @Override
    protected void initializeComponents() {
        ((FormBox) super.lookup("#box-find-filters")).remove(super.lookup("#separator-btn-find-advanced"));
        ((FormBox) super.lookup("#box-find-filters")).remove(btnFindAdvanced);

        singleFindCidade.editable.bind(navegation.inEdition);
        textCep.editable.bind(navegation.inEdition);
        textBairro.editable.bind(navegation.inEdition);
    }

    @Override
    protected FormTableColumn[] setColumnsTable() {
        return new FormTableColumn[]{
                FormTableColumn.create(cln -> {
                    cln.title("CEP");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadCep, CadCep>, ObservableValue<CadCep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCep()));
                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadCep, CadCep>, ObservableValue<CadCep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCidade().toString()));
                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Bairro");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadCep, CadCep>, ObservableValue<CadCep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBairro()));
                }).build(), /*Tipo*/
        };
    }

    @Override
    protected void btnFindDefaultOnAction(String defaultValue) {
        AtomicReference<List<CadCep>> listCeps = new AtomicReference<>(Collections.emptyList());
        new RunAsyncWithOverlay(this).exec(task -> {
            listCeps.set((List<CadCep>) new FluentDao().selectFrom(CadCep.class)
                    .where(it -> it
                            .like("cidade.nomeCid", super.tboxFiltroPadrao.value.getValueSafe(), TipoExpressao.AND, when -> !super.tboxFiltroPadrao.value.getValueSafe().equals(""))).resultList());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                super.listRegisters.set(FXCollections.observableArrayList(listCeps.get()));
            }
        });
    }

    @Override
    protected void btnFindAdvanced() {

    }

    @Override
    protected void bindData(CadCep selectItem) {
        singleFindCidade.value.bind(selectItem.cidadeProperty());
        textCep.value.bind(selectItem.cepProperty());
        textBairro.value.bind(selectItem.bairroProperty());
    }

    @Override
    protected void unbindData() {
        singleFindCidade.value.unbind();
        textCep.value.unbind();
        textBairro.value.unbind();
    }

    @Override
    protected void bindBidirectionalData(CadCep selectItem) {
        singleFindCidade.value.bindBidirectional(selectItem.cidadeProperty());
        textCep.value.bindBidirectional(selectItem.cepProperty());
        textBairro.value.bindBidirectional(selectItem.bairroProperty());
    }

    @Override
    protected void unbindBidirectionalData(CadCep selectItem) {
        singleFindCidade.value.unbindBidirectional(selectItem.cidadeProperty());
        textCep.value.unbindBidirectional(selectItem.cepProperty());
        textBairro.value.unbindBidirectional(selectItem.bairroProperty());
    }

    @Override
    protected void clearForm() {
        singleFindCidade.clear();
        textCep.clear();
        textBairro.clear();
    }

    @Override
    protected boolean validationForm() {
        return singleFindCidade.value.getValue() != null && !textCep.value.getValueSafe().equals("") && textCep.value.getValueSafe().length() == 8;
    }

    @Override
    public void closeWindow() {

    }
}
