package sysdeliz2.controllers.views.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class CadastroDepositoController extends WindowBase {

    protected List<Deposito> depositos = new ArrayList<>();

    public CadastroDepositoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getDepositos(Object[] codigo, String codcli, String pais, String tipoMat, String grupoDep){

        Object[] pCodigo = codigo != null ? codigo : new Object[]{};
        String pCodCli = codcli != null ? codcli : "";
        String pPais = pais != null ? pais : "";
        String pTipoMat = tipoMat != null ? tipoMat : "";
        String pGrupoDep = grupoDep != null ? grupoDep : "";



        JPAUtils.clearEntitys(depositos);
        depositos = (List<Deposito>)
                new FluentDao().selectFrom(Deposito.class)
                        .where(eb -> eb
                        .isIn("codigo", pCodigo , TipoExpressao.AND, b -> pCodigo.length > 0)
                        .like("codcli", pCodCli , TipoExpressao.AND, b -> !pCodCli.equals(""))
                        .equal("pais", pPais , TipoExpressao.AND, b -> !pPais.equals(""))
                        .like("tipoMat", pTipoMat , TipoExpressao.AND, b -> !pTipoMat.equals("A"))
                        .like("grupoDep", pGrupoDep , TipoExpressao.AND, b -> !pGrupoDep.equals(""))
                                .notEqual("codigo", "0")
                        ).resultList();
    }
}
