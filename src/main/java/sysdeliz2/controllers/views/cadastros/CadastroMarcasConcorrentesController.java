package sysdeliz2.controllers.views.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdMarcasConcorrentes001;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.window.base.CadastroBase;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CadastroMarcasConcorrentesController extends CadastroBase<SdMarcasConcorrentes001> {

    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });

    private final FormFieldText nomeMarcaField = FormFieldText.create(field -> {
        field.title("Nome");
        field.toUpper();
    });

    private final FormFieldSingleFind<Marca> marcaPropriaField = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca Própria");
    });

    public CadastroMarcasConcorrentesController() throws SQLException {
        super("Cadastro de Marcas Concorrentes", ImageUtils.getImage(ImageUtils.Icon.MARCA));
        super.tboxFiltroPadrao.placeHolder("BUSCA POR NOME");
        super.tboxFiltroPadrao.toUpper();
        super.cpaneFormulario.add(FormBox.create(boxForm -> {
            boxForm.horizontal();
            boxForm.add(codigoField.build(), nomeMarcaField.build(), marcaPropriaField.build());
        }));
    }

    @Override
    protected void initializeComponents() {
        ((FormBox) super.lookup("#box-find-filters")).remove(super.lookup("#separator-btn-find-advanced"));
        ((FormBox) super.lookup("#box-find-filters")).remove(btnFindAdvanced);
        nomeMarcaField.editable.bind(navegation.inEdition);
        marcaPropriaField.editable.bind(navegation.inEdition);
    }

    @Override
    protected FormTableColumn[] setColumnsTable() {
        return new FormTableColumn[]{
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMarcasConcorrentes001, SdMarcasConcorrentes001>, ObservableValue<SdMarcasConcorrentes001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMarcasConcorrentes001, SdMarcasConcorrentes001>, ObservableValue<SdMarcasConcorrentes001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca Própria");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMarcasConcorrentes001, SdMarcasConcorrentes001>, ObservableValue<SdMarcasConcorrentes001>>) param -> new ReadOnlyObjectWrapper( param.getValue().getMarca().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMarcasConcorrentes001, SdMarcasConcorrentes001>, ObservableValue<SdMarcasConcorrentes001>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtivo()));
                    cln.format(param -> new TableCell<SdMarcasConcorrentes001, Boolean>() {
                        @Override
                        protected void updateItem(Boolean item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(ImageUtils.getIcon(item ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                            }
                        }
                    });
                }).build() /*Código*/
        };
    }

    @Override
    protected void btnFindDefaultOnAction(String defaultValue) {
        AtomicReference<List<SdMarcasConcorrentes001>> listMarcas = new AtomicReference<>(Collections.emptyList());
        new RunAsyncWithOverlay(this).exec(task -> {
            listMarcas.set((List<SdMarcasConcorrentes001>) new FluentDao().selectFrom(SdMarcasConcorrentes001.class)
                    .where(it -> it
                            .like("nome", super.tboxFiltroPadrao.value.getValue())).orderBy("codigo", OrderType.ASC).resultList());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                super.listRegisters.set(FXCollections.observableArrayList(listMarcas.get()));
            }
        });
    }

    @Override
    protected void btnFindAdvanced() {

    }

    @Override
    protected void bindData(SdMarcasConcorrentes001 selectItem) {
        codigoField.value.bind(selectItem.codigoProperty().asString());
        nomeMarcaField.value.bind(selectItem.nomeProperty());
        marcaPropriaField.value.bind(selectItem.marcaProperty());
    }

    @Override
    protected void unbindData() {
        codigoField.value.unbind();
        nomeMarcaField.value.unbind();
        marcaPropriaField.value.unbind();
    }

    @Override
    protected void bindBidirectionalData(SdMarcasConcorrentes001 selectItem) {
        if (selectItem != null){
            Bindings.bindBidirectional(codigoField.value, selectItem.codigoProperty(), new NumberStringConverter());
            Bindings.bindBidirectional(nomeMarcaField.value, selectItem.nomeProperty());
            Bindings.bindBidirectional(marcaPropriaField.value, selectItem.marcaProperty());
        }
    }

    @Override
    protected void unbindBidirectionalData(SdMarcasConcorrentes001 selectItem) {
        codigoField.value.unbindBidirectional(selectItem.codigoProperty().asString());
        nomeMarcaField.value.unbindBidirectional(selectItem.nomeProperty());
        marcaPropriaField.value.unbindBidirectional(selectItem.marcaProperty());
    }

    @Override
    protected void clearForm() {
        codigoField.clear();
        nomeMarcaField.clear();
        marcaPropriaField.clear();
    }

    @Override
    protected boolean validationForm() {
        return !nomeMarcaField.value.getValue().equals("") && marcaPropriaField.value.getValue() != null;
    }

    @Override
    public void closeWindow() {

    }
}
