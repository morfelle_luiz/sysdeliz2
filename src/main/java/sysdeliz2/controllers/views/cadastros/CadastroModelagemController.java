package sysdeliz2.controllers.views.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.sysdeliz.SdModelagem;
import sysdeliz2.models.ti.EtqProd;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class CadastroModelagemController extends WindowBase {

    protected List<EtqProd> produtos = new ArrayList<>();
    protected List<EtqProd> produtosModelagem = new ArrayList<>();
    protected List<EtqProd> produtosGrupo = new ArrayList<>();
    protected List<VSdDadosProduto> produtosEtiqueta = new ArrayList<>();


    public CadastroModelagemController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getProdutos(Object[] codigo, Object[] modelagem, Object[] grupo){
        Object[] pCodigo = codigo == null ? new Object[]{} : codigo;
        Object[] pModelagem = modelagem == null ? new Object[]{} : modelagem;
        Object[] pGrupo = grupo == null ? new Object[]{} : grupo;

        produtos = (List<EtqProd>) new FluentDao().selectFrom(EtqProd.class).where(it -> it
        .notEqual("codigo", "0000")
        .isIn("codigo" , pCodigo, TipoExpressao.AND , b-> pCodigo.length > 0)
        .isIn("modelagem" , pModelagem, TipoExpressao.AND , b-> pModelagem.length > 0)
        .isIn("grupo" , pGrupo, TipoExpressao.AND , b-> pGrupo.length > 0)
        ).resultList();
    }

    protected void getProdutosModelagem( SdModelagem modelagem){
        String pModelagem = modelagem == null ? "" : String.valueOf(modelagem.getCodigo());

        produtosModelagem = (List<EtqProd>) new FluentDao().selectFrom(EtqProd.class).where(it -> it
                .equal("codModelagem.codigo" , pModelagem, TipoExpressao.AND , b-> !pModelagem.equals(""))
        ).orderBy("codModelagem.codigo",OrderType.ASC).resultList();
    }

    protected void getProdutosGrupo(SdGrupoModelagem grupo){
        String pGrupo = grupo == null ? "" : String.valueOf(grupo.getCodigo());

        produtosGrupo = (List<EtqProd>) new FluentDao().selectFrom(EtqProd.class).where(it -> it
                .equal("codGrupo.codigo" , pGrupo, TipoExpressao.AND , b-> !pGrupo.equals(""))
        ).orderBy("codGrupo.codigo",OrderType.ASC).resultList();
    }

    protected void getProdutosEtiqueta(EtqProd etq){
        produtosEtiqueta = (List<VSdDadosProduto>) new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codEtq", etq.getCodigo())).resultList();
    }

    protected SdModelagem getCodigoNewModelagem(SdModelagem modelagem){
        SdModelagem model = new FluentDao().selectFrom(SdModelagem.class).where(it -> it.equal("modelagem", modelagem.getModelagem())).singleResult();
        JPAUtils.clearEntity(model);
        return model;
    }
    protected SdGrupoModelagem getCodigoNewGrupo(SdGrupoModelagem grupo){
        SdGrupoModelagem grupoModelagem =  new FluentDao().selectFrom(SdGrupoModelagem.class).where(it -> it.equal("grupo", grupo.getGrupo())).singleResult();
        JPAUtils.clearEntity(grupoModelagem);
        return grupoModelagem;
    }
}
