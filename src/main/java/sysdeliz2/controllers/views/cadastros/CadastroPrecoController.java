package sysdeliz2.controllers.views.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Regiao;
import sysdeliz2.models.ti.TabPreco;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastroPrecoController extends WindowBase {

    protected List<VSdDadosProduto> produtos = new ArrayList<>();


    public CadastroPrecoController(String title, Image icone) {
        super(title, icone);
    }


    protected void buscarProdutos(Object[] marcas, Object[] colecoes, Object[] linhas, Object[] familia, Object[] codigos){

        Object[] pMarcas = marcas == null ? new Object[]{} : marcas;
        Object[] pColecoes = colecoes == null ? new Object[]{} : colecoes;
        Object[] pLinhas = linhas == null ? new Object[]{} : linhas;
        Object[] pFamilia = familia == null ? new Object[]{} : familia;
        Object[] pCodigos = codigos == null ? new Object[]{} : codigos;

        produtos = (List<VSdDadosProduto>) new FluentDao().selectFrom(VSdDadosProduto.class)
                .where(it -> it.
                        isIn("codMarca", pMarcas , TipoExpressao.AND, b -> pMarcas.length > 0).
                        isIn("codCol", pColecoes, TipoExpressao.AND, b -> pColecoes.length > 0).
                        isIn("codlin", pLinhas, TipoExpressao.AND, b -> pLinhas.length > 0).
                        isIn("codFam", pFamilia, TipoExpressao.AND, b -> pFamilia.length > 0).
                        isIn("codigo", pCodigos, TipoExpressao.AND, b -> pCodigos.length > 0)
                )
                .resultList();
        carregaTabPrecoProdutos();
    }

    private void carregaTabPrecoProdutos() {
        for (VSdDadosProduto produto : produtos) {
            List<TabPreco> tabPrecos = (List<TabPreco>) new FluentDao().selectFrom(TabPreco.class).where(it -> it.equal("id.codigo.codigo", produto.getCodigo())).resultList();
            if(tabPrecos!= null) produto.setListTabPreco(tabPrecos);
        }
    }

    protected VSdDadosProduto buscarProdutoPorcodigo(String codProduto){
        return new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codigo",codProduto)).singleResult();
    }

    protected Regiao buscarRegiao(String codRegiao){
        Regiao regiao = new FluentDao().selectFrom(Regiao.class).where(it -> it.equal("regiao", codRegiao)).singleResult();
        if(regiao == null) return new FluentDao().selectFrom(Regiao.class).where(it -> it.equal("regiao", "0" + codRegiao)).singleResult();
        return regiao;
    }

    protected TabPreco getTabPreco(VSdDadosProduto item, Regiao regiao) {
        if (item.getListTabPreco().stream().anyMatch(it -> it.getId().getRegiao().equals(regiao.getRegiao()))) {
            return item.getListTabPreco().stream().filter(it -> it.getId().getRegiao().equals(regiao.getRegiao())).findFirst().orElse(new TabPreco(item, regiao));
        }
        TabPreco tab = new TabPreco(item, regiao);
        item.getListTabPreco().add(tab);
        return tab;
    }

    @Override
    public void closeWindow() {

    }
}
