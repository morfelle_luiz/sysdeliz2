package sysdeliz2.controllers.views.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CadastroRepresentanteController extends WindowBase {

    protected List<Represen> representantes = new ArrayList<>();

    public CadastroRepresentanteController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getRepresentantes(Object[] codReps, String nomeReponsavel, String nomeRep, String ativo, String tipo) {
        Object[] pCod = codReps == null ? new Object[]{} : codReps;

        representantes = (List<Represen>) new FluentDao().selectFrom(Represen.class).where(it -> it
                .equal("ativo", Boolean.parseBoolean(ativo), TipoExpressao.AND, b -> !ativo.equals("T"))
                .equal("tipo", tipo, TipoExpressao.AND, b -> !tipo.equals("T"))
                .isIn("codRep", pCod, TipoExpressao.AND, b -> pCod.length > 0)
        ).resultList();

        String pNomeResponsavel = nomeReponsavel == null ? "" : nomeReponsavel;
        String pNomeRep = nomeRep == null ? "" : nomeRep;

        if (!pNomeRep.equals(""))
            representantes = representantes.stream().filter(it -> it.getNome().contains(pNomeRep)).collect(Collectors.toList());
        if (!pNomeResponsavel.equals(""))
            representantes = representantes.stream().filter(entry -> entry.getRespon() != null && !entry.getRespon().isEmpty()).filter(it -> it.getRespon().contains(pNomeResponsavel)).collect(Collectors.toList());

    }

}
