package sysdeliz2.controllers.views.cadastros;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdTipoSolicitacaoExp;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.window.base.CadastroBase;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CadastroTiposSolicitacaoExpController extends CadastroBase<SdTipoSolicitacaoExp> {

    private final FormFieldText tipoFilter = FormFieldText.create(field -> {
        field.title("Tipo");
        field.toUpper();
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(400);
        field.toUpper();
    });

    public CadastroTiposSolicitacaoExpController() {
        super("Cadastro de Tipo Solicitação Expedição", ImageUtils.getImage(ImageUtils.Icon.EXPEDICAO));
        super.tboxFiltroPadrao.toUpper();
        super.tboxFiltroPadrao.placeHolder("Buscar por Código");
        super.cpaneFormulario.add(FormBox.create(boxFiltro -> {
            boxFiltro.horizontal();
            boxFiltro.add(tipoFilter.build(), descricaoField.build());
        }));
    }

    @Override
    protected void initializeComponents() {
        ((FormBox) super.lookup("#box-find-filters")).remove(super.lookup("#separator-btn-find-advanced"));
        ((FormBox) super.lookup("#box-find-filters")).remove(btnFindAdvanced);
        tipoFilter.editable.bind(navegation.inEdition);
        descricaoField.editable.bind(navegation.inEdition);
    }

    @Override
    protected FormTableColumn[] setColumnsTable() {
        return new FormTableColumn[]{
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTipoSolicitacaoExp, SdTipoSolicitacaoExp>, ObservableValue<SdTipoSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(700);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTipoSolicitacaoExp, SdTipoSolicitacaoExp>, ObservableValue<SdTipoSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Tipo*/
        };
    }

    @Override
    protected void btnFindDefaultOnAction(String defaultValue) {
        AtomicReference<List<SdTipoSolicitacaoExp>> listMarcas = new AtomicReference<>(Collections.emptyList());
        new RunAsyncWithOverlay(this).exec(task -> {
            listMarcas.set((List<SdTipoSolicitacaoExp>) new FluentDao().selectFrom(SdTipoSolicitacaoExp.class)
                    .where(it -> it
                            .like("tipo", super.tboxFiltroPadrao.value.getValueSafe(), TipoExpressao.AND, when -> !super.tboxFiltroPadrao.value.getValueSafe().equals("") )).orderBy("tipo", OrderType.ASC).resultList());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                super.listRegisters.set(FXCollections.observableArrayList(listMarcas.get()));
            }
        });
    }

    @Override
    protected void btnFindAdvanced() {

    }

    @Override
    protected void bindData(SdTipoSolicitacaoExp selectItem) {
        tipoFilter.value.bind(selectItem.tipoProperty());
        descricaoField.value.bind(selectItem.descricaoProperty());
    }

    @Override
    protected void unbindData() {
        tipoFilter.value.unbind();
        descricaoField.value.unbind();
    }

    @Override
    protected void bindBidirectionalData(SdTipoSolicitacaoExp selectItem) {
        Bindings.bindBidirectional(tipoFilter.value, selectItem.tipoProperty());
        Bindings.bindBidirectional(descricaoField.value, selectItem.descricaoProperty());
    }

    @Override
    protected void unbindBidirectionalData(SdTipoSolicitacaoExp selectItem) {
        tipoFilter.value.unbindBidirectional(selectItem.tipoProperty());
        descricaoField.value.unbindBidirectional(selectItem.descricaoProperty());
    }

    @Override
    protected void clearForm() {
        tipoFilter.clear();
        descricaoField.clear();
    }

    @Override
    protected boolean validationForm() {
        return true;
    }

    @Override
    public void closeWindow() {

    }
}
