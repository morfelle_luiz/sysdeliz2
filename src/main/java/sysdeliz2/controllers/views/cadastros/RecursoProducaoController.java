package sysdeliz2.controllers.views.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.cadastros.SdFamiliasRecurso;
import sysdeliz2.models.sysdeliz.cadastros.SdRecursoProducao;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.lang.annotation.Native;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class RecursoProducaoController extends WindowBase {

    public RecursoProducaoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected List<SdRecursoProducao> getRecursos(Object[] familias, Object[] setores, String ativo, Object[] colecoes, Object[] turnos, Object[] fornecedores, Object[] controladores) {
        List<SdRecursoProducao> result = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                .where(eb -> eb
                        .isIn("setorExcia.codigo", setores, TipoExpressao.AND, b -> setores.length > 0)
                        .isIn("fornecedor.codcli", fornecedores, TipoExpressao.AND, b -> fornecedores.length > 0)
                        .isIn("turno.codigo", turnos, TipoExpressao.AND, b -> turnos.length > 0)
                        .isIn("colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("controlador.codigo", controladores, TipoExpressao.AND, b -> controladores.length > 0)
                        .equal("ativo", ativo.equals("S"), TipoExpressao.AND, b -> !ativo.equals("A")))
                .orderBy(Arrays.asList(new Ordenacao("fornecedor.codcli"), new Ordenacao("colecao.codigo")))
                .resultList();
        result.forEach(JPAUtils::refresh);

        if (familias.length > 0) {
            result.removeIf(recurso -> !recurso.getFamilias().stream().anyMatch(fam -> Arrays.asList(familias).stream().anyMatch(famFilter -> famFilter.equals(fam.getId().getFamilia().getCodigo()))));
        }
        result.forEach(recurso -> recurso.getFamilias().sort((o1, o2) -> o1.getPrioridade().compareTo(o2.getPrioridade())));

        return result;
    }

    protected List<SdRecursoProducao> getRecursosColecao(String colecao) {
        List<SdRecursoProducao> result = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                .where(eb -> eb
                        .equal("colecao.codigo", colecao))
                .resultList();
        result.forEach(recurso -> recurso.getFamilias().sort((o1, o2) -> o1.getPrioridade().compareTo(o2.getPrioridade())));
        return result;
    }

    protected boolean exist(String setorExcia, String colecao, String fornecedor, Integer turno) {
        if (new FluentDao().selectFrom(SdRecursoProducao.class)
                .where(eb -> eb.equal("setorExcia.codigo", setorExcia)
                        .equal("colecao.codigo", colecao)
                        .equal("fornecedor.codcli", fornecedor)
                        .equal("turno.codigo", turno))
                .singleResult() != null)
            return true;
        return false;
    }

    protected void delete(SdRecursoProducao recurso) {
        new FluentDao().delete(recurso);
        SysLogger.addSysDelizLog("Recursos de Produção", TipoAcao.EXCLUIR, recurso.codigo(), "Recurso excluído.");
    }

    protected void deleteFamilia(SdFamiliasRecurso familia) throws SQLException {
        if (familia.getId().getRecurso().getSetorExcia() == null)
            return;

        new NativeDAO().runNativeQueryUpdate("delete from sd_familias_recurso_001 where setor_excia = '%s' and fornecedor = '%s' and colecao = '%s' and turno = '%s' and familia = '%s'",
                familia.getId().getRecurso().getSetorExcia().getCodigo(), familia.getId().getRecurso().getFornecedor().getCodcli(), familia.getId().getRecurso().getColecao().getCodigo(), familia.getId().getRecurso().getTurno().getCodigo(), familia.getId().getFamilia().getCodigo());
        SysLogger.addSysDelizLog("Recursos de Produção", TipoAcao.EXCLUIR, familia.getId().getRecurso().codigo(), "Excluída a família " + familia.getId().getFamilia().toString() + " do recurso.");
    }

    protected SdRecursoProducao save(SdRecursoProducao recurso) throws SQLException {
        return new FluentDao().persist(recurso);
    }

    protected void update(SdRecursoProducao recurso) {
        new FluentDao().merge(recurso);
    }

    @Override
    public void closeWindow() {

    }
}
