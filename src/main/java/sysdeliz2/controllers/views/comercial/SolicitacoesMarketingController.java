package sysdeliz2.controllers.views.comercial;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.comercial.gestaopedidos.MarketingPedidoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.apps.mkt.MktImagemSolicitacao;
import sysdeliz2.models.apps.mkt.MktItemSolicitacao;
import sysdeliz2.models.apps.mkt.MktSolicitacao;
import sysdeliz2.models.apps.mkt.MktStatusSolicitacao;
import sysdeliz2.models.sysdeliz.comercial.SdItensMktPedido;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedido;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedidoPK;
import sysdeliz2.models.ti.FaixaItem;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdEstoqueB2B;
import sysdeliz2.models.view.VSdMrpEstoqueB2b;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static sysdeliz2.utils.sys.ImageUtils.noPhoto;

public class SolicitacoesMarketingController extends WindowBase {

    protected List<MktSolicitacao> solicitacoes = new ArrayList<>();
    protected List<MktItemSolicitacao> itensSolicitacao = new ArrayList<>();
    protected List<MktItemSolicitacao> materiais = new ArrayList<>();

    protected List<MktStatusSolicitacao> statusList = (List<MktStatusSolicitacao>) new FluentDao().selectFrom(MktStatusSolicitacao.class).where(it -> it.equal("visivel", true)).resultList();

    public SolicitacoesMarketingController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarSolicitacoes(Object[] clientes, Object[] representantes, Object[] colecoes, Object[] marcas, MktStatusSolicitacao status) {
//        if (solicitacoes != null && solicitacoes.size() > 0) JPAUtils.clearEntitys(solicitacoes);
        Integer codStatus = status == null ? 0 : status.getCodigo();
        solicitacoes = (List<MktSolicitacao>) new FluentDao().selectFrom(MktSolicitacao.class).where(it -> it
                .isIn("codcli.codcli", clientes, TipoExpressao.AND, when -> clientes.length > 0)
                .isIn("codrep.codrep", representantes, TipoExpressao.AND, when -> representantes.length > 0)
                .isIn("colecao.codigo", colecoes, TipoExpressao.AND, when -> colecoes.length > 0)
                .isIn("marca.codigo", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                .equal("status.codigo", codStatus, TipoExpressao.AND, when -> codStatus != 0)
                .notIn("status.codigo", new String[]{"1", "7"}, TipoExpressao.AND, when -> true)
        ).resultList();
    }

    protected void buscarMateriais(Object[] materiaisList, Object[] marcas, String imgAdicionada, String enviadoFornecedor, String comImg) {
        if (materiais != null && materiais.size() > 0) JPAUtils.clearEntitys(materiais);

        materiais = (List<MktItemSolicitacao>) new FluentDao().selectFrom(MktItemSolicitacao.class).where(it -> it
                .isIn("tipo.id", materiaisList, TipoExpressao.AND, when -> materiaisList.length > 0)
                .isIn("solicitacao.marca.codigo", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                .isNull("pathImagem", TipoExpressao.AND, when -> imgAdicionada.equals("N"))
                .isNotNull("pathImagem", TipoExpressao.AND, when -> imgAdicionada.equals("S"))
                .equal("enviadoFornecedor", enviadoFornecedor.equals("S"), TipoExpressao.AND, when -> !enviadoFornecedor.equals("A"))
                .equal("tipo.imagem", comImg.equals("S"), TipoExpressao.AND, when -> !comImg.equals("A"))
                .notIn("solicitacao.status.codigo", new String[]{"1", "7"}, TipoExpressao.AND, when -> true)
        ).resultList();


    }

    protected void carregaItens(MktSolicitacao solicitacao) {
        if (itensSolicitacao != null && itensSolicitacao.size() > 0) JPAUtils.clearEntitys(itensSolicitacao);
        itensSolicitacao = (List<MktItemSolicitacao>) new FluentDao().selectFrom(MktItemSolicitacao.class).where(it -> it
                .equal("solicitacao.id", solicitacao.getId())
        ).resultList();
    }

    protected boolean adicionarMarketing(Pedido pedido, Integer qtde) {
        AtomicBoolean result = new AtomicBoolean(false);

        new Fragment().show(fragment -> {
            fragment.title("Adicionar Marketing no Pedido");
            fragment.size(600.0, 600.0);

            // <editor-fold defaultstate="collapsed" desc="beans">
            final BooleanProperty hasSubItem = new SimpleBooleanProperty(false);
            final ListProperty<SdItensMktPedido> itensMktBean = new SimpleListProperty<>(FXCollections.observableArrayList());
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="components">
            final FormFieldSingleFind<Produto> fieldAddProduto = FormFieldSingleFind.create(Produto.class, field -> {
                field.title("Produto");
                field.width(400.0);
                field.editable.set(false);
                field.value.setValue(new FluentDao().selectFrom(Produto.class).where(it -> it.equal("codigo", "MKT001")).singleResult());
            });
            final FormFieldText fieldAddQtde = FormFieldText.create(field -> {
                field.title("Qtde");
                field.addStyle("lg");
                field.width(60.0);
                field.alignment(Pos.CENTER);
                field.editable.set(false);
                field.value.set("1");
                field.mask(FormFieldText.Mask.INTEGER);
            });
            final FormFieldTextArea fieldAddObservacao = FormFieldTextArea.create(field -> {
                field.title("Observações");
//                field.height(70.0);
                field.expanded();
                field.minHeight(70.0);
            });
            final FormFieldSingleFind<Produto> fieldAddProdutoSubItem = FormFieldSingleFind.create(Produto.class, field -> {
                field.title("Produto");
                field.width(350.0);
            });
            final FormTableView<SdItensMktPedido> tblItensMkt = FormTableView.create(SdItensMktPedido.class, table -> {
                table.title("Itens");
                table.expanded();
                table.items.bind(itensMktBean);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(200.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProduto()));
                        }).build() /*Código*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Tam");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tam*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Qtde*/
                );
            });
            // </editor-fold>

            fragment.box.getChildren().add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(FormBox.create(boxDadosProduto -> {
                    boxDadosProduto.horizontal();
                    boxDadosProduto.add(fieldAddProduto.build(), fieldAddQtde.build());
                }));
                content.add(fieldAddObservacao.build());
                content.add(FormTitledPane.create(tpane -> {
                    tpane.title("Adicionar Itens");
                    tpane.expandedProperty().unbindBidirectional(tpane.collapsabled);
                    tpane.setExpanded(true);
                    tpane.expanded();
                    tpane.setOnMouseClicked(evt -> {
                    });
                    tpane.add(FormBox.create(contentSubItem -> {
                        contentSubItem.vertical();
                        contentSubItem.expanded();
                        contentSubItem.add(FormBox.create(boxDadosProduto -> {
                            boxDadosProduto.horizontal();
                            boxDadosProduto.add(fieldAddProdutoSubItem.build());
                            boxDadosProduto.add(FormButton.create(btnAdicionar -> {
                                btnAdicionar.title("Add. Grade");
                                btnAdicionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                btnAdicionar.addStyle("lg").addStyle("success");
                                btnAdicionar.setAction(evt -> {
                                    if (fieldAddProdutoSubItem.value.get() != null)
                                        new Fragment().show(frgAddGrade -> {
                                            frgAddGrade.title("Digitação de Grade de Produto");
                                            frgAddGrade.size(1100.0, 700.0);

                                            Produto produtoAdicionado = fieldAddProdutoSubItem.value.get();
                                            File files = new File("K:/Loja Virtual/imagens/produtos/" + (produtoAdicionado.getMarca().getDescricao().toUpperCase()) + "/");
                                            List<VSdMrpEstoqueB2b> estoqueProduto = (List<VSdMrpEstoqueB2b>) new FluentDao().selectFrom(VSdMrpEstoqueB2b.class)
                                                    .where(eb -> eb.equal("codigo", produtoAdicionado.getCodigo())).resultList();

                                            final List<MarketingPedidoController.GradeDigitacao> gradeDigitacao = new ArrayList<>();
                                            estoqueProduto.stream().forEach(estoque -> {
                                                estoque.getTamanhos().forEach(tams -> {
                                                    MarketingPedidoController.GradeDigitacao grade = new MarketingPedidoController.GradeDigitacao(tams.getId().getCor(), estoque.getDescCor(), tams.getId().getTam(), tams.getId().getEntrega(), estoque.getDescEntrega(), 0);
                                                    gradeDigitacao.add(grade);
                                                });
                                            });
                                            final IntegerProperty qtdeTotalPecas = new SimpleIntegerProperty();
                                            final FormTableView<VSdMrpEstoqueB2b> coresProduto = FormTableView.create(VSdMrpEstoqueB2b.class, table -> {
                                                table.title("Cores Produto");
                                                table.expanded();
                                                table.items.set(FXCollections.observableList(estoqueProduto.stream().filter(estoque -> estoque.getEntrega().equals("0000")).collect(Collectors.toList())));
                                                table.columns(
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Código");
                                                            cln.width(50.0);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        }).build() /*Código*/,
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Descrição");
                                                            cln.width(150.0);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));
                                                        }).build() /*Descrição*///,
//                                                        FormTableColumn.create(cln -> {
//                                                            cln.title("Qtde");
//                                                            cln.width(40.0);
//                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
//                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
//                                                        }).build() /*Qtde*/
                                                );
                                                List<FaixaItem> grade = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class).where(it -> it.equal("faixaItemId.faixa", produtoAdicionado.getFaixa().getCodigo())).orderBy("posicao", OrderType.ASC).resultList();
                                                grade.forEach(tam -> {
                                                    FormTableColumn<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b> clnGrade = FormTableColumn.create(cln -> {
                                                        cln.title(tam.getFaixaItemId().getTamanho());
                                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) features -> {
                                                            VSdMrpEstoqueB2b estoque = features.getValue();
                                                            return new ReadOnlyObjectWrapper(estoque);
                                                        });
                                                        cln.format(param -> new TableCell<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>() {
                                                            @Override
                                                            protected void updateItem(VSdMrpEstoqueB2b item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                setGraphic(null);
                                                                if (item != null && !empty) {
                                                                    //setText(StringUtils.toIntegerFormat(item));
                                                                    VSdEstoqueB2B estoqueTam = item.getTamanho(tam.getFaixaItemId().getTamanho());
                                                                    setGraphic(FormBox.create(boxGrade -> {
                                                                        boxGrade.vertical();
                                                                        boxGrade.add(FormFieldText.create(field -> {
                                                                            MarketingPedidoController.GradeDigitacao gradeDigitada = new MarketingPedidoController.GradeDigitacao("-",
                                                                                    "-", tam.getFaixaItemId().getTamanho(),
                                                                                    "-", "-", 0);
                                                                            if (estoqueTam != null) {
                                                                                gradeDigitada = gradeDigitacao.stream()
                                                                                        .filter(tamDigitacao ->
                                                                                                tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                        tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega()) &&
                                                                                                        tamDigitacao.getTam().equals(estoqueTam.getId().getTam()))
                                                                                        .peek(gradeDigitacao1 -> System.out.println(gradeDigitacao1.getCor().concat("-").concat(gradeDigitacao1.getTam()).concat(":").concat(gradeDigitacao1.getEntrega())
                                                                                                .concat("|").concat(estoqueTam.getId().getCor())))
                                                                                        .findFirst()
                                                                                        .orElse(new MarketingPedidoController.GradeDigitacao(estoqueTam.getId().getCor(), "-", estoqueTam.getId().getTam(), estoqueTam.getId().getEntrega(), "-", 0));
                                                                            }
                                                                            field.withoutTitle();
                                                                            field.value.set(String.valueOf(gradeDigitada.getQtde()));
                                                                            field.alignment(FormFieldText.Alignment.CENTER);
                                                                            field.mask(FormFieldText.Mask.INTEGER);
                                                                            field.addStyle("xs");
                                                                            field.width(25.0);
                                                                            MarketingPedidoController.GradeDigitacao finalGradeDigitada = gradeDigitada;
                                                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                                                if (newValue == null || newValue.length() == 0) {
                                                                                    finalGradeDigitada.setQtde(0);
                                                                                } else {
                                                                                    finalGradeDigitada.setQtde(Integer.parseInt(newValue));
                                                                                }
                                                                                item.setQtdeDigitado(gradeDigitacao.stream()
                                                                                        .filter(tamDigitacao ->
                                                                                                tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                        tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega())).mapToInt(MarketingPedidoController.GradeDigitacao::getQtde).sum());
                                                                                qtdeTotalPecas.set(gradeDigitacao.stream().mapToInt(MarketingPedidoController.GradeDigitacao::getQtde).sum());
                                                                            });
                                                                        }).build());
//                                                                            boxGrade.add(FormFieldText.create(field -> {
//                                                                                field.withoutTitle();
//                                                                                field.editable(false);
//                                                                                field.alignment(FormFieldText.Alignment.CENTER);
//                                                                                field.value.set(String.valueOf(estoqueTam.getQtd()));
//                                                                                field.addStyle(estoqueTam.getQtd() <= 0 ? "danger" : "").addStyle("xs");
//                                                                                field.width(25.0);
//                                                                            }).build());
                                                                    }));
                                                                }
                                                            }
                                                        });
                                                    });
                                                    table.addColumn(clnGrade);
                                                });
                                            });

                                            frgAddGrade.box.getChildren().add(FormBox.create(dadosDoProduto -> {
                                                dadosDoProduto.horizontal();
                                                dadosDoProduto.expanded();
                                                dadosDoProduto.add(FormBox.create(dadosProduto -> {
                                                    dadosProduto.vertical();
                                                    dadosProduto.expanded();
                                                    dadosProduto.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.label("Produto");
                                                        field.value.set(produtoAdicionado.toString());
                                                        field.editable(false);
                                                    }).build());
                                                    dadosProduto.add(coresProduto.build());
                                                }));
                                                dadosDoProduto.add(FormBox.create(fotosProduto -> {
                                                    fotosProduto.vertical();
                                                    fotosProduto.width(250.0);
                                                    List<Node> imagesProdutos = new ArrayList<>();
                                                    try {
                                                        List<File> fotosPasta = Arrays.asList(files.listFiles((dir, name) -> name.contains(produtoAdicionado.getCodigo())));
                                                        fotosPasta.sort(Comparator.comparing(File::getName));
                                                        for (File imagem : fotosPasta) {
                                                            ImageView imageProduto = null;
                                                            imageProduto = new ImageView(new Image(new FileInputStream(imagem)));
                                                            imageProduto.setFitHeight(500.0);
                                                            imageProduto.setFitWidth(500.0);
                                                            imageProduto.setPickOnBounds(true);
                                                            imageProduto.setPreserveRatio(true);
                                                            imagesProdutos.add(imageProduto);
                                                            break;
                                                        }
                                                        if (imagesProdutos.size() == 0) {
                                                            ImageView imageProduto = null;

                                                            imageProduto = new ImageView(noPhoto());
                                                            imageProduto.setFitHeight(500.0);
                                                            imageProduto.setFitWidth(500.0);
                                                            imageProduto.setPickOnBounds(true);
                                                            imageProduto.setPreserveRatio(true);
                                                            imagesProdutos.add(imageProduto);
                                                        }
                                                    } catch (NullPointerException e) {
                                                        e.printStackTrace();
                                                    } catch (FileNotFoundException e) {
                                                        e.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(e);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                    fotosProduto.add(FormCarousel.create(carousel -> {
                                                        if (imagesProdutos.size() > 0)
                                                            carousel.setNodesCarousel(imagesProdutos);
                                                    }));
                                                    fotosProduto.add(FormFieldText.create(field -> {
                                                        field.title("Total de Peças");
                                                        field.value.bind(qtdeTotalPecas.asString());
                                                        field.editable(false);
                                                    }).build());
                                                    fotosProduto.add(FormBox.create(toolbarGrade -> {
                                                        toolbarGrade.horizontal();
                                                        toolbarGrade.add(FormButton.create(adicionarGrade -> {
                                                            adicionarGrade.title("Adicionar Grade");
                                                            adicionarGrade.addStyle("lg").addStyle("success");
                                                            adicionarGrade.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                                            adicionarGrade.setAction(evtAdicionar -> {
                                                                gradeDigitacao.stream().filter(item -> item.getQtde() > 0).forEach(item -> {
                                                                    itensMktBean.get().add(new SdItensMktPedido(pedido.getNumero(), fieldAddProduto.value.get().getCodigo(), "0", produtoAdicionado, item.getCor(), item.getTam(), item.getQtde()));
                                                                });
                                                                frgAddGrade.close();
                                                                fieldAddProdutoSubItem.clear();
                                                            });
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        });
                                });
                            }));
                        }));
                        contentSubItem.add(tblItensMkt.build());
                    }));
                }));
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAdicionarMkt -> {
                btnAdicionarMkt.title("Adicionar");
                btnAdicionarMkt.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                btnAdicionarMkt.addStyle("success");
                btnAdicionarMkt.setAction(evt -> {
                    try {
                        if (adicionarMarketing(pedido.getNumero(), fieldAddProduto.value.get(), Integer.parseInt(fieldAddQtde.value.get()), fieldAddObservacao.value.get(), itensMktBean.get())) {
                            fragment.close();
                            result.set(true);
                            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, pedido.getNumero(), "Incluído marketing " + fieldAddProduto.value.get().getCodigo() + " no pedido.");
                            MessageBox.create(message -> {
                                message.message("Marketing cadastrado com sucesso no pedido.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        }
                    } catch (SQLException | JRException | IOException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });

        return result.get();
    }

    private boolean adicionarMarketing(String pedido, Produto produto, int qtde, String observacao, ObservableList<SdItensMktPedido> itens) throws SQLException, JRException, IOException {

        if (produto.getSdProduto().isConfiguravel() && itens.size() == 0) {
            MessageBox.create(message -> {
                message.message("Você não incluiu os itens para o marketing selecionado.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }

        VSdDadosProdutoBarra produtoEstoque = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb
                        .equal("codigo", produto.getCodigo())
                        .equal("cor", "UN")
                        .equal("tam", "UN"))
                .singleResult();
        if (produtoEstoque == null) {
            MessageBox.create(message -> {
                message.message("Marketing sem barra cadastrada.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }
        if (produtoEstoque.getBarra() == null || produtoEstoque.getBarra28() == null || produtoEstoque.getLocal() == null) {
            MessageBox.create(message -> {
                message.message("O marketing selecionado não está com o cadastro completo, verifique o cadastro:\nBarra 28: " + produtoEstoque.getBarra28() + "\nLocal: " + produtoEstoque.getLocal() + "\nBarra: " + produtoEstoque.getBarra());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }

        SdMktPedido mktPedido = new SdMktPedido();
        mktPedido.setDatacad(LocalDateTime.now());
        mktPedido.setUsuario(Globals.getUsuarioLogado().getUsuario());
        mktPedido.setQtde(qtde);
        mktPedido.setObs(observacao);

        VSdDadosProduto produtoMkt = new FluentDao().selectFrom(VSdDadosProduto.class).where(eb -> eb.equal("codigo", produto.getCodigo())).singleResult();
        mktPedido.setId(new SdMktPedidoPK(pedido, produtoMkt, "0"));

        // criando as barras do produto
        List<String> ordemBarra = new ArrayList<>();
        List<SdMktPedido> mktsPedido = (List<SdMktPedido>) new FluentDao().selectFrom(SdMktPedido.class).where(eb -> eb.equal("id.pedido", pedido).equal("id.codigo.codigo", produto.getCodigo())).resultList();
        for (int i = 1; i <= qtde; i++) {
            ordemBarra.add(StringUtils.lpad(mktsPedido.stream().mapToInt(SdMktPedido::getQtde).sum() + i, 2, "0"));
        }

        for (SdItensMktPedido item : itens)
            new FluentDao().persist(item);

        mktPedido = new FluentDao().persist(mktPedido);
        JPAUtils.getEntityManager().refresh(mktPedido);

        if (produto.getSdProduto().isConfiguravel()) {
            entradaMktEstoque(mktPedido, produtoEstoque);
            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, produto.getCodigo(), "Incluído produto no estoque com movimentação.");
        }

        if (itens.size() > 0) {
            if (produto.getSdProduto().isBaixaEstoque())
                for (SdItensMktPedido item : itens)
                    saidaProdutoEstoque(item, qtde);

            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, pedido, "Incluído os itens " + itens.stream().map(item -> item.getId().getProduto().getCodigo()).distinct().collect(Collectors.joining(", ")) +
                    " no marketing " + produto.getCodigo() + " no pedido.");

            for (String barra : ordemBarra) {
                new ReportUtils().config()
                        .addReport(ReportUtils.ReportFile.ESPELHO_MARKETING,
                                new ReportUtils.ParameterReport("p_pedido", pedido),
                                new ReportUtils.ParameterReport("p_codigo", produto.getCodigo()),
                                new ReportUtils.ParameterReport("p_ordem", barra))
                        .view().printWithDialog();
            }
        }
        return true;
    }

    private void saidaProdutoEstoque(SdItensMktPedido itemMkt, Integer qtdeMkt) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade - %s where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '0005'",
                itemMkt.getQtde() * qtdeMkt, itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'S',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", itemMkt.getId().getPedido(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam(), "0005", itemMkt.getQtde() * qtdeMkt,
                Globals.getUsuarioLogado().getUsuario(), itemMkt.getId().getPedido(), itemMkt.getId().getCodigo(),
                itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));
    }

    private void entradaMktEstoque(SdMktPedido mktCadastro, VSdDadosProdutoBarra mktEstoque) throws SQLException {
        // update da PA_ITEN entrando no estoque
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "merge into pa_iten_001 pai using dual on\n" +
                        "(pai.codigo = '%s' and pai.cor = '%s' and pai.tam = '%s' and pai.deposito = '0005')\n" +
                        "when matched then \n" +
                        "  update set pai.quantidade = pai.quantidade + %s\n" +
                        "when not matched then \n" +
                        "  insert (quantidade, tam, cor, barra28, codigo, barra, lote, ativo, deposito, ordem, local)\n" +
                        "  values (%s, 'UN', 'UN', '%s', '%s', '%s', '000000', 'S', '0005', 1, '%s')",
                mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN",
                mktCadastro.getQtde(),
                mktCadastro.getQtde(), mktEstoque.getBarra28(), mktCadastro.getId().getCodigo().getCodigo(), mktEstoque.getBarra(), mktEstoque.getLocal()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN", "0005", mktCadastro.getQtde(),
                Globals.getUsuarioLogado().getUsuario(), mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(),
                mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));
    }

    @Override
    public void closeWindow() {

    }
}
