package sysdeliz2.controllers.views.comercial.bid;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.comercial.SdBid;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BidRepresentanteController extends WindowBase {

    public BidRepresentanteController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected List<SdBid> getBids(Object[] representantes, Object[] marcas, Object[] colecoes, Object[] regioes, Object[] ufs, String globais) {
        List<SdBid> bids = new ArrayList<>();
        if (!globais.equals("C"))
            bids.addAll((List<SdBid>) new FluentDao().selectFrom(SdBid.class)
                    .where(eb -> eb
                            .isIn("id.codrep", representantes, TipoExpressao.AND, b -> representantes.length > 0)
                            .between("id.codrep", (Object) "0000", (Object) "9999", TipoExpressao.AND, b -> representantes.length == 0)
                            .isIn("id.codcol", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                            .isIn("id.codmar", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                            .isIn("ufrep", ufs, TipoExpressao.AND, b -> ufs.length > 0)
                            .isIn("codreg", regioes, TipoExpressao.AND, b -> regioes.length > 0))
                    .resultList());
        if (!globais.equals("A"))
            bids.addAll((List<SdBid>) new FluentDao().selectFrom(SdBid.class)
                    .where(eb -> eb
                            .isIn("id.codrep", new String[]{"000", "SUL", "SUD", "NOR", "NOE", "CEN"})
                            .isIn("id.codcol", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                            .isIn("id.codmar", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                            .isIn("ufrep", ufs, TipoExpressao.AND, b -> ufs.length > 0)
                            .isIn("codreg", regioes, TipoExpressao.AND, b -> regioes.length > 0))
                    .resultList());

        bids.forEach(SdBid::refresh);
        bids.sort((o1, o2) -> o1.getId().getCodrep().compareTo(o2.getId().getCodrep()));
        return bids;
    }

    @Override
    public void closeWindow() {

    }
}
