package sysdeliz2.controllers.views.comercial.bid;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.comercial.SdMetasBid;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.SysLogger;

import java.util.Arrays;
import java.util.List;

public class MetasBidRepresentanteController extends WindowBase {


    public MetasBidRepresentanteController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected List<SdMetasBid> getMetasRepresentantes(Object[] colecoes, Object[] marcas, Object[] representantes) {
        return (List<SdMetasBid>) new FluentDao().selectFrom(SdMetasBid.class)
                .where(eb -> eb
                        .isIn("id.representante.codRep", representantes, TipoExpressao.AND, b -> representantes.length > 0)
                        .isIn("id.colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("id.marca.codigo", marcas, TipoExpressao.AND, b -> marcas.length > 0))
                .orderBy(Arrays.asList(new Ordenacao("id.colecao.codigo"), new Ordenacao("id.representante.codRep"), new Ordenacao("id.marca.codigo")))
                .resultList();
    }

    protected void excluirMetaBid(SdMetasBid item) {
        new FluentDao().delete(item);
        SysLogger.addSysDelizLog("Metas BID", TipoAcao.EXCLUIR, item.getId().getRepresentante().getCodRep(),
                "Excluído meta " + item.getId().getTipo() + " do representante na coleção/marca " + item.getId().getColecao().getCodigo()+"/"+item.getId().getMarca().getCodigo());
    }

    @Override
    public void closeWindow() {

    }
}
