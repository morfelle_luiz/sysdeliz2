package sysdeliz2.controllers.views.comercial.bid;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.view.VSdBidCidadesRep;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RelatorioBIDCidadeController extends WindowBase {

    public RelatorioBIDCidadeController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected List<VSdBidCidadesRep> cidades = new ArrayList<>();

    protected void buscarCidades(Object[] codMarca, Object[] codRep, Object[] codCidade, String nomeCidade, Object[] estado, Boolean incluirSemRep, Boolean apenasSemRep) {
        JPAUtils.clearEntitys(cidades);

        Object[] pCodMarca = codMarca == null ? new Object[]{} : codMarca;
        Object[] pCodRep = codRep == null ? new Object[]{} : codRep;
        Object[] pEstado = estado == null ? new Object[]{} : estado;
        Object[] pCidade = codCidade == null ? new Object[]{} : codCidade;

//        if (apenasSemRep) cidades = buscarApenasCidadesSemRep(pEstado, pCidade, pCodMarca);
//        else if (incluirSemRep) cidades = buscarCidadesIncluindoAsSemRep(pCodMarca, pCodRep, pEstado, pCidade);
//        else cidades = buscarCidadesComRep(pCodMarca, pCodRep, pEstado, pCidade);

        cidades = (List<VSdBidCidadesRep>) new FluentDao().selectFrom(VSdBidCidadesRep.class).where(it -> it
                .isIn("id.codMarca.codigo", pCodMarca, TipoExpressao.AND, b -> pCodMarca.length > 0 )
                .isIn("id.codRep.codRep", pCodRep, TipoExpressao.AND, b -> pCodRep.length > 0)

                .notEqual("id.codRep.codRep", "0261", TipoExpressao.AND, when -> !apenasSemRep && !incluirSemRep)
                .equal("id.codRep.codRep", "0261",TipoExpressao.AND, when -> apenasSemRep )

                .isIn("id.codCid.codEst.sigla", pEstado, TipoExpressao.AND, b -> pEstado.length > 0)
                .isIn("id.codCid.codCid", pCidade, TipoExpressao.AND, b -> pCidade.length > 0)
                .like("id.codCid.nomeCid", nomeCidade, TipoExpressao.AND, b -> !nomeCidade.equals(""))
        ).orderBy("id.codRep.codRep", OrderType.DESC).resultList();

//        if (!nomeCidade.isEmpty())
//            cidades = cidades.stream().filter(it -> it.getId().getCodCid().getNomeCid().contains(nomeCidade)).collect(Collectors.toList());
    }

    private List<VSdBidCidadesRep> buscarCidadesComRep(Object[] pCodMarca, Object[] pCodRep, Object[] pEstado, Object[] pCidade) {
        return (List<VSdBidCidadesRep>) new FluentDao().selectFrom(VSdBidCidadesRep.class).where(it -> it
                .isIn("id.codMarca.codigo", pCodMarca, TipoExpressao.AND, b -> pCodMarca.length > 0 )
                .isIn("id.codRep.codRep", pCodRep, TipoExpressao.AND, b -> pCodRep.length > 0)
                .notEqual("id.codRep.codRep", "0261")
                .isIn("id.codCid.codEst.sigla", pEstado, TipoExpressao.AND, b -> pEstado.length > 0)
                .isIn("id.codCid.codCid", pCidade, TipoExpressao.AND, b -> pCidade.length > 0)
        ).orderBy("id.codRep.codRep", OrderType.DESC).resultList();
    }

    private List<VSdBidCidadesRep> buscarCidadesIncluindoAsSemRep(Object[] pMarca, Object[] pCodRep, Object[] pEstado, Object[] pCidade) {
        return (List<VSdBidCidadesRep>) new FluentDao().selectFrom(VSdBidCidadesRep.class).where(it -> it
                .isIn("id.codMarca.codigo", pMarca, TipoExpressao.AND, b -> pMarca.length > 0)
                .isIn("id.codRep.codRep", pCodRep, TipoExpressao.AND, b -> pCodRep.length > 0)
                .isIn("id.codCid.codEst.sigla", pEstado, TipoExpressao.AND, b -> pEstado.length > 0)
                .isIn("id.codCid.codCid", pCidade, TipoExpressao.AND, b -> pCidade.length > 0)
        ).orderBy("id.codRep.codRep", OrderType.DESC).resultList();
    }

    private List<VSdBidCidadesRep> buscarApenasCidadesSemRep(Object[] pEstado, Object[] pCidade, Object[] pMarca) {
        return (List<VSdBidCidadesRep>) new FluentDao().selectFrom(VSdBidCidadesRep.class).where(it -> it
                .equal("id.codRep.codRep", "0261")
                .isIn("id.codMarca.codigo", pMarca, TipoExpressao.AND, b -> pMarca.length > 0)
                .isIn("id.codCid.codEst.sigla", pEstado, TipoExpressao.AND, b -> pEstado.length > 0)
                .isIn("id.codCid.codCid", pCidade, TipoExpressao.AND, b -> pCidade.length > 0)
        ).resultList();
    }
}
