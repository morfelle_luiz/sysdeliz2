package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Regiao;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastrarTabelaPrecoController extends WindowBase {

    protected List<Regiao> indices = new ArrayList<>();

    public CadastrarTabelaPrecoController(String titulo, Image image, String[] abas) {
        super(titulo,image,abas);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void filtrar(Object[] codigo, String ativado, String reserva, String mkt) {

        Object[] pcodigo = codigo != null ? codigo : new Object[]{};


        indices = (List<Regiao>) new FluentDao().selectFrom(Regiao.class)
                .where(it -> it
                        .isIn("regiao", pcodigo, TipoExpressao.AND, b -> pcodigo.length > 0)
                        .equal("sdAtivo", Boolean.parseBoolean(ativado), TipoExpressao.AND, b -> !ativado.equals("T"))
                        .equal("usaReserva", Boolean.parseBoolean(reserva), TipoExpressao.AND, b -> !reserva.equals("T"))
                        .equal("usaMkt", Boolean.parseBoolean(mkt), TipoExpressao.AND, b -> !mkt.equals("T"))
                ).resultList();

    }
}

