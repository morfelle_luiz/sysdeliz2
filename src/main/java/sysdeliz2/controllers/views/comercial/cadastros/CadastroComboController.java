package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.ProdCombo;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastroComboController extends WindowBase {

    protected List<ProdCombo> listCombo = new ArrayList<>();
    public CadastroComboController(String title, Image icon) {
        super(title, icon);
    }

    protected void buscarCombos(Object[] prodOrig, Object[] prodIrm, Object[] regiao) {

        listCombo = (List<ProdCombo>) new FluentDao().selectFrom(ProdCombo.class)
                .where(it -> it
                        .isIn("id.prodori", prodOrig, TipoExpressao.AND, when -> prodOrig.length > 0)
                        .isIn("id.prodirm", prodIrm, TipoExpressao.AND, when -> prodIrm.length > 0)
                        .isIn("id.tabela", regiao, TipoExpressao.AND, when -> regiao.length > 0)
                ).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
