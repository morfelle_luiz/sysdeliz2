package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdCliDullius;
import sysdeliz2.models.sysdeliz.SdCliPaludo;
import sysdeliz2.models.sysdeliz.SdCliRFStore;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ImportacaoEtiquetasController extends WindowBase {

    protected List<SdCliPaludo> etiquetasReggla = new ArrayList<>();
    protected List<SdCliDullius> etiquetasReisen = new ArrayList<>();
    protected List<SdCliRFStore> etiquetasRFStore = new ArrayList<>();

    @Override
    public void closeWindow() {

    }

    public ImportacaoEtiquetasController(String title, Image icone) {
        super(title, icone);
    }

    protected void buscaEtiquetasReggla(Object[] produto, Object[] cor, String codigo) {

        Object[] pProduto = produto == null ? new Object[]{} : produto;
        Object[] pCor = cor == null ? new Object[]{} : cor;
        String pCodigo = codigo == null ? "" : codigo;

        etiquetasReggla = (List<SdCliPaludo>) new FluentDao().selectFrom(SdCliPaludo.class).where(it -> it
                .equal("id.codItem", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                .isIn("id.codigo", pProduto, TipoExpressao.AND, b -> pProduto.length > 0)
                .isIn("coritem", pCor, TipoExpressao.AND, b -> pCor.length > 0)
        ).resultList();

    }

    protected void buscaEtiquetasReisen(Object[] produto, Object[] cor, String codigo) {

        Object[] pProduto = produto == null ? new Object[]{} : produto;
        Object[] pCor = cor == null ? new Object[]{} : cor;
        String pCodigo = codigo == null ? "" : codigo;

        etiquetasReisen = (List<SdCliDullius>) new FluentDao().selectFrom(SdCliDullius.class).where(it -> it
                .equal("coditem", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                .isIn("codigo", pProduto, TipoExpressao.AND, b -> pProduto.length > 0)
                .isIn("coritem", pCor, TipoExpressao.AND, b -> pCor.length > 0)
        ).resultList();

    }

    protected void buscaEtiquetasRFStore(Object[] produto, Object[] cor, String codigo) {

        Object[] pProduto = produto == null ? new Object[]{} : produto;
        Object[] pCor = cor == null ? new Object[]{} : cor;
        String pCodigo = codigo == null ? "" : codigo;

        etiquetasRFStore = (List<SdCliRFStore>) new FluentDao().selectFrom(SdCliRFStore.class).where(it -> it
                .equal("codproduto", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                .isIn("referencia", pProduto, TipoExpressao.AND, b -> pProduto.length > 0)
                .isIn("cor", pCor, TipoExpressao.AND, b -> pCor.length > 0)
        ).resultList();

    }

    protected void importarArquivoReggla(List<SdCliPaludo> etiquetasToAdd, List<SdCliPaludo> etiquetasBean) {
        try {
            File file = new FileChooser().showOpenDialog(new Stage());
            if (file != null) {
                File arquivo = new File(file.getAbsolutePath());
                Scanner sc = new Scanner(arquivo);
                if (sc.hasNext()) sc.nextLine();
                while (sc.hasNext()) {
                    String[] s = sc.nextLine().split(";");

                    String codItem = s[1].trim();
                    String codigoItem = s[4].trim();
                    String corItem = s[5].trim();
                    String tam = s[6].trim();
                    String np = s[7].trim();
                    BigDecimal valorParcela = new BigDecimal(s[8].replace("R$ ", "").replace(",", ".").trim());
                    BigDecimal valorTotal = new BigDecimal(s[9].replace("R$ ", "").replace(",", ".").trim());
                    String lote = s[10].trim();
                    if (etiquetasBean.stream().noneMatch(it -> it.getId().getCodItem().equals(codItem) && it.getId().getCodigo().equals(codigoItem))) {
                        etiquetasToAdd.add(new SdCliPaludo(codItem, s[2].trim(), s[3].trim(), codigoItem, corItem, tam, np, valorParcela, valorTotal, lote));
                    }
                }
                etiquetasToAdd.forEach(it -> {
                    if (!etiquetasBean.contains(it)) etiquetasBean.add(it);
                });
                sc.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Houve um erro ao importar o arquivo!");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
        }
    }

    protected void importarArquivoReisen(List<SdCliDullius> etiquetasToAdd, List<SdCliDullius> etiquetasBean) {
        try {
            File file = new FileChooser().showOpenDialog(new Stage());
            if (file != null) {
                BufferedReader csvReader = new BufferedReader(new FileReader(file.getAbsolutePath()));
                String row;
                csvReader.readLine();
                while ((row = csvReader.readLine()) != null) {
                    String[] s = row.split(";");

                    String codItem = s[1].trim();
                    String descricao = s[2].substring(0, s[2].lastIndexOf(" ")).trim();
                    String codigoItem = s[3].trim();
                    String corItem = s[4].trim();
                    BigDecimal valorVendaAVista = new BigDecimal(s[5].trim().replace(",", "."));
                    BigDecimal valorParcelaAVista = new BigDecimal(s[6].trim().replace(",", "."));
                    int qtdeParcelaAVista = Integer.parseInt(s[7].trim());
                    BigDecimal valorVendaAPrazo = new BigDecimal(s[8].trim().replace(",", "."));
                    BigDecimal valorParcelaPrazo = new BigDecimal(s[9].trim().replace(",", "."));
                    int qtdeParcelaPrazo = Integer.parseInt(s[10].trim());
                    String textoFPgto = s[11].trim();
                    String textoTroca = s[12].trim();
                    BigDecimal qtde = new BigDecimal(s[13].trim());
                    String tam = s[2].substring(s[2].lastIndexOf(" ")).trim();

                    if (etiquetasBean.stream().noneMatch(it -> it.getCoditem().equals(codItem))) {
                        etiquetasToAdd.add(new SdCliDullius(codItem, descricao, codigoItem, corItem, valorVendaAVista, valorParcelaAVista, qtdeParcelaAVista, valorVendaAPrazo, valorParcelaPrazo, qtdeParcelaPrazo, textoFPgto, textoTroca, qtde, tam));
                    }
                }
                etiquetasToAdd.forEach(it -> {
                    if (!etiquetasBean.contains(it)) etiquetasBean.add(it);
                });
                csvReader.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Houve um erro ao importar o arquivo!");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void importarArquivoRFStore(List<SdCliRFStore> etiquetasToAdd, ListProperty<SdCliRFStore> etiquetasBean) {

        File file = new FileChooser().showOpenDialog(new Stage());
        DataFormatter formatter = new DataFormatter();
        if (file != null) {

            try {
                FileInputStream fis = new FileInputStream(file);
                XSSFWorkbook wb = new XSSFWorkbook(fis);
                XSSFSheet sheet = wb.getSheetAt(0);
                for (Row row : sheet) {
                    String codProduto = "", referencia = "", descCor = "", codCor = "", tam = "", colecao = "", valor = "", descProduto = "", barra = "";
                    for (Cell cell : row) {
                        switch (cell.getColumnIndex()) {
                            case 0:
                                referencia = formatter.formatCellValue(cell);
                                break;
                            case 1:
                                codProduto = formatter.formatCellValue(cell);
                                break;
                            case 2:
                                colecao = formatter.formatCellValue(cell);
                                break;
                            case 3:
                                descProduto = formatter.formatCellValue(cell);
                                break;
                            case 4:
                                descCor = formatter.formatCellValue(cell);
                                break;
                            case 5:
                                codCor = formatter.formatCellValue(cell);
                                break;
                            case 6:
                                barra = formatter.formatCellValue(cell);
                                break;
                            case 7:
                                valor = formatter.formatCellValue(cell);
                                break;
                            case 8:
                                tam = formatter.formatCellValue(cell);
                                break;
                        }
                    }
                    String finalCodProduto = codProduto;
                    if (!finalCodProduto.equals("") && etiquetasBean.stream().noneMatch(it -> it.getCodproduto().equals(finalCodProduto))) {
                        try {
                            etiquetasToAdd.add(new SdCliRFStore(referencia.trim(), codProduto.trim(), colecao.trim(), descProduto.trim(), descCor.trim(), codCor.trim(), barra.trim(), valor.replace("R$", "").trim(), tam.trim()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    etiquetasToAdd.forEach(it -> {
                        if (!etiquetasBean.contains(it)) etiquetasBean.add(it);
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

        protected void salvarEtiquetas (List < ? > etiquetasToAdd){
            if (etiquetasToAdd.size() > 0) {
                etiquetasToAdd.forEach(it -> new FluentDao().merge(it));
                etiquetasToAdd.clear();
                MessageBox.create(message -> {
                    message.message("Etiquetas importadas com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.showAndWait();
                });
            }
        }

        protected void cancelarImportacao (List < ? > etiquetasToAdd, List < ?>etiquetasBean){
            if (etiquetasToAdd.size() > 0) {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Tem certeza que deseja cancelar?");
                    message.showAndWait();
                }).value.get())) {
                    etiquetasBean.removeAll(etiquetasToAdd);
                    etiquetasToAdd.clear();
                }
            }
        }

    }
