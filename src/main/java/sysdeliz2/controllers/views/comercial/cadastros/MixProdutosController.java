package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.comercial.SdMixColecao;
import sysdeliz2.models.sysdeliz.comercial.SdMixProdutos;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.view.comercial.VSdLastYearMix;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class MixProdutosController extends WindowBase {

    public MixProdutosController(String title, Image icon) {
        super(title, icon);
    }

    @Override
    public void closeWindow() {

    }

    protected Collection<? extends SdMixColecao> getMixColecao(String colecao) {
        Collection<? extends SdMixColecao> mixColecao = new FluentDao().selectFrom(SdMixColecao.class).where(eb -> eb.equal("id.colecao.codigo", colecao)).resultList();
        mixColecao.forEach(JPAUtils::refresh);

        return mixColecao;
    }

    protected void saveMixColecao(SdMixColecao mixColecao) {
        mixColecao.getFamilias().forEach(mixFam -> new FluentDao().merge(mixFam));
        new FluentDao().merge(mixColecao);
        SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.CADASTRAR, mixColecao.getId().getColecao().getCodigo(), "Cadastrando mix para a coleção: " + mixColecao.toLog());
    }

    protected void deleteMixColecao(SdMixColecao mixColecao) {
        mixColecao.getFamilias().forEach(this::deleteFamilia);
        new FluentDao().delete(mixColecao);
        SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.EXCLUIR, mixColecao.getId().getColecao().getCodigo(), "Excluindo o mix na coleção: " + mixColecao.toLog());
    }

    protected void deleteFamilia(SdMixProdutos familia) {
        new FluentDao().delete(familia);
//        SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.EXCLUIR, familia.getId().getColecao().getCodigo().concat("-").concat(familia.getId().getMarca().getCodigo()).concat("-").concat(familia.getId().getFamilia().getCodigo()),
//                "Excluído família " + familia.getId().getFamilia().toString() + " do mix de produtos para a coleção " + familia.getId().getColecao().toString() + " na marca " + familia.getId().getMarca().toString());
    }

    protected List<VSdLastYearMix> getLastYearMix(String marca, String colecao, String linha, Integer grupoModelagem, String familia) {
        SdColecaoMarca001 colecaoMarca = new FluentDao().selectFrom(SdColecaoMarca001.class)
                .where(eb -> eb.equal("id.colecao.codigo", colecao).equal("id.marca.codigo", marca))
                .singleResult();


        return (List<VSdLastYearMix>) new FluentDao().selectFrom(VSdLastYearMix.class)
                .where(eb -> eb
                        .equal("id.colecao.codigo", colecaoMarca.getColecaoLastYear())
                        .equal("id.marca.codigo", marca)
                        .equal("id.linha.codigo", linha)
                        .equal("id.grupoModelagem.codigo", grupoModelagem)
                        .equal("id.familia.codigo", familia, TipoExpressao.AND, b -> familia.length() > 0))
                .resultList();
    }

}
