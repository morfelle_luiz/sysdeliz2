package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.view.comercial.VSdMetasEntidade;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.sql.SQLException;
import java.util.List;

public class RegrasComerciaisController extends WindowBase {

    public RegrasComerciaisController(String title, Image icone, String[] tabs) {
        super(title, icone, tabs);
    }

    protected List<VSdMetasEntidade> getMetasEntidade(Object[] colecao, Object[] marca) {
        return (List<VSdMetasEntidade>) new FluentDao().selectFrom(VSdMetasEntidade.class)
                .where(eb -> eb.equal("id.cliente", "0")
                        .isIn("id.colecao.codigo", colecao, TipoExpressao.AND, b -> colecao.length > 0)
                        .isIn("id.marca.codigo", marca, TipoExpressao.AND, b -> marca.length > 0))
                .resultList();
    }

    protected StackPane getNode() {
        return this;
    }

    protected void salvarMetaEntidade(String colecao, String marca, int indice, String valorIndice, String gatilho,
                                      String valorGatilho, String job, String acao) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                        "merge into sd_metas_entidade_001 src\n" +
                        "using dual tar\n" +
                        "on (src.cliente = '0' and tipo = '%s' and indicador = '%s' and colecao = '%s' and marca = '%s' and job = '%s')\n" +
                        "when matched then\n" +
                        "  update set gatilho_tipo = '%s', acao = '%s', valor = %s\n" +
                        "when not matched then\n" +
                        "  insert\n" +
                        "    (cliente,\n" +
                        "     tipo,\n" +
                        "     indicador,\n" +
                        "     valor,\n" +
                        "     colecao,\n" +
                        "     marca,\n" +
                        "     gatilho_tipo,\n" +
                        "     acao,\n" +
                        "     job)\n" +
                        "  values\n" +
                        "    ('0', '%s', %s, %s, '%s', '%s', '%s', '%s', '%s')",
                gatilho, indice, colecao, marca, job,
                valorGatilho, acao.replaceAll("'","''"), valorIndice,
                gatilho, indice, valorIndice, colecao, marca, valorGatilho, acao.replaceAll("'","''"), job);
    }

    protected void excluirMeta(String cliente, String colecao, int indicador, String job, String marca, String gatilho) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("delete from sd_metas_entidade_001 where cliente = '%s' and colecao = '%s' and marca = '%s' and indicador = '%s' and job = '%s' and tipo = '%s'",
                cliente, colecao, marca, indicador, job, gatilho);
    }

    @Override
    public void closeWindow() {

    }
}
