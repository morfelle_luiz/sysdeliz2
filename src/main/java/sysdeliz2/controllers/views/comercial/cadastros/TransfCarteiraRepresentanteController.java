package sysdeliz2.controllers.views.comercial.cadastros;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosEntidadeMarca;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class TransfCarteiraRepresentanteController extends WindowBase {

    protected List<VSdDadosEntidadeMarca> clientes = new ArrayList<>();

    public TransfCarteiraRepresentanteController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    
    protected void filtrar(Object[] cidade, Object[] cliente, String estado, Represen codRep, String pmarca) {

        Object[] pCidade = cidade != null ? cidade : new Object[]{};
        Object[] pCliente = cliente != null ? cliente : new Object[]{};
        String pEstado = estado == null ? "" : estado;
        String pCodRep = codRep.getCodRep() == null ? "" : codRep.getCodRep();
        String marca = pmarca == null ? "" : pmarca;

        JPAUtils.clearEntitys(clientes);

        clientes = (List<VSdDadosEntidadeMarca>) new FluentDao().selectFrom(VSdDadosEntidadeMarca.class)
                .where(eb -> eb
                        .isIn("codcli", pCliente, TipoExpressao.AND, b -> pCliente.length > 0)
                        .equal("codrep", pCodRep, TipoExpressao.AND, b -> !pCodRep.equals(""))
                        .equal("codest", pEstado, TipoExpressao.AND, b -> !pEstado.equals(""))
                        .isIn("codcid", pCidade, TipoExpressao.AND, b -> pCidade.length > 0)
                        .equal("codmarca", marca, TipoExpressao.AND, b -> !marca.equals("")))
                .resultList();
//        if(clientes != null && !marca.equals("")) {
////           clientes.removeIf(entidade -> entidade.getMarcasEntidade().stream().noneMatch(sdMarcasEntidade001 -> sdMarcasEntidade001.getId().getMarca().getCodigo().equals(marca)));
//            clientes.removeIf(vSdDadosEntidadeMarca -> vSdDadosEntidadeMarca.get)
//        }


    }

}
