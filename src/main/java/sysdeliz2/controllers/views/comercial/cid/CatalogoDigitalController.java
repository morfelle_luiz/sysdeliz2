package sysdeliz2.controllers.views.comercial.cid;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.comercial.SdCatalogo;
import sysdeliz2.models.sysdeliz.comercial.SdConceitoCatalogo;
import sysdeliz2.models.sysdeliz.comercial.SdLookbookCatalogo;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CatalogoDigitalController extends WindowBase {

    protected List<SdCatalogo> catalogos = new ArrayList<>();

    public CatalogoDigitalController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void findCatalogos(String titulo, String ano, Object[] marcas, String statusEnvio, String statusServidor) {
        String pTitulo = ano == null ? "" : ano;
        String pAno = ano == null ? "" : ano;

        catalogos = (List<SdCatalogo>) new FluentDao().selectFrom(SdCatalogo.class)
                .where(eb -> eb
                        .like("nome", pTitulo, TipoExpressao.AND, b -> pTitulo.length() > 0)
                        .equal("ano", pAno, TipoExpressao.AND, b -> pAno.length() > 0)
                        .equal("status", statusServidor, TipoExpressao.AND, b -> !statusServidor.equals("T"))
                        .equal("statusEnvio", statusEnvio.equals("S"), TipoExpressao.AND, b -> !statusServidor.equals("T"))
                        .isIn("marca", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                ).resultList();
    }

    protected SdColecaoMarca001 getColecaoMarca(String marca, String codigo) {
        return new FluentDao().selectFrom(SdColecaoMarca001.class)
                .where(eb -> eb
                        .equal("id.marca.codigo", marca)
                        .equal("id.colecao.codigo", codigo))
                .singleResult();
    }

    protected void saveCatalogo(SdCatalogo catalogo) throws SQLException {
        for (SdConceitoCatalogo conceito : catalogo.getConceitos()) {
            new FluentDao().persist(conceito);
            if (conceito.getCodigo() == 0) {
                SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Cadastrado novo conceito " + conceito.getTitulo() + " na ordem " + conceito.getOrdem() + " no catálogo!");
            } else {
                SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Alterado conceito " + conceito.getTitulo() + " na ordem " + conceito.getOrdem() + " no catálogo!");
            }
        }
        for (SdLookbookCatalogo lookbook : catalogo.getLookbooks()) {
            if (lookbook.getCodigo() != 0 && lookbook.getTitulo() != null && lookbook.getTitulo().length() > 0) {
                new FluentDao().persist(lookbook);
                if (lookbook.getCodigo() == 0) {
                    SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Cadastrado novo lookbook " + lookbook.getTitulo() + " na ordem " + lookbook.getOrdem() + " no catálogo!");
                } else {
                    SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Alterado lookbook " + lookbook.getTitulo() + " na ordem " + lookbook.getOrdem() + " no catálogo!");
                }
            } else if (lookbook.getCodigo() == 0 && (lookbook.getTitulo() == null || lookbook.getTitulo().length() == 0)) {
                new FluentDao().persist(lookbook);
                SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Cadastrado novo lookbook " + lookbook.getTitulo() + " na ordem " + lookbook.getOrdem() + " no catálogo!");
            }
        }

        if (catalogo.getCodigo() == 0) {
            new FluentDao().persist(catalogo);
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Cadastrado novo catálogo!");
        } else {
            new FluentDao().merge(catalogo);
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.CADASTRAR, String.valueOf(catalogo.getCodigo()), "Alterado cadastro do catálogo!");
        }
    }

    @Override
    public void closeWindow() {

    }
}
