package sysdeliz2.controllers.views.comercial.cid;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.comercial.SdCatalogo;
import sysdeliz2.models.sysdeliz.comercial.SdListaFavoritosCliente;
import sysdeliz2.models.view.comercial.VSdListaFavoritosCliente;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GestaoFavoritosController extends WindowBase {

    protected List<VSdListaFavoritosCliente> listasClientes = new ArrayList<>();
    protected List<SdCatalogo> catalogos = new ArrayList<>();

    public GestaoFavoritosController(String title, Image icone, String[] tabs) {
        super(title, icone, tabs);
    }

    protected void getListasClientes(Object[] clientes, Object[] catalogos, Object[] colecoes, Object[] marcas, Object[] ufs, Object[] representantes) {
        JPAUtils.clearEntitys(listasClientes);
        listasClientes.clear();
        listasClientes = (List<VSdListaFavoritosCliente>) new FluentDao().selectFrom(VSdListaFavoritosCliente.class)
                .where(eb -> eb
                        .isIn("cliente.codcli", clientes, TipoExpressao.AND, b -> clientes.length > 0)
                        .isIn("catalogo.codigo", catalogos, TipoExpressao.AND, b -> catalogos.length > 0)
                        .isIn("catalogo.colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("catalogo.marca.codigo", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                        .isIn("cliente.cep.cidade.codEst.id.siglaEst", ufs, TipoExpressao.AND, b -> ufs.length > 0))
                .resultList();

        if (representantes.length > 0)
            listasClientes.removeIf(item -> !Arrays.asList(representantes)
                    .contains(item.getCliente().getMarcas().stream()
                            .filter(marcaCli -> marcaCli.getId().getMarca().getCodigo().equals(item.getCatalogo().getMarca().getCodigo()))
                            .findFirst()
                            .get().getCodrep().getCodRep()));
    }

    protected void getCatalogos(Object[] marcas, Object[] colecoes, Object[] catalogos) {
        JPAUtils.clearEntitys(this.catalogos);
        this.catalogos.clear();
        this.catalogos = (List<SdCatalogo>) new FluentDao().selectFrom(SdCatalogo.class)
                .where(eb -> eb
                        .isIn("codigo", catalogos, TipoExpressao.AND, b -> catalogos.length > 0)
                        .isIn("marca.codigo", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                        .isIn("colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0))
                .resultList();
    }

    @Override
    public void closeWindow() {

    }
}
