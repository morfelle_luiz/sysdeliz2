package sysdeliz2.controllers.views.comercial.exportacao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdEmbarque;
import sysdeliz2.models.sysdeliz.SdItemEmbarque;
import sysdeliz2.models.sysdeliz.SdItemGradeEmbarque;
import sysdeliz2.models.ti.NotaEntra;
import sysdeliz2.models.ti.NotaEntraIten;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdDadosOfPendenteSKU;
import sysdeliz2.models.view.VSdDadosOfPendenteSKUPK;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CadastroEmbarqueController extends WindowBase {

    protected List<SdEmbarque> embarques = new ArrayList<>();

    public CadastroEmbarqueController(String title, Image icone) {
        super(title, icone);
    }

    @Override
    public void closeWindow() {

    }

    protected void buscarEmbarques(Object[] codEmbarque, String tipoEmbarque, String statusEmbarque, LocalDate dataInicio, LocalDate dataFim) {

        Object[] pCodigo = codEmbarque == null ? new Object[]{} : codEmbarque;
        String pTipoEmbarque = tipoEmbarque == null ? "" : tipoEmbarque;
        String pStatusEmbarque = statusEmbarque == null ? "" : statusEmbarque;
        embarques.clear();

        embarques = (List<SdEmbarque>) new FluentDao().selectFrom(SdEmbarque.class)
                .where(it -> it
                        .isIn("codigo", pCodigo, TipoExpressao.AND, b -> pCodigo.length > 0)
                        .equal("tipo", pTipoEmbarque, TipoExpressao.AND, b -> !pTipoEmbarque.equals(""))
                        .equal("status", pStatusEmbarque, TipoExpressao.AND, b -> !pStatusEmbarque.equals(""))
                        .between("data", dataInicio, dataFim)
                ).resultList();
    }

    protected List<SdItemGradeEmbarque> buscaItensGrade(SdItemEmbarque itemEmbarque, VSdDadosOfPendente of) {
        List<SdItemGradeEmbarque> list = new ArrayList<>();

        try {
            List<Object> objects = new NativeDAO().runNativeQuery(String.format("" +
                    "select numero, codigo,  setor, parte, tam ,sum(qtde) SOMA\n" +
                    "  from v_sd_dados_of_pendente_sku\n" +
                    " where numero = '%s'\n" +
                    " and setor = '%s'\n" +
                    " group by numero, codigo,  setor, parte, tam", itemEmbarque.getNumero(), of.getId().getSetor().getCodigo()));

            for (Object item : objects) {
                HashMap<String, Object> map = (HashMap<String, Object>) item;
                VSdDadosOfPendenteSKUPK id = new VSdDadosOfPendenteSKUPK(map.get("NUMERO").toString(), map.get("CODIGO").toString(), map.get("TAM").toString(), map.get("SETOR").toString());

                VSdDadosOfPendenteSKU sdDadosOfPendenteSKU = new VSdDadosOfPendenteSKU(id, map.get("PARTE").toString(), new BigDecimal(map.get("SOMA").toString()));

                list.add(new SdItemGradeEmbarque(sdDadosOfPendenteSKU, itemEmbarque));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    protected List<NotaEntraIten> getItensNota(NotaEntra notaFiscal) {
        return (List<NotaEntraIten>) new FluentDao()
                .selectFrom(NotaEntraIten.class)
                .where(it -> it
                        .equal("id.numero", notaFiscal.getId().getNotafiscal())
                        .equal("id.serie", notaFiscal.getId().getSerie())
                        .equal("id.codcre", notaFiscal.getId().getCredor())
                )
                .resultList();
    }

}