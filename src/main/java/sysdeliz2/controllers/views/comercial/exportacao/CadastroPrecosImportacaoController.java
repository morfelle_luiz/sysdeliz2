package sysdeliz2.controllers.views.comercial.exportacao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdProformaModelagem;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastroPrecosImportacaoController extends WindowBase {

    protected List<SdProformaModelagem> precosModelagem = new ArrayList<>();

    public CadastroPrecosImportacaoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarPrecos(Object[] marcas, Object[] grupos, String genero) {
        precosModelagem = (List<SdProformaModelagem>) new FluentDao()
                .selectFrom(SdProformaModelagem.class)
                .where(it -> it
                        .isIn("marca.codigo", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                        .isIn("codGrupo.codigo", grupos, TipoExpressao.AND, when -> grupos.length > 0)
                        .equal("genero", genero, TipoExpressao.AND, when -> !genero.equals("Todos"))
                ).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
