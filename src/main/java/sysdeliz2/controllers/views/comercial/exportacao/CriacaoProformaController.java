package sysdeliz2.controllers.views.comercial.exportacao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdEmbarque;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CriacaoProformaController extends WindowBase {

    protected List<SdEmbarque> embarques = new ArrayList<>();

    public CriacaoProformaController(String title, Image icon) {
        super(title, icon);
    }

    @Override
    public void closeWindow() {

    }

    protected void carregarEmbarquesImportacao(Object[] codEmbarque, String statusEmbarque, LocalDate dataInicio, LocalDate dataFim) {
        JPAUtils.clearEntitys(embarques);

        Object[] pCodigo = codEmbarque == null ? new Object[]{} : codEmbarque;
        String pStatusEmbarque = statusEmbarque == null ? "" : statusEmbarque;
        embarques.clear();

        embarques = (List<SdEmbarque>) new FluentDao().selectFrom(SdEmbarque.class)
                .where(it -> it
                        .isIn("codigo", pCodigo, TipoExpressao.AND, b -> pCodigo.length > 0)
                        .equal("status", pStatusEmbarque, TipoExpressao.AND, b -> !pStatusEmbarque.equals(""))
                        .between("data", dataInicio, dataFim)
                ).resultList();
    }
}