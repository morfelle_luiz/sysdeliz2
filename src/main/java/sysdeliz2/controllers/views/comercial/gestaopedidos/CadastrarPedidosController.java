package sysdeliz2.controllers.views.comercial.gestaopedidos;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.TabPreco;
import sysdeliz2.models.view.VSdMrpEstoqueB2b;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastrarPedidosController extends WindowBase {
    
    protected List<ProdutoB2B> produtosB2B = new ArrayList<>();
    protected List<ProdutoB2B> filteredProdutosB2B = new ArrayList<>();
    
    public CadastrarPedidosController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getProdutos(String marca, String tabPreco) {
        String pmarca = marca != null ? marca : "";
        String pTabPreco = tabPreco != null ? tabPreco : "";
        List<TabPreco> produtos = (List<TabPreco>) new FluentDao().selectFrom(TabPreco.class)
                .where(it -> it
                        .isIn("id.regiao", pTabPreco.split(","), TipoExpressao.AND, b -> pTabPreco.length() > 0)
                        .equal("id.codigo.codMarca", pmarca, TipoExpressao.AND, b -> pmarca.length() > 0)
                        .equal("id.codigo.ativo", true)
                        .equal("id.codigo.liberacao", "LIBERADO P/ PRODUÇÃO"))
                .orderBy("id.codigo.codigo", OrderType.ASC)
                .resultList();
        
        if (produtos.size() > 0) {
            produtos.forEach(produto -> {
                List<VSdMrpEstoqueB2b> estoque = (List<VSdMrpEstoqueB2b>) new FluentDao().selectFrom(VSdMrpEstoqueB2b.class)
                        .where(it -> it
                                .equal("codigo", produto.getId().getCodigo().getCodigo())
                                .equal("b2b", true))
                        .resultList();
                if (estoque.size() > 0)
                    produtosB2B.add(new ProdutoB2B(
                            produto,
                            estoque
                    ));
            });
            filteredProdutosB2B.clear();
            filteredProdutosB2B.addAll(produtosB2B);
        }
    }
    
    protected class ProdutoB2B {
        
        private final ObjectProperty<TabPreco> tabPreco = new SimpleObjectProperty<>();
        private final ListProperty<VSdMrpEstoqueB2b> estoqueProduto = new SimpleListProperty<>();
        
        public ProdutoB2B(TabPreco tabPreco, List<VSdMrpEstoqueB2b> estoqueProduto) {
            this.tabPreco.set(tabPreco);
            this.estoqueProduto.set(FXCollections.observableList(estoqueProduto));
        }
        
        public TabPreco getTabPreco() {
            return tabPreco.get();
        }
        
        public ObjectProperty<TabPreco> tabPrecoProperty() {
            return tabPreco;
        }
        
        public void setTabPreco(TabPreco tabPreco) {
            this.tabPreco.set(tabPreco);
        }
        
        public ObservableList<VSdMrpEstoqueB2b> getEstoqueProduto() {
            return estoqueProduto.get();
        }
        
        public ListProperty<VSdMrpEstoqueB2b> estoqueProdutoProperty() {
            return estoqueProduto;
        }
        
        public void setEstoqueProduto(ObservableList<VSdMrpEstoqueB2b> estoqueProduto) {
            this.estoqueProduto.set(estoqueProduto);
        }
    }
    
    protected class GradeDigitacao {
    
        private final ObjectProperty<TabPreco> codigo = new SimpleObjectProperty<>();
        private final StringProperty cor = new SimpleStringProperty();
        private final StringProperty descCor = new SimpleStringProperty();
        private final StringProperty tam = new SimpleStringProperty();
        private final StringProperty entrega = new SimpleStringProperty();
        private final StringProperty descEntrega = new SimpleStringProperty();
        private final IntegerProperty qtde = new SimpleIntegerProperty();
    
        public GradeDigitacao(TabPreco codigo, String cor, String descCor, String tam, String entrega, String descEntrega, Integer qtde) {
            this.codigo.set(codigo);
            this.cor.set(cor);
            this.descCor.set(descCor);
            this.tam.set(tam);
            this.entrega.set(entrega);
            this.descEntrega.set(descEntrega);
            this.qtde.set(qtde);
        }
    
        public TabPreco getCodigo() {
            return codigo.get();
        }
    
        public ObjectProperty<TabPreco> codigoProperty() {
            return codigo;
        }
    
        public void setCodigo(TabPreco codigo) {
            this.codigo.set(codigo);
        }
    
        public String getCor() {
            return cor.get();
        }
    
        public StringProperty corProperty() {
            return cor;
        }
    
        public void setCor(String cor) {
            this.cor.set(cor);
        }
    
        public String getTam() {
            return tam.get();
        }
    
        public StringProperty tamProperty() {
            return tam;
        }
    
        public void setTam(String tam) {
            this.tam.set(tam);
        }
    
        public String getEntrega() {
            return entrega.get();
        }
    
        public StringProperty entregaProperty() {
            return entrega;
        }
    
        public void setEntrega(String entrega) {
            this.entrega.set(entrega);
        }
    
        public int getQtde() {
            return qtde.get();
        }
    
        public IntegerProperty qtdeProperty() {
            return qtde;
        }
    
        public void setQtde(int qtde) {
            this.qtde.set(qtde);
        }
    
        public String getDescCor() {
            return descCor.get();
        }
    
        public StringProperty descCorProperty() {
            return descCor;
        }
    
        public void setDescCor(String descCor) {
            this.descCor.set(descCor);
        }
    
        public String getDescEntrega() {
            return descEntrega.get();
        }
    
        public StringProperty descEntregaProperty() {
            return descEntrega;
        }
    
        public void setDescEntrega(String descEntrega) {
            this.descEntrega.set(descEntrega);
        }
    }
    
    protected class GradeCarrinho {
    
        private final ObjectProperty<TabPreco> produto = new SimpleObjectProperty<>();
        private final StringProperty cor = new SimpleStringProperty();
        private final StringProperty descCor = new SimpleStringProperty();
        private final StringProperty entrega = new SimpleStringProperty();
        private final StringProperty descEntrega = new SimpleStringProperty();
        private final StringProperty tam = new SimpleStringProperty();
        private final IntegerProperty qtde = new SimpleIntegerProperty();
    
        public GradeCarrinho(TabPreco produto, String cor, String descCor, String entrega, String descEntrega, String tam, Integer qtde) {
            this.produto.set(produto);
            this.cor.set(cor);
            this.descCor.set(descCor);
            this.entrega.set(entrega);
            this.descEntrega.set(descEntrega);
            this.tam.set(tam);
            this.qtde.set(qtde);
        }
    
        public TabPreco getProduto() {
            return produto.get();
        }
    
        public ObjectProperty<TabPreco> produtoProperty() {
            return produto;
        }
    
        public void setProduto(TabPreco produto) {
            this.produto.set(produto);
        }
    
        public String getCor() {
            return cor.get();
        }
    
        public StringProperty corProperty() {
            return cor;
        }
    
        public void setCor(String cor) {
            this.cor.set(cor);
        }
    
        public String getDescCor() {
            return descCor.get();
        }
    
        public StringProperty descCorProperty() {
            return descCor;
        }
    
        public void setDescCor(String descCor) {
            this.descCor.set(descCor);
        }
    
        public String getEntrega() {
            return entrega.get();
        }
    
        public StringProperty entregaProperty() {
            return entrega;
        }
    
        public void setEntrega(String entrega) {
            this.entrega.set(entrega);
        }
    
        public String getDescEntrega() {
            return descEntrega.get();
        }
    
        public StringProperty descEntregaProperty() {
            return descEntrega;
        }
    
        public void setDescEntrega(String descEntrega) {
            this.descEntrega.set(descEntrega);
        }
    
        public String getTam() {
            return tam.get();
        }
    
        public StringProperty tamProperty() {
            return tam;
        }
    
        public void setTam(String tam) {
            this.tam.set(tam);
        }
    
        public int getQtde() {
            return qtde.get();
        }
    
        public IntegerProperty qtdeProperty() {
            return qtde;
        }
    
        public void setQtde(int qtde) {
            this.qtde.set(qtde);
        }
    }
}
