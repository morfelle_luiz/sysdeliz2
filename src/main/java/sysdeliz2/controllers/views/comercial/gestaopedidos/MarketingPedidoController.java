package sysdeliz2.controllers.views.comercial.gestaopedidos;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdCaixaRemessa;
import sysdeliz2.models.sysdeliz.SdGradeItemPedRem;
import sysdeliz2.models.sysdeliz.SdItensPedidoRemessa;
import sysdeliz2.models.sysdeliz.comercial.SdItensMktPedido;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedido;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdMktPedido;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MarketingPedidoController extends WindowBase {

    protected List<Pedido> pedidos = new ArrayList<>();
    protected List<SdItensPedidoRemessa> mktsRemessa = new ArrayList<>();
    protected List<SdMktPedido> mktsManual = new ArrayList<>();
    protected List<VSdMktPedido> mktsRegra = new ArrayList<>();

    public MarketingPedidoController(String title, Image icone) {
        super(title, icone);
    }

    protected void getPedidos(LocalDate inicioEmissao, LocalDate fimEmissao, Object[] marcas, Object[] colecoes, Object[] representantes, Object[] sitclis, Object[] clientes, Object[] pedido, Object[] origens) {
        pedidos = (List<Pedido>) new FluentDao().selectFrom(Pedido.class)
                .where(eb -> eb
                        .between("dt_emissao", inicioEmissao, fimEmissao)
                        .isIn("numero", pedido, TipoExpressao.AND, b -> pedido.length > 0)
                        .isIn("codcli.codcli", clientes, TipoExpressao.AND, b -> clientes.length > 0)
                        .isIn("codcli.sitCli.codigo", sitclis, TipoExpressao.AND, b -> sitclis.length > 0)
                        .isIn("codrep.codRep", representantes, TipoExpressao.AND, b -> representantes.length > 0)
                        .isIn("sdPedido.origem", origens, TipoExpressao.AND, b -> origens.length > 0)
                        .isIn("sdPedido.colecao", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("sdPedido.marca", marcas, TipoExpressao.AND, b -> marcas.length > 0))
                .resultList();
    }

    protected void getMktsPedido(String pedido) {
        mktsRemessa = (List<SdItensPedidoRemessa>) new FluentDao().selectFrom(SdItensPedidoRemessa.class)
                .where(eb -> eb
                        .equal("id.numero", pedido)
                        .equal("tipo", "MKT"))
                .resultList();
        mktsManual = (List<SdMktPedido>) new FluentDao().selectFrom(SdMktPedido.class)
                .where(eb -> eb
                        .equal("id.pedido", pedido))
                .resultList();
        mktsRegra = (List<VSdMktPedido>) new FluentDao().selectFrom(VSdMktPedido.class)
                .where(eb -> eb
                        .equal("numero", pedido))
                .resultList();
    }

    protected void saidaMktEstoque(SdMktPedido mktCadastro) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "update pa_iten_001 set quantidade = quantidade - %s where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '0005'",
                mktCadastro.getQtde(), mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN"));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'S',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN", "0005", mktCadastro.getQtde(),
                Globals.getUsuarioLogado().getUsuario(), mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(),
                mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));

    }

    protected void entradaMktEstoque(SdMktPedido mktCadastro, VSdDadosProdutoBarra mktEstoque) throws SQLException {
        // update da PA_ITEN entrando no estoque
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "merge into pa_iten_001 pai using dual on\n" +
                        "(pai.codigo = '%s' and pai.cor = '%s' and pai.tam = '%s' and pai.deposito = '0005')\n" +
                        "when matched then \n" +
                        "  update set pai.quantidade = pai.quantidade + %s\n" +
                        "when not matched then \n" +
                        "  insert (quantidade, tam, cor, barra28, codigo, barra, lote, ativo, deposito, ordem, local)\n" +
                        "  values (%s, 'UN', 'UN', '%s', '%s', '%s', '000000', 'S', '0005', 1, '%s')",
                mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN",
                mktCadastro.getQtde(),
                mktCadastro.getQtde(), mktEstoque.getBarra28(), mktCadastro.getId().getCodigo().getCodigo(), mktEstoque.getBarra(), mktEstoque.getLocal()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(), "UN", "UN", "0005", mktCadastro.getQtde(),
                Globals.getUsuarioLogado().getUsuario(), mktCadastro.getId().getPedido(), mktCadastro.getId().getCodigo().getCodigo(),
                mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getCodigo().getCodigo(), mktCadastro.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));

    }

    protected void saidaProdutoEstoque(SdItensMktPedido itemMkt, Integer qtdeMkt) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade - %s where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '0005'",
                itemMkt.getQtde() * qtdeMkt, itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'S',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", itemMkt.getId().getPedido(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam(), "0005", itemMkt.getQtde() * qtdeMkt,
                Globals.getUsuarioLogado().getUsuario(), itemMkt.getId().getPedido(), itemMkt.getId().getCodigo(),
                itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));

    }

    protected void entradaProdutoEstoque(SdItensMktPedido itemMkt, Integer qtdeMkt) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade + %s where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '0005'",
                itemMkt.getQtde() * qtdeMkt, itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   %s,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Mkt - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", itemMkt.getId().getPedido(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getCor(), itemMkt.getId().getTam(), "0005", itemMkt.getQtde() * qtdeMkt,
                Globals.getUsuarioLogado().getUsuario(), itemMkt.getId().getPedido(), itemMkt.getId().getCodigo(),
                itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getProduto().getCodigo(), itemMkt.getId().getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));

    }

    @Override
    public void closeWindow() {

    }

    public static class GradeDigitacao {

        private final StringProperty cor = new SimpleStringProperty();
        private final StringProperty descCor = new SimpleStringProperty();
        private final StringProperty tam = new SimpleStringProperty();
        private final StringProperty entrega = new SimpleStringProperty();
        private final StringProperty descEntrega = new SimpleStringProperty();
        private final IntegerProperty qtde = new SimpleIntegerProperty();

        public GradeDigitacao(String cor, String descCor, String tam, String entrega, String descEntrega, Integer qtde) {
            this.cor.set(cor);
            this.descCor.set(descCor);
            this.tam.set(tam);
            this.entrega.set(entrega);
            this.descEntrega.set(descEntrega);
            this.qtde.set(qtde);
        }

        public String getCor() {
            return cor.get();
        }

        public StringProperty corProperty() {
            return cor;
        }

        public void setCor(String cor) {
            this.cor.set(cor);
        }

        public String getTam() {
            return tam.get();
        }

        public StringProperty tamProperty() {
            return tam;
        }

        public void setTam(String tam) {
            this.tam.set(tam);
        }

        public String getEntrega() {
            return entrega.get();
        }

        public StringProperty entregaProperty() {
            return entrega;
        }

        public void setEntrega(String entrega) {
            this.entrega.set(entrega);
        }

        public int getQtde() {
            return qtde.get();
        }

        public IntegerProperty qtdeProperty() {
            return qtde;
        }

        public void setQtde(int qtde) {
            this.qtde.set(qtde);
        }

        public String getDescCor() {
            return descCor.get();
        }

        public StringProperty descCorProperty() {
            return descCor;
        }

        public void setDescCor(String descCor) {
            this.descCor.set(descCor);
        }

        public String getDescEntrega() {
            return descEntrega.get();
        }

        public StringProperty descEntregaProperty() {
            return descEntrega;
        }

        public void setDescEntrega(String descEntrega) {
            this.descEntrega.set(descEntrega);
        }
    }

}
