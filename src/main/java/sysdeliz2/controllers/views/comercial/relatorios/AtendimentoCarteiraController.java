package sysdeliz2.controllers.views.comercial.relatorios;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdAtendCarteiraRep;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class AtendimentoCarteiraController extends WindowBase {

    protected List<VSdAtendCarteiraRep> clientes = new ArrayList<>();

    public AtendimentoCarteiraController(String title, Image icone, String[] tabs) {
        super(title, icone, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getClientes(Represen codRep, Object[] cidades, Object[] marca, Object[] perfil, Object[] estados) {
    if (clientes != null && clientes.size() > 0) JPAUtils.clearEntitys(clientes);
        String pCodRep = codRep.getCodRep() == null ? "" : codRep.getCodRep();
        Object[] pCidades = cidades == null ? new Object[]{} : cidades;
        Object[] pMarca = marca == null ? new Object[]{} : marca;
        Object[] pPerfil = perfil == null ? new Object[]{} : perfil;
        Object[] pEstados = estados == null ? new Object[]{} : estados;

        clientes = (List<VSdAtendCarteiraRep>) new FluentDao().selectFrom(VSdAtendCarteiraRep.class)
                .where(it -> it
                        .equal("codRep.codRep", pCodRep, TipoExpressao.AND, b -> !pCodRep.equals(""))
                        .isIn("codCid.codCid", pCidades, TipoExpressao.AND, b -> pCidades.length > 0)
                        .isIn("codMarca.codigo", pMarca, TipoExpressao.AND, b -> pMarca.length > 0)
                        .isIn("sitCli", pPerfil, TipoExpressao.AND, b -> pPerfil.length > 0)
                        .isIn("codCid.codEst.codigo", pEstados, TipoExpressao.AND, b-> pEstados.length > 0)
                ).resultList();
    }
}
