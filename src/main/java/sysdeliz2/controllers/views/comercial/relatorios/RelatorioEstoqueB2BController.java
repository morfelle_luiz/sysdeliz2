package sysdeliz2.controllers.views.comercial.relatorios;

import javafx.application.Platform;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.view.VB2BProdEstoqueResumido;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

public class RelatorioEstoqueB2BController extends WindowBase {

    protected List<VB2BProdEstoqueResumido> produtos = new ArrayList<>();

    public RelatorioEstoqueB2BController(String title, Image icon) {
        super(title, icon);
    }

    protected void buscarProdutos(Object[] produto, Object[] marca, Object[] colecao, Boolean incluirSemEntrega) {
        try {
            if (produtos != null && produtos.size() > 0) JPAUtils.clearEntitys(produtos);
            produtos = (List<VB2BProdEstoqueResumido>) new FluentDao()
                    .selectFrom(VB2BProdEstoqueResumido.class).where(it -> it
                            .isIn("codigo", produto, TipoExpressao.AND, when -> produto.length > 0)
                            .isIn("marca", marca, TipoExpressao.AND, when -> marca.length > 0)
                            .isIn("codCol", colecao, TipoExpressao.AND, when -> colecao.length > 0)
                            .equal("entrega", true, TipoExpressao.AND, when -> !incluirSemEntrega)
                    ).resultList();
        } catch (PersistenceException e) {
            e.printStackTrace();
            Platform.runLater(() -> {
                MessageBox.create(message -> {
                    message.message("Erro ao abrir o arquivo de imagens, tente novamente em alguns minutos.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            });
        }
    }

    @Override
    public void closeWindow() {

    }
}
