package sysdeliz2.controllers.views.comercial.representantes;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdCidadeRep;
import sysdeliz2.models.sysdeliz.SdPcaCodRepMarca;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdPedCidadesRep;
import sysdeliz2.models.view.VSdPedDataCidadesRep;
import sysdeliz2.models.view.VSdSemanaColecao;
import sysdeliz2.models.view.VSdUltColecaoMarca;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.DateUtils;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class AnaliseCidadesRepController extends WindowBase {

    protected List<VSdPedCidadesRep> cidades = new ArrayList<>();
    protected List<VSdPedDataCidadesRep> cidadesSt = new ArrayList<>();
    protected Map<Integer, List<VSdUltColecaoMarca>> colecoes;
    protected SdPcaCodRepMarca pca = null;
    protected Represen represenSelecionado = null;
    protected ObjectProperty<Marca> marcaSelecionada = new SimpleObjectProperty<>();

    private final Marca DLZ = new FluentDao().selectFrom(Marca.class).where(it -> it.equal("codigo", "D")).singleResult();
    private final Marca FLOR = new FluentDao().selectFrom(Marca.class).where(it -> it.equal("codigo", "F")).singleResult();

    public AnaliseCidadesRepController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
        colecoes = ((List<VSdUltColecaoMarca>) new FluentDao()
                .selectFrom(VSdUltColecaoMarca.class).get().resultList())
                .stream()
                .collect(Collectors.groupingBy(VSdUltColecaoMarca::getIndice));
    }

    protected List<VSdPedCidadesRep> buscarCidades(Represen represen, Marca marca, Colecao colecao, VSdSemanaColecao semana) {

        try {
            List<VSdPedDataCidadesRep> pedidos = (List<VSdPedDataCidadesRep>) new FluentDao()
                    .selectFrom(VSdPedDataCidadesRep.class)
                    .where(it -> it
                            .equal("codRep", represen != null ? represen.getCodRep() : "", TipoExpressao.AND, when -> represen != null)
                            .equal("marca", marca != null ? marca.getCodigo() : "", TipoExpressao.AND, when -> marca != null && !marca.getCodigo().equals("0"))
                    ).resultList();

            if (pedidos != null && pedidos.size() > 0) {
                if (!marca.getCodigo().equals("0")) verificaCidades(represen, marca);
                represenSelecionado = represen;
                Map<Cidade, List<VSdPedDataCidadesRep>> collect = pedidos.stream().filter(it -> it.getCodCid() != null).collect(Collectors.groupingBy(VSdPedDataCidadesRep::getCodCid));
                List<SdCidadeRep> cidadesRep = (List<SdCidadeRep>) new FluentDao().selectFrom(SdCidadeRep.class).where(it -> it.equal("id.codRep.codRep", represen.getCodRep()).equal("marca.codigo", marca.getCodigo())).resultList();
                pedidos.clear();
                cidades.clear();
                collect.forEach((cidade, vSdPedCidadesReps) -> {
                    if (cidadesRep.stream().anyMatch(it -> it.getId().getCodCid().getCodCid().equals(cidade.getCodCid()))) {
                        Colecao colPas = colecao.getColAnt();
                        Colecao colAnt = colPas.getColAnt();
                        Colecao colRef = colAnt.getColAnt();

                        colecoes.clear();

                        colecoes.put(1, Arrays.asList(new VSdUltColecaoMarca(colecao, DLZ), new VSdUltColecaoMarca(colecao, FLOR)));
                        colecoes.put(2, Arrays.asList(new VSdUltColecaoMarca(colPas, DLZ), new VSdUltColecaoMarca(colPas, FLOR)));
                        colecoes.put(3, Arrays.asList(new VSdUltColecaoMarca(colAnt, DLZ), new VSdUltColecaoMarca(colAnt, FLOR)));
                        colecoes.put(4, Arrays.asList(new VSdUltColecaoMarca(colRef, DLZ), new VSdUltColecaoMarca(colRef, FLOR)));

                        boolean isColRef = vSdPedCidadesReps.stream().anyMatch(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colRef.getCodigo()));
                        boolean isColAnt = vSdPedCidadesReps.stream().anyMatch(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colAnt.getCodigo()));
                        boolean isColPas = vSdPedCidadesReps.stream().anyMatch(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colPas.getCodigo()));

                        vSdPedCidadesReps = vSdPedCidadesReps.stream().filter(it -> DateUtils.between(it.getDtEmissao(), LocalDate.parse(colecao.getInicioVig().toString()), semana.getDtFim())).collect(Collectors.toList());

                        boolean isColAtu = vSdPedCidadesReps.stream().anyMatch(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colecao.getCodigo()));

                        int pv = vSdPedCidadesReps.stream().filter(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colecao.getCodigo())).mapToInt(VSdPedDataCidadesRep::getPv).sum();
                        BigDecimal tf = BigDecimal.valueOf(vSdPedCidadesReps.stream().filter(it -> it.getColecao() != null && it.getColecao().getCodigo().equals(colecao.getCodigo())).mapToDouble(it -> it.getValor().doubleValue()).sum());
                        cidades.add(new VSdPedCidadesRep(cidade, isColAtu, isColPas, isColAnt, isColRef, pv, tf));
                    }
                });
                pedidos.sort(Comparator.comparing(cid -> cid.getCodCid().getNomeCid()));
            }
            return cidades;
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Houve um erro ao carregar as cidades, verifique se uma coleção válida foi inserida");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            cidades.clear();
            return cidades;
        }
    }

    @Nullable
    protected SdPcaCodRepMarca getPcaCodRepMarca(Represen represen, Marca marca) {
        try {
            if (!marca.getCodigo().equals("0")) {
                return new FluentDao().selectFrom(SdPcaCodRepMarca.class).where(it -> it.equal("codRep", represen.getCodRep()).equal("marca", marca.getCodigo())).singleResult();
            } else {
                List<SdPcaCodRepMarca> pcas = (List<SdPcaCodRepMarca>) new FluentDao().selectFrom(SdPcaCodRepMarca.class).where(it -> it.equal("codRep", represen.getCodRep())).resultList();
                if (pcas == null) return null;
                return new SdPcaCodRepMarca(
                        represen.getCodRep(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpCurtoMes).sum(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpCurtoCid).sum(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpMedioMes).sum(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpMedioCid).sum(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpLongoMes).sum(),
                        pcas.stream().mapToInt(SdPcaCodRepMarca::getpLongoMes).sum()
                );
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void verificaCidades(Represen represen, Marca marca) {
        List<VSdPedCidadesRep> cidadesComErro = cidades.stream().filter(it -> it.getCodCid() == null).collect(Collectors.toList());
        if (cidadesComErro.size() > 0) {
            List<String> clientesComErroCep = getClientesComErroCep(represen, marca);
            Platform.runLater(() -> {
                MessageBox.create(message -> {
                    message.message("Atenção, alguns cliente estão com erro no cadastro do CEP.\nOs seguintes clientes estão com erro:\n-" + String.join("\n-", clientesComErroCep));
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            });
        }
    }

    private List<String> getClientesComErroCep(Represen represen, Marca marca) {

        List<String> clientesString = new ArrayList<>();
        List<Object> objects = new ArrayList<>();
        try {
            if (represen != null) {
                objects = new NativeDAO().runNativeQuery("" +
                        "select distinct ent.codcli || ' - ' || ent.nome cliente\n" +
                        "  from entidade_001 ent\n" +
                        "  left join cadcep_001 cep\n" +
                        "    on cep.cep = ent.cep\n" +
                        "  left join cidade_001 cid\n" +
                        "    on cid.cod_cid = cep.cod_cid\n" +
                        "  left join sd_cidade_rep_001 cidrep\n" +
                        "    on cidrep.codrep = ent.codrep\n" +
                        " where cid.cod_cid is null\n" +
                        " and cidrep.marca = '%s'\n" +
                        " and ent.codrep = '%s'\n", marca.getCodigo(), represen.getCodRep());
            } else {
                objects = new NativeDAO().runNativeQuery("" +
                        "select distinct ent.codcli || ' - ' || ent.nome cliente\n" +
                        "  from entidade_001 ent\n" +
                        "  left join cadcep_001 cep\n" +
                        "    on cep.cep = ent.cep\n" +
                        "  left join cidade_001 cid\n" +
                        "    on cid.cod_cid = cep.cod_cid\n" +
                        "  left join sd_cidade_rep_001 cidrep\n" +
                        "    on cidrep.codrep = ent.codrep\n" +
                        " where cid.cod_cid is null\n" +
                        " and cidrep.marca = '%s'", marca.getCodigo());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        objects.forEach(cliente -> clientesString.add(((Map<String, Object>) cliente).get("CLIENTE").toString()));
        return clientesString;
    }

    protected List<VSdPedDataCidadesRep> buscarCidadesSt(Represen represen, Marca marca) {
        return (List<VSdPedDataCidadesRep>) new FluentDao()
                .selectFrom(VSdPedDataCidadesRep.class)
                .where(it -> it
                        .equal("codRep", represen != null ? represen.getCodRep() : "", TipoExpressao.AND, when -> represen != null)
                        .equal("marca", marca.getCodigo(), TipoExpressao.AND, when -> !marca.getCodigo().equals("0"))
                ).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
