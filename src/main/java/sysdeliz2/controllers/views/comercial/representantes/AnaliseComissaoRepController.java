package sysdeliz2.controllers.views.comercial.representantes;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Condicao;
import sysdeliz2.models.view.VSdAnaliseComissaoHeader;
import sysdeliz2.models.view.VSdAnaliseComissaoItem;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class AnaliseComissaoRepController extends WindowBase {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    protected List<VSdAnaliseComissaoHeader> pedidos = new ArrayList<>();
    protected List<VSdAnaliseComissaoItem> linhasPedido = new ArrayList<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Objetos">
    protected final FormFilter filters = new FormFilter();
    // </editor-fold>
    
    public AnaliseComissaoRepController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void findPedidos(FormFilter filters) {
        pedidos = (List<VSdAnaliseComissaoHeader>) new FluentDao().selectFrom(VSdAnaliseComissaoHeader.class)
                .where(it -> it
                        .isIn("numero", filters.numeroPedido, TipoExpressao.AND, b -> filters.numeroPedido.length > 0)
                        .equal("status", filters.statusPedido, TipoExpressao.AND, b -> !filters.statusPedido.equals("AMBOS"))
                        .equal("comercial", filters.statusComercial, TipoExpressao.AND, b -> !filters.statusComercial.equals("AMBOS"))
                        .equal("financeiro", filters.statusFinanceiro, TipoExpressao.AND, b -> !filters.statusFinanceiro.equals("AMBOS"))
                        .equal("verificar", filters.verificarComissao, TipoExpressao.AND, b -> !filters.verificarComissao.equals("AMBOS"))
                        .between("dtemissao", filters.emissaoInicio, filters.emissaoFim, TipoExpressao.AND, b -> filters.emissaoInicio != null && filters.emissaoFim != null)
                        .isIn("codcli.codcli", filters.cliente, TipoExpressao.AND, b -> filters.cliente.length > 0)
                        .isIn("codrep.codRep", filters.representante, TipoExpressao.AND, b -> filters.representante.length > 0)
                        .isIn("marca.codigo", filters.marca, TipoExpressao.AND, b -> filters.marca.length > 0)
                        .isIn("colecao.codigo", filters.colecao, TipoExpressao.AND, b -> filters.colecao.length > 0)
                        .isIn("tabpre.regiao", filters.tabela, TipoExpressao.AND, b -> filters.tabela.length > 0)
                        .isIn("codcli.grupo.codigo", filters.grupoCli, TipoExpressao.AND, b -> filters.grupoCli.length > 0)
                ).resultList();
    }
    
    protected void findLinhasPedido(String numero) {
        JPAUtils.clearEntitys(linhasPedido);
        linhasPedido = (List<VSdAnaliseComissaoItem>) new FluentDao().selectFrom(VSdAnaliseComissaoItem.class)
                .where(it -> it
                        .equal("id.numero", numero)
                ).resultList();
    }
    
    protected void alterarDescontoPedido(String numero, Double desconto) {
        String descontoStr = StringUtils.rpad(StringUtils.toDecimalFormat(desconto, 2), 5, " ") + "0,00 0,00 0,00 ";
        new FluentDao().runNativeQueryUpdate(String.format("update pedido_001 set per_desc = %s, desconto = '%s' where numero = '%s'", desconto.toString(), descontoStr, numero));
    }
    
    protected String alterarCondicaoPagamentoPedido(String numero, Condicao condicao) {
        String condicaoStr = "";
        String condicaoFormatado = "";
        Integer prazo = condicao.getPrazoini();
        for (int i = 0; i < condicao.getNrpar(); i++) {
            condicaoStr += StringUtils.rpad(String.valueOf(prazo), 3, " ");
            condicaoFormatado += String.valueOf(prazo) + "/";
            prazo += condicao.getNrdia();
        }
        new FluentDao().runNativeQueryUpdate(String.format("update pedido_001 set pgto = '%s' where numero = '%s'", condicaoStr, numero));
        return condicaoFormatado;
    }
    
    protected void liberarFinanceiroPedido(String numero) {
        new FluentDao().runNativeQueryUpdate(String.format("update pedido_001 set financeiro = '1' where numero = '%s'", numero));
    }

    protected void liberarComercialPedido(String numero) {
        new FluentDao().runNativeQueryUpdate(String.format("update pedido_001 set bloqueio = '1' where numero = '%s'", numero));
    }
    
    protected void alterarComissaoLinhaPedido(String numero, String codlin, LocalDate dateReferencia, BigDecimal comissao) {
        List<Object> itensPedido = (List<Object>) new FluentDao().runNativeQuery(
                String.format("" +
                        "select pei.*\n" +
                        "  from pedido_001 ped\n" +
                        "  join ped_iten_001 pei\n" +
                        "    on pei.numero = ped.numero\n" +
                        "  join v_sd_dados_produto prd\n" +
                        "    on prd.codigo = pei.codigo\n" +
                        "  left join v_sd_produto_pack combo\n" +
                        "    on combo.prod_ori = pei.codigo\n" +
                        "   and combo.prod_ori not like 'Z%%'\n" +
                        "  left join pedido3_001 ped3\n" +
                        "    on ped3.numero = pei.numero\n" +
                        "   and ped3.codigo = pei.codigo\n" +
                        "   and ped3.cor = pei.cor\n" +
                        "   and ped3.tam = pei.tam\n" +
                        "  left join nota_001 nfe\n" +
                        "    on nfe.fatura = ped3.notafiscal\n" +
                        " where ped.numero = '%s'\n" +
                        "   and decode(combo.prod_ori, null, prd.tipo, 'P' || prd.tipo) = '%s'\n" +
                        "   and (nfe.fatura is null or nfe.dt_emissao >= '%s')", numero, codlin, dateReferencia.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        
//        List<PedIten> itensPedido = (List<PedIten>) new FluentDao().selectFrom(PedIten.class)
//                .where(it -> it
//                        .equal("id.numero", numero)
//                        .equal("id.codigo.linha.tipoLinha", codlin)
//                ).resultList();
        
//        JPAUtils.clearEntitys(itensPedido);
        itensPedido.forEach(item -> {
            //item.setPerc_comissao(comissao);
            new FluentDao().runNativeQueryUpdate(String.format("update ped_iten_001 set perc_comissao = %s where numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s' and ordem = %s",
                    comissao.doubleValue(),
                    ((Object[]) item)[6].toString(),
                    ((Object[]) item)[7].toString(),
                    ((Object[]) item)[5].toString(),
                    ((Object[]) item)[4].toString(),
                    ((BigDecimal) ((Object[]) item)[0]).intValue()));
            //new FluentDao().merge(item);
        });

        if (itensPedido.size() > 0) {
            new FluentDao().runNativeQueryUpdate(String.format("update sd_pedido_001 set analise_comissao = 'S' where numero = '%s'", numero));
        }
    }
    
    protected void aplicarComissaoCalculaPedido(String numero, LocalDate dateReferencia) {
        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                linhasPedido.forEach(linha -> {
                    alterarComissaoLinhaPedido(linha.getId().getNumero(), linha.getId().getCodlin(), dateReferencia,  linha.getComcal());
                    linha.setVerificar(false);
                    linha.setComped(linha.getComcal());
                });
            }
        }).exec(task -> {
            findLinhasPedido(numero);
            return ReturnAsync.OK.value;
        });
    }
    
    protected void aplicarComissaoLinhasPedido(String numero, LocalDate dateReferencia, BigDecimal comissao) {
        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                linhasPedido.forEach(linha -> {
                    alterarComissaoLinhaPedido(linha.getId().getNumero(), linha.getId().getCodlin(), dateReferencia, comissao);
                    linha.setVerificar(false);
                    linha.setComped(linha.getComcal());
                });
            }
        }).exec(task -> {
            findLinhasPedido(numero);
            return ReturnAsync.OK.value;
        });
    }
    
    protected class FormFilter {
        String statusPedido = "AMBOS";
        Object verificarComissao = "AMBOS";
        Object statusComercial = "AMBOS";
        Object statusFinanceiro = "AMBOS";
        LocalDate emissaoInicio = null;
        LocalDate emissaoFim = null;
        String[] numeroPedido = new String[]{};
        String[] cliente = new String[]{};
        String[] representante = new String[]{};
        String[] marca = new String[]{};
        String[] colecao = new String[]{};
        String[] tabela = new String[]{};
        String[] grupoCli = new String[]{};

        public void set(String statusPedido, Object verificarComissao, Object statusComercial, Object statusFinanceiro,
                        LocalDate emissaoInicio, LocalDate emissaoFim, String numeroPedido, String cliente,
                        String representante, String marca, String colecao, String tabela, String grupoCli) {
            this.statusPedido = statusPedido;
            this.verificarComissao = verificarComissao;
            this.statusComercial = statusComercial;
            this.statusFinanceiro = statusFinanceiro;
            this.emissaoInicio = emissaoInicio;
            this.emissaoFim = emissaoFim;
            this.numeroPedido = numeroPedido == null ? new String[]{} : numeroPedido.split(",");
            this.cliente = cliente == null ? new String[]{} : cliente.split(",");
            this.representante = representante == null ? new String[]{} : representante.split(",");
            this.marca = marca == null ? new String[]{} : marca.split(",");
            this.colecao = colecao == null ? new String[]{} : colecao.split(",");
            this.tabela = tabela == null ? new String[]{} : tabela.split(",");
            this.grupoCli = grupoCli == null ? new String[]{} : grupoCli.split(",");
        }
        
        public void clear() {
            this.statusPedido = "AMBOS";
            this.verificarComissao = "AMBOS";
            this.statusComercial = "AMBOS";
            this.statusFinanceiro = "AMBOS";
            this.emissaoInicio = null;
            this.emissaoFim = null;
            this.numeroPedido = new String[]{};
            this.cliente = new String[]{};
            this.representante = new String[]{};
            this.marca = new String[]{};
            this.colecao = new String[]{};
            this.tabela = new String[]{};
            this.grupoCli = new String[]{};
        }
    }
}