package sysdeliz2.controllers.views.comercial.representantes;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdCidadeRep;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class ControleCidadesRepController extends WindowBase {

    protected List<SdCidadeRep> cidades = new ArrayList<>();
    protected List<SdCidadeRep> cidadesBkp = new ArrayList<>();

    public ControleCidadesRepController(String title, Image icon) {
        super(title, icon);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getCidades(Represen codRep, Marca marca, Object[] cidade, TabUf estado){
        JPAUtils.clearEntitys(cidades);
        String pCodRep = codRep.getCodRep() == null ? "" : codRep.getCodRep();
        String pMarca = marca.getDescricao() == null ? "" : marca.getDescricao();
        Object[] pCidades = cidade == null ? new Object[]{} : cidade;
        String pEstado = estado.getCodigo() == null ? "" : estado.getCodigo();

        cidades.clear();
        cidades = (List<SdCidadeRep>) new FluentDao().selectFrom(SdCidadeRep.class).where(it -> it
            .equal("id.codRep.codRep", pCodRep, TipoExpressao.AND, b -> !pCodRep.equals("") && !pCodRep.equals("0"))
            .equal("marca.descricao", pMarca, TipoExpressao.AND, b -> !pMarca.equals(""))
            .equal("id.codCid.codEst.codigo", pEstado, TipoExpressao.AND, b -> !pEstado.equals(""))
            .isIn("id.codCid.codCid", pCidades, TipoExpressao.AND, b -> pCidades.length > 0))
                .resultList();

        JPAUtils.clearEntitys(cidadesBkp);
        cidadesBkp.clear();
        cidadesBkp.addAll(cidades);
    }
    protected boolean representanteJaPossui(SdCidadeRep cidade, Represen representante){
        return (new FluentDao().selectFrom(SdCidadeRep.class).where(it -> it
                .equal("id.codCid.codCid", cidade.getId().getCodCid().getCodCid())
                .equal("marca.codigo", cidade.getMarca().getCodigo())
                .equal("id.codRep.codRep", representante.getCodRep())
        ).singleResult()) != null;
    }
    protected Cidade buscaCidadeDouble(double codCid){
        String sCod = String.format("%07d",Integer.valueOf(Double.toString(codCid).substring(0, Double.toString(codCid).indexOf("."))));
        return new FluentDao().selectFrom(Cidade.class).where(it -> it.equal("codCid", sCod)).singleResult();
    }
    protected Cidade buscaCidadeString(String codCid){
        return new FluentDao().selectFrom(Cidade.class).where(it -> it.equal("codCid", codCid)).singleResult();
    }
    protected Represen buscaRepresentanteDouble(double codRep){
        String sCod = String.format("%04d",Integer.valueOf(Double.toString(codRep).substring(0, Double.toString(codRep).indexOf("."))));
        return new FluentDao().selectFrom(Represen.class).where(it -> it.equal("codRep", sCod)).singleResult();
    }
    protected Represen buscaRepresentanteString(String codRep){
        return new FluentDao().selectFrom(Represen.class).where(it -> it.equal("codRep", codRep)).singleResult();
    }
    protected Marca buscaMarca(String codMarca){
        return new FluentDao().selectFrom(Marca.class).where(it -> it.equal("codigo", codMarca)).singleResult();
    }
}
