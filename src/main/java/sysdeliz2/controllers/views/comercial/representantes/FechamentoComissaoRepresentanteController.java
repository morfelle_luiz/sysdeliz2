package sysdeliz2.controllers.views.comercial.representantes;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDevolucaoRep;
import sysdeliz2.models.view.VSdFaturamentoRep;
import sysdeliz2.models.view.VSdLancamentosRep;
import sysdeliz2.models.view.VSdRecebimentosRep;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FechamentoComissaoRepresentanteController extends WindowBase {
    
    // <editor-fold defaultstate="collapsed" desc="List">
    protected List<HeaderFechamento> fechamentos = new ArrayList<>();
    protected List<VSdFaturamentoRep> faturamentos = new ArrayList<>();
    protected List<VSdRecebimentosRep> recebimentos = new ArrayList<>();
    protected List<VSdDevolucaoRep> devolucoes = new ArrayList<>();
    protected List<VSdLancamentosRep> lancamentos = new ArrayList<>();
    // </editor-fold>
    
    public FechamentoComissaoRepresentanteController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getFechamentos(Object[] codrep, LocalDate dtInicio, LocalDate dtFim, String fechado, String ativo) {
        Object[] reps = codrep != null ? codrep : new Object[]{};
        String iniPeriodo = StringUtils.toDateFormat(dtInicio != null ? dtInicio : LocalDate.now());
        String fimPeriodo = StringUtils.toDateFormat(dtFim != null ? dtFim : LocalDate.now());
        String comissaoFechada = fechado != null ? fechado : "'N'";
        String repAtivo = ativo == null ? "true" : ativo;
        List<Represen> repsDb = (List<Represen>) new FluentDao().selectFrom(Represen.class)
                .where(it -> it
                        .isIn("codRep", reps, TipoExpressao.AND, b -> reps.length > 0)
                        .equal("ativo", Boolean.parseBoolean(repAtivo.equals("T") ? "true" : repAtivo), TipoExpressao.AND, b -> !repAtivo.equals("T")))
                .resultList();
        String codreps = repsDb.stream()
                .map(it -> "'" + it.getCodRep() + "'").collect(Collectors.joining(","));
        
        List<Object> fechamentos = new ArrayList<>();
        if (codreps.length() > 0) {
            fechamentos = new FluentDao().runNativeQuery(String.format(
                    "select codrep,\n" +
                            "       sum(faturamento) faturamento,\n" +
                            "       sum(recebimento) recebimento,\n" +
                            "       sum(devolucao_tot) devolucao_tot,\n" +
                            "       sum(devolucao_par) devolucao_par,\n" +
                            "       sum(lanc_credito) lanc_credito,\n" +
                            "       sum(lanc_debito) lanc_debito\n" +
                            "  from sd_fechamento_diario_rep_001 fec\n" +
                            " where fec.codrep in (%s)\n" +
                            "   and fec.dt_fecha between '%s' and '%s'\n" +
                            "   and fec.fechado in (%s)\n" +
                            " group by codrep", codreps, iniPeriodo, fimPeriodo, comissaoFechada));
        }
        
        this.fechamentos.clear();
        fechamentos.forEach(it -> {
            Object[] dados = (Object[]) it;
            this.fechamentos.add(new HeaderFechamento(
                    (String) dados[0],
                    (BigDecimal) dados[1],
                    (BigDecimal) dados[2],
                    (BigDecimal) dados[3],
                    (BigDecimal) dados[4],
                    (BigDecimal) dados[5],
                    (BigDecimal) dados[6]
            
            ));
        });
    }
    
    protected void getFaturamentos(String codrep, LocalDate dtInicio, LocalDate dtFim) {
        JPAUtils.clearEntitys(faturamentos);
        faturamentos = (List<VSdFaturamentoRep>) new FluentDao()
                .selectFrom(VSdFaturamentoRep.class)
                .where(it -> it
                        .equal("codrep", codrep)
                        .between("dtemissao", dtInicio, dtFim))
                .orderBy(Arrays.asList(
                        new Ordenacao("dtemissao"),
                        new Ordenacao("marca"),
                        new Ordenacao("cliente"),
                        new Ordenacao("tipoitem"),
                        new Ordenacao("tabela")))
                .resultList();
    }
    
    protected void getRecebimentos(String codrep, LocalDate dtInicio, LocalDate dtFim) {
        JPAUtils.clearEntitys(recebimentos);
        recebimentos = (List<VSdRecebimentosRep>) new FluentDao()
                .selectFrom(VSdRecebimentosRep.class)
                .where(it -> it
                        .equal("codrep", codrep)
                        .between("dtrecebe", dtInicio, dtFim))
                .orderBy(Arrays.asList(
                        new Ordenacao("dtrecebe"),
                        new Ordenacao("duplicata"),
                        new Ordenacao("marca"),
                        new Ordenacao("nome"),
                        new Ordenacao("tipo"),
                        new Ordenacao("tabpre")))
                .resultList();
    }
    
    protected void getLancamentos(String codrep, LocalDate dtInicio, LocalDate dtFim) {
        LocalDate inicioPeriodo = dtInicio.plusMonths(1).with(TemporalAdjusters.firstDayOfMonth());
        LocalDate fimPeriodo = dtFim.plusMonths(1).with(TemporalAdjusters.lastDayOfMonth());

        JPAUtils.clearEntitys(lancamentos);
        lancamentos = (List<VSdLancamentosRep>) new FluentDao()
                .selectFrom(VSdLancamentosRep.class)
                .where(it -> it
                        .equal("codrep", codrep)
                        .between("dtlan", inicioPeriodo, fimPeriodo))
                .orderBy(Arrays.asList(
                        new Ordenacao("dtlan"),
                        new Ordenacao("descricao")))
                .resultList();
    }
    
    protected void getDevolucoes(String codrep, LocalDate dtInicio, LocalDate dtFim) {

        JPAUtils.clearEntitys(devolucoes);
        devolucoes = (List<VSdDevolucaoRep>) new FluentDao()
                .selectFrom(VSdDevolucaoRep.class)
                .where(it -> it
                        .equal("codrep", codrep)
                        .between("dtemissao", dtInicio, dtFim))
                .orderBy(Arrays.asList(
                        new Ordenacao("dtemissao"),
                        new Ordenacao("cliente"),
                        new Ordenacao("tipoitem"),
                        new Ordenacao("tabela")))
                .resultList();
    }
    
    protected void fecharPeriodoRepresentante(HeaderFechamento repSelecionado, LocalDate dateIni, LocalDate dateEnd) {
        new FluentDao().runNativeQueryUpdate(String.format(
                "update sd_fechamento_diario_rep_001 rep\n" +
                        "   set rep.fechado = 'S'\n" +
                        " where rep.codrep = '%s'\n" +
                        "   and rep.dt_fecha between to_date('%s','DD/MM/YYYY') and to_date('%s','DD/MM/YYYY')",
                repSelecionado.getCodrep().getCodRep(),
                StringUtils.toDateFormat(dateIni),
                StringUtils.toDateFormat(dateEnd)));
        
        SysLogger.addSysDelizLog("Fechamento Comissão Rep.", TipoAcao.EDITAR, repSelecionado.getCodigo(),
                "Fechado comissão do representante para o período de "
                        + StringUtils.toDateFormat(dateIni)
                        + " até " + StringUtils.toDateFormat(dateEnd));
    }
    
    protected class HeaderFechamento {
    
        private final StringProperty codigo = new SimpleStringProperty();
        private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> faturamento = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> recebimento = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> devolucaoTot = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> devolucaoPar = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> lancCredito = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> lancDebito = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> saldo = new SimpleObjectProperty<>();
        
        public HeaderFechamento() {
        }
        
        public HeaderFechamento(String codrep, BigDecimal fechamento, BigDecimal recebimento, BigDecimal devolucaoTot, BigDecimal devolucaoPar, BigDecimal lancCredito, BigDecimal lancDebito) {
            this.codrep.set(new FluentDao().selectFrom(Represen.class).where(it -> it.equal("codRep", codrep)).singleResult());
            this.codigo.set(this.codrep.get() != null ? this.codrep.get().getCodRep() : "");
            this.faturamento.set(fechamento);
            this.recebimento.set(recebimento);
            this.devolucaoTot.set(devolucaoTot);
            this.devolucaoPar.set(devolucaoPar);
            this.lancCredito.set(lancCredito);
            this.lancDebito.set(lancDebito);
            this.saldo.set(fechamento.add(recebimento).add(devolucaoPar).add(devolucaoTot).add(lancCredito).add(lancDebito));
        }
        
        public Represen getCodrep() {
            return codrep.get();
        }
        
        public ObjectProperty<Represen> codrepProperty() {
            return codrep;
        }
        
        public void setCodrep(Represen codrep) {
            this.codrep.set(codrep);
        }
        
        public BigDecimal getFaturamento() {
            return faturamento.get();
        }
        
        public ObjectProperty<BigDecimal> faturamentoProperty() {
            return faturamento;
        }
        
        public void setFaturamento(BigDecimal faturamento) {
            this.faturamento.set(faturamento);
        }
        
        public BigDecimal getRecebimento() {
            return recebimento.get();
        }
        
        public ObjectProperty<BigDecimal> recebimentoProperty() {
            return recebimento;
        }
        
        public void setRecebimento(BigDecimal recebimento) {
            this.recebimento.set(recebimento);
        }
        
        public BigDecimal getDevolucaoTot() {
            return devolucaoTot.get();
        }
        
        public ObjectProperty<BigDecimal> devolucaoTotProperty() {
            return devolucaoTot;
        }
        
        public void setDevolucaoTot(BigDecimal devolucaoTot) {
            this.devolucaoTot.set(devolucaoTot);
        }
        
        public BigDecimal getDevolucaoPar() {
            return devolucaoPar.get();
        }
        
        public ObjectProperty<BigDecimal> devolucaoParProperty() {
            return devolucaoPar;
        }
        
        public void setDevolucaoPar(BigDecimal devolucaoPar) {
            this.devolucaoPar.set(devolucaoPar);
        }
        
        public BigDecimal getLancCredito() {
            return lancCredito.get();
        }
        
        public ObjectProperty<BigDecimal> lancCreditoProperty() {
            return lancCredito;
        }
        
        public void setLancCredito(BigDecimal lancCredito) {
            this.lancCredito.set(lancCredito);
        }
        
        public BigDecimal getLancDebito() {
            return lancDebito.get();
        }
        
        public ObjectProperty<BigDecimal> lancDebitoProperty() {
            return lancDebito;
        }
        
        public void setLancDebito(BigDecimal lancDebito) {
            this.lancDebito.set(lancDebito);
        }
        
        public BigDecimal getSaldo() {
            return saldo.get();
        }
        
        public ObjectProperty<BigDecimal> saldoProperty() {
            return saldo;
        }
        
        public void setSaldo(BigDecimal saldo) {
            this.saldo.set(saldo);
        }
    
        public String getCodigo() {
            return codigo.get();
        }
    
        public StringProperty codigoProperty() {
            return codigo;
        }
    
        public void setCodigo(String codigo) {
            this.codigo.set(codigo);
        }
    }
}
