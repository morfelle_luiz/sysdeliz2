package sysdeliz2.controllers.views.comercial.representantes;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.SdProdutoColecao;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.sysdeliz.comercial.SdTransfProdMostruario;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.comercial.VSdMostRetornoLiberado;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MostruarioRepController extends WindowBase {

    protected List<SdProdutoColecao> produtos = new ArrayList<>();
    protected List<Represen> representantes = new ArrayList<>();
    protected List<SdMostrRep> mostruarios = new ArrayList<>();

    public MostruarioRepController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void findProdutosColecao(String colecao, Object[] marcas, Object[] produto, Object[] modelagens) {
        JPAUtils.clearEntitys(produtos);
        produtos = (List<SdProdutoColecao>) new FluentDao().selectFrom(SdProdutoColecao.class)
                .where(eb -> eb
                        .equal("pc", true)
                        .equal("id.colecao.codigo", colecao)
                        .isIn("id.codigo.grupomodelagem", modelagens, TipoExpressao.AND, b -> modelagens.length > 0)
                        .isIn("id.codigo.codMarca", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                        .isIn("id.codigo.codigo", produto, TipoExpressao.AND, b -> produto.length > 0)
                )
                .orderBy("id.codigo.codigo", OrderType.ASC)
                .resultList();
        produtos.forEach(SdProdutoColecao::refresh);
    }

    protected void findRepresentantes(String colecao, Object[] marcas, Object[] representantes, Object[] cidades, Object[] estados, String ativo) {
        JPAUtils.clearEntitys(this.representantes);
        this.representantes = ((List<Represen>) new FluentDao().selectFrom(Represen.class)
                .where(eb -> eb
                        .isIn("codRep", representantes, TipoExpressao.AND, b -> representantes.length > 0)
                        .isIn("cep.cidade.codCid", cidades, TipoExpressao.AND, b -> cidades.length > 0)
                        .isIn("cep.cidade.codEst.id.siglaEst", estados, TipoExpressao.AND, b -> estados.length > 0)
                        .equal("ativo", Boolean.parseBoolean(ativo), TipoExpressao.AND, b -> !ativo.equals("t")))
                .orderBy("nome", OrderType.ASC)
                .resultList()).stream()
                .filter(represen -> represen.getMarcas().stream().anyMatch(marca -> marca.isAtivo() && Arrays.asList(marcas).contains(marca.getId().getMarca().getCodigo()))
                        && represen.getMarcas().stream().anyMatch(marca -> marca.getColecoes().stream()
                        .anyMatch(col -> col.getId().getColecao().getCodigo().equals(colecao))))
                .collect(Collectors.toList());
    }

    protected void saveMostruarioRepresentante(SdMostrRep mostruario) throws SQLException {
        new FluentDao().persist(mostruario);
    }

    protected void findMostruarios(Object[] colecao, Object[] representante, Object[] marca, String tipo) {
        JPAUtils.clearEntitys(mostruarios);
        mostruarios = (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .equal("tipo", tipo, TipoExpressao.AND, b -> !tipo.equals("T"))
                        .isIn("colvenda", colecao, TipoExpressao.AND, b -> colecao.length > 0)
                        .isIn("marca", marca, TipoExpressao.AND, b -> marca.length > 0)
                        .isIn("codrep.codRep", representante, TipoExpressao.AND, b -> representante.length > 0))
                .orderBy(Arrays.asList(
                        new Ordenacao("colvenda"),
                        new Ordenacao("marca"),
                        new Ordenacao("tipo"),
                        new Ordenacao("mostruario"),
                        new Ordenacao("codrep.nome")))
                .resultList();
    }

    protected void deleteMostruario(SdMostrRep mostruario) {
        new FluentDao().delete(mostruario);
    }

    protected void atualizarProdutosMostruario(List<SdMostrRepItens> produtos) throws SQLException {
        for (SdMostrRepItens produto : produtos) {
            new NativeDAO().runNativeQueryUpdate("" +
                            "insert into sd_mostr_rep_itens_001\n" +
                            "  (mostruario, codigo, qtde, tipo, marca, col_venda, qtde_lida)\n" +
                            "values\n" +
                            "  (%s, '%s', 1, '%s', '%s', '%s', 0)",
                    produto.getMostruario().getMostruario(),
                    produto.getCodigo().getCodigo(),
                    produto.getMostruario().getTipo(),
                    produto.getMostruario().getMarca(),
                    produto.getMostruario().getColvenda()
            );
        }
    }

    protected void deleteReferenciaMostruario(SdMostrRepItens produto) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(
                "  update sd_mostr_rep_itens_001\n" +
                        "   set status = 'X'\n" +
                        " where mostruario = %s\n" +
                        "   and tipo = '%s'\n" +
                        "   and marca = '%s'\n" +
                        "   and col_venda = '%s'\n" +
                        "   and codigo = '%s'",
                produto.getMostruario().getMostruario(),
                produto.getMostruario().getTipo(),
                produto.getMostruario().getMarca(),
                produto.getMostruario().getColvenda(),
                produto.getCodigo().getCodigo());
    }

    protected Object[] getRepsComMostruario(String colecao, String marca) {
        Object[] representantes = new FluentDao().selectFrom(SdMostrRep.class)
                .distinctColumn("codrep.codRep")
                .where(eb -> eb
                        .equal("colvenda", colecao)
                        .equal("marca", marca))
                .resultList().toArray();

        return representantes;
    }

    protected List<SdMostrRepItens> produtosMostruarioRepColecao(String produto, String representante, String colecao, Object[] mostruarios) {
        return (List<SdMostrRepItens>) new FluentDao().selectFrom(SdMostrRepItens.class)
                .where(eb -> eb
                        .isIn("mostruario.mostruario", mostruarios)
                        .equal("codigo.codigo", produto)
                        .equal("mostruario.codrep.codRep", representante)
                        .equal("mostruario.colvenda", colecao)
                        .equal("status","E")
                ).resultList();
    }

    protected void guardarPecasTransferencia(SdTransfProdMostruario produtoTransferencia) throws SQLException {
        new FluentDao().persist(produtoTransferencia);
    }

    protected List<VSdMostRetornoLiberado> getMostRetornoLiberados(String codRep, String colecao) {
        return (List<VSdMostRetornoLiberado>) new FluentDao().selectFrom(VSdMostRetornoLiberado.class)
                .where(eb -> eb
                        .equal("status", "F")
                        .equal("faturado", true)
                        .equal("devolvido", true)
                        .equal("id.colVenda", colecao)
                        .equal("id.codrep", codRep))
                .resultList();
    }

    protected List<SdColecaoMarca001> getProximaColecaoMarcas(Object[] marcas, String colecao) {
        return (List<SdColecaoMarca001>) new FluentDao().selectFrom(SdColecaoMarca001.class)
                .where(eb -> eb
//                        .equal("id.colecao.codigo", inputColecaoDestino.value.get().getCodigo())
                                .equal("colecaoAnterior", colecao)
                                .isIn("id.marca.codigo", marcas)
                ).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
