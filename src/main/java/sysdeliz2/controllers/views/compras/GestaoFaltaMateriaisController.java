package sysdeliz2.controllers.views.compras;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.MatIten;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.PcpFtOf;
import sysdeliz2.models.view.VSdGestaoMatFaltante;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GestaoFaltaMateriaisController extends WindowBase {
    
    protected List<VSdGestaoMatFaltante> materiaisFaltantes = new ArrayList<>();
    protected final ListProperty<Cor> coresMaterial = new SimpleListProperty<>(FXCollections.observableArrayList());
    
    public GestaoFaltaMateriaisController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getFaltantes(Object[] numero, Object[] material, Object[] cor, Object[] setor, Object[] setorMov, Object[] grupo, Object[] subgrupo, String substituto, String status, Object[] faccao, Object[] periodo)  {
        Object[] pnumero = numero != null ? numero : new Object[]{};
        Object[] pmaterial = material != null ? material : new Object[]{};
        Object[] pcor = cor != null ? cor : new Object[]{};
        Object[] psetor = setor != null ? setor : new Object[]{};
        Object[] psetorMov = setorMov != null ? setorMov : new Object[]{};
        Object[] pgrupo = grupo != null ? grupo : new Object[]{};
        Object[] psubgrupo = subgrupo != null ? subgrupo : new Object[]{};
        Object[] pfaccao = faccao != null ? faccao : new Object[]{};
        Object[] pperiodo = periodo != null ? periodo : new Object[]{};
        Object[] pstatus = status.equals("D") ? new Object[]{"A","C"} : new Object[]{status};
    
        JPAUtils.clearEntitys(materiaisFaltantes);
        materiaisFaltantes = (List<VSdGestaoMatFaltante>)
                new FluentDao().selectFrom(VSdGestaoMatFaltante.class)
                        .where(eb -> eb
                                .isIn("numero", pnumero, TipoExpressao.AND, b -> pnumero.length > 0)
                                .isIn("insumo.codigo", pmaterial, TipoExpressao.AND, b -> pmaterial.length > 0)
                                .isIn("cori.cor", pcor, TipoExpressao.AND, b -> pcor.length > 0)
                                .isIn("setor.codigo", psetor, TipoExpressao.AND, b -> psetor.length > 0)
                                .isIn("setoratual.codigo", psetorMov, TipoExpressao.AND, b -> psetorMov.length > 0)
                                .isIn("insumo.grupo.codigo", pgrupo, TipoExpressao.AND, b -> pgrupo.length > 0)
                                .isIn("insumo.subgrupo.codigo", psubgrupo, TipoExpressao.AND, b -> psubgrupo.length > 0)
                                .isIn("faccao.codcli", pfaccao, TipoExpressao.AND, b -> pfaccao.length > 0)
                                .isIn("periodo", pperiodo, TipoExpressao.AND, b -> pperiodo.length > 0)
                                .isIn("status", pstatus, TipoExpressao.AND, b -> !status.equals("T"))
                                .equal("substituto", Boolean.valueOf(substituto.equals("T") ? "false" : substituto), TipoExpressao.AND, b -> !substituto.equals("T"))
                        ).resultList();
    }
    
    protected void substituirMaterial(VSdGestaoMatFaltante materialFaltante, Material materialSubs, Cor corSubs) {
        if (materialFaltante != null && materialSubs != null && corSubs != null) {
            List<PcpFtOf> materiaisOriginal = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", materialFaltante.getNumero())
                            .equal("pcpFtOfID.insumo", materialFaltante.getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", materialFaltante.getCori().getCor())
                            .equal("pcpFtOfID.setor", materialFaltante.getSetor().getCodigo()))
                    .resultList();
            JPAUtils.clearEntitys(materiaisOriginal);
            materiaisOriginal.forEach(material -> {
                if (material.getInsumoOrig() == null){
                    new FluentDao().runNativeQueryUpdate(String.format("update pcpftof_001 set insumo_orig = insumo, cor_i_orig = cor_i where numero = '%s' and setor = '%s' and insumo = '%s' and cor_i = '%s'",
                            material.getPcpFtOfID().getNumero(),
                            material.getPcpFtOfID().getSetor(),
                            material.getPcpFtOfID().getInsumo(),
                            material.getPcpFtOfID().getCorI()));
                }
                new FluentDao().runNativeQueryUpdate(String.format("update pcpftof_001 set insumo = '%s', cor_i = '%s' where numero = '%s' and setor = '%s' and insumo = '%s' and cor_i = '%s'",
                        materialSubs.getCodigo(),
                        corSubs.getCor(),
                        material.getPcpFtOfID().getNumero(),
                        material.getPcpFtOfID().getSetor(),
                        material.getPcpFtOfID().getInsumo(),
                        material.getPcpFtOfID().getCorI()));
            });
        
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, materialFaltante.getNumero(),
                    "Subtituido material " + materialFaltante.getInsumo() + " na cor " + materialFaltante.getCori() +
                            " pelo material " + materialSubs + " na cor " + corSubs);
        
        }
    }
    
    protected void alterarStatusFalta(VSdGestaoMatFaltante materialFaltante, String novoStatus) {
        if (materialFaltante != null) {
            new FluentDao().runNativeQueryUpdate(String.format("update sd_materiais_faltantes_001 set status = '%s' where numero = '%s' and insumo = '%s' and cor_i = '%s'",
                    novoStatus, materialFaltante.getNumero(), materialFaltante.getInsumo().getCodigo(), materialFaltante.getCori().getCor()));
        }
    }
    
    protected void getCoresMaterial(String codigo) {
        List<String> coresMaterial = (List<String>) ((List<MatIten>) new FluentDao().selectFrom(MatIten.class)
                .where(it -> it
                        .equal("id.codigo", codigo)
                        .notEqual("id.deposito.codigo", "07153")).resultList()).stream()
                .map(cor -> cor.getId().getCor()).distinct().collect(Collectors.toList());
        
        this.coresMaterial.set(FXCollections.observableList((List<Cor>) new FluentDao().selectFrom(Cor.class)
                .where(it -> it
                        .isIn("cor", coresMaterial.toArray())
                ).resultList()));
    }
}
