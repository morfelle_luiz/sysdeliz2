package sysdeliz2.controllers.views.compras;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.view.VSdMateriasLote;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class ManutencaoDeLoteController extends WindowBase {

    protected List<VSdMateriasLote> materiais = new ArrayList<>();

    public ManutencaoDeLoteController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getItens(Object[] codigo, Object[] cor, String lote){
        Object[] pCodigo = codigo != null ? codigo : new Object[]{};
        Object[] pCor = cor != null ? cor : new Object[]{};

       materiais = (List<VSdMateriasLote>)
               new FluentDao().selectFrom(VSdMateriasLote.class)
                    .where(eb -> eb
                        .isIn("codigo", pCodigo, TipoExpressao.AND, b-> pCodigo.length > 0)
                        .isIn("cor", pCor, TipoExpressao.AND, b -> pCor.length > 0)
                        .like("lote",lote)
                    ).resultList();
   }
}
