package sysdeliz2.controllers.views.contabilidade;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Natureza;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class CadastroNaturezaController extends WindowBase {

    protected List<Natureza> naturezas = new ArrayList<>();

    public CadastroNaturezaController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    @Override
    public void closeWindow() {

    }

    protected void buscarNaturezas(Natureza natureza, String tipo, String entradaSaida, boolean ativo) {
        naturezas = (List<Natureza>) new FluentDao().selectFrom(Natureza.class).where(it -> it
                .equal("natureza", natureza.getNatureza(), TipoExpressao.AND, when -> natureza != null && natureza.getNatureza() != null)
                .equal("tipo", tipo.substring(0, tipo.indexOf(" - ")).trim(), TipoExpressao.AND, when -> !tipo.equals("T - Todos"))
                .equal("tpBase", entradaSaida.substring(0, entradaSaida.indexOf(" - ")).trim(), TipoExpressao.AND, when -> !entradaSaida.equals("0 - Ambos"))
                .equal("ativo", ativo)
        ).resultList();
    }

}
