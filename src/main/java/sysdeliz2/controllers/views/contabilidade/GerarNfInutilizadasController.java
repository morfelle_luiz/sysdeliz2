package sysdeliz2.controllers.views.contabilidade;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.view.VSdNotaInutilizadaHeader;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.PersistenceException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GerarNfInutilizadasController extends WindowBase {

    protected List<VSdNotaInutilizadaHeader> notasFiscais = new ArrayList<>();

    public GerarNfInutilizadasController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void gerarNotas() throws SQLException {
        JPAUtils.clearEntitys(notasFiscais);
        try {
            notasFiscais = (List<VSdNotaInutilizadaHeader>) new FluentDao().selectFrom(VSdNotaInutilizadaHeader.class).get().resultList();
            fixDate();
        } catch (SQLException | PersistenceException e) {
            e.printStackTrace();
        }
    }

    protected void fixDate() throws SQLException {
        notasFiscais.forEach(nota -> {
            try {
                getNextNota(nota);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    protected void getNextNota(VSdNotaInutilizadaHeader nota) throws SQLException {
        JPAUtils.clearEntitys(notasFiscais);
        List<Object> notas;
        String numnfi = nota.getNumnfi();
        int nNota = Integer.parseInt(numnfi);
        do {
            nNota++;
            String numeroNota = String.format("%06d", nNota);
            switch (nota.getCodemp()) {
                case 1:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 1, numeroNota));
                    break;
                case 2:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 2, numeroNota));
                    break;
                case 10:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 3, numeroNota));
                    break;
                case 4:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 4, numeroNota));
                    break;
                case 9:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 5, numeroNota));
                    break;
                case 6:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 6, numeroNota));
                    break;
                case 7:
                    notas = new NativeDAO().runNativeQuery(String.format("select * from nota_00%d where fatura = %s", 7, numeroNota));
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + nota.getCodemp());
            }
        } while (notas.isEmpty());

        notas.forEach(row -> {
            Map<String, Object> notaFiscal = (Map<String, Object>) row;

            Timestamp dataEmissao = (Timestamp) notaFiscal.get("DT_EMISSAO");
            LocalDate dataEmi = dataEmissao.toLocalDateTime().toLocalDate();

            Timestamp dataSaida = (Timestamp) notaFiscal.get("DT_SAIDA");
            LocalDate dataSai = dataSaida.toLocalDateTime().toLocalDate();

            nota.setDataSai(dataSai);
            nota.setDataEmi(dataEmi);
        });
    }
}


