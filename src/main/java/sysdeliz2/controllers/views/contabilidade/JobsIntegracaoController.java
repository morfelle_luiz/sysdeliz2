package sysdeliz2.controllers.views.contabilidade;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class JobsIntegracaoController extends WindowBase {

    public JobsIntegracaoController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected final StringProperty dataUltimoLog = new SimpleStringProperty();
    protected final StringProperty infoUltimoLog = new SimpleStringProperty();

    protected final StringProperty NFEinicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty NFETempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty NFEStatusJob = new SimpleStringProperty();
    protected final StringProperty NFEDataProximoLog = new SimpleStringProperty();

    protected final StringProperty NFSinicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty NFSTempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty NFSStatusJob = new SimpleStringProperty();
    protected final StringProperty NFSDataProximoLog = new SimpleStringProperty();

    protected void getLogJob(){
        List<Object> dadosLog = new FluentDao().runNativeQuery(""+
                "select TO_CHAR(log.LOG_DATE, 'dd/MM/yyyy HH24:MI:SS') dt_log,\n" +
                "       decode(to_char(additional_info),\n" +
                "              'REASON=\"manual slave run\"',\n" +
                "              'Execução Manual',\n" +
                "              null,\n" +
                "              'Execução Automática',\n" +
                "              to_char(additional_info)) info\n" +
                "  from USER_SCHEDULER_JOB_LOG log\n" +
                " where log.log_id = (select max(log_id)\n" +
                "                       from USER_SCHEDULER_JOB_LOG\n" +
                "                      where job_name = 'J_ERP_IMPORT_NF_ENTRADA')\n");

        if (dadosLog.size() > 0){
            Object[] infosLog = (Object[]) dadosLog.get(0);
            dataUltimoLog.set(infosLog[0].toString());
            infoUltimoLog.set(infosLog[1].toString());
        }
    }

    protected void getStatusJobNfe() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery(""+
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'J_ERP_IMPORT_NF_ENTRADA'");

        if (dadosLog.size() > 0){
            Map infosLog = (Map) dadosLog.get(0);
            NFEStatusJob.set(infosLog.get("ESTADO").toString());
            NFEinicioUltimoLog.set(infosLog.get("ULT_RODADA").toString());
            NFETempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            NFEDataProximoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }

    protected void getStatusJobNfs() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery(""+
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'J_ERP_IMPORT_NF_SAIDA'");

        if (dadosLog.size() > 0){
            Map infosLog = (Map) dadosLog.get(0);
            NFSStatusJob.set(infosLog.get("ESTADO").toString());
            NFSinicioUltimoLog.set(infosLog.get("ULT_RODADA") != null ?  infosLog.get("ULT_RODADA").toString() : "");
            NFSTempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            NFSDataProximoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }

    protected void runJobNFe() throws SQLException {
        new NativeDAO().runNativeQueryUpdate(""+
                "BEGIN\n" +
                "  DBMS_SCHEDULER.RUN_JOB(\n" +
                "    JOB_NAME            => 'J_ERP_IMPORT_NF_ENTRADA',\n" +
                "    USE_CURRENT_SESSION => FALSE);\n" +
                "END;");

        getStatusJobNfe();
    }

    protected void runJobNFs() throws SQLException {
        new NativeDAO().runNativeQueryUpdate(""+
                "BEGIN\n" +
                "  DBMS_SCHEDULER.RUN_JOB(\n" +
                "    JOB_NAME            => 'J_ERP_IMPORT_NF_SAIDA',\n" +
                "    USE_CURRENT_SESSION => FALSE);\n" +
                "END;");

        getStatusJobNfs();
    }

}
