package sysdeliz2.controllers.views.controlegeral;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.sistema.SdGrupoParametros;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.List;

public class ParametrosSistemaController extends WindowBase {

    protected final List<SdGrupoParametros> grupos = (List<SdGrupoParametros>) new FluentDao().selectFrom(SdGrupoParametros.class).get().resultList();

    public ParametrosSistemaController(String title, Image icone) {
        super(title, icone);
    }

    protected List<SdParametros> getParametros(int codigo) {
        return (List<SdParametros>) new FluentDao().selectFrom(SdParametros.class)
                .where(eb -> eb.equal("grupo.codigo", codigo))
                .resultList();
    }

    protected void atualizarParametro(SdParametros parametro) {
        new FluentDao().merge(parametro);
    }

    @Override
    public void closeWindow() {

    }
}
