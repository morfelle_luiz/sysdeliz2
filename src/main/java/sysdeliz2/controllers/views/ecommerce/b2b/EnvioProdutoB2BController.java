package sysdeliz2.controllers.views.ecommerce.b2b;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.models.view.b2b.VB2bProdutosEnvio;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.lang.annotation.Native;
import java.sql.SQLException;
import java.util.List;

public class EnvioProdutoB2BController extends WindowBase {

    public EnvioProdutoB2BController(String title, Image icone) {
        super(title, icone);
    }

    protected List<VB2bProdutosEnvio> carregarProdutos(Object[] produtos, Object[] marcas, Object[] colecoes,
                                                       Object[] linhas, Object[] modelagens, String status) {
        return (List<VB2bProdutosEnvio>) new FluentDao().selectFrom(VB2bProdutosEnvio.class)
                .where(eb -> eb
                        .equal("status", status, TipoExpressao.AND, b -> !status.equals("T"))
                        .isIn("codigo.codigo", produtos, TipoExpressao.AND, b -> produtos.length > 0)
                        .isIn("marca", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                        .isIn("codigo.colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("codigo.linha.codigo", linhas, TipoExpressao.AND, b -> linhas.length > 0)
                        .isIn("codigo.etiqueta.codigo", modelagens, TipoExpressao.AND, b -> modelagens.length > 0)
                )
                .resultListNoHint();
    }

    protected SdProduto getSdProduto(String codigo) {
        return new FluentDao().selectFrom(SdProduto.class)
                .where(eb -> eb.equal("codigo", codigo))
                .singleResult();
    }

    protected void salvarDadosProduto(SdProduto produtoBd) {
        new FluentDao().merge(produtoBd);
    }

    protected void atualizaStatusProdutos(List<VB2bProdutosEnvio> produtosSelecionados) {
        produtosSelecionados.forEach(produto -> {
            SdProduto produtoBd = getSdProduto(produto.getReferencia());
            produtoBd.setStatusb2b("4");
            new FluentDao().merge(produtoBd);
        });
    }

    protected void trocaSituacaoLoja(String prositua, String codigo) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update sd_produto_001 set situacao_loja = '%s' where codigo = '%s'", prositua, codigo);
    }

    @Override
    public void closeWindow() {

    }
}
