package sysdeliz2.controllers.views.ecommerce.b2b;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.ecommerce.b2b.SdProdutoCorTabela;
import sysdeliz2.models.ti.TabPreco;
import sysdeliz2.models.view.VSdMrpEstoqueB2b;
import sysdeliz2.utils.gui.window.base.WindowBase;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class ProductShippingMaintenceController extends WindowBase {

    // <editor-fold defaultstate="collapsed" desc="Declarations: Lists">
    protected final ListProperty<VSdMrpEstoqueB2b> shippingProducts = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected final ListProperty<VSdMrpEstoqueB2b> shippingProductsSelectSize = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected final ListProperty<String> sizesProducts = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    public ProductShippingMaintenceController(String title, Image icone) {
        super(title, icone);
    }

    @Override
    public void closeWindow() {

    }

    protected void filterShippingProducts(String[] colecao, String[] marca, String[] produto, String[] entrega, String[] tabPreco) {
//        shippingProducts.set(FXCollections.observableList((List<VSdMrpEstoqueB2b>) new FluentDao().selectFrom(VSdMrpEstoqueB2b.class).where(it ->
//                it
//                        .isIn("codigo", produto, TipoExpressao.AND, when -> produto.length > 0)
//                        .isIn("entrega", entrega, TipoExpressao.AND, when -> entrega.length > 0)
//                        .isIn("marca", marca, TipoExpressao.AND, when -> marca.length > 0)
//                        .isIn("colecao", colecao, TipoExpressao.AND, when -> colecao.length > 0)
//        ).resultList()));

        List<VSdMrpEstoqueB2b> estoques = (List<VSdMrpEstoqueB2b>) new FluentDao().selectFrom(VSdMrpEstoqueB2b.class).where(it ->
                it
                        .isIn("codigo", produto, TipoExpressao.AND, when -> produto.length > 0)
                        .isIn("entrega", entrega, TipoExpressao.AND, when -> entrega.length > 0)
                        .isIn("marca", marca, TipoExpressao.AND, when -> marca.length > 0)
                        .isIn("colecao", colecao, TipoExpressao.AND, when -> colecao.length > 0)
        ).resultList();

        if (tabPreco.length > 0)
            estoques.removeIf(estoque -> ((Long) new FluentDao().selectFrom(TabPreco.class).count("codigo").where(it -> it.equal("id.codigo.codigo", estoque.getCodigo()).isIn("id.regiao", tabPreco)).singleResult()) == 0);
        shippingProducts.set(FXCollections.observableList(estoques));
        loadData();
    }

    protected List<SdProdutoCorTabela> getTabelasProdutoCor(VSdMrpEstoqueB2b produtoCor) {
        return (List<SdProdutoCorTabela>) new FluentDao().selectFrom(SdProdutoCorTabela.class)
                .where(eb -> eb
                        .equal("id.codigo", produtoCor.getCodigo())
                        .equal("id.cor", produtoCor.getCor()))
                .resultList();
    }

    protected void loadData() {
        if (shippingProducts.size() == 0)
            return;

        sizesProducts.set(FXCollections.observableList(shippingProducts.stream().map(item -> item.getFaixa() + " - " + item.getDescFaixa()).distinct().collect(Collectors.toList())));
    }

    protected void saveProducts() throws SQLException {
        for (VSdMrpEstoqueB2b productShipping : shippingProductsSelectSize)
            try {
                //new GenericDaoImpl(SdEntProdutoB2b001.class).delete(new SdEntProdutoB2b001(productShipping.getCodigo(), productShipping.getCor(), productShipping.getEntrega()).getId());
                new NativeDAO().runNativeQueryUpdate(String.format("delete from SD_ENT_PRODUTO_B2B_001 where codigo = '%s' and cor = '%s' and entrega = '%s'", productShipping.getCodigo(), productShipping.getCor(), productShipping.getEntrega()));
                new NativeDAO().runNativeQueryUpdate(String.format("update SD_MRP_ESTOQUE_B2B_001 set b2b = 'N' where id_jpa = '%s'", productShipping.getId()));
            } catch (NoResultException e) {
                e.printStackTrace();
            }

        for (VSdMrpEstoqueB2b productShipping : shippingProductsSelectSize.stream().filter(item -> item.isSelected()).collect(Collectors.toList())) {
            new NativeDAO().runNativeQueryUpdate(String.format(
                    "insert into SD_ENT_PRODUTO_B2B_001 (codigo, cor, entrega) values ('%s','%s','%s')",
                    productShipping.getCodigo(), productShipping.getCor(), productShipping.getEntrega()));
            new NativeDAO().runNativeQueryUpdate(String.format(
                    "update SD_MRP_ESTOQUE_B2B_001 set b2b = 'S' where id_jpa = '%s'",
                    productShipping.getId()));
        }
    }

    protected void adicionarTabela(SdProdutoCorTabela novaTabela) throws EntityExistsException, SQLException {
        new FluentDao().persist(novaTabela);
    }

    protected void selectSize(String sizeProduct) {
        shippingProductsSelectSize.set(
                FXCollections.observableList(
                        shippingProducts.stream()
                                .filter(item -> item.getFaixa().equals(sizeProduct.split(" - ")[0]))
                                .collect(Collectors.toList())));
    }
}

