package sysdeliz2.controllers.views.ecommerce.b2c;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.models.view.VSdProdutoMagento;
import sysdeliz2.models.view.b2b.VB2bProdutosEnvio;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.util.ArrayList;
import java.util.List;

public class EnvioProdutoB2CController extends WindowBase {

    protected List<VB2cProdutosEnvio> produtosEnvios = new ArrayList<>();

    public EnvioProdutoB2CController(String title, Image icone) {
        super(title, icone);
    }

    protected List<VB2cProdutosEnvio> carregarProdutos(Object[] produtos, Object[] marcas, Object[] colecoes,
                                                       Object[] linhas, Object[] modelagens, String status) {
        if (produtosEnvios != null && produtosEnvios.size() > 0) JPAUtils.clearEntitys(produtosEnvios);
        try {
            produtosEnvios =  (List<VB2cProdutosEnvio>) new FluentDao().selectFrom(VB2cProdutosEnvio.class)
                    .where(eb -> eb
                            .equal("status", status, TipoExpressao.AND, b -> !status.equals("T"))
                            .isIn("referencia", produtos, TipoExpressao.AND, b -> produtos.length > 0)
                            .isIn("marca", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                            .isIn("colecao", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                            .isIn("linha", linhas, TipoExpressao.AND, b -> linhas.length > 0)
                            .isIn("modelagem", modelagens, TipoExpressao.AND, b -> modelagens.length > 0)
                    )
                    .resultListNoHint();
        } catch (RuntimeException e) {
            produtosEnvios = new ArrayList<>();
        }
        return produtosEnvios;
    }

    protected SdProduto getSdProduto(String codigo) {
        return new FluentDao().selectFrom(SdProduto.class)
                .where(eb -> eb.equal("codigo", codigo))
                .singleResult();
    }

    protected void salvarDadosProduto(SdProduto produtoBd) {
        produtoBd = new FluentDao().merge(produtoBd);
    }

    protected void atualizaStatusProdutos(List<VB2cProdutosEnvio> produtosSelecionados) {
        produtosSelecionados.stream().filter(it -> it.getStatusEnvioProduto().isEnviado()).forEach(produto -> {
            produto.getSdProduto().setLojastatus("1");
            new FluentDao().merge(produto.getSdProduto());
            produto.setSelected(false);
        });
    }

    @Override
    public void closeWindow() {

    }
}
