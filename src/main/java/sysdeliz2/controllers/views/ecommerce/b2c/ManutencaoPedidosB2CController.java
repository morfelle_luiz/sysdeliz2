package sysdeliz2.controllers.views.ecommerce.b2c;

import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdStatusPedidoB2C;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.view.VSdDadosPedidoB2C;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ManutencaoPedidosB2CController extends WindowBase {

    protected List<VSdDadosPedidoB2C> pedidos = new ArrayList<>();
    protected List<PedIten> itensPedido = new ArrayList<>();

    protected List<SdStatusPedidoB2C> statusList = (List<SdStatusPedidoB2C>) new FluentDao().selectFrom(SdStatusPedidoB2C.class).get().resultList();

    public ManutencaoPedidosB2CController(String title, Image icone) {
        super(title, icone);
    }

    protected void buscarPedidos(Object[] marcas, Object[] clientes, String codRastreio, Object[] status, String numeroPed, String numPedErp) {
        if (pedidos.size() > 0) JPAUtils.clearEntitys(pedidos);

        pedidos = (List<VSdDadosPedidoB2C>) new FluentDao().selectFrom(VSdDadosPedidoB2C.class).where(it -> it
                .isIn("marca", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                .isIn("codcli", clientes, TipoExpressao.AND, when -> clientes.length > 0)
                .isIn("codStatus.codigo", status, TipoExpressao.AND, when -> status.length > 0)
                .like("codrastreio", codRastreio, TipoExpressao.AND, when -> !codRastreio.equals(""))
                .isIn("numeroerp", numPedErp.split(","), TipoExpressao.AND, when -> !numPedErp.equals(""))
                .isIn(
                        "numero.numero", numeroPed.split(","), TipoExpressao.AND, when -> !numeroPed.equals(""))
        ).orderBy("numero.numero", OrderType.DESC).resultList();
    }

    protected void buscarItensPedido(VSdDadosPedidoB2C newValue) {
        itensPedido = (List<PedIten>) new FluentDao().selectFrom(PedIten.class).where(it -> it.equal("id.numero", newValue.getNumeroerp().getNumero())).resultList();
    }

    protected SdStatusPedidoB2C getStatusById(int codigo) {
        return statusList.stream().filter(it -> it.getCodigo() == codigo).findFirst().get();
    }

    @Override
    public void closeWindow() {

    }
}
