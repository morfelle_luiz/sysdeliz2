package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.*;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedido;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.view.*;
import sysdeliz2.models.view.expedicao.VSdResGradeRefCor;
import sysdeliz2.models.view.expedicao.VSdResPedidoCliente;
import sysdeliz2.models.view.expedicao.VSdResRefCorPedido;
import sysdeliz2.models.view.expedicao.VSdReservasAlocadas;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class AnaliseRemessasPedidoController extends WindowBase {

    protected List<VSdReservasAlocadas> reservas = new ArrayList<>();
    protected List<SdNumeroCaixa> caixas = new ArrayList<>();
    protected SdStatusRemessa statusCancelado = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "Z")).singleResult();

    protected SdParametros _LIBERA_IMPRESSAO = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", 2)).singleResult();

    protected SdStatusRemessa _STATUS_IMPRESSAO = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "I")).singleResult();
    protected SdStatusRemessa _STATUS_TABLETS = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "X")).singleResult();
    protected SdStatusRemessa _STATUS_REVISAO = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "J")).singleResult();

    private TempFormGetReservas formGetReservas = new TempFormGetReservas();

    public AnaliseRemessasPedidoController(String title, Image icone) {
        super(title, icone, new String[]{"Remessas", "Gerador Caixa"});
    }

    @Override
    public void closeWindow() {

    }

    protected void getReservas() {
//        getReservas("", new String[]{}, LocalDate.of(1980, 1, 1), LocalDate.of(2050, 12, 31), new String[]{"L"},
//                "ambos", "ambos", "ambos", "ambos", new String[]{}, new Object[]{});
    }

    protected void getReservas(String remessa, Object[] cliente, LocalDate inicio, LocalDate fim,
                               Object[] status, String tipoReserva, String reservaBloqueada,
                               String reserva, String prioridade, Object[] somenteNaoFaturadas,
                               Object[] estados, Integer minPecas, Integer maxPecas, String emissor) throws EntityNotFoundException {
        Integer minimoPecas = minPecas == null ? 0 : minPecas;
        Integer maximoPecas = maxPecas == null ? 9999 : maxPecas;
        String pEmissor = emissor == null ? "" : emissor;

        if (cliente.length > 0)
            if (new FluentDao().selectFrom(VSdDadosCliente.class).where(eb -> eb.isIn("codcli", cliente)).resultList().size() != cliente.length)
                throw new EntityNotFoundException("Um dos clientes filtrados não encontrado...");

        formGetReservas.setForm(remessa, cliente, inicio, fim, status, tipoReserva, reservaBloqueada, reserva,
                prioridade, somenteNaoFaturadas, estados, minimoPecas, maximoPecas, pEmissor);
        JPAUtils.clearEntitys(reservas);
        String pRemessa = remessa == null ? "" : remessa;
        reservas = (List<VSdReservasAlocadas>) new FluentDao().selectFrom(VSdReservasAlocadas.class)
                .where(it -> it
                        .equal("id.remessa", pRemessa, TipoExpressao.AND, b -> pRemessa.length() > 0)
                        .between("qtde", minimoPecas, maximoPecas)
                        .isIn("id.codcli.codcli", cliente, TipoExpressao.AND, b -> cliente.length > 0)
                        .between("dtentrega", inicio, fim)
                        .like("emissor", pEmissor.toLowerCase(), TipoExpressao.AND, b -> pEmissor.length() > 0)
                        .isIn("id.resstatus.codigo", status, TipoExpressao.AND, b -> status.length > 0)
                        .isIn("id.codcli.codCid.codEst.sigla", estados, TipoExpressao.AND, b -> estados.length > 0)
                        .notIn("id.resstatus.codigo", somenteNaoFaturadas, TipoExpressao.AND, b -> somenteNaoFaturadas.length > 0)
                        .equal("resautomatica", Boolean.valueOf(tipoReserva), TipoExpressao.AND, b -> !tipoReserva.equals("ambos"))
                        .equal("id.resstatus.reserva", Boolean.valueOf(reserva), TipoExpressao.AND, b -> !reserva.equals("ambos"))
                        .equal("prioridadecoleta", "999", TipoExpressao.AND, b -> prioridade.equals("false"))
                        .notEqual("prioridadecoleta", "999", TipoExpressao.AND, b -> prioridade.equals("true"))
                        .equal("reslocked", Boolean.valueOf(reservaBloqueada), TipoExpressao.AND, b -> !reservaBloqueada.equals("ambos")))
                .orderBy(Arrays.asList(new Ordenacao("prioridadecoleta"),
                        new Ordenacao("dtentrega")))
                .resultList();
    }

    protected void setReservaBloqueada(VSdReservasAlocadas reserva, Boolean status) throws SQLException {
        for (VSdResPedidoCliente pedido : reserva.getPedidos()) {
            new NativeDAO().runNativeQueryUpdate(String.format("" +
                            "update ped_reserva_001 res\n" +
                            "   set res.res_locked = '%s'\n" +
                            " where res.numero = '%s'\n" +
                            "   and res.res_status = '%s'",
                    (status ? "S" : "N"),
                    pedido.getId().getNumero().getNumero(),
                    reserva.getId().getResstatus().getCodigo()));

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(pedido.getId().getNumero().getNumero()),
                    "Bloqueando reserva do pedido");
        }
    }

    protected void liberarRemessaEmRevisao(VSdReservasAlocadas reserva) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update sd_remessa_cliente_002 set status = 'X' where remessa = '%s'", reserva.getId().getRemessa());
    }

    protected void alterarStatusRemessa(VSdReservasAlocadas remessa, SdStatusRemessa status) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update sd_remessa_cliente_001 set status = '%s' where remessa = %s", status.getCodigo(), remessa.getId().getRemessa());
    }

    protected void adicionarObservacaoRemessa(VSdReservasAlocadas remessa) throws SQLException, FormValidationException {
        SdStatusRemessa statusValidacao = confirmaStatusRemessa(remessa);
        if (statusValidacao.getCodigo().equals(remessa.getId().getResstatus().getCodigo()))
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set observacao = '%s' where remessa = '%s'", remessa.getObservacao(), remessa.getId().getRemessa()));
        else {
            remessa.getId().setResstatus(statusValidacao);
            throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        }

    }

    protected void adicionarObservacaoItem(ItensTreeView itemSelecionado, String observacao) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_pedido_remessa_001 set observacao = '%s' where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                observacao, itemSelecionado.remessa.get(), itemSelecionado.numero.get(), itemSelecionado.codigo.get().getCodigo(), itemSelecionado.cor.get().getCor()));
    }

    protected SdStatusRemessa confirmaStatusRemessa(VSdReservasAlocadas remessa) throws SQLException {
        List<Object> remessas = (List<Object>) new NativeDAO().runNativeQuery(String.format("select * from sd_remessa_cliente_001 where remessa = '%s'", remessa.getId().getRemessa()));
        if (remessas.size() > 0) {
            Map<String, Object> remessaAtualizada = (Map<String, Object>) remessas.get(0);
            if (((String) remessaAtualizada.get("STATUS")) == null)
                return remessa.getId().getResstatus();
            if (!((String) remessaAtualizada.get("STATUS")).equals(remessa.getId().getResstatus().getCodigo())) {
                return (SdStatusRemessa) new FluentDao().selectFrom(SdStatusRemessa.class).where(it -> it.equal("codigo", ((String) remessaAtualizada.get("STATUS")))).singleResult();
            }
        }
        return remessa.getId().getResstatus();
    }

    private List<VSdResRefCorPedido> getMarketingPedido(VSdResPedidoCliente numero, Integer remessa) throws SQLException {
        List<VSdResRefCorPedido> produtosMkt = new ArrayList<>();

        // verifica se o pedido roda regra de MKT
        if (numero.getId().getNumero().getTabPre().isUsaMkt()) {
            List<VSdMktPedido> mktsPedido = (List<VSdMktPedido>) new FluentDao().selectFrom(VSdMktPedido.class)
                    .where(it -> it
                            .equal("numero", numero.getId().getNumero().getNumero())
                            .greaterThan("qtde", 0))
                    .resultList();
            mktsPedido.forEach(mkt -> {
                VSdResRefCorPedido refCor = new VSdResRefCorPedido(numero, mkt.getCodigo(), mkt.getCor(), "P", LocalDate.now(), mkt.getMarca(), mkt.getQtde(), 0, BigDecimal.ZERO, "MKT");
                VSdResGradeRefCor gradeRef = new VSdResGradeRefCor(refCor, mkt.getTam(), "P", LocalDate.now(), mkt.getMarca(), mkt.getQtde(), 0, BigDecimal.ZERO, 0, "MKT", BigDecimal.ZERO);
                refCor.setGrade(Arrays.asList(gradeRef));
                produtosMkt.add(refCor);
            });
        }

        // verifica se existem marketings cadastrados para o pedido
        List<SdMktPedido> mktsPedido = (List<SdMktPedido>) new FluentDao().selectFrom(SdMktPedido.class)
                .where(it -> it
                        .equal("id.pedido", numero.getId().getNumero().getNumero())
                        .equal("id.reserva", "0"))
                .resultList();
        if (!mktsPedido.isEmpty()) {
            Cor corUn = (Cor) new FluentDao().selectFrom(Cor.class).where(it -> it.equal("cor", "UN")).singleResult();
            for (SdMktPedido mkt : mktsPedido) {
                VSdResRefCorPedido refCor = new VSdResRefCorPedido(numero, mkt.getId().getCodigo(), corUn, "P", LocalDate.now(), mkt.getId().getCodigo().getCodMarca(), mkt.getQtde(), 0, BigDecimal.ZERO, "MKT");
                VSdResGradeRefCor gradeRef = new VSdResGradeRefCor(refCor, "UN", "P", LocalDate.now(), mkt.getId().getCodigo().getCodMarca(), mkt.getQtde(), 0, BigDecimal.ZERO, 0, "MKT", BigDecimal.ZERO);
                refCor.setGrade(Arrays.asList(gradeRef));
                produtosMkt.add(refCor);
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_mkt_pedido_001 set reserva = '%s' where reserva = '0' and pedido = '%s' and codigo = '%s'",
                        String.valueOf(remessa), numero.getId().getNumero().getNumero(), mkt.getId().getCodigo().getCodigo()));
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_mkt_pedido_001 set reserva = '%s' where reserva = '0' and pedido = '%s' and codigo = '%s'",
                        String.valueOf(remessa), numero.getId().getNumero().getNumero(), mkt.getId().getCodigo().getCodigo()));
            }
        }

        return produtosMkt;
    }

    protected void refreshGetReservas() {
        getReservas(formGetReservas.remessa, formGetReservas.cliente, formGetReservas.inicio, formGetReservas.fim, formGetReservas.status, formGetReservas.tipoReserva,
                formGetReservas.reservaBloqueada, formGetReservas.reserva, formGetReservas.prioridade, formGetReservas.somenteNaoFaturadas, formGetReservas.estados,
                formGetReservas.minPecas, formGetReservas.maxPecas, formGetReservas.emissor);
    }

    protected List<Object> getProdutosDuplicados(String pedido) throws SQLException {
        return new NativeDAO().runNativeQuery("" +
                "select numero, codigo, cor, tam, dups\n" +
                "  from (select ped.numero, res.codigo, res.cor, res.tam, count(*) dups\n" +
                "          from pedido_001 ped\n" +
                "          join ped_reserva_001 res\n" +
                "            on res.numero = ped.numero\n" +
                "          join ped_iten_001 pei\n" +
                "            on pei.numero = ped.numero\n" +
                "           and pei.codigo = res.codigo\n" +
                "           and pei.cor = res.cor\n" +
                "           and pei.tam = res.tam\n" +
                "         where ped.numero in ('%s')\n" +
                "         group by ped.numero, res.codigo, res.cor, res.tam\n" +
                "        union\n" +
                "        select ped.numero, res.codigo, 'ORDEM', 'DUP', count(distinct ordem) dups\n" +
                "          from pedido_001 ped\n" +
                "          join ped_reserva_001 res\n" +
                "            on res.numero = ped.numero\n" +
                "          join ped_iten_001 pei\n" +
                "            on pei.numero = ped.numero\n" +
                "           and pei.codigo = res.codigo\n" +
                "           and pei.cor = res.cor\n" +
                "           and pei.tam = res.tam\n" +
                "         where ped.numero in ('%s')\n" +
                "         group by ped.numero, res.codigo\n" +
                "        union\n" +
                "        select ped.numero, res.codigo, 'PREÇO', 'DUP', count(distinct preco) dups\n" +
                "          from pedido_001 ped\n" +
                "          join ped_reserva_001 res\n" +
                "            on res.numero = ped.numero\n" +
                "          join ped_iten_001 pei\n" +
                "            on pei.numero = ped.numero\n" +
                "           and pei.codigo = res.codigo\n" +
                "           and pei.cor = res.cor\n" +
                "           and pei.tam = res.tam\n" +
                "         where ped.numero in ('%s')\n" +
                "         group by ped.numero, res.codigo\n" +
                "        union\n" +
                "        select ped.numero, mkt.codigo, 'UN', 'UN', count(*)\n" +
                "          from pedido_001 ped\n" +
                "          join sd_mkt_pedido_001 mkt\n" +
                "            on mkt.pedido = ped.numero\n" +
                "           and mkt.reserva = 0\n" +
                "         where ped.numero in ('%s')\n" +
                "         group by ped.numero, mkt.codigo)\n" +
                " where dups > 1", pedido, pedido, pedido, pedido);
    }

    protected Map<String, Object> getColetaDupla(String remessa) throws SQLException {

        List<Object> coletaDupla = new NativeDAO().runNativeQuery(
                "select case when sum(case when pai.local like '1%%' then 1 else 0 end) > 0 and\n" +
                        "                 sum(case when pai.local like '0%%' then 1 else 0 end) > 0 \n" +
                        "            then 'S' else 'N' end coleta_dupla,\n" +
                        "       case when sum(case when pai.local like '1%%' then 1 else 0 end) > 0 then 'S' else 'N' end piso1\n" +
                        "  from sd_grade_item_ped_rem_001 ite\n" +
                        "  left join pa_iten_001 pai\n" +
                        "    on pai.codigo = ite.codigo\n" +
                        "   and pai.cor = ite.cor\n" +
                        "   and pai.tam = ite.tam\n" +
                        "   and pai.deposito = '0005'\n" +
                        " where ite.remessa = '%s'\n" +
                        "   and pai.local is not null", remessa);

        return (Map<String, Object>) coletaDupla.get(0);
    }

    protected SdRemessaCliente criarRemessa(VSdReservasAlocadas reserva, SdStatusRemessa novoStatus) throws SQLException, FormValidationException {
        SdStatusRemessa status = confirmaStatusRemessa(reserva);

        if (status.getCodigo().equals(reserva.getId().getResstatus().getCodigo())) {
            // criando o cabeçalho da remessa
            reserva.refresh();
            int countItensRemessa = 0;
            JPAUtils.getEntityManager().refresh(reserva.getId().getCodcli());
            SdRemessaCliente remessa = new FluentDao().persist(new SdRemessaCliente(_STATUS_REVISAO,
                    reserva.isResautomatica(),
                    reserva.getId().getCodcli(),
                    reserva.getDtentrega(),
                    reserva.getQtde(),
                    reserva.getValor(),
                    reserva.getDeposito()));
            for (VSdResPedidoCliente vSdResPedidoCliente : reserva.getPedidos()) {
                // incluindo os pedidos da remessa
                SdPedidosRemessa pedidoRemessa = new SdPedidosRemessa(remessa.getRemessa(),
                        vSdResPedidoCliente.getId().getNumero(),
                        vSdResPedidoCliente.getMarca().getCodigo(),
                        vSdResPedidoCliente.getDtentrega(),
                        vSdResPedidoCliente.getQtde(),
                        vSdResPedidoCliente.getValor());
                new FluentDao().persist(pedidoRemessa);
                // get dos itens e mkts dos pedidos para a remessa
                List<VSdResRefCorPedido> prodsMkt = getMarketingPedido(vSdResPedidoCliente, remessa.getRemessa());
                List<VSdResRefCorPedido> produtosParaPersist = new ArrayList<>();
                produtosParaPersist.addAll(vSdResPedidoCliente.getItens());
                produtosParaPersist.addAll(prodsMkt);
                for (VSdResRefCorPedido item : produtosParaPersist) {
                    // incluindo os itens e mkts nos pedidos
                    SdItensPedidoRemessa itemPedido = new SdItensPedidoRemessa(remessa.getRemessa(),
                            vSdResPedidoCliente.getId().getNumero().getNumero(),
                            item.getId().getCodigo(),
                            item.getId().getCor().getCor(),
                            item.getStatusitem(),
                            item.getDtentrega(),
                            item.getQtde(),
                            item.getValor(),
                            item.getTipo(),
                            item.getOrdem());
                    new FluentDao().persist(itemPedido);
                    for (VSdResGradeRefCor grade : item.getGrade()) {
                        countItensRemessa++;
                        // incluindo a grade dos itens nos itens
                        SdGradeItemPedRem gradeItem = new SdGradeItemPedRem(remessa.getRemessa(),
                                vSdResPedidoCliente.getId().getNumero().getNumero(),
                                item.getId().getCodigo().getCodigo(),
                                item.getId().getCor().getCor(),
                                grade.getId().getTam(),
                                grade.getStatusitem(),
                                grade.getDtentrega(),
                                grade.getQtde(),
                                grade.getValor(),
                                grade.getTipo(),
                                grade.getPosicao(),
                                grade.getDesconto());
                        new FluentDao().persist(gradeItem);
                    }
                }
            }
            if (countItensRemessa > 0) {
                Map<String, Object> coletaDupla = getColetaDupla(remessa.getRemessa().toString());
                remessa.setPiso(((String) coletaDupla.get("PISO1")).equals("S") ? 1 : 0);
                remessa.setColetaDupla(((String) coletaDupla.get("COLETA_DUPLA")).equals("S"));

                remessa.setStatus(novoStatus);
                remessa = new FluentDao().merge(remessa);

                // atualizando a ped_reserva para colocar o número da remessa no pedido
                for (VSdResPedidoCliente pedido : reserva.getPedidos()) {
                    new NativeDAO().runNativeQueryUpdate(String.format("update ped_reserva_001 set sd_reserva = '%s', res_status = '%s' where numero = '%s' and res_status = '%s' and deposito = '%s'",
                            remessa.getRemessa(), novoStatus.getCodigo(), pedido.getId().getNumero().getNumero(), reserva.getId().getResstatus().getCodigo(), reserva.getDeposito()));
                }

                // atualizando os totais da remessa
                atualizaTotaisRemessa(remessa);
            } else {
                SysLogger.addSysDelizLog("Análise de Remessas", TipoAcao.CADASTRAR, String.valueOf(remessa.getRemessa()),
                        "Validação para remessa " + (novoStatus.getCodigo().equals("I") ? "impressa" : "tablet") +
                                " de inclução sem itens.");

                new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_grade_item_ped_rem_001 where remessa = '%s'",
                        remessa.getRemessa()));
                new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_itens_pedido_remessa_001 where remessa = '%s'",
                        remessa.getRemessa()));
                new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_pedidos_remessa_001 where remessa = '%s'",
                        remessa.getRemessa()));
                new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_remessa_cliente_001 where remessa = '%s'",
                        remessa.getRemessa()));

                throw new FormValidationException("Foi verificado um problema na criação da remessa. " +
                        "Não foi possível incluir itens na remessa. " +
                        "Verifique com o setor de TI para a solução do problema. " +
                        "A reserva está sendo liberada para análise.");
            }
            SysLogger.addSysDelizLog("Análise de Remessas", TipoAcao.CADASTRAR, String.valueOf(remessa.getRemessa()),
                    "Criando remessa e " + (novoStatus.getCodigo().equals("I") ? "imprimindo" : "enviando") +
                            " do cliente "+ remessa.getCodcli().getCodcli() +
                            " nos pedidos "+ remessa.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getNumero()).distinct().collect(Collectors.joining(",")) +
                            " para coleta");
            return remessa;
        } else {
            throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        }
    }

    private void atualizaTotaisRemessa(SdRemessaCliente remessa) throws SQLException {
        remessa.refresh();
        for (SdPedidosRemessa pedido : remessa.getPedidos()) {
            new NativeDAO().runNativeQueryUpdate("" +
                    "update sd_pedidos_remessa_001 ped\n" +
                    "   set ped.qtde  =\n" +
                    "       nvl((select sum(it.qtde)\n" +
                    "          from sd_itens_pedido_remessa_001 it\n" +
                    "         where it.remessa = ped.remessa\n" +
                    "           and it.numero = ped.numero),0),\n" +
                    "       ped.qtde_c =\n" +
                    "       nvl((select sum(it.qtde_c)\n" +
                    "          from sd_itens_pedido_remessa_001 it\n" +
                    "         where it.remessa = ped.remessa\n" +
                    "           and it.numero = ped.numero),0)\n" +
                    " where ped.numero = '%s'\n" +
                    "   and ped.remessa = '%s'", pedido.getId().getNumero().getNumero(), remessa.getRemessa());
        }
        new NativeDAO().runNativeQueryUpdate("" +
                "update sd_remessa_cliente_001 rem\n" +
                "   set rem.qtde  =\n" +
                "       nvl((select sum(ped.qtde)\n" +
                "          from sd_pedidos_remessa_001 ped\n" +
                "         where rem.remessa = ped.remessa),0),\n" +
                "       rem.qtde_c =\n" +
                "       nvl((select sum(ped.qtde_c)\n" +
                "          from sd_pedidos_remessa_001 ped\n" +
                "         where rem.remessa = ped.remessa),0)\n" +
                " where rem.remessa = '%s'", remessa.getRemessa());
    }

    protected void imprimirRemessa(VSdReservasAlocadas reserva) throws SQLException, JRException, FormValidationException, IOException {
        //SdStatusRemessa status = confirmaStatusRemessa(reserva);

        //if (status.getCodigo().equals(reserva.getResstatus().getCodigo())) {
        new ReportUtils()
                .config().addReport(ReportUtils.ReportFile.ROMANEIO_COLETA_REMESSA, new ReportUtils.ParameterReport("pRemessa", reserva.getId().getRemessa()), new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                .view().print();
        //} else {
        //    reserva.setResstatus(status);
        //    throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        //}
    }

    protected void excluirRemessa(VSdReservasAlocadas remessa) throws SQLException, FormValidationException {
        SdStatusRemessa status = confirmaStatusRemessa(remessa);

        if (status.getCodigo().equals(remessa.getId().getResstatus().getCodigo())) {
            for (VSdResPedidoCliente pedido : remessa.getPedidos()) {
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_mkt_pedido_001 set reserva = '0' where reserva = '%s' and pedido = '%s'", remessa.getId().getRemessa(), pedido.getId().getNumero().getNumero()));
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_mkt_pedido_001 set reserva = '0' where reserva = '%s' and pedido = '%s'", remessa.getId().getRemessa(), pedido.getId().getNumero().getNumero()));
            }
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_grade_item_ped_rem_001 where remessa = '%s'", remessa.getId().getRemessa()));
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_itens_pedido_remessa_001 where remessa = '%s'", remessa.getId().getRemessa()));
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_pedidos_remessa_001 where remessa = '%s'", remessa.getId().getRemessa()));
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_remessa_cliente_001 where remessa = '%s'", remessa.getId().getRemessa()));
            new NativeDAO().runNativeQueryUpdate(String.format("update ped_reserva_001 set sd_reserva = '', res_status = decode(res_automatica, 'N', 'M', 'L') where sd_reserva = '%s'", remessa.getId().getRemessa()));

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessa.getId().getRemessa()),
                    "Excluído a remessa de coleta");
        } else {
            remessa.getId().setResstatus(status);
            throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        }

        remessa.getId().setResstatus(status);
    }

    protected void migrarStatusRemessa(VSdReservasAlocadas remessa, String novoStatus) throws SQLException, FormValidationException {
        SdStatusRemessa status = confirmaStatusRemessa(remessa);

        if (status.getCodigo().equals(remessa.getId().getResstatus().getCodigo())) {

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EDITAR, String.valueOf(remessa.getId().getRemessa()),
                    "Alterando status da remessa de " + remessa.getId().getResstatus().getCodigo() + " para " + novoStatus);
            status = (SdStatusRemessa) new FluentDao().selectFrom(SdStatusRemessa.class).where(it -> it.equal("codigo", novoStatus)).singleResult();
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set status = '%s' where remessa = '%s'", novoStatus, remessa.getId().getRemessa()));
            new NativeDAO().runNativeQueryUpdate(String.format("update ped_reserva_001 set res_status = '%s' where sd_reserva = '%s'", novoStatus, remessa.getId().getRemessa()));
        } else {
            remessa.getId().setResstatus(status);
            throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        }

        remessa.getId().setResstatus(status);
    }

    protected void priorizarColeta(VSdReservasAlocadas remessa, String prioridade) throws SQLException {
        SdStatusRemessa status = confirmaStatusRemessa(remessa);

        if (status.getCodigo().equals(remessa.getId().getResstatus().getCodigo())) {
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set ordem_coleta = %s where remessa = '%s'", prioridade, remessa.getId().getRemessa()));

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EDITAR, String.valueOf(remessa.getId().getRemessa()),
                    "Alterando prioridade da remessa para " + prioridade);
        } else {
            remessa.getId().setResstatus(status);
            throw new FormValidationException("Status da remessa diferente da tela, atualizando status...");
        }

        remessa.getId().setResstatus(status);
    }

    protected void excluirItemReserva(TreeItem<ItensTreeView> itemSelecionado, VSdReservasAlocadas reservaSelecionada, VSdResPedidoCliente pedidoSelecionado) throws SQLException {
        // verifica se o a remoção é do produto/cor ou sku
        if (itemSelecionado.getValue().tam.get().equals("-")) {
            // remover da ped_reserva by numero/codigo/cor
            new NativeDAO().runNativeQueryUpdate(String.format("delete ped_reserva_001 where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and deposito = '%s'",
                    pedidoSelecionado.getId().getNumero().getNumero(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor(), reservaSelecionada.getDeposito()));
            reservaSelecionada.getPedidos().stream()
                    .filter(it -> it.getId().getNumero().getNumero().equals(pedidoSelecionado.getId().getNumero().getNumero()))
                    .forEach(it -> it.getItens().removeIf(it2 -> it2.getId().getCodigo().getCodigo().equals(itemSelecionado.getValue().codigo.get().getCodigo()) && it2.getId().getCor().getCor().equals(itemSelecionado.getValue().cor.get().getCor())));
        } else {
            // remover da ped_reserva by numero/codigo/cor/tam
            new NativeDAO().runNativeQueryUpdate(String.format("delete ped_reserva_001 where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                    pedidoSelecionado.getId().getNumero().getNumero(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor(), itemSelecionado.getValue().tam.get(), reservaSelecionada.getDeposito()));
            reservaSelecionada.getPedidos().stream()
                    .filter(it -> it.getId().getNumero().equals(pedidoSelecionado.getId().getNumero()))
                    .forEach(it -> it.getItens().stream()
                            .filter(it2 -> it2.getId().getCodigo().getCodigo().equals(itemSelecionado.getValue().codigo.get().getCodigo()) && it2.getId().getCor().getCor().equals(itemSelecionado.getValue().cor.get().getCor()))
                            .forEach(it2 -> {
                                it2.getGrade().removeIf(it3 -> it3.getId().getTam().equals(itemSelecionado.getValue().tam.get()));
                            }));

        }
        // atualização de qtde e valor dos pedidos/cliente
        reservaSelecionada.getPedidos()
                .forEach(pedidoCliente -> {
                    pedidoCliente.getItens().forEach(produtoCor -> {
                        produtoCor.setQtde(produtoCor.getGrade().stream().mapToInt(VSdResGradeRefCor::getQtde).sum());
                        produtoCor.setValor(new BigDecimal(produtoCor.getGrade().stream().mapToDouble(grade -> grade.getValor().doubleValue()).sum()));
                        produtoCor.refresh();
                    });
                    pedidoCliente.setQtde(pedidoCliente.getItens().stream().mapToInt(VSdResRefCorPedido::getQtde).sum());
                    pedidoCliente.setValor(new BigDecimal(pedidoCliente.getItens().stream().mapToDouble(produtoCor -> produtoCor.getValor().doubleValue()).sum()));
                    pedidoCliente.refresh();
                });
        reservaSelecionada.setQtde(reservaSelecionada.getPedidos().stream().mapToInt(VSdResPedidoCliente::getQtde).sum());
        reservaSelecionada.setValor(new BigDecimal(reservaSelecionada.getPedidos().stream().mapToDouble(pedidos -> pedidos.getValor().doubleValue()).sum()));
        reservaSelecionada.refresh();

        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(pedidoSelecionado.getId().getNumero().getNumero()),
                "Excluído reserva do item " + itemSelecionado.getValue().codigo.get().getCodigo() + " na cor " + itemSelecionado.getValue().cor.get().getCor() + " e tamanho " + itemSelecionado.getValue().numero);

        // excluindo da treetableview
        itemSelecionado.getParent().getChildren().remove(itemSelecionado);
    }

    protected void excluirGradeReserva(VSdResGradeRefCor gradeItem, VSdReservasAlocadas reservaSelecionada) throws SQLException {
        // Excluindo item da PED_RESERVA para liberar o SKU (caso for MKT, remove do SD_MKT_PEDIDO caso tenha sido um MKT cadastrado pelo comercial)
        if (!gradeItem.getTipo().equals("MKT")) {
            new NativeDAO().runNativeQueryUpdate("update ped_reserva_001 set sd_reserva = '' where numero = '%s' and sd_reserva = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                    gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                    gradeItem.getId().getTam());
            new NativeDAO().runNativeQueryUpdate("delete ped_reserva_001 where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and tam = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                    gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                    gradeItem.getId().getTam());
        } else {
            new NativeDAO().runNativeQueryUpdate("update sd_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo());
            new NativeDAO().runNativeQueryUpdate("update sd_itens_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo());
        }

        // Excluindo item da PED_RESERVA para liberar o SKU (caso for MKT, remove do SD_MKT_PEDIDO caso tenha sido um MKT cadastrado pelo comercial)
        new NativeDAO().runNativeQueryUpdate(String.format("update ped_reserva_001 set sd_reserva = '' where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                gradeItem.getId().getTam(), reservaSelecionada.getDeposito()));
        new NativeDAO().runNativeQueryUpdate(String.format("delete ped_reserva_001 where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                gradeItem.getId().getTam(), reservaSelecionada.getDeposito()));

    }

    protected void cancelarGradeRemessa(VSdResGradeRefCor gradeItem) throws SQLException {
        SysLogger.addSysDelizLog("Análise de Remessas", TipoAcao.EXCLUIR, gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                "Excluído da remessa o item " + gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo() +
                        " na cor " + gradeItem.getId().getCodigoCor().getId().getCor().getCor() + " tam " + gradeItem.getId().getTam() +
                        " do pedido " + gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero());

        // Excluindo item da PED_RESERVA para liberar o SKU (caso for MKT, remove do SD_MKT_PEDIDO caso tenha sido um MKT cadastrado pelo comercial)
        if (!gradeItem.getTipo().equals("MKT")) {
            new NativeDAO().runNativeQueryUpdate("update ped_reserva_001 set sd_reserva = '' where numero = '%s' and sd_reserva = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                    gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                    gradeItem.getId().getTam());
            new NativeDAO().runNativeQueryUpdate("delete ped_reserva_001 where numero = '%s' and sd_reserva is null and codigo = '%s' and cor = '%s' and tam = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                    gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                    gradeItem.getId().getTam());
        } else {
            new NativeDAO().runNativeQueryUpdate("update sd_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo());
            new NativeDAO().runNativeQueryUpdate("update sd_itens_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                    gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                    gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo());
        }

        // Cancelando o SKU na remessa (SD_GRADE_ITEM_PED_REM)
        new NativeDAO().runNativeQueryUpdate("update sd_grade_item_ped_rem_001 set qtde_c = qtde_c + qtde, qtde = 0, status_item = 'X' where remessa  = '%s' and numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                gradeItem.getId().getCodigoCor().getId().getCor().getCor(),
                gradeItem.getId().getTam());

        // Atualizando totais do ITEM do pedido (SD_ITENS_PEDIDO_REMESSA)
        new NativeDAO().runNativeQueryUpdate("" +
                "update sd_itens_pedido_remessa_001 item\n" +
                "   set item.qtde = (select sum(sku.qtde) from sd_grade_item_ped_rem_001 sku where sku.remessa = item.remessa and sku.numero = item.numero and sku.codigo = item.codigo and sku.cor = item.cor), \n" +
                "       item.qtde_c = (select sum(sku.qtde_c) from sd_grade_item_ped_rem_001 sku where sku.remessa = item.remessa and sku.numero = item.numero and sku.codigo = item.codigo and sku.cor = item.cor), \n" +
                "       item.status_item = (select min(sku.status_item) from sd_grade_item_ped_rem_001 sku where sku.remessa = item.remessa and sku.numero = item.numero and sku.codigo = item.codigo and sku.cor = item.cor)\n" +
                " where item.remessa = '%s'\n" +
                "   and item.numero = '%s'\n" +
                "   and item.codigo = '%s'\n" +
                "   and item.cor = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero(),
                gradeItem.getId().getCodigoCor().getId().getCodigo().getCodigo(),
                gradeItem.getId().getCodigoCor().getId().getCor().getCor());

        // Atualizando totais do pedido (SD_PEDIDOS_REMESSA)
        new NativeDAO().runNativeQueryUpdate("" +
                "update sd_pedidos_remessa_001 ped\n" +
                "   set ped.qtde = (select sum(item.qtde) from sd_itens_pedido_remessa_001 item where item.remessa = ped.remessa and item.numero = ped.numero), \n" +
                "       ped.qtde_c = (select sum(item.qtde_c) from sd_itens_pedido_remessa_001 item where item.remessa = ped.remessa and item.numero = ped.numero)\n" +
                " where ped.remessa = '%s'\n" +
                "   and ped.numero = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa(),
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getNumero().getNumero());

        // Atualizando totais da remessa (SD_REMESSA_CLIENTE)
        new NativeDAO().runNativeQueryUpdate("" +
                "update sd_remessa_cliente_001 rem\n" +
                "   set rem.qtde = (select sum(ped.qtde) from sd_pedidos_remessa_001 ped where ped.remessa = rem.remessa), \n" +
                "       rem.qtde_c = (select sum(ped.qtde_c) from sd_pedidos_remessa_001 ped where ped.remessa = rem.remessa),\n" +
                "       rem.status = (select case when sum(ped.qtde) = 0 then 'Z' else min((select irem.status from sd_remessa_cliente_001 irem where irem.remessa = rem.remessa)) end \n" +
                "                       from sd_pedidos_remessa_001 ped where ped.remessa = rem.remessa)\n" +
                " where rem.remessa = '%s'",
                gradeItem.getId().getCodigoCor().getId().getPedido().getId().getRemessa().getId().getRemessa());

    }

    protected void cancelarItemRemessa(TreeItem<ItensTreeView> itemSelecionado, VSdReservasAlocadas reservaSelecionada, VSdResPedidoCliente pedidoSelecionado) throws SQLException {
        // verifica se o a remoção é do produto/cor ou sku
        if (itemSelecionado.getValue().tam.get().equals("-")) {
            // cancelar da sd_grade_item_ped_rem by remessa/numero/codigo/cor

            // verificando se é produto COM (deleta a PED_RESERVA) ou MKT (update na SD_MKT_PEDIDO caso o mkt é um cadastrado no pedido)
            if (itemSelecionado.getValue().tipo.get().equals("COM"))
                new NativeDAO().runNativeQueryUpdate(String.format("delete ped_reserva_001 where numero = '%s' and sd_reserva = '%s' and codigo = '%s' and cor = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor()));
            else {
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo()));
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo()));
            }

            // cancelando qtde do item na sd_grade_item_ped_rem_001
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_grade_item_ped_rem_001 set qtde_c = qtde_c + qtde, qtde = 0, status_item = 'X' where remessa  = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                    reservaSelecionada.getId().getRemessa(), pedidoSelecionado.getId().getNumero().getNumero(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor()));

            // atualizando dados do objeto e arrays
            reservaSelecionada.getPedidos().stream()
                    .filter(it -> it.getId().getNumero().getNumero().equals(pedidoSelecionado.getId().getNumero().getNumero()))
                    .forEach(it -> it.getItens().stream()
                            .forEach(it2 -> it2.getGrade().stream()
                                    .filter(it3 -> it3.getId().getCodigoCor().getId().getCodigo().getCodigo().equals(itemSelecionado.getValue().codigo.get().getCodigo())
                                            && it3.getId().getCodigoCor().getId().getCor().getCor().equals(itemSelecionado.getValue().cor.get().getCor()))
                                    .forEach(it3 -> {
                                        it3.setQtdec(it3.getQtdec() + it3.getQtde());
                                        it3.setQtde(0);
                                        it3.setStatusitem("X");
                                    })));
            // atualizando dados do treeview
            itemSelecionado.getValue().qtdec.set(itemSelecionado.getValue().qtdec.get() + itemSelecionado.getValue().qtde.get());
            itemSelecionado.getValue().qtde.set(0);
            itemSelecionado.getChildren().forEach(subItem -> {
                subItem.getValue().qtdec.set(subItem.getValue().qtdec.get() + subItem.getValue().qtde.get());
                subItem.getValue().qtde.set(0);
            });
        } else {
            // cancelar da sd_grade_item_ped_rem by remessa/numero/codigo/cor/tam

            // verificando se é produto COM (deleta a PED_RESERVA) ou MKT (update na SD_MKT_PEDIDO caso o mkt é um cadastrado no pedido)
            if (itemSelecionado.getValue().tipo.get().equals("COM"))
                new NativeDAO().runNativeQueryUpdate(String.format("delete ped_reserva_001 where numero = '%s' and sd_reserva = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor(), itemSelecionado.getValue().tam.get()));
            else {
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo()));
                new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_mkt_pedido_001 set reserva = '0' where pedido = '%s' and reserva = '%s' and codigo = '%s'",
                        pedidoSelecionado.getId().getNumero().getNumero(), reservaSelecionada.getId().getRemessa(), itemSelecionado.getValue().codigo.get().getCodigo()));
            }

            // cancelando qtde do item na sd_grade_item_ped_rem_001
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_grade_item_ped_rem_001 set qtde_c = qtde_c + qtde, qtde = 0, status_item = 'X' where remessa  = '%s' and numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                    reservaSelecionada.getId().getRemessa(), pedidoSelecionado.getId().getNumero().getNumero(), itemSelecionado.getValue().codigo.get().getCodigo(), itemSelecionado.getValue().cor.get().getCor(), itemSelecionado.getValue().tam.get()));

            // atualizando dados do objeto e arrays
            reservaSelecionada.getPedidos().stream()
                    .filter(it -> it.getId().getNumero().getNumero().equals(pedidoSelecionado.getId().getNumero().getNumero()))
                    .forEach(it -> it.getItens().stream()
                            .forEach(it2 -> it2.getGrade().stream()
                                    .filter(it3 -> it3.getId().getCodigoCor().getId().getCodigo().getCodigo().equals(itemSelecionado.getValue().codigo.get().getCodigo())
                                            && it3.getId().getCodigoCor().getId().getCor().getCor().equals(itemSelecionado.getValue().cor.get().getCor())
                                            && it3.getId().getTam().equals(itemSelecionado.getValue().tam.get()))
                                    .forEach(it3 -> {
                                        it3.setQtdec(it3.getQtdec() + it3.getQtde());
                                        it3.setQtde(0);
                                        it3.setStatusitem("X");
                                    })));
            // atualizando dados do treeview
            itemSelecionado.getValue().qtdec.set(itemSelecionado.getValue().qtdec.get() + itemSelecionado.getValue().qtde.get());
            itemSelecionado.getValue().qtde.set(0);
        }

        // atualização de qtde e valor e qtdec dos pedidos/cliente
        reservaSelecionada.refresh();
        for (VSdResPedidoCliente pedidoCliente : reservaSelecionada.getPedidos()) {
            for (VSdResRefCorPedido produtoCor : pedidoCliente.getItens()) {
                produtoCor.setQtde(produtoCor.getGrade().stream().mapToInt(VSdResGradeRefCor::getQtde).sum());
                produtoCor.setQtdec(produtoCor.getGrade().stream().mapToInt(VSdResGradeRefCor::getQtdec).sum());
                produtoCor.setValor(new BigDecimal(produtoCor.getGrade().stream().mapToDouble(grade -> grade.getValor().doubleValue()).sum()));
                new NativeDAO().runNativeQueryUpdate(String.format(
                        "update sd_itens_pedido_remessa_001 set qtde_c = %s, qtde = %s, status_item = '%s' where remessa  = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                        produtoCor.getQtdec(),
                        produtoCor.getQtde(),
                        (produtoCor.getGrade().stream().mapToInt(VSdResGradeRefCor::getQtde).sum() == 0 ? "X" : produtoCor.getStatusitem()),
                        produtoCor.getId().getPedido().getId().getRemessa().getId().getRemessa(),
                        produtoCor.getId().getPedido().getId().getNumero().getNumero(),
                        produtoCor.getId().getCodigo().getCodigo(),
                        produtoCor.getId().getCor().getCor()));
            }
            pedidoCliente.refresh();
            pedidoCliente.setQtde(pedidoCliente.getItens().stream().mapToInt(VSdResRefCorPedido::getQtde).sum());
            pedidoCliente.setQtdec(pedidoCliente.getItens().stream().mapToInt(VSdResRefCorPedido::getQtdec).sum());
            pedidoCliente.setValor(new BigDecimal(pedidoCliente.getItens().stream().mapToDouble(produtoCor -> produtoCor.getValor().doubleValue()).sum()));
            new NativeDAO().runNativeQueryUpdate(String.format(
                    "update sd_pedidos_remessa_001 set qtde_c = %s, qtde = %s where remessa  = '%s' and numero = '%s'",
                    pedidoCliente.getQtdec(),
                    pedidoCliente.getQtde(),
                    pedidoCliente.getId().getRemessa().getId().getRemessa(),
                    pedidoCliente.getId().getNumero().getNumero()));
        }
        reservaSelecionada.setQtde(reservaSelecionada.getPedidos().stream().mapToInt(VSdResPedidoCliente::getQtde).sum());
        reservaSelecionada.setQtdec(reservaSelecionada.getPedidos().stream().mapToInt(VSdResPedidoCliente::getQtdec).sum());
        reservaSelecionada.setValor(new BigDecimal(reservaSelecionada.getPedidos().stream().mapToDouble(pedidos -> pedidos.getValor().doubleValue()).sum()));
        reservaSelecionada.getId().setResstatus(reservaSelecionada.getQtde().compareTo(0) == 0 ? statusCancelado : reservaSelecionada.getId().getResstatus());
        new NativeDAO().runNativeQueryUpdate(String.format(
                "update sd_remessa_cliente_001 set qtde_c = %s, qtde = %s, status = '%s' where remessa = '%s'",
                reservaSelecionada.getQtdec(),
                reservaSelecionada.getQtde(),
                (reservaSelecionada.getQtde() == 0 ? "Z" : reservaSelecionada.getId().getResstatus().getCodigo()),
                reservaSelecionada.getId().getRemessa()));

        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, reservaSelecionada.getId().getRemessa(),
                "Cancelado o item " + itemSelecionado.getValue().codigo.get().getCodigo() + " na cor " + itemSelecionado.getValue().cor.get().getCor() + " e tamanho " + itemSelecionado.getValue().numero);

    }

    protected void divideRemessa(VSdReservasAlocadas reservaOriginal, VSdResPedidoCliente pedidoOriginal) throws SQLException {
        // atualizando a remessa atual
        SdRemessaCliente remessa = new FluentDao().selectFrom(SdRemessaCliente.class)
                .where(eb -> eb.equal("remessa", reservaOriginal.getId().getRemessa()))
                .singleResult();
        SdPedidosRemessa pedidoRemessa = remessa.getPedidos().stream().filter(pedido -> pedido.getId().getNumero().getNumero().equals(pedidoOriginal.getId().getNumero().getNumero())).findFirst().get();
        List<SdItensPedidoRemessa> itensDoPedido = new ArrayList<>();
        pedidoRemessa.refresh();
        pedidoRemessa.getItens().forEach(item -> itensDoPedido.add(item));

        remessa.setQtde(remessa.getQtde() - pedidoRemessa.getQtde());
        remessa.setQtdec(remessa.getQtdec() - pedidoRemessa.getQtdec());
        remessa.setValor(remessa.getValor().subtract(pedidoRemessa.getValor()));

        // criando nova remessa
        SdRemessaCliente novaRemessa = new FluentDao().persist(new SdRemessaCliente(remessa.getStatus(), remessa.isAutomatica(), remessa.getCodcli(), remessa.getDtentrega(), pedidoRemessa.getQtde(), pedidoRemessa.getValor(), remessa.getDeposito()));
        SdPedidosRemessa pedidoNovaRemessa = new SdPedidosRemessa(novaRemessa.getRemessa(), pedidoRemessa.getId().getNumero(), pedidoRemessa.getMarca(), pedidoRemessa.getDtentrega(), pedidoRemessa.getQtde(), pedidoRemessa.getValor());
        pedidoNovaRemessa.setQtdec(pedidoRemessa.getQtdec());
        new FluentDao().persist(pedidoNovaRemessa);
        for (SdItensPedidoRemessa item : itensDoPedido) {
            SdItensPedidoRemessa itemPedido = new SdItensPedidoRemessa(novaRemessa.getRemessa(), item.getId().getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getStatusitem(), item.getDtentrega(), item.getQtde(), item.getValor(), item.getTipo(), item.getOrdem());
            itemPedido.setQtdec(item.getQtdec());
            new FluentDao().persist(itemPedido);
            for (SdGradeItemPedRem grade : item.getGrade()) {
                SdGradeItemPedRem gradeItem = new SdGradeItemPedRem(novaRemessa.getRemessa(), grade.getId().getNumero(), grade.getId().getCodigo(), grade.getId().getCor(), grade.getId().getTam(), grade.getStatusitem(), grade.getDtentrega(), grade.getQtde(), grade.getValor(), grade.getTipo(), grade.getOrdem(), grade.getDesconto());
                gradeItem.setQtdec(grade.getQtdec());
                new FluentDao().persist(gradeItem);
            }
        }

        // excluindo pedido da remessa atual
        pedidoRemessa.getItens().forEach(item -> {
            item.getGrade().forEach(grade -> new FluentDao().delete(grade));
            new FluentDao().delete(item);
        });
        new FluentDao().delete(pedidoRemessa);

        // atualizando entitymanager da reserva atual
//        reservaOriginal.refresh();

        // carregando a view da reserva atual para o entitymanager e o array de tela
//        VSdReservasAlocadas viewNovaRemessa = new FluentDao().selectFrom(VSdReservasAlocadas.class).where(eb -> eb.equal("remessa", novaRemessa.getRemessa())).singleResult();
//        if (viewNovaRemessa != null)
//            reservas.add(viewNovaRemessa);

        // update da sd_reserva na ped_reserva
        new NativeDAO().runNativeQueryUpdate(String.format("update ped_reserva_001 set sd_reserva = '%s' where numero = '%s' and sd_reserva = '%s'",
                novaRemessa.getRemessa(), pedidoOriginal.getId().getNumero().getNumero(), reservaOriginal.getId().getRemessa()));

        refreshGetReservas();
    }

    protected void incluirDataBase(VSdResPedidoCliente item, LocalDate dataBase) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update sd_pedidos_remessa_001 set dt_base = '%s' where remessa = '%s' and numero = '%s'",
                StringUtils.toDateFormat(dataBase), item.getId().getRemessa().getId().getRemessa(), item.getId().getNumero().getNumero());
    }

    protected void incluirDesconto(VSdResPedidoCliente item, BigDecimal desconto) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update sd_pedidos_remessa_001 set vlr_desc = %s where remessa = '%s' and numero = '%s'",
                desconto, item.getId().getRemessa().getId().getRemessa(), item.getId().getNumero().getNumero());
    }

    protected String getObservacaoItem(String remessa, String numero, String codigo, String cor) throws SQLException {

        List<Object> dadosItem = new NativeDAO().runNativeQuery("" +
                "select to_char(observacao) obs_item\n" +
                "  from sd_itens_pedido_remessa_001\n" +
                " where remessa = '%s'\n" +
                "   and numero = '%s'\n" +
                "   and codigo = '%s'\n" +
                "   and cor = '%s'", remessa, numero, codigo, cor);

        return (String) ((Map<String, Object>) dadosItem.get(0)).get("OBS_ITEM");

    }

    protected void adicionarObservacaoPedido(VSdResRefCorPedido pedido, String observacao) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_pedido_remessa_001 set observacao = observacao || ' ' || '%s' where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                observacao,
                pedido.getId().getPedido().getId().getRemessa().getId().getRemessa(),
                pedido.getId().getPedido().getId().getNumero().getNumero(),
                pedido.getId().getCodigo().getCodigo(),
                pedido.getId().getCor().getCor()));
    }

    protected Map<String, Object> getDadosAdicionaisCliente(VSdReservasAlocadas reserva) throws SQLException {
        AtomicReference<Object> faturasColecao = new AtomicReference<>(null);
        new NativeDAO().runNativeQuery(
                "select count(distinct nfe.dt_emissao) ent_total\n" +
                        "  from nota_001 nfe\n" +
                        "  join natureza_001 nat\n" +
                        "    on nat.natureza = nfe.natureza\n" +
                        "  join notaiten_001 nfi\n" +
                        "    on nfi.fatura = nfe.fatura\n" +
                        "  join pedido_001 ped\n" +
                        "    on ped.numero = nfi.pedido\n" +
                        "  join sd_pedido_001 sped\n" +
                        "    on sped.numero = ped.numero\n" +
                        " where nat.tipo = 'V'\n" +
                        "   and nat.tp_base = '1'\n" +
                        "   and nfe.impresso = 'S'\n" +
                        "   and nfe.codcli = '%s'\n" +
                        "   and f_sd_get_col_ped_marca(ped.dt_emissao, sped.marca) =\n" +
                        "       (select max(f_sd_get_col_ped_marca(uped.dt_emissao, sdped.marca)) colecao\n" +
                        "          from v_sd_res_pedido_cliente pedres\n" +
                        "          join pedido_001 uped\n" +
                        "            on uped.numero = pedres.numero\n" +
                        "          join sd_pedido_001 sdped\n" +
                        "            on sdped.numero = pedres.numero\n" +
                        "         where pedres.codcli = nfe.codcli\n" +
                        "           and pedres.numero in (select * from table(split('%s'))))\n" +
                        " group by f_sd_get_col_ped_marca(ped.dt_emissao, sped.marca)",
                reserva.getId().getCodcli().getCodcli(),
                reserva.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getNumero()).distinct().collect(Collectors.joining(","))).stream().findAny().ifPresent(sqlResult -> faturasColecao.set(sqlResult));

        AtomicReference<Object> faturasMes = new AtomicReference<>(null);
        new NativeDAO().runNativeQuery(String.format("" +
                        "select count(distinct nfe.dt_emissao) ent_total\n" +
                        "  from nota_001 nfe\n" +
                        "  join natureza_001 nat\n" +
                        "    on nat.natureza = nfe.natureza\n" +
                        " where nat.tipo = 'V'\n" +
                        "   and nat.tp_base = '1'\n" +
                        "   and nfe.impresso = 'S'\n" +
                        "   and to_char(nfe.dt_emissao, 'MM/YYYY') = to_char(sysdate, 'MM/YYYY')\n" +
                        "   and nfe.codcli = '%s'",
                reserva.getId().getCodcli().getCodcli())).stream().findFirst().ifPresent(sqlResult -> faturasMes.set(sqlResult));

        AtomicReference<Object> dtUltimaFatura = new AtomicReference<>(null);
        new NativeDAO().runNativeQuery(String.format("" +
                        "select to_char(max(nfe.dt_emissao),'DD/MM/YYYY') dt_fatura\n" +
                        "  from nota_001 nfe\n" +
                        "  join natureza_001 nat\n" +
                        "    on nat.natureza = nfe.natureza\n" +
                        " where nat.tipo = 'V'\n" +
                        "   and nat.tp_base = '1'\n" +
                        "   and nfe.impresso = 'S'\n" +
                        "   and nfe.codcli = '%s'",
                reserva.getId().getCodcli().getCodcli())).stream().findFirst().ifPresent(sqlResult -> dtUltimaFatura.set(sqlResult));

        Map<String, Object> dados = new HashMap<>();
        dados.put("ENT_TOTAL", faturasColecao.get() == null || ((Map<String, Object>) faturasColecao.get()).get("ENT_TOTAL") == null ? "-" : ((Map<String, Object>) faturasColecao.get()).get("ENT_TOTAL"));
        dados.put("ENT_MES", faturasMes.get() == null || ((Map<String, Object>) faturasMes.get()).get("ENT_TOTAL") == null ? "-" : ((Map<String, Object>) faturasMes.get()).get("ENT_TOTAL"));
        dados.put("DT_ULT_FAT", dtUltimaFatura.get() == null || ((Map<String, Object>) dtUltimaFatura.get()).get("DT_FATURA") == null ? "-" : ((Map<String, Object>) dtUltimaFatura.get()).get("DT_FATURA"));

        return dados;
    }

    protected void incluirMarcaPedido(VSdResPedidoCliente pedido, Marca marca) throws SQLException {
        new NativeDAO().runNativeQuery("update sd_pedido_001 set marca = '%s' where numero = '%s'",
                        marca.getCodigo(),
                        pedido.getId().getNumero().getNumero());
    }

    protected void getCaixas() {
        caixas = (List<SdNumeroCaixa>) new FluentDao().selectFrom(SdNumeroCaixa.class)
                .where(eb -> eb.equal("id.tipo", "E"))
                .orderBy("id.fim", OrderType.DESC)
                .resultList();
    }

    protected class ItensTreeView {

        public final StringProperty numero = new SimpleStringProperty();
        public final StringProperty remessa = new SimpleStringProperty();
        public final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
        public final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
        public final StringProperty tam = new SimpleStringProperty();
        public final ObjectProperty<LocalDate> dtEntrega = new SimpleObjectProperty<>();
        public final IntegerProperty qtde = new SimpleIntegerProperty();
        public final IntegerProperty qtdec = new SimpleIntegerProperty();
        public final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
        public final StringProperty tipo = new SimpleStringProperty();
        public final StringProperty status = new SimpleStringProperty();
        public final IntegerProperty faltaBarra = new SimpleIntegerProperty();
        public final StringProperty barra28 = new SimpleStringProperty();
        public final ObjectProperty<Object> object = new SimpleObjectProperty<>();

        public ItensTreeView() {
        }

        public ItensTreeView(String numero, String remessa, VSdDadosProduto codigo, Cor cor, String tam, LocalDate dtEntrega, Integer qtde, BigDecimal valor, Integer qtdec, String tipo, String status,
                             Object object, String barra28, Integer faltaBarra) {
            this.numero.set(numero);
            this.remessa.set(remessa);
            this.codigo.set(codigo);
            this.cor.set(cor);
            this.tam.set(tam);
            this.dtEntrega.set(dtEntrega);
            this.qtde.set(qtde);
            this.valor.set(valor);
            this.tipo.set(tipo);
            this.qtdec.set(qtdec);
            this.status.set(status);
            this.object.set(object);
            this.faltaBarra.set(faltaBarra);
            this.barra28.set(barra28);
        }
    }

    private class TempFormGetReservas {

        String remessa;
        Object[] cliente;
        LocalDate inicio;
        LocalDate fim;
        Object[] status;
        String tipoReserva;
        String reservaBloqueada;
        String reserva;
        String prioridade;
        Object[] somenteNaoFaturadas;
        Object[] estados;
        Integer minPecas;
        Integer maxPecas;
        String emissor;

        public TempFormGetReservas() {
        }

        public void setForm(String remessa, Object[] cliente, LocalDate inicio, LocalDate fim, Object[] status,
                            String tipoReserva, String reservaBloqueada,
                            String reserva, String prioridade, Object[] somenteNaoFaturadas, Object[] estados,
                            Integer minPecas, Integer maxPecas, String emissor) {
            this.remessa = remessa;
            this.cliente = cliente;
            this.inicio = inicio;
            this.fim = fim;
            this.status = status;
            this.tipoReserva = tipoReserva;
            this.reservaBloqueada = reservaBloqueada;
            this.reserva = reserva;
            this.prioridade = prioridade;
            this.somenteNaoFaturadas = somenteNaoFaturadas;
            this.estados = estados;
            this.minPecas = minPecas;
            this.maxPecas = maxPecas;
            this.emissor = emissor;
        }
    }
}
