package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdStatusSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdTipoSolicitacaoExp;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AnaliseSolicitacaoPecasController extends WindowBase {

    protected List<SdSolicitacaoExp> solicitacoesExp = new ArrayList<>();

    // <editor-fold defaultstate="collapsed" desc="Status">
    protected final SdStatusSolicitacaoExp STATUS_CRIADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 1)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_ENVIADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 2)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_VISUALIZADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 3)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_LIBERADO_LEITURA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 4)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_PCS_ENTREGUES = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 5)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_FINALIZADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 6)).singleResult();
    protected final SdStatusSolicitacaoExp STATUS_CANCELADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 7)).singleResult();
    // </editor-fold>

    public AnaliseSolicitacaoPecasController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarSolicitacoes(Object[] solicitacoes, SdTipoSolicitacaoExp tipoSolicitacaoExp, LocalDate dataStart, LocalDate dataEnd, Object[] produtos, Object[] colecao, Object[] marca, String usuario, SdStatusSolicitacaoExp status) {

        if (solicitacoesExp != null) JPAUtils.clearEntitys(solicitacoesExp);
        Object[] solicitacoesId = solicitacoes == null ? new Object[]{} : solicitacoes;

        List<SdSolicitacaoExp> listSolicitacao = (List<SdSolicitacaoExp>) new FluentDao().selectFrom(SdSolicitacaoExp.class).where(it -> it
                .isIn("id", solicitacoesId, TipoExpressao.AND, when -> solicitacoesId.length > 0)
                .equal("tipo.tipo", tipoSolicitacaoExp.getTipo(), TipoExpressao.AND, when -> tipoSolicitacaoExp.getTipo() != null)
                .like("usuario", usuario, TipoExpressao.AND, when -> !usuario.equals(""))
                .between("dtSolicitacao", dataStart, dataEnd)
                .equal("status.codigo", status.getCodigo(), TipoExpressao.AND, when -> status.getStatus() != null)
        ).resultList();

        if (listSolicitacao != null) {
            if (produtos.length > 0) {
                listSolicitacao = listSolicitacao.stream().filter(it -> it.getItens().stream().anyMatch(eb -> Arrays.stream(produtos).anyMatch(ob -> ob.toString().equals(eb.getProduto().getCodigo())))).collect(Collectors.toList());
            }

            if (colecao.length > 0) {
                listSolicitacao = listSolicitacao.stream().filter(it -> it.getItens().stream().anyMatch(eb -> Arrays.stream(colecao).anyMatch(ob -> ob.toString().equals(eb.getProduto().getColecao())))).collect(Collectors.toList());
            }

            if (marca.length > 0) {
                listSolicitacao = listSolicitacao.stream().filter(it -> it.getItens().stream().anyMatch(eb -> Arrays.stream(marca).anyMatch(ob -> ob.toString().equals(eb.getProduto().getMarca())))).collect(Collectors.toList());
            }
            atualizaStatusSolicitacoes(listSolicitacao);
        }

        solicitacoesExp = listSolicitacao;
    }

    protected void atualizaStatusSolicitacoes(List<SdSolicitacaoExp> listSolicitacao) {
        listSolicitacao.forEach(it -> {
            if(it.getStatus().equals(STATUS_ENVIADA)) SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.EDITAR, String.valueOf(it.getId()), "Solicitação " + it.getId() + " vizualidada");
            it.setStatus(it.getStatus().equals(STATUS_ENVIADA) ? STATUS_VISUALIZADA : it.getStatus());
            it = new FluentDao().merge(it);
        });
    }

    @Override
    public void closeWindow() {

    }
}
