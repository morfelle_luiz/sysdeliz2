package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdItensRemessa;
import sysdeliz2.models.view.VSdRemessaProducao;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ColetaExpedicaoController extends WindowBase {

    protected SdStatusRemessa statusEmRemessa = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "X")).singleResult();
    protected SdStatusRemessa statusEmColeta = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "S")).singleResult();
    protected SdStatusRemessa statusParaRevisao = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "J")).singleResult();
    protected SdStatusRemessa statusColetaFinalizada = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "T")).singleResult();

    protected SdColaborador coletor;
    protected SdRemessaCliente remessaEmColeta = null;
    protected SdCaixaRemessa caixaAberta = null;
    protected SdCaixaRemessa ultimaCaixaAberta = null;
    protected List<SdCaixaRemessa> caixasRemessa = new ArrayList<>();

    // producao
    protected VSdRemessaProducao producaoColaborador = new VSdRemessaProducao();
    protected final IntegerProperty qtdeTotalPecas = new SimpleIntegerProperty(0);
    protected final IntegerProperty qtdeTotalRemessa = new SimpleIntegerProperty(0);
    protected final ObjectProperty<BigDecimal> medTotalPecas = new SimpleObjectProperty<>(BigDecimal.ZERO);
    protected final ObjectProperty<BigDecimal> medMinColeta = new SimpleObjectProperty<>(BigDecimal.ZERO);
    protected final ObjectProperty<BigDecimal> medSegColetaPeca = new SimpleObjectProperty<>(BigDecimal.ZERO);

    protected List<SdItensPedidoRemessa> referenciasParaColeta = new ArrayList<>();
    protected List<SdItensPedidoRemessa> referenciasColetadas = new ArrayList<>();
    protected List<SdItensPedidoRemessa> referenciasEmFalta = new ArrayList<>();
    protected SdItensPedidoRemessa referenciaEmColeta = null;
    protected List<SdItensPedidoRemessa> referenciasFaltantesParaRemover = new ArrayList<>();

    public ColetaExpedicaoController(String title, Image icone, Boolean close) {
        super(title, icone, null, close);
    }

    protected SdColaborador getColetor(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
                        .equal("codigoFuncao", 21)
                        .equal("ativo", true))
                .singleResult();
    }

    protected void atualizaProducaoColaborador() {
        producaoColaborador = (VSdRemessaProducao) new FluentDao().selectFrom(VSdRemessaProducao.class)
                .where(eb -> eb
                        .equal("coletor", coletor.getCodigo())
                        .equal("data", LocalDate.now()))
                .singleResult();
        if (producaoColaborador != null) {
            producaoColaborador.refresh();
            qtdeTotalPecas.set(producaoColaborador.getTotqtdepc());
            qtdeTotalRemessa.set(producaoColaborador.getTotrem());
            medTotalPecas.set(producaoColaborador.getMedqtdepc());
            medMinColeta.set(producaoColaborador.getMedminutos());
            medSegColetaPeca.set(producaoColaborador.getMedpcseg());
        }
    }

    protected void carregarRemessa() throws EntityNotFoundException {
        // get de remessa iniciada e não finalizada pelo coletor
        getRemessaInicializada(coletor.getCodigo());
        // get de remessas pendentes caso não exista uma remessa aberta para o coletor
        if (remessaEmColeta == null) {
            getProximaRemessa();
            // verifica se retornou uma remessa pendente disponível para coleta e atribui o coletor para ela
            if (remessaEmColeta != null) {
                remessaEmColeta.setColetor(coletor);
                remessaEmColeta.setDtIniColeta(LocalDateTime.now());
                remessaEmColeta.setStatus(statusEmColeta);
                new FluentDao().merge(remessaEmColeta);
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(coletor.getCodigo()),
                        "Atribuída nova remessa (" + remessaEmColeta.getRemessa() + ") para o coletor.");
            }
        }

        if (remessaEmColeta != null) {
            prepararItensParaColeta();
            caixasRemessa = getCaixasRemessa(remessaEmColeta.getRemessa());
            caixasRemessa.stream().filter(cx -> !cx.getFechada()).findAny().ifPresent(caixa -> caixaAberta = caixa);

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()),
                    "Carga da remessa (" + remessaEmColeta.getRemessa() + ") atribuída para o coletor.");
        }
    }

    protected boolean verificaClienteRemessa() {
        Object[] cliente = remessaEmColeta.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getCodcli().getCodcli()).distinct().toArray();
        if (cliente.length > 1) {
            return false;
        } else if (!cliente[0].equals(remessaEmColeta.getCodcli().getCodcli())) {
            VSdDadosCliente clienteRemessaAlterado = new FluentDao().selectFrom(VSdDadosCliente.class).where(eb -> eb.equal("codcli", cliente[0])).singleResult();
            remessaEmColeta.setCodcli(clienteRemessaAlterado);
            remessaEmColeta = new FluentDao().merge(remessaEmColeta);
        }

        return true;
    }

    protected void carregarRemessa(String remessa) throws BreakException {
        JPAUtils.getEntityManager().clear();
        remessaEmColeta = (SdRemessaCliente) new FluentDao().selectFrom(SdRemessaCliente.class)
                .where(eb -> eb
                        .equal("remessa", remessa))
                .orderBy(Arrays.asList(
                        new Ordenacao("ordemcoleta"),
                        new Ordenacao("dtemissao")))
                .singleResult();

        if (remessaEmColeta != null) {
            if (remessaEmColeta.getStatus() == null)
                return;

            if (!Arrays.asList("X","S","I").contains(remessaEmColeta.getStatus().getCodigo()))
                throw new BreakException("A remessa " + remessaEmColeta.getRemessa() +
                        " consta com o status " + remessaEmColeta.getStatus().getDescricao() +
                        " e não pode ser carregada para coleta manual.");

            prepararItensParaColeta();
            caixasRemessa = getCaixasRemessa(remessaEmColeta.getRemessa());
            caixasRemessa.stream().filter(cx -> !cx.getFechada()).findAny().ifPresent(caixa -> caixaAberta = caixa);

            if (remessaEmColeta.getDtIniColeta() == null)
                remessaEmColeta.setDtIniColeta(LocalDateTime.now());

            new FluentDao().merge(remessaEmColeta);

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EDITAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Carregando remessa para coleta manual.");
        }

    }

    @Nullable
    protected List<SdCaixaRemessa> getCaixasRemessa(Integer remessa) {
        return (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class)
                .where(eb -> eb
                        .equal("remessa", remessa))
                .resultList();
    }

    protected void getRemessaInicializada(int codigo) throws EntityNotFoundException {
        JPAUtils.getEntityManager().clear();
        try {
            SdRemessaCliente remessa = (SdRemessaCliente) new FluentDao().selectFrom(SdRemessaCliente.class)
                    .where(eb -> eb
                            .equal("coletor.codigo", codigo)
                            .equal("status.codigo", "S")
                            .equal("pisoFinalizado", false)
                            .isNotNull("dtIniColeta")
                            .isNull("dtFimColeta"))
                    .singleResult();

            remessaEmColeta = remessa;
        }catch (EntityNotFoundException exc) {
            throw new EntityNotFoundException(exc.getMessage());
        }
    }

    protected void getProximaRemessa() throws EntityNotFoundException {
        JPAUtils.getEntityManager().clear();
        List<SdRemessaCliente> remessas = (List<SdRemessaCliente>) new FluentDao().selectFrom(SdRemessaCliente.class)
                .where(eb -> eb
                        .equal("status.codigo", "X")
                        .equal("piso", coletor.getPisoColeta())
                        .isNull("coletor"))
                .orderBy(Arrays.asList(
                        new Ordenacao("ordemcoleta"),
                        new Ordenacao("dtentrega"),
                        new Ordenacao("dtemissao")))
                .resultList();

        if (remessas.size() > 0)
            remessaEmColeta = remessas.get(0);
    }

    protected SdCaixaRemessa varificaCaixaExistente(String numeroCaixa) {
        return new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("numero", numeroCaixa)).singleResult();
    }

    protected void incluirCaixa(SdCaixaRemessa caixa) {
        caixaAberta = new FluentDao().merge(caixa);
        caixasRemessa.add(caixaAberta);
    }

    protected SdCaixaRemessa substituirCaixa(String fieldBarcodeCaixa) throws SQLException {
        SdCaixaRemessa caixaSubstituta = new SdCaixaRemessa(Integer.parseInt(fieldBarcodeCaixa), remessaEmColeta);
        caixaSubstituta.setPeso(caixaAberta.getPeso());
        caixaSubstituta.setQtde(caixaAberta.getQtde());
        caixaSubstituta = new FluentDao().persist(caixaSubstituta);
        for (SdItensCaixaRemessa barra : caixaAberta.getItens()) {
            SdItensCaixaRemessa item = new SdItensCaixaRemessa(caixaSubstituta, barra.getId().getBarra(), barra.getCodigo(), barra.getCor(), barra.getTam(), barra.getStatus(), barra.getPedido());
            barra.setStatus("C");
            new FluentDao().merge(barra);
            new FluentDao().persist(item);
            caixaSubstituta.getItens().add(item);
        }

        new FluentDao().deleteAll(caixaAberta.getItens());
        new FluentDao().delete(caixaAberta);
        caixasRemessa.remove(caixaAberta);
        caixasRemessa.add(caixaSubstituta);

        new NativeDAO().runNativeQueryUpdate("update pedido3_001 set caixa = '%s' where caixa = '%s' and numero in ('%s')", caixaSubstituta.getNumero(), caixaAberta.getNumero(),
                remessaEmColeta.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getNumero()).distinct().collect(Collectors.joining("','")));

        return caixaSubstituta;
    }

    protected void prepararItensParaColeta() {
        referenciasParaColeta.clear();
        referenciasColetadas.clear();
        referenciasEmFalta.clear();

        remessaEmColeta.getPedidos().forEach(pedido -> pedido.getItens().stream()
                .forEach(item -> {
                    item.getGrade().forEach(grade -> {
                        VSdDadosProdutoBarra produtoDep = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                                .where(eb -> eb
                                        .equal("codigo", item.getId().getCodigo().getCodigo())
                                        .equal("cor", item.getId().getCor())
                                        .equal("tam", grade.getId().getTam()))
                                .singleResult();
                        grade.setLocal(produtoDep != null && produtoDep.getLocal() != null && produtoDep.getLocal().matches("[0-9]-.*") ? produtoDep.getLocal() : "0-0-N/E");
                        grade.setBarra28(produtoDep != null && produtoDep.getBarra28() != null ? produtoDep.getBarra28() : null);
                        grade.setPiso(Integer.parseInt(grade.getLocal().split("-")[0]));
                    });
                    item.getGrade().sort(Comparator.comparingInt(SdGradeItemPedRem::getOrdem));
                    item.setLocal(item.getGrade().stream().map(SdGradeItemPedRem::getLocal).max(String::compareTo).get());
                    item.setPiso(item.getGrade().stream().map(SdGradeItemPedRem::getPiso).max(Integer::compareTo).get());
                }));
        remessaEmColeta.getPedidos().forEach(pedido -> pedido.getItens().stream()
                .filter(item -> coletor == null || item.getPiso() == coletor.getPisoColeta())
                .forEach(item -> {
                    switch (item.getStatusitem()) {
                        case "P":
                            referenciasParaColeta.add(item);
                            break;
                        case "C":
                        case "X":
                            referenciasColetadas.add(item);
                            break;
                        case "F":
                            referenciasEmFalta.add((item));
                            break;
                    }
                }));
        referenciasParaColeta.sort(Comparator.comparing(SdItensPedidoRemessa::getLocal));
        referenciasEmFalta.sort(Comparator.comparing(SdItensPedidoRemessa::getLocal));
    }

    private String getGradePedido(SdItensPedidoRemessa item) throws SQLException {
        List<Object> grades = (List<Object>) new NativeDAO().runNativeQuery(String.format("" +
                "with grade_pedido as\n" +
                " (select distinct pei.codigo, pei.tam, fai.posicao\n" +
                "    from ped_iten_001 pei\n" +
                "    join produto_001 prd\n" +
                "      on prd.codigo = pei.codigo\n" +
                "    join faixa_iten_001 fai\n" +
                "      on fai.faixa = prd.faixa\n" +
                "     and fai.tamanho = pei.tam\n" +
                "   where pei.numero = '%s'\n" +
                "     and pei.codigo = '%s'\n" +
                "   order by fai.posicao)\n" +
                "select codigo, listagg(tam) within\n" +
                " group(\n" +
                " order by posicao) grade\n" +
                "  from grade_pedido\n" +
                " group by codigo", item.getId().getNumero(), item.getId().getCodigo().getCodigo()));

        if (!grades.isEmpty()) {
            Map<String, Object> grade = (Map<String, Object>) grades.get(0);
            return (String) grade.get("GRADE");
        }

        return null;
    }

    private List<SdItensPedidoRemessa> getReferenciasParaFalta() throws SQLException {
        List<SdItensPedidoRemessa> itens = new ArrayList<>();
        if (referenciaEmColeta.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdel).sum() > 0)
            itens.add(referenciaEmColeta);
        referenciasEmFalta.add(referenciaEmColeta);
        referenciasParaColeta.remove(referenciaEmColeta);

        String gradePedido = getGradePedido(referenciaEmColeta);

        List<SdItensPedidoRemessa> referenciasColetas = new ArrayList<>(referenciasColetadas.stream()
                .filter(item -> item.getId().getCodigo().getCodigo().equals(referenciaEmColeta.getId().getCodigo().getCodigo()))
                .collect(Collectors.toList()));
        referenciasColetas.forEach(item -> {
            String gradeReferencia = item.getGrade().stream().sorted(Comparator.comparingInt(SdGradeItemPedRem::getOrdem)).map(grade -> grade.getId().getTam()).collect(Collectors.joining(""));
            if (!gradePedido.equals(gradeReferencia)) {
                itens.add(item);
                referenciasColetadas.remove(item);
                referenciasEmFalta.add(item);
            }
        });
        List<SdItensPedidoRemessa> referenciasParaColetaOutrasCores = new ArrayList<>(referenciasParaColeta.stream()
                .filter(item -> item.getId().getCodigo().getCodigo().equals(referenciaEmColeta.getId().getCodigo().getCodigo()))
                .collect(Collectors.toList()));
        referenciasParaColetaOutrasCores.forEach(item -> {
            String gradeReferencia = item.getGrade().stream().sorted(Comparator.comparingInt(SdGradeItemPedRem::getOrdem)).map(grade -> grade.getId().getTam()).collect(Collectors.joining(""));
            if (!gradePedido.equals(gradeReferencia)) {
                referenciasParaColeta.remove(item);
                referenciasEmFalta.add(item);
            }
        });

        return itens;
    }

    protected void faltaProduto() throws SQLException {
        referenciaEmColeta.setStatusitem("F");
        referenciaEmColeta.getGrade().forEach(grade -> grade.setStatusitem("F"));
        new FluentDao().merge(referenciaEmColeta);
        avisarReposicao(referenciaEmColeta, "F");
        referenciasFaltantesParaRemover = getReferenciasParaFalta();
    }

    protected void expedirExcia(SdItensPedidoRemessa produto, SdGradeItemPedRem item) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade - 1 where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   1,\n" +
                        "   '2',\n" +
                        "   'S',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Caixa - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", item.getId().getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito(), item.getId().getRemessa(), item.getId().getNumero(), caixaAberta.getNumero(),
                item.getId().getCodigo(), item.getId().getCodigo(), item.getId().getNumero(), (remessaEmColeta.getColetor() != null ? remessaEmColeta.getColetor().getCodigo() : Globals.getUsuarioLogado().getCodUsuarioTi())));

        // inclusão do item na PEDIDO3 para o faturamento no Excia
        if (!produto.getTipo().equals("MKT"))
            new NativeDAO().runNativeQueryUpdate(String.format("" +
                            "merge into pedido3_001 ped\n" +
                            "using dual\n" +
                            "on (ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s')\n" +
                            "when matched then\n" +
                            "  update set qtde = qtde + 1, data = sysdate\n" +
                            "when not matched then\n" +
                            "  insert\n" +
                            "    (caixa,\n" +
                            "     codigo,\n" +
                            "     cor,\n" +
                            "     numero,\n" +
                            "     qtde,\n" +
                            "     qtde_f,\n" +
                            "     peso_l,\n" +
                            "     peso,\n" +
                            "     tam,\n" +
                            "     ordem,\n" +
                            "     deposito,\n" +
                            "     lote,\n" +
                            "     data,\n" +
                            "     tcaixa,\n" +
                            "     funcionario,\n" +
                            "     notafiscal)\n" +
                            "  values\n" +
                            "    ('%s', '%s', '%s', '%s', 1, 0, 0, 0, '%s', %s, '%s', '000000', sysdate, '', '%s', '')",
                    item.getId().getNumero(), caixaAberta.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito(),
                    caixaAberta.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getNumero(), item.getId().getTam(), produto.getOrdem(), remessaEmColeta.getDeposito(),
                    (remessaEmColeta.getColetor() != null ? remessaEmColeta.getColetor().getCodigo() : Globals.getUsuarioLogado().getCodUsuarioTi())));

    }

    protected void estornoExcia(SdItensPedidoRemessa produto, SdGradeItemPedRem item, SdCaixaRemessa caixa) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade + 1 where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   1,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Caixa - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", item.getId().getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito(), item.getId().getRemessa(), item.getId().getNumero(), caixa.getNumero(),
                item.getId().getCodigo(), item.getId().getCodigo(), item.getId().getNumero(), (remessaEmColeta.getColetor() != null ? remessaEmColeta.getColetor().getCodigo() : Globals.getUsuarioLogado().getCodUsuarioTi())));

        // inclusão do item na PEDIDO3 para o faturamento no Excia
        if (!produto.getTipo().equals("MKT")) {
            if (item.getQtdel() > 0)
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "update pedido3_001 ped set ped.qtde = ped.qtde - 1, ped.data = sysdate where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getId().getNumero(), caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito()));
            else
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "delete from pedido3_001 ped where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getId().getNumero(), caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessaEmColeta.getDeposito()));
        }
    }

    protected void avisarReposicaoOld(SdItensPedidoRemessa produto, String tipo) throws SQLException {
        SdReposicaoEstoque reposicao = new FluentDao().selectFrom(SdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("produto.codigo", produto.getId().getCodigo().getCodigo())
                        .equal("cor", produto.getId().getCor())
                        .equal("coletor.codigo", coletor.getCodigo())
                        .greaterThanOrEqualTo("dtemissao", LocalDateTime.now().minusMinutes(2)))
                .singleResult();
        if (reposicao == null) {
            List<PaIten> estoque = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                    .where(eb -> eb
                            .equal("id.codigo", produto.getId().getCodigo().getCodigo())
                            .equal("id.cor", produto.getId().getCor())
                            .isIn("id.deposito", new String[]{"0001", "0005"}))
                    .resultList();
            AtomicReference<Boolean> avisar = new AtomicReference<>(false);
            estoque.forEach(itemEstoque -> {
                if (itemEstoque.getQuantidade().compareTo(BigDecimal.TEN) > 0)
                    avisar.set(true);
            });

            if (tipo.equals("F"))
                avisar.set(true);


            if (avisar.get()) {
                new FluentDao().persist(new SdReposicaoEstoque(produto.getId().getCodigo(), produto.getId().getCor(), produto.getLocal(), tipo, coletor, null));

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Enviando aviso de referência com estoque baixo para o produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " no local " + referenciaEmColeta.getLocal());
            } else {

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Enviando aviso de referência com estoque baixo para o produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " no local " + referenciaEmColeta.getLocal() + " porém produto não tem estoque");
            }
        }
    }

    protected void avisarReposicao(SdItensPedidoRemessa produto, String tipo) throws SQLException {
        SdReposicaoEstoque reposicao = new FluentDao().selectFrom(SdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("produto.codigo", produto.getId().getCodigo().getCodigo())
                        .equal("cor", produto.getId().getCor())
                        .equal("coletor.codigo", coletor.getCodigo(), TipoExpressao.AND, b -> tipo.equals("F"))
                        .equal("resolvido", false)
                        .greaterThanOrEqualTo("dtemissao", LocalDateTime.now().minusMinutes(2), TipoExpressao.AND, b -> tipo.equals("F")))
                .singleResult();

        if (reposicao == null) {
            new FluentDao().persist(new SdReposicaoEstoque(produto.getId().getCodigo(), produto.getId().getCor(), produto.getLocal(), tipo, coletor, tipo.equals("F") ? String.valueOf(produto.getId().getRemessa()) : null));

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Enviando aviso de referência com " + (tipo.equals("A") ? "estoque baixo" : "falta") + " para o produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " no local " + referenciaEmColeta.getLocal());
        } else {
            reposicao.setColetor(coletor);
            reposicao.setDtemissao(LocalDateTime.now());
            new FluentDao().merge(reposicao);

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Enviando aviso de referência com " + (tipo.equals("A") ? "estoque baixo" : "falta") + " para o produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " no local " + referenciaEmColeta.getLocal());
        }
    }

    /**
     * Select na VIEW V_SD_ITENS_REMESSA para exibição do botão produtos
     *
     * @param remessa
     * @return
     */
    protected List<VSdItensRemessa> getItensRemessa(Integer remessa) {
        return (List<VSdItensRemessa>) new FluentDao().selectFrom(VSdItensRemessa.class).where(eb -> eb.equal("remessa", remessa)).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
