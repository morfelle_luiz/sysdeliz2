package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.Inventario.SdInventario;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.view.VSdProdutoInventario;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.criteria.JoinType;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ControleInventarioController extends WindowBase {

    protected List<SdInventario> inventarios = new ArrayList<>();
    protected List<VSdProdutoInventario> produtos = new ArrayList<>();

    public ControleInventarioController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    @Override
    public void closeWindow() {

    }

    protected void buscarInventarios(Object[] codigo, Object[] produtos, Object[] colecao, Object[] marca, LocalDate dataCriacaoIni, LocalDate dataCriacaoFim, LocalDate dataLeituraIni, LocalDate dataLeituraFim) {
        try {
            if(inventarios.size() > 0) inventarios.forEach(it -> JPAUtils.getEntityManager().refresh(it));
        } catch (Exception e) {
            e.printStackTrace();
        }
        inventarios = (List<SdInventario>) new FluentDao()
                .selectFrom(SdInventario.class)
                .join("itens", JoinType.LEFT)
                .where(it -> it
                        .isIn("id", codigo, TipoExpressao.AND, when -> codigo.length > 0)
                        .isIn("itens>codigo.id.codigo", produtos, TipoExpressao.AND, when -> produtos.length > 0)
                        .isIn("itens>codigo.colecao", colecao, TipoExpressao.AND, when -> colecao.length > 0)
                        .isIn("itens>codigo.marca", marca, TipoExpressao.AND, when -> marca.length > 0)
                        .between("dtcriacao", dataCriacaoIni, dataCriacaoFim)
                        .between("dtleitura", dataLeituraIni, dataLeituraFim)
                ).resultList().stream().distinct().collect(Collectors.toList());

    }

    protected void buscarProdutos(Object[] codigos, Object[] cores, Object[] colecao, Object[] marcas, LocalDate dtPedidoInicio, LocalDate dtPedidoFim, int qtdeEstoqueIni, int qtdeEstoqueFim, boolean qtdeEstoque, boolean dtPedido) {
        produtos = (List<VSdProdutoInventario>) new FluentDao()
                .selectFrom(VSdProdutoInventario.class)
                .where(it -> it
                        .isIn("codigo", codigos, TipoExpressao.AND, when -> codigos.length > 0)
                        .isIn("cor", cores, TipoExpressao.AND, when -> cores.length > 0)
                        .isIn("colecao", colecao, TipoExpressao.AND, when -> colecao.length > 0)
                        .isIn("marca", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                        .between("estoque", qtdeEstoqueIni, qtdeEstoqueFim, TipoExpressao.AND, when -> qtdeEstoque)
                ).resultList();

        if (produtos != null && produtos.size() > 0 && dtPedido) {
            List<VSdProdutoInventario> produtosToRemove = new ArrayList<>();

            for (VSdProdutoInventario produto : produtos) {
                List<PedIten> list = (List<PedIten>) new FluentDao()
                        .selectFrom(PedIten.class)
                        .where(it -> it
                                .equal("id.codigo.codigo", produto.getCodigo())
                                .equal("id.cor.cor", produto.getCor())
                                .greaterThan("qtde", 0)
                                .between("dt_entrega", dtPedidoInicio, dtPedidoFim)
                        ).resultList();
                if(list == null || list.size() == 0){
                    produtosToRemove.add(produto);
                }
            }

            produtos.removeAll(produtosToRemove);
            produtos.forEach(it -> JPAUtils.getEntityManager().refresh(it));

        }
    }
}
