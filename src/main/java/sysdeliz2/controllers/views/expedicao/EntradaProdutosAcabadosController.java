package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import javafx.stage.Stage;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemLotePA;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.view.VSdDadosOfOsPendente;
import sysdeliz2.models.view.VSdDadosOfOsPendenteSKU;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdDadosOfPendenteSKU;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.Comparator;
import java.util.List;

public class EntradaProdutosAcabadosController extends WindowBase {

    protected SdColaborador colaborador;

    public EntradaProdutosAcabadosController(String title, Image icone, Boolean closeButton) {
        super(title, icone, null, closeButton);
        btnClose.setOnAction(evt -> {
            closeWindow();
        });
    }

    protected void colaboradorLogado(String usuario) {
        colaborador = new FluentDao().selectFrom(SdColaborador.class).where(it -> it.equal("usuario", usuario)).singleResult();
    }

    protected List<VSdDadosOfOsPendenteSKU> sortListaTamanhosGrade(List<VSdDadosOfOsPendenteSKU> listGrade) {
        if (listGrade.get(0).getId().getTam().matches("[0-9]*")) {
            listGrade.sort(Comparator.comparingInt(s -> Integer.parseInt(s.getId().getTam())));
        } else {
            listGrade.sort((o1, o2) -> sortTamanhos(o1.getId().getTam(), o2.getId().getTam()));
        }
        return listGrade;
    }

    protected int sortTamanhos(String tam1, String tam2) {
        int result = 0;
        if (tam1.equals("PP")) {
            if (!tam2.equals("PP")) result = -1;
        }

        if (tam1.equals("P") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP"))) result = 1;
            else result = -1;
        }

        if (tam1.equals("M") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP") || (tam2.equals("P")))) result = 1;
            else result = -1;
        }

        if (tam1.equals("G") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP") || (tam2.equals("P") || (tam2.equals("M")))))
                result = 1;
            else result = -1;
        }

        if (tam1.equals("GG") && !tam2.equals(tam1)) {
            if (tam2.equals("XGG")) result = -1;
            else result = 1;
        }

        if (tam1.equals("XGG") && !tam2.equals(tam1)) {
            result = 1;
        }
        return result;
    }

    @Override
    public void closeWindow() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja fechar?");
            message.showAndWait();
        }).value.get())) {
            ((Stage) box.getScene().getWindow()).close();
        }
    }

    protected VSdDadosOfOsPendente getOfPendente(SdItemLotePA itemLote) {
        VSdDadosOfOsPendente of = new FluentDao().selectFrom(VSdDadosOfOsPendente.class)
                .where(it -> it
                        .equal("numero", itemLote.getNumero())
                        .isIn("setor.codigo", new Object[]{"111","118"})
                )
                .singleResult();
        if( of != null ) {
            of.setCores((List<VSdDadosOfOsPendenteSKU>) new FluentDao().selectFrom(VSdDadosOfOsPendenteSKU.class)
                    .where(it -> it
                            .equal("id.numero", itemLote.getNumero())
                            .equal("id.setor", of.getSetor().getCodigo()))
                    .resultList());
            return of;
        }
        return null;
    }
}
