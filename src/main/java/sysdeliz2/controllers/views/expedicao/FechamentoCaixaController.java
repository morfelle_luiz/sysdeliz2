package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Nota;
import sysdeliz2.models.ti.NotaNumero;
import sysdeliz2.models.ti.TabTran;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.apis.braspress.ServicoBraspressWS;
import sysdeliz2.utils.apis.braspress.models.Rota;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class FechamentoCaixaController extends WindowBase {

    private static final Logger logger = Logger.getLogger(FechamentoCaixaController.class);
    private final String marcasEmpresa = ((List<Marca>) new FluentDao().selectFrom(Marca.class).get().resultList()).stream().map(marca -> marca.getCodigo()).distinct().collect(Collectors.joining(","));
    protected SdParametros _IMPRIME_BRASPRESS = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", 3)).singleResult();
    private final SdParametros _STATUS_IMPRESSO_NF = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", 7)).singleResult();

    protected SdColaborador repositor;
    protected List<SdCaixaRemessa> caixasRemessa = new ArrayList<>();
    protected List<SdCaixaRemessa> consultaCaixasRemessa = new ArrayList<>();
    protected SdRemessaCliente remessaCaixa = null;
    protected SerialPort serialPort = new SerialPort("COM3");
    protected String mensagemBraspress = "";

    public FechamentoCaixaController(String title, Image icone, String[] abas, Boolean closeButton) {
        super(title, icone, abas, closeButton);
    }

    protected SdColaborador getFechador(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
                        .equal("codigoFuncao", 81)
                        .equal("ativo", true))
                .singleResult();
    }

    protected boolean verificaClienteRemessa(SdRemessaCliente remessa) {
        Object[] cliente = remessa.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getCodcli().getCodcli()).distinct().toArray();
        if (cliente.length > 1) {
            return false;
        } else if (!cliente[0].equals(remessa.getCodcli().getCodcli())) {
            VSdDadosCliente clienteRemessaAlterado = new FluentDao().selectFrom(VSdDadosCliente.class).where(eb -> eb.equal("codcli", cliente[0])).singleResult();
            remessa.setCodcli(clienteRemessaAlterado);
            new FluentDao().merge(remessa);
        }

        return true;
    }

    protected SdCaixaRemessa carregarCaixa(String numeroCaixa) {
        JPAUtils.clearEntitys(caixasRemessa);
        if (remessaCaixa != null)
            JPAUtils.clearEntity(remessaCaixa);
        caixasRemessa.clear();
        SdCaixaRemessa caixa = new FluentDao().selectFrom(SdCaixaRemessa.class)
                .where(eb -> eb.equal("numero", numeroCaixa))
                .singleResult();
        if (caixa != null) {
            caixasRemessa = (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class)
                    .where(eb -> eb.equal("remessa.remessa", caixa.getRemessa().getRemessa()))
                    .resultList();
            remessaCaixa = new FluentDao().selectFrom(SdRemessaCliente.class)
                    .where(eb -> eb.equal("remessa", caixa.getRemessa().getRemessa()))
                    .singleResult();
            JPAUtils.getEntityManager().refresh(remessaCaixa);
        }

        return caixa;
    }

    protected BigDecimal lerPeso() throws SerialPortException {
        try {
            serialPort.openPort();
            serialPort.setParams(9600, 8, 1, 0);

            String strPeso = null;
            do {
                byte[] buffer = serialPort.readBytes(56);
                String convertido = new String(buffer).trim();


                String[] dadosLIdos = convertido.split("\\r");
                if (dadosLIdos[0].trim().startsWith("t0")) {
                    strPeso = dadosLIdos[0].split(" ")[1].trim();
                } else if (dadosLIdos[1].trim().startsWith("t0")) {
                    strPeso = dadosLIdos[1].split(" ")[1].trim();
                }
            } while (strPeso == null);
            serialPort.closePort();

            if (strPeso == null)
                return BigDecimal.ZERO;

            BigDecimal pesoLido = new BigDecimal(strPeso).divide(new BigDecimal(100000000));

            return pesoLido;
        } catch (NumberFormatException ex) {
            return BigDecimal.ZERO;
        }
    }

    protected String criarEtiquetaCaixaDeliz(SdCaixaRemessa caixa, List<SdCaixaRemessa> caixasRemessa) {

        String pedidosCaixa = caixa.getItens().stream().map(SdItensCaixaRemessa::getPedido).distinct().collect(Collectors.joining(", "));

        String nomeClienteParte1 = caixa.getRemessa().getCodcli().getRazaosocial().length() > 50 ? caixa.getRemessa().getCodcli().getRazaosocial().substring(0, 49) : caixa.getRemessa().getCodcli().getRazaosocial();
        String nomeClienteParte2 = caixa.getRemessa().getCodcli().getRazaosocial().length() > 50 ? caixa.getRemessa().getCodcli().getRazaosocial().substring(49, (caixa.getRemessa().getCodcli().getRazaosocial().length() - 1)) : "";
        String remessa = StringUtils.lpad(String.valueOf(caixa.getRemessa().getRemessa()), 10, "0");
        String ufEntrega = caixa.getRemessa().getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst();
        String volume = StringUtils.lpad(String.valueOf(caixa.getVolume()), 2, "0") + "/" + StringUtils.lpad(String.valueOf(caixasRemessa.stream().filter(caixas -> caixas.getNota().equals(caixa.getNota())).count()), 2, "0");
        String notaFiscal = caixa.getNota();
        String pedidoParte1 = pedidosCaixa.length() > 22 ? pedidosCaixa.substring(0, 21) : pedidosCaixa;
        String pedidoParte2 = pedidosCaixa.length() > 22 ? pedidosCaixa.substring(23, (pedidosCaixa.length() - 1)) : "";
        String enderecoNumero = caixa.getRemessa().getCodcli().getEnderecoentrega() + ", " + caixa.getRemessa().getCodcli().getNumeroentrega();
        String bairro = caixa.getRemessa().getCodcli().getBairroentrega();
        String complementoPart1 = caixa.getRemessa().getCodcli().getComplementoentrega() != null ? caixa.getRemessa().getCodcli().getComplementoentrega() : "";
        String complementoPart2 = "";
        String cep = caixa.getRemessa().getCodcli().getCepentrega().getCep();
        String cidadeUf = caixa.getRemessa().getCodcli().getCepentrega().getCidade().getNomeCid() + ", " + caixa.getRemessa().getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst();
        String transportador = caixa.getRemessa().getPedidos().get(0).getId().getNumero().getTabTrans().getNome();
        String peso = StringUtils.toDecimalFormat(caixa.getPeso());
        String numeroCaixa = String.valueOf(caixa.getNumero());

        return "" +
                "\u0010CT~~CD,~CC^~CT~\n" +
                "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5^JUS^LRN^CI0^XZ\n" +
                "^XA\n" +
                "^MMT\n" +
                "^PW799\n" +
                "^LL0959\n" +
                "^LS0\n" +
                "^FT243,917^A0I,20,19^FH\\^FDVOL:^FS\n" +
                "^FO13,9^GB772,934,8^FS\n" +
                "^FO14,9^GB771,360,8^FS\n" +
                "^FT43,317^BQN,2,10\n" +
                "^FH\\^FDLA," + numeroCaixa + "^FS\n" +
                "^FT196,33^A0I,34,33^FH\\^FD" + numeroCaixa + "^FS\n" +
                "^FT190,307^A0I,34,33^FH\\^FDCAIXA^FS\n" +
                "^FT774,337^A0I,20,19^FH\\^FDPeso:^FS\n" +
                "^FT773,728^A0I,20,19^FH\\^FDDestinatario:^FS\n" +
                "^FT393,816^A0I,20,19^FH\\^FDPedido:^FS\n" +
                "^FT624,914^A0I,20,19^FH\\^FDRemessa:^FS\n" +
                "^FT774,914^A0I,20,19^FH\\^FDUF:^FS\n" +
                "^FT776,816^A0I,20,19^FH\\^FDNF:^FS\n" +
                "^FT775,420^A0I,20,19^FH\\^FDTransportadora^FS\n" +
                "^FT773,387^A0I,28,28^FH\\^FD" + transportador + "^FS\n" +
                "^FT229,860^A0I,62,62^FH\\^FD" + volume + "^FS\n" +
                "^FT582,859^A0I,62,62^FH\\^FD" + remessa + "^FS\n" +
                "^FT741,852^A0I,79,79^FH\\^FD" + ufEntrega + "^FS\n" +
                "^FT766,272^A0I,73,72^FH\\^FD" + peso + " Kg^FS\n" +
                "^FO12,750^GB390,91,8^FS\n" +
                "^FT701,771^A0I,73,72^FH\\^FD" + notaFiscal + "^FS\n" +
                "^FO14,445^GB769,312,8^FS\n" +
                "^FO14,834^GB239,108,8^FS\n" +
                "^FO246,833^GB389,109,8^FS\n" +
                "^FO14,446^GB449,75,8^FS\n" +
                "^FO396,750^GB388,91,8^FS\n" +
                "^FT328,768^A0I,28,28^FH\\^FD" + pedidoParte2 + "^FS\n" +
                "^FT328,799^A0I,28,28^FH\\^FD" + pedidoParte1 + "^FS\n" +
                "^FT772,575^A0I,23,24^FH\\^FD" + bairro + "^FS\n" +
                "^FT452,464^A0I,23,24^FH\\^FD" + cidadeUf + "^FS\n" +
                "^FT772,22^A0I,17,16^FH\\^FD" + StringUtils.toDateTimeFormat(LocalDateTime.now()) + "^FS\n" +
                "^FT771,533^A0I,14,14^FH\\^FD" + complementoPart2 + "^FS\n" +
                "^FT772,554^A0I,14,14^FH\\^FD" + complementoPart1 + "^FS\n" +
                "^FT454,498^A0I,17,16^FH\\^FDCidade/UF:^FS\n" +
                "^FT764,467^A0I,45,45^FH\\^FDCEP: " + cep + "^FS\n" +
                "^FT772,631^A0I,17,16^FH\\^FDEndere\\87o:^FS\n" +
                "^FT772,602^A0I,23,24^FH\\^FD" + enderecoNumero + "^FS\n" +
                "^FT774,696^A0I,28,28^FH\\^FD" + nomeClienteParte1 + "^FS\n" +
                "^FT774,662^A0I,28,28^FH\\^FD" + nomeClienteParte2 + "^FS\n" +
                "^LRY^FO22,18^GB0,342,252^FS^LRN\n" +
                "^LRY^FO207,914^GB38,0,18^FS^LRN\n" +
                "^LRY^FO635,841^GB141,0,92^FS^LRN\n" +
                "^LRY^FO464,453^GB309,0,67^FS^LRN\n" +
                "^PQ1,0,1,Y^XZ";
    }

    protected String criarEtiquetaCaixaBrasspress(SdCaixaRemessa caixa, List<SdCaixaRemessa> caixasRemessa) throws IOException {

        Rota rotaBraspress = new ServicoBraspressWS().post(caixa.getRemessa().getCodcli().getCepentrega().getCep());
        if (rotaBraspress == null || !rotaBraspress.getMensagem().equals("OK")) {
            if (rotaBraspress == null) {
                mensagemBraspress = "ERRO NA COMUNICAÇÃO COM A BRASPRESS";
            } else {
                mensagemBraspress = rotaBraspress.getMensagem().toUpperCase();
            }
            return criarEtiquetaCaixaDeliz(caixa, caixasRemessa);
        }

        String pedidosCaixa = caixa.getItens().stream().map(SdItensCaixaRemessa::getPedido).distinct().collect(Collectors.joining(", "));
        String nomeClienteParte1 = caixa.getRemessa().getCodcli().getRazaosocial();
//        String nomeClienteParte1 = caixa.getRemessa().getCodcli().getRazaosocial().length() > 50 ? caixa.getRemessa().getCodcli().getRazaosocial().substring(0, 49) : caixa.getRemessa().getCodcli().getRazaosocial();
//        String nomeClienteParte2 = caixa.getRemessa().getCodcli().getRazaosocial().length() > 50 ? caixa.getRemessa().getCodcli().getRazaosocial().substring(49, (caixa.getRemessa().getCodcli().getRazaosocial().length() - 1)) : "";
        String remessa = StringUtils.lpad(String.valueOf(caixa.getRemessa().getRemessa()), 10, "0");
//        String ufEntrega = caixa.getRemessa().getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst();
        String volume = StringUtils.lpad(String.valueOf(caixa.getVolume()), 3, "0") + "/" + StringUtils.lpad(String.valueOf(caixasRemessa.stream().filter(caixas -> caixas.getNota().equals(caixa.getNota())).count()), 3, "0");
        String notaFiscal = caixa.getNota();
        String pedidoParte1 = pedidosCaixa.length() > 22 ? pedidosCaixa.substring(0, 21) : pedidosCaixa;
        String pedidoParte2 = pedidosCaixa.length() > 22 ? pedidosCaixa.substring(23, (pedidosCaixa.length() - 1)) : "";
        String enderecoNumero = caixa.getRemessa().getCodcli().getEnderecoentrega() + ", " + caixa.getRemessa().getCodcli().getNumeroentrega();
        String bairro = caixa.getRemessa().getCodcli().getBairroentrega();
        String cep = caixa.getRemessa().getCodcli().getCepentrega().getCep();
        String cidadeUf = caixa.getRemessa().getCodcli().getCepentrega().getCidade().getNomeCid() + ", " + caixa.getRemessa().getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst();
        String peso = StringUtils.toDecimalFormat(caixa.getPeso());
        String numeroCaixa = String.valueOf(caixa.getNumero());

        String serial128 = "6" + "0322" + "01" + StringUtils.lpad(notaFiscal, 9, "0") +
                StringUtils.lpad(String.valueOf(caixa.getVolume()), 4, "0") +
                StringUtils.lpad(rotaBraspress.getRota(), 5, "0") +
                StringUtils.lpad(rotaBraspress.getIdfilial(), 4, "0") + "1";

        return "" +
                "CT~~CD,~CC^~CT~\n" +
                "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5^JUS^LRN^CI0^XZ\n" +
                "^XA\n" +
                "^MMT\n" +
                "^PW799\n" +
                "^LL0959\n" +
                "^LS0\n" +
                "^FT246,920^A0I,20,19^FH\\^FDVOL:^FS\n" +
                "^FT141,824^A0I,20,19^FH\\^FDOrigem/Frota:^FS\n" +
                "^FT273,819^A0I,20,19^FH\\^FDViagem(celula)^FS\n" +
                "^FT496,920^A0I,20,19^FH\\^FDRota:^FS\n" +
                "^FT780,815^A0I,20,19^FH\\^FDNF:^FS\n" +
                "^FT782,690^A0I,20,19^FH\\^FDDestinatario:^FS\n" +
                "^FT68,134^A0I,20,19^FH\\^FDPeso:^FS\n" +
                "^FT783,741^A0I,19,19^FH\\^FDRemetente:^FS\n" +
                "^FT780,924^A0I,20,19^FH\\^FDDestino/Sigla^FS\n" +
                "^FT632,86^A0I,20,19^FH\\^FDPedido:^FS\n" +
                "^FT632,135^A0I,20,19^FH\\^FDRemessa:^FS\n" +
                "^FT229,867^A0I,62,62^FH\\^FD" + volume + "^FS\n" +
                "^FT749,46^A0R,34,33^FH\\^FDDELIZ^FS\n" +
                "^FT36,364^A0R,34,33^FH\\^FDBRASPRESS^FS\n" +
                "^FT702,777^A0I,72,72^FH\\^FD" + notaFiscal + "^FS\n" +
                "^FT634,111^A0I,25,24^FH\\^FD" + remessa + "^FS\n" +
                "^FT212,86^A0I,45,45^FH\\^FD" + peso + " Kg^FS\n" +
                "^FO16,710^GB138,140,8^FS\n" +
                "^FT447,870^A0I,73,72^FH\\^FD" + StringUtils.lpad(rotaBraspress.getRota(), 5, "0") + "^FS\n" +
                "^FO15,166^GB777,783,8^FS\n" +
                "^FO16,533^GB775,183,8^FS\n" +
                "^FT770,473^A0I,62,62^FH\\^FDCIF^FS\n" +
                "^FT671,866^A0I,85,84^FH\\^FD" + rotaBraspress.getFilial() + "^FS\n" +
                "^FO278,708^GB514,56,8^FS\n" +
                "^FT779,868^A0I,51,50^FH\\^FD" + StringUtils.lpad(rotaBraspress.getIdfilial(), 3, "0") + "^FS\n" +
                "^FO276,758^GB516,90,8^FS\n" +
                "^FT633,27^A0I,28,28^FH\\^FD" + pedidoParte2 + "^FS\n" +
                "^FT634,58^A0I,28,28^FH\\^FD" + pedidoParte1 + "^FS\n" +
                "^FO15,841^GB239,108,8^FS\n" +
                "^FO254,842^GB250,107,8^FS\n" +
                "^FT781,720^A0I,20,21^FH\\^FDDELIZ INDUSTRIA DO VESTUARIO LTDA^FS\n" +
                "^FT203,549^A0I,28,28^FH\\^FDCEP: " + cep + "^FS\n" +
                "^FT780,549^A0I,28,28^FH\\^FD" + cidadeUf + "^FS\n" +
                "^FT781,595^A0I,23,24^FH\\^FD" + bairro + "^FS\n" +
                "^FT781,627^A0I,23,24^FH\\^FD" + enderecoNumero + "^FS\n" +
                "^FT781,659^A0I,23,24^FH\\^FD" + nomeClienteParte1 + "^FS\n" +
                "^BY3,9^FT524,380^B7I,9,0,,,N\n" +
                "^FH\\^FD" + serial128 + "^FS\n" +
                "^BY3,3,160^FT710,202^BCI,,Y,N\n" +
                "^FD>;" + serial128 + "^FS\n" +
                "^FT134,749^A0I,79,79^FH\\^FDITJ^FS\n" +
                "^FO16,14^GB776,146,8^FS\n" +
                "^FT646,156^BQN,2,4\n" +
                "^FH\\^FDLA," + numeroCaixa + "^FS\n" +
                "^FT730,30^A0I,28,28^FH\\^FD" + numeroCaixa + "^FS\n" +
                "^LRY^FO505,849^GB279,0,92^FS^LRN\n" +
                "^LRY^FO202,916^GB44,0,24^FS^LRN\n" +
                "^LRY^FO453,916^GB43,0,23^FS^LRN\n" +
                "^LRY^FO688,737^GB95,0,21^FS^LRN\n" +
                "^LRY^FO675,462^GB106,0,66^FS^LRN\n" +
                "^LRY^FO737,22^GB0,129,46^FS^LRN\n" +
                "^LRY^FO24,364^GB0,170,46^FS^LRN\n" +
                "^LRY^FO154,717^GB0,123,122^FS^LRN\n" +
                "^LRY^FO636,22^GB0,129,99^FS^LRN\n" +
                "^PQ1,0,1,Y^XZ";
    }

    private String getNumeroNf() throws SQLException {
        String numero = new FluentDao().selectFrom(NotaNumero.class).max("id.numero").get().singleResult();
        numero = String.valueOf(Integer.parseInt(numero) + 1);
        new FluentDao().persist(new NotaNumero(numero, "1"));

        return numero;
    }

    protected void criarNfsRemessa(SdRemessaCliente remessa) throws SQLException {

        logger.log(Priority.INFO, "Iniciando buscas nos pedidos para criação das NFs!");

        // get das marcas caso cliente pede para faturar marcas separado
        JPAUtils.getEntityManager().refresh(remessa.getCodcli());
        Object[] marcasRemessa = !remessa.getCodcli().isSeparaMarcasFatura() ? new Object[]{marcasEmpresa} : remessa.getPedidos().stream()
                .filter(pedido -> pedido.getItens().stream().filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                        .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null)
                .map(pedido -> pedido.getId().getNumero().getSdPedido().getMarca())
                .distinct()
                .toArray();
        for (Object marca : marcasRemessa) {
            // get dos representantes atendidos pela remessa, caso sejam dois, uma nota para cada representante
            Object[] representantesRemessa = remessa.getPedidos().stream()
                    .filter(pedido -> pedido.getItens().stream().filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                            .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca()))
                    .map(pedido -> pedido.getId().getNumero().getCodrep().getCodRep())
                    .distinct()
                    .toArray();
            for (Object representante : representantesRemessa) {
                // verificando data base de faturamento dos pedidos
                Object[] datasBase = remessa.getPedidos().stream()
                        .filter(pedido -> pedido.getItens().stream()
                                .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca())
                                && pedido.getId().getNumero().getCodrep().getCodRep().equals((String) representante))
                        .map(pedido -> pedido.getDtBase())
                        .distinct()
                        .toArray();
                for (Object dataBase : datasBase) {
                    // verificando se dentro do representante, tem pedidos bofinicados ou de vendas
                    Object[] faturasAgrupadasPedidos = remessa.getPedidos().stream()
                            .filter(pedido -> pedido.getItens().stream()
                                    .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                    .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca())
                                    && (pedido.getDtBase() == null || pedido.getDtBase().equals((LocalDate) dataBase))
                                    && pedido.getId().getNumero().getCodrep().getCodRep().equals((String) representante))
                            .map(pedido -> pedido.getId().getNumero().getPeriodo().getTipoFaturamento())
                            .distinct()
                            .toArray();
                    for (Object faturaSeparado : faturasAgrupadasPedidos) {
                        // verificando se dentro dos pedidos agrupados para faturamento tem condicao de pagto diferentes
                        Object[] condicoesPedidos = remessa.getPedidos().stream()
                                .filter(pedido -> pedido.getItens().stream()
                                        .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                        .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca())
                                        && (pedido.getDtBase() == null || pedido.getDtBase().equals((LocalDate) dataBase))
                                        && pedido.getId().getNumero().getCodrep().getCodRep().equals((String) representante)
                                        && pedido.getId().getNumero().getPeriodo().getTipoFaturamento().equals((String) faturaSeparado))
                                .map(pedido -> pedido.getId().getNumero().getPagto().trim())
                                .distinct()
                                .toArray();
                        for (Object condicao : condicoesPedidos) {
                            // verificando se dentro dos pedidos agrupados para faturamento tem condicao de pagto diferentes
                            Object[] descontosPedidos = remessa.getPedidos().stream()
                                    .filter(pedido -> pedido.getItens().stream()
                                            .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                            .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca())
                                            && (pedido.getDtBase() == null || pedido.getDtBase().equals((LocalDate) dataBase))
                                            && pedido.getId().getNumero().getCodrep().getCodRep().equals((String) representante)
                                            && pedido.getId().getNumero().getPeriodo().getTipoFaturamento().equals((String) faturaSeparado)
                                            && pedido.getId().getNumero().getPagto().trim().equals((String) condicao))
                                    .map(pedido -> pedido.getId().getNumero().getPercDesc().add(pedido.getId().getNumero().getVlrDesc()))
                                    .distinct()
                                    .toArray();
                            for (Object desconto : descontosPedidos) {
                                List<SdPedidosRemessa> pedidosParaFaturamento = remessa.getPedidos().stream()
                                        .filter(pedido -> pedido.getItens().stream()
                                                .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                                .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() == null && ((String) marca).contains(pedido.getId().getNumero().getSdPedido().getMarca())
                                                && (pedido.getDtBase() == null || pedido.getDtBase().equals((LocalDate) dataBase))
                                                && pedido.getId().getNumero().getCodrep().getCodRep().equals((String) representante)
                                                && pedido.getId().getNumero().getPeriodo().getTipoFaturamento().equals((String) faturaSeparado)
                                                && pedido.getId().getNumero().getPagto().trim().equals((String) condicao)
                                                && pedido.getId().getNumero().getPercDesc().add(pedido.getId().getNumero().getVlrDesc()).compareTo((BigDecimal) desconto) == 0)
                                        .collect(Collectors.toList());
                                // gerador do número da NF para o faturamento do(s) pedido(s)
                                String numeroNF = getNumeroNf();
                                // criando o cabeçalho da NF
                                Nota nota = new Nota();
                                nota.setFatura(numeroNF);
                                nota.setImpresso("X");
                                nota.setDocto(String.valueOf(remessa.getRemessa()));
                                nota.setCodcli(remessa.getCodcli().getCodcli());
                                nota.setCodrep((String) representante);
                                // persiste da nota no banco
                                new FluentDao().persist(nota);

                                pedidosParaFaturamento.forEach(pedido -> {
                                    pedido.setNota(nota);
                                    // merge do pedido no banco
                                    new FluentDao().merge(pedido);

                                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.CADASTRAR, String.valueOf(remessa.getRemessa()), "Cadastrado NF " + nota.getFatura() + " para o pedido " + pedido.getId().getNumero().getNumero());
                                });
                            }
                        }
                    }
                }
            }
        }

        // atribuindo o número das NFs geradas para as caixas da remessa
        caixasRemessa.forEach(caixa -> {
            remessa.refresh();

            // definindo o rank dos pedidos com mais peças na caixa
            Map<String, Long> rankPedido = caixa.getItens().stream().collect(Collectors.groupingBy(it -> it.getPedido(), Collectors.counting()));

            //Sort a map rankPedido and add to rankPedidoSorted e get do numero do pedido com mais itens
            Map<String, Long> rankPedidoSorted = new LinkedHashMap<>();
            rankPedido.entrySet().stream()
                    .sorted(Map.Entry.<String, Long>comparingByValue()
                            .reversed()).forEachOrdered(e -> rankPedidoSorted.put(e.getKey(), e.getValue()));
            String pedidoComMaisItens = null;
            for (Map.Entry<String, Long> stringLongEntry : rankPedido.entrySet()) {
                pedidoComMaisItens = stringLongEntry.getKey();
                break;
            }

            // definindo a NF para a caixa do pedido com mais itens
            String finalPedidoComMaisItens = pedidoComMaisItens;
            remessa.getPedidos().stream()
                    .filter(pedido -> pedido.getId().getNumero().getNumero().equals(finalPedidoComMaisItens))
                    .findAny()
                    .ifPresent(pedido -> caixa.setNota(pedido.getNota() != null ? pedido.getNota().getFatura() : null));

            // merge da caixa para salvar a nf
            new FluentDao().merge(caixa);

            // add log acao
            SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.CADASTRAR, String.valueOf(caixa.getNumero()), "Cadastrado NF " + caixa.getNota() + " para a caixa.");
        });
        caixasRemessa.stream()
                .filter(caixa -> caixa.getNota() == null)
                .forEach(caixa -> {
                    // definindo o rank das NFs com mais caixas
                    Map<String, Long> rankNfs = caixasRemessa.stream()
                            .filter(it -> it.getNota() != null)
                            .collect(Collectors.groupingBy(it -> it.getNota(), Collectors.counting()));

                    //Sort a map rankPedido and add to rankPedidoSorted e get do numero do pedido com mais itens
                    Map<String, Long> rankNfsSorted = new LinkedHashMap<>();
                    rankNfs.entrySet().stream()
                            .sorted(Map.Entry.<String, Long>comparingByValue()
                                    .reversed()).forEachOrdered(e -> rankNfsSorted.put(e.getKey(), e.getValue()));
                    String nfComMaisCaixas = null;
                    for (Map.Entry<String, Long> stringLongEntry : rankNfs.entrySet()) {
                        nfComMaisCaixas = stringLongEntry.getKey();
                        break;
                    }
                    caixa.setNota(nfComMaisCaixas);
                    new FluentDao().merge(caixa);
                });

    }

    protected void getCaixasRemessa(String remessa) {
        consultaCaixasRemessa = (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("remessa.remessa", remessa)).resultList();
    }

    protected void excluiCaixa(SdCaixaRemessa item) throws SQLException {
        for (SdItensCaixaRemessa barra : item.getItens()) {
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_grade_item_ped_rem_001 set status_item = 'P', qtde_l = qtde_l - 1 where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                    item.getRemessa().getRemessa(), barra.getPedido(), barra.getCodigo().getCodigo(), barra.getCor(), barra.getTam()));
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_pedido_remessa_001 set status_item = 'P' where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                    item.getRemessa().getRemessa(), barra.getPedido(), barra.getCodigo().getCodigo(), barra.getCor()));
            new FluentDao().delete(barra);
            estornoExcia(barra, item);
        }

        if (item.getRemessa().isColetaDupla()) {
            AtomicReference<String> coletorPiso1 = new AtomicReference<>();
            try {
                item.getRemessa().getPedidos().forEach(pedido -> pedido.getItens().forEach(itemRemessa -> itemRemessa.getGrade().forEach(grade -> {
                    if (grade.getPiso() == 1 && grade.getColetor() != null) {
                        coletorPiso1.set(String.valueOf(grade.getColetor().getCodigo()));
                        throw new BreakException();
                    }
                })));
            }catch (BreakException ex) {}
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set status = 'S', coletor = '%s', piso_finalizado = 'N', dt_fim_coleta = null where remessa = '%s'", coletorPiso1.get(), item.getRemessa().getRemessa()));
        } else {
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set status = 'S', piso_finalizado = 'N', dt_fim_coleta = null where remessa = '%s'", item.getRemessa().getRemessa()));
        }
        new FluentDao().delete(item);
    }

    protected void excluiBarra(SdItensCaixaRemessa item, SdCaixaRemessa caixaSelecionada) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_grade_item_ped_rem_001 set status_item = 'P', qtde_l = qtde_l - 1 where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s'",
                caixaSelecionada.getRemessa().getRemessa(), item.getPedido(), item.getCodigo().getCodigo(), item.getCor(), item.getTam()));
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_itens_pedido_remessa_001 set status_item = 'P' where remessa = '%s' and numero = '%s' and codigo = '%s' and cor = '%s'",
                caixaSelecionada.getRemessa().getRemessa(), item.getPedido(), item.getCodigo().getCodigo(), item.getCor()));
        new FluentDao().delete(item);
        estornoExcia(item, caixaSelecionada);

        caixaSelecionada.refresh();
        caixaSelecionada.setQtde(caixaSelecionada.getQtde() - 1);
        caixaSelecionada.setPeso(caixaSelecionada.getPeso().subtract(item.getCodigo().getPeso()));
        new FluentDao().merge(caixaSelecionada);

        if (caixaSelecionada.getRemessa().isColetaDupla()) {
            AtomicReference<String> coletorPiso1 = new AtomicReference<>();
            try {
                caixaSelecionada.getRemessa().getPedidos().forEach(pedido -> pedido.getItens().forEach(itemRemessa -> itemRemessa.getGrade().forEach(grade -> {
                    if (grade.getPiso() == 1) {
                        coletorPiso1.set(String.valueOf(grade.getColetor().getCodigo()));
                        throw new BreakException();
                    }
                })));
            }catch (BreakException ex) {}
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set status = 'S', coletor = '%s', piso_finalizado = 'N', dt_fim_coleta = null where remessa = '%s'", coletorPiso1.get(), caixaSelecionada.getRemessa().getRemessa()));
        } else {
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_remessa_cliente_001 set status = 'S', piso_finalizado = 'N', dt_fim_coleta = null where remessa = '%s'", caixaSelecionada.getRemessa().getRemessa()));
        }
    }

    protected void estornoExcia(SdItensCaixaRemessa item, SdCaixaRemessa caixa) throws SQLException {
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade + 1 where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                item.getCodigo().getCodigo(), item.getCor(), item.getTam(), caixa.getRemessa().getDeposito()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   1,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Caixa - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')",
                item.getPedido(), item.getCodigo().getCodigo(), item.getCor(), item.getTam(), caixa.getRemessa().getDeposito(), caixa.getRemessa().getRemessa(), item.getPedido(), caixa.getNumero(),
                item.getCodigo().getCodigo(), item.getCodigo().getCodigo(), item.getPedido(), Globals.getUsuarioLogado().getCodUsuarioTi()));

        // inclusão do item na PEDIDO3 para o faturamento no Excia
        if (!item.getCodigo().getCodCol().equals("MKT")) {
            List<Object> qtdeLidaBarra = new NativeDAO().runNativeQuery(String.format("" +
                    "select sum(qtde_l) qtde\n" +
                    "  from sd_grade_item_ped_rem_001\n" +
                    " where remessa = '%s'\n" +
                    "   and numero = '%s'\n" +
                    "   and codigo = '%s'\n" +
                    "   and cor = '%s'\n" +
                    "   and tam = '%s'", caixa.getRemessa().getRemessa(), item.getPedido(), item.getCodigo().getCodigo(), item.getCor(), item.getTam()));
            BigDecimal qtdeLida = (BigDecimal) ((Map<String, Object>) qtdeLidaBarra.get(0)).get("QTDE");
            if (qtdeLida.compareTo(BigDecimal.ZERO) > 0)
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "update pedido3_001 ped set ped.qtde = ped.qtde - 1, ped.data = sysdate where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getPedido(), caixa.getNumero(), item.getCodigo().getCodigo(), item.getCor(), item.getTam(), caixa.getRemessa().getDeposito()));
            else
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "delete from pedido3_001 ped where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getPedido(), caixa.getNumero(), item.getCodigo().getCodigo(), item.getCor(), item.getTam(), caixa.getRemessa().getDeposito()));
        }
    }

    protected void gerarNfRemessa(SdRemessaCliente remessa) {
        LiberacaoRemessaController controllerLiberacao = new LiberacaoRemessaController();

        Map<Nota, List<SdPedidosRemessa>> notasRemessa = remessa.getPedidos().stream()
                .filter(pedido -> pedido.getItens().stream()
                        .filter(item ->  item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                        .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() != null && pedido.getNota().getImpresso().equals("X"))
                .collect(Collectors.groupingBy(pedido -> pedido.getNota() != null ? pedido.getNota() : new Nota()));
        for (Map.Entry<Nota, List<SdPedidosRemessa>> notaListEntry : notasRemessa.entrySet()) {
            Nota nfPedido = notaListEntry.getKey();
            List<SdPedidosRemessa> pedidosNf = notaListEntry.getValue();

            pedidosNf.sort(Comparator.comparing(pedido -> pedido.getId().getNumero().getNumero()));
            List<SdCaixaRemessa> caixasNf = (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class)
                    .where(eb -> eb
                            .equal("nota", nfPedido.getFatura()))
                    .resultList();
            if (pedidosNf.stream()
                    .anyMatch(pedido -> pedido.getId().getNumero().getFinanceiro().equals("0") || pedido.getId().getNumero().getBloqueio().equals("0"))) {
                SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENVIAR, nfPedido.getFatura(), "Não foi possível liberar a NF devido o pedido estar com bloqueio financeiro/comercial.");
                continue;
            }
            controllerLiberacao.parcelasFaturamento.clear();
            controllerLiberacao.contabilFaturamento.clear();
            controllerLiberacao.dupsRepFaturamento.clear();

            // completando dados do cabeçalho da NF
            Nota notaAtualizada = null;
            try {
                notaAtualizada = controllerLiberacao.atualizaNotaFiscal(nfPedido, pedidosNf, caixasNf, remessa);

                // transportadora
                AtomicReference<TabTran> transportadora = new AtomicReference<>();
                pedidosNf.stream().findFirst().ifPresent(pedido -> transportadora.set(pedido.getId().getNumero().getTabTrans()));
                notaAtualizada.setTransport(transportadora.get().getCodigo());
                String condicaoPedido = pedidosNf.get(0).getId().getNumero().getPagto();
                String condicaoFaturamento = controllerLiberacao.getCondicaoFaturamento(remessa, pedidosNf, notaAtualizada);
                if (controllerLiberacao.parcelasFaturamento.size() == 0 && notaAtualizada.getNatureza().isDupli()) {
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENVIAR, notaAtualizada.getFatura(), "Não foi possível liberar a NF devido o parcelamento não ser definido automaticamente.");
                    continue;
                }

                try {
                    // persistindo a NF
                    new FluentDao().merge(notaAtualizada);
                    // persistindo as duplicatas
                    new FluentDao().persistAll(controllerLiberacao.parcelasFaturamento);
                    new FluentDao().persistAll(controllerLiberacao.dupsRepFaturamento);
                    // persistindo o contábil
                    new FluentDao().persistAll(controllerLiberacao.contabilFaturamento.stream()
                            .filter(contabil -> contabil.getValor() != null && contabil.getValor().compareTo(BigDecimal.ZERO) > 0)
                            .collect(Collectors.toList()));
                    // log da ação
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.MOVIMENTAR, notaAtualizada.getFatura(),
                            "Atualizando dados da NF pré gerada e cadastrando os parcelamentos/duplicatas.");
                    // atualizando os dados do TI (PEDIDO3, PED_ITEN e PED_RESERVA)
                    new NativeDAO().runNativeQueryProcedure(String.format("p_sd_atualiza_faturamento('%s')", nfPedido.getFatura()));
                    // log da ação
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.MOVIMENTAR, notaAtualizada.getFatura(),
                            "Movimentando remessa/pedido para faturamento.");


                    // liberando NF para transmissão e impressão
                    _STATUS_IMPRESSO_NF.refresh();
                    notaAtualizada.setImpresso(_STATUS_IMPRESSO_NF.getValor());
                    new FluentDao().merge(notaAtualizada);

                    // verificação de nfs pendentes na remessa para atualizar o status da remessa
                    if (remessa.getPedidos().stream()
                            .filter(pedido -> pedido.getQtde() > 0)
                            .noneMatch(pedido -> pedido.getNota().getImpresso().equals("X"))) {
                        remessa.setStatus(controllerLiberacao.statusRemessaFaturada);
                        remessa.setDtFatura(LocalDate.now());
                        new FluentDao().merge(remessa);
                    }

                    // log da ação
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENVIAR, notaAtualizada.getFatura(), "Liberado NF para transmissão ao SEFAZ.");

                } catch (SQLException e) {
                    e.printStackTrace();
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    String exceptionText = sw.toString();
                    SimpleMail.INSTANCE.addDestinatario("diego@deliz.com.br")
                            .comAssunto("Exception report SysDeliz2 [LIBERAÇÃO DE REMESSA]")
                            .comCorpo(e.getLocalizedMessage() + "\n\n" + exceptionText)
                            .send();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                    // log da ação
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENVIAR, notaAtualizada.getFatura(), "Não foi possível liberar a NF para trasmissão: " + e.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
                // log da ação
                SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENVIAR, notaAtualizada.getFatura(), "Não foi possível liberar a NF para trasmissão: " + e.getMessage());
            }
        }
    }

    @Override
    public void closeWindow() {
        Globals.getMainStage().close();
    }
}
