package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdRemessaCliente;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.view.expedicao.VSdDistribuicaoMostruario;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.List;

public class GerenciarMostruariosController extends WindowBase {

    public GerenciarMostruariosController(String title, Image icone) {
        super(title, icone);
    }

    protected VSdDistribuicaoMostruario getProdutoCodigo(String colecao, String codigo) {

        return new FluentDao().selectFrom(VSdDistribuicaoMostruario.class)
                        .where(eb -> eb
                                .equal("colvenda", colecao)
                                .equal("codigo", codigo)
                                .equal("status", "P"))
                        .findAny();
    }

    protected List<SdMostrRep> getMostruarios(Object[] codigoColecao, Object[] marcasProduto, Object[] representantes) {
        List<SdMostrRep> mostruarios = (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .isIn("colvenda", codigoColecao, TipoExpressao.AND, b -> codigoColecao.length > 0)
                        .isIn("marca", marcasProduto, TipoExpressao.AND, b -> marcasProduto.length > 0)
                        .isIn("codrep.codRep", representantes, TipoExpressao.AND, b -> representantes.length > 0))
                .resultList();

        mostruarios.forEach(mostruario -> {
            mostruario.getItens().forEach(SdMostrRepItens::refresh);
            if (mostruario.getItens().stream().anyMatch(produto -> produto.getStatus().equals("P"))
                && mostruario.getRemessa() == null) {
                mostruario.setStatusExpedicao("Em separação");
            } else if (mostruario.getItens().stream().anyMatch(produto -> produto.getStatus().equals("P"))
                    && mostruario.getRemessa() != null) {
                mostruario.setStatusExpedicao("Em leitura");
            } else if (mostruario.getItens().stream().noneMatch(produto -> produto.getStatus().equals("P"))
                    && mostruario.getRemessa() != null) {
                SdRemessaCliente remessa = new FluentDao().selectFrom(SdRemessaCliente.class)
                        .where(eb -> eb.equal("remessa", mostruario.getRemessa()))
                        .singleResult();

                if (remessa.getStatus().getCodigo().equals("T")) {
                    mostruario.setStatusExpedicao("Em expedição");
                } else if (remessa.getStatus().getCodigo().equals("E")) {
                    mostruario.setStatusExpedicao("Em faturamento");
                } else if (remessa.getStatus().getCodigo().equals("F")) {
                    mostruario.setStatusExpedicao("Faturado");
                }
            }
        });

        return mostruarios;
    }

    @Override
    public void closeWindow() {

    }
}
