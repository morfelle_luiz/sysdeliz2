package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ClientesEtiqueta;
import sysdeliz2.models.sysdeliz.SdCliDullius;
import sysdeliz2.models.sysdeliz.SdCliPaludo;
import sysdeliz2.models.sysdeliz.SdCliRFStore;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class ImpressaoEtiquetasClientesController extends WindowBase {

    protected List<PedIten> itensPedido = new ArrayList<>();
    protected Pedido pedidoSelecionado;

    protected final Map<String, String> mapClientes = new HashMap<>();

    {
        mapClientes.put("P", "Paludo");
        mapClientes.put("RF", "RF Store");
    }

    public ImpressaoEtiquetasClientesController(String title, Image icone) {
        super(title, icone);
    }

    protected void buscarEtiquetas(String cliente, Pedido pedido, Object[] produtos, Object[] cores) {

        itensPedido.clear();
        pedidoSelecionado = pedido;

        Object[] listProdutos = produtos == null ? new String[]{} : produtos;
        Object[] listCores = cores == null ? new String[]{} : cores;

        List<PedIten> itens = pedido.getItens();

        if (listProdutos.length > 0) {
            itens = itens.stream().filter(it -> Arrays.stream(listProdutos).anyMatch(eb -> eb.equals(it.getId().getCodigo().getCodigo()))).collect(Collectors.toList());
        }
        if (listCores.length > 0) {
            itens = itens.stream().filter(it -> Arrays.stream(listCores).anyMatch(eb -> eb.equals(it.getId().getCor().getCor()))).collect(Collectors.toList());
        }

        if (cliente.equals("Paludo")) {
            buscarEtiquetas(SdCliPaludo.class, "id.codigo", "id.tamitem", "", itens);
        } else {
            buscarEtiquetas(SdCliRFStore.class, "referencia", "tam", "cor", itens);
        }
    }

    private void buscarEtiquetas(Class<? extends ClientesEtiqueta> classe, String codigoString, String tamString, String corString, List<PedIten> itens) {

        for (PedIten item : itens) {
            List<ClientesEtiqueta> etqsList = (List<ClientesEtiqueta>) new FluentDao()
                    .selectFrom(classe)
                    .where(it -> it
                            .equal(codigoString, item.getId().getCodigo().getCodigo())
                            .equal(tamString, item.getId().getTam()))
                    .resultList();
            if (etqsList != null && etqsList.size() > 0) {
                if (classe.equals(SdCliPaludo.class)) {
                    etqsList.forEach(etq -> itensPedido.add(new PedIten(item, ((SdCliPaludo) etq))));
                } else {
                    itensPedido.add(item);
                }
            }
        }
        itensPedido = itensPedido.stream().distinct().collect(Collectors.toList());

        if(classe.equals(SdCliPaludo.class)) {
            itensPedido.sort(Comparator.comparing((PedIten o) -> o.getId().getCodigo().getCodigo()).thenComparing(PedIten::getCorEtq).thenComparing((o1, o2) -> SortUtils.sortTamanhos(o1.getId().getTam(), o2.getId().getTam())));
        } else {
            itensPedido.sort(Comparator.comparing((PedIten o) -> o.getId().getCodigo().getCodigo()).thenComparing(o -> o.getId().getCor().getCor()).thenComparing((o1, o2) -> SortUtils.sortTamanhos(o1.getId().getTam(), o2.getId().getTam())));
        }
    }

    @Override
    public void closeWindow() {

    }
}
