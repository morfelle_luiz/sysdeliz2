package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import org.apache.commons.collections.ComparatorUtils;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.EntradaPA.SdBarraCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InspecaoCaixasEntradaController extends WindowBase {

    protected FluentDao fluentDao;
    protected NativeDAO nativeDao;
    protected String deposito;

    public InspecaoCaixasEntradaController(String title, Image icone) {
        super(title, icone);
    }

    @Override
    public void closeWindow() {

    }
}
