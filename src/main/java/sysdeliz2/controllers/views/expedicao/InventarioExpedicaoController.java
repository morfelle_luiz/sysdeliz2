package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.Inventario.SdInventario;
import sysdeliz2.models.sysdeliz.Inventario.SdGradeItemInventario;
import sysdeliz2.models.sysdeliz.Inventario.SdItemInventario;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

public class InventarioExpedicaoController extends WindowBase {

    protected SdColaborador usuario;
    protected SdItemInventario itemInventarioSelecionado;
    protected SdInventario inventarioSelecionado;
    protected final ListProperty<SdGradeItemInventario> gradeInventarioBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    public InventarioExpedicaoController(String title, Image icone) {
        super(title, icone, null, true);
    }

    protected SdColaborador getUsuario(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
//                        .equal("codigoFuncao", 21)
                        .equal("ativo", true))
                .singleResult();
    }

    @Override
    public void closeWindow() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja fechar?");
                message.showAndWait();
            }).value.get())) {
            JPAUtils.destroy();
            System.exit(0);
        }
    }
}
