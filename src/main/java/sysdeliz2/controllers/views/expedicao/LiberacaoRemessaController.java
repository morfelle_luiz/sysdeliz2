package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class LiberacaoRemessaController extends WindowBase {

    protected List<SdRemessaCliente> remessas = new ArrayList<>();
    protected Empresa empresa = new FluentDao().selectFrom(Empresa.class).where(eb -> eb.equal("emppat", "_001")).singleResult();
    protected SdParametros _GERA_DIFAL = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", "1")).singleResult();
    protected SdStatusRemessa statusRemessaFaturada = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "F")).singleResult();
    protected SdStatusRemessa statusRemessaColetaFinalizada = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "T")).singleResult();
    protected List<Receber> parcelasFaturamento = new ArrayList<>();
    protected List<RecCom> dupsRepFaturamento = new ArrayList<>();
    protected List<Contabil> contabilFaturamento = new ArrayList<>();
    protected Nota tempNota = null;

    public LiberacaoRemessaController() {
    }

    public LiberacaoRemessaController(String title, Image icone) {
        super(title, icone);
    }

    protected void getRemessas(Object[] clientes, String status, LocalDate inicioEmissao, LocalDate fimEmissao, String boletoImpresso) {
//        JPAUtils.getEntityManager().clear();
        JPAUtils.clearEntitys(remessas);
        remessas = (List<SdRemessaCliente>) new FluentDao().selectFrom(SdRemessaCliente.class)
                .where(eb -> eb
                        .isIn("codcli.codcli", clientes, TipoExpressao.AND, b -> clientes.length > 0)
                        .equal("status.exibeLiberacao", true, TipoExpressao.AND, b -> status.equals("0"))
                        .equal("status.codigo", status, TipoExpressao.AND, b -> !status.equals("0"))
                        .between("dtFatura", inicioEmissao, fimEmissao, TipoExpressao.AND, b -> status.equals("F"))
                        .equal("codcli.sitDup.codigo", "26", TipoExpressao.AND, b -> boletoImpresso.equals("S"))
                        .notEqual("codcli.sitDup.codigo", "26", TipoExpressao.AND, b -> boletoImpresso.equals("N")))
                .resultList();
    }

    private Condicao getCondicaoAtende(List<Condicao> condicaos, BigDecimal valorLiquidoNf) {
        Condicao condicaoAtende = null;
        BigDecimal valorParcela = BigDecimal.ZERO;
        for (Condicao condicao : condicaos) {
            valorParcela = valorLiquidoNf.divide(new BigDecimal(condicao.getNrpar()), 2, RoundingMode.UP);
            if (valorParcela.compareTo(empresa.getEmpminfat()) >= 0) {
                condicaoAtende = condicao;
                break;
            }
        }

        return condicaoAtende;
    }

    protected String getCondicaoFaturamento(SdRemessaCliente remessa, List<SdPedidosRemessa> pedidosNf, Nota nfPedido) {
        BigDecimal valorLiquidoNf = nfPedido.getValor();
        AtomicReference<String> atmCondicaoPedido = new AtomicReference<>();
        AtomicReference<String> atmCondicaoFaturamento = new AtomicReference<>();
        AtomicReference<Pedido> atmPedido = new AtomicReference<>();
        AtomicReference<SdPedidosRemessa> atmPedidoRemessa = new AtomicReference<>();
        AtomicReference<String> atmSituacaoLiberada = new AtomicReference<>("N");

        // validação do faturamento com a condição do pedido
        pedidosNf.stream().findFirst().ifPresent(pedido -> {
            atmCondicaoPedido.set(pedido.getId().getNumero().getPagto());
            atmPedido.set(pedido.getId().getNumero());
            atmPedidoRemessa.set(pedido);
        });
        String[] parcelas = atmCondicaoPedido.get().trim().split("(?<=\\G.{3})");

        // get da situação da duplicata
        AtomicReference<String> atmSitDupDuplicata = new AtomicReference<>();
        if (atmPedido.get().getCodcli().getSitDup() != null) {
            atmSitDupDuplicata.set(atmPedido.get().getCodcli().getSitDup().getCodigo());
        } else {
            atmSitDupDuplicata.set("25");
        }
        pedidosNf.stream().filter(pedido -> pedido.getId().getNumero().getSitDup() != null).findFirst().ifPresent(pedido -> {
            atmSitDupDuplicata.set(pedido.getId().getNumero().getSitDup());
        });
        // get de liberação de parcela minima para pedidos pagos com cartão ou pedidos de representante
        pedidosNf.stream().filter(pedido -> pedido.getId().getNumero().getSitDup() != null && pedido.getId().getNumero().getSitDup().matches("(19|12)")).findFirst().ifPresent(pedido -> {
            atmSituacaoLiberada.set("S");
        });

        // variáveis de controle das condições de parcelamento do faturamento
        Integer numeroParcelas = parcelas.length;
        BigDecimal valorParcela = valorLiquidoNf.divide(new BigDecimal(numeroParcelas), 2, RoundingMode.FLOOR);
        Integer diasParcela = parcelas.length > 1 ? Integer.parseInt(parcelas[1].trim()) - Integer.parseInt(parcelas[0].trim()) : Integer.parseInt(parcelas[0].trim());
        Integer prazoMedio = ((Double) Arrays.asList(parcelas).stream().mapToInt(s -> Integer.parseInt(s.trim())).average().getAsDouble()).intValue();
        Integer diasInicio = Integer.parseInt(parcelas[0].trim());

        // verificando se a condição do pedido atende a parcela mínima
        if (valorParcela.compareTo(empresa.getEmpminfat()) < 0
                && !remessa.isLiberaMinimo()
                && !atmPedido.get().getPeriodo().getPrazo().equals("B2C")
                && atmSituacaoLiberada.get().equals("N")) {
            Integer finalDiasParcela = diasParcela;
            Integer finalPrazoMedio = prazoMedio;
            Condicao condicaoFaturamento = null;

            //caso a condição do pedido não atende, verificando as condições cadastradas com o prazo médio e os dias de boleto igual da condição do pedido
            condicaoFaturamento = getCondicaoAtende((List<Condicao>) new FluentDao().selectFrom(Condicao.class)
                    .where(eb -> eb.equal("nrdia", finalDiasParcela)
                            .equal("prazoMedio", finalPrazoMedio))
                    .orderBy(Arrays.asList(new Ordenacao("desc", "nrpar"), new Ordenacao("desc", "nrdia")))
                    .resultList(), valorLiquidoNf);

            //caso ainda não foi encontrado uma condição que atenda o prazo médio e o número de dias, verifica todos as condições com o mesmo prazo médio
            if (condicaoFaturamento == null)
                condicaoFaturamento = getCondicaoAtende((List<Condicao>) new FluentDao().selectFrom(Condicao.class).where(eb -> eb.equal("prazoMedio", finalPrazoMedio))
                        .orderBy(Arrays.asList(new Ordenacao("desc", "nrpar"), new Ordenacao("desc", "nrdia")))
                        .resultList(), valorLiquidoNf);

            //caso ainda não foi encontrado uma condição que atenda o parcelamento mínimo, então o parcelamento deve ser colocado manualmente.
            if (condicaoFaturamento == null) {
                atmCondicaoFaturamento.set(null);
                return atmCondicaoFaturamento.get();
            }

            // atribuindo valores para as variáveis de controle do parcelamento do faturamento
            numeroParcelas = condicaoFaturamento.getNrpar();
            diasParcela = condicaoFaturamento.getNrdia();
            prazoMedio = condicaoFaturamento.getPrazoMedio();
            diasInicio = condicaoFaturamento.getPrazoini();
            valorParcela = valorLiquidoNf.divide(new BigDecimal(numeroParcelas), 2, RoundingMode.FLOOR);
            atmCondicaoFaturamento.set(condicaoFaturamento.getDescricao());
        } else if (valorParcela.compareTo(empresa.getEmpminfat()) < 0
                && remessa.isLiberaMinimo()) {
            numeroParcelas = 1;
            diasParcela = prazoMedio;
            diasInicio = prazoMedio;
            valorParcela = valorLiquidoNf.divide(new BigDecimal(numeroParcelas), 2, RoundingMode.FLOOR);
            atmCondicaoFaturamento.set(String.valueOf(prazoMedio));
        } else {
            atmCondicaoFaturamento.set(atmCondicaoPedido.get());
        }

        // gerar o receber
        parcelasFaturamento.clear();
        dupsRepFaturamento.clear();
        if (nfPedido.getNatureza().isDupli()) {
            BigDecimal diferencaParcela = valorLiquidoNf.subtract(valorParcela.multiply(new BigDecimal(numeroParcelas)));
            for (int parcela = 1; parcela <= numeroParcelas; parcela++) {
                BigDecimal valorDuplicata = valorParcela.add(diferencaParcela);
                diferencaParcela = BigDecimal.ZERO;
                LocalDate dataBase = nfPedido.getDtemissao();
                if (atmPedidoRemessa.get().getDtBase() != null) {
                    dataBase = atmPedidoRemessa.get().getDtBase();
                }

                Receber duplicata = new Receber();
                duplicata.setFatura(nfPedido.getFatura());
                duplicata.setNumero(nfPedido.getFatura() + "/" + parcela);
                duplicata.setCodcli(nfPedido.getCodcli());
                duplicata.setCodrep(nfPedido.getCodrep());
                duplicata.setNumcx(1);
                duplicata.setCom1(atmPedido.get().getCom1());
                duplicata.setCom2(atmPedido.get().getCom2());
                duplicata.setTaxa(empresa.getEmptaxa());
                duplicata.setDtvencto(dataBase.plusDays((diasParcela * parcela) + (diasInicio - diasParcela)));
                duplicata.setDtemissao(nfPedido.getDtemissao());
                duplicata.setOriginal(duplicata.getDtvencto());
                duplicata.setValor(nfPedido.getValor());
                duplicata.setValor2(valorDuplicata);
                duplicata.setQuant(nfPedido.getPecas());
                duplicata.setPesob(nfPedido.getPesob());
                duplicata.setValorigin(duplicata.getValor2());
                duplicata.setSituacao(atmSitDupDuplicata.get());
                duplicata.setTipfat(nfPedido.getTipfat());
                duplicata.setBanco(atmPedido.get().getCodcli().getBanco() != null ? atmPedido.get().getCodcli().getBanco().getBanco() : "341");
                duplicata.setClasse(atmPedido.get().getCodcli().getClasse() != null ? atmPedido.get().getCodcli().getClasse().getCodigo() : "0003");
                duplicata.setPedido(atmPedido.get().getNumero());
                duplicata.setStatus("N");
                duplicata.setDtprevisao(duplicata.getDtvencto());
                duplicata.setHistorico("0301");

                RecComPK pkRecCom = new RecComPK();
                pkRecCom.setCodrep(duplicata.getCodrep());
                pkRecCom.setNumero(duplicata.getNumero());
                RecCom recCom = new RecCom();
                recCom.setId(pkRecCom);
                recCom.setCom1(duplicata.getCom1());
                recCom.setCom2(duplicata.getCom2());

                dupsRepFaturamento.add(recCom);
                parcelasFaturamento.add(duplicata);
            }
        }

        // gerar contabilização
        contabilFaturamento.clear();
        if (nfPedido.getNatureza().getNatCont() != null) {
            List<NatConta> regrasContabil = (List<NatConta>) new FluentDao().selectFrom(NatConta.class).where(eb -> eb.equal("id.codigo", nfPedido.getNatureza().getNatCont())).orderBy("id.tipo", OrderType.DESC).resultList();
            AtomicInteger ordem = new AtomicInteger(10);
            AtomicReference<String> lancamento = new AtomicReference<>(Globals.getProximoCodigo("CONTACOR", "LANCAMENTO"));
            regrasContabil.forEach(regra -> {
                ContabilPK pkContabil = new ContabilPK();
                pkContabil.setOrdem(ordem.getAndAdd(10));
                pkContabil.setLancamento(lancamento.get());
                Contabil contabil = new Contabil();
                contabil.setId(pkContabil);
                contabil.setData(nfPedido.getDtemissao());
                contabil.setTipo("SF");
                contabil.setDocto(nfPedido.getFatura());
                contabil.setNumero(nfPedido.getFatura());
                contabil.setCodcli(nfPedido.getCodcli());
                contabil.setDatalan(contabil.getData());
                switch (regra.getId().getTipo()) {
                    case "VF":
                        contabil.setValor(nfPedido.getValor().subtract(nfPedido.getValipi()));
                        contabil.setOperacao("+");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(atmPedido.get().getCodcli().getContac().getCodigo());
                        contabil.setObservacao("NF." + nfPedido.getFatura() + " de " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContac());
                        break;
                    case "IC":
                        contabil.setValor(nfPedido.getValicms());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "IP":
                        contabil.setValor(nfPedido.getValipi());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "PI":
                        contabil.setValor(nfPedido.getValpis());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "CO":
                        contabil.setValor(nfPedido.getValcofins());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "ID":
                        contabil.setValor(nfPedido.getValicmsdest());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                }
                contabilFaturamento.add(contabil);
            });
        }

        return atmCondicaoFaturamento.get();
    }

    protected boolean getParcelamento(Condicao condicaoSelecionada, SdRemessaCliente remessa, List<SdPedidosRemessa> pedidosNf, Nota nfPedido) {
        BigDecimal valorLiquidoNf = nfPedido.getValor();
        AtomicReference<Pedido> atmPedido = new AtomicReference<>();
        AtomicReference<SdPedidosRemessa> atmPedidoRemessa = new AtomicReference<>();
        AtomicReference<String> atmSituacaoLiberada = new AtomicReference<>("N");

        // validação do faturamento com a condição do pedido
        pedidosNf.stream().findFirst().ifPresent(pedido -> {
            atmPedido.set(pedido.getId().getNumero());
            atmPedidoRemessa.set(pedido);
        });

        // get da situação da duplicata
        AtomicReference<String> atmSitDupDuplicata = new AtomicReference<>(atmPedido.get().getCodcli().getSitDup() != null ? atmPedido.get().getCodcli().getSitDup().getCodigo() : "25");
        pedidosNf.stream().filter(pedido -> pedido.getId().getNumero().getSitDup() != null && pedido.getId().getNumero().getSitDup().equals("26")).findFirst().ifPresent(pedido -> {
            atmSitDupDuplicata.set(pedido.getId().getNumero().getSitDup());
        });
        // get de liberação de parcela minima para pedidos pagos com cartão ou pedidos de representante
        pedidosNf.stream().filter(pedido -> pedido.getId().getNumero().getSitDup() != null && pedido.getId().getNumero().getSitDup().matches("(19|12)")).findFirst().ifPresent(pedido -> {
            atmSituacaoLiberada.set("S");
        });


        // atribuindo valores para as variáveis de controle do parcelamento do faturamento
        Integer numeroParcelas = condicaoSelecionada.getNrpar();
        Integer diasParcela = condicaoSelecionada.getNrdia();
        Integer prazoMedio = condicaoSelecionada.getPrazoMedio();
        Integer diasInicio = condicaoSelecionada.getPrazoini();
        BigDecimal valorParcela = valorLiquidoNf.divide(new BigDecimal(numeroParcelas), 2, RoundingMode.FLOOR);

        if (valorParcela.compareTo(empresa.getEmpminfat()) < 0 && !remessa.isLiberaMinimo() && !atmPedido.get().getPeriodo().getPrazo().equals("B2C")
                && atmSituacaoLiberada.get().equals("N")) {
            return false;
        }

        // gerar o receber
        parcelasFaturamento.clear();
        dupsRepFaturamento.clear();
        if (nfPedido.getNatureza().isDupli()) {
            BigDecimal diferencaParcela = valorLiquidoNf.subtract(valorParcela.multiply(new BigDecimal(numeroParcelas)));
            for (int parcela = 1; parcela <= numeroParcelas; parcela++) {
                BigDecimal valorDuplicata = valorParcela.add(diferencaParcela);
                diferencaParcela = BigDecimal.ZERO;
                LocalDate dataBase = nfPedido.getDtemissao();
                if (atmPedidoRemessa.get().getDtBase() != null) {
                    dataBase = atmPedidoRemessa.get().getDtBase();
                }

                Receber duplicata = new Receber();
                duplicata.setFatura(nfPedido.getFatura());
                duplicata.setNumero(nfPedido.getFatura() + "/" + parcela);
                duplicata.setCodcli(nfPedido.getCodcli());
                duplicata.setCodrep(nfPedido.getCodrep());
                duplicata.setNumcx(1);
                duplicata.setCom1(atmPedido.get().getCom1());
                duplicata.setCom2(atmPedido.get().getCom2());
                duplicata.setTaxa(empresa.getEmptaxa());
                duplicata.setDtvencto(dataBase.plusDays((diasParcela * parcela) + (diasInicio - diasParcela)));
                duplicata.setDtemissao(nfPedido.getDtemissao());
                duplicata.setOriginal(duplicata.getDtvencto());
                duplicata.setValor(nfPedido.getValor());
                duplicata.setValor2(valorDuplicata);
                duplicata.setQuant(nfPedido.getPecas());
                duplicata.setPesob(nfPedido.getPesob());
                duplicata.setValorigin(duplicata.getValor2());
                duplicata.setSituacao(atmSitDupDuplicata.get());
                duplicata.setTipfat(nfPedido.getTipfat());
                duplicata.setBanco(atmPedido.get().getCodcli().getBanco() != null ? atmPedido.get().getCodcli().getBanco().getBanco() : "341");
                duplicata.setClasse(atmPedido.get().getCodcli().getClasse() != null ? atmPedido.get().getCodcli().getClasse().getCodigo() : "0003");
                duplicata.setPedido(atmPedido.get().getNumero());
                duplicata.setStatus("N");
                duplicata.setDtprevisao(duplicata.getDtvencto());
                duplicata.setHistorico("0301");

                RecComPK pkRecCom = new RecComPK();
                pkRecCom.setCodrep(duplicata.getCodrep());
                pkRecCom.setNumero(duplicata.getNumero());
                RecCom recCom = new RecCom();
                recCom.setId(pkRecCom);
                recCom.setCom1(duplicata.getCom1());
                recCom.setCom2(duplicata.getCom2());

                dupsRepFaturamento.add(recCom);
                parcelasFaturamento.add(duplicata);
            }
        }

        // gerar contabilização
        contabilFaturamento.clear();
        if (nfPedido.getNatureza().getNatCont() != null) {
            List<NatConta> regrasContabil = (List<NatConta>) new FluentDao().selectFrom(NatConta.class).where(eb -> eb.equal("id.codigo", nfPedido.getNatureza().getNatCont())).orderBy("id.tipo", OrderType.DESC).resultList();
            AtomicInteger ordem = new AtomicInteger(10);
            AtomicReference<String> lancamento = new AtomicReference<>(Globals.getProximoCodigo("CONTACOR", "LANCAMENTO"));
            regrasContabil.forEach(regra -> {
                ContabilPK pkContabil = new ContabilPK();
                pkContabil.setOrdem(ordem.getAndAdd(10));
                pkContabil.setLancamento(lancamento.get());
                Contabil contabil = new Contabil();
                contabil.setId(pkContabil);
                contabil.setData(nfPedido.getDtemissao());
                contabil.setTipo("SF");
                contabil.setDocto(nfPedido.getFatura());
                contabil.setNumero(nfPedido.getFatura());
                contabil.setCodcli(nfPedido.getCodcli());
                contabil.setDatalan(contabil.getData());
                switch (regra.getId().getTipo()) {
                    case "VF":
                        contabil.setValor(nfPedido.getValor().subtract(nfPedido.getValipi()));
                        contabil.setOperacao("+");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(atmPedido.get().getCodcli().getContac().getCodigo());
                        contabil.setObservacao("NF." + nfPedido.getFatura() + " de " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContac());
                        break;
                    case "IC":
                        contabil.setValor(nfPedido.getValicms());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "IP":
                        contabil.setValor(nfPedido.getValipi());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "PI":
                        contabil.setValor(nfPedido.getValpis());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "CO":
                        contabil.setValor(nfPedido.getValcofins());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                    case "ID":
                        contabil.setValor(nfPedido.getValicmsdest());
                        contabil.setOperacao("-");
                        contabil.setContac(regra.getContac());
                        contabil.setContad(regra.getContad());
                        contabil.setObservacao("Nossa Fatura Nr." + nfPedido.getFatura() + " p\\ " + atmPedido.get().getCodcli().getNome());
                        contabil.setContar(regra.getContad());
                        break;
                }
                contabilFaturamento.add(contabil);
            });
        }

        return true;
    }

    private String getCodigoNatureza(List<SdPedidosRemessa> pedidosNf, String referencia) {
        AtomicReference<Entidade> cliente = new AtomicReference<>(null);
        AtomicReference<String> tipoFaturamento = new AtomicReference<>(null);
        pedidosNf.stream().findFirst().ifPresent(pedido -> {
            cliente.set(pedido.getId().getNumero().getCodcli());
            tipoFaturamento.set(pedido.getId().getNumero().getPeriodo().getTipoFaturamento());
        });

        if (tipoFaturamento.get().equals("M") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC"))
            return "5912000"; // remessa de mostruario dentro do estado
        else if (tipoFaturamento.get().equals("M") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC"))
            return "6912000"; // remessa de mostruario fora do estado
        else if (tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("EX"))
            return "7949000"; // exterior e pedido de brinde ou bonificado
        else if (!tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("EX"))
            return referencia == null ? "7101000" : "7102000"; // exterior e venda
        else if (tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getInscricao().equals("ISENTO"))
            return "5910001"; // brinde ou bonificado dentro do estado e isendo de IE
        else if (tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getTipo().equals("1"))
            return "5910001"; // brinde ou bonificado dentro do estado e pessoa física
        else if (tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && !cliente.get().getInscricao().equals("ISENTO"))
            return "5910000"; // brinde ou bonificado dentro do estado e com IE
        else if (tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getInscricao().equals("ISENTO"))
            return "6910001"; // brinde ou bonificado fora do estado e isendo de IE
        else if (tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getTipo().equals("1"))
            return "6910001"; // brinde ou bonificado fora do estado e pessoa física
        else if (tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && !cliente.get().getInscricao().equals("ISENTO"))
            return "6910000"; // brinde ou bonificado fora do estado e com IE
        else if (!tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getInscricao().equals("ISENTO") && cliente.get().getTipo().equals("2"))
            return referencia == null ? "5101004" : "5102004"; // venda dentro do estado e isendo de IE
        else if (!tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && !cliente.get().getInscricao().equals("ISENTO") && cliente.get().getTipo().equals("2"))
            return referencia == null ? "5101000" : "5102000"; // venda dentro do estado e com IE
        else if (!tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getSuframa() != null && cliente.get().getTipo().equals("2"))
            return referencia == null ? "6109000" : "6110000"; // venda fora do estado e com SUFRAMA
        else if (!tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getInscricao().equals("ISENTO") && cliente.get().getTipo().equals("2"))
            return referencia == null ? "6101001" : "6102001"; // venda fora do estado e isendo de IE
        else if (!tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && !cliente.get().getInscricao().equals("ISENTO") && cliente.get().getTipo().equals("2"))
            return referencia == null ? "6101000" : "6102000"; // venda fora do estado e com IE
        else if (!tipoFaturamento.get().equals("B") && cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getTipo().equals("1"))
            return referencia == null ? "5101004" : "5102004"; // venda dentro do estado e pessoa física
        else if (!tipoFaturamento.get().equals("B") && !cliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC") && cliente.get().getTipo().equals("1"))
            return referencia == null ? "6107001" : "6108001"; // venda fora do estado e pessoa física


        return null;
    }

    private String getCaixasNf(String remessa, String pedidos) {
        try {
            List<Object> caixas = new NativeDAO().runNativeQuery(String.format("" +
                    "select listagg(numero, ', ') within group(order by numero) caixas\n" +
                    "  from (select distinct cai.numero\n" +
                    "          from sd_caixa_remessa_001 cai\n" +
                    "          join sd_itens_caixa_remessa_001 ite\n" +
                    "            on ite.numero = cai.numero\n" +
                    "         where cai.remessa = %s\n" +
                    "           and ite.pedido in\n" +
                    "               (select * from table(split('%s'))))", remessa, pedidos));

            return ((Map<String, Object>) caixas.get(0)).get("CAIXAS").toString();
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    protected Nota atualizaNotaFiscal(Nota nfPedido, List<SdPedidosRemessa> pedidosNf, List<SdCaixaRemessa> caixasNf, SdRemessaCliente remessa) throws BreakException, Exception {
        _GERA_DIFAL.refresh();

        // get do cliente e dados dos pedidos para o cabeçalho da NF
        AtomicReference<Entidade> atmCliente = new AtomicReference<>();
        AtomicReference<Pedido> atmPedido = new AtomicReference<>();
        pedidosNf.stream().findFirst().ifPresent(pedido -> {
            atmCliente.set(pedido.getId().getNumero().getCodcli());
            atmPedido.set(pedido.getId().getNumero());
        });

        // criando objeto nota temporária para configuração de faturamento
        tempNota = new Nota();
        // <editor-fold defaultstate="collapsed" desc="Migrando da NF da expedição">
        tempNota.setCodcli(nfPedido.getCodcli());
        tempNota.setFatura(nfPedido.getFatura());
        tempNota.setCodrep(nfPedido.getCodrep());
        tempNota.setImpresso(nfPedido.getImpresso());
        tempNota.setDocto(nfPedido.getDocto());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Definindo a natureza">
        String codigoCfop = getCodigoNatureza(pedidosNf, null);
        Natureza natureza = new FluentDao().selectFrom(Natureza.class).where(eb -> eb.equal("natureza", codigoCfop)).singleResult();
        tempNota.setNatureza(natureza);
        // testa se a aliquota da natureza é ZERO, se sim utiliza a cadastrada no estado, senão utiliza a da natureza
        tempNota.setAliquota(atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                ? BigDecimal.ZERO
                : natureza.getAliquota().compareTo(BigDecimal.ZERO) != 0 ? natureza.getAliquota() : atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Datas">
        tempNota.setDtemissao(LocalDate.now());
        tempNota.setDtsaida(LocalDate.now());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Migrados do Pedido">
        tempNota.setValfrete(new BigDecimal(pedidosNf.stream()
                .mapToDouble(pedido -> pedido.getId().getNumero().getFrete().doubleValue())
                .sum()));
        tempNota.setRedesp(atmPedido.get().getTabRedes());
        tempNota.setCif(atmPedido.get().getCif());
        tempNota.setRedespcif(atmPedido.get().getRedespCif());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Obs Nota">
        String concatRemessa = " Remessa: " + tempNota.getDocto();
        String concatItensCaixa = " Caixa: " + getCaixasNf(tempNota.getDocto(), pedidosNf.stream().map(pedido -> pedido.getId().getNumero().getNumero()).collect(Collectors.joining(","))) + concatRemessa;
        String concatMensagemNf = (remessa.getCodcli().getObsNota() != null ? remessa.getCodcli().getObsNota() + "\n" : "")
                + "Ped. Cli.: " + pedidosNf.stream().map(pedido -> pedido.getId().getNumero().getPedCli()).collect(Collectors.joining(", ")) + " | "
                + "Ped. ERP: " + pedidosNf.stream().map(pedido -> pedido.getId().getNumero().getNumero()).collect(Collectors.joining(", ")) + concatItensCaixa + "\n";
        tempNota.setMensagem(concatMensagemNf);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Itens NF">
        // desconto total do pedido
        BigDecimal descontoTotalPedidoRemessa = new BigDecimal(pedidosNf.stream()
                .mapToDouble(pedido -> pedido.getItens().stream()
                        .filter(item -> !item.getTipo().equals("MKT"))
                        .mapToDouble(item -> item.getGrade().stream()
                                .filter(grade -> grade.getQtdel() > 0)
                                .mapToDouble(grade -> pedido.getId().getNumero().getItens().stream()
                                        .filter(itemPed -> itemPed.getId().isEqual(grade.getId().getNumero(),
                                                grade.getId().getCodigo(),
                                                grade.getId().getCor(),
                                                grade.getId().getTam()))
                                        .mapToDouble(itemPed -> itemPed.getPreco()
                                                .multiply(pedido.getId().getNumero().getPercDesc()
                                                        .divide(new BigDecimal(100), 9, RoundingMode.FLOOR))
                                                .multiply(new BigDecimal(grade.getQtdel())).doubleValue())
                                        .sum())
                                .sum())
                        .sum())
                .sum()).setScale(2, RoundingMode.HALF_UP);
        // valor total da remessa
        BigDecimal valorTotalRemessa = new BigDecimal(pedidosNf.stream()
                .mapToDouble(pedido -> pedido.getItens().stream()
                        .filter(item -> !item.getTipo().equals("MKT"))
                        .mapToDouble(item -> item.getGrade().stream()
                                .filter(grade -> grade.getQtdel() > 0)
                                .mapToDouble(grade -> pedido.getId().getNumero().getItens().stream()
                                        .filter(itemPed -> itemPed.getId().isEqual(grade.getId().getNumero(),
                                                grade.getId().getCodigo(),
                                                grade.getId().getCor(),
                                                grade.getId().getTam()))
                                        .mapToDouble(itemPed -> itemPed.getPreco()
                                                .multiply(new BigDecimal(grade.getQtdel())).doubleValue())
                                        .sum())
                                .sum())
                        .sum())
                .sum());
        // valor total liquido da remessa
        BigDecimal valorLiquidoTotalRemessa = valorTotalRemessa.subtract(descontoTotalPedidoRemessa);
        // desconto total cadastrado na remessa
        BigDecimal descontoTotalRemessa = new BigDecimal(pedidosNf.stream()
                .mapToDouble(pedido -> pedido.getVlrDesc().doubleValue()).sum());
        // percentual desconto da remessa
        BigDecimal percentualDescontoRemessa = descontoTotalRemessa
                .divide(valorLiquidoTotalRemessa, 9, RoundingMode.FLOOR);

        AtomicInteger orderItem = new AtomicInteger(10);
        List<NotaIten> itensNf = new ArrayList<>();
        pedidosNf.forEach(pedido -> pedido.getItens().stream()
                .filter(item -> !item.getTipo().equals("MKT"))
                .forEach(item -> item.getGrade().stream()
                        .filter(grade -> grade.getQtdel() > 0)
                        .forEach(grade -> {
                            // get do item a partir da remessa nos itens do pedido para pegar dados de venda do item
                            PedIten itemPedido = pedido.getId().getNumero().getItens().stream()
                                    .filter(itePed -> itePed.getId().isEqual(grade.getId().getNumero(),
                                            grade.getId().getCodigo(),
                                            grade.getId().getCor(),
                                            grade.getId().getTam()))
                                    .findFirst().orElse(null);
                            if (itemPedido == null)
                                throw new BreakException("O item: " +
                                        grade.getId().getCodigo() + "/" + grade.getId().getCor() + "/" + grade.getId().getTam() +
                                        " da remessa não foi encontrado no pedido "+ grade.getId().getNumero() + " do cliente.");

                            // gerador da chave PK
                            NotaItenPK pkItem = new NotaItenPK();
                            pkItem.setFatura(tempNota.getFatura());
                            pkItem.setOrdem(orderItem.getAndAdd(10));
                            pkItem.setEmpenho("N");
                            // incluindo dados do item
                            NotaIten itemNF = new NotaIten();
                            itemNF.setId(pkItem);
                            itemNF.setQtde(new BigDecimal(grade.getQtdel()));
                            itemNF.setDatamvto(LocalDate.now());
                            itemNF.setPreco(itemPedido.getPreco());
                            itemNF.setValor(itemNF.getPreco().multiply(itemNF.getQtde()));
                            BigDecimal valorDescontoPedido = itemNF.getValor()
                                    .multiply(pedido.getId().getNumero().getPercDesc()
                                            .divide(new BigDecimal(100), 9, RoundingMode.FLOOR));
                            BigDecimal valorDescontoRemessa = itemNF.getValor().subtract(valorDescontoPedido)
                                    .multiply(percentualDescontoRemessa);
                            itemNF.setDesconto(valorDescontoPedido.add(valorDescontoRemessa).setScale(2, RoundingMode.HALF_UP));

                            // transient valor do frete
                            itemNF.setValfrete(itemNF.getValor().subtract(itemNF.getDesconto())
                                    .divide(valorLiquidoTotalRemessa, 9, RoundingMode.HALF_UP)
                                    .multiply(tempNota.getValfrete()).setScale(2, RoundingMode.HALF_UP));

                            // natureza e aliquota do item
                            Natureza naturezaItem = !itemPedido.getId().getCodigo().getCodSped().equals("00") // teste de produto revenda
                                    ? tempNota.getNatureza()
                                    : new FluentDao().selectFrom(Natureza.class).where(eb -> eb.equal("natureza", getCodigoNatureza(pedidosNf, itemPedido.getId().getCodigo().getCodigo()))).singleResult();
                            itemNF.setNatureza(naturezaItem);
                            itemNF.setPercicms(atmCliente.get().getSuframa() != null
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO
                                    : itemNF.getNatureza().getAliquota().compareTo(BigDecimal.ZERO) != 0 ? itemNF.getNatureza().getAliquota() : atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota());
                            //-------------------------------------------------

                            itemNF.setTipo(atmPedido.get().getPeriodo().getTipoFaturamento().equals("M") ? null : "1");
                            itemNF.setTamanho(grade.getId().getTam());
                            itemNF.setClafis(item.getId().getCodigo().getCodfis());
                            itemNF.setUnidade(item.getId().getCodigo().getUnidade());
                            itemNF.setClatrib(itemNF.getNatureza().getClatrib() != null
                                    ? itemNF.getNatureza().getClatrib() : item.getId().getCodigo().getCodtrib());
                            itemNF.setPedido(pedido.getId().getNumero().getNumero());
                            itemNF.setCor(grade.getId().getCor());
                            itemNF.setCodigo(grade.getId().getCodigo());
                            itemNF.setDescricao(item.getId().getCodigo().getDescricao());
                            itemNF.setDeposito(remessa.getDeposito());

                            itemNF.setLote("000000");
                            itemNF.setCusto(item.getId().getCodigo().getCusto());
                            itemNF.setTipoit("P");
                            itemNF.setOrdped(item.getOrdem());
                            itemNF.setOrigem(item.getId().getCodigo().getOrigem());
                            itemNF.setCodsped(item.getId().getCodigo().getCodsped());
                            // % de IPI, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setPercipi(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getNatureza().equals("7101000")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : item.getId().getCodigo().getIpi());
                            itemNF.setTribipi(itemNF.getNatureza().getTribIpi() != null
                                    ? itemNF.getNatureza().getTribIpi() : item.getId().getCodigo().getTribIpi());
                            // % de PIS, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setPercpis(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getNatureza().equals("7101000")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getNatureza().getAliqPis());
                            itemNF.setTribpis(itemNF.getNatureza().getTribPis() != null
                                    ? itemNF.getNatureza().getTribPis() : item.getId().getCodigo().getTribPis());
                            // % de COFINS, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setPerccofins(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getNatureza().equals("7101000")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getNatureza().getAliqCofins());
                            itemNF.setTribcofins(itemNF.getNatureza().getTribCofins() != null
                                    ? itemNF.getNatureza().getTribCofins() : item.getId().getCodigo().getTribCofins());
                            // Base ICMS, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setBaseicms(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getNatureza().equals("7101000")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getValor().subtract(itemNF.getDesconto()).add(itemNF.getValfrete()));
                            // Base PIS, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setBasepis(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getPisCofins().equals("N")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getValor().subtract(itemNF.getDesconto()).add(itemNF.getValfrete()));
                            // Base COFINS, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setBasecofins(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getPisCofins().equals("N")
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getValor().subtract(itemNF.getDesconto()).add(itemNF.getValfrete()));
                            // Base IPI, se cliente exportação, suframa (zona franca) ou pedido mostruário valor é ZERO
                            itemNF.setBaseipi(atmCliente.get().getSuframa() != null
                                    || itemNF.getNatureza().getNatureza().equals("7101000")
                                    || item.getId().getCodigo().getIpi().compareTo(BigDecimal.ZERO) == 0
                                    || atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")
                                    ? BigDecimal.ZERO : itemNF.getValor().subtract(itemNF.getDesconto()).add(itemNF.getValfrete()));
                            // Se cliente PF ou cliente PJ, ISENTO e fora de SC, calcular o DIFAL
                            if (_GERA_DIFAL.getValor().equals("S")) {
                                if (atmCliente.get().getTipo().equals("1") ||
                                        (atmCliente.get().getInscricao().equals("ISENTO")
                                                && !atmCliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC")))
                                    itemNF.setValicmsdest(itemNF.getBaseicms()
                                            .multiply(atmCliente.get().getCepEnt().getCidade().getCodEst().getAliqInt()
                                                    .subtract(itemNF.getPercicms())
                                                    .divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_UP));
                            }
                            //new FluentDao().merge(itemNF);
                            itensNf.add(itemNF);
                        })));

        // desconto efeito cartão de crédito
        BigDecimal totalDescontoItens = new BigDecimal(itensNf.stream()
                .mapToDouble(item -> item.getDesconto().doubleValue()).sum()).setScale(2, RoundingMode.HALF_UP);

        if (descontoTotalPedidoRemessa.add(descontoTotalRemessa).compareTo(totalDescontoItens) != 0) {
            itensNf.get(0).setDesconto(itensNf.get(0).getDesconto()
                    .add(descontoTotalPedidoRemessa.add(descontoTotalRemessa).subtract(totalDescontoItens)));


            if (atmCliente.get().getSuframa() == null && !tempNota.getNatureza().getNatureza().equals("7101000")
                    && !atmPedido.get().getPeriodo().getTipoFaturamento().equals("M")) {
                itensNf.get(0).setBaseicms(itensNf.get(0).getBaseicms()
                        .subtract(descontoTotalPedidoRemessa.add(descontoTotalRemessa).subtract(totalDescontoItens)));
                itensNf.get(0).setBasepis(itensNf.get(0).getBasepis()
                        .subtract(descontoTotalPedidoRemessa.add(descontoTotalRemessa).subtract(totalDescontoItens)));
                itensNf.get(0).setBasecofins(itensNf.get(0).getBasecofins()
                        .subtract(descontoTotalPedidoRemessa.add(descontoTotalRemessa).subtract(totalDescontoItens)));
            }
            if (itensNf.get(0).getBaseipi().compareTo(BigDecimal.ZERO) != 0)
                itensNf.get(0).setBaseipi(itensNf.get(0).getBaseipi()
                        .subtract(descontoTotalPedidoRemessa.add(descontoTotalRemessa).subtract(totalDescontoItens)));

            if (_GERA_DIFAL.getValor().equals("S")) {
                if (atmCliente.get().getTipo().equals("1") ||
                        (atmCliente.get().getInscricao().equals("ISENTO")
                                && !atmCliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC")))
                    itensNf.get(0).setValicmsdest(itensNf.get(0).getBaseicms()
                            .multiply(atmCliente.get().getCepEnt().getCidade().getCodEst().getAliqInt()
                                    .subtract(itensNf.get(0).getPercicms())
                                    .divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_UP));
            }
        }

        BigDecimal totalProdutos = new BigDecimal(itensNf.stream()
                .mapToDouble(item -> item.getValor().doubleValue()).sum()).setScale(2, RoundingMode.HALF_UP);
        totalDescontoItens = new BigDecimal(itensNf.stream()
                .mapToDouble(item -> item.getDesconto().doubleValue())
                .sum()).setScale(2, RoundingMode.HALF_UP);
        BigDecimal totalLiquido = totalProdutos.subtract(totalDescontoItens);
        BigDecimal totalIcms = new BigDecimal(itensNf.stream()
                .filter(item -> item.getId().getEmpenho().equals("N"))
                .mapToDouble(item -> (item.getValor().subtract(item.getDesconto()).add(item.getValfrete()))
                        .multiply((atmPedido.get().getPeriodo().getTipoFaturamento().equals("M") ? BigDecimal.ZERO :
                                item.getNatureza().getAliquota().compareTo(BigDecimal.ZERO) != 0 ? item.getNatureza().getAliquota() : atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota()).divide(new BigDecimal(100), 9, RoundingMode.FLOOR))
                        .setScale(2, RoundingMode.HALF_UP).doubleValue())
                .sum());
        BigDecimal totalPis = new BigDecimal(itensNf.stream()
                .filter(item -> item.getId().getEmpenho().equals("N"))
                .mapToDouble(item -> (item.getValor().subtract(item.getDesconto()).add(item.getValfrete()))
                        .multiply((atmCliente.get().getSuframa() != null ? empresa.getEmppis() : item.getNatureza().getAliqPis())
                                .divide(new BigDecimal(100), 9, RoundingMode.FLOOR))
                        .setScale(2, RoundingMode.HALF_UP).doubleValue())
                .sum());
        BigDecimal totalCofins = new BigDecimal(itensNf.stream()
                .filter(item -> item.getId().getEmpenho().equals("N"))
                .mapToDouble(item -> (item.getValor().subtract(item.getDesconto()).add(item.getValfrete()))
                        .multiply((atmCliente.get().getSuframa() != null
                                ? empresa.getEmpcofins() : item.getNatureza().getAliqCofins())
                                .divide(new BigDecimal(100), 9, RoundingMode.FLOOR))
                        .setScale(2, RoundingMode.HALF_UP).doubleValue())
                .sum());
        BigDecimal totalIpi = new BigDecimal(itensNf.stream()
                .filter(item -> item.getId().getEmpenho().equals("N") && item.getPercipi().compareTo(BigDecimal.ZERO) > 0)
                .mapToDouble(item -> (item.getValor().subtract(item.getDesconto()).add(item.getValfrete()))
                        .multiply((atmPedido.get().getPeriodo().getTipoFaturamento().equals("M") ? BigDecimal.ZERO :
                                ((VSdDadosProduto) new FluentDao().selectFrom(VSdDadosProduto.class).where(eb -> eb.equal("codigo", item.getCodigo())).singleResult()).getIpi()).divide(new BigDecimal(100), 9, RoundingMode.FLOOR))
                        .setScale(2, RoundingMode.HALF_UP).doubleValue())
                .sum());
        BigDecimal totalProdutosIpi = new BigDecimal(itensNf.stream()
                .filter(item -> item.getId().getEmpenho().equals("N") && item.getPercipi().compareTo(BigDecimal.ZERO) > 0)
                .mapToDouble(item -> (item.getValor().subtract(item.getDesconto()).add(item.getValfrete()))
                        .setScale(2, RoundingMode.HALF_UP).doubleValue())
                .sum());

        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (DESCONTO)">
        if (totalDescontoItens.compareTo(BigDecimal.ZERO) > 0) {
            // <editor-fold defaultstate="collapsed" desc="Desconto">
            // gerador da chave PK
            NotaItenPK pkItemDesc = new NotaItenPK();
            pkItemDesc.setFatura(tempNota.getFatura());
            pkItemDesc.setOrdem(orderItem.getAndAdd(10));
            pkItemDesc.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFDesc = new NotaIten();
            itemNFDesc.setId(pkItemDesc);
            itemNFDesc.setPreco(totalDescontoItens.divide(totalProdutos, 5, RoundingMode.UP).multiply(new BigDecimal(100)));
            itemNFDesc.setValor(totalDescontoItens);
            itemNFDesc.setTipo("D");
            itemNFDesc.setTamanho("S");
            itemNFDesc.setCor(atmPedido.get().getPercDesc().compareTo(BigDecimal.ZERO) > 0 ? "P" : "V");
            itemNFDesc.setCodigo("9999900");
            itemNFDesc.setDescricao(itemNFDesc.getCor().equals("P") ? "DESCONTO CONCEDIDO EM PERCENTUAL." : "DESCONTO CONCEDIDO EM VALOR.");
            //new FluentDao().merge(itemNFDesc);
            // incluindo no cabeçalho
            itensNf.add(itemNFDesc);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Sub Total">
            // gerador da chave PK
            NotaItenPK pkItemSubTotal = new NotaItenPK();
            pkItemSubTotal.setFatura(tempNota.getFatura());
            pkItemSubTotal.setOrdem(orderItem.getAndAdd(10));
            pkItemSubTotal.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFSubTotal = new NotaIten();
            itemNFSubTotal.setId(pkItemSubTotal);
            itemNFSubTotal.setValor(totalLiquido);
            itemNFSubTotal.setTipo("T");
            itemNFSubTotal.setTamanho(" ");
            itemNFSubTotal.setCor("T");
            itemNFSubTotal.setCodigo("9999900");
            itemNFSubTotal.setDescricao("SUB TOTAL...............:");
            //new FluentDao().merge(itemNFSubTotal);
            // incluindo no cabeçalho
            itensNf.add(itemNFSubTotal);
            // </editor-fold>
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (SUFRAMA)">
        if (atmCliente.get().getSuframa() != null) {
            // <editor-fold defaultstate="collapsed" desc="ICMS">
            // gerador da chave PK
            NotaItenPK pkItemIcms = new NotaItenPK();
            pkItemIcms.setFatura(tempNota.getFatura());
            pkItemIcms.setOrdem(orderItem.getAndAdd(10));
            pkItemIcms.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFIcms = new NotaIten();
            itemNFIcms.setId(pkItemIcms);
            itemNFIcms.setPreco(atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota());
            itemNFIcms.setValor(totalIcms);
            itemNFIcms.setTipo("D");
            itemNFIcms.setTamanho("N");
            itemNFIcms.setCor("P");
            itemNFIcms.setCodigo(String.valueOf(pkItemIcms.getOrdem()));
            itemNFIcms.setDescricao("DESCONTO ICMS");
            //new FluentDao().merge(itemNFIcms);
            // incluindo no cabeçalho
            itensNf.add(itemNFIcms);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PIS/COFINS">
            // gerador da chave PK
            NotaItenPK pkItemPisCofins = new NotaItenPK();
            pkItemPisCofins.setFatura(tempNota.getFatura());
            pkItemPisCofins.setOrdem(orderItem.getAndAdd(10));
            pkItemPisCofins.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFPisCofins = new NotaIten();
            itemNFPisCofins.setId(pkItemPisCofins);
            itemNFPisCofins.setPreco(empresa.getEmppis().add(empresa.getEmpcofins()));
            itemNFPisCofins.setValor(totalPis.add(totalCofins));
            itemNFPisCofins.setTipo("D");
            itemNFPisCofins.setTamanho("N");
            itemNFPisCofins.setCor("P");
            itemNFPisCofins.setCodigo(String.valueOf(pkItemPisCofins.getOrdem()));
            itemNFPisCofins.setDescricao("DESCONTO PIS/COFINS");
            //new FluentDao().merge(itemNFPisCofins);
            // incluindo no cabeçalho
            itensNf.add(itemNFPisCofins);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="IPI">
            if (tempNota.getValipi().compareTo(BigDecimal.ZERO) > 0) {
                // gerador da chave PK
                NotaItenPK pkItemIpi = new NotaItenPK();
                pkItemIpi.setFatura(tempNota.getFatura());
                pkItemIpi.setOrdem(orderItem.getAndAdd(10));
                pkItemIpi.setEmpenho("S");
                // incluindo dados do item
                NotaIten itemNFIpi = new NotaIten();
                itemNFIpi.setId(pkItemIpi);
                itemNFIpi.setPreco(BigDecimal.ZERO);
                itemNFIpi.setValor(totalIpi);
                itemNFIpi.setTipo("D");
                itemNFIpi.setTamanho("N");
                itemNFIpi.setCor("P");
                itemNFIpi.setCodigo(String.valueOf(pkItemIpi.getOrdem()));
                itemNFIpi.setDescricao("DESCONTO IPI");
                //new FluentDao().merge(itemNFIpi);
                // incluindo no cabeçalho
                itensNf.add(itemNFIpi);
            }
            // </editor-fold>
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (7101000)">
        if (tempNota.getNatureza().getNatureza().equals("7101000")) {
            // <editor-fold defaultstate="collapsed" desc="OBS 1">
            // gerador da chave PK
            NotaItenPK pkItemObs1 = new NotaItenPK();
            pkItemObs1.setFatura(tempNota.getFatura());
            pkItemObs1.setOrdem(orderItem.getAndAdd(10));
            pkItemObs1.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs1 = new NotaIten();
            itemNFObs1.setId(pkItemObs1);
            itemNFObs1.setTipo("S");
            itemNFObs1.setTamanho("N");
            itemNFObs1.setCor("O");
            itemNFObs1.setCodigo("9999900");
            itemNFObs1.setDescricao("Não incidência do IPI nos termos do Art. 18, inciso II, Decre");
            //new FluentDao().merge(itemNFObs1);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs1);
            tempNota.setMensagem(tempNota.getMensagem() + itemNFObs1.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 2">
            // gerador da chave PK
            NotaItenPK pkItemObs2 = new NotaItenPK();
            pkItemObs2.setFatura(tempNota.getFatura());
            pkItemObs2.setOrdem(orderItem.getAndAdd(10));
            pkItemObs2.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs2 = new NotaIten();
            itemNFObs2.setId(pkItemObs2);
            itemNFObs2.setTipo("S");
            itemNFObs2.setTamanho("N");
            itemNFObs2.setCor("O");
            itemNFObs2.setCodigo("9999900");
            itemNFObs2.setDescricao("Não incidência do PIS/COFINS, conforme Art. 5º da Lei nº 10.");
            //new FluentDao().merge(itemNFObs2);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs2);
            tempNota.setMensagem(tempNota.getMensagem() + itemNFObs2.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 3">
            // gerador da chave PK
            NotaItenPK pkItemObs3 = new NotaItenPK();
            pkItemObs3.setFatura(tempNota.getFatura());
            pkItemObs3.setOrdem(orderItem.getAndAdd(10));
            pkItemObs3.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs3 = new NotaIten();
            itemNFObs3.setId(pkItemObs3);
            itemNFObs3.setTipo("S");
            itemNFObs3.setTamanho("N");
            itemNFObs3.setCor("O");
            itemNFObs3.setCodigo("9999900");
            itemNFObs3.setDescricao("Não incidência do COFINS , conforme Art. 6º da Lei nº 10.833/");
            //new FluentDao().merge(itemNFObs3);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs3);
            tempNota.setMensagem(tempNota.getMensagem() + itemNFObs3.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 4">
            // gerador da chave PK
            NotaItenPK pkItemObs4 = new NotaItenPK();
            pkItemObs4.setFatura(tempNota.getFatura());
            pkItemObs4.setOrdem(orderItem.getAndAdd(10));
            pkItemObs4.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs4 = new NotaIten();
            itemNFObs4.setId(pkItemObs4);
            itemNFObs4.setTipo("S");
            itemNFObs4.setTamanho("N");
            itemNFObs4.setCor("O");
            itemNFObs4.setCodigo("9999900");
            itemNFObs4.setDescricao("Não incidência, conforme artigo 6°, inciso II do RICMS/SC - D");
            //new FluentDao().merge(itemNFObs4);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs4);
            tempNota.setMensagem(tempNota.getMensagem() + itemNFObs4.getDescricao() + "\n");
            // </editor-fold>
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Itens de Endereço de entrega">
        if (!atmCliente.get().getEndEnt().equals(atmCliente.get().getEndereco())
                || !atmCliente.get().getBairroEnt().equals(atmCliente.get().getBairro())
                || !atmCliente.get().getNumEnt().equals(atmCliente.get().getNumEnd())
                || !atmCliente.get().getCepEnt().getCep().equals(atmCliente.get().getCep().getCep())) {
            // <editor-fold defaultstate="collapsed" desc="OBS">
            // gerador da chave PK
            NotaItenPK pkItemObs1 = new NotaItenPK();
            pkItemObs1.setFatura(tempNota.getFatura());
            pkItemObs1.setOrdem(orderItem.getAndAdd(10));
            pkItemObs1.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs1 = new NotaIten();
            itemNFObs1.setId(pkItemObs1);
            itemNFObs1.setTipo("S");
            itemNFObs1.setTamanho("N");
            itemNFObs1.setCor("O");
            itemNFObs1.setCodigo("9999900");
            itemNFObs1.setDescricao("Endereço de entrega: " + atmCliente.get().getEndEnt() + ", " + atmCliente.get().getNumEnt()
                    + " - " + atmCliente.get().getCepEnt().getCidade().getNomeCid()
                    + " - " + atmCliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst()
                    + " - CEP " + atmCliente.get().getCepEnt().getCep());
            //new FluentDao().merge(itemNFObs1);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs1);
            //tempNota.setMensagem(tempNota.getMensagem() + itemNFObs1.getDescricao() + "\n");
        }
        // </editor-fold>
        // incluindo no cabeçalho
        tempNota.getItens().addAll(itensNf);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Volumes e Qtdes">
        tempNota.setVolumes(caixasNf.size());
        tempNota.setPecas(new BigDecimal(pedidosNf.stream()
                .mapToInt(pedido -> pedido.getItens().stream()
                        .mapToInt(item -> item.getGrade().stream()
                                .filter(grade -> !grade.getTipo().equals("MKT") && grade.getQtdel() > 0)
                                .mapToInt(SdGradeItemPedRem::getQtdel)
                                .sum())
                        .sum())
                .sum()));
        tempNota.setPesob(new BigDecimal(caixasNf.stream().mapToDouble(caixa -> caixa.getPeso().doubleValue()).sum()));
//        tempNota.setPesol(new BigDecimal(caixasNf.stream().mapToDouble(caixa -> caixa.getPeso().doubleValue()).sum()).subtract(new BigDecimal(caixasNf.stream().mapToDouble(caixa -> atmPedido.get().getNumero().startsWith("W") ? 0.020 : 0.785).sum())));
        tempNota.setNrvolumes(String.valueOf(tempNota.getPecas().intValue()));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Valores e Base">
        tempNota.setValor(totalLiquido.add(totalIpi).add(tempNota.getValfrete()));
        tempNota.setValprodutos(totalProdutos);
        tempNota.setDesconto(totalDescontoItens);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Difal do cabeçalho">
        if (_GERA_DIFAL.getValor().equals("S")) {
            if (atmCliente.get().getTipo().equals("1") || (atmCliente.get().getInscricao().equals("ISENTO") && !atmCliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC")))
                tempNota.setValicmsdest(new BigDecimal(itensNf.stream().mapToDouble(item -> item.getValicmsdest().doubleValue()).sum()));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="IPI">
        tempNota.setBaseipi(totalProdutosIpi);
        tempNota.setValipi(totalIpi);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIS/Cofins/ICMS">
        tempNota.setBasecalc(atmPedido.get().getPeriodo().getTipoFaturamento().equals("M") ? BigDecimal.ZERO : totalLiquido.add(tempNota.getValfrete()));
        tempNota.setValicms(totalIcms);
        tempNota.setValpis(totalPis);
        tempNota.setValcofins(totalCofins);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Exceções SUFRAMA e 7101000">
        if (atmCliente.get().getAtividade() == null)
            throw new Exception("Cliente sem atividade cadastrada. Verificar o cadastro do cliente!");

        tempNota.setDesconto(tempNota.getDesconto().add(atmCliente.get().getSuframa() != null
                ? tempNota.getValipi().add(tempNota.getValpis()).add(tempNota.getValcofins()).add(tempNota.getValicms())
                : atmCliente.get().getAtividade().getCodigo().equals("0008") ? tempNota.getValipi() : BigDecimal.ZERO));
        if (atmCliente.get().getSuframa() != null) {
            tempNota.getItens().stream().filter(item -> item.getId().getEmpenho().equals("N")).forEach(item -> {
                Produto produto = new FluentDao().selectFrom(Produto.class).where(eb -> eb.equal("codigo", item.getCodigo())).singleResult();
                item.setDesconto(item.getDesconto()
                        .add(item.getValor().subtract(item.getDesconto()).multiply(empresa.getEmppis().divide(new BigDecimal(100), 5, RoundingMode.FLOOR)).setScale(2, RoundingMode.HALF_UP))
                        .add(item.getValor().subtract(item.getDesconto()).multiply(empresa.getEmpcofins().divide(new BigDecimal(100), 5, RoundingMode.FLOOR)).setScale(2, RoundingMode.HALF_UP))
                        .add(item.getValor().subtract(item.getDesconto()).multiply(tempNota.getAliquota().divide(new BigDecimal(100), 5, RoundingMode.FLOOR)).setScale(2, RoundingMode.HALF_UP))
                        .add(item.getValor().subtract(item.getDesconto()).multiply(produto.getPercIpi().divide(new BigDecimal(100), 5, RoundingMode.FLOOR)).setScale(2, RoundingMode.HALF_UP)));
            });
            tempNota.setValor(tempNota.getValprodutos().subtract(tempNota.getDesconto()));
            tempNota.setAliquota(BigDecimal.ZERO);
            tempNota.setBasecalc(BigDecimal.ZERO);
            tempNota.setValicms(BigDecimal.ZERO);
            tempNota.setValcofins(BigDecimal.ZERO);
            tempNota.setValpis(BigDecimal.ZERO);
            tempNota.setValipi(BigDecimal.ZERO);
        }
        if (tempNota.getNatureza().getNatureza().equals("7101000")) {
            tempNota.setValor(tempNota.getValor());
            tempNota.setDesconto(tempNota.getDesconto());
            tempNota.setBasecalc(BigDecimal.ZERO);
            tempNota.setValicms(BigDecimal.ZERO);
            tempNota.setValcofins(BigDecimal.ZERO);
            tempNota.setValpis(BigDecimal.ZERO);
            tempNota.setBaseipi(BigDecimal.ZERO);
            tempNota.setValipi(BigDecimal.ZERO);
        }
        // </editor-fold>

        //new FluentDao().merge(nfPedido);
        //JPAUtils.getEntityManager().refresh(nfPedido);
        return tempNota;
    }

    protected void deleteNotaRemessa(SdRemessaCliente item) throws SQLException {
        for (Nota nota : item.getPedidos().stream().map(SdPedidosRemessa::getNota).distinct().collect(Collectors.toList())) {
            // NativeDAO delete nota_001
            new NativeDAO().runNativeQueryUpdate(String.format("delete from nota_001 where fatura = '%s'", nota.getFatura()));
            new NativeDAO().runNativeQueryUpdate(String.format("update sd_caixa_remessa_001 set nota = '' where nota = '%s'", nota.getFatura()));
        }
    }

    protected void geradorDeItens(Nota nfPedido, List<SdPedidosRemessa> pedidosNf, List<SdCaixaRemessa> caixasNf, SdRemessaCliente remessa) {
        AtomicReference<Entidade> atmCliente = new AtomicReference<>();
        AtomicReference<Pedido> atmPedido = new AtomicReference<>();
        pedidosNf.stream().findFirst().ifPresent(pedido -> {
            atmCliente.set(pedido.getId().getNumero().getCodcli());
            atmPedido.set(pedido.getId().getNumero());
        });

        // <editor-fold defaultstate="collapsed" desc="Itens NF">
        AtomicInteger orderItem = new AtomicInteger(1);
        List<NotaIten> itensNf = new ArrayList<>();
        pedidosNf.forEach(pedido -> pedido.getItens().stream()
                .filter(item -> !item.getTipo().equals("MKT"))
                .forEach(item -> item.getGrade().stream()
                        .filter(grade -> grade.getQtdel() > 0)
                        .forEach(grade -> {
                            // gerador da chave PK
                            NotaItenPK pkItem = new NotaItenPK();
                            pkItem.setFatura(nfPedido.getFatura());
                            pkItem.setOrdem(orderItem.getAndIncrement());
                            pkItem.setEmpenho("N");
                            // incluindo dados do item
                            NotaIten itemNF = new NotaIten();
                            itemNF.setId(pkItem);
                            itemNF.setQtde(new BigDecimal(grade.getQtdel()));
                            itemNF.setPercipi(item.getId().getCodigo().getIpi());
                            itemNF.setDatamvto(LocalDate.now());
                            itemNF.setPreco(grade.getValor().add(grade.getDesconto()).divide(new BigDecimal(grade.getQtde())));
                            itemNF.setValor(grade.getValor().add(grade.getDesconto()));
                            itemNF.setTipo("1");
                            itemNF.setTamanho(grade.getId().getTam());
                            itemNF.setClafis(item.getId().getCodigo().getCodfis());
                            itemNF.setUnidade(item.getId().getCodigo().getUnidade());
                            itemNF.setClatrib(nfPedido.getNatureza() != null ? nfPedido.getNatureza().getClatrib() : item.getId().getCodigo().getCodtrib());
                            itemNF.setPedido(pedido.getId().getNumero().getNumero());
                            itemNF.setCor(grade.getId().getCor());
                            itemNF.setCodigo(grade.getId().getCodigo());
                            itemNF.setDescricao(item.getId().getCodigo().getDescricao());
                            itemNF.setDeposito(remessa.getDeposito());
                            itemNF.setPercicms(atmCliente.get().getSuframa() == null ? atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota() : BigDecimal.ZERO);
                            itemNF.setLote("000000");
                            itemNF.setNatureza(nfPedido.getNatureza());
                            itemNF.setCusto(item.getId().getCodigo().getCusto());
                            itemNF.setTipoit("P");
                            itemNF.setOrdped(item.getOrdem());
                            itemNF.setOrigem(item.getId().getCodigo().getOrigem());
                            itemNF.setCodsped(item.getId().getCodigo().getCodsped());
                            itemNF.setTribipi(nfPedido.getNatureza() != null ? nfPedido.getNatureza().getTribIpi() : item.getId().getCodigo().getTribIpi());
                            itemNF.setPercpis(nfPedido.getNatureza().getAliqPis());
                            itemNF.setTribcofins(nfPedido.getNatureza() != null ? nfPedido.getNatureza().getTribCofins() : item.getId().getCodigo().getTribCofins());
                            itemNF.setPerccofins(nfPedido.getNatureza().getAliqCofins());
                            itemNF.setBaseicms(atmCliente.get().getSuframa() != null || itemNF.getNatureza().getNatureza().equals("7101000") ? BigDecimal.ZERO : grade.getValor());
                            itemNF.setBasepis(atmCliente.get().getSuframa() != null || itemNF.getNatureza().getNatureza().equals("7101000") ? BigDecimal.ZERO : grade.getValor());
                            itemNF.setBasecofins(atmCliente.get().getSuframa() != null || itemNF.getNatureza().getNatureza().equals("7101000") ? BigDecimal.ZERO : grade.getValor());
                            itemNF.setBaseipi(atmCliente.get().getSuframa() != null || itemNF.getNatureza().getNatureza().equals("7101000") || item.getId().getCodigo().getIpi().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : grade.getValor());
                            itemNF.setDesconto(grade.getDesconto());
                            if (atmCliente.get().getTipo().equals("1") || (atmCliente.get().getInscEnt().equals("ISENTO") && !atmCliente.get().getCepEnt().getCidade().getCodEst().getId().getSiglaEst().equals("SC")))
                                itemNF.setValicmsdest(itemNF.getBaseicms().multiply(atmCliente.get().getCepEnt().getCidade().getCodEst().getAliqInt().subtract(itemNF.getPercicms()).multiply(new BigDecimal(100))));

                            new FluentDao().merge(itemNF);
                            itensNf.add(itemNF);
                        })));
        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (DESCONTO)">
        if (nfPedido.getDesconto().compareTo(BigDecimal.ZERO) > 0) {
            // <editor-fold defaultstate="collapsed" desc="Desconto">
            // gerador da chave PK
            NotaItenPK pkItemDesc = new NotaItenPK();
            pkItemDesc.setFatura(nfPedido.getFatura());
            pkItemDesc.setOrdem(orderItem.getAndIncrement());
            pkItemDesc.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFDesc = new NotaIten();
            itemNFDesc.setId(pkItemDesc);
            itemNFDesc.setPreco(nfPedido.getDesconto().divide(nfPedido.getValprodutos(), 5, RoundingMode.UP).multiply(new BigDecimal(100)));
            itemNFDesc.setValor(nfPedido.getDesconto());
            itemNFDesc.setTipo("D");
            itemNFDesc.setTamanho("S");
            itemNFDesc.setCor(atmPedido.get().getPercDesc().compareTo(BigDecimal.ZERO) > 0 ? "P" : "V");
            itemNFDesc.setCodigo("9999900");
            itemNFDesc.setDescricao(itemNFDesc.getCor().equals("P") ? "DESCONTO CONCEDIDO EM PERCENTUAL." : "DESCONTO CONCEDIDO EM VALOR.");
            new FluentDao().merge(itemNFDesc);
            // incluindo no cabeçalho
            itensNf.add(itemNFDesc);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Sub Total">
            // gerador da chave PK
            NotaItenPK pkItemSubTotal = new NotaItenPK();
            pkItemSubTotal.setFatura(nfPedido.getFatura());
            pkItemSubTotal.setOrdem(orderItem.getAndIncrement());
            pkItemSubTotal.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFSubTotal = new NotaIten();
            itemNFSubTotal.setId(pkItemSubTotal);
            itemNFSubTotal.setValor(nfPedido.getValprodutos().subtract(nfPedido.getDesconto()));
            itemNFSubTotal.setTipo("T");
            itemNFSubTotal.setTamanho(" ");
            itemNFSubTotal.setCor("T");
            itemNFSubTotal.setCodigo("9999900");
            itemNFSubTotal.setDescricao("SUB TOTAL...............:");
            new FluentDao().merge(itemNFSubTotal);
            // incluindo no cabeçalho
            itensNf.add(itemNFSubTotal);
            // </editor-fold>
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (SUFRAMA)">
        if (atmCliente.get().getSuframa() != null) {
            // <editor-fold defaultstate="collapsed" desc="ICMS">
            // gerador da chave PK
            NotaItenPK pkItemIcms = new NotaItenPK();
            pkItemIcms.setFatura(nfPedido.getFatura());
            pkItemIcms.setOrdem(orderItem.getAndIncrement());
            pkItemIcms.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFIcms = new NotaIten();
            itemNFIcms.setId(pkItemIcms);
            itemNFIcms.setPreco(atmCliente.get().getCepEnt().getCidade().getCodEst().getAliquota());
            itemNFIcms.setValor(nfPedido.getValicms());
            itemNFIcms.setTipo("D");
            itemNFIcms.setTamanho("N");
            itemNFIcms.setCor("P");
            itemNFIcms.setCodigo(String.valueOf(pkItemIcms.getOrdem()));
            itemNFIcms.setDescricao("DESCONTO ICMS");
            new FluentDao().merge(itemNFIcms);
            // incluindo no cabeçalho
            itensNf.add(itemNFIcms);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PIS/COFINS">
            // gerador da chave PK
            NotaItenPK pkItemPisCofins = new NotaItenPK();
            pkItemPisCofins.setFatura(nfPedido.getFatura());
            pkItemPisCofins.setOrdem(orderItem.getAndIncrement());
            pkItemPisCofins.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFPisCofins = new NotaIten();
            itemNFPisCofins.setId(pkItemPisCofins);
            itemNFPisCofins.setPreco(nfPedido.getNatureza().getAliqPis().add(nfPedido.getNatureza().getAliqCofins()));
            itemNFPisCofins.setValor(nfPedido.getValcofins().add(nfPedido.getValpis()));
            itemNFPisCofins.setTipo("D");
            itemNFPisCofins.setTamanho("N");
            itemNFPisCofins.setCor("P");
            itemNFPisCofins.setCodigo(String.valueOf(pkItemPisCofins.getOrdem()));
            itemNFPisCofins.setDescricao("DESCONTO PIS/COFINS");
            new FluentDao().merge(itemNFPisCofins);
            // incluindo no cabeçalho
            itensNf.add(itemNFPisCofins);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="IPI">
            if (nfPedido.getValipi().compareTo(BigDecimal.ZERO) > 0) {
                // gerador da chave PK
                NotaItenPK pkItemIpi = new NotaItenPK();
                pkItemIpi.setFatura(nfPedido.getFatura());
                pkItemIpi.setOrdem(orderItem.getAndIncrement());
                pkItemIpi.setEmpenho("S");
                // incluindo dados do item
                NotaIten itemNFIpi = new NotaIten();
                itemNFIpi.setId(pkItemIpi);
                itemNFIpi.setPreco(nfPedido.getValipi().divide(nfPedido.getValprodutos().subtract(nfPedido.getDesconto()), 5, RoundingMode.UP).multiply(new BigDecimal(100)));
                itemNFIpi.setValor(nfPedido.getValipi());
                itemNFIpi.setTipo("D");
                itemNFIpi.setTamanho("N");
                itemNFIpi.setCor("P");
                itemNFIpi.setCodigo(String.valueOf(pkItemIpi.getOrdem()));
                itemNFIpi.setDescricao("DESCONTO IPI");
                new FluentDao().merge(itemNFIpi);
                // incluindo no cabeçalho
                itensNf.add(itemNFIpi);
            }
            // </editor-fold>
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Itens de Totais (7101000)">
        if (nfPedido.getNatureza().getNatureza().equals("7101000")) {
            // <editor-fold defaultstate="collapsed" desc="OBS 1">
            // gerador da chave PK
            NotaItenPK pkItemObs1 = new NotaItenPK();
            pkItemObs1.setFatura(nfPedido.getFatura());
            pkItemObs1.setOrdem(orderItem.getAndIncrement());
            pkItemObs1.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs1 = new NotaIten();
            itemNFObs1.setId(pkItemObs1);
            itemNFObs1.setTipo("S");
            itemNFObs1.setTamanho("N");
            itemNFObs1.setCor("O");
            itemNFObs1.setCodigo("9999900");
            itemNFObs1.setDescricao("Não incidência do IPI nos termos do Art. 18, inciso II, Decre");
            new FluentDao().merge(itemNFObs1);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs1);
            nfPedido.setMensagem(nfPedido.getMensagem() + itemNFObs1.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 2">
            // gerador da chave PK
            NotaItenPK pkItemObs2 = new NotaItenPK();
            pkItemObs2.setFatura(nfPedido.getFatura());
            pkItemObs2.setOrdem(orderItem.getAndIncrement());
            pkItemObs2.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs2 = new NotaIten();
            itemNFObs2.setId(pkItemObs2);
            itemNFObs2.setTipo("S");
            itemNFObs2.setTamanho("N");
            itemNFObs2.setCor("O");
            itemNFObs2.setCodigo("9999900");
            itemNFObs2.setDescricao("Não incidência do PIS/COFINS, conforme Art. 5º da Lei nº 10.");
            new FluentDao().merge(itemNFObs2);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs2);
            nfPedido.setMensagem(nfPedido.getMensagem() + itemNFObs2.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 3">
            // gerador da chave PK
            NotaItenPK pkItemObs3 = new NotaItenPK();
            pkItemObs3.setFatura(nfPedido.getFatura());
            pkItemObs3.setOrdem(orderItem.getAndIncrement());
            pkItemObs3.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs3 = new NotaIten();
            itemNFObs3.setId(pkItemObs3);
            itemNFObs3.setTipo("S");
            itemNFObs3.setTamanho("N");
            itemNFObs3.setCor("O");
            itemNFObs3.setCodigo("9999900");
            itemNFObs3.setDescricao("Não incidência do COFINS , conforme Art. 6º da Lei nº 10.833/");
            new FluentDao().merge(itemNFObs3);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs3);
            nfPedido.setMensagem(nfPedido.getMensagem() + itemNFObs3.getDescricao() + "\n");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="OBS 4">
            // gerador da chave PK
            NotaItenPK pkItemObs4 = new NotaItenPK();
            pkItemObs4.setFatura(nfPedido.getFatura());
            pkItemObs4.setOrdem(orderItem.getAndIncrement());
            pkItemObs4.setEmpenho("S");
            // incluindo dados do item
            NotaIten itemNFObs4 = new NotaIten();
            itemNFObs4.setId(pkItemObs4);
            itemNFObs4.setTipo("S");
            itemNFObs4.setTamanho("N");
            itemNFObs4.setCor("O");
            itemNFObs4.setCodigo("9999900");
            itemNFObs4.setDescricao("Não incidência, conforme artigo 6°, inciso II do RICMS/SC - D");
            new FluentDao().merge(itemNFObs4);
            // incluindo no cabeçalho
            itensNf.add(itemNFObs4);
            nfPedido.setMensagem(nfPedido.getMensagem() + itemNFObs4.getDescricao() + "\n");
            // </editor-fold>
        }
        // </editor-fold>
        // incluindo no cabeçalho
        nfPedido.getItens().addAll(itensNf);
        // </editor-fold>
    }

    protected void devolverNf(List<SdCaixaRemessa> caixasNf) {
        for (SdCaixaRemessa caixaRemessa : caixasNf) {
            for (SdItensCaixaRemessa item : caixaRemessa.getItens()) {
                item.setStatus("D");
                new FluentDao().merge(item);
            }
        }
    }

    @Override
    public void closeWindow() {

    }
}
