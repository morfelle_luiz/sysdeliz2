package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.EntradaPA.SdBarraCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdLotePa;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.*;
import sysdeliz2.models.view.expedicao.VSdDistribuicaoMostruario;
import sysdeliz2.models.view.expedicao.VSdEstoqueProdutoCor;
import sysdeliz2.models.view.expedicao.VSdProdutoReposicao;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MinutaController {
    private static final Logger logger = Logger.getLogger(MinutaController.class);

    private LocalDate dtMinuta;
    private String transportadora;
    protected String usuario;
    protected FluentDao fluentDao;
    protected NativeDAO nativeDao;
    protected SdLotePa loteEscolhido;
    protected String deposito = "";
    // <editor-fold defaultstate="collapsed" desc="Bean Criar Minuta">
    protected final ListProperty<SdMinutaExpedicao> beanNotasMinuta = new SimpleListProperty<>();
    protected final ListProperty<VSdNfsMinuta> beanNfsParaLeitura = new SimpleListProperty<>();
    protected final IntegerProperty beanVolumesLidos = new SimpleIntegerProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Bean Consulta Caixa">
    protected final StringProperty beanNfConsultaCaixa = new SimpleStringProperty();
    protected final StringProperty beanRemessaConsultaCaixa = new SimpleStringProperty();
    protected final StringProperty beanClienteConsultaCaixa = new SimpleStringProperty();
    protected final StringProperty beanVolumeConsultaCaixa = new SimpleStringProperty();
    protected final StringProperty beanDtMinutaConsultaCaixa = new SimpleStringProperty();
    protected final StringProperty beanTranspConsultaCaixa = new SimpleStringProperty();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Bean Minuta Manual">
    protected final ListProperty<VSdNfsMinuta> beanNfsMinutaManual = new SimpleListProperty<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="BeanCaixasPa">
    protected final IntegerProperty qtdeLidaCaixaPa = new SimpleIntegerProperty(0);
    protected final IntegerProperty totalCaixaPa = new SimpleIntegerProperty(0);
    protected final IntegerProperty saldoCaixaPa = new SimpleIntegerProperty(0);
    protected final IntegerProperty inspecaoCaixaPa = new SimpleIntegerProperty(0);
    // </editor-fold>

    public MinutaController() {

    }

    protected List<SdMinutaExpedicao> carregaMinutaExistente() {
        this.dtMinuta = LocalDate.now();

        return (List<SdMinutaExpedicao>) new FluentDao().selectFrom(SdMinutaExpedicao.class)
                .where(eb -> eb
                        .equal("id.dtMinuta", LocalDate.now())
                        .equal("id.transportadora", transportadora))
                .resultList();
    }

    protected List<SdMinutaExpedicao> carregaNfsConferenciaMinuta(String transportadora) {
        return (List<SdMinutaExpedicao>) new FluentDao().selectFrom(SdMinutaExpedicao.class)
                .where(eb -> eb
                        .equal("id.dtMinuta", LocalDate.now())
                        .equal("id.transportadora", transportadora))
                .resultList();
    }

    protected List<VSdNfsMinuta> carregaNfsParaLeituraMinuta(String transportadora) {
        this.transportadora = transportadora;
        return (List<VSdNfsMinuta>) new FluentDao().selectFrom(VSdNfsMinuta.class)
                .where(eb -> eb
                        .isNotNull("remessa")
                        .equal("transport", transportadora))
                .resultList();
    }

    protected void consultaCaixa(String caixa) {
        beanNfConsultaCaixa.set("");
        beanRemessaConsultaCaixa.set("");
        beanClienteConsultaCaixa.set("");
        beanVolumeConsultaCaixa.set("");
        beanDtMinutaConsultaCaixa.set("");
        beanTranspConsultaCaixa.set("");

        SdCaixaRemessa caixaRemessa = new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("numero", caixa)).singleResult();

        if (caixaRemessa != null) {
            beanNfConsultaCaixa.set(caixaRemessa.getNota());
            beanRemessaConsultaCaixa.set(String.valueOf(caixaRemessa.getRemessa().getRemessa()));
            beanClienteConsultaCaixa.set(caixaRemessa.getRemessa().getCodcli().getRazaosocial());
            beanVolumeConsultaCaixa.set(String.valueOf(caixaRemessa.getVolume()));

            SdMinutaExpedicao minutaCaixa = new FluentDao().selectFrom(SdMinutaExpedicao.class).where(eb -> eb.like("caixas", caixa)).singleResult();
            if (minutaCaixa != null) {
                beanDtMinutaConsultaCaixa.set(StringUtils.toDateFormat(minutaCaixa.getId().getDtMinuta()));
                beanTranspConsultaCaixa.set(minutaCaixa.getId().getTransportadora());
            } else {
                beanTranspConsultaCaixa.set("Volume sem minuta!");
            }
        } else {
            beanNfConsultaCaixa.set("Caixa não encontrada!");
        }

    }

    protected void nfsMinutaManual(String transportadora) {
        beanNfsMinutaManual.set(FXCollections.observableList((List<VSdNfsMinuta>) new FluentDao()
                .selectFrom(VSdNfsMinuta.class)
                .where(eb -> eb
                        .isNull("remessa")
                        .equal("transport", transportadora))
                .resultList()));
    }

    protected void incluirVolumeMinuta(String caixa) {
        SdCaixaRemessa caixaRemessa = new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("numero", caixa)).singleResult();
        if (caixaRemessa == null || caixaRemessa.getNota() == null) {
            MessageBox.create(message -> {
                message.message("Nota da caixa não faturada ou caixa não existe.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
            return;
        }

        String notasDaRemessa = caixaRemessa.getRemessa().getPedidos().stream().filter(pedido -> pedido.getQtde() > 0 && pedido.getNota() != null)
                .map(pedido -> pedido.getNota().getFatura()).distinct().collect(Collectors.joining(","));
        List<Nota> notasRemessa = (List<Nota>) new FluentDao().selectFrom(Nota.class)
                .where(eb -> eb.isIn("fatura", notasDaRemessa.split(",")))
                .resultList();
        if (notasRemessa.stream().anyMatch(nota -> nota.getImpresso().equals("X"))) {
            MessageBox.create(message -> {
                message.message("Existem notas vinculadas a remessa desta caixa que não foram faturadas ainda.\nVerifique: " +
                        notasDaRemessa);
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }

        Nota notaCaixa = new FluentDao().selectFrom(Nota.class).where(eb -> eb.equal("fatura", caixaRemessa.getNota())).singleResult();
        if (notaCaixa.getTransport().equals(transportadora)) {
            if (caixaRemessa.getRemessa().getStatus().getCodigo().equals("F") && notaCaixa.getImpresso().equals("S")) {
                AtomicReference<VSdNfsMinuta> itemMinuta = new AtomicReference<>(null);
                itemMinuta.set(new FluentDao().selectFrom(VSdNfsMinuta.class).where(eb -> eb.equal("fatura", notaCaixa.getFatura()).equal("transport", notaCaixa.getTransport())).singleResult());

                if (itemMinuta.get() != null) {
                    SdMinutaExpedicao nfMinuta = new FluentDao().selectFrom(SdMinutaExpedicao.class)
                            .where(eb -> eb
                                    .equal("id.dtMinuta", dtMinuta)
                                    .equal("id.transportadora", transportadora)
                                    .equal("id.nota.fatura", notaCaixa.getFatura()))
                            .singleResult();
                    if (nfMinuta != null)
                        nfMinuta.refresh();

                    if (nfMinuta != null) {
                        if (!nfMinuta.getCaixas().contains(caixa)) {
                            if (nfMinuta.getPendente() > 0) {
                                nfMinuta.setVolumesLidos(nfMinuta.getVolumesLidos() + 1);
                                nfMinuta.setPendente(nfMinuta.getPendente() - 1);
                                nfMinuta.setCaixas(nfMinuta.getCaixas() == null ? "" : nfMinuta.getCaixas() + "," + caixa);
                                new FluentDao().merge(nfMinuta);
                            } else {
                                MessageBox.create(message -> {
                                    message.message("Já foram lidos todos os volumes da NF para a minuta.");
                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                    message.showFullScreenMobile();
                                });
                            }
                        } else {
                            MessageBox.create(message -> {
                                message.message("Este volume já foi lido na minuta.");
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.showFullScreenMobile();
                            });
                        }
                    } else {
                        SdMinutaExpedicaoPK pkMinuta = new SdMinutaExpedicaoPK();
                        pkMinuta.setNota(notaCaixa);
                        pkMinuta.setDtMinuta(dtMinuta);
                        pkMinuta.setTransportadora(transportadora);
                        nfMinuta = new SdMinutaExpedicao();
                        nfMinuta.setId(pkMinuta);
                        nfMinuta.setQtdeVolumes(notaCaixa.getVolumes());
                        nfMinuta.setVolumesLidos(1);
                        nfMinuta.setCaixas(caixa);
                        nfMinuta.setPendente(nfMinuta.getQtdeVolumes() - nfMinuta.getVolumesLidos());
                        nfMinuta = new FluentDao().merge(nfMinuta);
                        beanNotasMinuta.add(nfMinuta);
                    }
                    if (nfMinuta.getPendente() == 0) {
                        beanNfsParaLeitura.remove(itemMinuta.get());
                    } else {
                        itemMinuta.get().refresh();
                    }
                    beanVolumesLidos.set(beanVolumesLidos.add(1).get());
                } else {
                    MessageBox.create(message -> {
                        message.message("Esta NF já foi lido todos os volumes");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showFullScreenMobile();
                    });
                }
            } else {
                MessageBox.create(message -> {
                    message.message("A NF da caixa ainda não foi transmitida e impressa.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
            }
        } else {
            MessageBox.create(message -> {
                message.message("A NF desta caixa está vinculada a transportadora: " + notaCaixa.getTransport());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
        }
    }

    protected void incluirNfMinuta(VSdNfsMinuta nfParaMinuta, String transportadora) {
        Nota notaCaixa = new FluentDao().selectFrom(Nota.class)
                .where(eb -> eb.equal("fatura", nfParaMinuta.getFatura()))
                .singleResult();

        SdMinutaExpedicaoPK pkMinuta = new SdMinutaExpedicaoPK();
        pkMinuta.setNota(notaCaixa);
        pkMinuta.setDtMinuta(LocalDate.now());
        pkMinuta.setTransportadora(transportadora);
        SdMinutaExpedicao nfMinuta = new SdMinutaExpedicao();
        nfMinuta.setId(pkMinuta);
        nfMinuta.setQtdeVolumes(notaCaixa.getVolumes());
        nfMinuta.setVolumesLidos(notaCaixa.getVolumes());
        nfMinuta.setCaixas("MANUAL");
        nfMinuta.setPendente(nfMinuta.getQtdeVolumes() - nfMinuta.getVolumesLidos());
        new FluentDao().merge(nfMinuta);
        beanNfsMinutaManual.remove(nfParaMinuta);
    }

    private List<Object> getNfsVolumesZerado(String cliente, String remessa) throws SQLException {
        return new NativeDAO().runNativeQuery("" +
                "select fatura\n" +
                "  from nota_001\n" +
                " where codcli = '%s'\n" +
                "   and docto = '%s'\n" +
                "   and volumes = 0\n" +
                "   and impresso = 'S'\n" +
                "   and fatura not in (select mint.nota from sd_minuta_expedicao_001 mint)", cliente, remessa);
    }

    protected void incluirNfsVolumesZerado(String transportadora) throws SQLException {

        List<SdMinutaExpedicao> nfsMinutaHoje = (List<SdMinutaExpedicao>) new FluentDao().selectFrom(SdMinutaExpedicao.class)
                .where(eb -> eb
                        .equal("id.transportadora", transportadora)
                        .equal("id.dtMinuta", LocalDate.now()))
                .resultList();
        for (SdMinutaExpedicao nfMinuta : nfsMinutaHoje) {
            List<Object> nfsZeradas = getNfsVolumesZerado(nfMinuta.getId().getNota().getCodcli(),
                    nfMinuta.getId().getNota().getDocto());
            if (nfsZeradas.size() > 0) {
                for (Object nfZerada : nfsZeradas) {
                    Map<String, Object> nota = (Map<String, Object>) nfZerada;

                    SdMinutaExpedicao minutaNfZerada = new FluentDao().selectFrom(SdMinutaExpedicao.class)
                            .where(eb -> eb.equal("id.nota.fatura", nota.get("FATURA")))
                            .singleResult();

                    if (minutaNfZerada == null) {
                        Nota notaCaixa = new FluentDao().selectFrom(Nota.class).where(eb -> eb.equal("fatura", nota.get("FATURA"))).singleResult();
                        SdMinutaExpedicaoPK pkMinuta = new SdMinutaExpedicaoPK();
                        pkMinuta.setNota(notaCaixa);
                        pkMinuta.setDtMinuta(LocalDate.now());
                        pkMinuta.setTransportadora(notaCaixa.getTransport());
                        SdMinutaExpedicao nfParaMinuta = new SdMinutaExpedicao();
                        nfParaMinuta.setId(pkMinuta);
                        nfParaMinuta.setQtdeVolumes(notaCaixa.getVolumes());
                        nfParaMinuta.setVolumesLidos(notaCaixa.getVolumes());
                        nfParaMinuta.setCaixas("SEM VOLUMES");
                        nfParaMinuta.setPendente(nfParaMinuta.getQtdeVolumes() - nfParaMinuta.getVolumesLidos());
                        new FluentDao().persist(nfParaMinuta);
                    }
                }
            }
        }
    }

    protected void refreshBeansCaixasPa() {
        totalCaixaPa.set((int) loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> !eb.isIncompleta() && !eb.isPerdida())).count());
        qtdeLidaCaixaPa.set((int) loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> !eb.isIncompleta() && !eb.isPerdida() && eb.isRecebida())).count());
        inspecaoCaixaPa.set((int) loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(SdCaixaPA::isInspecao)).count());
        saldoCaixaPa.set(totalCaixaPa.get() - qtdeLidaCaixaPa.get() - inspecaoCaixaPa.get());
    }



    // distribuicao mostruario
    @Nullable
    protected VSdDistribuicaoMostruario getProdutoBarra(String barcode) {
        VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb.equal("barra28", barcode.substring(0, 6)))
                .singleResult();

        return produtoBarra == null ? null :
                new FluentDao().selectFrom(VSdDistribuicaoMostruario.class)
                        .where(eb -> eb
                                .equal("codigo", produtoBarra.getCodigo())
                                .equal("status", "P"))
                        .singleResult();
    }

    protected boolean separarProdutoCaixa(Integer mostruario, String colecao, String codigo) {
        SdMostrRepItens itemMostruario = new FluentDao().selectFrom(SdMostrRepItens.class)
                .where(eb -> eb
                        .equal("mostruario.mostruario", mostruario)
                        .equal("mostruario.colvenda", colecao)
                        .equal("codigo.codigo", codigo))
                .singleResult();
        if (itemMostruario.getStatus().equals("S"))
            return false;

        itemMostruario.setStatus("S");
        new FluentDao().merge(itemMostruario);
        SysLogger.addSysDelizLog("Distribuição Mostruário", TipoAcao.MOVIMENTAR, codigo,
                "Confirmando produto de mostruário na caixa " + mostruario +
                        " para a coleção " + colecao);
        return true;
    }

    // reposição estoque
    protected VSdDadosProdutoBarra getDadosProdutoBarra(String barcode) {
        return new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb.equal("barra28", barcode.substring(0, 6)))
                .singleResult();
    }

    protected VSdDadosProdutoBarra getDadosProdutoBarra(String codigo, String cor) {
        return new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb.equal("codigo", codigo).equal("cor", cor))
                .findAny();
    }

    protected VSdEstoqueProdutoCor getEstoqueProdutoBarra(String barcode) {
        return new FluentDao().selectFrom(VSdEstoqueProdutoCor.class)
                .where(eb -> eb.equal("barra28", barcode.substring(0, 6)))
                .singleResult();
    }

    protected VSdEstoqueProdutoCor getEstoqueProdutoBarra(String codigo, String cor) {
        return new FluentDao().selectFrom(VSdEstoqueProdutoCor.class)
                .where(eb -> eb.equal("codigo", codigo).equal("cor", cor))
                .findAny();
    }

    protected VSdDadosProduto getDadosProduto(String codigo) {
        return new FluentDao().selectFrom(VSdDadosProduto.class)
                .where(eb -> eb.equal("codigo", codigo))
                .singleResult();
    }

    protected void incluirAvisoReposicao(VSdDadosProdutoBarra dadosBarra, VSdDadosProduto dadosProduto) throws SQLException {
        SdReposicaoEstoque reposicao = new FluentDao().selectFrom(SdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("produto.codigo", dadosBarra.getCodigo())
                        .equal("cor", dadosBarra.getCor())
                        .equal("resolvido", false)
                        .equal("furoEstoque", false))
                .singleResult();

        if (reposicao == null) {
            new FluentDao().persist(new SdReposicaoEstoque(
                    dadosProduto,
                    dadosBarra.getCor(),
                    dadosBarra.getLocal(),
                    "A",
                    new FluentDao().selectFrom(SdColaborador.class).where(eb -> eb.equal("codigo", "662")).singleResult(),
                    null));
        }
    }

    protected List<VSdProdutoReposicao> getProdutosReposicao(String tipo) {
        return (List<VSdProdutoReposicao>) new FluentDao().selectFrom(VSdProdutoReposicao.class)
                .where(eb -> eb.equal("tipo", tipo))
                .resultList();
    }

    protected void informarFuroEstoque(VSdProdutoReposicao item) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "update sd_reposicao_estoque_001\n" +
                        "   set furo_estoque = 'S', dt_resolvido = sysdate, repositor = '662'\n" +
                        " where produto = '%s'\n" +
                        "   and cor = '%s'\n" +
                        "   and furo_estoque = 'N'\n" +
                        "   and resolvido = 'N'",
                item.getProduto(),
                item.getCor()));
    }

    protected void resolveReposicao(VSdProdutoReposicao item) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "update sd_reposicao_estoque_001\n" +
                        "   set resolvido = 'S', dt_resolvido = sysdate, repositor = '662'\n" +
                        " where produto = '%s'\n" +
                        "   and cor = '%s'\n" +
                        "   and resolvido = 'N'\n" +
                        "   and furo_estoque = 'N'",
                item.getProduto(),
                item.getCor()));
    }

    public String getMesByDate(LocalDate dtEntrega) {
        Instant instant = dtEntrega.atStartOfDay(ZoneId.systemDefault()).toInstant();
        return new SimpleDateFormat("MMM").format(Date.from(instant)).toUpperCase();
    }
}
