package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.view.VSdReposicaoEstoque;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ReposicaoEstoqueController extends WindowBase {
    
    protected SdColaborador repositor;
    protected final ListProperty<VSdReposicaoEstoque> beanReposicoes = new SimpleListProperty<>();
    protected final ListProperty<VSdReposicaoEstoque> beanFurosDeEstoque= new SimpleListProperty<>();
    
    public ReposicaoEstoqueController(String title, Image icone, Boolean closeButton) {
        super(title, icone, new String[]{"Apontamentos", "Furos Estoque"}, closeButton);
    }
    
    protected SdColaborador getColetor(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
                        .equal("codigoFuncao", 61)
                        .equal("ativo", true))
                .singleResult();
    }
    
    protected void resolveReposicao(VSdReposicaoEstoque item) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_reposicao_estoque_001 set resolvido = 'S', dt_resolvido = sysdate, repositor = '%s'\n" +
                "where codigo = %s", repositor == null ? Globals.getUsuarioLogado().getCodUsuarioTi() : repositor.getCodigo(), item.getCodigo().getCodigo()));
    }
    
    protected void getReposicoes() {
        beanReposicoes.set(FXCollections.observableList((List<VSdReposicaoEstoque>) new FluentDao().selectFrom(VSdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("codigo.resolvido", false)
                        .equal("codigo.furoEstoque", false))
                .orderBy(Arrays.asList(new Ordenacao("desc", "codigo.status"), new Ordenacao("asc", "codigo.dtemissao")))
                .resultList()));
    }
    
    protected void getFurosEstoque() {
        beanFurosDeEstoque.set(FXCollections.observableList((List<VSdReposicaoEstoque>) new FluentDao().selectFrom(VSdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("codigo.resolvido", false)
                        .equal("codigo.furoEstoque", true))
                .orderBy(Arrays.asList(new Ordenacao("desc", "codigo.status"), new Ordenacao("asc", "codigo.dtemissao")))
                .resultList()));
    }
    
    protected void informarFuroEstoque(VSdReposicaoEstoque item) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("update sd_reposicao_estoque_001 set furo_estoque = 'S', dt_resolvido = sysdate, repositor = '%s'\n" +
                "where codigo = %s", repositor == null ? Globals.getUsuarioLogado().getCodUsuarioTi() : repositor.getCodigo(), item.getCodigo().getCodigo()));
    }
    
    @Override
    public void closeWindow() {
        Globals.getMainStage().close();
    }
}
