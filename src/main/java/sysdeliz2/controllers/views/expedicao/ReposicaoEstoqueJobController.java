package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.collections.FXCollections;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.view.VSdReposicaoEstoque;

import java.util.Arrays;
import java.util.List;

public class ReposicaoEstoqueJobController implements Job {
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<VSdReposicaoEstoque> reposicoes = (List<VSdReposicaoEstoque>) new FluentDao().selectFrom(VSdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("codigo.resolvido", false)
                        .equal("codigo.furoEstoque", false))
                .orderBy(Arrays.asList(new Ordenacao("desc", "codigo.status"), new Ordenacao("asc", "codigo.dtemissao")))
                .resultList();
        
        List<VSdReposicaoEstoque> furosEstoque = (List<VSdReposicaoEstoque>) new FluentDao().selectFrom(VSdReposicaoEstoque.class)
                .where(eb -> eb
                        .equal("codigo.resolvido", false)
                        .equal("codigo.furoEstoque", true))
                .orderBy(Arrays.asList(new Ordenacao("desc", "codigo.status"), new Ordenacao("asc", "codigo.dtemissao")))
                .resultList();
        
        ListProperty<VSdReposicaoEstoque> list = (ListProperty<VSdReposicaoEstoque>) context.getTrigger().getJobDataMap().get("list");
        ListProperty<VSdReposicaoEstoque> furos = (ListProperty<VSdReposicaoEstoque>) context.getTrigger().getJobDataMap().get("furos");
        list.set(FXCollections.observableList(reposicoes));
        furos.set(FXCollections.observableList(furosEstoque));
    }
}
