package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdItensCaixaRemessa;
import sysdeliz2.models.sysdeliz.SdMostrRepBarraRetorno;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.view.expedicao.VSdDevolucaoMostRep;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class RetornoMostruarioController extends WindowBase {

    protected List<SdMostrRep> mostruarios = new ArrayList<>();
    protected List<SdMostrRep> mostruariosColecao = new ArrayList<>();
    protected List<VSdDevolucaoMostRep> itensDevolucao = new ArrayList<>();

    public RetornoMostruarioController() {
    }

    public RetornoMostruarioController(String title, Image icone, Boolean portatil) {
        super(title, icone, null, portatil);
    }

    protected void getMostruariosNaoDevolvidosRepresentante(String codrep) {
        JPAUtils.clearEntitys(mostruarios);
        mostruarios.clear();
        mostruarios = (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .equal("codrep.codRep", codrep)
                        .equal("status", "F"))
                .resultList();
    }

    protected void getMostruariosColecaoRepresentante(String codrep, String colecao) throws SQLException, FormValidationException {
        JPAUtils.clearEntitys(mostruariosColecao);
        mostruariosColecao.clear();
        mostruariosColecao = (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .equal("codrep.codRep", codrep)
                        .equal("colvenda", colecao))
                .orderBy(Arrays.asList(new Ordenacao("mostruario"), new Ordenacao("marca"), new Ordenacao("tipo")))
                .resultList();

        //assert mostruariosColecao != null;
        if (mostruariosColecao.size() > 0) {
            String pedidos = mostruariosColecao.stream().map(SdMostrRep::getNumero).distinct().collect(Collectors.joining("','"));
            AtomicReference<Boolean> pedidosDevolvidos = new AtomicReference<>(false);
            try {
                List<Object> nfsPedido3 = new NativeDAO().runNativeQuery("select distinct notafiscal from pedido3_001 where numero in ('%s')", pedidos);
                nfsPedido3.forEach(nfPedido3 -> {
                    String notafiscal = (String) ((Map<String, Object>) nfPedido3).get("NOTAFISCAL");
                    if (notafiscal == null) {
                        pedidosDevolvidos.set(true);
                    }
                });
                if (!pedidosDevolvidos.get()) {
                    mostruariosColecao.clear();
                    throw new FormValidationException("Notas fiscais de devolução não lançadas no sistema. Verifique as devoluções!");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }

        }
    }

    protected void carregarItensMostruario(String codrep, String colecao) {
        JPAUtils.clearEntitys(itensDevolucao);
        itensDevolucao.clear();
        itensDevolucao = (List<VSdDevolucaoMostRep>) new FluentDao().selectFrom(VSdDevolucaoMostRep.class)
                .where(eb -> eb
                        .equal("codrep", codrep)
                        .equal("colvenda", colecao))
                .resultList();
        //itensDevolucao.forEach(VSdDevolucaoMostRep::refresh); // excluir aqui
    }

    public void retornaPecaPedido(VSdDevolucaoMostRep itemDevolucao, SdItensCaixaRemessa barraCaixa) throws SQLException {
        PedIten itemPedido = new FluentDao().selectFrom(PedIten.class)
                .where(eb -> eb.isIn("id.numero", itemDevolucao.getPedidos().toArray())
                        .equal("id.codigo.codigo", itemDevolucao.getCodigo().getCodigo())
                        .equal("id.cor.cor", itemDevolucao.getCor().getCor())
                        .equal("id.tam", itemDevolucao.getTam())
                        .equal("id.numero", barraCaixa.getPedido()))
                .orderBy("id.numero", OrderType.ASC)
                .findFirst();
        itemPedido.setQtde(itemPedido.getQtde() - 1);
        itemPedido.setQtde_canc(itemPedido.getQtde_canc() + 1);
        itemPedido.setMotivo("28");
        new FluentDao().merge(itemPedido);
        new NativeDAO().runNativeQueryUpdate("delete from pedido3_001 where numero = '%s' and codigo = '%s' and cor = '%s' and tam = '%s' and caixa = '%s'",
                barraCaixa.getPedido(), barraCaixa.getCodigo().getCodigo(), barraCaixa.getCor(), barraCaixa.getTam(), barraCaixa.getId().getCaixa().getNumero());

        SysLogger.addSysDelizLog("Retorno Mostruario", TipoAcao.MOVIMENTAR, itemDevolucao.getCodrep(),
                "Cancelando item " + barraCaixa.getCodigo().getCodigo() + "-" + barraCaixa.getCor() + "-" + barraCaixa.getTam() + " no pedido " + barraCaixa.getPedido() + " e removendo da PEDIDO3 na caixa " + barraCaixa.getId().getCaixa().getNumero());
    }

    public SdItensCaixaRemessa devolverBarraParaEstoque(VSdDevolucaoMostRep itemDevolucao, SdItensCaixaRemessa barraCaixa) {
        if (barraCaixa == null || barraCaixa.getStatus().equals("D")) {
            barraCaixa = new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                    .where(eb -> eb.isIn("id.caixa.remessa.remessa", itemDevolucao.getRemessas().toArray())
                            .equal("status", "E")
                            .equal("codigo.codigo", itemDevolucao.getCodigo().getCodigo())
                            .equal("cor", itemDevolucao.getCor().getCor())
                            .equal("tam", itemDevolucao.getTam()))
                    .orderBy("id.caixa.remessa.remessa", OrderType.ASC)
                    .findFirst();
        }
        barraCaixa.setStatus("D");
        barraCaixa = new FluentDao().merge(barraCaixa);

        SysLogger.addSysDelizLog("Retorno Mostruario", TipoAcao.MOVIMENTAR, itemDevolucao.getCodrep(),
                "Marcando barra " + barraCaixa.getId().getBarra() + "  do item " + barraCaixa.getCodigo().getCodigo() + "-" + barraCaixa.getCor() + "-" + barraCaixa.getTam() + " no pedido " + barraCaixa.getPedido() + " como devolvida.");
        return barraCaixa;
    }

    public void devolverItemMostruario(VSdDevolucaoMostRep itemDevolucao, SdItensCaixaRemessa barraCaixa) {
        SdMostrRepItens itemMostruario = new FluentDao().selectFrom(SdMostrRepItens.class)
                .where(eb -> eb.isIn("mostruario.mostruario", itemDevolucao.getMostruarios().toArray())
                        .equal("mostruario.numero", barraCaixa.getPedido())
                        .equal("mostruario.tipo", itemDevolucao.getTipo())
                        .equal("codigo.codigo", itemDevolucao.getCodigo().getCodigo()))
                .orderBy("mostruario.mostruario", OrderType.ASC)
                .findFirst();
        itemMostruario.setQtdeDevoldida(itemMostruario.getQtdeDevoldida() + 1);
        if (itemMostruario.getQtdelido() - itemMostruario.getQtdeDevoldida() == 0)
            itemMostruario.setStatus("D");
        new FluentDao().merge(itemMostruario);
    }

    protected void verificaMostruariosDevolvidos() {
        mostruariosColecao.forEach(mostruario -> {
            if (mostruario.getItens().stream().allMatch(item -> item.getStatus().equals("D"))) {
                mostruario.setStatus("D");
                new FluentDao().merge(mostruario);
            }
        });
    }

    public void verificaMostruariosDevolvidos(VSdDevolucaoMostRep itemDevolucao) {
        List<SdMostrRep> mostRepresen = (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .equal("codrep.codRep", itemDevolucao.getCodrep())
                        .equal("colvenda", itemDevolucao.getColvenda())
                        .equal("marca", itemDevolucao.getMarca())
                        .isIn("mostruario", itemDevolucao.getMostruarios().toArray()))
                .resultList();

        mostRepresen.forEach(most -> {
            if (most.getItens().stream().allMatch(it -> it.getStatus().equals("D") || it.getStatus().equals("X"))) {
                most.setStatus("D");
                new FluentDao().merge(most);
            }
        });
    }

    public void guardarBarraRetorno(String barra, VSdDevolucaoMostRep itemDevolucao) throws SQLException {
        SdMostrRepBarraRetorno barraRetorno;
        barraRetorno = new SdMostrRepBarraRetorno();
        barraRetorno.setBarra(barra);
        barraRetorno.setCodrep(itemDevolucao.getCodrep());
        barraRetorno.setColecao(itemDevolucao.getColvenda());
        new FluentDao().persist(barraRetorno);
    }

    @Override
    public void closeWindow() {
        Globals.getMainStage().close();
    }
}