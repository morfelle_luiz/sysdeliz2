package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.sysdeliz.comercial.SdPedido;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.exceptions.LambdaException;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class SaidaMostruarioController extends WindowBase {

    // CONSTANTES
    protected final AtomicReference<TabPrz> _PERIODO_MOSTRUARIO = new AtomicReference<>(new FluentDao().selectFrom(TabPrz.class).where(eb -> eb.equal("prazo", "MOST")).singleResult());
    protected final AtomicReference<Regiao> _TABELA_PRECO_MOSTRUARIO = new AtomicReference<>(new FluentDao().selectFrom(Regiao.class).where(eb -> eb.equal("regiao", "MOST")).singleResult());
    protected final AtomicReference<Represen> _REPRESENTANTE_DELIZ = new AtomicReference<>(new FluentDao().selectFrom(Represen.class).where(eb -> eb.equal("codRep", "0126")).singleResult());
    protected final AtomicReference<SdStatusRemessa> _STATUS_S_REMESSA_MOSTRUARIO = new AtomicReference<>(new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "S")).singleResult());
    protected final AtomicReference<SdStatusRemessa> _STATUS_T_REMESSA_MOSTRUARIO = new AtomicReference<>(new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "T")).singleResult());

    public SaidaMostruarioController(String title, Image icone, Boolean closeBtn) {
        super(title, icone, null, closeBtn);
    }

    protected SdColaborador getColetor(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
                        .equal("ativo", true))
                .singleResult();
    }

    protected SdCaixaRemessa varificaCaixaExistente(String numeroCaixa) {
        return new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("numero", numeroCaixa)).singleResult();
    }

    protected SdCaixaRemessa substituirCaixa(String fieldBarcodeCaixa, SdCaixaRemessa caixaAberta,
                                             SdRemessaCliente remessaEmColeta, List<SdCaixaRemessa> caixasRemessa) throws SQLException {
        SdCaixaRemessa caixaSubstituta = new SdCaixaRemessa(Integer.parseInt(fieldBarcodeCaixa), remessaEmColeta);
        caixaSubstituta.setPeso(caixaAberta.getPeso());
        caixaSubstituta.setQtde(caixaAberta.getQtde());
        caixaSubstituta = new FluentDao().persist(caixaSubstituta);
        for (SdItensCaixaRemessa barra : caixaAberta.getItens()) {
            SdItensCaixaRemessa item = new SdItensCaixaRemessa(caixaSubstituta, barra.getId().getBarra(), barra.getCodigo(), barra.getCor(), barra.getTam(), barra.getStatus(), barra.getPedido());
            barra.setStatus("C");
            new FluentDao().merge(barra);
            new FluentDao().persist(item);
            caixaSubstituta.getItens().add(item);
        }

        new FluentDao().deleteAll(caixaAberta.getItens());
        new FluentDao().delete(caixaAberta);
        caixasRemessa.remove(caixaAberta);
        caixasRemessa.add(caixaSubstituta);

        new NativeDAO().runNativeQueryUpdate("update pedido3_001 set caixa = '%s' where caixa = '%s' and numero in ('%s')",
                caixaSubstituta.getNumero(), caixaAberta.getNumero(),
                remessaEmColeta.getPedidos().stream()
                        .map(pedido -> pedido.getId().getNumero().getNumero())
                        .distinct()
                        .collect(Collectors.joining("','")));

        return caixaSubstituta;
    }

    protected List<SdCaixaRemessa> getCaixasRemessaMostruario(SdMostrRep mostruario) {
        return (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class)
                .where(eb -> eb.equal("remessa.remessa", mostruario.getRemessa()))
                .resultList();
    }

    protected SdRemessaCliente getRemessaMostruario(SdMostrRep mostruario) {
        return new FluentDao().selectFrom(SdRemessaCliente.class)
                .where(eb -> eb.equal("remessa", mostruario.getRemessa()))
                .singleResult();
    }

    protected List<SdMostrRep> getMostruariosMarca(String mostruario, VSdDadosProduto produto) {
        return (List<SdMostrRep>) new FluentDao().selectFrom(SdMostrRep.class)
                .where(eb -> eb
                        .equal("status", "P")
                        .equal("marca", produto.getCodMarca())
                        .equal("mostruario", mostruario))
                .resultList();
    }

    protected VSdDadosProdutoBarra getProdutoBarra(String barraProduto) {
        return new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb.equal("barra28", barraProduto.substring(0, 6)))
                .singleResult();
    }

    protected Pedido criarPedido(Colecao colecao, Entidade entidade, TabTran tabTran, String marca, Integer mostruario) {
        Pedido pedido = new Pedido();
        pedido.setNumero("M" + Globals.getProximoCodigo("PEDIDO_MOST", "NUMERO"));
        pedido.setDtdigita(LocalDate.now());
        pedido.setDt_emissao(LocalDate.now());
        pedido.setDt_fatura(LocalDate.of(2050, 12, 31));
        pedido.setEntrerga(LocalDate.of(2050, 12, 31));
        pedido.setBloqueio("1");
        pedido.setCif("1");
        pedido.setPedCli(String.valueOf(mostruario));
        pedido.setPeriodo(_PERIODO_MOSTRUARIO.get());
        pedido.setColecao(colecao);
        pedido.setTabPre(_TABELA_PRECO_MOSTRUARIO.get());
        pedido.setCodcli(entidade);
        pedido.setTabTrans(tabTran);
        pedido.setCodrep(_REPRESENTANTE_DELIZ.get());
        pedido.setPagto("1");
        pedido.setObs("PEDIDO AUTOMATICO GERADO NA LEITURA DO MOSTRUARIO");
        pedido.setFinanceiro("1");
        pedido.setSitDup("17");
        pedido.setPeriodoProd("M");
        pedido.setColecao(colecao);
        pedido = new FluentDao().merge(pedido);

        SdPedido sdPedido = new SdPedido();
        sdPedido.setNumero(pedido.getNumero());
        sdPedido.setMarca(marca);
        sdPedido.setColecao(colecao.getCodigo());
        new FluentDao().merge(sdPedido);

        return pedido;
    }

    protected SdRemessaCliente criarRemessa(Pedido pedido, VSdDadosCliente cliente, String marca) {
        SdRemessaCliente remessa = new SdRemessaCliente();
        remessa.setStatus(_STATUS_S_REMESSA_MOSTRUARIO.get());
        remessa.setAutomatica(true);
        remessa.setCodcli(cliente);
        remessa.setDtemissao(LocalDateTime.now());
        remessa.setDtIniColeta(LocalDateTime.now());
        remessa.setDtentrega(LocalDate.of(2050, 12, 31));
        remessa.setDeposito("0011");
        remessa.setObservacao("REMESSA DE MOSTRUARIO");
        remessa = new FluentDao().merge(remessa);

        SdPedidosRemessa pedidoRemessa = new SdPedidosRemessa();
        pedidoRemessa.setId(new SdPedidosRemessaPK(remessa.getRemessa(), pedido));
        pedidoRemessa.setDtentrega(LocalDate.of(2050, 12, 31));
        pedidoRemessa.setMarca(marca);
        new FluentDao().merge(pedidoRemessa);

        JPAUtils.getEntityManager().refresh(remessa);
        return remessa;
    }

    protected void incluirItemPedidoRemessa(VSdDadosProdutoBarra produtoBarra, VSdDadosProduto produto,
                                            Pedido pedido, SdRemessaCliente remessa, SdCaixaRemessa caixa) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                        "insert into ped_iten_001\n" +
                        "  (ordem,\n" +
                        "   qtde,\n" +
                        "   preco,\n" +
                        "   tam,\n" +
                        "   cor,\n" +
                        "   codigo,\n" +
                        "   numero,\n" +
                        "   qtde_orig,\n" +
                        "   preco_orig,\n" +
                        "   dt_entrega)\n" +
                        "values\n" +
                        "  (nvl((select decode(min(pei2.ordem), null, max(pei.ordem) + 1, min(pei2.ordem))\n" +
                        "  from ped_iten_001 pei\n" +
                        "  left join ped_iten_001 pei2\n" +
                        "    on pei2.numero = pei.numero\n" +
                        "   and pei2.codigo = '%s'\n" +
                        " where pei.numero = '%s'),\n" +
                        "       1),\n" +
                        "   1,\n" +
                        "   nvl((select max(preProd.Real)\n" +
                        "         from custoproduto preProd\n" +
                        "        where preProd.codigo = '%s'),\n" +
                        "       30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   1,\n" +
                        "   nvl((select max(preProd.Real)\n" +
                        "         from custoproduto preProd\n" +
                        "        where preProd.codigo = '%s'),\n" +
                        "       30),\n" +
                        "   '31/12/2050')", produto.getCodigo(), pedido.getNumero(), produto.getCodigo(), produtoBarra.getTam(),
                produtoBarra.getCor(), produto.getCodigo(), pedido.getNumero(), produto.getCodigo());
        JPAUtils.getEntityManager().refresh(pedido);
        PedIten itemPedido = new FluentDao().selectFrom(PedIten.class)
                .where(eb -> eb.equal("id.numero", pedido.getNumero())
                        .equal("id.codigo.codigo", produtoBarra.getCodigo())
                        .equal("id.cor.cor", produtoBarra.getCor())
                        .equal("id.tam", produtoBarra.getTam()))
                .singleResult();
//                pedido.getItens().stream()
//                .filter(item -> item.getId().getCodigo().getCodigo().equals(produtoBarra.getCodigo()))
//                .findFirst()
//                .get();

        SdItensPedidoRemessa itemRemessa = new SdItensPedidoRemessa();
        itemRemessa.setId(new SdItensPedidoRemessaPK(remessa.getRemessa(), pedido.getNumero(), produto, produtoBarra.getCor()));
        itemRemessa.setStatusitem("C");
        itemRemessa.setDtentrega(LocalDate.of(2050, 12, 31));
        itemRemessa.setQtde(1);
        itemRemessa.setValor(itemPedido.getPreco());
        itemRemessa.setTipo("COM");
        itemRemessa.setLocal("N-D");
        itemRemessa.setOrdem(itemPedido.getId().getOrdem());
        new FluentDao().persist(itemRemessa);

        SdGradeItemPedRem gradeItemRemessa = new SdGradeItemPedRem();
        gradeItemRemessa.setId(new SdGradeItemPedRemPK(remessa.getRemessa(), pedido.getNumero(),
                produtoBarra.getCodigo(), produtoBarra.getCor(), produtoBarra.getTam()));
        gradeItemRemessa.setStatusitem("C");
        gradeItemRemessa.setDtentrega(LocalDate.of(2050, 12, 31));
        gradeItemRemessa.setQtde(1);
        gradeItemRemessa.setValor(itemPedido.getPreco());
        gradeItemRemessa.setTipo("COM");
        gradeItemRemessa.setLocal("N-D");
        gradeItemRemessa.setOrdem(itemPedido.getId().getOrdem());
        gradeItemRemessa.setQtdel(1);
        gradeItemRemessa.setBarra28(produtoBarra.getBarra28());
        gradeItemRemessa.setColetor(Globals.getColaboradorSistema());
        gradeItemRemessa.setDhColeta(LocalDateTime.now());
        new FluentDao().persist(gradeItemRemessa);
        JPAUtils.getEntityManager().refresh(remessa);

        expedirExcia(remessa, caixa, itemRemessa, gradeItemRemessa);
    }
    protected void expedirExcia(SdRemessaCliente remessa, SdCaixaRemessa caixa,
                                SdItensPedidoRemessa produto, SdGradeItemPedRem item) throws SQLException {
        /**
         * Bloco desativado temporariamente até fazer as alterações nos relatórios
         * Precisa descomentar também do estorno.
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade - 1 where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   1,\n" +
                        "   '2',\n" +
                        "   'S',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Caixa - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", item.getId().getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito(), item.getId().getRemessa(), item.getId().getNumero(), caixa.getNumero(),
                item.getId().getCodigo(), item.getId().getCodigo(), item.getId().getNumero(), (remessa.getColetor() != null ? remessa.getColetor().getCodigo() : Globals.getUsuarioLogado().getCodUsuarioTi())));
         */

        // inclusão do item na PEDIDO3 para o faturamento no Excia
        if (!produto.getTipo().equals("MKT"))
            new NativeDAO().runNativeQueryUpdate(String.format("" +
                            "merge into pedido3_001 ped\n" +
                            "using dual\n" +
                            "on (ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s')\n" +
                            "when matched then\n" +
                            "  update set qtde = qtde + 1, data = sysdate\n" +
                            "when not matched then\n" +
                            "  insert\n" +
                            "    (caixa,\n" +
                            "     codigo,\n" +
                            "     cor,\n" +
                            "     numero,\n" +
                            "     qtde,\n" +
                            "     qtde_f,\n" +
                            "     peso_l,\n" +
                            "     peso,\n" +
                            "     tam,\n" +
                            "     ordem,\n" +
                            "     deposito,\n" +
                            "     lote,\n" +
                            "     data,\n" +
                            "     tcaixa,\n" +
                            "     funcionario,\n" +
                            "     notafiscal)\n" +
                            "  values\n" +
                            "    ('%s', '%s', '%s', '%s', 1, 0, 0, 0, '%s', %s, '%s', '000000', sysdate, '', '%s', '')",
                    item.getId().getNumero(), caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito(),
                    caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getNumero(), item.getId().getTam(), produto.getOrdem(), remessa.getDeposito(),
                    Globals.getColaboradorSistema().getCodigo()));

    }

    protected void removerItemPedidoRemessa(VSdDadosProdutoBarra produtoBarra, VSdDadosProduto produto,
                                            Pedido pedido, SdRemessaCliente remessa, SdCaixaRemessa caixa) throws SQLException, LambdaException {
        remessa.refresh();
        remessa.getPedidos().stream()
                .filter(pedRemessa -> pedRemessa.getId().getNumero().getNumero().equals(pedido.getNumero()))
                .findAny()
                .ifPresent(pedidoRemessa -> {
                    pedidoRemessa.refresh();
                    pedidoRemessa.getItens().stream()
                            .filter(itemRemessa -> itemRemessa.getId().getCodigo().getCodigo().equals(produto.getCodigo()))
                            .findAny()
                            .ifPresent(itemRemessa -> {
                                itemRemessa.refresh();
                                itemRemessa.getGrade().stream()
                                        .findAny()
                                        .ifPresent(gradeItem -> {
                                            try {
                                                gradeItem.refresh();
                                                gradeItem.setQtdel(gradeItem.getQtdel() - 1);
                                                estornoExcia(remessa, caixa, itemRemessa, gradeItem);
                                                new FluentDao().delete(gradeItem);
                                                new FluentDao().delete(itemRemessa);
                                                //pedidoRemessa.getItens().remove(itemRemessa);
                                                pedido.getItens().stream()
                                                        .filter(itemPedido -> itemPedido.getId().getCodigo().getCodigo().equals(gradeItem.getId().getCodigo()))
                                                        .findAny()
                                                        .ifPresent(itemPedido -> {
                                                            new FluentDao().delete(itemPedido);
                                                        });
                                            } catch (SQLException e) {
                                                e.printStackTrace();
                                                throw new LambdaException(e);
                                            }
                                        });
                            });
                });

    }
    protected void estornoExcia(SdRemessaCliente remessa, SdCaixaRemessa caixa,
                                SdItensPedidoRemessa produto, SdGradeItemPedRem item) throws SQLException {
        /**
         * Bloco desativado temporariamente até fazer as alterações nos relatórios
         * Precisa descomentar também do expedir.
        // update da PA_ITEN removendo do estoque
        new NativeDAO().runNativeQueryUpdate(String.format("update pa_iten_001 set quantidade = quantidade + 1 where codigo = '%s' and cor = '%s' and tam = '%s' and deposito = '%s'",
                item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito()));
        // inclusão na PA_MOV para movimento do item no Excia
        new NativeDAO().runNativeQueryUpdate(String.format("" +
                        "insert into pa_mov_001\n" +
                        "  (ordem,\n" +
                        "   num_docto,\n" +
                        "   codigo,\n" +
                        "   cor,\n" +
                        "   tamanho,\n" +
                        "   lote,\n" +
                        "   deposito,\n" +
                        "   tipo,\n" +
                        "   qtde,\n" +
                        "   tip_bai,\n" +
                        "   operacao,\n" +
                        "   dt_mvto,\n" +
                        "   descricao,\n" +
                        "   observacao,\n" +
                        "   turno,\n" +
                        "   preco,\n" +
                        "   custo,\n" +
                        "   codcli,\n" +
                        "   codfun,\n" +
                        "   tp_mov)\n" +
                        "VALUES\n" +
                        "  ((SELECT MAX(ORDEM) + 1 ORDEM\n" +
                        "     FROM PA_MOV_001\n" +
                        "    WHERE DT_MVTO > sysdate - 30),\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '%s',\n" +
                        "   '000000',\n" +
                        "   '%s',\n" +
                        "   '1',\n" +
                        "   1,\n" +
                        "   '2',\n" +
                        "   'E',\n" +
                        "   SYSDATE2,\n" +
                        "   '%s',\n" +
                        "   'Pedido - ' || '%s' || ' Caixa - ' || '%s',\n" +
                        "   '1',\n" +
                        "   (SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = '%s'),\n" +
                        "   (SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = '%s'),\n" +
                        "   '%s',\n" +
                        "   'EX')", item.getId().getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito(), item.getId().getRemessa(), item.getId().getNumero(), caixa.getNumero(),
                item.getId().getCodigo(), item.getId().getCodigo(), item.getId().getNumero(), (remessa.getColetor() != null ? remessa.getColetor().getCodigo() : Globals.getUsuarioLogado().getCodUsuarioTi())));
        */

        // inclusão do item na PEDIDO3 para o faturamento no Excia
        if (!produto.getTipo().equals("MKT")) {
            if (item.getQtdel() > 0)
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "update pedido3_001 ped set ped.qtde = ped.qtde - 1, ped.data = sysdate where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getId().getNumero(), caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito()));
            else
                new NativeDAO().runNativeQueryUpdate(String.format("" +
                                "delete from pedido3_001 ped where ped.numero = '%s' and ped.caixa = '%s' and ped.codigo = '%s' and ped.cor = '%s' and ped.tam = '%s' and ped.deposito = '%s'",
                        item.getId().getNumero(), caixa.getNumero(), item.getId().getCodigo(), item.getId().getCor(), item.getId().getTam(), remessa.getDeposito()));
        }
    }

    protected void incluirItemEmCaixa(SdItensCaixaRemessa itemEmCaixa, VSdDadosProduto produto, SdCaixaRemessa caixaAberta) {
        caixaAberta.getItens().add(itemEmCaixa);
        caixaAberta.setQtde(caixaAberta.getQtde() + 1);
        caixaAberta.setPeso(caixaAberta.getPeso().add(produto.getPeso()));
        new FluentDao().merge(caixaAberta);
    }

    protected void removerItemCaixa(SdItensCaixaRemessa itemEmCaixa) {
        SdCaixaRemessa caixaItem = itemEmCaixa.getId().getCaixa();
        new FluentDao().delete(itemEmCaixa);
        caixaItem.refresh();
        caixaItem.setQtde(caixaItem.getQtde() - 1);
        caixaItem.setPeso(caixaItem.getPeso().subtract(itemEmCaixa.getCodigo().getPeso()));
        new FluentDao().merge(caixaItem);
    }

    protected VSdDadosProduto getProduto(VSdDadosProdutoBarra produtoBarra) {
        return new FluentDao().selectFrom(VSdDadosProduto.class)
                .where(eb -> eb.equal("codigo", produtoBarra.getCodigo())).singleResult();
    }

    protected SdItensCaixaRemessa incluirItemCaixa(SdCaixaRemessa caixaAberta,
                                                   VSdDadosProdutoBarra produtoBarra,
                                                   VSdDadosProduto produto,
                                                   String barra, String pedido) {
        SdItensCaixaRemessa itemCaixa = new SdItensCaixaRemessa(caixaAberta,
                barra,
                produto,
                produtoBarra.getCor(),
                produtoBarra.getTam(),
                "E",
                pedido);

        return new FluentDao().merge(itemCaixa);
    }

    protected void atualizarStatusItemMost(SdMostrRepItens itemMost) {
        new FluentDao().merge(itemMost);
    }

    protected SdItensCaixaRemessa barraJaEmCaixa(String barra) {
        return new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                .where(eb -> eb
                        .equal("id.barra", barra)
                        .equal("status", "E"))
                .singleResult();
    }

    @Override
    public void closeWindow() {
        Globals.getMainStage().close();
    }
}
