package sysdeliz2.controllers.views.expedicao;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.daos.MarketingPedidoDAO;
import sysdeliz2.daos.ReservaPedidoDAO;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.implementation.ReservaPedidoDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.daos.generics.interfaces.ReservaPedidoDao;
import sysdeliz2.models.ProdutosReservaPedido;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.view.VSdReservasPedidos;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.StaticButtonBuilder;
import sysdeliz2.utils.StaticGraphicsBuilder;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.views.helpers.SceneKanbanColetaHelper;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author lima.joao
 * @since 09/10/2019 11:54
 */
//TODO converter para kotlin + tornadoFX
public class SceneKanbanColetaView {

    private BorderPane borderPane;
    private TableView<VSdReservasPedidos> tablePedidosPendentes;

    private ReservaPedidoDao reservaPedidoDaoJPA;
    private ReservaPedidoDAO daoReservaPedido;
    private MarketingPedidoDAO daoMarketingPedido;
    private Button btnSearch;

    private VBox getBoxLeitor(String nome) {
        // Loop para criar todos os blocos referentes aos coletores.
        VBox leitor = StaticGraphicsBuilder.buildVBox(SceneKanbanColetaHelper.HEIGHT_VBOX_LEITOR, SceneKanbanColetaHelper.WIDTH_VBOX_LEITOR);
        VBox.setVgrow(leitor, Priority.ALWAYS);
        HBox.setMargin(leitor, new Insets(SceneKanbanColetaHelper.MARGIN_DEFAULT));

        Button btnAdicionar = new Button("Adicionar");
        Button btnRemover = new Button("Desalocar");

        //<editor-fold desc="Table">
        // Carrega a lista pelo leitor
        ObservableList<VSdReservasPedidos> lista = reservaPedidoDaoJPA.carregaPorUsuarioLeitor(nome, new String[]{"I", "C"});

        TableView<VSdReservasPedidos> tblLeitor = new TableView<>(lista);

        //<editor-fold desc="Linha de Botões de Ação">
        HBox linhaBotoes = StaticGraphicsBuilder.buildHBox(SceneKanbanColetaHelper.HEIGHT_HBOX_BOTOES, SceneKanbanColetaHelper.WIDTH_VBOX_LEITOR);

        linhaBotoes.setPadding(new Insets(SceneKanbanColetaHelper.PADING_DEFAULT));

        btnAdicionar.setOnAction(event -> {
            if (tablePedidosPendentes.getSelectionModel().getSelectedItem() != null) {
                VSdReservasPedidos selecionado = tablePedidosPendentes.getSelectionModel().getSelectedItem();

                List<ProdutosReservaPedido> mktsParaRomaneio = new ArrayList<>();
                String grupoCliente;
                String colecaoPedido;

                // executa o filtro para remover o registro. Visto que a remoção direta não funciona.
                btnSearch.getOnAction().handle(null);

                if (selecionado.getReserva().equals("0")) {
                    selecionado.setStatus("I");
                    //rpCurrentReservaPedidoSelected.strDescStatusProperty().set(GUIUtils.getStatusReserva(rpCurrentReservaPedidoSelected.strStatusProperty().get()));
                    selecionado.setUsuarioLeitura(nome);

                    Integer lastOrdem = reservaPedidoDaoJPA.getLastOrdem(nome);
                    selecionado.setOrdem(lastOrdem + 1);

                    String strAIReserva = reservaPedidoDaoJPA.addSdReservaPedido(selecionado);

                    // Atualiza o pedido em memória
                    selecionado.setReserva(strAIReserva);
                    // Atualiza o pedido no grid
                    tablePedidosPendentes
                            .getSelectionModel()
                            .getSelectedItem()
                            .setReserva(strAIReserva);

                    // verificar as marcas da reserva
                    List<String> marcasReserva;
                    try {
                        marcasReserva = daoReservaPedido.getMarcasReserva(selecionado.toReservaPedido());

                        grupoCliente = marcasReserva.get(0); // obtendo o SIT_CLI do cliente
                        marcasReserva.remove(0);
                        colecaoPedido = marcasReserva.get(0); // obtendo a coleção do pedido
                        marcasReserva.remove(0);

                        if (!colecaoPedido.equals("MKT")) {
                            for (String marca : marcasReserva) {
                                // verificar se o cliente é novo
                                List<String> dadosCliente = daoMarketingPedido.isClienteNovo(selecionado.getNumero(), marca);

                                if (dadosCliente.contains("SIM")) {
                                    // se for novo ver se já foi entregue marketing de cliente novo (está na
                                    // consulta)
                                    // verificar materiais de cliente novo
                                    mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsClienteNovo(selecionado.getNumero(), marca));
                                    grupoCliente = "CN";
                                } else {
                                    // se não for verificar se já foram entregues marketings unico no cliente (na
                                    // consulta)
                                    // se não listar marketings unico no cliente
                                    // colocar em um arraylist para o romaneio
                                    mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoCliente(selecionado.getNumero(), marca, grupoCliente));
                                }
                                // verificar se foram entregues materiais unico no pedido (na consulta)
                                // se não listar marketings unico no pedido
                                // colocar em um arraylist para o romaneio
                                mktsParaRomaneio.addAll(daoMarketingPedido.loadMktsUnicoPedido(selecionado.getNumero(), marca, grupoCliente));
                            }
                        }
                        // Verificar se tem cadastro de marketing no pedido
                        List<ProdutosReservaPedido> mktsCadastradosPedido = daoMarketingPedido.loadMktsCadastradoNoPedido(selecionado.getNumero());
                        // se tiver colocar no array para o romaneio
                        mktsParaRomaneio.addAll(mktsCadastradosPedido);
                        // atualizar o cadastro do marketing do pedido
                        daoMarketingPedido.updByMktPedido(selecionado.getNumero(), strAIReserva, mktsCadastradosPedido);

                        // salvar materias do array no banco na tabela SD_RESERVA_PEDIDO_001
                        daoReservaPedido.saveMktReservaPedido(selecionado.toReservaPedido(), mktsParaRomaneio, SceneMainController.getAcesso(), nome);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                lista.setAll(reservaPedidoDaoJPA.carregaPorUsuarioLeitor(nome, new String[]{"I", "P"}));
            }
        });

        btnRemover.setOnAction(event -> {
            if (tblLeitor.getSelectionModel().getSelectedItem() != null) {
                VSdReservasPedidos selecionado = tblLeitor.getSelectionModel().getSelectedItem();

                if (selecionado.getStatus().equals("P") || selecionado.getStatus().equals("E") || selecionado.getStatus().equals("F")) {
                    GUIUtils.showMessage("Você pode excluir somente reservas com o STATUS = ALOCADO", Alert.AlertType.INFORMATION);
                    return;
                }

                if (GUIUtils.showQuestionMessageDefaultNao("Tem certeza que deseja excluir a reserva?")) {
                    try {
                        daoReservaPedido.deleteReservaPedido(selecionado.toReservaPedido());
                        daoReservaPedido.limpaReservaPedido_PED_RESERVA(selecionado.toReservaPedido());
                        daoMarketingPedido.updReservaByReservaPedido(selecionado.toReservaPedido());

                        selecionado.setReserva("0");
                        selecionado.setStatus("A");
                        tablePedidosPendentes.getItems().add(selecionado);
                        tblLeitor.getItems().remove(selecionado);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        GUIUtils.showException(e);
                    }
                }
            }
        });

        linhaBotoes.getChildren().add(btnAdicionar);
        linhaBotoes.getChildren().add(btnRemover);

        leitor.getChildren().add(linhaBotoes);
        //</editor-fold>

        //<editor-fold desc="Configurações da Tabela">
        tblLeitor.setPrefHeight(SceneKanbanColetaHelper.HEIGHT_VBOX_LEITOR);
        tblLeitor.setPrefWidth(SceneKanbanColetaHelper.WIDTH_VBOX_LEITOR);


        TableColumn<VSdReservasPedidos, String> clnLeitor = new TableColumn<>(nome);
        TableColumn<VSdReservasPedidos, String> clnPedido = new TableColumn<>("Pedido");
        TableColumn<VSdReservasPedidos, String> clnReserva = new TableColumn<>("Reserva");
        TableColumn<VSdReservasPedidos, String> clnStatus = new TableColumn<>("Status");
        TableColumn<VSdReservasPedidos, VSdReservasPedidos> clnActions = new TableColumn<>("Ações");

        clnPedido.setPrefWidth(SceneKanbanColetaHelper.WIDTH_COLUMN_PEDIDO);
        clnReserva.setPrefWidth(SceneKanbanColetaHelper.WIDTH_COLUMN_RESERVA);
        clnStatus.setPrefWidth(SceneKanbanColetaHelper.WIDTH_COLUMN_STATUS);
        clnActions.setPrefWidth(SceneKanbanColetaHelper.WIDTH_COLUMN_ACTIONS);

        clnPedido.setCellValueFactory(param -> (param.getValue()).numeroProperty());
        clnReserva.setCellValueFactory(param -> (param.getValue()).reservaProperty());
        clnStatus.setCellValueFactory(param -> {
            switch (param.getValue().getStatus()) {
                case "A":
                    return new SimpleStringProperty("Aberto");
                case "C":
                    return new SimpleStringProperty("Em Leitura");
                case "E":
                    return new SimpleStringProperty("Expedido");
                case "F":
                    return new SimpleStringProperty("Faturado");
                case "I":
                    return new SimpleStringProperty("Alocado");
                case "P":
                    return new SimpleStringProperty("Lido");
                default:
                    return new SimpleStringProperty("Não Informado");
            }
        });

        clnActions.setCellFactory(new Callback<TableColumn<VSdReservasPedidos, VSdReservasPedidos>, TableCell<VSdReservasPedidos, VSdReservasPedidos>>() {
            @Override
            public TableCell<VSdReservasPedidos, VSdReservasPedidos> call(TableColumn<VSdReservasPedidos, VSdReservasPedidos> param) {
                return new TableCell<VSdReservasPedidos, VSdReservasPedidos>() {
                    final Button btnUp = StaticButtonBuilder.genericButtom(
                            ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._16),
                            "Aumentar prioridade",
                            "Acima",
                            new String[]{"info", "xs"},
                            ContentDisplay.GRAPHIC_ONLY
                    );

                    final Button btnDown = StaticButtonBuilder.genericButtom(
                            ImageUtils.getIcon(ImageUtils.Icon.EXPANDIR, ImageUtils.IconSize._16),
                            "Reduzir prioridade",
                            "Abaixo",
                            new String[]{"warn", "xs"},
                            ContentDisplay.GRAPHIC_ONLY
                    );
                    final HBox boxButtonsRow = new HBox(3);

                    @Override
                    protected void updateItem(VSdReservasPedidos item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            btnUp.setPrefWidth(SceneKanbanColetaHelper.WIDTH_BUTONS_ACTIONS);
                            btnDown.setPrefWidth(SceneKanbanColetaHelper.WIDTH_BUTONS_ACTIONS);

                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().add(btnUp);
                            boxButtonsRow.getChildren().add(btnDown);

                            setGraphic(boxButtonsRow);

                            btnUp.setOnAction(event -> {
                                if(tblLeitor.getSelectionModel() != null){
                                    int index = tblLeitor.getSelectionModel().getSelectedIndex();

                                    if(tblLeitor.getItems().size() > 1){

                                        VSdReservasPedidos reservaUp = tblLeitor.getSelectionModel().getSelectedItem();
                                        VSdReservasPedidos reservaDown = tblLeitor.getItems().get(index-1);

                                        int ordem = reservaUp.getOrdem();
                                        reservaUp.setOrdem(reservaDown.getOrdem());
                                        reservaDown.setOrdem(ordem);

                                        reservaPedidoDaoJPA.updateOrdem(reservaUp, reservaDown);

                                        tblLeitor.getSelectionModel().getSelectedItem().setOrdem(index-1);
                                    }
                                }
                            });

                            btnDown.setOnAction(event -> {
                                if(tblLeitor.getSelectionModel() != null){
                                    int index = tblLeitor.getSelectionModel().getSelectedIndex();

                                    if(tblLeitor.getItems().size() > 1){

                                        VSdReservasPedidos reservaUp = tblLeitor.getItems().get(index+1);
                                        VSdReservasPedidos reservaDown = tblLeitor.getSelectionModel().getSelectedItem();

                                        int ordem = reservaUp.getOrdem();
                                        reservaUp.setOrdem(reservaDown.getOrdem());
                                        reservaDown.setOrdem(ordem);

                                        reservaPedidoDaoJPA.updateOrdem(reservaUp, reservaDown);

                                        tblLeitor.getSelectionModel().getSelectedItem().setOrdem(index+1);
                                    }
                                }
                            });
                        }
                    }
                };
            }
        });

        clnLeitor.getColumns().add(clnPedido);
        clnLeitor.getColumns().add(clnReserva);
        clnLeitor.getColumns().add(clnStatus);
        clnLeitor.getColumns().add(clnActions);

        tblLeitor.getColumns().add(clnLeitor);
        //</editor-fold>

        leitor.getChildren().add(tblLeitor);
        //</editor-fold>
        return leitor;
    }

    @SuppressWarnings("unchecked")
    private SceneKanbanColetaView() {

        double HEIGHT_BORDER_PANE = 768.0;
        double WIDTH_BORDER_PANE = 1024.0;

        borderPane = StaticGraphicsBuilder.buildBorderPane(HEIGHT_BORDER_PANE, WIDTH_BORDER_PANE);

        reservaPedidoDaoJPA = new ReservaPedidoDaoImpl(JPAUtils.getEntityManager());

        daoReservaPedido = DAOFactory.getReservaPedidoDAO();
        daoMarketingPedido = DAOFactory.getMarketingPedidoDAO();

        ObservableList<VSdReservasPedidos> lista = reservaPedidoDaoJPA.carregaPorUsuarioLeitor("", new String[]{"A"});

        //<editor-fold desc="TOP">
        // top
        // Cria o container do topo
        HBox top = StaticGraphicsBuilder.buildHBox(55.0, 200.0, "-fx-background-color: #F5F5F5;");
        HBox.setHgrow(top, Priority.ALWAYS);

        // Adiciona o icone a ser exibido no topo
        top.getChildren().add(StaticGraphicsBuilder.buildImageView("/images/icons/expedicao_kanban_4.png", 65.0, 55.0, true, true));

        // Cria o label que será exibido ao lado do icone
        Label labelTop = new Label("Expedição > Kanban");
        labelTop.setFont(Font.font("System Bold", 18.0));
        HBox.setMargin(labelTop, new Insets(10.0));

        // Adiciona o label no topo
        top.getChildren().add(labelTop);

        // Seta o padding esquerdo do container para que o icone não fique colado na lateral esquerda.
        top.setPadding(new Insets(0.0, 0.0, 0.0, 5.0));

        borderPane.setTop(top);
        //</editor-fold>

        //<editor-fold desc="LEFT">
        // Left
        VBox left = StaticGraphicsBuilder.buildVBox(713.0, 400.0);

        left.setPadding(new Insets(5.0));

        HBox lTop = StaticGraphicsBuilder.buildHBox(50, 400);
        TextField search = new TextField();
        lTop.getChildren().add(search);

        search.setPrefWidth(300);
        HBox.setMargin(search, new Insets(0,0,0,5));

        Button btnSearch = StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16),
                "Pesquisar",
                "Pesuqisar",
                new String[]{"warning", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );

        Button btnRefresh = StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._16),
                "Atualizar",
                "Atualizar",
                new String[]{"info", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );

        btnSearch.setOnAction(event -> {
            ObservableList<VSdReservasPedidos> filtred = lista.filtered(vSdReservasPedidos ->
                 (vSdReservasPedidos.getNumero().equals(search.getText())) || (vSdReservasPedidos.getNome().toUpperCase().contains(search.getText().toUpperCase()))
            );
            tablePedidosPendentes.setItems(filtred);
        });

        btnRefresh.setOnAction(event -> {
            lista.setAll(reservaPedidoDaoJPA.carregaPorUsuarioLeitor("", new String[]{"A"}));
            tablePedidosPendentes.setItems(lista);
        });

        lTop.getChildren().add(btnSearch);
        lTop.getChildren().add(btnRefresh);

        left.getChildren().add(lTop);


        // Add tableview
        tablePedidosPendentes = new TableView<>();
        tablePedidosPendentes.setPrefWidth(400);

        tablePedidosPendentes.setItems(lista);

        TableColumn<VSdReservasPedidos, String> clnPendentes = new TableColumn<>();
        TableColumn<VSdReservasPedidos, String> clnPedidoPend = new TableColumn<>();
        TableColumn<VSdReservasPedidos, BigDecimal> clnQtdPend = new TableColumn<>();
        TableColumn<VSdReservasPedidos, String> clnNomeCliente = new TableColumn<>();

        clnPedidoPend.setId("clnPedido");
        clnPedidoPend.setText("Numero");
        clnPedidoPend.setPrefWidth(75);

        clnQtdPend.setId("clnQtdPend");
        clnQtdPend.setText("Qtd");
        clnQtdPend.setPrefWidth(40.0);

        clnNomeCliente.setId("clnNomeCliente");
        clnNomeCliente.setText("Cliente");
        clnNomeCliente.setPrefWidth(80.0);

        clnPendentes.setText("Pedidos");
        clnPendentes.getColumns().add(clnPedidoPend);
        clnPendentes.getColumns().add(clnQtdPend);
        clnPendentes.getColumns().add(clnNomeCliente);

        tablePedidosPendentes.getColumns().add(clnPendentes);

        VBox.setVgrow(tablePedidosPendentes, Priority.ALWAYS);
        VBox.setMargin(tablePedidosPendentes, new Insets(5.0));

        left.getChildren().add(tablePedidosPendentes);

        clnPedidoPend.setCellValueFactory(param -> (param.getValue()).numeroProperty());
        clnQtdPend.setCellValueFactory(param -> (param.getValue()).qtdeProperty());
        clnNomeCliente.setCellValueFactory(param -> (param.getValue()).nomeProperty());

        VBox.setMargin(tablePedidosPendentes, new Insets(0.0, 0.0, 0.0, 5.0));
        borderPane.setLeft(left);

        //</editor-fold>

        // Center
        FlowPane center = new FlowPane();
        //HBox center = StaticGraphicsBuilder.buildHBox(100.0, 200.0);
        BorderPane.setAlignment(center, Pos.CENTER);

        GenericDao<SdColaborador> colaborador001GenericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdColaborador.class);
        ObservableList<SdColaborador> sdColaboradorObservableList = FXCollections.observableArrayList();
        try {
            sdColaboradorObservableList.setAll(colaborador001GenericDao
                    .initCriteria()
                    .addPredicateIsNotNull("usuario")
                    .addPredicateNe("usuario", "", true)
                    .addPredicateEq("codigoFuncao", 21)
                    .addPredicateEq("ativo", true)
                    .addOrderByAsc("usuario")
                    .loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sdColaboradorObservableList.forEach(sdColaborador001 -> {
            center.getChildren().add(this.getBoxLeitor(sdColaborador001.getNome()));
            center.getChildren().add(StaticGraphicsBuilder.buildHBox(300, 10));
        });

        borderPane.setCenter(center);
    }

    public static BorderPane getBorderPane() {
        SceneKanbanColetaView instance = new SceneKanbanColetaView();
        return instance.borderPane;
    }

}
