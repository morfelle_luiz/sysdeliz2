package sysdeliz2.controllers.views.expedicao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.view.VSdLocalExpedicaoQuant;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.ArrayList;
import java.util.List;

public class VizualizacaoEstoqueController extends WindowBase {

    protected List<VSdLocalExpedicaoQuant> locais = new ArrayList<>();

    public VizualizacaoEstoqueController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarLocais(String andar, String ruas, String posicoes, String lado, String status, Object[] produtos, Object[] cores) {

        locais = (List<VSdLocalExpedicaoQuant>) new FluentDao().selectFrom(VSdLocalExpedicaoQuant.class).where(it -> it
                .equal("andar", andar, TipoExpressao.AND, when -> !andar.equals("Todos"))
                .equal("lado", lado, TipoExpressao.AND, when -> !lado.equals("Ambos"))
                .equal("rua", ruas, TipoExpressao.AND, when -> !ruas.equals("Todas"))
                .equal("posicao", posicoes, TipoExpressao.AND, when -> !posicoes.equals("Todas"))
                .equal("estoque", "0", TipoExpressao.AND, when -> status.equals("S/Estoque"))
                .greaterThan("estoque", 0, TipoExpressao.AND, when -> status.equals("C/Estoque"))
                .equal("id.produto", "S PROD", TipoExpressao.AND, when -> status.equals("S/Prod"))
                .isIn("id.produto", produtos, TipoExpressao.AND, when -> produtos.length > 0)
                .isIn("id.cor", cores, TipoExpressao.AND, when -> cores.length > 0)
        ).orderBy("id.local", OrderType.ASC).resultList();

    }

    @Override
    public void closeWindow() {

    }
}
