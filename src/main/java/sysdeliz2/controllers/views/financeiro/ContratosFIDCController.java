package sysdeliz2.controllers.views.financeiro;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.dao.fluentDao.Where;
import sysdeliz2.models.sysdeliz.financeiro.SdContratoFidic;
import sysdeliz2.models.ti.CadBan;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Receber;
import sysdeliz2.models.ti.TabSit;
import sysdeliz2.models.view.VSdTitulosDispContrato;
import sysdeliz2.utils.ParametroConsulta;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ContratosFIDCController extends WindowBase {
    protected List<SdContratoFidic> contratos = new ArrayList<>();
    protected List<CadBan> bancos = new ArrayList<>();
    protected List<Receber> titulosAuxiliares = new ArrayList<>();
    protected List<Receber> todosTitulos = new ArrayList<>();
    protected List<Receber> titulosRecomendados = new ArrayList<>();


    protected final BigDecimal BIGDECIMAL_100 = new BigDecimal("100.0");

    public ContratosFIDCController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarContratos(Object[] numBorderos, Object[] bancos, Object[] contas, Object[] carteiras) {
//        if (contratos.size() > 0) JPAUtils.clearEntitys(contratos);
        contratos = (List<SdContratoFidic>) new FluentDao().selectFrom(SdContratoFidic.class).where(it -> it
                .isIn("bordero.numero", numBorderos, TipoExpressao.AND, when -> numBorderos.length > 0)
                .isIn("bordero.conta.banco.banco", bancos, TipoExpressao.AND, when -> bancos.length > 0)
                .isIn("bordero.conta.conta", contas, TipoExpressao.AND, when -> contas.length > 0)
                .isIn("bordero.carteira.carteira", carteiras, TipoExpressao.AND, when -> carteiras.length > 0)
        ).resultList();
//        contratos.forEach(it -> JPAUtils.getEntityManager().refresh(it));
    }

    protected void buscarBancos(Object[] numBancos, String nomeBanco) {
        bancos = (List<CadBan>) new FluentDao().selectFrom(CadBan.class).where(it -> it
                .isIn("banco", numBancos, TipoExpressao.AND, when -> numBancos.length > 0)
                .like("nomeBanco", nomeBanco, TipoExpressao.AND, when -> !nomeBanco.equals(""))
        ).resultList();
    }

    protected void buscarTitulos(SdContratoFidic contratoSelecionado, String valorTotal, LocalDate dtEmissBegin, LocalDate dtEmissEnd, String clienteSerasa, LocalDate dtVencBegin, LocalDate dtVencEnd,
                                 Integer atrasoCliBegin, Integer atrasoCliEnd, Double valTituloBegin, Double valTituloEnd, Object[] tabSit, String taxa, Object[] sitClis,
                                 ListProperty<ParametroConsulta> parametrosBean, ListProperty<ClienteConsulta> clientesConsultaBean, String manterTitulos, boolean checkBoxIgnorarComDescricao) {

        List<String> manterTitulosList = Arrays.stream(manterTitulos.split("\\.")).collect(Collectors.toList());
        boolean contratoCompleto = false;

        try {
            if (todosTitulos.size() > 0) JPAUtils.clearEntitys(todosTitulos);
        } catch (Exception e) {
            e.printStackTrace();
        }

        todosTitulos.clear();


        boolean manterTit = manterTitulosList.stream().anyMatch(it -> it.equals("T"));
        boolean manterAux = manterTitulosList.stream().anyMatch(it -> it.equals("A"));

        if (!manterTit) {
            try {
                if (titulosRecomendados.size() > 0) JPAUtils.clearEntitys(titulosRecomendados);
            } catch (Exception e) {
                e.printStackTrace();
            }
            titulosRecomendados.clear();
        }
        if (!manterAux) {
            try {
                if (titulosAuxiliares.size() > 0) JPAUtils.clearEntitys(titulosAuxiliares);
            } catch (Exception e) {
                e.printStackTrace();
            }
            titulosAuxiliares.clear();
        }

        Where.ResultBuilder where = new FluentDao().selectFrom(VSdTitulosDispContrato.class).where(it -> it
                .between("dtemissao", dtEmissBegin, dtEmissEnd)
                .between("dtvencto", dtVencBegin, dtVencEnd)
                .isNull("obs", TipoExpressao.AND, when -> checkBoxIgnorarComDescricao)
                .notIn("codcli", clientesConsultaBean.stream().map(cliente -> cliente.getCodcli().getStringCodcli()).toArray(), TipoExpressao.AND, when -> clientesConsultaBean.size() > 0)
                .equal("dividaserasa", clienteSerasa, TipoExpressao.AND, when -> !clienteSerasa.equals("A"))
                .between("valor", valTituloBegin, valTituloEnd)
                .isIn("sitcli", sitClis, TipoExpressao.AND, when -> sitClis.length > 0)
                .isIn("situacao", tabSit, TipoExpressao.AND, when -> tabSit.length > 0)
                .between("mediadiasatraso", atrasoCliBegin, atrasoCliEnd)
        );
//
//        Where.ResultBuilder where = new FluentDao().selectFrom(Receber.class).where(it -> it
//                .between("dtemissao", dtEmissBegin, dtEmissEnd)
//                .between("dtvencto", dtVencBegin, dtVencEnd)
//                .equal("status", "N")
//                .equal("bordero", "0")
//                .isNull("obs", TipoExpressao.AND, when -> checkBoxIgnorarComDescricao )
//                .notIn("entidade.codcli", clientesConsultaBean.stream().map(eb -> eb.getCodcli().getStringCodcli()).toArray(), TipoExpressao.AND, when -> clientesConsultaBean.size() > 0)
//                .equal("entidade.sdEntidade.dividaSerasa", clienteSerasa.equals("S"), TipoExpressao.AND, when -> !clienteSerasa.equals("A"))
//                .isIn("entidade.sitCli.codigo", sitClis, TipoExpressao.AND, when -> sitClis.length > 0)
//                .isIn("situacao", tabSit, TipoExpressao.AND, when -> tabSit.length > 0)
//        );
        for (ParametroConsulta parametro : parametrosBean.sorted(Comparator.comparing(ParametroConsulta::getIndice))) {
            where = where.orderBy(parametro.getField(), parametro.getTipoOrder().equals("ASC") ? OrderType.ASC : OrderType.DESC);
        }


        todosTitulos = ((List<VSdTitulosDispContrato>) where.resultList()).stream().map(it -> it.getNumero()).collect(Collectors.toList());

//        List<SdItemContratoFidic> itensContratoSemBordero = (List<SdItemContratoFidic>) new FluentDao().selectFrom(SdItemContratoFidic.class).where(it -> it
//                .notEqual("contrato.id", contratoSelecionado.getId())
//                .notEqual("titulo.bordero", 0)
//        ).resultList();


        BigDecimal valorMaximo = new BigDecimal(valorTotal).multiply(BIGDECIMAL_100.add(new BigDecimal(taxa.replace(",", "."))).divide(BIGDECIMAL_100, 2, RoundingMode.HALF_UP));
        BigDecimal somador = titulosRecomendados.stream().map(Receber::getValorTitulo).reduce(BigDecimal.ZERO, BigDecimal::add);


        BigDecimal valorMinimoTitulos = new BigDecimal(valTituloBegin);

        for (Receber titulo : todosTitulos) {
            if (somador.add(titulo.getValorTitulo()).compareTo(valorMaximo) <= 0 && !manterTit) {
                titulosRecomendados.add(titulo);
                somador = somador.add(titulo.getValorTitulo());
            }

            if (somador.add(valorMinimoTitulos).compareTo(valorMaximo) > 0) {
                contratoCompleto = true;
                break;
            }
        }
        for (ClienteConsulta clienteConsulta : clientesConsultaBean) {
            where = new FluentDao().selectFrom(Receber.class).where(it -> it
                    .between("dtemissao", clienteConsulta.getDtEmissaoBegin(), clienteConsulta.getDtEmissaoEnd())
                    .between("dtvencto", clienteConsulta.getDtVenctoBegin(), clienteConsulta.getDtVenctoEnd())
                    .between("valor", clienteConsulta.getValTituloBegin(), clienteConsulta.getValTituloEnd())
                    .equal("bordero", "0")
                    .equal("status", "N")
                    .equal("codcli", clienteConsulta.getCodcli().getStringCodcli())
                    .isIn("situacao", clienteConsulta.getSituacoes() == null ? new Object[]{} : clienteConsulta.getSituacoes().stream().map(TabSit::getCodigo).toArray(), TipoExpressao.AND, when -> clienteConsulta.getSituacoes() != null && clienteConsulta.getSituacoes().size() > 0)
            );
            for (ParametroConsulta parametro : parametrosBean.sorted(Comparator.comparing(ParametroConsulta::getIndice))) {
                where = where.orderBy(parametro.getField(), parametro.getTipoOrder().equals("ASC") ? OrderType.ASC : OrderType.DESC);
            }

            BigDecimal somadorCliente = BigDecimal.ZERO;

            List<Receber> titulos = (List<Receber>) where.resultList();
            titulos.removeIf(item -> item.getValorTitulo().doubleValue() < clienteConsulta.getValTituloBegin().doubleValue() || item.getValorTitulo().doubleValue() > clienteConsulta.getValTituloEnd().doubleValue());
            todosTitulos.addAll(titulos);
            if (!contratoCompleto) {
                for (Receber titulo : titulos) {
                    if (somador.add(titulo.getValorTitulo()).compareTo(valorMaximo) <= 0 && (clienteConsulta.getValorMaximo().compareTo(BigDecimal.ZERO) == 0 || somadorCliente.add(titulo.getValorTitulo()).compareTo(clienteConsulta.getValorMaximo()) <= 0) && !manterTit) {
                        titulosRecomendados.add(titulo);
                        somador = somador.add(titulo.getValorTitulo());
                        somadorCliente = somadorCliente.add(titulo.getValorTitulo());
                    }

                    if (clienteConsulta.getValorMaximo().compareTo(BigDecimal.ZERO) > 0 && somadorCliente.add(BigDecimal.valueOf(clienteConsulta.getValTituloBegin().doubleValue())).compareTo(clienteConsulta.getValorMaximo()) >= 0) {
                        break;
                    }

                    if (somador.add(valorMinimoTitulos).compareTo(valorMaximo) >= 0) {
                        contratoCompleto = true;
                        break;
                    }
                }
            }
            if (contratoCompleto) break;
        }
        todosTitulos.removeAll(titulosRecomendados);
        titulosAuxiliares.addAll(todosTitulos);
    }

    @Override
    public void closeWindow() {

    }


    public static class ClienteConsulta {
        private Entidade codcli;
        private final ObjectProperty<BigDecimal> valorMaximo = new SimpleObjectProperty<>(BigDecimal.ZERO);
        private final ListProperty<TabSit> situacoes = new SimpleListProperty<>();
        private final ObjectProperty<LocalDate> dtEmissaoBegin = new SimpleObjectProperty<>(LocalDate.now().minusMonths(3));
        private final ObjectProperty<LocalDate> dtEmissaoEnd = new SimpleObjectProperty<>(LocalDate.now());
        private final ObjectProperty<LocalDate> dtVenctoBegin = new SimpleObjectProperty<>(LocalDate.now());
        private final ObjectProperty<LocalDate> dtVenctoEnd = new SimpleObjectProperty<>(LocalDate.now().plusMonths(3));
        private final ObjectProperty<Number> valTituloBegin = new SimpleObjectProperty<>(0);
        private final ObjectProperty<Number> valTituloEnd = new SimpleObjectProperty<>(10000);

        public ClienteConsulta(Entidade entidade) {
            this.codcli = entidade;
        }

        public Entidade getCodcli() {
            return codcli;
        }

        public void setCodcli(Entidade codcli) {
            this.codcli = codcli;
        }

        public BigDecimal getValorMaximo() {
            return valorMaximo.get();
        }

        public ObjectProperty<BigDecimal> valorMaximoProperty() {
            return valorMaximo;
        }

        public void setValorMaximo(BigDecimal valorMaximo) {
            this.valorMaximo.set(valorMaximo);
        }

        public ObservableList<TabSit> getSituacoes() {
            return situacoes.get();
        }

        public ListProperty<TabSit> situacoesProperty() {
            return situacoes;
        }

        public void setSituacoes(ObservableList<TabSit> situacoes) {
            this.situacoes.set(situacoes);
        }

        public LocalDate getDtEmissaoBegin() {
            return dtEmissaoBegin.get();
        }

        public ObjectProperty<LocalDate> dtEmissaoBeginProperty() {
            return dtEmissaoBegin;
        }

        public void setDtEmissaoBegin(LocalDate dtEmissaoBegin) {
            this.dtEmissaoBegin.set(dtEmissaoBegin);
        }

        public LocalDate getDtEmissaoEnd() {
            return dtEmissaoEnd.get();
        }

        public ObjectProperty<LocalDate> dtEmissaoEndProperty() {
            return dtEmissaoEnd;
        }

        public void setDtEmissaoEnd(LocalDate dtEmissaoEnd) {
            this.dtEmissaoEnd.set(dtEmissaoEnd);
        }

        public LocalDate getDtVenctoBegin() {
            return dtVenctoBegin.get();
        }

        public ObjectProperty<LocalDate> dtVenctoBeginProperty() {
            return dtVenctoBegin;
        }

        public void setDtVenctoBegin(LocalDate dtVenctoBegin) {
            this.dtVenctoBegin.set(dtVenctoBegin);
        }

        public LocalDate getDtVenctoEnd() {
            return dtVenctoEnd.get();
        }

        public ObjectProperty<LocalDate> dtVenctoEndProperty() {
            return dtVenctoEnd;
        }

        public void setDtVenctoEnd(LocalDate dtVenctoEnd) {
            this.dtVenctoEnd.set(dtVenctoEnd);
        }

        public Number getValTituloBegin() {
            return valTituloBegin.get();
        }

        public ObjectProperty<Number> valTituloBeginProperty() {
            return valTituloBegin;
        }

        public void setValTituloBegin(Number valTituloBegin) {
            this.valTituloBegin.set(valTituloBegin);
        }

        public Number getValTituloEnd() {
            return valTituloEnd.get();
        }

        public ObjectProperty<Number> valTituloEndProperty() {
            return valTituloEnd;
        }

        public void setValTituloEnd(Number valTituloEnd) {
            this.valTituloEnd.set(valTituloEnd);
        }
    }
}
