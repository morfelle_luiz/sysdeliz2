package sysdeliz2.controllers.views.financeiro;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.financeiro.SdTransacaoCartao;
import sysdeliz2.utils.gui.window.base.WindowBase;

import javax.persistence.criteria.JoinType;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class RecebimentoComCartaoController extends WindowBase {

    public RecebimentoComCartaoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }



    protected List<SdTransacaoCartao> getTransacoes(LocalDateTime dataInicio, LocalDateTime dataFim, String documentos, Object[] clientes) {
        String[] doctos = documentos == null ? new String[]{} : documentos.replace(" ", "").split(",");
        return (List<SdTransacaoCartao>) new FluentDao().selectFrom(SdTransacaoCartao.class)
                .join("doctos", JoinType.INNER)
                .distinct()
                .where(eb -> eb
                        .isIn("codcli.codcli",clientes, TipoExpressao.AND, b -> clientes.length > 0)
                        .between("dttransacao", dataInicio, dataFim)
                        .isIn("doctos>id.docto", doctos, TipoExpressao.AND, b -> doctos.length > 0))
                .resultList();
    }

    protected void criarAntecipacao(double valorBrt, double valorLiq, String codcli, String observacao, String transacao) throws SQLException {
        new NativeDAO().runNativeQueryProcedure("pkg_b2b.cadastraAntecipacao(%s,%s,'%s','%s','%s','CIELO')",
                valorBrt, valorLiq, codcli, observacao, transacao);
    }

    @Override
    public void closeWindow() {

    }
}
