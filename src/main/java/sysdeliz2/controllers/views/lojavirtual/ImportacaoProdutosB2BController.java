package sysdeliz2.controllers.views.lojavirtual;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Pedido3Bean;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static sysdeliz2.utils.SortUtils.distinctByKey;

public class ImportacaoProdutosB2BController extends WindowBase {

    protected List<Pedido3Bean> pedidos = new ArrayList<>();

    public ImportacaoProdutosB2BController(String title, Image icone) {
        super(title, icone);
    }

    protected void bucarPedidos(String numeroPedido, String nota, LocalDate data, boolean useDtLeitura) {
        pedidos = (List<Pedido3Bean>) new FluentDao().selectFrom(Pedido3Bean.class).where(it -> it
                .like("id.numero", numeroPedido, TipoExpressao.AND, when -> !numeroPedido.equals(""))
                .equal("notaFiscal", nota, TipoExpressao.AND, when -> !nota.equals(""))
                .equal("data", data, TipoExpressao.AND, when -> useDtLeitura)
        ).resultList();

        if (pedidos != null && pedidos.size() > 0) {

            pedidos = pedidos.stream()
                    .map(it -> new Pedido3Bean(it.getId().getNumero(), it.getNotaFiscal(), it.getData()))
                    .filter(distinctByKey(p -> p.getData()))
                    .collect(Collectors.toList());

            for (Pedido3Bean pedido : pedidos) {
                List<Pedido3Bean> itens = (List<Pedido3Bean>) new FluentDao()
                        .selectFrom(Pedido3Bean.class)
                        .where(it -> it
                                .equal("id.numero", pedido.getId().getNumero())
                                .equal("data", pedido.getData()))
                        .resultList();
                if (itens != null) {
                    pedido.setItens(itens);
                    pedido.setImportado(itens.stream().allMatch(Pedido3Bean::getImportado));
                }
            }
        }
    }

    @Override
    public void closeWindow() {

    }
}
