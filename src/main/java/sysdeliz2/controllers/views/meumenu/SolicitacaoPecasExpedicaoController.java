package sysdeliz2.controllers.views.meumenu;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdTipoSolicitacaoExp;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SolicitacaoPecasExpedicaoController extends WindowBase {

    protected List<SdSolicitacaoExp> solicitacoesExp = new ArrayList<>();

    public SolicitacaoPecasExpedicaoController(String title, Image icone) {
        super(title, icone);
    }

    protected void buscarSolicitacoes(Object[] solicitacoes, SdTipoSolicitacaoExp tipoSolicitacaoExp, LocalDate dataStart, LocalDate dataEnd, Object[] produtos) {
        Object[] solicitacoesId = solicitacoes == null ? new Object[]{} : solicitacoes;

        List<SdSolicitacaoExp> listSolicitacao = (List<SdSolicitacaoExp>) new FluentDao().selectFrom(SdSolicitacaoExp.class).where(it -> it
                .isIn("id", solicitacoesId, TipoExpressao.AND, when -> solicitacoesId.length > 0)
                .equal("tipo.tipo", tipoSolicitacaoExp.getTipo(), TipoExpressao.AND, when -> tipoSolicitacaoExp.getTipo() != null)
                .between("dtSolicitacao", dataStart, dataEnd)
                .equal("usuario", Globals.getNomeUsuario())
        ).resultList();

        if (listSolicitacao != null && produtos.length > 0) {
            listSolicitacao = listSolicitacao.stream().filter(it -> it.getItens().stream().anyMatch(eb -> Arrays.stream(produtos).anyMatch(ob -> ob.toString().equals(eb.getProduto().getCodigo())))).collect(Collectors.toList());
        }

        solicitacoesExp = listSolicitacao;
    }

    @Override
    public void closeWindow() {

    }
}
