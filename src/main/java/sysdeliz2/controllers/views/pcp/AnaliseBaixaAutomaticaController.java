package sysdeliz2.controllers.views.pcp;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdMateriaisFaltantes;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdGestaoMatFaltante;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class AnaliseBaixaAutomaticaController extends WindowBase {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    protected final ListProperty<VSdDadosOfPendente> ofsPendentes = new SimpleListProperty<>();
    protected final ListProperty<SdReservaMateriais> reservaMateriaisOf = new SimpleListProperty<>();
    protected final ListProperty<SdMateriaisFaltantes> materiaisFaltantesOf = new SimpleListProperty<>();
    protected final ListProperty<VSdGestaoMatFaltante> materiaisFaltantes = new SimpleListProperty<>();
    protected final ListProperty<Cor> coresMaterial = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Objeto">
    protected final ObjectProperty<VSdDadosOfPendente> ofSelecionada = new SimpleObjectProperty<>(new VSdDadosOfPendente());
    // </editor-fold>
    
    public AnaliseBaixaAutomaticaController(String title, Image icone, String[] abas) {
        super(title, icone, abas);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getReservasOf(String numero) {
        reservaMateriaisOf.set(FXCollections.observableList((List<SdReservaMateriais>)
                new FluentDao().selectFrom(SdReservaMateriais.class)
                        .where(eb -> eb
                                .equal("id.numero", numero)
                        ).resultList()
        ));
    }
    
    protected void getFaltantesOf(String numero) {
        materiaisFaltantesOf.set(FXCollections.observableList((List<SdMateriaisFaltantes>)
                new FluentDao().selectFrom(SdMateriaisFaltantes.class)
                        .where(eb -> eb
                                .equal("id.numero", numero)
                                .notEqual("status", "R")
                        ).resultList()
        ));
    }
    
    protected void loadOfsPendentes() {
        this.loadOfsPendentes(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    
    protected void loadOfsPendentes(String corte, String costura, String acabamento, String outros, String lavanderia,
                                    String[] of, Object[] marca, Object[] produto, Object[] colecao, Object[] linha,
                                    Object[] familia, Object[] setor, Object[] faccao, String pais) {
        String pcorte = corte != null ? corte : "TODOS";
        String pcostura = costura != null ? costura : "TODOS";
        String pacabamento = acabamento != null ? acabamento : "TODOS";
        String poutros = outros != null ? outros : "TODOS";
        String ppais = pais != null ? pais : "A";
        lavanderia = lavanderia == null ? "AMBOS" : lavanderia;
        Boolean plavanderia = lavanderia.equals("AMBOS") ? true : Boolean.valueOf(lavanderia);
        String[] pof = of != null ? of : new String[]{};
        Object[] psetor = setor != null ? setor : new Object[]{};
        Object[] pproduto = produto != null ? produto : new Object[]{};
        Object[] pmarca = marca != null ? marca : new Object[]{};
        Object[] pcolecao = colecao != null ? colecao : new Object[]{};
        Object[] plinha = linha != null ? linha : new Object[]{};
        Object[] pfamilia = familia != null ? familia : new Object[]{};
        Object[] pFaccao = faccao != null ? faccao : new Object[]{};
        String finalLavanderia = lavanderia;
        JPAUtils.clearEntitys(ofsPendentes);
        ofsPendentes.set(FXCollections.observableList((List<VSdDadosOfPendente>) new FluentDao().selectFrom(VSdDadosOfPendente.class).where(it -> it
                .equal("cort", pcorte, TipoExpressao.AND, b -> !pcorte.equals("TODOS"))
                .equal("cost", pcostura, TipoExpressao.AND, b -> !pcostura.equals("TODOS"))
                .equal("acab", pacabamento, TipoExpressao.AND, b -> !pacabamento.equals("TODOS"))
                .equal("aplic", poutros, TipoExpressao.AND, b -> !poutros.equals("TODOS"))
                .equal("lavand", plavanderia, TipoExpressao.AND, b -> !finalLavanderia.equals("AMBOS"))
                .equal("pais", ppais, TipoExpressao.AND, b -> !ppais.equals("A"))
                .isIn("id.numero", pof, TipoExpressao.AND, b -> pof.length > 0)
                .isIn("id.setor.codigo", psetor, TipoExpressao.AND, b -> psetor.length > 0)
                .isIn("codigo.codigo", pproduto, TipoExpressao.AND, b -> pproduto.length > 0)
                .isIn("codigo.codMarca", pmarca, TipoExpressao.AND, b -> pmarca.length > 0)
                .isIn("codigo.codCol", pcolecao, TipoExpressao.AND, b -> pcolecao.length > 0)
                .isIn("codigo.codlin", plinha, TipoExpressao.AND, b -> plinha.length > 0)
                .isIn("codigo.codFam", pfamilia, TipoExpressao.AND, b -> pfamilia.length > 0)
                .isIn("faccao.codcli", pFaccao, TipoExpressao.AND, b -> pFaccao.length > 0)
        ).resultList()));
    }
    
    protected void estornarReservaMaterial(SdReservaMateriais reservaMaterial, BigDecimal qtdeEstorno) {
        if (reservaMaterial != null && qtdeEstorno.compareTo(BigDecimal.ZERO) > 0) {
            try {
                MatMov matMovSaida = new FluentDao().selectFrom(MatMov.class)
                        .where(it -> it
                                .equal("ordem", reservaMaterial.getMovimento()))
                        .singleResult();
                if (matMovSaida == null) {
                    MessageBox.create(message -> {
                        message.message("Não foi possível encontra o movimento da reserva do material.");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showAndWait();
                    });
                    return;
                }
                MatMov matMovSaidaExtorno = null;
                
                List<SdReservaMateriais> baixasDoMaterial = (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                        .where(it -> it
                                .equal("id.numero", reservaMaterial.getId().getNumero())
                                .equal("id.insumo.codigo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("id.corI.cor", reservaMaterial.getId().getCorI().getCor())
                                .equal("id.setor", reservaMaterial.getId().getSetor()))
                        .resultList();
                BigDecimal baixadoOf = new BigDecimal(baixasDoMaterial.stream().mapToDouble(reserva -> reserva.getQtdeB().doubleValue()).sum());
                if (qtdeEstorno.compareTo(baixadoOf) < 0) {
                    matMovSaidaExtorno = new MatMov(reservaMaterial.getId().getInsumo().getCodigo(),
                            "S",
                            null,
                            reservaMaterial.getId().getNumero(),
                            qtdeEstorno,
                            reservaMaterial.getId().getInsumo().getPreco(),
                            reservaMaterial.getId().getInsumo().getPreco(),
                            reservaMaterial.getId().getCorI().getCor(),
                            "99",
                            null,
                            reservaMaterial.getId().getDeposito(),
                            null,
                            LocalDate.now(),
                            reservaMaterial.getId().getLote(),
                            matMovSaida.getDescricao(),
                            null,
                            null,
                            "EC",
                            LocalDateTime.now());
                    new FluentDao().merge(matMovSaidaExtorno);
                    
                    matMovSaida.setQtde(matMovSaida.getQtde().subtract(qtdeEstorno));
                } else {
                    matMovSaida.setTpmov("EC");
                }
                
                MatMov matMovEntrada = new MatMov(reservaMaterial.getId().getInsumo().getCodigo(),
                        "E",
                        null,
                        reservaMaterial.getId().getNumero(),
                        qtdeEstorno,
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getCorI().getCor(),
                        "99",
                        null,
                        reservaMaterial.getId().getDeposito(),
                        null,
                        LocalDate.now(),
                        reservaMaterial.getId().getLote(),
                        SceneMainController.getAcesso().getUsuario() + "-Estorno de consumo",
                        null,
                        null,
                        "EC",
                        LocalDateTime.now());
                
                MatIten matIten = (MatIten) new FluentDao().selectFrom(MatIten.class)
                        .where(it -> it
                                .equal("id.cor", reservaMaterial.getId().getCorI().getCor())
                                .equal("id.lote", reservaMaterial.getId().getLote())
                                .equal("id.codigo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("id.deposito.codigo", reservaMaterial.getId().getDeposito())
                        ).singleResult();
                if (matIten != null)
                    matIten.setQtde(matIten.getQtde().add(qtdeEstorno));
                
                List<PcpFtOf> pcpFtOf = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                        .where(it -> it
                                .equal("pcpFtOfID.numero", reservaMaterial.getId().getNumero())
                                .equal("pcpFtOfID.insumo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("pcpFtOfID.corI", reservaMaterial.getId().getCorI().getCor())
                                .equal("pcpFtOfID.setor", reservaMaterial.getId().getSetor())
                                .equal("pcpFtOfID.faixa", reservaMaterial.getId().getFaixa())
                                .equal("pcpFtOfID.id", reservaMaterial.getId().getIdPcpftof())
                        ).resultList();
                
                pcpFtOf.forEach(it -> {
                    it.setSituacao(qtdeEstorno.compareTo(baixadoOf) == 0 ? "A" : "P");
                    new FluentDao().merge(it);
                });
                if (matIten != null)
                    new FluentDao().merge(matIten);
                new FluentDao().persist(matMovEntrada);
                
                if (reservaMaterial.getQtdeB().subtract(qtdeEstorno).doubleValue() == 0) {
                    new FluentDao().delete(reservaMaterial);
                    new NativeDAO().runNativeQueryUpdate("delete from mat_reserva_001 where numero = '%s' and codigo = '%s' and lote = '%s' and cor = '%s' and deposito = '%s'",
                            reservaMaterial.getId().getNumero(), reservaMaterial.getId().getInsumo().getCodigo(), reservaMaterial.getId().getLote(), reservaMaterial.getId().getCorI().getCor(), reservaMaterial.getId().getDeposito());
                    reservaMateriaisOf.remove(reservaMaterial);
                } else {
                    reservaMaterial.setQtdeB(reservaMaterial.getQtdeB().subtract(qtdeEstorno));
                    new FluentDao().merge(reservaMaterial);
                }
                // jogar material para falta
                SdMateriaisFaltantes faltaMaterial = new FluentDao().selectFrom(SdMateriaisFaltantes.class)
                        .where(it -> it
                                .equal("id.insumo.codigo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("id.numero", reservaMaterial.getId().getNumero())
                                .equal("id.corI.cor", reservaMaterial.getId().getCorI().getCor())
                                .equal("id.setor", reservaMaterial.getId().getSetor())
                                .equal("id.faixa", reservaMaterial.getId().getFaixa())
                                .equal("id.idPcpftof", reservaMaterial.getId().getIdPcpftof()))
                        .singleResult();
                if (faltaMaterial != null) {
                    faltaMaterial.setDhAnalise(LocalDateTime.now());
                    faltaMaterial.setQtde(faltaMaterial.getQtde().add(qtdeEstorno));
                    faltaMaterial.setStatus(faltaMaterial.getStatus().equals("R") ? "A" : faltaMaterial.getStatus());
                } else {
                    faltaMaterial = new SdMateriaisFaltantes(reservaMaterial.getId().getNumero(), reservaMaterial.getId().getInsumo(),
                            reservaMaterial.getId().getCorI(), reservaMaterial.getId().getSetor(), reservaMaterial.getId().getFaixa(),
                            reservaMaterial.getId().getIdPcpftof(), qtdeEstorno, LocalDateTime.now(), "A", reservaMaterial.isSubstituto());
                    faltaMaterial.setDhCriacao(LocalDateTime.now());
                    materiaisFaltantesOf.add(faltaMaterial);
                }
                new FluentDao().merge(faltaMaterial);
                
                SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.MOVIMENTAR, reservaMaterial.getId().getNumero(),
                        "Extorno de baixa de consumo de " + reservaMaterial.getQtdeB() + " " + reservaMaterial.getId().getInsumo().getUnidade().getDescricao() +
                                " do material " + reservaMaterial.getId().getInsumo() + " na cor " + reservaMaterial.getId().getCorI() + " do depósito " + reservaMaterial.getId().getDeposito());
                
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }
    
    protected void movimentarEstoqueMaterial(SdReservaMateriais reservaMaterial, MatMov movSaida, MatMov movEntrada) {
        if (reservaMaterial != null) {
            try {
                MatMov matMovSaida = new MatMov(reservaMaterial.getId().getInsumo().getCodigo(),
                        "S",
                        null,
                        reservaMaterial.getId().getNumero(),
                        reservaMaterial.getQtdeB(),
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getCorI().getCor(),
                        "5",
                        null,
                        movEntrada.getDeposito(),
                        null,
                        LocalDate.now(),
                        reservaMaterial.getId().getLote(),
                        SceneMainController.getAcesso().getUsuario() + "-Estorno de movimento extra",
                        null,
                        null,
                        "MN",
                        LocalDateTime.now());
                
                MatMov matMovEntrada = new MatMov(reservaMaterial.getId().getInsumo().getCodigo(),
                        "E",
                        null,
                        reservaMaterial.getId().getNumero(),
                        reservaMaterial.getQtdeB(),
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getInsumo().getPreco(),
                        reservaMaterial.getId().getCorI().getCor(),
                        "5",
                        null,
                        movSaida.getDeposito(),
                        null,
                        LocalDate.now(),
                        reservaMaterial.getId().getLote(),
                        SceneMainController.getAcesso().getUsuario() + "-Estorno de movimento extra",
                        null,
                        null,
                        "MN",
                        LocalDateTime.now());
                
                MatIten matItenEntrada = (MatIten) new FluentDao().selectFrom(MatIten.class)
                        .where(it -> it
                                .equal("id.cor", reservaMaterial.getId().getCorI().getCor())
                                .equal("id.lote", reservaMaterial.getId().getLote())
                                .equal("id.codigo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("id.deposito.codigo", movSaida.getDeposito())
                        ).singleResult();
                if (matItenEntrada != null)
                    matItenEntrada.setQtde(matItenEntrada.getQtde().add(reservaMaterial.getQtdeB()));
                
                MatIten matItenSaida = (MatIten) new FluentDao().selectFrom(MatIten.class)
                        .where(it -> it
                                .equal("id.cor", reservaMaterial.getId().getCorI().getCor())
                                .equal("id.lote", reservaMaterial.getId().getLote())
                                .equal("id.codigo", reservaMaterial.getId().getInsumo().getCodigo())
                                .equal("id.deposito.codigo", movEntrada.getDeposito())
                        ).singleResult();
                if (matItenSaida != null)
                    matItenSaida.setQtde(matItenSaida.getQtde().subtract(reservaMaterial.getQtdeB()));
                
                if (matItenEntrada != null)
                    new FluentDao().merge(matItenEntrada);
                if (matItenSaida != null)
                    new FluentDao().merge(matItenSaida);
                new FluentDao().persist(matMovEntrada);
                new FluentDao().persist(matMovSaida);
                new FluentDao().delete(reservaMaterial);
                reservaMateriaisOf.remove(reservaMaterial);
                
                SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.MOVIMENTAR, reservaMaterial.getId().getNumero(),
                        "Extorno de baixa de consumo de " + reservaMaterial.getQtdeB() + " " + reservaMaterial.getId().getInsumo().getUnidade().getDescricao() +
                                " do material " + reservaMaterial.getId().getInsumo() + " na cor " + reservaMaterial.getId().getCorI() + " do depósito " + reservaMaterial.getId().getDeposito());
                
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }
    
    protected void abrirReservaMaterial(SdReservaMateriais reservaMaterial) {
        if (reservaMaterial != null) {
            List<PcpFtOf> pcpFtOf = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", reservaMaterial.getId().getNumero())
                            .equal("pcpFtOfID.insumo", reservaMaterial.getId().getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", reservaMaterial.getId().getCorI().getCor())
                            .equal("pcpFtOfID.setor", reservaMaterial.getId().getSetor())
                            .equal("pcpFtOfID.faixa", reservaMaterial.getId().getFaixa())
                            .equal("pcpFtOfID.id", reservaMaterial.getId().getIdPcpftof())
                    ).resultList();
            pcpFtOf.forEach(it -> {
                it.setSituacao("P");
                new FluentDao().merge(it);
            });
            
            
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, reservaMaterial.getId().getNumero(),
                    "Baixa de consumo de " + reservaMaterial.getQtdeB() + " " + reservaMaterial.getId().getInsumo().getUnidade().getDescricao() +
                            " do material " + reservaMaterial.getId().getInsumo() + " na cor " + reservaMaterial.getId().getCorI() + " do depósito " + reservaMaterial.getId().getDeposito() +
                            " aberta para nova baixa automática");
            
        }
    }
    
    protected void sendColetaOf(VSdDadosOfPendente ofPendente) {
        if (ofPendente != null) {
            new FluentDao().runNativeQueryUpdate(String.format("update sd_of1_001 set liberado_coleta = 'S' where numero = '%s'", ofPendente.getId().getNumero()));
            
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, ofPendente.getId().getNumero(),
                    "OF enviada para coleta.");
            
        }
    }
    
    protected void substituirMaterial(SdMateriaisFaltantes materialFaltante, Material materialSubs, Cor corSubs) {
        if (materialFaltante != null && materialSubs != null && corSubs != null) {
            List<PcpFtOf> materiaisOriginal = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", materialFaltante.getId().getNumero())
                            .equal("pcpFtOfID.insumo", materialFaltante.getId().getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", materialFaltante.getId().getCorI().getCor())
                            .equal("pcpFtOfID.setor", materialFaltante.getId().getSetor()))
                    .resultList();
            JPAUtils.clearEntitys(materiaisOriginal);
            materiaisOriginal.forEach(material -> {
                if (material.getInsumoOrig() == null) {
                    new FluentDao().runNativeQueryUpdate(String.format("update pcpftof_001 set insumo_orig = insumo, cor_i_orig = cor_i where numero = '%s' and setor = '%s' and insumo = '%s' and cor_i = '%s'",
                            material.getPcpFtOfID().getNumero(),
                            material.getPcpFtOfID().getSetor(),
                            material.getPcpFtOfID().getInsumo(),
                            material.getPcpFtOfID().getCorI()));
                }
                new FluentDao().runNativeQueryUpdate(String.format("update pcpftof_001 set insumo = '%s', cor_i = '%s' where numero = '%s' and setor = '%s' and insumo = '%s' and cor_i = '%s'",
                        materialSubs.getCodigo(),
                        corSubs.getCor(),
                        material.getPcpFtOfID().getNumero(),
                        material.getPcpFtOfID().getSetor(),
                        material.getPcpFtOfID().getInsumo(),
                        material.getPcpFtOfID().getCorI()));
            });
            
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, materialFaltante.getId().getNumero(),
                    "Subtituido material " + materialFaltante.getId().getInsumo() + " na cor " + materialFaltante.getId().getCorI() +
                            " pelo material " + materialSubs + " na cor " + corSubs);
            
        }
    }
    
    protected void considerarMaterialBaixado(SdMateriaisFaltantes materialFaltante) {
        if (materialFaltante != null) {
            List<PcpFtOf> pcpFtOf = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", materialFaltante.getId().getNumero())
                            .equal("pcpFtOfID.insumo", materialFaltante.getId().getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", materialFaltante.getId().getCorI().getCor())
                            .equal("pcpFtOfID.setor", materialFaltante.getId().getSetor())
                    ).resultList();
            pcpFtOf.forEach(it -> {
                it.setSituacao("B");
                new FluentDao().merge(it);
            });
            
            materialFaltante.setStatus("R");
            new FluentDao().merge(materialFaltante);
            materiaisFaltantesOf.remove(materialFaltante);
            
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, materialFaltante.getId().getNumero(),
                    "Material " + materialFaltante.getId().getInsumo() + " na cor " + materialFaltante.getId().getCorI() + " marcado com baixado na falta.");
            
        }
    }
    
    protected void considerarFaltaComoBaixado(VSdGestaoMatFaltante materialFaltante) {
        if (materialFaltante != null) {
            List<PcpFtOf> pcpFtOf = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", materialFaltante.getNumero())
                            .equal("pcpFtOfID.insumo", materialFaltante.getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", materialFaltante.getCori().getCor())
                            .equal("pcpFtOfID.setor", materialFaltante.getSetor().getCodigo())
                    ).resultList();
            pcpFtOf.forEach(it -> {
                it.setSituacao("B");
                new FluentDao().merge(it);
            });
            
            List<SdMateriaisFaltantes> faltaMaterial = (List<SdMateriaisFaltantes>) new FluentDao().selectFrom(SdMateriaisFaltantes.class)
                    .where(it -> it
                            .equal("id.numero", materialFaltante.getNumero())
                            .equal("id.setor", materialFaltante.getSetor().getCodigo())
                            .equal("id.insumo.codigo", materialFaltante.getInsumo().getCodigo())
                            .equal("id.corI.cor", materialFaltante.getCori().getCor())
                            .notEqual("status", "R"))
                    .resultList();
            faltaMaterial.forEach(falta -> {
                falta.setStatus("R");
                new FluentDao().merge(falta);
            });
            
            materiaisFaltantes.remove(materialFaltante);
            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, materialFaltante.getNumero(),
                    "Material " + materialFaltante.getInsumo() + " na cor " + materialFaltante.getCori() + " marcado com baixado na falta.");
            
        }
    }
    
    protected void getCoresMaterial(String codigo) {
        List<String> coresMaterial = (List<String>) ((List<MatIten>) new FluentDao().selectFrom(MatIten.class)
                .where(it -> it
                        .equal("id.codigo", codigo)
                        .notEqual("id.deposito.codigo", "07153")).resultList()).stream()
                .map(cor -> cor.getId().getCor()).distinct().collect(Collectors.toList());
        
        this.coresMaterial.set(FXCollections.observableList((List<Cor>) new FluentDao().selectFrom(Cor.class)
                .where(it -> it
                        .isIn("cor", coresMaterial.toArray())
                ).resultList()));
    }
    
    protected void getFaltantes(Object[] numero, Object[] material, Object[] cor, Object[] setor, String substituto, String status, String tipoProduto, String pais) {
        Object[] pnumero = numero != null ? numero : new Object[]{};
        Object[] pmaterial = material != null ? material : new Object[]{};
        Object[] pcor = cor != null ? cor : new Object[]{};
        Object[] psetor = setor != null ? setor : new Object[]{};
        Object[] pstatus = status.equals("D") ? new Object[]{"A", "C"} : new Object[]{status};
        String ptipoProduto = tipoProduto != null ? tipoProduto : "T";
        
        JPAUtils.clearEntitys(materiaisFaltantes);
        materiaisFaltantes.set(FXCollections.observableList((List<VSdGestaoMatFaltante>)
                new FluentDao().selectFrom(VSdGestaoMatFaltante.class)
                        .where(eb -> eb
                                .isIn("numero", pnumero, TipoExpressao.AND, b -> pnumero.length > 0)
                                .isIn("insumo.codigo", pmaterial, TipoExpressao.AND, b -> pmaterial.length > 0)
                                .isIn("cori.cor", pcor, TipoExpressao.AND, b -> pcor.length > 0)
                                .isIn("setor.codigo", psetor, TipoExpressao.AND, b -> psetor.length > 0)
                                .isIn("status", pstatus, TipoExpressao.AND, b -> !status.equals("T"))
                                .equal("tipolinha", ptipoProduto, TipoExpressao.AND, b -> !ptipoProduto.equals("T"))
                                .equal("pais", pais, TipoExpressao.AND, b -> !pais.equals("A"))
                                .equal("substituto", Boolean.valueOf(substituto.equals("T") ? "false" : substituto), TipoExpressao.AND, b -> !substituto.equals("T"))
                        ).resultList()
        ));
    }
}
