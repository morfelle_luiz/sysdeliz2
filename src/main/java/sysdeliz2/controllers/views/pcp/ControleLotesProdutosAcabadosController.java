package sysdeliz2.controllers.views.pcp;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.EntradaPA.SdBarraCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdLotePa;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ControleLotesProdutosAcabadosController extends WindowBase {

    protected List<SdLotePa> listLotes = new ArrayList<>();

    public ControleLotesProdutosAcabadosController(String title, Image icone) {
        super(title, icone);
    }

    protected void buscarLotes(Object[] lotes, Object[] faccao, ObservableList<Of1> ofs, ObservableList<SdCaixaPA> caixas, ObservableList<VSdDadosProduto> produto, ObservableList<SdBarraCaixaPA> barra, LocalDate dtEnvioInicio, LocalDate dtEnvioFim, LocalDate dtEmissaoInicio, LocalDate dtEmissaoFim) {
        JPAUtils.clearEntitys(listLotes);
        listLotes = (List<SdLotePa>) new FluentDao()
                .selectFrom(SdLotePa.class)
                .where(it -> it
                        .isIn("id", lotes, TipoExpressao.AND, when -> lotes.length > 0)
                        .isIn("codfor.codcli", faccao, TipoExpressao.AND, when -> faccao.length > 0)
                        .between("dtemissao", dtEmissaoInicio, dtEmissaoFim)
                        .between("dtenvio", dtEnvioInicio, dtEnvioFim)
                ).resultList();

        if (listLotes != null && listLotes.size() > 0) {
            if (ofs.size() > 0) {
                listLotes = listLotes.stream().filter(it -> it.getItensLote().stream().anyMatch(eb -> ofs.stream().anyMatch(ol -> ol.getNumero().equals(eb.getNumero())))).collect(Collectors.toList());
            }
            if (caixas.size() > 0) {
                listLotes = listLotes.stream().filter(it -> it.getItensLote().stream().anyMatch(eb -> eb.getListCaixas().stream().anyMatch(ol -> caixas.stream().anyMatch(cx -> cx.getId().equals(ol.getId()))))).collect(Collectors.toList());
            }
            if (produto.size() > 0) {
                listLotes = listLotes.stream().filter(it -> it.getItensLote().stream().anyMatch(eb -> produto.stream().anyMatch(ol -> ol.getCodigo().equals(eb.getProduto())))).collect(Collectors.toList());
            }
            if (barra.size() > 0) {
                listLotes = listLotes.stream().filter(it -> it.getItensLote().stream().anyMatch(eb -> eb.getListCaixas().stream().anyMatch(ol -> ol.getItensCaixa().stream().anyMatch(or -> or.getListBarra().stream().anyMatch(br -> barra.stream().anyMatch(bl -> bl.getBarra().equals(br.getBarra()))))))).collect(Collectors.toList());
            }
            listLotes = listLotes.stream().sorted(Comparator.comparing(SdLotePa::getId)).collect(Collectors.toList());
        }
    }

    protected ImageUtils.Icon getCaixaIcon(SdCaixaPA caixa) {

        return caixa.isSegunda() ?
                caixa.isFechada() ?
                        ImageUtils.Icon.CAIXA2 : ImageUtils.Icon.CAIXA_ABERTA2 :
                caixa.isIncompleta() ?
                        ImageUtils.Icon.CAIXA_INCOMPLETA :
                        caixa.isFechada() ?
                                ImageUtils.Icon.CAIXA : ImageUtils.Icon.CAIXA_ABERTA;

//        if (caixa.isSegunda()) {
//            if (caixa.isFechada()) return ImageUtils.Icon.CAIXA2;
//            else return ImageUtils.Icon.CAIXA_ABERTA2;
//        } else if(caixa.isIncompleta()){
//            return ImageUtils.Icon.CAIXA_INCOMPLETA;
//        } else {
//            if (caixa.isFechada()) return ImageUtils.Icon.CAIXA;
//            else return ImageUtils.Icon.CAIXA_ABERTA;
//        }
    }

    protected int sortTamanhos(String tam1, String tam2) {
        int result = 0;
        if (tam1.matches("[0-9]*")) {
            result = Integer.compare(Integer.parseInt(tam1), Integer.parseInt(tam2));
        } else {
            if (tam1.equals("PP")) {
                if (!tam2.equals("PP")) result = -1;
            }

            if (tam1.equals("P") && !tam2.equals(tam1)) {
                if ((tam2.equals("PP"))) result = 1;
                else result = -1;
            }

            if (tam1.equals("M") && !tam2.equals(tam1)) {
                if ((tam2.equals("PP") || (tam2.equals("P")))) result = 1;
                else result = -1;
            }

            if (tam1.equals("G") && !tam2.equals(tam1)) {
                if ((tam2.equals("PP") || (tam2.equals("P") || (tam2.equals("M")))))
                    result = 1;
                else result = -1;
            }

            if (tam1.equals("GG") && !tam2.equals(tam1)) {
                if (tam2.equals("XGG")) result = -1;
                else result = 1;
            }

            if (tam1.equals("XGG") && !tam2.equals(tam1)) {
                result = 1;
            }
        }
        return result;
    }

    @Override
    public void closeWindow() {

    }

}
