package sysdeliz2.controllers.views.pcp.almoxarifado;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdMateriaisFaltantes;
import sysdeliz2.models.sysdeliz.SdOfsColetor;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdFaccoesOf;
import sysdeliz2.models.view.VSdMateriaisParaColeta;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class ColetaOfController extends WindowBase {
    
    protected final ListProperty<SdOfsColetor> ofsColetor = new SimpleListProperty<>();
    protected final ListProperty<VSdMateriaisParaColeta> materiaisParaColeta = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected final ListProperty<VSdMateriaisParaColeta> materiaisColetados = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected SdColaborador colaborador;
    
    public ColetaOfController(String title, Image icone, Boolean closeButton) {
        super(title, icone, null, closeButton);
    }
    
    protected void colaboradorLogado(String usuario) {
        colaborador = new FluentDao().selectFrom(SdColaborador.class).where(it -> it.equal("usuario", usuario)).singleResult();
    }
    
    protected void carregaOfsColetor() {
        JPAUtils.getEntityManager().clear();
        ofsColetor.set(FXCollections.observableList((List<SdOfsColetor>) new FluentDao().selectFrom(SdOfsColetor.class)
                .where(it -> it
                        .equal("coletor", colaborador.getCodigo())
                        .notEqual("status", "C"))
                .orderBy("ordem", OrderType.ASC)
                .resultList()));
    }
    
    protected void carregaMateriaisOfColeta(String numero, String setor, String tipo) {
        JPAUtils.clearEntitys(materiaisColetados);
        JPAUtils.clearEntitys(materiaisParaColeta);
        List<VSdMateriaisParaColeta> materiaisOf = (List<VSdMateriaisParaColeta>) new FluentDao().selectFrom(VSdMateriaisParaColeta.class)
                .where(it -> it
                        .equal("coletor", colaborador.getCodigo())
                        .equal("numero", numero)
                        .equal("setor.codigo", setor)
                        .equal("tipo", tipo))
                .orderBy(Arrays.asList(new Ordenacao[]{
                        new Ordenacao("deposito"),
                        new Ordenacao("local"),
                        new Ordenacao("insumo.codigo"),
                        new Ordenacao("cori.cor")}))
                .resultList();
        
        materiaisParaColeta.clear();
        materiaisColetados.clear();
        materiaisOf.forEach(material -> {
            if (material.isColeta())
                materiaisColetados.add(material);
            else
                materiaisParaColeta.add(material);
        });
    }
    
    protected void coletarMaterial(VSdMateriaisParaColeta materialAcum) {
        List<SdReservaMateriais> reservasAcum = (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                .where(it -> it
                        .equal("id.numero", materialAcum.getNumero())
                        .equal("id.setor", materialAcum.getSetor().getCodigo())
                        .equal("id.insumo.codigo", materialAcum.getInsumo().getCodigo())
                        .equal("id.corI.cor", materialAcum.getCori().getCor())
                        .equal("id.lote", materialAcum.getLote())
                        .equal("id.deposito", materialAcum.getDeposito().getCodigo())
                        .equal("coletor", materialAcum.getColetor().getCodigo())
                        .equal("coleta", false)
                        .equal("tipo", materialAcum.getTipo()))
                .resultList();
        for (SdReservaMateriais materialColeta : reservasAcum) {
            materialColeta.setColeta(true);
            new FluentDao().merge(materialColeta);
            SysLogger.addSysDelizLog(
                    "Coleta Material",
                    TipoAcao.CADASTRAR,
                    materialColeta.getId().getNumero(),
                    "Coletado material: " + materialColeta.getId().getInsumo() + " na cor: " + materialColeta.getId().getCorI() + ", qtde: " + materialColeta.getQtdeB() + " por: " + materialColeta.getColetor() +
                            ". Setor: " + materialColeta.getId().getSetor() + " / Depósito: " + materialColeta.getId().getDeposito());
        }
        if (reservasAcum.size() > 0)
            materiaisColetados.add(0, materialAcum);
    }
    
    protected VSdFaccoesOf getFaccaoOfSetor(String numero, String setor) {
        return new FluentDao().selectFrom(VSdFaccoesOf.class)
                .where(it -> it
                        .equal("numero", numero)
                        .equal("setor", setor))
                .singleResult();
    }
    
    protected void finalizarColeta(SdOfsColetor ofColetor) {
        ofColetor.setStatus("C");
        ofColetor.setDtcoleta(LocalDateTime.now());
        if ((ofColetor.getDeposito().getCodigo().equals("0002") || ofColetor.getDeposito().getCodigo().equals("0008")) && ofColetor.getTipo().equals("N"))
            ofColetor.setImpresso(true);
        new FluentDao().merge(ofColetor);
        SysLogger.addSysDelizLog(
                "Coleta Material",
                TipoAcao.CADASTRAR,
                ofColetor.getNumero(),
                "Finalizada coleta da OF: " + ofColetor.getNumero() + " no setor: " + ofColetor.getSetor() + ", depósito: " + ofColetor.getDeposito().getCodigo());
    }
    
    protected void adicionarQtdeMaterial(VSdMateriaisParaColeta materialColeta, BigDecimal qtdeAdicional, SdOfsColetor ofColetor) {
        SdOfsColetor finalOfColetor = (SdOfsColetor) new FluentDao().selectFrom(SdOfsColetor.class).where(it -> it.equal("programacao", ofColetor.getProgramacao())).singleResult();
        ofColetor.setFaccao(finalOfColetor.getFaccao());
        Deposito depositoFaccao = new FluentDao().selectFrom(Deposito.class).where(it -> it.equal("codigo", ofColetor.getFaccao().getStringCodcli())).singleResult();
        if (depositoFaccao != null) {
            MatIten maiDepositoSaida = new FluentDao().selectFrom(MatIten.class)
                    .where(it -> it
                            .equal("id.codigo", materialColeta.getInsumo().getCodigo())
                            .equal("id.cor", materialColeta.getCori().getCor())
                            .equal("id.deposito.codigo", materialColeta.getDeposito().getCodigo())
                            .equal("id.lote", materialColeta.getLote()))
                    .singleResult();
            if (maiDepositoSaida != null)
                if (maiDepositoSaida.getQtde().compareTo(qtdeAdicional) > 0) {
                    MatIten maiDepositoEntrada = new FluentDao().selectFrom(MatIten.class)
                            .where(it -> it
                                    .equal("id.codigo", materialColeta.getInsumo().getCodigo())
                                    .equal("id.cor", materialColeta.getCori().getCor())
                                    .equal("id.deposito.codigo", depositoFaccao.getGrupoDep())
                                    .equal("id.lote", materialColeta.getLote()))
                            .singleResult();
                    if (maiDepositoEntrada == null)
                        maiDepositoEntrada = new MatIten(materialColeta.getCori().getCor(), materialColeta.getLote(), materialColeta.getInsumo().getCodigo(), depositoFaccao);
                    maiDepositoEntrada.setQtde(maiDepositoEntrada.getQtde().add(qtdeAdicional));
                    new FluentDao().merge(maiDepositoEntrada);
                    maiDepositoSaida.setQtde(maiDepositoSaida.getQtde().subtract(qtdeAdicional));
                    new FluentDao().merge(maiDepositoSaida);
                    
                    MatMov movSaida = new MatMov(
                            materialColeta.getInsumo().getCodigo(),
                            "S",
                            materialColeta.getNumero(),
                            qtdeAdicional,
                            materialColeta.getInsumo().getPreco(),
                            materialColeta.getInsumo().getPreco(),
                            materialColeta.getCori().getCor(),
                            "5",
                            materialColeta.getDeposito().getCodigo(),
                            LocalDate.now(),
                            materialColeta.getLote(),
                            ofColetor.getColetor() + " - Movto adicional facção coletor",
                            "MN",
                            LocalDateTime.now());
                    movSaida = new FluentDao().merge(movSaida);
                    MatMov movEntrada = new MatMov(
                            materialColeta.getInsumo().getCodigo(),
                            "E",
                            materialColeta.getNumero(),
                            qtdeAdicional,
                            materialColeta.getInsumo().getPreco(),
                            materialColeta.getInsumo().getPreco(),
                            materialColeta.getCori().getCor(),
                            "5",
                            depositoFaccao.getGrupoDep(),
                            LocalDate.now(),
                            materialColeta.getLote(),
                            ofColetor.getColetor() + " - Movto adicional facção coletor",
                            "MN",
                            LocalDateTime.now());
                    movEntrada = new FluentDao().merge(movEntrada);
                    SdReservaMateriais movExtra = new SdReservaMateriais(materialColeta.getNumero(), materialColeta.getInsumo(),
                            materialColeta.getCori(), materialColeta.getSetor().getCodigo(), materialColeta.getDeposito().getCodigo(),
                            materialColeta.getLote(), materialColeta.getFaixa(), materialColeta.getIdPcpftof(), LocalDateTime.now(), materialColeta.getOrdem(), qtdeAdicional, "B",
                            true, false, true, materialColeta.isSubstituto(), materialColeta.getColetor().getCodigo(), "MOV-EXTRA", materialColeta.getDepagrupador());
                    movExtra.setTipo("M");
                    movExtra.setMovimento(movSaida.getOrdem());
                    movExtra.setMovEstorno(movEntrada.getOrdem());
                    new FluentDao().merge(movExtra);
                    
                    if (maiDepositoSaida.getQtde().compareTo(BigDecimal.ZERO) <= 0) {
                        List<MatReserva> reservasMaterial = (List<MatReserva>) new FluentDao().selectFrom(MatReserva.class)
                                .where(it -> it
                                        .equal("id.codigo", materialColeta.getInsumo().getCodigo())
                                        .equal("id.cor", materialColeta.getCori().getCor())
                                        .equal("id.deposito", materialColeta.getDeposito().getCodigo())
                                        .equal("id.lote", materialColeta.getLote())
                                        .greaterThan("qtde", 0)
                                        .lessThanOrEqualTo("qtdeb", 0))
                                .resultList();
                        
                        new FluentDao().deleteAll(reservasMaterial);
                    }
                    
                    SysLogger.addSysDelizLog(
                            "Coleta Material",
                            TipoAcao.MOVIMENTAR,
                            materialColeta.getInsumo().getCodigo(),
                            "Movimentado material: " + materialColeta.getInsumo() + " na cor: " + materialColeta.getCori() + ", qtde: " + qtdeAdicional + " por: " + materialColeta.getColetor()
                                    + " à mais para o depósito da facção da OF: " + materialColeta.getNumero() + " no setor: " + materialColeta.getSetor() + " - Facção: " + ofColetor.getFaccao());
                    
                } else {
                    MessageBox.create(message -> {
                        message.message("Quantidade no depósito " + maiDepositoSaida.getId().getDeposito() + " insuficiente para movimentação.");
                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                        message.showAndWait();
                    });
                }
        } else {
            MessageBox.create(message -> {
                message.message("Depósito da facção " + ofColetor.getFaccao() + "\nnão foi encontrado, solicite o cadastro do depósito para a facção.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
        }
    }
    
    protected void imprimirRomaneio(SdOfsColetor ofColetaSelecionada) throws SQLException, JRException, IOException {
        new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_COLETA_DEPOSITO,
                new ReportUtils.ParameterReport[]{
                        new ReportUtils.ParameterReport("pNumero", ofColetaSelecionada.getNumero()),
                        new ReportUtils.ParameterReport("pSetor", ofColetaSelecionada.getSetor()),
                        new ReportUtils.ParameterReport("pTipo", ofColetaSelecionada.getTipo()),
                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo())
                }).view().print();
    }
    
    protected void apontaFaltaMaterial(VSdMateriaisParaColeta reservaAcum) throws SQLException {
        List<SdReservaMateriais> reservasAcum = (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                .where(it -> it
                        .equal("id.numero", reservaAcum.getNumero())
                        .equal("id.setor", reservaAcum.getSetor().getCodigo())
                        .equal("id.insumo.codigo", reservaAcum.getInsumo().getCodigo())
                        .equal("id.corI.cor", reservaAcum.getCori().getCor())
                        .equal("id.lote", reservaAcum.getLote())
                        .equal("coletor", reservaAcum.getColetor().getCodigo())
                        .equal("coleta", false)
                        .equal("tipo", reservaAcum.getTipo()))
                .resultList();
        for (SdReservaMateriais reserva : reservasAcum) {
            MatMov matMovDaBaixaConsumo = new FluentDao().selectFrom(MatMov.class)
                    .where(it -> it
                            .equal("ordem", reserva.getMovimento()))
                    .singleResult();
            if (matMovDaBaixaConsumo == null) {
                MessageBox.create(message -> {
                    message.message("Não foi possível encontra o movimento da reserva do material.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
                return;
            }
            MatMov matMovDoEstorno = null;
            
            List<SdReservaMateriais> reservas = (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                    .where(it -> it
                            .equal("id.numero", reserva.getId().getNumero())
                            .equal("id.insumo.codigo", reserva.getId().getInsumo().getCodigo())
                            .equal("id.corI.cor", reserva.getId().getCorI().getCor())
                            .equal("id.setor", reserva.getId().getSetor()))
                    .resultList();
            BigDecimal baixadoOf = matMovDaBaixaConsumo.getQtde();
            if (reserva.getQtdeB().compareTo(baixadoOf) < 0) {
                matMovDoEstorno = new MatMov(reserva.getId().getInsumo().getCodigo(),
                        "S",
                        null,
                        reserva.getId().getNumero(),
                        reserva.getQtdeB(),
                        reserva.getId().getInsumo().getPreco(),
                        reserva.getId().getInsumo().getPreco(),
                        reserva.getId().getCorI().getCor(),
                        "99",
                        null,
                        reserva.getId().getDeposito(),
                        null,
                        LocalDate.now(),
                        reserva.getId().getLote(),
                        matMovDaBaixaConsumo.getDescricao(),
                        null,
                        null,
                        "EC",
                        LocalDateTime.now());
                new FluentDao().merge(matMovDoEstorno);
                
                matMovDaBaixaConsumo.setQtde(matMovDaBaixaConsumo.getQtde().subtract(reserva.getQtdeB()));
            } else {
                matMovDaBaixaConsumo.setTpmov("EC");
            }
            
            MatMov matMovEntrada = new MatMov(reserva.getId().getInsumo().getCodigo(),
                    "E",
                    null,
                    reserva.getId().getNumero(),
                    reserva.getQtdeB(),
                    reserva.getId().getInsumo().getPreco(),
                    reserva.getId().getInsumo().getPreco(),
                    reserva.getId().getCorI().getCor(),
                    "99",
                    null,
                    reserva.getId().getDeposito(),
                    null,
                    LocalDate.now(),
                    reserva.getId().getLote(),
                    colaborador.getUsuario() + "-Estorno de consumo",
                    null,
                    null,
                    "EC",
                    LocalDateTime.now());
            
            MatIten matIten = (MatIten) new FluentDao().selectFrom(MatIten.class)
                    .where(it -> it
                            .equal("id.cor", reserva.getId().getCorI().getCor())
                            .equal("id.lote", reserva.getId().getLote())
                            .equal("id.codigo", reserva.getId().getInsumo().getCodigo())
                            .equal("id.deposito.codigo", reserva.getId().getDeposito())
                    ).singleResult();
            if (matIten != null)
                matIten.setQtde(matIten.getQtde().add(reserva.getQtdeB()));
            
            List<PcpFtOf> pcpFtOf = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", reserva.getId().getNumero())
                            .equal("pcpFtOfID.insumo", reserva.getId().getInsumo().getCodigo())
                            .equal("pcpFtOfID.corI", reserva.getId().getCorI().getCor())
                            .equal("pcpFtOfID.setor", reserva.getId().getSetor())
                    ).resultList();
            
            pcpFtOf.forEach(it -> {
                it.setSituacao(reserva.getQtdeB().compareTo(baixadoOf) == 0 ? "A" : "P");
                new FluentDao().merge(it);
            });
            
            if (matIten != null)
                new FluentDao().merge(matIten);
            new FluentDao().persist(matMovEntrada);
            
            new FluentDao().delete(reserva);
            SdMateriaisFaltantes faltaMaterial = new SdMateriaisFaltantes(reserva.getId().getNumero(), reserva.getId().getInsumo(), reserva.getId().getCorI(), reserva.getId().getSetor(), reserva.getId().getFaixa(),
                    reserva.getId().getIdPcpftof(), reserva.getQtdeB(), LocalDateTime.now(), "A", reserva.isSubstituto());
            faltaMaterial.setDhCriacao(LocalDateTime.now());
            new FluentDao().merge(faltaMaterial);
            SysLogger.addSysDelizLog(
                    "Coleta Material",
                    TipoAcao.CADASTRAR,
                    reserva.getId().getInsumo().getCodigo(),
                    "Apontado falta em estoque do material para a OF: " + reserva.getId().getNumero() + " no setor: " + reserva.getId().getSetor() + ", depósito: " + reserva.getId().getDeposito() + ". " +
                            "Qtde: " + reserva.getQtdeB() + " na cor: " + reserva.getId().getCorI().getCor() + " pelo coletor: " + reserva.getColetor());
        }
    }
    
    @Override
    public void closeWindow() {
        Globals.getMainStage().close();
    }
}
