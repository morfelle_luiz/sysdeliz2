package sysdeliz2.controllers.views.pcp.almoxarifado;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOfsColetor;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.models.view.VSdFaccoesOf;
import sysdeliz2.models.view.VSdOfsColetaLiberada;
import sysdeliz2.models.view.VSdOfsParaColeta;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GerenciadorColetasAlmoxarifadoController extends WindowBase {
    
    // <editor-fold defaultstate="collapsed" desc="Lists">
    protected final ListProperty<VSdOfsParaColeta> ofsParaColeta = new SimpleListProperty<>();
    protected final ListProperty<VSdOfsColetaLiberada> ofsColetaLiberadas = new SimpleListProperty<>();
    protected final ListProperty<SdColaborador> coletores = new SimpleListProperty<>();
    protected final ListProperty<SdOfsColetor> ofsProgramadas = new SimpleListProperty<>();
    // </editor-fold>
    
    public GerenciadorColetasAlmoxarifadoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getOfsColetaLiberadas(String numero, String setor, String pais) {
        ofsColetaLiberadas.set(FXCollections.observableList((List<VSdOfsColetaLiberada>) new FluentDao().selectFrom(VSdOfsColetaLiberada.class)
                .where(it -> it
                        .lessThan("perccoleta", 1, TipoExpressao.AND, b -> numero.length() == 0 && setor.length() == 0)
                        .equal("deposito.pais", pais, TipoExpressao.AND, b -> !pais.equals("A"))
                        .isIn("numero", numero.split(","), TipoExpressao.AND, b -> numero.length() > 0)
                        .isIn("setor.codigo", setor.split(","), TipoExpressao.AND, b -> setor.length() > 0))
                .resultList()));
    }
    
    protected void getOfsParaColeta(String setor, Object[] deposito, String pais) {
        String setorColeta = setor == null ? "" : setor;
        String paisColeta = pais == null ? "A" : pais;
        Object[] depositoColeta = deposito == null ? new Object[]{} : deposito;
        JPAUtils.clearEntitys(ofsParaColeta);
        ofsParaColeta.set(FXCollections.observableList((List<VSdOfsParaColeta>) new FluentDao().selectFrom(VSdOfsParaColeta.class)
                .where(it -> it
                        .equal("pais", paisColeta, TipoExpressao.AND, b -> !paisColeta.equals("A"))
                        .equal("setor", setorColeta, TipoExpressao.AND, b -> setorColeta.length() > 0)
                        .isIn("deposito", depositoColeta, TipoExpressao.AND, b -> depositoColeta.length > 0))
                .orderBy(Arrays.asList(new Ordenacao[]{new Ordenacao("numero"), new Ordenacao("setor"), new Ordenacao("deposito")}))
                .resultList()));
    }
    
    protected void getColetores(Object[] deposito, String pais) {
        Object[] depositoColetor = deposito == null ? new Object[]{} : deposito;
        String paisColetor = pais == null ? "A" : pais;
        JPAUtils.clearEntitys(coletores);
        coletores.set(FXCollections.observableList((List<SdColaborador>) new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("codigoFuncao", 41)
                        .equal("ativo", true)
                        .equal("deposito.pais", paisColetor, TipoExpressao.AND, b -> !paisColetor.equals("A"))
                        .isIn("deposito.codigo", depositoColetor, TipoExpressao.AND, b -> depositoColetor.length > 0))
                .resultList()));
    }
    
    protected ObservableList<SdOfsColetor> getOfsColetor(Integer coletor) {
        ObservableList<SdOfsColetor> ofsColetor = FXCollections.observableList((List<SdOfsColetor>) new FluentDao().selectFrom(SdOfsColetor.class)
                .where(it -> it
                        .equal("coletor", coletor)
                        .isIn("status", new String[]{"A", "P", "E"}))
                .orderBy("ordem", OrderType.ASC)
                .resultList());
        ofsColetor.stream().forEach(ofColeta -> {
            ofColeta.setOrdem(ofsColetor.indexOf(ofColeta));
        });
        new FluentDao().mergeAll(ofsColetor);
        return ofsColetor;
    }
    
    protected VSdFaccoesOf getFaccaoOfSetor(String numero, String setor) {
        return new FluentDao().selectFrom(VSdFaccoesOf.class)
                .where(it -> it
                        .equal("numero", numero)
                        .equal("setor", setor))
                .singleResult();
    }
    
    protected Object[] depositosColeta(String deposito) {
        return (Object[]) new FluentDao().selectFrom(Deposito.class)
                .where(it -> it
                        .equal("grupoDep", deposito))
                .resultList().stream().map(it -> ((Deposito) it).getCodigo()).distinct().toArray();
    }
    
    protected List<SdReservaMateriais> getReservasColeta(SdOfsColetor ofColeta) {
        Object[] depositos = depositosColeta(ofColeta.getDeposito().getCodigo());
        return (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                .where(it -> it
                        .equal("id.numero", ofColeta.getNumero())
                        .equal("id.setor", ofColeta.getSetor())
                        .equal("baixado", true)
                        .equal("coleta", false)
                        .equal("coletor", 0)
                        .equal("tipo", ofColeta.getTipo())
                        .and(in -> in
                                .isIn("depagrupador", depositos, TipoExpressao.AND, true)
                                .equal("id.deposito", "0000", TipoExpressao.OR, true)
                        )
                ).resultList();
    }
    
    protected List<SdReservaMateriais> getReservasColetor(SdOfsColetor ofColeta) {
        Object[] depositos = depositosColeta(ofColeta.getDeposito().getCodigo());
        return (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                .where(it -> it
                        .equal("id.numero", ofColeta.getNumero())
                        .equal("id.setor", ofColeta.getSetor())
                        .equal("baixado", true)
                        .equal("coleta", false)
                        .equal("coletor", ofColeta.getColetor().getCodigo())
                        .equal("tipo", ofColeta.getTipo())
                        .and(in -> in
                                .isIn("depagrupador", depositos, TipoExpressao.AND, true)
                                .equal("id.deposito", "0000", TipoExpressao.OR, true)
                        )
                ).resultList();
    }
    
    protected void addOfColetor(SdOfsColetor ofColeta, List<SdReservaMateriais> reservas) throws SQLException {
        for (SdReservaMateriais reserva : reservas) {
            reserva.setColetor(ofColeta.getColetor().getCodigo());
            reserva.setDepagrupador(ofColeta.getDeposito().getCodigo());
            new FluentDao().merge(reserva);
        }
        ofColeta = new FluentDao().persist(ofColeta);
        SysLogger.addSysDelizLog("Gerenciar Coletas Almoxarifado", TipoAcao.CADASTRAR, String.valueOf(ofColeta.getColetor().getCodigo()), "Adicionando a OF " + ofColeta.getNumero() + " para coleta.");
    }
    
    protected void addOfColetaRequisicao(SdOfsColetor ofColeta, List<SdReservaMateriais> reservasColeta) throws SQLException {
        for (SdReservaMateriais reserva : reservasColeta) {
            reserva.setColetor(ofColeta.getColetor().getCodigo());
            reserva.setDepagrupador(ofColeta.getDeposito().getCodigo());
            reserva.setColeta(true);
            new FluentDao().merge(reserva);
        }
        ofColeta.setDtcoleta(LocalDateTime.now());
        ofColeta.setStatus("C");
        ofColeta = new FluentDao().persist(ofColeta);
        SysLogger.addSysDelizLog("Gerenciar Coletas Almoxarifado", TipoAcao.CADASTRAR, String.valueOf(ofColeta.getNumero()), "OF no setor: " + ofColeta.getSetor() + " depósito: " + ofColeta.getDeposito() + " programada para coleta com romaneio.");
    }
    
    protected void deleteOfColetor(SdOfsColetor ofColeta) {
        List<SdReservaMateriais> reservasColeta = getReservasColetor(ofColeta);
        for (SdReservaMateriais reserva : reservasColeta) {
            reserva.setColetor(0);
            reserva.setDepagrupador(reserva.getId().getDeposito());
            new FluentDao().merge(reserva);
        }
        //new FluentDao().delete(ofColeta);
        new FluentDao().runNativeQueryUpdate(String.format("delete from sd_ofs_coletor_001 where programacao = %s", ofColeta.getProgramacao()));
        SysLogger.addSysDelizLog("Gerenciar Coletas Almoxarifado", TipoAcao.CADASTRAR, String.valueOf(ofColeta.getColetor().getCodigo()), "Removido a OF " + ofColeta.getNumero() + " de coleta.");
    }
    
    protected void agruparOfs(ObservableList<VSdOfsParaColeta> ofsParaAgrupar, Deposito depositoAgrupador) {
        ofsParaAgrupar.forEach(it -> {
            new FluentDao().runNativeQueryUpdate(String.format(
                    "update sd_reserva_materiais_001 set dep_agrupador = '%s' where numero = '%s' and setor = '%s' and deposito in (select codigo from deposito_001 where grupo_dep = '%s') and coleta = 'N' and baixado = 'S' and coletor = 0",
                    depositoAgrupador.getCodigo(),
                    it.getNumero(),
                    it.getSetor(),
                    it.getDeposito()
            ));
        });
        VSdOfsParaColeta coletaAgrupada = ofsParaAgrupar.stream().filter(it -> it.getDeposito().equals(depositoAgrupador.getCodigo())).findFirst().get();
        ObservableList<VSdOfsParaColeta> coletasParaExcluir = FXCollections.observableArrayList();
        coletasParaExcluir.addAll(ofsParaAgrupar.stream().filter(it -> !it.getDeposito().equals(depositoAgrupador.getCodigo())).collect(Collectors.toList()));
        coletaAgrupada.setItens(coletaAgrupada.getItens() + coletasParaExcluir.stream().mapToInt(it -> it.getItens()).sum());
        coletasParaExcluir.forEach(it -> {
            ofsParaColeta.remove(it);
        });
        SysLogger.addSysDelizLog("Gerenciar Coletas Almoxarifado", TipoAcao.CADASTRAR, String.valueOf(coletaAgrupada.getNumero()),
                "Agrupado os depósitos " + coletaAgrupada.getDeposito() + "," + coletasParaExcluir.stream().map(it -> it.getDeposito()).distinct().collect(Collectors.joining(",")) +
                        " para coleta juntos no setor " + coletaAgrupada.getSetor() + ".");
    }
    
    protected void getOfsProgramadas(String numero, String setor, String deposito, Object[] faccao, Object[] coletor, String status, String impresso, String pais) {
        ofsProgramadas.set(FXCollections.observableList((List<SdOfsColetor>) new FluentDao().selectFrom(SdOfsColetor.class)
                .where(it -> it
                        .isIn("numero", numero.split(","), TipoExpressao.AND, b -> numero.length() > 0)
                        .isIn("setor", setor.split(","), TipoExpressao.AND, b -> setor.length() > 0)
                        .isIn("deposito.codigo", deposito.split(","), TipoExpressao.AND, b -> deposito.length() > 0)
                        .isIn("faccao.codcli", faccao, TipoExpressao.AND, b -> faccao.length > 0)
                        .isIn("coletor.codigo", coletor, TipoExpressao.AND, b -> coletor.length > 0)
                        .equal("status", status, TipoExpressao.AND, b -> !status.equals("T") && status.length() > 0)
                        .equal("deposito.pais", pais, TipoExpressao.AND, b -> !pais.equals("A"))
                        .equal("impresso", new Boolean((impresso.equals("T") || impresso.length() == 0) ? "false" : impresso), TipoExpressao.AND, b -> !impresso.equals("T") && impresso.length() > 0))
                .resultList()));
    }
}
