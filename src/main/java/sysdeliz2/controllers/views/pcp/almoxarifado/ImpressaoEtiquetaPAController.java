package sysdeliz2.controllers.views.pcp.almoxarifado;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.glassfish.jersey.client.ClientProperties;
import sysdeliz2.controllers.fxml.pcp.ScenePcpImpressaoEtiquetaComposicaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.pcp.almoxarifado.SdProdutoPASequencia;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.OfIten;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdComposicaoCorOf;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.gui.dialog.FileChoice;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.StringUtils;

import javax.print.*;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


public class ImpressaoEtiquetaPAController extends WindowBase {

    private int paginadorTags = 1;
    private String zplString = "";
    private int quantidadeEtiquetas = 0;
    private int pos1 = 160;
    private int pos2 = 140;
    private AtomicReference<String> zplPreviewString = new AtomicReference<>("");

    @Override
    public void closeWindow() {

    }

    public ImpressaoEtiquetaPAController(String title, Image icone) {
        super(title, icone);
    }

    private Image getPreviewEtiqueta(String zpl, Double tamanho, Integer rotation) {
        Client client = ClientBuilder.newBuilder().register(ScenePcpImpressaoEtiquetaComposicaoController.class).build();
        client.property(ClientProperties.CONNECT_TIMEOUT, 5000);
        client.property(ClientProperties.READ_TIMEOUT, 60000);
        // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
        WebTarget target = client.target("http://api.labelary.com/v1/printers/8dpmm/labels/4x" + tamanho.toString() + "/0/");
        Invocation.Builder request = target.request();
        request.accept("image/png"); // omit this line to get PNG images back
        request.header("X-Rotation", rotation.toString());
        request.header("Content-Type", "application/x-www-form-urlencoded");

        try {
            Response response = response = request.post(Entity.entity(zpl, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus() == 200) {
                byte[] body = response.readEntity(byte[].class
                );
                File file = new File("C:\\SysDelizLocal\\local_files\\labelComposicao.png"); // change file name for PNG images
                Files.write(file.toPath(), body);
                String imaPath = file.toURI().toString();
                return new Image(imaPath);
            } else {
                String body = response.readEntity(String.class);
                GUIUtils.showMessage("Não foi possível gerar a imagem da etiqueta. Erro: " + body);
                return new Image(this.getClass().getResource("/images/erroGeracaoImagemTag.png").toExternalForm());
                //throw new SocketTimeoutException(body);
            }
        } catch (Exception ex) {
            return new Image(this.getClass().getResource("/images/erroGeracaoImagemTag.png").toExternalForm());
        }
    }

    protected Image criarPreviewComposicao(List<OfIten> of1, ObservableList<Cor> cores, Produto produto) {
        paginadorTags = 1;
        zplString = "";
        quantidadeEtiquetas = 0;
        zplPreviewString.set("");

        of1.stream().filter(ofIten -> cores.stream()
                        .anyMatch(cor -> ofIten
                                .getId().getCor().getCor()
                                .equals(cor.getCor()))).sorted(Comparator.comparing(ofIten -> ((OfIten) ofIten)
                                .getId().getCor().getCor())
                        .thenComparing((o1, o2) -> SortUtils.sortTamanhos(((OfIten) o1)
                                .getId().getTamanho(), ((OfIten) o2)
                                .getId().getTamanho())))
                .forEach(ofItem -> {
                    if (ofItem.getQtde().intValue() > 0) {


                        List<VSdComposicaoCorOf> composicaoCorOfList = (List<VSdComposicaoCorOf>) new FluentDao().selectFrom(VSdComposicaoCorOf.class).where(it -> it
                                .equal("cor", ofItem.getId().getCor().getCor()).and(eb -> eb
                                        .equal("produto", ofItem.getId().getNumero(), TipoExpressao.AND, true)
                                        .equal("numero", ofItem.getId().getNumero(), TipoExpressao.OR, true))).orderBy(Arrays.asList(new Ordenacao("desc", "codigocomposicao"), new Ordenacao("desc", "numero"))).resultList();


                        if (composicaoCorOfList != null) {
                            List<VSdComposicaoCorOf> finalComposicaoCorOfList = composicaoCorOfList;
                            composicaoCorOfList = composicaoCorOfList.stream().filter(comp -> comp.getNumero().equals(finalComposicaoCorOfList.get(0).getNumero())).collect(Collectors.toList());
                            for (int i = 0; i < ofItem.getQtde().intValue(); i++) {
                                if (paginadorTags == 1) {
                                    zplString += ""
                                            + "^XA\n"
                                            + "^MMT\n"
                                            + "^PW791\n"
                                            + "^LL0679\n"
                                            + "^LS0\n";
                                    zplString += "" +
                                            "^FO709,96^GFA,\n" +
                                            (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                                            "^FO544,544^GFA,02560,02560,00020,:Z64:\n" +
                                            "eJzt1MFu00AQBuDZLvL2kMYcg3DrPgKoCFxhxa9SngBLvQQ1ahb10BuXXpF4CF5goxx67JUDUhdxyI0acSBIls0/s3GaVBUPgLpRrOTzer27M7NED22jKcvXwYZpsWSzn+Nrjm+xEOA+Ff/gv0fbq7FGtLyxNLd8TPmVaba0RXPcIZjvzPIEgpVrFlGPbUvMTXwK69Ejtihjm7ZVYfkNER0R9Xn6uzdte2JpfxAs4RGSiU2/Lw3vHPTZTsmsWWbYxqRhz79QNLi0VML8st+rBUX5zFOpsarkLcVLO50uyCsyLpnbvWNLw0Mbzd2YnLKp3Z25/MBSfmjNzKUzp6YN7U796LWl7ImNlNW/LM091lZ62ECMWqIJ1mbfBbt4idgURKdshcN6k1QsdmrBxh9KYhiRqXQwfKmnxPp1/PGuJcOiJ+Pxs3118QJLzeK6z3bii2AR0TOzMLz3nucSBSuLU4ywO3VlsBxP+SZVsJnLDlY2q8wUE7u0OfbFqIv9HqlLp/7AbmhP7PMR7BMRkiKZyJ5q+u16pBvknO9ipG2CsJsFL49N+iEVtinm7Ku7+CqxIadHgXdUzXFnNRvHo60xZ87PgW6l+dRJDin8XphrpsbzvsCIbasQO+pycr3dZ5Nljq+3qquFtea7G7ct1Fu5YaEus3tss36VXB/TfU1W8C/bokmlVoadwbwMtTVelJGxZmVnbWvYYtweiDlz5dIrnuEejAswU05fSwQz/Ddw/NJOIQZiI1iMlQ6QeQjpmO2nM6QrKUqOVc2jfINxgvdqUg3x45lm4yd2KlJ4xw+uWzEEHWPqK2t4LvEbNiRHjJmfu/gctieGIwelpM9cygmfdIZ6M8oPeb0jMWpUy3tQjdmqYKk54zNpVMEOfTBzzRVGuUfwXrtgulVsKVthg6kamdjnmoXRsl8wY2HDYFoSSvrh3AhmJKF4PImHWCpmaFzdWl6rhYeV2a2deBqXSBif83qDHWMPYO/d8Nbm2CsYTqn+yhqpN5Rs/KEzjgLHBPv8NWRliBFHrMV6OjOc2BOLM4vvBpNaRczHa5Zy9iFw68+K7Th149Kms6dSg07P2jbt8lQKuMHJ29bxHYtt4Y3dTHjZ8DtFsHVvuTy0/6v9Be9JwdU=:238A\n" +
                                            "^FT738,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                                            "^FT722,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                                            "^FT728,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                                            "^FT713,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + ofItem.getId().getTamanho() + "^FS\n" +
                                            "^FT737,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                                            "^FT737,493^A0R,16,16^FH\\^FD" + ofItem.getId().getNumero() + "^FS\n" +
                                            "^FT721,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                                            "^FT721,504^A0R,16,16^FH\\^FD" + ofItem.getId().getCor().getCor() + "^FS\n" +
                                            "^FT754,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                                            "^FT754,505^A0R,16,16^FH\\^FD" + produto.getCodigo() + "^FS\n";
                                    pos1 = 702;
                                    pos2 = 682;
                                    for (VSdComposicaoCorOf composicaoCorOf : composicaoCorOfList) {

                                        zplString += adicionaComposicao(composicaoCorOf, composicaoCorOfList.size());
                                        pos1 -= 43;
                                        pos2 -= 43;
                                    }

                                } else if (paginadorTags == 2) {
                                    zplString += "" +
                                            "^FO445,96^GFA,\n" +
                                            (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                                            "^FO256,544^GFA,03072,03072,00024,:Z64:\n" +
                                            "eJzt1T9v00AUAPB3PZTrkMaMQThxPwKoCFLVir9K+ARY6hLUKDnUoRtLVyQ+BF/gogwduzIg9RBDNmrEgJEsm/fuj2MnrRhg7KuUOL9c7+x3714AHuJ/BJP02t9xbjzcHa/oNXafkty80diMLnLnk/167in4Lzeu3BRMt5yTRxWG8otZ196lvznr6ZZ3oEtve8bVQkfOu/DIfD0iX1ZZIv2qHZgA9OgxB7dVdWb9sG89pNnChYy+NRzvpd8jn4PY8pEgnwF3/vwzdPpXElJ03Rj/KodOvNKQcsxA+AaChs+XOWgGQoVrOTy1Pj6WnbWagWIykoOVio+sx8dSrFS0UmxZwmCppyfWR09kh0n+U8JaYx5S7bxvHCqABeZBvt345Uvc7wRgTp4ol58wMh4olpPTn/UAHUBk3Do47zLjvSL4cJeH46Rr5vfz9NjlC0zNKCh65Gc62XgH4JnIBe2j9vfZsZ4mc5xtsFTpxmOcQZcRQ1+p0VHLV5lYSgivZOzyKdjlYRfYlWK/0W9hWPunCfpHACzAcFHvC4dfqgu8xHrXzX3nMsTSEjmlgrwej2/7ENCHolk/zPiYLhNcNytPm17QJe1vVbjnonPS55UJHam6bhl+zsUNcakpn86BfC8xPmmei2bc54vGuWsGHd36nDZC+y/bYftDuuO2n4zu8d3+w8zr4911G2Ge9G++B4uMtRwzivctoCrs4nhXQoraz6tKeA9wWN+4EtcqugbnQ3RqGiOm+I2pDOsh+pCuuGK4n7VP0QOFz4mVj+Uy8/5DCeCZaSa0/4Vz9hWdDl23AFYC887J6b8PMmC47nfnwjgWFq7Br6Xw9xm8JsdCDPDpLlRw4XxoHNsqHnl+riLhPPS+oMOgxz4/U+NQsoryls28Z9YjcU69d5o5P9bWxQ11A4i1LQo4UdZ5xcgj74m0zgo8CT3qN87BjbcupPOxdW4K2Yy368bWhSlkmr/eX+ORcQGzrO1xwXKNno7afqZhlmKB6tjnx/op5g39nRq3fY15RseO3Gt5afoDtpvgfdNpV2mPcb++eKf0klElVPjcTRd02BYSezTLm276DNbVbMsjusSC2J7H+IFityoqm/7U9A3FV1UVWbfnxTSgEn+RqiK4wwOZaHdeWmE2bpfxPD7EQ/xz/AFsKcHV:E6E3\n" +
                                            "^FT474,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                                            "^FT458,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                                            "^FT463,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                                            "^FT448,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + ofItem.getId().getTamanho() + "^FS\n" +
                                            "^FT473,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                                            "^FT473,493^A0R,16,16^FH\\^FD" + ofItem.getId().getNumero() + "^FS\n" +
                                            "^FT457,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                                            "^FT457,504^A0R,16,16^FH\\^FD" + ofItem.getId().getCor().getCor() + "^FS\n" +
                                            "^FT490,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                                            "^FT490,505^A0R,16,16^FH\\^FD" + produto.getCodigo() + "^FS\n";
                                    pos1 = 435;
                                    pos2 = 415;
                                    for (VSdComposicaoCorOf composicaoCorOf : composicaoCorOfList) {
                                        zplString += adicionaComposicao(composicaoCorOf, composicaoCorOfList.size());
                                        pos1 -= 43;
                                        pos2 -= 43;
                                    }
                                } else {
                                    zplString += "" +
                                            "^FO181,96^GFA,\n" +
                                            (Globals.getEmpresaLogada().getCodigo().equals("1000") ? StaticsLabel.imgCompressedDeliz : StaticsLabel.imgCompressedUpWave) + "\n" +
                                            "^FO0,544^GFA,03072,03072,00024,:Z64:\n" +
                                            "eJzt1D9vGzcUAPDHvEDsoBwzNohy9xW89QwoUj+KgwxdD/AQAS10LAykYzu3QD9DxixFaGQovDRzNgodvAQ1O1kFBF3feyQVykoQBMhoGj6dfnd6/PPIB3DbvkRzfBkfsBKvDt/3fKnTl2YjH9/S/4Jvfkh+dHcXewr54Xv3OUTYc8VuBmo+dxY9ZHd5cNFf3nCEkXjL7ufBJB/BHXk8ER/Wjcu9IhwBaJ5mdT0Mf0S/P45ecbRq7sy/hXe0Wloc8IZP0JLPQCX/+h3g+KmDlrwr3n+4AayfBGiVhVB9Y3Xh85MNhbLoq1NnjqM3E4enfgYn4IyrVr5+EL0mX3mz8uAHqHyYPoo+GTsEp/5zcBpovm2XfCwOvYW5BeOmYecXv1G+G3JaH9f4tD6VEdceNuz8F12TW8C1im6Tj0BcL/VfH/KqaZ5L/BxHw8WvtDQTvdXsj0NTOC0fbuip8V0eP0Zvm1cczfv2vf9OEbreWPKVnzzY8ydr9A6qp65O64n24hdK7d80L/Jn1uz87QvyNxZ6T/nd5UXZ7/0I1ECZCGXelatobkiLgGv23fv0cRc0f1mW+wfEG0mprZ6t++PSl3zH+R22aV58TsZqkBaM7/O+Bfq+wSvxjtcze0+uGvGj8lyU7WM+L89d0eTohkPvdg/3m4RoDzjWk8lH/LD+gOXLV4deNJmp/YQrmK9jsOy0omtOdr+NndOo0OHOz4fhLLu2aMfiJ3jpzaVNbujnldx5dWV5Z0SvyA3fUSnbUj6yT8m1p3nSzqcaOcu+8GjVWooJ53+ZPZDzoRuRUPBtciVOvx7Rb6jf6+QoThuL+lCXDvM49Qt22oiaZvfa69fJzRE7lVU68urcm7PkVXaqD2hDk9dnKk7TGdgXs+yL6ObsnBymi+whOl79SL1D3cVNAY98dDVYdhOSNy46zZOGypNIbtP70dFlhzhO2cjyfuy3jo6ykTn+Lr/iRhztbLHv9RI2HXk72ffHAWYtbdCuRlv6Ma3bS3Lf7PspDYacKvJPUHov9YHKjf5zzzmrlOPvHP6TnbclG++E3qpt6RwD5o5r9KZ0qTO0r2Y33PDgaG1uxhEfebj2Zij9ntSNE7Uahp/jvOJ5iQUI0Pdb/QHXrgkY122vSeLsoatDum237bPb/8Z052s=:1FA9\n" +
                                            "^FT211,213^A0R,16,16^FB183,1,0,C^FH\\^FDCNPJ " + StringUtils.formatCnpj(Globals.getEmpresaLogada().getCnpj()) + "^FS\n" +
                                            "^FT195,212^A0R,16,16^FB163,1,0,C^FH\\^FDIND\\E9STRIA BRASILEIRA^FS\n" +
                                            "^FT200,591^A0N,20,19^FB37,1,0,C^FH\\^FDTAM^FS\n" +
                                            "^FT184,635^A0N,39,38^FB80,1,0,C^FH\\^FD" + ofItem.getId().getTamanho() + "^FS\n" +
                                            "^FT210,470^A0R,16,16^FH\\^FDOF:^FS\n" +
                                            "^FT210,493^A0R,16,16^FH\\^FD" + ofItem.getId().getNumero() + "^FS\n" +
                                            "^FT194,470^A0R,16,16^FH\\^FDCOR:^FS\n" +
                                            "^FT194,504^A0R,16,16^FH\\^FD" + ofItem.getId().getCor().getCor() + "^FS\n" +
                                            "^FT227,470^A0R,16,16^FH\\^FDREF.:^FS\n" +
                                            "^FT227,505^A0R,16,16^FH\\^FD" + produto.getCodigo() + "^FS\n";
                                    pos1 = 175;
                                    pos2 = 155;
                                    for (VSdComposicaoCorOf composicaoCorOf : composicaoCorOfList) {
                                        zplString += adicionaComposicao(composicaoCorOf, composicaoCorOfList.size());
                                        pos1 -= 43;
                                        pos2 -= 43;
                                    }
                                    zplString += ""
                                            + "^PQ1,0,1,Y\n"
                                            + "^XZ\n";
                                    paginadorTags = 0;
                                }
                                paginadorTags++;
                                quantidadeEtiquetas++;
                                if (quantidadeEtiquetas <= 3) {
                                    zplPreviewString.set(zplString);
                                }
                            }
                        }
                    }
                });

        if (quantidadeEtiquetas <= 2 && quantidadeEtiquetas > 0) {
            zplPreviewString.set(zplString + finalTagProdutoAcabado());
        }
        if (quantidadeEtiquetas % 3 != 0) {
            zplString += finalTagProdutoAcabado();
        }
        if (!zplPreviewString.get().equals("")) {
            return getPreviewEtiqueta(zplPreviewString.get(), 3.5, 270);
        }
        return null;
    }

    private String adicionaComposicao(VSdComposicaoCorOf composicaoCorOf, int quantidade) {

        String composicaoPrimeiraLinha;
        String composicaoSegundaLinha;
        String composicao = composicaoCorOf.getComposicao();
        int porcentagem;

        if (quantidade == 1) {
            pos1 -= 40;
            pos2 -= 40;
        } else if (quantidade == 2) {
            pos1 -= 20;
            pos2 -= 20;
        }

        String zpl = "^FT" + pos1 + ",95^A0R,20,19^FH\\^FD" + composicaoCorOf.getDescricao() + "^FS\n";
        if (composicao.length() > 40) {
            porcentagem = composicao.substring(30).indexOf("%") + 30;

            for (int i = porcentagem; "0123456789.,".contains(String.valueOf(composicao.charAt(porcentagem - 2))); i--) {
                porcentagem = i;
            }
            composicaoPrimeiraLinha = composicao.substring(0, porcentagem - 1);
            composicaoSegundaLinha = composicao.substring(porcentagem - 1);

            zpl += "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicaoPrimeiraLinha + "^FS\n";
            pos1 -= 18;
            pos2 -= 18;
            zpl += "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicaoSegundaLinha + "^FS\n";
            return zpl;
        }

        zpl += "^FT" + pos2 + ",95^A0R,20,19^FH\\^FD" + composicao + "^FS\n";
        return zpl;

    }

    protected Image criarPreviewProduto(List<OfIten> of1, ObservableList<Cor> cores, Produto produto) {

        paginadorTags = 1;
        zplString = "";
        quantidadeEtiquetas = 0;
        zplPreviewString.set("");

        of1.stream().filter(ofIten -> cores.stream().anyMatch(cor -> ofIten.getId().getCor().getCor().equals(cor.getCor()))).sorted(Comparator.comparing(ofIten -> ((OfIten) ofIten).getId().getCor().getCor()).thenComparing((o1, o2) -> SortUtils.sortTamanhos(((OfIten) o1).getId().getTamanho(), ((OfIten) o2).getId().getTamanho()))).forEach(etiqueta -> {

            String tamanho = etiqueta.getId().getTamanho();
            String cor = etiqueta.getId().getCor().getCor();
            String numeroOF = etiqueta.getId().getNumero();
            String descricaoCor = etiqueta.getId().getCor().getDescricao();

            etiqueta.getId().getNumero();
            String codigoProduto = produto.getCodigo();
            String nomeProduto = produto.getDescricao();

            VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it
                    .equal("cor", cor)
                    .equal("tam", tamanho)
                    .equal("codigo", codigoProduto)
            ).singleResult();

            if (produtoBarra != null) {
                String codigoDeBarras = produtoBarra.getBarra();
                String barra28 = produtoBarra.getBarra28();

                for (int i = 0; i < etiqueta.getQtde().intValue(); i++) {
                    String sequencia = gerarSequencia(codigoProduto, cor, tamanho);
                    String codigoQRCode = barra28 + sequencia + numeroOF;

                    zplString += getZplStringProdutoAcabado(tamanho, cor, numeroOF, descricaoCor, codigoProduto, nomeProduto, codigoDeBarras, codigoQRCode);
                    if (quantidadeEtiquetas <= 3) {
                        zplPreviewString.set(zplString);
                    }
                    paginadorTags++;
                }
            }
        });
        if (quantidadeEtiquetas <= 2) {
            zplPreviewString.set(zplString + finalTagProdutoAcabado());

        }
        if (quantidadeEtiquetas % 3 != 0) {
            zplString += finalTagProdutoAcabado();
        }
        if (!zplPreviewString.get().equals("")) {
            return getPreviewEtiqueta(zplPreviewString.get(), 2.5, 90);
        }
        return null;
    }

    private String gerarSequencia(String codigoProduto, String cor, String tamanho) {

        String sequencia = null;
        SdProdutoPASequencia produtoSequencia = new FluentDao().selectFrom(SdProdutoPASequencia.class)
                .where(eb -> eb
                        .equal("cor", cor)
                        .equal("tamanho", tamanho)
                        .equal("produto", codigoProduto))
                .singleResult();
        if (produtoSequencia != null) {
            JPAUtils.getEntityManager().refresh(produtoSequencia);
            String sequence = String.valueOf(new BigDecimal((Integer.parseInt(produtoSequencia.getSequence()) < 9999 ? produtoSequencia.getSequence() : "0")).add(BigDecimal.ONE));
            sequencia = StringUtils.lpad(sequence, 4, "0");
            produtoSequencia.setSequence(sequencia);
        } else {
            produtoSequencia = new SdProdutoPASequencia(cor, tamanho, codigoProduto);
            sequencia = produtoSequencia.getSequence();
        }
        new FluentDao().merge(produtoSequencia);
        return sequencia;
    }

    private String getZplStringProdutoAcabado(String tamanho, String cor, String numeroOF, String descricaoCor, String codigoProduto, String nomeProduto, String codigoDeBarras, String codigoQRCode) {
        String zplString = "";

        quantidadeEtiquetas++;

        if (paginadorTags == 1) {
            zplString = inicioTagProdutoAcabado();
            zplString += ""
                    + "^BY3,2,86^FT686,43^BER,,Y,N\n"
                    + "^FD" + codigoDeBarras + "^FS\n"
                    + "^FT586,109^A0B,39,38^FH^FD" + tamanho + "^FS\n"
                    + "^FT616,448^A0B,28,28^FH^FD" + nomeProduto + "^FS\n"
                    + "^FT586,252^A0B,39,38^FH^FD" + cor + "^FS\n"
                    + "^FT586,448^A0B,39,38^FH^FD" + codigoProduto + "^FS\n"
                    + "^FT639,448^A0B,23,24^FH^FD" + descricaoCor + "^FS\n"
                    + "^FT667,458^BQN,2,5\n"
                    + "^FH^FDLA," + codigoQRCode + "^FS\n"
                    + "^FT659,447^A0B,23,24^FH^FDOF:^FS\n"
                    + "^FT659,411^A0B,23,24^FH^FD" + numeroOF + "^FS"
                    + "^LRY^FO551,38^GB0,410,41^FS^LRN";
        } else if (paginadorTags == 2) {
            zplString = "^BY3,2,86^FT420,43^BER,,Y,N\n"
                    + "^FD" + codigoDeBarras + "^FS\n"
                    + "^FT320,109^A0B,39,38^FH^FD" + tamanho + "^FS\n"
                    + "^FT350,448^A0B,28,28^FH^FD" + nomeProduto + "^FS\n"
                    + "^FT320,252^A0B,39,38^FH^FD" + cor + "^FS\n"
                    + "^FT320,448^A0B,39,38^FH^FD" + codigoProduto + "^FS\n"
                    + "^FT373,448^A0B,23,24^FH^FD" + descricaoCor + "^FS\n"
                    + "^FT401,458^BQN,2,5\n"
                    + "^FH^FDLA," + codigoQRCode + "^FS\n"
                    + "^FT393,447^A0B,23,24^FH^FDOF:^FS\n"
                    + "^FT393,411^A0B,23,24^FH^FD" + numeroOF + "^FS"
                    + "^LRY^FO285,38^GB0,410,41^FS^LRN";
        } else {
            zplString = "^BY3,2,86^FT154,43^BER,,Y,N\n"
                    + "^FD" + codigoDeBarras + "^FS\n"
                    + "^FT54,109^A0B,39,38^FH^FD" + tamanho + "^FS\n"
                    + "^FT84,448^A0B,28,28^FH^FD" + nomeProduto + "^FS\n"
                    + "^FT54,252^A0B,39,38^FH^FD" + cor + "^FS\n"
                    + "^FT54,448^A0B,39,38^FH^FD" + codigoProduto + "^FS\n"
                    + "^FT107,448^A0B,23,24^FH^FD" + descricaoCor + "^FS\n"
                    + "^FT135,458^BQN,2,5\n"
                    + "^FH^FDLA," + codigoQRCode + "^FS\n"
                    + "^FT127,447^A0B,23,24^FH^FDOF:^FS\n"
                    + "^FT127,411^A0B,23,24^FH^FD" + numeroOF + "^FS"
                    + "^LRY^FO19,38^GB0,410,41^FS^LRN";
            zplString += finalTagProdutoAcabado();
            paginadorTags = 0;
        }
        return zplString;
    }

    private String finalTagProdutoAcabado() {
        return "^PQ1,0,1,Y\n"
                + "^XZ\n";
    }

    private String inicioTagProdutoAcabado() {
        return "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5^JUS^LRN^CI0^XZ"
                + "^XA\n"
                + "^MMT\n"
                + "^PW799\n"
                + "^LL0448\n"
                + "^LS0\n";
    }

    protected boolean imprimirAction(boolean checkbox, Window window) {

        if (checkbox) {

            String pathFile = FileChoice.show(box -> {
                box.addExtension(new FileChooser.ExtensionFilter("Arquivo de Texto", "*.txt"));
            }).stringSavePath();

            if (pathFile != null) {
                FileWriter myWriter = null;
                try {
                    myWriter = new FileWriter(pathFile);
                    myWriter.write(zplString);
                    myWriter.close();
                    MessageBox.create(message -> {
                        message.message("Arquivo TXT de etiqueta criado com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
                return true;
            }
            return false;
        }

        PrinterJob jobPrinterSelect = PrinterJob.createPrinterJob();
        if (jobPrinterSelect == null) {
            MessageBox.create(message -> {
                message.message("Impossível criar o job de impressão com as impressoras instaladas neste usuário, tente reiniciar o computador para atualização da lista de impressoras.");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
            return false;
        }


        if (!jobPrinterSelect.showPrintDialog(window)) {
            return false;
        }

        String printerName = jobPrinterSelect.getPrinter().getName();
        PrintService ps = null;
        try {

            byte[] by = zplString.getBytes();
            DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
            PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);

            if (pss.length == 0) {
                throw new RuntimeException("No printer services available.");
            }

            for (PrintService psRead : pss) {
                if (psRead.getName().contains(printerName)) {
                    ps = psRead;
                    break;
                }
            }

            if (ps == null) {
                throw new RuntimeException("No printer mapped in TS.");
            }

            DocPrintJob job = ps.createPrintJob();
            Doc doc = new SimpleDoc(by, flavor, null);

            job.print(doc, null);

        } catch (PrintException e) {
            GUIUtils.showMessage("Cannot print label on this printer : " + ps.getName(), Alert.AlertType.ERROR);
        }

        return true;
    }

    private static class StaticsLabel {

        public static String imgCompressedDeliz = "01024,01024,00008,:Z64:eJyt0U1OhTAQAOAhNc5OLmDkIoZeyxX0xYVLj+BNfN15DY7QJYum4/wReTEvmBcnIR/QmWFaAEmiwqEJOJ7dl+tmM6jlx4XpCnSbheWXm7CyUlzdie25aWQHvuJ2z2tN1riH5vJsUhsoz9ILeFz5BsxEOtNAki4Jugxx8ceq9kT2mih7mSbQ11nyu4Zv2n7FkxSEgkn64YJpckcdGZOMiem3/R+9qON+0R133wts3c3TVZsPGuq8rM0/ou3nEW1/clBmhl0c/fdbQ+rbdOy1uHPvt37pcnrkTeq5rG51m8s/Xz1n86OY76v5Ws0TmWm2c04xm0MxcTVDMzsyYf40YzN7F93gwubkPrkPN57tP8Y3xyLbQg==:5AEB";
        public static String imgCompressedUpWave = "938,938,7, ,::N0G3IFH0N0G7IFH0::N0G7GCJ0N0G7G8J0::N0G3GCJ0N0G7IFH0:::,M0KFH0::::N0G7GCG1GFH0N0G7G8G0GFH0:::N0G7GCG1GFH0N0G7IFH0N0G3HFGEG0GCN0G3HFGEG1GCO0HFG8G3GCO0G1GCG0G7GCR0GFG0R0G3GCP0G3HFGCO0G1IFGCP0G7HFGCP0G3HFGCQ0G7GFGCQ0G1GFGCR0G7GCR0G1GC,:::P0G1GFH0P0G7GFH0O0G7HFH0N0G3IFH0N0G7HFGCH0N0G7GFGEI0N0G7GEJ0N0G7GFGCI0N0G7HFGEH0O0IFH0P0HFH0:O0IFH0N0G7HFGEH0N0G7GFGCI0N0G7GEJ0N0G7GFGEI0N0G7HFGCH0N0G3IFH0O0G7HFH0P0G7GFH0P0G1GFH0N0G1GFG0G1H0N0G3GFG9GCH0N0G7GFGDGEH0N0G7GFGDGFH0N0G7GBGDGFH0N0G7G1GCG7H0N0G7G1GCG7G8G0:N0G7G0GEG7G8G0N0G7G8GEG7G8G0N0G3GEHFH0N0G7IFH0:N0G7HFGEH0N0G7HFG8H0,Q0GFH0P0G3GFH0O0G1HFH0O0G7HFH0N0G7HFGCH0N0G7HFI0N0G7GFJ0N0G7GFG8I0N0G7HFI0N0G7IFH0O0G7HFH0O0G1HFH0P0G3GFH0P0G4GFH0O0HFG8H0N0G1HFGEH0N0G3HFGEH0N0G7IFH0N0G7HDGFH0N0G7G9GCGFH0N0G7G1GCG7G8G0::N0G7G9GCGFG8G0N0G7GDGCGFH0N0G7GDHFH0N0G3GDGFGEH0N0G1GDGFGCH0O0GDGFG8H0,:::::::::::::::::::";

        public static String imagemInstrucaoGeral = ""
                + "~DGR:COMP.GRF,2158,26," +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000FF0000000000000000000000000000000000000000000000\n" +
                "0000BD8000000000000000000000000000000000000000000000\n" +
                "00018080000000000001800000300000000000000000003F0000\n" +
                "00010080000000000001800000700000000000000000807F8040\n" +
                "00010080000000400000C0000060000000000000000181FFE040\n" +
                "00030080000000400000C00000E00000000000000001C3FFF0E0\n" +
                "00040080000000E00000600000C1FFFFFF801FFFC000E78079C0\n" +
                "00080040000000E000003FFFFF81FFFFFF801FFFC000FE001F80\n" +
                "00080040000001E000003FFFFF01800C01801FFFC0007C000F80\n" +
                "181000406000033000003C3F87018018018E000060E038000F00\n" +
                "181000406000031000003CFFCF018030018F000061E07C000F80\n" +
                "1810004060000318000036FFCD018030018F000061C07C000F80\n" +
                "09D000466000060C00003380390180E60181C0003700C70039C0\n" +
                "09D0004F6000040C00003380390180E60180E0003E00C70038C0\n" +
                "0FF0004F43000C0C00303380390181E6018070003C00C38070C0\n" +
                "0F720059C1C00C0600F036C06D0183E601803800780183C0E0C0\n" +
                "0E320070C1E0180601E036E0CD0183E601801C00F00181C0E060\n" +
                "0E320070C0F0180201E03460CD0186E601801E00F00181E1E060\n" +
                "0C120070C078180303C03C61CD01866601800F01F80180E1C060\n" +
                "04120040C00F30011E003C1B07018CE6018003C7180180738060\n" +
                "0412DB40C007A001F8003C1E070198E6018001EE0801803F0060\n" +
                "0616DB40C003E001F800381E070198E6018000FE0801803F0060\n" +
                "060ADA408000F003C000380E0701B0E60180007C0C01801E0060\n" +
                "0602DA4080007807C000381E0701E0E6018008FC1C01801E0060\n" +
                "0602DA4180007C0FC000381E0701E0E601801FFFFC01801E0060\n" +
                "0202DA41800087F860003C31870180E6018073C7C601807F0060\n" +
                "0202DA41800183F020003C618D0180E60180E783C60180738060\n" +
                "0202DA41800181F030003460CD0180E60180CF01E60180E38060\n" +
                "0302DA41000101F0300034E0CD0180E60180CE00E60180E1C0C0\n" +
                "0302DBC100030F3E10003780790180E60181B8007A00C1C0E0C0\n" +
                "0302DB8300030F1E18003780790180E60181B8003F00C1C0E0C0\n" +
                "0302DA8300023C0F18003380390180E60181F0001F00C38071C0\n" +
                "0103DC030007E001EC0037E0F90180E60183C000078067003B80\n" +
                "01017403000FC000FC0036F3ED0180000187800003C07E003F80\n" +
                "01800002000F00003E003C3F87018000018F000001E03C001F00\n" +
                "01800002003C00000F003C1F07018000019E000001F03C000E00\n" +
                "018000060078000007C03FFFFF018000019E000001B03E001F00\n" +
                "0180000601F0000003E07000038180000183FFFFFF807F007F80\n" +
                "00FFFFFE03BFFFFFFFB0E00000C1FFFFFF83FFFFFF80E3FFF1C0\n" +
                "00FFFFFE033FFFFFFFB0C00000C1FFFFFF0000000000E1FFE1C0\n" +
                "00FFFFFC000000000001C00000E00000000000000001C0FFC1E0\n" +
                "00000000000000000001800000600000000000000001C03F00E0\n" +
                "0000000000000000000100000020000000000000000080000040\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000000000000000";

        public static String imagemInstrucaoF60266 = ""
                + "~DGR:COMP.GRF,2158,26, "
                + "0000000008000000000001000000070000000000000000000000\n"
                + "080000000E000000000003800000078000000000000000000000\n"
                + "280000000F000002000003E000000F8000000000000000000000\n"
                + "380000001F000007000001FFFFFFFE0000FFFFE0000003F80000\n"
                + "7DE01E01FF000007000000FFFFFFF80000FFFFF000403C078040\n"
                + "7F383F873F00600F0030007CFFF0700000FFFFF800607803C1C0\n"
                + "3E1FE1FC3E00781D80E0007DF87EF00008FFFFFD0038E000E380\n"
                + "0F8780703C001C18C3C0006F801FF00008001FFF001CC0006700\n"
                + "07C00000F8001E18C38000630003B0000C0000FF000F80003E00\n"
                + "07E00001F8000730EF0000630003B0000E00001E000380007800\n"
                + "07F00007F80001E07C000065C01FB0000300000C0007E000FC00\n"
                + "07FC000FF80000E07000006CF01EF0000380003C0006F001CC00\n"
                + "073E003FB00000F8F000006CF83C700000E0007E000638078C00\n"
                + "071F003E300000DF9800007C7870700000E000FE000E1C060E00\n"
                + "070FC07C3000018F1C00007C3CE07000007FFFFE000C03380600\n"
                + "0387C1F03000019F9C0000780EE0700001FFFFFE000C03F00600\n"
                + "0381F3E020000378E600007807C0700003FFFFFE000C01E00600\n"
                + "0381FFC0200003E07E00007807C0700007C79C07000C03F80600\n"
                + "03807F80200007E03F0000783EE070000F03FC07000C03380600\n"
                + "01C03F80600007801F00007C7CE070001800E00700061C070C00\n"
                + "01C07F8060001E000380006CF87070003801F003000678038C00\n"
                + "01C1FFC060001C0001E0006CF83C70007003F8030003F001D800\n"
                + "01C1F3E06000780000E00065F01EF00070039C030003C000F800\n"
                + "00E7C0F86001F00000F80067C01FB000600F1E03800380003800\n"
                + "00FFC0FCE003F00000FE0063800FB000E03C0383800EC0006E00\n"
                + "00FFFFFFC007E00000670067000FB000FFFFFFFF803CF001E700\n"
                + "00FFFFFFC00E60000037806FC01FF000FFFFFFFFC0303C078380\n"
                + "00FFFFFFC018E0000038C07CF8FCF001FFFFFFFFC0601FFF00C0\n"
                + "01F00007E010FFFFFFF8407C7FE070000300001C004007FC0040\n"
                + "03E00001E001FFFFFFF8007FFFFFF8000700000E000000000000\n"
                + "03C0000060000000000001FBFFFFFE000E00000E000000000000\n"
                + "0180000020000000000001E000001F000C000003000000000000\n"
                + "0000000000000000000003800000078008000003000000000000\n"
                + "0000000000000000000000000000070000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000007C00000\n"
                + "000000000000000000000000000000000000000000000FF00000\n"
                + "00000000000000000000000000000000000000000001F83E0000\n"
                + "0000000000000000000003FFFFFFFC00000000000003E00F8000\n"
                + "0000000000000000000003FFFFFFFC000000000000078007C000\n"
                + "0000000000000000000003FFFFFFFC0000000000000E0000F000\n"
                + "0000000000000000000003807000040000000000000800007000\n"
                + "000000000000000000000381E0000C0000000000001800003000\n"
                + "000000000000000000000383C000040000000000003000001000\n"
                + "0000000000000000000003878000040000000000003000000800\n"
                + "00000000000000000000038F0000040000000000007000000800\n"
                + "00000000000000000000039E0000040000000000007387830C00\n"
                + "0000000000000000000003BC0000040000000000006186C70C00\n"
                + "0000000000000000000003B8000004000000000000E184C60C00\n"
                + "0000000000000000000003F0000004000000000000E1CCC60C00\n"
                + "0000000000000000000003E0000004000000000000E0CC6C0C00\n"
                + "0000000000000000000003C0000004000000000000E0C86C0C00\n"
                + "000000000000000000000380000004000000000000E0687C0C00\n"
                + "000000000000000000000381FFFF040000000000007060380C00\n"
                + "000000000000000000000381FFFF040000000000007030180800\n"
                + "000000000000000000000381FFFF040000000000003030100800\n"
                + "0000000000000000000003800002040000000000001000001000\n"
                + "0000000000000000000003800000040000000000001000003000\n"
                + "0000000000000000000003800000040000000000000800003000\n"
                + "0000000000000000000003800000040000000000000E0001E000\n"
                + "0000000000000000000003800000040000000000000F8003C000\n"
                + "00000000000000000000038000000400000000000007C00F8000\n"
                + "00000000000000000000038000000400000000000001F00F0000\n"
                + "000000000000000000000380000004000000000000003FF00000\n"
                + "000000000000000000000380000004000000000000000FC00000\n"
                + "0000000000000000000003800000040000000000000000000000\n"
                + "0000000000000000000003800000040000000000000FFFFFF000\n"
                + "0000000000000000000003FFFFFFFC0000000000000FFFFFF000\n"
                + "0000000000000000000003FFFFFFFC0000000000000000000000\n"
                + "0000000000000000000000000000000000000000000FFFFFF000\n"
                + "0000000000000000000000000000000000000000000FFFFFF000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000\n"
                + "0000000000000000000000000000000000000000000000000000";

        public static String imagemLogoDeliz = ""
                + "~DGR:LOGO,792,8, "
                + "3FFFFFFFFFFFF800                                                    \n"
                + "7FFFFFFFFFFFF800                                                                     \n"
                + "3FFFFFFFFFFFF800                                                                     \n"
                + "389FFFFFFFF53800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3800000000003800                                                                     \n"
                + "3C00000000007000                                                                     \n"
                + "1C00000000007000                                                                     \n"
                + "1C00000000007000                                                                     \n"
                + "1E0000000000F000                                                                     \n"
                + "0E0000000000E000                                                                     \n"
                + "0F0000000001E000                                                                     \n"
                + "070000000001C000                                                                     \n"
                + "078000000003C000                                                                     \n"
                + "03C0000000078000                                                                     \n"
                + "03C00000000F0000                                                                     \n"
                + "01F00000001F0000                                                                     \n"
                + "00F80000003E0000                                                                     \n"
                + "007C0000007C0000                                                                     \n"
                + "003F000001F80000                                                                     \n"
                + "000FC00007E00000                                                                     \n"
                + "0007FC007FC00000                                                                     \n"
                + "0001FFFFFF000000                                                                     \n"
                + "00003FFFF8000000                                                                     \n"
                + "000007FF80000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "001FFE0000000000                                                                     \n"
                + "007FFF8000000000                                                                     \n"
                + "01FFFFE000000000                                                                     \n"
                + "03F8E7F000000000                                                                     \n"
                + "07C0E0F800000000                                                                     \n"
                + "0F00E07C00000000                                                                     \n"
                + "1E00E03C00000000                                                                     \n"
                + "1C00E01E00000000                                                                     \n"
                + "3C00E00E00000000                                                                     \n"
                + "3800E00F00000000                                                                     \n"
                + "3800E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "7000E00700000000                                                                     \n"
                + "7000E00700000000                                                                     \n"
                + "7800E00700000000                                                                     \n"
                + "3800E00700000000                                                                     \n"
                + "3800E00F00000000                                                                     \n"
                + "3800E00E00000000                                                                     \n"
                + "3C00E01E00000000                                                                     \n"
                + "1E00E03C00000000                                                                     \n"
                + "1F00E07C00000000                                                                     \n"
                + "0F80E1F800000000                                                                     \n"
                + "07C0E7F000000000                                                                     \n"
                + "03C0FFC000000000                                                                     \n"
                + "0180FF8000000000                                                                     \n"
                + "0000FC0000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "3FFFFFFFFFFFFF00                                                                     \n"
                + "7FFFFFFFFFFFFF80                                                                     \n"
                + "7FFFFFFFFFFFFF80                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "7FFFFFFE3E000000                                                                     \n"
                + "3FFFFFFE3E000000                                                                     \n"
                + "7FFFFFFE3E000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "2000000000000000                                                                     \n"
                + "7000000000000000                                                                     \n"
                + "7800000000000000                                                                     \n"
                + "7E00000E00000000                                                                     \n"
                + "7F00000E00000000                                                                     \n"
                + "7F80000E00000000                                                                     \n"
                + "7FE0000E00000000                                                                     \n"
                + "79F0000E00000000                                                                     \n"
                + "70FC000E00000000                                                                     \n"
                + "707E000E00000000                                                                     \n"
                + "781F000E00000000                                                                     \n"
                + "700FC00E00000000                                                                     \n"
                + "7803E00E00000000                                                                     \n"
                + "7801F80E00000000                                                                     \n"
                + "7800FC0E00000000                                                                     \n"
                + "78003E0E00000000                                                                     \n"
                + "78001F8E00000000                                                                     \n"
                + "78000FCE00000000                                                                     \n"
                + "780003FE00000000                                                                     \n"
                + "780001FE00000000                                                                     \n"
                + "7800007E00000000                                                                     \n"
                + "7800003E00000000                                                                     \n"
                + "7800001E00000000                                                                     \n"
                + "7800000600000000                                                                     \n"
                + "0000000000000000                                                                     \n"
                + "0000000000000000 \n";
    }


}
