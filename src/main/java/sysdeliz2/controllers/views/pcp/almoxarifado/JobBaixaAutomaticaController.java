package sysdeliz2.controllers.views.pcp.almoxarifado;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class JobBaixaAutomaticaController extends WindowBase {
    
    protected final StringProperty inicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty tempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty statusJobLog = new SimpleStringProperty();
    protected final StringProperty dataProximoLog = new SimpleStringProperty();
    
    public JobBaixaAutomaticaController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getStatusJob() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery(""+
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'J_SD_BAIXA_MATERIAL_CONSUMO'");
        
        if (dadosLog.size() > 0){
            Map infosLog = (Map) dadosLog.get(0);
            statusJobLog.set(infosLog.get("ESTADO").toString());
            inicioUltimoLog.set(infosLog.get("ULT_RODADA").toString());
            tempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            dataProximoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }
    
    protected void runJob() throws SQLException {
       new NativeDAO().runNativeQueryUpdate(""+
               "BEGIN\n" +
               "  DBMS_SCHEDULER.RUN_JOB(\n" +
               "    JOB_NAME            => 'J_SD_BAIXA_MATERIAL_CONSUMO',\n" +
               "    USE_CURRENT_SESSION => FALSE);\n" +
               "END;");
        
        getStatusJob();
    }
}
