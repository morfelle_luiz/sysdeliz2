package sysdeliz2.controllers.views.pcp.enfesto;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdCorRisco;
import sysdeliz2.models.sysdeliz.SdMateriaisRiscoCor;
import sysdeliz2.models.sysdeliz.SdPlanejamentoEncaixe;
import sysdeliz2.models.ti.FaixaItem;
import sysdeliz2.models.ti.Of2;
import sysdeliz2.models.view.VSdAplicacoesOf;
import sysdeliz2.models.view.VSdMatAplicacaoOf;
import sysdeliz2.models.view.VSdMateriaisCorte;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PlanejamentoEnfestoController extends WindowBase {
    
    protected Of2 ofCarregada = null;
    protected List<VSdMateriaisCorte> materiasOf = new ArrayList<>();
    protected List<VSdAplicacoesOf> aplicacoesOf = new ArrayList<>();
    protected List<VSdMatAplicacaoOf> materialAplicacoesOf = new ArrayList<>();
    protected List<FaixaItem> faixaProduto = new ArrayList<>();
    protected List<SdPlanejamentoEncaixe> planejamentosDaOf = new ArrayList<>();
    
    protected SdPlanejamentoEncaixe planejamentoAberto = new SdPlanejamentoEncaixe();
    
    public PlanejamentoEnfestoController(String title, Image icone) {
        super(title, icone, new String[]{"Planejamento", "Listagem"});
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected Of2 getOf(@NotNull String numero) throws EntityNotFoundException {
        ofCarregada = new FluentDao().selectFrom(Of2.class)
                .where(it -> it
                        .equal("numero", numero))
                .singleResult();
        return ofCarregada;
    }
    
    protected void getMateriais(@NotNull String numero) {
        JPAUtils.clearEntitys(materiasOf);
        materiasOf = (List<VSdMateriaisCorte>) new FluentDao().selectFrom(VSdMateriaisCorte.class)
                .where(it -> it
                        .equal("numero", numero))
                .resultList();
    }
    
    protected void getAplicacoes(@NotNull String numero) {
        JPAUtils.clearEntitys(aplicacoesOf);
        aplicacoesOf = (List<VSdAplicacoesOf>) new FluentDao().selectFrom(VSdAplicacoesOf.class)
                .where(it -> it
                        .equal("numero", numero))
                .resultList();
    }
    
    protected void getMaterialAplicacoes(@NotNull String numero) {
        JPAUtils.clearEntitys(materialAplicacoesOf);
        materialAplicacoesOf = (List<VSdMatAplicacaoOf>) new FluentDao().selectFrom(VSdMatAplicacaoOf.class)
                .where(it -> it
                        .equal("numero", numero))
                .resultList();
    }
    
    protected void getFaixaProduto(@NotNull String faixa) {
        JPAUtils.clearEntitys(faixaProduto);
        faixaProduto = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class)
                .where(it -> it
                        .equal("faixaItemId.faixa", faixa))
                .orderBy("posicao", OrderType.ASC)
                .resultList();
    }
    
    protected void getPlanejamentosOf(@NotNull String numero) {
        JPAUtils.clearEntitys(planejamentosDaOf);
        planejamentosDaOf = (List<SdPlanejamentoEncaixe>) new FluentDao().selectFrom(SdPlanejamentoEncaixe.class)
                .where(it -> it
                        .equal("numero.numero", numero))
                .orderBy("dtplano", OrderType.DESC)
                .resultList();
    }
    
    protected void getPlanejamentosOf(String numero, String usuario, String referencia, LocalDateTime inicio, LocalDateTime fim) {
        planejamentosDaOf = (List<SdPlanejamentoEncaixe>) new FluentDao().selectFrom(SdPlanejamentoEncaixe.class)
                .where(it -> it
                        .equal("numero.numero", numero, TipoExpressao.AND, b -> numero != null)
                        .equal("usuario", usuario, TipoExpressao.AND, b -> usuario != null)
                        .equal("numero.produto.codigo", referencia, TipoExpressao.AND, b -> referencia != null)
                        .between("dtplano", inicio, fim, TipoExpressao.AND, b -> inicio != null && fim != null))
                .orderBy("dtplano", OrderType.DESC)
                .resultList();
    }
    
    protected void deleteAllMateriaisRisco(SdCorRisco corRisco) throws SQLException {
        new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_materiais_risco_cor_001 where id_risco = %s", corRisco.getId()));
    }
    
    protected void insertMaterialRisco(List<SdMateriaisRiscoCor> rolos) throws SQLException {
        for (SdMateriaisRiscoCor rolo : rolos)
            new NativeDAO().runNativeQueryUpdate(String.format("" +
                            "merge into sd_materiais_risco_cor_001 mat\n" +
                            "using dual\n" +
                            "on (mat.id = %s)\n" +
                            "when matched then\n" +
                            "  update\n" +
                            "     set mat.consumido   = %s,\n" +
                            "         mat.qtde_folhas = %s,\n" +
                            "         mat.qtde_pecas  = %s,\n" +
                            "         mat.cons_debrum = %s,\n" +
                            "         mat.seq_enfesto = %s\n" +
                            "when not matched then\n" +
                            "  insert\n" +
                            "    (id, id_risco, cor, material, cor_i, lote, tonalidade, partida, largura, gramatura, sequencia, consumido, qtde_folhas, qtde_pecas, cons_debrum, seq_enfesto)\n" +
                            "  values\n" +
                            "    (seq_sd_materiais_risco_cor.nextval, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s, %s, '%s', %s, %s, %s, %s, %s)",
                    rolo.getId(),
                    rolo.getConsumido(), rolo.getQtdefolhas(), rolo.getQtdepecas(), rolo.getConsumidoDebrum(), rolo.getSeqEnfesto(),
                    rolo.getIdrisco(), rolo.getCor(), rolo.getMaterial(), rolo.getCori(), rolo.getLote(), rolo.getTonalidade(), rolo.getPartida(), rolo.getLargura(), rolo.getGramatura(), rolo.getSequencia(), rolo.getConsumido(), rolo.getQtdefolhas(), rolo.getQtdepecas(), rolo.getConsumidoDebrum(), rolo.getSeqEnfesto()));
    }
    
    protected List<Object> getDebrumsMaterialCorpo(Integer idPlanejamento, String numero) throws SQLException {
        return new NativeDAO().runNativeQuery(String.format("" +
                "select plano.id id_plano,\n" +
                "       nvl(mat.id_risco, cor.id) id_risco,\n" +
                "       cons.aplic_pcp,\n" +
                "       nvl(mat.cor, 'AGRP') cor_prod,\n" +
                "       nvl(mat.material, cons.insumo) material,\n" +
                "       nvl(mat.cor_i, cons.cor_i) cor_i,\n" +
                "       nvl(mat.lote, mcort.lote) lote,\n" +
                "       nvl(mat.tonalidade, mcort.tonalidade) tonalidade,\n" +
                "       nvl(mat.partida, mcort.partida) partida,\n" +
                "       nvl(mat.sequencia, mcort.sequencia) sequencia,\n" +
                "       nvl(mat.largura, mcort.largura) largura,\n" +
                "       nvl(mat.gramatura, mcort.gramatura) gramatura,\n" +
                "       cons.consumo cons_deb_peca,\n" +
                "       cons.consumo *\n" +
                "       to_number(nvl(mat.qtde_pecas, nvl(cor.total_realizado, 0))) cons_deb_total,\n" +
                "       0 qtde_folhas,\n" +
                "       to_number(nvl(mat.qtde_pecas, nvl(cor.total_realizado, 0))) qtde_pecas\n" +
                "  from v_sd_consumos_of_cor_apl cons\n" +
                "  join sd_planejamento_encaixe_001 plano\n" +
                "    on plano.numero = cons.numero\n" +
                "   and plano.id = %s -- parametro\n" +
                "  join sd_risco_plan_encaixe_001 risco\n" +
                "    on risco.id_planejamento = plano.id\n" +
                "  join sd_cor_risco_001 cor\n" +
                "    on cor.id_risco = risco.id\n" +
                "   and cor.cor = cons.cor\n" +
                "  left join sd_materiais_risco_cor_001 mat\n" +
                "    on mat.id_risco = cor.id\n" +
                "   and cons.insumo = mat.material\n" +
                "   and cons.cor_i = mat.cor_i\n" +
                "  left join v_sd_materiais_corte mcort\n" +
                "    on mcort.numero = cons.numero\n" +
                "   and mcort.codigo = cons.insumo\n" +
                "   and mcort.cor_i = cons.cor_i\n" +
                "   and (mcort.lote = mat.lote or mat.lote is null)\n" +
                "   and (mcort.tonalidade = mat.tonalidade or mat.tonalidade is null)\n" +
                "   and (mcort.partida = mat.partida or mat.partida is null)\n" +
                "   and (mcort.sequencia = mat.sequencia or mat.sequencia is null)\n" +
                " where cons.numero = '%s' -- parametro\n" +
                "   and cons.aplicacao = '001'\n" +
                "   and (mcort.lote is not null or mat.lote is not null)", idPlanejamento, numero));
    }
    
    protected List<Object> getDebrumsSemMaterialCorpo(Integer idPlanejamento, String numero) throws SQLException {
        return new NativeDAO().runNativeQuery(String.format("" +
                "select gradeCor.id id_plano,\n" +
                "       0 id_risco,\n" +
                "       cons.aplic_pcp,\n" +
                "       cons.cor cor_prod,\n" +
                "       mats.codigo material,\n" +
                "       mats.cor_i,\n" +
                "       mats.lote,\n" +
                "       mats.tonalidade,\n" +
                "       mats.partida,\n" +
                "       mats.sequencia,\n" +
                "       mats.largura,\n" +
                "       mats.gramatura,\n" +
                "       cons.consumo cons_deb_peca,\n" +
                "       (round(f_verifica_divisor(mats.reserva,\n" +
                "                                 (sum(mats.reserva)\n" +
                "                                  over(partition by gradeCor.id,\n" +
                "                                       cons.aplic_pcp,\n" +
                "                                       mats.codigo,\n" +
                "                                       mats.cor_i))) * gradeCor.realizado,\n" +
                "              0)) * cons.consumo cons_deb_total,\n" +
                "       0 qtde_folhas,\n" +
                "       round(f_verifica_divisor(mats.reserva,\n" +
                "                                (sum(mats.reserva)\n" +
                "                                 over(partition by gradeCor.id,\n" +
                "                                      cons.aplic_pcp,\n" +
                "                                      mats.codigo,\n" +
                "                                      mats.cor_i))) * gradeCor.realizado,\n" +
                "             0) qtde_pecas\n" +
                "  from v_sd_consumos_of_cor_apl cons\n" +
                "  join v_sd_materiais_corte mats\n" +
                "    on mats.numero = cons.numero\n" +
                "   and mats.codigo = cons.insumo\n" +
                "   and mats.cor_i = cons.cor_i\n" +
                "  join (select plano.id,\n" +
                "               plano.numero,\n" +
                "               grade.cor,\n" +
                "               sum(realizado) realizado,\n" +
                "               sum(grade)\n" +
                "          from sd_planejamento_encaixe_001 plano\n" +
                "          join sd_risco_plan_encaixe_001 risco\n" +
                "            on risco.id_planejamento = plano.id\n" +
                "           and risco.tipo_risco = 1\n" +
                "          join sd_cor_risco_001 cor\n" +
                "            on cor.id_risco = risco.id\n" +
                "          join sd_grade_risco_cor_001 grade\n" +
                "            on grade.id_risco = cor.id\n" +
                "         group by plano.id, plano.numero, grade.cor) gradeCor\n" +
                "    on gradeCor.numero = cons.numero\n" +
                "   and gradeCor.cor = cons.cor\n" +
                " where gradeCor.id = %s\n" +
                "   and cons.numero = '%s'\n" +
                "   and cons.aplicacao = '001'\n", idPlanejamento, numero));
    }
    
    protected List<ConsumoTecido> getConsumosCorProduto(String numero, String cor, String aplicacao) {
        List<ConsumoTecido> consumos = new ArrayList<>();
        String query = String.format("" +
                "select pcp.numero,\n" +
                "       nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) aplicacao,\n" +
                "       apl.descricao,\n" +
                "       pcp.insumo,\n" +
                "       flu.sd_grupo,\n" +
                "       pcp.cor,\n" +
                "       pcp.cor_i,\n" +
                "       pcp.consumo,\n" +
                "       sum(cof.qtde_of) * pcp.consumo consumoTotal,\n" +
                "       mat.unidade,\n" +
                "       (select '|'||listagg(trim(tam),'|') within group (order by tam)||'|' from v_pcpfaixa where faixa = pcp.faixa) faixa,\n" +
                "       min(cort.largura) largura,\n" +
                "       min(cort.gramatura) gramatura\n" +
                "  from pcpftof_001 pcp\n" +
                "  join v_pcpfaixa fai\n" +
                "    on fai.faixa = pcp.faixa\n" +
                "  join material_001 mat\n" +
                "    on mat.codigo = pcp.insumo\n" +
                "  join v_sd_cortam_of cof\n" +
                "    on cof.numero = pcp.numero\n" +
                "   and cof.cor = pcp.cor\n" +
                "   and cof.tam = fai.tam\n" +
                "  join pcpapl_001 apl\n" +
                "    on apl.codigo = pcp.aplicacao\n" +
                "  join cadfluxo_001 flu\n" +
                "    on flu.codigo = pcp.setor\n" +
                "  join v_sd_materiais_corte cort\n" +
                "    on cort.numero = pcp.numero\n" +
                "   and cort.codigo = pcp.insumo\n" +
                "   and cort.cor_i = pcp.cor_i" +
                " where pcp.numero = '%s'\n" +
                "   and flu.sd_grupo = '103'\n" +
                "   and pcp.cor_i <> '*****'\n" +
                "   and pcp.cor in ('%s')\n" +
                "   and (apl.sd_risco_apl <> '%s' or apl.sd_risco_apl is null)\n" +
                " group by pcp.numero,\n" +
                "          apl.sd_risco_apl,\n" +
                "          pcp.insumo,\n" +
                "          mat.unidade,\n" +
                "          pcp.faixa,\n" +
                "          pcp.consumo,\n" +
                "          flu.sd_grupo,\n" +
                "          nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)),\n" +
                "          apl.descricao,\n" +
                "          pcp.cor,\n" +
                "          pcp.cor_i", numero, cor, aplicacao);
        
        new FluentDao().runNativeQuery(query).stream().forEach(value -> {
            consumos.add(new ConsumoTecido(((Object[]) value)[0].toString(),
                    ((Object[]) value)[1].toString(),
                    ((Object[]) value)[2].toString(),
                    ((Object[]) value)[3].toString(),
                    ((Object[]) value)[4].toString(),
                    ((Object[]) value)[5].toString(),
                    ((Object[]) value)[6].toString(),
                    new BigDecimal(((Object[]) value)[7].toString()),
                    new BigDecimal(((Object[]) value)[8].toString()),
                    ((Object[]) value)[9].toString(),
                    ((Object[]) value)[10].toString(),
                    new BigDecimal(((Object[]) value)[11].toString()),
                    new BigDecimal(((Object[]) value)[12].toString())
            ));
        });
        
        return consumos;
    }
    
    protected List<ConsumoTecido> getConsumosUnidade(String numero, String cor) {
        List<ConsumoTecido> consumos = new ArrayList<>();
        String query = String.format("" +
                "select pcp.numero,\n" +
                "       nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) aplicacao,\n" +
                "       apl.descricao,\n" +
                "       pcp.insumo,\n" +
                "       flu.sd_grupo,\n" +
                "       pcp.cor,\n" +
                "       pcp.cor_i,\n" +
                "       pcp.consumo,\n" +
                "       nvl((select sum(res.qtde) qtde\n" +
                "          from mat_reserva_001 res\n" +
                "         where res.numero = pcp.numero\n" +
                "           and res.codigo = pcp.insumo\n" +
                "           and res.cor = pcp.cor_i), 0) reservaTotal,\n" +
                "       mat.unidade,\n" +
                "       (select '|' || listagg(trim(tam), '|') within group(order by tam) || '|'\n" +
                "          from v_pcpfaixa\n" +
                "         where faixa = pcp.faixa) faixa,\n" +
                "       min(cort.largura) largura,\n" +
                "       min(cort.gramatura) gramatura\n" +
                "  from pcpftof_001 pcp\n" +
                "  join v_pcpfaixa fai\n" +
                "    on fai.faixa = pcp.faixa\n" +
                "  join material_001 mat\n" +
                "    on mat.codigo = pcp.insumo\n" +
                "  join v_sd_cortam_of cof\n" +
                "    on cof.numero = pcp.numero\n" +
                "   and cof.cor = pcp.cor\n" +
                "   and cof.tam = fai.tam\n" +
                "  join pcpapl_001 apl\n" +
                "    on apl.codigo = pcp.aplicacao\n" +
                "  join cadfluxo_001 flu\n" +
                "    on flu.codigo = pcp.setor\n" +
                "  join v_sd_materiais_corte cort\n" +
                "    on cort.numero = pcp.numero\n" +
                "   and cort.codigo = pcp.insumo\n" +
                "   and cort.cor_i = pcp.cor_i" +
                " where pcp.numero = '%s'\n" +
                "   and flu.sd_grupo = '103' -- fixo\n" +
                "   and pcp.cor_i <> '*****' -- fixo\n" +
                "   and pcp.cor in ('%s')\n" +
                "   and mat.unidade in ('UN', 'PC', 'KIT') -- fixo\n" +
                " group by pcp.numero,\n" +
                "          apl.sd_risco_apl,\n" +
                "          pcp.insumo,\n" +
                "          mat.unidade,\n" +
                "          pcp.faixa,\n" +
                "          pcp.consumo,\n" +
                "          flu.sd_grupo,\n" +
                "          nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)),\n" +
                "          apl.descricao,\n" +
                "          pcp.cor,\n" +
                "          pcp.cor_i", numero, cor);
        
        new FluentDao().runNativeQuery(query).stream().forEach(value -> {
            consumos.add(new ConsumoTecido(((Object[]) value)[0].toString(),
                    ((Object[]) value)[1].toString(),
                    ((Object[]) value)[2].toString(),
                    ((Object[]) value)[3].toString(),
                    ((Object[]) value)[4].toString(),
                    ((Object[]) value)[5].toString(),
                    ((Object[]) value)[6].toString(),
                    new BigDecimal(((Object[]) value)[7].toString()),
                    new BigDecimal(((Object[]) value)[8].toString()),
                    ((Object[]) value)[9].toString(),
                    ((Object[]) value)[10].toString(),
                    new BigDecimal(((Object[]) value)[11].toString()),
                    new BigDecimal(((Object[]) value)[12].toString())
            ));
        });
        
        return consumos;
    }
    
    protected List<ConsumoTecido> getConsumosTecido(String numero, String cor, String insumo, String cori) {
        List<ConsumoTecido> consumos = new ArrayList<>();
        String query = String.format("" +
                "select pcp.numero,\n" +
                "       nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) aplicacao,\n" +
                "       apl.descricao,\n" +
                "       pcp.insumo,\n" +
                "       flu.sd_grupo,\n" +
                "       pcp.cor,\n" +
                "       pcp.cor_i,\n" +
                "       pcp.consumo,\n" +
                "       sum(cof.qtde_of) * pcp.consumo consumoTotal,\n" +
                "       mat.unidade,\n" +
                "       (select '|'||listagg(trim(tam),'|') within group (order by tam)||'|' from v_pcpfaixa where faixa = pcp.faixa) faixa,\n" +
                "       min(cort.largura) largura,\n" +
                "       min(cort.gramatura) gramatura\n" +
                "  from pcpftof_001 pcp\n" +
                "  join v_pcpfaixa fai\n" +
                "    on fai.faixa = pcp.faixa\n" +
                "  join material_001 mat\n" +
                "    on mat.codigo = pcp.insumo\n" +
                "  join v_sd_cortam_of cof\n" +
                "    on cof.numero = pcp.numero\n" +
                "   and cof.cor = pcp.cor\n" +
                "   and cof.tam = fai.tam\n" +
                "  join pcpapl_001 apl\n" +
                "    on apl.codigo = pcp.aplicacao\n" +
                "  join cadfluxo_001 flu\n" +
                "    on flu.codigo = pcp.setor\n" +
                "  join v_sd_materiais_corte cort\n" +
                "    on cort.numero = pcp.numero\n" +
                "   and cort.codigo = pcp.insumo\n" +
                "   and cort.cor_i = pcp.cor_i" +
                " where pcp.numero = '%s'\n" +
                "   and flu.sd_grupo = '103'\n" +
                "   and pcp.cor_i <> '*****'\n" +
                "   and pcp.cor not in ('%s')\n" +
                "   and pcp.insumo = '%s'\n" +
                "   and pcp.cor_i = '%s'\n" +
                " group by pcp.numero,\n" +
                "          apl.sd_risco_apl,\n" +
                "          pcp.insumo,\n" +
                "          mat.unidade,\n" +
                "          pcp.consumo,\n" +
                "          pcp.faixa,\n" +
                "          flu.sd_grupo,\n" +
                "          nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)),\n" +
                "          apl.descricao,\n" +
                "          pcp.cor,\n" +
                "          pcp.cor_i", numero, cor, insumo, cori);
        
        new FluentDao().runNativeQuery(query).stream().forEach(value -> {
            consumos.add(new ConsumoTecido(((Object[]) value)[0].toString(),
                    ((Object[]) value)[1].toString(),
                    ((Object[]) value)[2].toString(),
                    ((Object[]) value)[3].toString(),
                    ((Object[]) value)[4].toString(),
                    ((Object[]) value)[5].toString(),
                    ((Object[]) value)[6].toString(),
                    new BigDecimal(((Object[]) value)[7].toString()),
                    new BigDecimal(((Object[]) value)[8].toString()),
                    ((Object[]) value)[9].toString(),
                    ((Object[]) value)[10].toString(),
                    new BigDecimal(((Object[]) value)[11].toString()),
                    new BigDecimal(((Object[]) value)[12].toString())
            ));
        });
        
        return consumos;
    }
    
    protected List<ConsumoTecido> getConsumosTecidoAplicacao(String numero, String cor, String tipoRisco, String insumo, String cori) {
        List<ConsumoTecido> consumos = new ArrayList<>();
        String query = String.format("" +
                "select pcp.numero,\n" +
                "       nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) aplicacao,\n" +
                "       apl.descricao,\n" +
                "       pcp.insumo,\n" +
                "       flu.sd_grupo,\n" +
                "       pcp.cor,\n" +
                "       pcp.cor_i,\n" +
                "       pcp.consumo,\n" +
                "       sum(cof.qtde_of) * pcp.consumo consumoTotal,\n" +
                "       mat.unidade,\n" +
                "       (select '|' || listagg(trim(tam), '|') within group(order by tam) || '|'\n" +
                "          from v_pcpfaixa\n" +
                "         where faixa = pcp.faixa) faixa,\n" +
                "       min(cort.largura) largura,\n" +
                "       min(cort.gramatura) gramatura\n" +
                "  from pcpftof_001 pcp\n" +
                "  join v_pcpfaixa fai\n" +
                "    on fai.faixa = pcp.faixa\n" +
                "  join material_001 mat\n" +
                "    on mat.codigo = pcp.insumo\n" +
                "  join v_sd_cortam_of cof\n" +
                "    on cof.numero = pcp.numero\n" +
                "   and cof.cor = pcp.cor\n" +
                "   and cof.tam = fai.tam\n" +
                "  join pcpapl_001 apl\n" +
                "    on apl.codigo = pcp.aplicacao\n" +
                "  join cadfluxo_001 flu\n" +
                "    on flu.codigo = pcp.setor\n" +
                "  join v_sd_materiais_corte cort\n" +
                "    on cort.numero = pcp.numero\n" +
                "   and cort.codigo = pcp.insumo\n" +
                "   and cort.cor_i = pcp.cor_i" +
                " where pcp.numero = '%s'\n" +
                "   and flu.sd_grupo = '103'\n" +
                "   and pcp.cor_i <> '*****'\n" +
                "   and nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) <> '717' -- fixo\n" +
                "   and (nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)) <> '%s' or pcp.cor not in ('%s')) -- risco/cor que está sendo feito\n" +
                "   and pcp.insumo = '%s'\n" +
                "   and pcp.cor_i = '%s'\n" +
                " group by pcp.numero,\n" +
                "          apl.sd_risco_apl,\n" +
                "          pcp.insumo,\n" +
                "          mat.unidade,\n" +
                "          pcp.consumo,\n" +
                "          pcp.faixa,\n" +
                "          flu.sd_grupo,\n" +
                "          nvl(apl.sd_risco_apl, nvl(apl.tipo, apl.codigo)),\n" +
                "          apl.descricao,\n" +
                "          pcp.cor,\n" +
                "          pcp.cor_i", numero, tipoRisco, cor, insumo, cori);
        
        new FluentDao().runNativeQuery(query).stream().forEach(value -> {
            consumos.add(new ConsumoTecido(((Object[]) value)[0].toString(),
                    ((Object[]) value)[1].toString(),
                    ((Object[]) value)[2].toString(),
                    ((Object[]) value)[3].toString(),
                    ((Object[]) value)[4].toString(),
                    ((Object[]) value)[5].toString(),
                    ((Object[]) value)[6].toString(),
                    new BigDecimal(((Object[]) value)[7].toString()),
                    new BigDecimal(((Object[]) value)[8].toString()),
                    ((Object[]) value)[9].toString(),
                    ((Object[]) value)[10].toString(),
                    new BigDecimal(((Object[]) value)[11].toString()),
                    new BigDecimal(((Object[]) value)[12].toString())
            ));
        });
        
        return consumos;
    }
    
    public class ConsumoTecido {
        
        public String numero;
        public String aplicacao;
        public String descAplicacao;
        public String insumo;
        public String setor;
        public String cor;
        public String corI;
        public String unidade;
        public String faixa;
        public BigDecimal consumo;
        public BigDecimal consumoTotal;
        public BigDecimal largura;
        public BigDecimal gramatura;
        
        public ConsumoTecido(String numero, String aplicacao, String descAplicacao, String insumo, String setor, String cor, String corI, BigDecimal consumo, BigDecimal consumoTotal, String unidade, String faixa, BigDecimal largura, BigDecimal gramatura) {
            this.numero = numero;
            this.aplicacao = aplicacao;
            this.descAplicacao = descAplicacao;
            this.insumo = insumo;
            this.setor = setor;
            this.cor = cor;
            this.corI = corI;
            this.consumo = consumo;
            this.consumoTotal = consumoTotal;
            this.unidade = unidade;
            this.faixa = faixa;
            this.largura = largura;
            this.gramatura = gramatura;
        }
    }
    
}
