package sysdeliz2.controllers.views.pcp.gestaomrp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import org.apache.poi.ss.usermodel.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdMrpProduto;
import sysdeliz2.models.sysdeliz.SdTempMrp;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ExportUtils;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExportarMrpController extends WindowBase {

    protected List<SdTempMrp> mrp = new ArrayList<>();
    protected List<SdMrpProduto> mrpProducao = new ArrayList<>();
    protected List<Object> mrpResumo = new ArrayList<>();
    protected List<String> mrpHeaderResumo = new ArrayList<>();
    protected final StringProperty dataUltimoLog = new SimpleStringProperty();
    protected final StringProperty infoUltimoLog = new SimpleStringProperty();

    protected final StringProperty inicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty tempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty statusJobLog = new SimpleStringProperty();
    protected final StringProperty dataProximoLog = new SimpleStringProperty();

    protected final StringProperty inicioUltimoProducaoLog = new SimpleStringProperty();
    protected final StringProperty tempoUltimoProducaoLog = new SimpleStringProperty();
    protected final StringProperty statusJobProducaoLog = new SimpleStringProperty();
    protected final StringProperty dataProximoProducaoLog = new SimpleStringProperty();

    public ExportarMrpController(String title, Image icone) {
        super(title, icone);
    }

    @Override
    public void closeWindow() {

    }

    protected void getLogJob() {
        List<Object> dadosLog = new FluentDao().runNativeQuery("" +
                "select TO_CHAR(log.LOG_DATE, 'dd/MM/yyyy HH24:MI:SS') dt_log,\n" +
                "       decode(to_char(additional_info),\n" +
                "              'REASON=\"manual slave run\"',\n" +
                "              'Execução Manual',\n" +
                "              null,\n" +
                "              'Execução Automática',\n" +
                "              to_char(additional_info)) info\n" +
                "  from USER_SCHEDULER_JOB_LOG log\n" +
                " where log.log_id = (select max(log_id)\n" +
                "                       from USER_SCHEDULER_JOB_LOG\n" +
                "                      where job_name = 'SD_MRP')\n");

        if (dadosLog.size() > 0) {
            Object[] infosLog = (Object[]) dadosLog.get(0);
            dataUltimoLog.set(infosLog[0].toString());
            infoUltimoLog.set(infosLog[1].toString());
        }
    }

    protected void getStatusJob() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery("" +
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO','S/ ESTADO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'SD_MRP'");

        if (dadosLog.size() > 0) {
            Map infosLog = (Map) dadosLog.get(0);
            statusJobLog.set(infosLog.get("ESTADO").toString());
            inicioUltimoLog.set(infosLog.get("ULT_RODADA").toString());
            tempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            dataProximoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }

    protected void getStatusJobProducao() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery("" +
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'SD_MRP_PRODUCAO'");

        if (dadosLog.size() > 0) {
            Map infosLog = (Map) dadosLog.get(0);
            statusJobProducaoLog.set(infosLog.get("ESTADO").toString());
            inicioUltimoProducaoLog.set(infosLog.get("ULT_RODADA") != null ? infosLog.get("ULT_RODADA").toString() : "");
            tempoUltimoProducaoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            dataProximoProducaoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }

    protected void runJob() throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                "BEGIN\n" +
                "  DBMS_SCHEDULER.RUN_JOB(\n" +
                "    JOB_NAME            => 'SD_MRP',\n" +
                "    USE_CURRENT_SESSION => FALSE);\n" +
                "END;");

        getStatusJob();
    }

    protected void runJobProducao() throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                "BEGIN\n" +
                "  DBMS_SCHEDULER.RUN_JOB(\n" +
                "    JOB_NAME            => 'SD_MRP_PRODUCAO',\n" +
                "    USE_CURRENT_SESSION => FALSE);\n" +
                "END;");

        getStatusJobProducao();
    }

    protected void getMrp(String pais) {
        JPAUtils.clearEntitys(mrp);
        mrp = (List<SdTempMrp>) new FluentDao().selectFrom(SdTempMrp.class)
                .where(it -> it
                        .equal("pais", pais))
                .orderBy(Arrays.asList(new Ordenacao("desc", "status"), new Ordenacao("numero")))
                .resultList();
    }

    protected void getMrpProducao() {
        JPAUtils.clearEntitys(mrpProducao);
        mrpProducao = (List<SdMrpProduto>) new FluentDao().selectFrom(SdMrpProduto.class).get()
                .orderBy(Arrays.asList(new Ordenacao("desc", "status"), new Ordenacao("pedido")))
                .resultList();
    }

    private void getMrpResumido(String pais) {
        mrpHeaderResumo.clear();
        mrpHeaderResumo.addAll(Arrays.asList("pais", "grp", "codigo", "descricao", "cor", "desccor", "um", "sobra_estoque", "sobra_compra", "b_nao_conf"));
        List<Object> periodos = new FluentDao().runNativeQuery("" +
                "select distinct decode(periodo, 'M', '0000', 'S', '0001', periodo) ord,\n" +
                "                periodo,\n" +
                "                status_of\n" +
                "  from v_sd_temp_mrp\n" +
                " where status = 'T'\n" +
                "   and estoque = 'A'\n" +
                "   and pais = '" + pais + "'\n" +
                " order by 1");
        String sqlMrpResumo = "" +
                "select mrp.pais as PAIS,\n" +
                "       mrp.grp_comprador as GRP,\n" +
                "       mrp.insumo Codigo,\n" +
                "       mrp.desc_insumo as Descricao,\n" +
                "       mrp.cor_insumo as Cor,\n" +
                "       mrp.desc_cor_insumo as DescCor,\n" +
                "       mrp.unidade as UM,\n" +
                "       sum(case when (mrp.status = 'A' and mrp.estoque = 'C') then mrp.consumo else 0 end) Sobra_Estoque,\n" +
                "       sum(case when (mrp.status = 'A' and mrp.estoque = 'B') then mrp.consumo else 0 end)  Sobra_Compra,\n" +
                "       sum(case when mrp.b_nao_conf = 'S' then mrp.consumo else 0 end)  b_nao_conf,\n";
        for (Object periodo : periodos) {
            sqlMrpResumo += "sum(case when (mrp.status = 'T' and mrp.estoque = 'A' and mrp.periodo = '" + ((Object[]) periodo)[1] + "' and mrp.status_of = '" + ((Object[]) periodo)[2] + "') then mrp.consumo else 0 end) as " + ((Object[]) periodo)[2] + "_" + ((Object[]) periodo)[1] + ",\n";
            mrpHeaderResumo.add(((Object[]) periodo)[2] + "_" + ((Object[]) periodo)[1]);
        }
        sqlMrpResumo += "" +
                "       ent.codcli CodFor,\n" +
                "       ent.nome Fornecedor\n" +
                "  from sd_temp_mrp mrp\n" +
                "  join material_001 mat on mat.codigo = mrp.insumo\n" +
                "  left join entidade_001 ent on ent.codcli = mat.codcli\n" +
                " where mrp.pais = '" + pais + "'\n" +
                "group by mrp.grp_comprador,\n" +
                "         mrp.insumo,\n" +
                "         mrp.desc_insumo,\n" +
                "         mrp.cor_insumo,\n" +
                "          mrp.desc_cor_insumo,\n" +
                "         mrp.unidade,\n" +
                "         ent.codcli,\n" +
                "         ent.nome,\n" +
                "         mrp.pais\n" +
                "order by 1, 2,4";
        mrpHeaderResumo.addAll(Arrays.asList("CodFor", "Fornecedor"));
        mrpResumo = new FluentDao().runNativeQuery(sqlMrpResumo);
    }

    protected void criarExcelOrigemTab(String filePath, String comResumo, String pais) {
        try {
            if (comResumo.equals("S"))
                new ExportUtils().excel(filePath)
                        .addSheet("ORIGEM", SdTempMrp.class, mrp)
                        .addSheet("MRP", sheet -> {
                            getMrpResumido(pais);

                            Font headerFont = sheet.workbook.createFont();
                            headerFont.setBold(true);

                            Font dangerFont = sheet.workbook.createFont();
                            dangerFont.setColor(IndexedColors.WHITE.getIndex());
                            dangerFont.setBold(true);

                            Font blackFont = sheet.workbook.createFont();
                            blackFont.setColor(IndexedColors.YELLOW.getIndex());
                            blackFont.setBold(true);

                            // Create a CellStyle with the font
                            CellStyle headerCellStyle = sheet.workbook.createCellStyle();
                            headerCellStyle.setFont(headerFont);
                            headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

                            CellStyle dangerCellStyle = sheet.workbook.createCellStyle();
                            dangerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                            dangerCellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
                            dangerCellStyle.setAlignment(HorizontalAlignment.CENTER);
                            dangerCellStyle.setFont(dangerFont);

                            CellStyle blackCellStyle = sheet.workbook.createCellStyle();
                            blackCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                            blackCellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
                            blackCellStyle.setAlignment(HorizontalAlignment.CENTER);
                            blackCellStyle.setFont(blackFont);

                            // Create a Row
                            Row headerRow = sheet.sheet.createRow(1);

                            // Create cells
                            for (int i = 0; i < mrpHeaderResumo.size(); i++) {
                                Cell cell = headerRow.createCell(i);
                                cell.setCellValue(mrpHeaderResumo.get(i).toUpperCase());
                                cell.setCellStyle(headerCellStyle);
                            }

                            headerRow.setHeight(new Short("900"));

                            Row overTitleRow = sheet.sheet.createRow(0);

                            if (mrpHeaderResumo.contains("EP_M")) {
                                overTitleRow.createCell(mrpHeaderResumo.indexOf("EP_M")).setCellValue("IMEDIATO");
                                overTitleRow.getCell(mrpHeaderResumo.indexOf("EP_M")).setCellStyle(dangerCellStyle);

                            }

                            List<String> lotes = mrpHeaderResumo.stream().filter(it -> it.startsWith("PL") || it.startsWith("EP") && !it.equals("EP_M") && !it.equals("EP_PR")).map(it -> it.substring(3)).sorted().collect(Collectors.toList());
                            for (int i = 0; i < lotes.size(); i++) {
                                String stringLote = lotes.get(i);
                                TabPrz lote = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", stringLote)).singleResult();
                                Cell cell = overTitleRow.createCell((!mrpHeaderResumo.contains("EP_M") ? mrpHeaderResumo.indexOf("b_nao_conf") : mrpHeaderResumo.indexOf("EP_M") )+ 1 + i);
                                if (lote.getDtInicio().isBefore(LocalDate.now())) {
                                    cell.setCellValue("ATRASADO");
                                    cell.setCellStyle(dangerCellStyle);
                                } else {
                                    cell.setCellValue(StringUtils.toShortDateFormat(lote.getDtInicio()));
                                    cell.setCellStyle(blackCellStyle);
                                }
                            }

                            // Create Other rows and cells with employees data
                            int rowNum = 2;
                            for (Object classExport : mrpResumo) {
                                Row row = sheet.sheet.createRow(rowNum++);
                                Object[] dadosClassValue = (Object[]) classExport;
                                int columnIndex = 0;
                                for (Object column : dadosClassValue) {
                                    String columnValue = column == null ? "" : String.valueOf(column);
                                    if (columnIndex > 6 && columnIndex < mrpHeaderResumo.size() - 2)
                                        sheet.cellFormatValue(row, columnIndex, columnValue, "bigdecimal");
                                    else
                                        sheet.cellFormatValue(row, columnIndex, columnValue, "string");
                                    columnIndex++;
                                }
                            }
                        })
                        .export();
            else
                new ExportUtils().excel(filePath)
                        .<SdTempMrp>addSheet("ORIGEM", SdTempMrp.class, mrp)
                        .export();
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }
}
