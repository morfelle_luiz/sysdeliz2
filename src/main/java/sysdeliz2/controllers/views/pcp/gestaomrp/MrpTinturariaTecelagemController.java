package sysdeliz2.controllers.views.pcp.gestaomrp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaProducao;
import sysdeliz2.models.sysdeliz.pcp.SdProgramacaoBarca;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.ProgOf;
import sysdeliz2.models.ti.Tintur;
import sysdeliz2.models.ti.TinturIten;
import sysdeliz2.models.view.VSdMrpTinturaria;
import sysdeliz2.models.view.VSdOfsMrpTinturaria;
import sysdeliz2.models.view.mrptinturaria.*;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class MrpTinturariaTecelagemController {

    // job
    protected final StringProperty inicioUltimoLog = new SimpleStringProperty();
    protected final StringProperty tempoUltimoLog = new SimpleStringProperty();
    protected final StringProperty statusJobLog = new SimpleStringProperty();
    protected final StringProperty dataProximoLog = new SimpleStringProperty();

    // fio
    protected List<String> lotesFioMrp = new ArrayList<>();

    // tecelagem
    protected List<String> lotesTecelagemMrp = new ArrayList<>();

    // tinturaria
    protected List<String> lotesTinturariaMrp = new ArrayList<>();
    protected List<Cor> coresMateriaisTinturariaMrp = new ArrayList<>();

    public MrpTinturariaTecelagemController() {

    }

    // job

    /**
     * Run do JOB para MRP Tinturaria
     *
     * @throws SQLException
     */
    protected void runJob() throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                "BEGIN\n" +
                "  DBMS_SCHEDULER.RUN_JOB(\n" +
                "    JOB_NAME            => 'SD_MRP_TINTURARIA',\n" +
                "    USE_CURRENT_SESSION => FALSE);\n" +
                "END;");

        getStatusJob();
    }

    /**
     * Get do status do JOB do MRP
     *
     * @throws SQLException
     */
    protected void getStatusJob() throws SQLException {
        List<Object> dadosLog = new NativeDAO().runNativeQuery("" +
                "select decode(state,'SCHEDULED','PROGRAMADO','RUNNING','EM EXECUÇÃO','S/ ESTADO') estado,\n" +
                "       to_char(last_start_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') ult_rodada,\n" +
                "       last_run_duration tempo,\n" +
                "       to_char(next_run_date - 0.0416666666666667, 'DD/MM/YYYY HH24:MI:SS') prox_rodada\n" +
                "  from user_scheduler_jobs\n" +
                " where job_name = 'SD_MRP_TINTURARIA'");

        if (dadosLog.size() > 0) {
            Map infosLog = (Map) dadosLog.get(0);
            statusJobLog.set(infosLog.get("ESTADO").toString());
            inicioUltimoLog.set(infosLog.get("ULT_RODADA").toString());
            tempoUltimoLog.set(infosLog.get("TEMPO") != null ? infosLog.get("TEMPO").toString() : "");
            dataProximoLog.set(infosLog.get("PROX_RODADA").toString());
        }
    }

    // mrp

    /**
     * Get dos lotes com base no MRP para a aba FIO
     * Where fixo: grpcomprador = "10" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE)
     * Order By loteOf
     *
     * @param aComprarSomente
     */
    protected void getLotesFioMrp(Boolean aComprarSomente, Boolean dadosExplosao) {
        lotesFioMrp = (List<String>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .distinctColumn("loteof")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> aComprarSomente)
                        .equal("grpcomprador", "10"))
                .orderBy("loteof", OrderType.ASC)
                .resultList();
    }

    /**
     * Get dos lotes com base no MRP para a aba TECELAGEM
     * Where fixo: grpcomprador = "20" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE)
     * Order By loteOf
     *
     * @param aComprarSomente
     */
    protected void getLotesTecelagemMrp(Boolean aComprarSomente, Boolean dadosExplosao) {
        lotesTecelagemMrp = (List<String>) new FluentDao().selectFrom(VSdMrpTinturaria.class)
                .distinctColumn("loteOf")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> aComprarSomente)
                        .equal("grpcomprador", "20"))
                .resultList();
        lotesTecelagemMrp.sort(Comparator.naturalOrder());
    }

    /**
     * Get dos lotes com base no MRP para a aba TINTURARIA
     * Where fixo: grpcomprador = "40" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE)
     * Order By loteOf
     *
     * @param aComprarSomente
     */
    protected void getLotesTinturariaMrp(Boolean aComprarSomente, Boolean dadosExplosao, Object[] materiais) {
        lotesTinturariaMrp = (List<String>) new FluentDao().selectFrom(VSdMrpTinturaria.class)
                .distinctColumn("loteOf")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> aComprarSomente)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0)
                        .equal("grpcomprador", "40"))
                .resultList();
        lotesTinturariaMrp.sort(Comparator.naturalOrder());
    }

    /**
     * Get do MRP de um material selecionado nos lotes não selecionados
     *
     * @param material
     * @param cor
     * @param lotes
     * @return
     */
    protected List<VSdMrpTinturariaV> getOutrosLotesMaterialSelecionado(Boolean dadosExplosao, Boolean somenteFalta, String material, String cor, Object[] lotes) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "deposito", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .equal("insumo.codigo", material)
                        .equal("corinsumo.cor", cor)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get do MRP dos materiais de origem de uma material selecionado
     *
     * @param material
     * @param cor
     * @param lotes
     * @return
     */
    protected List<VSdMrpTinturariaH> getOrigemMaterialSelecionado(Boolean dadosExplosao, Object[] material, Object[] cor, Object[] lotes) {
        return (List<VSdMrpTinturariaH>) new FluentDao().selectFrom(VSdMrpTinturariaH.class)
                .agregate(Arrays.asList("loteof", "insumo", "corinsumo", "unidadeestq", "forinsumo"),
                        func -> func.sum("consestqestoque").sum("consestqcompra").sum("consestqfalta"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .isIn("insumo.codigo", material)
                        .isIn("corinsumo.cor", cor)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("insumo.codigo"),
                        new Ordenacao("loteof")))
                .resultList();
    }

    /**
     * Get das OFs do MRP de uma material/cor/depósito e os lotes selecionados em tela
     *
     * @param codigo
     * @param cor
     * @param lotes
     * @return List<VSdOfsMrpTinturaria>
     */
    protected List<VSdOfsMrpTinturaria> getOfsMaterialSelecionado(Boolean dadosExplosao, Boolean somenteFalta, String codigo, String cor, Boolean principal, Object[] lotes) {
        return (List<VSdOfsMrpTinturaria>) new FluentDao().selectFrom(VSdOfsMrpTinturaria.class)
                .where(eb -> eb
                        .equal("corinsumo", cor)
                        .equal("insumo", codigo)
                        .equal("principal", principal)
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidoDe", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("loteOf", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("numeroof.periodo"),
                        new Ordenacao("numeroof.numero"),
                        new Ordenacao("corproduto.cor")))
                .resultList();
    }

    /**
     * Get das sobras do material/cor no MRP
     *
     * @param material
     * @param cor
     * @return List<VSdMrpTinturariaAcumulado>
     */
    protected List<VSdMrpTinturariaV> getSobrasMaterialSelecionado(Boolean dadosExplosao, String material, String cor) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "deposito", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "A")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("corinsumo.cor", cor)
                        .equal("insumo.codigo", material))
                .orderBy(Arrays.asList(new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get dos materiais de composição de um material de consumo
     *
     * @param lotes
     * @param materiais
     * @param cores
     * @return List<VSdMrpTinturariaAcumulado>
     */
    protected List<VSdMrpTinturariaV> getNecessidadesMaterialSelecionado(Boolean dadosExplosao, Object[] lotes, Object[] materiais, Object[] cores) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("tipoinsumo", "insumo", "principal", "corinsumo", "contrastante", "emBarca", "unidade", "unidadeestq", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .isIn("loteof", lotes)
                        .isIn("corinsumo.cor", cores)
                        .isIn("insumo.codigo", materiais))
                .orderBy(Arrays.asList(new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get dos pais de um material
     *
     * @param material
     * @param cor
     * @return
     */
    protected List<VSdMrpTinturariaV> getMateriaisPai(Boolean dadosExplosao, String material, String cor) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .columns(Arrays.asList("materialpai", "cormaterialpai"))
                .distinct()
                .where(eb -> eb
                        .isNotNull("materialpai")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("insumo.codigo", material)
                        .equal("corinsumo.cor", cor))
                .resultList();
    }

    /**
     * Get dos filhos de um material
     *
     * @param material
     * @param cor
     * @return
     */
    protected List<VSdMrpTinturariaV> getMateriaisFilho(Boolean dadosExplosao, String material, String cor) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .columns(Arrays.asList("insumo", "corinsumo"))
                .distinct()
                .where(eb -> eb
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("materialpai", material)
                        .equal("cormaterialpai", cor))
                .resultList();
    }

    /**
     * Get da qtde de consumo com a lista de lotes e cores
     *
     * @param lotes
     * @param cores
     * @return
     */
    protected BigDecimal getSumQtdeConsumo(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] cores) {
        return (BigDecimal) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .sum("consumoestq")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("loteof", lotes)
                        .isIn("corinsumo.cor", cores))
                .singleResult();
    }

    /**
     * Get da qtde de consumo com a lista de lotes e cores
     *
     * @param lotes
     * @param cores
     * @param materiais
     * @return
     */
    protected BigDecimal getSumQtdeConsumo(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] cores, Object[] materiais, Object[] fornecedor) {
        return (BigDecimal) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .sum("consumoestq")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("loteof", lotes)
                        .isIn("forinsumo.codcli", fornecedor, TipoExpressao.AND, b -> fornecedor.length > 0)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0)
                        .isIn("corinsumo.cor", cores, TipoExpressao.AND, b -> cores.length > 0))
                .singleResult();
    }

    /**
     * Get da qtde de consumo com a lista de lotes e cores
     *
     * @param lotes
     * @param cores
     * @return
     */
    protected String getUnidadeEstqConsumo(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] cores, Object[] materiais) {
        return (String) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .max("unidadeestq")
                .where(eb -> eb.equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .isIn("loteof", lotes)
                        .isIn("corinsumo.cor", cores)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0))
                .singleResult();
    }

    /**
     * Get dos materiais principais de um material não principal
     *
     * @param materiais
     * @param cores
     * @param somenteFalta
     * @return
     */
    private Object[] materialPrincipal(Object[] materiais, Object[] cores, Boolean somenteFalta, Boolean dadosExplosao) {
        List<VSdMrpTinturariaV> materiaisMrp = (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .columns(Arrays.asList("insumo", "principal"))
                .distinct()
                .where(eb -> eb
                        .isIn("insumo.codigo", materiais)
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao))
                .resultList();
        Set<Object> materiaisPrincipal = materiaisMrp.stream()
                .filter(mat -> mat.isPrincipal())
                .map(mat -> mat.getInsumo().getCodigo())
                .distinct()
                .collect(Collectors.toSet());
        List<Object> materiaisNaoPrincipal = Arrays.stream(materiais)
                .filter(material -> Arrays.stream(materiaisPrincipal.toArray()).noneMatch(matPrincipal -> matPrincipal.equals(material)))
                .distinct()
                .collect(Collectors.toList());
        materiaisNaoPrincipal.forEach(material -> {
            List<String> ofMateriais = (List<String>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("numeroof")
                    .where(eb -> eb
                            .isNotNull("numeroof")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .equal("insumo.codigo", material)
                            .isIn("corinsumo.cor", cores))
                    .resultList();
            // get dos materiais que estão nas OFs do material filtrado
            List<Object> materiaisOfs = (List<Object>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("insumo.codigo")
                    .where(eb -> eb
                            .isNotNull("insumo")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .equal("principal", true)
                            .isIn("numeroof", ofMateriais.toArray())
                            .isIn("corinsumo.cor", cores)
                            .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                            .equal("statusmrp", "T"))
                    .resultList();
            materiaisPrincipal.addAll(materiaisOfs);
        });

        return materiaisPrincipal.toArray();
    }

    /**
     * Get do consumo estoque do material e cor dos lotes
     *
     * @param somenteComprar
     * @param cores
     * @param lotes
     * @param materiais
     * @return
     */
    protected BigDecimal getSaldoLotesMaterialCor(Boolean dadosExplosao, boolean somenteComprar, Object[] cores, Object[] lotes, Object[] materiais) {
        BigDecimal result = new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .sum("consumoestq")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteComprar)
                        .isIn("loteof", lotes)
                        .isIn("insumo.codigo", materiais)
                        .isIn("corinsumo.cor", cores))
                .singleResult();
        return result != null ? result : BigDecimal.ZERO;
    }

    // fio

    /**
     * Get dos materiais do grupo 10 no MRP com base nos lotes selecionados
     * Where fixo: grpcomprador = "10" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE) e loteOf = [Lotes Selecionados]
     * Order By consumidode, insumo.codigo
     *
     * @param somenteFalta
     * @param lotes
     */
    protected List<VSdMrpTinturariaV> getMateriaisGrupo10(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] materiais, Object[] cores) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "deposito", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("grpcomprador", "10")
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0)
                        .isIn("corinsumo.cor", cores, TipoExpressao.AND, b -> cores.length > 0)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    // tecelagem

    /**
     * Get dos materiais do grupo 20 no MRP com base nos lotes selecionados
     * Where fixo: grpcomprador = "20" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE) e loteOf = [Lotes Selecionados]
     * Order By consumidode, insumo.codigo
     *
     * @param somenteFalta
     * @param lotes
     */
    protected List<VSdMrpTinturariaV> getMateriaisGrupo20(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] materiais, Object[] cores) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "deposito", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("grpcomprador", "20")
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0)
                        .isIn("corinsumo.cor", cores, TipoExpressao.AND, b -> cores.length > 0)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    // tinturaria

    /**
     * Get das cores de materiais do MRP a partir dos lotes
     *
     * @param somenteFalta
     * @param lotes
     */
    protected void getCoresMateriaisTinturariaMrp(Boolean dadosExplosao, Boolean somenteFalta, Object[] lotes, Object[] materiais) {
        coresMateriaisTinturariaMrp = (List<Cor>) new FluentDao().selectFrom(VSdMrpTinturaria.class)
                .distinctColumn("corinsumo")
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("loteOf", lotes)
                        .isIn("insumo.codigo", materiais, TipoExpressao.AND, b -> materiais.length > 0))
                .resultList();
        coresMateriaisTinturariaMrp.sort(Comparator.comparing(Cor::getCor));
    }

    /**
     * Get dos materiais do grupo 40 no MRP com base nas cores e lotes selecionados no filtro
     * Where fixo: grpcomprador = "40" e statusmrp = "T"
     * variável: consumidode = "A" (caso parâmetro TRUE), loteOf = [Lotes Selecionados] e corinsumo.cor = [cores selecionadas]
     * Order By consumidode, insumo.codigo
     *
     * @param somenteFalta
     * @param corMaterial
     * @param lotes
     */
    protected List<VSdMrpTinturariaV> getMateriaisGrupo40(Boolean dadosExplosao, Boolean somenteFalta, Object[] corMaterial, Object[] lotes, Object[] materiais, Object[] fornecedor, Object[] deposito) {
        if (materiais.length > 0) {
            // get das OFs que comsomem os materiais filtrados
            Object[] finalMateriais = materialPrincipal(materiais, corMaterial, somenteFalta, dadosExplosao);
            List<String> ofMateriais = (List<String>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("numeroof")
                    .where(eb -> eb
                            .isNotNull("numeroof")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("insumo.codigo", finalMateriais)
                            .isIn("corinsumo.cor", corMaterial))
                    .resultList();
            // get dos materiais que estão nas OFs do material filtrado
            List<Object> materiaisOfs = (List<Object>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("insumo.codigo")
                    .where(eb -> eb
                            .isNotNull("insumo")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("numeroof", ofMateriais.toArray())
                            .isIn("corinsumo.cor", corMaterial)
                            .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                            .equal("statusmrp", "T"))
                    .resultList();
            materiais = materiaisOfs.toArray();
        }

        Object[] finalMateriais = materiais;
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "deposito", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("insumo.codigo", finalMateriais, TipoExpressao.AND, b -> finalMateriais.length > 0)
                        .isIn("forinsumo.codcli", fornecedor, TipoExpressao.AND, b -> fornecedor.length > 0)
                        .isIn("deposito", deposito, TipoExpressao.AND, b -> deposito.length > 0)
                        .isIn("corinsumo.cor", corMaterial)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    protected List<VSdMrpTinturariaV> getMateriaisGrupo40SemDeposito(Boolean dadosExplosao, Boolean somenteFalta, Object[] corMaterial, Object[] lotes, Object[] materiais) {
        Object[] numerosOfs = new Object[]{};
        if (materiais.length > 0) {
            // get das OFs que comsomem os materiais filtrados
            Object[] finalMateriais = materialPrincipal(materiais, corMaterial, somenteFalta, dadosExplosao);
            List<String> ofMateriais = (List<String>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("numeroof")
                    .where(eb -> eb
                            .isNotNull("numeroof")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("insumo.codigo", finalMateriais)
                            .isIn("corinsumo.cor", corMaterial))
                    .resultList();
            numerosOfs = ofMateriais.toArray();
            // get dos materiais que estão nas OFs do material filtrado
            List<Object> materiaisOfs = (List<Object>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("insumo.codigo")
                    .where(eb -> eb
                            .isNotNull("insumo")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("numeroof", ofMateriais.toArray())
                            .isIn("corinsumo.cor", corMaterial)
                            .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                            .equal("statusmrp", "T"))
                    .resultList();
            materiais = materiaisOfs.toArray();
        }

        Object[] finalMateriais = materiais;
        Object[] finalNumerosOfs = numerosOfs;
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade", "unidadeestq", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("insumo.codigo", finalMateriais, TipoExpressao.AND, b -> finalMateriais.length > 0)
                        .isIn("corinsumo.cor", corMaterial)
                        .isIn("numeroof", finalNumerosOfs, TipoExpressao.AND, b -> finalNumerosOfs.length > 0)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get dos materiais com base nas OFs do MRP e cor
     * Sem depósito no group by
     * Somente grupo 40
     *
     * @param somenteFalta
     * @param corMaterial
     * @param ofsMateriais
     * @return
     */
    protected List<VSdMrpTinturariaV> getMateriaisGrupo40SemDeposito(Boolean dadosExplosao, Boolean somenteFalta, Object[] corMaterial, Object[] ofsMateriais) {
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .agregate(Arrays.asList("insumo", "principal", "corinsumo", "contrastante", "emBarca", "tipoinsumo", "unidade",
                        "unidadeestq", "ordcompra", "dtentrega", "forinsumo", "consumidode"),
                        func -> func.sum("consumido").sum("consumoestq").sum("consumoof"))
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("corinsumo.cor", corMaterial)
                        .isIn("numeroof", ofsMateriais))
                .orderBy(Arrays.asList(
                        new Ordenacao("consumidode"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    protected List<VSdMrpTinturariaV> getMateriaisMrpGrupo40(Boolean dadosExplosao, Boolean somenteFalta, Object[] corMaterial, Object[] lotes, Object[] materiais) {
        Object[] numerosOfs = new Object[]{};
        if (materiais.length > 0) {
            // get das OFs que comsomem os materiais filtrados
            Object[] finalMateriais = materialPrincipal(materiais, corMaterial, somenteFalta, dadosExplosao);
            List<String> ofMateriais = (List<String>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("numeroof")
                    .where(eb -> eb
                            .isNotNull("numeroof")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("insumo.codigo", finalMateriais)
                            .isIn("corinsumo.cor", corMaterial))
                    .resultList();
            numerosOfs = ofMateriais.toArray();
            // get dos materiais que estão nas OFs do material filtrado
            List<Object> materiaisOfs = (List<Object>) new FluentDao().selectFrom(VSdMrpTinturariaV.class).distinctColumn("insumo.codigo")
                    .where(eb -> eb
                            .isNotNull("insumo")
                            .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                            .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                            .isIn("numeroof", ofMateriais.toArray())
                            .isIn("corinsumo.cor", corMaterial)
                            .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                            .equal("statusmrp", "T"))
                    .resultList();
            materiais = materiaisOfs.toArray();
        }

        Object[] finalMateriais = materiais;
        Object[] finalNumerosOfs = numerosOfs;
        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("insumo.codigo", finalMateriais, TipoExpressao.AND, b -> finalMateriais.length > 0)
                        .isIn("numeroof", finalNumerosOfs, TipoExpressao.AND, b -> finalNumerosOfs.length > 0)
                        .isIn("corinsumo.cor", corMaterial)
                        .isIn("loteof", lotes))
                .orderBy(Arrays.asList(
                        new Ordenacao("numeroof"),
                        new Ordenacao("loteof"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get completo dos materiais do MRP para as OFs e cores selecionadas
     * Somente grupo 40
     *
     * @param somenteFalta
     * @param corMaterial
     * @param ofsMateriais
     * @return
     */
    protected List<VSdMrpTinturariaV> getMateriaisMrpGrupo40(Boolean dadosExplosao, Boolean somenteFalta, Object[] corMaterial, Object[] ofsMateriais) {

        return (List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .where(eb -> eb
                        .equal("statusmrp", "T")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .equal("grpcomprador", "40")
                        .equal("consumidode", "A", TipoExpressao.AND, b -> somenteFalta)
                        .isIn("numeroof", ofsMateriais)
                        .isIn("corinsumo.cor", corMaterial))
                .orderBy(Arrays.asList(
                        new Ordenacao("numeroof"),
                        new Ordenacao("loteof"),
                        new Ordenacao("insumo.codigo")))
                .resultList();
    }

    /**
     * Get das OFs dos materiais selecionados para a barca
     *
     * @param materiais
     * @param cor
     * @param periodos
     * @return
     */
    protected Set<String> getOFsMateriaisBarca(Boolean dadosExplosao, Object[] materiais, Object cor, Object[] periodos, Object[] depositos) {
        return ((List<VSdMrpTinturariaV>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                .where(eb -> eb
                        .equal("consumidode", "A")
                        .equal("origemMrp", "M", TipoExpressao.AND, b -> !dadosExplosao)
                        .equal("origemMrp", "E", TipoExpressao.AND, b -> dadosExplosao)
                        .isIn("loteof", periodos)
                        .isIn("deposito", depositos, TipoExpressao.AND, b -> depositos.length > 0)
                        .equal("corinsumo.cor", cor)
                        .isIn("insumo.codigo", materiais))
                .resultList()).stream()
                .map(VSdMrpTinturariaV::getNumeroof)
                .distinct()
                .collect(Collectors.toSet());
    }

    // programacao
    protected void persistirProgramacaoBarcas(SdProgramacaoBarca programacao) throws SQLException {
        new FluentDao().persist(programacao);
    }

    protected List<Object> getProgramadoContrastante(String loteProgramacao, String numeroOf) throws SQLException {

        return new NativeDAO().runNativeQuery("" +
                        "with consumos as\n" +
                        " (select itens.principal,\n" +
                        "         itens.combinado,\n" +
                        "         pcp.aplicacao,\n" +
                        "         itens.qtde,\n" +
                        "         pcp.consumo,\n" +
                        "         mat.ind_tin,\n" +
                        "         pcp.consumo / sum(pcp.consumo) over(partition by prog.lote, itens.numero, itens.insumo, itens.principal, itens.combinado) prop_aplicacao,\n" +
                        "         round(((itens.qtde * (1 - (mat.ind_tin / 100))) *\n" +
                        "               (pcp.consumo / sum(pcp.consumo)\n" +
                        "                over(partition by prog.lote,\n" +
                        "                      itens.numero,\n" +
                        "                      itens.insumo,\n" +
                        "                      itens.principal,\n" +
                        "                      itens.combinado))) / pcp.consumo,\n" +
                        "               0) qtde_grupo\n" +
                        "    from sd_programacao_barca_001 prog\n" +
                        "    join sd_barca_producao_001 barca\n" +
                        "      on barca.programacao = prog.programacao\n" +
                        "    join sd_itens_barca_001 itens\n" +
                        "      on itens.programacao = barca.programacao\n" +
                        "     and itens.ordem = barca.ordem\n" +
                        "    join pcpftof_001 pcp\n" +
                        "      on pcp.numero = itens.numero\n" +
                        "     and pcp.cor = itens.cor_produto\n" +
                        "     and pcp.insumo = itens.insumo\n" +
                        "     and pcp.cor_i = itens.cor_insumo\n" +
                        "    join material_001 mat\n" +
                        "      on mat.codigo = itens.insumo\n" +
                        "    join tablin_001 comb\n" +
                        "      on comb.codigo = itens.combinado\n" +
                        "   where prog.lote = '%s'\n" +
                        "     and barca.status = 'P'\n" +
                        "     and itens.numero = '%s')\n" +
                        "select principal, combinado, aplicacao, sum(qtde_grupo) qtde\n" +
                        "  from consumos\n" +
                        " group by principal, combinado, aplicacao",
                loteProgramacao,
                numeroOf);

    }

    protected String getLoteProgramacaoPendente() throws SQLException {
        List<Object> lotes = new NativeDAO().runNativeQuery("select distinct prog.lote\n" +
                "  from sd_programacao_barca_001 prog\n" +
                "  join sd_barca_producao_001 barca\n" +
                "    on barca.programacao = prog.programacao\n" +
                " where barca.status = 'P'");

        if (lotes.size() == 0) {
            return null;
        } else {
            return ((Map<String, Object>) lotes.get(0)).get("LOTE").toString();
        }
    }

    protected void deleteProgramacaoPendente() throws SQLException {
        new NativeDAO().runNativeQueryUpdate("delete from sd_itens_barca_001 itens\n" +
                " where itens.programacao in\n" +
                "       (select barca.programacao\n" +
                "          from sd_barca_producao_001 barca\n" +
                "         where barca.status = 'P')\n" +
                "   and itens.ordem in (select barca.ordem\n" +
                "                         from sd_barca_producao_001 barca\n" +
                "                        where barca.status = 'P')");
        new NativeDAO().runNativeQueryUpdate("delete from sd_barca_producao_001 barca where barca.status = 'P'");
        new NativeDAO().runNativeQueryUpdate("delete from sd_programacao_barca_001 prog\n" +
                " where (select count(*)\n" +
                "          from sd_barca_producao_001 barca\n" +
                "         where barca.programacao = prog.programacao) = 0");
    }

    protected void firmarBarca(SdBarcaProducao barca, LocalDate dataEnvio) {
        barca.getItens().forEach(it -> {
            it.setFirmadoEm(null);
            new FluentDao().merge(it);
        });
        barca.setDtenvio(dataEnvio);
        barca.setDtretorno(dataEnvio.plusDays(barca.getMaquina().getPrazo()));
        barca.setStatus("F");
        new FluentDao().merge(barca);
        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.EDITAR, String.valueOf(barca.getProgramacao().getProgramacao()),
                "Firmando barca #" + barca.getOrdem() + " na programação");

    }

    protected void cancelarProgramacaoBarca(SdBarcaProducao barca) {
        barca.getItens().forEach(it -> {
            it.setFirmadoEm(null);
            new FluentDao().merge(it);
        });
        barca.setDtenvio(null);
        barca.setDtretorno(null);
        barca.setStatus("P");
        new FluentDao().merge(barca);
        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.EDITAR, String.valueOf(barca.getProgramacao().getProgramacao()),
                "Cancelando programação de envia da barca #" + barca.getOrdem());
    }

    protected void excluirBarca(SdBarcaProducao barca) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("delete from sd_itens_barca_001 where programacao = %s and ordem = %s",
                barca.getProgramacao().getProgramacao(), barca.getOrdem());
        new NativeDAO().runNativeQueryUpdate("delete from sd_barca_producao_001 where programacao = %s and ordem = %s",
                barca.getProgramacao().getProgramacao(), barca.getOrdem());
        new NativeDAO().runNativeQueryUpdate("delete from sd_programacao_barca_001 prog " +
                        "where prog.programacao = %s" +
                        "  and (select count(*) from sd_barca_producao_001 barc where barc.programacao = prog.programacao) = 0",
                barca.getProgramacao().getProgramacao());

        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.EXCLUIR, String.valueOf(barca.getProgramacao().getProgramacao()),
                "Excluindo barca #" + barca.getOrdem() + " da programação");
    }

    // excia
    protected void criarOrdemTinturaria(SdBarcaProducao barca) throws SQLException {
        SdBarcaProducao finalBarca = barca;
        VTintur tintur = new FluentDao().selectFrom(VTintur.class)
                .where(eb -> eb
                        .equal("programacao", finalBarca.getProgramacao().getProgramacao())
                        .equal("barca", finalBarca.getOrdem()))
                .singleResult();
        Tintur novaOtn = new FluentDao().persist(new Tintur(tintur.getDats(), tintur.getDatr(), tintur.getCliente(), tintur.getPedido(),
                tintur.getFicha(), tintur.getCodcli(), tintur.getPeriodo(), tintur.getTinturaria(), tintur.getObservacao(),
                tintur.getDatabaixa(), tintur.getReproc(), tintur.getMotivo(), tintur.getTipo()));
        String numeroOtn = novaOtn.getNumero();
        List<VTinturIten> itens = (List<VTinturIten>) new FluentDao().selectFrom(VTinturIten.class)
                .where(eb -> eb
                        .equal("programacao", finalBarca.getProgramacao().getProgramacao())
                        .equal("barca", finalBarca.getOrdem()))
                .resultList();
        for (VTinturIten item : itens)
            new FluentDao().persist(new TinturIten(item.getCodigo2(), item.getCodigo(), numeroOtn, item.getGramatura(), item.getEstampa(),
                    item.getCor(), item.getLargura(), item.getRolos(), item.getPesobruto(), item.getPesoliquido(), item.getPesoretorno(),
                    item.getSituacao(), item.getFluxo(), item.getCort(), item.getCusto(), item.getCors(), item.getDeposito(),
                    item.getPecas(), item.getPreco(), item.getOrdem(), item.getLote(), item.getPesobaixado(), item.getObs(),
                    item.getCustofluxo(), item.getDtretorno(), item.getNfcob(), item.getDepretorno()));

        List<VProgOf> ofs = (List<VProgOf>) new FluentDao().selectFrom(VProgOf.class)
                .where(eb -> eb
                        .equal("programacao", finalBarca.getProgramacao().getProgramacao())
                        .equal("barca", finalBarca.getOrdem()))
                .resultList();
        for (VProgOf of : ofs)
            new FluentDao().persist(new ProgOf(of.getTipo(), numeroOtn, of.getNumero(),
                    of.getPartida(), of.getQtde(), of.getCodigo(), of.getCor()));

        barca.setNumero(numeroOtn);
        barca.setStatus("A");
        barca = new FluentDao().merge(barca);

        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.CADASTRAR, numeroOtn,
                "Cadastrando OTN para a barca #" + barca.getOrdem() + " na programação "+barca.getProgramacao().getProgramacao());
    }

    protected SdBarcaProducao removerOtnBarca(SdBarcaProducao barca) {
        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.EDITAR, String.valueOf(barca.getProgramacao().getProgramacao()),
                "Desvinculando número de OTN "+barca.getNumero()+" da barca #" + barca.getOrdem() + " na programação");
        barca.setNumero(null);
        barca.setStatus("F");
        return new FluentDao().merge(barca);
    }

    protected SdBarcaProducao marcarBarcaConcluida(SdBarcaProducao barca) {
        barca.setStatus("C");
        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.EDITAR, String.valueOf(barca.getProgramacao().getProgramacao()),
                "Marcando barca #" + barca.getOrdem() + " como finalizada");
        return new FluentDao().merge(barca);
    }
}
