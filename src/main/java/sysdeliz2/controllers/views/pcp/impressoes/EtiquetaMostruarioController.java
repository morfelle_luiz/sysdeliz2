package sysdeliz2.controllers.views.pcp.impressoes;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EtiquetaMostruarioController extends WindowBase {
    
    protected List<VSdDadosProduto> filteredProdutos = new ArrayList<>();
    
    public EtiquetaMostruarioController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void getFilteredProdutos(List<Produto> codigo, List<Colecao> colecao, List<Marca> marca, List<Linha> linha) {
        Object[] codigos = codigo == null ? new Object[]{} : codigo.stream().map(Produto::getCodigo).toArray();
        Object[] colecoes = colecao == null ? new Object[]{} : colecao.stream().map(Colecao::getCodigo).toArray();
        Object[] marcas = marca == null ? new Object[]{} : marca.stream().map(Marca::getCodigo).toArray();
        Object[] linhas = linha == null ? new Object[]{} : linha.stream().map(Linha::getCodigo).toArray();
        
        filteredProdutos = (List<VSdDadosProduto>) new FluentDao().selectFrom(VSdDadosProduto.class)
                .where(it -> it
                        .isIn("codigo", codigos, TipoExpressao.AND, b -> codigos.length > 0)
                        .isIn("codCol", colecoes, TipoExpressao.AND, b -> colecoes.length > 0)
                        .isIn("codMarca", marcas, TipoExpressao.AND, b -> marcas.length > 0)
                        .isIn("codlin", linhas, TipoExpressao.AND, b -> linhas.length > 0))
                .resultList();
    }
    
    protected List<SelectedCores> getCoresFilteredProduto(String codigoProduto) throws SQLException {
        List<SelectedCores> coresSelecao = new ArrayList<>();
        
        List<Object> cores = new NativeDAO().runNativeQuery(String.format("" +
                "select *\n" +
                "  from (select cores.cor ||\n" +
                "               (select distinct ' [M]'\n" +
                "                  from v_sd_dados_produto p\n" +
                "                 where p.codigo in\n" +
                "                       (select * from table(split2('%s')))\n" +
                "                   and p.cormost = cores.cor) cor,\n" +
                "               cores.descricao,\n" +
                "               dense_rank() over(order by nvl((select sum(0)\n" +
                "                                                 from v_sd_dados_produto p\n" +
                "                                                where p.codigo in\n" +
                "                                                      (select *\n" +
                "                                                         from table(split2('%s')))\n" +
                "                                                  and p.cormost = cores.cor), 1), cor) seq,\n" +
                "               listagg(substr(codigo, 6, 2)) within group(order by cor) versao\n" +
                "          from (select prd.codigo, prd.cor, cor.descricao\n" +
                "                  from vproduto_cortam prd\n" +
                "                  join cadcor_001 cor\n" +
                "                    on cor.cor = prd.cor\n" +
                "                 where prd.codigo in\n" +
                "                       (select * from table(split2('%s')))\n" +
                "                 group by prd.cor, cor.descricao, prd.codigo\n" +
                "                 order by 2) cores\n" +
                "         group by cor, descricao)\n", codigoProduto, codigoProduto, codigoProduto));
        
        cores.forEach(row -> {
            Map<String, Object> cor = (Map<String, Object>) row;
            coresSelecao.add(new SelectedCores(
                    (String) cor.get("COR"),
                    (String) cor.get("DESCRICAO"),
                    ((BigDecimal) cor.get("SEQ")).intValue(),
                    (String)  cor.get("VERSAO")));
        });
        
        return coresSelecao;
    }
    
    protected Object getDadosCodigo(String codigo) throws SQLException {
        List<Object> dados = new ArrayList<>();
        
        dados = new NativeDAO().runNativeQuery(String.format("" +
                "select prd.codigo,\n" +
                "       prd.descricao,\n" +
                "       mat.composicao,\n" +
                "       gch.descricao2 apelo,\n" +
                "       (select listagg(fai.tamanho, ' ') within group(order by fai.posicao)\n" +
                "          from faixa_iten_001 fai\n" +
                "         where fai.faixa = prd.faixa) grade,\n" +
                "       (select listagg((fai.posicao - 1) +\n" +
                "                       decode(gch.preco_com, 0, null, gch.preco_com),\n" +
                "                       ' ') within group(order by fai.posicao)\n" +
                "          from faixa_iten_001 fai\n" +
                "         where fai.faixa = prd.faixa) gancho\n" +
                "  from v_sd_dados_produto prd\n" +
                "  join material_001 mat\n" +
                "    on mat.codigo = prd.TecidoPrincial\n" +
                "  join produto_001 gch\n" +
                "    on gch.codigo = prd.codigo\n" +
                " where prd.codigo = '%s'", codigo));
        return dados.size() == 0 ? null : dados.get(0);
    }
    
    protected class SelectedProduto {
    
        public final ObjectProperty<VSdDadosProduto> produto = new SimpleObjectProperty<>();
        public final ObjectProperty<Produto> produtoVersao = new SimpleObjectProperty<>();
        public final ListProperty<SelectedCores> cores = new SimpleListProperty<>(FXCollections.observableArrayList());
    
        public SelectedProduto(VSdDadosProduto produto) {
            this.produto.set(produto);
        }
    }
    
    protected class SelectedCores extends BasicModel {
    
        public final StringProperty cor = new SimpleStringProperty();
        public final StringProperty descricao = new SimpleStringProperty();
        public final IntegerProperty sequencial = new SimpleIntegerProperty();
        public final StringProperty versao = new SimpleStringProperty();
    
        public SelectedCores(String cor, String descricao, Integer sequencial, String versao) {
            this.cor.set(cor);
            this.descricao.set(descricao);
            this.sequencial.set(sequencial);
            this.versao.set(versao);
        }
    }
}
