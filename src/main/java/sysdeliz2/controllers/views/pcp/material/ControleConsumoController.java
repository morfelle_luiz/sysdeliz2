package sysdeliz2.controllers.views.pcp.material;

import javafx.beans.property.ListProperty;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdConsumoProd;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ControleConsumoController extends WindowBase {

    protected List<SdConsumoProd> consumos = new ArrayList<>();

    public ControleConsumoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected Material getMaterial(String valueSafe) {
        return new FluentDao().selectFrom(Material.class).where(it -> it.equal("codigo", valueSafe)).singleResult();
    }

    protected void definirMaterialConsumo(SdConsumoProd consumoProd, FormFieldText field, ListProperty<SdConsumoProd> listConsumoProd) {
        Material material = getMaterial(field.value.getValueSafe());
        if (material == null) {
            if (consumoProd.getMaterial() != null) {
                try {
                    new FluentDao().delete(getConsumoProd(consumoProd));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                consumoProd.setMaterial(null);
                MessageBox.create(message -> {
                    message.message("Material removido");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }

            field.clear();
            return;
        }

        if (verificaMaterialAplicacaoRepetido(listConsumoProd, material, consumoProd.getAplicacao(), consumoProd)) {
            consumoProd.setMaterial(null);
            field.clear();
            return;
        }

        if (consumoProd.getMaterial() != null && consumoProd.getMaterial().getCodigo().equals(material.getCodigo()))
            return;

        try {
            SdConsumoProd finalConsumoProd1 = consumoProd;
            SdConsumoProd oldConsumo = new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it
                    .equal("codigo.codigo", finalConsumoProd1.getCodigo().getCodigo())
                    .equal("material.codigo", finalConsumoProd1.getMaterial().getCodigo())
                    .equal("aplicacao.codigo", finalConsumoProd1.getAplicacao().getCodigo())
            ).singleResult();
            new FluentDao().delete(oldConsumo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        consumoProd.setUnidade(material.getUnicom());
        consumoProd.setMaterial(material);
        consumoProd.setLargmod(material.getLargura());
        consumoProd.setGrammod(material.getGramatura().divide(new BigDecimal("1000"), 4, RoundingMode.CEILING));

        if (consumoProd.getMaterial() != null && consumoProd.getAplicacao() != null) {
            consumoProd = new FluentDao().merge(consumoProd);
            MessageBox.create(message -> {
                message.message("Material alterado com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    protected void definirAplicacaoConsumo(SdConsumoProd consumoProd, FormFieldSingleFind field, ListProperty<SdConsumoProd> listConsumoProd) {
        Pcpapl aplicacao = (Pcpapl) field.value.getValue();
        if (aplicacao == null) {
            if (consumoProd.getAplicacao() != null) {
                try {
                    new FluentDao().delete(getConsumoProd(consumoProd));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MessageBox.create(message -> {
                    message.message("Aplicação removida");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                consumoProd.setAplicacao(null);
            }
            return;
        }

        if (verificaMaterialAplicacaoRepetido(listConsumoProd, consumoProd.getMaterial(), aplicacao, consumoProd)) {
            field.clear();
            consumoProd.setAplicacao(null);
            return;
        }

        if (consumoProd.getAplicacao() != null && consumoProd.getAplicacao().getCodigo().equals(aplicacao.getCodigo()))
            return;

        try {
            SdConsumoProd finalConsumoProd1 = consumoProd;
            SdConsumoProd oldConsumo = new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it
                    .equal("codigo.codigo", finalConsumoProd1.getCodigo().getCodigo())
                    .equal("material.codigo", finalConsumoProd1.getMaterial().getCodigo())
                    .equal("aplicacao.codigo", finalConsumoProd1.getAplicacao().getCodigo())
            ).singleResult();
            new FluentDao().delete(oldConsumo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        consumoProd.setAplicacao(aplicacao);

        if (consumoProd.getMaterial() != null && consumoProd.getAplicacao() != null) {
            consumoProd = new FluentDao().merge(consumoProd);
            MessageBox.create(message -> {
                message.message("Aplicação alterada com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private SdConsumoProd getConsumoProd(SdConsumoProd consumoProd) {
        return new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it
                .equal("codigo.codigo", consumoProd.getCodigo().getCodigo())
                .equal("material.codigo", consumoProd.getMaterial().getCodigo())
                .equal("aplicacao.codigo", consumoProd.getAplicacao().getCodigo())
        ).singleResult();
    }

    private boolean verificaMaterialAplicacaoRepetido(ListProperty<SdConsumoProd> listConsumoProd, Material material, Pcpapl aplicacao, SdConsumoProd consumoProd) {
        if (
                listConsumoProd.stream().anyMatch(it -> it
                        .getMaterial() != null &&
                        material != null &&
                        it.getMaterial().getCodigo().equals(material.getCodigo()) &&
                        it.getAplicacao() != null &&
                        aplicacao != null &&
                        it.getAplicacao().getCodigo().equals(aplicacao.getCodigo()) &&
                        !it.equals(consumoProd)
                )
        ) {
            MessageBox.create(message -> {
                message.message("Material/Aplicação já pertence a esse produto");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return true;
        }
        return false;
    }


    protected void buscarProdutos(Object[] produtos, Object[] marcas, Object[] colecoes) {
        if (consumos != null) JPAUtils.clearEntitys(consumos);
        consumos = (List<SdConsumoProd>) new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it
                .isIn("codigo.codigo", produtos, TipoExpressao.AND, when -> produtos.length > 0)
                .isIn("codigo.marca.codigo", marcas, TipoExpressao.AND, when -> marcas.length > 0)
                .isIn("codigo.colecao.codigo", colecoes, TipoExpressao.AND, when -> colecoes.length > 0)
        ).resultList();
    }


    @Override
    public void closeWindow() {

    }
}
