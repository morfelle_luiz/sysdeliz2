package sysdeliz2.controllers.views.pcp.material;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.view.VSdMateriaisMalha;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PrecificadorMalhaController extends WindowBase {

    protected List<VSdMateriaisMalha> materiais = new ArrayList<>();
    protected List<VSdMateriaisMalha> fios = new ArrayList<>();
    protected List<VSdMateriaisMalha> filhos = new ArrayList<>();
    protected ListProperty<VSdMateriaisMalha> filhosBean = new SimpleListProperty<>();
    protected ListProperty<VSdMateriaisMalha> fiosBean = new SimpleListProperty<>();

    public PrecificadorMalhaController(String title, Image icone) {
        super(title, icone);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void consultarMaterial(Material codigo) {
        JPAUtils.clearEntitys(materiais);
        String pCodigo = codigo == null ? "" : codigo.getCodigo();
        materiais = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                        .equal("codigoretorno.codigo", pCodigo)
        ).resultList();
    }

    protected void getFilho(VSdMateriaisMalha codigo) {
        JPAUtils.clearEntitys(filhos);
        String pCodigo = codigo.getCodigo() == null ? "" : codigo.getCodigo().getCodigo();
        String pCodigoFio = codigo.getFio() == null ? "" : codigo.getFio().getCodigo();
        String pCodFor = codigo.getCodfor() == null ? "" : String.format("%05d",codigo.getCodfor().getCodcli());
        String pCorFor = codigo.getCorfor() == null ? "" : codigo.getCorfor();

        filhos = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                .equal("codigoretorno.codigo", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                .equal("codigoretorno.codigo", pCodigoFio, TipoExpressao.AND, b -> !pCodigoFio.equals(""))
                .equal("codfor.codcli", pCodFor, TipoExpressao.AND, b -> !pCodFor.equals(""))
                .equal("corfor", pCorFor, TipoExpressao.AND, b -> !pCorFor.equals(""))
        ).resultList();

        if (filhos.isEmpty()) {
            JPAUtils.clearEntitys(filhos);
            filhos = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                    .equal("codigoretorno.codigo", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                    .equal("codigoretorno.codigo", pCodigoFio, TipoExpressao.AND, b -> !pCodigoFio.equals(""))
                    .equal("corfor", pCorFor, TipoExpressao.AND, b -> !pCorFor.equals(""))
            ).resultList();

            if(filhos.isEmpty()){
                filhos = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                        .equal("codigoretorno.codigo", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                        .equal("codigoretorno.codigo", pCodigoFio, TipoExpressao.AND, b -> !pCodigoFio.equals(""))
                ).resultList();
            }
        }

        filhosBean.set(FXCollections.observableList(filhos));
    }

    protected void consultaFios(VSdMateriaisMalha codigo) {
        JPAUtils.clearEntitys(fios);
        String pCodigo = codigo.getFio() == null ? "" : codigo.getFio().getCodigo();
        String pCodFor = codigo.getCodfor() == null ? "" : codigo.getCodfor().getCodcli().toString();
        fios = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                .equal("codigoretorno.codigo", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
                .equal("codfor.codcli", pCodFor, TipoExpressao.AND, b -> !pCodFor.equals(""))
        ).resultList();
        if (fios.isEmpty()) {
            JPAUtils.clearEntitys(fios);
            fios = (List<VSdMateriaisMalha>) new FluentDao().selectFrom(VSdMateriaisMalha.class).where(it -> it
                    .equal("codigoretorno.codigo", pCodigo, TipoExpressao.AND, b -> !pCodigo.equals(""))
            ).resultList();
        }
        filhosBean.set(FXCollections.observableList(filhos));
    }

    protected BigDecimal getPrecoAntigo(String codigo) {
        Material material = new FluentDao().selectFrom(Material.class).where(it -> it
                .equal("codigo", codigo)).singleResult();
        JPAUtils.clearEntity(material);
        return material.getPreco();
    }

    protected boolean hasTinturaria(VSdMateriaisMalha malha) {
        return malha.getFio() == null && malha.getCodigo() != null && !malha.getCustotint().equals(BigDecimal.ZERO);
    }

    protected boolean hasTecelagem(VSdMateriaisMalha malha) {
        return malha.getCodigo() == null && malha.getFio() != null;
    }

    protected boolean notFio(VSdMateriaisMalha malha) {
        return malha.getCodigo() != null || malha.getFio() != null;
    }

}
