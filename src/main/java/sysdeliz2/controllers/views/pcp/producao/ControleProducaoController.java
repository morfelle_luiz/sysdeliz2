package sysdeliz2.controllers.views.pcp.producao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdPesosCamposOrd;
import sysdeliz2.models.sysdeliz.cadastros.SdFamiliasRecurso;
import sysdeliz2.models.sysdeliz.cadastros.SdRecursoProducao;
import sysdeliz2.models.sysdeliz.pcp.SdDemandaFaccao;
import sysdeliz2.models.sysdeliz.pcp.SdDemandaOf;
import sysdeliz2.models.sysdeliz.pcp.SdFluxoAtualOf;
import sysdeliz2.models.sysdeliz.pcp.SdProgFaccao;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.models.ti.Ano;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.pcp.VSdDemandaFaccao;
import sysdeliz2.models.view.pcp.VSdProgOfs30dias;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class ControleProducaoController extends WindowBase {

    protected final SdParametros indiceProximoSetor = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", 6)).singleResult();

    public ControleProducaoController(String title, Image icon) {
        super(title, icon);
    }

    @Override
    public void closeWindow() {
    }

    protected List<SdRecursoProducao> getRecursos(Object[] faccoes, Object[] setores, Object[] familias, Object[] controladores, Object[] produtos, LocalDate dataEntrega) {
        Object[] faccoesFamilia = new Object[]{};
        if (familias.length > 0) {
            List<SdFamiliasRecurso> recursosFamilia = (List<SdFamiliasRecurso>) new FluentDao().selectFrom(SdFamiliasRecurso.class)
                    .where(eb -> eb.isIn("id.familia.codigo", familias)
                            .isIn("id.recurso.setorExcia.codigo", setores, TipoExpressao.AND, b -> setores.length > 0))
                    .resultList();
            faccoesFamilia = recursosFamilia.stream().map(rec -> rec.getId().getRecurso().getFornecedor().getCodcli()).distinct().toArray();
        }

        Object[] faccoesProduto = new Object[]{};
        if (produtos.length > 0) {
            List<SdFluxoAtualOf> recursosProduto = (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class).where(eb -> eb.isIn("id.numero.produto.codigo", produtos)).resultList();
            faccoesProduto = recursosProduto.stream().map(rec -> rec.getId().getFaccao().getCodcli()).distinct().toArray();
        }

        Object[] faccoesDataEntrega = new Object[]{};
        if (dataEntrega != null) {
            List<SdFluxoAtualOf> recursosData = (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class).where(eb -> eb.equal("id.dtRetorno", dataEntrega)).resultList();
            faccoesDataEntrega = recursosData.stream().map(rec -> rec.getId().getFaccao().getCodcli()).distinct().toArray();
        }

        List<Object> concatArrays = new ArrayList<>();
        concatArrays.addAll(Arrays.asList(faccoes));
        concatArrays.addAll(Arrays.asList(faccoesFamilia));
        concatArrays.addAll(Arrays.asList(faccoesProduto));
        concatArrays.addAll(Arrays.asList(faccoesDataEntrega));
        Object[] finalFaccoesFamilia = concatArrays.stream().filter(fac -> fac != null).distinct().toArray();
        List<SdRecursoProducao> recursos = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                .where(eb -> eb.equal("ativo", true)
                        .isIn("setorExcia.codigo", setores, TipoExpressao.AND, b -> setores.length > 0)
                        .isIn("fornecedor.codcli", finalFaccoesFamilia, TipoExpressao.AND, b -> finalFaccoesFamilia.length > 0)
                        .isIn("controlador.codigo", controladores, TipoExpressao.AND, b -> controladores.length > 0))
                .resultList();


        return recursos;
    }

    protected List<SdRecursoProducao> getRecursosProducao(Object[] setores, Object[] faccoes) {
        List<SdRecursoProducao> recursos = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                .where(eb -> eb
                        .isIn("fornecedor.codcli", faccoes, TipoExpressao.AND, b -> faccoes.length > 0)
                        .isIn("setorExcia.codigo", setores, TipoExpressao.AND, b -> setores.length > 0))
                .resultList();
        return recursos;
    }

    protected List<SdFluxoAtualOf> getOfsDemanda(String faccao, String setor, boolean setorAtual, boolean emProducao, List<Ordenacao> order) {
        List<Ordenacao> orderBy = order == null ? Arrays.asList(new Ordenacao("id.dtRetorno")) : order;

        return (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class).where(eb -> eb
                        .and(and -> and.equal("id.setorAtual", true, TipoExpressao.AND,true)
                                .equal("comProgramacaoRetorno", true, TipoExpressao.OR,true))
                        .equal("id.faccao.codcli", faccao)
                        .equal("id.setor.codigo", setor)
                        .equal("emProducao", emProducao))
                .orderBy(orderBy)
                .resultList();
    }

    protected List<SdFluxoAtualOf> getOfsProximoSetor(String setor, Object[] familias, Boolean emPlanejamento, Boolean proximoSetor, Object[] colecoes) {
        return (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class)
                .where(eb -> eb
                        .equal("id.setorAtual", false)
                        .lessThanOrEqualTo("proximoSetor", Integer.parseInt(indiceProximoSetor.getValor()), TipoExpressao.AND, b -> proximoSetor)
                        .equal("emProducao", true, TipoExpressao.AND, b -> !emPlanejamento)
                        .equal("id.setor.codigo", setor)
                        .isIn("id.numero.produto.familia.codigo", familias, TipoExpressao.AND, b -> familias.length > 0)
                        .isIn("id.numero.produto.colecao.codigo", colecoes, TipoExpressao.AND, b -> colecoes.length > 0))
                .orderBy("id.dtEnvio", OrderType.ASC)
                .resultList();
    }

    protected List<SdFluxoAtualOf> getOfsDoDia(VSdDemandaFaccao recurso, LocalDate diaInicio, LocalDate diaFim, Boolean proximoSetor, Boolean setorAtual) {
        return (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class)
                .where(eb -> eb
                        .and(and -> and.equal("id.setorAtual", true, TipoExpressao.AND, true)
                                .equal("comProgramacaoRetorno", true, TipoExpressao.OR, true))
                        .lessThanOrEqualTo("proximoSetor", Integer.parseInt(indiceProximoSetor.getValor()), TipoExpressao.AND, b -> proximoSetor)
                        .lessThanOrEqualTo("id.dtInicio", diaInicio)
                        .greaterThanOrEqualTo("id.dtRetorno", diaFim)
                        .equal("id.faccao.codcli", recurso.getId().getFaccao().getCodcli())
                        .equal("id.setor.codigo", recurso.getId().getSetor().getCodigo()))
                .resultList();
    }

    protected List<SdFluxoAtualOf> getOfsEmAtraso(VSdDemandaFaccao recurso) {
        return (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class).where(eb -> eb.equal("id.setorAtual", true)
                        .lessThanOrEqualTo("id.dtRetorno", LocalDate.now())
                        .equal("id.faccao.codcli", recurso.getId().getFaccao().getCodcli())
                        .equal("id.setor.codigo", recurso.getId().getSetor().getCodigo()))
                .resultList();
    }

    protected SdDemandaOf getDemandaOfDia(VSdDemandaFaccao recurso, Ano dia, SdFluxoAtualOf of) {
        return new FluentDao().selectFrom(SdDemandaOf.class)
                .where(eb -> eb
                        .equal("id.dtDemanda", dia.getData())
                        .equal("id.numero", of.getId().getNumero().getNumero())
                        .equal("id.faccao", recurso.getId().getFaccao().getCodcli()))
                .singleResult();
    }

    protected List<VSdDemandaFaccao> getDemandasFaccao(Object[] faccoes, String grupoSetor, Object[] familias,  Object[] controladores, Object[] produtos, LocalDate dataEntrega) {
        List<SdRecursoProducao> recursos = getRecursos(faccoes, new Object[]{grupoSetor}, familias, controladores, produtos, dataEntrega);
        Object[] finalFaccoes = recursos.stream().map(fac -> fac.getFornecedor().getCodcli()).distinct().toArray();

        return (List<VSdDemandaFaccao>) new FluentDao().selectFrom(VSdDemandaFaccao.class).where(eb -> eb
                        .isIn("id.faccao.codcli", finalFaccoes)
                        .equal("id.setor.sdgrupo", grupoSetor)
                        .equal("id.emProducao", true)
                        .equal("id.setorAtual", true))
                .orderBy(Arrays.asList(new Ordenacao("id.faccao.codcli"),
                        new Ordenacao("id.setor.codigo"),
                        new Ordenacao("ordem"),
                        new Ordenacao("ordemProduc")))
                .resultList();
    }

    protected List<SdDemandaFaccao> getCalendarioFaccaoSetor(String faccao, String setor, String setorAtual) {
        return (List<SdDemandaFaccao>) new FluentDao().selectFrom(SdDemandaFaccao.class).where(eb -> eb
                        .lessThanOrEqualTo("id.dataDemanda.data", LocalDate.now().plusDays(30))
                        .equal("id.faccao.codcli", faccao)
                        .equal("id.setor.codigo", setor)
                        .equal("setorAtual", setorAtual.equals("S"), TipoExpressao.AND, b -> !setorAtual.equals("A")))
                .orderBy("id.dataDemanda.data", OrderType.ASC)
                .resultList();
    }

    protected List<VSdProgOfs30dias> getCalendarioOfsSetorFaccao(Object[] faccoes, Object[] setores, String setorAtual) {
        return (List<VSdProgOfs30dias>) new FluentDao().selectFrom(VSdProgOfs30dias.class)
                .where(eb -> eb
                        .between("id.data", LocalDate.now(), LocalDate.now().plusDays(30))
                        .isIn("id.faccao.codcli", faccoes)
                        .isIn("id.setor.codigo", setores)
                        .equal("setorAtual", setorAtual.equals("S"), TipoExpressao.AND, b -> !setorAtual.equals("A")))
                .resultList();
    }

    protected SdProgFaccao getProgramacaoFaccao(String numeroOF, String codFaccao, String codSetor) {
        return new FluentDao().selectFrom(SdProgFaccao.class)
                .where(eb -> eb.equal("id.numero", numeroOF)
                        .equal("id.setor.codigo", codSetor)
                        .equal("id.faccao.codcli", codFaccao))
                .singleResult();
    }

    protected String getObservacaoFaccao(String numeroOf, String faccao, String setor) throws SQLException {

        AtomicReference<String> obsFaccao = new AtomicReference<>("");
        List<Object> observacao = new NativeDAO().runNativeQuery("select distinct to_char(obs) observacao from faccao_001 where numero = '%s' and op = '%s' and codcli = '%s'",
                numeroOf, setor, faccao);
        observacao.stream().findFirst().ifPresent(obs -> {
            obsFaccao.set((String) ((Map<String, Object>) obs).get("OBSERVACAO"));
        });
        return obsFaccao.get();
    }

    protected VSdDadosOfPendente getDadosOfPendente(SdFluxoAtualOf fluxoOf) {
        return new FluentDao().selectFrom(VSdDadosOfPendente.class).where(eb -> eb.equal("id.numero", fluxoOf.getId().getNumero().getNumero()).equal("id.setor.codigo", fluxoOf.getId().getSetor().getCodigo())).singleResult();
    }

    protected List<String> getDatasEntrega(String numero, String codigo) throws SQLException {
        String where = numero.length() > 0 ? numero : codigo;
        List<Object> entregas = new NativeDAO().runNativeQuery("" +
                "select initcap(trim(to_char(dt_entrega, 'MONTH'))) || to_char(dt_entrega, '/YYYY') dt_entrega,\n" +
                "       sum(qtde) qtde\n" +
                "  from sd_mrp_produto_001\n" +
                " where status = 'T'\n" +
                (codigo.length() > 0 ? "and codigo = '%s'\n" : "and ordem_p = '%s'\n") +
                " group by initcap(trim(to_char(dt_entrega, 'MONTH'))) || to_char(dt_entrega, '/YYYY')\n" +
                " order by dt_entrega\n", where);

        List<String> datas = new ArrayList<>();
        entregas.forEach(value -> {
            datas.add(((String) ((Map<String, Object>) value).get("DT_ENTREGA")).concat(" - Venda: ").concat(String.valueOf((BigDecimal) ((Map<String, Object>) value).get("QTDE"))));
        });
        return datas;
    }

    protected Map<String, Object> getStatusJob() {
        try {
            List<Object> status = new NativeDAO().runNativeQuery("" +
                    "select decode(state,\n" +
                    "              'SCHEDULED',\n" +
                    "              'PROGRAMADO',\n" +
                    "              'RUNNING',\n" +
                    "              'EM EXECUÇÃO',\n" +
                    "              'S/ ESTADO') estado,\n" +
                    "       to_char(last_start_date - 0.0416666666666667,\n" +
                    "               'DD/MM/YYYY HH24:MI:SS') ult_rodada\n" +
                    "  from user_scheduler_jobs\n" +
                    " where job_name = 'SD_IMPORT_FLUXO_ATUAL_OF'");
            return (Map<String, Object>) status.get(0);
        } catch (SQLException e) {
            return null;
        }
    }

    protected void atualizaManualFluxoOf(SdFluxoAtualOf fluxoOf, LocalDate dtRetorno, LocalDate dtinicio, LocalDate dtEnvio, VSdDadosEntidade faccao, Boolean prioridade) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("" +
                "update sd_fluxo_atual_of_001 flx\n" +
                "   set flx.dt_envio         = '%s',\n" +
                "       flx.dt_inicio        = '%s',\n" +
                "       flx.dt_retorno_prog  = '%s',\n" +
                "       flx.faccao           = '%s',\n" +
                "       flx.prioridade       = '%s',\n" +
                "       flx.com_prog_retorno = 'S'\n" +
                " where flx.setor = '%s'\n" +
                "   and flx.faccao = '%s'\n" +
                "   and flx.numero = '%s'\n" +
                "   and flx.dt_envio = '%s'\n" +
                "   and flx.dt_inicio = '%s'\n" +
                "   and flx.dt_retorno_prog = '%s'",
                StringUtils.toDateFormat(dtEnvio),
                StringUtils.toDateFormat(dtinicio),
                StringUtils.toDateFormat(dtRetorno),
                faccao.getCodcli(),
                (prioridade ? "S" : "N"),
                fluxoOf.getId().getSetor().getCodigo(),
                fluxoOf.getId().getFaccao().getCodcli(),
                fluxoOf.getId().getNumero().getNumero(),
                StringUtils.toDateFormat(fluxoOf.getId().getDtEnvio()),
                StringUtils.toDateFormat(fluxoOf.getId().getDtInicio()),
                StringUtils.toDateFormat(fluxoOf.getId().getDtRetorno())
        );
    }

    protected void saveObservacaoFaccao(String observacao, String numeroOf, String faccao, String setor) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update faccao_001 set obs = '%s' where numero = '%s' and op = '%s' and codcli = '%s'",
                observacao, numeroOf, setor, faccao);
        SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, numeroOf,
                "Alterado observação da facção " + faccao + " no setor " + setor);
    }

    protected void saveAjuste(SdPesosCamposOrd atributo) {
        new FluentDao().merge(atributo);
        SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, String.valueOf(atributo.getCodigo()), "Atualizado valor para o atributo para " + atributo.getPeso() + "/" + atributo.getTipo());
    }

    protected SdDemandaOf saveDemandaOf(SdDemandaOf demanda) {
        SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, demanda.getId().getNumero(),
                "Definindo demanda na OF para o dia " + StringUtils.toDateFormat(demanda.getId().getDtDemanda()) + " com o tempo: " + demanda.getTempo() + " e qtde: " + demanda.getQtde());
        return new FluentDao().merge(demanda);
    }

    protected SdProgFaccao saveProgFaccaoOf(SdProgFaccao progFaccao) {
        return new FluentDao().merge(progFaccao);
    }

    protected void updateFaccaoPcp(String numero, String setor, String faccao, LocalDate dtInicio, LocalDate dtFim) throws SQLException {
//        new NativeDAO().runNativeQueryUpdate("update pcp_mov_001 pcp set pcp.dt_inicio = '%s', pcp.dt_fim = '%s', pcp.codcli = '%s' where numero = '%s' and setor = '%s'",
//                StringUtils.toDateFormat(dtInicio), StringUtils.toDateFormat(dtFim), faccao, numero, setor);
        new NativeDAO().runNativeQueryUpdate("update pcp_mov_001 pcp set pcp.codcli = '%s' where numero = '%s' and setor = '%s'",
                faccao, numero, setor);
    }

    protected void updateMovimentoFaccao(LocalDate novaData, String numeroOf, String codFaccao, String codSetor) throws SQLException {
        new NativeDAO().runNativeQueryUpdate("update faccao_001 fac set fac.dt_r = '%s' where fac.numero = '%s' and fac.codcli = '%s' and fac.op = '%s'",
                StringUtils.toDateFormat(novaData), numeroOf, codFaccao, codSetor);
    }

}
