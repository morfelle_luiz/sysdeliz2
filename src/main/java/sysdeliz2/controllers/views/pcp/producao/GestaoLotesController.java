package sysdeliz2.controllers.views.pcp.producao;

import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GestaoLotesController extends WindowBase {

    protected List<TabPrz> lotesList = new ArrayList<>();

    public GestaoLotesController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }

    protected void buscarLotes(Object[] lote, Object[] colecao, LocalDate beginValue, LocalDate endValue) {

        lotesList = (List<TabPrz>) new FluentDao().selectFrom(TabPrz.class).where(it -> it
                .isIn("prazo", lote, TipoExpressao.AND, when -> lote.length > 0)
                .isIn("colecao.codigo", colecao, TipoExpressao.AND, when -> colecao.length > 0)
                .between("dtInicio", beginValue, endValue)
        ).orderBy("prazo", OrderType.ASC).resultList();
    }

    @Override
    public void closeWindow() {

    }
}
