package sysdeliz2.controllers.views.pcp.producao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.SdIndiceProjecao001;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.utils.gui.window.base.WindowBase;

import java.util.List;

public class IndiceProjecaoController extends WindowBase {
    
    // <editor-fold defaultstate="collapsed" desc="Lists">
    protected final ListProperty<SdIndiceProjecao001> indices = new SimpleListProperty<>();
    protected final ListProperty<Linha> linhas = new SimpleListProperty<>();
    // </editor-fold>
    
    public IndiceProjecaoController(String title, Image icon, String[] tabs) {
        super(title, icon, tabs);
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    protected void procurarIndices(String marca, String colecao) {
        indices.set(FXCollections.observableList((List<SdIndiceProjecao001>) new FluentDao().selectFrom(SdIndiceProjecao001.class)
                .where(it -> it
                        .isIn("id.marca.codigo", marca.split(","), TipoExpressao.AND, b -> marca.length() > 0)
                        .isIn("id.colecao.codigo", colecao.split(","), TipoExpressao.AND, b -> colecao.length() > 0))
                .resultList()));
    }
    
    protected void carregarLinhasMarcas(String marca) {
        linhas.set(FXCollections.observableList((List<Linha>) new FluentDao().selectFrom(Linha.class)
                .where(it -> it
                        .equal("sdMarca", marca))
                .resultList()));
    }
}
