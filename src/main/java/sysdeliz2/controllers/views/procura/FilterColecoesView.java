package sysdeliz2.controllers.views.procura;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.controllers.views.procura.helpers.FilterGenericView;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.ti.Colecao;

/**
 * @author lima.joao
 * @since 06/09/2019 21:09
 */
@Deprecated
public class FilterColecoesView extends FilterGenericView<Colecao> {

    public FilterColecoesView() {
        this(null);
    }

    public FilterColecoesView(TitledPane filterPane) {
        super(
                new GenericDaoImpl<>(Colecao.class),
                filterPane,
                "Consultar Coleção",
                "/images/icons/colecao (3).png");
    }

    @Override
    protected void init() {
        super.init();

        ObservableList<BasicFilter> basicFilters = FXCollections.observableArrayList();

        basicFilters.add(new BasicFilter("codigo", "Código"));
        basicFilters.add(new BasicFilter("descricao", "Descrição"));

        this.cboxCampoFiltrar.setItems(basicFilters);

        TableColumn<Colecao, String> clsCodigo = new TableColumn<>();
        clsCodigo.setEditable(false);
        clsCodigo.setPrefWidth(74.0);
        clsCodigo.setText("Código");

        clsCodigo.setCellValueFactory(param -> param.getValue().codigoProperty());


        TableColumn<Colecao, String> clsDescricao = new TableColumn<>();
        clsDescricao.setEditable(false);
        clsDescricao.setPrefWidth(292.0);
        clsDescricao.setText("Descrição");

        clsDescricao.setCellValueFactory(param -> param.getValue().descricaoProperty());

        TableColumn<Colecao, String> clsIniVigencia = new TableColumn<>();
        clsIniVigencia.setEditable(false);
        clsIniVigencia.setMinWidth(0.0);
        clsIniVigencia.setPrefWidth(145.0);
        clsIniVigencia.setText("Início Vigência");

        clsIniVigencia.setCellValueFactory(param -> {
            if(param.getValue().getInicioVig() == null){
                return new SimpleStringProperty("");
            } else {
                return param.getValue().inicioVigProperty().asString();
            }
        });

        TableColumn<Colecao, String> clsFimVigencia = new TableColumn<>();
        clsFimVigencia.setEditable(false);
        clsFimVigencia.setPrefWidth(145.0);
        clsFimVigencia.setText("Fim Vigência");

        clsFimVigencia.setCellValueFactory(param -> {
            if(param.getValue().getFimVig() == null){
                return new SimpleStringProperty("");
            } else {
                return param.getValue().fimVigProperty().asString();
            }
        });

        this.center.getColumns().add(clsCodigo);
        this.center.getColumns().add(clsDescricao);
        this.center.getColumns().add(clsIniVigencia);
        this.center.getColumns().add(clsFimVigencia);
    }

    @Override
    protected void btnLoadOnAction(ActionEvent event) {
        this.genericDao.addPredicateEq("sdAtivo", "X");

        this.genericDao.addOrderByAsc("codigo");
        super.btnLoadOnAction(event);
    }

    @Override
    public String getResultAsString(boolean withQuotes){
        if (this.itensSelecionados.size() > 0){
            StringBuilder colecoes = new StringBuilder();
            for (Colecao itensSelecionado : this.itensSelecionados) {
                if(withQuotes){
                    colecoes.append("'");
                }

                colecoes.append(itensSelecionado.getCodigo().trim());

                if(withQuotes){
                    colecoes.append("'");
                }

                colecoes.append(",");
            }
            colecoes.deleteCharAt(colecoes.length()-1);
            return colecoes.toString();
        } else {
            return super.getResultAsString(withQuotes);
        }
    }

}