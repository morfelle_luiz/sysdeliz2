package sysdeliz2.controllers.views.procura;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.controllers.views.procura.helpers.FilterGenericView;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.ti.EtqProd;
import sysdeliz2.utils.hibernate.JPAUtils;

/**
 * @author lima.joao
 * @since 06/09/2019 21:09
 */
public class FilterEtiqProdView extends FilterGenericView<EtqProd> {

    public FilterEtiqProdView() {
        this(null);
    }

    public FilterEtiqProdView(TitledPane filterPane) {
        super(
                new GenericDaoImpl<>(JPAUtils.getEntityManager(), EtqProd.class),
                filterPane,
                "Consultar Classe/Modelagem",
                "/images/icons/produto (3).png");
    }

    @Override
    protected void init() {
        super.init();

        ObservableList<BasicFilter> basicFilters = FXCollections.observableArrayList();

        basicFilters.add(new BasicFilter("codigo", "Código"));
        basicFilters.add(new BasicFilter("descricao", "Descrição"));
        basicFilters.add(new BasicFilter("grupo", "Classe"));
        basicFilters.add(new BasicFilter("modelagem", "Modelagem"));

        this.cboxCampoFiltrar.setItems(basicFilters);

        TableColumn<EtqProd, String> clsCodigo = new TableColumn<>();
        clsCodigo.setEditable(false);
        clsCodigo.setPrefWidth(74.0);
        clsCodigo.setText("Código");

        clsCodigo.setCellValueFactory(param ->new SimpleStringProperty(String.valueOf(param.getValue().getCodigo())));

        TableColumn<EtqProd, String> clsDescricao = new TableColumn<>();
        clsDescricao.setEditable(false);
        clsDescricao.setPrefWidth(292.0);
        clsDescricao.setText("Descrição");

        clsDescricao.setCellValueFactory(param -> param.getValue().descricaoProperty());

        TableColumn<EtqProd, String> clsClasse = new TableColumn<>();
        clsClasse.setEditable(false);
        clsClasse.setPrefWidth(98.0);
        clsClasse.setText("Classe");

        clsClasse.setCellValueFactory(param -> param.getValue().grupoProperty());

        TableColumn<EtqProd, String> clsModelagem = new TableColumn<>();
        clsModelagem.setEditable(false);
        clsModelagem.setPrefWidth(292.0);
        clsModelagem.setText("Modelagem");

        clsModelagem.setCellValueFactory(param -> param.getValue().modelagemProperty());

        this.center.getColumns().add(clsCodigo);
        this.center.getColumns().add(clsDescricao);
        this.center.getColumns().add(clsClasse);
        this.center.getColumns().add(clsModelagem);
    }

    @Override
    protected void btnLoadOnAction(ActionEvent event) {
        this.genericDao.addOrderByAsc("codigo");
        super.btnLoadOnAction(event);
    }

    @Override
    public String getResultAsString(boolean withQuotes){
        if (this.itensSelecionados.size() > 0){
            StringBuilder resultado = new StringBuilder();
            for (EtqProd itensSelecionado : this.itensSelecionados) {
                if(withQuotes){
                    resultado.append("'");
                }

                resultado.append(itensSelecionado.getCodigo());

                if(withQuotes){
                    resultado.append("'");
                }

                resultado.append(",");
            }
            resultado.deleteCharAt(resultado.length()-1);
            return resultado.toString();
        } else {
            return super.getResultAsString(withQuotes);
        }
    }

}