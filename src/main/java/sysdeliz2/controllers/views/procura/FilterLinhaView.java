package sysdeliz2.controllers.views.procura;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.controllers.views.procura.helpers.FilterGenericView;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.utils.hibernate.JPAUtils;

/**
 * @author lima.joao
 * @since 06/09/2019 21:09
 */
public class FilterLinhaView extends FilterGenericView<Linha> {

    public FilterLinhaView() {
        this(null);
    }

    public FilterLinhaView(TitledPane filterPane) {
        super(
                new GenericDaoImpl<>(JPAUtils.getEntityManager(), Linha.class),
                filterPane,
                "Consultar Linha",
                "/images/icons/linha (3).png");
    }

    @Override
    protected void init() {
        super.init();

        ObservableList<BasicFilter> basicFilters = FXCollections.observableArrayList();

        basicFilters.add(new BasicFilter("codigo", "Código"));
        basicFilters.add(new BasicFilter("descricao", "Descrição"));
        basicFilters.add(new BasicFilter("sdLinhaDesc", "Linha"));

        this.cboxCampoFiltrar.setItems(basicFilters);

        TableColumn<Linha, String> clsCodigo = new TableColumn<>();
        clsCodigo.setEditable(false);
        clsCodigo.setPrefWidth(74.0);
        clsCodigo.setText("Código");

        clsCodigo.setCellValueFactory(param -> param.getValue().codigoProperty());

        TableColumn<Linha, String> clsDescricao = new TableColumn<>();
        clsDescricao.setEditable(false);
        clsDescricao.setPrefWidth(292.0);
        clsDescricao.setText("Descrição");

        clsDescricao.setCellValueFactory(param -> param.getValue().descricaoProperty());

        TableColumn<Linha, String> clsLinha = new TableColumn<>();
        clsLinha.setEditable(false);
        clsLinha.setPrefWidth(98.0);
        clsLinha.setText("Linha");

        clsLinha.setCellValueFactory(param -> param.getValue().sdLinhaDescProperty());

        this.center.getColumns().add(clsCodigo);
        this.center.getColumns().add(clsDescricao);
        this.center.getColumns().add(clsLinha);
    }

    @Override
    protected void btnLoadOnAction(ActionEvent event) {
        this.genericDao.addPredicateIsNotNull("sdLinhaDesc");
        this.genericDao.addOrderByAsc("codigo");
        super.btnLoadOnAction(event);
    }

    @Override
    public String getResultAsString(boolean withQuotes){
        if (this.itensSelecionados.size() > 0){
            StringBuilder resultado = new StringBuilder();
            for (Linha itensSelecionado : this.itensSelecionados) {
                if(withQuotes){
                    resultado.append("'");
                }

                resultado.append(itensSelecionado.getCodigo().trim());

                if(withQuotes){
                    resultado.append("'");
                }

                resultado.append(",");
            }
            resultado.deleteCharAt(resultado.length()-1);
            return resultado.toString();
        } else {
            return super.getResultAsString(withQuotes);
        }
    }

}