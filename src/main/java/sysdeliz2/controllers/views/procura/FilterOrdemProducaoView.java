package sysdeliz2.controllers.views.procura;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.controllers.views.procura.helpers.FilterGenericView;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.utils.hibernate.JPAUtils;

/**
 * @author lima.joao
 * @since 06/09/2019 21:09
 */
public class FilterOrdemProducaoView extends FilterGenericView<Of1> {

    private FilterColecoesView filterColecoesView = new FilterColecoesView();

    public FilterOrdemProducaoView() {
        this(null);
    }

    public FilterOrdemProducaoView(TitledPane filterPane) {
        super(
                new GenericDaoImpl<>(JPAUtils.getEntityManager(), Of1.class),
                filterPane,
                "Consultar Ordem de produção",
                "/images/icons/ordem de producao (3).png");
    }

    @Override
    protected void init() {
        // Inicializa a tela padrão
        super.init();

        //<editor-fold desc="Regras especificas de filtragem">
        ObservableList<BasicFilter> basicFilters = FXCollections.observableArrayList();
        basicFilters.add(new BasicFilter("numero", "Numero"));
        basicFilters.add(new BasicFilter("periodo", "Periodo"));
        basicFilters.add(new BasicFilter("produto.codigo", "Referência"));

        this.cboxCampoFiltrar.setItems(basicFilters);
        this.cboxCampoFiltrar.getSelectionModel().selectFirst();
        //</editor-fold>

        TableColumn<Of1, String> clsNumero = new TableColumn<>();
        clsNumero.setEditable(false);
        clsNumero.setPrefWidth(74.0);
        clsNumero.setText("Numero");
        clsNumero.setCellValueFactory(param -> param.getValue().numeroProperty());

        TableColumn<Of1, String> clsPeriodo = new TableColumn<>();
        clsPeriodo.setEditable(false);
        clsPeriodo.setMinWidth(0.0);
        clsPeriodo.setPrefWidth(145.0);
        clsPeriodo.setText("Período");

        clsPeriodo.setCellValueFactory(param -> param.getValue().periodoProperty());

        TableColumn<Of1, String> clsProdutoReferencia = new TableColumn<>();
        clsProdutoReferencia.setEditable(false);
        clsProdutoReferencia.setMinWidth(0.0);
        clsProdutoReferencia.setPrefWidth(145.0);
        clsProdutoReferencia.setText("Referência");

        clsProdutoReferencia.setCellValueFactory(param -> param.getValue().getProduto().codigoProperty());

        TableColumn<Of1, String> clsProdutoNome = new TableColumn<>();
        clsProdutoNome.setEditable(false);
        clsProdutoNome.setMinWidth(0.0);
        clsProdutoNome.setPrefWidth(145.0);
        clsProdutoNome.setText("Descrição Referência");

        clsProdutoNome.setCellValueFactory(param -> param.getValue().getProduto().descricaoProperty());

        this.center.getColumns().add(clsNumero);
        this.center.getColumns().add(clsPeriodo);
        this.center.getColumns().add(clsProdutoReferencia);
        this.center.getColumns().add(clsProdutoNome);
    }

    @Override
    protected void btnLoadOnAction(ActionEvent event) {
        this.genericDao.addOrderByAsc("numero");
        super.btnLoadOnAction(event);
    }

    @Override
    protected void addClausulaWhere(boolean isOr) {
        super.addClausulaWhere(isOr);
    }

    @Override
    protected void btnFindFilterOnAction(ActionEvent event) {
        super.btnFindFilterOnAction(event);
    }
}