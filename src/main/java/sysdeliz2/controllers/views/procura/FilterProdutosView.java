package sysdeliz2.controllers.views.procura;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.controllers.views.procura.helpers.FilterGenericView;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.hibernate.JPAUtils;

/**
 * @author lima.joao
 * @since 06/09/2019 21:09
 */
public class FilterProdutosView extends FilterGenericView<Produto> {

    private FilterColecoesView filterColecoesView = new FilterColecoesView();

    public FilterProdutosView() {
        this(null);
    }

    public FilterProdutosView(TitledPane filterPane) {
        super(
                new GenericDaoImpl<>(JPAUtils.getEntityManager(), Produto.class),
                filterPane,
                "Consultar Produtos",
                "/images/icons/produto (3).png");
    }

    @Override
    protected void init() {
        // Inicializa a tela padrão
        super.init();

        //<editor-fold desc="Regras especificas de filtragem">
        ObservableList<BasicFilter> basicFilters = FXCollections.observableArrayList();
        basicFilters.add(new BasicFilter("codigo", "Código"));

        basicFilters.add(new BasicFilter("descricao", "Descrição"));
        basicFilters.add(new BasicFilter("marca", "Marca"));
        basicFilters.add(new BasicFilter("linha", "Linha"));
        basicFilters.add(new BasicFilter("colecao", "Coleção"));
        basicFilters.add(new BasicFilter("familia", "Família"));

        this.cboxCampoFiltrar.setItems(basicFilters);

        this.cboxCampoFiltrar.valueProperty().addListener((observable, oldValue, newValue) -> {
            btnFindFilter.setVisible(false);
            btnFindFilter.setDisable(true);
            tboxValorFiltro.setEditable(true);
            if (newValue.getDescricao().equals("Marca") || newValue.getDescricao().equals("Linha") || newValue.getDescricao().equals("Coleção") || newValue.getDescricao().equals("Família")) {
                btnFindFilter.setVisible(true);
                btnFindFilter.setDisable(false);
                tboxValorFiltro.setEditable(false);
            }
        });
        //</editor-fold>

        TableColumn<Produto, String> clsCodigo = new TableColumn<>();
        clsCodigo.setEditable(false);
        clsCodigo.setPrefWidth(74.0);
        clsCodigo.setText("Código");
        clsCodigo.setCellValueFactory(param -> param.getValue().codigoProperty());

        TableColumn<Produto, String> clsDescricao = new TableColumn<>();
        clsDescricao.setEditable(false);
        clsDescricao.setPrefWidth(320.0);
        clsDescricao.setText("Descrição");

        clsDescricao.setCellValueFactory(param -> param.getValue().descricaoProperty());

        TableColumn<Produto, String> clsColecao = new TableColumn<>();
        clsColecao.setEditable(false);
        clsColecao.setMinWidth(0.0);
        clsColecao.setPrefWidth(145.0);
        clsColecao.setText("Coleção");

        clsColecao.setCellValueFactory(param -> param.getValue().getColecao().codigoProperty());

        TableColumn<Produto, String> clsMarca = new TableColumn<>();
        clsMarca.setEditable(false);
        clsMarca.setMinWidth(0.0);
        clsMarca.setPrefWidth(145.0);
        clsMarca.setText("Marca");

        clsMarca.setCellValueFactory(param -> param.getValue().getMarca().sdGrupoProperty());

        this.center.getColumns().add(clsCodigo);
        this.center.getColumns().add(clsDescricao);
        this.center.getColumns().add(clsColecao);
        this.center.getColumns().add(clsMarca);
    }

    @Override
    protected void btnLoadOnAction(ActionEvent event) {
        this.genericDao.addOrderByAsc("codigo");
        super.btnLoadOnAction(event);
    }

    @Override
    protected void addClausulaWhere(boolean isOr) {
        String field = cboxCampoFiltrar.getSelectionModel().getSelectedItem().getCampo();
        String descricao = cboxCampoFiltrar.getSelectionModel().getSelectedItem().getDescricao();

        if(descricao.equals("Marca") || descricao.equals("Linha") || descricao.equals("Coleção") || descricao.equals("Família")){
            switch (cboxTipoFiltro.getSelectionModel().getSelectedIndex()){
                case 0: // Igual
                    if(descricao.equals("Coleção")){
                        genericDao.addPredicateEq(field, filterColecoesView.itensSelecionados.get(0), isOr);
                    }

                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " igual " + tboxValorFiltro.getText().trim());
                    break;
                case 1: // Diferente
                    if(descricao.equals("Coleção")){
                        //genericDao.addPredicateNe(field, filterColecoesView.itensSelecionados.get(0), isOr);
                        //genericDao.addPredicateNe(field, filterColecoesView.itensSelecionados.get(0), isOr);
                    }

                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " diferente " + tboxValorFiltro.getText().trim());
                    break;
                case 2: // Contem
                    // Verifica se o conteudo é separado por virgula
                    genericDao.addPredicateLike(field, tboxValorFiltro.getText().trim(), isOr);
                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " contém " + tboxValorFiltro.getText().trim());
                    break;
                case 3: // Contido
                    genericDao.addPredicateIn(field, tboxValorFiltro.getText().trim().split(","), isOr);
                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " contido em [" + tboxValorFiltro.getText().trim() + "]");
                    break;
                case 4: // Não contido Em
                    genericDao.addPredicateNIn(field, tboxValorFiltro.getText().trim().replaceAll(" ", "").split(","), isOr);
                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " não contido em [" + tboxValorFiltro.getText().trim() + "]");
                    break;
                case 5: // É Nullo
                    genericDao.addPredicateIsNull(field, isOr);
                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " é nullo");
                    break;
                case 6: // Não Nullo
                    genericDao.addPredicateIsNotNull(field, isOr);
                    listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " é não nullo");
                    break;
            }

        } else {
            super.addClausulaWhere(isOr);
        }
    }

    @Override
    protected void btnFindFilterOnAction(ActionEvent event) {
        String descricao = cboxCampoFiltrar.getSelectionModel().getSelectedItem().getDescricao();
        if(descricao.equals("Marca") || descricao.equals("Linha") || descricao.equals("Coleção") || descricao.equals("Família")){

            if(descricao.equals("Coleção")){
                filterColecoesView.show(false);
                tboxValorFiltro.setText(filterColecoesView.getResultAsString(false));
            }


        } else {
            super.btnFindFilterOnAction(event);
        }
    }
}