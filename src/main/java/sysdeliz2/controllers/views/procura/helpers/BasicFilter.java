package sysdeliz2.controllers.views.procura.helpers;


/**
 * @author lima.joao
 * @since 10/09/2019 10:14
 */
public class BasicFilter {
    private String campo;
    private String descricao;
    private String filterClass;

    public BasicFilter() {
    }

    public BasicFilter(String campo, String descricao) {
        this.campo = campo;
        this.descricao = descricao;
    }
    
    public BasicFilter(String campo, String descricao, String filterClass) {
        this.campo = campo;
        this.descricao = descricao;
        this.filterClass = filterClass;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public String getFilterClass() {
        return filterClass;
    }
    
    public void setFilterClass(String filterClass) {
        this.filterClass = filterClass;
    }
    
    @Override
    public String toString() {
        return this.descricao;
    }
}
