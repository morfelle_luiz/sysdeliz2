package sysdeliz2.controllers.views.procura.helpers;

import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.StaticGraphicsBuilder;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.sql.SQLException;

/**
 * @author lima.joao
 * @since 09/09/2019 15:09
 */
public class FilterGenericView<E> {

    private Stage modalStage;
    private boolean singleSelect = false;

    public GenericDao<E> genericDao;
    // View
    private BorderPane view;
    private TitledPane filterPane;
    private HBox grupoFiltroNivel1;
    private Button btnLoad;

    protected ComboBox<BasicFilter> cboxCampoFiltrar;
    private Button buttomMarcarTudo;

    protected HBox top;
    protected Button btnWhereAnd;
    protected Button btnWhereOr;
    protected ComboBox<String> cboxTipoFiltro;
    protected TextField tboxValorFiltro;
    protected Button btnFindFilter;
    protected ListView<String> listWheres;

    protected TableView<E> center;
    private TableColumn<E, Boolean> clsSelected;

    protected HBox bottom;
    private String titulo;
    private String icone;
    private ObservableList<E> itensFiltrados = FXCollections.observableArrayList();

    public ObservableList<E> itensSelecionados = FXCollections.observableArrayList();

    public void show(){
        this.show(false);
    }

    public void show(boolean singleSelect){
        this.singleSelect = singleSelect;
        TextFieldUtils.upperCase(tboxValorFiltro);

        clsSelected.setVisible(!singleSelect);
        this.buttomMarcarTudo.setVisible(!singleSelect);

        modalStage = new Stage();
        Scene scene = new Scene(asParent(), 800.0, 650);
        scene.getStylesheets().add("/styles/bootstrap2.css");
        scene.getStylesheets().add("/styles/bootstrap3.css");
        scene.getStylesheets().add("/styles/styles.css");

        Image applicationIcon = new Image(FilterGenericView.class.getResourceAsStream("/images/delizIcon.png"));

        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> modalStage.close());
        modalStage.showAndWait();
    }

    public FilterGenericView(GenericDao<E> genericDao, TitledPane filterPane, String titulo, String icone) {
        this.genericDao = genericDao;
        this.genericDao.initCriteria();

        if (filterPane != null) {
            this.filterPane = filterPane;
        }

        this.titulo = titulo;
        this.icone = icone;
        init();
    }

    private Parent asParent() {
        return view;
    }

    private void buildGrupoFiltro() {
        grupoFiltroNivel1 = StaticGraphicsBuilder.buildHBox(100.0, 800.0, StaticGraphicsBuilder.DEFAULT_STYLE, Pos.CENTER_LEFT, 5.0);

        VBox grupoFiltroNivel2Left = StaticGraphicsBuilder.buildVBox(48.0, 400.0);
        HBox.setHgrow(grupoFiltroNivel2Left, Priority.ALWAYS);

        HBox grupoFiltroNivel3Top = StaticGraphicsBuilder.buildHBox(48.0, 100.0);
        VBox.setVgrow(grupoFiltroNivel3Top, Priority.ALWAYS);

        // Filtro Campo
        VBox grupoFiltroNivel4_1 = StaticGraphicsBuilder.buildVBox(48.0, 125.0);
        Label lblComboFiltro = new Label("Campo");
        lblComboFiltro.getStyleClass().add("form-field");

        cboxCampoFiltrar = new ComboBox<>();
        cboxCampoFiltrar.prefWidth(1292.0);
        cboxCampoFiltrar.getStyleClass().add("sm");
        cboxCampoFiltrar.getStyleClass().add("form-field");

        grupoFiltroNivel4_1.getChildren().add(lblComboFiltro);
        grupoFiltroNivel4_1.getChildren().add(cboxCampoFiltrar);

        VBox grupoFiltroNivel4_2 = StaticGraphicsBuilder.buildVBox(48.0, 150.0);
        Label lblComboRegra = new Label("Regra");
        lblComboRegra.getStyleClass().add("form-field");

        cboxTipoFiltro.prefWidth(1292.0);
        cboxTipoFiltro.getStyleClass().add("sm");
        cboxTipoFiltro.getStyleClass().add("form-field");

        grupoFiltroNivel4_2.getChildren().add(lblComboRegra);
        grupoFiltroNivel4_2.getChildren().add(cboxTipoFiltro);

        VBox grupoFiltroNivel4_3 = StaticGraphicsBuilder.buildVBox(48.0, 100.0);
        grupoFiltroNivel4_3.setAlignment(Pos.BOTTOM_CENTER);

        btnWhereAnd = StaticGraphicsBuilder.buildButton("btnWhereAnd", 22.0, 45.0, "E", this::btnWhereAndOnAction);
        btnWhereAnd.getStyleClass().add("xs");
        btnWhereAnd.getStyleClass().add("info");

        btnWhereOr = StaticGraphicsBuilder.buildButton("btnWhereOr", 22.0, 45, "OU", this::btnWhereOrOnAction);
        btnWhereOr.getStyleClass().add("xs");
        btnWhereOr.getStyleClass().add("info");

        grupoFiltroNivel4_3.getChildren().add(btnWhereAnd);
        grupoFiltroNivel4_3.getChildren().add(btnWhereOr);

        grupoFiltroNivel3Top.getChildren().add(grupoFiltroNivel4_1);
        grupoFiltroNivel3Top.getChildren().add(grupoFiltroNivel4_2);
        grupoFiltroNivel3Top.getChildren().add(grupoFiltroNivel4_3);

        HBox grupoFiltroNivel3Bottom = StaticGraphicsBuilder.buildHBox(486.0, 398.0);
        grupoFiltroNivel3Bottom.setPadding(new Insets(1.0, 0.0, 0.0, 0.0));

        VBox grupoFiltroNivel4_4 = StaticGraphicsBuilder.buildVBox(48.0, 200.0);
        HBox.setHgrow(grupoFiltroNivel4_4, Priority.ALWAYS);

        Label lblValor = new Label("Valor");
        lblValor.getStyleClass().add("form-field");
        grupoFiltroNivel4_4.getChildren().add(lblValor);

        tboxValorFiltro = new TextField();
        tboxValorFiltro.setId("tboxValorFiltro");
        tboxValorFiltro.setMinHeight(18.0);
        tboxValorFiltro.setPrefHeight(21.0);
        tboxValorFiltro.setPrefWidth(200.0);

        tboxValorFiltro.getStyleClass().add("form-field");
        tboxValorFiltro.getStyleClass().add("sm");

        grupoFiltroNivel4_4.getChildren().add(tboxValorFiltro);

        grupoFiltroNivel3Bottom.getChildren().add(grupoFiltroNivel4_4);

        VBox grupoFiltroNivel4_5 = StaticGraphicsBuilder.buildVBox(48.0, 18.0);
        grupoFiltroNivel4_5.setAlignment(Pos.BOTTOM_CENTER);

        btnFindFilter = new Button();
        btnFindFilter.setId("btnFindFilter");
        btnFindFilter.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnFindFilter.setDisable(true);
        btnFindFilter.setMnemonicParsing(false);
        btnFindFilter.setPrefWidth(195.0);
        btnFindFilter.setText("Consultar Filtro");
        btnFindFilter.setOnAction(this::btnFindFilterOnAction);
        btnFindFilter.getStyleClass().add("sm");
        btnFindFilter.getStyleClass().add("warning");
        btnFindFilter.setGraphic(StaticGraphicsBuilder.buildImageView("/images/icons/buttons/find (1).png", 16.0, 16.0, true, true));

        grupoFiltroNivel4_5.getChildren().add(btnFindFilter);
        grupoFiltroNivel4_5.setAlignment(Pos.BOTTOM_CENTER);

        grupoFiltroNivel3Bottom.getChildren().add(grupoFiltroNivel4_5);

        grupoFiltroNivel2Left.getChildren().add(grupoFiltroNivel3Top);
        grupoFiltroNivel2Left.getChildren().add(grupoFiltroNivel3Bottom);

        VBox grupoFiltroNivel2Center = StaticGraphicsBuilder.buildVBox(48.0, 300.0);
        grupoFiltroNivel2Center.setAlignment(Pos.BOTTOM_CENTER);

        listWheres = new ListView<>();
        listWheres.setId("listWheres");
        listWheres.setPrefHeight(300.0);
        listWheres.setPrefWidth(300.0);
        listWheres.setOnMouseClicked(this::listWheresOnMouseClicked);

        HBox.setHgrow(listWheres, Priority.ALWAYS);

        grupoFiltroNivel2Center.getChildren().add(listWheres);

        VBox grupoFiltroNivel2Right = StaticGraphicsBuilder.buildVBox(48.0, 100.0);
        grupoFiltroNivel2Right.setAlignment(Pos.BOTTOM_RIGHT);

        btnLoad = new Button();
        btnLoad.setId("btnLoad");
        btnLoad.setMnemonicParsing(false);
        btnLoad.setText("Consultar");
        btnLoad.setOnAction(this::btnLoadOnAction);

        btnLoad.getStyleClass().add("sm");
        btnLoad.getStyleClass().add("default");

        btnLoad.getStylesheets().add("/styles/sysDelizDesktop.css");

        btnLoad.setGraphic(StaticGraphicsBuilder.buildImageView("/images/icons/buttons/select (1).png", 16.0, 16.0, true, true));

        grupoFiltroNivel2Right.getChildren().add(btnLoad);

        HBox.setHgrow(grupoFiltroNivel2Right, Priority.ALWAYS);

        grupoFiltroNivel1.getChildren().add(grupoFiltroNivel2Left);
        grupoFiltroNivel1.getChildren().add(grupoFiltroNivel2Center);
        grupoFiltroNivel1.getChildren().add(grupoFiltroNivel2Right);
    }

    protected void init() {

        cboxTipoFiltro = new ComboBox<>();
//                                                                          like      equals  !equals         in            Not In
        cboxTipoFiltro.setItems(FXCollections.observableArrayList( "Contém", "Igual", "Diferente","Contido em", "Não contido em", "Não é nullo", "É nullo"));

       // Raiz da janela
        view = StaticGraphicsBuilder.buildBorderPane(
                800.0,
                1000.0,
                "-fx-background-color: #fff;");

        //<editor-fold desc="TOP">
        // É um HBox onde estará o icone o titulo e também o filtro
        top = StaticGraphicsBuilder.buildHBox(0, 1000.0);
        BorderPane.setAlignment(top, Pos.CENTER);

        VBox topPai = StaticGraphicsBuilder.buildVBox(0, 1000.0);
        HBox.setHgrow(topPai, Priority.ALWAYS);

        HBox topHeader = StaticGraphicsBuilder.buildHBox(24.0, 1000.0, "-fx-background-color: #802046;", Pos.CENTER_LEFT, 5.0);
        BorderPane.setAlignment(topHeader, Pos.CENTER);
        topHeader.getChildren().add(StaticGraphicsBuilder.buildImageView(icone, 45.0, 40.0, true, true));

        Label lblTitulo = new Label(titulo);
        lblTitulo.setTextFill(Paint.valueOf("WHITE"));
        lblTitulo.setFont(Font.font("System Bold", 16.0));
        topHeader.getChildren().add(lblTitulo);

        HBox filterBase = StaticGraphicsBuilder.buildHBox(0, 1000.0, StaticGraphicsBuilder.DEFAULT_STYLE, Pos.CENTER_LEFT, 5.0);

        if (filterPane == null) {
            filterPane = StaticGraphicsBuilder.buildTitledPane(false, true, 126.0, 1000.0, "Filtros");
            filterPane.setGraphic(StaticGraphicsBuilder.buildImageView("/images/icons/buttons/filter (1).png", 150.0, 16.0, true, true));
            buildGrupoFiltro();
            filterPane.setContent(grupoFiltroNivel1);
        }

        VBox.setMargin(filterBase, new Insets(5.0));
        VBox.setVgrow(filterBase, Priority.ALWAYS);

        filterBase.getChildren().add(filterPane);

        topPai.getChildren().add(topHeader);
        topPai.getChildren().add(filterBase);

        top.getChildren().add(topPai);
        view.setTop(top);
        //</editor-fold>

        //<editor-fold desc="Center">
        center = new TableView<>();
        center.setId("tblConsulta");
        center.setOnMouseClicked(this::tblConsultaOnMouseClicked);
        center.setPrefHeight(200.0);
        center.setPrefWidth(1000.0);

        center.setEditable(true);

        BorderPane.setAlignment(center, Pos.CENTER);

        center.setItems(itensFiltrados);

        clsSelected = new TableColumn<>();
        clsSelected.setId("clmSelected");
        clsSelected.setPrefWidth(22.0);
        clsSelected.setText("#");

        clsSelected.setCellFactory((col) -> new CheckBoxTableCell<>());

        clsSelected.setCellValueFactory((cellData) -> {
            E cellValue = cellData.getValue();
            BooleanProperty property = ((BasicModel) cellValue).selectedProperty();
            property.addListener((observable, oldValue, newValue) -> ((BasicModel) cellValue).setSelected(newValue));
            return property;
        });

        center.getColumns().add(clsSelected);

        BorderPane.setMargin(center, new Insets(5.0));

        view.setCenter(center);
        //</editor-fold>

        bottom = StaticGraphicsBuilder.buildHBox(38.0, 989.0);
        BorderPane.setAlignment(bottom, Pos.CENTER);
        BorderPane.setMargin(bottom, new Insets(5.0));

        HBox bottomNivel1 = StaticGraphicsBuilder.buildHBox(100.0, 200.0);
        HBox.setHgrow(bottomNivel1, Priority.ALWAYS);

        VBox bottomNivel2 = StaticGraphicsBuilder.buildVBox(200.0, 100.0);
        HBox.setHgrow(bottomNivel2, Priority.ALWAYS);

        buttomMarcarTudo = StaticGraphicsBuilder.buildButton("btnMarcarTudp", 24.0, 94.0, "Marcar Tudo", this::btnMarcarTudoOnAction);
        buttomMarcarTudo.setGraphic(StaticGraphicsBuilder.buildImageView("/images/icons/buttons/check-all (1).png", 16.0, 16.0, true, true));
        buttomMarcarTudo.setPadding(new Insets(-2.0));

        buttomMarcarTudo.getStylesheets().add("/styles/sysDelizDesktop.css");
        buttomMarcarTudo.getStyleClass().add("xs");
        buttomMarcarTudo.getStyleClass().add("default");
        buttomMarcarTudo.getStyleClass().add("last");

        bottomNivel2.getChildren().add(buttomMarcarTudo);
        bottomNivel1.getChildren().add(bottomNivel2);

        Button btnFinalizar = StaticGraphicsBuilder.buildButton("btnFinalizar", 25.0, 94.0, "Usar este(s)", this::btnFinalizarOnAction);
        btnFinalizar.setGraphic(StaticGraphicsBuilder.buildImageView("/images/icons/buttons/use-this (1).png", 16.0, 16.0, true, true));
        btnFinalizar.getStylesheets().add("/styles/sysDelizDesktop.css");
        btnFinalizar.getStyleClass().add("sm");
        btnFinalizar.getStyleClass().add("success");

        bottomNivel1.getChildren().add(btnFinalizar);

        bottom.getChildren().add(bottomNivel1);
        view.setBottom(bottom);

        this.cboxCampoFiltrar.getSelectionModel().selectFirst();
        this.cboxTipoFiltro.getSelectionModel().selectFirst();
    }

    protected void addClausulaWhere(boolean isOr ){
        String field = cboxCampoFiltrar.getSelectionModel().getSelectedItem().getCampo();
        String descricao = cboxCampoFiltrar.getSelectionModel().getSelectedItem().getDescricao();

        switch (cboxTipoFiltro.getSelectionModel().getSelectedIndex()){
            case 0: // Contem
                // Verifica se o conteudo é separado por virgula
                genericDao.addPredicateLike(field, tboxValorFiltro.getText().trim(), isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " contém " + tboxValorFiltro.getText().trim());
                break;
            case 2: // Diferente
                genericDao.addPredicateNe(field, tboxValorFiltro.getText().trim(), isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " diferente " + tboxValorFiltro.getText().trim());
                break;
            case 1: // Igual
                genericDao.addPredicateEqCascateField(field, tboxValorFiltro.getText().trim(), isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " igual " + tboxValorFiltro.getText().trim());
                break;
            case 3: // Contido
                genericDao.addPredicateIn(field, tboxValorFiltro.getText().trim().split(","), isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " contido em [" + tboxValorFiltro.getText().trim() + "]");
                break;
            case 4: // Não contido Em
                genericDao.addPredicateNIn(field, tboxValorFiltro.getText().trim().replaceAll(" ", "").split(","), isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " não contido em [" + tboxValorFiltro.getText().trim() + "]");
                break;
            case 5: // É Nullo
                genericDao.addPredicateIsNull(field, isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " é nullo");
                break;
            case 6: // Não Nullo
                genericDao.addPredicateIsNotNull(field, isOr);
                listWheres.getItems().add(0,(isOr? "ou " : "e ") + descricao + " é não nullo");
                break;
        }
    }

    /**
     * Este método deve ser implementado nas heranças. Ele esta declarado aqui para que o funcionamento da tela seja
     * automatizado evitando ter que reimplementar as chamandas que são padrões aos filtros.
     *
     * @param event evento da tela
     */
    protected void btnWhereAndOnAction(ActionEvent event) {
        // Não insere o filtro caso o conteudo não seja informado
        if (tboxValorFiltro.getText().isEmpty() && cboxTipoFiltro.getSelectionModel().getSelectedIndex() < 5) {
            return;
        }

        addClausulaWhere(false);

        tboxValorFiltro.clear();
        cboxCampoFiltrar.requestFocus();
    }

    /**
     * Este método deve ser implementado nas heranças. Ele esta declarado aqui para que o funcionamento da tela seja
     * automatizado evitando ter que reimplementar as chamandas que são padrões aos filtros.
     *
     * @param event evento da tela
     */
    protected void btnWhereOrOnAction(ActionEvent event) {
        // Não insere o filtro caso o conteudo não seja informado
        if (tboxValorFiltro.getText().isEmpty() && cboxTipoFiltro.getSelectionModel().getSelectedIndex() < 5) {
            return;
        }

        addClausulaWhere(true);

        tboxValorFiltro.clear();
        cboxCampoFiltrar.requestFocus();
    }

    protected void btnFindFilterOnAction(ActionEvent event) {
        // não faz nada
    }

    protected void btnLoadOnAction(ActionEvent event) {
        try {
            itensFiltrados.setAll(this.genericDao.loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.genericDao.clearPredicate();
        this.listWheres.getItems().clear();
        // não faz nada
    }

    protected void listWheresOnMouseClicked(MouseEvent event) {
    }

    protected void tblConsultaOnMouseClicked(MouseEvent event){
        // no action
    }

    protected void btnMarcarTudoOnAction(ActionEvent event){
        this.center.getItems().forEach(entity -> ((BasicModel) entity).selectedProperty().set(!((BasicModel) entity).selectedProperty().get()));
    }

    protected void btnFinalizarOnAction(ActionEvent event){
        if( singleSelect){
            itensSelecionados.clear();
            itensSelecionados.add(center.getSelectionModel().getSelectedItem());
        } else {
            center.getItems().forEach(entity -> {
                if (((BasicModel)entity).selectedProperty().get()) {
                    itensSelecionados.add(entity);
                }
            });
        }
        Stage main = (Stage) btnLoad.getScene().getWindow();
        main.close();
    }

    public String getResultAsString(boolean withQuotes){
        if (withQuotes){
            return "''";
        } else {
            return "";
        }
    }

    public void setValorFiltroInicial(String valor){
        if(valor != null) {
            this.tboxValorFiltro.setText(valor);
        } else {
            this.tboxValorFiltro.setText("");
        }
    }
}
