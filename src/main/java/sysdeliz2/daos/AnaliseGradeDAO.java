package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdAnaliseGrade;

import java.sql.SQLException;

/**
 * @author lima.joao
 * @since 03/09/2019 08:40
 */
public interface AnaliseGradeDAO {

    ObservableList<SdAnaliseGrade> loadAnaliseDeGrade(String inColecao, String modelagem, String classe, String faixa, String linha) throws SQLException;

}
