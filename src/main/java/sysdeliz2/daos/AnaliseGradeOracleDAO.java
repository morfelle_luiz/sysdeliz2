/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdAnaliseGrade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author lima.joao
 * @since 03/09/2019 08:40
 */
public class AnaliseGradeOracleDAO implements AnaliseGradeDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    @Deprecated
    public ObservableList<SdAnaliseGrade> loadAnaliseDeGrade(String inColecao, String modelagem, String classe, String faixa, String linha) throws SQLException {
//        ObservableList<SdAnaliseGrade> analiseGrades = FXCollections.observableArrayList();
//
//        StringBuilder builder = new StringBuilder();
//
//        //<editor-fold desc="Bloco principal do SQL">
//        builder
//                .append(" SELECT")
//                .append("     col.codigo colecao,")
//                .append("     pei.tam tamanho, ")
//                .append("     fai.posicao posicao,")
//                .append("     linha.sd_linhadesc linha,")
//                .append("     etq.grupo modelagem,")
//                .append("     etq.descricao classe,")
//                .append("     prd.faixa faixa,")
//                .append("     sum(pei.qtde + pei.qtde_f) qtde,")
//                .append("     sum(sum(pei.qtde+pei.qtde_f)) over (partition by col.codigo, PEI.tam) total_tamanho,")
//                .append("     sum(sum(pei.qtde+pei.qtde_f)) over (partition by col.codigo, prd.faixa) total_faixa,")
//                .append("     ROUND((")
//                .append("             sum(sum(pei.qtde+pei.qtde_f)) over (partition by col.codigo, PEI.tam) /")
//                .append("             sum(sum(pei.qtde+pei.qtde_f)) over (partition by col.codigo, prd.faixa) * 100), 2) PERC_TAMANHO")
//                .append(" FROM")
//                .append("    pedido_001 ped")
//                .append("    LEFT JOIN ped_iten_001 pei")
//                .append("    ON        pei.numero = ped.numero")
//                .append("    LEFT JOIN PRODUTO_001 prd")
//                .append("    ON        prd.codigo = pei.codigo")
//                .append("    LEFT JOIN TABLIN_001 linha")
//                .append("    ON        linha.codigo = prd.linha")
//                .append("    LEFT JOIN colecao_001 col")
//                .append("    ON        ped.dt_emissao BETWEEN col.inicio_vig AND col.fim_vig")
//                .append("    LEFT JOIN vpedido_concentrados conc")
//                .append("    ON        conc.numero = ped.numero AND conc.codigo = pei.codigo")
//                .append("    LEFT JOIN ETQ_PROD_001 etq")
//                .append("    ON        ETQ.codigo = prd.etiqueta")
//                .append("    LEFT JOIN FAIXA_ITEN_001 fai")
//                .append("    ON        fai.faixa = prd.faixa and fai.tamanho = pei.tam")
//                .append(" WHERE")
//                .append("    conc.numero IS NULL")
//                .append("    AND linha.sd_linhadesc is not null");
//        //</editor-fold>
//
//        if (!inColecao.trim().equals("")) {
//            builder
//                    .append("    AND col.codigo IN (")
//                    .append(inColecao)
//                    .append("    )");
//        }
//
//        if (modelagem != null && !modelagem.equals("")) {
//            builder
//                    .append("    AND etq.grupo = '")
//                    .append(modelagem)
//                    .append("'");
//        }
//
//        if (classe != null && !classe.equals("")) {
//            builder
//                    .append("    AND etq.descricao = '")
//                    .append(classe)
//                    .append("'");
//        }
//
//        if (linha != null && !linha.equals("")) {
//            builder
//                    .append("    AND linha.sd_linhadesc = '")
//                    .append(linha)
//                    .append("'");
//        }
//
//        if (faixa != null && !faixa.equals("")) {
//            builder
//                    .append("    AND prd.faixa = '")
//                    .append(faixa)
//                    .append("'");
//        }
//
//        builder
//                .append(" GROUP BY")
//                .append("    col.codigo, pei.codigo, prd.faixa, pei.cor, pei.tam, fai.posicao, linha.sd_linhadesc, etq.descricao, etq.grupo")
//                .append(" ORDER BY" )
//                .append("    COL.codigo, etq.descricao, etq.grupo, fai.posicao, PEI.tam");
//
//        connection = ConnectionFactory.getDelizConnection();
//        statement = connection.createStatement();
//        preparedStatement = connection.prepareStatement(builder.toString());
//        preparedStatement.execute();
//        ResultSet resultSetSql = preparedStatement.getResultSet();
//
//        String _colecao = "";
//        int _posicao = 0;
//
//        SdAnaliseGrade analiseGrade = null;
//
//        GenericDao<Faixa> faixaDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Faixa.class);
//        Faixa tmpFaixa = null;
//
//        while (resultSetSql.next()) {
//
//            if(!_colecao.equals(resultSetSql.getString("colecao"))){
//                if(analiseGrade != null){
//                    analiseGrades.add(analiseGrade);
//                }
//                analiseGrade = new SdAnaliseGrade();
//
//                analiseGrade.setColecao(resultSetSql.getString("colecao"));
//                analiseGrade.setLinha( resultSetSql.getString("linha"));
//                analiseGrade.setModelagem( resultSetSql.getString("modelagem"));
//                analiseGrade.setClasse(resultSetSql.getString("classe"));
//
//                if(tmpFaixa == null){
//                    tmpFaixa = faixaDao.load(resultSetSql.getString("faixa"));
//                }
//
//                for (FaixaItem iten : tmpFaixa.getItens()) {
//                    analiseGrade.getDetalhe().add(new AnaliseGradeDetalhe(
//                            iten.getFaixaItemId().getTamanho(),
//                            resultSetSql.getString("faixa"),
//                            0,
//                            BigDecimal.ZERO
//                    ));
//                }
//
//                for (AnaliseGradeDetalhe analiseGradeDetalhe : analiseGrade.getDetalhe()) {
//                    if(analiseGradeDetalhe.getTamanho().equals(resultSetSql.getString("tamanho"))){
//                        analiseGradeDetalhe.setTotalTamanho(resultSetSql.getInt("total_tamanho"));
//                        analiseGradeDetalhe.setPercTamanho(resultSetSql.getBigDecimal("PERC_TAMANHO"));
//                    }
//                }
//
//                _colecao = resultSetSql.getString("colecao");
//                _posicao = resultSetSql.getInt("posicao");
//            } else {
//                if(_posicao != resultSetSql.getInt("posicao")){
//                    assert analiseGrade != null;
//                    for (AnaliseGradeDetalhe analiseGradeDetalhe : analiseGrade.getDetalhe()) {
//                        if(analiseGradeDetalhe.getTamanho().equals(resultSetSql.getString("tamanho"))){
//                            analiseGradeDetalhe.setTotalTamanho(resultSetSql.getInt("total_tamanho"));
//                            analiseGradeDetalhe.setPercTamanho(resultSetSql.getBigDecimal("PERC_TAMANHO"));
//                        }
//                    }
//                    _posicao = resultSetSql.getInt("posicao");
//                }
//            }
//        }
//
//        if(analiseGrade != null){
//            analiseGrades.add(analiseGrade);
//        }
//
//        resultSetSql.close();
//        this.closeConnection();
//        return analiseGrades;
        return null;
    }


}
