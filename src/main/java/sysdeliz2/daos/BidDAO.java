/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.*;
import sysdeliz2.models.properties.ComercialMetasBid;
import sysdeliz2.models.properties.ComercialMetasBidPremiado;
import sysdeliz2.models.properties.IndicadoresBidLinha;

import java.sql.SQLException;
import java.util.Map;

/**
 * @author cristiano.diego
 */
public interface BidDAO {

    public ObservableList<Bid> loadBid(String cod_rep, String cod_colecao, String cod_marca, String familia) throws SQLException;

    public ObservableList<ComercialMetasBid> getMetasAll() throws SQLException;

    public void deleteMetaRepr(String codrep, String marca, String colecao) throws SQLException;

    public void mergeMetaRepr(String codrep, String marca, String colecao, String tipo, String[] metas) throws SQLException;

    public ObservableList<ComercialMetasBid> getMetasReps(String codrep, String marca, String codcol) throws SQLException;

    public ComercialMetasBid getMetaReps(String codrep, String marca, String codcol) throws SQLException;

    public ObservableList<ComercialMetasBidPremiado> getMetasPremioAll() throws SQLException;

    public ObservableList<ComercialMetasBidPremiado> getMetasPremioByWindowsForm(String marca, String colecao) throws SQLException;

    public ComercialMetasBidPremiado getMetaPremio(String marca, String colecao) throws SQLException;

    public void deleteMetaPremio(String marca, String colecao) throws SQLException;

    public void mergeMetaPremio(String marca, String colecao, String indicador, String graduacao, String perc_premio, String obrigatorio, Integer ordem) throws SQLException;

    public ObservableList<AtendimentoCidadeRep> getCidadesRep(String codrep, String codmar) throws SQLException;

    public ObservableList<BidPremiado> getBidPremiadoRep(String codrep, String codmar, String codcol) throws SQLException;

    public ObservableList<BidCliente> getBidCliente(String codrep, String codcli, String cidade, String colecao, String marca, String compra) throws SQLException;

    public ObservableList<BidClienteIndicadores> getBidClienteIndicadores(String codcli, String codmar, String codcol, String linha) throws SQLException;

    public ObservableList<BidClienteIndicadores> getBidClienteIndicadoresTotais(String codcli, String codmar, String codcol, String linha) throws SQLException;

    public Map<String, IndicadoresBidLinha> getBidLinha(String codrep, String colecao, String marca) throws SQLException;
}
