/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.*;
import sysdeliz2.models.properties.ComercialMetasBid;
import sysdeliz2.models.properties.ComercialMetasBidPremiado;
import sysdeliz2.models.properties.IndicadoresBidLinha;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cristiano.diego
 */
public class BidOracleDAO implements BidDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    private ObservableList<Sd_Metas_Bid_001> getMetasByRep(String codrep, String codcol, String codmar) throws SQLException {
        ObservableList<Sd_Metas_Bid_001> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODREP, MARCA, COLECAO, TIPO, PM, CD, PV, KA,\n"
                        + "PP, VP, TP, TF, PG, PR, MOSTR, CLN\n"
                        + "FROM SD_METAS_BID_001 MTA\n"
                        + "WHERE MTA.MARCA = ?\n"
                        + "	AND MTA.CODREP = ?\n"
                        + "	AND MTA.COLECAO  = ?");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codmar);
        preparedStatement.setString(2, codrep);
        preparedStatement.setString(3, codcol);

        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            metas.add(new Sd_Metas_Bid_001(resultSetSqlLocal.getString("CODREP"), resultSetSqlLocal.getString("TIPO"), resultSetSqlLocal.getString("MARCA"),
                    resultSetSqlLocal.getString("COLECAO"), resultSetSqlLocal.getInt("MOSTR"), resultSetSqlLocal.getDouble("PM"), resultSetSqlLocal.getInt("CD"),
                    resultSetSqlLocal.getInt("PV"), resultSetSqlLocal.getInt("KA"), resultSetSqlLocal.getInt("PP"), resultSetSqlLocal.getDouble("VP"),
                    resultSetSqlLocal.getInt("TP"), resultSetSqlLocal.getDouble("TF"), resultSetSqlLocal.getDouble("PG"), resultSetSqlLocal.getDouble("PR"),
                    resultSetSqlLocal.getInt("CLN")));
        }
        resultSetSqlLocal.close();

        return metas;
    }

    private ObservableList<IndicadorMetaBid> getIndicadoresRep(String codrep, String codcol, String codmar) throws SQLException {
        ObservableList<IndicadorMetaBid> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODREP, MARCA, COLECAO,\n" +
                        "  DECODE(METAS, '01MOSTR', 'Mostruario', '02CLN', 'Cliente Novo', '03PM', 'Preço Médio (PM)'\n" +
                        "            , '04CD', 'Cidades Atendidas (CD)', '05PV', 'Clientes Atendidos (PV)', '06KA', 'Key Acounts (KA)'\n" +
                        "            , '07PP', 'Peças por Pedido (PP)', '08VP', 'Valor por Pedido (VP)', '09TP', 'Total de Peças (TP)'\n" +
                        "            , '10TF', 'Total Financeiro (TF))', '11PG', 'Profundidade da Grade (PG)', '12PR', 'Profundidade Referências (PR)') Indicador,\n" +
                        "MTA, RPM, BAS, DEN, COLL, CASU, WIS\n" +
                        "FROM(\n" +
                        "SELECT * FROM (\n" +
                        "SELECT CODREP, MARCA, COLECAO, TIPO, PM, CD, PV, KA,\n" +
                        "PP, VP, TP, TF, PG, PR, MOSTR, CLN\n" +
                        "FROM SD_METAS_BID_001 MTA\n" +
                        "WHERE MTA.MARCA = ? \n" +
                        "  AND MTA.CODREP = ? \n" +
                        "  AND MTA.COLECAO  = ? )\n" +
                        "UNPIVOT (\n" +
                        "  META FOR METAS IN (MOSTR AS '01MOSTR', CLN AS '02CLN', PM AS '03PM', CD AS '04CD', PV AS '05PV', KA AS '06KA', \n" +
                        "            PP AS '07PP', VP AS '08VP', TP AS '09TP', TF AS '10TF', PG AS '11PG', PR AS '12PR')\n" +
                        " ))\n" +
                        " PIVOT (\n" +
                        "   SUM(META) FOR TIPO IN ('MTA' AS MTA, 'RPM' AS RPM, 'BAS' AS BAS, 'DEN' AS DEN, 'COL' AS COLL, 'CAS' AS CASU, 'WIS' as WIS) \n" +
                        " )\n" +
                        " ORDER BY METAS");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codmar);
        preparedStatement.setString(2, codrep);
        preparedStatement.setString(3, codcol);

        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            metas.add(new IndicadorMetaBid(resultSetSqlLocal.getString("CODREP"), resultSetSqlLocal.getString("MARCA"), resultSetSqlLocal.getString("COLECAO"),
                    resultSetSqlLocal.getString("INDICADOR"), resultSetSqlLocal.getDouble("MTA"), resultSetSqlLocal.getDouble("RPM"), resultSetSqlLocal.getDouble("BAS"),
                    resultSetSqlLocal.getDouble("DEN"), resultSetSqlLocal.getDouble("COLL"), resultSetSqlLocal.getDouble("CASU"), resultSetSqlLocal.getDouble("WIS")));
        }
        resultSetSqlLocal.close();

        return metas;
    }

    @Override
    public ObservableList<AtendimentoCidadeRep> getCidadesRep(String codrep, String codmar) throws SQLException {
        ObservableList<AtendimentoCidadeRep> cidades = FXCollections.observableArrayList();

        String marca = codmar.equals("D") ? "DLZ" : codmar.equals("U") ? "UP WAVE" : "FLOR DE LIS";

        StringBuilder query = new StringBuilder(
                "SELECT *\n"
                        + "FROM SD_BID_CIDADES_REP_001 CIDADES_REP\n"
                        + "WHERE CIDADES_REP.MARCA = '" + marca + "'\n"
                        + "	AND CIDADES_REP.CODREP = '" + codrep + "'"
                        + "ORDER BY NOME_CID");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());

        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            cidades.add(new AtendimentoCidadeRep(resultSetSqlLocal.getString("CODREP"), resultSetSqlLocal.getString("COD_EST"), resultSetSqlLocal.getString("NOME_CID"), resultSetSqlLocal.getString("CLASSE"),
                    resultSetSqlLocal.getInt("POP_CID"), resultSetSqlLocal.getInt("COMP_COL_ATUAL"), resultSetSqlLocal.getInt("COMP_COL_ANT"), resultSetSqlLocal.getInt("COMP_COL_PAS"),
                    resultSetSqlLocal.getInt("COMP_COL_REF"), resultSetSqlLocal.getDouble("IPC")));
        }
        closeConnection();
        resultSetSqlLocal.close();

        return cidades;
    }

    public ObservableList<AtendimentoCidadeRep> getCidadesRep_OLD(String codrep, String codmar) throws SQLException {
        ObservableList<AtendimentoCidadeRep> cidades = FXCollections.observableArrayList();
        String marca = codmar.equals("D") ? "DLZ" : codmar.equals("U") ? "UP WAVE" : "FLOR DE LIS";

        StringBuilder query = new StringBuilder(
                "SELECT \n"
                        + "	CODREP, COD_EST, NOME_CID, POP_CID, NVL(IPC,0) IPC, CLASSE,\n"
                        + "	SUM(CASE WHEN COLECAO IN (SELECT COL.CODIGO FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG) THEN\n"
                        + "		1\n"
                        + "	ELSE \n"
                        + "		0\n"
                        + "	END) COMP_COL_ATUAL,\n"
                        + "	SUM(CASE WHEN COLECAO = (SELECT MAX(CODIGO) FROM COLECAO_001 COL_ANT WHERE CODIGO < (SELECT MIN(COL.CODIGO) FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG)) THEN\n"
                        + "		1\n"
                        + "	ELSE \n"
                        + "		0\n"
                        + "	END) COMP_COL_ANT,\n"
                        + "	SUM(CASE WHEN COLECAO = (SELECT MAX(CODIGO) FROM COLECAO_001 WHERE CODIGO < (SELECT MAX(CODIGO) FROM COLECAO_001 COL_ANT WHERE CODIGO < (SELECT MIN(COL.CODIGO) FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG))) THEN\n"
                        + "		1\n"
                        + "	ELSE \n"
                        + "		0\n"
                        + "	END) COMP_COL_PAS\n"
                        + "FROM (\n"
                        + "	SELECT DISTINCT REP.CODREP, CID.COD_EST, CID.NOME_CID, CID.POP_CID, CID.IPC, CID.CLASSE, PRD.COLECAO, \n"
                        + "		COUNT(DISTINCT CLI.CODCLI) OVER (PARTITION BY REP.CODREP, CID.COD_EST, CID.NOME_CID) QTDE_CLI,\n"
                        + "		COUNT(DISTINCT PED.NUMERO) OVER (PARTITION BY REP.CODREP, CID.COD_EST, CID.NOME_CID) QTDE_PEDS,\n"
                        + "		COUNT(DISTINCT PED.NUMERO) OVER (PARTITION BY REP.CODREP, CID.COD_EST, CID.NOME_CID, PRD.COLECAO) QTDE_PEDS_COL\n"
                        + "	FROM REPRESEN_001 REP \n"
                        + "	INNER JOIN SD_CIDADE_REP_001 CIDREP ON CIDREP.CODREP = REP.CODREP\n"
                        + "	INNER JOIN CIDADE_001 CID ON CID.COD_CID = CIDREP.COD_CID\n"
                        + "	INNER JOIN CADCEP_001 CEP ON CEP.COD_CID = CID.COD_CID\n"
                        + "	LEFT JOIN CLI_COM_001 CLICOM ON CLICOM.CODREP = REP.CODREP\n"
                        + "	LEFT JOIN ENTIDADE_001 CLI ON CLI.CODCLI = CLICOM.CODCLI AND CLI.CEP = CEP.CEP\n"
                        + "	LEFT JOIN PEDIDO_001 PED ON PED.CODCLI = CLI.CODCLI AND PED.CODREP = REP.CODREP\n"
                        + "	LEFT JOIN PED_ITEN_001 PEDI ON PEDI.NUMERO = PED.NUMERO\n"
                        + "	LEFT JOIN PRODUTO_001 PRD ON PRD.CODIGO = PEDI.CODIGO \n"
                        + "	LEFT JOIN TABLIN_001 LIN ON LIN.CODIGO = PRD.LINHA\n"
                        + "	WHERE REP.CODREP = '" + codrep + "'\n"
                        + "		AND PRD.COLECAO <> '17CO' /*CONSTANTE*/\n"
                        + "		AND F_SD_GET_MARCA_LINHA(LIN.DESCRICAO) = '" + marca + "'\n"
                        + "	GROUP BY REP.CODREP, CID.COD_EST, CID.NOME_CID, CID.POP_CID, CID.IPC, CID.CLASSE, CLI.CODCLI, PED.NUMERO, PRD.COLECAO\n"
                        + "	ORDER BY NOME_CID, PRD.COLECAO DESC\n"
                        + ")\n"
                        + "GROUP BY CODREP, COD_EST, NOME_CID, POP_CID, IPC, CLASSE\n"
                        + "ORDER BY NOME_CID");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());

        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            cidades.add(new AtendimentoCidadeRep(resultSetSqlLocal.getString("CODREP"), resultSetSqlLocal.getString("COD_EST"), resultSetSqlLocal.getString("NOME_CID"), resultSetSqlLocal.getString("CLASSE"),
                    resultSetSqlLocal.getInt("POP_CID"), resultSetSqlLocal.getInt("COMP_COL_ATUAL"), resultSetSqlLocal.getInt("COMP_COL_ANT"), resultSetSqlLocal.getInt("COMP_COL_PAS"),
                    resultSetSqlLocal.getInt("COMP_COL_REF"), resultSetSqlLocal.getDouble("IPC")));
        }
        resultSetSqlLocal.close();

        return cidades;
    }

    private ObservableList<IndicadorMetaBidPremiado> getIndicadoresPremio(String codcol, String codmar) throws SQLException {
        ObservableList<IndicadorMetaBidPremiado> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT BID.MARCA, BID.COLECAO, BID.INDICADOR,\n"
                        + "	BID.GRADUACAO, BID.PERC_PREMIO, BID.OBRIGATORIO\n"
                        + "FROM SD_METAS_BID_PREMIADO_001 BID\n"
                        + "WHERE BID.MARCA = ? AND BID.COLECAO = ?");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codmar);
        preparedStatement.setString(2, codcol);

        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            metas.add(new IndicadorMetaBidPremiado(resultSetSqlLocal.getString("MARCA"), resultSetSqlLocal.getString("COLECAO"), resultSetSqlLocal.getString("INDICADOR"),
                    resultSetSqlLocal.getDouble("GRADUACAO"), resultSetSqlLocal.getDouble("PERC_PREMIO"), resultSetSqlLocal.getString("OBRIGATORIO")));
        }
        resultSetSqlLocal.close();

        return metas;
    }

    @Override
    public ObservableList<Bid> loadBid(String cod_rep, String cod_colecao, String cod_marca, String familia) throws SQLException {
        ObservableList<Bid> dados_bid = FXCollections.observableArrayList();
        String cod_fam = familia.length() == 0 ? "000" : familia;

        cod_rep = cod_rep.replace("'", "").replace(",", "|");
        cod_colecao = cod_colecao.replace("'", "").replace(",", "|");

        StringBuilder query = new StringBuilder(
                "SELECT *\n"
                        + "FROM SD_BID_001 BID\n"
                        + "WHERE BID.CODMAR = SUBSTR('" + cod_marca + "',1,1)\n"
                        + "	AND REGEXP_LIKE(BID.CODREP, '*(" + cod_rep + ")')\n"
                        + "	AND REGEXP_LIKE(BID.CODCOL, '*(" + cod_colecao + ")')\n"
                        + "	AND REGEXP_LIKE(BID.CODFAM, '*(" + cod_fam + ")')"
                        + "ORDER BY 5 DESC, 6, 2"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            dados_bid.add(new Bid(resultSetSql.getString("CODREP"),
                    resultSetSql.getString("REPRESENT"),
                    resultSetSql.getString("NOME"),
                    resultSetSql.getString("UF_REP"),
                    resultSetSql.getString("REGIAO"),
                    resultSetSql.getString("GERENTE"),
                    resultSetSql.getString("MARCA"),
                    resultSetSql.getString("CODMAR"),
                    resultSetSql.getString("COLECAO"),
                    resultSetSql.getString("CODCOL"),
                    resultSetSql.getString("FAMILIA"),
                    resultSetSql.getInt("MOSTR"),
                    resultSetSql.getDouble("PM_RCM"),
                    resultSetSql.getDouble("PM_RPM"),
                    resultSetSql.getDouble("PM_MTA"),
                    resultSetSql.getInt("CD_RCM"),
                    resultSetSql.getInt("CD_RPM"),
                    resultSetSql.getInt("CD_MTA"),
                    resultSetSql.getInt("PV_RCM"),
                    resultSetSql.getInt("PV_RPM"),
                    resultSetSql.getInt("PV_MTA"),
                    resultSetSql.getInt("KA_RCM"),
                    resultSetSql.getInt("KA_RPM"),
                    resultSetSql.getInt("KA_MTA"),
                    resultSetSql.getInt("PP_RCM"),
                    resultSetSql.getInt("PP_RPM"),
                    resultSetSql.getInt("PP_MTA"),
                    resultSetSql.getDouble("VP_RCM"),
                    resultSetSql.getDouble("VP_RPM"),
                    resultSetSql.getDouble("VP_MTA"),
                    resultSetSql.getInt("TP_RCM"),
                    resultSetSql.getInt("TP_RPM"),
                    resultSetSql.getInt("TP_MTA"),
                    resultSetSql.getDouble("TF_RCM"),
                    resultSetSql.getDouble("TF_RPM"),
                    resultSetSql.getDouble("TF_MTA"),
                    resultSetSql.getDouble("PG_RCM"),
                    resultSetSql.getDouble("PG_RPM"),
                    resultSetSql.getDouble("PG_MTA"),
                    resultSetSql.getDouble("PR_RCM"),
                    resultSetSql.getDouble("PR_RPM"),
                    resultSetSql.getDouble("PR_MTA"),
                    resultSetSql.getInt("CLN_RCM"),
                    resultSetSql.getInt("CLN_MTA"),
                    resultSetSql.getInt("BAS_TP_RCM"),
                    resultSetSql.getInt("BAS_TP_MTA"),
                    resultSetSql.getDouble("PER_BAS_TP"),
                    resultSetSql.getDouble("BAS_TF_RCM"),
                    resultSetSql.getInt("DEN_TP_RCM"),
                    resultSetSql.getInt("DEN_TP_MTA"),
                    resultSetSql.getDouble("PER_DEN_TP"),
                    resultSetSql.getDouble("DEN_TF_RCM"),
                    resultSetSql.getInt("COL_TP_RCM"),
                    resultSetSql.getInt("COL_TP_MTA"),
                    resultSetSql.getDouble("PER_COL_TP"),
                    resultSetSql.getDouble("COL_TF_RCM"),
                    resultSetSql.getInt("CAS_TP_RCM"),
                    resultSetSql.getInt("CAS_TP_MTA"),
                    resultSetSql.getDouble("PER_CAS_TP"),
                    resultSetSql.getDouble("CAS_RS_RCM"),
                    resultSetSql.getInt("SEM_TOTAL"),
                    resultSetSql.getInt("SEM_HOJE"),
                    resultSetSql.getString("EMAIL_REP"),
                    resultSetSql.getString("EMAIL_GER"),
                    resultSetSql.getDouble("PM_LY"),
                    resultSetSql.getInt("CD_LY"),
                    resultSetSql.getInt("PV_LY"),
                    resultSetSql.getInt("KA_LY"),
                    resultSetSql.getInt("PP_LY"),
                    resultSetSql.getDouble("VP_LY"),
                    resultSetSql.getInt("TP_LY"),
                    resultSetSql.getDouble("TF_LY"),
                    resultSetSql.getDouble("PG_LY"),
                    resultSetSql.getDouble("PR_LY"),
                    resultSetSql.getInt("BAS_TP_LY"),
                    resultSetSql.getDouble("BAS_TF_LY"),
                    resultSetSql.getInt("DEN_TP_LY"),
                    resultSetSql.getDouble("DEN_TF_LY"),
                    resultSetSql.getInt("COL_TP_LY"),
                    resultSetSql.getDouble("COL_TF_LY"),
                    resultSetSql.getInt("CAS_TP_LY"),
                    resultSetSql.getDouble("CAS_TF_LY"),
                    resultSetSql.getInt("CLN_LY"),
                    resultSetSql.getInt("WIS_TP_LY"),
                    resultSetSql.getDouble("WIS_TF_LY"),
                    resultSetSql.getInt("CAM_TP_LY"),
                    resultSetSql.getDouble("CAM_TF_LY"),
                    resultSetSql.getInt("WIS_TP_MTA"),
                    resultSetSql.getInt("CAM_TP_MTA")
            ));
        }
        closeConnection();
        resultSetSql.close();

        return dados_bid;
    }

    public ObservableList<Bid> loadBid_OLD(String data_fim, String cod_rep, String cod_colecao, String cod_marca, String familia) throws SQLException {
        ObservableList<Bid> dados_bid = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "WITH PRODS_REF AS (\n"
                        + "	SELECT CL.CODIGO COLECAO, TRIM(REPLACE(MR.DESCRICAO, 'MOOVE ','')) MARCA,\n"
                        + "		TRIM(REGEXP_REPLACE(TL.DESCRICAO, '*('||MR.DESCRICAO||'|MOOVE|FEM|MASC)*','')) LINHA, COUNT(*) QTDE\n"
                        + "	FROM SD_PRODUTO_COLECAO_001 CPRD \n"
                        + "		INNER JOIN PRODUTO_001 PD ON PD.CODIGO = CPRD.CODIGO\n"
                        + "		INNER JOIN COLECAO_001 CL ON CL.CODIGO = CPRD.COLECAO\n"
                        + "		INNER JOIN MARCA_001 MR ON MR.CODIGO = PD.MARCA\n"
                        + "		INNER JOIN TABLIN_001 TL ON TL.CODIGO = PD.LINHA\n"
                        + "		INNER JOIN TABPRECO_001 PR ON PR.CODIGO = PD.CODIGO AND PR.REGIAO IN ('F','D')\n"
                        + "	WHERE CPRD.PC = 'S'\n"
                        + "	GROUP BY CL.CODIGO, TRIM(REPLACE(MR.DESCRICAO, 'MOOVE ','')),\n"
                        + "		TRIM(REGEXP_REPLACE(TL.DESCRICAO, '*('||MR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))\n"
                        + "), PROD_BID AS (\n"
                        + "	SELECT DISTINCT\n"
                        + "		CLI.CODCLI,\n"
                        + "		CLI.NOME CLIENTE,\n"
                        + "		CLI.SIT_CLI,\n"
                        + "		CID.NOME_CID CIDADE,\n"
                        + "		CID.COD_EST UF,\n"
                        + "		REP.CODREP,\n"
                        + "		REP.NOME REPRESENT,\n"
                        + "		REP.RESPON NOME,\n"
                        + "		REP.REGIAO,\n"
                        + "		CID_REP.COD_EST UF_REP,\n"
                        + "		UPPER(GER.SITE) GERENTE,\n"
                        + "		TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')) MARCA,\n"
                        + "		LIN.SD_MARCA CODMAR,\n"
                        + "		TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*','')) LINHA,\n"
                        + "		COL.DESCRICAO COLECAO,\n"
                        + "             " + (familia.length() == 0 ? "'TODAS'" : "FAM.DESCRICAO") + " FAMILIA,\n"
                        + "		COL.CODIGO CODCOL,\n"
                        + "		SUM(PEDI.PRECO * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) \"RS_BRUTO_LIN\",\n"
                        + "		SUM(ROUND(PEDI.PRECO - (PEDI.PRECO * PED.PER_DESC / 100),2) * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) \"RS_LIQ_LIN\",\n"
                        + "		COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) \"REFS_LIN\",\n"
                        + "		SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) \"PCA_LIN\",\n"
                        + "		ROUND(SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) / \n"
                        + "		COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))),2) \"PG_LIN\",\n"
                        + "		ROUND(COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) /\n"
                        + "		(SELECT QTDE FROM PRODS_REF WT WHERE WT.MARCA = TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')) AND WT.COLECAO = COL.CODIGO AND WT.LINHA = TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) * 100,2) \"PC_LIN\",\n"
                        + "		ROUND(SUM(ROUND(PEDI.PRECO - (PEDI.PRECO * PED.PER_DESC / 100),2) * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))) /\n"
                        + "		SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')), TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '*('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)*',''))),2)\"RS_MED_LIN\",\n"
                        + "		SUM(PEDI.PRECO * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) \"RS_BRUTO_MAR\",\n"
                        + "		SUM(ROUND(PEDI.PRECO - (PEDI.PRECO * PED.PER_DESC / 100),2) * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) \"RS_LIQ_MAR\",\n"
                        + "		COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) \"REFS_MAR\",\n"
                        + "		SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) \"PCA_MAR\",\n"
                        + "		ROUND(SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) / \n"
                        + "		COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))),2) \"PG_MAR\",\n"
                        + "		ROUND(COUNT(DISTINCT PRD.CODIGO) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) /\n"
                        + "		(SELECT SUM(QTDE) FROM PRODS_REF WT WHERE WT.MARCA = TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ','')) AND WT.COLECAO = COL.CODIGO) * 100,2) \"PC_MAR\",\n"
                        + "		ROUND(SUM(ROUND(PEDI.PRECO - (PEDI.PRECO * PED.PER_DESC / 100),2) * (PEDI.QTDE + PEDI.QTDE_F)) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))) / \n"
                        + "		SUM(PEDI.QTDE + PEDI.QTDE_F) OVER (PARTITION BY CLI.CODCLI, REP.CODREP, COL.DESCRICAO, TRIM(REPLACE(MAR.DESCRICAO, 'MOOVE ',''))),2) \"RS_MED_MAR\"\n"
                        + "	FROM\n"
                        + "		ENTIDADE_001 CLI\n"
                        + "	INNER JOIN CLI_COM_001 CLIC ON\n"
                        + "		CLIC.CODCLI = CLI.CODCLI\n"
                        + "	INNER JOIN REPRESEN_001 REP ON\n"
                        + "		REP.CODREP = CLIC.CODREP\n"
                        + "	INNER JOIN CADCEP_001 CEP ON\n"
                        + "		CEP.CEP = CLI.CEP\n"
                        + "	INNER JOIN CIDADE_001 CID ON\n"
                        + "		CID.COD_CID = CEP.COD_CID\n"
                        + "	INNER JOIN CADCEP_001 CEP_REP ON\n"
                        + "		CEP_REP.CEP = REP.CEP\n"
                        + "	INNER JOIN CIDADE_001 CID_REP ON\n"
                        + "		CID_REP.COD_CID = CEP_REP.COD_CID\n"
                        + "	LEFT JOIN ENTIDADE_001 GER ON \n"
                        + "		GER.CODCLI = REP.CODCLI \n"
                        + "		AND GER.SITE IS NOT NULL\n"
                        + "	LEFT JOIN PEDIDO_001 PED ON\n"
                        + "		PED.CODCLI = CLI.CODCLI\n"
                        + "		AND PED.CODREP = REP.CODREP\n"
                        + "		AND PED.TAB_PRE IN ('F','FS','D','DS','PJ','PJI','DP','FP','DPS','FPS')\n"
                        + "	LEFT JOIN PED_ITEN_001 PEDI ON\n"
                        + "		PEDI.NUMERO = PED.NUMERO\n"
                        + "	LEFT JOIN PRODUTO_001 PRD ON\n"
                        + "		PRD.CODIGO = PEDI.CODIGO\n"
                        + "	LEFT JOIN SD_PRODUTO_COLECAO_001 COLPRD ON\n"
                        + "		COLPRD.CODIGO = PRD.CODIGO\n"
                        + "	LEFT JOIN MARCA_001 MAR ON\n"
                        + "		MAR.CODIGO = PRD.MARCA\n"
                        + "	LEFT JOIN TABLIN_001 LIN ON\n"
                        + "		LIN.CODIGO = PRD.LINHA\n"
                        + "	LEFT JOIN COLECAO_001 COL ON \n"
                        + "		COL.CODIGO = COLPRD.COLECAO\n"
                        + "     LEFT JOIN FAMILIA_001 FAM ON\n"
                        + "		FAM.CODIGO = PRD.FAMILIA"
                        + "	WHERE \n"
                        + "		PED.DT_EMISSAO BETWEEN COL.INICIO_VIG AND TO_DATE('" + data_fim + "','dd/MM/yyyy')\n"
                        + "		AND PEDI.QTDE + PEDI.QTDE_F > 0\n"
                        + "		AND REGEXP_LIKE(REP.CODREP, '*(" + cod_rep + ")')\n"
                        + "		AND REGEXP_LIKE(COL.CODIGO, '*(" + cod_colecao + ")')\n"
                        + "		AND REGEXP_LIKE(FAM.CODIGO, '*(" + familia + ")')\n"
                        + "		AND LIN.SD_MARCA = SUBSTR('" + cod_marca + "',1,1)\n"
                        + "), PROD_BID_PIVOT AS (\n"
                        + "	SELECT \n"
                        + "		*\n"
                        + "	FROM PROD_BID	\n"
                        + "		pivot (\n"
                        + "			SUM(PCA_LIN) PCA, \n"
                        + "			SUM(REFS_LIN) REFS,\n"
                        + "			SUM(RS_BRUTO_LIN) RS_BRUTO,\n"
                        + "			SUM(RS_LIQ_LIN) RS_LIQ,\n"
                        + "			SUM(PG_LIN) PG,\n"
                        + "			SUM(PC_LIN) PC,\n"
                        + "			SUM(RS_MED_LIN) RS_MED\n"
                        + "		FOR LINHA IN ('COLLECTION' col, 'DENIM' den, 'BASICO' bas, 'CASUAL' cas))\n"
                        + "), PRODUCAO AS (\n"
                        + "SELECT \n"
                        + "	CODCLI, CLIENTE, SIT_CLI, CIDADE, UF, CODREP, REPRESENT, NOME, REGIAO, UF_REP, GERENTE, MARCA, CODMAR, COLECAO, CODCOL, FAMILIA, \n"
                        + "	RS_BRUTO_MAR, RS_LIQ_MAR, REFS_MAR, PCA_MAR, PG_MAR, PC_MAR, RS_MED_MAR, \n"
                        + "	COL_PCA, COL_REFS, COL_RS_BRUTO, COL_RS_LIQ, COL_PG, COL_PC, COL_RS_MED,\n"
                        + "	DEN_PCA, DEN_REFS, DEN_RS_BRUTO, DEN_RS_LIQ, DEN_PG, DEN_PC, DEN_RS_MED,\n"
                        + "	BAS_PCA, BAS_REFS, BAS_RS_BRUTO, BAS_RS_LIQ, BAS_PG, BAS_PC, BAS_RS_MED,\n"
                        + "	CAS_PCA, CAS_REFS, CAS_RS_BRUTO, CAS_RS_LIQ, CAS_PG, CAS_PC, CAS_RS_MED,\n"
                        + "	COUNT(CODCLI) OVER (PARTITION BY COLECAO, MARCA, CODREP) CLIENTES,\n"
                        + "	COUNT(DISTINCT CIDADE||UF) OVER (PARTITION BY COLECAO, MARCA, CODREP) CIDADES,\n"
                        + "	SUM(CASE WHEN SIT_CLI = '1' THEN 1 ELSE 0 END) OVER (PARTITION BY COLECAO, MARCA, CODREP) CLIENTES_NOVOS,\n"
                        + "	SUM(CASE WHEN SIT_CLI = 'G5' THEN 1 ELSE 0 END) OVER (PARTITION BY COLECAO, MARCA, CODREP) CLIENTES_KA\n"
                        + "FROM PROD_BID_PIVOT PROD\n"
                        + ")\n"
                        + "SELECT PROD.CODREP, PROD.REPRESENT, PROD.NOME, PROD.UF_REP, PROD.REGIAO, PROD.GERENTE, PROD.MARCA, PROD.CODMAR, PROD.COLECAO, PROD.CODCOL, PROD.FAMILIA, MTA.MOSTR,\n"
                        + "	   \n"
                        + "	   ROUND(SUM(RS_LIQ_MAR)/SUM(PCA_MAR),2) PM_RCM, RPM.PM PM_RPM, MTA.PM PM_MTA, \n"
                        + "	   PROD.CIDADES CD_RCM, RPM.CD CD_RPM, MTA.CD CD_MTA, \n"
                        + "	   PROD.CLIENTES PV_RCM, RPM.PV PV_RPM, MTA.PV PV_MTA, \n"
                        + "	   PROD.CLIENTES_KA KA_RCM, RPM.KA KA_RPM, MTA.KA KA_MTA,\n"
                        + "	   ROUND(SUM(PCA_MAR)/PROD.CLIENTES,0) PP_RCM, ROUND((MTA.MOSTR * RPM.PG * RPM.PR),0) PP_RPM, MTA.PP PP_MTA,  \n"
                        + "	   ROUND(SUM(RS_LIQ_MAR)/PROD.CLIENTES,2) VP_RCM, ROUND(((MTA.MOSTR * RPM.PG * RPM.PR) * RPM.PM),2) VP_RPM, MTA.VP VP_MTA, \n"
                        + "	   SUM(PCA_MAR) TP_RCM, ROUND(((MTA.MOSTR * RPM.PG * RPM.PR) * RPM.PV),0) TP_RPM, MTA.TP TP_MTA, \n"
                        + "	   SUM(RS_LIQ_MAR) TF_RCM, ROUND((((MTA.MOSTR * RPM.PG * RPM.PR) * RPM.PM) * RPM.PV),2) TF_RPM, MTA.TF TF_MTA, \n"
                        + "	   ROUND(SUM(PCA_MAR)/SUM(REFS_MAR),2) PG_RCM, RPM.PG PG_RPM, MTA.PG PG_MTA, \n"
                        + "	   ROUND((SUM(REFS_MAR)/PROD.CLIENTES/MTA.MOSTR)*100,2) PR_RCM, RPM.PR PR_RPM, ROUND((MTA.PP/MTA.PG)/MTA.MOSTR*100,2) PR_MTA, \n"
                        + "	   PROD.CLIENTES_NOVOS CLN_RCM, MTA.CLN CLN_MTA, \n"
                        + "	   SUM(BAS_PCA) BAS_TP_RCM, BAS.TP BAS_TP_MTA, SUM(BAS_PCA)/BAS.TP*100 PER_BAS_TP, SUM(BAS_RS_LIQ) BAS_TF_RCM, \n"
                        + "	   SUM(DEN_PCA) DEN_TP_RCM, DEN.TP DEN_TP_MTA, SUM(DEN_PCA)/DEN.TP*100 PER_DEN_TP, SUM(DEN_RS_LIQ) DEN_TF_RCM,\n"
                        + "	   SUM(COL_PCA) COL_TP_RCM, COL.TP COL_TP_MTA, SUM(COL_PCA)/COL.TP*100 PER_COL_TP, SUM(COL_RS_LIQ) COL_TF_RCM, \n"
                        + "	   SUM(CAS_PCA) CAS_TP_RCM, CAS.TP CAS_TP_MTA, SUM(CAS_PCA)/CAS.TP*100 PER_CAS_TP, SUM(CAS_RS_LIQ) CAS_RS_RCM\n"
                        + "FROM PRODUCAO PROD\n"
                        + "LEFT JOIN SD_METAS_BID_001 MTA ON MTA.CODREP = PROD.CODREP AND MTA.MARCA = PROD.CODMAR AND MTA.COLECAO = PROD.CODCOL AND MTA.TIPO = 'MTA'\n"
                        + "LEFT JOIN SD_METAS_BID_001 RPM ON RPM.CODREP = PROD.CODREP AND RPM.MARCA = PROD.CODMAR AND RPM.COLECAO = PROD.CODCOL AND RPM.TIPO = 'RPM'\n"
                        + "LEFT JOIN SD_METAS_BID_001 DEN ON DEN.CODREP = PROD.CODREP AND DEN.MARCA = PROD.CODMAR AND DEN.COLECAO = PROD.CODCOL AND DEN.TIPO = 'DEN'\n"
                        + "LEFT JOIN SD_METAS_BID_001 COL ON COL.CODREP = PROD.CODREP AND COL.MARCA = PROD.CODMAR AND COL.COLECAO = PROD.CODCOL AND COL.TIPO = 'COL'\n"
                        + "LEFT JOIN SD_METAS_BID_001 CAS ON CAS.CODREP = PROD.CODREP AND CAS.MARCA = PROD.CODMAR AND CAS.COLECAO = PROD.CODCOL AND CAS.TIPO = 'CAS'\n"
                        + "LEFT JOIN SD_METAS_BID_001 BAS ON BAS.CODREP = PROD.CODREP AND BAS.MARCA = PROD.CODMAR AND BAS.COLECAO = PROD.CODCOL AND BAS.TIPO = 'BAS'\n"
                        + "GROUP BY PROD.CODREP, PROD.REPRESENT, PROD.NOME, PROD.UF_REP, PROD.REGIAO, PROD.GERENTE, PROD.MARCA, PROD.CODMAR, PROD.COLECAO, PROD.CODCOL, MTA.MOSTR, RPM.PM, MTA.PM, \n"
                        + "		 PROD.CIDADES, RPM.CD, MTA.CD, PROD.CLIENTES, RPM.PV, MTA.PV, PROD.CLIENTES_KA, RPM.KA, MTA.KA, RPM.PG, RPM.PR, MTA.PP, MTA.VP, MTA.TP, MTA.TF, MTA.PG, MTA.PR,\n"
                        + "		 PROD.CLIENTES_NOVOS, MTA.CLN, BAS.TP, DEN.TP, COL.TP, CAS.TP, PROD.FAMILIA\n"
                        + "ORDER BY 13, 12 , 11, 6, 1"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            dados_bid.add(new Bid(resultSetSql.getString("CODREP"),
                    resultSetSql.getString("REPRESENT"),
                    resultSetSql.getString("NOME"),
                    resultSetSql.getString("UF_REP"),
                    resultSetSql.getString("REGIAO"),
                    resultSetSql.getString("GERENTE"),
                    resultSetSql.getString("MARCA"),
                    resultSetSql.getString("CODMAR"),
                    resultSetSql.getString("COLECAO"),
                    resultSetSql.getString("CODCOL"),
                    resultSetSql.getString("FAMILIA"),
                    resultSetSql.getInt("MOSTR"),
                    resultSetSql.getDouble("PM_RCM"),
                    resultSetSql.getDouble("PM_RPM"),
                    resultSetSql.getDouble("PM_MTA"),
                    resultSetSql.getInt("CD_RCM"),
                    resultSetSql.getInt("CD_RPM"),
                    resultSetSql.getInt("CD_MTA"),
                    resultSetSql.getInt("PV_RCM"),
                    resultSetSql.getInt("PV_RPM"),
                    resultSetSql.getInt("PV_MTA"),
                    resultSetSql.getInt("KA_RCM"),
                    resultSetSql.getInt("KA_RPM"),
                    resultSetSql.getInt("KA_MTA"),
                    resultSetSql.getInt("PP_RCM"),
                    resultSetSql.getInt("PP_RPM"),
                    resultSetSql.getInt("PP_MTA"),
                    resultSetSql.getDouble("VP_RCM"),
                    resultSetSql.getDouble("VP_RPM"),
                    resultSetSql.getDouble("VP_MTA"),
                    resultSetSql.getInt("TP_RCM"),
                    resultSetSql.getInt("TP_RPM"),
                    resultSetSql.getInt("TP_MTA"),
                    resultSetSql.getDouble("TF_RCM"),
                    resultSetSql.getDouble("TF_RPM"),
                    resultSetSql.getDouble("TF_MTA"),
                    resultSetSql.getDouble("PG_RCM"),
                    resultSetSql.getDouble("PG_RPM"),
                    resultSetSql.getDouble("PG_MTA"),
                    resultSetSql.getDouble("PR_RCM"),
                    resultSetSql.getDouble("PR_RPM"),
                    resultSetSql.getDouble("PR_MTA"),
                    resultSetSql.getInt("CLN_RCM"),
                    resultSetSql.getInt("CLN_MTA"),
                    resultSetSql.getInt("BAS_TP_RCM"),
                    resultSetSql.getInt("BAS_TP_MTA"),
                    resultSetSql.getDouble("PER_BAS_TP"),
                    resultSetSql.getDouble("BAS_TF_RCM"),
                    resultSetSql.getInt("DEN_TP_RCM"),
                    resultSetSql.getInt("DEN_TP_MTA"),
                    resultSetSql.getDouble("PER_DEN_TP"),
                    resultSetSql.getDouble("DEN_TF_RCM"),
                    resultSetSql.getInt("COL_TP_RCM"),
                    resultSetSql.getInt("COL_TP_MTA"),
                    resultSetSql.getDouble("PER_COL_TP"),
                    resultSetSql.getDouble("COL_TF_RCM"),
                    resultSetSql.getInt("CAS_TP_RCM"),
                    resultSetSql.getInt("CAS_TP_MTA"),
                    resultSetSql.getDouble("PER_CAS_TP"),
                    resultSetSql.getDouble("CAS_RS_RCM"),
                    resultSetSql.getInt("SEM_TOTAL"),
                    resultSetSql.getInt("SEM_HOJE"),
                    resultSetSql.getString("EMAIL_REP"),
                    resultSetSql.getString("EMAIL_GER"),
                    resultSetSql.getDouble("PM_LY"),
                    resultSetSql.getInt("CD_LY"),
                    resultSetSql.getInt("PV_LY"),
                    resultSetSql.getInt("KA_LY"),
                    resultSetSql.getInt("PP_LY"),
                    resultSetSql.getDouble("VP_LY"),
                    resultSetSql.getInt("TP_LY"),
                    resultSetSql.getDouble("TF_LY"),
                    resultSetSql.getDouble("PG_LY"),
                    resultSetSql.getDouble("PR_LY"),
                    resultSetSql.getInt("BAS_TP_LY"),
                    resultSetSql.getDouble("BAS_TF_LY"),
                    resultSetSql.getInt("DEN_TP_LY"),
                    resultSetSql.getDouble("DEN_TF_LY"),
                    resultSetSql.getInt("COL_TP_LY"),
                    resultSetSql.getDouble("COL_TF_LY"),
                    resultSetSql.getInt("CAS_TP_LY"),
                    resultSetSql.getDouble("CAS_TF_LY"),
                    resultSetSql.getInt("CLN_LY"), 0, 0.0, 0, 0.0, 0, 0
            ));
        }
        closeConnection();
        resultSetSql.close();

        return dados_bid;
    }

    @Override
    public ObservableList<ComercialMetasBid> getMetasAll() throws SQLException {
        ObservableList<ComercialMetasBid> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT REP.CODREP, REP.NOME, REP.RESPON, CID.NOME_CID||'/'||CID.COD_EST CIDADE,\n"
                        + "	COL.CODIGO CODCOL, COL.DESCRICAO DESC_COL, MAR.CODIGO CODMAR, MAR.DESCRICAO DESC_MAR\n"
                        + "FROM SD_METAS_BID_001 MTA\n"
                        + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = MTA.CODREP\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = MTA.COLECAO\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = MTA.MARCA\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            metas.add(new ComercialMetasBid(resultSetSql.getString("CODREP"), resultSetSql.getString("CODMAR"), resultSetSql.getString("CODCOL"),
                    resultSetSql.getString("NOME"), resultSetSql.getString("RESPON"), resultSetSql.getString("CIDADE"), resultSetSql.getString("DESC_COL"),
                    resultSetSql.getString("DESC_MAR"), this.getIndicadoresRep(resultSetSql.getString("CODREP"), resultSetSql.getString("CODCOL"), resultSetSql.getString("CODMAR"))));
        }
        closeConnection();
        resultSetSql.close();

        return metas;
    }

    @Override
    public void deleteMetaRepr(String codrep, String marca, String colecao) throws SQLException {
        StringBuilder query = new StringBuilder(
                "DELETE FROM SD_METAS_BID_001\n"
                        + "WHERE CODREP = ? AND MARCA = ? AND COLECAO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codrep);
        preparedStatement.setString(2, marca);
        preparedStatement.setString(3, colecao);
        preparedStatement.execute();
        this.closeConnection();
    }

    @Override
    public void mergeMetaRepr(String codrep, String marca, String colecao, String tipo, String[] metas) throws SQLException {
        StringBuilder query = new StringBuilder(
                "MERGE INTO SD_METAS_BID_001 MTA USING DUAL ON (\n"
                        + "	MTA.CODREP = '" + codrep + "'\n"
                        + "	AND MTA.MARCA = '" + marca + "'\n"
                        + "	AND MTA.COLECAO = '" + colecao + "'\n"
                        + "	AND MTA.TIPO = '" + tipo + "'\n"
                        + ") WHEN MATCHED THEN UPDATE SET\n"
                        + "	MTA.MOSTR = " + metas[1] + ",\n"
                        + "	MTA.CLN = " + metas[2] + ",\n"
                        + "	MTA.PM = " + metas[3] + ",\n"
                        + "	MTA.CD = " + metas[4] + ",\n"
                        + "	MTA.PV = " + metas[5] + ",\n"
                        + "	MTA.KA = " + metas[6] + ",\n"
                        + "	MTA.PP = " + metas[7] + ",\n"
                        + "	MTA.VP = " + metas[8] + ",\n"
                        + "	MTA.TP = " + metas[9] + ",\n"
                        + "	MTA.TF = " + metas[10] + ",\n"
                        + "	MTA.PG = " + metas[11] + ",\n"
                        + "	MTA.PR = " + (Double.parseDouble(metas[12]) / 100.0) + "\n"
                        + "WHEN NOT MATCHED THEN INSERT \n"
                        + "(CODREP, MARCA, COLECAO, TIPO, MOSTR, CLN, PM, CD, PV, KA, PP, VP, TP, TF, PG, PR)\n"
                        + "VALUES ('" + codrep + "', '" + marca + "', '" + colecao + "', '" + tipo + "', " + metas[1]
                        + ", " + metas[2] + ", " + metas[3] + ", " + metas[4] + ", " + metas[5] + ", " + metas[6] + ", " + metas[7] + ", "
                        + metas[8] + ", " + metas[9] + ", " + metas[10] + ", " + metas[11] + ", " + (Double.parseDouble(metas[12]) / 100.0) + ")");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        this.closeConnection();
    }

    @Override
    public ObservableList<ComercialMetasBid> getMetasReps(String codrep, String marca, String codcol) throws SQLException {
        ObservableList<ComercialMetasBid> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT REP.CODREP, REP.NOME, REP.RESPON, CID.NOME_CID||'/'||CID.COD_EST CIDADE,\n"
                        + "	COL.CODIGO CODCOL, COL.DESCRICAO DESC_COL, MAR.CODIGO CODMAR, MAR.DESCRICAO DESC_MAR\n"
                        + "FROM SD_METAS_BID_001 MTA\n"
                        + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = MTA.CODREP\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = MTA.COLECAO\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = MTA.MARCA\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE MAR.CODIGO = '" + marca + "'\n"
                        + "      AND REGEXP_LIKE(COL.CODIGO, '*(" + codcol + ")')\n"
                        + "      AND REGEXP_LIKE(REP.CODREP,'*(" + codrep + ")')");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            metas.add(new ComercialMetasBid(resultSetSql.getString("CODREP"), resultSetSql.getString("CODMAR"), resultSetSql.getString("CODCOL"),
                    resultSetSql.getString("NOME"), resultSetSql.getString("RESPON"), resultSetSql.getString("CIDADE"), resultSetSql.getString("DESC_COL"),
                    resultSetSql.getString("DESC_MAR"), this.getIndicadoresRep(resultSetSql.getString("CODREP"), resultSetSql.getString("CODCOL"), resultSetSql.getString("CODMAR"))));
        }
        closeConnection();
        resultSetSql.close();

        return metas;
    }

    @Override
    public ComercialMetasBid getMetaReps(String codrep, String marca, String codcol) throws SQLException {
        ComercialMetasBid meta = null;

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT REP.CODREP, REP.NOME, REP.RESPON, CID.NOME_CID||'/'||CID.COD_EST CIDADE,\n"
                        + "	COL.CODIGO CODCOL, COL.DESCRICAO DESC_COL, MAR.CODIGO CODMAR, MAR.DESCRICAO DESC_MAR\n"
                        + "FROM SD_METAS_BID_001 MTA\n"
                        + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = MTA.CODREP\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = MTA.COLECAO\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = MTA.MARCA\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE MAR.CODIGO = '" + marca + "'\n"
                        + "      AND COL.CODIGO = '" + codcol + "'\n"
                        + "      AND REP.CODREP = '" + codrep + "'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            meta = new ComercialMetasBid(resultSetSql.getString("CODREP"), resultSetSql.getString("CODMAR"), resultSetSql.getString("CODCOL"),
                    resultSetSql.getString("NOME"), resultSetSql.getString("RESPON"), resultSetSql.getString("CIDADE"), resultSetSql.getString("DESC_COL"),
                    resultSetSql.getString("DESC_MAR"), this.getIndicadoresRep(resultSetSql.getString("CODREP"), resultSetSql.getString("CODCOL"), resultSetSql.getString("CODMAR")));
        }
        closeConnection();
        resultSetSql.close();

        return meta;
    }

    @Override
    public ObservableList<ComercialMetasBidPremiado> getMetasPremioAll() throws SQLException {
        ObservableList<ComercialMetasBidPremiado> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT BID.MARCA, MAR.DESCRICAO DESC_MARCA, BID.COLECAO, COL.DESCRICAO DESC_COLECAO\n"
                        + "FROM SD_METAS_BID_PREMIADO_001 BID\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = BID.MARCA\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = BID.COLECAO\n"
                        + "GROUP BY BID.MARCA, MAR.DESCRICAO, BID.COLECAO, COL.DESCRICAO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            metas.add(new ComercialMetasBidPremiado(resultSetSql.getString("COLECAO"), resultSetSql.getString("DESC_COLECAO"), resultSetSql.getString("MARCA"),
                    resultSetSql.getString("DESC_MARCA"), this.getIndicadoresPremio(resultSetSql.getString("COLECAO"), resultSetSql.getString("MARCA"))));
        }
        closeConnection();
        resultSetSql.close();

        return metas;
    }

    @Override
    public ObservableList<ComercialMetasBidPremiado> getMetasPremioByWindowsForm(String marca, String colecao) throws SQLException {
        ObservableList<ComercialMetasBidPremiado> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT BID.MARCA, MAR.DESCRICAO DESC_MARCA, BID.COLECAO, COL.DESCRICAO DESC_COLECAO\n"
                        + "FROM SD_METAS_BID_PREMIADO_001 BID\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = BID.MARCA\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = BID.COLECAO\n"
                        + "WHERE REGEXP_LIKE(BID.MARCA, '*(" + marca + ")') OR REGEXP_LIKE(BID.COLECAO, '*(" + colecao + ")')\n"
                        + "GROUP BY BID.MARCA, MAR.DESCRICAO, BID.COLECAO, COL.DESCRICAO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            metas.add(new ComercialMetasBidPremiado(resultSetSql.getString("COLECAO"), resultSetSql.getString("DESC_COLECAO"), resultSetSql.getString("MARCA"),
                    resultSetSql.getString("DESC_MARCA"), this.getIndicadoresPremio(resultSetSql.getString("COLECAO"), resultSetSql.getString("MARCA"))));
        }
        closeConnection();
        resultSetSql.close();

        return metas;
    }

    @Override
    public ComercialMetasBidPremiado getMetaPremio(String marca, String colecao) throws SQLException {
        ComercialMetasBidPremiado metas = null;

        StringBuilder query = new StringBuilder(
                "SELECT BID.MARCA, MAR.DESCRICAO DESC_MARCA, BID.COLECAO, COL.DESCRICAO DESC_COLECAO\n"
                        + "FROM SD_METAS_BID_PREMIADO_001 BID\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = BID.MARCA\n"
                        + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = BID.COLECAO\n"
                        + "WHERE REGEXP_LIKE(BID.MARCA, '*(" + marca + ")') OR REGEXP_LIKE(BID.COLECAO, '*(" + colecao + ")')\n"
                        + "GROUP BY BID.MARCA, MAR.DESCRICAO, BID.COLECAO, COL.DESCRICAO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            metas = new ComercialMetasBidPremiado(resultSetSql.getString("COLECAO"), resultSetSql.getString("DESC_COLECAO"), resultSetSql.getString("MARCA"),
                    resultSetSql.getString("DESC_MARCA"), this.getIndicadoresPremio(resultSetSql.getString("COLECAO"), resultSetSql.getString("MARCA")));
        }
        closeConnection();
        resultSetSql.close();

        return metas;
    }

    @Override
    public void deleteMetaPremio(String marca, String colecao) throws SQLException {
        StringBuilder query = new StringBuilder(
                "DELETE FROM SD_METAS_BID_PREMIADO_001\n"
                        + "WHERE MARCA = ? AND COLECAO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, marca);
        preparedStatement.setString(2, colecao);
        preparedStatement.execute();
        this.closeConnection();
    }

    @Override
    public void mergeMetaPremio(String marca, String colecao, String indicador, String graduacao, String perc_premio, String obrigatorio, Integer ordem) throws SQLException {
        StringBuilder query = new StringBuilder(
                "MERGE INTO SD_METAS_BID_PREMIADO_001 BID USING DUAL ON (\n"
                        + "	BID.MARCA = '" + marca + "'\n"
                        + "	AND BID.COLECAO = '" + colecao + "' \n"
                        + "	AND BID.INDICADOR = '" + indicador + "'\n"
                        + ") WHEN MATCHED THEN UPDATE SET\n"
                        + "	BID.GRADUACAO = " + graduacao + ",\n"
                        + "	BID.PERC_PREMIO = " + perc_premio + ",\n"
                        + "	BID.OBRIGATORIO = '" + obrigatorio + "'\n"
                        + "WHEN NOT MATCHED THEN INSERT\n"
                        + "	(MARCA, COLECAO, INDICADOR, GRADUACAO, PERC_PREMIO, OBRIGATORIO, ORDEM)\n"
                        + "	VALUES ('" + marca + "', '" + colecao + "', '" + indicador + "', " + graduacao + ", " + perc_premio + ", '" + obrigatorio + "' , " + ordem + ")");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        this.closeConnection();
    }

    @Override
    public ObservableList<BidPremiado> getBidPremiadoRep(String codrep, String codmar, String codcol) throws SQLException {
        ObservableList<BidPremiado> metas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT distinct\n" +
                        "  INDICADOR, OBRIGATORIO, REAL_META, GRADUACAO, \n" +
                        "  ROUND((REAL_META/100)*GRADUACAO,2) REALIZADO,\n" +
                        "  PERC_PREMIO, \n" +
                        "  ROUND(TF_MTA* CASE WHEN REAL_META < 100 THEN ((REAL_META/100)*GRADUACAO) ELSE (GRADUACAO) END *(PERC_PREMIO/100)/100,2) VAL_PREMIO,\n" +
                        "     ORDEM\n" +
                        "FROM sd_bid_premiado_001\n" +
                        "  WHERE\n" +
                        "    MARCA = '" + codmar + "'\n" +
                        "    and COLECAO = '" + codcol + "'\n" +
                        "    and codrep = '" + codrep + "'\n" +
                        "  ORDER BY ORDEM");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        Double rel_meta = 0.0;
        Double graduacao = 0.0;
        Double realizado = 0.0;
        Double per_premio = 0.0;
        Double val_premio = 0.0;

        while (resultSetSql.next()) {
            metas.add(new BidPremiado(resultSetSql.getString("INDICADOR"), resultSetSql.getString("OBRIGATORIO"), resultSetSql.getDouble("REAL_META"),
                    resultSetSql.getDouble("GRADUACAO"), resultSetSql.getDouble("REALIZADO"), resultSetSql.getDouble("PERC_PREMIO"), resultSetSql.getDouble("VAL_PREMIO")));
            rel_meta += resultSetSql.getDouble("REAL_META");
            graduacao += resultSetSql.getDouble("GRADUACAO");
            realizado += resultSetSql.getDouble("REALIZADO");
            per_premio += resultSetSql.getDouble("PERC_PREMIO");
            val_premio += resultSetSql.getDouble("VAL_PREMIO");
        }

        closeConnection();
        resultSetSql.close();
        BidPremiado bidPremiadoTotal = new BidPremiado("Total", null, rel_meta / 10.0, graduacao, realizado, per_premio / 10.0, val_premio);
        metas.add(bidPremiadoTotal);
        return metas;
    }

    @Override
    public ObservableList<BidClienteIndicadores> getBidClienteIndicadores(String codcli, String codmar, String codcol, String linha) throws SQLException {
        ObservableList<BidClienteIndicadores> indicadores = FXCollections.observableArrayList();

        String col_ant = (Integer.parseInt(codcol) - 100) + "";

        StringBuilder query = new StringBuilder(
                "SELECT LINHA, DECODE(INDICADOR, \n"
                        + "			'PG', 'PG - Profundidade Grade',\n"
                        + "			'PM', 'PM - Preço Médio',\n"
                        + "			'PR', 'PR - Produtividade Referências',\n"
                        + "         'RF', 'RF - Referências',\n"
                        + "			'TF', 'TF - Total Financeiro',\n"
                        + "			'TP', 'TP - Total de Peças') INDICADOR, \n"
                        + "	NVL(ATUAL,0) ATUAL, NVL(ANT,0) ANT, \n"
                        + "	CASE WHEN NVL(ANT,0) > 0 THEN ROUND((NVL(ATUAL,0)/NVL(ANT,0))-1,4) ELSE 0 END COMP\n"
                        + "FROM (SELECT CODCOL, LINHA, INDICADOR, QTDE FROM SD_BID_CLIENTE_001 BID\n"
                        + "WHERE (BID.CODCOL = '" + codcol + "' OR BID.CODCOL = ('" + codcol + "'-100))\n"
                        + "	AND BID.CODCLI = '" + codcli + "'\n"
                        + "	AND BID.CODMAR = '" + codmar + "'\n"
                        + "     AND BID.LINHA = '" + linha + "')\n"
                        + "PIVOT(\n"
                        + "	SUM(QTDE) FOR CODCOL IN ('" + col_ant + "' ANT, '" + codcol + "' ATUAL)\n"
                        + ")\n"
                        + "ORDER BY LINHA, INDICADOR"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            indicadores.add(new BidClienteIndicadores(resultSetSql.getString("LINHA"), resultSetSql.getString("INDICADOR"),
                    resultSetSql.getDouble("ATUAL"), resultSetSql.getDouble("ANT"), resultSetSql.getDouble("COMP")));
        }
        resultSetSql.close();
        closeConnection();
        return indicadores;
    }

    @Override
    public ObservableList<BidCliente> getBidCliente(String codrep, String codcli, String cidade, String colecao, String marca, String compra) throws SQLException {
        ObservableList<BidCliente> bid = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT * FROM (\n"
                + "SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                + "		CASE WHEN ATUAL = 1 AND ANT = 1 THEN 'AMBAS' ELSE\n"
                + "			CASE WHEN ATUAL = 1 AND ANT = 0 THEN 'ATUAL' ELSE\n"
                + "				CASE WHEN ATUAL = 0 AND ANT = 1 THEN 'ANTERIOR' ELSE 'NA' END\n"
                + "			END\n"
                + "		END COMPRA\n"
                + "FROM (SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                + "		SUM(CASE WHEN CODCOL = '" + colecao + "' THEN 1 ELSE 0 END) ATUAL,\n"
                + "		SUM(CASE WHEN CODCOL = '" + colecao + "'-100 THEN 1 ELSE 0 END) ANT\n"
                + "FROM (SELECT DISTINCT BID.CODCLI, BID.CLIENTE, BID.CIDADE, BID.UF, REP.CODREP, REP.NOME REPRESENT, BID.CODMAR, BID.MARCA, BID.CODCOL\n"
                + "FROM SD_BID_CLIENTE_001 BID \n"
                + "INNER JOIN CLI_COM_001 CC ON CC.CODCLI = BID.CODCLI AND (CC.MARCA = BID.CODMAR OR CC.MARCA = '1')\n"
                + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = CC.CODREP\n"
                + "WHERE (BID.CODCOL = '" + colecao + "' OR BID.CODCOL = ('" + colecao + "'-100))\n"
                + "      AND REGEXP_LIKE(BID.CODMAR, '*(" + marca.replace(';', '|') + ")')\n"
                + "      AND REGEXP_LIKE(BID.CODCLI, '*(" + codcli.replace(';', '|') + ")')\n"
                + "      AND REGEXP_LIKE(BID.CIDADE, '*(" + cidade.replace(';', '|') + ")')\n"
                + "      AND REGEXP_LIKE(REP.CODREP, '*(" + codrep.replace(';', '|') + ")'))\n"
                + "GROUP BY CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA))\n"
                + "WHERE REGEXP_LIKE(COMPRA, '*(" + compra + ")')\n");
        if (codcli.isEmpty() && cidade.isEmpty() && codrep.isEmpty()) {
            query.append(""
                    + "UNION\n"
                    + "SELECT * FROM (\n"
                    + "SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                    + "		CASE WHEN ATUAL = 1 AND ANT = 1 THEN 'AMBAS' ELSE\n"
                    + "			CASE WHEN ATUAL = 1 AND ANT = 0 THEN 'ATUAL' ELSE\n"
                    + "				CASE WHEN ATUAL = 0 AND ANT = 1 THEN 'ANTERIOR' ELSE 'NA' END\n"
                    + "			END\n"
                    + "		END COMPRA\n"
                    + "FROM (SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                    + "		SUM(CASE WHEN CODCOL = '" + colecao + "' THEN 1 ELSE 0 END) ATUAL,\n"
                    + "		SUM(CASE WHEN CODCOL = '" + colecao + "'-100 THEN 1 ELSE 0 END) ANT\n"
                    + "FROM (SELECT DISTINCT BID.CODCLI, BID.CLIENTE, BID.CIDADE, BID.UF, BID.CODREP, BID.REPRESENT, BID.CODMAR, BID.MARCA, BID.CODCOL\n"
                    + "FROM SD_BID_CLIENTE_001 BID \n"
                    + "WHERE (BID.CODCOL = '" + colecao + "' OR BID.CODCOL = ('" + colecao + "'-100))\n"
                    + "      AND REGEXP_LIKE(BID.CODMAR, '*(" + marca.replace(';', '|') + ")')\n"
                    + "      AND BID.CODCLI = '000'\n"
                    + "      AND BID.CODREP = '000')\n"
                    + "GROUP BY CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA))\n");
        } else if (codcli.isEmpty() && cidade.isEmpty()) {
            query.append(""
                    + "UNION\n"
                    + "SELECT * FROM (\n"
                    + "SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                    + "		CASE WHEN ATUAL = 1 AND ANT = 1 THEN 'AMBAS' ELSE\n"
                    + "			CASE WHEN ATUAL = 1 AND ANT = 0 THEN 'ATUAL' ELSE\n"
                    + "				CASE WHEN ATUAL = 0 AND ANT = 1 THEN 'ANTERIOR' ELSE 'NA' END\n"
                    + "			END\n"
                    + "		END COMPRA\n"
                    + "FROM (SELECT CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA,\n"
                    + "		SUM(CASE WHEN CODCOL = '" + colecao + "' THEN 1 ELSE 0 END) ATUAL,\n"
                    + "		SUM(CASE WHEN CODCOL = '" + colecao + "'-100 THEN 1 ELSE 0 END) ANT\n"
                    + "FROM (SELECT DISTINCT BID.CODCLI, BID.CLIENTE, BID.CIDADE, BID.UF, BID.CODREP, BID.REPRESENT, BID.CODMAR, BID.MARCA, BID.CODCOL\n"
                    + "FROM SD_BID_CLIENTE_001 BID \n"
                    + "WHERE (BID.CODCOL = '" + colecao + "' OR BID.CODCOL = ('" + colecao + "'-100))\n"
                    + "      AND REGEXP_LIKE(BID.CODMAR, '*(" + marca.replace(';', '|') + ")')\n"
                    + "      AND REGEXP_LIKE(BID.CODCLI, '*(" + codrep.replace(';', '|') + ")')\n"
                    + "      AND REGEXP_LIKE(BID.CODREP, '*(" + codrep.replace(';', '|') + ")'))"
                    + "GROUP BY CODCLI, CLIENTE, CIDADE, UF, CODREP, REPRESENT, CODMAR, MARCA))\n");
        }
        query.append("ORDER BY 6, 4, 3, 2");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            bid.add(new BidCliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("CLIENTE"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("CODREP"), resultSetSql.getString("REPRESENT"), resultSetSql.getString("CODMAR"),
                    resultSetSql.getString("MARCA"), resultSetSql.getString("COMPRA"), null));
        }
        closeConnection();
        resultSetSql.close();
        return bid;
    }

    private List<String> breakInValues(String[] inValues) {
        List<String> newInValues = new ArrayList<>();

        int divInValues = (inValues.length / 1000) + 1;
        int valueInitial = inValues.length;
        int valueFinal = 0;

        for (int i = 1; i <= divInValues; i++) {
            valueFinal = valueInitial - 1000;
            int j = 0;
            String inValue = "";
            for (j = valueInitial; (j > valueFinal && j > 0); j--) {
                inValue += inValues[j - 1] + ",";
            }
            newInValues.add(inValue.substring(0, inValue.length() - 1));
            valueInitial = j;
        }

        return newInValues;
    }

    @Override
    public ObservableList<BidClienteIndicadores> getBidClienteIndicadoresTotais(String codcli, String codmar, String codcol, String linha) throws SQLException {
        ObservableList<BidClienteIndicadores> indicadores = FXCollections.observableArrayList();

        String col_ant = (Integer.parseInt(codcol) - 100) + "";

        String inCodCli = "BID.CODCLI IN (" + codcli + ")\n";
        String[] codsCliente = codcli.split(",");
        if (codsCliente.length >= 1000) {
            inCodCli = "";
            inCodCli += "(1 <> 1\n";
            for (String inValue : breakInValues(codsCliente)) {
                inCodCli += " or BID.CODCLI IN (" + inValue + ")\n";
            }
            inCodCli += ")\n";
        }

        StringBuilder query = new StringBuilder(
                "SELECT LINHA, DECODE(INDICADOR, \n"
                        + "			'PG', 'PG - Profundidade Grade',\n"
                        + "			'PM', 'PM - Preço Médio',\n"
                        + "			'PR', 'PR - Produtividade Referências',\n"
                        + "            'RF', 'RF - Referências',\n"
                        + "			'TF', 'TF - Total Financeiro',\n"
                        + "			'TP', 'TP - Total de Peças') INDICADOR, \n"
                        + "	NVL(ATUAL,0) ATUAL, NVL(ANT,0) ANT, \n"
                        + "	CASE WHEN NVL(ANT,0) > 0 THEN ROUND((NVL(ATUAL,0)/NVL(ANT,0))-1,4) ELSE 0 END COMP\n"
                        + "FROM (SELECT CODCOL, REPLACE(LINHA,' CAMISA','') LINHA, INDICADOR, \n"
                        + "		CASE WHEN INDICADOR IN ('PG','PR','PM') THEN AVG(QTDE) ELSE SUM(QTDE) END QTDE	\n"
                        + "FROM SD_BID_CLIENTE_001 BID\n"
                        + "WHERE (BID.CODCOL = '" + codcol + "' OR BID.CODCOL = ('" + codcol + "'-100))\n"
                        + "      AND REGEXP_LIKE(BID.CODMAR, '*(" + codmar.replace(';', '|') + ")')\n"
                        + "      AND " + inCodCli + " "
                        + "     AND REGEXP_LIKE(REPLACE(LINHA,' CAMISA',''), '*(" + linha + ")')\n"
                        + "     GROUP BY CODCOL, REPLACE(LINHA,' CAMISA',''), INDICADOR)\n"
                        + "PIVOT(\n"
                        + "	SUM(QTDE) FOR CODCOL IN ('" + col_ant + "' ANT, '" + codcol + "' ATUAL)\n"
                        + ")\n"
                        + "ORDER BY LINHA, INDICADOR"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            indicadores.add(new BidClienteIndicadores(resultSetSql.getString("LINHA"), resultSetSql.getString("INDICADOR"),
                    resultSetSql.getDouble("ATUAL"), resultSetSql.getDouble("ANT"), resultSetSql.getDouble("COMP")));
        }

        resultSetSql.close();
        closeConnection();
        return indicadores;
    }

    @Override
    public Map<String, IndicadoresBidLinha> getBidLinha(String codrep, String colecao, String marca) throws SQLException {
        Map<String, IndicadoresBidLinha> bidLinha = new HashMap<String, IndicadoresBidLinha>();

        StringBuilder query = new StringBuilder(
                "select decode(meta.tipo,\n"
                        + "              'BAS',\n"
                        + "              'ESSENTIAL',\n"
                        + "              'DEN',\n"
                        + "              'DENIM',\n"
                        + "              'COL',\n"
                        + "              'COLLECTION',\n"
                        + "              'CAS',\n"
                        + "              'CASUAL',\n"
                        + "              'WIS',\n"
                        + "              'WISHES',\n"
                        + "              'CAM',\n"
                        + "              'CAMISA',meta.tipo) linha,\n"
                        + "       nvl(vendas.qtde_pecas,0) qtde_pecas,\n"
                        + "       nvl(meta.tp, 0) meta_tp,\n"
                        + "       f_verifica_divisor(nvl(vendas.qtde_pecas,0), meta.tp) * 100 att_tp,\n"
                        + "       nvl(l_year.qtde_pecas, 0) ly_tp,\n"
                        + "       nvl(vendas.vlr_liq_itens,0) vlr_liq_itens,\n"
                        + "       nvl(meta.tf, 0) meta_tf,\n"
                        + "       f_verifica_divisor(nvl(vendas.vlr_liq_itens,0), meta.tf) * 100 att_tf,\n"
                        + "       nvl(l_year.vlr_liq_itens, 0) ly_tf,\n"
                        + "       f_verifica_divisor(nvl(vendas.qtde_pecas,0), nvl(vendas.qtde_refs,0)) pg,\n"
                        + "       f_verifica_divisor(f_verifica_divisor(nvl(vendas.qtde_refs,0),\n"
                        + "                                             nvl(vendas.qtde_clis,0)),\n"
                        + "                          nvl(mostruario.QTDE_MOSTRUARIO,0)) * 100 pr\n"
                        + "  from v_sd_metas_bid_com_global meta\n"
                        + "  left join v_sd_venda_rep_mar_col_lin vendas\n"
                        + "    on vendas.codrep = meta.codrep\n"
                        + "   and vendas.marca = meta.marca\n"
                        + "   and vendas.colecao = meta.colecao\n"
                        + "   and decode(vendas.linha,\n"
                        + "              'ESSENTIAL',\n"
                        + "              'BAS',\n"
                        + "              'DENIM',\n"
                        + "              'DEN',\n"
                        + "              'COLLECTION',\n"
                        + "              'COL',\n"
                        + "              'CASUAL',\n"
                        + "              'CAS',\n"
                        + "              'WISHES',\n"
                        + "              'WIS',\n"
                        + "              'CAMISA',\n"
                        + "              'CAM') = meta.tipo\n"
                        + "  left join v_sd_venda_rep_mar_col_lin l_year\n"
                        + "    on l_year.codrep = vendas.codrep\n"
                        + "   and l_year.marca = vendas.marca\n"
                        + "   and l_year.linha = vendas.linha\n"
                        + "   and to_number(l_year.colecao) = to_number(vendas.colecao) - 100\n"
                        + "  left join v_sd_mostruario_linha mostruario\n"
                        + "    on mostruario.COLECAO = vendas.colecao\n"
                        + "   and mostruario.MARCA = vendas.marca\n"
                        + "   and mostruario.DESC_LINHA = vendas.linha\n"
                        + " where meta.tipo not in ('RPM','MTA')\n"
                        + "   and meta.colecao = ?\n"
                        + "   and meta.marca = ?\n"
                        + "   and meta.codrep = ?"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, colecao);
        preparedStatement.setString(2, marca);
        preparedStatement.setString(3, codrep);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            bidLinha.put(resultSetSql.getString("linha"), new IndicadoresBidLinha(resultSetSql.getString("linha"), resultSetSql.getInt("qtde_pecas"), resultSetSql.getInt("meta_tp"),
                    resultSetSql.getDouble("att_tp"), resultSetSql.getInt("ly_tp"), resultSetSql.getDouble("vlr_liq_itens"), resultSetSql.getDouble("meta_tf"), resultSetSql.getDouble("att_tf"),
                    resultSetSql.getDouble("ly_tf"), resultSetSql.getDouble("pg"), resultSetSql.getDouble("pr")));
        }

        resultSetSql.close();
        closeConnection();
        return bidLinha;
    }

}
