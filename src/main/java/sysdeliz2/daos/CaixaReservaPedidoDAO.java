package sysdeliz2.daos;

import sysdeliz2.models.*;
import sysdeliz2.utils.AcessoSistema;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface CaixaReservaPedidoDAO {

    List<CaixaReservaPedido> loadByPedidoAndReserva(String pPedido, String pReserva) throws SQLException;

    List<ProdutoCaixaReservaPedido> loadProdutosByPedidoAndReservaAndCaixa(String pPedido, String pReserva, String pCaixa) throws SQLException;

    void saveBarra(String pedido, String reserva, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto, AcessoSistema usuario) throws SQLException;

    void deleteBarra(String pedido, String reserva, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto) throws SQLException;

    void deleteCaixa(String pedido, String reserva, CaixaReservaPedido caixa) throws SQLException;

    void updStatusCaixaReserva(String pedido, String reserva, CaixaReservaPedido caixa) throws SQLException;

    CaixaReservaPedido newNovaCaixa() throws SQLException;

    CaixaReservaPedido getCaixaByProduto(ReservaPedido reserva, ProdutosReservaPedido produto) throws SQLException;

    @Deprecated
    void deleteReferenciaCaixa(ReservaPedido reserva, CaixaReservaPedido caixa, ProdutosReservaPedido produto) throws SQLException;

    List<String> getRastreioBarra(String barra) throws SQLException;

    ///Métodos para movimentação do Ti
    void adicionaBarra_PEDIDO3(String pedido, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra, AcessoSistema usuario) throws SQLException;

    void removeBarra_PEDIDO3(String pedido, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto) throws SQLException;

    @Deprecated
    void removeReferencia_PEDIDO3(ReservaPedido reserva, CaixaReservaPedido caixa, ProdutosReservaPedido produto) throws SQLException;

    void alteraPesoCaixas_PEDIDO3(String pedido, CaixaReservaPedido caixa) throws SQLException;

    @Deprecated
    void removeItem_PED_RESERVA(ReservaPedido reserva, ProdutoCaixaReservaPedido produto) throws SQLException;

    @Deprecated
    void adicionaItem_PED_RESERVA(ReservaPedido reserva, ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra) throws SQLException;

    void removeItem_PA_ITEM(ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra) throws SQLException;

    void adicionaItem_PA_ITEM(ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra) throws SQLException;

    void adicionaMov_PA_MOV(String pedido, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra, String tp_mov, AcessoSistema usuario) throws SQLException;

    void adicionaLogPrimeiraCaixa(ReservaPedido reserva) throws SQLException;

    void adicionaLogEncerramentoCaixa(ReservaPedido reserva, CaixaReservaPedido caixa) throws SQLException;

    void adicionaLogEncerramentoReserva(ReservaPedido reserva) throws SQLException;

    void adicionaLogRemocaoBarra(ReservaPedido reserva, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto) throws SQLException;

}
