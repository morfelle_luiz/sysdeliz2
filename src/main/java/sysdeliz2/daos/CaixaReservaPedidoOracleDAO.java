/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.models.*;
import sysdeliz2.utils.AcessoSistema;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class CaixaReservaPedidoOracleDAO implements CaixaReservaPedidoDAO {
    
    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    
    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }
    
    @Override
    public List<CaixaReservaPedido> loadByPedidoAndReserva(String pPedido, String pReserva) throws SQLException {
        List<CaixaReservaPedido> caixasReserva = new ArrayList<>();
        
        StringBuilder query = new StringBuilder(
                "SELECT CRP.CAIXA, CRP.STATUS, COUNT(CRP.BARRA) QTDE\n" +
                        "  FROM SD_CAIXA_RESERVA_PEDIDO_001 CRP\n" +
                        " INNER JOIN SD_RESERVA_PEDIDO_001 RES\n" +
                        "    ON RES.PEDIDO = CRP.PEDIDO\n" +
                        "   AND RES.RESERVA = CRP.RESERVA\n" +
                        "   AND RES.CODIGO = CRP.CODIGO\n" +
                        "   AND RES.COR = CRP.COR\n" +
                        "   AND RES.TAM = CRP.TAM\n" +
                        " WHERE RES.PED_AGRUPADOR = ?\n" +
                        "   AND CRP.RESERVA = ?\n" +
                        " GROUP BY CRP.CAIXA, CRP.STATUS\n" +
                        " ORDER BY CRP.STATUS");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pReserva);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            caixasReserva.add(new CaixaReservaPedido(resultSetSql.getString("CAIXA"), resultSetSql.getString("STATUS"),
                    Integer.parseInt(resultSetSql.getString("QTDE"))));
        }
        resultSetSql.close();
        for (CaixaReservaPedido caixa : caixasReserva) {
            caixa.setListProdutosCaixa(FXCollections
                    .observableList(loadProdutosByPedidoAndReservaAndCaixa(pPedido, pReserva, caixa.getStrCaixa())));
        }
        
        return caixasReserva;
    }
    
    @Override
    public List<ProdutoCaixaReservaPedido> loadProdutosByPedidoAndReservaAndCaixa(String pPedido, String pReserva,
                                                                                  String pCaixa) throws SQLException {
        List<ProdutoCaixaReservaPedido> produtosCaixa = new ArrayList<>();
        
        StringBuilder query = new StringBuilder(
                "SELECT CRP.PEDIDO,\n" +
                        "       CRP.RESERVA,\n" +
                        "       CRP.CODIGO,\n" +
                        "       CRP.COR,\n" +
                        "       CRP.TAM,\n" +
                        "       CRP.BARRA,\n" +
                        "       PROD.DESCRICAO,\n" +
                        "       CASE\n" +
                        "         WHEN PROD.COLECAO = 'MKT' THEN\n" +
                        "          'MKT'\n" +
                        "         ELSE\n" +
                        "          'COM'\n" +
                        "       END TIPO\n" +
                        "  FROM SD_CAIXA_RESERVA_PEDIDO_001 CRP\n" +
                        " INNER JOIN PRODUTO_001 PROD\n" +
                        "    ON (PROD.CODIGO = CRP.CODIGO)\n" +
                        " INNER JOIN SD_RESERVA_PEDIDO_001 RES\n" +
                        "    ON RES.PEDIDO = CRP.PEDIDO\n" +
                        "   AND RES.RESERVA = CRP.RESERVA\n" +
                        "   AND RES.CODIGO = CRP.CODIGO\n" +
                        "   AND RES.COR = CRP.COR\n" +
                        "   AND RES.TAM = CRP.TAM\n" +
                        " WHERE RES.PED_AGRUPADOR = ?\n" +
                        "   AND CRP.RESERVA = ?\n" +
                        "   AND CAIXA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pReserva);
        preparedStatement.setString(3, pCaixa);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            produtosCaixa.add(new ProdutoCaixaReservaPedido(resultSetSql.getString("PEDIDO"), resultSetSql.getString("RESERVA"), resultSetSql.getString("CODIGO"),
                    resultSetSql.getString("COR"), resultSetSql.getString("TAM"), resultSetSql.getString("BARRA"),
                    resultSetSql.getString("DESCRICAO"), resultSetSql.getString("TIPO")));
        }
        resultSetSql.close();
        
        return produtosCaixa;
    }
    
    @Override
    public void saveBarra(String pedido, String reserva, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto,
                          AcessoSistema usuario) throws SQLException {
        
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_CAIXA_RESERVA_PEDIDO_001\n"
                + "(CAIXA, PEDIDO, RESERVA, BARRA, CODIGO, COR, TAM, STATUS, USUARIO, DATA_LANC)\n"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE)");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, StringUtils.leftPad(caixa.getStrCaixa(), 6, "0"));
        preparedStatement.setString(2, pedido);
        preparedStatement.setString(3, reserva);
        preparedStatement.setString(4, produto.getStrBarra());
        preparedStatement.setString(5, produto.getStrCodigo());
        preparedStatement.setString(6, produto.getStrCor());
        preparedStatement.setString(7, produto.getStrTam());
        preparedStatement.setString(8, caixa.getStrStatus());
        preparedStatement.setString(9, usuario.getUsuario());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void updStatusCaixaReserva(String pedido, String reserva, CaixaReservaPedido caixa) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "UPDATE SD_CAIXA_RESERVA_PEDIDO_001 \n"
                + "SET STATUS = ?\n"
                + "WHERE PEDIDO = ? AND RESERVA = ? AND CAIXA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, caixa.getStrStatus());
        preparedStatement.setString(2, pedido);
        preparedStatement.setString(3, reserva);
        preparedStatement.setString(4, caixa.getStrCaixa());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public CaixaReservaPedido newNovaCaixa() throws SQLException {
        CaixaReservaPedido caixa = new CaixaReservaPedido();
        
        StringBuilder query = new StringBuilder(
                "" + "SELECT LPAD(SD_SEQ_NUM_CX_RES_PED.NEXTVAL, 6,'0') NUM_CAIXA FROM DUAL");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            caixa.setStrCaixa(resultSetSql.getString("NUM_CAIXA"));
            caixa.setIntQtde(0);
            caixa.setStrStatus("A");
            caixa.setListProdutosCaixa(FXCollections.observableList(new ArrayList<ProdutoCaixaReservaPedido>()));
        }
        resultSetSql.close();
        this.closeConnection();
        
        return caixa;
    }
    
    @Override
    public void deleteCaixa(String pedido, String reserva, CaixaReservaPedido caixa) throws SQLException {
        StringBuilder query = new StringBuilder(
                "" + "DELETE FROM SD_CAIXA_RESERVA_PEDIDO_001\n" + "WHERE PEDIDO = ? AND RESERVA = ? AND CAIXA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, reserva);
        preparedStatement.setString(3, caixa.getStrCaixa());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void deleteBarra(String pedido, String reserva, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto)
            throws SQLException {
        StringBuilder query = new StringBuilder("" + "DELETE FROM SD_CAIXA_RESERVA_PEDIDO_001 \n"
                + "WHERE PEDIDO = ? AND RESERVA = ? AND CAIXA = ? AND BARRA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, reserva);
        preparedStatement.setString(3, caixa.getStrCaixa());
        preparedStatement.setString(4, produto.getStrBarra());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public CaixaReservaPedido getCaixaByProduto(ReservaPedido reserva, ProdutosReservaPedido produto)
            throws SQLException {
        CaixaReservaPedido caixa = null;
        
        StringBuilder query = new StringBuilder(
                "" + "SELECT CAIXA, STATUS, COUNT(CODIGO) QTDE FROM SD_CAIXA_RESERVA_PEDIDO_001 \n"
                        + "WHERE PEDIDO = ? AND RESERVA = ? AND CODIGO = ? AND COR = ? AND TAM = ? \n"
                        + "GROUP BY CAIXA, STATUS");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroReserva());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            caixa = new CaixaReservaPedido(resultSetSql.getString("CAIXA"), resultSetSql.getString("STATUS"),
                    Integer.parseInt(resultSetSql.getString("QTDE")));
        }
        resultSetSql.close();
        this.closeConnection();
        
        return caixa;
    }
    
    @Override
    public void deleteReferenciaCaixa(ReservaPedido reserva, CaixaReservaPedido caixa, ProdutosReservaPedido produto)
            throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "DELETE FROM SD_CAIXA_RESERVA_PEDIDO_001 \n"
                + "WHERE PEDIDO = ? AND RESERVA = ? AND CAIXA = ? AND CODIGO = ? AND COR = ? AND TAM = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroReserva());
        preparedStatement.setString(3, caixa.getStrCaixa());
        preparedStatement.setString(4, produto.getStrCodigo());
        preparedStatement.setString(5, produto.getStrCor());
        preparedStatement.setString(6, produto.getStrTam());
        preparedStatement.execute();
        
        this.closeConnection();
        
    }
    
    @Override
    public List<String> getRastreioBarra(String barra) throws SQLException {
        List<String> rastreio = new ArrayList<>();
        
        StringBuilder query = new StringBuilder(
                "" + "SELECT CAI.CAIXA, CAI.PEDIDO, CAI.RESERVA, CAI.DATA_LANC, UPPER(CAI.USUARIO) USUARIO,\n"
                        + "CASE WHEN PED.NOTAFISCAL IS NULL THEN 'NÃO FATURADO' ELSE PED.NOTAFISCAL END FATURA \n"
                        + "FROM SD_CAIXA_RESERVA_PEDIDO_001 CAI\n"
                        + "LEFT JOIN PEDIDO3_001 PED ON PED.NUMERO = CAI.PEDIDO AND PED.CAIXA = CAI.CAIXA \n"
                        + "								AND PED.CODIGO = CAI.CODIGO AND PED.TAM = CAI.TAM AND PED.COR = CAI.COR\n"
                        + "WHERE BARRA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, barra);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            rastreio.add(resultSetSql.getString("CAIXA"));
            rastreio.add(resultSetSql.getString("PEDIDO"));
            rastreio.add(resultSetSql.getString("RESERVA"));
            rastreio.add(resultSetSql.getString("DATA_LANC"));
            rastreio.add(resultSetSql.getString("USUARIO"));
            rastreio.add(resultSetSql.getString("FATURA"));
        }
        resultSetSql.close();
        this.closeConnection();
        
        return rastreio;
    }
    
    // Movimentação de lançamento no TI
    @Override
    public void adicionaBarra_PEDIDO3(String pedido, CaixaReservaPedido caixa,
                                      ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra, AcessoSistema usuario)
            throws SQLException {
        
        StringBuilder query = new StringBuilder("" + "MERGE INTO PEDIDO3_001 PED USING DUAL ON (\n"
                + "	PED.NUMERO = ?\n" + "	AND PED.CAIXA = ?\n" + "	AND PED.CODIGO = ?\n" + "	AND PED.COR = ?\n"
                + "	AND PED.TAM = ? )\n" + "WHEN MATCHED THEN\n" + "	UPDATE SET QTDE = QTDE + 1, DATA = SYSDATE\n"
                + "WHEN NOT MATCHED THEN \n"
                + "	INSERT (CAIXA, CODIGO, COR, NUMERO, QTDE, QTDE_F, PESO_L, PESO, TAM, ORDEM, DEPOSITO, LOTE, DATA, TCAIXA, FUNCIONARIO, NOTAFISCAL)\n"
                + "         VALUES (? ,? ,? ,? ,1 ,0 , 0, 0,? ,? ,? ,'000000' ,SYSDATE ,'' ,? ,'')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.setString(6, caixa.getStrCaixa());
        preparedStatement.setString(7, produto.getStrCodigo());
        preparedStatement.setString(8, produto.getStrCor());
        preparedStatement.setString(9, pedido);
        preparedStatement.setString(10, produto.getStrTam());
        preparedStatement.setInt(11, barra.getIntOrdem());
        preparedStatement.setString(12, barra.getStrDeposito());
        preparedStatement.setString(13, usuario.getCodFunTi());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void removeBarra_PEDIDO3(String pedido, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto)
            throws SQLException {
        
        StringBuilder query = new StringBuilder(""
                + "UPDATE PEDIDO3_001 PED SET QTDE = QTDE - 1 , DATA = SYSDATE\n"
                + "	WHERE PED.NUMERO = ? AND PED.CAIXA = ? \n"
                + "		AND PED.CODIGO = ? AND PED.COR = ? AND PED.TAM = ? AND QTDE > 0");
        StringBuilder query2 = new StringBuilder(
                "" + "DELETE FROM PEDIDO3_001 PED \n"
                        + "	WHERE PED.NUMERO = ? AND PED.CAIXA = ? \n"
                        + "		AND PED.CODIGO = ? AND PED.COR = ? AND PED.TAM = ? AND QTDE = 0");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();
        
        preparedStatement = connection.prepareStatement(query2.toString());
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void removeReferencia_PEDIDO3(ReservaPedido reserva, CaixaReservaPedido caixa, ProdutosReservaPedido produto)
            throws SQLException {
        
        StringBuilder query2 = new StringBuilder(
                "" + "DELETE FROM PEDIDO3_001 PED \n"
                        + "	WHERE PED.NUMERO = ? AND PED.CAIXA = ? \n"
                        + "		AND PED.CODIGO = ? AND PED.COR = ? AND PED.TAM = ?");
        
        preparedStatement = connection.prepareStatement(query2.toString());
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void alteraPesoCaixas_PEDIDO3(String pedido, CaixaReservaPedido caixa) throws SQLException {
        StringBuilder query = new StringBuilder("" + "UPDATE PEDIDO3_001 \n"
                + "SET PESO = (SELECT SUM(CAI.QTDE)*AVG(PROD.PESO) FROM PEDIDO3_001 CAI \n"
                + "                 INNER JOIN PRODUTO_001 PROD ON CAI.CODIGO = PROD.CODIGO WHERE CAI.NUMERO = ? AND CAI.CAIXA = ? GROUP BY CAI.CAIXA)\n"
                + "WHERE NUMERO = ? AND CAIXA = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, pedido);
        preparedStatement.setString(4, caixa.getStrCaixa());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void removeItem_PED_RESERVA(ReservaPedido reserva, ProdutoCaixaReservaPedido produto) throws SQLException {
        StringBuilder query = new StringBuilder("" + "UPDATE PED_RESERVA_001 PED SET QTDE = QTDE - 1 \n"
                + "     WHERE PED.NUMERO = ? AND PED.CODIGO = ? \n"
                + "         AND PED.COR = ? AND PED.TAM = ? AND QTDE > 0");
        StringBuilder query2 = new StringBuilder(
                "" + "DELETE FROM PED_RESERVA_001 PED \n" + "     WHERE PED.NUMERO = ? AND PED.CODIGO = ? \n"
                        + "         AND PED.COR = ? AND PED.TAM = ? AND QTDE = 0");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, produto.getStrCodigo());
        preparedStatement.setString(3, produto.getStrCor());
        preparedStatement.setString(4, produto.getStrTam());
        preparedStatement.execute();
        
        preparedStatement = connection.prepareStatement(query2.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, produto.getStrCodigo());
        preparedStatement.setString(3, produto.getStrCor());
        preparedStatement.setString(4, produto.getStrTam());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaItem_PED_RESERVA(ReservaPedido reserva, ProdutoCaixaReservaPedido produto,
                                         ProdutoLancamentoReserva barra) throws SQLException {
        StringBuilder query = new StringBuilder("" + "MERGE INTO PED_RESERVA_001 PED USING DUAL ON (\n"
                + "	PED.NUMERO = ? \n" + "	AND PED.CODIGO = ? \n" + "	AND PED.COR = ? \n" + "	AND PED.TAM = ? \n"
                + "	AND PED.SD_RESERVA = ?)\n" + "WHEN MATCHED THEN \n" + "UPDATE SET PED.QTDE = QTDE + 1\n"
                + "WHEN NOT MATCHED THEN \n"
                + "INSERT (NUMERO, CODIGO, COR, TAM, QTDE, QTDE_B, RESERVA, QTDE_EXP, DEPOSITO, LOTE, SD_RESERVA)\n"
                + "VALUES (?, ?, ?, ?, 1, 0, 1, 0, ?, '000000', ?)");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, produto.getStrCodigo());
        preparedStatement.setString(3, produto.getStrCor());
        preparedStatement.setString(4, produto.getStrTam());
        preparedStatement.setString(5, reserva.getStrNumeroReserva());
        preparedStatement.setString(6, reserva.getStrNumeroPedido());
        preparedStatement.setString(7, produto.getStrCodigo());
        preparedStatement.setString(8, produto.getStrCor());
        preparedStatement.setString(9, produto.getStrTam());
        preparedStatement.setString(10, barra.getStrDeposito());
        preparedStatement.setString(11, reserva.getStrNumeroReserva());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void removeItem_PA_ITEM(ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra)
            throws SQLException {
        StringBuilder query = new StringBuilder("" + "UPDATE PA_ITEN_001 SET  QUANTIDADE = QUANTIDADE - 1 \n"
                + "WHERE CODIGO = ? AND COR = ? AND TAM = ? AND DEPOSITO = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, produto.getStrCodigo());
        preparedStatement.setString(2, produto.getStrCor());
        preparedStatement.setString(3, produto.getStrTam());
        preparedStatement.setString(4, barra.getStrDeposito());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaItem_PA_ITEM(ProdutoCaixaReservaPedido produto, ProdutoLancamentoReserva barra)
            throws SQLException {
        StringBuilder query = new StringBuilder("" + "UPDATE PA_ITEN_001 SET  QUANTIDADE = QUANTIDADE + 1 \n"
                + "WHERE CODIGO = ? AND COR = ? AND TAM = ? AND DEPOSITO = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, produto.getStrCodigo());
        preparedStatement.setString(2, produto.getStrCor());
        preparedStatement.setString(3, produto.getStrTam());
        preparedStatement.setString(4, barra.getStrDeposito());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaMov_PA_MOV(String pedido, CaixaReservaPedido caixa, ProdutoCaixaReservaPedido produto,
                                   ProdutoLancamentoReserva barra, String tp_mov, AcessoSistema usuario) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO PA_MOV_001  \n"
                + "(ORDEM, NUM_DOCTO, CODIGO, COR, TAMANHO, LOTE, DEPOSITO, TIPO, QTDE, TIP_BAI, OPERACAO, \n"
                + "DT_MVTO, DESCRICAO, OBSERVACAO, TURNO, PRECO, CUSTO, CODCLI, CODFUN, TP_MOV) \n" + "VALUES \n"
                + "((SELECT MAX(ORDEM) + 1 ORDEM FROM PA_MOV_001 WHERE DT_MVTO > sysdate2-30) ,? ,? ,? ,? ,'000000' ,? ,'1' ,1 ,'2' ,? ,\n"
                + "SYSDATE ,'' ,'Pedido - '||?||' Caixa - '||? ,'1' ,(SELECT PRECO FROM PRODUTO_001 WHERE CODIGO = ?) ,\n"
                + "(SELECT CUSTO FROM PRODUTO_001 WHERE CODIGO = ?) ,(SELECT CODCLI FROM PEDIDO_001 WHERE NUMERO = ?) ,? ,'EX')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, pedido);
        preparedStatement.setString(2, produto.getStrCodigo());
        preparedStatement.setString(3, produto.getStrCor());
        preparedStatement.setString(4, produto.getStrTam());
        preparedStatement.setString(5, barra.getStrDeposito());
        preparedStatement.setString(6, tp_mov);
        preparedStatement.setString(7, pedido);
        preparedStatement.setString(8, caixa.getStrCaixa());
        preparedStatement.setString(9, produto.getStrCodigo());
        preparedStatement.setString(10, produto.getStrCodigo());
        preparedStatement.setString(11, pedido);
        preparedStatement.setString(12, usuario.getCodFunTi());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaLogPrimeiraCaixa(ReservaPedido reserva) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO LOG \n"
                + "(USUARIO, DATA, HORA, TELA, DESCRICAO, EMPRESA, CHAVE, OPERACAO, FORMULARIO) \n" + "VALUES \n"
                + "('SYSDELIZ' , sysdate  , current_timestamp  ,'SD - EXPEDICAO' ,'ALOCAÇÃO DO PEDIDO '||? ,'_001' ,? ,'Movimentação' ,'LANCAMENTO RESERVA')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroPedido());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaLogEncerramentoCaixa(ReservaPedido reserva, CaixaReservaPedido caixa) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO LOG \n"
                + "(USUARIO, DATA, HORA, TELA, DESCRICAO, EMPRESA, CHAVE, OPERACAO, FORMULARIO) \n" + "VALUES \n"
                + "('SYSDELIZ' , sysdate  , current_timestamp  ,'SD - EXPEDICAO' ,'GRAVACAO DA CAIXA '||?||' NO PEDIDO '||? ,'_001' ,? ,'Movimentação' ,'LANCAMENTO RESERVA')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, caixa.getStrCaixa());
        preparedStatement.setString(2, reserva.getStrNumeroPedido());
        preparedStatement.setString(3, caixa.getStrCaixa());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaLogEncerramentoReserva(ReservaPedido reserva) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO LOG \n"
                + "(USUARIO, DATA, HORA, TELA, DESCRICAO, EMPRESA, CHAVE, OPERACAO, FORMULARIO) \n" + "VALUES \n"
                + "('SYSDELIZ' , sysdate  , current_timestamp  ,'SD - EXPEDICAO' ,'DESALOCAÇÃO DO PEDIDO '||? ,'_001' ,? ,'Movimentação' ,'LANCAMENTO RESERVA')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroPedido());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
    @Override
    public void adicionaLogRemocaoBarra(ReservaPedido reserva, CaixaReservaPedido caixa,
                                        ProdutoCaixaReservaPedido produto) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO LOG \n"
                + "(USUARIO, DATA, HORA, TELA, DESCRICAO, EMPRESA, CHAVE, OPERACAO, FORMULARIO) \n" + "VALUES \n"
                + "('SYSDELIZ' , sysdate  , current_timestamp  ,'SD - EXPEDICAO' ,'ESTORNO PED. '||?||' CX. '||?||' PROD. '||?||' COR '||?||' TAM '||? ,'_001' ,? ,'Exclusão' ,'LANCAMENTO RESERVA')");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, caixa.getStrCaixa());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.setString(6, reserva.getStrNumeroPedido());
        preparedStatement.execute();
        
        this.closeConnection();
    }
    
}
