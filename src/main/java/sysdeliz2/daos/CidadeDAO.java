/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Cidade;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface CidadeDAO {

    public ObservableList<Cidade> getAll() throws SQLException;

    public ObservableList<Cidade> getByForm(String cidade, String informacoes) throws SQLException;

    public ObservableList<Cidade> getCidadesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

}
