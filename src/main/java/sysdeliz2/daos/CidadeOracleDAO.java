/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.Cidade;
import sysdeliz2.models.ConnectionFactory;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class CidadeOracleDAO implements CidadeDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Cidade> getAll() throws SQLException {
        ObservableList<Cidade> cidade = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT CID.COD_CID CODIGO, CID.NOME_CID NOME, \n"
                + "       CID.COD_EST ESTADO, REGIAO, CLASSE \n"
                + "FROM CIDADE_001 CID");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            cidade.add(new Cidade(resultSetSql.getString("CODIGO"), resultSetSql.getString("NOME"),
                    resultSetSql.getString("ESTADO"), resultSetSql.getString("CLASSE"), resultSetSql.getString("REGIAO")));
        }
        closeConnection();
        resultSetSql.close();
        return cidade;
    }

    @Override
    public ObservableList<Cidade> getByForm(String cidade, String informacoes) throws SQLException {
        ObservableList<Cidade> cidades = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT CID.COD_CID CODIGO, CEP.CEP, CID.NOME_CID NOME, \n"
                + "       CID.COD_EST ESTADO, REGIAO, CLASSE \n"
                + "FROM CIDADE_001 CID\n"
                + "JOIN CADCEP_001 CEP ON CEP.COD_CID = CID.COD_CID\n"
                + "WHERE (CID.COD_CID LIKE '%" + cidade + "%' OR\n"
                + "       CID.NOME_CID LIKE '%" + cidade + "%') AND\n"
                + "      (CID.COD_EST LIKE '%" + informacoes + "%' OR\n"
                + "       CID.REGIAO LIKE '%" + informacoes + "%' OR\n"
                + "       CID.CLASSE LIKE '%" + informacoes + "%')");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            cidades.add(new Cidade(resultSetSql.getString("CODIGO"), resultSetSql.getString("NOME"),
                    resultSetSql.getString("ESTADO"), resultSetSql.getString("CLASSE"), resultSetSql.getString("REGIAO")));
        }
        closeConnection();
        resultSetSql.close();
        return cidades;
    }

    @Override
    public ObservableList<Cidade> getCidadesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Cidade> cidades = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "SELECT CID.COD_CID CODIGO, CID.NOME_CID NOME, \n"
                + "       CID.COD_EST ESTADO, uf.REGIAO \n"
                + "FROM CIDADE_001 CID\n"
                + "join tabuf_001 uf\n"
                + "    on uf.sigla_est = cid.cod_est\n"
                + "WHERE" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Cidade cidade = new Cidade();
            cidade.setCodigo(resultSetSql.getString("codigo"));
            cidade.setNome(resultSetSql.getString("nome"));
            cidade.setEstado(resultSetSql.getString("estado"));
            cidade.setRegiao(resultSetSql.getString("regiao"));
            cidades.add(cidade);
        }
        resultSetSql.close();
        this.closeConnection();
        return cidades;
    }

}
