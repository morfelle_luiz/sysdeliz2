/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Cliente;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface ClienteDAO {

    public ObservableList<Cliente> getAll() throws SQLException;

    public ObservableList<Cliente> getAllFornecedor() throws SQLException;

    public ObservableList<Cliente> getByForm(String cliente, String informacao) throws SQLException;

    public Cliente getByCodcli(String codcli) throws SQLException;

    public ObservableList<Cliente> getClientesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

    public ObservableList<Cliente> getFornecedoresFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;
}
