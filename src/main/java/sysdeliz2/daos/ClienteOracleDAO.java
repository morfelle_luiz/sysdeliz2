/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.Cliente;
import sysdeliz2.models.ConnectionFactory;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class ClienteOracleDAO implements ClienteDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Cliente> getAll() throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public ObservableList<Cliente> getAllFornecedor() throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S' AND ENT.TIPO_ENTIDADE LIKE '%F%'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public ObservableList<Cliente> getByForm(String cliente, String informacao) throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S'\n"
                        + "	AND (ENT.CODCLI LIKE ?\n"
                        + "	OR ENT.NOME LIKE ?\n"
                        + "	OR ENT.FANTASIA LIKE ?\n"
                        + "	OR ENT.CNPJ LIKE ?)\n"
                        + "	AND (CEP.CEP LIKE ?\n"
                        + "	OR CID.NOME_CID LIKE ?\n"
                        + "	OR CID.COD_CID LIKE ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(2, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(3, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(4, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(5, "%" + informacao.toUpperCase() + "%");
        preparedStatement.setString(6, "%" + informacao.toUpperCase() + "%");
        preparedStatement.setString(7, "%" + informacao.toUpperCase() + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public Cliente getByCodcli(String codcli) throws SQLException {
        Cliente client = null;

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST, ENT.EMAIL\n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S'\n"
                        + "	AND ENT.CODCLI = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codcli);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            client = new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"), resultSetSql.getString("CNPJ"),
                    resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP"), resultSetSql.getString("EMAIL"));
        }

        closeConnection();
        resultSetSql.close();

        return client;
    }

    @Override
    public ObservableList<Cliente> getClientesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).contains("in") ? " (" : filters.get(2).contains("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).contains("in") ? ")" : filters.get(2).contains("like") ? "%'" : "'")
                    + (filters.get(2).contains("not in") ? " or " + filters.get(1) + " is null" : "")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(
                "SELECT CLI.CODCLI, CLI.NOME, CLI.FANTASIA, CLI.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST \n"
                        + "FROM ENTIDADE_001 CLI\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = CLI.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "INNER JOIN TABUF_001 UF ON CID.COD_EST = UF.sigla_est\n"
                        + "WHERE CLI.ATIVO = 'S' AND" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public ObservableList<Cliente> getFornecedoresFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).contains("in") ? " (" : filters.get(2).contains("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).contains("in") ? ")" : filters.get(2).contains("like") ? "%'" : "'")
                    + (filters.get(2).contains("not in") ? " or " + filters.get(1) + " is null" : "")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(
                "SELECT CLI.CODCLI, CLI.NOME, CLI.FANTASIA, CLI.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST \n"
                        + "FROM ENTIDADE_001 CLI\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = CLI.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "INNER JOIN TABUF_001 UF ON CID.COD_EST = UF.sigla_est\n"
                        + "WHERE CLI.ATIVO = 'S' AND ENT.TIPO_ENTIDADE LIKE '%F%' AND" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }
}
