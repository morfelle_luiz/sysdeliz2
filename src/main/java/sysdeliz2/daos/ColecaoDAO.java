package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Colecao;

import java.sql.SQLException;
import java.util.List;

public interface ColecaoDAO {

    public List<Colecao> loadAtivos() throws SQLException;

    public ObservableList<String> loadLinhas(String pMarca) throws SQLException;

    public ObservableList<Colecao> getAll() throws SQLException;

    public ObservableList<Colecao> getColecoes(String form1) throws SQLException;

    public String[] getTresUltimasColecoes() throws SQLException;

    public Colecao getColecaoAtual() throws SQLException;

    public ObservableList<Colecao> getColecoesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;
}
