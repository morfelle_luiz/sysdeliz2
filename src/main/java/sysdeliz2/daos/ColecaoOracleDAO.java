package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.Colecao;
import sysdeliz2.models.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ColecaoOracleDAO implements ColecaoDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public List<Colecao> loadAtivos() throws SQLException {
        List<Colecao> colecoes = new ArrayList<>();

        StringBuilder query = new StringBuilder(
                ""
                        + "SELECT CODIGO, DESCRICAO FROM COLECAO_001 WHERE SD_ATIVO = 'X' \n"
                        + "ORDER BY SEQUENCIA");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSqlLocal = preparedStatement.getResultSet();
        while (resultSetSqlLocal.next()) {
            colecoes.add(new Colecao(resultSetSqlLocal.getString("CODIGO"), resultSetSqlLocal.getString("DESCRICAO")));
        }
        closeConnection();
        resultSetSqlLocal.close();

        return colecoes;
    }

    @Override
    public ObservableList<String> loadLinhas(String pMarca) throws SQLException {
        ObservableList<String> linhas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT TRIM(REGEXP_REPLACE(LIN.DESCRICAO, '('||MAR.DESCRICAO||'|MOOVE|FEM|MASC)', '')) LINHA\n"
                        + "FROM TABLIN_001 LIN\n"
                        + "INNER JOIN MARCA_001 MAR ON MAR.CODIGO = LIN.SD_MARCA\n"
                        + "WHERE MAR.DESCRICAO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pMarca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            linhas.add(resultSetSql.getString("LINHA"));
        }
        closeConnection();
        resultSetSql.close();

        return linhas;
    }

    @Override
    public ObservableList<Colecao> getAll() throws SQLException {
        ObservableList<Colecao> colecao = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO, INICIO_VIG, FIM_VIG\n"
                        + "FROM COLECAO_001\n"
                        + "WHERE SD_ATIVO = 'X'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            colecao.add(new Colecao(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"),
                    resultSetSql.getString("INICIO_VIG"), resultSetSql.getString("FIM_VIG")));
        }
        closeConnection();
        resultSetSql.close();

        return colecao;
    }

    @Override
    public ObservableList<Colecao> getColecoes(String form1) throws SQLException {
        ObservableList<Colecao> colecao = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO, INICIO_VIG, FIM_VIG\n"
                        + "FROM COLECAO_001\n"
                        + "WHERE (CODIGO LIKE '%" + form1 + "%' OR DESCRICAO LIKE '%" + form1 + "%') AND SD_ATIVO = 'X'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            colecao.add(new Colecao(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"),
                    resultSetSql.getString("INICIO_VIG"), resultSetSql.getString("FIM_VIG")));
        }
        closeConnection();
        resultSetSql.close();

        return colecao;
    }

    @Override
    public String[] getTresUltimasColecoes() throws SQLException {
        String colecoes = "";

        StringBuilder query = new StringBuilder(
                "SELECT \n"
                        + "(SELECT DESCRICAO FROM COLECAO_001 WHERE CODIGO = (SELECT COL.CODIGO FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG)) PRI,\n"
                        + "(SELECT DESCRICAO FROM COLECAO_001 WHERE CODIGO = (SELECT MAX(CODIGO) FROM COLECAO_001 COL_ANT WHERE CODIGO < (SELECT MIN(COL.CODIGO) FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG))) SEG,\n"
                        + "(SELECT DESCRICAO FROM COLECAO_001 WHERE CODIGO = (SELECT MAX(CODIGO) FROM COLECAO_001 WHERE CODIGO < (SELECT MAX(CODIGO) FROM COLECAO_001 COL_ANT WHERE CODIGO < (SELECT MIN(COL.CODIGO) FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG)))) TER,\n"
                        + "(SELECT DESCRICAO FROM COLECAO_001 WHERE CODIGO <> '17CO' AND CODIGO = (SELECT MAX(CODIGO) FROM COLECAO_001 WHERE CODIGO <> '17CO' AND CODIGO < (SELECT MAX(CODIGO) FROM COLECAO_001 WHERE CODIGO <> '17CO' AND CODIGO < (SELECT MAX(CODIGO) FROM COLECAO_001 COL_ANT WHERE CODIGO <> '17CO' AND CODIGO < (SELECT MIN(COL.CODIGO) FROM COLECAO_001 COL WHERE CODIGO <> '17CO' AND SD_ATIVO = 'X' AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG))))) QUA\n"
                        + "FROM DUAL");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            colecoes = resultSetSql.getString("PRI") + ";" + resultSetSql.getString("SEG") + ";" + resultSetSql.getString("TER") + ";" + resultSetSql.getString("QUA");
        }
        closeConnection();
        resultSetSql.close();

        return colecoes.split(";");
    }

    @Override
    public Colecao getColecaoAtual() throws SQLException {
        Colecao colecao = null;

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO\n"
                        + "  FROM COLECAO_001\n"
                        + " WHERE CODIGO =\n"
                        + "       (SELECT COL.CODIGO\n"
                        + "          FROM COLECAO_001 COL\n"
                        + "         WHERE CODIGO <> '17CO'\n"
                        + "           AND SYSDATE BETWEEN COL.INICIO_VIG AND COL.FIM_VIG)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            colecao = new Colecao(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"));
        }
        closeConnection();
        resultSetSql.close();

        return colecao;
    }

    @Override
    public ObservableList<Colecao> getColecoesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Colecao> colecao = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO, INICIO_VIG, FIM_VIG\n"
                        + "FROM COLECAO_001\n"
                        + "WHERE SD_ATIVO = 'X' AND" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            colecao.add(new Colecao(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"),
                    resultSetSql.getString("INICIO_VIG"), resultSetSql.getString("FIM_VIG")));
        }
        closeConnection();
        resultSetSql.close();

        return colecao;
    }

}
