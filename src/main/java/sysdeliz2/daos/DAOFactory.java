package sysdeliz2.daos;

/**
 * @author cristiano.diego
 */
public class DAOFactory {

    private static final String RESERVA_PEDIDO_DAO_CLASSNAME = "sysdeliz2.daos.ReservaPedidoOracleDAO";
    private static final String MARKETING_PEDIDO_DAO_CLASSNAME = "sysdeliz2.daos.MarketingPedidoOracleDAO";
    private static final String PRODUTO_DAO_CLASSNAME = "sysdeliz2.daos.ProdutoOracleDAO";
    private static final String CAIXA_RESERVA_PEDIDO_DAO_CLASSNAME = "sysdeliz2.daos.CaixaReservaPedidoOracleDAO";
    private static final String USUARIOS_DAO_CLASSNAME = "sysdeliz2.daos.UsuariosOracleDAO";
    private static final String COLECAO_DAO_CLASSNAME = "sysdeliz2.daos.ColecaoOracleDAO";
    private static final String PERIODO_DAO_CLASSNAME = "sysdeliz2.daos.PeriodoOracleDAO";
    private static final String GESTAO_PRODUCAO_DAO_CLASSNAME = "sysdeliz2.daos.GestaoProducaoOracleDAO";
    private static final String INDICE_PROJECAO_DAO_CLASSNAME = "sysdeliz2.daos.IndiceProjecaoOracleDAO";
    private static final String BID_DAO_CLASSNAME = "sysdeliz2.daos.BidOracleDAO";
    private static final String REPRESENTANTE_DAO_CLASSNAME = "sysdeliz2.daos.RepresentanteOracleDAO";
    private static final String FAMILIA_DAO_CLASSNAME = "sysdeliz2.daos.FamiliaOracleDAO";
    private static final String MARCA_DAO_CLASSNAME = "sysdeliz2.daos.MarcaOracleDAO";
    private static final String FATURA_A_RECEBER_DAO_CLASSNAME = "sysdeliz2.daos.FaturaAReceberOracleDAO";
    private static final String CLIENTE_DAO_CLASSNAME = "sysdeliz2.daos.ClienteOracleDAO";
    private static final String FORNECEDOR_DAO_CLASSNAME = "sysdeliz2.daos.FornecedorOracleDAO";
    private static final String LOG_DAO_CLASSNAME = "sysdeliz2.daos.LogOracleDAO";
    private static final String CIDADE_DAO_CLASSNAME = "sysdeliz2.daos.CidadeOracleDAO";
    private static final String ROTEIROS_DAO_CLASSNAME = "sysdeliz2.daos.db.RoteirosOracleTA";
    private static final String PEDIDO_DAO_CLASSNAME = "sysdeliz2.daos.db.PedidoOracleTA";
    private static final String RECEBER_CARTAO_DAO_CLASSNAME = "sysdeliz2.daos.db.ReceberCartaoOracleTA";
    private static final String RECEBER_DAO_CLASSNAME = "sysdeliz2.daos.db.ReceberOracleTA";
    private static final String ESTAMPA_DAO_CLASSNAME = "sysdeliz2.daos.db.EstampaOracleTA";
    private static final String GRUPOCLI_DAO_CLASSNAME = "sysdeliz2.daos.db.GrupoCliOracleTA";
    private static final String SITCLI_DAO_CLASSNAME = "sysdeliz2.daos.db.SitCliOracleTA";
    private static final String SD_FUNCAO_001_DAO = "sysdeliz2.daos.SdFuncao001DAO";
    private static final String SD_TURNO_001_DAO = "sysdeliz2.daos.SdTurno001DAO";
    private static final String SD_NIVEL_POLIVALENCIA_001_DAO = "sysdeliz2.daos.SdNivelPolivalencia001DAO";
    private static final String SD_GRUPO_OPERACAO_001_DAO = "sysdeliz2.daos.SdGrupoOperacao001DAO";
    private static final String SD_OPERACOES_GRP_001_DAO = "sysdeliz2.daos.SdOperacoesGrp001DAO";
    private static final String SD_OPERACAO_ORGANIZE_001_DAO = "sysdeliz2.daos.SdOperacaoOrganize001DAO";
    private static final String SD_POLIVALENCIA_001_DAO = "sysdeliz2.daos.SdPolivalencia001DAO";
    private static final String SD_COLABORADOR_001_DAO = "sysdeliz2.daos.SdColaborador001DAO";
    private static final String FAMILIA_GRUPO_001_DAO = "sysdeliz2.daos.FamiliaGrupo001DAO";
    private static final String SD_SETOR_OP_001_DAO = "sysdeliz2.daos.SdSetorOp001DAO";
    private static final String SD_FAMILIAS_CELULA_001_DAO = "sysdeliz2.daos.SdFamiliasCelula001DAO";
    private static final String SD_COLAB_CELULA_001_DAO = "sysdeliz2.daos.SdColabCelula001DAO";
    private static final String SD_CELULA_001_DAO = "sysdeliz2.daos.SdCelula001DAO";
    private static final String MAT_RESERVA_001_DAO_CLASSNAME = "sysdeliz2.daos.MatReserva001DAO";
    private static final String OF_ITEN_001_DAO_CLASSNAME = "sysdeliz2.daos.OfIten001DAO";
    private static final String OF1_001_DAO_CLASSNAME = "sysdeliz2.daos.Of1001DAO";
    private static final String PROPERTIES_DAO_CLASSNAME = "sysdeliz2.daos.PropertiesDAO";
    private static final String TABPRZ_001_DAO_CLASSNAME = "sysdeliz2.daos.TabPrz001DAO";
    private static final String GESTAO_COMPRA_00X_DAO_CLASSNAME = "sysdeliz2.daos.GestaoCompra00XDAO";
    private static final String SD_PRODUTO_PRIORIDADE_MRP_001_DAO_CLASSNAME = "sysdeliz2.daos.SdProdutoPrioridadeMrp001DAO";
    private static final String SD_OBSERVACAO_OC_001_DAO_CLASSNAME = "sysdeliz2.daos.SdObservacaoOc001DAO";
    private static final String SD_LOG_EMAIL_OC_001_DAO_CLASSNAME = "sysdeliz2.daos.SdLogEmailOc001DAO";
    private static final String SD_AGENDA_SERVICO_001_DAO_CLASSNAME = "sysdeliz2.daos.SdAgendaServico001DAO";
    private static final String SD_ROTEIRO_ORGANIZE_001_DAO_CLASSNAME = "sysdeliz2.daos.SdRoteiroOrganize001DAO";
    private static final String SD_OPER_ROTEIRO_ORGANIZE_001_DAO_CLASSNAME = "sysdeliz2.daos.SdOperRoteiroOrganize001DAO";
    private static final String LOG_EMAIL_OC_00X_DAO_CLASSNAME = "sysdeliz2.daos.LogEmailOc00XDAO";
    private static final String SD_PRODUTO_001_DAO_CLASSNAME = "sysdeliz2.daos.SdProduto001DAO";
    private static final String SD_REGISTRO_ENTRADAS_DAO_CLASSNAME = "sysdeliz2.daos.SdRegistroEntradas001DAO";
    private static final String SD_COLABORADOR_SENIOR_DAO_CLASSNAME = "sysdeliz2.daos.SdColaboradorSenior001DAO";
    private static final String SD_ALTERACAO_OC_001_DAO_CLASSNAME = "sysdeliz2.daos.SdAlteracaoOc001DAO";
    private static final String FAMILIA_001_DAO_CLASSNAME = "sysdeliz2.daos.Familia001DAO";
    private static final String PRODUTO_001_DAO_CLASSNAME = "sysdeliz2.daos.Produto001DAO";
    private static final String SD_PACOTE_001_DAO_CLASSNAME = "sysdeliz2.daos.SdPacote001DAO";
    private static final String SD_PACOTE_LANCAMENTO_001_DAO_CLASSNAME = "sysdeliz2.daos.SdPacoteLancamento001DAO";
    private static final String SD_PROGRAMACAO_OF_001_DAO_CLASSNAME = "sysdeliz2.daos.SdProgramacaoOf001DAO";
    private static final String SD_PROGRAMACAO_PACOTE_001_DAO_CLASSNAME = "sysdeliz2.daos.SdProgramacaoPacote001DAO";
    private static final String SD_EQUIPAMENTOS_ORGANIZE_001_DAO_CLASSNAME = "sysdeliz2.daos.SdEquipamentosOrganize001DAO";
    private static final String SD_TURNO_CELULA_001_DAO_CLASSNAME = "sysdeliz2.daos.SdTurnoCelula001DAO";
    private static final String V_SD_OFS_PARA_PROGRAMAR_DAO_CLASSNAME = "sysdeliz2.daos.VSdOfsParaProgramarDAO";
    private static final String V_SD_IMP_PROG_COLABORADOR = "sysdeliz2.daos.VSdImpProgColaboradorDAO";
    private static final String DASHBOARD_PRODUCAO_CLASSNAME = "sysdeliz2.daos.DashboardProducaoDAO";
    private static final String V_SD_PROD_DIARIA_CELULA_CLASSNAME = "sysdeliz2.daos.VSdProdDiariaCelulaDAO";
    private static final String FIREBIRD_SQL_DAO_CLASSNAME = "sysdeliz2.daos.FirebirdSQLDAO";

    private static Object getDao(String className) {
        try {
            return Class.forName(className).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ReservaPedidoDAO getReservaPedidoDAO() {
        return (ReservaPedidoDAO) getDao(RESERVA_PEDIDO_DAO_CLASSNAME);
    }

    public static MarketingPedidoDAO getMarketingPedidoDAO() {
        return (MarketingPedidoDAO) getDao(MARKETING_PEDIDO_DAO_CLASSNAME);
    }

    public static ProdutoDAO getProdutoDAO() {
        return (ProdutoDAO) getDao(PRODUTO_DAO_CLASSNAME);
    }

    public static CaixaReservaPedidoDAO getCaixaReservaPedidoDAO() {
        return (CaixaReservaPedidoDAO) getDao(CAIXA_RESERVA_PEDIDO_DAO_CLASSNAME);
    }

    public static UsuariosDAO getUsuariosDAO() {
        return (UsuariosDAO) getDao(USUARIOS_DAO_CLASSNAME);
    }

    public static ColecaoDAO getColecaoDAO() {
        return (ColecaoDAO) getDao(COLECAO_DAO_CLASSNAME);
    }

    public static PeriodoDAO getPeriodoDAO() {
        return (PeriodoDAO) getDao(PERIODO_DAO_CLASSNAME);
    }

    public static GestaoProducaoDAO getGestaoProducaoDAO() {
        return (GestaoProducaoDAO) getDao(GESTAO_PRODUCAO_DAO_CLASSNAME);
    }

    public static IndiceProjecaoDAO getIndiceProjecaoDAO() {
        return (IndiceProjecaoDAO) getDao(INDICE_PROJECAO_DAO_CLASSNAME);
    }

    public static BidDAO getBidDAO() {
        return (BidDAO) getDao(BID_DAO_CLASSNAME);
    }

    public static RepresentanteDAO getRepresentanteDAO() {
        return (RepresentanteDAO) getDao(REPRESENTANTE_DAO_CLASSNAME);
    }

    public static FamiliaDAO getFamiliaDAO() {
        return (FamiliaDAO) getDao(FAMILIA_DAO_CLASSNAME);
    }

    public static MarcaDAO getMarcaDAO() {
        return (MarcaDAO) getDao(MARCA_DAO_CLASSNAME);
    }

    public static FaturaAReceberDAO getFaturaAReceberDAO() {
        return (FaturaAReceberDAO) getDao(FATURA_A_RECEBER_DAO_CLASSNAME);
    }

    public static ClienteDAO getClienteDAO() {
        return (ClienteDAO) getDao(CLIENTE_DAO_CLASSNAME);
    }

    public static FornecedorDAO getFornecedorDAO() {
        return (FornecedorDAO) getDao(FORNECEDOR_DAO_CLASSNAME);
    }

    public static LogDAO getLogDAO() {
        return (LogDAO) getDao(LOG_DAO_CLASSNAME);
    }

    public static CidadeDAO getCidadeDAO() {
        return (CidadeDAO) getDao(CIDADE_DAO_CLASSNAME);
    }

    public static RoteirosDAO getRoteirosDAO() {
        return (RoteirosDAO) getDao(ROTEIROS_DAO_CLASSNAME);
    }

    public static PedidoDAO getPedidoDAO() {
        return (PedidoDAO) getDao(PEDIDO_DAO_CLASSNAME);
    }

    public static ReceberCartaoDAO getReceberCartaoDAO() {
        return (ReceberCartaoDAO) getDao(RECEBER_CARTAO_DAO_CLASSNAME);
    }

    public static ReceberDAO getReceberDAO() {
        return (ReceberDAO) getDao(RECEBER_DAO_CLASSNAME);
    }

    public static EstampaDAO getEstampaDAO() {
        return (EstampaDAO) getDao(ESTAMPA_DAO_CLASSNAME);
    }

    public static GrupoCliDAO getGrupoCliDAO() {
        return (GrupoCliDAO) getDao(GRUPOCLI_DAO_CLASSNAME);
    }

    public static SitCliDAO getSitCliDAO() {
        return (SitCliDAO) getDao(SITCLI_DAO_CLASSNAME);
    }

    public static MatReserva001DAO getMatReserva001DAO() {
        return (MatReserva001DAO) getDao(MAT_RESERVA_001_DAO_CLASSNAME);
    }

    public static SdFuncao001DAO getSdFuncaoDAO() {
        return (SdFuncao001DAO) getDao(SD_FUNCAO_001_DAO);
    }

    public static SdTurno001DAO getSdTurnoDAO() {
        return (SdTurno001DAO) getDao(SD_TURNO_001_DAO);
    }

    public static OfIten001DAO getOfIten001DAO() {
        return (OfIten001DAO) getDao(OF_ITEN_001_DAO_CLASSNAME);
    }

    public static SdNivelPolivalencia001DAO getSdNivelPolivalenciaDAO() {
        return (SdNivelPolivalencia001DAO) getDao(SD_NIVEL_POLIVALENCIA_001_DAO);
    }

    public static SdGrupoOperacao001DAO getSdGrupoOperacao001DAO() {
        return (SdGrupoOperacao001DAO) getDao(SD_GRUPO_OPERACAO_001_DAO);
    }

    public static Of1001DAO getOf1001DAO() {
        return (Of1001DAO) getDao(OF1_001_DAO_CLASSNAME);
    }

    public static SdOperacoesGrp001DAO getSdOperacoesGrp001DAO() {
        return (SdOperacoesGrp001DAO) getDao(SD_OPERACOES_GRP_001_DAO);
    }

    public static SdOperacaoOrganize001DAO getSdOperacaoOrganize001DAO() {
        return (SdOperacaoOrganize001DAO) getDao(SD_OPERACAO_ORGANIZE_001_DAO);
    }

    public static PropertiesDAO getPropertiesDAO() {
        return (PropertiesDAO) getDao(PROPERTIES_DAO_CLASSNAME);
    }

    public static SdPolivalencia001DAO getSdPolivalencia001DAO() {
        return (SdPolivalencia001DAO) getDao(SD_POLIVALENCIA_001_DAO);
    }

    public static SdColaborador001DAO getSdColaborador001DAO() {
        return (SdColaborador001DAO) getDao(SD_COLABORADOR_001_DAO);
    }

    public static FamiliaGrupo001DAO getFamiliaGrupo001DAO() {
        return (FamiliaGrupo001DAO) getDao(FAMILIA_GRUPO_001_DAO);
    }

    public static SdSetorOp001DAO getSdSetorOp001DAO() {
        return (SdSetorOp001DAO) getDao(SD_SETOR_OP_001_DAO);
    }

    public static SdFamiliasCelula001DAO getSdFamiliasCelula001DAO() {
        return (SdFamiliasCelula001DAO) getDao(SD_FAMILIAS_CELULA_001_DAO);
    }

    public static SdColabCelula001DAO getSdColabCelula001DAO() {
        return (SdColabCelula001DAO) getDao(SD_COLAB_CELULA_001_DAO);
    }

    public static SdCelula001DAO getSdCelula001DAO() {
        return (SdCelula001DAO) getDao(SD_CELULA_001_DAO);
    }

    public static TabPrz001DAO getTabPrz001DAO() {
        return (TabPrz001DAO) getDao(TABPRZ_001_DAO_CLASSNAME);
    }

    public static GestaoCompra00XDAO getGestaoCompra00XDAO() {
        return (GestaoCompra00XDAO) getDao(GESTAO_COMPRA_00X_DAO_CLASSNAME);
    }

    public static SdProdutoPrioridadeMrp001DAO getSdProdutoPrioridadeMrp001DAO() {
        return (SdProdutoPrioridadeMrp001DAO) getDao(SD_PRODUTO_PRIORIDADE_MRP_001_DAO_CLASSNAME);
    }

    public static SdObservacaoOc001DAO getSdObservacaoOc001DAO() {
        return (SdObservacaoOc001DAO) getDao(SD_OBSERVACAO_OC_001_DAO_CLASSNAME);
    }

    public static SdLogEmailOc001DAO getSdLogEmailOc001DAO() {
        return (SdLogEmailOc001DAO) getDao(SD_LOG_EMAIL_OC_001_DAO_CLASSNAME);
    }

    public static SdAgendaServico001DAO getSdAgendaServico001DAO() {
        return (SdAgendaServico001DAO) getDao(SD_AGENDA_SERVICO_001_DAO_CLASSNAME);
    }

    public static SdRoteiroOrganize001DAO getSdRoteiroOrganize001DAO() {
        return (SdRoteiroOrganize001DAO) getDao(SD_ROTEIRO_ORGANIZE_001_DAO_CLASSNAME);
    }

    public static SdOperRoteiroOrganize001DAO getSdOperRoteiroOrganize001DAO() {
        return (SdOperRoteiroOrganize001DAO) getDao(SD_OPER_ROTEIRO_ORGANIZE_001_DAO_CLASSNAME);
    }

    public static LogEmailOc00XDAO getLogEmailOc00XDAO() {
        return (LogEmailOc00XDAO) getDao(LOG_EMAIL_OC_00X_DAO_CLASSNAME);
    }

    public static SdProduto001DAO getSdProduto001DAO() {
        return (SdProduto001DAO) getDao(SD_PRODUTO_001_DAO_CLASSNAME);
    }

    public static SdRegistroEntradas001DAO getSdRegistroEntradas001DAO() {
        return (SdRegistroEntradas001DAO) getDao(SD_REGISTRO_ENTRADAS_DAO_CLASSNAME);
    }

    public static Familia001DAO getFamilia001DAO() {
        return (Familia001DAO) getDao(FAMILIA_001_DAO_CLASSNAME);
    }

    public static SdColaboradorSenior001DAO getSdColaboradorSenior001DAO() {
        return (SdColaboradorSenior001DAO) getDao(SD_COLABORADOR_SENIOR_DAO_CLASSNAME);
    }

    public static Produto001DAO getProduto001DAO() {
        return (Produto001DAO) getDao(PRODUTO_001_DAO_CLASSNAME);
    }

    public static SdPacote001DAO getSdPacote001DAO() {
        return (SdPacote001DAO) getDao(SD_PACOTE_001_DAO_CLASSNAME);
    }

    public static SdPacoteLancamento001DAO getSdPacoteLancamento001DAO() {
        return (SdPacoteLancamento001DAO) getDao(SD_PACOTE_LANCAMENTO_001_DAO_CLASSNAME);
    }

    public static SdProgramacaoOf001DAO getSdProgramacaoOf001DAO() {
        return (SdProgramacaoOf001DAO) getDao(SD_PROGRAMACAO_OF_001_DAO_CLASSNAME);
    }

    public static SdProgramacaoPacote001DAO getSdProgramacaoPacote001DAO() {
        return (SdProgramacaoPacote001DAO) getDao(SD_PROGRAMACAO_PACOTE_001_DAO_CLASSNAME);
    }

    public static SdEquipamentosOrganize001DAO getSdEquipamentosOrganize001DAO() {
        return (SdEquipamentosOrganize001DAO) getDao(SD_EQUIPAMENTOS_ORGANIZE_001_DAO_CLASSNAME);
    }

    public static SdAlteracaoOc001DAO getSdAlteracaoOc001DAO() {
        return (SdAlteracaoOc001DAO) getDao(SD_ALTERACAO_OC_001_DAO_CLASSNAME);
    }

    public static SdTurnoCelula001DAO getSdTurnoCelula001DAO() {
        return (SdTurnoCelula001DAO) getDao(SD_TURNO_CELULA_001_DAO_CLASSNAME);
    }

    public static VSdOfsParaProgramarDAO getVSdOfsParaProgramarDAO() {
        return (VSdOfsParaProgramarDAO) getDao(V_SD_OFS_PARA_PROGRAMAR_DAO_CLASSNAME);
    }

    public static VSdImpProgColaboradorDAO getVSdImpProgColaboradorDAO() {
        return (VSdImpProgColaboradorDAO) getDao(V_SD_IMP_PROG_COLABORADOR);
    }
    
    public static DashboardProducaoDAO getDashboardProducaoDAO() {
        return (DashboardProducaoDAO) getDao(DASHBOARD_PRODUCAO_CLASSNAME);
    }
    
    public static VSdProdDiariaCelulaDAO getVSdProdDiariaCelulaDAO() {
        return (VSdProdDiariaCelulaDAO) getDao(V_SD_PROD_DIARIA_CELULA_CLASSNAME);
    }
    
    public static FirebirdSQLDAO getFirebirdSQLDAO() {
        return (FirebirdSQLDAO) getDao(FIREBIRD_SQL_DAO_CLASSNAME);
    }
}
