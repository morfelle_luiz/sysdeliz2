package sysdeliz2.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DashboardProducaoDAO extends FactoryConnection {
    
    public DashboardProducaoDAO() throws SQLException {
        this.initConnection();
    }
    
    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
    
    public Map<String, Object> getProducaoDia(Integer celula) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'DD') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado), \n" +
                "                           sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador))-sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador)) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado), \n" +
                "                           sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador))-sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado,\n" +
                "       (select count(*)\n" +
                "          from sd_lancamento_parada_001 par\n" +
                "          join sd_colab_celula_001 cel\n" +
                "            on cel.colaborador = par.colaborador\n" +
                "         where cel.celula = " + celula + "\n" +
                "           and to_char(par.dh_inicio, 'DD/MM/YYYY') =\n" +
                "               to_char(sysdate, 'DD/MM/YYYY')) tot_paradas,\n" +
                "       nvl((select min(par.motivo)\n" +
                "          from sd_lancamento_parada_001 par\n" +
                "          join sd_colab_celula_001 cel\n" +
                "            on cel.colaborador = par.colaborador\n" +
                "         where cel.celula = " + celula + "\n" +
                "           and to_char(par.dh_inicio, 'DD/MM/YYYY') =\n" +
                "               to_char(sysdate, 'DD/MM/YYYY')\n" +
                "           and par.dh_fim is null),0) par_agora\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.celula = " + celula + " and dia.prod_cel = 'S'\n" +
                "   and to_char(dia.data, 'DD/MM/YYYY') = to_char(sysdate, 'DD/MM/YYYY')\n" +
                " group by to_char(data, 'DD')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("dia", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
            row.put("tot_paradas", resultSetSql.getInt(12));
            row.put("par_agora", resultSetSql.getInt(13));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoMes(Integer celula) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'MONTH') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum(tmp_disponivel) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.celula = " + celula + " and dia.prod_cel = 'S'\n" +
                "   and to_char(dia.data, 'MM/YYYY') = to_char(sysdate, 'MM/YYYY')\n" +
                "   and to_char(dia.data, 'DD/MM/YYYY') < to_char(sysdate-1, 'DD/MM/YYYY')\n" +
                " group by to_char(data, 'MONTH')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("mes", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoAno(Integer celula) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'YYYY') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum(tmp_disponivel) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.celula = " + celula + " and dia.prod_cel = 'S'\n" +
                "   and to_char(dia.data, 'YYYY') = to_char(sysdate, 'YYYY')\n" +
                "   and to_char(dia.data, 'MM/YYYY') < to_char(sysdate, 'MM/YYYY')\n" +
                " group by to_char(data, 'YYYY')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("ano", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoColaboradorDia(Integer colaborador) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'DD') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado), \n" +
                "                           sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador))-sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador)) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado), \n" +
                "                           sum((select tmp_disponivel - tmp_intervalo from v_sd_prod_hora_colab where colaborador = dia.colaborador))-sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado,\n" +
                "       (select count(*)\n" +
                "          from sd_lancamento_parada_001 par\n" +
                "         where par.colaborador = " + colaborador + "\n" +
                "           and to_char(par.dh_inicio, 'DD/MM/YYYY') =\n" +
                "               to_char(sysdate, 'DD/MM/YYYY')) tot_paradas,\n" +
                "       nvl((select min(par.motivo)\n" +
                "          from sd_lancamento_parada_001 par\n" +
                "         where par.colaborador = " + colaborador + "\n" +
                "           and to_char(par.dh_inicio, 'DD/MM/YYYY') =\n" +
                "               to_char(sysdate, 'DD/MM/YYYY')\n" +
                "           and par.dh_fim is null),0) par_agora\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.colaborador = " + colaborador + "\n" +
                "   and to_char(dia.data, 'DD/MM/YYYY') = to_char(sysdate, 'DD/MM/YYYY')\n" +
                " group by to_char(data, 'DD')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("dia", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
            row.put("tot_paradas", resultSetSql.getInt(12));
            row.put("par_agora", resultSetSql.getInt(13));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoColaboradorMes(Integer colaborador) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'MONTH') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum(tmp_disponivel) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.colaborador = " + colaborador + "\n" +
                "   and to_char(dia.data, 'MM/YYYY') = to_char(sysdate, 'MM/YYYY')\n" +
                "   and to_char(dia.data, 'DD/MM/YYYY') < to_char(sysdate-1, 'DD/MM/YYYY')\n" +
                " group by to_char(data, 'MONTH')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("mes", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoColaboradorAno(Integer colaborador) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select to_char(data, 'YYYY') dia,\n" +
                "       count(distinct colaborador) qtde_colab,\n" +
                "       round(avg(qtde_oper), 0) med_opers,\n" +
                "       round(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum(tmp_disponivel) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel) - sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tmp_parado\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.colaborador = " + colaborador + "\n" +
                "   and to_char(dia.data, 'YYYY') = to_char(sysdate, 'YYYY')\n" +
                "   and to_char(dia.data, 'MM/YYYY') < to_char(sysdate, 'MM/YYYY')\n" +
                " group by to_char(data, 'YYYY')");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("ano", resultSetSql.getString(1));
            row.put("qtde_colab", resultSetSql.getInt(2));
            row.put("med_opers", resultSetSql.getInt(3));
            row.put("med_pecas", resultSetSql.getInt(4));
            row.put("tmp_realizado", resultSetSql.getDouble(5));
            row.put("tmp_programado", resultSetSql.getDouble(6));
            row.put("efic", resultSetSql.getDouble(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getDouble(10));
            row.put("tmp_parado", resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public Map<String, Object> getProducaoColaboradorPeriodo(Integer codigoColaborador, String dtInicio, String dtFim) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        
        StringBuilder query = new StringBuilder("" +
                "select trunc(avg(qtde_oper), 0) med_opers,\n" +
                "       trunc(avg(med_pecas), 0) med_pecas,\n" +
                "       sum(tmp_realizado) tmp_realizado,\n" +
                "       sum(tmp_programado) tmp_programado,\n" +
                "       sum(tmp_deduz_efic) tmp_deduz_efic,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_realizado))*100,0) oper,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel)-sum(tmp_deduz_efic))*100,0) efic,\n" +
                "       sum(tmp_prod_extra) tmp_prod_extra,\n" +
                "       sum(tmp_disponivel) tmp_disponivel,\n" +
                "       trunc(f_verifica_divisor(sum(tmp_programado),sum(tmp_disponivel)-sum(tmp_prod_extra))*100,0) prod,\n" +
                "       sum(tmp_parado) tp_parado\n" +
                "  from v_sd_diario_prod_colab dia\n" +
                " where dia.data between '" + dtInicio + "' and '" + dtFim + "'\n" +
                "   and dia.colaborador = " + codigoColaborador + "\n");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("med_opers", resultSetSql.getInt(1));
            row.put("med_pecas", resultSetSql.getInt(2));
            row.put("tmp_realizado", resultSetSql.getDouble(3));
            row.put("tmp_programado", resultSetSql.getDouble(4));
            row.put("tmp_deduz_efic", resultSetSql.getDouble(5));
            row.put("oper", resultSetSql.getInt(6));
            row.put("efic", resultSetSql.getInt(7));
            row.put("tmp_prod_extra", resultSetSql.getDouble(8));
            row.put("tmp_disponivel", resultSetSql.getDouble(9));
            row.put("prod", resultSetSql.getInt(10));
            row.put("tp_parado", resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
}
