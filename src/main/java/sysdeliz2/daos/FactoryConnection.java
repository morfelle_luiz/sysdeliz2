/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import org.apache.log4j.Logger;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.utils.Globals;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public abstract class FactoryConnection {

    private static final Logger LOG = Logger.getLogger(ConnectionFactory.class);

    protected Connection connection = null;
    protected Statement statement = null;
    protected PreparedStatement preparedStatement = null;
    protected CallableStatement callStatement = null;

    // Bloco comentado substituído pelo POOL HIKARI
    {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException exc) {
            exc.printStackTrace();
        }
    }

//    private static HikariConfig configDeliz = new HikariConfig();
//    private static HikariConfig configUpwave = new HikariConfig();
//    private static HikariDataSource dsDeliz = null;
//    private static HikariDataSource dsUpwave = null;

    private Connection getConnection(String url, String usuario, String senha) throws SQLException {
        Connection conn = DriverManager.getConnection(url, usuario, senha);
        conn.setAutoCommit(false);
        return conn;
    }

    protected Connection getArteliz3Connection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "deliz_ind", "deliz_ind");
    }

    public Connection getEmpresaConnection() throws SQLException {
        if (Globals.getEmpresaLogada().getCodigo().equals("1000")) {
            return getDelizConnection();
        } else {
            return getUPWaveConnection();
        }
    }

    protected Connection getDelizConnection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "divtierp", "divtierp");
//        Connection connectDs = null;
//
//        try {
//            if (dsDeliz == null) {
//                configDeliz.setJdbcUrl("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01");
//                configDeliz.setUsername("divtierp");
//                configDeliz.setPassword("divtierp");
//                configDeliz.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//                configDeliz.setIdleTimeout(600000);
//                configDeliz.setPoolName("NativePoolDlz");
//                configDeliz.setMaximumPoolSize(1);
//                configDeliz.setMinimumIdle(1);
//                configDeliz.addDataSourceProperty("cachePrepStmts" ,"false");
//                //config.addDataSourceProperty("prepStmtCacheSize" , "250");
//                //config.addDataSourceProperty("prepStmtCacheSqlLimit" , "2048");
//                dsDeliz = new HikariDataSource( configDeliz );
//                System.out.println("Iniciando novo POOL conexão NATIVA - DELIZ");
//            }
//            connectDs = dsDeliz.getConnection();
//        }catch (SQLTransientConnectionException exc) {
//            exc.printStackTrace();
//            LOG.info("Fechando POOL atual", exc);
//            dsDeliz.close();
//            dsDeliz = new HikariDataSource(configDeliz);
//            LOG.info("Iniciando novo POOL");
//            connectDs = dsDeliz.getConnection();
//        }
//
//        System.out.println("Return connection POOL NATIVE DELIZ");
//        return connectDs;
    }

    protected Connection getUPWaveConnection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "upwerp", "upwerp");
//        Connection connectDs = null;
//
//        try {
//            if (dsUpwave == null) {
//                configUpwave.setJdbcUrl("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01");
//                configUpwave.setUsername("upwerp");
//                configUpwave.setPassword("upwerp");
//                configUpwave.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//                configUpwave.setIdleTimeout(600000);
//                configUpwave.setPoolName("NativePoolUpw");
//                configUpwave.setMaximumPoolSize(1);
//                configUpwave.setMinimumIdle(1);
//                configUpwave.addDataSourceProperty("cachePrepStmts" ,"false");
//                //config.addDataSourceProperty("prepStmtCacheSize" , "250");
//                //config.addDataSourceProperty("prepStmtCacheSqlLimit" , "2048");
//                dsUpwave = new HikariDataSource( configUpwave );
//                System.out.println("Iniciando novo POOL conexão NATIVA - UPWAVE");
//            }
//            connectDs = dsUpwave.getConnection();
//        }catch (SQLTransientConnectionException exc) {
//            exc.printStackTrace();
//            LOG.info("Fechando POOL atual", exc);
//            dsUpwave.close();
//            dsUpwave = new HikariDataSource(configUpwave);
//            LOG.info("Iniciando novo POOL");
//            connectDs = dsUpwave.getConnection();
//        }
//
//        System.out.println("Return connection POOL NATIVE UPWAVE");
//        return connectDs;
    }

    protected Connection getDelizTestConnection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "divtitest", "divtitest");
    }
    
    protected Connection getFirebirdEmpresaConnection() throws SQLException {
//        if (Globals.getEmpresaLogada().getCodigo().equals("1000")) {
//            return getFirebirdDelizConnection();
//        } else {
//            return getFirebirdUPWaveConnection();
//        }
        return null;
    }
    
    protected Connection getFirebirdDelizConnection() throws SQLException {
        return getConnection("jdbc:firebirdsql://divad02.nowar.corp:3050/C:\\DBVendas\\CONFIG1.FDB", "SYSDBA", "masterkey");
    }
    
    protected Connection getFirebirdUPWaveConnection() throws SQLException {
        return getConnection("jdbc:firebirdsql://s-upw-001.nowar.corp:3050/C:\\UPW_DB\\CONFIGUPW.FDB", "SYSDBA", "masterkey");
    }

    protected abstract void initConnection() throws SQLException;

    protected void initPreparedStatement(String sqlQuery) throws SQLException {
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(sqlQuery);
    }

    protected void initPreparedStatement(String sqlQuery, String[] argsPrepared) throws SQLException {
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(sqlQuery, argsPrepared);
    }

    protected void initCallableStatement(String sqlQuery) throws SQLException {
        statement = connection.createStatement();
        callStatement = connection.prepareCall(sqlQuery);
    }

    protected void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (callStatement != null) {
            callStatement.close();
        }
        statement.close();
        connection.commit();
        connection.close();
    }
    
    protected void closeConnectionFirebird() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (callStatement != null) {
            callStatement.close();
        }
        statement.close();
        connection.close();
    }
}
