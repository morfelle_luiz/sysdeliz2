/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.Familia001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class Familia001DAO extends FactoryConnection {

    public Familia001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public Familia001 getByCode(String codigo) throws SQLException {
        Familia001 row = null;

        StringBuilder query = new StringBuilder("select * from familia_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, codigo);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new Familia001(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

}
