/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Familia;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface FamiliaDAO {

    public ObservableList<Familia> getAll() throws SQLException;

    public ObservableList<Familia> getByWindowsForm(String p_familia, String p_grupo) throws SQLException;

    public Familia getByCode(String codigo) throws SQLException;

    public ObservableList<Familia> getFamiliasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

}
