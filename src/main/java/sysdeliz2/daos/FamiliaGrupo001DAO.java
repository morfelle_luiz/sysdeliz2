/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.FamiliaGrupo001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class FamiliaGrupo001DAO extends FactoryConnection {

    public FamiliaGrupo001DAO() throws SQLException {
        this.initConnection();
    }

    private String whereFilter;

    public ObservableList<FamiliaGrupo001> getAll() throws SQLException {
        ObservableList<FamiliaGrupo001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select distinct grp_fam.codigo, grp_fam.descricao, grp_fam.meta\n" +
                "  from familia_001 fam\n" +
                "  join familia_001 grp_fam\n" +
                "    on grp_fam.codigo = fam.fam_grp\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(FamiliaGrupo001 objectToSave) throws SQLException {
//        String[] returnId = {"CODIGO"};
//        StringBuilder query = new StringBuilder(""
//                + " insert into sd_turno_001\n"
//                + "   (descricao, hr_inicio, hr_fim, intervalo_inicio, intervalo_fim, ativo)\n"
//                + " values\n"
//                + "   (?, to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), ?)");
//
//        super.initPreparedStatement(query.toString(), returnId);
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
//        }
//        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
//        if (resultSetSql.next()) {
//            objectToSave.setCodigo(resultSetSql.getInt(1));
//        }
//        super.closeConnection();
    }

    public void update(FamiliaGrupo001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_turno_001\n"
//                + "   set descricao        = ?,\n"
//                + "       hr_inicio        = to_date(?, 'HH24:MI'),\n"
//                + "       hr_fim           = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_inicio = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_fim    = to_date(?, 'HH24:MI'),\n"
//                + "       ativo            = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(7, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public void delete(FamiliaGrupo001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder("delete from sd_turno_001 where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setInt(1, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public ObservableList<FamiliaGrupo001> getByDefault(String valueDefault) throws SQLException {
        ObservableList<FamiliaGrupo001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("*\n"
                + "  from familia_grupo_001\n"
                + " where descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    //--------------------------------------------------------------------------
    public ObservableList<FamiliaGrupo001> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<FamiliaGrupo001> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder("" +
                "select distinct grp_fam.codigo, grp_fam.descricao, grp_fam.meta\n" +
                "  from familia_001 fam\n" +
                "  join familia_001 grp_fam\n" +
                "    on grp_fam.codigo = fam.fam_grp\n" +
                " where " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public FamiliaGrupo001 getByCode(Integer code) throws SQLException {
        FamiliaGrupo001 row = null;

        StringBuilder query = new StringBuilder("" +
                "select distinct grp_fam.codigo, grp_fam.descricao, grp_fam.meta\n" +
                "  from familia_001 fam\n" +
                "  join familia_001 grp_fam\n" +
                "    on grp_fam.codigo = fam.fam_grp\n" +
                " where grp_fam.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public ObservableList<FamiliaGrupo001> getNotInCell(Integer cellCode) throws SQLException {
        ObservableList<FamiliaGrupo001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select distinct grp_fam.codigo, grp_fam.descricao, grp_fam.meta\n" +
                "  from familia_001 fam\n" +
                "  join familia_001 grp_fam\n" +
                "    on grp_fam.codigo = fam.fam_grp\n" +
                " where grp_fam.codigo not in (select familia_grp from sd_familias_celula_001 where celula = ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, cellCode);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<FamiliaGrupo001> getInCell(Integer cellCode) throws SQLException {
        ObservableList<FamiliaGrupo001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from familia_grupo_001\n"
                + " where codigo not in (select familia_grp from sd_familias_celula_001 where celula = ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, cellCode);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new FamiliaGrupo001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
}
