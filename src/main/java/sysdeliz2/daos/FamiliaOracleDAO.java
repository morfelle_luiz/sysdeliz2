/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Familia;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class FamiliaOracleDAO implements FamiliaDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Familia> getAll() throws SQLException {
        ObservableList<Familia> familia = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT FAM.CODIGO, FAM.DESCRICAO, FAM.FAM_GRP, \n"
                        + "	GFA.DESCRICAO DESC_GFAM \n"
                        + "FROM FAMILIA_001 FAM\n"
                        + "INNER JOIN FAMILIA_GRUPO_001 GFA \n"
                        + "	ON GFA.CODIGO = FAM.FAM_GRP");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            familia.add(new Familia(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("FAM_GRP"), resultSetSql.getString("DESC_GFAM")));
        }
        closeConnection();
        resultSetSql.close();

        return familia;
    }

    @Override
    public Familia getByCode(String codigo) throws SQLException {
        Familia familia = null;

        StringBuilder query = new StringBuilder(
                "SELECT FAM.CODIGO, FAM.DESCRICAO, FAM.FAM_GRP, \n"
                        + "	GFA.DESCRICAO DESC_GFAM \n"
                        + "FROM FAMILIA_001 FAM\n"
                        + "INNER JOIN FAMILIA_GRUPO_001 GFA \n"
                        + "	ON GFA.CODIGO = FAM.FAM_GRP\n"
                        + "WHERE FAM.CODIGO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            familia = new Familia(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("FAM_GRP"), resultSetSql.getString("DESC_GFAM"));
        }
        closeConnection();
        resultSetSql.close();

        return familia;
    }

    @Override
    public ObservableList<Familia> getByWindowsForm(String p_familia, String p_grupo) throws SQLException {
        ObservableList<Familia> familia = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT FAM.CODIGO, FAM.DESCRICAO, FAM.FAM_GRP, \n"
                        + "	GFA.DESCRICAO DESC_GFAM \n"
                        + "FROM FAMILIA_001 FAM\n"
                        + "INNER JOIN FAMILIA_GRUPO_001 GFA \n"
                        + "	ON GFA.CODIGO = FAM.FAM_GRP\n"
                        + "WHERE (FAM.CODIGO LIKE ? OR FAM.DESCRICAO LIKE ?) AND GFA.DESCRICAO LIKE ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + p_familia + "%");
        preparedStatement.setString(2, "%" + p_familia + "%");
        preparedStatement.setString(3, "%" + p_grupo + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            familia.add(new Familia(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("FAM_GRP"), resultSetSql.getString("DESC_GFAM")));
        }
        closeConnection();
        resultSetSql.close();

        return familia;
    }

    @Override
    public ObservableList<Familia> getFamiliasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Familia> familias = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from FAMILIA_001 \n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Familia familia = new Familia();
            familia.setCodigo(resultSetSql.getString("codigo"));
            familia.setDescricao(resultSetSql.getString("descricao"));
            familias.add(familia);
        }
        resultSetSql.close();
        this.closeConnection();
        return familias;
    }

}
