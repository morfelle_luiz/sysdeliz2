/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.ClienteFaturaAReceber;
import sysdeliz2.models.EmailBoleto;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface FaturaAReceberDAO {

    public ObservableList<ClienteFaturaAReceber> getAll() throws SQLException;

    public void saveLogMailBoleto(String p_codCli, String p_usuario, String p_duplicatas, String p_dataRef) throws SQLException;

    public ObservableList<EmailBoleto> getLogMailDia(String data_ref) throws SQLException;

    public ObservableList<ClienteFaturaAReceber> getByForm(String codcli, String data_ref, Boolean only_vencidas, Boolean only_aberta) throws SQLException;
}
