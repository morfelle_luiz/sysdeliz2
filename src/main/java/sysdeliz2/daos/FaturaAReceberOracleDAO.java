/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ClienteFaturaAReceber;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.EmailBoleto;
import sysdeliz2.models.FaturaAReceber;

import java.sql.*;

/**
 *
 * @author cristiano.diego
 */
public class FaturaAReceberOracleDAO implements FaturaAReceberDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    public ObservableList<FaturaAReceber> getAllFaturaCliente(String cod_cli) throws SQLException {
        ObservableList<FaturaAReceber> faturas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CLI.CODCLI, REC.FATURA, REC.NUMERO DUPLICATA, REC.VALOR VALOR_FATURA, (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV) VALOR_DUPLICATA, REC.DT_VENCTO, REC.DT_EMISSAO, PED.NUMERO PEDIDO, REC.OBS, 'A' STATUS\n"
                + "FROM RECEBER_001 REC\n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO\n"
                + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = PED.CODREP\n"
                + "WHERE REC.DT_VENCTO BETWEEN TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') + 1 AND TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') + CASE WHEN TO_CHAR(SYSDATE,'d') = 6 THEN 3 ELSE 1 END\n"
                + "      AND REC.SITUACAO NOT IN ('4','5')\n"
                + "      AND REC.STATUS = 'N'\n"
                + "	 AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                + "      AND CLI.CODCLI = '" + cod_cli + "'\n"
                + "UNION ALL\n"
                + "SELECT CLI.CODCLI, REC.FATURA, REC.NUMERO DUPLICATA, REC.VALOR VALOR_FATURA, (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV) VALOR_DUPLICATA, REC.DT_VENCTO, REC.DT_EMISSAO, PED.NUMERO PEDIDO, REC.OBS, 'B' STATUS\n"
                + "FROM RECEBER_001 REC\n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO\n"
                + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = PED.CODREP\n"
                + "WHERE REC.DT_VENCTO = TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') - 5\n"
                + "      AND REC.SITUACAO NOT IN ('4','5')\n"
                + "      AND REC.STATUS = 'N'\n"
                + "	 AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                + "      AND CLI.CODCLI = '" + cod_cli + "'\n"
                + "ORDER BY 1, 3");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            faturas.add(new FaturaAReceber(resultSetSql.getString("CODCLI"), resultSetSql.getString("OBS"), resultSetSql.getString("FATURA"), resultSetSql.getString("DUPLICATA"), resultSetSql.getString("PEDIDO"),
                    resultSetSql.getDouble("VALOR_FATURA"), resultSetSql.getDouble("VALOR_DUPLICATA"), resultSetSql.getString("DT_VENCTO"), resultSetSql.getString("DT_EMISSAO"), resultSetSql.getString("STATUS")));
        }
        resultSetSql.close();

        return faturas;
    }

    public ObservableList<FaturaAReceber> getAllFaturaClienteByForm(String cod_cli, String data_ref, Boolean only_vencidas, Boolean only_aberta) throws SQLException {
        ObservableList<FaturaAReceber> faturas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder();
        if (only_aberta) {
            query.append("SELECT CLI.CODCLI, REC.FATURA, REC.NUMERO DUPLICATA, REC.VALOR VALOR_FATURA, (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV) VALOR_DUPLICATA, REC.DT_VENCTO, REC.DT_EMISSAO, PED.NUMERO PEDIDO, REC.OBS, 'A' STATUS\n"
                    + "FROM RECEBER_001 REC\n"
                    + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                    + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO\n"
                    + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = PED.CODREP\n"
                    + "WHERE REC.DT_VENCTO BETWEEN TO_DATE('" + data_ref + "','dd/MM/yyyy') + 1 AND TO_DATE('" + data_ref + "','dd/MM/yyyy') + CASE WHEN TO_CHAR(TO_DATE('" + data_ref + "','dd/MM/yyyy'),'d') = 6 THEN 3 ELSE 1 END\n"
                    + "      AND REC.SITUACAO NOT IN ('4','5')\n"
                    + "      AND REC.STATUS = 'N'\n"
                    + "	 AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                    + "      AND CLI.CODCLI = '" + cod_cli + "'\n");
        }
        if (only_aberta && only_vencidas) {
            query.append("UNION ALL\n");
        }
        if (only_vencidas) {
            query.append("SELECT CLI.CODCLI, REC.FATURA, REC.NUMERO DUPLICATA, REC.VALOR VALOR_FATURA, (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV) VALOR_DUPLICATA, REC.DT_VENCTO, REC.DT_EMISSAO, PED.NUMERO PEDIDO, REC.OBS, 'B' STATUS\n"
                    + "FROM RECEBER_001 REC\n"
                    + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                    + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO\n"
                    + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = PED.CODREP\n"
                    + "WHERE REC.DT_VENCTO = TO_DATE('" + data_ref + "','dd/MM/yyyy') - 5\n"
                    + "      AND REC.SITUACAO NOT IN ('4','5')\n"
                    + "      AND REC.STATUS = 'N'\n"
                    + "	 AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                    + "      AND CLI.CODCLI = '" + cod_cli + "'\n"
                    + "ORDER BY 1, 3");
        }

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            faturas.add(new FaturaAReceber(resultSetSql.getString("CODCLI"), resultSetSql.getString("OBS"), resultSetSql.getString("FATURA"), resultSetSql.getString("DUPLICATA"), resultSetSql.getString("PEDIDO"),
                    resultSetSql.getDouble("VALOR_FATURA"), resultSetSql.getDouble("VALOR_DUPLICATA"), resultSetSql.getString("DT_VENCTO"), resultSetSql.getString("DT_EMISSAO"), resultSetSql.getString("STATUS")));
        }
        resultSetSql.close();

        return faturas;
    }

    @Override
    public ObservableList<ClienteFaturaAReceber> getAll() throws SQLException {
        ObservableList<ClienteFaturaAReceber> faturas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT CLI.CODCLI, CLI.NOME CLIENTE, CLI.CNPJ, LOWER(CLI.EMAIL) EMAIL, CLI.TELEFONE, CLI.SIT_CLI TIPO_CLI, REP.NOME REPRESENT, CASE WHEN MAIL.CODCLI IS NULL THEN 'A' ELSE 'E' END STS, CLI.SITE TP_ENV\n"
                + "FROM RECEBER_001 REC\n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO AND PED.CODCLI = CLI.CODCLI\n"
                + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = CLI.CODREP\n"
                + "LEFT JOIN SD_LOG_MAIL_BOLETO_001 MAIL ON MAIL.CODCLI = CLI.CODCLI AND TO_DATE(MAIL.DATA_REF,'dd/MM/yyyy') = TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy')\n"
                + "WHERE REC.DT_VENCTO BETWEEN TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') + 1 AND TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') + CASE WHEN TO_CHAR(SYSDATE,'d') = 6 THEN 3 ELSE 1 END \n"
                + "	  AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                + "       AND REC.BORDERO <> 0 AND REC.NRBANCO IS NOT NULL\n"
                + "      AND REC.SITUACAO NOT IN ('04','05')\n"
                + "	  AND REC.CLASSE NOT IN ('0172','0173')\n"
                + "	  AND (UPPER(CLI.SITE) = 'S' OR UPPER(CLI.SITE) = 'SL')\n"
                + "UNION\n"
                + "SELECT DISTINCT CLI.CODCLI, CLI.NOME CLIENTE, CLI.CNPJ, LOWER(CLI.EMAIL) EMAIL, CLI.TELEFONE, CLI.SIT_CLI TIPO_CLI, REP.NOME REPRESENT, CASE WHEN MAIL.CODCLI IS NULL THEN 'A' ELSE 'E' END STS, CLI.SITE TP_ENV\n"
                + "FROM RECEBER_001 REC\n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO AND PED.CODCLI = CLI.CODCLI\n"
                + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = CLI.CODREP\n"
                + "LEFT JOIN SD_LOG_MAIL_BOLETO_001 MAIL ON MAIL.CODCLI = CLI.CODCLI AND TO_DATE(MAIL.DATA_REF,'dd/MM/yyyy') = TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy')\n"
                + "WHERE REC.DT_VENCTO = TO_DATE(TO_CHAR(SYSDATE,'dd/MM/yyyy'),'dd/MM/yyyy') - 5 \n"
                + "	  AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                + "       AND REC.BORDERO <> 0 AND REC.NRBANCO IS NOT NULL\n"
                + "      AND REC.SITUACAO NOT IN ('04','05')\n"
                + "	  AND REC.CLASSE NOT IN ('0172','0173')\n"
                + "	  AND (UPPER(CLI.SITE) = 'S' OR UPPER(CLI.SITE) = 'SL')\n"
                + "ORDER BY 2");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            faturas.add(new ClienteFaturaAReceber(resultSetSql.getString("CODCLI"), resultSetSql.getString("CLIENTE"), resultSetSql.getString("CNPJ"), resultSetSql.getString("EMAIL"),
                    resultSetSql.getString("TELEFONE"), resultSetSql.getString("TIPO_CLI"), resultSetSql.getString("REPRESENT"), resultSetSql.getString("STS"), resultSetSql.getString("TP_ENV"),
                    getAllFaturaCliente(resultSetSql.getString("CODCLI"))));
        }

        closeConnection();
        resultSetSql.close();

        return faturas;
    }

    @Override
    public void saveLogMailBoleto(String p_codCli, String p_usuario, String p_duplicatas, String p_dataRef) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_LOG_MAIL_BOLETO_001\n"
                + "(CODCLI, DATA_MAIL, USUARIO, DUPLICATA, DATA_REF)\n"
                + "VALUES(?, SYSDATE, ?, ?, ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, p_codCli);
        preparedStatement.setString(2, p_usuario);
        preparedStatement.setString(3, p_duplicatas);
        preparedStatement.setString(4, p_dataRef);
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public ObservableList<EmailBoleto> getLogMailDia(String data_ref) throws SQLException {
        ObservableList<EmailBoleto> logs = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT MAIL.DATA_MAIL, CLI.CODCLI, CLI.NOME, MAIL.DUPLICATA, MAIL.USUARIO, MAIL.DATA_REF \n"
                + "FROM SD_LOG_MAIL_BOLETO_001 MAIL \n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = MAIL.CODCLI\n"
                + "WHERE TO_DATE(MAIL.DATA_REF,'dd/MM/yyyy') = TO_DATE('" + data_ref + "','dd/MM/yyyy')\n"
                + "ORDER BY CLI.NOME");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            logs.add(new EmailBoleto(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("DUPLICATA"),
                    resultSetSql.getString("USUARIO"), resultSetSql.getString("DATA_MAIL"), resultSetSql.getString("DATA_REF")));
        }

        closeConnection();
        resultSetSql.close();

        return logs;
    }

    @Override
    public ObservableList<ClienteFaturaAReceber> getByForm(String codcli, String data_ref, Boolean only_vencidas, Boolean only_aberta) throws SQLException {
        ObservableList<ClienteFaturaAReceber> faturas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder();
        if (only_aberta) {
            query.append("SELECT DISTINCT CLI.CODCLI, CLI.NOME CLIENTE, CLI.CNPJ, LOWER(CLI.EMAIL) EMAIL, CLI.TELEFONE, CLI.SIT_CLI TIPO_CLI, REP.NOME REPRESENT, CASE WHEN MAIL.CODCLI IS NULL THEN 'A' ELSE 'E' END STS, CLI.SITE TP_ENV\n"
                    + "FROM RECEBER_001 REC\n"
                    + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                    + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO AND PED.CODCLI = CLI.CODCLI\n"
                    + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = CLI.CODREP\n"
                    + "LEFT JOIN SD_LOG_MAIL_BOLETO_001 MAIL ON MAIL.CODCLI = CLI.CODCLI AND TO_DATE(MAIL.DATA_REF,'dd/MM/yyyy') = TO_DATE('" + data_ref + "','dd/MM/yyyy')\n"
                    + "WHERE REC.DT_VENCTO BETWEEN TO_DATE('" + data_ref + "','dd/MM/yyyy') + 1 AND TO_DATE('" + data_ref + "','dd/MM/yyyy') + CASE WHEN TO_CHAR(TO_DATE('" + data_ref + "','dd/MM/yyyy'),'d') = 6 THEN 3 ELSE 1 END \n"
                    + "	  AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                    + "   AND REC.BORDERO <> 0 AND REC.NRBANCO IS NOT NULL\n"
                    + "      AND REC.SITUACAO NOT IN ('04','05')\n"
                    + "	  AND REC.CLASSE NOT IN ('0172','0173')\n"
                    + "	  AND (UPPER(CLI.SITE) = 'S' OR UPPER(CLI.SITE) = 'SL')\n"
                    + "	  AND REGEXP_LIKE(CLI.CODCLI,'*(" + codcli + ")')\n");
        }
        if (only_aberta && only_vencidas) {
            query.append("UNION\n");
        }
        if (only_vencidas) {
            query.append("SELECT DISTINCT CLI.CODCLI, CLI.NOME CLIENTE, CLI.CNPJ, LOWER(CLI.EMAIL) EMAIL, CLI.TELEFONE, CLI.SIT_CLI TIPO_CLI, REP.NOME REPRESENT, CASE WHEN MAIL.CODCLI IS NULL THEN 'A' ELSE 'E' END STS, CLI.SITE TP_ENV\n"
                    + "FROM RECEBER_001 REC\n"
                    + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = REC.CODCLI\n"
                    + "INNER JOIN PEDIDO_001 PED ON PED.NUMERO = REC.PEDIDO AND PED.CODCLI = CLI.CODCLI\n"
                    + "INNER JOIN REPRESEN_001 REP ON REP.CODREP = CLI.CODREP\n"
                    + "LEFT JOIN SD_LOG_MAIL_BOLETO_001 MAIL ON MAIL.CODCLI = CLI.CODCLI AND TO_DATE(MAIL.DATA_REF,'dd/MM/yyyy') = TO_DATE('" + data_ref + "','dd/MM/yyyy')\n"
                    + "WHERE REC.DT_VENCTO = TO_DATE('" + data_ref + "','dd/MM/yyyy') - 5 \n"
                    + "	  AND VALOR_PAGO < (REC.VALOR2-REC.DESCONTO+REC.JUROS-REC.VAL_DEV)\n"
                    + "   AND REC.BORDERO <> 0 AND REC.NRBANCO IS NOT NULL\n"
                    + "      AND REC.SITUACAO NOT IN ('04','05')\n"
                    + "	  AND REC.CLASSE NOT IN ('0172','0173')\n"
                    + "	  AND (UPPER(CLI.SITE) = 'S' OR UPPER(CLI.SITE) = 'SL')\n"
                    + "	  AND REGEXP_LIKE(CLI.CODCLI,'*(" + codcli + ")')\n"
                    + "ORDER BY 2");
        }

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            faturas.add(new ClienteFaturaAReceber(resultSetSql.getString("CODCLI"), resultSetSql.getString("CLIENTE"), resultSetSql.getString("CNPJ"), resultSetSql.getString("EMAIL"),
                    resultSetSql.getString("TELEFONE"), resultSetSql.getString("TIPO_CLI"), resultSetSql.getString("REPRESENT"), resultSetSql.getString("STS"), resultSetSql.getString("TP_ENV"),
                    getAllFaturaClienteByForm(resultSetSql.getString("CODCLI"), data_ref, only_vencidas, only_aberta)));
        }

        closeConnection();
        resultSetSql.close();

        return faturas;
    }

}
