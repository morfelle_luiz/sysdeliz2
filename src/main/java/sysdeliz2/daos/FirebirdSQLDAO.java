/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cristiano.diego
 */
public class FirebirdSQLDAO extends FactoryConnection {

    public FirebirdSQLDAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getFirebirdEmpresaConnection();
    }

    public ObservableList<Map<String, String>> getProdutosEsgotados(String codigo) throws SQLException {
        ObservableList<Map<String, String>> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "SELECT DISTINCT  cie.CIE_COR, cie.CIE_TAM\n" +
                "  FROM CONFIG_ITENS_ESGOTADOS cie\n" +
                " WHERE cie.CIE_CODI = '"+codigo+"'" +
                "   AND cie.CIE_CONFIG IN\n" +
                "       (SELECT car.CONF_CODIGO\n" +
                "          FROM CONFIG car\n" +
                "         WHERE car.CONF_DTULTALTER =\n" +
                "               (SELECT MAX(car2.CONF_DTCRIACAO) FROM CONFIG car2))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, String> linha = new HashMap<>();
            linha.put("cor", resultSetSql.getString(1));
            linha.put("tam", resultSetSql.getString(2));
            rows.add(linha);
        }
        resultSetSql.close();
        super.closeConnectionFirebird();

        return rows;
    }
    
    public ObservableList<Map<String, String>> getProdutosBloqueados(String codigo) throws SQLException {
        ObservableList<Map<String, String>> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "SELECT DISTINCT cie.CIB_COR, cie.CIB_TAM\n" +
                "  FROM CONFIG_ITENS_BLOQ_EST cie\n" +
                " WHERE cie.CIB_CODI = '"+codigo+"'" +
                "   AND cie.CIB_CONFIG IN\n" +
                "       (SELECT car.CONF_CODIGO\n" +
                "          FROM CONFIG car\n" +
                "         WHERE car.CONF_DTULTALTER =\n" +
                "               (SELECT MAX(car2.CONF_DTCRIACAO) FROM CONFIG car2))");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, String> linha = new HashMap<>();
            linha.put("cor", resultSetSql.getString(1));
            linha.put("tam", resultSetSql.getString(2));
            rows.add(linha);
        }
        resultSetSql.close();
        super.closeConnectionFirebird();
        
        return rows;
    }
}
