/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Cliente;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface FornecedorDAO {

    public ObservableList<Cliente> getAll() throws SQLException;

    public ObservableList<Cliente> getByForm(String cliente, String informacao) throws SQLException;

    public Cliente getByCodigo(String codcli) throws SQLException;

}
