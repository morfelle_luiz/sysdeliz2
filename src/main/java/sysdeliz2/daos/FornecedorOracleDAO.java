/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.Cliente;
import sysdeliz2.models.ConnectionFactory;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class FornecedorOracleDAO implements FornecedorDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Cliente> getAll() throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST,\n"
                        + "	   ENT.END_ENT||','||ENT.NUM_ENT ENDERECO, ENT.BAIRRO_ENT BAIRRO, ENT.COMPLEMENTO REFERENCIA, ENT.TELEFONE \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S' AND ENT.TIPO_ENTIDADE LIKE '%F%'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP"),
                    resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public ObservableList<Cliente> getByForm(String cliente, String informacao) throws SQLException {
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST,\n"
                        + "	   ENT.END_ENT||','||ENT.NUM_ENT ENDERECO, ENT.BAIRRO_ENT BAIRRO, ENT.COMPLEMENTO REFERENCIA, ENT.TELEFONE \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S' AND ENT.TIPO_ENTIDADE LIKE '%F%'\n"
                        + "	AND (ENT.CODCLI LIKE ?\n"
                        + "	OR ENT.NOME LIKE ?\n"
                        + "	OR ENT.FANTASIA LIKE ?\n"
                        + "	OR ENT.CNPJ LIKE ?)\n"
                        + "	AND (CEP.CEP LIKE ?\n"
                        + "	OR CID.NOME_CID LIKE ?\n"
                        + "	OR CID.COD_CID LIKE ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(2, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(3, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(4, "%" + cliente.toUpperCase() + "%");
        preparedStatement.setString(5, "%" + informacao.toUpperCase() + "%");
        preparedStatement.setString(6, "%" + informacao.toUpperCase() + "%");
        preparedStatement.setString(7, "%" + informacao.toUpperCase() + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            clientes.add(new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP"),
                    resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE")));
        }

        closeConnection();
        resultSetSql.close();

        return clientes;
    }

    @Override
    public Cliente getByCodigo(String codcli) throws SQLException {
        Cliente cliente = null;

        StringBuilder query = new StringBuilder(
                "SELECT ENT.CODCLI, ENT.NOME, ENT.FANTASIA, ENT.CNPJ, CEP.CEP, CID.NOME_CID, CID.COD_EST,\n"
                        + "	   ENT.END_ENT||','||ENT.NUM_ENT ENDERECO, ENT.BAIRRO_ENT BAIRRO, ENT.COMPLEMENTO REFERENCIA, ENT.TELEFONE \n"
                        + "FROM ENTIDADE_001 ENT\n"
                        + "INNER JOIN CADCEP_001 CEP ON CEP.CEP = ENT.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON CID.COD_CID = CEP.COD_CID\n"
                        + "WHERE ENT.ATIVO = 'S' AND ENT.TIPO_ENTIDADE LIKE '%F%'\n"
                        + "	AND ENT.CODCLI = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codcli);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            cliente = new Cliente(resultSetSql.getString("CODCLI"), resultSetSql.getString("NOME"), resultSetSql.getString("FANTASIA"),
                    resultSetSql.getString("CNPJ"), resultSetSql.getString("NOME_CID"), resultSetSql.getString("COD_EST"), resultSetSql.getString("CEP"),
                    resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"));
        }

        closeConnection();
        resultSetSql.close();

        return cliente;
    }

}
