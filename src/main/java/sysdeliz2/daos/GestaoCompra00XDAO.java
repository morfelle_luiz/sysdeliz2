/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.properties.GestaoCompra00X;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class GestaoCompra00XDAO extends FactoryConnection {

    public GestaoCompra00XDAO() throws SQLException {
        this.initConnection();
    }

    public ObservableList<GestaoCompra00X> getAll() throws SQLException {
        return this.getFromFilter("and status not in ('G','E')");
//        ObservableList<GestaoCompra00X> rows = FXCollections.observableArrayList();
//
//        String query = "select *\n"
//                + "  from ( ---------------- SELECT ORDENS DE COMPRA --------------------\n"
//                + "        select forn.codcli,\n"
//                + "                forn.nome,\n"
//                + "                case\n"
//                + "                  when sdcop.ultimo_email is not null then\n"
//                + "                   sdcop.ultimo_email\n"
//                + "                  else\n"
//                + "                   forn.email\n"
//                + "                end email,\n"
//                + "                cop.numero,\n"
//                + "                sdcoi.ped_fornecedor,\n"
//                + "                'MAT' tipo,\n"
//                + "                mat.codigo,\n"
//                + "                mat.descricao,\n"
//                + "                coi.cor,\n"
//                + "                coi.tam,\n"
//                + "                coi.ordem,\n"
//                + "                mat.grupo,\n"
//                + "                mat.sub_grupo,\n"
//                + "                coi.unidade,\n"
//                + "                coi.qtde,\n"
//                + "                coi.preco,\n"
//                + "                coi.valor,\n"
//                + "                coi.deposito,\n"
//                + "                to_char(coi.dt_entrega, 'DD/MM/YYYY') dt_entrega,\n"
//                + "                to_char(coi.dt_fatura, 'DD/MM/YYYY') dt_fatura,\n"
//                + "                sdcoi.nf_fornecedor,\n"
//                + "                case\n"
//                + "                  when coi.dt_fatura is null then\n"
//                + "                   'S'\n"
//                + "                  else\n"
//                + "                   case\n"
//                + "                     when coi.dt_fatura <\n"
//                + "                          to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n"
//                + "                          sdcoi.nf_fornecedor is null then\n"
//                + "                      'R'\n"
//                + "                     else\n"
//                + "                      case\n"
//                + "                        when coi.dt_fatura >=\n"
//                + "                             to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') then\n"
//                + "                         'Y'\n"
//                + "                        else\n"
//                + "                         case\n"
//                + "                           when coi.dt_fatura <\n"
//                + "                                to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n"
//                + "                                sdcoi.nf_fornecedor is not null then\n"
//                + "                            'G'\n"
//                + "                         end\n"
//                + "                      end\n"
//                + "                   end\n"
//                + "                end status,\n"
//                + "                sdcop.obs_recebimento,\n"
//                + "                trim(cop.condicao) condicao,\n"
//                + "                coi.especifica,\n"
//                + "                case\n"
//                + "                  when nvl((select count(*)\n"
//                + "                             from sd_agenda_servico_001\n"
//                + "                            where tipo = 'OC1'\n"
//                + "                              and campo1 = cop.numero\n"
//                + "                              and campo2 = forn.codcli\n"
//                + "                              and dt_agenda >= sysdate),\n"
//                + "                           0) > 0 then\n"
//                + "                   'B'\n"
//                + "                  else\n"
//                + "                   case\n"
//                + "                     when nvl((select count(*)\n"
//                + "                                from sd_log_email_oc_001\n"
//                + "                               where codcli = forn.codcli\n"
//                + "                                 and numero = cop.numero\n"
//                + "                                 and codigo = coi.codigo\n"
//                + "                                 and cor = coi.cor),\n"
//                + "                              0) > 0 then\n"
//                + "                      'E'\n"
//                + "                   end\n"
//                + "                end status_envio,\n"
//                + "                fis.descricao ncm,\n"
//                + "                nvl(corfor.cor1,'N/D') corf\n"
//                + "          from compra_001 cop\n"
//                + "         inner join sd_compra_001 sdcop\n"
//                + "            on sdcop.numero = cop.numero\n"
//                + "         inner join co_iten_001 coi\n"
//                + "            on coi.numero = cop.numero\n"
//                + "         inner join sd_co_iten_001 sdcoi\n"
//                + "            on sdcoi.numero = coi.numero\n"
//                + "           and sdcoi.codigo = coi.codigo\n"
//                + "           and sdcoi.cor = coi.cor\n"
//                + "           and sdcoi.tam = coi.tam\n"
//                + "           and sdcoi.ordem = coi.ordem\n"
//                + "         inner join material_001 mat\n"
//                + "            on mat.codigo = coi.codigo\n"
//                + "         inner join tabfis_001 fis\n"
//                + "            on fis.codigo = mat.codfis\n"
//                + "         inner join entidade_001 forn\n"
//                + "            on forn.codcli = cop.codfor\n"
//                + "          left join cadcorti_001 corfor\n"
//                + "            on corfor.cor = coi.cor\n"
//                + "           and corfor.codcli = forn.codcli\n"
//                + "           and corfor.codigo = mat.codigo\n"
//                + "         where coi.situacao <> 'B'\n"
//                + "        ---------------- SELECT ORDENS DE TECELAGEM --------------------\n"
//                + "        union all\n"
//                + "        select forn.codcli,\n"
//                + "               forn.nome,\n"
//                + "               case\n"
//                + "                 when sdcop.ultimo_email is not null then\n"
//                + "                  sdcop.ultimo_email\n"
//                + "                 else\n"
//                + "                  forn.email\n"
//                + "               end email,\n"
//                + "               cop.numero,\n"
//                + "               sdcoi.ped_fornecedor,\n"
//                + "               'TEC' tipo,\n"
//                + "               mat.codigo,\n"
//                + "               mat.descricao,\n"
//                + "               coi.cor,\n"
//                + "               'X',\n"
//                + "               0,\n"
//                + "               mat.grupo,\n"
//                + "               mat.sub_grupo,\n"
//                + "               mat.unidade,\n"
//                + "               coi.qtde,\n"
//                + "               coi.custo,\n"
//                + "               (coi.qtde * coi.custo) valor,\n"
//                + "               cop.deposito,\n"
//                + "               to_char(cop.dt_inicio, 'DD/MM/YYYY') dt_entrega,\n"
//                + "               to_char(cop.dt_inicio, 'DD/MM/YYYY') dt_fatura,\n"
//                + "               sdcoi.nf_fornecedor,\n"
//                + "               case\n"
//                + "                 when cop.dt_inicio is null then\n"
//                + "                  'S'\n"
//                + "                 else\n"
//                + "                  case\n"
//                + "                    when cop.dt_inicio <\n"
//                + "                         to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n"
//                + "                         sdcoi.nf_fornecedor is null then\n"
//                + "                     'R'\n"
//                + "                    else\n"
//                + "                     case\n"
//                + "                       when cop.dt_inicio >=\n"
//                + "                            to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') then\n"
//                + "                        'Y'\n"
//                + "                       else\n"
//                + "                        case\n"
//                + "                          when cop.dt_inicio <\n"
//                + "                               to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n"
//                + "                               sdcoi.nf_fornecedor is not null then\n"
//                + "                           'G'\n"
//                + "                        end\n"
//                + "                     end\n"
//                + "                  end\n"
//                + "               end status,\n"
//                + "               sdcop.obs_recebimento,\n"
//                + "               trim(sdcop.condicao) condicao,\n"
//                + "               null especifica,\n"
//                + "               case\n"
//                + "                 when nvl((select count(*)\n"
//                + "                            from sd_agenda_servico_001\n"
//                + "                           where tipo = 'OT1'\n"
//                + "                             and campo1 = cop.numero\n"
//                + "                             and campo2 = forn.codcli\n"
//                + "                             and dt_agenda >= sysdate),\n"
//                + "                          0) > 0 then\n"
//                + "                  'B'\n"
//                + "                 else\n"
//                + "                  case\n"
//                + "                    when nvl((select count(*)\n"
//                + "                               from sd_log_email_oc_001\n"
//                + "                              where codcli = forn.codcli\n"
//                + "                                and numero = cop.numero\n"
//                + "                                and codigo = coi.codigo\n"
//                + "                                and cor = coi.cor),\n"
//                + "                             0) > 0 then\n"
//                + "                     'E'\n"
//                + "                  end\n"
//                + "               end status_envio,\n"
//                + "               fis.descricao ncm,\n"
//                + "               nvl(corfor.cor1,'N/D') corf\n"
//                + "          from tecel_001 cop\n"
//                + "         inner join sd_tecel_001 sdcop\n"
//                + "            on sdcop.numero = cop.numero\n"
//                + "         inner join tecel_iten_001 coi\n"
//                + "            on coi.numero = cop.numero\n"
//                + "         inner join sd_tecel_iten_001 sdcoi\n"
//                + "            on sdcoi.numero = coi.numero\n"
//                + "           and sdcoi.codigo = coi.codigo\n"
//                + "           and sdcoi.cor = coi.cor\n"
//                + "         inner join material_001 mat\n"
//                + "            on mat.codigo = coi.codigo\n"
//                + "         inner join tabfis_001 fis\n"
//                + "            on fis.codigo = mat.codfis\n"
//                + "         inner join entidade_001 forn\n"
//                + "            on forn.codcli = cop.malharia\n"
//                + "          left join cadcorti_001 corfor\n"
//                + "            on corfor.cor = coi.cor\n"
//                + "           and corfor.codcli = forn.codcli\n"
//                + "           and corfor.codigo = mat.codigo\n"
//                + "         where coi.baixada <> 'S') ordens_compra\n"
//                + " where status not in ('G','E')";
//
//        super.initPreparedStatement(query);
//
//        super.preparedStatement.execute();
//        ResultSet resultSetSql = super.preparedStatement.getResultSet();
//        while (resultSetSql.next()) {
//            rows.add(new GestaoCompra00X(resultSetSql.getString(1),
//                    resultSetSql.getString(2),
//                    resultSetSql.getString(3),
//                    resultSetSql.getString(4),
//                    resultSetSql.getString(5),
//                    resultSetSql.getString(6),
//                    resultSetSql.getString(7),
//                    resultSetSql.getString(8),
//                    resultSetSql.getString(9),
//                    resultSetSql.getString(10),
//                    resultSetSql.getInt(11),
//                    resultSetSql.getString(12),
//                    resultSetSql.getString(13),
//                    resultSetSql.getString(14),
//                    resultSetSql.getDouble(15),
//                    resultSetSql.getDouble(16),
//                    resultSetSql.getDouble(17),
//                    resultSetSql.getString(18),
//                    resultSetSql.getString(19),
//                    resultSetSql.getString(20),
//                    resultSetSql.getString(21),
//                    resultSetSql.getString(22),
//                    resultSetSql.getString(23),
//                    resultSetSql.getString(24),
//                    resultSetSql.getString(25),
//                    resultSetSql.getString(26),
//                    resultSetSql.getString(27),
//                    resultSetSql.getString(28)));
//        }
//        resultSetSql.close();
//        super.closeConnection();
//
//        return rows;
    }

    public ObservableList<GestaoCompra00X> getFromFilter(String filters) throws SQLException {
        ObservableList<GestaoCompra00X> rows = FXCollections.observableArrayList();

        String query = "select *\n" +
                "                  from ( ---------------- SELECT ORDENS DE COMPRA --------------------\n" +
                "                        select forn.codcli,\n" +
                "                                forn.cnpj,\n" +
                "                                forn.nome,\n" +
                "                                forn.dias,\n" +
                "                                (select min(mai.barra) from mat_iten_001 mai where mai.codigo = mat.codigo and mai.cor = coi.cor) min_forn,\n" +
                "                                case\n" +
                "                                  when sdcop.ultimo_email is not null then\n" +
                "                                   sdcop.ultimo_email\n" +
                "                                  else\n" +
                "                                   forn.email\n" +
                "                                end email,\n" +
                "                                cop.numero,\n" +
                "                                sdcoi.ped_fornecedor,\n" +
                "                                'MAT' tipo,\n" +
                "                                mat.codigo,\n" +
                "                                mat.descricao,\n" +
                "                                coi.cor,\n" +
                "                                coi.tam,\n" +
                "                                coi.ordem,\n" +
                "                                mat.grupo,\n" +
                "                                mat.sub_grupo,\n" +
                "                                coi.unidade,\n" +
                "                                coi.qtde,\n" +
                "                                coi.preco,\n" +
                "                                coi.valor,\n" +
                "                                coi.deposito,\n" +
                "                                to_char(coi.dt_entrega, 'DD/MM/YYYY') dt_entrega,\n" +
                "                                to_char(coi.dt_fatura, 'DD/MM/YYYY') dt_fatura,\n" +
                "                                sdcoi.nf_fornecedor,\n" +
                "                                case\n" +
                "                                  when coi.dt_fatura is null then\n" +
                "                                   'S'\n" +
                "                                  else\n" +
                "                                   case\n" +
                "                                     when coi.dt_fatura <\n" +
                "                                          to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n" +
                "                                          sdcoi.nf_fornecedor is null then\n" +
                "                                      'R'\n" +
                "                                     else\n" +
                "                                      case\n" +
                "                                        when coi.dt_fatura >=\n" +
                "                                             to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') then\n" +
                "                                         'Y'\n" +
                "                                        else\n" +
                "                                         case\n" +
                "                                           when coi.dt_fatura <\n" +
                "                                                to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n" +
                "                                                sdcoi.nf_fornecedor is not null then\n" +
                "                                            'G'\n" +
                "                                         end\n" +
                "                                      end\n" +
                "                                   end\n" +
                "                                end status,\n" +
                "                                sdcop.obs_recebimento,\n" +
                "                                trim(cop.condicao) condicao,\n" +
                "                                coi.especifica,\n" +
                "                                case\n" +
                "                                  when nvl((select count(*)\n" +
                "                                             from sd_agenda_servico_001\n" +
                "                                            where tipo = 'OC1'\n" +
                "                                              and campo1 = cop.numero\n" +
                "                                              and campo2 = forn.codcli\n" +
                "                                              and dt_agenda >= sysdate),\n" +
                "                                           0) > 0 then\n" +
                "                                   'B'\n" +
                "                                  else\n" +
                "                                   case\n" +
                "                                     when nvl((select count(*)\n" +
                "                                                from sd_log_email_oc_001\n" +
                "                                               where codcli = forn.codcli\n" +
                "                                                 and numero = cop.numero\n" +
                "                                                 and codigo = coi.codigo\n" +
                "                                                 and cor = coi.cor),\n" +
                "                                              0) > 0 then\n" +
                "                                      'E'\n" +
                "                                   end\n" +
                "                                end status_envio,\n" +
                "                                fis.descricao ncm,\n" +
                "                                nvl(corfor.cor1,'N/D') corf,\n" +
                "                                to_char(cop.dt_emissao, 'DD/MM/YYYY') dt_emissao,\n" +
                "                                sdcoi.periodo,\n" +
                "                                sdcoi.periodo_orig,\n" +
                "                                'C' origem\n" +
                "                          from compra_001 cop\n" +
                "                         inner join sd_compra_001 sdcop\n" +
                "                            on sdcop.numero = cop.numero\n" +
                "                         inner join co_iten_001 coi\n" +
                "                            on coi.numero = cop.numero\n" +
                "                         inner join sd_co_iten_001 sdcoi\n" +
                "                            on sdcoi.numero = coi.numero\n" +
                "                           and sdcoi.codigo = coi.codigo\n" +
                "                           and sdcoi.cor = coi.cor\n" +
                "                           and sdcoi.tam = coi.tam\n" +
                "                           and sdcoi.ordem = coi.ordem\n" +
                "                         inner join material_001 mat\n" +
                "                            on mat.codigo = coi.codigo\n" +
                "                         inner join tabfis_001 fis\n" +
                "                            on fis.codigo = mat.codfis\n" +
                "                         inner join entidade_001 forn\n" +
                "                            on forn.codcli = cop.codfor\n" +
                "                          left join cadcorti_001 corfor\n" +
                "                            on corfor.cor = coi.cor\n" +
                "                           and corfor.codcli = forn.codcli\n" +
                "                           and corfor.codigo = mat.codigo\n" +
                "                         where coi.situacao <> 'B'\n" +
                "                        ---------------- SELECT ORDENS DE TECELAGEM --------------------\n" +
                "                        union all\n" +
                "                        select forn.codcli,\n" +
                "                               forn.cnpj,\n" +
                "                               forn.nome,\n" +
                "                               forn.dias,\n" +
                "                               (select min(mai.barra) from mat_iten_001 mai where mai.codigo = mat.codigo and mai.cor = coi.cor) min_forn,\n" +
                "                               case\n" +
                "                                 when sdcop.ultimo_email is not null then\n" +
                "                                  sdcop.ultimo_email\n" +
                "                                 else\n" +
                "                                  forn.email\n" +
                "                               end email,\n" +
                "                               cop.numero,\n" +
                "                               sdcoi.ped_fornecedor,\n" +
                "                               'TEC' tipo,\n" +
                "                               mat.codigo,\n" +
                "                               mat.descricao,\n" +
                "                               coi.cor,\n" +
                "                               'X',\n" +
                "                               0,\n" +
                "                               mat.grupo,\n" +
                "                               mat.sub_grupo,\n" +
                "                               mat.unidade,\n" +
                "                               coi.qtde,\n" +
                "                               coi.custo,\n" +
                "                               (coi.qtde * coi.custo) valor,\n" +
                "                               cop.deposito,\n" +
                "                               to_char(cop.dt_inicio, 'DD/MM/YYYY') dt_entrega,\n" +
                "                               to_char(cop.dt_inicio, 'DD/MM/YYYY') dt_fatura,\n" +
                "                               sdcoi.nf_fornecedor,\n" +
                "                               case\n" +
                "                                 when cop.dt_inicio is null then\n" +
                "                                  'S'\n" +
                "                                 else\n" +
                "                                  case\n" +
                "                                    when cop.dt_inicio <\n" +
                "                                         to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n" +
                "                                         sdcoi.nf_fornecedor is null then\n" +
                "                                     'R'\n" +
                "                                    else\n" +
                "                                     case\n" +
                "                                       when cop.dt_inicio >=\n" +
                "                                            to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') then\n" +
                "                                        'Y'\n" +
                "                                       else\n" +
                "                                        case\n" +
                "                                          when cop.dt_inicio <\n" +
                "                                               to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n" +
                "                                               sdcoi.nf_fornecedor is not null then\n" +
                "                                           'G'\n" +
                "                                        end\n" +
                "                                     end\n" +
                "                                  end\n" +
                "                               end status,\n" +
                "                               sdcop.obs_recebimento,\n" +
                "                               trim(sdcop.condicao) condicao,\n" +
                "                               null especifica,\n" +
                "                               case\n" +
                "                                 when nvl((select count(*)\n" +
                "                                            from sd_agenda_servico_001\n" +
                "                                           where tipo = 'OT1'\n" +
                "                                             and campo1 = cop.numero\n" +
                "                                             and campo2 = forn.codcli\n" +
                "                                             and dt_agenda >= sysdate),\n" +
                "                                          0) > 0 then\n" +
                "                                  'B'\n" +
                "                                 else\n" +
                "                                  case\n" +
                "                                    when nvl((select count(*)\n" +
                "                                               from sd_log_email_oc_001\n" +
                "                                              where codcli = forn.codcli\n" +
                "                                                and numero = cop.numero\n" +
                "                                                and codigo = coi.codigo\n" +
                "                                                and cor = coi.cor),\n" +
                "                                             0) > 0 then\n" +
                "                                     'E'\n" +
                "                                  end\n" +
                "                               end status_envio,\n" +
                "                               fis.descricao ncm,\n" +
                "                               nvl(corfor.cor1,'N/D') corf,\n" +
                "                               to_char(cop.dt_inicio, 'DD/MM/YYYY') dt_emissao,\n" +
                "                               sdcoi.periodo,\n" +
                "                               sdcoi.periodo_orig,\n" +
                "                               'T' origem\n" +
                "                          from tecel_001 cop\n" +
                "                         inner join sd_tecel_001 sdcop\n" +
                "                            on sdcop.numero = cop.numero\n" +
                "                         inner join tecel_iten_001 coi\n" +
                "                            on coi.numero = cop.numero\n" +
                "                         inner join sd_tecel_iten_001 sdcoi\n" +
                "                            on sdcoi.numero = coi.numero\n" +
                "                           and sdcoi.codigo = coi.codigo\n" +
                "                           and sdcoi.cor = coi.cor\n" +
                "                         inner join material_001 mat\n" +
                "                            on mat.codigo = coi.codigo\n" +
                "                         inner join tabfis_001 fis\n" +
                "                            on fis.codigo = mat.codfis\n" +
                "                         inner join entidade_001 forn\n" +
                "                            on forn.codcli = cop.malharia\n" +
                "                          left join cadcorti_001 corfor\n" +
                "                            on corfor.cor = coi.cor\n" +
                "                           and corfor.codcli = forn.codcli\n" +
                "                           and corfor.codigo = mat.codigo\n" +
                "                         where coi.baixada <> 'S') ordens_compra\n" +
                "                 where 1 = 1\n"
                + "   " + filters;
        super.initPreparedStatement(query);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new GestaoCompra00X(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getString(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13),
                    resultSetSql.getInt(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16),
                    resultSetSql.getString(17),
                    resultSetSql.getDouble(18),
                    resultSetSql.getDouble(19),
                    resultSetSql.getDouble(20),
                    resultSetSql.getString(21),
                    resultSetSql.getString(22),
                    resultSetSql.getString(23),
                    resultSetSql.getString(24),
                    resultSetSql.getString(25),
                    resultSetSql.getString(26),
                    resultSetSql.getString(27),
                    resultSetSql.getString(28),
                    resultSetSql.getString(29),
                    resultSetSql.getString(30),
                    resultSetSql.getString(31),
                    resultSetSql.getString(32),
                    resultSetSql.getString(33),
                    resultSetSql.getString(34),
                    resultSetSql.getString(35)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void updateObsRecebimentoCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        super.initPreparedStatement("update sd_compra_001 set obs_recebimento = to_clob(?) where numero = ?");
        super.preparedStatement.setString(1, ordemCompraMaterial.getObsRecebimento());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateObsRecebimentoTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        super.initPreparedStatement("update sd_tecel_001 set obs_recebimento = to_clob(?) where numero = ?");
        super.preparedStatement.setString(1, ordemCompraMaterial.getObsRecebimento());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateDatasCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = "update co_iten_001\n"
                + "   set dt_fatura  = to_date(?, 'DD/MM/YYYY'),\n"
                + "       dt_entrega = to_date(?, 'DD/MM/YYYY')\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?\n"
                + "   and tam = ?\n"
                + "   and ordem = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getDtFaturaAsString());
        super.preparedStatement.setString(2, ordemCompraMaterial.getDtEntregaAsString());
        super.preparedStatement.setString(3, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(5, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(6, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(7, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateDatasTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = "update tecel_iten_001\n"
                + "   set dt_inicio  = to_date(?, 'DD/MM/YYYY')\n"
                + " where numero = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getDtFaturaAsString());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateCodforCompraTi(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update compra_001\n"
                + "     set codfor  = ?\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getCodcli());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateNfCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_co_iten_001\n"
                + "     set nf_fornecedor  = ?\n"
                + "   where numero = ?\n"
                + "     and codigo = ?\n"
                + "     and cor = ?\n"
                + "     and tam = ?\n"
                + "     and ordem = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getFatura());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(5, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(6, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateNfCompraTi(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update co_iten_001 \n"
                + "     set especifica = 'NF '||?||' - '||especifica \n"
                + "   where numero = ?\n"
                + "     and codigo = ?\n"
                + "     and cor = ?\n"
                + "     and tam = ?\n"
                + "     and ordem = ?;"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getFatura());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(5, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(6, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updatePedidoCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_co_iten_001\n"
                + "     set ped_fornecedor  = ?\n"
                + "   where numero = ?\n"
                + "     and codigo = ?\n"
                + "     and cor = ?\n"
                + "     and tam = ?\n"
                + "     and ordem = ?;"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getPedFornecedor());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(5, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(6, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updatePedidoCompraTi(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update compra_001\n"
                + "     set obs = to_clob('PEDIDO ITEM '||?||' '||?||'\n'||to_char(obs))\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setInt(1, ordemCompraMaterial.getOrdem());
        super.preparedStatement.setString(2, ordemCompraMaterial.getPedFornecedor());
        super.preparedStatement.setString(3, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateNfTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_tecel_iten_001\n"
                + "     set nf_fornecedor  = ?\n"
                + "   where numero = ?\n"
                + "     and codigo = ?\n"
                + "     and cor = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getFatura());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateNfTecelagemTi(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update tecel_001\n"
                + "     set observacao = to_clob('NF '||?||'\n'||to_char(observacao))\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getFatura());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updatePedidoTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_tecel_iten_001\n"
                + "     set ped_fornecedor  = ?\n"
                + "   where numero = ?\n"
                + "     and codigo = ?\n"
                + "     and cor = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getPedFornecedor());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updatePedidoTecelagemTi(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update tecel_001\n"
                + "     set observacao = to_clob('PEDIDO '||?||'\n'||to_char(observacao))\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getPedFornecedor());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateEmailCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_compra_001\n"
                + "     set ultimo_email  = ?\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getEmail());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateEmailTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "begin\n"
                + "  update sd_tecel_001\n"
                + "     set ultimo_email  = ?\n"
                + "   where numero = ?;\n"
                + "end;";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getEmail());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateCustoItemCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update co_iten_001\n"
                + "   set preco = round(?,5),\n"
                + "       valor = round(?,3)\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?\n"
                + "   and tam = ?\n"
                + "   and ordem = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setDouble(1, ordemCompraMaterial.getCusto());
        super.preparedStatement.setDouble(2, ordemCompraMaterial.getValor());
        super.preparedStatement.setString(3, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(5, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(6, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(7, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateCustoItemTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update tecel_iten_001\n"
                + "   set custo = round(?,3)\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?";
        super.initPreparedStatement(query);
        super.preparedStatement.setDouble(1, ordemCompraMaterial.getCusto());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateQtdeItemCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update co_iten_001\n"
                + "   set qtde = ?,\n"
                + "       valor = round(?,3)\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?\n"
                + "   and tam = ?\n"
                + "   and ordem = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setDouble(1, ordemCompraMaterial.getQtde());
        super.preparedStatement.setDouble(2, ordemCompraMaterial.getValor());
        super.preparedStatement.setString(3, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(5, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(6, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(7, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateQtdeItemTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update tecel_iten_001\n"
                + "   set qtde = ?\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?";
        super.initPreparedStatement(query);
        super.preparedStatement.setDouble(1, ordemCompraMaterial.getQtde());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateCondicaoCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update compra_001\n"
                + "   set condicao = ?\n"
                + " where numero = ?";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getCondicao());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateCondicaoTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update sd_tecel_001\n"
                + "   set condicao = ?\n"
                + " where numero = ?";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getCondicao());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateDepositoItemCompra(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update co_iten_001\n"
                + "   set deposito = ?\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?\n"
                + "   and tam = ?\n"
                + "   and ordem = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getDeposito());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        super.preparedStatement.setString(3, ordemCompraMaterial.getCodigo());
        super.preparedStatement.setString(4, ordemCompraMaterial.getCor());
        super.preparedStatement.setString(5, ordemCompraMaterial.getTam());
        super.preparedStatement.setInt(6, ordemCompraMaterial.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateDepositoTecelagem(GestaoCompra00X ordemCompraMaterial) throws SQLException {

        String query = ""
                + "update tecel_001\n"
                + "   set deposito = ?\n"
                + " where numero = ?\n";
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, ordemCompraMaterial.getDeposito());
        super.preparedStatement.setString(2, ordemCompraMaterial.getNumero());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void updateNumeroLote(GestaoCompra00X selectedRow) throws SQLException {
        String query = ""
                + "update " + (selectedRow.getOrigem().equals("C") ? "sd_co_iten_001" : "sd_tecel_iten_001") + "\n"
                + "   set periodo = ?\n"
                + " where numero = ?\n" +
                "and codigo = ?\n" +
                "and cor = ?\n" +
                (selectedRow.getOrigem().equals("C") ? "and ordem = ?" : "");
        super.initPreparedStatement(query);
        super.preparedStatement.setString(1, selectedRow.getPeriodo());
        super.preparedStatement.setString(2, selectedRow.getNumero());
        super.preparedStatement.setString(3, selectedRow.getCodigo());
        super.preparedStatement.setString(4, selectedRow.getCor());
        if (selectedRow.getOrigem().equals("C")) super.preparedStatement.setInt(5, selectedRow.getOrdem());
        int affectedRows = super.preparedStatement.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
}
