package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.HistoricoRevisaoMeta;
import sysdeliz2.models.LiberacaoPcp;
import sysdeliz2.models.properties.GestaoDeLote;
import sysdeliz2.models.properties.GestaoDeProduto;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface GestaoProducaoDAO {

    public ObservableList<Map> getProducaoByDataAndColecaoAndPeriodo(String pDataInicio, String pDataFim, List<String> pColecoes, List<String> pPeriodos, List<String> pReferencias) throws SQLException;

    public Map getProducaoByDataAndReferenciaAndPeriodo(String pDataInicio, String pDataFim, String pReferencia, List<String> pPeriodos) throws SQLException;

    public ObservableList<GestaoDeProduto> getAnaliseProduto(String pCodigo, String pDataInicio, String pDataFim, List<String> pPeriodos, List<String> pColecoes) throws SQLException;

    public ObservableList<GestaoDeLote> getLotesProduto(String pCodigo) throws SQLException;

    public void callFunctionUpdateOf(String pNumeroOf, String pCodigo, String pGrade, String pSetor, String pUsuario) throws SQLException;

    public String callFunctionCreateOf(String pLote, String pCodigo, String pGrade, String pUsuario) throws SQLException;

    public String callFunctionDeleteOf(String pLote, String pUsuario) throws SQLException;

    public void saveLiberacaoPcp(String codigo, String colecao, String status, String observacao, String usuario) throws SQLException;

    public void saveHistoricoRev(String codigo, String colecao, Integer meta_ant, Integer meta_nova, String usuario) throws SQLException;

    public ObservableList<LiberacaoPcp> getHistLiberacaoCodigo(String codigo, String colecao) throws SQLException;

    public ObservableList<HistoricoRevisaoMeta> getHistRevisaoMeta(String codigo, String colecao) throws SQLException;

    public void saveHistoricoRevMeta(String codigo, String colecao, String obs, String usuario, String revisao) throws SQLException;

    public List<String[]> getSetoresProducao() throws SQLException;

    public ObservableList<Map> getProducaoSetores(List<String> pColecoes, List<String[]> setores) throws SQLException;

    public void zerarAnalista(String colecao, String usuario) throws SQLException;

    public void updateLoteOf(String numeroOf, String numeroLote, String dtInicio, String dtFim) throws SQLException;

    public void movimentaSetor99(GestaoDeLote gestaoOf, String codigoRef) throws SQLException;

    public void saveObservacaoPcp(String codigo, String observacao) throws SQLException;
}
