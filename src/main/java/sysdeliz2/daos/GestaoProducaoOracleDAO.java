package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.HistoricoRevisaoMeta;
import sysdeliz2.models.LiberacaoPcp;
import sysdeliz2.models.SdProduto001;
import sysdeliz2.models.properties.GestaoDeLote;
import sysdeliz2.models.properties.GestaoDeLoteGrade;
import sysdeliz2.models.properties.GestaoDeProduto;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestaoProducaoOracleDAO implements GestaoProducaoDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private SimpleDateFormat formatoDb = new SimpleDateFormat("yyyy-MM-dd");

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (statement != null) {
            statement.close();
        }
        connection.commit();
        connection.close();
    }

    private ObservableList<GestaoDeLoteGrade> getGradeLote(String pCodigo, String pNumero, String pSetor) throws SQLException {
        ObservableList<GestaoDeLoteGrade> producao = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT\n"
                + "	OF1.PERIODO,\n"
                + "	OFI.COR, \n"
                + "	CCOR.DESCRICAO, \n"
                + "	OFI.TAM, \n"
                + "     SUM(NVL(FAC.QT_ORIG,0) - (NVL(FAC.QUANT,0) + NVL(FAC.QUANT_2,0) + NVL(FAC.QUANT_I,0) + NVL(FAC.QUANT_F,0))) PLANEJ_GRADE\n"
                + "FROM\n"
                + "	OF1_001 OF1\n"
                + "	INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO\n"
                + "	INNER JOIN OF_ITEN_001 OFI ON OFI.NUMERO = OF1.NUMERO\n"
                + "	INNER JOIN CADCOR_001 CCOR ON CCOR.COR = OFI.COR\n"
                + "	INNER JOIN FAIXA_001 FAI ON FAI.CODIGO = PRD.FAIXA\n"
                + "	INNER JOIN FAIXA_ITEN_001 FII ON FII.FAIXA = FAI.CODIGO AND FII.TAMANHO = OFI.TAM\n"
                + "	LEFT JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.CODIGO = OF1.CODIGO AND FAC.COR = OFI.COR AND FAC.TAM = OFI.TAM AND FAC.OP = '" + pSetor + "'\n"
                + "									--AND (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_I + FAC.QUANT_F) < FAC.QT_ORIG\n"
                + "WHERE\n"
                + "	OF1.CODIGO = '" + pCodigo + "' AND OF1.NUMERO = '" + pNumero + "'\n"
                + "	AND OF1.PERIODO NOT IN ('M','S') /*CONSTANTE*/\n"
                + "GROUP BY \n"
                + "	OF1.PERIODO, OFI.COR, CCOR.DESCRICAO, OFI.TAM, FII.POSICAO\n"
                + "ORDER BY\n"
                + "	OF1.PERIODO, OFI.COR, FII.POSICAO");

        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            producao.add(new GestaoDeLoteGrade(resultSetSql.getString("PERIODO"), resultSetSql.getString("COR"),
                    resultSetSql.getString("DESCRICAO"), resultSetSql.getString("TAM"), resultSetSql.getInt("PLANEJ_GRADE")));
        }
        resultSetSql.close();

        return producao;
    }

    @Override
    public ObservableList<Map> getProducaoByDataAndColecaoAndPeriodo(String pDataInicio, String pDataFim, List<String> pColecoes, List<String> pPeriodos, List<String> pReferencias) throws SQLException {
        ObservableList<Map> producao = FXCollections.observableArrayList();

        String inColecoes = "('x'";
        for (String colecao : pColecoes) {
            inColecoes = inColecoes.concat(",'" + colecao + "'");
        }
        inColecoes = inColecoes.concat(")");

        String inPeriodos = "('x'";
        for (String periodo : pPeriodos) {
            inPeriodos = inPeriodos.concat(",'" + periodo + "'");
        }
        inPeriodos = inPeriodos.concat(")");

        String inCodigos = "('x'";
        for (String codigo : pReferencias) {
            inCodigos = inCodigos.concat(",'" + codigo + "'");
        }
        inCodigos = inCodigos.concat(")");

        StringBuilder query = new StringBuilder(""
                + "WITH SALDOS AS (/* SALDOS */\n"
                + "SELECT PRD.CODIGO, SUM(NVL(PRODUC.ESTQ,0)) EST, SUM(NVL(PREF.PLANEJADO,0)) PLANEJ, SUM(NVL(PED.PEND,0)) PEND, SUM(NVL(PED.FAT,0)) FAT, SUM(NVL(PREF.PROD_PEND,0)) PROD_PEND, \n"
                + "    SUM(NVL(PREF.PROD_REAL,0)) PROD_BOAS, SUM(NVL(PRODUC.EM_EXP,0)) EM_EXP, SUM(NVL(PRODUC.MOSTR,0)) MOST, SUM(NVL(PREF.PROD_SEG,0)) PROD_SEG,\n"
                + "    (SELECT SUM(CON.QTDE) FROM VPEDIDO_CONCENTRADOS_001 CON JOIN pedido_001 ped1 on ped1.numero = con.numero \n"
                + "            WHERE CON.CODIGO = PRD.CODIGO AND PED1.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy') AND PED1.PERIODO BETWEEN '0000' AND '9999') CONC,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_EST_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_EST_NEG,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PROD_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PROD_NEG,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PLAN_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PLAN_NEG\n"
                + "      FROM PRODUTO_001 PRD \n"
                + "      INNER JOIN VPRODUTO_CORTAM_001 CORTAM ON CORTAM.CODIGO = PRD.CODIGO\n"
                + "      LEFT JOIN (SELECT PEDI.CODIGO, PEDI.COR, PEDI.TAM, SUM(PEDI.QTDE) PEND, SUM(PEDI.QTDE_F) FAT\n"
                + "          FROM PEDIDO_001 PED\n"
                + "          INNER JOIN PED_ITEN_001 PEDI ON PEDI.NUMERO = PED.NUMERO\n"
                + "          WHERE PED.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy')\n"
                + "          AND PED.PERIODO BETWEEN '0000' AND '9999'\n"
                + "          GROUP BY PEDI.CODIGO, PEDI.COR, PEDI.TAM) PED ON PED.CODIGO = CORTAM.CODIGO AND PED.COR = CORTAM.COR AND PED.TAM = CORTAM.TAM\n"
                + "      LEFT JOIN (SELECT PCT.CODIGO,PCT.COR,PCT.TAM,\n"
                + "               NVL(DEP_1.QUANTIDADE,0) + NVL(DEP_5.QUANTIDADE,0) ESTQ, NVL(PAI_MOST.QUANTIDADE,0) MOSTR,\n"
                + "               (SELECT NVL(SUM(EXPD.QTDE), 0) FROM PEDIDO3_001 EXPD WHERE EXPD.CODIGO = PCT.CODIGO AND EXPD.COR = PCT.COR AND EXPD.TAM = PCT.TAM AND EXPD.QTDE > 0 AND EXPD.DEPOSITO IN ('0001','0005')) EM_EXP\n"
                + "          FROM PRODUTO_001 PRD\n"
                + "          JOIN VPRODUTO_CORTAM_001 PCT ON PCT.CODIGO = PRD.CODIGO\n"
                + "          LEFT JOIN PA_ITEN_001 DEP_1 ON DEP_1.CODIGO = PCT.CODIGO AND DEP_1.COR = PCT.COR AND DEP_1.TAM = PCT.TAM AND DEP_1.DEPOSITO IN ('0001')\n"
                + "          LEFT JOIN PA_ITEN_001 DEP_5 ON DEP_5.CODIGO = PCT.CODIGO AND DEP_5.COR = PCT.COR AND DEP_5.TAM = PCT.TAM AND DEP_5.DEPOSITO IN ('0005')\n"
                + "          LEFT JOIN PA_ITEN_001 PAI_MOST ON PAI_MOST.CODIGO = PCT.CODIGO AND PAI_MOST.COR = PCT.COR  AND PAI_MOST.TAM = PCT.TAM AND PAI_MOST.DEPOSITO = '0011'\n"
                + "         WHERE PRD.COLECAO in " + inColecoes + ") PRODUC ON PRODUC.CODIGO = CORTAM.CODIGO AND PRODUC.COR = CORTAM.COR AND PRODUC.TAM = CORTAM.TAM\n");
        if (Boolean.logicalOr(inColecoes.contains("17CO"), inColecoes.contains("GR"))) {
            query.append("      LEFT JOIN (SELECT PROD_PERIODO.COLECAO, PROD_PERIODO.CODIGO, PROD_PERIODO.COR, PROD_PERIODO.TAM, \n"
                    + "                 	   SUM(PROD_PERIODO.PROD_PEND) PROD_PEND, SUM(PROD_PERIODO.PLANEJADO) PLANEJADO, \n"
                    + "                 	   SUM(PROD_PERIODO.PROD_REAL) PROD_REAL, SUM(PROD_PERIODO.PROD_SEG) PROD_SEG \n"
                    + "                     FROM V_SD_PRODUCAO_REFER_PERIODO PROD_PERIODO \n"
                    + "                     WHERE PERIODO IN " + inPeriodos + "\n"
                    + "                     GROUP BY PROD_PERIODO.COLECAO, PROD_PERIODO.CODIGO, PROD_PERIODO.COR, PROD_PERIODO.TAM) PREF ON PREF.CODIGO = CORTAM.CODIGO AND PREF.COR = CORTAM.COR AND PREF.TAM = CORTAM.TAM AND PREF.COLECAO = PRD.COLECAO\n");
        } else {
            query.append("      LEFT JOIN V_SD_PRODUCAO_REFER PREF ON PREF.CODIGO = CORTAM.CODIGO AND PREF.COR = CORTAM.COR AND PREF.TAM = CORTAM.TAM AND PREF.COLECAO = PRD.COLECAO\n");
        }
        query.append("      WHERE PRD.ATIVO = 'S' AND PRD.COLECAO in " + inColecoes + "\n"
                + "  GROUP BY PRD.CODIGO),\n"
                + "SETORES AS (/* SELECT SETORES COLUNAS */\n"
                + "SELECT CODIGO, \n"
                + "       SUM(CASE WHEN SETOR = '111' THEN SALDO ELSE 0 END) \"111\",\n"
                + "       SUM(CASE WHEN SETOR = '110' THEN SALDO ELSE 0 END) \"110\",\n"
                + "       SUM(CASE WHEN SETOR = '109' THEN SALDO ELSE 0 END) \"109\",\n"
                + "       SUM(CASE WHEN SETOR = '209' THEN SALDO ELSE 0 END) \"209\",\n"
                + "       SUM(CASE WHEN SETOR = '201' THEN SALDO ELSE 0 END) \"201\",\n"
                + "       SUM(CASE WHEN SETOR = '101' THEN SALDO ELSE 0 END) \"101\",\n"
                + "       SUM(CASE WHEN SETOR = '99' THEN SALDO ELSE 0 END) \"99\",\n"
                + "       SUM(CASE WHEN SETOR = '100' THEN SALDO ELSE 0 END) \"100\",\n"
                + "       SUM(SALDO) \"TOT\"\n"
                + "FROM (/* PENDENTES NOS SETORES DA PARTE DO PCP [V2] */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S' \n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                                   AND FAC.PARTE IN CASE WHEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) IS NOT NULL \n"
                + "                                                      THEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) \n"
                + "                                                      ELSE \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO GROUP BY PARTE)\n"
                + "                                                      END\n"
                + "      WHERE PRD.COLECAO in " + inColecoes + "\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S')\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, FAC.OP, FAC.PARTE\n"
                + "      UNION\n"
                + "      /* PENDENTES NOS SETORES DA PARTE DIFERENTE DO PCP */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "      WHERE PRD.COLECAO in " + inColecoes + "\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S')\n"
                + "      AND FAC.PARTE NOT IN (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE)\n"
                + "      AND nvl((SELECT SUM(FAC2.QT_ORIG) - SUM(FAC2.QUANT + FAC2.QUANT_2 + FAC2.QUANT_F + FAC2.QUANT_I)\n"
                + "                  FROM FACCAO_001 FAC2 WHERE NUMERO = OF1.NUMERO\n"
                + "                       AND PARTE = (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = FAC2.NUMERO \n"
                + "                                      AND OP IN ('100') GROUP BY PARTE)),0) <= 0\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, FAC.OP, FAC.PARTE)\n"
                + "GROUP BY CODIGO),\n"
                + "PERIODOS AS (/* PENDENTES NO PCP POR PER�ODO */\n"
                + "SELECT CODIGO,\n");
        for (String periodo : pPeriodos) {
            query.append("       SUM(CASE WHEN PERIODO = '" + periodo + "' AND OP = '99' THEN SALDO ELSE 0 END) \"" + periodo + "-C\", \n");
            query.append("       SUM(CASE WHEN PERIODO = '" + periodo + "' AND OP = '100' THEN SALDO ELSE 0 END) \"" + periodo + "-P\", \n");
        }
        query.append("       SUM(SALDO) TOTAL\n"
                + "FROM (SELECT OF1.NUMERO, OF1.PERIODO, PRD.CODIGO, FAC.OP, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "FROM OF1_001 OF1\n"
                + "INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                             AND FAC.OP IN ('100','99')\n"
                + "WHERE OF1.PERIODO IN " + inPeriodos + "\n"
                + "GROUP BY OF1.NUMERO, OF1.PERIODO, PRD.CODIGO, FAC.OP, FAC.PARTE)\n"
                + "GROUP BY CODIGO)\n"
                + "    \n"
                + "SELECT DISTINCT\n"
                + "  NVL(PART.PAIS,'-') \"PAÍS\", \n"
                + "  NVL(COL.CODIGO,'-') \"COLEÇÃO\", \n"
                + "  NVL(REPLACE(COL.DESCRICAO,'COLEÇÃO ',''),'-') \"DESC. COLEÇÃO\", \n"
                + "  NVL(PROD.PAG_CATALOGO,'-') \"PÁG\", \n"
                + "  NVL(PROD.CODIGO,'-') \"REFER.\", \n"
                + "  NVL(PROD.DESCRICAO,'-') || CASE WHEN SDPROD.OBSERVACAO_PCP IS NOT NULL THEN ' *' END \"DESCRIÇÃO\",\n"
                + "  nvl(to_char(prod.dt_entrega,'DD/MM/YYYY'), sysdate2) \"DT. ENTREGA\",\n"
                + "/*AQUI*/  NVL(PCP_LIB.STATUS,'LIVRE') \"VENDA\",\n"
                + "  NVL(REPLACE(MAR.DESCRICAO, 'MOOVE ',''),'-') MARCA,\n"
                + "  NVL(TRIM(REPLACE(REPLACE(REGEXP_REPLACE(LIN.DESCRICAO,'*(MASC)*(FEM)*',''), 'MOOVE ',''), MAR.DESCRICAO,'')),'-') LINHA, \n"
                + "  NVL(ETQ.DESCRICAO,'-') \"CLASSE/MODEL.\", \n"
                + "  NVL(FAM.DESCRICAO,'-') \"FAMÍLIA\",\n"
                + "/*AQUI*/  NVL(ESTP.ESTAMPA,'-') \"CJ-F\",\n"
                + "  NVL(CASE WHEN COMB.MALHA IS NULL THEN '0' ELSE COMB.MALHA END,'-') \"CÓD TEC.\", \n"
                + "  NVL(CASE WHEN MATT.DESCRICAO IS NULL THEN 'SEM CADASTRO' ELSE MATT.DESCRICAO END,'-') \"TEC. PRINCIPAL\", \n"
                + "  NVL(CASE WHEN FT.CONSUMO IS NULL THEN 0 ELSE FT.CONSUMO END,0) CONSUMO,\n"
                + "  NVL(TO_CHAR(PRE.PRECO_00),'-') \"PREÇO R$\", \n"
                + "  NVL((SLDS.PEND + SLDS.FAT),0) \"VENDA TOTAL\", \n"
                + "  NVL(SLDS.PEND,0) \"VENDA PENDENTE\", \n"
                + "  NVL(SLDS.CONC,0) \"CONCENTRADO\",\n"
                + "  NVL(REV.META_NOVA||'-'||REV.OBS,'0') ANALISTA,\n"
                + "  NVL((NVL((SLDS.PROD_BOAS + SLDS.PROD_PEND + SLDS.MOST),0) + SLDS.PLANEJ),0) META,\n"
                + "/*AQUI*/  0 \"% IND. PROJ.\",\n"
                + "/*AQUI*/  0 \"PROJ.\",\n"
                + "/*AQUI*/  0 \"PR-META\",\n"
                + "  NVL((SLDS.PROD_BOAS + SLDS.PROD_PEND),0) \"PRODUÇÃO\",\n"
                + "/*DBG  NVL(SLDS.PROD_BOAS,0) \"PRODUZIDO\", */\n"
                + "/*DBG  NVL(SLDS.PROD_SEG,0) \"PRODUZIDO 2a\",*/\n"
                + "  NVL(SLDS.EST,0) + NVL(SLDS.EM_EXP,0) ESTOQUE, \n"
                + "  NVL(SLDS.MOST,0) MOSTR,\n"
                + "  NVL(SLDS.SLD_EST_POS,0) \"SLD ESTQ POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_EST_NEG,0) \"SLD ESTQ NEGAT.(-)\",\n"
                + "  NVL(SLDS.SLD_PROD_POS,0) \"SLD PROD POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_PROD_NEG,0) \"SLD PROD NEGAT.(-)\",\n"
                + "  NVL(SLDS.SLD_PLAN_POS,0) \"SLD PLAN POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_PLAN_NEG,0) \"SLD PLAN NEGAT.(-)\",\n"
                + "  NVL(CASE WHEN STRS.\"111\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"111\") END,'-') \"ACAB.\",\n"
                + "  NVL(CASE WHEN STRS.\"110\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"110\") END,'-') \"LAV.\",\n"
                + "  NVL(CASE WHEN STRS.\"209\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"209\") END,'-') \"COSTURA PY\",\n"
                + "  NVL(CASE WHEN STRS.\"109\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"109\") END,'-') \"COSTURA\",\n"
                + "  NVL(CASE WHEN STRS.\"201\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"201\") END,'-') \"ENCAIXE PY\",\n"
                + "  NVL(CASE WHEN STRS.\"101\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"101\") END,'-') \"ENCAIXE BR\",\n"
                + "  NVL(CASE WHEN STRS.\"99\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"99\") END,'-') \"CONFIRMAÇÃO\",\n"
                + "  NVL(CASE WHEN STRS.\"100\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"100\") END,'-') \"PCP\",\n"
                + "  NVL(CASE WHEN STRS.\"TOT\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"TOT\") END,'-') \"TOTAL PROGRAMADO\",\n");
        for (String periodo : pPeriodos) {
            query.append("  NVL(CASE WHEN PERI.\"" + periodo + "-C\" = 0 THEN '-' ELSE TO_CHAR(PERI.\"" + periodo + "-C\") END, '-') \"LT " + periodo + "-C\",\n");
            query.append("  NVL(CASE WHEN PERI.\"" + periodo + "-P\" = 0 THEN '-' ELSE TO_CHAR(PERI.\"" + periodo + "-P\") END, '-') \"LT " + periodo + "-P\",\n");
        }
        query.append("  CASE WHEN NVL(PERI.TOTAL,0) = 0 THEN '-' ELSE TO_CHAR(PERI.TOTAL) END TOTAL,\n"
                + "LIN_INDICE.DESCRICAO GRP_INDICE\n"
                + "FROM PRODUTO_001 PROD\n"
                + "JOIN SD_PRODUTO_001 SDPROD ON SDPROD.CODIGO = PROD.CODIGO\n"
                + "JOIN COLECAO_001 COL ON COL.CODIGO = PROD.COLECAO\n"
                + "JOIN MARCA_001 MAR ON MAR.CODIGO = PROD.MARCA\n"
                + "JOIN TABLIN_001 LIN ON LIN.CODIGO = PROD.LINHA\n"
                + "JOIN ETQ_PROD_001 ETQ ON ETQ.CODIGO = PROD.ETIQUETA\n"
                + "JOIN FAMILIA_001 FAM ON FAM.CODIGO = PROD.FAMILIA\n"
                + "LEFT JOIN TABLIN_001 LIN_INDICE ON LIN_INDICE.CODIGO = LIN.GRP_INDICE\n"
                + "LEFT JOIN EST_PROD_001 ESTP ON ESTP.PRODUTO = PROD.CODIGO\n"
                + "LEFT JOIN (SELECT REV2.CODIGO, REV2.COLECAO, REV2.META_NOVA, MAX(NVL(OBS.REVISAO,'0')) OBS\n"
                + "  FROM SD_HISTORICO_REVPLAN_001 REV2 \n"
                + "  LEFT JOIN SD_OBS_REVPLAN_001 OBS ON OBS.CODIGO = REV2.CODIGO AND OBS.COLECAO = REV2.COLECAO\n"
                + "  WHERE REV2.DT_REVISAO >= (SELECT MAX(REV3.DT_REVISAO) FROM SD_HISTORICO_REVPLAN_001 REV3 \n"
                + "    WHERE REV3.CODIGO = REV2.CODIGO AND REV3.COLECAO = REV2.COLECAO)\n"
                + "  GROUP BY REV2.CODIGO, REV2.COLECAO, REV2.META_NOVA) REV ON REV.CODIGO = PROD.CODIGO AND REV.COLECAO = PROD.COLECAO\n"
                + "LEFT JOIN (SELECT LIB_PCP.CODIGO, LIB_PCP.COLECAO, '*'||LIB_PCP.STATUS STATUS\n"
                + "  FROM SD_LIBERACAO_PCP_001 LIB_PCP \n"
                + "  WHERE LIB_PCP.DT_MANUTENCAO >= (SELECT MAX(LIB_PCP2.DT_MANUTENCAO) FROM SD_LIBERACAO_PCP_001 LIB_PCP2 \n"
                + "    WHERE LIB_PCP2.CODIGO = LIB_PCP.CODIGO AND LIB_PCP2.COLECAO = LIB_PCP.COLECAO)) PCP_LIB ON PCP_LIB.CODIGO = PROD.CODIGO AND PCP_LIB.COLECAO = PROD.COLECAO\n"
                + "LEFT JOIN TABPRECO_001 PRE ON PRE.CODIGO = PROD.CODIGO AND PRE.REGIAO IN ('F','D','P','FP','DP')\n"
                + "LEFT JOIN COMBINACAO_001 COMB ON COMB.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN MATERIAL_001 MATT ON MATT.CODIGO = COMB.MALHA\n"
                + "LEFT JOIN v_sd_cons_ins_cor_most FT ON FT.INSUMO = COMB.MALHA AND FT.CODIGO = COMB.CODIGO\n"
                + "JOIN (SELECT FLX.CODIGO, MAX(PART.PAIS) PAIS\n"
                + "    FROM PRO_FLUXO_001 FLX\n"
                + "    JOIN TIPO_APL_001 PART ON PART.CODIGO = FLX.PARTE\n"
                + "    WHERE FLX.PADRAO = 'S'\n"
                + "    GROUP BY FLX.CODIGO) PART ON PART.CODIGO = PROD.CODIGO  \n"
                + "LEFT JOIN SALDOS SLDS ON SLDS.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN SETORES STRS ON STRS.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN PERIODOS PERI ON PERI.CODIGO = PROD.CODIGO\n"
                + "WHERE PROD.ATIVO = 'S'\n"
                + "AND PROD.STATUS = 'LIBERADO P/ PRODUÇÃO'\n"
                + "AND PROD.COLECAO in " + inColecoes + "\n");
        if (!pReferencias.get(0).isEmpty()) {
            query.append("AND PROD.CODIGO in " + inCodigos + "\n");
        }

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        ResultSetMetaData rsMetaData = resultSetSql.getMetaData();
        int qtdeColumns = rsMetaData.getColumnCount();
        Map<String, String> dataHeaderColumn = new HashMap<>();
        for (int i = 1; i <= qtdeColumns; i++) {
            dataHeaderColumn.put("clm" + i, rsMetaData.getColumnName(i));
        }
        producao.add(dataHeaderColumn);

        Map<String, String> dataIndice = new HashMap<>();
        while (resultSetSql.next()) {
            Map<String, String> dataRow = new HashMap<>();
            for (int i = 1; i <= qtdeColumns; i++) {
                dataRow.put("clm" + i, resultSetSql.getString(i));
            }
            producao.add(dataRow);
        }

        closeConnection();
        resultSetSql.close();

        return producao;
    }

    @Override
    public Map getProducaoByDataAndReferenciaAndPeriodo(String pDataInicio, String pDataFim, String pReferencia, List<String> pPeriodos) throws SQLException {
        Map<String, String> producao = new HashMap<>();

        String inPeriodos = "('x'";
        for (String periodo : pPeriodos) {
            inPeriodos = inPeriodos.concat(",'" + periodo + "'");
        }
        inPeriodos = inPeriodos.concat(")");

        StringBuilder query = new StringBuilder(
                ""
                + "WITH SALDOS AS (/* SALDOS */\n"
                + "SELECT PRD.CODIGO, SUM(NVL(PRODUC.ESTQ,0)) EST, SUM(NVL(PREF.PLANEJADO,0)) PLANEJ, SUM(NVL(PED.PEND,0)) PEND, SUM(NVL(PED.FAT,0)) FAT, SUM(NVL(PREF.PROD_PEND,0)) PROD_PEND, \n"
                + "    SUM(NVL(PREF.PROD_REAL,0)) PROD_BOAS, SUM(NVL(PRODUC.EM_EXP,0)) EM_EXP, SUM(NVL(PRODUC.MOSTR,0)) MOST, SUM(NVL(PREF.PROD_SEG,0)) PROD_SEG,\n"
                + "    (SELECT SUM(CON.QTDE) FROM VPEDIDO_CONCENTRADOS_001 CON JOIN pedido_001 ped1 on ped1.numero = con.numero \n"
                + "            WHERE CON.CODIGO = PRD.CODIGO AND PED1.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy') AND PED1.PERIODO BETWEEN '0000' AND '9999') CONC,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_EST_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_EST_NEG,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PROD_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PROD_NEG,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) >= 0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PLAN_POS,\n"
                + "    SUM(CASE WHEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) <  0 THEN ((NVL(PRODUC.ESTQ,0))+(NVL(PRODUC.EM_EXP,0))+(NVL(PRODUC.MOSTR,0))+(NVL(PREF.PROD_PEND,0))+ (NVL(PREF.PLANEJADO,0))) - (NVL(PED.PEND,0)) ELSE 0 END) SLD_PLAN_NEG\n"
                + "      FROM PRODUTO_001 PRD \n"
                + "      INNER JOIN VPRODUTO_CORTAM_001 CORTAM ON CORTAM.CODIGO = PRD.CODIGO\n"
                + "      LEFT JOIN (SELECT PEDI.CODIGO, PEDI.COR, PEDI.TAM, SUM(PEDI.QTDE) PEND, SUM(PEDI.QTDE_F) FAT\n"
                + "          FROM PEDIDO_001 PED\n"
                + "          INNER JOIN PED_ITEN_001 PEDI ON PEDI.NUMERO = PED.NUMERO\n"
                + "          WHERE PED.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy')\n"
                + "          AND PED.PERIODO BETWEEN '0000' AND '9999'\n"
                + "          GROUP BY PEDI.CODIGO, PEDI.COR, PEDI.TAM) PED ON PED.CODIGO = CORTAM.CODIGO AND PED.COR = CORTAM.COR AND PED.TAM = CORTAM.TAM\n"
                + "      LEFT JOIN (SELECT PCT.CODIGO,PCT.COR,PCT.TAM,\n"
                + "               NVL(DEP_1.QUANTIDADE,0) + NVL(DEP_5.QUANTIDADE,0) ESTQ, NVL(PAI_MOST.QUANTIDADE,0) MOSTR,\n"
                + "               (SELECT NVL(SUM(EXPD.QTDE), 0) FROM PEDIDO3_001 EXPD WHERE EXPD.CODIGO = PCT.CODIGO AND EXPD.COR = PCT.COR AND EXPD.TAM = PCT.TAM AND EXPD.QTDE > 0 and expd.deposito in ('0001','0005')) EM_EXP\n"
                + "          FROM PRODUTO_001 PRD\n"
                + "          JOIN VPRODUTO_CORTAM_001 PCT ON PCT.CODIGO = PRD.CODIGO\n"
                + "          LEFT JOIN PA_ITEN_001 DEP_1 ON DEP_1.CODIGO = PCT.CODIGO AND DEP_1.COR = PCT.COR AND DEP_1.TAM = PCT.TAM AND DEP_1.DEPOSITO IN ('0001')\n"
                + "          LEFT JOIN PA_ITEN_001 DEP_5 ON DEP_5.CODIGO = PCT.CODIGO AND DEP_5.COR = PCT.COR AND DEP_5.TAM = PCT.TAM AND DEP_5.DEPOSITO IN ('0005')\n"
                + "          LEFT JOIN PA_ITEN_001 PAI_MOST ON PAI_MOST.CODIGO = PCT.CODIGO AND PAI_MOST.COR = PCT.COR  AND PAI_MOST.TAM = PCT.TAM AND PAI_MOST.DEPOSITO = '0011'\n"
                + "         WHERE PRD.CODIGO = '" + pReferencia + "') PRODUC ON PRODUC.CODIGO = CORTAM.CODIGO AND PRODUC.COR = CORTAM.COR AND PRODUC.TAM = CORTAM.TAM\n"
                + "      LEFT JOIN V_SD_PRODUCAO_REFER PREF ON PREF.CODIGO = '" + pReferencia + "' AND PREF.COR = CORTAM.COR AND PREF.TAM = CORTAM.TAM AND PREF.COLECAO = PRD.COLECAO\n"
                + "      WHERE PRD.ATIVO = 'S' AND PRD.CODIGO = '" + pReferencia + "'\n"
                + "  GROUP BY PRD.CODIGO),\n"
                + "SETORES AS (/* SELECT SETORES COLUNAS */\n"
                + "SELECT CODIGO, \n"
                + "       SUM(CASE WHEN SETOR = '111' THEN SALDO ELSE 0 END) \"111\",\n"
                + "       SUM(CASE WHEN SETOR = '110' THEN SALDO ELSE 0 END) \"110\",\n"
                + "       SUM(CASE WHEN SETOR = '109' THEN SALDO ELSE 0 END) \"109\",\n"
                + "       SUM(CASE WHEN SETOR = '209' THEN SALDO ELSE 0 END) \"209\",\n"
                + "       SUM(CASE WHEN SETOR = '201' THEN SALDO ELSE 0 END) \"201\",\n"
                + "       SUM(CASE WHEN SETOR = '101' THEN SALDO ELSE 0 END) \"101\",\n"
                + "       SUM(CASE WHEN SETOR = '99' THEN SALDO ELSE 0 END) \"99\",\n"
                + "       SUM(CASE WHEN SETOR = '100' THEN SALDO ELSE 0 END) \"100\",\n"
                + "       SUM(SALDO) \"TOT\"\n"
                + "FROM (/* PENDENTES NOS SETORES DA PARTE DO PCP [V2] */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S' \n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                                   AND FAC.PARTE IN CASE WHEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) IS NOT NULL \n"
                + "                                                      THEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) \n"
                + "                                                      ELSE \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO GROUP BY PARTE)\n"
                + "                                                      END\n"
                + "      WHERE PRD.CODIGO = '" + pReferencia + "'\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S', 'P')\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, FAC.OP, FAC.PARTE\n"
                + "      UNION\n"
                + "      /* PENDENTES NOS SETORES DA PARTE DIFERENTE DO PCP */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "      WHERE PRD.CODIGO = '" + pReferencia + "'\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S', 'P')\n"
                + "      AND FAC.PARTE NOT IN (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE)\n"
                + "      AND nvl((SELECT SUM(FAC2.QT_ORIG) - SUM(FAC2.QUANT + FAC2.QUANT_2 + FAC2.QUANT_F + FAC2.QUANT_I)\n"
                + "                  FROM FACCAO_001 FAC2 WHERE NUMERO = OF1.NUMERO\n"
                + "                       AND PARTE = (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = FAC2.NUMERO \n"
                + "                                      AND OP IN ('100') GROUP BY PARTE)),0) <= 0\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, FAC.OP, FAC.PARTE)\n"
                + "GROUP BY CODIGO),\n"
                + "PERIODOS AS (/* PENDENTES NO PCP POR PER�ODO */\n"
                + "SELECT CODIGO,\n");
        for (String periodo : pPeriodos) {
            query.append("       SUM(CASE WHEN PERIODO = '" + periodo + "' AND OP = '99' THEN SALDO ELSE 0 END) \"" + periodo + "-C\", \n");
            query.append("       SUM(CASE WHEN PERIODO = '" + periodo + "' AND OP = '100' THEN SALDO ELSE 0 END) \"" + periodo + "-P\", \n");
        }
        query.append("       SUM(SALDO) TOTAL\n"
                + "FROM (SELECT OF1.NUMERO, OF1.PERIODO, PRD.CODIGO, FAC.OP, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "FROM OF1_001 OF1\n"
                + "INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                             AND FAC.OP IN ('100','99')\n"
                + "WHERE OF1.PERIODO IN " + inPeriodos + "\n"
                + "GROUP BY OF1.NUMERO, OF1.PERIODO, PRD.CODIGO, FAC.OP, FAC.PARTE)\n"
                + "GROUP BY CODIGO)\n"
                + "    \n"
                + "SELECT DISTINCT\n"
                + "  NVL(PART.PAIS,'-') \"PAÍS\", \n"
                + "  NVL(COL.CODIGO,'-') \"COLEÇÃO\", \n"
                + "  NVL(REPLACE(COL.DESCRICAO,'COLEÇÃO ',''),'-') \"DESC. COLEÇÃO\", \n"
                + "  NVL(PROD.PAG_CATALOGO,'-') \"PÁG\", \n"
                + "  NVL(PROD.CODIGO,'-') \"REFER.\", \n"
                + "  NVL(PROD.DESCRICAO,'-') || CASE WHEN SDPROD.OBSERVACAO_PCP IS NOT NULL THEN ' *' END \"DESCRIÇÃO\",\n"
                + "  nvl(to_char(prod.dt_entrega,'DD/MM/YYYY'), sysdate2) \"DT. ENTREGA\",\n"
                + "/*AQUI*/  NVL(PCP_LIB.STATUS,'LIVRE') \"VENDA\",\n"
                + "  NVL(REPLACE(MAR.DESCRICAO, 'MOOVE ',''),'-') MARCA,\n"
                + "  NVL(TRIM(REPLACE(REPLACE(REGEXP_REPLACE(LIN.DESCRICAO,'*(MASC)*(FEM)*',''), 'MOOVE ',''), MAR.DESCRICAO,'')),'-') LINHA, \n"
                + "  NVL(ETQ.DESCRICAO,'-') \"CLASSE/MODEL.\", \n"
                + "  NVL(FAM.DESCRICAO,'-') \"FAMÍLIA\",\n"
                + "/*AQUI*/  NVL(ESTP.ESTAMPA,'-') \"CJ-F\",\n"
                + "  NVL(CASE WHEN COMB.MALHA IS NULL THEN '0' ELSE COMB.MALHA END,'-') \"CÓD TEC.\", \n"
                + "  NVL(CASE WHEN MATT.DESCRICAO IS NULL THEN 'SEM CADASTRO' ELSE MATT.DESCRICAO END,'-') \"TEC. PRINCIPAL\", \n"
                + "  NVL(CASE WHEN FT.CONSUMO IS NULL THEN 0 ELSE FT.CONSUMO END,0) CONSUMO,\n"
                + "  NVL(TO_CHAR(PRE.PRECO_00),'-') \"PREÇO R$\", \n"
                + "  NVL((SLDS.PEND + SLDS.FAT),0) \"VENDA TOTAL\", \n"
                + "  NVL(SLDS.PEND,0) \"VENDA PENDENTE\", \n"
                + "  NVL(SLDS.CONC,0) \"CONCENTRADO\",\n"
                + "  NVL(REV.META_NOVA||'-'||REV.OBS,0) ANALISTA, \n"
                + "  NVL((NVL((SLDS.PROD_BOAS + SLDS.PROD_PEND + SLDS.MOST),0) + SLDS.PLANEJ),0) META,\n"
                + "/*AQUI*/  0 \"% IND. PROJ.\",\n"
                + "/*AQUI*/  0 \"PROJ.\",\n"
                + "/*AQUI*/  0 \"PR-META\",\n"
                + "  NVL((SLDS.PROD_BOAS + SLDS.PROD_PEND),0) \"PRODUÇÃO\",\n"
                + "/*DBG  NVL(SLDS.PROD_BOAS,0) \"PRODUZIDO\", */\n"
                + "/*DBG  NVL(SLDS.PROD_SEG,0) \"PRODUZIDO 2a\",*/\n"
                + "  NVL(SLDS.EST,0) + NVL(SLDS.EM_EXP,0) ESTOQUE, \n"
                + "  NVL(SLDS.MOST,0) MOSTR,\n"
                + "  NVL(SLDS.SLD_EST_POS,0) \"SLD ESTQ POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_EST_NEG,0) \"SLD ESTQ NEGAT.(-)\",\n"
                + "  NVL(SLDS.SLD_PROD_POS,0) \"SLD PROD POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_PROD_NEG,0) \"SLD PROD NEGAT.(-)\",\n"
                + "  NVL(SLDS.SLD_PLAN_POS,0) \"SLD PLAN POSIT.(+)\",\n"
                + "  NVL(SLDS.SLD_PLAN_NEG,0) \"SLD PLAN NEGAT.(-)\",\n"
                + "  NVL(CASE WHEN STRS.\"111\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"111\") END,'-') \"ACAB.\",\n"
                + "  NVL(CASE WHEN STRS.\"110\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"110\") END,'-') \"LAV.\",\n"
                + "  NVL(CASE WHEN STRS.\"209\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"209\") END,'-') \"COSTURA PY\",\n"
                + "  NVL(CASE WHEN STRS.\"109\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"109\") END,'-') \"COSTURA\",\n"
                + "  NVL(CASE WHEN STRS.\"201\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"201\") END,'-') \"ENCAIXE PY\",\n"
                + "  NVL(CASE WHEN STRS.\"101\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"101\") END,'-') \"ENCAIXE BR\",\n"
                + "  NVL(CASE WHEN STRS.\"99\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"99\") END,'-') \"CONFIRMAÇÃO\",\n"
                + "  NVL(CASE WHEN STRS.\"100\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"100\") END,'-') \"PCP\",\n"
                + "  NVL(CASE WHEN STRS.\"TOT\" <= 0 THEN '-' ELSE TO_CHAR(STRS.\"TOT\") END,'-') \"TOTAL PROGRAMADO\",\n");
        for (String periodo : pPeriodos) {
            query.append("  NVL(CASE WHEN PERI.\"" + periodo + "-C\" = 0 THEN '-' ELSE TO_CHAR(PERI.\"" + periodo + "-C\") END, '-') \"LT " + periodo + "-C\",\n");
            query.append("  NVL(CASE WHEN PERI.\"" + periodo + "-P\" = 0 THEN '-' ELSE TO_CHAR(PERI.\"" + periodo + "-P\") END, '-') \"LT " + periodo + "-P\",\n");
        }
        query.append("  CASE WHEN NVL(PERI.TOTAL,0) = 0 THEN '-' ELSE TO_CHAR(PERI.TOTAL) END TOTAL,\n"
                + "LIN_INDICE.DESCRICAO GRP_INDICE\n"
                + "FROM PRODUTO_001 PROD\n"
                + "JOIN SD_PRODUTO_001 SDPROD ON SDPROD.CODIGO = PROD.CODIGO\n"
                + "JOIN COLECAO_001 COL ON COL.CODIGO = PROD.COLECAO\n"
                + "JOIN MARCA_001 MAR ON MAR.CODIGO = PROD.MARCA\n"
                + "JOIN TABLIN_001 LIN ON LIN.CODIGO = PROD.LINHA\n"
                + "JOIN ETQ_PROD_001 ETQ ON ETQ.CODIGO = PROD.ETIQUETA\n"
                + "JOIN FAMILIA_001 FAM ON FAM.CODIGO = PROD.FAMILIA\n"
                + "LEFT JOIN TABLIN_001 LIN_INDICE ON LIN_INDICE.CODIGO = LIN.GRP_INDICE\n"
                + "LEFT JOIN EST_PROD_001 ESTP ON ESTP.PRODUTO = PROD.CODIGO\n"
                + "LEFT JOIN (SELECT REV2.CODIGO, REV2.COLECAO, REV2.META_NOVA, MAX(NVL(OBS.REVISAO,'0')) OBS\n"
                + "  FROM SD_HISTORICO_REVPLAN_001 REV2 \n"
                + "  LEFT JOIN SD_OBS_REVPLAN_001 OBS ON OBS.CODIGO = REV2.CODIGO AND OBS.COLECAO = REV2.COLECAO\n"
                + "  WHERE REV2.DT_REVISAO >= (SELECT MAX(REV3.DT_REVISAO) FROM SD_HISTORICO_REVPLAN_001 REV3 \n"
                + "    WHERE REV3.CODIGO = REV2.CODIGO AND REV3.COLECAO = REV2.COLECAO)\n"
                + "  GROUP BY REV2.CODIGO, REV2.COLECAO, REV2.META_NOVA) REV ON REV.CODIGO = PROD.CODIGO AND REV.COLECAO = PROD.COLECAO\n"
                + "LEFT JOIN (SELECT LIB_PCP.CODIGO, LIB_PCP.COLECAO, '*'||LIB_PCP.STATUS STATUS\n"
                + "  FROM SD_LIBERACAO_PCP_001 LIB_PCP \n"
                + "  WHERE LIB_PCP.DT_MANUTENCAO >= (SELECT MAX(LIB_PCP2.DT_MANUTENCAO) FROM SD_LIBERACAO_PCP_001 LIB_PCP2 \n"
                + "    WHERE LIB_PCP2.CODIGO = LIB_PCP.CODIGO AND LIB_PCP2.COLECAO = LIB_PCP.COLECAO)) PCP_LIB ON PCP_LIB.CODIGO = PROD.CODIGO AND PCP_LIB.COLECAO = PROD.COLECAO\n"
                + "LEFT JOIN TABPRECO_001 PRE ON PRE.CODIGO = PROD.CODIGO AND PRE.REGIAO IN ('F','D','P')\n"
                + "LEFT JOIN COMBINACAO_001 COMB ON COMB.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN MATERIAL_001 MATT ON MATT.CODIGO = COMB.MALHA\n"
                + "LEFT JOIN (SELECT FT2.CODIGO, FT2.INSUMO, SUM(FT2.CONSUMO) CONSUMO\n" +
                "  FROM PCPFT2_001 FT2\n" +
                "  JOIN PRO_FLUXO_001 FLUXP\n" +
                "    ON FLUXP.CODIGO = FT2.CODIGO\n" +
                "   AND FLUXP.PARTE = FT2.PARTE\n" +
                "   AND FLUXP.PADRAO = 'S'\n" +
                " GROUP BY FT2.CODIGO, FT2.INSUMO) FT ON FT.INSUMO = COMB.MALHA AND FT.CODIGO = COMB.CODIGO\n"
                + "JOIN (SELECT FLX.CODIGO, MAX(PART.PAIS) PAIS\n"
                + "    FROM PRO_FLUXO_001 FLX\n"
                + "    JOIN TIPO_APL_001 PART ON PART.CODIGO = FLX.PARTE\n"
                + "    WHERE FLX.PADRAO = 'S'\n"
                + "    GROUP BY FLX.CODIGO) PART ON PART.CODIGO = PROD.CODIGO  \n"
                + "LEFT JOIN SALDOS SLDS ON SLDS.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN SETORES STRS ON STRS.CODIGO = PROD.CODIGO\n"
                + "LEFT JOIN PERIODOS PERI ON PERI.CODIGO = PROD.CODIGO\n"
                + "WHERE PROD.ATIVO = 'S'\n"
                + "AND PROD.STATUS = 'LIBERADO P/ PRODUÇÃO'\n"
                + "AND PROD.CODIGO = '" + pReferencia + "'\n");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        ResultSetMetaData rsMetaData = resultSetSql.getMetaData();
        int qtdeColumns = rsMetaData.getColumnCount();
//        Map<String, String> dataHeaderColumn = new HashMap<>();
//        for (int i = 1; i <= qtdeColumns; i++) {
//            dataHeaderColumn.put("clm" + i, rsMetaData.getColumnName(i));
//        }
//        producao.add(dataHeaderColumn);

//        Map<String, String> dataIndice = new HashMap<>();
        while (resultSetSql.next()) {
//            Map<String, String> dataRow = new HashMap<>();
            for (int i = 1; i <= qtdeColumns; i++) {
                String columnValue = resultSetSql.getString(i);
                producao.put("clm" + i, columnValue);
            }
        }

        closeConnection();
        resultSetSql.close();

        return producao;
    }

    @Override
    public ObservableList<GestaoDeProduto> getAnaliseProduto(String pCodigo, String pDataInicio, String pDataFim, List<String> pPeriodos, List<String> pColecoes) throws SQLException {
        ObservableList<GestaoDeProduto> producao = FXCollections.observableArrayList();

        String inColecoes = "('x'";
        for (String colecao : pColecoes) {
            inColecoes = inColecoes.concat(",'" + colecao + "'");
        }
        inColecoes = inColecoes.concat(")");

        String inPeriodos = "('x'";
        for (String periodo : pPeriodos) {
            inPeriodos = inPeriodos.concat(",'" + periodo + "'");
        }
        inPeriodos = inPeriodos.concat(")");

        StringBuilder query = new StringBuilder(
                "WITH \n"
                + "VENDAS AS ("
                + "SELECT PEI.CODIGO, PEI.COR, PEI.TAM, SUM(PEI.QTDE) CART, SUM(PEI.QTDE_F) FAT\n"
                + "         FROM PEDIDO_001 PED INNER JOIN PED_ITEN_001 PEI ON PED.NUMERO = PEI.NUMERO\n"
                + "         INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = PEI.CODIGO\n"
                + "         WHERE PEI.CODIGO = '" + pCodigo + "' AND PED.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy') AND PED.PERIODO BETWEEN '0000' AND '9999'\n"
                + "         GROUP BY PEI.CODIGO, PRD.LINHA, PEI.COR, PEI.TAM),\n"
                + "CONCENTRADO AS\n"
                + " (select pei.codigo,\n"
                + "         pei.cor,\n"
                + "         pei.tam,\n"
                + "         sum(nvl(pei.qtde, 0) + nvl(pei.qtde_f, 0)) conc,\n"
                + "         sum(nvl(pei.qtde, 0)) conc_p\n"
                + "    from pedido_001 ped \n"
                + "    join ped_iten_001 pei\n"
                + "      on pei.numero = ped.numero\n"
                + "    join vpedido_concentrados_001 conc on conc.numero = ped.numero\n"
                + "     and conc.codigo = pei.codigo\n"
                + "     and PED.DT_EMISSAO BETWEEN TO_DATE('" + pDataInicio + "','dd/MM/yyyy') AND TO_DATE('" + pDataFim + "','dd/MM/yyyy')\n"
                + "   where pei.codigo = '" + pCodigo + "'\n"
                + "     and ped.periodo between '0000' and '9999'\n"
                + "   group by pei.codigo, pei.cor, pei.tam),\n"
                + "PRODUCAO AS (SELECT CODIGO, COR, TAM, SUM(SALDO) QTDE FROM (\n"
                + "/* PENDENTES NOS SETORES DA PARTE DO PCP [V2] */  \n"
                + "SELECT OF1.NUMERO, FAC.OP, PRD.CODIGO, FAC.COR, FAC.TAM, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "FROM OF1_001 OF1\n"
                + "INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S' \n"
                + "INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0  AND FAC.OP NOT IN ('100','99')\n"
                + "                           	AND FAC.PARTE IN CASE WHEN \n"
                + "                                                 	(SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100','99') GROUP BY PARTE) IS NOT NULL \n"
                + "                                                  THEN \n"
                + "                                                     (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100','99') GROUP BY PARTE) \n"
                + "                                                  ELSE \n"
                + "                                                     (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO GROUP BY PARTE)\n"
                + "                                                  END\n"
                + "WHERE 1 = 1 AND PRD.CODIGO = '" + pCodigo + "' AND OF1.PERIODO NOT IN ('M','S','P')\n"
                + (Boolean.logicalOr(inColecoes.contains("17CO"), inColecoes.contains("GR")) ? "and OF1.PERIODO IN " + inPeriodos : "") + "\n"
                + "GROUP BY OF1.NUMERO, FAC.OP, PRD.CODIGO, FAC.COR, FAC.TAM\n"
                + "UNION\n"
                + "/* PENDENTES NOS SETORES DA PARTE DIFERENTE DO PCP */\n"
                + "SELECT OF1.NUMERO, FAC.OP, PRD.CODIGO, FAC.COR, FAC.TAM, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "FROM OF1_001 OF1\n"
                + "INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "WHERE 1 = 1 AND PRD.CODIGO = '" + pCodigo + "' AND OF1.PERIODO NOT IN ('M','S','P')\n"
                + (Boolean.logicalOr(inColecoes.contains("17CO"), inColecoes.contains("GR")) ? "and OF1.PERIODO IN " + inPeriodos : "") + "\n"
                + "AND (SELECT SUM(FAC2.QT_ORIG) - SUM(FAC2.QUANT + FAC2.QUANT_2 + FAC2.QUANT_F + FAC2.QUANT_I)\n"
                + "              FROM FACCAO_001 FAC2 WHERE NUMERO = OF1.NUMERO\n"
                + "               AND PARTE = (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = FAC2.NUMERO \n"
                + "                  							AND OP IN ('100','99') GROUP BY PARTE)) <= 0\n"
                + "GROUP BY OF1.NUMERO, FAC.OP, PRD.CODIGO, FAC.COR, FAC.TAM) GROUP BY CODIGO, COR, TAM),\n"
                + "EXPEDICAO AS (SELECT CODIGO, COR, TAM, SUM(QTDE) QTDE\n"
                + "			  FROM PEDIDO3_001 \n"
                + "			  WHERE QTDE > 0 AND QTDE_F = 0 and  deposito in ('0001','0005')\n"
                + "			  GROUP BY CODIGO, COR, TAM)"
                + "	\n"
                + "SELECT 	TAM, COR, \n"
                + "		ESTQ + EXPD \"ESTQ\", \n"
                + "		MOST \"MOSTR\",\n"
                + "		EM_PRODUC \"EM_PRODUC\",\n"
                + "		QTD_PRODUZ \"PRODUZIDO\",\n"
                + "		SEG_QUA \"SEG_QUA\",\n"
                + "		CASE WHEN PLANEJ < 0 THEN 0 ELSE PLANEJ END \"PLAN_OF\",\n"
                + "		' ' \"SIMULACAO\",\n"
                + "		CARTEI  - TOT_CONC_PEND \"CARTEIRA\",\n"
                + "		TOT_CONC_PEND \"CONCENT_PEND\",\n"
                + "		TOT_CONC \"CONCENTRADO\",\n"
                + "		FATUR + CARTEI \"VENDA_TOTAL\",\n"
                + "		TO_NUMBER(ROUND(CASE WHEN VEND_TOT_COR = 0 THEN 0 ELSE (VEND_TOT_TAM/VEND_TOT)*100 END,2)) \"P_VEND_TAM\",\n"
                + "		TO_NUMBER(ROUND(CASE WHEN VEND_TOT_COR = 0 THEN 0 ELSE (VEND_TOT_TAM/VEND_TOT_COR)*100 END,2)) \"P_VEND_TAMxCOR\",\n"
                + "		TO_NUMBER(ROUND(CASE WHEN VEND_TOT_COR = 0 THEN 0 ELSE (VEND_TOT_COR/VEND_TOT)*100 END,2)) \"P_VEND_COR\",\n"
                + "		TO_NUMBER(ROUND(P_PRD_TAM,2)) \"P_PRD_TAM\", \n"
                + "		TO_NUMBER(ROUND(P_PRD_COR,2)) \"P_PRD_COR\",\n"
                + "		TO_NUMBER(ROUND(CASE WHEN VEND_TOT_COR = 0 THEN 0 ELSE ((f_verifica_divisor(P_PRD_TAM,P_PRD_COR)))*100 END,2)) \"P_PRD_TAMxCOR\",\n"
                + "		ESTQ + EXPD + MOST - CARTEI \"SLD_ESTQ\",\n"
                + "		ESTQ + EXPD + MOST + EM_PRODUC - CARTEI \"SLD_PRODUC\",\n"
                + "		ESTQ + EXPD + MOST + EM_PRODUC + PLANEJ - CARTEI \"SLD_PLANEJ\",\n" +
                        "    STATUS_PCP,\n" +
                        "    STATUS_COMERCIAL\n"
                + "FROM (SELECT \n"
                + "	PRD.CODIGO,\n"
                + "	CORTAM.COR AS \"COR\", \n"
                + "	CORTAM.TAM AS \"TAM\",\n"
                + "	NVL(PAI1.QUANTIDADE,0) + NVL(PAI5.QUANTIDADE,0) AS \"ESTQ\",\n"
                + "	NVL(EXPD.QTDE,0) AS \"EXPD\",\n"
                + "	NVL(MOST.QUANTIDADE,0) AS \"MOST\",\n"
                + "	NVL((select sum(QTDE) seg from PA_MOV_001 pmov where pmov.codigo = PRD.CODIGO and pmov.cor = CORTAM.COR and pmov.tamanho = CORTAM.TAM AND pmov.TIP_BAI = '8' AND pmov.OPERACAO = 'E' AND pmov.DEPOSITO IN ('0007')),0) AS \"SEG_QUA\",\n"
                + "	SUM(NVL(FAC.QT_ORIG,0) - (NVL(FAC.QUANT,0) + NVL(FAC.QUANT_2,0) + NVL(FAC.QUANT_F,0) + NVL(FAC.QUANT_I,0))) AS \"PLANEJ\",\n"
                + "	SUM(NVL(OFI.QTDE_B,0)) AS \"QTD_PRODUZ\",\n"
                + "	NVL(VENDAS.FAT,0) AS \"FATUR\", \n"
                + "	NVL(VENDAS.CART,0) AS \"CARTEI\",\n"
                + "	NVL(VENDAS.FAT,0)+NVL(VENDAS.CART,0)-NVL((select conc from concentrado con where con.codigo = prd.codigo and con.cor = cortam.cor and con.tam = cortam.tam),0) AS \"VEND_TOT_TAM\",\n"
                + "	SUM(NVL(VENDAS.FAT,0)+NVL(VENDAS.CART,0)-NVL((select conc from concentrado con where con.codigo = prd.codigo and con.cor = cortam.cor and con.tam = cortam.tam),0)) OVER (PARTITION BY PRD.CODIGO, CORTAM.COR) AS \"VEND_TOT_COR\",\n"
                + "	SUM(NVL(VENDAS.FAT,0)+NVL(VENDAS.CART,0)-NVL((select conc from concentrado con where con.codigo = prd.codigo and con.cor = cortam.cor and con.tam = cortam.tam),0)) OVER (PARTITION BY PRD.CODIGO) AS \"VEND_TOT\",\n"
                + "	NVL((select conc from concentrado con where con.codigo = prd.codigo and con.cor = cortam.cor and con.tam = cortam.tam),0) AS \"TOT_CONC\",\n"
                + "	NVL((select conc_p from concentrado con where con.codigo = prd.codigo and con.cor = cortam.cor and con.tam = cortam.tam),0) AS \"TOT_CONC_PEND\",\n"
                + "	NVL(PRODUC.QTDE,0) AS \"EM_PRODUC\",\n"
                + "	POR.PERCENTUAL AS \"P_PRD_TAM\",\n"
                + "	SUM(POR.PERCENTUAL) OVER (PARTITION BY PRD.CODIGO, CORTAM.COR) AS \"P_PRD_COR\",\n" +
                        "    decode(prodordem.percentual, null, 5, nvl(libgrade.status_pcp,1)) status_pcp,\n" +
                        "  nvl(libgrade.status_comercial,1) status_comercial\n"
                + "FROM \n"
                + "	PRODUTO_001 PRD\n"
                + "	INNER JOIN FAIXA_001 FAI ON FAI.CODIGO = PRD.FAIXA\n"
                + "	INNER JOIN FAIXA_ITEN_001 FII ON FII.FAIXA = FAI.CODIGO\n"
                + "	INNER JOIN VPRODUTO_CORTAM_001 CORTAM ON CORTAM.CODIGO = PRD.CODIGO AND CORTAM.TAM = FII.TAMANHO\n" +
                        "  left join sd_status_grade_prod_001 libgrade on libgrade.codigo = cortam.codigo and libgrade.cor = cortam.cor and libgrade.tam = cortam.tam\n" +
                        "  left join produto_ordem_001 prodordem on prodordem.codigo = cortam.codigo and prodordem.cor = cortam.cor and prodordem.tam = cortam.tam\n"
                + "	LEFT JOIN OF1_001 OF1 ON OF1.CODIGO = PRD.CODIGO AND OF1.PERIODO NOT IN ('M','S','P') /*CONSTANTE*/\n"
                + "	LEFT JOIN VENDAS ON VENDAS.CODIGO = CORTAM.CODIGO AND VENDAS.COR = CORTAM.COR AND VENDAS.TAM = CORTAM.TAM\n"
                + "	LEFT JOIN PRODUCAO PRODUC ON PRODUC.CODIGO = CORTAM.CODIGO AND PRODUC.COR = CORTAM.COR AND PRODUC.TAM = CORTAM.TAM\n"
                + "	LEFT JOIN PRODUTO_ORDEM_001 POR ON POR.CODIGO = PRD.CODIGO AND POR.COR = CORTAM.COR AND POR.TAM = CORTAM.TAM AND POR.TIPO = 'V'\n"
                + "	LEFT JOIN PA_ITEN_001 PAI5 ON PAI5.CODIGO = CORTAM.CODIGO AND PAI5.COR = CORTAM.COR AND PAI5.TAM = CORTAM.TAM AND PAI5.DEPOSITO IN ('0005') /*CONSTANTE*/\n"
                + "	LEFT JOIN PA_ITEN_001 PAI1 ON PAI1.CODIGO = CORTAM.CODIGO AND PAI1.COR = CORTAM.COR AND PAI1.TAM = CORTAM.TAM AND PAI1.DEPOSITO IN ('0001') /*CONSTANTE*/\n"
                + "	LEFT JOIN PA_ITEN_001 MOST ON MOST.CODIGO = CORTAM.CODIGO AND MOST.COR = CORTAM.COR AND MOST.TAM = CORTAM.TAM AND MOST.DEPOSITO IN ('0011') /*CONSTANTE*/\n"
                + "	--LEFT JOIN PA_MOV_001 SEG ON SEG.CODIGO = CORTAM.CODIGO AND SEG.COR = CORTAM.COR AND SEG.TAMANHO = CORTAM.TAM AND SEG.TIP_BAI = '8' AND SEG.OPERACAO = 'E' AND SEG.DEPOSITO IN ('0007') /*CONSTANTE*/\n"
                + "	LEFT JOIN OF_ITEN_001 OFI ON OFI.NUMERO = OF1.NUMERO AND OFI.COR = CORTAM.COR AND OFI.TAM = CORTAM.TAM\n"
                + "	LEFT JOIN EXPEDICAO EXPD ON EXPD.CODIGO = CORTAM.CODIGO AND EXPD.COR = CORTAM.COR AND EXPD.TAM = CORTAM.TAM\n"
                + "	LEFT JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.CODIGO = OF1.CODIGO AND FAC.COR = OFI.COR AND FAC.TAM = OFI.TAM AND FAC.OP IN ('100','99')  /*CONSTANTE*/\n"
                + "                                 AND NVL(FAC.QT_ORIG,0) - (NVL(FAC.QUANT,0) + NVL(FAC.QUANT_2,0) + NVL(FAC.QUANT_F,0) + NVL(FAC.QUANT_I,0)) > 0\n"
                + "WHERE \n"
                + "	PRD.ATIVO = 'S' /*CONSTANTE*/\n"
                + "	AND PRD.STATUS = 'LIBERADO P/ PRODUÇÃO' /*CONSTANTE*/\n"
                + "	AND PRD.CODIGO = '" + pCodigo + "'\n"
                + "GROUP BY\n"
                + "	PRD.CODIGO, CORTAM.COR, CORTAM.TAM, VENDAS.FAT, VENDAS.CART, POR.PERCENTUAL, FII.POSICAO,\n"
                + "	PRODUC.QTDE, MOST.QUANTIDADE, PAI1.QUANTIDADE, PAI5.QUANTIDADE, EXPD.QTDE, libgrade.status_pcp,\n" +
                        "  libgrade.status_comercial, prodordem.percentual\n"
                + "ORDER BY\n"
                + "	CORTAM.COR, FII.POSICAO) PLANEJ_PRODUTOS");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            producao.add(new GestaoDeProduto(resultSetSql.getString("TAM"), resultSetSql.getString("COR"), resultSetSql.getInt("ESTQ"), resultSetSql.getInt("MOSTR"), resultSetSql.getInt("EM_PRODUC"), resultSetSql.getInt("PRODUZIDO"),
                    resultSetSql.getInt("SEG_QUA"), resultSetSql.getInt("PLAN_OF"), resultSetSql.getInt("CARTEIRA"), resultSetSql.getInt("CONCENT_PEND"), resultSetSql.getInt("CONCENTRADO"),
                    resultSetSql.getInt("VENDA_TOTAL"), resultSetSql.getInt("SLD_ESTQ"), resultSetSql.getInt("SLD_PRODUC"), resultSetSql.getInt("SLD_PLANEJ"),
                    resultSetSql.getDouble("P_VEND_TAM"), resultSetSql.getDouble("P_VEND_COR"), resultSetSql.getDouble("P_PRD_TAM"), resultSetSql.getDouble("P_PRD_COR"),
                    resultSetSql.getDouble("P_PRD_TAMxCOR"), resultSetSql.getDouble("P_VEND_TAMxCOR"), resultSetSql.getInt("STATUS_PCP"), resultSetSql.getInt("STATUS_COMERCIAL")));
        }

        closeConnection();
        resultSetSql.close();

        return producao;
    }

    @Override
    public ObservableList<GestaoDeLote> getLotesProduto(String pCodigo) throws SQLException {
        ObservableList<GestaoDeLote> producao = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT\n"
                + "     OF1.NUMERO,\n"
                + "     FAC.OP SETOR,\n"
                + "	OF1.PERIODO,\n"
                + "	to_char(PER.DT_INICIO,'DD/MM/YYYY') DT_INICIO, \n"
                + "	to_char(PER.DT_FIM,'DD/MM/YYYY') DT_FIM, \n"
                + "	SUM(FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_I + FAC.QUANT_F)) PLANEJ_LOTE,\n"
                + " PRD.COLECAO\n"
                + "FROM\n"
                + "	OF1_001 OF1\n"
                + "	INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO\n"
                + "	INNER JOIN OF_ITEN_001 OFI ON OFI.NUMERO = OF1.NUMERO\n"
                + "	INNER JOIN TABPRZ_001 PER ON PER.PRAZO = OF1.PERIODO\n"
                + "	INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.CODIGO = OF1.CODIGO AND FAC.COR = OFI.COR AND FAC.TAM = OFI.TAM AND FAC.OP IN ('100','99')\n"
                + "									AND (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_I + FAC.QUANT_F) < FAC.QT_ORIG\n"
                + "WHERE\n"
                + "	OF1.CODIGO = '" + pCodigo + "'\n"
                + "	AND OF1.PERIODO NOT IN ('M','S') /*CONSTANTE*/\n"
                + "GROUP BY \n"
                + "	OF1.NUMERO, FAC.OP, OF1.PERIODO, PER.DT_INICIO, PER.DT_FIM, PRD.COLECAO\n"
                + "ORDER BY\n"
                + "	OF1.PERIODO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            producao.add(new GestaoDeLote(resultSetSql.getString("NUMERO"), resultSetSql.getString("PERIODO"), resultSetSql.getString("DT_INICIO"), resultSetSql.getString("DT_FIM"),
                    resultSetSql.getString("SETOR"), resultSetSql.getInt("PLANEJ_LOTE"), this.getGradeLote(pCodigo, resultSetSql.getString("NUMERO"), resultSetSql.getString("SETOR")), resultSetSql.getString("COLECAO")));
        }

        closeConnection();
        resultSetSql.close();

        return producao;
    }

    @Override
    public void callFunctionUpdateOf(String pNumeroOf, String pCodigo, String pGrade, String pSetor, String pUsuario) throws SQLException {
        StringBuilder query = new StringBuilder("{call P_SD_UPDATE_OF(?,?,?,?,?)}");

        connection = ConnectionFactory.getEmpresaConnection();
        CallableStatement callStatement = connection.prepareCall(query.toString());
        callStatement.setString(1, pNumeroOf);
        callStatement.setString(2, pGrade);
        callStatement.setString(3, pCodigo);
        callStatement.setString(4, pSetor);
        callStatement.setString(5, pUsuario);

        callStatement.execute();
        closeConnection();
    }

    @Override
    public String callFunctionCreateOf(String pLote, String pCodigo, String pGrade, String pUsuario) throws SQLException {
        String numeroNovaOf = "";
        StringBuilder query = new StringBuilder("{? = call F_SD_CREATE_OF(?,?,?,?)}");

        connection = ConnectionFactory.getEmpresaConnection();
        CallableStatement callStatement = connection.prepareCall(query.toString());
        callStatement.registerOutParameter(1, Types.VARCHAR);
        callStatement.setString(2, pCodigo);
        callStatement.setString(3, pLote);
        callStatement.setString(4, pGrade);
        callStatement.setString(5, pUsuario);

        callStatement.execute();
        numeroNovaOf = callStatement.getString(1);
        closeConnection();

        return numeroNovaOf;
    }

    @Override
    public String callFunctionDeleteOf(String pLote, String pUsuario) throws SQLException {
        String statusDelete = "";
        StringBuilder query = new StringBuilder("{? = call F_SD_DELETE_OF(?,?)}");

        connection = ConnectionFactory.getEmpresaConnection();
        CallableStatement callStatement = connection.prepareCall(query.toString());
        callStatement.registerOutParameter(1, Types.VARCHAR);
        callStatement.setString(2, pLote);
        callStatement.setString(3, pUsuario);

        callStatement.execute();
        statusDelete = callStatement.getString(1);
        closeConnection();

        return statusDelete;
    }

    @Override
    public void saveLiberacaoPcp(String codigo, String colecao, String status, String observacao, String usuario) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_LIBERACAO_PCP_001\n"
                + "(CODIGO, COLECAO, STATUS, OBS, DT_MANUTENCAO, USUARIO)\n"
                + "VALUES(?, ?, ?, ?, SYSDATE, ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, colecao);
        preparedStatement.setString(3, status);
        preparedStatement.setString(4, observacao);
        preparedStatement.setString(5, usuario);
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void saveHistoricoRev(String codigo, String colecao, Integer meta_ant, Integer meta_nova, String usuario) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "MERGE INTO SD_HISTORICO_REVPLAN_001 REV USING DUAL ON (\n"
                + "	REV.CODIGO = ?\n"
                + "	AND REV.COLECAO = ?\n"
                + "	AND TO_CHAR(REV.DT_REVISAO,'dd/MM/yyyy') = TO_CHAR(SYSDATE, 'dd/MM/yyyy')\n"
                + ")\n"
                + "WHEN MATCHED THEN \n"
                + "	UPDATE SET REV.HR_REVISAO = SYSDATE,\n"
                + "                REV.META_ANT = ?, \n"
                + "		   REV.META_NOVA = ?, \n"
                + "		   REV.USUARIO = ?\n"
                + "WHEN NOT MATCHED THEN \n"
                + "	INSERT (DT_REVISAO, CODIGO, COLECAO, META_ANT, META_NOVA, USUARIO, HR_REVISAO)\n"
                + "         VALUES (SYSDATE, ?, ?, ?, ?, ?, SYSDATE)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, colecao);
        preparedStatement.setInt(3, meta_ant);
        preparedStatement.setInt(4, meta_nova);
        preparedStatement.setString(5, usuario);
        preparedStatement.setString(6, codigo);
        preparedStatement.setString(7, colecao);
        preparedStatement.setInt(8, meta_ant);
        preparedStatement.setInt(9, meta_nova);
        preparedStatement.setString(10, usuario);
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public ObservableList<LiberacaoPcp> getHistLiberacaoCodigo(String codigo, String colecao) throws SQLException {
        ObservableList<LiberacaoPcp> historico = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT CODIGO, COLECAO, STATUS, OBS, DT_MANUTENCAO, USUARIO\n"
                + "FROM SD_LIBERACAO_PCP_001\n"
                + "WHERE CODIGO = ? AND COLECAO = ?\n"
                + "ORDER BY DT_MANUTENCAO DESC");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, colecao);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            historico.add(new LiberacaoPcp(resultSetSql.getString("CODIGO"), resultSetSql.getString("COLECAO"), resultSetSql.getString("DT_MANUTENCAO"),
                    resultSetSql.getString("OBS"), resultSetSql.getString("STATUS"), resultSetSql.getString("USUARIO")));
        }

        closeConnection();
        resultSetSql.close();

        return historico;
    }

    @Override
    public List<String[]> getSetoresProducao() throws SQLException {
        List<String[]> setores = new ArrayList();

        StringBuilder query = new StringBuilder(
                "select codigo, descricao from cadfluxo_001 where ordem <> 0 and grupo = '999' order by ordem DESC, codigo DESC");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            setores.add(new String[]{resultSetSql.getString("codigo"), resultSetSql.getString("descricao")});
        }

        closeConnection();
        resultSetSql.close();

        return setores;
    }

    @Override
    public ObservableList<Map> getProducaoSetores(List<String> pColecoes, List<String[]> setores) throws SQLException {
        ObservableList<Map> producao = FXCollections.observableArrayList();

        String inColecoes = "('x'";
        for (String colecao : pColecoes) {
            inColecoes = inColecoes.concat(",'" + colecao + "'");
        }
        inColecoes = inColecoes.concat(")");

        StringBuilder query = new StringBuilder(""
                + "SELECT CODIGO, DESCRICAO, " + this.printSetores(setores) + ", SUM(TOT) TOTAL\n"
                + "FROM(SELECT * FROM\n"
                + "(SELECT PRODUC.CODIGO, PRODUC.DESCRICAO, SUM(PRODUC.SALDO) TOT, PRODUC.SETOR, SUM(PRODUC.SALDO) QTD_OP\n"
                + "FROM (/* PENDENTES NOS SETORES DA PARTE DO PCP [V2] */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, PRD.DESCRICAO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S' \n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                                   AND FAC.PARTE IN CASE WHEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) IS NOT NULL \n"
                + "                                                      THEN \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE) \n"
                + "                                                      ELSE \n"
                + "                                                         (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO GROUP BY PARTE)\n"
                + "                                                      END\n"
                + "      WHERE PRD.COLECAO in " + inColecoes + "\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S')\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, PRD.DESCRICAO, FAC.OP, FAC.PARTE\n"
                + "      UNION\n"
                + "      /* PENDENTES NOS SETORES DA PARTE DIFERENTE DO PCP */\n"
                + "      SELECT OF1.NUMERO, PRD.CODIGO, PRD.DESCRICAO, FAC.OP SETOR, FAC.PARTE, SUM(FAC.QT_ORIG) - SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) SALDO\n"
                + "      FROM OF1_001 OF1\n"
                + "      INNER JOIN PRODUTO_001 PRD ON PRD.CODIGO = OF1.CODIGO AND PRD.ATIVO = 'S'\n"
                + "      INNER JOIN FACCAO_001 FAC ON FAC.NUMERO = OF1.NUMERO AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "      WHERE PRD.COLECAO in " + inColecoes + "\n"
                + "      AND OF1.PERIODO NOT IN ('M', 'S')\n"
                + "      AND FAC.PARTE NOT IN (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = OF1.NUMERO AND OP IN ('100') GROUP BY PARTE)\n"
                + "      AND (SELECT SUM(FAC2.QT_ORIG) - SUM(FAC2.QUANT + FAC2.QUANT_2 + FAC2.QUANT_F + FAC2.QUANT_I)\n"
                + "                  FROM FACCAO_001 FAC2 WHERE NUMERO = OF1.NUMERO\n"
                + "                       AND PARTE = (SELECT PARTE FROM FACCAO_001 WHERE NUMERO = FAC2.NUMERO \n"
                + "                                      AND OP IN ('100') GROUP BY PARTE)) <= 0\n"
                + "      GROUP BY OF1.NUMERO, PRD.CODIGO, PRD.DESCRICAO, FAC.OP, FAC.PARTE) PRODUC\n"
                + "GROUP BY PRODUC.CODIGO,PRODUC.DESCRICAO, PRODUC.SETOR) GERAL\n"
                + "PIVOT (\n"
                + "      SUM(QTD_OP) FOR (SETOR) IN (" + this.printCodigoSetores(setores) + ")\n"
                + "))\n"
                + "GROUP BY CODIGO, DESCRICAO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        ResultSetMetaData rsMetaData = resultSetSql.getMetaData();
        int qtdeColumns = rsMetaData.getColumnCount();
        Map<String, String> dataHeaderColumn = new HashMap<>();
        for (int i = 1; i <= qtdeColumns; i++) {
            dataHeaderColumn.put("clm" + i, rsMetaData.getColumnName(i));
        }
        producao.add(dataHeaderColumn);

        Map<String, String> dataIndice = new HashMap<>();
        while (resultSetSql.next()) {
            Map<String, String> dataRow = new HashMap<>();
            for (int i = 1; i <= qtdeColumns; i++) {
                dataRow.put("clm" + i, resultSetSql.getString(i));
            }
            producao.add(dataRow);
        }

        closeConnection();
        resultSetSql.close();

        return producao;
    }

    private String printSetores(List<String[]> setores) {
        String setor = "";
        for (String[] duplaSetor : setores) {
            setor += "SUM(\"" + duplaSetor[1] + "\") \"" + duplaSetor[1] + "\",";
        }
        return setor.substring(0, setor.length() - 1);
    }

    private String printCodigoSetores(List<String[]> setores) {
        String setor = "";
        for (String[] duplaSetor : setores) {
            setor += "'" + duplaSetor[0] + "' \"" + duplaSetor[1] + "\",";
        }
        return setor.substring(0, setor.length() - 1);
    }

    @Override
    public ObservableList<HistoricoRevisaoMeta> getHistRevisaoMeta(String codigo, String colecao) throws SQLException {
        ObservableList<HistoricoRevisaoMeta> historico = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT * \n"
                + "FROM SD_OBS_REVPLAN_001\n"
                + "WHERE CODIGO = ? AND COLECAO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, colecao);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            historico.add(new HistoricoRevisaoMeta(resultSetSql.getString("DT_REVISAO"), resultSetSql.getString("CODIGO"), resultSetSql.getString("COLECAO"),
                    resultSetSql.getString("OBS"), resultSetSql.getString("USUARIO"), resultSetSql.getString("REVISAO")));
        }

        closeConnection();
        resultSetSql.close();

        return historico;
    }

    @Override
    public void saveHistoricoRevMeta(String codigo, String colecao, String obs, String usuario, String revisao) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "MERGE INTO SD_OBS_REVPLAN_001 REV USING DUAL ON (\n"
                + "	REV.CODIGO = ?\n"
                + "	AND REV.USUARIO = ?\n"
                + "	AND REV.COLECAO = ?\n"
                + "	AND TO_CHAR(REV.DT_REVISAO,'dd/MM/yyyy') = TO_CHAR(SYSDATE, 'dd/MM/yyyy')\n"
                + ")\n"
                + "WHEN MATCHED THEN \n"
                + "	UPDATE SET REV.OBS = ?,\n"
                + "                REV.HR_REVISAO = SYSDATE\n"
                + "WHEN NOT MATCHED THEN \n"
                + "	INSERT (DT_REVISAO, CODIGO, COLECAO, OBS, USUARIO, REVISAO, HR_REVISAO)\n"
                + "         VALUES (SYSDATE, ?, ?, ?, ?, ?, SYSDATE)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, usuario);
        preparedStatement.setString(3, colecao);
        preparedStatement.setString(4, obs);
        preparedStatement.setString(5, codigo);
        preparedStatement.setString(6, colecao);
        preparedStatement.setString(7, obs);
        preparedStatement.setString(8, usuario);
        preparedStatement.setString(9, revisao);
        preparedStatement.execute();

        this.closeConnection();
    }

    public void saveObsRevRegs(String colecao, String usuario) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_HISTORICO_REVPLAN_001\n"
                + "(DT_REVISAO, CODIGO, COLECAO, META_ANT, META_NOVA, USUARIO, HR_REVISAO)\n"
                + "SELECT SYSDATE DATA_REVISAO,\n"
                + "	PROD.CODIGO, PROD.COLECAO, \n"
                + "	   nvl((SELECT META_NOVA FROM SD_HISTORICO_REVPLAN_001 REV WHERE REV.CODIGO =  PROD.CODIGO AND REV.COLECAO = PROD.COLECAO AND\n"
                + "	   		   REV.DT_REVISAO = (SELECT MAX(REV2.DT_REVISAO) FROM SD_HISTORICO_REVPLAN_001 REV2 WHERE REV2.CODIGO = REV.CODIGO AND REV2.COLECAO = REV.COLECAO)),0) META_ANTERIOR,\n"
                + "	   0 META_NOVA,\n"
                + "	   '" + usuario + "' USUARIO,\n"
                + "	   SYSDATE HORA_REVISAO\n"
                + "FROM PRODUTO_001 PROD\n"
                + "WHERE PROD.COLECAO = '" + colecao + "' AND PROD.ATIVO = 'S'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();

        this.closeConnection();
    }

    public void saveHistoricoRevRegs(String colecao, String usuario) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_OBS_REVPLAN_001\n"
                + "(DT_REVISAO, HR_REVISAO, CODIGO, COLECAO, OBS, USUARIO, REVISAO)\n"
                + "SELECT SYSDATE DATA_REVISAO,\n"
                + "	   SYSDATE HORA_REVISAO,\n"
                + "	   PROD.CODIGO, PROD.COLECAO, \n"
                + "	   'Zerado analista para revisão' OBS,\n"
                + "	   '" + usuario + "' USUARIO,\n"
                + "	   ((SELECT COUNT(*) FROM SD_OBS_REVPLAN_001 WHERE CODIGO = PROD.CODIGO AND COLECAO = PROD.COLECAO) + 1) QTDE_REVS\n"
                + "FROM PRODUTO_001 PROD\n"
                + "WHERE PROD.COLECAO = '" + colecao + "' AND PROD.ATIVO = 'S'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void zerarAnalista(String colecao, String usuario) throws SQLException {
        this.saveHistoricoRevRegs(colecao, usuario);
        this.saveObsRevRegs(colecao, usuario);
    }

    @Override
    public void updateLoteOf(String numeroOf, String numeroLote, String dtInicio, String dtFim) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "begin\n"
                + "  update of1_001 set periodo = ?, dt_inicio = ?, dt_final = ? where numero = ?;\n" +
                "  update faccao_001 fac\n" +
                "   set fac.dt_s = to_date(?, 'DD/MM/YYYY'),\n" +
                "       fac.dt_r = (to_date(?, 'DD/MM/YYYY') +\n" +
                "                  (select setor.dias\n" +
                "                     from cadfluxo_001 setor\n" +
                "                    where setor.codigo = fac.op))\n" +
                " where fac.numero = ? and fac.op = '100';\n"
                + "end;");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numeroLote);
        preparedStatement.setString(2, dtInicio);
        preparedStatement.setString(3, dtFim);
        preparedStatement.setString(4, numeroOf);
        preparedStatement.setString(5, dtInicio);
        preparedStatement.setString(6, dtFim);
        preparedStatement.setString(7, numeroOf);
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void movimentaSetor99(GestaoDeLote gestaoOf, String codigoRef) throws SQLException {
        gestaoOf.getGrade().forEach(gradeOf -> {
            try {
                StringBuilder query = new StringBuilder("{call PKG_SD_PCP.MOVIMENTA99(?,?,?,?,?,?)}");

                connection = ConnectionFactory.getEmpresaConnection();
                CallableStatement callStatement = connection.prepareCall(query.toString());
                callStatement.setString(1, gestaoOf.getOf());
                callStatement.setString(2, codigoRef);
                callStatement.setString(3, gradeOf.getCod_cor());
                callStatement.setString(4, gradeOf.getTam());
                callStatement.setString(5, SceneMainController.getAcesso().getUsuario());
                callStatement.setInt(6, gradeOf.getPlanejado());

                callStatement.execute();
                callStatement.close();
                closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(GestaoProducaoOracleDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @Override
    public void saveObservacaoPcp(String codigo, String observacao) throws SQLException {
        SdProduto001 produto = new SdProduto001(codigo, observacao);
        DAOFactory.getSdProduto001DAO().update(produto);
    }

}
