/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.GrupoCli;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface GrupoCliDAO {

    public ObservableList<GrupoCli> getAll() throws SQLException;

    public ObservableList<GrupoCli> getGruposCliFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

}
