/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.IndiceProjecao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface IndiceProjecaoDAO {

    public List<IndiceProjecao> load() throws SQLException;

    public String getRegraIndice(String colecao, String marca) throws SQLException;

    public String getRegraIndiceByMeta(String colecao, String marca) throws SQLException;

    public String getRegraIndiceByMeta(String colecao, String marca, String linha) throws SQLException;

    public void deleteRegraIndice(IndiceProjecao indice) throws SQLException;

    public void updateRegraIndice(IndiceProjecao indice) throws SQLException;

    public void insertRegraIndice(IndiceProjecao indice) throws SQLException;

}
