/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.Colecao;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.IndiceProjecao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class IndiceProjecaoOracleDAO implements IndiceProjecaoDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public List<IndiceProjecao> load() throws SQLException {
        List<IndiceProjecao> regras = new ArrayList<>();

        StringBuilder query = new StringBuilder(
                "SELECT IND.COLECAO, COL.DESCRICAO, IND.MARCA, \n"
                + "	   CASE WHEN IND.BYMARCA IS NULL THEN 'N' ELSE IND.BYMARCA END BYMARCA, \n"
                + "	   CASE WHEN IND.BYLINHA IS NULL THEN 'N' ELSE IND.BYLINHA END BYLINHA, \n"
                + "	   NVL(IND.BYFIXO,0) BYFIXO,\n"
                + "	   NVL(IND.BYMETA,0) BYMETA,\n"
                + "	   NVL(IND.LINHAMETA,'-') LINHAMETA\n"
                + "FROM SD_INDICE_PROJECAO_001 IND\n"
                + "INNER JOIN COLECAO_001 COL ON COL.CODIGO = IND.COLECAO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            regras.add(new IndiceProjecao(resultSetSql.getString("MARCA"), new Colecao(resultSetSql.getString("COLECAO"), resultSetSql.getString("DESCRICAO")),
                    resultSetSql.getString("BYMARCA").equals("X"), resultSetSql.getString("BYLINHA").equals("X"), resultSetSql.getDouble("BYFIXO"),
                    resultSetSql.getDouble("BYMETA"), resultSetSql.getString("LINHAMETA")));
        }

        closeConnection();
        resultSetSql.close();

        return regras;
    }

    @Override
    public String getRegraIndice(String colecao, String marca) throws SQLException {
        String regra = "MARCA";

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT \n"
                + "	   CASE WHEN BYMARCA = 'X' AND BYMETA <= 0 THEN 'MARCA' ELSE\n"
                + "	   		CASE WHEN BYLINHA = 'X' AND BYMETA <= 0 THEN 'LINHA' ELSE\n"
                + "	   			CASE WHEN BYFIXO > 0 THEN TO_CHAR(BYFIXO) ELSE\n"
                + "	   				CASE WHEN BYMARCA = 'X' AND BYMETA > 0 THEN 'METAMARCA' ELSE	\n"
                + "	   					'METALINHA'\n"
                + "	   				END\n"
                + "	   			END\n"
                + "	   		END\n"
                + "	   END REGRA\n"
                + "FROM SD_INDICE_PROJECAO_001 \n"
                + "WHERE COLECAO = ? AND MARCA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, colecao);
        preparedStatement.setString(2, marca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            regra = resultSetSql.getString("REGRA");
        }

        closeConnection();
        resultSetSql.close();

        return regra;
    }

    @Override
    public void deleteRegraIndice(IndiceProjecao indice) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "DELETE FROM SD_INDICE_PROJECAO_001\n"
                + "WHERE COLECAO = ? AND MARCA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, indice.objColecaoProperty().get().strCodigoProperty().get());
        preparedStatement.setString(2, indice.strMarcaProperty().get());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void updateRegraIndice(IndiceProjecao indice) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "UPDATE SD_INDICE_PROJECAO_001 \n"
                + "SET BYMARCA = ?, BYLINHA = ?, BYFIXO = ?, BYMETA = ?, LINHAMETA = ?\n"
                + "WHERE COLECAO = ? AND MARCA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, indice.isByMarcaProperty().get() ? "X" : null);
        preparedStatement.setString(2, indice.isByLinhaProperty().get() ? "X" : null);
        preparedStatement.setDouble(3, indice.isByFixoProperty().get() ? indice.douIndiceProperty().get() : 0);
        preparedStatement.setDouble(4, indice.isByMetaProperty().get() ? indice.douMetaProperty().get() : 0);
        preparedStatement.setString(5, indice.isByMetaProperty().get() ? indice.strLinhaProperty().get() : null);
        preparedStatement.setString(6, indice.objColecaoProperty().get().strCodigoProperty().get());
        preparedStatement.setString(7, indice.strMarcaProperty().get());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void insertRegraIndice(IndiceProjecao indice) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_INDICE_PROJECAO_001 (COLECAO, MARCA, BYMARCA, BYLINHA, BYFIXO, BYMETA, LINHAMETA)\n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, indice.objColecaoProperty().get().strCodigoProperty().get());
        preparedStatement.setString(2, indice.strMarcaProperty().get());
        preparedStatement.setString(3, indice.isByMarcaProperty().get() ? "X" : null);
        preparedStatement.setString(4, indice.isByLinhaProperty().get() ? "X" : null);
        preparedStatement.setDouble(5, indice.isByFixoProperty().get() ? indice.douIndiceProperty().get() : 0);
        preparedStatement.setDouble(6, indice.isByMetaProperty().get() ? indice.douMetaProperty().get() : 0);
        preparedStatement.setString(7, indice.isByMetaProperty().get() ? indice.strLinhaProperty().get() : null);
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public String getRegraIndiceByMeta(String colecao, String marca) throws SQLException {
        String regra = "0";

        StringBuilder query = new StringBuilder(
                "SELECT BYMETA REGRA\n"
                + "FROM SD_INDICE_PROJECAO_001 \n"
                + "WHERE COLECAO = ? AND MARCA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, colecao);
        preparedStatement.setString(2, marca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            regra = resultSetSql.getString("REGRA");
        }

        closeConnection();
        resultSetSql.close();

        return regra;
    }

    @Override
    public String getRegraIndiceByMeta(String colecao, String marca, String linha) throws SQLException {
        String regra = "0";

        StringBuilder query = new StringBuilder(
                "SELECT BYMETA REGRA\n"
                + "FROM SD_INDICE_PROJECAO_001 \n"
                + "WHERE COLECAO = ? AND MARCA = ? AND LINHAMETA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, colecao);
        preparedStatement.setString(2, marca);
        preparedStatement.setString(3, linha);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            regra = resultSetSql.getString("REGRA");
        }

        closeConnection();
        resultSetSql.close();

        return regra;
    }

}
