/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Linha;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface LinhaDAO {

    public ObservableList<Linha> getAll() throws SQLException;

    public ObservableList<Linha> getByWindowsForm(String linha, String marca) throws SQLException;

    public ObservableList<Linha> getLinhasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

}
