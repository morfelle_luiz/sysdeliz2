/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Linha;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class LinhaOracleDAO implements LinhaDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Linha> getAll() throws SQLException {
        ObservableList<Linha> linhas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT LIN.CODIGO, LIN.DESCRICAO, LIN.SD_MARCA, LIN.CONCENTRADO, LIN.SD_LINHADESC, \n"
                        + "	CASE WHEN MAR.DESCRICAO IS NULL THEN 'NÃO CADASTRADA' ELSE MAR.DESCRICAO END DESC_MARCA\n"
                        + "FROM TABLIN_001 LIN\n"
                        + "LEFT JOIN MARCA_001 MAR ON MAR.CODIGO = LIN.SD_MARCA");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            linhas.add(new Linha(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("DESC_MARCA"),
                    resultSetSql.getString("SD_MARCA"), resultSetSql.getInt("CONCENTRADO"), resultSetSql.getString("SD_LINHADESC")));
        }
        closeConnection();
        resultSetSql.close();

        return linhas;
    }

    @Override
    public ObservableList<Linha> getByWindowsForm(String linha, String marca) throws SQLException {
        ObservableList<Linha> linhas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT DISTINCT LIN.CODIGO, LIN.DESCRICAO, LIN.SD_MARCA, LIN.CONCENTRADO, \n"
                        + "	CASE WHEN MAR.DESCRICAO IS NULL THEN 'NÃO CADASTRADA' ELSE MAR.DESCRICAO END DESC_MARCA\n"
                        + "FROM TABLIN_001 LIN\n"
                        + "LEFT JOIN MARCA_001 MAR ON MAR.CODIGO = LIN.SD_MARCA\n"
                        + "WHERE (LIN.CODIGO LIKE ? OR LIN.DESCRICAO LIKE ?) AND MAR.DESCRICAO LIKE ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + linha + "%");
        preparedStatement.setString(2, "%" + linha + "%");
        preparedStatement.setString(3, "%" + marca + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            linhas.add(new Linha(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("DESC_MARCA"),
                    resultSetSql.getString("SD_MARCA"), resultSetSql.getInt("CONCENTRADO")));
        }
        closeConnection();
        resultSetSql.close();

        return linhas;
    }

    @Override
    public ObservableList<Linha> getLinhasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Linha> linhas = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from TABLIN_001 \n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Linha linha = new Linha();
            linha.setCodigo(resultSetSql.getString("codigo"));
            linha.setDescricao(resultSetSql.getString("descricao"));
            linha.setSdDescLinha(resultSetSql.getString("SD_LINHADESC"));
            linhas.add(linha);
        }
        resultSetSql.close();
        this.closeConnection();
        return linhas;
    }

}
