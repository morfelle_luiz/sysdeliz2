/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.Log;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface LogDAO {

    public void saveLog(Log p_log) throws SQLException;

    public void saveLogTI(Log p_log) throws SQLException;

    public String getLastLogJobOracle() throws SQLException;
}
