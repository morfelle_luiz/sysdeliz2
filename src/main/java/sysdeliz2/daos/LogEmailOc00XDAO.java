/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.properties.LogEmailOc00X;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class LogEmailOc00XDAO extends FactoryConnection {

    public LogEmailOc00XDAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter = "";

    public ObservableList<LogEmailOc00X> getLogByOc(String numero) throws SQLException {
        ObservableList<LogEmailOc00X> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select ent.codcli,\n"
                + "       ent.nome,\n"
                + "       log.numero,\n"
                + "       mat.codigo,\n"
                + "       mat.descricao,\n"
                + "       log.cor,\n"
                + "       to_char(log.dt_envio,'DD/MM/YYYY HH24:MI:SS') dt_envio,\n"
                + "       log.usuario\n"
                + "  from sd_log_email_oc_001 log\n"
                + "  join entidade_001 ent\n"
                + "    on ent.codcli = log.codcli\n"
                + "  join material_001 mat\n"
                + "    on mat.codigo = log.codigo\n"
                + " where log.numero = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, numero);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new LogEmailOc00X(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

}
