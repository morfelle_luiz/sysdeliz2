/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Log;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class LogOracleDAO implements LogDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public void saveLog(Log p_log) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO SD_LOG_SYSDELIZ_001\n"
                + "(DATA_LOG, USUARIO, TELA, ACAO, REFERENCIA, DESCRICAO)\n"
                + "VALUES(SYSDATE, ?, ?, ?, ?, ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, p_log.getUsuario());
        preparedStatement.setString(2, p_log.getTela());
        preparedStatement.setString(3, p_log.getAcao());
        preparedStatement.setString(4, p_log.getReferencia());
        preparedStatement.setString(5, p_log.getDescricao());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void saveLogTI(Log p_log) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "INSERT INTO LOG \n"
                + "(USUARIO, DATA, HORA, TELA, DESCRICAO, EMPRESA, CHAVE, OPERACAO, FORMULARIO) \n"
                + "VALUES \n"
                + "(?, sysdate, current_timestamp, ?, ?, '_001' ,? , ?, ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, p_log.getUsuario());
        preparedStatement.setString(2, p_log.getTela());
        preparedStatement.setString(3, p_log.getDescricao());
        preparedStatement.setString(4, p_log.getReferencia());
        preparedStatement.setString(5, p_log.getAcao());
        preparedStatement.setString(6, p_log.getTela());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public String getLastLogJobOracle() throws SQLException {
        String last_log = null;

        StringBuilder query = new StringBuilder(
                "SELECT log.LOG_ID, job_name, job_class, operation, status, \n"
                        + "	TO_CHAR(log.LOG_DATE,'dd/MM/yyyy HH24:MI:SS') \"DATA_LOG\"\n"
                        + "FROM USER_SCHEDULER_JOB_LOG log \n"
                        + "WHERE job_name = 'J_SD_IMPORT_BID'\n"
                        + "ORDER BY LOG_ID DESC");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            last_log = resultSetSql.getString("DATA_LOG") + " :: " + resultSetSql.getString("STATUS");
        }

        closeConnection();
        resultSetSql.close();

        return last_log;
    }
}
