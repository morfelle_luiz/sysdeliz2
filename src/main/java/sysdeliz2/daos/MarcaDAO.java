/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Marca;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface MarcaDAO {

    public ObservableList<Marca> getAll() throws SQLException;

    public ObservableList<Marca> getByWindowsForm(String p_marca) throws SQLException;

    public ObservableList<Marca> getMarcasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;
}
