/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Marca;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class MarcaOracleDAO implements MarcaDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Marca> getAll() throws SQLException {
        ObservableList<Marca> marca = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO \n"
                        + "FROM MARCA_001");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            marca.add(new Marca(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO")));
        }
        closeConnection();
        resultSetSql.close();

        return marca;
    }

    @Override
    public ObservableList<Marca> getByWindowsForm(String p_marca) throws SQLException {
        ObservableList<Marca> marca = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT CODIGO, DESCRICAO \n"
                        + "FROM MARCA_001 \n"
                        + "WHERE CODIGO LIKE ? OR DESCRICAO LIKE ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + p_marca + "%");
        preparedStatement.setString(2, "%" + p_marca + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            marca.add(new Marca(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO")));
        }
        closeConnection();
        resultSetSql.close();

        return marca;
    }

    @Override
    public ObservableList<Marca> getMarcasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Marca> marcas = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from MARCA_001 \n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            marcas.add(new Marca(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO")));
        }
        resultSetSql.close();
        this.closeConnection();
        return marcas;
    }

}
