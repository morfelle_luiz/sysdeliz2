/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.MarketingPedido;
import sysdeliz2.models.ProdutosMarketingPedido;
import sysdeliz2.models.ProdutosReservaPedido;
import sysdeliz2.models.ReservaPedido;
import sysdeliz2.utils.AcessoSistema;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface MarketingPedidoDAO {

    public List<MarketingPedido> load() throws SQLException;

    public List<MarketingPedido> loadByForm(String pPedido, String pCliente, String pMarca) throws SQLException;

    public List<ProdutosMarketingPedido> loadProdutosByPedidoAndReserva(String pPedido) throws SQLException;

    public void updByMktPedido(String pPedido, String pReserva, List<ProdutosReservaPedido> pProdutos) throws SQLException;

    public void save(MarketingPedido pMktPedido, List<ProdutosMarketingPedido> pProdutosMktPedido, AcessoSistema usuario) throws SQLException;

    public void delete(ProdutosMarketingPedido pProdutoMktPedido, String pPedido) throws SQLException;

    public List<String> isClienteNovo(String pPedido, String pMarca) throws SQLException;

    public List<String> isEntregueMktUnicoCliente(String pPedido, String pMarca) throws SQLException;

    public List<String> isEntregueMktUnicoPedido(String pPedido, String pMarca) throws SQLException;

    public List<ProdutosReservaPedido> loadMktsClienteNovo(String pPedido, String pMarca) throws SQLException;

    public List<ProdutosReservaPedido> loadMktsUnicoCliente(String pPedido, String pMarca, String pGrupo) throws SQLException;

    public List<ProdutosReservaPedido> loadMktsUnicoPedido(String pPedido, String pMarca, String pGrupo) throws SQLException;

    public List<ProdutosReservaPedido> loadMktsCadastradoNoPedido(String pPedido) throws SQLException;

    public ProdutosMarketingPedido getProdutoMktPedido(String pPedido, String pCodigo) throws SQLException;

    public void updReservaByReservaPedido(ReservaPedido reserva) throws SQLException;
}
