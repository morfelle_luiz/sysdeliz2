/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import sysdeliz2.models.*;
import sysdeliz2.utils.AcessoSistema;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class MarketingPedidoOracleDAO implements MarketingPedidoDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    private Integer getQtdRegraSacola(String pPedido, String pMarca) throws SQLException {
        Integer returnValue = 0;
        StringBuilder query = new StringBuilder(""
                + "WITH REGRA_SACOLAS AS\n" +
                " (SELECT PED.NUMERO,\n" +
                "         REGEXP_REPLACE(MARC.DESCRICAO, 'MOOVE ', '') MARCA,\n" +
                "         ENT.SIT_CLI,\n" +
                "         SUM(CASE\n" +
                "               WHEN LIN.DESCRICAO NOT LIKE '%ESSENTIAL%' THEN\n" +
                "                PEDI.QTDE + PEDI.QTDE_F\n" +
                "               ELSE\n" +
                "                0\n" +
                "             END) QTDE_TOTAL,\n" +
                "         SUM(CASE\n" +
                "               WHEN LIN.DESCRICAO LIKE '%DENIM%' THEN\n" +
                "                PEDI.QTDE + PEDI.QTDE_F\n" +
                "               ELSE\n" +
                "                0\n" +
                "             END) QTDE_DENIM,\n" +
                "         SUM(CASE\n" +
                "               WHEN LIN.DESCRICAO LIKE '%COLLECTION%' OR\n" +
                "                    LIN.DESCRICAO LIKE '%CASUAL%' THEN\n" +
                "                PEDI.QTDE + PEDI.QTDE_F\n" +
                "               ELSE\n" +
                "                0\n" +
                "             END) QTDE_COLLECT_CASUAL,\n" +
                "         F_VERIFICA_DIVISOR((SUM(CASE\n" +
                "                                   WHEN LIN.DESCRICAO LIKE '%COLLECTION%' OR\n" +
                "                                        LIN.DESCRICAO LIKE '%CASUAL%' THEN\n" +
                "                                    PEDI.QTDE + PEDI.QTDE_F\n" +
                "                                   ELSE\n" +
                "                                    0\n" +
                "                                 END)),\n" +
                "                            SUM(CASE\n" +
                "                                  WHEN LIN.DESCRICAO NOT LIKE '%ESSENTIAL%' THEN\n" +
                "                                   PEDI.QTDE + PEDI.QTDE_F\n" +
                "                                  ELSE\n" +
                "                                   0\n" +
                "                                END)) * 100 PERC_COLLECT_CASUAL\n" +
                "    FROM PEDIDO_001 PED\n" +
                "   INNER JOIN PED_ITEN_001 PEDI\n" +
                "      ON (PEDI.NUMERO = PED.NUMERO)\n" +
                "   INNER JOIN PRODUTO_001 PROD\n" +
                "      ON (PROD.CODIGO = PEDI.CODIGO)\n" +
                "   INNER JOIN MARCA_001 MARC\n" +
                "      ON (MARC.CODIGO = PROD.MARCA)\n" +
                "   INNER JOIN TABLIN_001 LIN\n" +
                "      ON (LIN.CODIGO = PROD.LINHA)\n" +
                "   INNER JOIN ENTIDADE_001 ENT\n" +
                "      ON (ENT.CODCLI = PED.CODCLI)\n" +
                "   WHERE PED.NUMERO = ?\n" +
                "   GROUP BY PED.NUMERO,\n" +
                "            REGEXP_REPLACE(MARC.DESCRICAO, 'MOOVE ', ''),\n" +
                "            ENT.SIT_CLI)\n" +
                "SELECT NUMERO, MARCA, SIT_CLI, QTDE_TOTAL, QTDE_DENIM, QTDE_COLLECT_CASUAL, PERC_COLLECT_CASUAL, \n" +
                "       CASE WHEN QTDE_TOTAL >= 200 \n" +
                "            AND PERC_COLLECT_CASUAL >= case when marca = 'DLZ' \n" +
                "                then 0 \n" +
                "                else 20 end \n" +
                "            THEN 2 \n" +
                "            ELSE CASE WHEN QTDE_TOTAL >= 100 \n" +
                "                 AND PERC_COLLECT_CASUAL >= case when marca = 'DLZ' \n" +
                "                     then 0 \n" +
                "                     else 30 end \n" +
                "                 THEN 1 \n" +
                "                 ELSE 0 END END QTDE_SACOLAS\n" +
                "  FROM REGRA_SACOLAS\n" +
                " WHERE MARCA = ?");

        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pMarca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            returnValue = Integer.parseInt(resultSetSql.getString("QTDE_SACOLAS"));
        }
        resultSetSql.close();

        return returnValue;
    }

    @Override
    public List<MarketingPedido> load() throws SQLException {
        List<MarketingPedido> mkts = new ArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT 	PED.NUMERO, ENT.SIT_CLI, ENT.NOME CLIENTE, REP.NOME REPRESENTANTE, \n"
                + "		PED.DT_EMISSAO, TRIM(REGEXP_REPLACE(MAR.DESCRICAO,'(MOOVE|MASC|FEM)','')) MARCA, SUM(PEDI.QTDE) SALDO\n"
                + "FROM PEDIDO_001 PED\n"
                + "INNER JOIN PED_ITEN_001 PEDI ON (PEDI.NUMERO = PED.NUMERO AND PEDI.QTDE > 0)\n"
                + "INNER JOIN PRODUTO_001 PRD ON (PRD.CODIGO = PEDI.CODIGO)\n"
                + "INNER JOIN MARCA_001 MAR ON (MAR.CODIGO = PRD.MARCA)\n"
                + "INNER JOIN ENTIDADE_001 ENT ON (ENT.CODCLI = PED.CODCLI)\n"
                + "LEFT JOIN REPRESEN_001 REP ON (REP.CODREP = PED.CODREP)\n"
                + "GROUP BY PED.NUMERO, ENT.SIT_CLI, ENT.NOME, REP.NOME, PED.DT_EMISSAO, TRIM(REGEXP_REPLACE(MAR.DESCRICAO,'(MOOVE|MASC|FEM)',''))");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mkts.add(new MarketingPedido(resultSetSql.getString("NUMERO"), resultSetSql.getString("CLIENTE"),
                    resultSetSql.getString("REPRESENTANTE"), resultSetSql.getString("DT_EMISSAO"),
                    Integer.parseInt(resultSetSql.getString("SALDO")), resultSetSql.getString("SIT_CLI"), resultSetSql.getString("MARCA")));
        }
        resultSetSql.close();
        this.closeConnection();

        for (MarketingPedido mkt : mkts) {
            mkt.setListProdutos(
                    FXCollections.observableList(this.loadProdutosByPedidoAndReserva(mkt.getStrNumeroPedido())));
        }
        return mkts;
    }

    @Override
    public List<MarketingPedido> loadByForm(String pPedido, String pCliente, String pMarca) throws SQLException {
        List<MarketingPedido> mkts = new ArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT 	PED.NUMERO, ENT.SIT_CLI, ENT.NOME CLIENTE, REP.NOME REPRESENTANTE, \n"
                + "		PED.DT_EMISSAO, TRIM(REGEXP_REPLACE(MAR.DESCRICAO,'(MOOVE|MASC|FEM)','')) MARCA, SUM(PEDI.QTDE) SALDO\n"
                + "FROM PEDIDO_001 PED\n"
                + "INNER JOIN PED_ITEN_001 PEDI ON (PEDI.NUMERO = PED.NUMERO AND PEDI.QTDE > 0)\n"
                + "INNER JOIN PRODUTO_001 PRD ON (PRD.CODIGO = PEDI.CODIGO)\n"
                + "INNER JOIN MARCA_001 MAR ON (MAR.CODIGO = PRD.MARCA)\n"
                + "INNER JOIN ENTIDADE_001 ENT ON (ENT.CODCLI = PED.CODCLI)\n"
                + "LEFT JOIN REPRESEN_001 REP ON (REP.CODREP = PED.CODREP)\n"
                + "WHERE PED.NUMERO LIKE ? AND (ENT.NOME LIKE ? OR ENT.CODCLI LIKE ?) AND TRIM(REGEXP_REPLACE(MAR.DESCRICAO,'(MOOVE|MASC|FEM)','')) LIKE ?\n"
                + "GROUP BY PED.NUMERO, ENT.SIT_CLI, ENT.NOME, REP.NOME, PED.DT_EMISSAO, TRIM(REGEXP_REPLACE(MAR.DESCRICAO,'(MOOVE|MASC|FEM)',''))");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "%" + pPedido.trim());
        preparedStatement.setString(2, "%" + pCliente.trim() + "%");
        preparedStatement.setString(3, "%" + pCliente.trim() + "%");
        preparedStatement.setString(4, "%" + pMarca.trim() + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mkts.add(new MarketingPedido(resultSetSql.getString("NUMERO"), resultSetSql.getString("CLIENTE"),
                    resultSetSql.getString("REPRESENTANTE"), resultSetSql.getString("DT_EMISSAO"),
                    Integer.parseInt(resultSetSql.getString("SALDO")), resultSetSql.getString("SIT_CLI"), resultSetSql.getString("MARCA")));
        }
        resultSetSql.close();
        this.closeConnection();

        for (MarketingPedido mkt : mkts) {
            mkt.setListProdutos(
                    FXCollections.observableList(this.loadProdutosByPedidoAndReserva(mkt.getStrNumeroPedido())));
        }
        return mkts;
    }

    @Override
    public List<ProdutosMarketingPedido> loadProdutosByPedidoAndReserva(String pPedido) throws SQLException {
        List<ProdutosMarketingPedido> produtos = new ArrayList();

        StringBuilder query = new StringBuilder(""
                + "SELECT PROD.CODIGO, PROD.DESCRICAO, MKT.DATA_CAD, MKT.USUARIO, MKT.QTDE, MKT.OBS, MKT.RESERVA,\n"
                + "' ' STATUS\n"
                + "FROM SD_MKT_PEDIDO_001 MKT\n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "LEFT JOIN SD_CAIXA_RESERVA_PEDIDO_001 CAI ON (CAI.CODIGO = PROD.CODIGO AND CAI.PEDIDO = MKT.PEDIDO)\n"
                + "WHERE MKT.PEDIDO = ?\n"
                + "GROUP BY PROD.CODIGO, PROD.DESCRICAO, MKT.DATA_CAD, MKT.USUARIO, MKT.QTDE, MKT.OBS, MKT.RESERVA");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {

            produtos.add(
                    new ProdutosMarketingPedido(resultSetSql.getString("CODIGO"), resultSetSql.getString("RESERVA"),
                            resultSetSql.getString("STATUS"), resultSetSql.getString("USUARIO"),
                            resultSetSql.getString("DATA_CAD"), resultSetSql.getString("DESCRICAO"),
                            resultSetSql.getString("OBS"), Integer.parseInt(resultSetSql.getString("QTDE"))));
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public void updByMktPedido(String pPedido, String pReserva, List<ProdutosReservaPedido> pProdutos)
            throws SQLException {
        StringBuilder query = new StringBuilder(
                "" + "UPDATE SD_MKT_PEDIDO_001 \n" + "SET RESERVA = ? \n" + "WHERE PEDIDO = ? AND CODIGO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        for (ProdutosReservaPedido produto : pProdutos) {
            preparedStatement.setString(1, pReserva);
            preparedStatement.setString(2, pPedido);
            preparedStatement.setString(3, produto.getStrCodigo());
            preparedStatement.execute();
        }

        this.closeConnection();
    }

    @Override
    public void updReservaByReservaPedido(ReservaPedido reserva) throws SQLException {
        StringBuilder query = new StringBuilder(
                "" + "UPDATE SD_MKT_PEDIDO_001 \n" + "SET RESERVA = '0' \n" + "WHERE PEDIDO = ? AND RESERVA = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroReserva());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void save(MarketingPedido pMktPedido, List<ProdutosMarketingPedido> pProdutosMktPedido,
            AcessoSistema usuario) throws SQLException {
        StringBuilder query = new StringBuilder("" + "INSERT INTO SD_MKT_PEDIDO_001\n"
                + "(PEDIDO, CODIGO, QTDE, DATA_CAD, USUARIO, RESERVA, OBS)\n" + "VALUES (?, ?, ?, SYSDATE, ?, '0', ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());

        for (ProdutosMarketingPedido produtoMarketingPedido : pProdutosMktPedido) {
            preparedStatement.setString(1, pMktPedido.getStrNumeroPedido());
            preparedStatement.setString(2, produtoMarketingPedido.getStrCodigo());
            preparedStatement.setInt(3, Integer.parseInt(produtoMarketingPedido.getIntQtde()));
            preparedStatement.setString(4, usuario.getUsuario());
            preparedStatement.setString(5, produtoMarketingPedido.getStrObs());
            preparedStatement.execute();
        }

        this.closeConnection();
    }

    @Override
    public void delete(ProdutosMarketingPedido pProdutoMktPedido, String pPedido) throws SQLException {
        StringBuilder query = new StringBuilder(
                "" + "DELETE FROM SD_MKT_PEDIDO_001\n" + "WHERE CODIGO = ? AND PEDIDO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pProdutoMktPedido.getStrCodigo());
        preparedStatement.setString(2, pPedido);
        preparedStatement.execute();
        this.closeConnection();
    }

    @Override
    public List<String> isClienteNovo(String pPedido, String pMarca) throws SQLException {
        List<String> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder("" + "SELECT COUNT(*) IS_NEW\n" + "FROM PEDIDO_001 PED \n"
                + "INNER JOIN ENTIDADE_001 CLI ON CLI.CODCLI = PED.CODCLI\n" + "WHERE PED.NUMERO = ?\n"
                + "      AND CLI.DATA_CAD >= TO_DATE('16/04/2018') AND CLI.SIT_CLI = '1' \n"
                + "      AND CLI.ATIVO = 'S' AND CLI.TIPO_ENTIDADE IN ('C','CF')");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            if (Integer.parseInt(resultSetSql.getString("IS_NEW")) == 0) {
                returnValue.add("NAO");
            } else {
                returnValue.add("SIM");
            }
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override // INATIVO
    public List<String> isEntregueMktUnicoCliente(String pPedido, String pMarca) throws SQLException {
        List<String> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder("" + "SELECT RESP.PEDIDO, RESP.RESERVA, \n"
                + "	SUM(CASE WHEN MAR.DESCRICAO = 'DLZ' THEN 1 ELSE 0 END) HAS_DLZ, \n"
                + "	SUM(CASE WHEN MAR.DESCRICAO = 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_FLOR,\n"
                + "	SUM(CASE WHEN MAR.DESCRICAO <> 'DLZ' AND MARCA <> 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_OUTROS\n"
                + "FROM SD_RESERVA_PEDIDO_001 RESP\n" + "INNER JOIN PEDIDO_001 PED ON (PED.NUMERO = RESP.PEDIDO)\n"
                + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = RESP.CODIGO)\n"
                + "INNER JOIN MARCA_001 MAR ON (MAR.CODIGO = PROD.MARCA)\n"
                + "WHERE PED.CODCLI = (SELECT PED1.CODCLI FROM PEDIDO_001 PED1 WHERE PED1.NUMERO = ?)\n"
                + "	  AND RESP.CODIGO IN (SELECT MKT.CODIGO FROM SD_REGRAMKT_001 MKT WHERE MARCA = ? AND UN_CLI IS NOT NULL /**AND MKT.COLECAO = (SELECT PED.COLECAO FROM PEDIDO_001 PED WHERE PED.NUMERO = 0000056198)**/)\n"
                + "GROUP BY RESP.PEDIDO, RESP.RESERVA");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pMarca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            returnValue.add(resultSetSql.getString("PEDIDO"));
            returnValue.add(resultSetSql.getString("RESERVA"));
            if (Integer.parseInt(resultSetSql.getString("HAS_DLZ")) > 0) {
                returnValue.add("DLZ");
            }
            if (Integer.parseInt(resultSetSql.getString("HAS_FLOR")) > 0) {
                returnValue.add("FLOR DE LIS");
            }
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override // INATIVO
    public List<String> isEntregueMktUnicoPedido(String pPedido, String pMarca) throws SQLException {
        List<String> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder("" + "SELECT RESP.PEDIDO, RESP.RESERVA, \n"
                + "	SUM(CASE WHEN MAR.DESCRICAO = 'DLZ' THEN 1 ELSE 0 END) HAS_DLZ, \n"
                + "	SUM(CASE WHEN MAR.DESCRICAO = 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_FLOR,\n"
                + "	SUM(CASE WHEN MAR.DESCRICAO <> 'DLZ' AND MARCA <> 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_OUTROS\n"
                + "FROM SD_RESERVA_PEDIDO_001 RESP\n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = RESP.CODIGO)\n"
                + "INNER JOIN MARCA_001 MAR ON (MAR.CODIGO = PROD.MARCA)\n" + "WHERE RESP.PEDIDO = ?\n"
                + "	  AND RESP.CODIGO IN (SELECT MKT.CODIGO FROM SD_REGRAMKT_001 MKT WHERE MARCA = ? AND UN_PED IS NOT NULL /**AND MKT.COLECAO = (SELECT PED.COLECAO FROM PEDIDO_001 PED WHERE PED.NUMERO = '0000056182')**/)\n"
                + "GROUP BY RESP.PEDIDO, RESP.RESERVA");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pMarca);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            returnValue.add(resultSetSql.getString("PEDIDO"));
            returnValue.add(resultSetSql.getString("RESERVA"));
            if (Integer.parseInt(resultSetSql.getString("HAS_DLZ")) > 0) {
                returnValue.add("DLZ");
            }
            if (Integer.parseInt(resultSetSql.getString("HAS_FLOR")) > 0) {
                returnValue.add("FLOR DE LIS");
            }
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override
    public List<ProdutosReservaPedido> loadMktsClienteNovo(String pPedido, String pMarca) throws SQLException {
        List<ProdutosReservaPedido> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder("" + "SELECT MKT.CODIGO, PROD.DESCRICAO, MKT.MARCA, MKT.QTDE,\n"
                + "CASE WHEN PAI.DEPOSITO IS NULL THEN 'N/D' ELSE CASE WHEN PAI.QUANTIDADE <= 0 THEN 'SEM ESTOQUE' ELSE PAI.DEPOSITO END END DEPOSITO,\n"
                + "CASE WHEN PAI.\"LOCAL\" IS NULL THEN 'N/D' ELSE PAI.\"LOCAL\" END \"LOCAL\"\n"
                + "FROM SD_REGRAMKT_001 MKT \n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = MKT.CODIGO)\n" + "WHERE MKT.MARCA = ?\n"
                + "	  AND MKT.CLI_NOVO IS NOT NULL\n" + "	  AND PAI.DEPOSITO = '0005'\n"
                + "	  AND PAI.QUANTIDADE > 0	  \n"
                + "	  AND MKT.CODIGO NOT IN (SELECT RES.CODIGO FROM SD_RESERVA_PEDIDO_001 RES \n"
                + "	  				INNER JOIN PEDIDO_001 PED ON PED.NUMERO = RES.PEDIDO\n"
                + "	  				WHERE PED.CODCLI IN (SELECT PED2.CODCLI FROM PEDIDO_001 PED2 WHERE PED2.NUMERO = ?))");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pMarca);
        preparedStatement.setString(2, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            if (resultSetSql.getString("CODIGO").equals("MKTD003")
                    || resultSetSql.getString("CODIGO").equals("MKTF003")) {
                Integer qtdSacolas = this.getQtdRegraSacola(pPedido, pMarca);
                if (qtdSacolas > 0) {
                    returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN", qtdSacolas,
                            0, resultSetSql.getString("LOCAL"), resultSetSql.getString("DEPOSITO")));
                }
                continue;
            }
            returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN",
                    Integer.parseInt(resultSetSql.getString("QTDE")), 0, resultSetSql.getString("LOCAL"),
                    resultSetSql.getString("DEPOSITO")));
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    private Integer executeRegraEspecifica(String sqlRegra) throws SQLException {
        Integer returnValue = 0;
        StringBuilder query = new StringBuilder(sqlRegra);

        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            returnValue = resultSetSql.getInt(1);
        }
        resultSetSql.close();

        return returnValue;
    }

    @Override
    public List<ProdutosReservaPedido> loadMktsUnicoCliente(String pPedido, String pMarca, String pGrupo)
            throws SQLException {
        List<ProdutosReservaPedido> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder(""
                + "SELECT MKT.CODIGO, PROD.DESCRICAO, MKT.MARCA, MKT.QTDE,\n"
                + "CASE WHEN PAI.DEPOSITO IS NULL THEN 'N/D' ELSE CASE WHEN PAI.QUANTIDADE <= 0 THEN 'SEM ESTOQUE' ELSE PAI.DEPOSITO END END DEPOSITO,\n"
                + "CASE WHEN PAI.\"LOCAL\" IS NULL THEN 'N/D' ELSE PAI.\"LOCAL\" END \"LOCAL\",\n"
                + "MKT.OBS_REGRA\n"
                + "FROM SD_REGRAMKT_001 MKT \n"
                + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = MKT.CODIGO)\n"
                + "WHERE MKT.MARCA = ?\n"
                + "	  AND MKT.SIT_CLI = ?\n"
                + "	  AND MKT.UN_CLI IS NOT NULL\n"
                + "	  AND MKT.EMAIL IS NULL\n"
                + "	  AND PAI.DEPOSITO = '0005'\n"
                + "	  AND PAI.QUANTIDADE > 0\n"
                + "	  AND MKT.CODIGO NOT IN (SELECT RES.CODIGO FROM SD_RESERVA_PEDIDO_001 RES \n"
                + "	  				INNER JOIN PEDIDO_001 PED ON PED.NUMERO = RES.PEDIDO\n"
                + "                                     WHERE PED.CODCLI IN (SELECT PED2.CODCLI FROM PEDIDO_001 PED2 WHERE PED2.NUMERO = ?))");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pMarca);
        preparedStatement.setString(2, pGrupo);
        preparedStatement.setString(3, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            if (resultSetSql.getString("CODIGO").equals("MKTD003")
                    || resultSetSql.getString("CODIGO").equals("MKTF003")) {
                Integer qtdSacolas = this.getQtdRegraSacola(pPedido, pMarca);
                if (qtdSacolas > 0) {
                    returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN", qtdSacolas,
                            0, resultSetSql.getString("LOCAL"), resultSetSql.getString("DEPOSITO")));
                }
                continue;
            }
            Integer qtdeMkt = resultSetSql.getInt("QTDE");
            if (resultSetSql.getString("OBS_REGRA") != null) {
                String sqlSubRegra = resultSetSql.getString("OBS_REGRA")
                        .replace("?PEDIDO", pPedido);
                qtdeMkt = qtdeMkt * this.executeRegraEspecifica(sqlSubRegra);
            }
            if (qtdeMkt > 0) {
                returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN",
                        qtdeMkt, 0, resultSetSql.getString("LOCAL"),
                        resultSetSql.getString("DEPOSITO")));
            }
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override
    public List<ProdutosReservaPedido> loadMktsUnicoPedido(String pPedido, String pMarca, String pGrupo)
            throws SQLException {
        List<ProdutosReservaPedido> returnValue = new ArrayList<>();

        if (pGrupo == "CN") {
            pGrupo = "G5";
        }

        StringBuilder query = new StringBuilder("" + "SELECT MKT.CODIGO, PROD.DESCRICAO, MKT.MARCA, MKT.QTDE, \n"
                + "CASE WHEN PAI.DEPOSITO IS NULL THEN 'N/D' ELSE CASE WHEN PAI.QUANTIDADE <= 0 THEN 'SEM ESTOQUE' ELSE PAI.DEPOSITO END END DEPOSITO,\n"
                + "CASE WHEN PAI.\"LOCAL\" IS NULL THEN 'N/D' ELSE PAI.\"LOCAL\" END \"LOCAL\"\n"
                + "FROM SD_REGRAMKT_001 MKT \n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = MKT.CODIGO)\n" + "WHERE MKT.MARCA = ?\n"
                + "	  AND MKT.SIT_CLI = ?\n" + "	  AND MKT.UN_PED IS NOT NULL\n" + "	  AND MKT.EMAIL IS NULL\n"
                + "	  AND PAI.DEPOSITO = '0005'\n" + "	  AND PAI.QUANTIDADE > 0\n"
                + "	  AND MKT.CODIGO NOT IN (SELECT RES.CODIGO FROM SD_RESERVA_PEDIDO_001 RES \n"
                + "	  				WHERE RES.PEDIDO = ?)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pMarca);
        preparedStatement.setString(2, pGrupo);
        preparedStatement.setString(3, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            if (resultSetSql.getString("CODIGO").equals("MKTD003")
                    || resultSetSql.getString("CODIGO").equals("MKTF003")) {
                Integer qtdSacolas = this.getQtdRegraSacola(pPedido, pMarca);
                if (qtdSacolas > 0) {
                    returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN", qtdSacolas,
                            0, resultSetSql.getString("LOCAL"), resultSetSql.getString("DEPOSITO")));
                }
                continue;
            }
            returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN",
                    Integer.parseInt(resultSetSql.getString("QTDE")), 0, resultSetSql.getString("LOCAL"),
                    resultSetSql.getString("DEPOSITO")));
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override
    public List<ProdutosReservaPedido> loadMktsCadastradoNoPedido(String pPedido) throws SQLException {
        List<ProdutosReservaPedido> returnValue = new ArrayList<>();
        StringBuilder query = new StringBuilder(""
                + "SELECT PROD.CODIGO, PROD.DESCRICAO, MKT.DATA_CAD, MKT.USUARIO, MKT.QTDE, MKT.OBS, \n"
                + "CASE WHEN PAI.DEPOSITO IS NULL THEN 'N/D' ELSE CASE WHEN PAI.QUANTIDADE <= 0 THEN 'SEM ESTOQUE' ELSE PAI.DEPOSITO END END DEPOSITO, \n"
                + "CASE WHEN PAI.\"LOCAL\" IS NULL THEN 'N/D' ELSE PAI.\"LOCAL\" END \"LOCAL\"\n"
                + "FROM SD_MKT_PEDIDO_001 MKT\n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = MKT.CODIGO)\n"
                + "WHERE MKT.PEDIDO IN ('"+pPedido+"') AND MKT.RESERVA = '0' AND PAI.DEPOSITO = '0005'\n"
                + "       AND PAI.QUANTIDADE > 0");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        //preparedStatement.setString(1, pPedido);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            returnValue.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), "UN", "UN",
                    Integer.parseInt(resultSetSql.getString("QTDE")), 0, resultSetSql.getString("LOCAL"),
                    resultSetSql.getString("DEPOSITO")));
        }
        resultSetSql.close();
        this.closeConnection();

        return returnValue;
    }

    @Override
    public ProdutosMarketingPedido getProdutoMktPedido(String pPedido, String pCodigo) throws SQLException {
        ProdutosMarketingPedido produto = new ProdutosMarketingPedido();

        StringBuilder query = new StringBuilder(
                "" + "SELECT PROD.CODIGO, PROD.DESCRICAO, MKT.DATA_CAD, MKT.USUARIO, MKT.QTDE, MKT.OBS \n"
                + "FROM SD_MKT_PEDIDO_001 MKT\n" + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = MKT.CODIGO)\n"
                + "WHERE MKT.PEDIDO IN ('"+pPedido+"') AND PROD.CODIGO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, pCodigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            produto.setIntQtde(Integer.parseInt(resultSetSql.getString("QTDE")));
            produto.setStrCodigo(resultSetSql.getString("CODIGO"));
            produto.setStrDataCad(resultSetSql.getString("DATA_CAD"));
            produto.setStrDescProduto(resultSetSql.getString("DESCRICAO"));
            produto.setStrObs(resultSetSql.getString("OBS"));
            produto.setStrUsuario(resultSetSql.getString("USUARIO"));
        }
        resultSetSql.close();
        this.closeConnection();
        return produto;
    }

}
