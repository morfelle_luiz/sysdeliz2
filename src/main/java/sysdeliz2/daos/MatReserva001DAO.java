/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.MatReserva001;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class MatReserva001DAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    public ObservableList<MatReserva001> getReservasOf(String numeroOf) throws SQLException {
        ObservableList<MatReserva001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from mat_reserva_001 \n"
                + " where numero = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numeroOf);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new MatReserva001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getInt(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getString(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13)));
        }

        resultSetSql.close();

        return rows;
    }

    public Boolean isReservaOf(String numeroOf) throws SQLException {
        ObservableList<MatReserva001> materiais = getReservasOf(numeroOf);

        if (materiais.stream().filter(material -> material.getQtde() > 0).count() > 0) {
            return true;
        }

        return false;
    }
}
