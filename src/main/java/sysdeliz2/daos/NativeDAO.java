package sysdeliz2.daos;

import com.github.vertical_blank.sqlformatter.SqlFormatter;
import sysdeliz2.models.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NativeDAO {

    // Variaveis locais
    private static Connection connection = null;
    private static Statement statement = null;
    private static CallableStatement callStatement = null;
    private static PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    public static void closeConnection() throws SQLException, SQLRecoverableException {
        if (preparedStatement != null)
            preparedStatement.close();
        if (statement != null)
            statement.close();
        if (callStatement != null)
            callStatement.close();
        connection.commit();
        connection.close();
    }

    public List<Object> runNativeQuery(String query, Object... args) throws SQLException {
        return runNativeQuery(String.format(query, args));
    }

    public List<Object> runNativeQuery(String query) throws SQLException {
        System.out.println("Native Query:\n" + SqlFormatter.of("pl/sql").format(query));

        List<Object> values = new ArrayList<>();
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        ResultSetMetaData rsmd = resultSetSql.getMetaData();
        while (resultSetSql.next()) {
            Map<String, Object> value = new HashMap<>();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                value.put(rsmd.getColumnName(i), resultSetSql.getObject(i));
            }
            values.add(value);
        }
        resultSetSql.close();
        closeConnection();
        return values;
    }

    public void runNativeQueryUpdate(String query, Object... args) throws SQLException {
        runNativeQueryUpdate(String.format(query, args));
    }

    public void runNativeQueryUpdate(String query) throws SQLException {
        System.out.println("Native Update Query:\n" + SqlFormatter.of("pl/sql").format(query));

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        closeConnection();
    }

    public void runNativeQueryProcedure(String query, Object... args) throws SQLException {
        runNativeQueryProcedure(String.format(query, args));
    }

    /**
     * Para a query de parâmetro usar: <i>nome_da_procedure(parametros)</i>
     *
     * @param query
     * @throws SQLException
     */
    public void runNativeQueryProcedure(String query) throws SQLException {
        System.out.println("Native Update Query:\n" + SqlFormatter.of("pl/sql").format(query));

        connection = ConnectionFactory.getEmpresaConnection();
        callStatement = connection.prepareCall("{call " + query + "}");
        callStatement.execute();
        closeConnection();
    }

    public Object runNativeQueryFunction(Integer returnSqlType, String query, Object... args) throws SQLException {
        return runNativeQueryFunction(String.format(query, args), returnSqlType);
    }

    /**
     * Para a query de parâmetro usar: <i>nome_da_function(parametros)</i>
     *
     * @param query
     * @param returnSqlType (USE ENUM <i>Types</i>)
     * @return
     * @throws SQLException
     */
    public Object runNativeQueryFunction(String query, Integer returnSqlType) throws SQLException {
        Object objectReturn = null;
        connection = ConnectionFactory.getEmpresaConnection();
        callStatement = connection.prepareCall("{? = call " + query + "}");
        callStatement.registerOutParameter(1, returnSqlType);
        callStatement.execute();

        objectReturn = callStatement.getString(1);
        closeConnection();

        return objectReturn;
    }

    public NativeDAO createConnection() throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        connection.setAutoCommit(false);

        return this;
    }

    public void runNativeQueryUpdateWithouCommit(String query) throws SQLException {
        System.out.println("Native Update Query:\n" + SqlFormatter.of("pl/sql").format(query));

        if (connection == null) {
            connection = ConnectionFactory.getEmpresaConnection();
            statement = connection.createStatement();
            connection.setAutoCommit(false);
        }

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
    }

    public void rollBackTransaction() throws SQLException {
        if (preparedStatement != null)
            preparedStatement.close();
        if (statement != null)
            statement.close();
        if (callStatement != null)
            callStatement.close();
        connection.rollback();
        connection.close();
    }

    public void runNativeQueryUpdateWithouCommit(String query, Object... args) throws SQLException {
        System.out.println("Native Update Query:\n" + SqlFormatter.of("pl/sql").format(query));

        if (connection == null) {
            connection = ConnectionFactory.getEmpresaConnection();
            statement = connection.createStatement();
            connection.setAutoCommit(false);
        }

        preparedStatement = connection.prepareStatement(String.format(query, args));
        preparedStatement.execute();
    }

}
