/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Of1001;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class Of1001DAO {
// Variáveis locais

    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    public Of1001 getOfByNumero(String numero) throws SQLException {
        Of1001 of1 = null;

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from of1_001\n"
                + " where numero = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            of1 = new Of1001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getInt(10),
                    resultSetSql.getString(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13),
                    resultSetSql.getString(14),
                    resultSetSql.getInt(15),
                    resultSetSql.getInt(16),
                    resultSetSql.getInt(17),
                    resultSetSql.getString(18),
                    resultSetSql.getInt(19),
                    resultSetSql.getString(20),
                    resultSetSql.getString(21),
                    resultSetSql.getString(22),
                    resultSetSql.getInt(23),
                    resultSetSql.getString(24));
        }
        resultSetSql.close();
        this.closeConnection();

        if (of1 != null) {
            of1.setGrade(DAOFactory.getOfIten001DAO().getGradeOf(numero));
        }

        return of1;
    }

    public Of1001 getOfByCodigoWithoutDb(String codigo) throws SQLException {
        Of1001 of1 = null;
        of1 = new Of1001(0, null, null, null, null, "0", codigo, null, null, null, null, null, null, null, 0, 0, 0, null, 0, null, null, null, 0, null,
                DAOFactory.getOfIten001DAO().getGradeProduto(codigo));

        return of1;
    }

}
