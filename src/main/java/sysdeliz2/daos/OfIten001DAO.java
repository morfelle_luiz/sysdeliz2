/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.OfIten001;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class OfIten001DAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    private ObservableMap<String, Integer> getSaldosTamanhos(String codigoPrd, String corRef, String of) throws SQLException {
        ObservableMap<String, Integer> mapTamanhos = FXCollections.observableHashMap();

        StringBuilder query = new StringBuilder(
                "select prod.tam, nvl(ofi.qtde, 0) qtde\n"
                        + "  from vproduto_cortam prod\n"
                        + "  join produto_001 prd\n"
                        + "    on prd.codigo = prod.codigo\n"
                        + "  join faixa_iten_001 fai\n"
                        + "    on fai.tamanho = prod.tam\n"
                        + "   and fai.faixa = prd.faixa\n"
                        + "  left join of1_001 of1\n"
                        + "    on of1.numero = ?\n"
                        + "   and of1.codigo = prod.codigo\n"
                        + "  left join of_iten_001 ofi\n"
                        + "    on ofi.numero = of1.numero\n"
                        + "   and ofi.cor = prod.cor\n"
                        + "   and ofi.tam = prod.tam\n"
                        + " where prod.codigo = ?\n"
                        + "   and prod.cor = ?\n"
                        + " order by fai.posicao");

        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, of);
        preparedStatement.setString(2, codigoPrd);
        preparedStatement.setString(3, corRef);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getString("tam"),
                    resultSetSql.getInt("qtde"));
        }

        resultSetSql.close();

        return mapTamanhos;
    }

    private ObservableMap<Integer, String> getHeaderTamanhosRef(String codigoPrd, String corRef) throws SQLException {
        ObservableMap<Integer, String> mapTamanhos = FXCollections.observableHashMap();

        StringBuilder query = new StringBuilder(""
                + "select vct.tam, fai.posicao\n"
                + "  from produto_001 prd\n"
                + "  join vproduto_cortam vct\n"
                + "    on vct.codigo = prd.codigo\n"
                + "  join faixa_iten_001 fai\n"
                + "    on fai.faixa = prd.faixa\n"
                + "   and fai.tamanho = vct.tam\n"
                + " where prd.codigo = ?\n"
                + "   and vct.cor = ?\n"
                + " order by fai.posicao");

        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigoPrd);
        preparedStatement.setString(2, corRef);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getInt("posicao"), resultSetSql.getString("tam"));
        }

        resultSetSql.close();

        return mapTamanhos;
    }

    public ObservableList<OfIten001> getGradeOf(String numero) throws SQLException {
        ObservableList<OfIten001> grade = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select distinct of1.numero, of1.codigo, ofi.cor, mat.composicao\n"
                + "  from of1_001 of1\n"
                + "  join of_iten_001 ofi on ofi.numero = of1.numero\n\n"
                + "  join pcpftof_001 pcp on pcp.numero = of1.numero and pcp.cor = ofi.cor and pcp.cor_i <> '*****'\n"
                + "  join cadfluxo_001 flux on flux.codigo = pcp.setor and flux.sd_grupo = '103'\n"
                + "  join pcpapl_001 apl on apl.codigo = pcp.aplicacao and apl.tipo = '717'\n"
                + "  join material_001 mat on mat.codigo = pcp.insumo\n"
                + " where of1.numero = ?\n"
                + " order by (select sum(ofi1.qtde) from of_iten_001 ofi1 where ofi1.numero = of1.numero and ofi1.cor = ofi.cor) desc");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            grade.add(new OfIten001(resultSetSql.getString("numero"),
                    resultSetSql.getString("cor"),
                    resultSetSql.getString("composicao"),
                    this.getSaldosTamanhos(resultSetSql.getString("codigo"), resultSetSql.getString("cor"), resultSetSql.getString("numero")),
                    this.getHeaderTamanhosRef(resultSetSql.getString("codigo"), resultSetSql.getString("cor"))));
        }
        resultSetSql.close();
        this.closeConnection();
        return grade;
    }

    public ObservableList<OfIten001> getGradeOfSemCorte(String numero) throws SQLException {
        ObservableList<OfIten001> grade = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select distinct of1.numero, of1.codigo, ofi.cor, mat.composicao\n"
                + "  from of1_001 of1\n"
                + "  join of_iten_001 ofi on ofi.numero = of1.numero\n\n"
                + "  join pcpftof_001 pcp on pcp.numero = of1.numero and pcp.cor = ofi.cor and pcp.cor_i <> '*****'\n"
                + "  join pcpapl_001 apl on apl.codigo = pcp.aplicacao and apl.tipo = '717'\n"
                + "  join material_001 mat on mat.codigo = pcp.insumo\n"
                + " where of1.numero = ?\n"
                + " order by (select sum(ofi1.qtde) from of_iten_001 ofi1 where ofi1.numero = of1.numero and ofi1.cor = ofi.cor) desc");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            grade.add(new OfIten001(resultSetSql.getString("numero"),
                    resultSetSql.getString("cor"),
                    resultSetSql.getString("composicao"),
                    this.getSaldosTamanhos(resultSetSql.getString("codigo"), resultSetSql.getString("cor"), resultSetSql.getString("numero")),
                    this.getHeaderTamanhosRef(resultSetSql.getString("codigo"), resultSetSql.getString("cor"))));
        }
        resultSetSql.close();
        this.closeConnection();
        return grade;
    }

    public ObservableList<OfIten001> getGradeProduto(String codigo) throws SQLException {
        ObservableList<OfIten001> grade = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select distinct '0' numero, prd.codigo, cor.cor\n"
                + "  from produto_001 prd\n"
                + "  join vproduto_cortam cor\n"
                + "    on prd.codigo = cor.codigo\n"
                + " where prd.codigo = ?\n"
                + " order by (select sum(ofi1.qtde) from of_iten_001 ofi1 where ofi1.numero = of1.numero and ofi1.cor = ofi.cor) desc");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, codigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            grade.add(new OfIten001(resultSetSql.getString("numero"),
                    resultSetSql.getString("cor"),
                    this.getSaldosTamanhos(resultSetSql.getString("codigo"), resultSetSql.getString("cor"), "0"),
                    this.getHeaderTamanhosRef(resultSetSql.getString("codigo"), resultSetSql.getString("cor"))));
        }
        resultSetSql.close();
        this.closeConnection();
        return grade;
    }

}
