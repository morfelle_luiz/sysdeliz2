/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableMap;
import sysdeliz2.models.Pedido;
import sysdeliz2.models.table.TableManutencaoPedidosPedido;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface PedidoDAO {

    public Pedido getPedido(String numero) throws SQLException;

    public List<TableManutencaoPedidosPedido> getPedidosManutencao(String codigo, String listCor, String additionalWhere) throws SQLException;

    public void cancelRefPedido(String numero, String codigo, String cor) throws SQLException;

    public Integer trocarRefPedido(String numero, String codigo, String cor, String novoCodigo, ObservableMap<String, Integer> tams) throws SQLException;

    public void reservarRefPedido(String numero, String codigo, String cor, String tam, Integer qtde) throws SQLException;

    public String getReservaReferencia(String numero, String codigo, String cor) throws SQLException;

    public Integer alterarGradePedido(String numero, String codigo, String cor, String tam, Integer alteracao, String motivoCancelamento) throws SQLException;

}
