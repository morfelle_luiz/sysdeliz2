package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.Periodo;
import sysdeliz2.models.properties.GestaoDeLote;

import java.sql.SQLException;
import java.util.List;

public interface PeriodoDAO {

    public List<Periodo> loadAtivosAndTipoP() throws SQLException;

    public GestaoDeLote getByPrazo(String prazo) throws SQLException;

    public ObservableList<Periodo> getPeriodoFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;
}
