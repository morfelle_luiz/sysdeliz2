/**
 *
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Periodo;
import sysdeliz2.models.properties.GestaoDeLote;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cristiano.diego
 *
 */
public class PeriodoOracleDAO implements PeriodoDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public List<Periodo> loadAtivosAndTipoP() throws SQLException {
        List<Periodo> periodos = new ArrayList<>();

        StringBuilder query = new StringBuilder(""
                + "SELECT DISTINCT PRAZO, DESCRICAO FROM TABPRZ_001 \n"
                + "WHERE ATIVO = 'S' \n"
                + "ORDER BY PRAZO");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            periodos.add(new Periodo(resultSetSql.getString("PRAZO"), resultSetSql.getString("DESCRICAO")));
        }
        closeConnection();
        resultSetSql.close();

        return periodos;
    }

    @Override
    public GestaoDeLote getByPrazo(String prazo) throws SQLException {
        GestaoDeLote periodo = null;

        StringBuilder query = new StringBuilder(""
                + "SELECT DISTINCT PRAZO, DT_INICIO, DT_FIM FROM TABPRZ_001 \n"
                + "WHERE ATIVO = 'S' AND PRAZO = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, prazo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            periodo = new GestaoDeLote("0", resultSetSql.getString("PRAZO"), resultSetSql.getString("DT_INICIO"), resultSetSql.getString("DT_FIM"), "100", 0, null, null);
        }
        closeConnection();
        resultSetSql.close();

        return periodo;
    }

    @Override
    public ObservableList<Periodo> getPeriodoFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Periodo> periodos = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select distinct prazo, descricao\n"
                + "  from tabprz_001\n"
                + " where ativo = 'S'\n"
                + "   and" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            periodos.add(new Periodo(resultSetSql.getString("PRAZO"), resultSetSql.getString("DESCRICAO")));
        }
        closeConnection();
        resultSetSql.close();

        return periodos;
    }

}
