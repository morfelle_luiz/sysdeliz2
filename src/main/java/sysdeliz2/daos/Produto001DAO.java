/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.Produto001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class Produto001DAO extends FactoryConnection {

    public Produto001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public Produto001 getByCode(String codigo) throws SQLException {
        Produto001 row = null;

        StringBuilder query = new StringBuilder("select * from produto_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, codigo);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new Produto001(
                    resultSetSql.getDouble(1),
                    resultSetSql.getDouble(2),
                    resultSetSql.getDouble(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getString(7),
                    resultSetSql.getDouble(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getInt(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16),
                    resultSetSql.getString(17),
                    resultSetSql.getString(18),
                    resultSetSql.getDouble(19),
                    resultSetSql.getDouble(20),
                    resultSetSql.getDouble(21),
                    resultSetSql.getDouble(22),
                    resultSetSql.getString(23),
                    resultSetSql.getString(24),
                    resultSetSql.getString(25),
                    resultSetSql.getString(26),
                    resultSetSql.getString(27),
                    resultSetSql.getString(28),
                    resultSetSql.getString(29),
                    resultSetSql.getString(30),
                    resultSetSql.getString(31),
                    resultSetSql.getString(32),
                    resultSetSql.getString(33),
                    resultSetSql.getString(34),
                    resultSetSql.getString(35),
                    resultSetSql.getDouble(36),
                    resultSetSql.getDouble(37),
                    resultSetSql.getDouble(38),
                    resultSetSql.getString(39),
                    resultSetSql.getString(40),
                    resultSetSql.getString(41),
                    resultSetSql.getDouble(42),
                    resultSetSql.getString(43),
                    resultSetSql.getString(44),
                    resultSetSql.getDouble(45),
                    resultSetSql.getString(46),
                    resultSetSql.getDouble(47),
                    resultSetSql.getInt(48),
                    resultSetSql.getString(49),
                    resultSetSql.getString(50),
                    resultSetSql.getString(51),
                    resultSetSql.getString(52),
                    resultSetSql.getString(53),
                    resultSetSql.getDouble(54),
                    resultSetSql.getDouble(55),
                    resultSetSql.getString(56),
                    resultSetSql.getString(57),
                    resultSetSql.getString(58),
                    resultSetSql.getString(59),
                    resultSetSql.getString(60),
                    resultSetSql.getDouble(61),
                    resultSetSql.getString(62),
                    resultSetSql.getString(63),
                    resultSetSql.getString(64),
                    resultSetSql.getString(65),
                    resultSetSql.getString(66),
                    resultSetSql.getString(67),
                    resultSetSql.getString(68),
                    resultSetSql.getString(69),
                    resultSetSql.getString(70),
                    resultSetSql.getString(71),
                    resultSetSql.getInt(72));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

}
