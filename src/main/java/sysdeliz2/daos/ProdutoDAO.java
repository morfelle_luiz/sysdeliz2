/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.*;
import sysdeliz2.models.table.TableManutencaoPedidosCoresReferencia;
import sysdeliz2.models.table.TableProdutoGradeProduto;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface ProdutoDAO {

    List<ProdutosMarketing> loadProdutosMarketing() throws SQLException;

    List<ProdutosMarketing> loadProdutosMarketingByForm(String pCodigo, String pDescricao) throws SQLException;

    ProdutosMarketing getProdutosMarketingByCodigo(String pCodigo) throws SQLException;

    ProdutoLancamentoReserva getProdutoLancamentoByPedidoAndReservaAndBarra(String pPedido, String pReserva, String pBarra) throws SQLException;

    String getDescCorProduto(String pCodigo) throws SQLException;

    Integer getEstoqueSku(String codigo, String cor, String tam, String deposito) throws SQLException;

    String getEstoqueSkuRelatorio(String codigo, String cor, String tam, String deposito) throws SQLException;

    List<Produto> getProdutosColecao(String colecao) throws SQLException;

    List<Produto> getProdutosPedidos(String whereFilter) throws SQLException;

    List<Produto> getProdutosWithoutPedidos(String codigos) throws SQLException;

    ObservableList<TableManutencaoPedidosCoresReferencia> getCoresProduto(String codigo) throws SQLException;

    ObservableList<Produto> getProdutosFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException;

    ObservableList<TableProdutoGradeProduto> getGradeProduto(String referencia) throws SQLException;

    ObservableList<Cor> getCoresPorProduto(String codigo) throws SQLException;

    ObservableList<Tamanho> getTamanhosPorProduto(String codigo) throws SQLException;

    void updateProdutoOrdem(List<ProdutoOrdem> list) throws SQLException;

    Produto getByCodigo(String codigo) throws SQLException;

}
