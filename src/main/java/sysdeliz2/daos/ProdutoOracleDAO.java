/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import sysdeliz2.models.*;
import sysdeliz2.models.table.TableManutencaoPedidosCoresReferencia;
import sysdeliz2.models.table.TableProdutoGradeProduto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author cristiano.diego
 */
public class ProdutoOracleDAO implements ProdutoDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    private ObservableMap<String, Integer> getSaldosTamanhos(String codigoPrd, String corRef) throws SQLException {
        ObservableMap<String, Integer> mapTamanhos = FXCollections.observableHashMap();

        String query = "select prod.tam,\n"
                + "       nvl((select sum(qt_orig - (quant + quant_2 + quant_f + quant_i))\n"
                + "             from faccao_001\n"
                + "            where codigo = prod.codigo\n"
                + "              and cor = prod.cor\n"
                + "              and tam = prod.tam\n"
                + "              and op in ('100', '99')\n"
                + "              and qt_orig > quant + quant_2 + quant_f + quant_i),\n"
                + "           0) planj,\n"
                + "       nvl((select sum(saldo) saldo\n"
                + "             from (SELECT OF1.NUMERO,\n"
                + "                          PRD.CODIGO,\n"
                + "                          FAC.OP,\n"
                + "                          FAC.PARTE,\n"
                + "                          FAC.COR,\n"
                + "                          FAC.TAM,\n"
                + "                          SUM(FAC.QT_ORIG) -\n"
                + "                          SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F +\n"
                + "                              FAC.QUANT_I) SALDO\n"
                + "                     FROM OF1_001 OF1\n"
                + "                    INNER JOIN PRODUTO_001 PRD\n"
                + "                       ON PRD.CODIGO = OF1.CODIGO\n"
                + "                      AND PRD.ATIVO = 'S'\n"
                + "                    INNER JOIN FACCAO_001 FAC\n"
                + "                       ON FAC.NUMERO = OF1.NUMERO\n"
                + "                      AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 +\n"
                + "                          FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                      AND FAC.PARTE IN CASE\n"
                + "                            WHEN (SELECT PARTE\n"
                + "                                    FROM FACCAO_001\n"
                + "                                   WHERE NUMERO = OF1.NUMERO\n"
                + "                                     AND OP IN ('100')\n"
                + "                                   GROUP BY PARTE) IS NOT NULL THEN\n"
                + "                             (SELECT PARTE\n"
                + "                                FROM FACCAO_001\n"
                + "                               WHERE NUMERO = OF1.NUMERO\n"
                + "                                 AND OP IN ('100')\n"
                + "                               GROUP BY PARTE)\n"
                + "                            ELSE\n"
                + "                             (SELECT PARTE\n"
                + "                                FROM FACCAO_001\n"
                + "                               WHERE NUMERO = OF1.NUMERO\n"
                + "                               GROUP BY PARTE)\n"
                + "                          END\n"
                + "                    WHERE OF1.PERIODO NOT IN ('M', 'S')\n"
                + "                      and fac.op not in ('100', '99')\n"
                + "                    GROUP BY OF1.NUMERO,\n"
                + "                             PRD.CODIGO,\n"
                + "                             FAC.OP,\n"
                + "                             FAC.PARTE,\n"
                + "                             FAC.COR,\n"
                + "                             FAC.TAM\n"
                + "                   UNION\n"
                + "                   SELECT OF1.NUMERO,\n"
                + "                          PRD.CODIGO,\n"
                + "                          FAC.OP,\n"
                + "                          FAC.PARTE,\n"
                + "                          FAC.COR,\n"
                + "                          FAC.TAM,\n"
                + "                          SUM(FAC.QT_ORIG) -\n"
                + "                          SUM(FAC.QUANT + FAC.QUANT_2 + FAC.QUANT_F +\n"
                + "                              FAC.QUANT_I) SALDO\n"
                + "                     FROM OF1_001 OF1\n"
                + "                    INNER JOIN PRODUTO_001 PRD\n"
                + "                       ON PRD.CODIGO = OF1.CODIGO\n"
                + "                      AND PRD.ATIVO = 'S'\n"
                + "                    INNER JOIN FACCAO_001 FAC\n"
                + "                       ON FAC.NUMERO = OF1.NUMERO\n"
                + "                      AND FAC.QT_ORIG - (FAC.QUANT + FAC.QUANT_2 +\n"
                + "                          FAC.QUANT_F + FAC.QUANT_I) > 0\n"
                + "                    WHERE prd.codigo = '" + codigoPrd + "'\n"
                + "                      and OF1.PERIODO NOT IN ('M', 'S')\n"
                + "                      and fac.op not in ('100', '99')\n"
                + "                      AND FAC.PARTE NOT IN\n"
                + "                          (SELECT PARTE\n"
                + "                             FROM FACCAO_001\n"
                + "                            WHERE NUMERO = OF1.NUMERO\n"
                + "                              AND OP IN ('100')\n"
                + "                            GROUP BY PARTE)\n"
                + "                      AND (SELECT SUM(FAC2.QT_ORIG) -\n"
                + "                                  SUM(FAC2.QUANT + FAC2.QUANT_2 +\n"
                + "                                      FAC2.QUANT_F + FAC2.QUANT_I)\n"
                + "                             FROM FACCAO_001 FAC2\n"
                + "                            WHERE NUMERO = OF1.NUMERO\n"
                + "                              AND PARTE = (SELECT PARTE\n"
                + "                                             FROM FACCAO_001\n"
                + "                                            WHERE NUMERO = FAC2.NUMERO\n"
                + "                                              AND OP IN ('100')\n"
                + "                                            GROUP BY PARTE)) <= 0\n"
                + "                    GROUP BY OF1.NUMERO,\n"
                + "                             PRD.CODIGO,\n"
                + "                             FAC.OP,\n"
                + "                             FAC.PARTE,\n"
                + "                             FAC.COR,\n"
                + "                             FAC.TAM)\n"
                + "            where codigo = prod.codigo\n"
                + "              and cor = prod.cor\n"
                + "              and tam = prod.tam),\n"
                + "           0) em_prodc,\n"
                + "       nvl((select sum(quant)\n"
                + "             from faccao_001 fac, of1_001 of1\n"
                + "            where fac.numero = of1.numero\n"
                + "              and of1.periodo not in ('S','M')\n"
                + "              and fac.codigo = prod.codigo\n"
                + "              and fac.cor = prod.cor\n"
                + "              and fac.tam = prod.tam\n"
                + "              and fac.op in ('111', '118')\n"
                + "              and fac.qt_orig <= fac.quant + fac.quant_2 + fac.quant_f + fac.quant_i),\n"
                + "           0) prodz,\n"
                + "       nvl((select sum(quantidade)\n"
                + "             from pa_iten_001\n"
                + "            where codigo = prod.codigo\n"
                + "              and cor = prod.cor\n"
                + "              and tam = prod.tam\n"
                + "              and deposito in ('0005', '0001', '0011')),\n"
                + "           0) estoq,\n"
                + "       nvl((select sum(qtde)\n"
                + "             from pedido3_001 ped\n"
                + "            where ped.codigo = prod.codigo\n"
                + "              and ped.cor = prod.cor\n"
                + "              and ped.tam = prod.tam\n"
                + "              and ped.deposito in ('0001','0005')),\n"
                + "           0) exped,\n"
                + "       nvl((select sum(qtde)\n"
                + "             from ped_iten_001 pei\n"
                + "             join pedido_001 ped\n"
                + "               on pei.numero = ped.numero\n"
                + "            where pei.codigo = prod.codigo\n"
                + "              and ped.periodo not in ('MOST')\n"
                + "              and pei.cor = prod.cor\n"
                + "              and pei.tam = prod.tam\n"
                + "              and pei.qtde > 0),\n"
                + "           0) cart\n"
                + "  from vproduto_cortam prod\n"
                + "  join produto_001 prd\n"
                + "    on prd.codigo = prod.codigo\n"
                + "  join faixa_iten_001 fai\n"
                + "    on fai.tamanho = prod.tam\n"
                + "   and fai.faixa = prd.faixa\n"
                + " where prod.codigo = '" + codigoPrd + "'\n"
                + "   and prod.cor = '" + corRef + "'\n"
                + " order by fai.posicao";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getString("tam"),
                    resultSetSql.getInt("planj")
                            + resultSetSql.getInt("em_prodc")
                            + resultSetSql.getInt("estoq")
                            + resultSetSql.getInt("exped")
                            - resultSetSql.getInt("cart"));
        }

        resultSetSql.close();

        return mapTamanhos;
    }

    private ObservableMap<Integer, String> getHeaderTamanhosRef(String codigoPrd, String corRef) throws SQLException {
        ObservableMap<Integer, String> mapTamanhos = FXCollections.observableHashMap();

        String query = "select   vct.tam,\n"
                + "       fai.posicao\n"
                + "  from produto_001 prd\n"
                + "  join vproduto_cortam vct\n"
                + "    on vct.codigo = prd.codigo\n"
                + "  join faixa_iten_001 fai\n"
                + "    on fai.faixa = prd.faixa\n"
                + "   and fai.tamanho = vct.tam\n"
                + " where prd.codigo = '" + codigoPrd + "'\n"
                + "   and vct.cor = '" + corRef + "'\n"
                + " order by fai.posicao";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getInt("posicao"), resultSetSql.getString("tam"));
        }

        resultSetSql.close();

        return mapTamanhos;
    }

    @Override
    public ObservableList<TableManutencaoPedidosCoresReferencia> getCoresProduto(String codigo) throws SQLException {
        ObservableList<TableManutencaoPedidosCoresReferencia> cores = FXCollections.observableArrayList();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select distinct prod.codigo, cortam.cor\n"
                + "  from produto_001 prod\n"
                + "  join vproduto_cortam cortam\n"
                + "    on cortam.codigo = prod.codigo\n"
                + " where prod.codigo = '" + codigo + "'"
                + " order by cortam.cor";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            cores.add(new TableManutencaoPedidosCoresReferencia(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("cor"),
                    this.getSaldosTamanhos(
                            resultSetSql.getString("codigo"),
                            resultSetSql.getString("cor")
                    ),
                    this.getHeaderTamanhosRef(
                            resultSetSql.getString("codigo"),
                            resultSetSql.getString("cor")
                    )));
        }
        resultSetSql.close();
        this.closeConnection();
        return cores;
    }

    @Override
    public List<ProdutosMarketing> loadProdutosMarketing() throws SQLException {
        List<ProdutosMarketing> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "SELECT 	PROD.CODIGO, PROD.DESCRICAO, \n"
                + "		CASE WHEN PAI.QUANTIDADE IS NULL THEN 0 ELSE PAI.QUANTIDADE END QTDE \n"
                + "FROM PRODUTO_001 PROD\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = PROD.CODIGO AND PAI.DEPOSITO = '0005')\n"
                + "WHERE 	PROD.COLECAO = 'MKT'\n"
                + "		AND PROD.ATIVO = 'S'";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            produtos.add(new ProdutosMarketing(
                    resultSetSql.getString("CODIGO"),
                    resultSetSql.getString("DESCRICAO"),
                    resultSetSql.getString("QTDE")));
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public List<ProdutosMarketing> loadProdutosMarketingByForm(String pCodigo, String pDescricao) throws SQLException {
        List<ProdutosMarketing> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "SELECT 	PROD.CODIGO, PROD.DESCRICAO, \n"
                + "		CASE WHEN PAI.QUANTIDADE IS NULL THEN 0 ELSE PAI.QUANTIDADE END QTDE \n"
                + "FROM PRODUTO_001 PROD\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = PROD.CODIGO AND PAI.DEPOSITO = '0005')\n"
                + "WHERE 	PROD.COLECAO = 'MKT'\n"
                + "		AND PROD.ATIVO = 'S' \n"
                + "		AND PROD.CODIGO LIKE ?\n"
                + "		AND PROD.DESCRICAO LIKE ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, "%" + pCodigo + "%");
        preparedStatement.setString(2, "%" + pDescricao + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            produtos.add(new ProdutosMarketing(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("QTDE")));
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public ProdutosMarketing getProdutosMarketingByCodigo(String pCodigo) throws SQLException {
        ProdutosMarketing produto = null;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "SELECT 	PROD.CODIGO, PROD.DESCRICAO, \n"
                + "		CASE WHEN PAI.QUANTIDADE IS NULL THEN 0 ELSE PAI.QUANTIDADE END QTDE \n"
                + "FROM PRODUTO_001 PROD\n"
                + "LEFT JOIN PA_ITEN_001 PAI ON (PAI.CODIGO = PROD.CODIGO AND PAI.DEPOSITO = '0005')\n"
                + "WHERE 	PROD.COLECAO = 'MKT'\n"
                + "		AND PROD.ATIVO = 'S' \n"
                + "		AND PROD.CODIGO = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pCodigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            produto = new ProdutosMarketing(resultSetSql.getString("CODIGO"), resultSetSql.getString("DESCRICAO"), resultSetSql.getString("QTDE"));
        }
        resultSetSql.close();
        this.closeConnection();
        return produto;
    }

    @Override
    public ProdutoLancamentoReserva getProdutoLancamentoByPedidoAndReservaAndBarra(String pPedido, String pReserva, String pBarra) throws SQLException {
        ProdutoLancamentoReserva produto = null;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "SELECT RES.PEDIDO, RES.RESERVA, PROD.CODIGO, PAI.COR, PAI.TAM, PROD.DESCRICAO, RES.DEPOSITO, PROD.MINIMO, \n"
                + "      COALESCE(PEI.ORDEM, 0) ORDEM, \n"
                + "		CASE WHEN PROD.COLECAO = 'MKT' THEN 'MKT' ELSE CASE WHEN PROD.COLECAO = 'KTKT' THEN 'KIT' ELSE 'COM' END END TIPO,\n"
                + "		CASE WHEN RES.PEDIDO IS NULL THEN 'N' ELSE 'S' END DO_PEDIDO,\n"
                + "		CASE WHEN MAX(EXI.CAIXA) IS NULL THEN 'N' ELSE 'S' END EXISTE, \n"
                + "		CASE WHEN RES.PEDIDO IS NULL THEN 0 ELSE (RES.QTDE - COUNT(CAI.BARRA)) END SALDO,\n"
                + "		TO_CHAR(SDPROD.EXPEDICAO) INFOEXP\n"
                + " FROM PA_ITEN_001 PAI\n"
                + " INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = PAI.CODIGO)\n"
                + " INNER JOIN SD_PRODUTO_001 SDPROD ON (SDPROD.CODIGO = PROD.CODIGO)\n"
                + " LEFT JOIN SD_RESERVA_PEDIDO_001 RES ON (PAI.CODIGO = RES.CODIGO AND PAI.COR = RES.COR AND PAI.TAM = RES.TAM AND RES.PED_AGRUPADOR = ? AND RES.RESERVA = ?)\n"
                + " LEFT JOIN PED_ITEN_001 PEI ON (PEI.NUMERO = RES.PEDIDO AND PEI.CODIGO = RES.CODIGO AND PEI.COR = RES.COR AND PEI.TAM = RES.TAM)\n"
                + " LEFT JOIN SD_CAIXA_RESERVA_PEDIDO_001 CAI ON (CAI.CODIGO = RES.CODIGO AND CAI.COR = RES.COR AND CAI.TAM = RES.TAM AND CAI.PEDIDO = RES.PEDIDO AND CAI.RESERVA = RES.RESERVA)\n"
                + " LEFT JOIN SD_CAIXA_RESERVA_PEDIDO_001 EXI ON (EXI.BARRA = ?)\n"
                + " WHERE PAI.BARRA28 = SUBSTR(?,1,6) AND PAI.DEPOSITO = '0001' AND PAI.ATIVO = 'S' AND LOTE = '000000'\n"
                + " GROUP BY RES.PEDIDO, RES.RESERVA, PROD.CODIGO, PAI.COR, PAI.TAM, PROD.DESCRICAO, RES.QTDE, RES.PEDIDO, PROD.COLECAO, RES.DEPOSITO, PEI.ORDEM, PROD.MINIMO, TO_CHAR(SDPROD.EXPEDICAO)\n"
                + " order by 13 desc";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pReserva);
        preparedStatement.setString(3, pBarra);
        preparedStatement.setString(4, pBarra);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            produto = new ProdutoLancamentoReserva(
                    resultSetSql.getString("PEDIDO"),
                    resultSetSql.getString("RESERVA"),
                    resultSetSql.getString("CODIGO"),
                    resultSetSql.getString("DESCRICAO"),
                    resultSetSql.getString("COR"),
                    resultSetSql.getString("TAM"),
                    resultSetSql.getString("TIPO"),
                    resultSetSql.getString("DEPOSITO"),
                    Integer.parseInt(resultSetSql.getString("ORDEM")),
                    resultSetSql.getString("DO_PEDIDO"),
                    resultSetSql.getString("EXISTE"),
                    Integer.parseInt(resultSetSql.getString("SALDO")),
                    Integer.parseInt(resultSetSql.getString("MINIMO")),
                    resultSetSql.getString("INFOEXP"));
        }
        resultSetSql.close();
        this.closeConnection();
        return produto;
    }

    @Override
    public String getDescCorProduto(String pCodigo) throws SQLException {
        String cor = "NÃO CADASTRADA";

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "SELECT DESCRICAO FROM CADCOR_001\n"
                + "WHERE COR = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pCodigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            cor = resultSetSql.getString("DESCRICAO");
        }
        resultSetSql.close();
        this.closeConnection();
        return cor;
    }

    @Override
    public Integer getEstoqueSku(String codigo, String cor, String tam, String deposito) throws SQLException {
        int qtdeEstoque = 0;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select pai.quantidade - (sum(res.qtde)-sum(nvl(ped3.qtde,0))) qtde\n"
                + "  from pa_iten_001 pai\n"
                + "  left join ped_reserva_001 res\n"
                + "    on res.codigo = pai.codigo\n"
                + "   and res.cor = pai.cor\n"
                + "   and res.tam = pai.tam\n"
                + "   and res.deposito = pai.deposito\n"
                + "  left join pedido3_001 ped3\n"
                + "    on ped3.codigo = res.codigo\n"
                + "   and ped3.cor = res.cor\n"
                + "   and ped3.tam = res.tam\n"
                + "   and ped3.deposito = res.deposito\n"
                + "   and ped3.numero = res.numero\n"
                + "   and ped3.deposito in ('0001','0005')\n"
                + " where pai.codigo = ?\n"
                + "   and pai.cor = ?\n"
                + "   and pai.tam = ?\n"
                + "   and pai.deposito = ?\n"
                + " group by pai.quantidade";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, cor);
        preparedStatement.setString(3, tam);
        preparedStatement.setString(4, deposito);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            qtdeEstoque = resultSetSql.getInt("qtde");
        }
        resultSetSql.close();
        this.closeConnection();
        return qtdeEstoque;
    }

    @Override
    public String getEstoqueSkuRelatorio(String codigo, String cor, String tam, String deposito) throws SQLException {
        String gradeRelatorio = null;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "SELECT PKG_SD_PRODUTO.get_tams_ref_listagg(?, 6) tams,\n"
                + "PKG_SD_EXPEDICAO.get_qtdes_ref_estq_listagg(?, ?, ?) qtdes FROM DUAL";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codigo);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, cor);
        preparedStatement.setString(4, deposito);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            gradeRelatorio = resultSetSql.getString("tams") + "\n" + resultSetSql.getString("qtdes");
        }
        resultSetSql.close();
        this.closeConnection();
        return gradeRelatorio;
    }

    @Override
    public List<Produto> getProdutosColecao(String colecao) throws SQLException {
        List<Produto> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select prod.* from produto_001 prod \n"
                + "join sd_produto_colecao_001 prc on prc.codigo = prod.codigo\n"
                + "where prc.colecao = ?\n"
                + "order by prod.codigo";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, colecao);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Produto produto = new Produto();
            produto.setCodigo(resultSetSql.getString("codigo"));
            produto.setDescricao(resultSetSql.getString("descricao"));
            //wproduto.setCores_saldos(this.getCoresProduto(resultSetSql.getString("codigo")));
            produtos.add(produto);
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public List<Produto> getProdutosPedidos(String whereFilter) throws SQLException {
        List<Produto> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select distinct pei.codigo, prd.descricao\n"
                + "  from ped_iten_001 pei\n"
                + "  join pedido_001 ped\n"
                + "    on ped.numero = pei.numero\n"
                + "  join produto_001 prd\n"
                + "    on prd.codigo = pei.codigo\n"
                + "  join sd_produto_001 sdprd\n"
                + "    on sdprd.codigo = prd.codigo\n"
                + "  join entidade_001 cli\n"
                + "    on cli.codcli = ped.codcli\n"
                + "  join represen_001 rep\n"
                + "    on rep.codrep = ped.codrep\n"
                + " where pei.qtde > 0\n"
                + whereFilter;
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Produto produto = new Produto();
            produto.setCodigo(resultSetSql.getString("codigo"));
            produto.setDescricao(resultSetSql.getString("descricao"));
            produtos.add(produto);
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public List<Produto> getProdutosWithoutPedidos(String codigos) throws SQLException {
        List<Produto> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select distinct prd.codigo, prd.descricao\n"
                + "  from produto_001 prd\n"
                + "  join sd_produto_001 sdprd\n"
                + "    on sdprd.codigo = prd.codigo\n"
                + " where sdprd.revisao_comercial = 'S'\n"
                + (codigos != null && codigos.length() > 0 ? "and prd.codigo in (" + codigos + ")\n" : "");
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Produto produto = new Produto();
            produto.setCodigo(resultSetSql.getString("codigo"));
            produto.setDescricao(resultSetSql.getString("descricao"));
            produtos.add(produto);
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public ObservableList<Produto> getProdutosFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Produto> produtos = FXCollections.observableArrayList();

        whereFilter = " and ";
        wheresClauses.forEach(filters -> whereFilter += filters.get(0)
                + " "
                + filters.get(1)
                + " "
                + filters.get(2)
                + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                + filters.get(3).toUpperCase()
                + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                + "\n");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();

        String query = "SELECT PROD.* " +
                " FROM produto_001 PROD" +
                " LEFT JOIN ETQ_PROD_001 E ON PROD.ETIQUETA = E.CODIGO" +
                " LEFT JOIN TABLIN_001 L ON PROD.LINHA = L.CODIGO" +
                " WHERE ativo = 'S' " + whereFilter;

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Produto produto = new Produto();
            produto.setCodigo(resultSetSql.getString("codigo"));
            produto.setDescricao(resultSetSql.getString("descricao"));
            produto.setMarca(resultSetSql.getString("marca"));
            produto.setColecao(resultSetSql.getString("colecao"));
            produtos.add(produto);
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public ObservableList<TableProdutoGradeProduto> getGradeProduto(String referencia) throws SQLException {
        ObservableList<TableProdutoGradeProduto> grade = FXCollections.observableArrayList();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "select prod.cor, prod.tam,\n"
                + "       nvl((select sum(qt_orig - (quant + quant_2 + quant_f + quant_i))\n"
                + "             from faccao_001\n"
                + "            where codigo = prod.codigo\n"
                + "              and cor = prod.cor\n"
                + "              and tam = prod.tam\n"
                + "              and op in ('100', '99')\n"
                + "              and qt_orig > quant + quant_2 + quant_f + quant_i),\n"
                + "           0) planj,\n"
                + "       nvl(pkg_sd_produto.get_em_producao(prod.codigo, prod.cor, prod.tam),\n"
                + "           0) em_prodc,\n"
                + "       nvl((select sum(quant)\n"
                + "             from faccao_001 fac, of1_001 of1\n"
                + "            where fac.numero = of1.numero\n"
                + "              and of1.periodo not in ('S','M')\n"
                + "              and fac.codigo = prod.codigo\n"
                + "              and fac.cor = prod.cor\n"
                + "              and fac.tam = prod.tam\n"
                + "              and fac.op in ('111', '118')\n"
                + "              and fac.qt_orig <= fac.quant + fac.quant_2 + fac.quant_f + fac.quant_i),\n"
                + "           0) prodz,\n"
                + "       nvl((select sum(quantidade)\n"
                + "             from pa_iten_001\n"
                + "            where codigo = prod.codigo\n"
                + "              and cor = prod.cor\n"
                + "              and tam = prod.tam\n"
                + "              and deposito in ('0005', '0001', '0011')),\n"
                + "           0) estoq,\n"
                + "       nvl((select sum(qtde)\n"
                + "             from pedido3_001 ped\n"
                + "            where ped.codigo = prod.codigo\n"
                + "              and ped.cor = prod.cor\n"
                + "              and ped.tam = prod.tam\n"
                + "              and ped.deposito in ('0001','0005')),\n"
                + "           0) exped,\n"
                + "       nvl((select sum(qtde)\n"
                + "             from ped_iten_001 pei\n"
                + "             join pedido_001 ped\n"
                + "               on pei.numero = ped.numero\n"
                + "            where pei.codigo = prod.codigo\n"
                + "                and (ped.periodo between '0000' and '9999' or ped.periodo = 'GVT')\n"
                + "              and pei.cor = prod.cor\n"
                + "              and pei.tam = prod.tam\n"
                + "              and pei.qtde > 0),\n"
                + "           0) cart\n"
                + "  from vproduto_cortam prod\n"
                + "  join produto_001 prd\n"
                + "    on prd.codigo = prod.codigo\n"
                + "  join faixa_iten_001 fai\n"
                + "    on fai.tamanho = prod.tam\n"
                + "   and fai.faixa = prd.faixa\n"
                + " where prod.codigo = '" + referencia + "'\n"
                + " order by prod.cor, fai.posicao";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            grade.add(new TableProdutoGradeProduto(resultSetSql.getString("cor"), resultSetSql.getString("tam"),
                    resultSetSql.getInt("planj"), resultSetSql.getInt("em_prodc"), resultSetSql.getInt("prodz"),
                    resultSetSql.getInt("estoq"), resultSetSql.getInt("exped"), resultSetSql.getInt("cart"),
                    resultSetSql.getInt("planj") + resultSetSql.getInt("em_prodc") + resultSetSql.getInt("estoq") + resultSetSql.getInt("exped") - resultSetSql.getInt("cart")));
        }

        resultSetSql.close();
        this.closeConnection();

        return grade;
    }

    @Override
    public ObservableList<Cor> getCoresPorProduto(String codigo) throws SQLException {

        ObservableList<Cor> cores = FXCollections.observableArrayList();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "SELECT DISTINCT " +
                "    prd.codigo, " +
                "    prd.colecao, " +
                "    pai.cor, " +
                "    ccor.descricao as desc_cor, " +
                "    NVL(( " +
                "        SELECT SUM(GRADE.PERCENTUAL) " +
                "        FROM PRODUTO_ORDEM_001 GRADE " +
                "        WHERE GRADE.CODIGO = PAI.CODIGO AND GRADE.COR = PAI.COR AND GRADE.TIPO = 'V' " +
                "    ),0) AS perccor " +
                "FROM " +
                "    produto_001 prd " +
                "    INNER JOIN pa_iten_001 pai " +
                "    ON pai.codigo = prd.codigo " +
                "    INNER JOIN cadcor_001 ccor " +
                "    ON ccor.cor = pai.cor " +
                "WHERE " +
                "    prd.codigo = '%s' " +
                "ORDER BY pai.cor";
        preparedStatement = connection.prepareStatement(String.format(query, codigo));
        preparedStatement.execute();

        try (ResultSet resultSetSql = preparedStatement.getResultSet()) {
            while (resultSetSql.next()) {
                cores.add(
                        new Cor(
                                resultSetSql.getString("codigo"),
                                resultSetSql.getString("colecao"),
                                resultSetSql.getString("cor"),
                                resultSetSql.getString("desc_cor"),
                                resultSetSql.getString("perccor")
                        )
                );
            }
        }
        this.closeConnection();
        return cores;
    }

    @Override
    public ObservableList<Tamanho> getTamanhosPorProduto(String codigo) throws SQLException {

        ObservableList<Tamanho> tamanhos = FXCollections.observableArrayList();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "SELECT DISTINCT " +
                "    prd.codigo, " +
                "    prd.colecao, " +
                "    pai.tam, " +
                "    fai.posicao," +
                "    NVL(" +
                "        (SELECT SUM( pro.percentual )" +
                "         FROM produto_ordem_001 pro " +
                "         WHERE " +
                "            pro.codigo = prd.codigo " +
                "            AND pro.tam = pai.tam " +
//                "            AND pro.COR = pai.COR " +
                "         ) , 0 ) as perctam " +
                "FROM " +
                "    produto_001 prd " +
                "    INNER JOIN pa_iten_001 pai " +
                "    ON pai.codigo = prd.codigo " +
                "    INNER JOIN faixa_iten_001 fai " +
                "    ON fai.faixa = prd.faixa " +
                "    AND fai.tamanho = pai.tam " +
                "WHERE " +
                "    prd.codigo = '%s' " +
                "    AND pai.ATIVO = 'S'" +
                "ORDER BY fai.posicao ASC";
        preparedStatement = connection.prepareStatement(String.format(query, codigo));
        preparedStatement.execute();

        try (ResultSet resultSetSql = preparedStatement.getResultSet()) {
            while (resultSetSql.next()) {

                AtomicReference<Boolean> encontrou = new AtomicReference<>(false);
                tamanhos.forEach(tamanho -> {
                    try {
                        if (
                                (tamanho.getCodigo().equals(resultSetSql.getString("codigo"))) &&
                                (tamanho.getTam().equals(resultSetSql.getString("tam"))) &&
                                (tamanho.getColecao().equals(resultSetSql.getString("colecao"))) ){
                            // Soma o percentual se encontrar o percentual já na tabela
                            Double perc = tamanho.getPercTamAsDouble();
                            perc += Double.parseDouble(resultSetSql.getString("perctam"));

                            tamanho.setPerctam(perc.toString());


                            encontrou.set(true);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });

                if(!encontrou.get()) {
                    tamanhos.add(
                            new Tamanho(
                                    resultSetSql.getString("codigo"),
                                    resultSetSql.getString("tam"),
                                    resultSetSql.getString("colecao"),
                                    resultSetSql.getString("perctam")
                            )
                    );
                }

            }
        }
        this.closeConnection();
        return tamanhos;
    }

    @Override
    public void updateProdutoOrdem(List<ProdutoOrdem> list) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();

        for (ProdutoOrdem prod : list) {
            //LoggerExtensions.getLogger(ScenePcpPriorizarReferenciasController.class.getName()).log(Level.INFO, prod.getInsertUpdateSQL());
            statement.execute(prod.getInsertUpdateSQL());
        }
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public Produto getByCodigo(String codigo) throws SQLException {
        Produto produto = null;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "select distinct codigo, descricao\n"
                + "  from produto_001\n"
                + " where ativo = 'S'\n"
                + "   and codigo = ?"
                + whereFilter;
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, codigo);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            produto = new Produto();
            produto.setCodigo(resultSetSql.getString("codigo"));
            produto.setDescricao(resultSetSql.getString("descricao"));
        }
        resultSetSql.close();
        this.closeConnection();
        return produto;
    }

}
