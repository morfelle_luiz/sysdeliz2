/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.properties.Composicao00X;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class PropertiesDAO {

    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    public ObservableList<Composicao00X> getComposicaoProduto(String numero, String cor, String setor) throws SQLException {
        ObservableList<Composicao00X> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select nvl(comp.codigo,'0') codigo, nvl(comp.descricao,'Definir') descricao, mt.composicao\n" +
                "  from pcpftof_001 ft\n" +
                "  join pcpapl_001 apl\n" +
                "    on apl.codigo = ft.aplicacao\n" +
                "  join material_001 mt\n" +
                "    on mt.codigo = ft.insumo\n" +
                "  join cadfluxo_001 flx\n" +
                "    on flx.codigo = ft.setor\n" +
                "  left join pcpapl_001 comp\n" +
                "    on comp.codigo = apl.tipo\n" +
                " where flx.sd_grupo in ('103')\n" +
                "   and apl.tipo in ('002', '468', '717')\n" +
                "   and ft.cor_i <> '*****'\n" +
                "   and ft.numero = ?\n" +
                "   and ft.cor = ?\n" +
                " order by decode(comp.codigo, '717', 1, '002', 2, '468', 3)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
//        preparedStatement.setString(1, setor);
        preparedStatement.setString(1, numero);
        preparedStatement.setString(2, cor);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new Composicao00X(resultSetSql.getString(2),
                    resultSetSql.getString(3)));
        }
        resultSetSql.close();
        this.closeConnection();

        return rows;
    }

}
