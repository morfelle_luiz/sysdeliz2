/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.ReceberCartao;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface ReceberCartaoDAO {

    public String save_transaction(String payment_id, String auth_code, String terminal, String ati, String verification_id, Double valor, String status,
                                   String pedido, Integer parcelas, String brand, String token_card, String token_oauth, String cvv, String validade, String cartao, String data, String pedido_agrupado) throws SQLException;

    public ObservableList<ReceberCartao> get_all() throws SQLException;

    public ObservableList<ReceberCartao> get_form(String pedido, String cliente, String data) throws SQLException;

    public void cancel_transaction(String numero, String data_cancel, String payment_id, String request_id, String status) throws SQLException;

    public void status_transaction(String numero, String status) throws SQLException;
}
