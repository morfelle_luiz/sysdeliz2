/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.Pedido;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface ReceberDAO {

    public Pedido getReceber(String numero) throws SQLException;
}
