/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Representante;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class RepresentanteOracleDAO implements RepresentanteDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Representante> getAll() throws SQLException {
        ObservableList<Representante> dados_db = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT \n"
                        + "	REP.CODREP, REP.NOME, REP.RESPON RESP, CID.REGIAO, \n"
                        + "	CID.NOME_CID CIDADE, UPPER(ENT.SITE) GERENTE \n"
                        + "FROM REPRESEN_001 REP \n"
                        + "INNER JOIN CADCEP_001 CCEP ON\n"
                        + "	CCEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON\n"
                        + "	CID.COD_CID = CCEP.COD_CID\n"
                        + "LEFT JOIN ENTIDADE_001 ENT ON\n"
                        + "	ENT.CODCLI = REP.CODCLI"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            dados_db.add(new Representante(resultSetSql.getString("CODREP"), resultSetSql.getString("NOME"), resultSetSql.getString("RESP"),
                    resultSetSql.getString("REGIAO"), resultSetSql.getString("CIDADE"), resultSetSql.getString("GERENTE")));
        }
        closeConnection();
        resultSetSql.close();

        return dados_db;
    }

    @Override
    public ObservableList<Representante> getRepresentantes(String representante, String informacao) throws SQLException {
        ObservableList<Representante> dados_db = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT \n"
                        + "	REP.CODREP, REP.NOME, REP.RESPON RESP, CID.REGIAO, \n"
                        + "	CID.NOME_CID CIDADE, UPPER(ENT.SITE) GERENTE \n"
                        + "FROM REPRESEN_001 REP \n"
                        + "INNER JOIN CADCEP_001 CCEP ON\n"
                        + "	CCEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON\n"
                        + "	CID.COD_CID = CCEP.COD_CID\n"
                        + "LEFT JOIN ENTIDADE_001 ENT ON\n"
                        + "	ENT.CODCLI = REP.CODCLI\n"
                        + "WHERE \n"
                        + "	(REP.CODREP LIKE '%" + representante + "%' OR REP.NOME LIKE '%" + representante + "%' OR\n"
                        + "		REP.RESPON LIKE '%" + representante + "%') AND \n"
                        + "	(CID.REGIAO LIKE '%" + informacao + "%' OR CID.NOME_CID LIKE '%" + informacao + "%' OR\n"
                        + "		ENT.SITE LIKE '%" + informacao + "%')"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            dados_db.add(new Representante(resultSetSql.getString("CODREP"), resultSetSql.getString("NOME"), resultSetSql.getString("RESP"),
                    resultSetSql.getString("REGIAO"), resultSetSql.getString("CIDADE"), resultSetSql.getString("GERENTE")));
        }
        closeConnection();
        resultSetSql.close();

        return dados_db;
    }

    @Override
    public ObservableList<Representante> getRepresentantesFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Representante> dados_db = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(
                "SELECT \n"
                        + "	REP.CODREP, REP.NOME, REP.RESPON RESP, CID.REGIAO, \n"
                        + "	CID.NOME_CID CIDADE, UPPER(ENT.SITE) GERENTE \n"
                        + "FROM REPRESEN_001 REP \n"
                        + "INNER JOIN CADCEP_001 CCEP ON\n"
                        + "	CCEP.CEP = REP.CEP\n"
                        + "INNER JOIN CIDADE_001 CID ON\n"
                        + "	CID.COD_CID = CCEP.COD_CID\n"
                        + "join tabuf_001 uf\n"
                        + "     on uf.sigla_est = cid.cod_est\n"
                        + "LEFT JOIN ENTIDADE_001 ENT ON\n"
                        + "	ENT.CODCLI = REP.CODCLI\n"
                        + "WHERE" + whereFilter
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            dados_db.add(new Representante(resultSetSql.getString("CODREP"), resultSetSql.getString("NOME"), resultSetSql.getString("RESP"),
                    resultSetSql.getString("REGIAO"), resultSetSql.getString("CIDADE"), resultSetSql.getString("GERENTE")));
        }
        closeConnection();
        resultSetSql.close();

        return dados_db;
    }

}
