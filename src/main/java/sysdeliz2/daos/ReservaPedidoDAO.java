/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.ProdutosReservaPedido;
import sysdeliz2.models.ReservaPedido;
import sysdeliz2.utils.AcessoSistema;

import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public interface ReservaPedidoDAO {

    List<ReservaPedido> load() throws SQLException;

    List<ProdutosReservaPedido> loadByPedidoAndReserva(String pPedido, String pReserva, String pDeposito) throws SQLException;

    List<ReservaPedido> loadByFormReservaPedido(String pPedido, String pReserva, String pCliente, String pStatus) throws SQLException;

//    ReservaPedido getByPedidoAndReserva(String pPedido, String pReserva) throws SQLException;

    ReservaPedido getFromLancByPedidoAndReserva(String pPedido, String pReserva) throws SQLException;

    String updByPedidoCodigoCorTam(ReservaPedido rpReserva, AcessoSistema usuario) throws SQLException;

    void updStatusByReservaPedido(ReservaPedido rpReserva) throws SQLException;

    void saveMktReservaPedido(ReservaPedido rpReserva, List<ProdutosReservaPedido> pMkts, AcessoSistema usuario) throws SQLException;

    void saveMktReservaPedido(ReservaPedido rpReserva, List<ProdutosReservaPedido> pMkts, AcessoSistema usuario, String usuarioLeitor) throws SQLException;

    void deleteReservaPedido(ReservaPedido pReserva) throws SQLException;

    void deleteReservaPedido_PED_RESERVA(ReservaPedido pReserva) throws SQLException;

    void limpaReservaPedido_PED_RESERVA(ReservaPedido pReserva) throws SQLException;

    void deleteReferenciaReserva(ReservaPedido reserva, ProdutosReservaPedido produto) throws SQLException;

    void deleteReferenciaReserva_PED_RESERVA(ReservaPedido reserva, ProdutosReservaPedido produto) throws SQLException;

    List<String> getMarcasReserva(ReservaPedido rpReserva) throws SQLException;

    void lockPedido(ReservaPedido reserva) throws SQLException;

    void unlockPedido(ReservaPedido reserva) throws SQLException;
}
