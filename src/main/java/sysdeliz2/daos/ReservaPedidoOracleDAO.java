/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.ProdutosReservaPedido;
import sysdeliz2.models.ReservaPedido;
import sysdeliz2.utils.AcessoSistema;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class ReservaPedidoOracleDAO implements ReservaPedidoDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    private String getAIReservaPedido() throws SQLException {
        String strAIReserva = "";

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("" + "SELECT SD_SEQ_RESERVA_PEDIDO.NEXTVAL AS AIRESERVA FROM DUAL");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            strAIReserva = resultSetSql.getString("AIRESERVA");
        }
        resultSetSql.close();
        this.closeConnection();
        return strAIReserva;
    }

    // Métodos PUBLIC
    @Override
    public List<ReservaPedido> load() throws SQLException {
        List<ReservaPedido> reservas = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "" + "WITH reservas AS(\n"
                + "SELECT PEDR.NUMERO, CASE WHEN LANR.RESERVA IS NOT NULL THEN LANR.RESERVA ELSE '0' END RESERVA, \n"
                + "        PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO,\n"
                + "        CASE WHEN LANR.STATUS IS NOT NULL THEN LANR.STATUS ELSE 'A' END STATUS,\n"
                + "        NVL(LANR.PED_AGRUPADOR, PEDR.NUMERO) PED_AGRUPADOR\n"
                + "FROM PED_RESERVA_001 PEDR\n"
                + "LEFT JOIN SD_RESERVA_PEDIDO_001 LANR ON (LANR.RESERVA = PEDR.SD_RESERVA\n"
                + "                      AND LANR.PEDIDO = PEDR.NUMERO \n"
                + "                      AND LANR.CODIGO = PEDR.CODIGO\n"
                + "                      AND LANR.COR = PEDR.COR\n" + "                      AND LANR.TAM = PEDR.TAM\n"
                + "                      AND LANR.DEPOSITO = PEDR.DEPOSITO) \n"
                + "GROUP BY PEDR.NUMERO, LANR.RESERVA, PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO,LANR.STATUS, LANR.PED_AGRUPADOR\n"
                + "UNION\n"
                + "SELECT LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO, LANR.STATUS, LANR.PED_AGRUPADOR \n"
                + "FROM SD_RESERVA_PEDIDO_001 LANR\n"
                + "WHERE LANR.STATUS <> 'F'\n"
                + "GROUP BY LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO,LANR.STATUS, LANR.PED_AGRUPADOR)\n"
                + "\n"
                + "SELECT RES.NUMERO, ENT.NOME CLIENTE, RES.RESERVA, RES.DEPOSITO, RES.STATUS, SUM(RES.QTDE) QTDE, f_get_marca_pedido(res.numero) marca, ped.per_desc, res.ped_agrupador, ped.codrep, ped.pgto FROM reservas RES \n"
                + "INNER JOIN PEDIDO_001 PED ON (RES.NUMERO = PED.NUMERO)\n"
                + "INNER JOIN ENTIDADE_001 ENT ON (ENT.CODCLI = PED.CODCLI)\n"
                + "GROUP BY RES.NUMERO, ENT.NOME, RES.RESERVA, RES.DEPOSITO, RES.STATUS, ped.per_desc, res.ped_agrupador, ped.codrep, ped.pgto";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            reservas.add(new ReservaPedido(
                    resultSetSql.getString("NUMERO"),
                    resultSetSql.getString("RESERVA"),
                    resultSetSql.getString("PED_AGRUPADOR"),
                    resultSetSql.getString("DEPOSITO"),
                    resultSetSql.getString("STATUS"),
                    Integer.parseInt(resultSetSql.getString("QTDE")),
                    resultSetSql.getString("CLIENTE"),
                    resultSetSql.getString("MARCA"),
                    resultSetSql.getString("PER_DESC"),
                    resultSetSql.getString("CODREP"),
                    resultSetSql.getString("PGTO")));
        }
        resultSetSql.close();
        this.closeConnection();

        for (ReservaPedido reserva : reservas) {
            reserva.setListProdutos(FXCollections.observableList(this.loadByPedidoAndReserva(
                    reserva.getStrNumeroPedido(), reserva.getStrNumeroReserva(), reserva.getStrDeposito())));
        }
        return reservas;
    }

    @Override
    public List<ProdutosReservaPedido> loadByPedidoAndReserva(String pPedido, String pReserva, String pDeposito) throws SQLException {
        List<ProdutosReservaPedido> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "WITH reservas AS(\n"
                + "SELECT PEDR.NUMERO, COALESCE(LANR.RESERVA, '0') RESERVA, \n"
                + "        PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO,\n"
                + "        (SELECT POSICAO FROM FAIXA_ITEN_001 WHERE FAIXA = (SELECT FAIXA FROM PRODUTO_001 WHERE CODIGO = PEDR.CODIGO) AND TAMANHO = PEDR.TAM) FAIXA,\n"
                + "        (SELECT FAIXA FROM PRODUTO_001 WHERE CODIGO = PEDR.CODIGO) GP_FAIXA,\n"
                + "        (SELECT DISTINCT \"LOCAL\" FROM PA_ITEN_001 PAI WHERE (PAI.CODIGO = PEDR.CODIGO AND PAI.COR = PEDR.COR AND PAI.TAM = PEDR.TAM AND PAI.QUANTIDADE > 0 AND LOCAL IS NOT NULL AND ROWNUM = 1)) \"LOCAL\"\n"
                + "FROM PED_RESERVA_001 PEDR\n"
                + "     LEFT JOIN SD_RESERVA_PEDIDO_001 LANR ON (LANR.RESERVA = PEDR.SD_RESERVA\n"
                + "                      AND LANR.PEDIDO = PEDR.NUMERO \n"
                + "                      AND LANR.CODIGO = PEDR.CODIGO\n"
                + "                      AND LANR.COR = PEDR.COR\n"
                + "                      AND LANR.TAM = PEDR.TAM\n"
                + "                      AND LANR.DEPOSITO = PEDR.DEPOSITO) \n"
                + "GROUP BY PEDR.NUMERO, LANR.RESERVA, PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO\n"
                + "UNION\n"
                + "SELECT LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO,\n"
                + "(SELECT POSICAO FROM FAIXA_ITEN_001 WHERE FAIXA = (SELECT FAIXA FROM PRODUTO_001 WHERE CODIGO = LANR.CODIGO) AND TAMANHO = LANR.TAM) FAIXA,\n"
                + "(SELECT FAIXA FROM PRODUTO_001 WHERE CODIGO = LANR.CODIGO) GP_FAIXA,\n"
                + "(SELECT DISTINCT \"LOCAL\" FROM PA_ITEN_001 PAI WHERE (PAI.CODIGO = LANR.CODIGO AND PAI.COR = LANR.COR AND PAI.TAM = LANR.TAM AND PAI.QUANTIDADE > 0 AND LOCAL IS NOT NULL AND ROWNUM = 1)) \"LOCAL\"\n"
                + "FROM SD_RESERVA_PEDIDO_001 LANR\n"
                + "GROUP BY LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO)\n"
                + "\n"
                + "SELECT NUMERO, RESERVA, QTDE, COR, TAM, CODIGO, FAIXA, GP_FAIXA, DEPOSITO, CASE WHEN \"LOCAL\" IS NULL THEN 'N/D' ELSE \"LOCAL\" END \"LOCAL\" \n"
                + "FROM reservas WHERE NUMERO = ? AND RESERVA = ? AND DEPOSITO = ? ORDER BY LOCAL, CODIGO, FAIXA";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pReserva);
        preparedStatement.setString(3, pDeposito);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {

            produtos.add(new ProdutosReservaPedido(resultSetSql.getString("CODIGO"), resultSetSql.getString("COR"),
                    resultSetSql.getString("TAM"), Integer.parseInt(resultSetSql.getString("QTDE")),
                    Integer.parseInt(resultSetSql.getString("FAIXA")), resultSetSql.getString("LOCAL"),
                    resultSetSql.getString("DEPOSITO")));
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public String updByPedidoCodigoCorTam(ReservaPedido rpReserva, AcessoSistema usuario) throws SQLException {

        String strAIReserva;
        if (rpReserva.getStrNumeroReserva().equals("0")) {
            strAIReserva = StringUtils.leftPad(getAIReservaPedido(), 10, "0");
        } else {
            strAIReserva = StringUtils.leftPad(rpReserva.getStrNumeroReserva(), 10, "0");
        }
        
        if (rpReserva.getStrPedAgrupador() == null){
            rpReserva.setStrPedAgrupador(rpReserva.getStrNumeroPedido());
        }

        //TODO Alterado aqui
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "MERGE INTO SD_RESERVA_PEDIDO_001 SDRP USING DUAL ON ( \n"
                + " SDRP.PEDIDO = ?\n"
                + " AND SDRP.RESERVA = ?\n"
                + " AND SDRP.CODIGO = ?\n"
                + " AND SDRP.COR = ?\n"
                + " AND SDRP.TAM = ? )\n"
                + "WHEN MATCHED THEN \n"
                + " UPDATE SET \n"
                + " SDRP.STATUS = ?,\n"
                + " SDRP.USUARIO = ?, \n"
                + " SDRP.\"DATA\" = SYSDATE \n"
                + "WHEN NOT MATCHED THEN \n"
                + " INSERT VALUES (?,?,?,?,?,SYSDATE,?,?,?,?,'COM','',0,?)";

        preparedStatement = connection.prepareStatement(query);
        for (ProdutosReservaPedido prdReserva : rpReserva.getListProdutos()) {
            preparedStatement.setString(1, rpReserva.getStrNumeroPedido());
            preparedStatement.setString(2, rpReserva.getStrNumeroReserva());
            preparedStatement.setString(3, prdReserva.getStrCodigo());
            preparedStatement.setString(4, prdReserva.getStrCor());
            preparedStatement.setString(5, prdReserva.getStrTam());
            preparedStatement.setString(6, rpReserva.getStrStatus());
            preparedStatement.setString(7, usuario.getUsuario());
            preparedStatement.setString(8, rpReserva.getStrNumeroPedido());
            preparedStatement.setString(9, strAIReserva);
            preparedStatement.setString(10, prdReserva.getStrCodigo());
            preparedStatement.setString(11, prdReserva.getStrCor());
            preparedStatement.setString(12, prdReserva.getStrTam());
            preparedStatement.setString(13, usuario.getUsuario());
            preparedStatement.setString(14, rpReserva.getStrStatus());
            preparedStatement.setString(15, rpReserva.getStrDeposito());
            preparedStatement.setInt(16, prdReserva.getIntQtde());
            preparedStatement.setString(17, rpReserva.getStrPedAgrupador());
            preparedStatement.execute();
        }

        String query_2 = "UPDATE PED_RESERVA_001 PEDR \n"
                + "	SET PEDR.SD_RESERVA = ?\n"
                + "	WHERE PEDR.NUMERO = ? AND PEDR.CODIGO = ? AND PEDR.COR = ? AND PEDR.TAM = ?\n"
                + "		  AND PEDR.QTDE = ? AND PEDR.DEPOSITO = ?";
        preparedStatement = connection.prepareStatement(query_2);
        for (ProdutosReservaPedido prdReserva : rpReserva.getListProdutos()) {
            preparedStatement.setString(1, strAIReserva);
            preparedStatement.setString(2, rpReserva.getStrNumeroPedido());
            preparedStatement.setString(3, prdReserva.getStrCodigo());
            preparedStatement.setString(4, prdReserva.getStrCor());
            preparedStatement.setString(5, prdReserva.getStrTam());
            preparedStatement.setInt(6, prdReserva.getIntQtde());
            preparedStatement.setString(7, rpReserva.getStrDeposito());
            preparedStatement.execute();
        }
        this.closeConnection();

        return strAIReserva;
    }

    @Override
    public List<ReservaPedido> loadByFormReservaPedido(String pPedido, String pReserva, String pCliente, String pStatus) throws SQLException {
        List<ReservaPedido> reservas = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "WITH reservas AS(\n"
                + "SELECT PEDR.NUMERO, COALESCE(LANR.RESERVA, '0') RESERVA, \n"
                + "        PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO,\n"
                + "        COALESCE(LANR.STATUS, 'A') STATUS,\n"
                + "        NVL(LANR.PED_AGRUPADOR, PEDR.NUMERO) PED_AGRUPADOR\n"
                + "FROM PED_RESERVA_001 PEDR\n"
                + "LEFT JOIN SD_RESERVA_PEDIDO_001 LANR ON (LANR.RESERVA = PEDR.SD_RESERVA\n"
                + "                      AND LANR.PEDIDO = PEDR.NUMERO \n"
                + "                      AND LANR.CODIGO = PEDR.CODIGO\n"
                + "                      AND LANR.COR = PEDR.COR\n" + "                      AND LANR.TAM = PEDR.TAM\n"
                + "                      AND LANR.DEPOSITO = PEDR.DEPOSITO) \n"
                + "GROUP BY PEDR.NUMERO, LANR.RESERVA, PEDR.QTDE, PEDR.COR, PEDR.TAM, PEDR.CODIGO, PEDR.DEPOSITO,LANR.STATUS, LANR.PED_AGRUPADOR\n"
                + "UNION\n"
                + "SELECT LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO, LANR.STATUS, LANR.PED_AGRUPADOR \n"
                + "FROM SD_RESERVA_PEDIDO_001 LANR\n"
                + "GROUP BY LANR.PEDIDO, LANR.RESERVA, LANR.QTDE, LANR.COR, LANR.TAM, LANR.CODIGO, LANR.DEPOSITO,LANR.STATUS, LANR.PED_AGRUPADOR)\n"
                + "\n"
                + "SELECT RES.NUMERO, ENT.NOME || ' [' || ent.codcli || ']' CLIENTE, RES.RESERVA, RES.DEPOSITO, RES.STATUS, SUM(RES.QTDE) QTDE, f_get_marca_pedido(res.numero) marca, ped.per_desc, res.ped_agrupador, ped.codrep, ped.pgto \n"
                + "FROM reservas RES \n" + "INNER JOIN PEDIDO_001 PED ON (RES.NUMERO = PED.NUMERO)\n"
                + "INNER JOIN ENTIDADE_001 ENT ON (ENT.CODCLI = PED.CODCLI)\n"
                + "WHERE RES.NUMERO LIKE ? AND RES.RESERVA LIKE ? AND RES.STATUS LIKE ? AND ENT.NOME LIKE ?\n"
                + "GROUP BY RES.NUMERO, ENT.NOME, ent.codcli, RES.RESERVA, RES.DEPOSITO, RES.STATUS, ped.per_desc, res.ped_agrupador, ped.codrep, ped.pgto";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, "%" + pPedido.trim());
        preparedStatement.setString(2, "%" + pReserva.trim());
        preparedStatement.setString(3, "%" + pStatus.trim());
        preparedStatement.setString(4, "%" + pCliente.trim() + "%");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            reservas.add(new ReservaPedido(
                    resultSetSql.getString("NUMERO"),
                    resultSetSql.getString("RESERVA"),
                    resultSetSql.getString("PED_AGRUPADOR"),
                    resultSetSql.getString("DEPOSITO"),
                    resultSetSql.getString("STATUS"),
                    Integer.parseInt(resultSetSql.getString("QTDE")),
                    resultSetSql.getString("CLIENTE"),
                    resultSetSql.getString("MARCA"),
                    resultSetSql.getString("PER_DESC"),
                    resultSetSql.getString("CODREP"),
                    resultSetSql.getString("PGTO")));
        }
        resultSetSql.close();
        this.closeConnection();

        for (ReservaPedido reserva : reservas) {
            reserva.setListProdutos(FXCollections.observableList(this.loadByPedidoAndReserva(
                    reserva.getStrNumeroPedido(), reserva.getStrNumeroReserva(), reserva.getStrDeposito())));
        }
        return reservas;
    }

    @Override
    public List<String> getMarcasReserva(ReservaPedido rpReserva) throws SQLException {
        List<String> marcas = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "WITH RESERVA_PEDIDO_CLIENTE AS (\n"
                + "SELECT ENT.SIT_CLI, PED.COLECAO, REGEXP_REPLACE(MAR.DESCRICAO, 'MOOVE ','') MARCA \n"
                + "FROM PED_RESERVA_001 PRES\n" + "INNER JOIN PEDIDO_001 PED ON (PED.NUMERO = PRES.NUMERO)\n"
                + "INNER JOIN ENTIDADE_001 ENT ON (ENT.CODCLI = PED.CODCLI)\n"
                + "INNER JOIN PRODUTO_001 PROD ON (PROD.CODIGO = PRES.CODIGO)\n"
                + "INNER JOIN MARCA_001 MAR ON (MAR.CODIGO = PROD.MARCA)\n"
                + "WHERE PRES.NUMERO = ?\n"
                + "GROUP BY ENT.SIT_CLI, PED.COLECAO, REGEXP_REPLACE(MAR.DESCRICAO, 'MOOVE ',''))\n"
                + "SELECT \n"
                + "    SIT_CLI, COLECAO, \n"
                + "    SUM(CASE WHEN MARCA = 'DLZ' THEN 1 ELSE 0 END) HAS_DLZ, \n"
                + "    SUM(CASE WHEN MARCA = 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_FLOR,\n"
                + "    SUM(CASE WHEN MARCA <> 'DLZ' AND MARCA <> 'FLOR DE LIS' THEN 1 ELSE 0 END) HAS_OUTROS\n"
                + "FROM RESERVA_PEDIDO_CLIENTE\n"
                + "GROUP BY SIT_CLI , COLECAO";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, rpReserva.getStrNumeroPedido());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            marcas.add(resultSetSql.getString("SIT_CLI"));
            marcas.add(resultSetSql.getString("COLECAO"));
            if (resultSetSql.getString("HAS_DLZ").equals("1")) {
                marcas.add("DLZ");
            }
            if (resultSetSql.getString("HAS_FLOR").equals("1")) {
                marcas.add("FLOR DE LIS");
            }
        }
        resultSetSql.close();
        this.closeConnection();

        return marcas;
    }

    @Override
    public void updStatusByReservaPedido(ReservaPedido rpReserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("UPDATE SD_RESERVA_PEDIDO_001 SET STATUS = ? WHERE PED_AGRUPADOR = ? AND RESERVA = ?");
        preparedStatement.setString(1, rpReserva.getStrStatus());
        preparedStatement.setString(2, rpReserva.getStrPedAgrupador());
        preparedStatement.setString(3, rpReserva.getStrNumeroReserva());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void saveMktReservaPedido(ReservaPedido rpReserva, List<ProdutosReservaPedido> pMkts, AcessoSistema usuario) throws SQLException {
        saveMktReservaPedido(rpReserva, pMkts, usuario, "");
    }

    @Override
    public void saveMktReservaPedido(ReservaPedido rpReserva, List<ProdutosReservaPedido> pMkts, AcessoSistema usuario, String usuarioLeitor) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "INSERT INTO SD_RESERVA_PEDIDO_001\n"
                + "(PEDIDO, RESERVA, CODIGO, COR, TAM, \"DATA\", USUARIO, STATUS, DEPOSITO, QTDE, TIPO, USUARIO_LEITURA, PED_AGRUPADOR)\n"
                + "VALUES(?, ?, ?, 'UN', 'UN', SYSDATE, ?, 'I', '0005', ?, 'MKT', ?, ?)";
        preparedStatement = connection.prepareStatement(query);
        for (ProdutosReservaPedido prdReserva : pMkts) {
            preparedStatement.setString(1, rpReserva.getStrNumeroPedido());
            preparedStatement.setString(2, rpReserva.getStrNumeroReserva());
            preparedStatement.setString(3, prdReserva.getStrCodigo());
            preparedStatement.setString(4, usuario.getUsuario());
            preparedStatement.setInt(5, prdReserva.getIntQtde());
            preparedStatement.setString(6, usuarioLeitor);
            preparedStatement.setString(7, rpReserva.getStrPedAgrupador());

            preparedStatement.execute();
        }
        this.closeConnection();
    }

    private List<ProdutosReservaPedido> loadProdutosFromLancByPedidoAndReserva(String pPedido, String pReserva) throws SQLException {
        List<ProdutosReservaPedido> produtos = new ArrayList<>();

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = ""
                + "WITH PRODUTOS_RESERVA AS (SELECT res.pedido, res.reserva, RES.CODIGO, RES.COR, RES.TAM, RES.DEPOSITO,\n"
                + "		(SELECT POSICAO FROM FAIXA_ITEN_001 WHERE FAIXA = (SELECT FAIXA FROM PRODUTO_001 WHERE CODIGO = RES.CODIGO) AND TAMANHO = RES.TAM) FAIXA,\n"
                + "     (SELECT DISTINCT \"LOCAL\" FROM PA_ITEN_001 PAI WHERE (PAI.CODIGO = RES.CODIGO AND PAI.COR = RES.COR AND PAI.TAM = RES.TAM AND PAI.QUANTIDADE > 0 AND LOCAL IS NOT NULL AND ROWNUM = 1)) \"LOCAL\",\n"
                + "     RES.QTDE, (RES.QTDE - COUNT(CAI.BARRA)) SALDO\n"
                + "FROM SD_RESERVA_PEDIDO_001 RES\n"
                + "LEFT JOIN SD_CAIXA_RESERVA_PEDIDO_001 CAI ON (CAI.CODIGO = RES.CODIGO AND CAI.COR = RES.COR AND CAI.TAM = RES.TAM AND CAI.PEDIDO = RES.PEDIDO AND CAI.RESERVA = RES.RESERVA)\n"
                + "WHERE RES.PED_AGRUPADOR = ? AND RES.RESERVA = ?\n"
                + "GROUP BY res.pedido, res.reserva, RES.CODIGO, RES.COR, RES.TAM, RES.QTDE, RES.DEPOSITO)\n"
                + "SELECT pedido, reserva, CODIGO, COR, TAM, FAIXA, CASE WHEN \"LOCAL\" IS NULL THEN 'N/D' ELSE \"LOCAL\" END \"LOCAL\", \n"
                + "DEPOSITO, QTDE, SALDO FROM PRODUTOS_RESERVA ORDER BY CODIGO, COR, FAIXA";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pPedido);
        preparedStatement.setString(2, pReserva);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            ProdutosReservaPedido produto = new ProdutosReservaPedido(
                    resultSetSql.getString("CODIGO"),
                    resultSetSql.getString("COR"),
                    resultSetSql.getString("TAM"),
                    Integer.parseInt(resultSetSql.getString("QTDE")),
                    Integer.parseInt(resultSetSql.getString("FAIXA")),
                    resultSetSql.getString("LOCAL"),
                    resultSetSql.getString("DEPOSITO"),
                    Integer.parseInt(resultSetSql.getString("SALDO")));
            produto.setStrPedido(resultSetSql.getString("PEDIDO"));
            produto.setStrReserva(resultSetSql.getString("RESERVA"));
            produtos.add(produto);
        }
        resultSetSql.close();
        this.closeConnection();
        return produtos;
    }

    @Override
    public ReservaPedido getFromLancByPedidoAndReserva(String pPedido, String pReserva) throws SQLException {
        ReservaPedido reserva = null;

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "SELECT RES.PED_AGRUPADOR PEDIDO,\n" +
                "       RES.RESERVA,\n" +
                "       RES.STATUS,\n" +
                "       cli.NOME || ' [' || cli.codcli || ']'  CLIENTE,\n" +
                "       CASE WHEN TRANSP.NOME IS NULL THEN 'NAO DEFINIDA' ELSE TRANSP.NOME END TRANSP,\n" +
                "       SUM(RES.QTDE) QTDE,\n" +
                "       SUM(CASE WHEN PROD.UNICO_CAIXA = 'S' THEN 1 ELSE 0 END) UNICO_CAIXA,\n" +
                "       f_get_marca_pedido(res.ped_agrupador) marca,\n" +
                "       ped.per_desc,\n " +
                "       res.ped_agrupador,\n " +
                "       ped.codrep,\n " +
                "       ped.pgto\n " +
                "  FROM SD_RESERVA_PEDIDO_001 RES\n" +
                " INNER JOIN SD_PRODUTO_001 PROD\n" +
                "    ON PROD.CODIGO = RES.CODIGO\n" +
                " INNER JOIN PEDIDO_001 PED\n" +
                "    ON (PED.NUMERO = RES.PEDIDO)\n" +
                " INNER JOIN ENTIDADE_001 CLI\n" +
                "    ON (CLI.CODCLI = PED.CODCLI)\n" +
                "  LEFT JOIN TABTRAN_001 TRANSP\n" +
                "    ON (TRANSP.CODIGO = PED.TAB_TRANS)\n" +
                " WHERE RES.PED_AGRUPADOR = ?\n" +
                "   AND RES.RESERVA = ?\n" +
                " GROUP BY RES.PED_AGRUPADOR, RES.RESERVA, RES.STATUS, CLI.NOME, cli.codcli, TRANSP.NOME, ped.per_desc, ped.codrep, res.ped_agrupador, ped.pgto";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, pPedido.trim());
        preparedStatement.setString(2, pReserva.trim());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            reserva = new ReservaPedido(
                    resultSetSql.getString("PEDIDO"),
                    resultSetSql.getString("RESERVA"),
                    resultSetSql.getString("PED_AGRUPADOR"),
                    "0000",
                    resultSetSql.getString("STATUS"),
                    Integer.parseInt(resultSetSql.getString("QTDE")),
                    resultSetSql.getString("CLIENTE"),
                    resultSetSql.getString("MARCA"),
                    resultSetSql.getString("PER_DESC"),
                    resultSetSql.getString("TRANSP"),
                    (resultSetSql.getInt("UNICO_CAIXA") > 0 ? "RESERVA COM PRODUTOS ÚNICOS NA CAIXA." : ""),
                    resultSetSql.getString("CODREP"),
                    resultSetSql.getString("PGTO"));
        }
        resultSetSql.close();
        this.closeConnection();
        if (reserva != null) {
            reserva.setListProdutos(FXCollections.observableList(this.loadProdutosFromLancByPedidoAndReserva(
                    reserva.getStrNumeroPedido(),
                    reserva.getStrNumeroReserva())));
        }

        return reserva;
    }

    @Override
    public void lockPedido(ReservaPedido reserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("UPDATE PEDIDO_001 SET LOCADO = 'EXPEDICAO' WHERE NUMERO = ?");
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void unlockPedido(ReservaPedido reserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("UPDATE PEDIDO_001 SET LOCADO = '0' WHERE NUMERO = ?");
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void deleteReservaPedido(ReservaPedido pReserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("DELETE FROM SD_RESERVA_PEDIDO_001 WHERE PEDIDO = ? AND RESERVA = ? AND STATUS = 'I'");
        preparedStatement.setString(1, pReserva.getStrNumeroPedido());
        preparedStatement.setString(2, pReserva.getStrNumeroReserva());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void deleteReservaPedido_PED_RESERVA(ReservaPedido pReserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("DELETE FROM PED_RESERVA_001 WHERE NUMERO = ? AND SD_RESERVA = ?");
        preparedStatement.setString(1, pReserva.getStrNumeroPedido());
        preparedStatement.setString(2, pReserva.getStrNumeroReserva());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void limpaReservaPedido_PED_RESERVA(ReservaPedido pReserva) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement("UPDATE PED_RESERVA_001 SET SD_RESERVA = null WHERE NUMERO = ? AND SD_RESERVA = ?");
        preparedStatement.setString(1, pReserva.getStrNumeroPedido());
        preparedStatement.setString(2, pReserva.getStrNumeroReserva());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void deleteReferenciaReserva(ReservaPedido reserva, ProdutosReservaPedido produto) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "DELETE FROM SD_RESERVA_PEDIDO_001 WHERE PEDIDO = ? AND RESERVA = ? AND CODIGO = ? AND COR = ? AND TAM = ? ";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroReserva());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();

        this.closeConnection();
    }

    @Override
    public void deleteReferenciaReserva_PED_RESERVA(ReservaPedido reserva, ProdutosReservaPedido produto) throws SQLException {
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        String query = "DELETE FROM PED_RESERVA_001 WHERE NUMERO = ? AND SD_RESERVA = ? AND CODIGO = ? AND COR = ? AND TAM = ? ";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, reserva.getStrNumeroPedido());
        preparedStatement.setString(2, reserva.getStrNumeroReserva());
        preparedStatement.setString(3, produto.getStrCodigo());
        preparedStatement.setString(4, produto.getStrCor());
        preparedStatement.setString(5, produto.getStrTam());
        preparedStatement.execute();

        this.closeConnection();
    }

}
