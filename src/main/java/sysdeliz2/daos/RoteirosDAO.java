/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.ObservableList;
import sysdeliz2.models.properties.Roteiros;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public interface RoteirosDAO {

    public ObservableList<Roteiros> getAll() throws SQLException;

    public ObservableList<Roteiros> getAllAbertos() throws SQLException;

    public ObservableList<Roteiros> getUser(String user) throws SQLException;

    public ObservableList<Roteiros> getByForm(String dt_solicitacao, String tipo, String fornecedor, String cidade, String mercadoria) throws SQLException;

    public ObservableList<Roteiros> getByFormDtRoteiro(String dt_roteiro, String tipo, String fornecedor, String cidade, String mercadoria) throws SQLException;

    public ObservableList<Roteiros> getByFormUser(String dt_roteiro, String tipo, String fornecedor, String cidade, String mercadoria, String usuario, String status) throws SQLException;

    public String save(Roteiros roteiro) throws SQLException;

    public void update(Roteiros roteiro) throws SQLException;

    public void updateStatus(Roteiros roteiro) throws SQLException;

    public void delete(Roteiros roteiro) throws SQLException;
}
