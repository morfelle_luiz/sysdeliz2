/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.SdAgendaServico001;
import sysdeliz2.models.properties.GestaoCompra00X;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdAgendaServico001DAO extends FactoryConnection {

    public SdAgendaServico001DAO() throws SQLException {
        this.initConnection();
    }

    private String whereFilter;

    public ObservableList<SdAgendaServico001> getCrmOrdemCompraMaterial(GestaoCompra00X ordemCompra) throws SQLException {
        ObservableList<SdAgendaServico001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select to_char(dt_cadastro, 'DD/MM/YYYY HH24:MI:SS'), to_char(dt_agenda, 'DD/MM/YYYY'), to_char(dt_agenda, 'HH24:MI'),\n"
                + "       tipo, campo1, campo2, campo3, usuario, to_char(dt_agenda, 'DD/MM/YYYY HH24:MI:SS')\n"
                + "  from sd_agenda_servico_001\n"
                + " where campo1 = ? and campo2 = ?\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, ordemCompra.getNumero());
        super.preparedStatement.setString(2, ordemCompra.getCodcli());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdAgendaServico001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    null, null,
                    resultSetSql.getString(8),
                    resultSetSql.getString(9)));
        }
        resultSetSql.close();
        super.closeConnection();
        return rows;
    }

    public void save(SdAgendaServico001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_agenda_servico_001\n"
                + "   (dt_cadastro, dt_agenda, tipo, campo1, campo2, campo3, campo4, campo5, usuario)\n"
                + " values\n"
                + "   (sysdate, to_date(?,'DD/MM/YYYY HH24:MI'), ?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getDtAgendaAsString() + " " + objectToSave.getHrAgenda());
        super.preparedStatement.setString(2, objectToSave.getTipo());
        super.preparedStatement.setString(3, objectToSave.getCampo1());
        super.preparedStatement.setString(4, objectToSave.getCampo2());
        super.preparedStatement.setString(5, objectToSave.getCampo3());
        super.preparedStatement.setString(6, objectToSave.getCampo4());
        super.preparedStatement.setString(7, objectToSave.getCampo5());
        super.preparedStatement.setString(8, SceneMainController.getAcesso().getUsuario());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();

    }

    public void delete(SdAgendaServico001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_agenda_servico_001 where campo1 = ? and campo2 = ? and to_date(to_char(dt_agenda,'DD/MM/YYYY'),'DD/MM/YYYY') = to_date(?,'DD/MM/YYYY')");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getCampo1());
        super.preparedStatement.setString(2, objectToSave.getCampo2());
        super.preparedStatement.setString(3, objectToSave.getDtAgendaAsString());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

}
