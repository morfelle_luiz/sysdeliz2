/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.models.SdAlteracaoOc001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

/**
 * @author cristiano.diego
 */
public class SdAlteracaoOc001DAO extends FactoryConnection {

    public SdAlteracaoOc001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdAlteracaoOc001> getAll() throws SQLException {
        ObservableList<SdAlteracaoOc001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from sd_alteracao_oc_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdAlteracaoOc001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getTimestamp(8).toLocalDateTime(),
                    resultSetSql.getDate(9).toLocalDate(),
                    resultSetSql.getDate(10).toLocalDate()));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdAlteracaoOc001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "merge into sd_alteracao_oc_001 alt\n"
                + "using dual\n"
                + "on (alt.numero = ? and alt.codigo = ? and alt.cor = ? and alt.deposito = ? and alt.ordem = ?)\n"
                + "when matched then\n"
                + "  update set alt.dt_alteracao = to_date(?, 'DD/MM/YYYY HH24:MI:SS'), alt.dt_novo = to_date(?, 'DD/MM/YYYY')\n"
                + "when not matched then\n"
                + "  insert\n"
                + "  values\n"
                + "    (?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     SYSDATE,\n"
                + "     to_date(?, 'DD/MM/YYYY HH24:MI'),\n"
                + "     to_date(?, 'DD/MM/YYYY'))");

        super.initPreparedStatement(query.toString());

        super.preparedStatement.setString(1, objectToSave.getNumero());
        super.preparedStatement.setString(2, objectToSave.getCodigo());
        super.preparedStatement.setString(3, objectToSave.getCor());
        super.preparedStatement.setString(4, objectToSave.getDeposito());
        super.preparedStatement.setInt(5, objectToSave.getOrdem());

        super.preparedStatement.setString(6, objectToSave.getDtAtualizacao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        super.preparedStatement.setString(7, objectToSave.getDtNovo().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        super.preparedStatement.setString(8, objectToSave.getNumero());
        super.preparedStatement.setString(9, objectToSave.getCodigo());
        super.preparedStatement.setString(10, objectToSave.getCor());
        super.preparedStatement.setString(11, objectToSave.getTipo());
        super.preparedStatement.setString(12, objectToSave.getStatus());
        super.preparedStatement.setString(13, objectToSave.getDeposito());
        super.preparedStatement.setInt(14, objectToSave.getOrdem());
        //Concatenado a ordem do material na data anterior para incluir como chave primária a ordem e não interferir no join com o MRP
        super.preparedStatement.setString(15, objectToSave.getDtAnterior().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " 00:" + StringUtils.leftPad(objectToSave.getOrdem() + "", 2, "0"));
        super.preparedStatement.setString(16, objectToSave.getDtNovo().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        super.closeConnection();
    }

}
