/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdSetorOp001;
import sysdeliz2.utils.GUIUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SdCelula001DAO extends FactoryConnection {

    public SdCelula001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdCelula> getAll() throws SQLException {
        ObservableList<SdCelula> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo")));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(celula -> {
            try {
                celula.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(celula.getCodigo()));
                celula.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(celula.getCodigo()));
                celula.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(celula.getCodigo()));
                celula.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(celula.getTurnos().stream()
                        .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                        .collect(Collectors.toList())));
            } catch (SQLException ex) {
                Logger.getLogger(SdCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    public ObservableList<SdCelula> getAll(Integer setorOp) throws SQLException {
        ObservableList<SdCelula> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and sop.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, setorOp);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo")));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(celula -> {
            try {
                celula.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(celula.getCodigo()));
                celula.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(celula.getCodigo()));
                celula.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(celula.getCodigo()));
                celula.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(celula.getTurnos().stream()
                        .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                        .collect(Collectors.toList())));
            } catch (SQLException ex) {
                Logger.getLogger(SdCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    public void save(SdCelula objectToSave) throws SQLException {
        String[] returnId = {"CODIGO"};
        StringBuilder query = new StringBuilder(""
                + " insert into sd_celula_001\n"
                + "   (descricao, lider, setor_op, qtde_operadores, ativo)\n"
                + " values\n"
                + "   (?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString(), returnId);
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setInt(2, objectToSave.getCodigoLider());
        super.preparedStatement.setInt(3, objectToSave.getCodigoSetorOp());
        super.preparedStatement.setInt(4, objectToSave.getQtdeOperadores());
        super.preparedStatement.setString(5, objectToSave.isAtivo() ? "S" : "N");
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
        if (resultSetSql.next()) {
            objectToSave.setCodigo(resultSetSql.getInt(1));
        }
        super.closeConnection();

        objectToSave.getFamilias().forEach(familyGroup -> {
            familyGroup.setCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdFamiliasCelula001DAO().save(familyGroup);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        objectToSave.getColaboradores().forEach(colaborador -> {
            colaborador.setCodigoCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdColabCelula001DAO().save(colaborador);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        objectToSave.getTurnos().forEach(turno -> {
            turno.setCodigoCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdTurnoCelula001DAO().save(turno);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
    }

    public void update(SdCelula objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_celula_001\n"
                        + "   set descricao       = ?,\n"
                        + "       lider           = ?,\n"
                        + "       setor_op        = ?,\n"
                        + "       qtde_operadores = ?,\n"
                        + "       ativo           = ?\n"
                        + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setInt(2, objectToSave.getCodigoLider());
        super.preparedStatement.setInt(3, objectToSave.getCodigoSetorOp());
        super.preparedStatement.setInt(4, objectToSave.getQtdeOperadores());
        super.preparedStatement.setString(5, objectToSave.isAtivo() ? "S" : "N");
        super.preparedStatement.setInt(6, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();

        DAOFactory.getSdFamiliasCelula001DAO().deleteCelula(objectToSave.getCodigo());
        DAOFactory.getSdColabCelula001DAO().deleteCelula(objectToSave.getCodigo());
        DAOFactory.getSdTurnoCelula001DAO().deleteCelula(objectToSave.getCodigo());
        objectToSave.getFamilias().forEach(familyGroup -> {
            familyGroup.setCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdFamiliasCelula001DAO().save(familyGroup);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        objectToSave.getColaboradores().forEach(colaborador -> {
            colaborador.setCodigoCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdColabCelula001DAO().save(colaborador);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
        objectToSave.getTurnos().forEach(turno -> {
            turno.setCodigoCelula(objectToSave.getCodigo());
            try {
                DAOFactory.getSdTurnoCelula001DAO().save(turno);
            } catch (SQLException ex) {
                Logger.getLogger(SdGrupoOperacao001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });
    }

    public void delete(SdCelula objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "begin\n"
                + " delete from sd_familias_celula_001 where celula = ?;\n"
                + " delete from sd_colab_celula_001 where celula = ?;\n"
                + " delete from sd_turno_celula_001 where celula = ?;\n"
                + " delete from sd_celula_001 where codigo = ?;\n"
                + "end;");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigo());
        super.preparedStatement.setInt(2, objectToSave.getCodigo());
        super.preparedStatement.setInt(3, objectToSave.getCodigo());
        super.preparedStatement.setInt(4, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }

    public ObservableList<SdCelula> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdCelula> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and cel.descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo")));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(celula -> {
            try {
                celula.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(celula.getCodigo()));
                celula.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(celula.getCodigo()));
                celula.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(celula.getCodigo()));
                celula.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(celula.getTurnos().stream()
                        .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                        .collect(Collectors.toList())));
            } catch (SQLException ex) {
                Logger.getLogger(SdCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    public ObservableList<SdCelula> getByDefault(String valueDefault, Integer setorOp) throws SQLException {
        ObservableList<SdCelula> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and sop.codigo = ?\n" +
                "   and cel.descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, setorOp);
        super.preparedStatement.setString(2, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo")));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(celula -> {
            try {
                celula.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(celula.getCodigo()));
                celula.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(celula.getCodigo()));
                celula.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(celula.getCodigo()));
                celula.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(celula.getTurnos().stream()
                        .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                        .collect(Collectors.toList())));
            } catch (SQLException ex) {
                Logger.getLogger(SdCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    //--------------------------------------------------------------------------
    public ObservableList<SdCelula> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdCelula> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo")));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(celula -> {
            try {
                celula.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(celula.getCodigo()));
                celula.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(celula.getCodigo()));
                celula.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(celula.getCodigo()));
                celula.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(celula.getTurnos().stream()
                        .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                        .collect(Collectors.toList())));
            } catch (SQLException ex) {
                Logger.getLogger(SdCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    public SdCelula getByCode(Integer code) throws SQLException {
        SdCelula row = null;

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and cel.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo"));
        }
        resultSetSql.close();
        super.closeConnection();
        if (row != null) {
            row.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(row.getCodigo()));
            row.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(row.getCodigo()));
            row.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(row.getCodigo()));
            row.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(row.getTurnos().stream()
                    .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                    .collect(Collectors.toList())));
        }

        return row;
    }

    public SdCelula getByCode(Integer code, Integer setor) throws SQLException {
        SdCelula row = null;

        StringBuilder query = new StringBuilder("" +
                "select cel.codigo cel_codigo,\n" +
                "       cel.descricao cel_descricao,\n" +
                "       cel.lider cel_lider,\n" +
                "       cel.setor_op cel_setor_op,\n" +
                "       cel.ativo cel_ativo,\n" +
                "       cel.qtde_operadores cel_qtde_operadores,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       sop.codigo sop_codigo,\n" +
                "       sop.descricao sop_descricao,\n" +
                "       sop.ativo sop_ativo \n" +
                "  from sd_celula_001 cel, sd_colaborador_001 col, sd_setor_op_001 sop\n" +
                " where cel.lider = col.codigo\n" +
                "   and cel.setor_op = sop.codigo\n" +
                "   and cel.codigo = ?\n" +
                "   and sop.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.setInt(2, setor);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdCelula(resultSetSql.getInt("cel_codigo"),
                    resultSetSql.getString("cel_descricao"),
                    resultSetSql.getInt("cel_lider"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    resultSetSql.getInt("cel_setor_op"),
                    new SdSetorOp001(
                            resultSetSql.getInt("sop_codigo"),
                            resultSetSql.getString("sop_descricao"),
                            resultSetSql.getString("sop_ativo")),
                    resultSetSql.getInt("cel_qtde_operadores"),
                    resultSetSql.getString("cel_ativo"));
        }
        resultSetSql.close();
        super.closeConnection();
        if (row != null) {
            row.setFamilias(DAOFactory.getSdFamiliasCelula001DAO().getByCell(row.getCodigo()));
            row.setColaboradores(DAOFactory.getSdColabCelula001DAO().getByCell(row.getCodigo()));
            row.setTurnos(DAOFactory.getSdTurnoCelula001DAO().getByCelula(row.getCodigo()));
            row.setIntervalos(DAOFactory.getSdTurnoDAO().getIntervalosInterjornadasTurno(row.getTurnos().stream()
                    .map(turno -> turno.codigoTurnoProperty().getValue().toString())
                    .collect(Collectors.toList())));
        }

        return row;
    }
}
