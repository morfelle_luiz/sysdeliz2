/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColabCelula001;
import sysdeliz2.models.sysdeliz.SdColaborador;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdColabCelula001DAO extends FactoryConnection {

    public SdColabCelula001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdColabCelula001> getAll() throws SQLException {
        ObservableList<SdColabCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select ccel.celula ccel_celula,\n" +
                "       ccel.colaborador ccel_colaborador,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo\n" +
                "  from sd_colab_celula_001 ccel\n" +
                " inner join sd_colaborador_001 col\n" +
                "    on col.codigo = ccel.colaborador");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColabCelula001(
                    resultSetSql.getInt("ccel_celula"),
                    resultSetSql.getInt("ccel_colaborador"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo"))
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdColabCelula001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_colab_celula_001\n"
                + "   (celula, colaborador)\n"
                + " values\n"
                + "   (?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoCelula());
        super.preparedStatement.setInt(2, objectToSave.getCodigoColaborador());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
    }

    public void update(SdColabCelula001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_colaborador_001\n"
//                + "   set nome        = ?,\n"
//                + "       funcao      = ?,\n"
//                + "       turno       = ?,\n"
//                + "       codigo_rh   = ?,\n"
//                + "       ativo       = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getNome());
//        super.preparedStatement.setInt(2, objectToSave.getCodigoFuncao());
//        super.preparedStatement.setInt(3, objectToSave.getCodigoTurno());
//        super.preparedStatement.setInt(4, objectToSave.getCodigoRH());
//        super.preparedStatement.setString(5, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(6, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
//
//        DAOFactory.getSdPolivalencia001DAO().deletePerson(objectToSave);
//        objectToSave.getOperacoes().forEach(operacao -> {
//            operacao.setCodigoColaborador(objectToSave.getCodigo());
//            try {
//                DAOFactory.getSdPolivalencia001DAO().save(operacao);
//            } catch (SQLException ex) {
//                LoggerExtensions.getLogger(SdColabCelula001DAO.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        });

    }

    public void delete(SdColabCelula001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "begin\n"
                + " delete from sd_colab_celula_001 where celula = ? and colaborador = ?;\n"
                + "end;");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoCelula());
        super.preparedStatement.setInt(2, objectToSave.getCodigoColaborador());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void deleteCelula(Integer cellCode) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "begin\n"
                + " delete from sd_colab_celula_001 where celula = ?;\n"
                + "end;");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, cellCode);
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }

    public ObservableList<SdColabCelula001> getByCell(Integer codeCell) throws SQLException {
        ObservableList<SdColabCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select ccel.celula ccel_celula,\n" +
                "       ccel.colaborador ccel_colaborador,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo\n" +
                "  from sd_colab_celula_001 ccel\n" +
                " inner join sd_colaborador_001 col\n" +
                "    on col.codigo = ccel.colaborador\n" +
                " where ccel.celula = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codeCell);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColabCelula001(
                    resultSetSql.getInt("ccel_celula"),
                    resultSetSql.getInt("ccel_colaborador"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo"))
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public SdColabCelula001 getByPerson(Integer codePerson) throws SQLException {
        SdColabCelula001 row = null;

        StringBuilder query = new StringBuilder("" +
                "select ccel.celula ccel_celula,\n" +
                "       ccel.colaborador ccel_colaborador,\n" +
                "       cel.codigo cel_codigo,\n" +
                "       cel.descricao  cel_descricao,\n" +
                "       cel.lider  cel_lider,\n" +
                "       cel.setor_op  cel_setor_op,\n" +
                "       cel.ativo  cel_ativo,\n" +
                "       cel.qtde_operadores  cel_qtde_operadores\n" +
                "  from sd_colab_celula_001 ccel\n" +
                " inner join sd_celula_001 cel\n" +
                "    on cel.codigo = ccel.celula\n" +
                " where ccel.colaborador = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codePerson);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdColabCelula001(
                    resultSetSql.getInt("ccel_celula"),
                    resultSetSql.getInt("ccel_colaborador"),
                    new SdCelula(
                            resultSetSql.getInt("cel_codigo"),
                            resultSetSql.getString("cel_descricao"),
                            resultSetSql.getInt("cel_lider"),
                            null,
                            resultSetSql.getInt("cel_setor_op"),
                            null,
                            resultSetSql.getInt("cel_qtde_operadores"),
                            resultSetSql.getString("cel_ativo"))
            );
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public ObservableList<SdColabCelula001> getAvailablePersons(Integer cellCode) throws SQLException {
        ObservableList<SdColabCelula001> rows = FXCollections.observableArrayList();

        DAOFactory.getSdColaborador001DAO().getNotInCell().forEach(colaborador -> {
            rows.add(new SdColabCelula001(cellCode,
                    colaborador.getCodigo(),
                    colaborador));
        });

        return rows;
    }
}
