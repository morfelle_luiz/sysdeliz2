/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdFuncao001;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdTurno;
import sysdeliz2.models.view.ViewOcupacaoColaborador;
import sysdeliz2.utils.GUIUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author cristiano.diego
 */
public class SdColaborador001DAO extends FactoryConnection {

    public SdColaborador001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdColaborador> getAll() throws SQLException {
        ObservableList<SdColaborador> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col, sd_funcao_001 fun, sd_turno_001 tur\n" +
                " where col.funcao = fun.codigo\n" +
                "   and col.turno = tur.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime())));
        }
        resultSetSql.close();
        super.closeConnection();

//        rows.forEach(row -> {
//            try {
//                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
//            } catch (SQLException ex) {
//                GUIUtils.showException(ex);
//            }
//        });
        return rows;
    }

    public void save(SdColaborador objectToSave) throws SQLException {
        String[] returnId = {"CODIGO"};
        StringBuilder query = new StringBuilder(""
                + " insert into sd_colaborador_001\n"
                + "   (nome, funcao, turno, codigo_rh, ativo, usuario, codfor, impressora_padrao, entrada_direta, perdidas_ativas, incompletas_ativas, piso_coleta, segundo_coletor)\n"
                + " values\n"
                + "   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString(), returnId);
        super.preparedStatement.setString(1, objectToSave.getNome());
        super.preparedStatement.setInt(2, objectToSave.getCodigoFuncao());
        super.preparedStatement.setInt(3, objectToSave.getCodigoTurno());
        super.preparedStatement.setInt(4, objectToSave.getCodigoRH());
        super.preparedStatement.setString(5, objectToSave.isAtivo() ? "S" : "N");
        super.preparedStatement.setString(6, objectToSave.getUsuario());
        super.preparedStatement.setString(7, objectToSave.getCodfor());
        super.preparedStatement.setString(8, objectToSave.isImpressoraPadrao() ? "S" : "N");
        super.preparedStatement.setString(9, objectToSave.isEntradaDireta() ? "S" : "N");
        super.preparedStatement.setString(10, objectToSave.isPerdidasAtivas() ? "S" : "N");
        super.preparedStatement.setString(11, objectToSave.isIncompletasAtivas() ? "S" : "N");
        super.preparedStatement.setInt(12, objectToSave.getPisoColeta());
        super.preparedStatement.setString(13, objectToSave.isSegundoColetor() ? "S" : "N");

        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
        if (resultSetSql.next()) {
            objectToSave.setCodigo(resultSetSql.getInt(1));
        }
        super.closeConnection();

        if (objectToSave.getOperacoes() != null) {
            objectToSave.getOperacoes().forEach(operacao -> {
                operacao.setCodigoColaborador(objectToSave.getCodigo());
                try {
                    DAOFactory.getSdPolivalencia001DAO().save(operacao);
                } catch (SQLException ex) {
                    Logger.getLogger(SdColaborador001DAO.class.getName()).log(Level.SEVERE, null, ex);
                    GUIUtils.showException(ex);
                }
            });
        }

    }

    public void update(SdColaborador objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_colaborador_001\n"
                        + "   set nome        = ?,\n"
                        + "       funcao      = ?,\n"
                        + "       turno       = ?,\n"
                        + "       codigo_rh   = ?,\n"
                        + "       ativo       = ?,\n"
                        + "       codfor      = ?,\n"
                        + " impressora_padrao = ?,\n"
                        + " perdidas_ativas = ?,\n"
                        + " incompletas_ativas = ?,\n"
                        + " entrada_direta = ?,\n"
                        + "       usuario     = ?,\n"
                        + "       piso_coleta     = ?,\n"
                        + "       segundo_coletor     = ?\n"
                        + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getNome());
        super.preparedStatement.setInt(2, objectToSave.getCodigoFuncao());
        super.preparedStatement.setInt(3, objectToSave.getCodigoTurno());
        super.preparedStatement.setInt(4, objectToSave.getCodigoRH());
        super.preparedStatement.setString(5, objectToSave.isAtivo() ? "S" : "N");
        super.preparedStatement.setString(6, objectToSave.getCodfor());
        super.preparedStatement.setString(7, objectToSave.isImpressoraPadrao() ? "S" : "N");
        super.preparedStatement.setString(8, objectToSave.isPerdidasAtivas() ? "S" : "N");
        super.preparedStatement.setString(9, objectToSave.isIncompletasAtivas() ? "S" : "N");
        super.preparedStatement.setString(10, objectToSave.isEntradaDireta() ? "S" : "N");
        super.preparedStatement.setString(11, objectToSave.getUsuario());
        super.preparedStatement.setInt(12, objectToSave.getPisoColeta());
        super.preparedStatement.setString(13, objectToSave.isSegundoColetor() ? "S" : "N");
        super.preparedStatement.setInt(14, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();

        DAOFactory.getSdPolivalencia001DAO().deletePerson(objectToSave);
        objectToSave.getOperacoes().forEach(operacao -> {
            operacao.setCodigoColaborador(objectToSave.getCodigo());
            try {
                DAOFactory.getSdPolivalencia001DAO().save(operacao);
            } catch (SQLException ex) {
                Logger.getLogger(SdColaborador001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

    }

    public void delete(SdColaborador objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "begin\n"
                + " delete from sd_polivalencia_001 where colaborador = ?;\n"
                + " delete from sd_colaborador_001 where codigo = ?;\n"
                + "end;");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigo());
        super.preparedStatement.setInt(2, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public ObservableList<SdColaborador> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdColaborador> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col, sd_funcao_001 fun, sd_turno_001 tur\n" +
                " where col.funcao = fun.codigo\n" +
                "   and col.turno = tur.codigo\n" +
                "   and col.nome like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime())));
        }
        resultSetSql.close();
        super.closeConnection();

//        rows.forEach(row -> {
//            try {
//                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
//            } catch (SQLException ex) {
//                GUIUtils.showException(ex);
//            }
//        });
        return rows;
    }

    //------------------------------------------------------------------------------------------------------
    public ObservableList<SdColaborador> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdColaborador> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col, sd_funcao_001 fun, sd_turno_001 tur\n" +
                " where col.funcao = fun.codigo\n" +
                "   and col.turno = tur.codigo\n" +
                "   and " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime())));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public SdColaborador getByCode(Integer codePerson) throws SQLException {
        SdColaborador row = null;

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col, sd_funcao_001 fun, sd_turno_001 tur\n" +
                " where col.funcao = fun.codigo\n" +
                "   and col.turno = tur.codigo\n" +
                "   and col.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codePerson);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

//        rows.forEach(row -> {
//            try {
//                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
//            } catch (SQLException ex) {
//                GUIUtils.showException(ex);
//            }
//        });
        return row;
    }

    public ObservableList<SdColaborador> getNotInCell() throws SQLException {
        ObservableList<SdColaborador> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col, sd_funcao_001 fun, sd_turno_001 tur\n" +
                " where col.funcao = fun.codigo\n" +
                "   and col.turno = tur.codigo\n" +
                "   and col.codigo not in (select colaborador from sd_colab_celula_001)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime())));
        }
        resultSetSql.close();
        super.closeConnection();

//        rows.forEach(row -> {
//            try {
//                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
//            } catch (SQLException ex) {
//                GUIUtils.showException(ex);
//            }
//        });
        return rows;
    }

    public SdColaborador getLivreProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        SdColaborador row = null;

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n " +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_polivalencia_001 pol\n" +
                "    on pol.colaborador = col.codigo\n" +
                "   and pol.ativo = 'S'\n" +
                " inner join sd_nivel_polivalencia_001 niv\n" +
                "    on niv.codigo = pol.nivel\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                " where col.ativo = 'S'\n" +
                "   and pol.operacao = ?\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula = ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo not in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between\n" +
                "               dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between\n" +
                "               dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') <\n" +
                "                dh_inicio and\n" +
                "                to_date(?, 'DD/MM/YYYY HH24:MI:SS') >\n" +
                "                dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') >\n" +
                "                dh_inicio and\n" +
                "                to_date(?, 'DD/MM/YYYY HH24:MI:SS') <\n" +
                "                dh_fim))\n" +
                " order by niv.ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, programacao.getOperacao());
        super.preparedStatement.setInt(2, codigoTurno);
        super.preparedStatement.setInt(3, programacao.getCelula());
        super.preparedStatement.setString(4, programacao.getDhInicioAsString());
        super.preparedStatement.setString(5, programacao.getDhFimAsString());
        super.preparedStatement.setString(6, programacao.getDhInicioAsString());
        super.preparedStatement.setString(7, programacao.getDhFimAsString());
        super.preparedStatement.setString(8, programacao.getDhInicioAsString());
        super.preparedStatement.setString(9, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public SdColaborador getOcupadoProgramacao(SdColaborador operador, SdProgramacaoPacote001 programacao) throws SQLException {
        SdColaborador row = null;

        StringBuilder query = new StringBuilder("" +
                "select col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       col.usuario col_usuario,\n" +
                "       col.codfor col_codfor,\n" +
                "       col.impressora_padrao impressora_padrao,\n" +
                "       col.perdidas_ativas perdidas_ativas,\n" +
                "       col.incompletas_ativas incompletas_ativas,\n" +
                "       col.entrada_direta entrada_direta,\n" +
                "       col.piso_coleta col_piso_coleta,\n" +
                "       col.segundo_coletor col_segundo_coletor,\n" +
                "       fun.codigo fun_codigo,\n" +
                "       fun.descricao fun_descricao,\n" +
                "       tur.codigo tur_codigo,\n" +
                "       tur.descricao tur_descricao,\n" +
                "       tur.hr_inicio tur_hr_inicio,\n" +
                "       tur.hr_fim tur_hr_fim,\n" +
                "       tur.ativo tur_ativo,\n" +
                "       tur.tempo_interjornada tur_tempo_interjornada\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n " +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_polivalencia_001 pol\n" +
                "    on pol.colaborador = col.codigo\n" +
                "   and pol.ativo = 'S'\n" +
                " inner join sd_nivel_polivalencia_001 niv\n" +
                "    on niv.codigo = pol.nivel\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                " where col.codigo = ?\n" +
                "   and col.codigo in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where pacote <> ?\n" +
                "           and (to_date(?, 'DD/MM/YYYY HH24:MI:SS') between\n" +
                "               dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between\n" +
                "               dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') <\n" +
                "                dh_inicio and\n" +
                "                to_date(?, 'DD/MM/YYYY HH24:MI:SS') >\n" +
                "                dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') >\n" +
                "                dh_inicio and\n" +
                "                to_date(?, 'DD/MM/YYYY HH24:MI:SS') <\n" +
                "                dh_fim)))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, operador.getCodigo());
        super.preparedStatement.setString(2, programacao.getPacote());
        super.preparedStatement.setString(3, programacao.getDhInicioAsString());
        super.preparedStatement.setString(4, programacao.getDhFimAsString());
        super.preparedStatement.setString(5, programacao.getDhInicioAsString());
        super.preparedStatement.setString(6, programacao.getDhFimAsString());
        super.preparedStatement.setString(7, programacao.getDhInicioAsString());
        super.preparedStatement.setString(8, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdColaborador(
                    resultSetSql.getInt("col_codigo"),
                    resultSetSql.getString("col_nome"),
                    resultSetSql.getInt("col_funcao"),
                    resultSetSql.getInt("col_turno"),
                    resultSetSql.getInt("col_codigo_rh"),
                    resultSetSql.getString("col_ativo"),
                    resultSetSql.getString("col_usuario"),
                    resultSetSql.getString("col_codfor"),
                    resultSetSql.getString("impressora_padrao"),
                    resultSetSql.getString("perdidas_ativas"),
                    resultSetSql.getString("incompletas_ativas"),
                    resultSetSql.getString("entrada_direta"),
                    Integer.parseInt(resultSetSql.getString("col_piso_coleta")),
                    resultSetSql.getString("col_segundo_coletor").equals("S"),
                    new SdFuncao001(
                            resultSetSql.getInt("fun_codigo"),
                            resultSetSql.getString("fun_descricao")),
                    new SdTurno(
                            resultSetSql.getInt("tur_codigo"),
                            resultSetSql.getString("tur_descricao"),
                            resultSetSql.getString("tur_hr_inicio"),
                            resultSetSql.getString("tur_hr_fim"),
                            resultSetSql.getString("tur_ativo"),
                            resultSetSql.getTimestamp("tur_hr_inicio").toLocalDateTime(),
                            resultSetSql.getTimestamp("tur_hr_fim").toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public ObservableList<ViewOcupacaoColaborador> getPolivalenteCelulaLivreProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        ObservableList<ViewOcupacaoColaborador> rows = FXCollections.observableArrayList();

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       col.funcao,\n" +
                "       col.turno,\n" +
                "       col.codigo_rh,\n" +
                "       col.ativo,\n" +
                "       col.usuario,\n" +
                "       niv.sigla,\n" +
                "       dc.eficiencia eficiencia,\n" +
                "       ((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo) minutosTurno,\n" +
                "       nvl(dc.min_ocupados,0) minutosOcupados, \n" +
                "       (((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo)) - nvl(dc.min_ocupados,0) minutosLivres\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n" +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_polivalencia_001 pol\n" +
                "    on pol.colaborador = col.codigo\n" +
                "   and pol.ativo = 'S'\n" +
                " inner join sd_nivel_polivalencia_001 niv\n" +
                "    on niv.codigo = pol.nivel\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                "  left join sd_diario_colaborador_001 dc\n" +
                "    on dc.colaborador = col.codigo\n" +
                "   and dc.dt_diario = to_date(?, 'DD/MM/YYYY')\n" +
                " where col.ativo = 'S'\n" +
                "   and pol.operacao = ?\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula = ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo not in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_fim))\n" +
                " order by niv.ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(2, programacao.getOperacao());
        super.preparedStatement.setInt(3, codigoTurno);
        super.preparedStatement.setInt(4, programacao.getCelula());
        super.preparedStatement.setString(5, programacao.getDhInicioAsString());
        super.preparedStatement.setString(6, programacao.getDhFimAsString());
        super.preparedStatement.setString(7, programacao.getDhInicioAsString());
        super.preparedStatement.setString(8, programacao.getDhFimAsString());
        super.preparedStatement.setString(9, programacao.getDhInicioAsString());
        super.preparedStatement.setString(10, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new ViewOcupacaoColaborador(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("nome"),
                    resultSetSql.getString("minutosLivres"),
                    resultSetSql.getString("minutosTurno"),
                    resultSetSql.getString("minutosOcupados"),
                    resultSetSql.getString("eficiencia"),
                    resultSetSql.getString("sigla"),
                    new SdColaborador(
                            resultSetSql.getInt("codigo"),
                            resultSetSql.getString("nome"),
                            resultSetSql.getInt("funcao"),
                            resultSetSql.getInt("turno"),
                            resultSetSql.getInt("codigo_rh"),
                            resultSetSql.getString("ativo")
                    ),
                    "G"
            ));
        }
        resultSetSql.close();
        super.closeConnection();

//        for (ViewOcupacaoColaborador colaborador : rows) {
//            colaborador.setColaborador(DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(colaborador.getCodigo())));
//            colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacao.getDhInicio()));
//        }

        return rows;
    }

    public ObservableList<ViewOcupacaoColaborador> getCelulaLivreProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        ObservableList<ViewOcupacaoColaborador> rows = FXCollections.observableArrayList();

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       col.funcao,\n" +
                "       col.turno,\n" +
                "       col.codigo_rh,\n" +
                "       col.ativo,\n" +
                "       col.usuario,\n" +
                "       'N' sigla,\n" +
                "       dc.eficiencia eficiencia,\n" +
                "       ((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo) minutosTurno,\n" +
                "       nvl(dc.min_ocupados,0) minutosOcupados,\n" +
                "       (((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo)) - nvl(dc.min_ocupados,0) minutosLivres\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n" +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                "  left join sd_diario_colaborador_001 dc\n" +
                "    on dc.colaborador = col.codigo\n" +
                "   and dc.dt_diario = to_date(?, 'DD/MM/YYYY')\n" +
                " where col.ativo = 'S'\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula = ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo not in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_fim))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(2, codigoTurno);
        super.preparedStatement.setInt(3, programacao.getCelula());
        super.preparedStatement.setString(4, programacao.getDhInicioAsString());
        super.preparedStatement.setString(5, programacao.getDhFimAsString());
        super.preparedStatement.setString(6, programacao.getDhInicioAsString());
        super.preparedStatement.setString(7, programacao.getDhFimAsString());
        super.preparedStatement.setString(8, programacao.getDhInicioAsString());
        super.preparedStatement.setString(9, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new ViewOcupacaoColaborador(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("nome"),
                    resultSetSql.getString("minutosLivres"),
                    resultSetSql.getString("minutosTurno"),
                    resultSetSql.getString("minutosOcupados"),
                    resultSetSql.getString("eficiencia"),
                    resultSetSql.getString("sigla"),
                    new SdColaborador(
                            resultSetSql.getInt("codigo"),
                            resultSetSql.getString("nome"),
                            resultSetSql.getInt("funcao"),
                            resultSetSql.getInt("turno"),
                            resultSetSql.getInt("codigo_rh"),
                            resultSetSql.getString("ativo")
                    ),
                    "B"
            ));
        }
        resultSetSql.close();
        super.closeConnection();

//        for (ViewOcupacaoColaborador colaborador : rows) {
//            colaborador.setColaborador(DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(colaborador.getCodigo())));
//            colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacao.getDhInicio()));
//        }

        return rows;
    }

    public ObservableList<ViewOcupacaoColaborador> getPolivalenteLivreProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        ObservableList<ViewOcupacaoColaborador> rows = FXCollections.observableArrayList();

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       col.funcao,\n" +
                "       col.turno,\n" +
                "       col.codigo_rh,\n" +
                "       col.ativo,\n" +
                "       col.usuario,\n" +
                "       niv.sigla,\n" +
                "       dc.eficiencia eficiencia,\n" +
                "       ((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo) minutosTurno,\n" +
                "       nvl(dc.min_ocupados,0) minutosOcupados,\n" +
                "       (((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo)) - nvl(dc.min_ocupados,0) minutosLivres\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n" +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_polivalencia_001 pol\n" +
                "    on pol.colaborador = col.codigo\n" +
                "   and pol.ativo = 'S'\n" +
                " inner join sd_nivel_polivalencia_001 niv\n" +
                "    on niv.codigo = pol.nivel\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                "  left join sd_diario_colaborador_001 dc\n" +
                "    on dc.colaborador = col.codigo\n" +
                "   and dc.dt_diario = to_date(?, 'DD/MM/YYYY')\n" +
                " where col.ativo = 'S'\n" +
                "   and pol.operacao = ?\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula <> ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo not in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_fim))\n" +
                " order by niv.ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(2, programacao.getOperacao());
        super.preparedStatement.setInt(3, codigoTurno);
        super.preparedStatement.setInt(4, programacao.getCelula());
        super.preparedStatement.setString(5, programacao.getDhInicioAsString());
        super.preparedStatement.setString(6, programacao.getDhFimAsString());
        super.preparedStatement.setString(7, programacao.getDhInicioAsString());
        super.preparedStatement.setString(8, programacao.getDhFimAsString());
        super.preparedStatement.setString(9, programacao.getDhInicioAsString());
        super.preparedStatement.setString(10, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new ViewOcupacaoColaborador(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("nome"),
                    resultSetSql.getString("minutosLivres"),
                    resultSetSql.getString("minutosTurno"),
                    resultSetSql.getString("minutosOcupados"),
                    resultSetSql.getString("eficiencia"),
                    resultSetSql.getString("sigla"),
                    new SdColaborador(
                            resultSetSql.getInt("codigo"),
                            resultSetSql.getString("nome"),
                            resultSetSql.getInt("funcao"),
                            resultSetSql.getInt("turno"),
                            resultSetSql.getInt("codigo_rh"),
                            resultSetSql.getString("ativo")
                    ),
                    "Y"
            ));
        }
        resultSetSql.close();
        super.closeConnection();

//        for (ViewOcupacaoColaborador colaborador : rows) {
//            colaborador.setColaborador(DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(colaborador.getCodigo())));
//            colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacao.getDhInicio()));
//        }

        return rows;
    }

    public ObservableList<ViewOcupacaoColaborador> getPolivalenteCelulaOcupadaProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        ObservableList<ViewOcupacaoColaborador> rows = FXCollections.observableArrayList();

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       col.funcao,\n" +
                "       col.turno,\n" +
                "       col.codigo_rh,\n" +
                "       col.ativo,\n" +
                "       col.usuario,\n" +
                "       'N' sigla,\n" +
                "       dc.eficiencia eficiencia,\n" +
                "       ((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo) minutosTurno,\n" +
                "       nvl(dc.min_ocupados,0) minutosOcupados,\n" +
                "       (((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo)) - nvl(dc.min_ocupados,0) minutosLivres\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n" +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                "  left join sd_diario_colaborador_001 dc\n" +
                "    on dc.colaborador = col.codigo\n" +
                "   and dc.dt_diario = to_date(?, 'DD/MM/YYYY')\n" +
                " where col.ativo = 'S'\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula = ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_fim))\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(2, codigoTurno);
        super.preparedStatement.setInt(3, programacao.getCelula());
        super.preparedStatement.setString(4, programacao.getDhInicioAsString());
        super.preparedStatement.setString(5, programacao.getDhFimAsString());
        super.preparedStatement.setString(6, programacao.getDhInicioAsString());
        super.preparedStatement.setString(7, programacao.getDhFimAsString());
        super.preparedStatement.setString(8, programacao.getDhInicioAsString());
        super.preparedStatement.setString(9, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new ViewOcupacaoColaborador(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("nome"),
                    resultSetSql.getString("minutosLivres"),
                    resultSetSql.getString("minutosTurno"),
                    resultSetSql.getString("minutosOcupados"),
                    resultSetSql.getString("eficiencia"),
                    resultSetSql.getString("sigla"),
                    new SdColaborador(
                            resultSetSql.getInt("codigo"),
                            resultSetSql.getString("nome"),
                            resultSetSql.getInt("funcao"),
                            resultSetSql.getInt("turno"),
                            resultSetSql.getInt("codigo_rh"),
                            resultSetSql.getString("ativo")
                    ),
                    "R"
            ));
        }
        resultSetSql.close();
        super.closeConnection();

//        for (ViewOcupacaoColaborador colaborador : rows) {
//            colaborador.setColaborador(DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(colaborador.getCodigo())));
//            colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacao.getDhInicio()));
//        }

        return rows;
    }

    public ObservableList<ViewOcupacaoColaborador> getPolivalenteOcupadaProgramacao(SdProgramacaoPacote001 programacao) throws SQLException {
        ObservableList<ViewOcupacaoColaborador> rows = FXCollections.observableArrayList();

        SdTurno turno = DAOFactory.getSdTurnoDAO().getByDateTime(programacao.getDhInicio());
        int codigoTurno = 0;
        if (turno != null) {
            codigoTurno = turno.getCodigo();
        }

        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       col.funcao,\n" +
                "       col.turno,\n" +
                "       col.codigo_rh,\n" +
                "       col.ativo,\n" +
                "       col.usuario,\n" +
                "       niv.sigla,\n" +
                "       dc.eficiencia eficiencia,\n" +
                "       ((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo) minutosTurno,\n" +
                "       nvl(dc.min_ocupados,0) minutosOcupados,\n" +
                "       (((tur.hr_fim - tur.hr_inicio) * 1440) - (select sum(ptur.tempo_intervalo) from sd_paradas_turno_001 ptur where ptur.turno = tur.codigo)) - nvl(dc.min_ocupados,0) minutosLivres\n" +
                "  from sd_colaborador_001 col\n" +
                " inner join sd_funcao_001 fun\n" +
                "    on fun.codigo = col.funcao\n" +
                " inner join sd_turno_001 tur\n" +
                "    on tur.codigo = col.turno\n" +
                "   and tur.ativo = 'S'\n" +
                " inner join sd_polivalencia_001 pol\n" +
                "    on pol.colaborador = col.codigo\n" +
                "   and pol.ativo = 'S'\n" +
                " inner join sd_nivel_polivalencia_001 niv\n" +
                "    on niv.codigo = pol.nivel\n" +
                " inner join sd_colab_celula_001 colcel\n" +
                "    on colcel.colaborador = col.codigo\n" +
                "  left join sd_diario_colaborador_001 dc\n" +
                "    on dc.colaborador = col.codigo\n" +
                "   and dc.dt_diario = to_date(?, 'DD/MM/YYYY')\n" +
                " where col.ativo = 'S'\n" +
                "   and pol.operacao = ?\n" +
                "   and col.turno = ?\n" +
                "   and colcel.celula <> ?\n" +
                "   and col.funcao not in (16, 19, 20)\n" +
                "   and col.codigo in\n" +
                "       (select distinct colaborador\n" +
                "          from sd_programacao_pacote_001\n" +
                "         where to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or to_date(?, 'DD/MM/YYYY HH24:MI:SS') between dh_inicio and dh_fim\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_fim)\n" +
                "            or (to_date(?, 'DD/MM/YYYY HH24:MI:SS') > dh_inicio and to_date(?, 'DD/MM/YYYY HH24:MI:SS') < dh_fim))\n" +
                " order by niv.ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getDhInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(2, programacao.getOperacao());
        super.preparedStatement.setInt(3, codigoTurno);
        super.preparedStatement.setInt(4, programacao.getCelula());
        super.preparedStatement.setString(5, programacao.getDhInicioAsString());
        super.preparedStatement.setString(6, programacao.getDhFimAsString());
        super.preparedStatement.setString(7, programacao.getDhInicioAsString());
        super.preparedStatement.setString(8, programacao.getDhFimAsString());
        super.preparedStatement.setString(9, programacao.getDhInicioAsString());
        super.preparedStatement.setString(10, programacao.getDhFimAsString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new ViewOcupacaoColaborador(
                    resultSetSql.getString("codigo"),
                    resultSetSql.getString("nome"),
                    resultSetSql.getString("minutosLivres"),
                    resultSetSql.getString("minutosTurno"),
                    resultSetSql.getString("minutosOcupados"),
                    resultSetSql.getString("eficiencia"),
                    resultSetSql.getString("sigla"),
                    new SdColaborador(
                            resultSetSql.getInt("codigo"),
                            resultSetSql.getString("nome"),
                            resultSetSql.getInt("funcao"),
                            resultSetSql.getInt("turno"),
                            resultSetSql.getInt("codigo_rh"),
                            resultSetSql.getString("ativo")
                    ),
                    "D"
            ));
        }
        resultSetSql.close();
        super.closeConnection();

//        for (ViewOcupacaoColaborador colaborador : rows) {
//            colaborador.setColaborador(DAOFactory.getSdColaborador001DAO().getByCode(Integer.parseInt(colaborador.getCodigo())));
//            colaborador.setOcupacao(DAOFactory.getSdProgramacaoPacote001DAO().getOcupacaoColaboradorDia(Integer.parseInt(colaborador.getCodigo()), programacao.getDhInicio()));
//        }

        return rows;
    }

    //-------------------------------------------------------------------------------------------------------
    public void mergeToOcupados(Integer colaborador, LocalDate dtDiario, Double minOcupados) throws SQLException {
        StringBuilder query = new StringBuilder(
                "merge into sd_diario_colaborador_001 dc\n" +
                        "using dual\n" +
                        "on (dc.colaborador = ? and dc.dt_diario = to_date(?,'DD/MM/YYYY'))\n" +
                        "when matched then\n" +
                        "  update set dc.min_ocupados = dc.min_ocupados + ?\n" +
                        "when not matched then\n" +
                        "  insert\n" +
                        "    (dc.colaborador, dc.dt_diario, dc.min_ocupados)\n" +
                        "  values\n" +
                        "    (?, to_date(?,'DD/MM/YYYY'), ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.setString(2, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setDouble(3, minOcupados);
        super.preparedStatement.setInt(4, colaborador);
        super.preparedStatement.setString(5, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setDouble(6, minOcupados);
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public void mergeToEficiencia(Integer colaborador, LocalDate dtDiario, Double eficiencia) throws SQLException {
        StringBuilder query = new StringBuilder(
                "merge into sd_diario_colaborador_001 dc\n" +
                        "using dual\n" +
                        "on (dc.colaborador = ? and dc.dt_diario = to_date(?,'DD/MM/YYYY'))\n" +
                        "when matched then\n" +
                        "  update\n" +
                        "     set dc.eficiencia = (select avg(lanc.eficiencia) avg_efic\n" +
                        "                            from sd_lancamento_producao_001 lanc\n" +
                        "                           where lanc.colaborador = ?\n" +
                        "                             and to_char(lanc.dh_inicio, 'DD/MM/YYYY') = ?)\n" +
                        "when not matched then\n" +
                        "  insert\n" +
                        "    (dc.colaborador, dc.dt_diario, dc.eficiencia)\n" +
                        "  values\n" +
                        "    (?, to_date(?,'DD/MM/YYYY'), ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.setString(2, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(3, colaborador);
        super.preparedStatement.setString(4, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setInt(5, colaborador);
        super.preparedStatement.setString(6, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.setDouble(7, eficiencia);
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public Map<String, Object> getDiario(Integer colaborador, LocalDate dtDiario) throws SQLException {
        Map<String, Object> row = new HashMap<>();

        StringBuilder query = new StringBuilder("" +
                "select * \n" +
                "  from sd_diario_colaborador_001\n" +
                " where colaborador = ?\n" +
                "   and dt_diario = to_date(?,'DD/MM/YYYY')");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.setString(2, dtDiario.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row.put("colaborador", resultSetSql.getInt(1));
            row.put("dt_diario", resultSetSql.getTimestamp(2).toLocalDateTime().toLocalDate());
            row.put("min_ocupados", resultSetSql.getDouble(3));
            row.put("eficiencia", resultSetSql.getDouble(4));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }
    
    //-------------------------------------------------------------------------------------------------------
    public LocalDateTime getHoraPontoCostureira(String codigoRh) throws SQLException {
        LocalDateTime horaPonto = null;
    
        StringBuilder query = new StringBuilder("" +
                "select nvl(min(horacc),0) hora_ponto\n" +
                "  from senior_ind.r070acc\n" +
                " where numcad = "+codigoRh+"\n" +
                "   and numemp = 6\n" +
                "   and oriacc = 'E'\n" +
                "   and to_char(datacc, 'DD/MM/YYYY') = to_char(sysdate, 'DD/MM/YYYY')");
    
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if(resultSetSql.next()) {
            if(resultSetSql.getLong(1) > 0) {
                horaPonto = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0);
                horaPonto = horaPonto.plusMinutes(resultSetSql.getLong(1));
            }
        }
        resultSetSql.close();
        super.closeConnection();
        
        return horaPonto;
    }
}
