/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdColaboradorSenior001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author lima.joao
 */
public class SdColaboradorSenior001DAO extends FactoryConnection {

    public SdColaboradorSenior001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdColaboradorSenior001> getAll() throws SQLException {
        return getBy("");
    }

    public ObservableList<SdColaboradorSenior001> getBy(String whereFilter) throws SQLException {
        ObservableList<SdColaboradorSenior001> rows = FXCollections.observableArrayList();

        String mSQL = " SELECT " +
                "     f.NUMCAD COD_FUNC, " +
                "     f.NUMEMP, " +
                "     (SELECT e.nomemp FROM SENIOR_IND.r030emp e WHERE e.NUMEMP = f.NUMEMP) NOME, " +
                "     f.TIPCOL, " +
                "     f.NUMLOC NUM_LOC," +
                "     f.NOMFUN NomeFuncionario," +
                "     (select NOMLOC from senior_ind.r016orn WHERE NUMLOC = f.NUMLOC) SETOR" +
                " FROM " +
                "     senior_ind.r034fun f" +
                " WHERE " +
                "     (f.SITAFA in (1)) ";


        if(!whereFilter.trim().equals("")){
            mSQL += " AND " + whereFilter;
        }

        mSQL += " ORDER BY " +
                "     NUMLOC, NUMEMP, NUMCAD, tipcol";

        super.initPreparedStatement(mSQL);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            rows.add(
                    new SdColaboradorSenior001(
                            resultSetSql.getString(1), // NumCad
                            resultSetSql.getString(2), // NumEmp
                            resultSetSql.getString(3), // NomeEmp
                            resultSetSql.getString(4), // TipCol
                            resultSetSql.getString(5), // NumLoc
                            resultSetSql.getString(7), // NomeSetor
                            resultSetSql.getString(6)  // NomeFuncionario
                    )
            );
        }

        resultSetSql.close();
        super.closeConnection();
        return rows;
    }

}
