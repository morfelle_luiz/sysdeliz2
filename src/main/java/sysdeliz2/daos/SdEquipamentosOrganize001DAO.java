/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.sysdeliz.SdEquipamentosOrganize001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdEquipamentosOrganize001DAO extends FactoryConnection {

    public SdEquipamentosOrganize001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public SdEquipamentosOrganize001 getByCode(Integer code) throws SQLException {
        SdEquipamentosOrganize001 row = null;

        StringBuilder query = new StringBuilder("select * from sd_equipamentos_organize_001 where codorg = ?");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        if (retornoSql.next()) {
            row = new SdEquipamentosOrganize001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getString(3),
                    retornoSql.getDouble(4)
            );
        }
        retornoSql.close();
        super.closeConnection();

        return row;
    }

}
