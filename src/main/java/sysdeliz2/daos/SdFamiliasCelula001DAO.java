/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.FamiliaGrupo001;
import sysdeliz2.models.SdFamiliasCelula001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdFamiliasCelula001DAO extends FactoryConnection {

    public SdFamiliasCelula001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdFamiliasCelula001> getAll() throws SQLException {
        ObservableList<SdFamiliasCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_familias_celula_001 famcel, familia_grupo_001 fam\n"
                + " where famcel.familia_grp = fam.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdFamiliasCelula001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    new FamiliaGrupo001(resultSetSql.getString(3), resultSetSql.getString(4), resultSetSql.getInt(5))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdFamiliasCelula001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_familias_celula_001\n"
                + "   (celula, familia_grp)\n"
                + " values\n"
                + "   (?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCelula());
        super.preparedStatement.setString(2, objectToSave.getFamiliaGrp());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        super.closeConnection();
    }

    public void update(SdFamiliasCelula001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_turno_001\n"
//                + "   set descricao        = ?,\n"
//                + "       hr_inicio        = to_date(?, 'HH24:MI'),\n"
//                + "       hr_fim           = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_inicio = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_fim    = to_date(?, 'HH24:MI'),\n"
//                + "       ativo            = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(7, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public void delete(SdFamiliasCelula001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_familias_celula_001 where celula = ? and familia_grp = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCelula());
        super.preparedStatement.setString(2, objectToSave.getFamiliaGrp());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void deleteCelula(Integer cellCode) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_familias_celula_001 where celula = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, cellCode);
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }

    public ObservableList<SdFamiliasCelula001> getByCell(Integer cellCode) throws SQLException {
        ObservableList<SdFamiliasCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_familias_celula_001 famcel, familia_grupo_001 fam\n"
                + " where famcel.familia_grp = fam.codigo\n"
                + "   and celula = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, cellCode);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdFamiliasCelula001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    new FamiliaGrupo001(resultSetSql.getString(3), resultSetSql.getString(4), resultSetSql.getInt(5))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    //-------------------------------------------------------------------------------
    public ObservableList<SdFamiliasCelula001> getAvailableFamilyGroups(Integer cellCode) throws SQLException {
        ObservableList<SdFamiliasCelula001> rows = FXCollections.observableArrayList();

        DAOFactory.getFamiliaGrupo001DAO().getNotInCell(cellCode).forEach(familyGroup -> {
            rows.add(new SdFamiliasCelula001(0,
                    familyGroup.getCodigo(),
                    familyGroup));
        });

        return rows;
    }

}
