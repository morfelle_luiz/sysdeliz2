/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.SdLogEmailOc001;

import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdLogEmailOc001DAO extends FactoryConnection {

    public SdLogEmailOc001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public void save(SdLogEmailOc001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_log_email_oc_001\n"
                + "   (codcli, numero, codigo, cor, dt_envio, dt_faturamento, email, usuario)\n"
                + " values\n"
                + "   (?, ?, ?, ?, sysdate, to_date(?,'DD/MM/YYYY'), ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getCodcli());
        super.preparedStatement.setString(2, objectToSave.getNumero());
        super.preparedStatement.setString(3, objectToSave.getCodigo());
        super.preparedStatement.setString(4, objectToSave.getCor());
        super.preparedStatement.setString(5, objectToSave.getDtFaturamento());
        super.preparedStatement.setString(6, objectToSave.getEmail());
        super.preparedStatement.setString(7, SceneMainController.getAcesso().getUsuario());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();

    }

}
