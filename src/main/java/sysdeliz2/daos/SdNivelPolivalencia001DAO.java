/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdNivelPolivalencia001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class SdNivelPolivalencia001DAO extends FactoryConnection {

    public SdNivelPolivalencia001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter = "";

    public ObservableList<SdNivelPolivalencia001> getAll() throws SQLException {
        ObservableList<SdNivelPolivalencia001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_nivel_polivalencia_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdNivelPolivalencia001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getDouble(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getString(6)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdNivelPolivalencia001 objectToSave) throws SQLException {
        String[] returnId = {"CODIGO"};
        StringBuilder query = new StringBuilder(""
                + " insert into sd_nivel_polivalencia_001\n"
                + "   (descricao, meta_inicio, meta_fim, premio, sigla)\n"
                + " values\n"
                + "   (?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString(), returnId);
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setDouble(2, objectToSave.getMetaInicio().doubleValue());
        super.preparedStatement.setDouble(3, objectToSave.getMetaFim().doubleValue());
        super.preparedStatement.setDouble(4, objectToSave.getPremio().doubleValue());
        super.preparedStatement.setString(5, objectToSave.getSigla());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
        if (resultSetSql.next()) {
            objectToSave.setCodigo(resultSetSql.getInt(1));
        }
        super.closeConnection();
    }

    public void update(SdNivelPolivalencia001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_nivel_polivalencia_001\n"
                + "   set descricao        = ?,\n"
                + "       meta_inicio      = ?,\n"
                + "       meta_fim         = ?,\n"
                + "       premio           = ?,\n"
                + "       sigla            = ?\n"
                + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setDouble(2, objectToSave.getMetaInicio().doubleValue());
        super.preparedStatement.setDouble(3, objectToSave.getMetaFim().doubleValue());
        super.preparedStatement.setDouble(4, objectToSave.getPremio().doubleValue());
        super.preparedStatement.setString(5, objectToSave.getSigla());
        super.preparedStatement.setInt(6, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void delete(SdNivelPolivalencia001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_nivel_polivalencia_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public ObservableList<SdNivelPolivalencia001> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdNivelPolivalencia001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_nivel_polivalencia_001\n"
                + " where descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdNivelPolivalencia001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getDouble(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getString(6)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdNivelPolivalencia001> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdNivelPolivalencia001> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_nivel_polivalencia_001\n"
                + " where " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdNivelPolivalencia001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getDouble(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getString(6)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public SdNivelPolivalencia001 getByCode(Integer code) throws SQLException {
        SdNivelPolivalencia001 row = null;

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_nivel_polivalencia_001\n"
                + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdNivelPolivalencia001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getDouble(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getString(6));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }
}
