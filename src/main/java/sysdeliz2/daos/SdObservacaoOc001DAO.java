/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.SdObservacaoOc001;
import sysdeliz2.models.properties.GestaoCompra00X;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdObservacaoOc001DAO extends FactoryConnection {

    public SdObservacaoOc001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdObservacaoOc001> getCrmOrdemCompraMaterial(GestaoCompra00X ordemCompra) throws SQLException {
        ObservableList<SdObservacaoOc001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select numero, codigo, cor, to_char(dt_observacao, 'DD/MM/YYYY HH24:MI:SS'),\n"
                + "       usuario, to_char(observacao)\n"
                + "  from sd_observacao_oc_001\n"
                + " where numero = ?\n"
                + "   and codigo = ?\n"
                + "   and cor = ?\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, ordemCompra.getNumero());
        super.preparedStatement.setString(2, ordemCompra.getCodigo());
        super.preparedStatement.setString(3, ordemCompra.getCor());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdObservacaoOc001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6)));
        }
        resultSetSql.close();
        super.closeConnection();

//        rows.forEach(row -> {
//            try {
//                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
//            } catch (SQLException ex) {
//                GUIUtils.showException(ex);
//            }
//        });
        return rows;
    }

    public void save(SdObservacaoOc001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_observacao_oc_001\n"
                + "   (numero, codigo, cor, dt_observacao, usuario, observacao)\n"
                + " values\n"
                + "   (?, ?, ?, sysdate, ?, to_clob(?))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getNumero());
        super.preparedStatement.setString(2, objectToSave.getCodigo());
        super.preparedStatement.setString(3, objectToSave.getCor());
        super.preparedStatement.setString(4, SceneMainController.getAcesso().getUsuario());
        super.preparedStatement.setString(5, objectToSave.getObservacao());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();

    }

}
