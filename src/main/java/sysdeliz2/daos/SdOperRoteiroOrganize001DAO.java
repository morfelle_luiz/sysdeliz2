/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdOperRoteiroOrganize001DAO extends FactoryConnection {

    public SdOperRoteiroOrganize001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter = "";

    public ObservableList<SdOperRoteiroOrganize001> getAll() throws SQLException {
        ObservableList<SdOperRoteiroOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_oper_roteiro_organize_001 rot\n"
                + "  join sd_operacao_organize_001 ope\n"
                + "    on ope.codorg = rot.operacao\n"
                + "  join sd_equipamentos_organize_001 equ\n"
                + "    on equ.codorg = ope.equipamento\n"
                + " order by 1,3");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getInt(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    new SdOperacaoOrganize001(
                            resultSetSql.getInt(9),
                            resultSetSql.getString(10),
                            resultSetSql.getString(11),
                            resultSetSql.getDouble(12),
                            resultSetSql.getDouble(13),
                            resultSetSql.getDouble(14),
                            resultSetSql.getDouble(15),
                            resultSetSql.getString(16),
                            resultSetSql.getString(17),
                            resultSetSql.getString(18),
                            resultSetSql.getDouble(19),
                            new SdEquipamentosOrganize001(
                                    resultSetSql.getString(20),
                                    resultSetSql.getInt(21),
                                    resultSetSql.getString(22),
                                    resultSetSql.getDouble(23)))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void deleteByRoteiro(SdRoteiroOrganize001 roteiro) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_oper_roteiro_organize_001\n"
                + " where roteiro = ?");
    
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, String.valueOf(roteiro.getCodorg()));
        int affectedRows = super.preparedStatement.executeUpdate();
    
        super.closeConnection();
    }
    
    public void update(SdOperRoteiroOrganize001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("" +
                "merge into sd_oper_roteiro_organize_001 oper\n" +
                "using dual bd\n" +
                "on (oper.roteiro = ? and oper.operacao = ? and oper.ordem = ?)\n" +
                "when matched then\n" +
                "  update\n" +
                "     set oper.setor          = ?,\n" +
                "         oper.setor_operacao = ?,\n" +
                "         oper.desc_set_op    = ?,\n" +
                "         oper.tempo_op       = ?\n" +
                "when not matched then\n" +
                "  insert values (?, ?, ?, ?, ?, ?, ?, null)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getId().getRoteiro());
        super.preparedStatement.setInt(2, objectToSave.getId().getOperacao().getCodorg());
        super.preparedStatement.setInt(3, objectToSave.getId().getOrdem());
        super.preparedStatement.setString(4, objectToSave.getSetor());
        super.preparedStatement.setInt(5, objectToSave.getSetorOp().getCodigo());
        super.preparedStatement.setString(6, objectToSave.getSetorOp().getDescricao());
        super.preparedStatement.setDouble(7, objectToSave.getTempoOp().doubleValue());
        super.preparedStatement.setInt(8, objectToSave.getId().getRoteiro());
        super.preparedStatement.setInt(9, objectToSave.getId().getOperacao().getCodorg());
        super.preparedStatement.setInt(10, objectToSave.getId().getOrdem());
        super.preparedStatement.setString(11, objectToSave.getSetor());
        super.preparedStatement.setInt(12, objectToSave.getSetorOp().getCodigo());
        super.preparedStatement.setString(13, objectToSave.getSetorOp().getDescricao());
        super.preparedStatement.setDouble(14, objectToSave.getTempoOp().doubleValue());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }
    
    public void updateTempoOp(String referencia, Integer operacao, Double tempoOp) throws SQLException {
    
        StringBuilder query = new StringBuilder("{call PKG_SD_SHOPFLOOR.P_SD_UPD_TEMPO_OPERACAO(?, ?, ?)}");
        super.initCallableStatement(query.toString());
        
        super.callStatement.setString(1, referencia);
        super.callStatement.setInt(2, operacao);
        super.callStatement.setDouble(3, tempoOp);
    
        super.callStatement.execute();
        super.closeConnection();
    }
    
    public ObservableList<SdOperRoteiroOrganize001> getByRoteiro(Integer codorgRoteiro) throws SQLException {
        ObservableList<SdOperRoteiroOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select *\n"
                + "  from sd_oper_roteiro_organize_001 rot\n"
                + "  join sd_operacao_organize_001 ope\n"
                + "    on ope.codorg = rot.operacao\n"
                + "  join sd_equipamentos_organize_001 equ\n"
                + "    on equ.codorg = ope.equipamento\n"
                + " where rot.setor_operacao <> 1\n"
                + "   and roteiro = ?\n"
                + " order by 1,3");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codorgRoteiro);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getInt(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    new SdOperacaoOrganize001(
                            resultSetSql.getInt(9),
                            resultSetSql.getString(10),
                            resultSetSql.getString(11),
                            resultSetSql.getDouble(12),
                            resultSetSql.getDouble(13),
                            resultSetSql.getDouble(14),
                            resultSetSql.getDouble(15),
                            resultSetSql.getString(16),
                            resultSetSql.getString(17),
                            resultSetSql.getString(18),
                            resultSetSql.getDouble(19),
                            new SdEquipamentosOrganize001(
                                    resultSetSql.getString(20),
                                    resultSetSql.getInt(21),
                                    resultSetSql.getString(22),
                                    resultSetSql.getDouble(23)))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdOperRoteiroOrganize001> getByReferencia(String referencia) throws SQLException {
        ObservableList<SdOperRoteiroOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select rot.*,ope.*,equ.*\n"
                + "  from sd_oper_roteiro_organize_001 rot\n"
                + "  join sd_roteiro_organize_001 rtr\n"
                + "    on rtr.codorg = rot.roteiro\n"
                + "  join sd_operacao_organize_001 ope\n"
                + "    on ope.codorg = rot.operacao\n"
                + "  join sd_equipamentos_organize_001 equ\n"
                + "    on equ.codorg = ope.equipamento\n"
                + " where rot.setor_operacao <> 1\n"
                + "   and rtr.referencia = ?\n"
                + " order by 4,3");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, referencia);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getInt(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    new SdOperacaoOrganize001(
                            resultSetSql.getInt(9),
                            resultSetSql.getString(10),
                            resultSetSql.getString(11),
                            resultSetSql.getDouble(12),
                            resultSetSql.getDouble(13),
                            resultSetSql.getDouble(14),
                            resultSetSql.getDouble(15),
                            resultSetSql.getString(16),
                            resultSetSql.getString(17),
                            resultSetSql.getString(18),
                            resultSetSql.getDouble(19),
                            new SdEquipamentosOrganize001(
                                    resultSetSql.getString(20),
                                    resultSetSql.getInt(21),
                                    resultSetSql.getString(22),
                                    resultSetSql.getDouble(23)))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdOperRoteiroOrganize001> getByReferenciaAndSetor(String referencia, String setor) throws SQLException {
        ObservableList<SdOperRoteiroOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select rot.*,ope.*,equ.*\n"
                + "  from sd_oper_roteiro_organize_001 rot\n"
                + "  join sd_roteiro_organize_001 rtr\n"
                + "    on rtr.codorg = rot.roteiro\n"
                + "  join sd_operacao_organize_001 ope\n"
                + "    on ope.codorg = rot.operacao\n"
                + "  join sd_equipamentos_organize_001 equ\n"
                + "    on equ.codorg = ope.equipamento\n"
                + " where rot.setor_operacao <> 1\n"
                + "   and rtr.referencia = ?\n"
                + "   and rot.setor = ?\n"
                + " order by 4,3");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, referencia);
        super.preparedStatement.setString(2, setor);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getInt(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    new SdOperacaoOrganize001(
                            resultSetSql.getInt(9),
                            resultSetSql.getString(10),
                            resultSetSql.getString(11),
                            resultSetSql.getDouble(12),
                            resultSetSql.getDouble(13),
                            resultSetSql.getDouble(14),
                            resultSetSql.getDouble(15),
                            resultSetSql.getString(16),
                            resultSetSql.getString(17),
                            resultSetSql.getString(18),
                            resultSetSql.getDouble(19),
                            new SdEquipamentosOrganize001(
                                    resultSetSql.getString(20),
                                    resultSetSql.getInt(21),
                                    resultSetSql.getString(22),
                                    resultSetSql.getDouble(23)))));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdSetorOp001> getSetoresOperacaoByReferenciaAndSetor(String referencia, String setor) throws SQLException {
        ObservableList<SdSetorOp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select distinct decode(setor_operacao, 1, 0, 2, 1, 5, 2, 3, 3, 6, 4) ordem,\n"
                + "                setor_operacao,\n"
                + "                desc_set_op\n"
                + "  from sd_oper_roteiro_organize_001 rot\n"
                + "  join sd_roteiro_organize_001 rtr\n"
                + "    on rtr.codorg = rot.roteiro\n"
                + " where rot.setor_operacao <> 1\n"
                + "   and rtr.referencia = ?\n"
                + "   and rot.setor = ?\n"
                + " order by 1");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, referencia);
        super.preparedStatement.setString(2, setor);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdSetorOp001(
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    "S"));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
}
