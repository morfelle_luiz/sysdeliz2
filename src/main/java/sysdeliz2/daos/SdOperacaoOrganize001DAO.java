/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdOperacaoOrganize001DAO extends FactoryConnection {

    public SdOperacaoOrganize001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdOperacaoOrganize001> getAll() throws SQLException {
        ObservableList<SdOperacaoOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacao_organize_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacaoOrganize001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getDouble(11)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdOperacaoOrganize001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_turno_001\n"
                + "   (codorg, codigo, descricao, tempo, tempo_hora, custo, concessao, equipamento, dt_inclusao, dt_alteracao, comp_custura)\n"
                + " values\n"
                + "   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodorg());
        super.preparedStatement.setString(2, objectToSave.getCodigo());
        super.preparedStatement.setString(3, objectToSave.getDescricao());
        super.preparedStatement.setDouble(4, objectToSave.getTempo().doubleValue());
        super.preparedStatement.setDouble(5, objectToSave.getTempoHora().doubleValue());
        super.preparedStatement.setDouble(6, objectToSave.getCusto().doubleValue());
        super.preparedStatement.setDouble(7, objectToSave.getConcessao().doubleValue());
        super.preparedStatement.setString(8, objectToSave.getEquipamento());
        super.preparedStatement.setString(9, objectToSave.getDtInclusao());
        super.preparedStatement.setString(10, objectToSave.getDtAlteracao());
        super.preparedStatement.setDouble(11, objectToSave.getCompCustura().doubleValue());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        super.closeConnection();
    }

    public void update(SdOperacaoOrganize001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_operacao_organize_001\n"
                + "   set descricao    = ?,\n"
                + "       codigo       = ?,\n"
                + "       tempo        = ?,\n"
                + "       tempo_hora   = ?,\n"
                + "       custo        = ?,\n"
                + "       concessao    = ?,\n"
                + "       equipamento  = ?,\n"
                + "       dt_inclusao  = ?,\n"
                + "       dt_alteracao = ?,\n"
                + "       comp_custura = ?,\n"
                + " where codorg = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setString(2, objectToSave.getCodigo());
        super.preparedStatement.setDouble(3, objectToSave.getTempo().doubleValue());
        super.preparedStatement.setDouble(4, objectToSave.getTempoHora().doubleValue());
        super.preparedStatement.setDouble(5, objectToSave.getCusto().doubleValue());
        super.preparedStatement.setDouble(6, objectToSave.getConcessao().doubleValue());
        super.preparedStatement.setString(7, objectToSave.getEquipamento());
        super.preparedStatement.setString(8, objectToSave.getDtInclusao());
        super.preparedStatement.setString(9, objectToSave.getDtAlteracao());
        super.preparedStatement.setDouble(10, objectToSave.getCompCustura().doubleValue());
        super.preparedStatement.setInt(11, objectToSave.getCodorg());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void delete(SdOperacaoOrganize001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_operacao_organize_001 where codorg = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodorg());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public ObservableList<SdOperacaoOrganize001> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdOperacaoOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacao_organize_001\n"
                + " where descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacaoOrganize001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getDouble(11)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public SdOperacaoOrganize001 getByCode(Integer codOrg) throws SQLException {
        SdOperacaoOrganize001 operation = null;

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacao_organize_001\n"
                + " where codorg = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codOrg);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            operation = new SdOperacaoOrganize001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getDouble(11));
        }
        resultSetSql.close();
        super.closeConnection();

        if (operation == null) {
            throw new SQLException("Não foi encontrado a operação com o codigo: " + codOrg);
        }

        return operation;
    }

    //-------------------------------
    public ObservableList<SdOperacaoOrganize001> getNotInGroup(Integer codeGroup) throws SQLException {
        ObservableList<SdOperacaoOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacao_organize_001\n"
                + " where codorg not in\n"
                + "       (select operacao from sd_operacoes_grp_001 where grupo = ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codeGroup);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacaoOrganize001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getDouble(11)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdOperacaoOrganize001> getNotInPersonAndInGroup(Integer codeGroup, Integer codePerson) throws SQLException {
        ObservableList<SdOperacaoOrganize001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacao_organize_001\n"
                + " where codorg in\n"
                + "       (select operacao from sd_operacoes_grp_001 where grupo = ?)\n"
                + "   and codorg not in\n"
                + "       (select operacao from sd_polivalencia_001 where colaborador = ?)\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codeGroup);
        super.preparedStatement.setInt(2, codePerson);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacaoOrganize001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getDouble(4),
                    resultSetSql.getDouble(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getDouble(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getDouble(11)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
}
