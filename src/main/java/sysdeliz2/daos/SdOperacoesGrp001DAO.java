/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdOperacoesGrp001;
import sysdeliz2.utils.GUIUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author cristiano.diego
 */
public class SdOperacoesGrp001DAO extends FactoryConnection {

    public SdOperacoesGrp001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdOperacoesGrp001> getAll() throws SQLException {
        ObservableList<SdOperacoesGrp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacoes_grp_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacoesGrp001(resultSetSql.getInt(1),
                    resultSetSql.getInt(2)));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(row -> {
            try {
                row.setOperacao(DAOFactory.getSdOperacaoOrganize001DAO().getByCode(row.getCodigoOperacao()));
            } catch (SQLException ex) {
                Logger.getLogger(SdOperacoesGrp001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    public void save(SdOperacoesGrp001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_operacoes_grp_001\n"
                + "   (grupo, operacao)\n"
                + " values\n"
                + "   (?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getGrupo());
        super.preparedStatement.setInt(2, objectToSave.getCodigoOperacao());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        super.closeConnection();
    }

    public void update(SdOperacoesGrp001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_turno_001\n"
//                + "   set descricao        = ?,\n"
//                + "       hr_inicio        = to_date(?, 'HH24:MI'),\n"
//                + "       hr_fim           = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_inicio = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_fim    = to_date(?, 'HH24:MI'),\n"
//                + "       ativo            = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(7, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public void delete(SdOperacoesGrp001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_operacoes_grp_001 where grupo = ? and operacao = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getGrupo());
        super.preparedStatement.setInt(2, objectToSave.getCodigoOperacao());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void deleteGroup(Integer codeGroup) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_operacoes_grp_001 where grupo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codeGroup);
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public ObservableList<SdOperacoesGrp001> getByGroup(Integer groupCode) throws SQLException {
        ObservableList<SdOperacoesGrp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_operacoes_grp_001\n"
                + " where grupo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, groupCode);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdOperacoesGrp001(resultSetSql.getInt(1),
                    resultSetSql.getInt(2)));
        }
        resultSetSql.close();
        super.closeConnection();

        rows.forEach(row -> {
            try {
                row.setOperacao(DAOFactory.getSdOperacaoOrganize001DAO().getByCode(row.getCodigoOperacao()));
            } catch (SQLException ex) {
                Logger.getLogger(SdOperacoesGrp001DAO.class.getName()).log(Level.SEVERE, null, ex);
                GUIUtils.showException(ex);
            }
        });

        return rows;
    }

    //--------------
    public ObservableList<SdOperacoesGrp001> getAvailableOperations(Integer codeGroup) throws SQLException {
        ObservableList<SdOperacoesGrp001> rows = FXCollections.observableArrayList();

        DAOFactory.getSdOperacaoOrganize001DAO().getNotInGroup(codeGroup).forEach(operacao -> {
            rows.add(new SdOperacoesGrp001(0,
                    operacao.getCodorg(),
                    operacao));
        });

        return rows;
    }

    public ObservableList<SdOperacoesGrp001> getAvailableOperationsGroup(Integer codeGroup, Integer codePerson) throws SQLException {
        ObservableList<SdOperacoesGrp001> rows = FXCollections.observableArrayList();

        DAOFactory.getSdOperacaoOrganize001DAO().getNotInPersonAndInGroup(codeGroup, codePerson).forEach(operacao -> {
            rows.add(new SdOperacoesGrp001(0,
                    operacao.getCodorg(),
                    operacao));
        });

        return rows;
    }
}
