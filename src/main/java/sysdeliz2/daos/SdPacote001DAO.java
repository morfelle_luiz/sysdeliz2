/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdPacote001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdPacote001DAO extends FactoryConnection {

    public SdPacote001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdPacote001> getByNumero(String numero) throws SQLException {
        ObservableList<SdPacote001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select * from sd_pacote_001 where numero = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, numero);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
    
    public ObservableList<SdPacote001> getListByPacote(String pacote) throws SQLException {
        ObservableList<SdPacote001> row = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("select * from sd_pacote_001 where codigo LIKE ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote+"%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.add(new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9)
            ));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public SdPacote001 getByPacote(String pacote) throws SQLException {
        SdPacote001 row = null;

        StringBuilder query = new StringBuilder("select * from sd_pacote_001 where codigo LIKE ? order by ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote+"%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9)
            );
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public ObservableList<SdPacote001> getByNumeroAndCodigo(String numero, String referencia) throws SQLException {
        ObservableList<SdPacote001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select * from sd_pacote_001 where numero = ? and referencia = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, numero);
        super.preparedStatement.setString(2, referencia);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("insert into sd_pacote_001 values (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.setInt(2, pacote.getOrdem());
        super.preparedStatement.setString(3, pacote.getNumero());
        super.preparedStatement.setString(4, pacote.getReferencia());
        super.preparedStatement.setString(5, pacote.getCor());
        super.preparedStatement.setString(6, pacote.getTam());
        super.preparedStatement.setInt(7, pacote.getQtde());
        super.preparedStatement.setString(8, pacote.getTipo());
        super.preparedStatement.setString(9, pacote.getTipoLancamento());
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public void update(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("" +
                "merge into sd_pacote_001 pct\n" +
                "using dual\n" +
                "on (pct.codigo = ?)\n" +
                "when matched then update set pct.tipo_lanc = ?\n" +
                "when not matched then insert(pct.codigo, pct.ordem, pct.numero, pct.referencia, pct.cor, pct.tam, pct.qtde, pct.tipo, pct.tipo_lanc) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.setString(2, pacote.getTipoLancamento());
        super.preparedStatement.setString(3, pacote.getCodigo());
        super.preparedStatement.setInt(4, pacote.getOrdem());
        super.preparedStatement.setString(5, pacote.getNumero());
        super.preparedStatement.setString(6, pacote.getReferencia());
        super.preparedStatement.setString(7, pacote.getCor());
        super.preparedStatement.setString(8, pacote.getTam());
        super.preparedStatement.setInt(9, pacote.getQtde());
        super.preparedStatement.setString(10, pacote.getTipo());
        super.preparedStatement.setString(11, pacote.getTipoLancamento());
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public void delete(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_pacote_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.execute();

        super.closeConnection();
    }
}
