/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdPacote001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdPacoteLancamento001DAO extends FactoryConnection {

    public SdPacoteLancamento001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdPacote001> getByPacoteProg(String pacote) throws SQLException {
        ObservableList<SdPacote001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select pct.codigo,\n" +
                "       pct.ordem,\n" +
                "       pct.referencia,\n" +
                "       pct.cor,\n" +
                "       cor.descricao,\n" +
                "       pct.tam,\n" +
                "       pct.qtde,\n" +
                "       pct.tipo\n" +
                "  from sd_pacote_lancamento_001 pct\n" +
                "  join cadcor_001 cor\n" +
                "    on cor.cor = pct.cor\n" +
                " where pct.codigo = ?\n" +
                " order by pct.ordem");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    null,
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    null));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
    
    public SdPacote001 getByPacoteProgOrdem(String pacote, Integer ordem) throws SQLException {
        SdPacote001 row = null;
        
        StringBuilder query = new StringBuilder("" +
                "select pct.codigo,\n" +
                "       pct.ordem,\n" +
                "       pct.referencia,\n" +
                "       pct.cor,\n" +
                "       cor.descricao,\n" +
                "       pct.tam,\n" +
                "       pct.qtde,\n" +
                "       pct.tipo\n" +
                "  from sd_pacote_lancamento_001 pct\n" +
                "  join cadcor_001 cor\n" +
                "    on cor.cor = pct.cor\n" +
                " where pct.codigo = ?\n" +
                "   and pct.ordem = ?\n" +
                " order by pct.ordem");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote);
        super.preparedStatement.setInt(2, ordem);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdPacote001(
                    resultSetSql.getString(1),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    null,
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    null);
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public void save(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("insert into sd_pacote_lancamento_001 values (?, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.setInt(2, pacote.getOrdem());
        super.preparedStatement.setString(3, pacote.getReferencia());
        super.preparedStatement.setString(4, pacote.getCor());
        super.preparedStatement.setString(5, pacote.getTam());
        super.preparedStatement.setInt(6, pacote.getQtde());
        super.preparedStatement.setString(7, pacote.getTipo());
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public void delete(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_pacote_lancamento_001 where codigo = ? and ordem = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.setInt(2, pacote.getOrdem());
        super.preparedStatement.execute();

        super.closeConnection();
    }

    public void deleteByPacoteProg(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_pacote_lancamento_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        super.preparedStatement.execute();

        super.closeConnection();
    }
}
