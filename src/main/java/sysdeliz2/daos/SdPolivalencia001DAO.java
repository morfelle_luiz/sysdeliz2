/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdNivelPolivalencia001;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.sysdeliz.SdPolivalencia001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdPolivalencia001DAO extends FactoryConnection {

    public SdPolivalencia001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdPolivalencia001> getAll() throws SQLException {
        ObservableList<SdPolivalencia001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select pol.colaborador pol_colaborador,\n" +
                "       pol.operacao pol_operacao,\n" +
                "       pol.nivel pol_nivel,\n" +
                "       pol.ativo pol_ativo,\n" +
                "       ope.codigo ope_codigo,\n" +
                "       ope.codorg ope_codorg,\n" +
                "       ope.descricao ope_descricao,\n" +
                "       ope.tempo ope_tempo,\n" +
                "       ope.tempo_hora ope_tempo_hora,\n" +
                "       ope.custo ope_custo,\n" +
                "       ope.concessao ope_concessao,\n" +
                "       ope.equipamento ope_equipamento,\n" +
                "       ope.dt_inclusao ope_dt_inclusao,\n" +
                "       ope.dt_alteracao ope_dt_alteracao,\n" +
                "       ope.comp_custura ope_comp_custura,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       niv.codigo niv_codigo,\n" +
                "       niv.descricao niv_descricao,\n" +
                "       niv.meta_inicio niv_meta_inicio,\n" +
                "       niv.meta_fim niv_meta_fim,\n" +
                "       niv.premio niv_premio,\n" +
                "       niv.sigla niv_sigla,\n" +
                "       niv.ordem niv_ordem\n" +
                "  from sd_polivalencia_001 pol,\n" +
                "       sd_operacao_organize_001 ope,\n" +
                "       sd_colaborador_001 col,\n" +
                "       sd_nivel_polivalencia_001 niv" +
                " where pol.colaborador = col.codigo\n" +
                "   and pol.operacao = ope.codorg\n" +
                "   and pol.nivel = niv.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdPolivalencia001(resultSetSql.getInt("pol_colaborador"),
                    resultSetSql.getInt("pol_operacao"),
                    resultSetSql.getInt("pol_nivel"),
                    resultSetSql.getString("pol_ativo"),
                    new SdColaborador(
                            resultSetSql.getInt("col_codigo"),
                            resultSetSql.getString("col_nome"),
                            resultSetSql.getInt("col_funcao"),
                            resultSetSql.getInt("col_turno"),
                            resultSetSql.getInt("col_codigo_rh"),
                            resultSetSql.getString("col_ativo")),
                    new SdOperacaoOrganize001(
                            resultSetSql.getInt("ope_codorg"),
                            resultSetSql.getString("ope_codigo"),
                            resultSetSql.getString("ope_descricao"),
                            resultSetSql.getDouble("ope_tempo"),
                            resultSetSql.getDouble("ope_tempo_hora"),
                            resultSetSql.getDouble("ope_custo"),
                            resultSetSql.getDouble("ope_concessao"),
                            resultSetSql.getString("ope_equipamento"),
                            resultSetSql.getString("ope_dt_inclusao"),
                            resultSetSql.getString("ope_dt_alteracao"),
                            resultSetSql.getDouble("ope_comp_custura")),
                    new SdNivelPolivalencia001(
                            resultSetSql.getInt("niv_codigo"),
                            resultSetSql.getString("niv_descricao"),
                            resultSetSql.getDouble("niv_meta_inicio"),
                            resultSetSql.getDouble("niv_meta_fim"),
                            resultSetSql.getDouble("niv_premio"),
                            resultSetSql.getString("niv_sigla"))
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdPolivalencia001 objectToSave) throws SQLException {

        StringBuilder query = new StringBuilder(""
                + " insert into sd_polivalencia_001\n"
                + "   (colaborador, operacao, nivel, ativo)\n"
                + " values\n"
                + "   (?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoColaborador());
        super.preparedStatement.setInt(2, objectToSave.getCodigoOperacao());
        super.preparedStatement.setInt(3, objectToSave.getCodigoNivel());
        super.preparedStatement.setString(4, objectToSave.isAtivo() ? "S" : "N");
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void forcarPolivalencia(SdPolivalencia001 objectToSave) throws SQLException {

        StringBuilder query = new StringBuilder(""
                + " insert into sd_polivalencia_001\n"
                + "   (colaborador, operacao, nivel, ativo)\n"
                + " values\n"
                + "   (?, ?, (select codigo from sd_nivel_polivalencia_001 where ordem = (select max(ordem) from sd_nivel_polivalencia_001)), ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoColaborador());
        super.preparedStatement.setInt(2, objectToSave.getCodigoOperacao());
        super.preparedStatement.setString(3, objectToSave.isAtivo() ? "S" : "N");
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void update(SdPolivalencia001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_polivalencia_001\n"
                        + "   set nivel = ?,\n"
                        + "       ativo = ?\n"
                        + " where colaborador = ? and operacao = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoNivel());
        super.preparedStatement.setString(2, objectToSave.isAtivo() ? "S" : "N");
        super.preparedStatement.setInt(3, objectToSave.getCodigoColaborador());
        super.preparedStatement.setInt(4, objectToSave.getCodigoOperacao());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void delete(SdPolivalencia001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_polivalencia_001 where colaborador = ? and operacao = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoColaborador());
        super.preparedStatement.setInt(2, objectToSave.getCodigoOperacao());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void deletePerson(SdColaborador objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_polivalencia_001 where colaborador = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }

    public ObservableList<SdPolivalencia001> getByColaborador(SdColaborador colaborador) throws SQLException {
        ObservableList<SdPolivalencia001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select pol.colaborador pol_colaborador,\n" +
                "       pol.operacao pol_operacao,\n" +
                "       pol.nivel pol_nivel,\n" +
                "       pol.ativo pol_ativo,\n" +
                "       ope.codigo ope_codigo,\n" +
                "       ope.codorg ope_codorg,\n" +
                "       ope.descricao ope_descricao,\n" +
                "       ope.tempo ope_tempo,\n" +
                "       ope.tempo_hora ope_tempo_hora,\n" +
                "       ope.custo ope_custo,\n" +
                "       ope.concessao ope_concessao,\n" +
                "       ope.equipamento ope_equipamento,\n" +
                "       ope.dt_inclusao ope_dt_inclusao,\n" +
                "       ope.dt_alteracao ope_dt_alteracao,\n" +
                "       ope.comp_custura ope_comp_custura,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       niv.codigo niv_codigo,\n" +
                "       niv.descricao niv_descricao,\n" +
                "       niv.meta_inicio niv_meta_inicio,\n" +
                "       niv.meta_fim niv_meta_fim,\n" +
                "       niv.premio niv_premio,\n" +
                "       niv.sigla niv_sigla,\n" +
                "       niv.ordem niv_ordem\n" +
                "  from sd_polivalencia_001       pol,\n" +
                "       sd_operacao_organize_001  ope,\n" +
                "       sd_colaborador_001        col,\n" +
                "       sd_nivel_polivalencia_001 niv\n" +
                " where pol.colaborador = col.codigo\n" +
                "   and pol.operacao = ope.codorg\n" +
                "   and pol.nivel = niv.codigo" +
                "   and pol.colaborador = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador.getCodigo());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            SdOperacaoOrganize001 operacaoPolivalencia = new SdOperacaoOrganize001(
                    resultSetSql.getInt("ope_codorg"),
                    resultSetSql.getString("ope_codigo"),
                    resultSetSql.getString("ope_descricao"),
                    resultSetSql.getDouble("ope_tempo"),
                    resultSetSql.getDouble("ope_tempo_hora"),
                    resultSetSql.getDouble("ope_custo"),
                    resultSetSql.getDouble("ope_concessao"),
                    resultSetSql.getString("ope_equipamento"),
                    resultSetSql.getString("ope_dt_inclusao"),
                    resultSetSql.getString("ope_dt_alteracao"),
                    resultSetSql.getDouble("ope_comp_custura"));
            SdNivelPolivalencia001 nivelOperacao = new SdNivelPolivalencia001(
                    resultSetSql.getInt("niv_codigo"),
                    resultSetSql.getString("niv_descricao"),
                    resultSetSql.getDouble("niv_meta_inicio"),
                    resultSetSql.getDouble("niv_meta_fim"),
                    resultSetSql.getDouble("niv_premio"),
                    resultSetSql.getString("niv_sigla"));
            rows.add(new SdPolivalencia001(resultSetSql.getInt("pol_colaborador"),
                    resultSetSql.getInt("pol_operacao"),
                    resultSetSql.getInt("pol_nivel"),
                    resultSetSql.getString("pol_ativo"),
                    colaborador,
                    operacaoPolivalencia,
                    nivelOperacao
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdPolivalencia001> getByPolivalencia(SdColaborador colaborador, SdOperacaoOrganize001 operacao) throws SQLException {
        ObservableList<SdPolivalencia001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select pol.colaborador pol_colaborador,\n" +
                "       pol.operacao pol_operacao,\n" +
                "       pol.nivel pol_nivel,\n" +
                "       pol.ativo pol_ativo,\n" +
                "       ope.codigo ope_codigo,\n" +
                "       ope.codorg ope_codorg,\n" +
                "       ope.descricao ope_descricao,\n" +
                "       ope.tempo ope_tempo,\n" +
                "       ope.tempo_hora ope_tempo_hora,\n" +
                "       ope.custo ope_custo,\n" +
                "       ope.concessao ope_concessao,\n" +
                "       ope.equipamento ope_equipamento,\n" +
                "       ope.dt_inclusao ope_dt_inclusao,\n" +
                "       ope.dt_alteracao ope_dt_alteracao,\n" +
                "       ope.comp_custura ope_comp_custura,\n" +
                "       col.codigo col_codigo,\n" +
                "       col.nome col_nome,\n" +
                "       col.funcao col_funcao,\n" +
                "       col.turno col_turno,\n" +
                "       col.codigo_rh col_codigo_rh,\n" +
                "       col.ativo col_ativo,\n" +
                "       niv.codigo niv_codigo,\n" +
                "       niv.descricao niv_descricao,\n" +
                "       niv.meta_inicio niv_meta_inicio,\n" +
                "       niv.meta_fim niv_meta_fim,\n" +
                "       niv.premio niv_premio,\n" +
                "       niv.sigla niv_sigla,\n" +
                "       niv.ordem niv_ordem\n" +
                "  from sd_polivalencia_001       pol,\n" +
                "       sd_operacao_organize_001  ope,\n" +
                "       sd_colaborador_001        col,\n" +
                "       sd_nivel_polivalencia_001 niv\n" +
                " where pol.colaborador = col.codigo\n" +
                "   and pol.operacao = ope.codorg\n" +
                "   and pol.nivel = niv.codigo\n" +
                "   and pol.colaborador = ?\n" +
                "   and pol.operacao = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador.getCodigo());
        super.preparedStatement.setInt(2, operacao.getCodorg());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            SdOperacaoOrganize001 operacaoPolivalencia = new SdOperacaoOrganize001(
                    resultSetSql.getInt("ope_codorg"),
                    resultSetSql.getString("ope_codigo"),
                    resultSetSql.getString("ope_descricao"),
                    resultSetSql.getDouble("ope_tempo"),
                    resultSetSql.getDouble("ope_tempo_hora"),
                    resultSetSql.getDouble("ope_custo"),
                    resultSetSql.getDouble("ope_concessao"),
                    resultSetSql.getString("ope_equipamento"),
                    resultSetSql.getString("ope_dt_inclusao"),
                    resultSetSql.getString("ope_dt_alteracao"),
                    resultSetSql.getDouble("ope_comp_custura"));
            SdNivelPolivalencia001 nivelOperacao = new SdNivelPolivalencia001(
                    resultSetSql.getInt("niv_codigo"),
                    resultSetSql.getString("niv_descricao"),
                    resultSetSql.getDouble("niv_meta_inicio"),
                    resultSetSql.getDouble("niv_meta_fim"),
                    resultSetSql.getDouble("niv_premio"),
                    resultSetSql.getString("niv_sigla"));
            rows.add(new SdPolivalencia001(resultSetSql.getInt("pol_colaborador"),
                    resultSetSql.getInt("pol_operacao"),
                    resultSetSql.getInt("pol_nivel"),
                    resultSetSql.getString("pol_ativo"),
                    colaborador,
                    operacaoPolivalencia,
                    nivelOperacao
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
}
