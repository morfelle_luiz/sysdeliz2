/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdProduto001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cristiano.diego
 */
public class SdProduto001DAO extends FactoryConnection {

    public SdProduto001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdProduto001> getAll() throws SQLException {
        ObservableList<SdProduto001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select codigo, to_char(observacao_pcp) from sd_produto_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProduto001(resultSetSql.getString(1),
                    resultSetSql.getString(2)));
        }
        resultSetSql.close();
        super.closeConnection();
        return rows;
    }

    public void save(SdProduto001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_produto_001\n"
                + "   (codigo, observacao_pcp)\n"
                + " values\n"
                + "   (?, to_clob(?))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getCodigo());
        super.preparedStatement.setString(2, objectToSave.getObservacaoPcp());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        super.closeConnection();
    }

    public void update(SdProduto001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_produto_001\n"
                        + "   set observacao_pcp = to_clob(?)\n"
                        + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getObservacaoPcp());
        super.preparedStatement.setString(2, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }
        super.closeConnection();
    }

    public SdProduto001 getByCodigo(String codigo) throws SQLException {
        SdProduto001 row = null;

        StringBuilder query = new StringBuilder(""
                + "select codigo, to_char(observacao_pcp) from sd_produto_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, codigo);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdProduto001(resultSetSql.getString(1),
                    resultSetSql.getString(2));
        }
        resultSetSql.close();
        super.closeConnection();
        return row;
    }
}
