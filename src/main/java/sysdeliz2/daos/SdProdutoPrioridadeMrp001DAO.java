/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdProdutoPrioridadeMrp001;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdProdutoPrioridadeMrp001DAO extends FactoryConnection {

    public SdProdutoPrioridadeMrp001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public void save(SdProdutoPrioridadeMrp001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_produto_prioridade_mrp_001\n"
                + "   (codigo, prioridade)\n"
                + " values\n"
                + "   (?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getCodigo());
        super.preparedStatement.setInt(2, objectToSave.getPrioridade());
        int affectedRows = super.preparedStatement.executeUpdate();
        super.closeConnection();
    }

    public void deleteAll() throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_produto_prioridade_mrp_001");

        super.initPreparedStatement(query.toString());
        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
//        }

        super.closeConnection();
    }

    public void delete(SdProdutoPrioridadeMrp001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_produto_prioridade_mrp_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
//        }

        super.closeConnection();
    }

    public ObservableList<SdProdutoPrioridadeMrp001> getAvailableCollectionActual() throws SQLException {
        ObservableList<SdProdutoPrioridadeMrp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select codigo, descricao, 5 prioridade\n"
                + "  from produto_001\n"
                + " where colecao = (select codigo from colecao_001 where codigo <> '17CO' and sysdate between inicio_vig and fim_vig)\n"
                + "   and codigo not in (select codigo from sd_produto_prioridade_mrp_001)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProdutoPrioridadeMrp001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdProdutoPrioridadeMrp001> getAvailableByFamily(String familyCode) throws SQLException {
        ObservableList<SdProdutoPrioridadeMrp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select codigo, descricao, 5 prioridade\n"
                + "  from produto_001\n"
                + " where familia in (" + familyCode + ")\n"
                + "   and codigo not in (select codigo from sd_produto_prioridade_mrp_001)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProdutoPrioridadeMrp001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdProdutoPrioridadeMrp001> getAvailableByFilter(String familyCode, String collectionCode) throws SQLException {
        ObservableList<SdProdutoPrioridadeMrp001> rows = FXCollections.observableArrayList();

        String filterWindow = "";
        filterWindow += familyCode.length() > 0 ? "and familia in (" + familyCode + ")\n" : "";
        filterWindow += collectionCode.length() > 0 ? "and colecao in (" + collectionCode + ")\n" : "";

        StringBuilder query = new StringBuilder(""
                + "select codigo, descricao, 5 prioridade\n"
                + "  from produto_001\n"
                + " where codigo not in (select codigo from sd_produto_prioridade_mrp_001)\n"
                + filterWindow);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProdutoPrioridadeMrp001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdProdutoPrioridadeMrp001> getSelectedPriorities() throws SQLException {
        ObservableList<SdProdutoPrioridadeMrp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select prd.codigo, prd.descricao, mrp.prioridade\n"
                + "  from sd_produto_prioridade_mrp_001 mrp\n"
                + " inner join produto_001 prd\n"
                + "    on prd.codigo = mrp.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProdutoPrioridadeMrp001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getInt(3)));
        }

        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
}
