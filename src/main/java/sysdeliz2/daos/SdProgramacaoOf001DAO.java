/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdProgramacaoOf001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

/**
 * @author cristiano.diego
 */
public class SdProgramacaoOf001DAO extends FactoryConnection {

    public SdProgramacaoOf001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdProgramacaoOf001> getAll() throws SQLException {
        ObservableList<SdProgramacaoOf001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select * from sd_programacao_of_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdProgramacaoOf001(
                    resultSetSql.getTimestamp(1).toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getInt(6),
                    resultSetSql.getString(7)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdProgramacaoOf001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder("insert into sd_programacao_of_001 values (sysdate, ?, ?, ?, ?, ?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, programacao.getProgramador());
        super.preparedStatement.setString(2, programacao.getSetor());
        super.preparedStatement.setString(3, programacao.getOrdemProd());
        super.preparedStatement.setString(4, programacao.getTipoPacote());
        super.preparedStatement.setInt(5, programacao.getQtde());
        super.preparedStatement.setString(6, programacao.isProdFinalizada() ? "S" : "N");
        super.preparedStatement.execute();
        super.closeConnection();
    }

    public void delete(SdProgramacaoOf001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_programacao_of_001 where ordem_prod = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getOrdemProd());
        super.preparedStatement.execute();
        super.closeConnection();
    }

    public void deleteBySetor(SdProgramacaoOf001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_programacao_of_001 where ordem_prod = ? and setor = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getOrdemProd());
        super.preparedStatement.setString(2, programacao.getSetor());
        super.preparedStatement.execute();
        super.closeConnection();
    }

    public SdProgramacaoOf001 getByOf(String numero) throws SQLException {
        SdProgramacaoOf001 row = null;

        StringBuilder query = new StringBuilder("select * from sd_programacao_of_001 where ordem_prod = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, numero);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdProgramacaoOf001(
                    resultSetSql.getTimestamp(1).toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")),
                    resultSetSql.getInt(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getInt(6),
                    resultSetSql.getString(7)
            );
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }
}
