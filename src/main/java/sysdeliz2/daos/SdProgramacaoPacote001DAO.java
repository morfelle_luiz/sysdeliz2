/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.SdPacote001;
import sysdeliz2.models.sysdeliz.SdProgramacaoPacote002;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author cristiano.diego
 */
public class SdProgramacaoPacote001DAO extends FactoryConnection {
    
    public SdProgramacaoPacote001DAO() throws SQLException {
        this.initConnection();
    }
    
    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
    
    public ObservableList<SdProgramacaoPacote001> getAll() throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdProgramacaoPacote001> getByPacote(String pacote) throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " where prog.pacote = ?\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote);
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdProgramacaoPacote001> getByOf(String numero) throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " where substr(pacote,1,6) = ?\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, numero);
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdProgramacaoPacote001> getOcupacaoColaborador(Integer colaborador) throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.*\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_hist_imp_prog_colab_001 imp\n" +
                "    on imp.colaborador = prog.colaborador\n" +
                "   and imp.pacote = prog.pacote\n" +
                "   and imp.operacao = prog.operacao\n" +
                "   and imp.ordem = prog.ordem\n" +
                "   and imp.quebra = prog.quebra\n" +
                " where prog.colaborador = ?\n" +
                "   and (imp.colaborador is null or imp.impresso = 'N')\n" +
                " order by prog.dh_inicio, prog.ordem, prog.quebra");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17)
            
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdProgramacaoPacote001> getOcupacaoColaborador(Integer colaborador, LocalDateTime dataHoraProgramacao) throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " where colaborador = ? and to_date(?,'DD/MM/YYYY HH24:MI') between dh_inicio and dh_fim\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.setString(2, dataHoraProgramacao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdProgramacaoPacote001> getOcupacaoColaboradorDia(Integer colaborador, LocalDateTime dataHoraProgramacao) throws SQLException {
        ObservableList<SdProgramacaoPacote001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " where prog.colaborador = ?\n" +
                "   and to_date(to_char(prog.dh_inicio,'DD/MM/YYYY'),'DD/MM/YYYY') = to_date(?,'DD/MM/YYYY')\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, colaborador);
        super.preparedStatement.setString(2, dataHoraProgramacao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        while (retornoSql.next()) {
            rows.add(new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            ));
        }
        retornoSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public SdProgramacaoPacote001 getUltimaOcupacaoCelula(Integer codigoCelula) throws SQLException {
        SdProgramacaoPacote001 row = null;
        
        StringBuilder query = new StringBuilder("" +
                "select prog.pacote,\n" +
                "       prog.operacao,\n" +
                "       prog.ordem,\n" +
                "       prog.dh_inicio,\n" +
                "       prog.dh_ib,\n" +
                "       prog.dh_fim,\n" +
                "       prog.celula,\n" +
                "       prog.setor_op,\n" +
                "       prog.maquina,\n" +
                "       prog.colaborador,\n" +
                "       prog.qtde,\n" +
                "       prog.independencia,\n" +
                "       prog.agrupador,\n" +
                "       prog.ib,\n" +
                "       prog.quebra,\n" +
                "       prog.qtde_ib,\n" +
                "       prog.tempo_op,\n" +
                "       count(lanc.pacote_lanc) has_lanc\n" +
                "  from sd_programacao_pacote_001 prog\n" +
                "  join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_lancamento_producao_001 lanc\n" +
                "    on lanc.pacote_prog = prog.pacote\n" +
                "   and lanc.operacao = prog.operacao\n" +
                "   and lanc.ordem = prog.ordem\n" +
                " where prog.celula = ?\n" +
                "   and prog.dh_fim =\n" +
                "       (select max(prog2.dh_fim) from sd_programacao_pacote_001 prog2 where prog.pacote <> prog2.pacote and prog2.celula = ?)\n" +
                " group by prog.pacote,\n" +
                "          prog.operacao,\n" +
                "          prog.ordem,\n" +
                "          prog.dh_inicio,\n" +
                "          prog.dh_ib,\n" +
                "          prog.dh_fim,\n" +
                "          prog.celula,\n" +
                "          prog.setor_op,\n" +
                "          prog.maquina,\n" +
                "          prog.colaborador,\n" +
                "          prog.qtde,\n" +
                "          prog.independencia,\n" +
                "          prog.agrupador,\n" +
                "          prog.ib,\n" +
                "          prog.quebra,\n" +
                "          prog.qtde_ib,\n" +
                "          prog.tempo_op\n" +
                " order by prog.ordem");
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codigoCelula);
        super.preparedStatement.setInt(2, codigoCelula);
        super.preparedStatement.execute();
        ResultSet retornoSql = super.preparedStatement.getResultSet();
        if (retornoSql.next()) {
            row = new SdProgramacaoPacote001(
                    retornoSql.getString(1),
                    retornoSql.getInt(2),
                    retornoSql.getInt(3),
                    retornoSql.getTimestamp(4).toLocalDateTime(),
                    retornoSql.getTimestamp(5).toLocalDateTime(),
                    retornoSql.getTimestamp(6).toLocalDateTime(),
                    retornoSql.getInt(7),
                    retornoSql.getInt(8),
                    retornoSql.getInt(9),
                    retornoSql.getInt(10),
                    retornoSql.getInt(11),
                    retornoSql.getString(12),
                    retornoSql.getInt(13),
                    retornoSql.getDouble(14),
                    retornoSql.getInt(15),
                    retornoSql.getInt(16),
                    retornoSql.getDouble(17),
                    retornoSql.getInt(18) > 0
            );
        }
        retornoSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public void save(SdProgramacaoPacote001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "insert into sd_programacao_pacote_001\n"
                + "values\n"
                + "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getPacote());
        super.preparedStatement.setInt(2, programacao.getOperacao());
        super.preparedStatement.setInt(3, programacao.getOrdem());
        super.preparedStatement.setTimestamp(4, Timestamp.valueOf(programacao.getDhInicio()));
        super.preparedStatement.setTimestamp(5, Timestamp.valueOf(programacao.getDhIb()));
        super.preparedStatement.setTimestamp(6, Timestamp.valueOf(programacao.getDhFim()));
        super.preparedStatement.setInt(7, programacao.getCelula());
        super.preparedStatement.setInt(8, programacao.getSetorOp());
        super.preparedStatement.setInt(9, programacao.getMaquina());
        super.preparedStatement.setInt(10, programacao.getColaborador());
        super.preparedStatement.setInt(11, programacao.getQtde());
        super.preparedStatement.setString(12, programacao.getIndependencia());
        super.preparedStatement.setInt(13, programacao.getAgrupador());
        super.preparedStatement.setDouble(14, programacao.getIb());
        super.preparedStatement.setInt(15, programacao.getQuebra());
        super.preparedStatement.setInt(16, programacao.getQtdeIb());
        super.preparedStatement.setDouble(17, programacao.getTempoOperacao());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void update(SdProgramacaoPacote001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder("" +
                "update sd_programacao_pacote_001 prog\n" +
                "   set prog.dh_inicio = ?,\n" +
                "       prog.dh_ib = ?,\n" +
                "       prog.dh_fim = ?,\n" +
                "       prog.celula = ?,\n" +
                "       prog.maquina = ?,\n" +
                "       prog.colaborador = ?,\n" +
                "       prog.qtde = ?,\n" +
                "       prog.independencia = ?,\n" +
                "       prog.agrupador = ?,\n" +
                "       prog.ib = ?,\n" +
                "       prog.qtde_ib = ?,\n" +
                "       prog.tempo_op = ?\n" +
                " where prog.pacote = ?\n" +
                "   and prog.setor_op = ?\n" +
                "   and prog.operacao = ?\n" +
                "   and prog.ordem = ?\n" +
                "   and prog.quebra = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setTimestamp(1, Timestamp.valueOf(programacao.getDhInicio()));
        super.preparedStatement.setTimestamp(2, Timestamp.valueOf(programacao.getDhIb()));
        super.preparedStatement.setTimestamp(3, Timestamp.valueOf(programacao.getDhFim()));
        super.preparedStatement.setInt(4, programacao.getCelula());
        super.preparedStatement.setInt(5, programacao.getMaquina());
        super.preparedStatement.setInt(6, programacao.getColaborador());
        super.preparedStatement.setInt(7, programacao.getQtde());
        super.preparedStatement.setString(8, programacao.getIndependencia());
        super.preparedStatement.setInt(9, programacao.getAgrupador());
        super.preparedStatement.setDouble(10, programacao.getIb());
        super.preparedStatement.setInt(11, programacao.getQtdeIb());
        super.preparedStatement.setDouble(12, programacao.getTempoOperacao());
        super.preparedStatement.setString(13, programacao.getPacote());
        super.preparedStatement.setInt(14, programacao.getSetorOp());
        super.preparedStatement.setInt(15, programacao.getOperacao());
        super.preparedStatement.setInt(16, programacao.getOrdem());
        super.preparedStatement.setInt(17, programacao.getQuebra());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void delete(SdProgramacaoPacote001 programacao) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_programacao_pacote_001 where pacote = ? and setor_op = ? and operacao = ? and ordem = ? and quebra = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getPacote());
        super.preparedStatement.setInt(2, programacao.getSetorOp());
        super.preparedStatement.setInt(3, programacao.getOperacao());
        super.preparedStatement.setInt(4, programacao.getOrdem());
        super.preparedStatement.setInt(5, programacao.getQuebra());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void deleteByPacote(SdPacote001 pacote) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_programacao_pacote_001 where pacote = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, pacote.getCodigo());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void save(SdProgramacaoPacote002 programacao) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "insert into sd_programacao_pacote_002\n"
                + "values\n"
                + "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getId().getPacote());
        super.preparedStatement.setInt(2, programacao.getId().getOperacao().getCodorg());
        super.preparedStatement.setInt(3, programacao.getId().getOrdem());
        super.preparedStatement.setString(4, (programacao.getCelula() != null && programacao.getCelula().getCodigo() != 0) ? String.valueOf(programacao.getCelula().getCodigo()) : null);
        super.preparedStatement.setInt(5, programacao.getId().getSetorOp().getCodigo());
        super.preparedStatement.setInt(6, programacao.getMaquina().getCodorg());
        super.preparedStatement.setString(7, (programacao.getColaborador() != null && programacao.getColaborador().getCodigo() != 0) ? String.valueOf(programacao.getColaborador().getCodigo()) : null);
        super.preparedStatement.setInt(8, programacao.getQtde());
        super.preparedStatement.setInt(9, programacao.getAgrupador());
        super.preparedStatement.setInt(10, programacao.getId().getQuebra());
        super.preparedStatement.setDouble(11, programacao.getTempoOp().doubleValue());
        super.preparedStatement.setTimestamp(12, Timestamp.valueOf(programacao.getDtProgramacao()));
        super.preparedStatement.setInt(13, programacao.getSeqColaborador());
        super.preparedStatement.setInt(14, programacao.getStatusProg());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void update(SdProgramacaoPacote002 programacao) throws SQLException {
        StringBuilder query = new StringBuilder("" +
                "update sd_programacao_pacote_002 prog\n" +
                "   set prog.celula = ?,\n" +
                "       prog.maquina = ?,\n" +
                "       prog.colaborador = ?,\n" +
                "       prog.qtde = ?,\n" +
                "       prog.agrupador = ?,\n" +
                "       prog.tempo_op = ?,\n" +
                "       prog.SEQ_COLABORADOR = ?,\n" +
                "       prog.STATUS_PROG = ?,\n" +
                "       prog.DT_PROGRAMACAO = ?\n" +
                " where prog.pacote = ?\n" +
                "   and prog.setor_op = ?\n" +
                "   and prog.operacao = ?\n" +
                "   and prog.ordem = ?\n" +
                "   and prog.quebra = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, (programacao.getCelula() != null && programacao.getCelula().getCodigo() != 0) ? String.valueOf(programacao.getCelula().getCodigo()) : null);
        super.preparedStatement.setInt(2, programacao.getMaquina().getCodorg());
        super.preparedStatement.setString(3, (programacao.getColaborador() != null && programacao.getColaborador().getCodigo() != 0) ? String.valueOf(programacao.getColaborador().getCodigo()) : null);
        super.preparedStatement.setInt(4, programacao.getQtde());
        super.preparedStatement.setInt(5, programacao.getAgrupador());
        super.preparedStatement.setDouble(6, programacao.getTempoOp().doubleValue());
        super.preparedStatement.setInt(7, programacao.getSeqColaborador());
        super.preparedStatement.setInt(8, programacao.getStatusProg());
        super.preparedStatement.setTimestamp(9, Timestamp.valueOf(LocalDateTime.now()));
        super.preparedStatement.setString(10, programacao.getId().getPacote());
        super.preparedStatement.setInt(11, programacao.getId().getSetorOp().getCodigo());
        super.preparedStatement.setInt(12, programacao.getId().getOperacao().getCodorg());
        super.preparedStatement.setInt(13, programacao.getOrdemOriginal());
        super.preparedStatement.setInt(14, programacao.getId().getQuebra());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
    
    public void delete(SdProgramacaoPacote002 programacao) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_programacao_pacote_002 where pacote = ? and setor_op = ? and operacao = ? and ordem = ? and quebra = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, programacao.getId().getPacote());
        super.preparedStatement.setInt(2, programacao.getId().getSetorOp().getCodigo());
        super.preparedStatement.setInt(3, programacao.getId().getOperacao().getCodorg());
        super.preparedStatement.setInt(4, programacao.getOrdemOriginal());
        super.preparedStatement.setInt(5, programacao.getId().getQuebra());
        
        super.preparedStatement.execute();
        super.closeConnection();
        
    }
}
