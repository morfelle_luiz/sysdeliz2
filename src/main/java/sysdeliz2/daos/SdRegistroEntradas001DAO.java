/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdRegistroEntrada001;
import sysdeliz2.utils.sys.DateUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lima.joao
 */
public class SdRegistroEntradas001DAO extends FactoryConnection {

    private String clauses;

    public SdRegistroEntradas001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdRegistroEntrada001> getAll() throws SQLException {
        return getBy(null);
    }

    public ObservableList<SdRegistroEntrada001> getBy(List<ObservableList<String>> wheresClauses) throws SQLException {

        ObservableList<SdRegistroEntrada001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder();
        query
                .append(" SELECT")
                .append("     codigo,")
                .append("     NOME_RESPONSAVEL,")
                .append("     EMAIL_RESPONSAVEL,")
                .append("     to_char(dh_entrada, 'dd/MM/yyyy HH24:MI:SS'),")
                .append("     to_char(dh_saida, 'dd/MM/yyyy HH24:MI:SS'),")
                .append("     pedir_lanche,")
                .append("     tipo_lanche,")
                .append("     situacao,")
                .append("     impresso,")
                .append("     to_char(dh_emissao, 'dd/MM/yyyy'),")
                .append("     to_char(dh_impresso, 'dd/MM/yyyy HH24:MI:SS'),")
                .append("     nome_funcionario nome_func,")
                .append("     setor nome_setor,")
                .append("     observacao,")
                .append("     com_cafe")
                .append(" FROM")
                .append("     SD_REGISTROS_ENTRADAS_001 s  ");

        if (wheresClauses != null) {
            clauses = "";
            wheresClauses.forEach(list -> clauses += list.get(0));

            query
                    .append(" WHERE ")
                    .append(clauses);
        }
           
        query
                .append("ORDER BY")
                .append("    dh_entrada asc");
        

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();

        while (resultSetSql.next()) {
            rows.add(
                    new SdRegistroEntrada001(
                            resultSetSql.getString(1),  // Codigo
                            resultSetSql.getString(2),  // nomeResponsavel
                            resultSetSql.getString(3),  // emailResponsavel
                            resultSetSql.getString(4),  // dataEntrada precisa converter
                            resultSetSql.getString(5),  // dataSaida precisa converter
                            resultSetSql.getString(6),  // pedirLanche
                            resultSetSql.getString(7),  // tipoLanche
                            resultSetSql.getString(8),  // situacao
                            resultSetSql.getString(9),  // impresso
                            resultSetSql.getString(10), // dataEmissao
                            resultSetSql.getString(11), // dataImpressao
                            resultSetSql.getString(12), // Nome Funcionario
                            resultSetSql.getString(13), // Nome Setor
                            resultSetSql.getString(14),  // Observacao
                            resultSetSql.getString(15)  // ComCafe
                    )
            );
        }
        resultSetSql.close();
        super.closeConnection();
        return rows;
    }

    public Integer add(SdRegistroEntrada001 registroEntrada001) throws SQLException {
        super.initPreparedStatement(registroEntrada001.buildInsertSQL());
        super.preparedStatement.execute();
        super.preparedStatement.close();

        String mSQL = "select max(codigo) from SD_REGISTROS_ENTRADAS_001";
        super.initPreparedStatement(mSQL);
        super.preparedStatement.execute();

        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        resultSetSql.next();
        Integer retorno = resultSetSql.getInt(1);

        super.preparedStatement.close();
        //super.preparedStatement.clearParameters();

        super.closeConnection();
        return retorno;
    }

    public Integer update(SdRegistroEntrada001 registroEntrada001) throws SQLException {
        super.initPreparedStatement(registroEntrada001.buildUpdateSQL());
        int affectedRows = super.preparedStatement.executeUpdate();
        super.preparedStatement.close();
        super.closeConnection();

        return affectedRows;
    }

    public Integer updateStatus(SdRegistroEntrada001 registroEntrada001) throws SQLException{
        String mSQL = "UPDATE SD_REGISTROS_ENTRADAS_001 " +
                "  SET situacao = '" + registroEntrada001.getSituacao() + "'," +
                "  impresso = '" + registroEntrada001.getImpresso() + "'," +
                "  dh_impresso = " + DateUtils.getInstance().getUpdateTimestamp(registroEntrada001.getDataImpressao()) +
                " WHERE codigo = " + registroEntrada001.getCodigo();

        super.initPreparedStatement(mSQL);
        int affectedRows = super.preparedStatement.executeUpdate();
        super.preparedStatement.close();
        super.closeConnection();

        return affectedRows;
    }

    public Boolean delete(int codigo) throws SQLException {
        if (codigo <= 0) {
            return false;
        }

        String mSQL = "DELETE FROM SD_REGISTROS_ENTRADAS_001 WHERE codigo = " + codigo;

        super.initPreparedStatement(mSQL);
        boolean success = super.preparedStatement.execute();
        super.preparedStatement.close();
        super.closeConnection();

        return success;
    }

    public Boolean existRegistroAberto(SdRegistroEntrada001 registro) throws SQLException {

        if (registro == null) {
            return false;
        }

        String mSQL = "SELECT" +
                "     count(codigo)" +
                " FROM" +
                "     SD_REGISTROS_ENTRADAS_001" +
                " WHERE" +
                "     NOME_FUNCIONARIO = '" + registro.getNomeFuncionario() + "'" +
                "     AND SITUACAO = 'Aberto'" +
                "     AND DH_ENTRADA = " + DateUtils.getInstance().getToDate(registro.getDataEntrada());

        super.initPreparedStatement(mSQL);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();

        resultSetSql.next();
        boolean retorno = Integer.parseInt(resultSetSql.getString(1)) > 0;

        super.preparedStatement.close();
        super.closeConnection();

        return retorno;
    }

    public void updateImpresso() throws SQLException {
        String mSQL = "UPDATE SD_REGISTROS_ENTRADAS_001 " +
                "  SET impresso = 'S'" +
                " s.SITUACAO = 'Fechado' AND s.PEDIR_LANCHE = 'S' AND s.IMPRESSO = 'N'";

        super.initPreparedStatement(mSQL);
        super.preparedStatement.executeUpdate();
        super.preparedStatement.close();
        super.closeConnection();
    }

}
