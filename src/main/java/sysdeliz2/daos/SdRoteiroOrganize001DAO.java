/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdRoteiroOrganize001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class SdRoteiroOrganize001DAO extends FactoryConnection {
    
    public SdRoteiroOrganize001DAO() throws SQLException {
        this.initConnection();
    }
    
    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
    
    private String whereFilter = "";
    
    public ObservableList<SdRoteiroOrganize001> getAll() throws SQLException {
        ObservableList<SdRoteiroOrganize001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                "       (select max(dt_alteracao)\n" +
                "          from sd_operacao_organize_001 oper\n" +
                "          join sd_oper_roteiro_organize_001 rot2\n" +
                "            on rot2.operacao = oper.codorg\n" +
                "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                "  from sd_roteiro_organize_001 rot\n");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16)));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public SdRoteiroOrganize001 getByReferenciaAndSetor(String codigo, String setor) throws SQLException {
        SdRoteiroOrganize001 row = null;
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                "       (select max(dt_alteracao)\n" +
                "          from sd_operacao_organize_001 oper\n" +
                "          join sd_oper_roteiro_organize_001 rot2\n" +
                "            on rot2.operacao = oper.codorg\n" +
                "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                "  from sd_roteiro_organize_001 rot\n" +
                " where referencia = ? and substr(desc_setor,1,3) = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, codigo);
        super.preparedStatement.setString(2, setor);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
    
    public ObservableList<SdRoteiroOrganize001> getColecaoAtual() throws SQLException {
        ObservableList<SdRoteiroOrganize001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                "       (select max(dt_alteracao)\n" +
                "          from sd_operacao_organize_001 oper\n" +
                "          join sd_oper_roteiro_organize_001 rot2\n" +
                "            on rot2.operacao = oper.codorg\n" +
                "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                "  from sd_roteiro_organize_001 rot\n" +
                " where rot.referencia in\n" +
                "       (select codigo\n" +
                "          from sd_produto_colecao_001\n" +
                "         where colecao =\n" +
                "               (select codigo\n" +
                "                  from colecao_001\n" +
                "                 where sysdate between inicio_vig and fim_vig))");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16)));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdRoteiroOrganize001> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdRoteiroOrganize001> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                "       (select max(dt_alteracao)\n" +
                "          from sd_operacao_organize_001 oper\n" +
                "          join sd_oper_roteiro_organize_001 rot2\n" +
                "            on rot2.operacao = oper.codorg\n" +
                "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                "  from sd_roteiro_organize_001 rot\n" +
                " where upper(descricao) like ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16)));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<SdRoteiroOrganize001> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdRoteiroOrganize001> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                        "       (select max(dt_alteracao)\n" +
                        "          from sd_operacao_organize_001 oper\n" +
                        "          join sd_oper_roteiro_organize_001 rot2\n" +
                        "            on rot2.operacao = oper.codorg\n" +
                        "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                        "  from sd_roteiro_organize_001 rot\n" +
                        " where " + whereFilter);
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16)));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public SdRoteiroOrganize001 getByCode(Integer code) throws SQLException {
        SdRoteiroOrganize001 row = null;
        
        StringBuilder query = new StringBuilder("" +
                "select rot.*,\n" +
                "       (select max(dt_alteracao)\n" +
                "          from sd_operacao_organize_001 oper\n" +
                "          join sd_oper_roteiro_organize_001 rot2\n" +
                "            on rot2.operacao = oper.codorg\n" +
                "         where rot2.roteiro = rot.codorg) dt_alt_oper\n" +
                "  from sd_roteiro_organize_001 rot\n" +
                " where codorg = ?");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdRoteiroOrganize001(
                    resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getInt(4),
                    resultSetSql.getString(5),
                    resultSetSql.getDouble(6),
                    resultSetSql.getInt(7),
                    resultSetSql.getString(8),
                    resultSetSql.getDouble(9),
                    resultSetSql.getDouble(10),
                    resultSetSql.getDouble(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getString(16));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return row;
    }
}
