/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdSetorOp001;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class SdSetorOp001DAO extends FactoryConnection {

    public SdSetorOp001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdSetorOp001> getAll() throws SQLException {
        ObservableList<SdSetorOp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select *\n"
                + "  from sd_setor_op_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdSetorOp001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdSetorOp001 objectToSave) throws SQLException {
//        String[] returnId = {"CODIGO"};
//        StringBuilder query = new StringBuilder(""
//                + " insert into sd_turno_001\n"
//                + "   (descricao, hr_inicio, hr_fim, intervalo_inicio, intervalo_fim, ativo)\n"
//                + " values\n"
//                + "   (?, to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), to_date(?, 'HH24:MI'), ?)");
//
//        super.initPreparedStatement(query.toString(), returnId);
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
//        }
//        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
//        if (resultSetSql.next()) {
//            objectToSave.setCodigo(resultSetSql.getInt(1));
//        }
//        super.closeConnection();
    }

    public void update(SdSetorOp001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_turno_001\n"
//                + "   set descricao        = ?,\n"
//                + "       hr_inicio        = to_date(?, 'HH24:MI'),\n"
//                + "       hr_fim           = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_inicio = to_date(?, 'HH24:MI'),\n"
//                + "       intervalo_fim    = to_date(?, 'HH24:MI'),\n"
//                + "       ativo            = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.getInicioTurno());
//        super.preparedStatement.setString(3, objectToSave.getFimTurno());
//        super.preparedStatement.setString(4, objectToSave.getInicioIntervalo());
//        super.preparedStatement.setString(5, objectToSave.getFimIntervalo());
//        super.preparedStatement.setString(6, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(7, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public void delete(SdSetorOp001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder("delete from sd_turno_001 where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setInt(1, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public ObservableList<SdSetorOp001> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdSetorOp001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("*\n"
                + "  from sd_setor_op_001\n"
                + " where descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdSetorOp001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    //--------------------------------------------------------------------------
    public ObservableList<SdSetorOp001> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdSetorOp001> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_setor_op_001\n"
                + " where " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdSetorOp001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public SdSetorOp001 getByCode(Integer code) throws SQLException {
        SdSetorOp001 row = null;

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_setor_op_001\n"
                + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdSetorOp001(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3));
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

}
