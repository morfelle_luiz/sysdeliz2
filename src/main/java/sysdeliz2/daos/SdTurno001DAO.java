/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.sysdeliz.SdTurno;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SdTurno001DAO extends FactoryConnection {

    public SdTurno001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    private String whereFilter;

    public ObservableList<SdTurno> getAll() throws SQLException {
        ObservableList<SdTurno> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdTurno objectToSave) throws SQLException {
        String[] returnId = {"CODIGO"};
        StringBuilder query = new StringBuilder(""
                + " insert into sd_turno_001\n"
                + "   (descricao, hr_inicio, hr_fim, ativo)\n"
                + " values\n"
                + "   (?, to_date(?, 'DD/MM/YYYY HH24:MI'), to_date(?, 'DD/MM/YYYY HH24:MI'), ?)");

        super.initPreparedStatement(query.toString(), returnId);
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setString(2, "01/01/1900 " + objectToSave.getInicioTurno());
        super.preparedStatement.setString(3, "01/01/1900 " + objectToSave.getFimTurno());
        super.preparedStatement.setString(4, objectToSave.isAtivo() ? "S" : "N");
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }
        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
        if (resultSetSql.next()) {
            objectToSave.setCodigo(resultSetSql.getInt(1));
        }
        super.closeConnection();
    }

    public void update(SdTurno objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(
                "  update sd_turno_001\n"
                        + "   set descricao        = ?,\n"
                        + "       hr_inicio        = to_date(?, 'DD/MM/YYYY HH24:MI'),\n"
                        + "       hr_fim           = to_date(?, 'DD/MM/YYYY HH24:MI'),\n"
                        + "       ativo            = ?\n"
                        + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, objectToSave.getDescricao());
        super.preparedStatement.setString(2, "01/01/1900 " + objectToSave.getInicioTurno());
        super.preparedStatement.setString(3, "01/01/1900 " + objectToSave.getFimTurno());
        super.preparedStatement.setString(4, objectToSave.isAtivo() ? "S" : "N");
        super.preparedStatement.setInt(5, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public void delete(SdTurno objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder("delete from sd_turno_001 where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigo());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
        }

        super.closeConnection();
    }

    public ObservableList<SdTurno> getByDefault(String valueDefault) throws SQLException {
        ObservableList<SdTurno> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where descricao like ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, "%" + valueDefault + "%");
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    //--------------------------------------------------------------------------
    public ObservableList<SdTurno> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SdTurno> rows = FXCollections.observableArrayList();
        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where " + whereFilter);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime()));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<Map<String, Object>> getIntervalosInterjornadasTurno(List<String> turnos) throws SQLException {
        ObservableList<Map<String, Object>> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select inicio,\n"
                + "       fim\n"
                + "  from sd_paradas_turno_001\n"
                + " where turno in (" + turnos.stream().collect(Collectors.joining(",")) + ")");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, Object> intervalo = new HashMap<String, Object>();
            intervalo.put("inicio", resultSetSql.getTimestamp(1).toLocalDateTime());
            intervalo.put("fim", resultSetSql.getTimestamp(2).toLocalDateTime());
            intervalo.put("intervalo", Duration.between(resultSetSql.getTimestamp(1).toLocalDateTime(), resultSetSql.getTimestamp(2).toLocalDateTime()).getSeconds() / 60);
            rows.add(intervalo);
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<Map<String, Object>> getIntervalosTurno(String turno) throws SQLException {
        return getIntervalosTurno(Arrays.asList(turno));
    }

    public ObservableList<Map<String, Object>> getIntervalosTurno(List<String> turnos) throws SQLException {
        ObservableList<Map<String, Object>> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select 'inter' tipo,\n"
                + "       max(hr_fim) inicio_intervalo,\n"
                + "       min(hr_inicio)+1 fim_intervalo\n"
                + "  from sd_turno_001\n"
                + " where codigo in (" + turnos.stream().collect(Collectors.joining(",")) + ")\n"
                + "union\n"
                + "select 'intra' tipo,\n" +
                "       inicio,\n" +
                "       fim\n" +
                "  from sd_paradas_turno_001\n" +
                " where turno in (" + turnos.stream().collect(Collectors.joining(",")) + ")");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, Object> intervalo = new HashMap<String, Object>();
            intervalo.put("tipo", resultSetSql.getString(1));
            intervalo.put("inicio", resultSetSql.getTimestamp(2).toLocalDateTime());
            intervalo.put("fim", resultSetSql.getTimestamp(3).toLocalDateTime());
            intervalo.put("intervalo", Duration.between(resultSetSql.getTimestamp(2).toLocalDateTime(), resultSetSql.getTimestamp(3).toLocalDateTime()).getSeconds() / 60);
            rows.add(intervalo);
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<Map<String, Object>> getInicioTurno(List<String> turnos) throws SQLException {
        ObservableList<Map<String, Object>> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select min(hr_inicio) inicio_intervalo\n"
                + "  from sd_turno_001\n"
                + " where codigo in (" + turnos.stream().collect(Collectors.joining(",")) + ")");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, Object> intervalo = new HashMap<String, Object>();
            intervalo.put("inicio", resultSetSql.getTimestamp(1).toLocalDateTime());
            rows.add(intervalo);
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<Map<String, Object>> getFimTurno(List<String> turnos) throws SQLException {
        ObservableList<Map<String, Object>> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select min(hr_inicio) inicio_turno, max(hr_fim) fim_turno\n"
                + "  from sd_turno_001\n"
                + " where codigo in (" + turnos.stream().collect(Collectors.joining(",")) + ")");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Map<String, Object> intervalo = new HashMap<String, Object>();
            intervalo.put("inicio", resultSetSql.getTimestamp(1).toLocalDateTime());
            intervalo.put("fim", resultSetSql.getTimestamp(2).toLocalDateTime());
            rows.add(intervalo);
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public Map<String, Object> getProximoTurno(Integer codigo) throws SQLException {
        Map<String, Object> row = new HashMap<String, Object>();

        StringBuilder query = new StringBuilder(""
                + "select nvl(prox.codigo, atual.codigo) codigo,\n"
                + "       nvl(prox.hr_inicio, atual.hr_inicio) inicio,\n"
                + "       nvl(prox.hr_fim, atual.hr_fim) fim\n"
                + "  from sd_turno_001 atual\n"
                + "  left join sd_turno_001 prox\n"
                + "    on prox.hr_inicio = atual.hr_fim\n"
                + " where atual.codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, codigo);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            row.put("codigo", resultSetSql.getInt(1));
            row.put("inicio", resultSetSql.getTimestamp(2).toLocalDateTime());
            row.put("fim", resultSetSql.getTimestamp(3).toLocalDateTime());
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public SdTurno getByCode(Integer code) throws SQLException {
        SdTurno row = null;

        StringBuilder query = new StringBuilder(""
                + "select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where codigo = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, code);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime());
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public SdTurno getByTimeAndCode(String horaProgramacao, List<String> turnos) throws SQLException {
        SdTurno row = null;

        StringBuilder query = new StringBuilder(""
                + "select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where codigo in (" + turnos.stream().collect(Collectors.joining(",")) + ")\n"
                + "   and to_date('01/01/1900 " + horaProgramacao + "','DD/MM/YYYY HH24:MI:SS') between hr_inicio and hr_fim");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime());
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public SdTurno getByDateTime(LocalDateTime dateTime) throws SQLException {
        SdTurno row = null;

        StringBuilder query = new StringBuilder(""
                + "select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where ? between\n"
                + "         to_char(hr_inicio, 'HH24:MI') and\n"
                + "         to_char(hr_fim, 'HH24:MI')\n"
                + "   and ativo = 'S'\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, dateTime.format(DateTimeFormatter.ofPattern("HH:mm")));
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime());
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public SdTurno getByDateTime(LocalDateTime dateTime, List<String> turnos) throws SQLException {
        SdTurno row = null;

        StringBuilder query = new StringBuilder(""
                + "select codigo,\n"
                + "       descricao,\n"
                + "       to_char(hr_inicio, 'HH24:MI') hr_inicio,\n"
                + "       to_char(hr_fim, 'HH24:MI') hr_fim,\n"
                + "       ativo,\n"
                + "       hr_inicio,\n"
                + "       hr_fim\n"
                + "  from sd_turno_001\n"
                + " where ? between\n"
                + "         to_char(hr_inicio, 'DD/MM/YYYY HH24:MI') and\n"
                + "         to_char(hr_fim, 'DD/MM/YYYY HH24:MI')\n"
                + "   and ativo = 'S'\n"
                + "   and codigo in (" + turnos.stream().collect(Collectors.joining(",")) + ")");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, dateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            row = new SdTurno(resultSetSql.getInt(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getTimestamp(6).toLocalDateTime(),
                    resultSetSql.getTimestamp(7).toLocalDateTime());
        }
        resultSetSql.close();
        super.closeConnection();

        return row;
    }

    public ObservableList<String> getDateNotUtil(LocalDateTime dateTime) throws SQLException {
        ObservableList<String> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select * from ano_001 where dia_util = 'N' and data = to_date(?,'DD/MM/YYYY')");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, dateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(resultSetSql.getString("data"));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }
}
