/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdTurnoCelula001;
import sysdeliz2.models.sysdeliz.SdTurno;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author cristiano.diego
 */
public class SdTurnoCelula001DAO extends FactoryConnection {

    public SdTurnoCelula001DAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<SdTurnoCelula001> getAll() throws SQLException {
        ObservableList<SdTurnoCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_turno_celula_001 tcel, sd_turno_001 tur\n"
                + " where tcel.turno = tur.codigo");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurnoCelula001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    new SdTurno(
                            resultSetSql.getInt(3),
                            resultSetSql.getString(4),
                            resultSetSql.getString(5),
                            resultSetSql.getString(6),
                            resultSetSql.getString(7),
                            resultSetSql.getTimestamp(5).toLocalDateTime(),
                            resultSetSql.getTimestamp(6).toLocalDateTime())
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdTurnoCelula001> getByCelula(Integer celula) throws SQLException {
        ObservableList<SdTurnoCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sd_turno_celula_001 tcel, sd_turno_001 tur\n"
                + " where tcel.turno = tur.codigo\n"
                + "   and tcel.celula = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, celula);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurnoCelula001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    new SdTurno(
                            resultSetSql.getInt(3),
                            resultSetSql.getString(4),
                            resultSetSql.getString(5),
                            resultSetSql.getString(6),
                            resultSetSql.getString(7),
                            resultSetSql.getTimestamp(5).toLocalDateTime(),
                            resultSetSql.getTimestamp(6).toLocalDateTime())
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<SdTurnoCelula001> getByAvailable(List<String> turnos, Integer codigo) throws SQLException {
        ObservableList<SdTurnoCelula001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select nvl(tcel.celula,0) celula, nvl(tcel.turno,tur.codigo) turno, tur.*\n" +
                "  from sd_turno_001 tur\n" +
                "  left join sd_turno_celula_001 tcel\n" +
                "    on tcel.turno = tur.codigo\n" +
                "   and tcel.celula = " + codigo + "\n" +
                " where tur.ativo = 'S'\n" +
                "   and (tcel.turno is null or tcel.turno not in (" + (turnos.size() > 0 ? turnos.stream().collect(Collectors.joining(",")) : "''") + "))");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new SdTurnoCelula001(
                    resultSetSql.getInt(1),
                    resultSetSql.getInt(2),
                    new SdTurno(
                            resultSetSql.getInt(3),
                            resultSetSql.getString(4),
                            resultSetSql.getString(5),
                            resultSetSql.getString(6),
                            resultSetSql.getString(7),
                            resultSetSql.getTimestamp(5).toLocalDateTime(),
                            resultSetSql.getTimestamp(6).toLocalDateTime())
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(SdTurnoCelula001 objectToSave) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + " insert into sd_turno_celula_001\n"
                + " values (?, ?)");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToSave.getCodigoCelula());
        super.preparedStatement.setInt(2, objectToSave.getCodigoTurno());
        int affectedRows = super.preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
        }

    }

    public void delete(SdTurnoCelula001 objectToDele) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_turno_celula_001 where celula = ? and turno = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, objectToDele.getCodigoCelula());
        super.preparedStatement.setInt(2, objectToDele.getCodigoTurno());
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }

    public void deleteCelula(Integer celula) throws SQLException {
        StringBuilder query = new StringBuilder(""
                + "delete from sd_turno_celula_001 where celula = ?");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setInt(1, celula);
        int affectedRows = super.preparedStatement.executeUpdate();

        super.closeConnection();
    }
}
