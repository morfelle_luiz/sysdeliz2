/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.TabPrz001;
import sysdeliz2.utils.sys.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class TabPrz001DAO extends FactoryConnection {

    public TabPrz001DAO() throws SQLException {
        this.initConnection();
    }

    private String whereFilter = "";

    public ObservableList<TabPrz001> getAll() throws SQLException {
        ObservableList<TabPrz001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from tabprz_001");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new TabPrz001(resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getInt(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public void save(TabPrz001 objectToSave) throws SQLException {
//        String[] returnId = {"CODIGO"};
//        StringBuilder query = new StringBuilder(""
//                + " insert into sd_grupo_operacao_001\n"
//                + "   (descricao, ativo)\n"
//                + " values\n"
//                + "   (?, ?)");
//
//        super.initPreparedStatement(query.toString(), returnId);
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.isAtivo() ? "S" : "N");
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível cadastrar o registro no banco de dados!");
//        }
//        ResultSet resultSetSql = super.preparedStatement.getGeneratedKeys();
//        if (resultSetSql.next()) {
//            objectToSave.setCodigo(resultSetSql.getInt(1));
//        }
//        super.closeConnection();
//
//        objectToSave.getOperacoes().forEach(operacao -> {
//            operacao.setGrupo(objectToSave.getCodigo());
//            try {
//                DAOFactory.getSdOperacoesGrp001DAO().save(operacao);
//            } catch (SQLException ex) {
//                LoggerExtensions.getLogger(SdGrupoOperacao001DAO1.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        });

    }

    public void update(TabPrz001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(
//                "  update sd_grupo_operacao_001\n"
//                + "   set descricao        = ?,\n"
//                + "       ativo            = ?\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, objectToSave.getDescricao());
//        super.preparedStatement.setString(2, objectToSave.isAtivo() ? "S" : "N");
//        super.preparedStatement.setInt(3, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível alterar o registro no banco de dados!");
//        }
//
//        super.closeConnection();
//
//        DAOFactory.getSdOperacoesGrp001DAO().deleteGroup(objectToSave.getCodigo());
//        objectToSave.getOperacoes().forEach(operacao -> {
//            operacao.setGrupo(objectToSave.getCodigo());
//            try {
//                DAOFactory.getSdOperacoesGrp001DAO().save(operacao);
//            } catch (SQLException ex) {
//                LoggerExtensions.getLogger(SdGrupoOperacao001DAO1.class.getName()).log(Level.SEVERE, null, ex);
//                GUIUtils.showException(ex);
//            }
//        });

    }

    public void delete(TabPrz001 objectToSave) throws SQLException {
//        StringBuilder query = new StringBuilder(""
//                + "begin\n"
//                + " delete from sd_operacoes_grp_001 where grupo = ?;\n"
//                + " delete from sd_grupo_operacao_001 where codigo = ?;\n"
//                + "end;");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setInt(1, objectToSave.getCodigo());
//        super.preparedStatement.setInt(2, objectToSave.getCodigo());
//        int affectedRows = super.preparedStatement.executeUpdate();
//        if (affectedRows == 0) {
//            throw new SQLException("Não foi possível excluir o registro no banco de dados!");
//        }
//
//        super.closeConnection();
    }

    public ObservableList<TabPrz001> getByDefault(String valueDefault) throws SQLException {
//        ObservableList<SdGrupoOperacao001> rows = FXCollections.observableArrayList();
//
//        StringBuilder query = new StringBuilder(""
//                + "select *\n"
//                + "  from sd_turno_001\n"
//                + " where descricao like ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setString(1, "%" + valueDefault + "%");
//        super.preparedStatement.execute();
//        ResultSet resultSetSql = super.preparedStatement.getResultSet();
//        while (resultSetSql.next()) {
//            rows.add(new SdGrupoOperacao001(resultSetSql.getInt(1),
//                    resultSetSql.getString(2),
//                    resultSetSql.getString(3)));
//        }
//        resultSetSql.close();
//        super.closeConnection();
//
////        rows.forEach(row -> {
////            try {
////                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
////            } catch (SQLException ex) {
////                GUIUtils.showException(ex);
////            }
////        });
//        return rows;
        return null;
    }

    //--------------------------------------------------------------------------
    public ObservableList<TabPrz001> getFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
//        ObservableList<SdGrupoOperacao001> rows = FXCollections.observableArrayList();
//        whereFilter = "";
//        wheresClauses.forEach(filters -> {
//            whereFilter += filters.get(0)
//                    + " "
//                    + filters.get(1)
//                    + " "
//                    + filters.get(2)
//                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
//                    + filters.get(3).toUpperCase()
//                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
//                    + "\n";
//        });
//
//        StringBuilder query = new StringBuilder(""
//                + "select select *\n"
//                + "  from sd_grupo_operacao_001\n"
//                + " where " + whereFilter);
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.execute();
//        ResultSet resultSetSql = super.preparedStatement.getResultSet();
//        while (resultSetSql.next()) {
//            rows.add(new SdGrupoOperacao001(resultSetSql.getInt(1),
//                    resultSetSql.getString(2),
//                    resultSetSql.getString(3)));
//        }
//        resultSetSql.close();
//        super.closeConnection();
//
//        return rows;
        return null;
    }

    public TabPrz001 getByCode(Integer code) throws SQLException {
//        SdGrupoOperacao001 row = null;
//
//        StringBuilder query = new StringBuilder(""
//                + "select *\n"
//                + "  from sd_grupo_operacao_001\n"
//                + " where codigo = ?");
//
//        super.initPreparedStatement(query.toString());
//        super.preparedStatement.setInt(1, code);
//        super.preparedStatement.execute();
//        ResultSet resultSetSql = super.preparedStatement.getResultSet();
//        if (resultSetSql.next()) {
//            row = new SdGrupoOperacao001(resultSetSql.getInt(1),
//                    resultSetSql.getString(2),
//                    resultSetSql.getString(3));
//        }
//        resultSetSql.close();
//        super.closeConnection();
//
////        rows.forEach(row -> {
////            try {
////                row.setOperacoes(DAOFactory.getSdOperacoesGrp001DAO().getByGroup(row.getCodigo()));
////            } catch (SQLException ex) {
////                GUIUtils.showException(ex);
////            }
////        });
//        return row;
        return null;
    }

    public ObservableList<TabPrz001> getByTipoPrazo(String prazo) throws SQLException {
        ObservableList<TabPrz001> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from tabprz_001\n"
                + " where prazo = ?\n"
                + "   and tipo = 'P'");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, prazo);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new TabPrz001(resultSetSql.getString(1),
                    StringUtils.toDateFormat(resultSetSql.getDate(2).toLocalDate()),
                    StringUtils.toDateFormat(resultSetSql.getDate(3).toLocalDate()),
                    resultSetSql.getString(4),
                    resultSetSql.getInt(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7)));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
}
