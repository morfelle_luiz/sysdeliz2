/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import sysdeliz2.models.ConnectionFactory;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class UsuariosOracleDAO implements UsuariosDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public String[] getDadosUsuario(String nomeFuncionario) throws SQLException {
        String[] dadosUsuario = {"", "", ""};

        StringBuilder query = new StringBuilder(""
                + "SELECT PES.CODIGO CODFUN, USE.NOME USU, USE.CODIGO CODUSU FROM PESSOAL_001 PES \n"
                + "LEFT JOIN ACESSO_USUARIOS USE ON (USE.CODFUN = PES.CODIGO)\n"
                + "WHERE UPPER(PES.NOME) = ?");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, "UPPER('" + nomeFuncionario + "')");
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            dadosUsuario[0] = resultSetSql.getString("CODFUN");
            dadosUsuario[1] = resultSetSql.getString("USU");
            dadosUsuario[2] = resultSetSql.getString("CODUSU");
        }
        resultSetSql.close();
        this.closeConnection();

        return dadosUsuario;
    }

}
