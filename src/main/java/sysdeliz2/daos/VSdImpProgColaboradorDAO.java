package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.view.VSdImpProgColaborador;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VSdImpProgColaboradorDAO extends FactoryConnection {
    
    public VSdImpProgColaboradorDAO() throws SQLException {
        this.initConnection();
    }
    
    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
    
    public ObservableList<VSdImpProgColaborador> getOfsParaProgramar(String wheres) throws SQLException {
        ObservableList<VSdImpProgColaborador> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select col.codigo,\n" +
                "       col.nome,\n" +
                "       count(distinct prog.ordem) qtde_oper,\n" +
                "       count(distinct imp.ordem) qtde_imp,\n" +
                "       case\n" +
                "         when count(distinct prog.ordem) = 0 then\n" +
                "          'SEM PROGRAMACAO'\n" +
                "         else\n" +
                "          case\n" +
                "            when count(distinct imp.ordem) = 0 then\n" +
                "             'PENDENTE'\n" +
                "            else\n" +
                "             case\n" +
                "               when count(distinct imp.ordem) > 0 and\n" +
                "                    count(distinct imp.ordem) < count(distinct prog.ordem) then\n" +
                "                'PARCIAL'\n" +
                "               else\n" +
                "                'IMPRESSO'\n" +
                "             end\n" +
                "          end\n" +
                "       end status\n" +
                "  from sd_colaborador_001 col\n" +
                "  left join sd_programacao_pacote_001 prog\n" +
                "    on col.codigo = prog.colaborador\n" +
                "  left join sd_pacote_001 pct\n" +
                "    on pct.codigo = prog.pacote\n" +
                "  left join sd_programacao_of_001 pof\n" +
                "    on pof.ordem_prod = pct.numero\n" +
                "  left join sd_hist_imp_prog_colab_001 imp\n" +
                "    on imp.colaborador = prog.colaborador\n" +
                "   and imp.pacote = prog.pacote\n" +
                "   and imp.operacao = prog.operacao\n" +
                "   and imp.ordem = prog.ordem\n" +
                "   and imp.quebra = prog.quebra\n" +
                " where 1 = 1\n" +
                "   " + wheres +
                " group by col.codigo, col.nome");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdImpProgColaborador(
                        resultSetSql.getInt(1),
                        resultSetSql.getString(2),
                        resultSetSql.getInt(3),
                        resultSetSql.getInt(4),
                        resultSetSql.getString(5)
                    ));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
}
