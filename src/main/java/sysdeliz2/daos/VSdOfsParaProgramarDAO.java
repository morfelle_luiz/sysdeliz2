/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.view.VSdOfsParaProgramar;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class VSdOfsParaProgramarDAO extends FactoryConnection {

    public VSdOfsParaProgramarDAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<VSdOfsParaProgramar> getOfsParaProgramar(String faccao) throws SQLException {
        ObservableList<VSdOfsParaProgramar> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from v_sd_ofs_para_programacao ofs\n"
                + " where ofs.setor in ('109')\n"
                + "   and ofs.status_of in ('R','Y','G','D')\n"
                + "   and ofs.codcli in (select codcli\n"
                + "                        from entidade_001\n"
                + "                       where nome like '%" + faccao + "%'\n"
                + "                         and tipo_entidade like '%T%')");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdOfsParaProgramar(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getString(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getInt(16),
                    resultSetSql.getString(17),
                    resultSetSql.getString(18),
                    resultSetSql.getString(19),
                    resultSetSql.getString(20),
                    resultSetSql.getInt(21),
                    resultSetSql.getDouble(22),
                    resultSetSql.getDouble(23),
                    resultSetSql.getDouble(24)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

    public ObservableList<VSdOfsParaProgramar> getOfsParaProgramarFromFilter(String faccao, String wheres) throws SQLException {
        ObservableList<VSdOfsParaProgramar> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from v_sd_ofs_para_programacao ofs\n"
                + " where ofs.codcli in (select codcli\n"
                + "                        from entidade_001\n"
                + "                       where nome like '%" + faccao + "%'\n"
                + "                         and tipo_entidade like '%T%')"
                + "   " + wheres);

        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdOfsParaProgramar(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getString(4),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getString(10),
                    resultSetSql.getString(11),
                    resultSetSql.getString(12),
                    resultSetSql.getString(13),
                    resultSetSql.getString(14),
                    resultSetSql.getString(15),
                    resultSetSql.getInt(16),
                    resultSetSql.getString(17),
                    resultSetSql.getString(18),
                    resultSetSql.getString(19),
                    resultSetSql.getString(20),
                    resultSetSql.getInt(21),
                    resultSetSql.getDouble(22),
                    resultSetSql.getDouble(23),
                    resultSetSql.getDouble(24)
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

}
