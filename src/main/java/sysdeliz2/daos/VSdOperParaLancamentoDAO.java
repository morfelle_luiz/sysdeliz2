package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.SdPacote001;
import sysdeliz2.models.view.VSdOperParaLancamento;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VSdOperParaLancamentoDAO extends FactoryConnection {

    public VSdOperParaLancamentoDAO() throws SQLException {
        this.initConnection();
    }

    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }

    public ObservableList<VSdOperParaLancamento> getPacotesLancamento(Integer operador, String of) throws SQLException {
        ObservableList<VSdOperParaLancamento> rows = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder("" +
                "select *\n" +
                "  from v_sd_oper_para_lancamento\n" +
                " where ordem_prod = ?\n" +
                "   and operador = ?\n" +
                "   and to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') between\n" +
                "       to_date(to_char(inicio_op, 'DD/MM/YYYY'), 'DD/MM/YYYY') and\n" +
                "       to_date(to_char(fim_op, 'DD/MM/YYYY'), 'DD/MM/YYYY')\n");

        super.initPreparedStatement(query.toString());
        super.preparedStatement.setString(1, of);
        super.preparedStatement.setInt(2, operador);
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdOperParaLancamento(
                    resultSetSql.getString(1),
                    new SdPacote001(resultSetSql.getString(2),
                            resultSetSql.getInt(3),
                            null,
                            resultSetSql.getString(4),
                            resultSetSql.getString(5),
                            resultSetSql.getString(6),
                            resultSetSql.getInt(7),
                            resultSetSql.getString(8),
                            null),
                    new SdProgramacaoPacote001(resultSetSql.getString(9),
                            resultSetSql.getInt(10),
                            resultSetSql.getInt(11),
                            resultSetSql.getTimestamp(12).toLocalDateTime(),
                            resultSetSql.getTimestamp(13).toLocalDateTime(),
                            resultSetSql.getTimestamp(14).toLocalDateTime(),
                            resultSetSql.getInt(15),
                            resultSetSql.getInt(16),
                            resultSetSql.getInt(17),
                            resultSetSql.getInt(18),
                            resultSetSql.getInt(19),
                            resultSetSql.getString(20),
                            resultSetSql.getInt(21),
                            resultSetSql.getDouble(22),
                            resultSetSql.getInt(23),
                            resultSetSql.getInt(24),
                            resultSetSql.getDouble(25))
            ));
        }
        resultSetSql.close();
        super.closeConnection();

        return rows;
    }

}
