package sysdeliz2.daos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.view.VSdProdDiariaCelula;
import sysdeliz2.models.view.VSdProdDiariaColaborador;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VSdProdDiariaCelulaDAO extends FactoryConnection {
    
    GenericDao<SdCelula> daoCelula = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCelula.class);
    
    public VSdProdDiariaCelulaDAO() throws SQLException {
        this.initConnection();
    }
    
    @Override
    protected void initConnection() throws SQLException {
        super.connection = super.getEmpresaConnection();
    }
    
    public ObservableList<VSdProdDiariaCelula> getProducaoCelula(String dtInicio, String dtFim, String diaUtil, String celula, String ordProd, String referencia) throws SQLException {
        ObservableList<VSdProdDiariaCelula> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select distinct to_char(prod.data, 'DD') DIA,\n" +
                "       to_char(prod.data, 'MONTH') MES,\n" +
                "       to_char(prod.data, 'YYYY') ANO,\n" +
                "       prod.data,\n" +
                "       prod.dia_util,\n" +
                "       cel.codigo cod_celula,\n" +
                "       cel.descricao celula,\n" +
                "       lid.codigo cod_lider,\n" +
                "       lid.nome lider,\n" +
                "       -- celula\n" +
                "       sum(sum(prod.qtde_opers)) over (partition by cel.codigo, prod.data) qtde_opers_celula,\n" +
                "       trunc(avg(sum(f_verifica_divisor(prod.tot_pecas, prod.qtde_opers))) over (partition by cel.codigo, prod.data),0) med_pecas_celula,\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by cel.codigo, prod.data) tmp_realizado_celula,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by cel.codigo, prod.data) tmp_programado_celula,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by cel.codigo, prod.data),sum(sum(prod.tot_tempo_prod)) over (partition by cel.codigo, prod.data))*100,0) operacao_celula,--%oper\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by cel.codigo, prod.data) tmp_deduz_efic_celula,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by cel.codigo, prod.data),sum(nvl(prod.tmp_produtivo,0) - nvl(prod.tmp_deduz_efic,0)) over (partition by cel.codigo, prod.data))*100,0) eficiencia_celula, --efic\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by cel.codigo, prod.data) tmp_prod_extra_celula,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by cel.codigo, prod.data) tmp_disponivel_celula,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by cel.codigo, prod.data),sum(nvl(prod.tmp_produtivo,0) - nvl(prod.tmp_parado_deduz,0)) over (partition by cel.codigo, prod.data))*100,0) produtividade_celula, --prod\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by cel.codigo, prod.data) tmp_parado_celula,\n" +
                "       -- dia\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by prod.data) tmp_realizado_dia,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by prod.data) tmp_programado_dia,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by prod.data),sum(sum(prod.tot_tempo_prod)) over (partition by prod.data))*100,0) operacao_dia,--%oper\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by prod.data) tmp_deduz_efic_dia,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by prod.data),sum(nvl(prod.tmp_produtivo,0) - nvl(prod.tmp_deduz_efic,0)) over (partition by prod.data))*100,0) eficiencia_dia, --efic\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by prod.data) tmp_prod_extra_dia,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by prod.data) tmp_disponivel_dia,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_prod)) over (partition by prod.data),sum(nvl(prod.tmp_produtivo,0)- nvl(prod.tmp_parado_deduz,0)) over (partition by prod.data))*100,0) produtividade_dia, --prod\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by prod.data) tmp_parado_dia,\n" +
                "       -- mes\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_realizado_mes,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_programado_mes,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'MM/YYYY')),sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'MM/YYYY')))*100,0) operacao_mes,--%oper\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_deduz_efic_mes,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'MM/YYYY')),sum(nvl(prod.tmp_produtivo,0) - nvl(prod.tmp_deduz_efic,0)) over (partition by to_char(prod.data,'MM/YYYY')))*100,0) eficiencia_mes, --efic\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_prod_extra_mes,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_disponivel_mes,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'MM/YYYY')),sum(nvl(prod.tmp_produtivo,0)- nvl(prod.tmp_parado_deduz,0)) over (partition by to_char(prod.data,'MM/YYYY')))*100,0) produtividade_mes, --prod\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by to_char(prod.data,'MM/YYYY')) tmp_parado_mes,\n" +
                "       -- ano\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'YYYY')) tmp_realizado_ano,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'YYYY')) tmp_programado_ano,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'YYYY')),sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'YYYY')))*100,0) operacao_ano,--%oper\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by to_char(prod.data, 'YYYY')) tmp_deduz_efic_mes,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_oper)) over (partition by to_char(prod.data,'YYYY')),sum(nvl(prod.tmp_produtivo,0) - nvl(prod.tmp_deduz_efic,0)) over (partition by to_char(prod.data,'YYYY')))*100,0) eficiencia_ano, --efic\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by to_char(prod.data,'YYYY')) tmp_prod_extra_ano,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by to_char(prod.data,'YYYY')) tmp_disponivel_ano,\n" +
                "       trunc(f_verifica_divisor(sum(sum(prod.tot_tempo_prod)) over (partition by to_char(prod.data,'YYYY')),sum(nvl(prod.tmp_produtivo,0)- nvl(prod.tmp_parado_deduz,0)) over (partition by to_char(prod.data,'YYYY')))*100,0) produtividade_ano, --prod\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by to_char(prod.data,'YYYY')) tmp_parado_ano,\n" +
                "\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by 1) tmp_produtivo_periodo\n" +
                "  from sd_prod_dia_costura_001 prod\n" +
                "  join sd_celula_001 cel\n" +
                "    on cel.codigo = prod.celula\n" +
                "  join sd_colaborador_001 lid\n" +
                "    on lid.codigo = prod.lider\n" +
                "  join sd_colaborador_001 col\n" +
                "    on col.codigo = prod.colaborador\n" +
                " where prod.data between to_date( '"+dtInicio+"' ,'DD/MM/YYYY') and to_date( '"+dtFim+"' ,'DD/MM/YYYY')\n" +
                "   and (to_char(prod.ord_prod) in (select * from table(split2('" + ordProd + "'))) or '" + ordProd + "' is null)\n" +
                "   and (to_char(prod.referencia) in (select * from table(split2('" + referencia + "'))) or '" + referencia + "' is null)\n" +
                "   and (to_char(prod.dia_util) in (select * from table(split2('" + diaUtil + "'))) or '" + diaUtil + "' is null)\n" +
                "   and (to_char(prod.celula) in (select * from table(split2('" + celula + "'))) or '" + celula + "' is null)\n" +
                "   and prod.prod_cel = 'S'\n" +
                " group by prod.data,\n" +
                "          prod.dia_util,\n" +
                "          cel.codigo,\n" +
                "          cel.descricao,\n" +
                "          lid.codigo,\n" +
                "          lid.nome,\n" +
                "          col.codigo,\n" +
                "          col.nome,\n" +
                "          prod.tmp_produtivo,\n" +
                "          prod.tmp_parado_deduz,\n" +
                "          prod.tmp_parado_n_deduz,\n" +
                "          prod.tmp_deduz_efic\n" +
                " order by prod.data, cel.codigo");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdProdDiariaCelula(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getTimestamp(4).toLocalDateTime().toLocalDate(),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getInt(10),
                    resultSetSql.getInt(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getDouble(14),
                    resultSetSql.getDouble(15),
                    resultSetSql.getDouble(16),
                    resultSetSql.getDouble(17),
                    resultSetSql.getDouble(18),
                    resultSetSql.getDouble(19),
                    resultSetSql.getDouble(20),
                    resultSetSql.getDouble(21),
                    resultSetSql.getDouble(22),
                    resultSetSql.getDouble(23),
                    resultSetSql.getDouble(24),
                    resultSetSql.getDouble(25),
                    resultSetSql.getDouble(26),
                    resultSetSql.getDouble(27),
                    resultSetSql.getDouble(28),
                    resultSetSql.getDouble(29),
                    resultSetSql.getDouble(30),
                    resultSetSql.getDouble(31),
                    resultSetSql.getDouble(32),
                    resultSetSql.getDouble(33),
                    resultSetSql.getDouble(34),
                    resultSetSql.getDouble(35),
                    resultSetSql.getDouble(36),
                    resultSetSql.getDouble(37),
                    resultSetSql.getDouble(38),
                    resultSetSql.getDouble(39),
                    resultSetSql.getDouble(40),
                    resultSetSql.getDouble(41),
                    resultSetSql.getDouble(42),
                    resultSetSql.getDouble(43),
                    resultSetSql.getDouble(44),
                    resultSetSql.getDouble(45),
                    resultSetSql.getDouble(46),
                    resultSetSql.getDouble(47)
            ));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
    
    public ObservableList<VSdProdDiariaColaborador> getProducaoColaborador(String dtInicio, String dtFim, String diaUtil, String celula, String ordProd, String referencia, String colaborador) throws SQLException {
        ObservableList<VSdProdDiariaColaborador> rows = FXCollections.observableArrayList();
        
        StringBuilder query = new StringBuilder("" +
                "select distinct to_char(prod.data, 'DD') DIA,\n" +
                "       to_char(prod.data, 'MONTH') MES,\n" +
                "       to_char(prod.data, 'YYYY') ANO,\n" +
                "       prod.data,\n" +
                "       prod.dia_util,\n" +
                "       cel.codigo cod_celula,\n" +
                "       cel.descricao celula,\n" +
                "       col.codigo cod_colaborador,\n" +
                "       col.nome colaborador,\n" +
                "       -- colaborador\n" +
                "       sum(prod.qtde_opers) qtde_opers,\n" +
                "       trunc(sum(f_verifica_divisor(prod.tot_pecas, prod.qtde_opers)),0) med_pecas,\n" +
                "       sum(prod.tot_tempo_prod) tmp_realizado,\n" +
                "       sum(prod.tot_tempo_oper) tmp_programado,\n" +
                "       nvl(prod.tmp_produtivo,0) tmp_disponivel,\n" +
                "       nvl(prod.tmp_parado_deduz,0) tmp_extra_prod,\n" +
                "       nvl(prod.tmp_parado_n_deduz,0) tmp_parado,\n" +
                "       nvl(prod.tmp_deduz_efic,0) tmp_deduz_efic,\n" +
                "       -- mes\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_realizado_mes,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_programado_mes,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_disponivel_mes,\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_extra_prod_mes,\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_parado_mes,\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by col.codigo, to_char(prod.data,'MM/YYYY')) tmp_deduz_efic_mes,\n" +
                "       -- ano\n" +
                "       sum(sum(prod.tot_tempo_prod)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_realizado_ano,\n" +
                "       sum(sum(prod.tot_tempo_oper)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_programado_ano,\n" +
                "       sum(nvl(prod.tmp_produtivo,0)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_disponivel_ano,\n" +
                "       sum(nvl(prod.tmp_parado_deduz,0)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_extra_prod_ano,\n" +
                "       sum(nvl(prod.tmp_parado_n_deduz,0)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_parado_ano,\n" +
                "       sum(nvl(prod.tmp_deduz_efic,0)) over (partition by col.codigo, to_char(prod.data,'YYYY')) tmp_deduz_efic_ano,\n" +
                "\n" +
                "       avg(nvl(prod.tmp_produtivo,0)) over (partition by 1) tmp_disponivel_periodo\n" +
                "  from sd_prod_dia_costura_001 prod\n" +
                "  join sd_celula_001 cel\n" +
                "    on cel.codigo = prod.celula\n" +
                "  join sd_colaborador_001 lid\n" +
                "    on lid.codigo = prod.lider\n" +
                "  join sd_colaborador_001 col\n" +
                "    on col.codigo = prod.colaborador\n" +
                " where prod.data between to_date( '" + dtInicio + "' ,'DD/MM/YYYY') and to_date( '" + dtFim + "' ,'DD/MM/YYYY')\n" +
                "   and (to_char(prod.ord_prod) in (select * from table(split2('" + ordProd + "'))) or '" + ordProd + "' is null)\n" +
                "   and (to_char(prod.referencia) in (select * from table(split2('" + referencia + "'))) or '" + referencia + "' is null)\n" +
                "   and (to_char(prod.dia_util) in (select * from table(split2('" + diaUtil + "'))) or '" + diaUtil + "' is null)\n" +
                "   and (to_char(prod.colaborador) in (select * from table(split2('" + colaborador + "'))) or '" + colaborador + "' is null)\n" +
                "   and (to_char(prod.celula) in (select * from table(split2('" + celula + "'))) or '" + celula + "' is null)\n" +
                " group by prod.data,\n" +
                "          prod.dia_util,\n" +
                "          cel.codigo,\n" +
                "          cel.descricao,\n" +
                "          lid.codigo,\n" +
                "          lid.nome,\n" +
                "          col.codigo,\n" +
                "          col.nome,\n" +
                "          prod.tmp_produtivo,\n" +
                "          prod.tmp_parado_deduz,\n" +
                "          prod.tmp_parado_n_deduz,\n" +
                "          prod.tmp_deduz_efic\n" +
                " order by cel.codigo, col.nome, prod.data desc");
        
        super.initPreparedStatement(query.toString());
        super.preparedStatement.execute();
        ResultSet resultSetSql = super.preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            rows.add(new VSdProdDiariaColaborador(
                    resultSetSql.getString(1),
                    resultSetSql.getString(2),
                    resultSetSql.getString(3),
                    resultSetSql.getTimestamp(4).toLocalDateTime().toLocalDate(),
                    resultSetSql.getString(5),
                    resultSetSql.getString(6),
                    resultSetSql.getString(7),
                    resultSetSql.getString(8),
                    resultSetSql.getString(9),
                    resultSetSql.getInt(10),
                    resultSetSql.getInt(11),
                    resultSetSql.getDouble(12),
                    resultSetSql.getDouble(13),
                    resultSetSql.getDouble(14),
                    resultSetSql.getDouble(15),
                    resultSetSql.getDouble(16),
                    resultSetSql.getDouble(17),
                    resultSetSql.getDouble(18),
                    resultSetSql.getDouble(19),
                    resultSetSql.getDouble(20),
                    resultSetSql.getDouble(21),
                    resultSetSql.getDouble(22),
                    resultSetSql.getDouble(23),
                    resultSetSql.getDouble(24),
                    resultSetSql.getDouble(25),
                    resultSetSql.getDouble(26),
                    resultSetSql.getDouble(27),
                    resultSetSql.getDouble(28),
                    resultSetSql.getDouble(29)
            ));
        }
        resultSetSql.close();
        super.closeConnection();
        
        return rows;
    }
}
