/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.EstampaDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Estampa;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class EstampaOracleTA implements EstampaDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Estampa> getAll() throws SQLException {
        ObservableList<Estampa> estampas = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from estampa_001\n");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Estampa estampa = new Estampa();
            estampa.setCodigo(resultSetSql.getString("codigo"));
            estampa.setDescricao(resultSetSql.getString("descricao"));
            estampas.add(estampa);
        }
        resultSetSql.close();
        this.closeConnection();
        return estampas;
    }

    @Override
    public ObservableList<Estampa> getEstampasFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<Estampa> estampas = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from estampa_001\n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            Estampa estampa = new Estampa();
            estampa.setCodigo(resultSetSql.getString("codigo"));
            estampa.setDescricao(resultSetSql.getString("descricao"));
            estampas.add(estampa);
        }
        resultSetSql.close();
        this.closeConnection();
        return estampas;
    }

}
