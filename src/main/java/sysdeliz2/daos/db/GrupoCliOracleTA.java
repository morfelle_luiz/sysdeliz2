/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.GrupoCliDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.GrupoCli;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class GrupoCliOracleTA implements GrupoCliDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<GrupoCli> getAll() throws SQLException {
        ObservableList<GrupoCli> grupos = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from grupo_cli_001\n");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            GrupoCli grupo = new GrupoCli();
            grupo.setCodigo(resultSetSql.getString("codigo"));
            grupo.setDescricao(resultSetSql.getString("descricao"));
            grupos.add(grupo);
        }
        resultSetSql.close();
        this.closeConnection();
        return grupos;
    }

    @Override
    public ObservableList<GrupoCli> getGruposCliFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<GrupoCli> grupos = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select * \n"
                + "  from grupo_cli_001\n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            GrupoCli grupo = new GrupoCli();
            grupo.setCodigo(resultSetSql.getString("codigo"));
            grupo.setDescricao(resultSetSql.getString("descricao"));
            grupos.add(grupo);
        }
        resultSetSql.close();
        this.closeConnection();
        return grupos;
    }

}
