/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import sysdeliz2.daos.PedidoDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.PedIten;
import sysdeliz2.models.Pedido;
import sysdeliz2.models.table.TableManutencaoPedidosPedido;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class PedidoOracleTA implements PedidoDAO {
    
    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    
    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (statement != null) {
            statement.close();
        }
        connection.commit();
        connection.close();
    }
    
    private ObservableMap<String, Integer> getTamanhosRef(String numeroOf, String codigoPrd, String corRef) throws SQLException {
        ObservableMap<String, Integer> mapTamanhos = FXCollections.observableHashMap();
        
        StringBuilder query = new StringBuilder(
                "select   vct.tam,\n"
                        + "       nvl(pei.qtde, 0) qtde\n"
                        + "  from produto_001 prd\n"
                        + "  join vproduto_cortam vct\n"
                        + "    on vct.codigo = prd.codigo\n"
                        + "  join faixa_iten_001 fai\n"
                        + "    on fai.faixa = prd.faixa\n"
                        + "   and fai.tamanho = vct.tam\n"
                        + "  left join ped_iten_001 pei\n"
                        + "    on pei.codigo = vct.codigo\n"
                        + "   and pei.cor = vct.cor\n"
                        + "   and pei.tam = vct.tam\n"
                        + "   and pei.numero = '" + numeroOf + "'\n"
                        + " where prd.codigo = '" + codigoPrd + "'\n"
                        + "   and vct.cor = '" + corRef + "'\n"
                        + " order by fai.posicao");
        
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getString("tam"), resultSetSql.getInt("qtde"));
        }

        preparedStatement.close();
        resultSetSql.close();
        
        return mapTamanhos;
    }
    
    private ObservableMap<Integer, String> getHeaderTamanhosRef(String codigoPrd, String corRef) throws SQLException {
        ObservableMap<Integer, String> mapTamanhos = FXCollections.observableHashMap();
        
        StringBuilder query = new StringBuilder(
                "select   vct.tam,\n"
                        + "       fai.posicao\n"
                        + "  from produto_001 prd\n"
                        + "  join vproduto_cortam vct\n"
                        + "    on vct.codigo = prd.codigo\n"
                        + "  join faixa_iten_001 fai\n"
                        + "    on fai.faixa = prd.faixa\n"
                        + "   and fai.tamanho = vct.tam\n"
                        + " where prd.codigo = '" + codigoPrd + "'\n"
                        + "   and vct.cor = '" + corRef + "'\n"
                        + " order by fai.posicao");
        
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            mapTamanhos.put(resultSetSql.getInt("posicao"), resultSetSql.getString("tam"));
        }

        preparedStatement.close();
        resultSetSql.close();
        
        return mapTamanhos;
    }
    
    private PedIten getItenPedido(String numero, String codigo) throws SQLException {
        PedIten pedIten = null;
        
        StringBuilder query = new StringBuilder(
                "select distinct ordem, preco, dt_entrega\n"
                        + "  from ped_iten_001\n"
                        + " where numero = '" + numero + "'\n"
                        + "   and codigo = '" + codigo + "'");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            pedIten = new PedIten();
            pedIten.setOrdem(resultSetSql.getInt("ordem"));
            pedIten.setPreco(resultSetSql.getDouble("preco"));
            pedIten.setDt_entrega(resultSetSql.getString("dt_entrega"));
        }
        
        resultSetSql.close();
        closeConnection();
        
        return pedIten;
    }
    
    private void aplicaComissaoPedido(String numero) throws SQLException {
        StringBuilder query = new StringBuilder("" +
                "begin\n" +
                "  pkg_sd_comercial.aplica_comissao_ped(?);\n" +
                "end;");
    
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.execute();
    
        this.closeConnection();
    }

    private Integer updateNovoRefPedido(String numero, String codigo, String cor, String novoCodigo) throws SQLException  {
        StringBuilder query = new StringBuilder(
                "update ped_iten_001\n"
                        + "   set codigo = ?\n"
                        + " where codigo = ?\n"
                        + "   and numero = ?\n"
                        + "   and cor = ?"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, novoCodigo);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, numero);
        preparedStatement.setString(4, cor);
        Integer affectedRows = preparedStatement.executeUpdate();
        closeConnection();

        aplicaComissaoPedido(numero);

        return affectedRows;
    }

    @Override
    public Pedido getPedido(String numero) throws SQLException {
        Pedido pedido = null;
        
        StringBuilder query = new StringBuilder(
                "select ped.numero, cli.nome, cli.cnpj_cob, cli.sit_cli, cli.end_cob, cli.num_cob, cli.bairro_cob,\n"
                        + "       cid.nome_cid, cid.cod_est, cli.telefone, cli.email, to_char(ped.obs) \"obs\", \n"
                        + "       sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0)) valor, round(sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0)*(nvl(ped.per_desc,0)/100)),2) desconto, \n"
                        + "       (sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0))-round(sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0)*(nvl(ped.per_desc,0)/100)),2)) - nvl(rec.valor,0) total,\n"
                        + "       cli.tipo, cli.codcli, cli.cep_cob, nvl(rec.valor,0) recebido, cli.grupo\n"
                        + "from pedido_001 ped \n"
                        + "inner join ped_iten_001 pedi on ped.numero = pedi.numero\n"
                        + "inner join entidade_001 cli on ped.codcli = cli.codcli\n"
                        + "inner join cadcep_001 cep on cli.cep_cob = cep.cep\n"
                        + "inner join cidade_001 cid on cep.cod_cid = cid.cod_cid\n"
                        + "left join (select pedido, sum(valor) valor from sd_receber_cartao_001 where status = 'APPROVED' group by pedido) rec on rec.pedido = ped.numero\n"
                        + "where ped.numero = '" + numero + "' \n"
                        + "having nvl(rec.valor,0) < (sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0))-round(sum(nvl(pedi.qtde + pedi.qtde_f,0)*nvl(pedi.preco,0)*(nvl(ped.per_desc,0)/100)),2))\n"
                        + "group by ped.numero, cli.nome, cli.cnpj_cob, cli.sit_cli, cli.end_cob, cli.num_cob, cli.bairro_cob,\n"
                        + "cid.nome_cid, cid.cod_est, cli.telefone, cli.email, to_char(ped.obs), cli.tipo, cli.codcli, cli.cep_cob, nvl(rec.valor,0), cli.grupo");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            pedido = new Pedido(resultSetSql.getString("numero"), resultSetSql.getString("nome"), resultSetSql.getString("cnpj_cob"), resultSetSql.getString("sit_cli"),
                    resultSetSql.getString("end_cob"), resultSetSql.getString("num_cob"), resultSetSql.getString("bairro_cob"), resultSetSql.getString("nome_cid"),
                    resultSetSql.getString("cod_est"), resultSetSql.getString("telefone"), resultSetSql.getString("email"), resultSetSql.getString("obs"),
                    resultSetSql.getDouble("valor"), resultSetSql.getDouble("desconto"), resultSetSql.getDouble("total"), resultSetSql.getString("tipo"),
                    resultSetSql.getString("codcli"), resultSetSql.getString("cep_cob"), resultSetSql.getDouble("recebido"), resultSetSql.getString("grupo"));
        }
        
        closeConnection();
        resultSetSql.close();
        
        return pedido;
    }
    
    @Override
    public List<TableManutencaoPedidosPedido> getPedidosManutencao(String codigo, String listCor, String additionalWhere) throws SQLException {
        List<TableManutencaoPedidosPedido> pedidosManutencao = new ArrayList<>();
        
        StringBuilder query = new StringBuilder(
                "select ped.numero,\n"
                        + "       prd.codigo,\n"
                        + "       pei.ordem,\n"
                        + "       prd.colecao,\n"
                        + "       ped.periodo, \n"
                        + "       ped.entrega,\n"
                        + "       cli.codcli,\n"
                        + "       cli.nome cliente,\n"
                        + "       nvl(cli.grupo,'-') grupo_cli,\n"
                        + "       decode(cli.sit_cli,'1','CN','2','FR','3','RP',cli.sit_cli) sit_cli,\n"
                        + "       rep.nome represent,\n"
                        + "       pei.cor,\n"
                        + "       sum(pei.qtde) tot_qtde,\n"
                        + "       sum(pei.qtde) * pei.preco vlr_brt,\n"
                        + "       pei.preco vlr_unit,\n"
                        + "       ped.per_desc descont,\n"
                        + "       decode(ped.financeiro,'0','BLOQUEADO','1','LIBERADO') financ,\n"
                        + "       decode(ped.bloqueio,'0','BLOQUEADO','1','LIBERADO') comerc,\n"
                        + "       decode(cli.nrloja,'NPA','NÃO','SIM') npa,\n"
                        + "       decode(trim(ped.pgto),'1','SIM','NÃO') pagto_ant,\n"
                        + "       (select sum(case when pei2.motivo = '08' then pei2.qtde_canc else 0 end)/(sum(pei2.qtde_f)+sum(pei2.qtde)+sum(pei2.qtde_canc)) from ped_iten_001 pei2 where pei2.numero = ped.numero) perc_canc\n"
                        + "  from pedido_001 ped\n"
                        + "  join ped_iten_001 pei\n"
                        + "    on pei.numero = ped.numero\n"
                        + "  join produto_001 prd\n"
                        + "    on prd.codigo = pei.codigo\n"
                        + "  join sd_produto_001 sdprd\n"
                        + "    on sdprd.codigo = prd.codigo\n"
                        + "  join entidade_001 cli\n"
                        + "    on cli.codcli = ped.codcli\n"
                        + "  join represen_001 rep\n"
                        + "    on rep.codrep = ped.codrep\n"
                        + "  left join ped_reserva_001 res\n"
                        + "    on res.numero = ped.numero\n"
                        + "   and res.codigo = pei.codigo\n"
                        + "   and res.cor = pei.cor\n"
                        + "   and res.tam = pei.tam\n"
                        + " where pei.codigo = '" + codigo + "' and pei.cor in (" + listCor + ") and ped.periodo not in 'MOST' \n"
                        + additionalWhere
                        + "having sum(pei.qtde) > sum(pei.qtde_f) and sum(nvl(res.qtde,0)) = 0 \n"
                        + " group by ped.numero,\n"
                        + "          prd.codigo,\n"
                        + "          pei.ordem,\n"
                        + "          prd.colecao,\n"
                        + "          ped.periodo, \n"
                        + "          ped.entrega,\n"
                        + "          cli.codcli,\n"
                        + "          cli.nome,\n"
                        + "          cli.grupo,\n"
                        + "          cli.sit_cli,\n"
                        + "          rep.nome,\n"
                        + "          pei.cor,\n"
                        + "          ped.per_desc,\n"
                        + "          pei.preco,\n"
                        + "          ped.financeiro,\n"
                        + "          ped.bloqueio,\n"
                        + "          cli.nrloja,\n"
                        + "          ped.pgto\n"
                        + " order by ped.numero desc");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            ObservableMap<String, Integer> mapTamanhos = this.getTamanhosRef(resultSetSql.getString("numero"), resultSetSql.getString("codigo"), resultSetSql.getString("cor"));
            ObservableMap<Integer, String> mapHeadTamanhos = this.getHeaderTamanhosRef(resultSetSql.getString("codigo"), resultSetSql.getString("cor"));
            pedidosManutencao.add(new TableManutencaoPedidosPedido(resultSetSql.getString("numero"), resultSetSql.getString("codigo"), resultSetSql.getString("ordem"), resultSetSql.getString("colecao"),
                    resultSetSql.getString("periodo"), resultSetSql.getString("entrega"), resultSetSql.getString("codcli"), resultSetSql.getString("cliente"),
                    resultSetSql.getString("grupo_cli"), resultSetSql.getString("represent"), resultSetSql.getString("cor"), resultSetSql.getInt("tot_qtde"),
                    new BigDecimal(resultSetSql.getDouble("vlr_brt")), new BigDecimal(resultSetSql.getDouble("vlr_unit")), new BigDecimal(resultSetSql.getDouble("descont")),
                    resultSetSql.getString("financ"), resultSetSql.getString("comerc"), resultSetSql.getString("npa"),
                    resultSetSql.getString("pagto_ant"), new BigDecimal(resultSetSql.getDouble("perc_canc")), mapTamanhos, mapHeadTamanhos));
        }
        
        closeConnection();
        resultSetSql.close();
        
        return pedidosManutencao;
    }
    
    @Override
    public void cancelRefPedido(String numero, String codigo, String cor) throws SQLException {
        StringBuilder query = new StringBuilder(
                "update ped_iten_001 pei set pei.motivo = '08', pei.qtde = pei.qtde - (pei.qtde -\n"
                        + "       nvl((select sum(qtde)\n"
                        + "          from ped_reserva_001\n"
                        + "         where numero = pei.numero\n"
                        + "           and codigo = pei.codigo\n"
                        + "           and cor = pei.cor\n"
                        + "           and tam = pei.tam),0)),\n"
                        + "       pei.qtde_canc = pei.qtde_canc + pei.qtde -\n"
                        + "       nvl((select sum(qtde)\n"
                        + "          from ped_reserva_001\n"
                        + "         where numero = pei.numero\n"
                        + "           and codigo = pei.codigo\n"
                        + "           and cor = pei.cor\n"
                        + "           and tam = pei.tam),0) \n"
                        + "\n"
                        + " where pei.numero = '" + numero + "'\n"
                        + "   and pei.codigo = '" + codigo + "'\n"
                        + "   and pei.cor = '" + cor + "'\n"
                        + "   and pei.tam not in (select tam\n"
                        + "                         from ped_reserva_001\n"
                        + "                        where numero = pei.numero\n"
                        + "                          and codigo = pei.codigo\n"
                        + "                          and cor = pei.cor\n"
                        + "                          and tam = pei.tam\n"
                        + "                          and qtde >= pei.qtde)"
        );
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        closeConnection();
        
        aplicaComissaoPedido(numero);
    }

    @Override
    public Integer trocarRefPedido(String numero, String codigo, String cor, String novoCodigo, ObservableMap<String, Integer> tams) throws SQLException {
        Integer affectedRows = 0;
//        sysdeliz2.models.ti.PedIten peiJpa = new FluentDao().selectFrom(sysdeliz2.models.ti.PedIten.class)
//                .where(eb -> eb
//                        .equal("id.numero", numero)
//                        .equal("id.codigo.codigo", novoCodigo))
//                .findAny();
//        if (peiJpa == null) {
            affectedRows = updateNovoRefPedido(numero, codigo, cor, novoCodigo);
//        } else {
//            for(Map.Entry<String, Integer> mapTam : tams.entrySet()) {
//                if (mapTam.getValue() > 0) {
//                    sysdeliz2.models.ti.PedIten peiTamJpa = new FluentDao().selectFrom(sysdeliz2.models.ti.PedIten.class)
//                            .where(eb -> eb
//                                    .equal("id.numero", numero)
//                                    .equal("id.codigo.codigo", novoCodigo)
//                                    .equal("id.cor.codigo", cor)
//                                    .equal("id.tam", mapTam.getKey()))
//                            .findAny();
//                    if (peiTamJpa != null) {
//                        peiTamJpa.setQtde(peiTamJpa.getQtde() + mapTam.getValue());
//                    } else {
//                        sysdeliz2.models.ti.PedIten oldPeiTamJpa = new FluentDao().selectFrom(sysdeliz2.models.ti.PedIten.class)
//                                .where(eb -> eb
//                                        .equal("id.numero", numero)
//                                        .equal("id.codigo.codigo", codigo)
//                                        .equal("id.cor.codigo", cor)
//                                        .equal("id.tam", mapTam.getKey()))
//                                .findAny();
//
//                        Produto novoProduto = new FluentDao().selectFrom(Produto.class).where(eb->eb.equal("codigo", novoCodigo)).singleResult();
//                        sysdeliz2.models.ti.PedIten newPeiTamJpa = oldPeiTamJpa;
//                        newPeiTamJpa.getId().setOrdem(peiJpa.getId().getOrdem());
//                        newPeiTamJpa.getId().setCodigo(novoProduto);
//                        new FluentDao().persist(newPeiTamJpa);
//                    }
//                }
//            }
//        }


        return affectedRows;
    }
    
    @Override
    public void reservarRefPedido(String numero, String codigo, String cor, String tam, Integer qtde) throws SQLException {
        StringBuilder query = new StringBuilder(
                "merge into ped_reserva_001 res\n"
                        + "using dual\n"
                        + "on (res.numero = ? and res.codigo = ? and res.cor = ? and res.tam = ?)\n"
                        + "when matched then\n"
                        + "  update set res.qtde = res.qtde + ?\n"
                        + "when not matched then\n"
                        + "  insert\n"
                        + "    (res.numero,\n"
                        + "     res.codigo,\n"
                        + "     res.cor,\n"
                        + "     res.tam,\n"
                        + "     res.qtde,\n"
                        + "     res.reserva,\n"
                        + "     res.deposito,\n"
                        + "     res.lote,\n"
                        + "     res.dt_cad)\n"
                        + "  values\n"
                        + "    (?,?,?,?,?,1,'0005','000000',sysdate)"
        );
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, cor);
        preparedStatement.setString(4, tam);
        
        preparedStatement.setInt(5, qtde);
        
        preparedStatement.setString(6, numero);
        preparedStatement.setString(7, codigo);
        preparedStatement.setString(8, cor);
        preparedStatement.setString(9, tam);
        preparedStatement.setInt(10, qtde);
        preparedStatement.execute();
        closeConnection();
    }
    
    @Override
    public String getReservaReferencia(String numero, String codigo, String cor) throws SQLException {
        String reservaReferencia = "X";
        
        StringBuilder query = new StringBuilder(""
                + "select distinct sd_reserva\n" +
                "  from ped_reserva_001\n" +
                " where numero = ?\n" +
                "   and codigo = ?\n" +
                "   and cor = ?\n" +
                "union\n" +
                "select distinct notafiscal\n" +
                "  from pedido3_001\n" +
                " where numero = ?\n" +
                "   and codigo = ?\n" +
                "   and cor = ?");
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, cor);
        preparedStatement.setString(4, numero);
        preparedStatement.setString(5, codigo);
        preparedStatement.setString(6, cor);
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            reservaReferencia = resultSetSql.getString("sd_reserva");
        }
        resultSetSql.close();
        this.closeConnection();
        return reservaReferencia;
    }
    
    @Override
    public Integer alterarGradePedido(String numero, String codigo, String cor, String tam, Integer alteracao, String motivoCancelamento) throws SQLException {
        
        PedIten pedIten = getItenPedido(numero, codigo);
        String motivo = "";
        String sqlCancela = "";
        if (alteracao < 0) {
            motivo = motivoCancelamento;
            sqlCancela = ", pei.qtde_canc = pei.qtde_canc + " + (alteracao * (-1));
        }
        
        StringBuilder query = new StringBuilder(
                "merge into ped_iten_001 pei using dual on (\n"
                        + "      pei.numero = ?\n"
                        + "      and pei.codigo = ?\n"
                        + "      and pei.cor = ?\n"
                        + "      and pei.tam = ?\n"
                        + ")\n"
                        + "when matched then \n"
                        + "  update set pei.qtde = pei.qtde + ?, pei.motivo = ?" + sqlCancela + "\n"
                        + "when not matched then\n"
                        + "  insert (ordem, qtde, preco, tam, cor, numero, codigo, qtde_packs, qualidade, qtde_orig, preco_orig, tipo, dt_entrega, nr_item)\n"
                        + "  values (    ?,    ?,     ?,   ?,   ?,   ?,      ?,      1,          '1',       ?,         ?,          'P',  to_date('" + pedIten.getDt_entrega().substring(0, 10) + "','yyyy-mm-dd'), ?)"
        );
        
        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.setString(1, numero);
        preparedStatement.setString(2, codigo);
        preparedStatement.setString(3, cor);
        preparedStatement.setString(4, tam);
        preparedStatement.setInt(5, alteracao);
        preparedStatement.setString(6, motivo);
        
        preparedStatement.setInt(7, pedIten.getOrdem());
        preparedStatement.setInt(8, alteracao);
        preparedStatement.setDouble(9, pedIten.getPreco());
        preparedStatement.setString(10, tam);
        preparedStatement.setString(11, cor);
        preparedStatement.setString(12, numero);
        preparedStatement.setString(13, codigo);
        preparedStatement.setInt(14, alteracao);
        preparedStatement.setDouble(15, pedIten.getPreco());
        preparedStatement.setInt(16, pedIten.getOrdem());
        Integer affectedRows = preparedStatement.executeUpdate();
        closeConnection();
        
        aplicaComissaoPedido(numero);
        
        return affectedRows;
    }
    
}
