/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import oracle.jdbc.OracleTypes;
import sysdeliz2.daos.ReceberCartaoDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.ReceberCartao;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class ReceberCartaoOracleTA implements ReceberCartaoDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (statement != null) {
            statement.close();
        }
        connection.commit();
        connection.close();
    }

    @Override
    public String save_transaction(String payment_id, String auth_code, String terminal, String ati,
                                   String verification_id, Double valor, String status, String pedido, Integer parcelas, String brand,
                                   String token_card, String token_oauth, String cvv, String validade, String cartao, String data, String pedido_agrupado) throws SQLException {
        String numero = "X";

        StringBuilder query = new StringBuilder(
                "BEGIN\n"
                        + "INSERT INTO SD_RECEBER_CARTAO_001\n"
                        + "(PAYMENT_ID, AUTHORIZATION_CODE, TERMINAL_NSU, AQUIRER_TRANSACTION_ID, VERIFICATION_ID, "
                        + "VALOR, STATUS, PEDIDO, PARCELAS, BRAND, TOKEN_CARD, TOKEN_OAUTH, CVV, VALIDADE_CARTAO, CARTAO, DATA_RECEBER, PEDIDO_AGRUPADO)\n"
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING NUMERO INTO ?;\n"
                        + "END;"
        );

        connection = ConnectionFactory.getEmpresaConnection();
        //statement = connection.createStatement();
        CallableStatement cPreparedStatement = connection.prepareCall(query.toString());
        cPreparedStatement.setString(1, payment_id);
        cPreparedStatement.setString(2, auth_code);
        cPreparedStatement.setString(3, terminal);
        cPreparedStatement.setString(4, ati);
        cPreparedStatement.setString(5, verification_id);
        cPreparedStatement.setDouble(6, valor);
        cPreparedStatement.setString(7, status);
        cPreparedStatement.setString(8, pedido);
        cPreparedStatement.setInt(9, parcelas);
        cPreparedStatement.setString(10, brand);
        cPreparedStatement.setString(11, token_card);
        cPreparedStatement.setString(12, token_oauth);
        cPreparedStatement.setString(13, cvv);
        cPreparedStatement.setString(14, validade);
        cPreparedStatement.setString(15, cartao);
        cPreparedStatement.setTimestamp(16, Timestamp.valueOf(data));
        cPreparedStatement.setString(17, pedido_agrupado);
        cPreparedStatement.registerOutParameter(18, OracleTypes.NUMBER);
        cPreparedStatement.execute();
        numero = cPreparedStatement.getString(18);
        closeConnection();

        return numero;
    }

    @Override
    public ObservableList<ReceberCartao> get_all() throws SQLException {
        ObservableList<ReceberCartao> recibos = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "select rec.numero, rec.payment_id, rec.authorization_code, rec.pedido, rec.valor, rec.cartao, rec.brand, rec.parcelas, rec.status, rec.data_receber,\n"
                        + "       cli.codcli, cli.nome, rec.pedido_agrupado agrupado\n"
                        + "from sd_receber_cartao_001 rec\n"
                        + "left join pedido_001 ped on rec.pedido = ped.numero\n"
                        + "LEFT JOIN RECEBER_001 dup ON dup.NUMERO = REPLACE(rec.PEDIDO,'.','/')\n"
                        + "left join entidade_001 cli on cli.codcli = CASE WHEN ped.codcli IS NOT NULL THEN ped.codcli ELSE dup.CODCLI END");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            recibos.add(new ReceberCartao(resultSetSql.getString("numero"), resultSetSql.getString("payment_id"), resultSetSql.getString("authorization_code"), resultSetSql.getString("pedido"),
                    resultSetSql.getDouble("valor"), resultSetSql.getString("cartao"), resultSetSql.getString("brand"), resultSetSql.getInt("parcelas"),
                    resultSetSql.getString("status"), resultSetSql.getString("data_receber"), resultSetSql.getString("codcli"), resultSetSql.getString("nome"), resultSetSql.getString("agrupado")));
        }

        closeConnection();
        resultSetSql.close();

        return recibos;
    }

    @Override
    public ObservableList<ReceberCartao> get_form(String pedido, String cliente, String data) throws SQLException {
        ObservableList<ReceberCartao> recibos = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "select rec.numero, rec.payment_id, rec.authorization_code, rec.pedido, rec.valor, rec.cartao, rec.brand, rec.parcelas, rec.status, rec.data_receber,\n"
                        + "       cli.codcli, cli.nome, rec.pedido_agrupado agrupado\n"
                        + "from sd_receber_cartao_001 rec\n"
                        + "left join pedido_001 ped on rec.pedido = ped.numero\n"
                        + "LEFT JOIN RECEBER_001 dup ON dup.NUMERO = REPLACE(rec.PEDIDO,'.','/')\n"
                        + "left join entidade_001 cli on cli.codcli = CASE WHEN ped.codcli IS NOT NULL THEN ped.codcli ELSE dup.CODCLI END\n"
                        + "where rec.pedido like '%" + pedido + "%' or\n"
                        + "      (cli.codcli like '%" + cliente + "%' or cli.nome like '%" + cliente + "%' or cli.cnpj like '%" + cliente + "%') or\n"
                        + "      to_char(rec.data_receber,'dd/MM/yyyy') = '" + data + "'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            recibos.add(new ReceberCartao(resultSetSql.getString("numero"), resultSetSql.getString("payment_id"), resultSetSql.getString("authorization_code"), resultSetSql.getString("pedido"),
                    resultSetSql.getDouble("valor"), resultSetSql.getString("cartao"), resultSetSql.getString("brand"), resultSetSql.getInt("parcelas"),
                    resultSetSql.getString("status"), resultSetSql.getString("data_receber"), resultSetSql.getString("codcli"), resultSetSql.getString("nome"), resultSetSql.getString("agrupado")));
        }

        closeConnection();
        resultSetSql.close();

        return recibos;
    }

    @Override
    public void cancel_transaction(String numero, String data_cancel, String payment_id, String request_id, String status) throws SQLException {
        StringBuilder query = new StringBuilder(
                "update sd_receber_cartao_001 rec\n"
                        + "   set rec.status            = '" + status + "',\n"
                        + "       rec.payment_id_cancel = '" + payment_id + "',\n"
                        + "       rec.request_id_cancel = '" + request_id + "',\n"
                        + "       rec.data_cancel       = to_date('" + data_cancel + "', 'YYYY-MM-DD HH24:MI:SS')\n"
                        + " where rec.numero = '" + numero + "'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();

        closeConnection();
    }

    @Override
    public void status_transaction(String numero, String status) throws SQLException {
        StringBuilder query = new StringBuilder(
                "update sd_receber_cartao_001 rec\n"
                        + "   set rec.status = '" + status + "'\n"
                        + " where rec.numero = '" + numero + "'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();

        closeConnection();
    }

}
