/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import sysdeliz2.daos.ReceberDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.Pedido;

import java.sql.*;

/**
 * @author cristiano.diego
 */
public class ReceberOracleTA implements ReceberDAO {
// Variaveis locais

    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (statement != null) {
            statement.close();
        }
        connection.commit();
        connection.close();
    }

    @Override
    public Pedido getReceber(String numero) throws SQLException {
        Pedido pedido = null;

        StringBuilder query = new StringBuilder(
                "select rec.numero, cli.nome, cli.cnpj_cob, cli.sit_cli, cli.end_cob, cli.num_cob, cli.bairro_cob,  \n"
                        + "cid.nome_cid, cid.cod_est, cli.telefone, cli.email, to_char(rec.obs) \"obs\", (rec.valor2 + rec.juros) valor, (rec.desconto + rec.val_dev) desconto,\n"
                        + "(rec.valor2 + rec.juros - rec.desconto - rec.val_dev) total, cli.tipo, cli.codcli, cli.cep_cob, nvl(recc.valor,0) recebido, cli.grupo\n"
                        + "from receber_001 rec\n"
                        + "inner join entidade_001 cli on rec.codcli = cli.codcli\n"
                        + "inner join cadcep_001 cep on cli.cep_cob = cep.cep\n"
                        + "inner join cidade_001 cid on cep.cod_cid = cid.cod_cid\n"
                        + "left join (select pedido, sum(valor) valor from sd_receber_cartao_001 where status = 'APPROVED' group by pedido) recc on recc.pedido = rec.numero\n"
                        + "where rec.numero = '" + numero + "' \n"
                        + "and nvl(recc.valor,0) < (rec.valor2 + rec.juros - rec.desconto - rec.val_dev)");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        if (resultSetSql.next()) {
            pedido = new Pedido(resultSetSql.getString("numero"), resultSetSql.getString("nome"), resultSetSql.getString("cnpj_cob"), resultSetSql.getString("sit_cli"),
                    resultSetSql.getString("end_cob"), resultSetSql.getString("num_cob"), resultSetSql.getString("bairro_cob"), resultSetSql.getString("nome_cid"),
                    resultSetSql.getString("cod_est"), resultSetSql.getString("telefone"), resultSetSql.getString("email"), resultSetSql.getString("obs"),
                    resultSetSql.getDouble("valor"), resultSetSql.getDouble("desconto"), resultSetSql.getDouble("total"), resultSetSql.getString("tipo"),
                    resultSetSql.getString("codcli"), resultSetSql.getString("cep_cob"), resultSetSql.getDouble("recebido"), resultSetSql.getString("grupo"));
        }

        closeConnection();
        resultSetSql.close();

        return pedido;
    }
}
