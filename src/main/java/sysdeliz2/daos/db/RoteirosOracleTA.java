/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.RoteirosDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.properties.Roteiros;

import java.sql.*;
import java.time.format.DateTimeFormatter;

/**
 * @author cristiano.diego
 */
public class RoteirosOracleTA implements RoteirosDAO {

    // Variaveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    // Metodos PRIVATE
    private void closeConnection() throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (statement != null) {
            statement.close();
        }
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<Roteiros> getAll() throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE, ROT.STATUS\n"
                        + "FROM SD_ROTEIROS_001 ROT");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public ObservableList<Roteiros> getUser(String user) throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE, ROT.STATUS\n"
                        + "FROM SD_ROTEIROS_001 ROT WHERE ROT.SOLICITANTE = '" + user + "' and rot.status <> 'Finalizado'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public ObservableList<Roteiros> getAllAbertos() throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE, ROT.STATUS\n"
                        + "FROM SD_ROTEIROS_001 ROT WHERE ROT.STATUS = 'Aberto'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public ObservableList<Roteiros> getByForm(String dt_solicitacao, String tipo, String fornecedor, String cidade, String mercadoria) throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        String dt_inicio = "01/01/1900";
        String dt_fim = "31/12/9999";
        String tp_rota = tipo.length() == 0 ? "" : tipo.substring(0, tipo.length() - 1);
        if (!dt_solicitacao.isEmpty()) {
            dt_inicio = dt_solicitacao;
            dt_fim = dt_solicitacao;
        }

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE\n"
                        + "FROM SD_ROTEIROS_001 ROT\n"
                        + "INNER JOIN ENTIDADE_001 FORN ON FORN.CODCLI = ROT.CODFOR AND FORN.TIPO_ENTIDADE LIKE '%F%'\n"
                        + "WHERE ROT.DT_SOLICITACAO BETWEEN TO_DATE('" + dt_inicio + "','dd/MM/yyyy') AND TO_DATE('" + dt_fim + "','dd/MM/yyyy')\n"
                        + "	  AND REGEXP_LIKE(ROT.TIPO, '*(" + tp_rota + ")')\n"
                        + "	  AND FORN.NOME LIKE '%" + fornecedor + "%'\n"
                        + "	  AND ROT.CIDADE LIKE '%" + cidade + "%'\n"
                        + "	  AND ROT.MERCADORIA LIKE '%" + mercadoria + "%'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public ObservableList<Roteiros> getByFormDtRoteiro(String dt_roteiro, String tipo, String fornecedor, String cidade, String mercadoria) throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        String dt_inicio = "01/01/1900";
        String dt_fim = "31/12/9999";
        String tp_rota = tipo.length() == 0 ? "" : tipo.substring(0, tipo.length() - 1);
        if (!dt_roteiro.isEmpty()) {
            dt_inicio = dt_roteiro;
            dt_fim = dt_roteiro;
        }

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE\n"
                        + "FROM SD_ROTEIROS_001 ROT\n"
                        + "INNER JOIN ENTIDADE_001 FORN ON FORN.CODCLI = ROT.CODFOR AND FORN.TIPO_ENTIDADE LIKE '%F%'\n"
                        + "WHERE ROT.DT_ROTEIRO BETWEEN TO_DATE('" + dt_inicio + "','dd/MM/yyyy') AND TO_DATE('" + dt_fim + "','dd/MM/yyyy')\n"
                        + "	  AND REGEXP_LIKE(ROT.TIPO, '*(" + tp_rota + ")')\n"
                        + "	  AND FORN.NOME LIKE '%" + fornecedor + "%'\n"
                        + "	  AND ROT.CIDADE LIKE '%" + cidade + "%'\n"
                        + "	  AND ROT.MERCADORIA LIKE '%" + mercadoria + "%'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public ObservableList<Roteiros> getByFormUser(String dt_roteiro, String tipo, String fornecedor, String cidade, String mercadoria, String usuario, String status) throws SQLException {
        ObservableList<Roteiros> roteiros = FXCollections.observableArrayList();

        String dt_inicio = "01/01/1900";
        String dt_fim = "31/12/9999";
        String tp_rota = tipo.length() == 0 ? "" : tipo.substring(0, tipo.length() - 1);
        if (!dt_roteiro.isEmpty()) {
            dt_inicio = dt_roteiro;
            dt_fim = dt_roteiro;
        }

        StringBuilder query = new StringBuilder(
                "SELECT ROT.CODIGO, ROT.STATUS, ROT.SOLICITANTE, TO_CHAR(ROT.DT_SOLICITACAO,'dd/MM/yyyy hh:mm:ss') DT_SOLICITACAO, TO_CHAR(ROT.DT_ROTEIRO,'dd/MM/yyyy') DT_ROTEIRO, \n"
                        + "	ROT.TIPO, ROT.MERCADORIA, ROT.OBSERVACAO, ROT.CODFOR, ROT.ENDERECO, ROT.BAIRRO, ROT.CIDADE, ROT.UF, ROT.REFERENCIA, ROT.TELEFONE\n"
                        + "FROM SD_ROTEIROS_001 ROT\n"
                        + "INNER JOIN ENTIDADE_001 FORN ON FORN.CODCLI = ROT.CODFOR AND FORN.TIPO_ENTIDADE LIKE '%F%'\n"
                        + "WHERE ROT.DT_ROTEIRO BETWEEN TO_DATE('" + dt_inicio + "','dd/MM/yyyy') AND TO_DATE('" + dt_fim + "','dd/MM/yyyy')\n"
                        + "	  AND REGEXP_LIKE(ROT.TIPO, '*(" + tp_rota + ")')\n"
                        + "	  AND FORN.NOME LIKE '%" + fornecedor + "%'\n"
                        + "	  AND ROT.CIDADE LIKE '%" + cidade + "%'\n"
                        + "	  AND ROT.MERCADORIA LIKE '%" + mercadoria + "%'"
                        + "	  AND (ROT.STATUS = '" + status + "' or '"+status+"' = 'Todos')"
                        + "	  AND ROT.SOLICITANTE = '" + usuario + "'");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            roteiros.add(new Roteiros(resultSetSql.getInt("CODIGO"), resultSetSql.getString("SOLICITANTE"), resultSetSql.getString("DT_SOLICITACAO"),
                    resultSetSql.getString("DT_ROTEIRO"), resultSetSql.getString("TIPO"), resultSetSql.getString("MERCADORIA"), resultSetSql.getString("OBSERVACAO"),
                    resultSetSql.getString("CODFOR"), resultSetSql.getString("ENDERECO"), resultSetSql.getString("BAIRRO"), resultSetSql.getString("CIDADE"),
                    resultSetSql.getString("UF"), resultSetSql.getString("REFERENCIA"), resultSetSql.getString("TELEFONE"), resultSetSql.getString("STATUS")));
        }

        closeConnection();
        resultSetSql.close();

        return roteiros;
    }

    @Override
    public String save(Roteiros roteiro) throws SQLException {
        String codigo = "";
        StringBuilder query = new StringBuilder("{? = call F_SD_CREATE_ROTEIROS(?,?,?,?,?,?,?,?,?,?,?,?)}");
        connection = ConnectionFactory.getEmpresaConnection();
        CallableStatement callStatement = connection.prepareCall(query.toString());
        callStatement.registerOutParameter(1, Types.VARCHAR);
        callStatement.setString(2, roteiro.getSolicitante());
        callStatement.setString(3, roteiro.getDt_roteiro().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        callStatement.setString(4, roteiro.getTipo());
        callStatement.setString(5, roteiro.getMercadoria());
        callStatement.setString(6, roteiro.getObservacao());
        callStatement.setString(7, roteiro.getCodfor());
        callStatement.setString(8, roteiro.getEndereco());
        callStatement.setString(9, roteiro.getBairro());
        callStatement.setString(10, roteiro.getCidade());
        callStatement.setString(11, roteiro.getUf());
        callStatement.setString(12, roteiro.getReferencia());
        callStatement.setString(13, roteiro.getTelefone());

        callStatement.execute();
        codigo = callStatement.getString(1);
        callStatement.close();
        closeConnection();

        return codigo;
    }

    @Override
    public void update(Roteiros roteiro) throws SQLException {
        StringBuilder query = new StringBuilder(
                "UPDATE SD_ROTEIROS_001 SET\n"
                        + "DT_ROTEIRO = to_date('" + roteiro.getDt_roteiro().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "','dd/MM/yyyy'),\n"
                        + "OBSERVACAO = '" + roteiro.getObservacao() + "',\n"
                        + "ENDERECO = '" + roteiro.getEndereco() + "',\n"
                        + "BAIRRO = '" + roteiro.getBairro() + "',\n"
                        + "CIDADE = '" + roteiro.getCidade() + "',\n"
                        + "UF = '" + roteiro.getUf() + "',\n"
                        + "REFERENCIA = '" + roteiro.getReferencia() + "',\n"
                        + "TELEFONE = '" + roteiro.getTelefone() + "'\n"
                        + "WHERE CODIGO = " + roteiro.getCodigo()
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        closeConnection();
    }

    @Override
    public void updateStatus(Roteiros roteiro) throws SQLException {
        StringBuilder query = new StringBuilder(
                "UPDATE SD_ROTEIROS_001 SET\n"
                        + "STATUS = '" + roteiro.getStatus() + "'\n"
                        + "WHERE CODIGO = " + roteiro.getCodigo()
        );

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        closeConnection();
    }

    @Override
    public void delete(Roteiros roteiro) throws SQLException {
        StringBuilder query = new StringBuilder(
                "DELETE FROM SD_ROTEIROS_001 WHERE CODIGO = " + roteiro.getCodigo());

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        closeConnection();
    }

}
