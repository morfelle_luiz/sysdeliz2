/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.daos.db;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.SitCliDAO;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.models.SitCli;

import java.sql.*;
import java.util.List;

/**
 * @author cristiano.diego
 */
public class SitCliOracleTA implements SitCliDAO {

    // Variáveis locais
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private String whereFilter = "";

    // Métodos PRIVATE
    private void closeConnection() throws SQLException {
        preparedStatement.close();
        statement.close();
        connection.commit();
        connection.close();
    }

    @Override
    public ObservableList<SitCli> getAll() throws SQLException {
        ObservableList<SitCli> sitclis = FXCollections.observableArrayList();

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sitcli_001\n");

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            SitCli sitcli = new SitCli();
            sitcli.setCodigo(resultSetSql.getString("codigo"));
            sitcli.setDescricao(resultSetSql.getString("descricao"));
            sitclis.add(sitcli);
        }
        resultSetSql.close();
        this.closeConnection();
        return sitclis;
    }

    @Override
    public ObservableList<SitCli> getSitCliFromFilter(List<ObservableList<String>> wheresClauses) throws SQLException {
        ObservableList<SitCli> sitclis = FXCollections.observableArrayList();

        whereFilter = "";
        wheresClauses.forEach(filters -> {
            whereFilter += filters.get(0)
                    + " "
                    + filters.get(1)
                    + " "
                    + filters.get(2)
                    + (filters.get(2).equals("in") ? " (" : filters.get(2).equals("like") ? " '%" : " '")
                    + filters.get(3).toUpperCase()
                    + (filters.get(2).equals("in") ? ")" : filters.get(2).equals("like") ? "%'" : "'")
                    + "\n";
        });

        StringBuilder query = new StringBuilder(""
                + "select *\n"
                + "  from sitcli_001\n"
                + " where" + whereFilter);

        connection = ConnectionFactory.getEmpresaConnection();
        statement = connection.createStatement();
        preparedStatement = connection.prepareStatement(query.toString());
        preparedStatement.execute();
        ResultSet resultSetSql = preparedStatement.getResultSet();
        while (resultSetSql.next()) {
            SitCli sitcli = new SitCli();
            sitcli.setCodigo(resultSetSql.getString("codigo"));
            sitcli.setDescricao(resultSetSql.getString("descricao"));
            sitclis.add(sitcli);
        }
        resultSetSql.close();
        this.closeConnection();
        return sitclis;
    }
}
