package sysdeliz2.daos.generics.helpers;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author lima.joao
 * @since 29/08/2019 09:38
 */
@Converter
public class BooleanSimNaoConverter implements AttributeConverter<Boolean, String> {
    @Override
    public String convertToDatabaseColumn(Boolean value) {
        return value ? "S" : "N";
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        if ("S".equals(value))
            return true;
        else if ("N".equals(value))
            return false;

        return null;
    }
}
