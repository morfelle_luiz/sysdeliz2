package sysdeliz2.daos.generics.helpers;

import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.sysdeliz.SdLogSysdeliz;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author lima.joao
 * @since 22/08/2019 09:59
 */
public class LogHelper {

    //private GenericDao<SdLogSysdeliz001> genericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLogSysdeliz001.class);

    private static LogHelper instance;

    // Construtor privado
    private LogHelper() {
    }

    // Pega a instancia
    public static LogHelper getInstance() {
        if (instance == null) {
            instance = new LogHelper();
        }
        return instance;
    }

    public void addLog(SdLogSysdeliz log) throws SQLException {
        if (log != null) {
            JPAUtils.getEntityManager().getTransaction().begin();
            JPAUtils.getEntityManager().createNativeQuery(log.buildNativeInsert()).executeUpdate();
            JPAUtils.getEntityManager().getTransaction().commit();
            JPAUtils.getEntityManager().flush();
            //genericDao.save(log);.
        }
    }

    public void addLog(TipoAcao acao, String tela, String referencia, String descricao) throws SQLException {
        SdLogSysdeliz log = new SdLogSysdeliz(
                new Date(),
                SceneMainController.getAcesso().getUsuario(),
                tela,
                acao.tipoAcao,
                referencia,
                descricao
        );
        this.addLog(log);
    }

    public void addLogInsert(String tela, String referencia, String descricao) throws SQLException {
        this.addLog(TipoAcao.CADASTRAR, tela, referencia, descricao);
    }

    public void addLogEdit(String tela, String referencia, String descricao) throws SQLException {
        this.addLog(TipoAcao.EDITAR, tela, referencia, descricao);
    }

    public void addLogExcluir(String tela, String referencia, String descricao) throws SQLException {
        this.addLog(TipoAcao.EXCLUIR, tela, referencia, descricao);
    }

}
