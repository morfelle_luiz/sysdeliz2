package sysdeliz2.daos.generics.helpers;

public class Ordenacao {
    private String tipo = "asc";
    private String fieldName;

    public Ordenacao() {
    }
    
    public Ordenacao(String fieldName) {
        this.fieldName = fieldName;
    }
    
    public Ordenacao(String tipo, String fieldName) {
        this.tipo = tipo;
        this.fieldName = fieldName;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}