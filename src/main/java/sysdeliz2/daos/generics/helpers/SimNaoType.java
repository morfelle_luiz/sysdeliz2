package sysdeliz2.daos.generics.helpers;

import org.hibernate.dialect.Dialect;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.DiscriminatorType;
import org.hibernate.type.PrimitiveType;
import org.hibernate.type.StringType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

import java.io.Serializable;

@Deprecated
/**
 * A type that maps between {@link java.sql.Types#VARCHAR VARCHAR2(1)} and {@link Boolean} (using 'S' and 'N')
 *
 * @author lima.joao
 * @since 31/07/2019 15:23
 */
public class SimNaoType extends AbstractSingleColumnStandardBasicType<Boolean> implements PrimitiveType<Boolean>, DiscriminatorType<Boolean> {

    public static final SimNaoType INSTANCE = new SimNaoType();

    public SimNaoType() {
        super(VarcharTypeDescriptor.INSTANCE, CustomBooleanTypeDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return "sim_nao";
    }

    @Override
    public Class getPrimitiveClass() {
        return boolean.class;
    }

    @Override
    public Boolean stringToObject(String xml) throws Exception {
        return fromString(xml);
    }

    @Override
    public Serializable getDefaultValue() {
        return Boolean.FALSE;
    }

    @Override
    public String objectToSQLString(Boolean value, Dialect dialect) throws Exception {
        return StringType.INSTANCE.objectToSQLString(value ? "S" : "N", dialect);
    }
}
