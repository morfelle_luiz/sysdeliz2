package sysdeliz2.daos.generics.implementation;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Esta classe recebe uma classe via parametro do construtor, desta forma é possivel utilizar apenas o GenericDaoImpl para manipular o banco de dados.
 * <p>
 * NOTA: Case seja usado injeção de dependencias via CDI isto não será possivel, será necessário ter uma classe que herde de GenericDao para cada entity
 * a ser manipulada, pois o WIELD não consegue inferir de forma generica os tipos. Limitação do bytecode que não armazena a classe dentro dele. Nessa situação será
 * necessário utilizar o ParametrizedType para resolver a classe, de forma generica (Gambiarra que funciona).
 *
 * @author lima.joao
 * @since 11/07/2019 17:48
 */
public class GenericDaoImpl<T> implements GenericDao<T> {
    
    protected EntityManager entityManager;
    private final Class<T> persistentClass;
    
    private CriteriaBuilder builder;
    protected CriteriaQuery<T> query;
    protected Root<T> from;
    private Predicate predicate;
    private Map<String, List<Predicate>> predicateGroup = new HashMap<>();
    
    private Boolean autoTransaction = true;
    
    @Deprecated
    public GenericDaoImpl(EntityManager entityManager, Class<T> entityClass) {
        this.entityManager = entityManager;
        persistentClass = entityClass; //(Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    public GenericDaoImpl(Class<T> entityClass) {
        this.entityManager = JPAUtils.getEntityManager();
        persistentClass = entityClass;
    }
    
    public GenericDaoImpl() {
        this.entityManager = JPAUtils.getEntityManager();
        persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    public void setAutoTransaction(Boolean autoTransaction) {
        this.autoTransaction = autoTransaction;
    }
    
    private void startTransaction() {
        if (autoTransaction) {
            if (!this.entityManager.getTransaction().isActive()) {
                this.entityManager.getTransaction().begin();
            }
        }
    }
    
    private void commitTransaction() {
        if (autoTransaction) {
            if (this.entityManager.getTransaction().isActive()) {
                this.entityManager.getTransaction().commit();
            }
        }
    }
    
    @Override
    public ObservableList<T> list() throws SQLException {
        if (entityManager == null || !entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        initCriteria();
        try {
            return FXCollections.observableArrayList(this.entityManager.createQuery(this.query).getResultList());
            //(ObservableList<T>) this.entityManager.createQuery(this.query).getResultList();
        } catch (NoResultException ne) {
            throw new SQLException("Não foi encontrado nenhum resultado");
        }
    }
    
    @Override
    public ObservableList<T> list(Integer limit) throws SQLException {
        if (entityManager == null || !entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        initCriteria();
        try {
            return FXCollections.observableArrayList(this.entityManager.createQuery(this.query).setMaxResults(limit).getResultList());
            //(ObservableList<T>) this.entityManager.createQuery(this.query).getResultList();
        } catch (NoResultException ne) {
            throw new SQLException("Não foi encontrado nenhum resultado");
        }
    }
    
    @Override
    public List<T> simpleList() throws SQLException {
        if (entityManager == null || !entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        initCriteria();
        try {
            return this.entityManager.createQuery(this.query).getResultList();
            //(ObservableList<T>) this.entityManager.createQuery(this.query).getResultList();
        } catch (NoResultException ne) {
            throw new SQLException("Não foi encontrado nenhum resultado");
        }
    }
    
    @Override
    public T save(T entity) throws SQLException {
        return this.save(entity, false);
    }
    
    @Override
    public T save(T entity, Boolean isPersist) throws SQLException {
        
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        if (entity == null) {
            throw new SQLException("Entidade não pode ser nula");
        }
        
        try {
            startTransaction();
            
            if (isPersist) {
                this.entityManager.persist(entity);
            } else {
                entity = this.entityManager.merge(entity);
            }
            
            this.entityManager.flush();
            
            commitTransaction();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
    
    
    @Override
    public ObservableList<T> saveList(ObservableList<T> entities) throws SQLException {
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        for (T entity : entities) {
            entity = save(entity);
        }
        
        return entities;
    }
    
    @Override
    public T update(T entity) throws SQLException {
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        if (entity == null) {
            throw new SQLException("Entidade não pode ser nula");
        }
        
        startTransaction();
        
        try {
            try {
                entity = this.entityManager.merge(entity);
                this.entityManager.flush();
            } catch (ConstraintViolationException e) {
                throw new SQLException(e.getMessage());
            }
        } finally {
            commitTransaction();
        }
        return entity;
    }
    
    @Override
    public void delete(Serializable id) throws SQLException {
        if (this.entityManager == null || !this.entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        T entity = this.load(id);
        if (entity == null)
            throw new NoResultException("Registro não encontrado para exclusão");
        
        this.delete(entity);
    }
    
    @Override
    public void delete(T entity) throws SQLException {
        startTransaction();
        this.entityManager.remove(entity);
        this.entityManager.flush();
        commitTransaction();
    }
    
    @Override
    public T load(Serializable id) throws SQLException {
        if (entityManager == null || !entityManager.isOpen()) {
            throw new SQLException("Entity manager não esta pronto");
        }
        
        return entityManager.find(persistentClass, id);
    }
    
    @Override
    public GenericDao<T> initCriteria() {
        this.builder = this.entityManager.getCriteriaBuilder();
        this.query = this.builder.createQuery(this.persistentClass);
        this.from = this.query.from(this.persistentClass);
        this.predicate = builder.and();
        this.query.select(from);
        return this;
    }
    
    @Override
    public T loadEntityByPredicate() {
        // prepara a query
        this.query.where(this.predicate);
        try {
            return (T) entityManager.createQuery(this.query).getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    @Override
    public ObservableList<T> loadListByPredicate() throws SQLException {
        // prepara a query
        this.query.where(this.predicate);
        
        try {
            
            return (ObservableList<T>) FXCollections.observableArrayList(this.entityManager.createQuery(this.query).getResultList());
        } catch (NoResultException ne) {
            throw new SQLException("Nenhum resultado encontrado");
        } catch (IllegalArgumentException iae){
            throw new SQLException("Erro ao fazer a consulta, campos numéricos não podem ser consultados com \"Contém\"");
        }
    }
    
    @Override
    public ObservableList<T> loadListByPredicate(Integer maxResults) throws SQLException {
        // prepara a query
        this.query.where(this.predicate);
        
        try {
            
            return (ObservableList<T>) FXCollections.observableArrayList(this.entityManager.createQuery(this.query).setMaxResults(maxResults).getResultList());
        } catch (NoResultException ne) {
            throw new SQLException("Nenhum resultado encontrado");
        }
    }
    
    @Override
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
    public void refreshEntity(EntityManager entityManager) {
        JPAUtils.getEntityManager().clear();
    }
    
    // <editor-fold defaultstate="collapsed" desc="No used">
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Integer valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEq(String fromField, Integer valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Long valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Long valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Boolean valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Boolean valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Serializable valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, Serializable valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, String valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateEq(String fromField, String valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEq(String fromField, Class valor) {
        this.addPredicateEq(fromField, valor, false);
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEq(String fromField, Class valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEqCascateField(String fromCascateField, String valor, boolean or) {
        Path<T> cascadeField = this.from;
        for (String s : fromCascateField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(cascadeField, valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(cascadeField, valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, String valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, Integer valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, Boolean valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromFieldPkEmbedded).get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Integer valor) {
        this.addPredicateNe(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Integer valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, String valor) {
        this.addPredicateNe(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, String valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        }
        
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Double valor) {
        this.addPredicateNe(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Double valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Class valor) {
        this.addPredicateNe(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNe(String fromField, Class valor, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.notEqual(this.from.get(fromField), valor));
        }
        return this;
    }
    
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateFrom(String fromField, String fromField2) {
        this.addPredicateFrom(fromField, fromField2, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateFrom(String fromField, String fromField2, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.equal(this.from.get(fromField), this.from.get(fromField2)));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.equal(this.from.get(fromField), this.from.get(fromField2)));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIn(String fromField, Object[] valores) {
        this.addPredicateIn(fromField, valores, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIn(String fromField, Object[] valores, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.from.get(fromField).in(valores));
        } else {
            this.predicate = this.builder.and(this.predicate, this.from.get(fromField).in(valores));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateInPkEmbedded(String fromFieldPkEmbedded, String fromField, Object[] valores, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.from.get(fromFieldPkEmbedded).get(fromField).in(valores));
        } else {
            this.predicate = this.builder.and(this.predicate, this.from.get(fromFieldPkEmbedded).get(fromField).in(valores));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNIn(String fromField, Object[] valores) {
        this.addPredicateNIn(fromField, valores, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNIn(String fromField, Object[] valores, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.from.get(fromField).in(valores).not());
        } else {
            this.predicate = this.builder.and(this.predicate, this.from.get(fromField).in(valores).not());
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateNInPkEmbedded(String fromFieldPkEmbedded, String fromField, Object[] valores, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.from.get(fromFieldPkEmbedded).get(fromField).in(valores).not());
        } else {
            this.predicate = this.builder.and(this.predicate, this.from.get(fromFieldPkEmbedded).get(fromField).in(valores).not());
        }
        return this;
    }
    
    /**
     * Para que o like seja executado em ambos lados é necessário mandar a string pronta com o % no começo e no final
     *
     * @param fromField Campo a ser filtrado
     * @param valor     valor do filtro
     */
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateLike(String fromField, String valor) {
        this.addPredicateLike(fromField, valor, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateLike(String fromField, String valor, boolean or) {
        String v;
        if (valor.startsWith("%") || valor.endsWith("%")) {
            v = valor;
        } else {
            v = "%" + valor + "%";
        }

        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.like(this.builder.upper(cascadeField.as(String.class)), v));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.like(this.builder.upper(cascadeField.as(String.class)), v));
        }
        return this;
    }

    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateLike(String fromField, Integer valor) {
        this.addPredicateLike(fromField, valor, false);
        return this;
    }

    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateLike(String fromField, Integer valor, boolean or) {
        String v = String.valueOf(valor);

        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.like(this.builder.upper(this.from.get(fromField)), v));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.like(this.builder.upper(this.from.get(fromField)), v));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIsNotNull(String fromField) {
        this.addPredicateIsNotNull(fromField, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIsNotNull(String fromField, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.isNotNull(this.from.get(fromField)));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.isNotNull(this.from.get(fromField)));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIsNull(String fromField) {
        this.addPredicateIsNull(fromField, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> addPredicateIsNull(String fromField, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.isNull(this.from.get(fromField)));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.isNull(this.from.get(fromField)));
        }
        return this;
    }
    //</editor-fold>
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromField, LocalDateTime begin, LocalDateTime end) {
        this.addPredicateBetween(fromField, begin, end, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromField, LocalDateTime begin, LocalDateTime end, boolean or) {
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.between(from.get(fromField), begin, end));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.between(from.get(fromField), begin, end));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromField, LocalDate begin, LocalDate end) {
        this.addPredicateBetween(fromField, begin, end, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromField, LocalDate begin, LocalDate end, boolean or) {
        if (begin == null && end == null)
            return this;
        
        if (begin == null && end != null)
            begin = end;
        else if (begin != null && end == null)
            end = begin;
        
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.between(cascadeField, begin, end));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.between(cascadeField, begin, end));
        }
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromFieldBegin, String fromFieldEnd, LocalDateTime value) {
        this.addPredicateBetween(fromFieldBegin, fromFieldEnd, value, false);
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao addPredicateBetween(String fromFieldBegin, String fromFieldEnd, LocalDateTime value, boolean or) {
        if (value == null)
            return this;
        
        Path cascadeFieldBegin = this.from;
        for (String s : fromFieldBegin.split("\\.")) {
            cascadeFieldBegin = cascadeFieldBegin.get(s);
        }
        Path cascadeFieldEnd = this.from;
        for (String s : fromFieldEnd.split("\\.")) {
            cascadeFieldEnd = cascadeFieldEnd.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.between(this.builder.literal(value), cascadeFieldBegin, cascadeFieldEnd));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.between(this.builder.literal(value), cascadeFieldBegin, cascadeFieldEnd));
        }
        return this;
    }
    
    // <editor-fold defaultstate="collapsed" desc="No used">
    @Override
    public GenericDao<T> addPredicateGt(String fromField, Object valor) {
        return addPredicateGt(fromField, valor, false);
    }
    
    @Override
    public GenericDao<T> addPredicateGt(String fromField, Object valor, boolean or) {
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateGtE(String fromField, Object valor) {
        return addPredicateGtE(fromField, valor, false);
    }
    
    @Override
    public GenericDao<T> addPredicateGtE(String fromField, Object valor, boolean or) {
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.greaterThanOrEqualTo(cascadeField, (Comparable) valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.greaterThanOrEqualTo(cascadeField, (Comparable) valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateLt(String fromField, Object valor) {
        return addPredicateLt(fromField, valor, false);
    }
    
    @Override
    public GenericDao<T> addPredicateLt(String fromField, Object valor, boolean or) {
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        }
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateLtE(String fromField, Object valor) {
        return addPredicateLtE(fromField, valor, false);
    }
    
    @Override
    public GenericDao<T> addPredicateLtE(String fromField, Object valor, boolean or) {
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        } else {
            this.predicate = this.builder.and(this.predicate, this.builder.greaterThan(cascadeField, (Comparable) valor));
        }
        return this;
    }
    //</editor-fold>
    
    @Override
    public GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType) {
        return addPredicate(fromField, valor, predicateType, false);
    }
    
    @Override
    public GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType, boolean or) {
        return addPredicate(fromField, valor, predicateType, or, "");
    }
    
    @Override
    public GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType, boolean or, String groupName) {
        if (valor == null)
            return this;
        if (valor.toString().length() == 0)
            return this;
        
        Path cascadeField = this.from;
        for (String s : fromField.split("\\."))
            cascadeField = cascadeField.get(s);
        
        Predicate predicateBuilder = this.builder.equal(cascadeField, (Comparable) valor);
        
        switch(predicateType) {
            case EQUAL:
                predicateBuilder = this.builder.equal(cascadeField, (Comparable) valor);
                break;
            case NOT_EQUAL:
                predicateBuilder = this.builder.notEqual(cascadeField, (Comparable) valor);
                break;
            case LIKE:
                predicateBuilder = this.builder.like(cascadeField, "%" + ((String) valor) + "%");
                break;
            case NOT_LIKE:
                predicateBuilder = this.builder.notLike(cascadeField, "%" + ((String) valor) + "%");
                break;
            case BEGIN:
                predicateBuilder = this.builder.like(cascadeField, ((String) valor) + "%");
                break;
            case NO_BEGIN:
                predicateBuilder = this.builder.notLike(cascadeField, ((String) valor) + "%");
                break;
            case END:
                predicateBuilder = this.builder.like(cascadeField, "%" + ((String) valor));
                break;
            case NOT_END:
                predicateBuilder = this.builder.notLike(cascadeField, "%" + ((String) valor));
                break;
            case IS_NULL:
                predicateBuilder = this.builder.isNull(cascadeField);
                break;
            case IS_NOT_NULL:
                predicateBuilder = this.builder.isNotNull(cascadeField);
                break;
            case GREATER_THAN:
                predicateBuilder = this.builder.greaterThan(cascadeField, (Comparable) valor);
                break;
            case GREATER_THAN_OR_EQUAL:
                predicateBuilder = this.builder.greaterThanOrEqualTo(cascadeField, (Comparable) valor);
                break;
            case LESS_THAN:
                predicateBuilder = this.builder.lessThan(cascadeField, (Comparable) valor);
                break;
            case LESS_THAN_OR_EQUAL:
                predicateBuilder = this.builder.lessThanOrEqualTo(cascadeField, (Comparable) valor);
                break;
        }
        
        if (groupName.length() == 0) {
            if (or)
                this.predicate = this.builder.or(this.predicate, predicateBuilder);
            else
                this.predicate = this.builder.and(this.predicate, predicateBuilder);
        } else {
            List<Predicate> group = this.predicateGroup.get(groupName);
            if (group != null)
                group.add(predicateBuilder);
            else {
                group = new ArrayList<>();
                group.add(predicateBuilder);
                predicateGroup.put(groupName, group);
            }
        }
        
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateGroup(String groupName, boolean or, boolean orInterno) {
        List<Predicate> group = predicateGroup.get(groupName);
        
        Predicate predicateInterno = null;
        if (group != null)
            for (Predicate predicate1 : group) {
                if (orInterno)
                    predicateInterno = this.builder.or(predicate1);
                else
                    predicateInterno = this.builder.and(predicate1);
            }
        
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, predicateInterno);
        } else {
            this.predicate = this.builder.and(this.predicate, predicateInterno);
        }
        
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object valor, PredicateType predicateType) {
        return addPredicateJoin(joinField, fromJoinField, valor, predicateType, false);
    }
    
    @Override
    public GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object valor, PredicateType predicateType, boolean or) {
        if (valor.toString().length() == 0)
            return this;
        
        Path cascadeField = this.from.join(joinField);
        for (String s : fromJoinField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        Predicate predicateBuilder = this.builder.equal(cascadeField, (Comparable) valor);
        
        switch(predicateType) {
            case EQUAL:
                predicateBuilder = this.builder.equal(cascadeField, (Comparable) valor);
                break;
            case NOT_EQUAL:
                predicateBuilder = this.builder.notEqual(cascadeField, (Comparable) valor);
                break;
            case LIKE:
                predicateBuilder = this.builder.like(cascadeField, "%" + ((String) valor) + "%");
                break;
            case NOT_LIKE:
                predicateBuilder = this.builder.notLike(cascadeField, "%" + ((String) valor) + "%");
                break;
            case IS_NULL:
                predicateBuilder = this.builder.isNull(cascadeField);
                break;
            case IS_NOT_NULL:
                predicateBuilder = this.builder.isNotNull(cascadeField);
                break;
            case GREATER_THAN:
                predicateBuilder = this.builder.greaterThan(cascadeField, (Comparable) valor);
                break;
            case GREATER_THAN_OR_EQUAL:
                predicateBuilder = this.builder.greaterThanOrEqualTo(cascadeField, (Comparable) valor);
                break;
            case LESS_THAN:
                predicateBuilder = this.builder.lessThan(cascadeField, (Comparable) valor);
                break;
            case LESS_THAN_OR_EQUAL:
                predicateBuilder = this.builder.lessThanOrEqualTo(cascadeField, (Comparable) valor);
                break;
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, predicateBuilder);
        } else {
            this.predicate = this.builder.and(this.predicate, predicateBuilder);
        }
        
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicate(String fromField, Object[] valor, PredicateType predicateType) {
        return addPredicate(fromField, valor, predicateType, false);
    }
    
    @Override
    public GenericDao<T> addPredicate(String fromField, Object[] valor, PredicateType predicateType, boolean or) {
        if (valor == null)
            return this;
        
        if (valor.length == 0)
            return this;
        else if (valor.length == 1)
            if (valor[0].toString().length() == 0)
                return this;
        
        Path cascadeField = this.from;
        for (String s : fromField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        Predicate predicateBuilder = null;
        
        switch(predicateType) {
            case IN:
                predicateBuilder = cascadeField.in(valor);
                break;
            case NOT_IN:
                predicateBuilder = cascadeField.in(valor).not();
                break;
            case IS_NULL:
                predicateBuilder = this.builder.isNull(cascadeField);
                break;
            case IS_NOT_NULL:
                predicateBuilder = this.builder.isNotNull(cascadeField);
                break;
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, predicateBuilder);
        } else {
            this.predicate = this.builder.and(this.predicate, predicateBuilder);
        }
        
        return this;
    }
    
    @Override
    public GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object[] valor, PredicateType predicateType) {
        return addPredicateJoin(joinField, fromJoinField, valor, predicateType, false);
    }
    
    @Override
    public GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object[] valor, PredicateType predicateType, boolean or) {
        if (valor.length == 0)
            return this;
        
        Join p = this.from.join(joinField);
        Path cascadeField = p;
        for (String s : fromJoinField.split("\\.")) {
            cascadeField = cascadeField.get(s);
        }
        
        Predicate predicateBuilder = builder.conjunction();
        
        switch(predicateType) {
            case IN:
                predicateBuilder = cascadeField.in(valor);
                break;
            case NOT_IN:
                predicateBuilder = cascadeField.in(valor).not();
                break;
            case IS_NULL:
                predicateBuilder = this.builder.isNull(cascadeField);
                break;
            case IS_NOT_NULL:
                predicateBuilder = this.builder.isNotNull(cascadeField);
                break;
        }
        
        if (or) {
            this.predicate = this.builder.or(this.predicate, predicateBuilder);
        } else {
            this.predicate = this.builder.and(this.predicate, predicateBuilder);
        }
        
        return this;
    }
    
    @Override
    public GenericDao<T> addDistinctValues() {
        this.query.distinct(true);
        return this;
    }
    
    @SuppressWarnings("unused")
    @Override
    public GenericDao<T> addOrderByAsc(String orderField) {
        this.query
                .orderBy(
                        this.builder
                                .asc(
                                        this.from.get(orderField)
                                )
                );
        return this;
    }
    
    @SuppressWarnings("unused")
    @Override
    public GenericDao<T> addOrderByDesc(String orderField) {
        this.query
                .orderBy(
                        this.builder.desc(
                                this.from.get(orderField)
                        )
                );
        return this;
    }
    
    @SuppressWarnings("unused")
    @Override
    public GenericDao<T> addOrderByList(List<Ordenacao> ordenacoes) {
        ArrayList<Order> orderList = new ArrayList<>();
        
        for (Ordenacao ordem : ordenacoes) {
            if (ordem.getTipo().toUpperCase().equals("ASC")) {
                orderList.add(
                        this.builder
                                .asc(this.from.get(ordem.getFieldName()))
                );
            } else {
                orderList.add(
                        this.builder
                                .desc(this.from.get(ordem.getFieldName()))
                );
            }
        }
        this.query.orderBy(orderList);
        return this;
    }
    
    @SuppressWarnings("unused")
    @Override
    public GenericDao<T> addOrderByAsc(String orderIdField, String orderField) {
        this.query
                .orderBy(
                        this.builder
                                .asc(
                                        this.from.get(orderIdField).get(orderField)
                                )
                );
        return this;
    }
    
    @SuppressWarnings("unused")
    @Override
    public GenericDao<T> addOrderByDesc(String orderIdField, String orderField) {
        this.query
                .orderBy(
                        this.builder.desc(
                                this.from.get(orderIdField).get(orderField)
                        )
                );
        return this;
    }
    
    @Override
    @SuppressWarnings("unused")
    public GenericDao<T> clearPredicate() {
        if (this.predicate != null) {
            this.predicate.getExpressions().clear();
            this.predicateGroup.clear();
        }
        return this;
    }
    
    @Override
    public void clear() {
        this.entityManager.clear();
    }
    
    @Override
    public GenericDao<T> flushClear() {
        if (this.entityManager.getTransaction().isActive()) {
            this.entityManager.flush();
        }
        this.entityManager.clear();
        return this;
    }
    
    @Override
    public GenericDao<T> flush() {
        if (this.entityManager.getTransaction().isActive()) {
            this.entityManager.flush();
        }
        return this;
    }
    
    @Override
    public List<Object[]> loadListBySQL(String sql) {
        return this.entityManager.createQuery(sql).getResultList();
    }
    
    
}