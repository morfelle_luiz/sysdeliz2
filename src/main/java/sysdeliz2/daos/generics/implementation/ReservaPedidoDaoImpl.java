package sysdeliz2.daos.generics.implementation;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.generics.interfaces.ReservaPedidoDao;
import sysdeliz2.models.view.VSdReservasPedidos;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lima.joao
 * @since 28/10/2019 17:32
 */
public class ReservaPedidoDaoImpl extends GenericDaoImpl<VSdReservasPedidos> implements ReservaPedidoDao {

    public ReservaPedidoDaoImpl(EntityManager entityManager) {
        super(entityManager, VSdReservasPedidos.class);
    }

    @SuppressWarnings("unchecked")
    private String getAIReservaPedido() {
        String strAIReserva;
        List<BigDecimal> result = entityManager.createNativeQuery("SELECT SD_SEQ_RESERVA_PEDIDO.NEXTVAL AS AIRESERVA FROM DUAL").getResultList();
        strAIReserva = String.valueOf(result.get(0).intValue());
        return strAIReserva;
    }

    public String addSdReservaPedido(VSdReservasPedidos reserva) {
        initCriteria();
        addPredicateEq("status", "A");
        addPredicateEq("numero", reserva.getNumero());

        List<VSdReservasPedidos> pedidos;
        try {
            pedidos = loadListByPredicate();
        } catch (SQLException e) {
            pedidos = new ArrayList<>();
            e.printStackTrace();
        }

        String sqlUpdateSdReservaPedido = "MERGE INTO SD_RESERVA_PEDIDO_001 SDRP USING DUAL ON ( \n"
                + "     SDRP.PEDIDO = ?\n"
                + "     AND SDRP.RESERVA = ?\n"
                + "     AND SDRP.CODIGO = ?\n"
                + "     AND SDRP.COR = ?\n"
                + "     AND SDRP.TAM = ? )\n"
                + " WHEN MATCHED THEN \n"
                + "     UPDATE SET \n"
                + "     SDRP.STATUS = ?,\n"
                + "     SDRP.USUARIO = ?, \n"
                + "     SDRP.\"DATA\" = SYSDATE,\n"
                + "     SDRP.ORDEM = ?"
                + " WHEN NOT MATCHED THEN \n"
                + "     INSERT VALUES (?,?,?,?,?,SYSDATE,?,?,?,?,'COM',?,?)";

        String sqlUpdatePedReserva = "UPDATE PED_RESERVA_001 PEDR\n"
                + "	    SET PEDR.SD_RESERVA = ?\n"
                + "	WHERE "
                + "     PEDR.NUMERO = ? "
                + "     AND PEDR.CODIGO = ? "
                + "     AND PEDR.COR = ? "
                + "     AND PEDR.TAM = ? "
                + "		AND PEDR.QTDE = ? "
                + "     AND PEDR.DEPOSITO = ?";

        String strAIReserva;
        if (reserva.getReserva().equals("0")) {
            strAIReserva = StringUtils.leftPad(getAIReservaPedido(), 10, "0");
        } else {
            strAIReserva = StringUtils.leftPad(reserva.getReserva(), 10, "0");
        }

        for (VSdReservasPedidos pedido : pedidos) {
            entityManager.getTransaction().begin();
            entityManager
                    .createNativeQuery(sqlUpdateSdReservaPedido)
                    // Filtro
                    .setParameter(1, pedido.getNumero())
                    .setParameter(2, pedido.getReserva())
                    .setParameter(3, pedido.getCodigo())
                    .setParameter(4, pedido.getCor())
                    .setParameter(5, pedido.getTam())
                    // Update
                    .setParameter(6, reserva.getStatus())
                    .setParameter(7, SceneMainController.getAcesso().getUsuario())
                    .setParameter(8, reserva.getOrdem())
                    // Insert
                    .setParameter(9, pedido.getNumero())
                    .setParameter(10, strAIReserva)
                    .setParameter(11, pedido.getCodigo())
                    .setParameter(12, pedido.getCor())
                    .setParameter(13, pedido.getTam())
                    .setParameter(14, SceneMainController.getAcesso().getUsuario())
                    .setParameter(15, reserva.getStatus())
                    .setParameter(16, pedido.getDeposito())
                    .setParameter(17, pedido.getQtde().toBigInteger())
                    .setParameter(18, reserva.getUsuarioLeitura())
                    .setParameter(19, reserva.getOrdem())
                    .executeUpdate();

            entityManager
                    .createNativeQuery(sqlUpdatePedReserva)
                    .setParameter(1, strAIReserva)
                    .setParameter(2, pedido.getNumero())
                    .setParameter(3, pedido.getCodigo())
                    .setParameter(4, pedido.getCor())
                    .setParameter(5, pedido.getTam())
                    .setParameter(6, pedido.getQtde().intValue())
                    .setParameter(7, pedido.getDeposito())
                    .executeUpdate();

            entityManager.getTransaction().commit();
        }
        return strAIReserva;
    }

    @Override
    public ObservableList<VSdReservasPedidos> carregaPorUsuarioLeitor(String usuario, String[] status) {
        ObservableList<VSdReservasPedidos> lista = FXCollections.observableArrayList();

        StringBuilder where = new StringBuilder();
        where.append("    1=1");

        if (!usuario.equals("")) {
            where.append("     and usuarioLeitura = '").append(usuario).append("'");
        }

        if (status.length > 0) {
            where.append("     and status in (");
            int count = 0;
            for (String s : status) {
                count++;
                where.append("'").append(s).append("'");
                if (status.length > count) {
                    where.append(",");
                }
            }
            where.append(")");
        }

        String sb = " SELECT" +
                "    numero," +
                "    reserva," +
                "    SUM(qtde)," +
                "    nome," +
                "    status," +
                "    nvl(ordem,0)" +
                " FROM" +
                "    VSdReservasPedidos" +
                " WHERE" +
                where.toString() +
                " GROUP BY" +
                "    numero, reserva, nome, status, ordem" +
                " ORDER BY" +
                "    ordem";

        List<Object[]> results = this.loadListBySQL(sb);

        for (Object[] result : results) {
            lista.add(
                    new VSdReservasPedidos(
                            result[0].toString(),
                            result[1].toString(),
                            BigDecimal.valueOf(Double.parseDouble(result[2].toString())),
                            "",
                            "",
                            "",
                            "",
                            result[4].toString(),
                            "",
                            "",
                            result[3] == null ? "" : result[3].toString(),
                            Integer.parseInt(result[5].toString())
                    )
            );
        }

        return lista;
    }

    public Integer getLastOrdem(String nome) {

        String sql = "SELECT nvl(Max(ordem),0) FROM V_SD_RESERVAS_PEDIDOS WHERE USUARIO_LEITURA = '" + nome + "'";

        List<BigDecimal> result = entityManager.createNativeQuery(sql).getResultList();

        Integer valor = Integer.parseInt(result.get(0).toString());

        return valor;
    }

    public void updateOrdem(VSdReservasPedidos reservaUp, VSdReservasPedidos reservaDown) {
        String sql = "UPDATE SD_RESERVA_PEDIDO_001 SET ORDEM = ?" +
                " WHERE" +
                "     PEDIDO = ?" +
                "     AND RESERVA = ?" +
                "     AND CODIGO = ?" +
                "     AND COR = ?" +
                "     AND TAM = ?";

        // Atualiza o registro para subir
        entityManager.createNativeQuery(sql)
                .setParameter(1, reservaUp.getOrdem())
                .setParameter(2, reservaUp.getNumero())
                .setParameter(3, reservaUp.getReserva())
                .setParameter(4, reservaUp.getCodigo())
                .setParameter(5, reservaUp.getCor())
                .setParameter(6, reservaUp.getTam());

        // Atualiza o registro para descr
        entityManager.createNativeQuery(sql)
                .setParameter(1, reservaDown.getOrdem())
                .setParameter(2, reservaDown.getNumero())
                .setParameter(3, reservaDown.getReserva())
                .setParameter(4, reservaDown.getCodigo())
                .setParameter(5, reservaDown.getCor())
                .setParameter(6, reservaDown.getTam());

    }

}
