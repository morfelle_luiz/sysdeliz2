package sysdeliz2.daos.generics.interfaces;

import javafx.collections.ObservableList;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.utils.enums.PredicateType;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Interface genérica que irá definir todos os métodos que devem ser implementados pelos dao (Data Object Acces) que serão implementações deste escopo.
 * <p>
 * As classes filhas devem implementar somente os métodos que não são definidas aqui, ou alguma sobrecarga(Override) necessária ao método para seu funcionamento
 * com o dao especifico;
 *
 * @param <T> Classe pojo que será gerenciada
 * @author lima.joao
 * @since 11/07/2019 16:33
 */
public interface GenericDao<T> {

    /**
     * @return lista de objetos
     */
    ObservableList<T> list() throws SQLException;
    
    ObservableList<T> list(Integer limit) throws SQLException;
    
    /**
     * @return lista de objetos
     */
    List<T> simpleList() throws SQLException;

    /**
     * @param entity entidade que será persistida
     */
    T save(T entity) throws SQLException;

    /**
     * @param entity entidade que será persistida
     */
    T save(T entity, Boolean isPersist) throws SQLException;


    ObservableList<T> saveList(ObservableList<T> entities) throws SQLException;

    /**
     * Interface generica para atualizar as informações do objeto passado.
     * O Objeto já deve existir no banco de dados, e este método sobreescreverá
     * alterações efetuadas no objeto para os dados que estão no banco de dados;
     *
     * @param entity entidade que será atualizada
     */
    T update(T entity) throws SQLException;

    /**
     * @param id da entity que será removida
     */
    void delete(Serializable id) throws SQLException;

    void delete(T entity) throws SQLException;

    /**
     * @param id da entidade que esta sendo pesquisada
     * @return entidade encontrada
     */
    T load(Serializable id) throws SQLException;

    /**
     * Método sem retorno que apenas inicializa a critéria para habilitar as consultas.
     */
    GenericDao initCriteria();

    /**
     * Método para carregar um entidades referente ao predicate informado na especialização.
     */
    T loadEntityByPredicate() throws SQLException;

    /**
     * Método para carregar uma lista de entidades referentes ao predicate informadoa na especialização
     */
    ObservableList<T> loadListByPredicate() throws SQLException;

    /**
     * Método para carregar uma lista de entidades referentes ao predicate informadoa na especialização
     */
    ObservableList<T> loadListByPredicate(Integer maxResults) throws SQLException;

    /**
     * Define o entity manager manualmente.
     */
    void setEntityManager(EntityManager entityManager);

    void refreshEntity(EntityManager entityManager);

    GenericDao<T> addPredicateEq(String fromField, Integer valor);

    GenericDao<T> addPredicateEq(String fromField, Integer valor, boolean or);

    GenericDao<T> addPredicateEq(String fromField, Long valor);

    GenericDao<T> addPredicateEq(String fromField, Long valor, boolean or);

    GenericDao<T> addPredicateEq(String fromField, Boolean valor);

    GenericDao<T> addPredicateEq(String fromField, Boolean valor, boolean or);

    GenericDao<T> addPredicateEq(String fromField, Serializable valor);

    GenericDao<T> addPredicateEq(String fromField, Serializable valor, boolean or);

    GenericDao<T> addPredicateEq(String fromField, String valor);

    GenericDao<T> addPredicateEq(String fromField, String valor, boolean or);

    GenericDao<T> addPredicateNe(String fromField, Integer valor);

    GenericDao<T> addPredicateNe(String fromField, Integer valor, boolean or);

    GenericDao<T> addPredicateNe(String fromField, String valor);

    GenericDao<T> addPredicateNe(String fromField, String valor, boolean or);

    GenericDao<T> addPredicateNe(String fromField, Double valor);

    GenericDao<T> addPredicateNe(String fromField, Double valor, boolean or);

    GenericDao<T> addPredicateEq(String fromField, Class valor);

    GenericDao<T> addPredicateEq(String fromField, Class valor, boolean or);

    GenericDao<T> addPredicateEqCascateField(String fromCascateField, String valor, boolean or);

    GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, String valor, boolean or);

    GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, Integer valor, boolean or);

    GenericDao<T> addPredicateEqPkEmbedded(String fromFieldPkEmbedded, String fromField, Boolean valor, boolean or);

    GenericDao<T> addPredicateNe(String fromField, Class valor);

    GenericDao<T> addPredicateNe(String fromField, Class valor, boolean or);

    GenericDao<T> addPredicateFrom(String fromField, String fromField2);

    GenericDao<T> addPredicateFrom(String fromField, String fromField2, boolean or);

    GenericDao<T> addPredicateIn(String fromField, Object[] valores);

    GenericDao<T> addPredicateInPkEmbedded(String fromFieldPkEmbedded, String fromField, Object[] valores, boolean or);

    GenericDao<T> addPredicateIn(String fromField, Object[] valores, boolean or);

    GenericDao<T> addPredicateNIn(String fromField, Object[] valores);

    GenericDao<T> addPredicateNIn(String fromField, Object[] valores, boolean or);

    GenericDao<T> addPredicateNInPkEmbedded(String fromFieldPkEmbedded, String fromField, Object[] valores, boolean or);

    GenericDao<T> addPredicateLike(String fromField, String valor);

    GenericDao<T> addPredicateLike(String fromField, String valor, boolean or);

    GenericDao<T> addPredicateLike(String fromField, Integer valor);

    GenericDao<T> addPredicateLike(String fromField, Integer valor, boolean or);

    GenericDao<T> addPredicateIsNotNull(String fromField);

    GenericDao<T> addPredicateIsNotNull(String fromField, boolean or);

    GenericDao<T> addPredicateIsNull(String fromField);

    GenericDao<T> addPredicateIsNull(String fromField, boolean or);

    GenericDao<T> addPredicateBetween(String fromField, LocalDateTime begin, LocalDateTime end);

    GenericDao<T> addPredicateBetween(String fromField, LocalDateTime begin, LocalDateTime end, boolean or);

    GenericDao<T> addPredicateBetween(String fromField, LocalDate begin, LocalDate end);

    GenericDao<T> addPredicateBetween(String fromField, LocalDate begin, LocalDate end, boolean or);

    GenericDao<T> addPredicateBetween(String fromFieldBegin, String fromFieldEnd, LocalDateTime value);

    GenericDao<T> addPredicateBetween(String fromFieldBegin, String fromFieldEnd, LocalDateTime value, boolean or);

    /**
     * Greater than predicate
     *
     * @param fromField
     * @param valor
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateGt(String fromField, Object valor);

    /**
     * Greater than predicate with OR clause
     *
     * @param fromField
     * @param valor
     * @param or
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateGt(String fromField, Object valor, boolean or);

    /**
     * Greater than or equal predicate
     *
     * @param fromField
     * @param valor
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateGtE(String fromField, Object valor);

    /**
     * Greater than or equal predicate with OR clause
     *
     * @param fromField
     * @param valor
     * @param or
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateGtE(String fromField, Object valor, boolean or);

    /**
     * Less than predicate
     *
     * @param fromField
     * @param valor
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateLt(String fromField, Object valor);

    /**
     * Less than predicate with OR clause
     *
     * @param fromField
     * @param valor
     * @param or
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateLt(String fromField, Object valor, boolean or);

    /**
     * Less than or equal predicate
     *
     * @param fromField
     * @param valor
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateLtE(String fromField, Object valor);

    /**
     * Less than or equal predicate with OR clause
     *
     * @param fromField
     * @param valor
     * @param or
     * @return GenericDao<T>
     */
    GenericDao<T> addPredicateLtE(String fromField, Object valor, boolean or);

    GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType);

    GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType, boolean or);

    GenericDao<T> addPredicate(String fromField, Object valor, PredicateType predicateType, boolean or, String groupName);
    
    GenericDao<T> addPredicateGroup(String groupName, boolean or, boolean orInterno);
    
    GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object valor, PredicateType predicateType);
    
    GenericDao<T> addPredicateJoin(String joinField, String fromJoinField, Object valor, PredicateType predicateType, boolean or);
    
    GenericDao<T> addPredicate(String fromField, Object[] valor, PredicateType predicateType);
    
    GenericDao<T> addPredicate(String fromField, Object[] valor, PredicateType predicateType, boolean or);
    
    GenericDao<T> addPredicateJoin(String joinField, String fromField, Object[] valor, PredicateType predicateType);

    GenericDao<T> addPredicateJoin(String joinField, String fromField, Object[] valor, PredicateType predicateType, boolean or);

    GenericDao<T> addDistinctValues();
    
    GenericDao<T> addOrderByAsc(String orderField);

    GenericDao<T> addOrderByDesc(String orderField);

    GenericDao<T> addOrderByAsc(String orderIdField, String orderField);

    GenericDao<T> addOrderByDesc(String orderIdField, String orderField);

    GenericDao<T> addOrderByList(List<Ordenacao> ordenacoes);

    GenericDao<T> clearPredicate();

    /**
     * Método para limpeza do Entity Manager
     * Caso a entidade esteja em edição utilizar flushClear()
     */
    void clear();

    GenericDao<T> flushClear();

    GenericDao<T> flush();

    List<Object[]> loadListBySQL(String sql);

}
// Escopo
//
//    fun isEntityValid(entity: T, respostas: MutableList<Resposta>): Boolean
//
//    fun isEntityValid(entity: T, respostas: MutableList<Resposta>, identificacao: String): Boolean
//
//    fun <T : Any> updateFieldWithDateTime(entity: T, fieldName: String): Boolean
//
//    fun <T : Any> updateFieldWithDateTime(entity: T, fieldName: String, localDateTime: LocalDateTime): Boolean
//}
