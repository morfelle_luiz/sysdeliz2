package sysdeliz2.daos.generics.interfaces;

import javafx.collections.ObservableList;
import sysdeliz2.models.view.VSdReservasPedidos;

/**
 * @author lima.joao
 * @since 29/10/2019 10:00
 */
public interface ReservaPedidoDao extends GenericDao<VSdReservasPedidos> {

    String addSdReservaPedido(VSdReservasPedidos reserva);

    ObservableList<VSdReservasPedidos> carregaPorUsuarioLeitor(String usuario, String[] status);

    Integer getLastOrdem(String nome);

    void updateOrdem(VSdReservasPedidos reservaUp, VSdReservasPedidos reservaDown);

}
