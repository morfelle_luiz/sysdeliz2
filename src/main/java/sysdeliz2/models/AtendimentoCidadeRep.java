/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class AtendimentoCidadeRep {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty uf = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty classe = new SimpleStringProperty();
    private final IntegerProperty popul = new SimpleIntegerProperty();
    private final IntegerProperty comp_col_atual = new SimpleIntegerProperty();
    private final IntegerProperty comp_col_ant = new SimpleIntegerProperty();
    private final IntegerProperty comp_col_pass = new SimpleIntegerProperty();
    private final IntegerProperty status_comp_col = new SimpleIntegerProperty();
    private final DoubleProperty ipc = new SimpleDoubleProperty();

    public AtendimentoCidadeRep(String codrep, String uf, String cidade, String classe,
            Integer popul, Integer comp_col_atual, Integer comp_col_ant, Integer comp_col_pass,
            Integer comp_col_ref, Double ipc) {
        this.codrep.set(codrep);
        this.uf.set(uf);
        this.cidade.set(cidade);
        this.classe.set(classe);
        this.popul.set(popul);
        this.comp_col_atual.set(comp_col_atual);
        this.comp_col_ant.set(comp_col_ant);
        this.comp_col_pass.set(comp_col_pass);
        this.status_comp_col.set(comp_col_atual > 0 ? 1 : comp_col_ant > 0 ? 2 : comp_col_pass > 0 ? 3 : comp_col_ref > 0 ? 4 : 5);
        this.ipc.set(ipc);
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getUf() {
        return uf.get();
    }

    public final void setUf(String value) {
        uf.set(value);
    }

    public StringProperty ufProperty() {
        return uf;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getClasse() {
        return classe.get();
    }

    public final void setClasse(String value) {
        classe.set(value);
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public final int getPopul() {
        return popul.get();
    }

    public final void setPopul(int value) {
        popul.set(value);
    }

    public IntegerProperty populProperty() {
        return popul;
    }

    public final int getComp_col_atual() {
        return comp_col_atual.get();
    }

    public final void setComp_col_atual(int value) {
        comp_col_atual.set(value);
    }

    public IntegerProperty comp_col_atualProperty() {
        return comp_col_atual;
    }

    public final int getComp_col_ant() {
        return comp_col_ant.get();
    }

    public final void setComp_col_ant(int value) {
        comp_col_ant.set(value);
    }

    public IntegerProperty comp_col_antProperty() {
        return comp_col_ant;
    }

    public final int getComp_col_pass() {
        return comp_col_pass.get();
    }

    public final void setComp_col_pass(int value) {
        comp_col_pass.set(value);
    }

    public IntegerProperty comp_col_passProperty() {
        return comp_col_pass;
    }

    public final int getStatus_comp_col() {
        return status_comp_col.get();
    }

    public final void setStatus_comp_col(int value) {
        status_comp_col.set(value);
    }

    public IntegerProperty status_comp_colProperty() {
        return status_comp_col;
    }

    public final double getIpc() {
        return ipc.get();
    }

    public final void setIpc(double value) {
        ipc.set(value);
    }

    public DoubleProperty ipcProperty() {
        return ipc;
    }

    @Override
    public String toString() {
        return "AtendimentoCidadeRep{" + "codrep=" + codrep + ", uf=" + uf + ", cidade=" + cidade + ", classe=" + classe + ", popul=" + popul + ", comp_col_atual=" + comp_col_atual + ", comp_col_ant=" + comp_col_ant + ", comp_col_pass=" + comp_col_pass + ", status_comp_col=" + status_comp_col + ", ipc=" + ipc + '}';
    }

}
