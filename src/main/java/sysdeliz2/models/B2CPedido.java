package sysdeliz2.models;

import javafx.beans.property.*;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "B2C_PEDIDO_001")
public class B2CPedido implements Serializable {
    private StringProperty numero = new SimpleStringProperty();
    private StringProperty numeroErp = new SimpleStringProperty();
    private IntegerProperty codStatus = new SimpleIntegerProperty();
    private StringProperty codTransporte = new SimpleStringProperty();
    private StringProperty codRastreio = new SimpleStringProperty();
    private StringProperty codMarca = new SimpleStringProperty();
    private StringProperty codcli = new SimpleStringProperty();
    private StringProperty idMagento = new SimpleStringProperty();
    private List<PedIten> itens = new ArrayList<>();

    public B2CPedido() {
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "NUMERO_ERP")
    public String getNumeroErp() {
        return numeroErp.get();
    }

    public StringProperty numeroErpProperty() {
        return numeroErp;
    }

    public void setNumeroErp(String numeroErp) {
        this.numeroErp.set(numeroErp);
    }

    @Column(name = "COD_STATUS")
    public int getCodStatus() {
        return codStatus.get();
    }

    public IntegerProperty codStatusProperty() {
        return codStatus;
    }

    public void setCodStatus(int codStatus) {
        this.codStatus.set(codStatus);
    }

    @Column(name = "COD_TRANSPORTE")
    public String getCodTransporte() {
        return codTransporte.get();
    }

    public StringProperty codTransporteProperty() {
        return codTransporte;
    }

    public void setCodTransporte(String codTransporte) {
        this.codTransporte.set(codTransporte);
    }

    @Column(name = "COD_RASTREIO")
    public String getCodRastreio() {
        return codRastreio.get();
    }

    public StringProperty codRastreioProperty() {
        return codRastreio;
    }

    public void setCodRastreio(String codRastreio) {
        this.codRastreio.set(codRastreio);
    }

    @Column(name = "COD_MARCA")
    public String getCodMarca() {
        return codMarca.get();
    }

    public StringProperty codMarcaProperty() {
        return codMarca;
    }

    public void setCodMarca(String codMarca) {
        this.codMarca.set(codMarca);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "ID_MAGENTO")
    public String getIdMagento() {
        return idMagento.get();
    }

    public StringProperty idMagentoProperty() {
        return idMagento;
    }

    public void setIdMagento(String idMagento) {
        this.idMagento.set(idMagento);
    }

    @OneToMany
    @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO_ERP")
    public List<PedIten> getItens() {
        return itens;
    }

    public void setItens(List<PedIten> itens) {
        this.itens = itens;
    }
}
