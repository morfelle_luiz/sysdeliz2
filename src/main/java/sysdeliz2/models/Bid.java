/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Bid {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty nome_uf = new SimpleStringProperty();
    private final StringProperty uf_rep = new SimpleStringProperty();
    private final StringProperty regiao = new SimpleStringProperty();
    private final StringProperty gerente = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty codmar = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty codcol = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final StringProperty email_rep = new SimpleStringProperty();
    private final StringProperty email_ger = new SimpleStringProperty();
    private final IntegerProperty mostr = new SimpleIntegerProperty();
    private final DoubleProperty pm_rcm = new SimpleDoubleProperty();
    private final DoubleProperty pm_rpm = new SimpleDoubleProperty();
    private final DoubleProperty pm_mta = new SimpleDoubleProperty();
    private final DoubleProperty pm_per = new SimpleDoubleProperty();
    private final DoubleProperty pm_ly = new SimpleDoubleProperty();
    private final IntegerProperty cd_rcm = new SimpleIntegerProperty();
    private final IntegerProperty cd_rpm = new SimpleIntegerProperty();
    private final IntegerProperty cd_mta = new SimpleIntegerProperty();
    private final DoubleProperty cd_per = new SimpleDoubleProperty();
    private final IntegerProperty cd_ly = new SimpleIntegerProperty();
    private final IntegerProperty pv_rcm = new SimpleIntegerProperty();
    private final IntegerProperty pv_rpm = new SimpleIntegerProperty();
    private final IntegerProperty pv_mta = new SimpleIntegerProperty();
    private final DoubleProperty pv_per = new SimpleDoubleProperty();
    private final IntegerProperty pv_ly = new SimpleIntegerProperty();
    private final IntegerProperty ka_rcm = new SimpleIntegerProperty();
    private final IntegerProperty ka_rpm = new SimpleIntegerProperty();
    private final IntegerProperty ka_mta = new SimpleIntegerProperty();
    private final DoubleProperty ka_per = new SimpleDoubleProperty();
    private final IntegerProperty ka_ly = new SimpleIntegerProperty();
    private final IntegerProperty pp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty pp_rpm = new SimpleIntegerProperty();
    private final IntegerProperty pp_mta = new SimpleIntegerProperty();
    private final DoubleProperty pp_per = new SimpleDoubleProperty();
    private final IntegerProperty pp_ly = new SimpleIntegerProperty();
    private final DoubleProperty vp_rcm = new SimpleDoubleProperty();
    private final DoubleProperty vp_rpm = new SimpleDoubleProperty();
    private final DoubleProperty vp_mta = new SimpleDoubleProperty();
    private final DoubleProperty vp_per = new SimpleDoubleProperty();
    private final DoubleProperty vp_ly = new SimpleDoubleProperty();
    private final IntegerProperty tp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty tp_rpm = new SimpleIntegerProperty();
    private final IntegerProperty tp_mta = new SimpleIntegerProperty();
    private final DoubleProperty tp_per = new SimpleDoubleProperty();
    private final IntegerProperty tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty tf_rcm = new SimpleDoubleProperty();
    private final DoubleProperty tf_rpm = new SimpleDoubleProperty();
    private final DoubleProperty tf_mta = new SimpleDoubleProperty();
    private final DoubleProperty tf_per = new SimpleDoubleProperty();
    private final DoubleProperty tf_ly = new SimpleDoubleProperty();
    private final DoubleProperty pg_rcm = new SimpleDoubleProperty();
    private final DoubleProperty pg_rpm = new SimpleDoubleProperty();
    private final DoubleProperty pg_mta = new SimpleDoubleProperty();
    private final DoubleProperty pg_per = new SimpleDoubleProperty();
    private final DoubleProperty pg_ly = new SimpleDoubleProperty();
    private final DoubleProperty pr_rcm = new SimpleDoubleProperty();
    private final DoubleProperty pr_rpm = new SimpleDoubleProperty();
    private final DoubleProperty pr_mta = new SimpleDoubleProperty();
    private final DoubleProperty pr_per = new SimpleDoubleProperty();
    private final DoubleProperty pr_ly = new SimpleDoubleProperty();
    private final IntegerProperty cln_rcm = new SimpleIntegerProperty();
    private final IntegerProperty cln_mta = new SimpleIntegerProperty();
    private final DoubleProperty cln_per = new SimpleDoubleProperty();
    private final IntegerProperty bas_tp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty bas_tp_mta = new SimpleIntegerProperty();
    private final DoubleProperty per_bas_tp = new SimpleDoubleProperty();
    private final DoubleProperty bas_tf_rcm = new SimpleDoubleProperty();
    private final IntegerProperty den_tp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty den_tp_mta = new SimpleIntegerProperty();
    private final DoubleProperty per_den_tp = new SimpleDoubleProperty();
    private final DoubleProperty den_tf_rcm = new SimpleDoubleProperty();
    private final IntegerProperty col_tp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty col_tp_mta = new SimpleIntegerProperty();
    private final DoubleProperty per_col_tp = new SimpleDoubleProperty();
    private final DoubleProperty col_tf_rcm = new SimpleDoubleProperty();
    private final IntegerProperty cas_tp_rcm = new SimpleIntegerProperty();
    private final IntegerProperty cas_tp_mta = new SimpleIntegerProperty();
    private final DoubleProperty per_cas_tp = new SimpleDoubleProperty();
    private final DoubleProperty cas_tf_rcm = new SimpleDoubleProperty();
    private final IntegerProperty sem_total = new SimpleIntegerProperty();
    private final IntegerProperty sem_hoje = new SimpleIntegerProperty();

    private final IntegerProperty bas_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty bas_tf_ly = new SimpleDoubleProperty();
    private final IntegerProperty den_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty den_tf_ly = new SimpleDoubleProperty();
    private final IntegerProperty col_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty col_tf_ly = new SimpleDoubleProperty();
    private final IntegerProperty cas_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty cas_tf_ly = new SimpleDoubleProperty();

    private final IntegerProperty wis_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty wis_tf_ly = new SimpleDoubleProperty();
    private final IntegerProperty cam_tp_ly = new SimpleIntegerProperty();
    private final DoubleProperty cam_tf_ly = new SimpleDoubleProperty();

    private final IntegerProperty wis_tp_mta = new SimpleIntegerProperty();
    private final IntegerProperty cam_tp_mta = new SimpleIntegerProperty();

    private final IntegerProperty cln_att_ly = new SimpleIntegerProperty();

    private final ListProperty<AtendimentoCidadeRep> cidades = new SimpleListProperty<>();
    private final ListProperty<BidPremiado> bid_premiado = new SimpleListProperty<>();

    public Bid(String codrep, String represent, String nome, String uf_rep, String regiao, String gerente, String marca, String codmar, String colecao,
            String codcol, String familia, int mostr, double pm_rcm, double pm_rpm, double pm_mta, int cd_rcm, int cd_rpm, int cd_mta, int pv_rcm, int pv_rpm, int pv_mta,
            int ka_rcm, int ka_rpm, int ka_mta, int pp_rcm, int pp_rpm, int pp_mta, double vp_rcm, double vp_rpm, double vp_mta, int tp_rcm, int tp_rpm,
            int tp_mta, double tf_rcm, double tf_rpm, double tf_mta, double pg_rcm, double pg_rpm, double pg_mta, double pr_rcm, double pr_rpm, double pr_mta,
            int cln_rcm, int cln_mta, int bas_tp_rcm, int bas_tp_mta, double per_bas_tp, double bas_tf_rcm, int den_tp_rcm, int den_tp_mta, double per_den_tp,
            double den_tf_rcm, int col_tp_rcm, int col_tp_mta, double per_col_tp, double col_tf_rcm, int cas_tp_rcm, int cas_tp_mta, double per_cas_tp, double cas_rs_rcm,
            int sem_total, int sem_hoje, String email_rep, String email_ger, double pm_ly, int cd_ly, int pv_ly, int ka_ly, int pp_ly, double vp_ly, int tp_ly, double tf_ly, double pg_ly, double pr_ly,
            int ly_bas_tp, double ly_bas_tf, int ly_den_tp, double ly_den_tf, int ly_col_tp, double ly_col_tf, int ly_cas_tp, double ly_cas_tf, int ly_cln_att,
            int ly_wis_tp, double ly_wis_tf, int ly_cam_tp, double ly_cam_tf, int wis_tp_mta, int cam_tp_mta
    ) {
        this.codrep.set(codrep);
        this.represent.set(represent);
        this.nome.set(nome);
        this.uf_rep.set(uf_rep);
        this.regiao.set(regiao);
        this.gerente.set(gerente);
        this.marca.set(marca);
        this.codmar.set(codmar);
        this.colecao.set(colecao);
        this.codcol.set(codcol);
        this.familia.set(familia);
        this.mostr.set(mostr);
        this.pm_rcm.set(pm_rcm);
        this.pm_rpm.set(pm_rpm);
        this.pm_mta.set(pm_mta);
        this.pm_per.set(pm_mta == 0 ? 0.0 : new BigDecimal(pm_rcm / pm_mta * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.cd_rcm.set(cd_rcm);
        this.cd_rpm.set(cd_rpm);
        this.cd_mta.set(cd_mta);
        this.cd_per.set(cd_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(cd_rcm) / Double.valueOf(cd_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.pv_rcm.set(pv_rcm);
        this.pv_rpm.set(pv_rpm);
        this.pv_mta.set(pv_mta);
        this.pv_per.set(pv_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(pv_rcm) / Double.valueOf(pv_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.ka_rcm.set(ka_rcm);
        this.ka_rpm.set(ka_rpm);
        this.ka_mta.set(ka_mta);
        this.ka_per.set(ka_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(ka_rcm) / Double.valueOf(ka_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.pp_rcm.set(pp_rcm);
        this.pp_rpm.set(pp_rpm);
        this.pp_mta.set(pp_mta);
        this.pp_per.set(pp_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(pp_rcm) / Double.valueOf(pp_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.vp_rcm.set(vp_rcm);
        this.vp_rpm.set(vp_rpm);
        this.vp_mta.set(vp_mta);
        this.vp_per.set(vp_mta == 0 ? 0.0 : new BigDecimal(vp_rcm / vp_mta * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.tp_rcm.set(tp_rcm);
        this.tp_rpm.set(tp_rpm);
        this.tp_mta.set(tp_mta);
        this.tp_per.set(tp_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(tp_rcm) / Double.valueOf(tp_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.tf_rcm.set(tf_rcm);
        this.tf_rpm.set(tf_rpm);
        this.tf_mta.set(tf_mta);
        this.tf_per.set(tf_mta == 0 ? 0.0 : new BigDecimal(tf_rcm / tf_mta * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.pg_rcm.set(pg_rcm);
        this.pg_rpm.set(pg_rpm);
        this.pg_mta.set(pg_mta);
        this.pg_per.set(pg_mta == 0 ? 0.0 : new BigDecimal(pg_rcm / pg_mta * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.pr_rcm.set(pr_rcm);
        this.pr_rpm.set(pr_rpm);
        this.pr_mta.set(pr_mta);
        this.pr_per.set(pr_mta == 0 ? 0.0 : new BigDecimal(pr_rcm / (pr_mta * 100.0) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.cln_rcm.set(cln_rcm);
        this.cln_mta.set(cln_mta);
        this.cln_per.set(cln_mta == 0 ? 0.0 : new BigDecimal(Double.valueOf(cln_rcm) / Double.valueOf(cln_mta) * Double.valueOf("100.0")).setScale(2, RoundingMode.UP).doubleValue());
        this.bas_tp_rcm.set(bas_tp_rcm);
        this.bas_tp_mta.set(bas_tp_mta);
        this.per_bas_tp.set(per_bas_tp);
        this.bas_tf_rcm.set(bas_tf_rcm);
        this.den_tp_rcm.set(den_tp_rcm);
        this.den_tp_mta.set(den_tp_mta);
        this.per_den_tp.set(per_den_tp);
        this.den_tf_rcm.set(den_tf_rcm);
        this.col_tp_rcm.set(col_tp_rcm);
        this.col_tp_mta.set(col_tp_mta);
        this.per_col_tp.set(per_col_tp);
        this.col_tf_rcm.set(col_tf_rcm);
        this.cas_tp_rcm.set(cas_tp_rcm);
        this.cas_tp_mta.set(cas_tp_mta);
        this.per_cas_tp.set(per_cas_tp);
        this.cas_tf_rcm.set(cas_rs_rcm);
        this.nome_uf.set(nome + "/" + uf_rep);
        this.sem_total.set(sem_total);
        this.sem_hoje.set(sem_hoje);
        this.email_rep.set(email_rep);
        this.email_ger.set(email_ger);
        this.pm_ly.set(pm_ly);
        this.cd_ly.set(cd_ly);
        this.pv_ly.set(pv_ly);
        this.ka_ly.set(ka_ly);
        this.pp_ly.set(pp_ly);
        this.vp_ly.set(vp_ly);
        this.tp_ly.set(tp_ly);
        this.tf_ly.set(tf_ly);
        this.pg_ly.set(pg_ly);
        this.pr_ly.set(pr_ly);
        this.bas_tp_ly.set(ly_bas_tp);
        this.bas_tf_ly.set(ly_bas_tf);
        this.den_tp_ly.set(ly_den_tp);
        this.den_tf_ly.set(ly_den_tf);
        this.col_tp_ly.set(ly_col_tp);
        this.col_tf_ly.set(ly_col_tf);
        this.cas_tp_ly.set(ly_cas_tp);
        this.cas_tf_ly.set(ly_cas_tf);
        this.wis_tp_ly.set(ly_wis_tp);
        this.wis_tf_ly.set(ly_wis_tf);
        this.wis_tp_mta.set(wis_tp_mta);
        this.cam_tp_ly.set(ly_cam_tp);
        this.cam_tf_ly.set(ly_cam_tf);
        this.cam_tp_mta.set(cam_tp_mta);
        this.cln_att_ly.set(ly_cln_att);
    }

    public final double getPm_ly() {
        return pm_ly.get();
    }

    public final void setPm_ly(double value) {
        pm_ly.set(value);
    }

    public DoubleProperty pm_lyProperty() {
        return pm_ly;
    }

    public final int getCd_ly() {
        return cd_ly.get();
    }

    public final void setCd_ly(int value) {
        cd_ly.set(value);
    }

    public IntegerProperty cd_lyProperty() {
        return cd_ly;
    }

    public final int getPv_ly() {
        return pv_ly.get();
    }

    public final void setPv_ly(int value) {
        pv_ly.set(value);
    }

    public IntegerProperty pv_lyProperty() {
        return pv_ly;
    }

    public final int getKa_ly() {
        return ka_ly.get();
    }

    public final void setKa_ly(int value) {
        ka_ly.set(value);
    }

    public IntegerProperty ka_lyProperty() {
        return ka_ly;
    }

    public final int getPp_ly() {
        return pp_ly.get();
    }

    public final void setPp_ly(int value) {
        pp_ly.set(value);
    }

    public IntegerProperty pp_lyProperty() {
        return pp_ly;
    }

    public final double getVp_ly() {
        return vp_ly.get();
    }

    public final void setVp_ly(double value) {
        vp_ly.set(value);
    }

    public DoubleProperty vp_lyProperty() {
        return vp_ly;
    }

    public final int getTp_ly() {
        return tp_ly.get();
    }

    public final void setTp_ly(int value) {
        tp_ly.set(value);
    }

    public IntegerProperty tp_lyProperty() {
        return tp_ly;
    }

    public final double getTf_ly() {
        return tf_ly.get();
    }

    public final void setTf_ly(double value) {
        tf_ly.set(value);
    }

    public DoubleProperty tf_lyProperty() {
        return tf_ly;
    }

    public final double getPg_ly() {
        return pg_ly.get();
    }

    public final void setPg_ly(double value) {
        pg_ly.set(value);
    }

    public DoubleProperty pg_lyProperty() {
        return pg_ly;
    }

    public final double getPr_ly() {
        return pr_ly.get();
    }

    public final void setPr_ly(double value) {
        pr_ly.set(value);
    }

    public DoubleProperty pr_lyProperty() {
        return pr_ly;
    }

    public final String getEmail_rep() {
        return email_rep.get();
    }

    public final void setEmail_rep(String value) {
        email_rep.set(value);
    }

    public StringProperty email_repProperty() {
        return email_rep;
    }

    public final String getEmail_ger() {
        return email_ger.get();
    }

    public final void setEmail_ger(String value) {
        email_ger.set(value);
    }

    public StringProperty email_gerProperty() {
        return email_ger;
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getRepresent() {
        return represent.get();
    }

    public final void setRepresent(String value) {
        represent.set(value);
    }

    public StringProperty representProperty() {
        return represent;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getUf_rep() {
        return uf_rep.get();
    }

    public final void setUf_rep(String value) {
        uf_rep.set(value);
    }

    public StringProperty uf_repProperty() {
        return uf_rep;
    }

    public final String getRegiao() {
        return regiao.get();
    }

    public final void setRegiao(String value) {
        regiao.set(value);
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public final String getGerente() {
        return gerente.get();
    }

    public final void setGerente(String value) {
        gerente.set(value);
    }

    public StringProperty gerenteProperty() {
        return gerente;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final String getCodmar() {
        return codmar.get();
    }

    public final void setCodmar(String value) {
        codmar.set(value);
    }

    public StringProperty codmarProperty() {
        return codmar;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getCodcol() {
        return codcol.get();
    }

    public final void setCodcol(String value) {
        codcol.set(value);
    }

    public StringProperty codcolProperty() {
        return codcol;
    }

    public final int getMostr() {
        return mostr.get();
    }

    public final void setMostr(int value) {
        mostr.set(value);
    }

    public IntegerProperty mostrProperty() {
        return mostr;
    }

    public final double getPm_rcm() {
        return pm_rcm.get();
    }

    public final void setPm_rcm(double value) {
        pm_rcm.set(value);
    }

    public DoubleProperty pm_rcmProperty() {
        return pm_rcm;
    }

    public final double getPm_rpm() {
        return pm_rpm.get();
    }

    public final void setPm_rpm(double value) {
        pm_rpm.set(value);
    }

    public DoubleProperty pm_rpmProperty() {
        return pm_rpm;
    }

    public final double getPm_mta() {
        return pm_mta.get();
    }

    public final void setPm_mta(double value) {
        pm_mta.set(value);
    }

    public DoubleProperty pm_mtaProperty() {
        return pm_mta;
    }

    public final double getPm_per() {
        return pm_per.get();
    }

    public final void setPm_per(double value) {
        pm_per.set(value);
    }

    public DoubleProperty pm_perProperty() {
        return pm_per;
    }

    public final int getCd_rcm() {
        return cd_rcm.get();
    }

    public final void setCd_rcm(int value) {
        cd_rcm.set(value);
    }

    public IntegerProperty cd_rcmProperty() {
        return cd_rcm;
    }

    public final int getCd_rpm() {
        return cd_rpm.get();
    }

    public final void setCd_rpm(int value) {
        cd_rpm.set(value);
    }

    public IntegerProperty cd_rpmProperty() {
        return cd_rpm;
    }

    public final int getCd_mta() {
        return cd_mta.get();
    }

    public final void setCd_mta(int value) {
        cd_mta.set(value);
    }

    public IntegerProperty cd_mtaProperty() {
        return cd_mta;
    }

    public final double getCd_per() {
        return cd_per.get();
    }

    public final void setCd_per(double value) {
        cd_per.set(value);
    }

    public DoubleProperty cd_perProperty() {
        return cd_per;
    }

    public final int getPv_rcm() {
        return pv_rcm.get();
    }

    public final void setPv_rcm(int value) {
        pv_rcm.set(value);
    }

    public IntegerProperty pv_rcmProperty() {
        return pv_rcm;
    }

    public final int getPv_rpm() {
        return pv_rpm.get();
    }

    public final void setPv_rpm(int value) {
        pv_rpm.set(value);
    }

    public IntegerProperty pv_rpmProperty() {
        return pv_rpm;
    }

    public final int getPv_mta() {
        return pv_mta.get();
    }

    public final void setPv_mta(int value) {
        pv_mta.set(value);
    }

    public IntegerProperty pv_mtaProperty() {
        return pv_mta;
    }

    public final double getPv_per() {
        return pv_per.get();
    }

    public final void setPv_per(double value) {
        pv_per.set(value);
    }

    public DoubleProperty pv_perProperty() {
        return pv_per;
    }

    public final int getKa_rcm() {
        return ka_rcm.get();
    }

    public final void setKa_rcm(int value) {
        ka_rcm.set(value);
    }

    public IntegerProperty ka_rcmProperty() {
        return ka_rcm;
    }

    public final int getKa_rpm() {
        return ka_rpm.get();
    }

    public final void setKa_rpm(int value) {
        ka_rpm.set(value);
    }

    public IntegerProperty ka_rpmProperty() {
        return ka_rpm;
    }

    public final int getKa_mta() {
        return ka_mta.get();
    }

    public final void setKa_mta(int value) {
        ka_mta.set(value);
    }

    public IntegerProperty ka_mtaProperty() {
        return ka_mta;
    }

    public final double getKa_per() {
        return ka_per.get();
    }

    public final void setKa_per(double value) {
        ka_per.set(value);
    }

    public DoubleProperty ka_perProperty() {
        return ka_per;
    }

    public final int getPp_rcm() {
        return pp_rcm.get();
    }

    public final void setPp_rcm(int value) {
        pp_rcm.set(value);
    }

    public IntegerProperty pp_rcmProperty() {
        return pp_rcm;
    }

    public final int getPp_rpm() {
        return pp_rpm.get();
    }

    public final void setPp_rpm(int value) {
        pp_rpm.set(value);
    }

    public IntegerProperty pp_rpmProperty() {
        return pp_rpm;
    }

    public final int getPp_mta() {
        return pp_mta.get();
    }

    public final void setPp_mta(int value) {
        pp_mta.set(value);
    }

    public IntegerProperty pp_mtaProperty() {
        return pp_mta;
    }

    public final double getPp_per() {
        return pp_per.get();
    }

    public final void setPp_per(double value) {
        pp_per.set(value);
    }

    public DoubleProperty pp_perProperty() {
        return pp_per;
    }

    public final double getVp_rcm() {
        return vp_rcm.get();
    }

    public final void setVp_rcm(double value) {
        vp_rcm.set(value);
    }

    public DoubleProperty vp_rcmProperty() {
        return vp_rcm;
    }

    public final double getVp_rpm() {
        return vp_rpm.get();
    }

    public final void setVp_rpm(double value) {
        vp_rpm.set(value);
    }

    public DoubleProperty vp_rpmProperty() {
        return vp_rpm;
    }

    public final double getVp_mta() {
        return vp_mta.get();
    }

    public final void setVp_mta(double value) {
        vp_mta.set(value);
    }

    public DoubleProperty vp_mtaProperty() {
        return vp_mta;
    }

    public final double getVp_per() {
        return vp_per.get();
    }

    public final void setVp_per(double value) {
        vp_per.set(value);
    }

    public DoubleProperty vp_perProperty() {
        return vp_per;
    }

    public final int getTp_rcm() {
        return tp_rcm.get();
    }

    public final void setTp_rcm(int value) {
        tp_rcm.set(value);
    }

    public IntegerProperty tp_rcmProperty() {
        return tp_rcm;
    }

    public final int getTp_rpm() {
        return tp_rpm.get();
    }

    public final void setTp_rpm(int value) {
        tp_rpm.set(value);
    }

    public IntegerProperty tp_rpmProperty() {
        return tp_rpm;
    }

    public final int getTp_mta() {
        return tp_mta.get();
    }

    public final void setTp_mta(int value) {
        tp_mta.set(value);
    }

    public IntegerProperty tp_mtaProperty() {
        return tp_mta;
    }

    public final double getTp_per() {
        return tp_per.get();
    }

    public final void setTp_per(double value) {
        tp_per.set(value);
    }

    public DoubleProperty tp_perProperty() {
        return tp_per;
    }

    public final double getTf_rcm() {
        return tf_rcm.get();
    }

    public final void setTf_rcm(double value) {
        tf_rcm.set(value);
    }

    public DoubleProperty tf_rcmProperty() {
        return tf_rcm;
    }

    public final double getTf_rpm() {
        return tf_rpm.get();
    }

    public final void setTf_rpm(double value) {
        tf_rpm.set(value);
    }

    public DoubleProperty tf_rpmProperty() {
        return tf_rpm;
    }

    public final double getTf_mta() {
        return tf_mta.get();
    }

    public final void setTf_mta(double value) {
        tf_mta.set(value);
    }

    public DoubleProperty tf_mtaProperty() {
        return tf_mta;
    }

    public final double getTf_per() {
        return tf_per.get();
    }

    public final void setTf_per(double value) {
        tf_per.set(value);
    }

    public DoubleProperty tf_perProperty() {
        return tf_per;
    }

    public final double getPg_rcm() {
        return pg_rcm.get();
    }

    public final void setPg_rcm(double value) {
        pg_rcm.set(value);
    }

    public DoubleProperty pg_rcmProperty() {
        return pg_rcm;
    }

    public final double getPg_rpm() {
        return pg_rpm.get();
    }

    public final void setPg_rpm(double value) {
        pg_rpm.set(value);
    }

    public DoubleProperty pg_rpmProperty() {
        return pg_rpm;
    }

    public final double getPg_mta() {
        return pg_mta.get();
    }

    public final void setPg_mta(double value) {
        pg_mta.set(value);
    }

    public DoubleProperty pg_mtaProperty() {
        return pg_mta;
    }

    public final double getPg_per() {
        return pg_per.get();
    }

    public final void setPg_per(double value) {
        pg_per.set(value);
    }

    public DoubleProperty pg_perProperty() {
        return pg_per;
    }

    public final double getPr_rcm() {
        return pr_rcm.get();
    }

    public final void setPr_rcm(double value) {
        pr_rcm.set(value);
    }

    public DoubleProperty pr_rcmProperty() {
        return pr_rcm;
    }

    public final double getPr_rpm() {
        return pr_rpm.get();
    }

    public final void setPr_rpm(double value) {
        pr_rpm.set(value);
    }

    public DoubleProperty pr_rpmProperty() {
        return pr_rpm;
    }

    public final double getPr_mta() {
        return pr_mta.get();
    }

    public final void setPr_mta(double value) {
        pr_mta.set(value);
    }

    public DoubleProperty pr_mtaProperty() {
        return pr_mta;
    }

    public final double getPr_per() {
        return pr_per.get();
    }

    public final void setPr_per(double value) {
        pr_per.set(value);
    }

    public DoubleProperty pr_perProperty() {
        return pr_per;
    }

    public final int getCln_rcm() {
        return cln_rcm.get();
    }

    public final void setCln_rcm(int value) {
        cln_rcm.set(value);
    }

    public IntegerProperty cln_rcmProperty() {
        return cln_rcm;
    }

    public final int getCln_mta() {
        return cln_mta.get();
    }

    public final void setCln_mta(int value) {
        cln_mta.set(value);
    }

    public IntegerProperty cln_mtaProperty() {
        return cln_mta;
    }

    public final double getCln_per() {
        return cln_per.get();
    }

    public final void setCln_per(double value) {
        cln_per.set(value);
    }

    public DoubleProperty cln_perProperty() {
        return cln_per;
    }

    public final int getBas_tp_rcm() {
        return bas_tp_rcm.get();
    }

    public final void setBas_tp_rcm(int value) {
        bas_tp_rcm.set(value);
    }

    public IntegerProperty bas_tp_rcmProperty() {
        return bas_tp_rcm;
    }

    public final int getBas_tp_mta() {
        return bas_tp_mta.get();
    }

    public final void setBas_tp_mta(int value) {
        bas_tp_mta.set(value);
    }

    public IntegerProperty bas_tp_mtaProperty() {
        return bas_tp_mta;
    }

    public final double getPer_bas_tp() {
        return per_bas_tp.get();
    }

    public final void setPer_bas_tp(double value) {
        per_bas_tp.set(value);
    }

    public DoubleProperty per_bas_tpProperty() {
        return per_bas_tp;
    }

    public final double getBas_tf_rcm() {
        return bas_tf_rcm.get();
    }

    public final void setBas_tf_rcm(double value) {
        bas_tf_rcm.set(value);
    }

    public DoubleProperty bas_tf_rcmProperty() {
        return bas_tf_rcm;
    }

    public final int getDen_tp_rcm() {
        return den_tp_rcm.get();
    }

    public final void setDen_tp_rcm(int value) {
        den_tp_rcm.set(value);
    }

    public IntegerProperty den_tp_rcmProperty() {
        return den_tp_rcm;
    }

    public final int getDen_tp_mta() {
        return den_tp_mta.get();
    }

    public final void setDen_tp_mta(int value) {
        den_tp_mta.set(value);
    }

    public IntegerProperty den_tp_mtaProperty() {
        return den_tp_mta;
    }

    public final double getPer_den_tp() {
        return per_den_tp.get();
    }

    public final void setPer_den_tp(double value) {
        per_den_tp.set(value);
    }

    public DoubleProperty per_den_tpProperty() {
        return per_den_tp;
    }

    public final double getDen_tf_rcm() {
        return den_tf_rcm.get();
    }

    public final void setDen_tf_rcm(double value) {
        den_tf_rcm.set(value);
    }

    public DoubleProperty den_tf_rcmProperty() {
        return den_tf_rcm;
    }

    public final int getCol_tp_rcm() {
        return col_tp_rcm.get();
    }

    public final void setCol_tp_rcm(int value) {
        col_tp_rcm.set(value);
    }

    public IntegerProperty col_tp_rcmProperty() {
        return col_tp_rcm;
    }

    public final int getCol_tp_mta() {
        return col_tp_mta.get();
    }

    public final void setCol_tp_mta(int value) {
        col_tp_mta.set(value);
    }

    public IntegerProperty col_tp_mtaProperty() {
        return col_tp_mta;
    }

    public final double getPer_col_tp() {
        return per_col_tp.get();
    }

    public final void setPer_col_tp(double value) {
        per_col_tp.set(value);
    }

    public DoubleProperty per_col_tpProperty() {
        return per_col_tp;
    }

    public final double getCol_tf_rcm() {
        return col_tf_rcm.get();
    }

    public final void setCol_tf_rcm(double value) {
        col_tf_rcm.set(value);
    }

    public DoubleProperty col_tf_rcmProperty() {
        return col_tf_rcm;
    }

    public final int getCas_tp_rcm() {
        return cas_tp_rcm.get();
    }

    public final void setCas_tp_rcm(int value) {
        cas_tp_rcm.set(value);
    }

    public IntegerProperty cas_tp_rcmProperty() {
        return cas_tp_rcm;
    }

    public final int getCas_tp_mta() {
        return cas_tp_mta.get();
    }

    public final void setCas_tp_mta(int value) {
        cas_tp_mta.set(value);
    }

    public IntegerProperty cas_tp_mtaProperty() {
        return cas_tp_mta;
    }

    public final double getPer_cas_tp() {
        return per_cas_tp.get();
    }

    public final void setPer_cas_tp(double value) {
        per_cas_tp.set(value);
    }

    public DoubleProperty per_cas_tpProperty() {
        return per_cas_tp;
    }

    public final double getCas_tf_rcm() {
        return cas_tf_rcm.get();
    }

    public final void setCas_tf_rcm(double value) {
        cas_tf_rcm.set(value);
    }

    public DoubleProperty cas_tf_rcmProperty() {
        return cas_tf_rcm;
    }

    public final String getNome_uf() {
        return nome_uf.get();
    }

    public final void setNome_uf(String value) {
        nome_uf.set(value);
    }

    public StringProperty nome_ufProperty() {
        return nome_uf;
    }

    public final String getFamilia() {
        return familia.get();
    }

    public final void setFamilia(String value) {
        familia.set(value);
    }

    public StringProperty familiaProperty() {
        return familia;
    }

    public final ObservableList<AtendimentoCidadeRep> getCidades() {
        return cidades.get();
    }

    public final void setCidades(ObservableList<AtendimentoCidadeRep> value) {
        cidades.set(value);
    }

    public ListProperty<AtendimentoCidadeRep> cidadesProperty() {
        return cidades;
    }

    public final ObservableList<BidPremiado> getBid_premiado() {
        return bid_premiado.get();
    }

    public final void setBid_premiado(ObservableList<BidPremiado> value) {
        bid_premiado.set(value);
    }

    public ListProperty<BidPremiado> bid_premiadoProperty() {
        return bid_premiado;
    }

    public final int getSem_total() {
        return sem_total.get();
    }

    public final void setSem_total(int value) {
        sem_total.set(value);
    }

    public IntegerProperty sem_totalProperty() {
        return sem_total;
    }

    public final int getSem_hoje() {
        return sem_hoje.get();
    }

    public final void setSem_hoje(int value) {
        sem_hoje.set(value);
    }

    public IntegerProperty sem_hojeProperty() {
        return sem_hoje;
    }

    public final int getBas_tp_ly() {
        return bas_tp_ly.get();
    }

    public final void setBas_tp_ly(int value) {
        bas_tp_ly.set(value);
    }

    public IntegerProperty bas_tp_lyProperty() {
        return bas_tp_ly;
    }

    public final double getBas_tf_ly() {
        return bas_tf_ly.get();
    }

    public final void setBas_tf_ly(double value) {
        bas_tf_ly.set(value);
    }

    public DoubleProperty bas_tf_lyProperty() {
        return bas_tf_ly;
    }

    public final int getDen_tp_ly() {
        return den_tp_ly.get();
    }

    public final void setDen_tp_ly(int value) {
        den_tp_ly.set(value);
    }

    public IntegerProperty den_tp_lyProperty() {
        return den_tp_ly;
    }

    public final double getDen_tf_ly() {
        return den_tf_ly.get();
    }

    public final void setDen_tf_ly(double value) {
        den_tf_ly.set(value);
    }

    public DoubleProperty den_tf_lyProperty() {
        return den_tf_ly;
    }

    public final int getCol_tp_ly() {
        return col_tp_ly.get();
    }

    public final void setCol_tp_ly(int value) {
        col_tp_ly.set(value);
    }

    public IntegerProperty col_tp_lyProperty() {
        return col_tp_ly;
    }

    public final double getCol_tf_ly() {
        return col_tf_ly.get();
    }

    public final void setCol_tf_ly(double value) {
        col_tf_ly.set(value);
    }

    public DoubleProperty col_tf_lyProperty() {
        return col_tf_ly;
    }

    public final int getCas_tp_ly() {
        return cas_tp_ly.get();
    }

    public final void setCas_tp_ly(int value) {
        cas_tp_ly.set(value);
    }

    public IntegerProperty cas_tp_lyProperty() {
        return cas_tp_ly;
    }

    public final double getCas_tf_ly() {
        return cas_tf_ly.get();
    }

    public final void setCas_tf_ly(double value) {
        cas_tf_ly.set(value);
    }

    public DoubleProperty cas_tf_lyProperty() {
        return cas_tf_ly;
    }

    public final int getCln_att_ly() {
        return cln_att_ly.get();
    }

    public final void setCln_att_ly(int value) {
        cln_att_ly.set(value);
    }

    public IntegerProperty cln_att_lyProperty() {
        return cln_att_ly;
    }

    public final int getWis_tp_ly() {
        return wis_tp_ly.get();
    }

    public final void setWis_tp_ly(int value) {
        wis_tp_ly.set(value);
    }

    public IntegerProperty wis_tp_lyProperty() {
        return wis_tp_ly;
    }

    public final double getWis_tf_ly() {
        return wis_tf_ly.get();
    }

    public final void setWis_tf_ly(double value) {
        wis_tf_ly.set(value);
    }

    public DoubleProperty wis_tf_lyProperty() {
        return wis_tf_ly;
    }

    public final int getCam_tp_ly() {
        return cam_tp_ly.get();
    }

    public final void setCam_tp_ly(int value) {
        cam_tp_ly.set(value);
    }

    public IntegerProperty cam_tp_lyProperty() {
        return cam_tp_ly;
    }

    public final double getCam_tf_ly() {
        return cam_tf_ly.get();
    }

    public final void setCam_tf_ly(double value) {
        cam_tf_ly.set(value);
    }

    public DoubleProperty cam_tf_lyProperty() {
        return cam_tf_ly;
    }

    public final int getWis_tp_mta() {
        return wis_tp_mta.get();
    }

    public final void setWis_tp_mta(int value) {
        wis_tp_mta.set(value);
    }

    public IntegerProperty wis_tp_mtaProperty() {
        return wis_tp_mta;
    }

    public final int getCam_tp_mta() {
        return cam_tp_mta.get();
    }

    public final void setCam_tp_mta(int value) {
        cam_tp_mta.set(value);
    }

    public IntegerProperty cam_tp_mtaProperty() {
        return cam_tp_mta;
    }

    @Override
    public String toString() {
        return "Bid{" + "codrep=" + codrep + ", represent=" + represent + ", nome=" + nome + ", nome_uf=" + nome_uf + ", uf_rep="
                + uf_rep + ", regiao=" + regiao + ", gerente=" + gerente + ", marca=" + marca + ", codmar=" + codmar + ", colecao="
                + colecao + ", codcol=" + codcol + ", familia=" + familia + ", email_rep=" + email_rep + ", email_ger=" + email_ger
                + ", mostr=" + mostr + ", pm_rcm=" + pm_rcm + ", pm_rpm=" + pm_rpm + ", pm_mta=" + pm_mta + ", pm_per=" + pm_per + ", pm_ly="
                + pm_ly + ", cd_rcm=" + cd_rcm + ", cd_rpm=" + cd_rpm + ", cd_mta=" + cd_mta + ", cd_per=" + cd_per + ", cd_ly=" + cd_ly
                + ", pv_rcm=" + pv_rcm + ", pv_rpm=" + pv_rpm + ", pv_mta=" + pv_mta + ", pv_per=" + pv_per + ", pv_ly=" + pv_ly + ", ka_rcm="
                + ka_rcm + ", ka_rpm=" + ka_rpm + ", ka_mta=" + ka_mta + ", ka_per=" + ka_per + ", ka_ly=" + ka_ly + ", pp_rcm=" + pp_rcm
                + ", pp_rpm=" + pp_rpm + ", pp_mta=" + pp_mta + ", pp_per=" + pp_per + ", pp_ly=" + pp_ly + ", vp_rcm=" + vp_rcm + ", vp_rpm="
                + vp_rpm + ", vp_mta=" + vp_mta + ", vp_per=" + vp_per + ", vp_ly=" + vp_ly + ", tp_rcm=" + tp_rcm + ", tp_rpm=" + tp_rpm
                + ", tp_mta=" + tp_mta + ", tp_per=" + tp_per + ", tp_ly=" + tp_ly + ", tf_rcm=" + tf_rcm + ", tf_rpm=" + tf_rpm + ", tf_mta="
                + tf_mta + ", tf_per=" + tf_per + ", tf_ly=" + tf_ly + ", pg_rcm=" + pg_rcm + ", pg_rpm=" + pg_rpm + ", pg_mta=" + pg_mta
                + ", pg_per=" + pg_per + ", pg_ly=" + pg_ly + ", pr_rcm=" + pr_rcm + ", pr_rpm=" + pr_rpm + ", pr_mta=" + pr_mta + ", pr_per="
                + pr_per + ", pr_ly=" + pr_ly + ", cln_rcm=" + cln_rcm + ", cln_mta=" + cln_mta + ", cln_per=" + cln_per + ", bas_tp_rcm="
                + bas_tp_rcm + ", bas_tp_mta=" + bas_tp_mta + ", per_bas_tp=" + per_bas_tp + ", bas_tf_rcm=" + bas_tf_rcm + ", den_tp_rcm="
                + den_tp_rcm + ", den_tp_mta=" + den_tp_mta + ", per_den_tp=" + per_den_tp + ", den_tf_rcm=" + den_tf_rcm + ", col_tp_rcm="
                + col_tp_rcm + ", col_tp_mta=" + col_tp_mta + ", per_col_tp=" + per_col_tp + ", col_tf_rcm=" + col_tf_rcm + ", cas_tp_rcm="
                + cas_tp_rcm + ", cas_tp_mta=" + cas_tp_mta + ", per_cas_tp=" + per_cas_tp + ", cas_tf_rcm=" + cas_tf_rcm + ", sem_total="
                + sem_total + ", sem_hoje=" + sem_hoje + ", bas_tp_ly=" + bas_tp_ly + ", bas_tf_ly=" + bas_tf_ly + ", den_tp_ly=" + den_tp_ly
                + ", den_tf_ly=" + den_tf_ly + ", col_tp_ly=" + col_tp_ly + ", col_tf_ly=" + col_tf_ly + ", cas_tp_ly=" + cas_tp_ly + ", cas_tf_ly="
                + cas_tf_ly + ", cln_att_ly=" + cln_att_ly + ", cidades=" + cidades + ", bid_premiado=" + bid_premiado + '}';
    }

}
