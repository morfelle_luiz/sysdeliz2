/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class BidCliente {

    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty uf = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty codmar = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty compra = new SimpleStringProperty();
    private final ListProperty<BidClienteIndicadores> indicadores = new SimpleListProperty<BidClienteIndicadores>();

    public BidCliente() {
    }

    public BidCliente(String codcli, String cliente, String cidade, String uf, String codrep, String represent,
            String codmar, String marca, String compra, ObservableList<BidClienteIndicadores> indicadores) {
        this.codcli.set(codcli);
        this.cliente.set(cliente);
        this.cidade.set(cidade);
        this.uf.set(uf);
        this.codrep.set(codrep);
        this.represent.set(represent);
        this.codmar.set(codmar);
        this.marca.set(marca);
        this.compra.set(compra);
        this.indicadores.set(indicadores);
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getUf() {
        return uf.get();
    }

    public final void setUf(String value) {
        uf.set(value);
    }

    public StringProperty ufProperty() {
        return uf;
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getRepresent() {
        return represent.get();
    }

    public final void setRepresent(String value) {
        represent.set(value);
    }

    public StringProperty representProperty() {
        return represent;
    }

    public final String getCodmar() {
        return codmar.get();
    }

    public final void setCodmar(String value) {
        codmar.set(value);
    }

    public StringProperty codmarProperty() {
        return codmar;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final ObservableList<BidClienteIndicadores> getIndicadores() {
        return indicadores.get();
    }

    public final void setIndicadores(ObservableList<BidClienteIndicadores> value) {
        indicadores.set(value);
    }

    public ListProperty<BidClienteIndicadores> indicadoresProperty() {
        return indicadores;
    }

    public final String getCompra() {
        return compra.get();
    }

    public final void setCompra(String value) {
        compra.set(value);
    }

    public StringProperty compraProperty() {
        return compra;
    }

    @Override
    public String toString() {
        return "BidCliente{" + "codcli=" + codcli + ", cliente=" + cliente + ", cidade=" + cidade + ", uf=" + uf + ", codrep=" + codrep + ", represent=" + represent + ", codmar=" + codmar + ", marca=" + marca + ", compra=" + compra + ", indicadores=" + indicadores + '}';
    }

}
