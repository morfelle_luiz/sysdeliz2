/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class BidClienteIndicadores {

    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty indicador = new SimpleStringProperty();
    private final DoubleProperty col_atual = new SimpleDoubleProperty();
    private final DoubleProperty col_ant = new SimpleDoubleProperty();
    private final DoubleProperty diferencial = new SimpleDoubleProperty();

    public BidClienteIndicadores() {
    }

    public BidClienteIndicadores(String linha, String indicador, Double col_atual, Double col_ant, Double diferencial) {
        this.linha.set(linha);
        this.indicador.set(indicador);
        this.col_atual.set(col_atual);
        this.col_ant.set(col_ant);
        this.diferencial.set(diferencial);
    }

    public final String getLinha() {
        return linha.get();
    }

    public final void setLinha(String value) {
        linha.set(value);
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public final String getIndicador() {
        return indicador.get();
    }

    public final void setIndicador(String value) {
        indicador.set(value);
    }

    public StringProperty indicadorProperty() {
        return indicador;
    }

    public final double getCol_atual() {
        return col_atual.get();
    }

    public final void setCol_atual(double value) {
        col_atual.set(value);
    }

    public DoubleProperty col_atualProperty() {
        return col_atual;
    }

    public final double getCol_ant() {
        return col_ant.get();
    }

    public final void setCol_ant(double value) {
        col_ant.set(value);
    }

    public DoubleProperty col_antProperty() {
        return col_ant;
    }

    public final double getDiferencial() {
        return diferencial.get();
    }

    public final void setDiferencial(double value) {
        diferencial.set(value);
    }

    public DoubleProperty diferencialProperty() {
        return diferencial;
    }

    @Override
    public String toString() {
        return "BidClienteIndicadores{" + "linha=" + linha + ", indicador=" + indicador + ", col_atual=" + col_atual + ", col_ant=" + col_ant + ", diferencial=" + diferencial + '}';
    }

}
