/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class BidPremiado {

    private final StringProperty indicador = new SimpleStringProperty();
    private final StringProperty obrigatorio = new SimpleStringProperty();
    private final DoubleProperty rel_meta = new SimpleDoubleProperty();
    private final DoubleProperty graduacao = new SimpleDoubleProperty();
    private final DoubleProperty realizado = new SimpleDoubleProperty();
    private final DoubleProperty perc_premio = new SimpleDoubleProperty();
    private final DoubleProperty premio = new SimpleDoubleProperty();

    public BidPremiado(String indicador, String obrigatorio, Double rel_meta, Double graduacao,
            Double realizado, Double perc_premio, Double premio) {
        this.indicador.set(indicador);
        this.obrigatorio.set(obrigatorio);
        this.rel_meta.set(rel_meta);
        this.graduacao.set(graduacao);
        this.realizado.set(realizado);
        this.perc_premio.set(perc_premio);
        this.premio.set(premio);
    }

    public final String getIndicador() {
        return indicador.get();
    }

    public final void setIndicador(String value) {
        indicador.set(value);
    }

    public StringProperty indicadorProperty() {
        return indicador;
    }

    public final String getObrigatorio() {
        return obrigatorio.get();
    }

    public final void setObrigatorio(String value) {
        obrigatorio.set(value);
    }

    public StringProperty obrigatorioProperty() {
        return obrigatorio;
    }

    public final double getRel_meta() {
        return rel_meta.get();
    }

    public final void setRel_meta(double value) {
        rel_meta.set(value);
    }

    public DoubleProperty rel_metaProperty() {
        return rel_meta;
    }

    public final double getGraduacao() {
        return graduacao.get();
    }

    public final void setGraduacao(double value) {
        graduacao.set(value);
    }

    public DoubleProperty graduacaoProperty() {
        return graduacao;
    }

    public final double getRealizado() {
        return realizado.get();
    }

    public final void setRealizado(double value) {
        realizado.set(value);
    }

    public DoubleProperty realizadoProperty() {
        return realizado;
    }

    public final double getPerc_premio() {
        return perc_premio.get();
    }

    public final void setPerc_premio(double value) {
        perc_premio.set(value);
    }

    public DoubleProperty perc_premioProperty() {
        return perc_premio;
    }

    public final double getPremio() {
        return premio.get();
    }

    public final void setPremio(double value) {
        premio.set(value);
    }

    public DoubleProperty premioProperty() {
        return premio;
    }

}
