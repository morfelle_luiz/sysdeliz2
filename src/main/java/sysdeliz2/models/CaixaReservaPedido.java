/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class CaixaReservaPedido {

    private final StringProperty strCaixa = new SimpleStringProperty();
    private final StringProperty strStatus = new SimpleStringProperty();
    private final IntegerProperty intQtde = new SimpleIntegerProperty();
    private final ListProperty<ProdutoCaixaReservaPedido> listProdutosCaixa = new SimpleListProperty<>();

    public CaixaReservaPedido() {
    }

    public CaixaReservaPedido(String pCaixa, String pStatus, Integer pQtde) {
        this.strCaixa.set(pCaixa);
        this.strStatus.set(pStatus);
        this.intQtde.set(pQtde);
    }

    public CaixaReservaPedido(String pCaixa, String pStatus, Integer pQtde, List<ProdutoCaixaReservaPedido> pProdutos) {
        this.strCaixa.set(pCaixa);
        this.strStatus.set(pStatus);
        this.intQtde.set(pQtde);
        this.listProdutosCaixa.set(FXCollections.observableArrayList(pProdutos));
    }

    public final String getStrCaixa() {
        return strCaixa.get();
    }

    public final void setStrCaixa(String value) {
        strCaixa.set(value);
    }

    public StringProperty strCaixaProperty() {
        return strCaixa;
    }

    public final String getStrStatus() {
        return strStatus.get();
    }

    public final void setStrStatus(String value) {
        strStatus.set(value);
    }

    public StringProperty strStatusProperty() {
        return strStatus;
    }

    public final int getIntQtde() {
        return intQtde.get();
    }

    public final void setIntQtde(int value) {
        intQtde.set(value);
    }

    public IntegerProperty intQtdeProperty() {
        return intQtde;
    }

    public final ObservableList<ProdutoCaixaReservaPedido> getListProdutosCaixa() {
        return listProdutosCaixa.get();
    }

    public final void setListProdutosCaixa(ObservableList<ProdutoCaixaReservaPedido> value) {
        listProdutosCaixa.set(value);
    }

    public ListProperty<ProdutoCaixaReservaPedido> listProdutosCaixaProperty() {
        return listProdutosCaixa;
    }

    @Override
    public String toString() {
        return "CaixaReservaPedido{" + "strCaixa=" + strCaixa + ", strStatus=" + strStatus + ", intQtde=" + intQtde + ", listProdutosCaixa=" + listProdutosCaixa + '}';
    }

}
