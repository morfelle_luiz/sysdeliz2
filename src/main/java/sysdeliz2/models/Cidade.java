/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Cidade {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty estado = new SimpleStringProperty();
    private final StringProperty classe = new SimpleStringProperty();
    private final StringProperty regiao = new SimpleStringProperty();

    public Cidade() {
    }

    public Cidade(String codigo, String nome, String estado, String classe, String regiao) {
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.estado.set(estado);
        this.classe.set(classe);
        this.regiao.set(regiao);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getEstado() {
        return estado.get();
    }

    public final void setEstado(String value) {
        estado.set(value);
    }

    public StringProperty estadoProperty() {
        return estado;
    }

    public final String getClasse() {
        return classe.get();
    }

    public final void setClasse(String value) {
        classe.set(value);
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public final String getRegiao() {
        return regiao.get();
    }

    public final void setRegiao(String value) {
        regiao.set(value);
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    @Override
    public String toString() {
        return "Cidade{" + "selected=" + selected + ", codigo=" + codigo + ", nome=" + nome + ", estado=" + estado + ", classe=" + classe + ", regiao=" + regiao + '}';
    }

}
