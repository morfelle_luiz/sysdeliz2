/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */

@Deprecated
public class Cliente {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty fantasia = new SimpleStringProperty();
    private final StringProperty cnpj = new SimpleStringProperty();
    private final StringProperty endereco = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty estado = new SimpleStringProperty();
    private final StringProperty cep = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();

    public Cliente() {
    }

    public Cliente(String codcli, String nome, String fantasia, String cnpj, String cidade, String estado, String cep) {
        this.codcli.set(codcli);
        this.nome.set(nome);
        this.fantasia.set(fantasia);
        this.cnpj.set(cnpj);
        this.cidade.set(cidade);
        this.estado.set(estado);
        this.cep.set(cep);
    }

    public Cliente(String codcli, String nome, String fantasia, String cnpj, String cidade, String estado, String cep, String email) {
        this.codcli.set(codcli);
        this.nome.set(nome);
        this.fantasia.set(fantasia);
        this.cnpj.set(cnpj);
        this.cidade.set(cidade);
        this.estado.set(estado);
        this.cep.set(cep);
        this.email.set(email.toLowerCase());
    }

    public Cliente(String codcli, String nome, String fantasia, String cnpj, String cidade, String estado, String cep,
            String endereco, String bairro, String referencia, String telefone) {
        this.codcli.set(codcli);
        this.nome.set(nome);
        this.fantasia.set(fantasia);
        this.cnpj.set(cnpj);
        this.cidade.set(cidade);
        this.estado.set(estado);
        this.cep.set(cep);
        this.endereco.set(endereco);
        this.bairro.set(bairro);
        this.referencia.set(referencia);
        this.telefone.set(telefone);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getFantasia() {
        return fantasia.get();
    }

    public final void setFantasia(String value) {
        fantasia.set(value);
    }

    public StringProperty fantasiaProperty() {
        return fantasia;
    }

    public final String getCnpj() {
        return cnpj.get();
    }

    public final void setCnpj(String value) {
        cnpj.set(value);
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getEstado() {
        return estado.get();
    }

    public final void setEstado(String value) {
        estado.set(value);
    }

    public StringProperty estadoProperty() {
        return estado;
    }

    public final String getCep() {
        return cep.get();
    }

    public final void setCep(String value) {
        cep.set(value);
    }

    public StringProperty cepProperty() {
        return cep;
    }

    public final String getEndereco() {
        return endereco.get();
    }

    public final void setEndereco(String value) {
        endereco.set(value);
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public final String getBairro() {
        return bairro.get();
    }

    public final void setBairro(String value) {
        bairro.set(value);
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public final String getReferencia() {
        return referencia.get();
    }

    public final void setReferencia(String value) {
        referencia.set(value);
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    @Override
    public String toString() {
        return "Cliente{" + "selected=" + selected + ", codcli=" + codcli + ", nome=" + nome + ", fantasia=" + fantasia + ", cnpj=" + cnpj + ", endereco=" + endereco + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado + ", cep=" + cep + ", referencia=" + referencia + ", telefone=" + telefone + ", email=" + email + '}';
    }

}
