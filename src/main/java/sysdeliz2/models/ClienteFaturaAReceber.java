/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

/**
 *
 * @author cristiano.diego
 */
public class ClienteFaturaAReceber {

    private final BooleanProperty is_selected = new SimpleBooleanProperty(false);
    private final StringProperty cod_cli = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty cnpj = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty tipo_cli = new SimpleStringProperty();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty tipo_env = new SimpleStringProperty();
    private final ListProperty<FaturaAReceber> faturas = new SimpleListProperty<>();

    public ClienteFaturaAReceber() {
    }

    public ClienteFaturaAReceber(String cod_cli, String cliente, String cnpj, String email, String telefone, String tipo_cli,
            String represent, String status, String tipo_env, ObservableList<FaturaAReceber> faturas) {
        this.cod_cli.set(cod_cli);
        this.cliente.set(cliente);
        this.cnpj.set(cnpj);
        this.email.set(email);
        this.telefone.set(telefone);
        this.tipo_cli.set(tipo_cli);
        this.represent.set(represent);
        this.status.set(status);
        this.tipo_env.set(tipo_env);
        this.faturas.set(faturas);
    }

    public final boolean isIs_selected() {
        return is_selected.get();
    }

    public final void setIs_selected(boolean value) {
        is_selected.set(value);
    }

    public BooleanProperty is_selectedProperty() {
        return is_selected;
    }

    public final String getCod_cli() {
        return cod_cli.get();
    }

    public final void setCod_cli(String value) {
        cod_cli.set(value);
    }

    public StringProperty cod_cliProperty() {
        return cod_cli;
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public final String getTipo_cli() {
        return tipo_cli.get();
    }

    public final void setTipo_cli(String value) {
        tipo_cli.set(value);
    }

    public StringProperty tipo_cliProperty() {
        return tipo_cli;
    }

    public final String getRepresent() {
        return represent.get();
    }

    public final void setRepresent(String value) {
        represent.set(value);
    }

    public StringProperty representProperty() {
        return represent;
    }

    public final ObservableList<FaturaAReceber> getFaturas() {
        return faturas.get();
    }

    public final void setFaturas(ObservableList<FaturaAReceber> value) {
        faturas.set(value);
    }

    public ListProperty<FaturaAReceber> faturasProperty() {
        return faturas;
    }

    public final String getCnpj() {
        return cnpj.get();
    }

    public final void setCnpj(String value) {
        cnpj.set(value);
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getTipo_env() {
        return tipo_env.get();
    }

    public final void setTipo_env(String value) {
        tipo_env.set(value);
    }

    public StringProperty tipo_envProperty() {
        return tipo_env;
    }

    @Override
    public String toString() {
        return "ClienteFaturaAReceber{" + "is_selected=" + is_selected + ", cod_cli=" + cod_cli + ", cliente=" + cliente + ", cnpj=" + cnpj + ", email=" + email + ", telefone=" + telefone + ", tipo_cli=" + tipo_cli + ", represent=" + represent + ", status=" + status + ", tipo_env=" + tipo_env + ", faturas=" + faturas + '}';
    }

}
