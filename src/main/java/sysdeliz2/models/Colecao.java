package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Deprecated
public class Colecao {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strDescricao = new SimpleStringProperty();
    private final StringProperty inicio_vig = new SimpleStringProperty();
    private final StringProperty fim_vig = new SimpleStringProperty();

    public Colecao(String pCodigo, String pDescricao) {
        this.strCodigo.set(pCodigo);
        this.strDescricao.set(pDescricao);
    }

    public Colecao(String pCodigo, String pDescricao, String inicio_vig, String fim_vig) {
        this.strCodigo.set(pCodigo);
        this.strDescricao.set(pDescricao);
        this.inicio_vig.set(inicio_vig);
        this.fim_vig.set(fim_vig);
    }

    public final StringProperty strCodigoProperty() {
        return this.strCodigo;
    }

    public final String getStrCodigo() {
        return this.strCodigoProperty().get();
    }

    public final void setStrCodigo(final String strCodigo) {
        this.strCodigoProperty().set(strCodigo);
    }

    public final StringProperty strDescricaoProperty() {
        return this.strDescricao;
    }

    public final String getStrDescricao() {
        return this.strDescricaoProperty().get();
    }

    public final void setStrDescricao(final String strDescricao) {
        this.strDescricaoProperty().set(strDescricao);
    }

    public final String getInicio_vig() {
        return inicio_vig.get();
    }

    public final void setInicio_vig(String value) {
        inicio_vig.set(value);
    }

    public StringProperty inicio_vigProperty() {
        return inicio_vig;
    }

    public final String getFim_vig() {
        return fim_vig.get();
    }

    public final void setFim_vig(String value) {
        fim_vig.set(value);
    }

    public StringProperty fim_vigProperty() {
        return fim_vig;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    @Override
    public String toString() {
        return "Colecao [strCodigo=" + strCodigo + ", strDescricao=" + strDescricao + "]";
    }

}
