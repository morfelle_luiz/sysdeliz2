/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import org.apache.log4j.Logger;
import sysdeliz2.daos.FactoryConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class ConnectionFactory {

    private static final Logger LOG = Logger.getLogger(ConnectionFactory.class);

    // Bloco comentado substituído pelo POOL HIKARI
//    static {
//        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//        } catch (ClassNotFoundException exc){
//            exc.printStackTrace();
//        }
//    }

//    private static HikariConfig configDeliz = new HikariConfig();
//    private static HikariConfig configUpwave = new HikariConfig();
//    private static HikariDataSource dsDeliz;
//    private static HikariDataSource dsUpwave;

    static Connection connectionPool = null;

//    static {
//        configDeliz.setJdbcUrl("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01");
//        configDeliz.setUsername("divtierp");
//        configDeliz.setPassword("divtierp");
//        configDeliz.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//        configDeliz.setIdleTimeout(600000);
//        configDeliz.setPoolName("NativePoolDlz");
//        configDeliz.setMaximumPoolSize(1);
//        configDeliz.setMinimumIdle(1);
//        configDeliz.addDataSourceProperty("cachePrepStmts" ,"false");
////        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
////        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
//        dsDeliz = new HikariDataSource( configDeliz );
//
//        configUpwave.setJdbcUrl("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01");
//        configUpwave.setUsername("upwerp");
//        configUpwave.setPassword("upwerp");
//        configUpwave.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//        configUpwave.setIdleTimeout(600000);
//        configUpwave.setPoolName("NativePoolUpw");
//        configUpwave.setMaximumPoolSize(1);
//        configUpwave.setMinimumIdle(1);
//        configUpwave.addDataSourceProperty("cachePrepStmts" ,"false");
////        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
////        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
//        dsUpwave = new HikariDataSource( configUpwave );
//    }

    public static Connection getConnection(String url, String usuario, String senha) throws SQLException {
        return DriverManager.getConnection(url, usuario, senha);
    }
    
    public static Connection getArteliz3Connection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "deliz_ind", "deliz_ind");
    }
    
    public static Connection getEmpresaConnection() throws SQLException {
//        if (Globals.getEmpresaLogada().getCodigo().equals("1000")) {
            return new FactoryConnection(){
                @Override
                protected void initConnection() throws SQLException {

                }
            }.getEmpresaConnection();
//        } else {
//            return getUPWaveConnection();
//        }
    }
    
    public static Connection getDelizConnection() throws SQLException {
        //return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "divtierp", "divtierp");
//        Connection connectDs = null;
//
//        try {
//            connectDs = dsDeliz.getConnection();
//        }catch (SQLTransientConnectionException exc) {
//            exc.printStackTrace();
//            LOG.info("Fechando POOL atual", exc);
//            dsDeliz.close();
//            dsDeliz = new HikariDataSource(configDeliz);
//            LOG.info("Iniciando novo POOL");
//            connectDs = dsDeliz.getConnection();
//        }
//        return connectDs;
        return null;
    }
    
    public static Connection getUPWaveConnection() throws SQLException {
        //return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "upwerp", "upwerp");
//        Connection connectDs = null;
//
//        try {
//            connectDs = dsUpwave.getConnection();
//        }catch (SQLTransientConnectionException exc) {
//            exc.printStackTrace();
//            LOG.info("Fechando POOL atual", exc);
//            dsUpwave.close();
//            dsUpwave = new HikariDataSource(configUpwave);
//            LOG.info("Iniciando novo POOL");
//            connectDs = dsUpwave.getConnection();
//        }
//        return connectDs;
        return null;
    }
    
    public static Connection getDelizTestConnection() throws SQLException {
        return getConnection("jdbc:oracle:thin:@divdb01.nowar.corp:1521:divdb01", "divtitest", "divtitest");
    }

}
