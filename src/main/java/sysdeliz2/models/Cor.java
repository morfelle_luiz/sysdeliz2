/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author lima.joao
 */
@Deprecated
public class Cor {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty desc_cor = new SimpleStringProperty();
    private final StringProperty perccor = new SimpleStringProperty();

    public Cor(String codigo, String colecao, String cor, String desc_cor, String perccor){
        this.codigo.set(codigo);
        this.colecao.set(colecao);
        this.cor.set(cor);
        this.desc_cor.set(desc_cor);
        this.perccor.set(perccor);
    }
    
    public String getPerccor() {
        return perccor.get();
    }

    public void setPerccor(String value) {
        perccor.set(value);
    }

    public StringProperty perccorProperty() {
        return perccor;
    }
    
    public String getDesc_cor() {
        return desc_cor.get();
    }

    public void setDesc_cor(String value) {
        desc_cor.set(value);
    }

    public StringProperty desc_corProperty() {
        return desc_cor;
    }
    
    public String getColecao() {
        return colecao.get();
    }

    public void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public String getCor() {
        return cor.get();
    }

    public void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }
 
    public Double getPercCorAsDouble(){
        if(this.getPerccor() == null || this.getPerccor().equals("0") || this.getPerccor().equals("")){
            return 0.0;
        } else {
            return Double.parseDouble(this.getPerccor());
        }
    }
    
}
