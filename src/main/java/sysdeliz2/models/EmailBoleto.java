/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class EmailBoleto {

    private final StringProperty cod_cli = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty duplicatas = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty data_envio = new SimpleStringProperty();
    private final StringProperty data_ref = new SimpleStringProperty();

    public EmailBoleto() {
    }

    public EmailBoleto(String cod_cli, String cliente, String duplicatas, String usuario, String data_envio, String data_ref) {
        this.cod_cli.set(cod_cli);
        this.cliente.set(cliente);
        this.duplicatas.set(duplicatas);
        this.usuario.set(usuario);
        this.data_envio.set(data_envio);
        this.data_ref.set(data_ref);
    }

    public final String getCod_cli() {
        return cod_cli.get();
    }

    public final void setCod_cli(String value) {
        cod_cli.set(value);
    }

    public StringProperty cod_cliProperty() {
        return cod_cli;
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getDuplicatas() {
        return duplicatas.get();
    }

    public final void setDuplicatas(String value) {
        duplicatas.set(value);
    }

    public StringProperty duplicatasProperty() {
        return duplicatas;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getData_envio() {
        return data_envio.get();
    }

    public final void setData_envio(String value) {
        data_envio.set(value);
    }

    public StringProperty data_envioProperty() {
        return data_envio;
    }

    public final String getData_ref() {
        return data_ref.get();
    }

    public final void setData_ref(String value) {
        data_ref.set(value);
    }

    public StringProperty data_refProperty() {
        return data_ref;
    }

    @Override
    public String toString() {
        return "EmailBoleto{" + "cod_cli=" + cod_cli + ", cliente=" + cliente + ", duplicatas=" + duplicatas + ", usuario=" + usuario + ", data_envio=" + data_envio + ", data_ref=" + data_ref + '}';
    }

}
