/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Familia {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty cod_grupo = new SimpleStringProperty();
    private final StringProperty desc_grupo = new SimpleStringProperty();

    public Familia() {
    }

    public Familia(String codigo, String descricao, String cod_grupo, String desc_grupo) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.cod_grupo.set(cod_grupo);
        this.desc_grupo.set(desc_grupo);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getCod_grupo() {
        return cod_grupo.get();
    }

    public final void setCod_grupo(String value) {
        cod_grupo.set(value);
    }

    public StringProperty cod_grupoProperty() {
        return cod_grupo;
    }

    public final String getDesc_grupo() {
        return desc_grupo.get();
    }

    public final void setDesc_grupo(String value) {
        desc_grupo.set(value);
    }

    public StringProperty desc_grupoProperty() {
        return desc_grupo;
    }

    public void copy(Familia toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.descricao.set(toCopy.descricao.get());
        this.cod_grupo.set(toCopy.cod_grupo.get());
        this.desc_grupo.set(toCopy.desc_grupo.get());
    }

    @Override
    public String toString() {
        return "Familia{" + "selected=" + selected + ", codigo=" + codigo + ", descricao=" + descricao + ", cod_grupo=" + cod_grupo + ", desc_grupo=" + desc_grupo + '}';
    }

}
