/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class Familia001 {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty cor = new SimpleIntegerProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty fam_grp = new SimpleStringProperty();
    private final IntegerProperty meta = new SimpleIntegerProperty();

    public Familia001() {
    }

    public Familia001(String codigo, String descricao, Integer cor, String tipo, String grupo, String fam_grp, Integer meta) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.cor.set(cor);
        this.tipo.set(tipo);
        this.grupo.set(grupo);
        this.fam_grp.set(fam_grp);
        this.meta.set(meta);
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final int getCor() {
        return cor.get();
    }

    public final void setCor(int value) {
        cor.set(value);
    }

    public IntegerProperty corProperty() {
        return cor;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getFam_grp() {
        return fam_grp.get();
    }

    public final void setFam_grp(String value) {
        fam_grp.set(value);
    }

    public StringProperty fam_grpProperty() {
        return fam_grp;
    }

    public final int getMeta() {
        return meta.get();
    }

    public final void setMeta(int value) {
        meta.set(value);
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

}
