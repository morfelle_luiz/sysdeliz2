/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class FamiliaGrupo001 {

    private final BooleanProperty selected = new SimpleBooleanProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty meta = new SimpleIntegerProperty();

    public FamiliaGrupo001() {
    }

    public FamiliaGrupo001(String codigo, String descricao, Integer meta) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.meta.set(meta);
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final int getMeta() {
        return meta.get();
    }

    public final void setMeta(int value) {
        meta.set(value);
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void copy(FamiliaGrupo001 toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.descricao.set(toCopy.descricao.get());
        this.meta.set(toCopy.meta.get());
    }

}
