/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class FaturaAReceber {

    private final StringProperty cod_cli = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final StringProperty duplicata = new SimpleStringProperty();
    private final DoubleProperty valor_fatura = new SimpleDoubleProperty();
    private final DoubleProperty valor_duplicata = new SimpleDoubleProperty();
    private final StringProperty dt_vencto = new SimpleStringProperty();
    private final StringProperty dt_emissao = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();

    public FaturaAReceber() {
    }

    public FaturaAReceber(String cod_cli, String obs, String fatura, String duplicata, String pedido,
            Double valor_fatura, Double valor_duplicata, String dt_vencto, String dt_emissao, String status) {
        this.cod_cli.set(cod_cli);
        this.obs.set(obs);
        this.fatura.set(fatura);
        this.duplicata.set(duplicata);
        this.pedido.set(pedido);
        this.valor_fatura.set(valor_fatura);
        this.valor_duplicata.set(valor_duplicata);
        this.dt_vencto.set(dt_vencto);
        this.dt_emissao.set(dt_emissao);
        this.status.set(status);
    }

    public final String getCod_cli() {
        return cod_cli.get();
    }

    public final void setCod_cli(String value) {
        cod_cli.set(value);
    }

    public StringProperty cod_cliProperty() {
        return cod_cli;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getFatura() {
        return fatura.get();
    }

    public final void setFatura(String value) {
        fatura.set(value);
    }

    public StringProperty faturaProperty() {
        return fatura;
    }

    public final String getDuplicata() {
        return duplicata.get();
    }

    public final void setDuplicata(String value) {
        duplicata.set(value);
    }

    public StringProperty duplicataProperty() {
        return duplicata;
    }

    public final double getValor_fatura() {
        return valor_fatura.get();
    }

    public final void setValor_fatura(double value) {
        valor_fatura.set(value);
    }

    public DoubleProperty valor_faturaProperty() {
        return valor_fatura;
    }

    public final double getValor_duplicata() {
        return valor_duplicata.get();
    }

    public final void setValor_duplicata(double value) {
        valor_duplicata.set(value);
    }

    public DoubleProperty valor_duplicataProperty() {
        return valor_duplicata;
    }

    public final String getDt_vencto() {
        return dt_vencto.get();
    }

    public final void setDt_vencto(String value) {
        dt_vencto.set(value);
    }

    public StringProperty dt_venctoProperty() {
        return dt_vencto;
    }

    public final String getDt_emissao() {
        return dt_emissao.get();
    }

    public final void setDt_emissao(String value) {
        dt_emissao.set(value);
    }

    public StringProperty dt_emissaoProperty() {
        return dt_emissao;
    }

    public final String getPedido() {
        return pedido.get();
    }

    public final void setPedido(String value) {
        pedido.set(value);
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    @Override
    public String toString() {
        return "FaturaAReceber{" + "cod_cli=" + cod_cli + ", obs=" + obs + ", fatura=" + fatura + ", duplicata=" + duplicata + ", valor_fatura=" + valor_fatura + ", valor_duplicata=" + valor_duplicata + ", dt_vencto=" + dt_vencto + ", dt_emissao=" + dt_emissao + ", pedido=" + pedido + ", status=" + status + '}';
    }

}
