/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class HistoricoRevisaoMeta {

    private final StringProperty dt_revisao = new SimpleStringProperty();
    private final StringProperty hr_revisao = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty revisao = new SimpleStringProperty();

    public HistoricoRevisaoMeta() {
    }

    public HistoricoRevisaoMeta(String dt_revisao, String codigo, String colecao, String obs, String usuario, String revisao) {
        this.dt_revisao.set(dt_revisao);
        this.codigo.set(codigo);
        this.colecao.set(colecao);
        this.obs.set(obs);
        this.usuario.set(usuario);
        this.revisao.set(revisao);
    }

    public final String getDt_revisao() {
        return dt_revisao.get();
    }

    public final void setDt_revisao(String value) {
        dt_revisao.set(value);
    }

    public StringProperty dt_revisaoProperty() {
        return dt_revisao;
    }

    public final String getHr_revisao() {
        return hr_revisao.get();
    }

    public final void setHr_revisao(String value) {
        hr_revisao.set(value);
    }

    public StringProperty hr_revisaoProperty() {
        return hr_revisao;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getRevisao() {
        return revisao.get();
    }

    public final void setRevisao(String value) {
        revisao.set(value);
    }

    public StringProperty revisaoProperty() {
        return revisao;
    }

    @Override
    public String toString() {
        return "HistoricoRevisaoMeta{" + "dt_revisao=" + dt_revisao + ", hr_revisao=" + hr_revisao + ", codigo=" + codigo + ", colecao=" + colecao + ", obs=" + obs + ", usuario=" + usuario + ", revisao=" + revisao + '}';
    }

}
