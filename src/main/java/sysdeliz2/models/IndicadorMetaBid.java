/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class IndicadorMetaBid {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty codmar = new SimpleStringProperty();
    private final StringProperty codcol = new SimpleStringProperty();
    private final StringProperty indicador = new SimpleStringProperty();
    private final DoubleProperty mta = new SimpleDoubleProperty();
    private final DoubleProperty rpm = new SimpleDoubleProperty();
    private final DoubleProperty bas = new SimpleDoubleProperty();
    private final DoubleProperty den = new SimpleDoubleProperty();
    private final DoubleProperty coll = new SimpleDoubleProperty();
    private final DoubleProperty casu = new SimpleDoubleProperty();
    private final DoubleProperty wish = new SimpleDoubleProperty();

    public IndicadorMetaBid(String codrep, String codmar, String codcol, String indicador,
            Double mta, Double rpm, Double bas, Double den, Double coll, Double casu, Double wish) {
        this.codrep.set(codrep);
        this.codmar.set(codmar);
        this.codcol.set(codcol);
        this.indicador.set(indicador);
        this.mta.set(mta);
        this.rpm.set(rpm);
        this.bas.set(bas);
        this.den.set(den);
        this.coll.set(coll);
        this.casu.set(casu);
        this.wish.set(wish);
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getCodmar() {
        return codmar.get();
    }

    public final void setCodmar(String value) {
        codmar.set(value);
    }

    public StringProperty codmarProperty() {
        return codmar;
    }

    public final String getCodcol() {
        return codcol.get();
    }

    public final void setCodcol(String value) {
        codcol.set(value);
    }

    public StringProperty codcolProperty() {
        return codcol;
    }

    public final String getIndicador() {
        return indicador.get();
    }

    public final void setIndicador(String value) {
        indicador.set(value);
    }

    public StringProperty indicadorProperty() {
        return indicador;
    }

    public final double getMta() {
        return mta.get();
    }

    public final void setMta(double value) {
        mta.set(value);
    }

    public DoubleProperty mtaProperty() {
        return mta;
    }

    public final double getRpm() {
        return rpm.get();
    }

    public final void setRpm(double value) {
        rpm.set(value);
    }

    public DoubleProperty rpmProperty() {
        return rpm;
    }

    public final double getBas() {
        return bas.get();
    }

    public final void setBas(double value) {
        bas.set(value);
    }

    public DoubleProperty basProperty() {
        return bas;
    }

    public final double getDen() {
        return den.get();
    }

    public final void setDen(double value) {
        den.set(value);
    }

    public DoubleProperty denProperty() {
        return den;
    }

    public final double getColl() {
        return coll.get();
    }

    public final void setColl(double value) {
        coll.set(value);
    }

    public DoubleProperty collProperty() {
        return coll;
    }

    public final double getCasu() {
        return casu.get();
    }

    public final void setCasu(double value) {
        casu.set(value);
    }

    public DoubleProperty casuProperty() {
        return casu;
    }

    public double getWish() {
        return wish.get();
    }

    public DoubleProperty wishProperty() {
        return wish;
    }

    public void setWish(double wish) {
        this.wish.set(wish);
    }

    @Override
    public String toString() {
        return "IndicadorMetaBid{" + "codrep=" + codrep + ", codmar=" + codmar + ", codcol=" + codcol + ", indicador=" + indicador + ", mta=" + mta + ", rpm=" + rpm + ", bas=" + bas + ", den=" + den + ", coll=" + coll + ", casu=" + casu + '}';
    }

}
