/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class IndicadorMetaBidPremiado {

    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty obrigatorio = new SimpleStringProperty();
    private final StringProperty indicador = new SimpleStringProperty();
    private final DoubleProperty graduacao = new SimpleDoubleProperty();
    private final DoubleProperty perc_premio = new SimpleDoubleProperty();

    public IndicadorMetaBidPremiado(String marca, String colecao, String indicador, Double graduacao, Double perc_premio, String obrigatorio) {
        this.marca.set(marca);
        this.colecao.set(colecao);
        this.indicador.set(indicador);
        this.graduacao.set(graduacao);
        this.perc_premio.set(perc_premio);
        this.obrigatorio.set(obrigatorio);
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final String getObrigatorio() {
        return obrigatorio.get();
    }

    public final void setObrigatorio(String value) {
        obrigatorio.set(value);
    }

    public StringProperty obrigatorioProperty() {
        return obrigatorio;
    }

    public final String getIndicador() {
        return indicador.get();
    }

    public final void setIndicador(String value) {
        indicador.set(value);
    }

    public StringProperty indicadorProperty() {
        return indicador;
    }

    public final double getGraduacao() {
        return graduacao.get();
    }

    public final void setGraduacao(double value) {
        graduacao.set(value);
    }

    public DoubleProperty graduacaoProperty() {
        return graduacao;
    }

    public final double getPerc_premio() {
        return perc_premio.get();
    }

    public final void setPerc_premio(double value) {
        perc_premio.set(value);
    }

    public DoubleProperty perc_premioProperty() {
        return perc_premio;
    }

    @Override
    public String toString() {
        return "IndicadorMetaBidPremiado{" + "colecao=" + colecao + ", marca=" + marca + ", obrigatorio=" + obrigatorio + ", indicador=" + indicador + ", graduacao=" + graduacao + ", perc_premio=" + perc_premio + '}';
    }

}
