/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
public class IndiceProjecao {

    private final StringProperty strMarca = new SimpleStringProperty();
    private final StringProperty strLinha = new SimpleStringProperty();
    private final ObjectProperty<Colecao> objColecao = new SimpleObjectProperty<>();
    private final BooleanProperty isByLinha = new SimpleBooleanProperty(false);
    private final BooleanProperty isByMarca = new SimpleBooleanProperty(false);
    private final BooleanProperty isByFixo = new SimpleBooleanProperty(false);
    private final BooleanProperty isByMeta = new SimpleBooleanProperty(false);
    private final DoubleProperty douIndice = new SimpleDoubleProperty(0);
    private final DoubleProperty douMeta = new SimpleDoubleProperty(0);

    public IndiceProjecao() {
    }

    public IndiceProjecao(String pMarca, Colecao pColecao, Boolean pByMarca, Boolean pByLinha, Double pByFixo, Double pByMeta, String pLinha) {
        this.strMarca.set(pMarca);
        this.objColecao.set(pColecao);
        this.isByLinha.set(pByLinha);
        this.isByMarca.set(pByMarca);
        this.douIndice.set(pByFixo);
        this.douMeta.set(pByMeta);
        this.strLinha.set(pLinha);
        this.isByFixo.set(Boolean.logicalAnd(pByFixo != null, pByFixo != 0.0));
        this.isByMeta.set(Boolean.logicalAnd(pByMeta != null, pByMeta != 0.0));
    }

    public final String getStrMarca() {
        return strMarca.get();
    }

    public final void setStrMarca(String value) {
        strMarca.set(value);
    }

    public StringProperty strMarcaProperty() {
        return strMarca;
    }

    public final Colecao getObjColecao() {
        return objColecao.get();
    }

    public final void setObjColecao(Colecao value) {
        objColecao.set(value);
    }

    public ObjectProperty<Colecao> objColecaoProperty() {
        return objColecao;
    }

    public final boolean isIsByLinha() {
        return isByLinha.get();
    }

    public final void setIsByLinha(boolean value) {
        isByLinha.set(value);
    }

    public BooleanProperty isByLinhaProperty() {
        return isByLinha;
    }

    public final boolean isIsByMarca() {
        return isByMarca.get();
    }

    public final void setIsByMarca(boolean value) {
        isByMarca.set(value);
    }

    public BooleanProperty isByMarcaProperty() {
        return isByMarca;
    }

    public final boolean isIsByFixo() {
        return isByFixo.get();
    }

    public final void setIsByFixo(boolean value) {
        isByFixo.set(value);
    }

    public BooleanProperty isByFixoProperty() {
        return isByFixo;
    }

    public final double getDouIndice() {
        return douIndice.get();
    }

    public final void setDouIndice(double value) {
        douIndice.set(value);
    }

    public DoubleProperty douIndiceProperty() {
        return douIndice;
    }

    public final String getStrLinha() {
        return strLinha.get();
    }

    public final void setStrLinha(String value) {
        strLinha.set(value);
    }

    public StringProperty strLinhaProperty() {
        return strLinha;
    }

    public final boolean isIsByMeta() {
        return isByMeta.get();
    }

    public final void setIsByMeta(boolean value) {
        isByMeta.set(value);
    }

    public BooleanProperty isByMetaProperty() {
        return isByMeta;
    }

    public final double getDouMeta() {
        return douMeta.get();
    }

    public final void setDouMeta(double value) {
        douMeta.set(value);
    }

    public DoubleProperty douMetaProperty() {
        return douMeta;
    }

    @Override
    public String toString() {
        return "IndiceProjecao{" + "strMarca=" + strMarca + ", strLinha=" + strLinha + ", objColecao=" + objColecao + ", isByLinha=" + isByLinha + ", isByMarca=" + isByMarca + ", isByFixo=" + isByFixo + ", isByMeta=" + isByMeta + ", douIndice=" + douIndice + ", douMeta=" + douMeta + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.strMarca);
        hash = 43 * hash + Objects.hashCode(this.objColecao.getValue().getStrCodigo());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IndiceProjecao other = (IndiceProjecao) obj;
        if (!Objects.equals(this.strMarca.get(), other.strMarca.get())) {
            return false;
        }
        if (!Objects.equals(this.objColecao.getValue().strCodigoProperty().get(), other.objColecao.getValue().strCodigoProperty().get())) {
            return false;
        }
        if(other.isByMetaProperty().get()) {
        	if (!Objects.equals(this.strLinhaProperty().get(), other.strLinhaProperty().get())) {
                return false;
            }
        }
        return true;
    }

}
