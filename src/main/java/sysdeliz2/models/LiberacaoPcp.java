/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class LiberacaoPcp {

    StringProperty codigo = new SimpleStringProperty();
    StringProperty colecao = new SimpleStringProperty();
    StringProperty status = new SimpleStringProperty();
    StringProperty obs = new SimpleStringProperty();
    StringProperty usuario = new SimpleStringProperty();
    StringProperty dt_manutencao = new SimpleStringProperty();

    public LiberacaoPcp() {
    }

    public LiberacaoPcp(String codigo, String colecao, String dt_manutencao, String obs, String status, String usuario) {
        this.codigo.set(codigo);
        this.colecao.set(colecao);
        this.dt_manutencao.set(dt_manutencao);
        this.obs.set(obs);
        this.status.set(status);
        this.usuario.set(usuario);
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getDt_manutencao() {
        return dt_manutencao.get();
    }

    public final void setDt_manutencao(String value) {
        dt_manutencao.set(value);
    }

    public StringProperty dt_manutencaoProperty() {
        return dt_manutencao;
    }

    @Override
    public String toString() {
        return "LiberacaoPcp{" + "codigo=" + codigo + ", colecao=" + colecao + ", status=" + status + ", obs=" + obs + ", usuario=" + usuario + ", dt_manutencao=" + dt_manutencao + '}';
    }

}
