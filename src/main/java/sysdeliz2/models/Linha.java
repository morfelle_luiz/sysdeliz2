/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Linha {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty sd_marca = new SimpleStringProperty();
    private final StringProperty desc_marca = new SimpleStringProperty();
    private final IntegerProperty concentrado = new SimpleIntegerProperty();
    private final StringProperty sdDescLinha = new SimpleStringProperty(this, "sdDescLinha");

    public Linha() {
    }

    public Linha(String codigo, String descricao, String sd_marca, String desc_marca, Integer concentrado) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.sd_marca.set(sd_marca);
        this.desc_marca.set(desc_marca);
        this.concentrado.set(concentrado);
    }

    public Linha(String codigo, String descricao, String sd_marca, String desc_marca, Integer concentrado, String sdDescLinha) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.sd_marca.set(sd_marca);
        this.desc_marca.set(desc_marca);
        this.concentrado.set(concentrado);
        this.sdDescLinha.set(sdDescLinha);
    }


    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getSd_marca() {
        return sd_marca.get();
    }

    public final void setSd_marca(String value) {
        sd_marca.set(value);
    }

    public StringProperty sd_marcaProperty() {
        return sd_marca;
    }

    public final String getDesc_marca() {
        return desc_marca.get();
    }

    public final void setDesc_marca(String value) {
        desc_marca.set(value);
    }

    public StringProperty desc_marcaProperty() {
        return desc_marca;
    }

    public final int getConcentrado() {
        return concentrado.get();
    }

    public final void setConcentrado(int value) {
        concentrado.set(value);
    }

    public IntegerProperty concentradoProperty() {
        return concentrado;
    }

    public String getSdDescLinha() {
        return sdDescLinha.get();
    }

    public StringProperty sdDescLinhaProperty() {
        return sdDescLinha;
    }

    public void setSdDescLinha(String sdDescLinha) {
        this.sdDescLinha.set(sdDescLinha);
    }

    @Override
    public String toString() {
        return "Linha{" + "selected=" + selected + ", codigo=" + codigo + ", descricao=" + descricao + ", sd_marca=" + sd_marca + ", desc_marca=" + desc_marca + ", concentrado=" + concentrado + '}';
    }

}
