/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author cristiano.diego
 */
public class Log {

    public enum TipoAcao {
        CADASTRAR("CADASTRAR"), EXCLUIR("EXCLUIR"), MOVIMENTAR("MOVIMENTAR"), ENVIAR("ENVIAR"), EDITAR("EDITAR");

        public String tipoAcao;

        TipoAcao(String value) {
            tipoAcao = value;
        }

        public String getValor() {
            return tipoAcao;
        }
    };

    private final StringProperty data_log = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty acao = new SimpleStringProperty();
    private final StringProperty tela = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();

    public Log() {
    }

    public Log(String usuario, String acao, String tela, String referencia, String descricao) {
        this.acao.set(acao);
        this.data_log.set(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.descricao.set(descricao);
        this.referencia.set(referencia);
        this.tela.set(tela);
        this.usuario.set(usuario);
    }

    public Log(String data_log, String usuario, String acao, String tela, String referencia, String descricao) {
        this.acao.set(acao);
        this.data_log.set(data_log);
        this.descricao.set(descricao);
        this.referencia.set(referencia);
        this.tela.set(tela);
        this.usuario.set(usuario);
    }

    public Log(String usuario, TipoAcao acao, String tela, String referencia, String descricao) {
        this.acao.set(acao.getValor());
        this.data_log.set(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.descricao.set(descricao);
        this.referencia.set(referencia);
        this.tela.set(tela);
        this.usuario.set(usuario);
    }

    public final String getData_log() {
        return data_log.get();
    }

    public final void setData_log(String value) {
        data_log.set(value);
    }

    public StringProperty data_logProperty() {
        return data_log;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getAcao() {
        return acao.get();
    }

    public final void setAcao(String value) {
        acao.set(value);
    }

    public StringProperty acaoProperty() {
        return acao;
    }

    public final String getTela() {
        return tela.get();
    }

    public final void setTela(String value) {
        tela.set(value);
    }

    public StringProperty telaProperty() {
        return tela;
    }

    public final String getReferencia() {
        return referencia.get();
    }

    public final void setReferencia(String value) {
        referencia.set(value);
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    @Override
    public String toString() {
        return "Log{" + "data_log=" + data_log + ", usuario=" + usuario + ", acao=" + acao + ", tela=" + tela + ", referencia=" + referencia + ", descricao=" + descricao + '}';
    }
}
