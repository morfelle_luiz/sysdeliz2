/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class MarketingPedido {

    private final StringProperty strNumeroPedido = new SimpleStringProperty();
    private final StringProperty strCliente = new SimpleStringProperty();
    private final StringProperty strMarcar = new SimpleStringProperty();
    private final StringProperty strRepresentante = new SimpleStringProperty();
    private final StringProperty strDataEmissao = new SimpleStringProperty();
    private final StringProperty strGrupo = new SimpleStringProperty();
    private final IntegerProperty intSaldo = new SimpleIntegerProperty();
    private final ListProperty<ProdutosMarketingPedido> listProdutos = new SimpleListProperty<>();

    public MarketingPedido() {
    }

    public MarketingPedido(String pPedido, String pCliente, String pRepresentante, String pDataEmissao, Integer pSaldo, String pGrupo, String marca) {
        this.strNumeroPedido.set(pPedido);
        this.strCliente.set(pCliente);
        this.strRepresentante.set(pRepresentante);
        this.strDataEmissao.set(pDataEmissao);
        this.intSaldo.set(pSaldo);
        this.strGrupo.set(pGrupo);
        this.strMarcar.set(marca);
    }

    public MarketingPedido(String pPedido, String pCliente, String pRepresentante, String pDataEmissao, Integer pSaldo, String pGrupo, String marca, List<ProdutosMarketingPedido> pProdutos) {
        this.strNumeroPedido.set(pPedido);
        this.strCliente.set(pCliente);
        this.strRepresentante.set(pRepresentante);
        this.strDataEmissao.set(pDataEmissao);
        this.intSaldo.set(pSaldo);
        this.strGrupo.set(pGrupo);
        this.strMarcar.set(marca);
        this.listProdutos.set(FXCollections.observableArrayList(pProdutos));
    }

    public final String getStrNumeroPedido() {
        return strNumeroPedido.get();
    }

    public final void setStrNumeroPedido(String value) {
        strNumeroPedido.set(value);
    }

    public StringProperty strNumeroPedidoProperty() {
        return strNumeroPedido;
    }

    public final String getStrCliente() {
        return strCliente.get();
    }

    public final void setStrCliente(String value) {
        strCliente.set(value);
    }

    public StringProperty strClienteProperty() {
        return strCliente;
    }

    public final String getStrRepresentante() {
        return strRepresentante.get();
    }

    public final void setStrRepresentante(String value) {
        strRepresentante.set(value);
    }

    public StringProperty strRepresentanteProperty() {
        return strRepresentante;
    }

    public final String getStrDataEmissao() {
        return strDataEmissao.get();
    }

    public final void setStrDataEmissao(String value) {
        strDataEmissao.set(value);
    }

    public StringProperty strDataEmissaoProperty() {
        return strDataEmissao;
    }

    public final int getIntSaldo() {
        return intSaldo.get();
    }

    public final void setIntSaldo(int value) {
        intSaldo.set(value);
    }

    public IntegerProperty intSaldoProperty() {
        return intSaldo;
    }

    public final ObservableList<ProdutosMarketingPedido> getListProdutos() {
        return listProdutos.get();
    }

    public final void setListProdutos(ObservableList<ProdutosMarketingPedido> value) {
        listProdutos.set(value);
    }

    public ListProperty<ProdutosMarketingPedido> listProdutosProperty() {
        return listProdutos;
    }

    public final String getStrGrupo() {
        return strGrupo.get();
    }

    public final void setStrGrupo(String value) {
        strGrupo.set(value);
    }

    public StringProperty strGrupoProperty() {
        return strGrupo;
    }

    public final String getStrMarcar() {
        return strMarcar.get();
    }

    public final void setStrMarcar(String value) {
        strMarcar.set(value);
    }

    public StringProperty strMarcarProperty() {
        return strMarcar;
    }

    @Override
    public String toString() {
        return "MarketingPedido{" + "strNumeroPedido=" + strNumeroPedido + ", strCliente=" + strCliente + ", strRepresentante=" + strRepresentante + ", strDataEmissao=" + strDataEmissao + ", strGrupo=" + strGrupo + ", intSaldo=" + intSaldo + ", listProdutos=" + listProdutos + '}';
    }

}
