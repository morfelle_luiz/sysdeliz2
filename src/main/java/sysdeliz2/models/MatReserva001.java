/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class MatReserva001 {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdeB = new SimpleIntegerProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty mov = new SimpleStringProperty();
    private final StringProperty romaneio = new SimpleStringProperty();
    private final StringProperty dataCad = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public MatReserva001() {
    }

    public MatReserva001(String numero, String codigo, String lote,
            String cor, String deposito, Integer qtde, Integer qtdeB,
            String nome, String obs, String mov, String romaneio,
            String dataCad, String tipo) {
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.lote.set(lote);
        this.cor.set(cor);
        this.deposito.set(deposito);
        this.qtde.set(qtde);
        this.qtdeB.set(qtdeB);
        this.nome.set(nome);
        this.obs.set(obs);
        this.mov.set(mov);
        this.romaneio.set(romaneio);
        this.dataCad.set(dataCad);
        this.tipo.set(tipo);
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getLote() {
        return lote.get();
    }

    public final void setLote(String value) {
        lote.set(value);
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getDeposito() {
        return deposito.get();
    }

    public final void setDeposito(String value) {
        deposito.set(value);
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public final int getQtde() {
        return qtde.get();
    }

    public final void setQtde(int value) {
        qtde.set(value);
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public final int getQtdeB() {
        return qtdeB.get();
    }

    public final void setQtdeB(int value) {
        qtdeB.set(value);
    }

    public IntegerProperty qtdeBProperty() {
        return qtdeB;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getMov() {
        return mov.get();
    }

    public final void setMov(String value) {
        mov.set(value);
    }

    public StringProperty movProperty() {
        return mov;
    }

    public final String getRomaneio() {
        return romaneio.get();
    }

    public final void setRomaneio(String value) {
        romaneio.set(value);
    }

    public StringProperty romaneioProperty() {
        return romaneio;
    }

    public final String getDataCad() {
        return dataCad.get();
    }

    public final void setDataCad(String value) {
        dataCad.set(value);
    }

    public StringProperty dataCadProperty() {
        return dataCad;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

}
