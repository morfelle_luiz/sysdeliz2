/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.daos.DAOFactory;

import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Of1001 {

    private final IntegerProperty impof = new SimpleIntegerProperty();
    private final StringProperty dtInicio = new SimpleStringProperty();
    private final StringProperty dtFinal = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final ObjectProperty<Produto001> relCodigo = new SimpleObjectProperty<Produto001>();
    private final StringProperty dtPrev = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final IntegerProperty ativos = new SimpleIntegerProperty();
    private final StringProperty consumo = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty facQtde = new SimpleStringProperty();
    private final IntegerProperty qtdeB = new SimpleIntegerProperty();
    private final IntegerProperty pesoBr = new SimpleIntegerProperty();
    private final IntegerProperty pesoLiq = new SimpleIntegerProperty();
    private final StringProperty maquina = new SimpleStringProperty();
    private final IntegerProperty perc = new SimpleIntegerProperty();
    private final StringProperty facPreco = new SimpleStringProperty();
    private final StringProperty programacao = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty sequencial = new SimpleIntegerProperty();
    private final StringProperty tabela = new SimpleStringProperty();
    private final ListProperty<OfIten001> grade = new SimpleListProperty<>();

    public Of1001() {
    }

    public Of1001(Integer impof, String dtInicio, String dtFinal, String periodo,
            String pedido, String numero, String codigo, String dtPrev, String codcli,
            Integer ativos, String consumo, String parte, String observacao, String facQtde,
            Integer qtdeB, Integer pesoBr, Integer pesoLiq, String maquina, Integer perc,
            String facPreco, String programacao, String tipo, Integer sequencial,
            String tabela) {
        this.impof.set(impof);
        this.dtInicio.set(dtInicio);
        this.dtFinal.set(dtFinal);
        this.periodo.set(periodo);
        this.pedido.set(pedido);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.dtPrev.set(dtPrev);
        this.codcli.set(codcli);
        this.ativos.set(ativos);
        this.consumo.set(consumo);
        this.parte.set(parte);
        this.observacao.set(observacao);
        this.facQtde.set(facQtde);
        this.qtdeB.set(qtdeB);
        this.pesoBr.set(pesoBr);
        this.pesoLiq.set(pesoLiq);
        this.maquina.set(maquina);
        this.perc.set(perc);
        this.facPreco.set(facPreco);
        this.programacao.set(programacao);
        this.tipo.set(tipo);
        this.sequencial.set(sequencial);
        this.tabela.set(tabela);
    }

    public Of1001(Integer impof, String dtInicio, String dtFinal, String periodo,
            String pedido, String numero, String codigo, String dtPrev, String codcli,
            Integer ativos, String consumo, String parte, String observacao, String facQtde,
            Integer qtdeB, Integer pesoBr, Integer pesoLiq, String maquina, Integer perc,
            String facPreco, String programacao, String tipo, Integer sequencial,
            String tabela, ObservableList<OfIten001> grade) {
        this.impof.set(impof);
        this.dtInicio.set(dtInicio);
        this.dtFinal.set(dtFinal);
        this.periodo.set(periodo);
        this.pedido.set(pedido);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.dtPrev.set(dtPrev);
        this.codcli.set(codcli);
        this.ativos.set(ativos);
        this.consumo.set(consumo);
        this.parte.set(parte);
        this.observacao.set(observacao);
        this.facQtde.set(facQtde);
        this.qtdeB.set(qtdeB);
        this.pesoBr.set(pesoBr);
        this.pesoLiq.set(pesoLiq);
        this.maquina.set(maquina);
        this.perc.set(perc);
        this.facPreco.set(facPreco);
        this.programacao.set(programacao);
        this.tipo.set(tipo);
        this.sequencial.set(sequencial);
        this.tabela.set(tabela);
        this.grade.set(grade);
    }

    public final int getImpof() {
        return impof.get();
    }

    public final void setImpof(int value) {
        impof.set(value);
    }

    public IntegerProperty impofProperty() {
        return impof;
    }

    public final String getDtInicio() {
        return dtInicio.get();
    }

    public final void setDtInicio(String value) {
        dtInicio.set(value);
    }

    public StringProperty dtInicioProperty() {
        return dtInicio;
    }

    public final String getDtFinal() {
        return dtFinal.get();
    }

    public final void setDtFinal(String value) {
        dtFinal.set(value);
    }

    public StringProperty dtFinalProperty() {
        return dtFinal;
    }

    public final String getPeriodo() {
        return periodo.get();
    }

    public final void setPeriodo(String value) {
        periodo.set(value);
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public final String getPedido() {
        return pedido.get();
    }

    public final void setPedido(String value) {
        pedido.set(value);
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDtPrev() {
        return dtPrev.get();
    }

    public final void setDtPrev(String value) {
        dtPrev.set(value);
    }

    public StringProperty dtPrevProperty() {
        return dtPrev;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final Integer getAtivos() {
        return ativos.get();
    }

    public final void setAtivos(Integer value) {
        ativos.set(value);
    }

    public IntegerProperty ativosProperty() {
        return ativos;
    }

    public final String getConsumo() {
        return consumo.get();
    }

    public final void setConsumo(String value) {
        consumo.set(value);
    }

    public StringProperty consumoProperty() {
        return consumo;
    }

    public final String getParte() {
        return parte.get();
    }

    public final void setParte(String value) {
        parte.set(value);
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public final String getObservacao() {
        return observacao.get();
    }

    public final void setObservacao(String value) {
        observacao.set(value);
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public final String getFacQtde() {
        return facQtde.get();
    }

    public final void setFacQtde(String value) {
        facQtde.set(value);
    }

    public StringProperty facQtdeProperty() {
        return facQtde;
    }

    public final int getQtdeB() {
        return qtdeB.get();
    }

    public final void setQtdeB(int value) {
        qtdeB.set(value);
    }

    public IntegerProperty qtdeBProperty() {
        return qtdeB;
    }

    public final int getPesoBr() {
        return pesoBr.get();
    }

    public final void setPesoBr(int value) {
        pesoBr.set(value);
    }

    public IntegerProperty pesoBrProperty() {
        return pesoBr;
    }

    public final int getPesoLiq() {
        return pesoLiq.get();
    }

    public final void setPesoLiq(int value) {
        pesoLiq.set(value);
    }

    public IntegerProperty pesoLiqProperty() {
        return pesoLiq;
    }

    public final String getMaquina() {
        return maquina.get();
    }

    public final void setMaquina(String value) {
        maquina.set(value);
    }

    public StringProperty maquinaProperty() {
        return maquina;
    }

    public final int getPerc() {
        return perc.get();
    }

    public final void setPerc(int value) {
        perc.set(value);
    }

    public IntegerProperty percProperty() {
        return perc;
    }

    public final String getFacPreco() {
        return facPreco.get();
    }

    public final void setFacPreco(String value) {
        facPreco.set(value);
    }

    public StringProperty facPrecoProperty() {
        return facPreco;
    }

    public final String getProgramacao() {
        return programacao.get();
    }

    public final void setProgramacao(String value) {
        programacao.set(value);
    }

    public StringProperty programacaoProperty() {
        return programacao;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final int getSequencial() {
        return sequencial.get();
    }

    public final void setSequencial(int value) {
        sequencial.set(value);
    }

    public IntegerProperty sequencialProperty() {
        return sequencial;
    }

    public final String getTabela() {
        return tabela.get();
    }

    public final void setTabela(String value) {
        tabela.set(value);
    }

    public StringProperty tabelaProperty() {
        return tabela;
    }

    public final ObservableList<OfIten001> getGrade() {
        return grade.get();
    }

    public final void setGrade(ObservableList<OfIten001> value) {
        grade.set(value);
    }

    public ListProperty<OfIten001> gradeProperty() {
        return grade;
    }

    public final Produto001 getRelCodigo() throws SQLException {
        relCodigo.set(DAOFactory.getProduto001DAO().getByCode(this.codigo.get()));
        return relCodigo.get();
    }

    public ObjectProperty<Produto001> relCodigoProperty() throws SQLException {
        relCodigo.set(DAOFactory.getProduto001DAO().getByCode(this.codigo.get()));
        return relCodigo;
    }

}
