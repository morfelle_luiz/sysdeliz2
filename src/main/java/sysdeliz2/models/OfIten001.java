/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableMap;

/**
 *
 * @author cristiano.diego
 */
public class OfIten001 {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty composicao = new SimpleStringProperty();
    private final MapProperty<String, Integer> tamanhos = new SimpleMapProperty<String, Integer>();
    private final MapProperty<Integer, String> headersTamanhos = new SimpleMapProperty<Integer, String>();

    public OfIten001() {
    }

    public OfIten001(String numero, String cor, ObservableMap<String, Integer> tamanhos, ObservableMap<Integer, String> headersTamanhos) {
        this.numero.set(numero);
        this.cor.set(cor);
        this.tamanhos.set(tamanhos);
        this.headersTamanhos.set(headersTamanhos);
    }
    
    public OfIten001(String numero, String cor, String composicao, ObservableMap<String, Integer> tamanhos, ObservableMap<Integer, String> headersTamanhos) {
        this.numero.set(numero);
        this.cor.set(cor);
        this.tamanhos.set(tamanhos);
        this.headersTamanhos.set(headersTamanhos);
        this.composicao.set(composicao);
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final ObservableMap<String, Integer> getTamanhos() {
        return tamanhos.get();
    }

    public final void setTamanhos(ObservableMap<String, Integer> value) {
        tamanhos.set(value);
    }

    public MapProperty<String, Integer> tamanhosProperty() {
        return tamanhos;
    }

    public final ObservableMap<Integer, String> getHeadersTamanhos() {
        return headersTamanhos.get();
    }

    public final void setHeadersTamanhos(ObservableMap<Integer, String> value) {
        headersTamanhos.set(value);
    }

    public MapProperty<Integer, String> headersTamanhosProperty() {
        return headersTamanhos;
    }
    
    public String getComposicao() {
        return composicao.get();
    }
    
    public StringProperty composicaoProperty() {
        return composicao;
    }
    
    public void setComposicao(String composicao) {
        this.composicao.set(composicao);
    }
}
