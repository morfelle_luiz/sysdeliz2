/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class PedIten {

    private Integer ordem;
    private Integer qtde;
    private Integer qtde_f;
    private Double preco;
    private String tam;
    private String cor;
    private String numero;
    private String codigo;
    private Integer qtde_canc;
    private Integer qtde_packs;
    private String qualidade;
    private Integer qtde_orig;
    private Double preco_orig;
    private Integer indice2;
    private Double perc_comissao;
    private String desconto;
    private String tipo;
    private Double ipi;
    private Double margem;
    private Integer bonif;
    private Double custo;
    private Integer indice;
    private Double impostos;
    private String embalagem;
    private String observacao;
    private String dt_entrega;
    private String desc_pack;
    private String estampa;
    private Integer nr_item;
    private String motivo;
    private Double valor_st;

    public PedIten() {
    }

    public PedIten(Integer ordem, Integer qtde, Integer qtde_f, Double preco, String tam, String cor, String numero,
            String codigo, Integer qtde_canc, Integer qtde_packs, String qualidade, Integer qtde_orig, Double preco_orig,
            Integer indice2, Double perc_comissao, String desconto, String tipo, Double ipi, Double margem, Integer bonif,
            Double custo, Integer indice, Double impostos, String embalagem, String observacao, String dt_entrega,
            String desc_pack, String estampa, Integer nr_item, String motivo, Double valor_st) {
        this.ordem = ordem;
        this.qtde = qtde;
        this.qtde_f = qtde_f;
        this.preco = preco;
        this.tam = tam;
        this.cor = cor;
        this.numero = numero;
        this.codigo = codigo;
        this.qtde_canc = qtde_canc;
        this.qtde_packs = qtde_packs;
        this.qualidade = qualidade;
        this.qtde_orig = qtde_orig;
        this.preco_orig = preco_orig;
        this.indice2 = indice2;
        this.perc_comissao = perc_comissao;
        this.desconto = desconto;
        this.tipo = tipo;
        this.ipi = ipi;
        this.margem = margem;
        this.bonif = bonif;
        this.custo = custo;
        this.indice = indice;
        this.impostos = impostos;
        this.embalagem = embalagem;
        this.observacao = observacao;
        this.dt_entrega = dt_entrega;
        this.desc_pack = desc_pack;
        this.estampa = estampa;
        this.nr_item = nr_item;
        this.motivo = motivo;
        this.valor_st = valor_st;
    }

    public PedIten(Integer ordem, Integer qtde, Double preco, String tam, String cor, String numero, String codigo,
            Integer qtde_packs, String qualidade, Integer qtde_orig, Double preco_orig, String tipo, String dt_entrega, Integer nr_item) {
        this.ordem = ordem;
        this.qtde = qtde;
        this.preco = preco;
        this.tam = tam;
        this.cor = cor;
        this.numero = numero;
        this.codigo = codigo;
        this.qtde_packs = qtde_packs;
        this.qualidade = qualidade;
        this.qtde_orig = qtde_orig;
        this.preco_orig = preco_orig;
        this.tipo = tipo;
        this.dt_entrega = dt_entrega;
        this.nr_item = nr_item;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    public Integer getQtde_f() {
        return qtde_f;
    }

    public void setQtde_f(Integer qtde_f) {
        this.qtde_f = qtde_f;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public String getTam() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam = tam;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getQtde_canc() {
        return qtde_canc;
    }

    public void setQtde_canc(Integer qtde_canc) {
        this.qtde_canc = qtde_canc;
    }

    public Integer getQtde_packs() {
        return qtde_packs;
    }

    public void setQtde_packs(Integer qtde_packs) {
        this.qtde_packs = qtde_packs;
    }

    public String getQualidade() {
        return qualidade;
    }

    public void setQualidade(String qualidade) {
        this.qualidade = qualidade;
    }

    public Integer getQtde_orig() {
        return qtde_orig;
    }

    public void setQtde_orig(Integer qtde_orig) {
        this.qtde_orig = qtde_orig;
    }

    public Double getPreco_orig() {
        return preco_orig;
    }

    public void setPreco_orig(Double preco_orig) {
        this.preco_orig = preco_orig;
    }

    public Integer getIndice2() {
        return indice2;
    }

    public void setIndice2(Integer indice2) {
        this.indice2 = indice2;
    }

    public Double getPerc_comissao() {
        return perc_comissao;
    }

    public void setPerc_comissao(Double perc_comissao) {
        this.perc_comissao = perc_comissao;
    }

    public String getDesconto() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto = desconto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getIpi() {
        return ipi;
    }

    public void setIpi(Double ipi) {
        this.ipi = ipi;
    }

    public Double getMargem() {
        return margem;
    }

    public void setMargem(Double margem) {
        this.margem = margem;
    }

    public Integer getBonif() {
        return bonif;
    }

    public void setBonif(Integer bonif) {
        this.bonif = bonif;
    }

    public Double getCusto() {
        return custo;
    }

    public void setCusto(Double custo) {
        this.custo = custo;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Double getImpostos() {
        return impostos;
    }

    public void setImpostos(Double impostos) {
        this.impostos = impostos;
    }

    public String getEmbalagem() {
        return embalagem;
    }

    public void setEmbalagem(String embalagem) {
        this.embalagem = embalagem;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getDt_entrega() {
        return dt_entrega;
    }

    public void setDt_entrega(String dt_entrega) {
        this.dt_entrega = dt_entrega;
    }

    public String getDesc_pack() {
        return desc_pack;
    }

    public void setDesc_pack(String desc_pack) {
        this.desc_pack = desc_pack;
    }

    public String getEstampa() {
        return estampa;
    }

    public void setEstampa(String estampa) {
        this.estampa = estampa;
    }

    public Integer getNr_item() {
        return nr_item;
    }

    public void setNr_item(Integer nr_item) {
        this.nr_item = nr_item;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Double getValor_st() {
        return valor_st;
    }

    public void setValor_st(Double valor_st) {
        this.valor_st = valor_st;
    }

}
