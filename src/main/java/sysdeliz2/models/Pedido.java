/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Pedido {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty documento = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty endereco = new SimpleStringProperty();
    private final StringProperty numero_end = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty uf = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty doc_tipo = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty cep = new SimpleStringProperty();
    private final StringProperty grupo_eco = new SimpleStringProperty();
    private final DoubleProperty valor = new SimpleDoubleProperty();
    private final DoubleProperty valor_recebi = new SimpleDoubleProperty();
    private final DoubleProperty desconto = new SimpleDoubleProperty();
    private final DoubleProperty total = new SimpleDoubleProperty();

    public Pedido() {
    }

    public Pedido(String numero, String cliente, String documento, String grupo, String endereco, String numero_end,
            String bairro, String cidade, String uf, String telefone, String email, String observacao, Double valor,
            Double desconto, Double total, String doc_tipo, String codcli, String cep, Double valor_recebido, String grupo_eco) {
        this.numero.set(numero);
        this.cliente.set(cliente);
        this.documento.set(documento);
        this.grupo.set(grupo);
        this.endereco.set(endereco);
        this.numero_end.set(numero_end);
        this.bairro.set(bairro);
        this.cidade.set(cidade);
        this.uf.set(uf);
        this.telefone.set(telefone);
        this.email.set(email);
        this.observacao.set(observacao);
        this.valor.set(valor);
        this.valor_recebi.set(valor_recebido);
        this.desconto.set(desconto);
        this.total.set(total);
        this.doc_tipo.set(doc_tipo.equals("2") ? "CNPJ" : "CPF");
        this.codcli.set(codcli);
        this.cep.set(cep);
        this.grupo_eco.set(grupo_eco);
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getDocumento() {
        return documento.get();
    }

    public final void setDocumento(String value) {
        documento.set(value);
    }

    public StringProperty documentoProperty() {
        return documento;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getEndereco() {
        return endereco.get();
    }

    public final void setEndereco(String value) {
        endereco.set(value);
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public final String getNumero_end() {
        return numero_end.get();
    }

    public final void setNumero_end(String value) {
        numero_end.set(value);
    }

    public StringProperty numero_endProperty() {
        return numero_end;
    }

    public final String getBairro() {
        return bairro.get();
    }

    public final void setBairro(String value) {
        bairro.set(value);
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getUf() {
        return uf.get();
    }

    public final void setUf(String value) {
        uf.set(value);
    }

    public StringProperty ufProperty() {
        return uf;
    }

    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public final String getObservacao() {
        return observacao.get();
    }

    public final void setObservacao(String value) {
        observacao.set(value);
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public final double getValor() {
        return valor.get();
    }

    public final void setValor(double value) {
        valor.set(value);
    }

    public DoubleProperty valorProperty() {
        return valor;
    }

    public final double getDesconto() {
        return desconto.get();
    }

    public final void setDesconto(double value) {
        desconto.set(value);
    }

    public DoubleProperty descontoProperty() {
        return desconto;
    }

    public final double getTotal() {
        return total.get();
    }

    public final void setTotal(double value) {
        total.set(value);
    }

    public DoubleProperty totalProperty() {
        return total;
    }

    public final String getDoc_tipo() {
        return doc_tipo.get();
    }

    public final void setDoc_tipo(String value) {
        doc_tipo.set(value);
    }

    public StringProperty doc_tipoProperty() {
        return doc_tipo;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getCep() {
        return cep.get();
    }

    public final void setCep(String value) {
        cep.set(value);
    }

    public StringProperty cepProperty() {
        return cep;
    }

    public final double getValor_recebi() {
        return valor_recebi.get();
    }

    public final void setValor_recebi(double value) {
        valor_recebi.set(value);
    }

    public DoubleProperty valor_recebiProperty() {
        return valor_recebi;
    }
    
    public String getGrupo_eco() {
        return grupo_eco.get();
    }
    
    public StringProperty grupo_ecoProperty() {
        return grupo_eco;
    }
    
    public void setGrupo_eco(String grupo_eco) {
        this.grupo_eco.set(grupo_eco);
    }
}
