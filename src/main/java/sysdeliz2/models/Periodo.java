package sysdeliz2.models;

import javafx.beans.property.*;

public class Periodo {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty prazo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty dt_inicio = new SimpleStringProperty();
    private final StringProperty dt_fim = new SimpleStringProperty();
    private final IntegerProperty minuto = new SimpleIntegerProperty();
    private final StringProperty ativo = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public Periodo() {
    }

    public Periodo(String prazo, String descricao, String dt_inicio, String dt_fim, Integer minuto, String ativo, String tipo) {
        this.prazo.set(prazo);
        this.descricao.set(descricao);
        this.dt_inicio.set(dt_inicio);
        this.dt_fim.set(dt_fim);
        this.minuto.set(minuto);
        this.ativo.set(ativo);
        this.tipo.set(tipo);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public Periodo(String pCodigo, String pDescricao) {
        this.prazo.set(pCodigo);
        this.descricao.set(pDescricao);
    }

    public final String getPrazo() {
        return prazo.get();
    }

    public final void setPrazo(String value) {
        prazo.set(value);
    }

    public StringProperty prazoProperty() {
        return prazo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getDt_inicio() {
        return dt_inicio.get();
    }

    public final void setDt_inicio(String value) {
        dt_inicio.set(value);
    }

    public StringProperty dt_inicioProperty() {
        return dt_inicio;
    }

    public final String getDt_fim() {
        return dt_fim.get();
    }

    public final void setDt_fim(String value) {
        dt_fim.set(value);
    }

    public StringProperty dt_fimProperty() {
        return dt_fim;
    }

    public final int getMinuto() {
        return minuto.get();
    }

    public final void setMinuto(int value) {
        minuto.set(value);
    }

    public IntegerProperty minutoProperty() {
        return minuto;
    }

    public final String getAtivo() {
        return ativo.get();
    }

    public final void setAtivo(String value) {
        ativo.set(value);
    }

    public StringProperty ativoProperty() {
        return ativo;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    @Override
    public String toString() {
        return "Periodo{" + "prazo=" + prazo + ", descricao=" + descricao + ", dt_inicio=" + dt_inicio + ", dt_fim=" + dt_fim + ", minuto=" + minuto + ", ativo=" + ativo + ", tipo=" + tipo + '}';
    }

}
