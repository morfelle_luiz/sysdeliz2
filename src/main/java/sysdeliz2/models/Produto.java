/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.models.table.TableManutencaoPedidosCoresReferencia;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Produto {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty desp_encai = new SimpleStringProperty();
    private final StringProperty minimo = new SimpleStringProperty();
    private final StringProperty peso = new SimpleStringProperty();
    private final StringProperty tempo = new SimpleStringProperty();
    private final StringProperty gramatura = new SimpleStringProperty();
    private final StringProperty ipi = new SimpleStringProperty();
    private final StringProperty data_cad = new SimpleStringProperty();
    private final StringProperty preco = new SimpleStringProperty();
    private final StringProperty preco_med = new SimpleStringProperty();
    private final StringProperty custo = new SimpleStringProperty();
    private final StringProperty quant = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty ativo = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty codfis = new SimpleStringProperty();
    private final StringProperty codtrib = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty largura = new SimpleStringProperty();
    private final StringProperty compri = new SimpleStringProperty();
    private final StringProperty espessura = new SimpleStringProperty();
    private final StringProperty densidade = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty fornecedor = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty codigo2 = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty comp1 = new SimpleStringProperty();
    private final StringProperty comp2 = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty descricao2 = new SimpleStringProperty();
    private final StringProperty ficha = new SimpleStringProperty();
    private final StringProperty tabela = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty preco_com = new SimpleStringProperty();
    private final StringProperty desp_cort = new SimpleStringProperty();
    private final StringProperty peso_talhado = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty obs1 = new SimpleStringProperty();
    private final StringProperty prototipo = new SimpleStringProperty();
    private final StringProperty icms = new SimpleStringProperty();
    private final StringProperty dt_entrega = new SimpleStringProperty();
    private final StringProperty dt_producao = new SimpleStringProperty();
    private final StringProperty custo_imp = new SimpleStringProperty();
    private final StringProperty uni_venda = new SimpleStringProperty();
    private final StringProperty peso_max = new SimpleStringProperty();
    private final StringProperty validade = new SimpleStringProperty();
    private final StringProperty origem = new SimpleStringProperty();
    private final StringProperty obs2 = new SimpleStringProperty();
    private final StringProperty etiqueta = new SimpleStringProperty();
    private final StringProperty obs3 = new SimpleStringProperty();
    private final StringProperty tributacao = new SimpleStringProperty();
    private final StringProperty preco_for = new SimpleStringProperty();
    private final StringProperty preco_repos = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty estilista = new SimpleStringProperty();
    private final StringProperty modelista = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty codsped = new SimpleStringProperty();
    private final StringProperty cubagem = new SimpleStringProperty();
    private final StringProperty trib_ipi = new SimpleStringProperty();
    private final StringProperty trib_pis = new SimpleStringProperty();
    private final StringProperty trib_cofins = new SimpleStringProperty();
    private final StringProperty conta = new SimpleStringProperty();
    private final StringProperty md5 = new SimpleStringProperty();
    private final StringProperty pag_catalogo = new SimpleStringProperty();
    private final StringProperty atualiza = new SimpleStringProperty();
    private final StringProperty linha2 = new SimpleStringProperty();
    private final StringProperty celula = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final StringProperty usu_seqatu = new SimpleStringProperty();
    private final ListProperty<TableManutencaoPedidosCoresReferencia> cores_saldos = new SimpleListProperty<>();
    
    private final ListProperty<Cor> cores_demanda = new SimpleListProperty<>();
    private final ListProperty<Tamanho> tamanhos_demanda = new SimpleListProperty<>();

    public ObservableList<Tamanho> getTamanhos_demanda() {
        return tamanhos_demanda.get();
    }

    public void setTamanhos_demanda(ObservableList value) {
        tamanhos_demanda.set(value);
    }

    public ListProperty tamanhos_demandaProperty() {
        return tamanhos_demanda;
    }
    
    public ObservableList<Cor> getCores_demanda() {
        return cores_demanda.get();
    }

    public void setCores_demanda(ObservableList value) {
        cores_demanda.set(value);
    }

    public ListProperty cores_demandaProperty() {
        return cores_demanda;
    }
       
    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getDesp_encai() {
        return desp_encai.get();
    }

    public final void setDesp_encai(String value) {
        desp_encai.set(value);
    }

    public StringProperty desp_encaiProperty() {
        return desp_encai;
    }

    public final String getMinimo() {
        return minimo.get();
    }

    public final void setMinimo(String value) {
        minimo.set(value);
    }

    public StringProperty minimoProperty() {
        return minimo;
    }

    public final String getPeso() {
        return peso.get();
    }

    public final void setPeso(String value) {
        peso.set(value);
    }

    public StringProperty pesoProperty() {
        return peso;
    }

    public final String getTempo() {
        return tempo.get();
    }

    public final void setTempo(String value) {
        tempo.set(value);
    }

    public StringProperty tempoProperty() {
        return tempo;
    }

    public final String getGramatura() {
        return gramatura.get();
    }

    public final void setGramatura(String value) {
        gramatura.set(value);
    }

    public StringProperty gramaturaProperty() {
        return gramatura;
    }

    public final String getIpi() {
        return ipi.get();
    }

    public final void setIpi(String value) {
        ipi.set(value);
    }

    public StringProperty ipiProperty() {
        return ipi;
    }

    public final String getData_cad() {
        return data_cad.get();
    }

    public final void setData_cad(String value) {
        data_cad.set(value);
    }

    public StringProperty data_cadProperty() {
        return data_cad;
    }

    public final String getPreco() {
        return preco.get();
    }

    public final void setPreco(String value) {
        preco.set(value);
    }

    public StringProperty precoProperty() {
        return preco;
    }

    public final String getPreco_med() {
        return preco_med.get();
    }

    public final void setPreco_med(String value) {
        preco_med.set(value);
    }

    public StringProperty preco_medProperty() {
        return preco_med;
    }

    public final String getCusto() {
        return custo.get();
    }

    public final void setCusto(String value) {
        custo.set(value);
    }

    public StringProperty custoProperty() {
        return custo;
    }

    public final String getQuant() {
        return quant.get();
    }

    public final void setQuant(String value) {
        quant.set(value);
    }

    public StringProperty quantProperty() {
        return quant;
    }

    public final String getLinha() {
        return linha.get();
    }

    public final void setLinha(String value) {
        linha.set(value);
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public final String getAtivo() {
        return ativo.get();
    }

    public final void setAtivo(String value) {
        ativo.set(value);
    }

    public StringProperty ativoProperty() {
        return ativo;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getCodfis() {
        return codfis.get();
    }

    public final void setCodfis(String value) {
        codfis.set(value);
    }

    public StringProperty codfisProperty() {
        return codfis;
    }

    public final String getCodtrib() {
        return codtrib.get();
    }

    public final void setCodtrib(String value) {
        codtrib.set(value);
    }

    public StringProperty codtribProperty() {
        return codtrib;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getUnidade() {
        return unidade.get();
    }

    public final void setUnidade(String value) {
        unidade.set(value);
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public final String getLargura() {
        return largura.get();
    }

    public final void setLargura(String value) {
        largura.set(value);
    }

    public StringProperty larguraProperty() {
        return largura;
    }

    public final String getCompri() {
        return compri.get();
    }

    public final void setCompri(String value) {
        compri.set(value);
    }

    public StringProperty compriProperty() {
        return compri;
    }

    public final String getEspessura() {
        return espessura.get();
    }

    public final void setEspessura(String value) {
        espessura.set(value);
    }

    public StringProperty espessuraProperty() {
        return espessura;
    }

    public final String getDensidade() {
        return densidade.get();
    }

    public final void setDensidade(String value) {
        densidade.set(value);
    }

    public StringProperty densidadeProperty() {
        return densidade;
    }

    public final String getLocal() {
        return local.get();
    }

    public final void setLocal(String value) {
        local.set(value);
    }

    public StringProperty localProperty() {
        return local;
    }

    public final String getFornecedor() {
        return fornecedor.get();
    }

    public final void setFornecedor(String value) {
        fornecedor.set(value);
    }

    public StringProperty fornecedorProperty() {
        return fornecedor;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getCodigo2() {
        return codigo2.get();
    }

    public final void setCodigo2(String value) {
        codigo2.set(value);
    }

    public StringProperty codigo2Property() {
        return codigo2;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getComp1() {
        return comp1.get();
    }

    public final void setComp1(String value) {
        comp1.set(value);
    }

    public StringProperty comp1Property() {
        return comp1;
    }

    public final String getComp2() {
        return comp2.get();
    }

    public final void setComp2(String value) {
        comp2.set(value);
    }

    public StringProperty comp2Property() {
        return comp2;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getDescricao2() {
        return descricao2.get();
    }

    public final void setDescricao2(String value) {
        descricao2.set(value);
    }

    public StringProperty descricao2Property() {
        return descricao2;
    }

    public final String getFicha() {
        return ficha.get();
    }

    public final void setFicha(String value) {
        ficha.set(value);
    }

    public StringProperty fichaProperty() {
        return ficha;
    }

    public final String getTabela() {
        return tabela.get();
    }

    public final void setTabela(String value) {
        tabela.set(value);
    }

    public StringProperty tabelaProperty() {
        return tabela;
    }

    public final String getFaixa() {
        return faixa.get();
    }

    public final void setFaixa(String value) {
        faixa.set(value);
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final String getPreco_com() {
        return preco_com.get();
    }

    public final void setPreco_com(String value) {
        preco_com.set(value);
    }

    public StringProperty preco_comProperty() {
        return preco_com;
    }

    public final String getDesp_cort() {
        return desp_cort.get();
    }

    public final void setDesp_cort(String value) {
        desp_cort.set(value);
    }

    public StringProperty desp_cortProperty() {
        return desp_cort;
    }

    public final String getPeso_talhado() {
        return peso_talhado.get();
    }

    public final void setPeso_talhado(String value) {
        peso_talhado.set(value);
    }

    public StringProperty peso_talhadoProperty() {
        return peso_talhado;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getObs1() {
        return obs1.get();
    }

    public final void setObs1(String value) {
        obs1.set(value);
    }

    public StringProperty obs1Property() {
        return obs1;
    }

    public final String getPrototipo() {
        return prototipo.get();
    }

    public final void setPrototipo(String value) {
        prototipo.set(value);
    }

    public StringProperty prototipoProperty() {
        return prototipo;
    }

    public final String getIcms() {
        return icms.get();
    }

    public final void setIcms(String value) {
        icms.set(value);
    }

    public StringProperty icmsProperty() {
        return icms;
    }

    public final String getDt_entrega() {
        return dt_entrega.get();
    }

    public final void setDt_entrega(String value) {
        dt_entrega.set(value);
    }

    public StringProperty dt_entregaProperty() {
        return dt_entrega;
    }

    public final String getDt_producao() {
        return dt_producao.get();
    }

    public final void setDt_producao(String value) {
        dt_producao.set(value);
    }

    public StringProperty dt_producaoProperty() {
        return dt_producao;
    }

    public final String getCusto_imp() {
        return custo_imp.get();
    }

    public final void setCusto_imp(String value) {
        custo_imp.set(value);
    }

    public StringProperty custo_impProperty() {
        return custo_imp;
    }

    public final String getUni_venda() {
        return uni_venda.get();
    }

    public final void setUni_venda(String value) {
        uni_venda.set(value);
    }

    public StringProperty uni_vendaProperty() {
        return uni_venda;
    }

    public final String getPeso_max() {
        return peso_max.get();
    }

    public final void setPeso_max(String value) {
        peso_max.set(value);
    }

    public StringProperty peso_maxProperty() {
        return peso_max;
    }

    public final String getValidade() {
        return validade.get();
    }

    public final void setValidade(String value) {
        validade.set(value);
    }

    public StringProperty validadeProperty() {
        return validade;
    }

    public final String getOrigem() {
        return origem.get();
    }

    public final void setOrigem(String value) {
        origem.set(value);
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public final String getObs2() {
        return obs2.get();
    }

    public final void setObs2(String value) {
        obs2.set(value);
    }

    public StringProperty obs2Property() {
        return obs2;
    }

    public final String getEtiqueta() {
        return etiqueta.get();
    }

    public final void setEtiqueta(String value) {
        etiqueta.set(value);
    }

    public StringProperty etiquetaProperty() {
        return etiqueta;
    }

    public final String getObs3() {
        return obs3.get();
    }

    public final void setObs3(String value) {
        obs3.set(value);
    }

    public StringProperty obs3Property() {
        return obs3;
    }

    public final String getTributacao() {
        return tributacao.get();
    }

    public final void setTributacao(String value) {
        tributacao.set(value);
    }

    public StringProperty tributacaoProperty() {
        return tributacao;
    }

    public final String getPreco_for() {
        return preco_for.get();
    }

    public final void setPreco_for(String value) {
        preco_for.set(value);
    }

    public StringProperty preco_forProperty() {
        return preco_for;
    }

    public final String getPreco_repos() {
        return preco_repos.get();
    }

    public final void setPreco_repos(String value) {
        preco_repos.set(value);
    }

    public StringProperty preco_reposProperty() {
        return preco_repos;
    }

    public final String getEstoque() {
        return estoque.get();
    }

    public final void setEstoque(String value) {
        estoque.set(value);
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public final String getEstilista() {
        return estilista.get();
    }

    public final void setEstilista(String value) {
        estilista.set(value);
    }

    public StringProperty estilistaProperty() {
        return estilista;
    }

    public final String getModelista() {
        return modelista.get();
    }

    public final void setModelista(String value) {
        modelista.set(value);
    }

    public StringProperty modelistaProperty() {
        return modelista;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getCodsped() {
        return codsped.get();
    }

    public final void setCodsped(String value) {
        codsped.set(value);
    }

    public StringProperty codspedProperty() {
        return codsped;
    }

    public final String getCubagem() {
        return cubagem.get();
    }

    public final void setCubagem(String value) {
        cubagem.set(value);
    }

    public StringProperty cubagemProperty() {
        return cubagem;
    }

    public final String getTrib_ipi() {
        return trib_ipi.get();
    }

    public final void setTrib_ipi(String value) {
        trib_ipi.set(value);
    }

    public StringProperty trib_ipiProperty() {
        return trib_ipi;
    }

    public final String getTrib_pis() {
        return trib_pis.get();
    }

    public final void setTrib_pis(String value) {
        trib_pis.set(value);
    }

    public StringProperty trib_pisProperty() {
        return trib_pis;
    }

    public final String getTrib_cofins() {
        return trib_cofins.get();
    }

    public final void setTrib_cofins(String value) {
        trib_cofins.set(value);
    }

    public StringProperty trib_cofinsProperty() {
        return trib_cofins;
    }

    public final String getConta() {
        return conta.get();
    }

    public final void setConta(String value) {
        conta.set(value);
    }

    public StringProperty contaProperty() {
        return conta;
    }

    public final String getMd5() {
        return md5.get();
    }

    public final void setMd5(String value) {
        md5.set(value);
    }

    public StringProperty md5Property() {
        return md5;
    }

    public final String getPag_catalogo() {
        return pag_catalogo.get();
    }

    public final void setPag_catalogo(String value) {
        pag_catalogo.set(value);
    }

    public StringProperty pag_catalogoProperty() {
        return pag_catalogo;
    }

    public final String getAtualiza() {
        return atualiza.get();
    }

    public final void setAtualiza(String value) {
        atualiza.set(value);
    }

    public StringProperty atualizaProperty() {
        return atualiza;
    }

    public final String getLinha2() {
        return linha2.get();
    }

    public final void setLinha2(String value) {
        linha2.set(value);
    }

    public StringProperty linha2Property() {
        return linha2;
    }

    public final String getCelula() {
        return celula.get();
    }

    public final void setCelula(String value) {
        celula.set(value);
    }

    public StringProperty celulaProperty() {
        return celula;
    }

    public final String getFamilia() {
        return familia.get();
    }

    public final void setFamilia(String value) {
        familia.set(value);
    }

    public StringProperty familiaProperty() {
        return familia;
    }

    public final String getUsu_seqatu() {
        return usu_seqatu.get();
    }

    public final void setUsu_seqatu(String value) {
        usu_seqatu.set(value);
    }

    public StringProperty usu_seqatuProperty() {
        return usu_seqatu;
    }

    public final ObservableList<TableManutencaoPedidosCoresReferencia> getCores_saldos() {
        return cores_saldos.get();
    }

    public final void setCores_saldos(ObservableList<TableManutencaoPedidosCoresReferencia> value) {
        cores_saldos.set(value);
    }

    public ListProperty<TableManutencaoPedidosCoresReferencia> cores_saldosProperty() {
        return cores_saldos;
    }

}
