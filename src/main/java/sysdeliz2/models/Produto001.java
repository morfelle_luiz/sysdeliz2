/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import sysdeliz2.daos.DAOFactory;

import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class Produto001 {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final DoubleProperty desp_encai = new SimpleDoubleProperty();
    private final DoubleProperty minimo = new SimpleDoubleProperty();
    private final DoubleProperty peso = new SimpleDoubleProperty();
    private final DoubleProperty tempo = new SimpleDoubleProperty();
    private final DoubleProperty gramatura = new SimpleDoubleProperty();
    private final DoubleProperty ipi = new SimpleDoubleProperty();
    private final StringProperty data_cad = new SimpleStringProperty();
    private final DoubleProperty preco = new SimpleDoubleProperty();
    private final DoubleProperty preco_med = new SimpleDoubleProperty();
    private final DoubleProperty custo = new SimpleDoubleProperty();
    private final IntegerProperty quant = new SimpleIntegerProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty ativo = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty codfis = new SimpleStringProperty();
    private final StringProperty codtrib = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final DoubleProperty largura = new SimpleDoubleProperty();
    private final DoubleProperty compri = new SimpleDoubleProperty();
    private final DoubleProperty espessura = new SimpleDoubleProperty();
    private final DoubleProperty densidade = new SimpleDoubleProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty fornecedor = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty codigo2 = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty comp1 = new SimpleStringProperty();
    private final StringProperty comp2 = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty descricao2 = new SimpleStringProperty();
    private final StringProperty ficha = new SimpleStringProperty();
    private final StringProperty tabela = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final DoubleProperty preco_com = new SimpleDoubleProperty();
    private final DoubleProperty desp_cort = new SimpleDoubleProperty();
    private final DoubleProperty peso_talhado = new SimpleDoubleProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty obs1 = new SimpleStringProperty();
    private final StringProperty prototipo = new SimpleStringProperty();
    private final DoubleProperty icms = new SimpleDoubleProperty();
    private final StringProperty dt_entrega = new SimpleStringProperty();
    private final StringProperty dt_producao = new SimpleStringProperty();
    private final DoubleProperty custo_imp = new SimpleDoubleProperty();
    private final StringProperty uni_venda = new SimpleStringProperty();
    private final DoubleProperty peso_max = new SimpleDoubleProperty();
    private final IntegerProperty validade = new SimpleIntegerProperty();
    private final StringProperty origem = new SimpleStringProperty();
    private final StringProperty obs2 = new SimpleStringProperty();
    private final StringProperty etiqueta = new SimpleStringProperty();
    private final StringProperty obs3 = new SimpleStringProperty();
    private final StringProperty tributacao = new SimpleStringProperty();
    private final DoubleProperty preco_for = new SimpleDoubleProperty();
    private final DoubleProperty preco_repos = new SimpleDoubleProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty estilista = new SimpleStringProperty();
    private final StringProperty modelista = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty codsped = new SimpleStringProperty();
    private final DoubleProperty cubagem = new SimpleDoubleProperty();
    private final StringProperty trib_ipi = new SimpleStringProperty();
    private final StringProperty trib_pis = new SimpleStringProperty();
    private final StringProperty trib_cofins = new SimpleStringProperty();
    private final StringProperty conta = new SimpleStringProperty();
    private final StringProperty md5 = new SimpleStringProperty();
    private final StringProperty pag_catalogo = new SimpleStringProperty();
    private final StringProperty atualiza = new SimpleStringProperty();
    private final StringProperty linha2 = new SimpleStringProperty();
    private final StringProperty celula = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final ObjectProperty<Familia001> relFamilia = new SimpleObjectProperty<Familia001>();
    private final IntegerProperty usu_seqatu = new SimpleIntegerProperty();

    public Produto001(Double desp_encai, Double minimo, Double peso, Double tempo, Double gramatura, Double ipi, String data_cad,
            Double preco, Double preco_med, Double custo, Integer quant, String linha, String ativo, String colecao, String codfis,
            String codtrib, String grupo, String unidade, Double largura, Double compri, Double espessura, Double densidade,
            String local, String fornecedor, String codcli, String codigo2, String codigo, String comp1, String comp2, String descricao,
            String descricao2, String ficha, String tabela, String faixa, String marca, Double preco_com, Double desp_cort,
            Double peso_talhado, String obs, String obs1, String prototipo, Double icms, String dt_entrega, String dt_producao,
            Double custo_imp, String uni_venda, Double peso_max, Integer validade, String origem, String obs2, String etiqueta, String obs3,
            String tributacao, Double preco_for, Double preco_repos, String estoque, String estilista, String modelista, String status,
            String codsped, Double cubagem, String trib_ipi, String trib_pis, String trib_cofins, String conta, String md5, String pag_catalogo,
            String atualiza, String linha2, String celula, String familia, Integer usu_seqatu) {
        this.desp_encai.set(desp_encai);
        this.minimo.set(minimo);
        this.peso.set(peso);
        this.tempo.set(tempo);
        this.gramatura.set(gramatura);
        this.ipi.set(ipi);
        this.data_cad.set(data_cad);
        this.preco.set(preco);
        this.preco_med.set(preco_med);
        this.custo.set(custo);
        this.quant.set(quant);
        this.linha.set(linha);
        this.ativo.set(ativo);
        this.colecao.set(colecao);
        this.codfis.set(codfis);
        this.codtrib.set(codtrib);
        this.grupo.set(grupo);
        this.unidade.set(unidade);
        this.largura.set(largura);
        this.compri.set(compri);
        this.espessura.set(espessura);
        this.densidade.set(densidade);
        this.local.set(local);
        this.fornecedor.set(fornecedor);
        this.codcli.set(codcli);
        this.codigo2.set(codigo2);
        this.codigo.set(codigo);
        this.comp1.set(comp1);
        this.comp2.set(comp2);
        this.descricao.set(descricao);
        this.descricao2.set(descricao2);
        this.ficha.set(ficha);
        this.tabela.set(tabela);
        this.faixa.set(faixa);
        this.marca.set(marca);
        this.preco_com.set(preco_com);
        this.desp_cort.set(desp_cort);
        this.peso_talhado.set(peso_talhado);
        this.obs.set(obs);
        this.obs1.set(obs1);
        this.prototipo.set(prototipo);
        this.icms.set(icms);
        this.dt_entrega.set(dt_entrega);
        this.dt_producao.set(dt_producao);
        this.custo_imp.set(custo_imp);
        this.uni_venda.set(uni_venda);
        this.peso_max.set(peso_max);
        this.validade.set(validade);
        this.origem.set(origem);
        this.obs2.set(obs2);
        this.etiqueta.set(etiqueta);
        this.obs3.set(obs3);
        this.tributacao.set(tributacao);
        this.preco_for.set(preco_for);
        this.preco_repos.set(preco_repos);
        this.estoque.set(estoque);
        this.estilista.set(estilista);
        this.modelista.set(modelista);
        this.status.set(status);
        this.codsped.set(codsped);
        this.cubagem.set(cubagem);
        this.trib_ipi.set(trib_ipi);
        this.trib_pis.set(trib_pis);
        this.trib_cofins.set(trib_cofins);
        this.conta.set(conta);
        this.md5.set(md5);
        this.pag_catalogo.set(pag_catalogo);
        this.atualiza.set(atualiza);
        this.linha2.set(linha2);
        this.celula.set(celula);
        this.familia.set(familia);
        this.usu_seqatu.set(usu_seqatu);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final double getDesp_encai() {
        return desp_encai.get();
    }

    public final void setDesp_encai(double value) {
        desp_encai.set(value);
    }

    public DoubleProperty desp_encaiProperty() {
        return desp_encai;
    }

    public final double getMinimo() {
        return minimo.get();
    }

    public final void setMinimo(double value) {
        minimo.set(value);
    }

    public DoubleProperty minimoProperty() {
        return minimo;
    }

    public final double getPeso() {
        return peso.get();
    }

    public final void setPeso(double value) {
        peso.set(value);
    }

    public DoubleProperty pesoProperty() {
        return peso;
    }

    public final double getTempo() {
        return tempo.get();
    }

    public final void setTempo(double value) {
        tempo.set(value);
    }

    public DoubleProperty tempoProperty() {
        return tempo;
    }

    public final double getGramatura() {
        return gramatura.get();
    }

    public final void setGramatura(double value) {
        gramatura.set(value);
    }

    public DoubleProperty gramaturaProperty() {
        return gramatura;
    }

    public final double getIpi() {
        return ipi.get();
    }

    public final void setIpi(double value) {
        ipi.set(value);
    }

    public DoubleProperty ipiProperty() {
        return ipi;
    }

    public final String getData_cad() {
        return data_cad.get();
    }

    public final void setData_cad(String value) {
        data_cad.set(value);
    }

    public StringProperty data_cadProperty() {
        return data_cad;
    }

    public final double getPreco() {
        return preco.get();
    }

    public final void setPreco(double value) {
        preco.set(value);
    }

    public DoubleProperty precoProperty() {
        return preco;
    }

    public final double getPreco_med() {
        return preco_med.get();
    }

    public final void setPreco_med(double value) {
        preco_med.set(value);
    }

    public DoubleProperty preco_medProperty() {
        return preco_med;
    }

    public final double getCusto() {
        return custo.get();
    }

    public final void setCusto(double value) {
        custo.set(value);
    }

    public DoubleProperty custoProperty() {
        return custo;
    }

    public final int getQuant() {
        return quant.get();
    }

    public final void setQuant(int value) {
        quant.set(value);
    }

    public IntegerProperty quantProperty() {
        return quant;
    }

    public final String getLinha() {
        return linha.get();
    }

    public final void setLinha(String value) {
        linha.set(value);
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public final String getAtivo() {
        return ativo.get();
    }

    public final void setAtivo(String value) {
        ativo.set(value);
    }

    public StringProperty ativoProperty() {
        return ativo;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getCodfis() {
        return codfis.get();
    }

    public final void setCodfis(String value) {
        codfis.set(value);
    }

    public StringProperty codfisProperty() {
        return codfis;
    }

    public final String getCodtrib() {
        return codtrib.get();
    }

    public final void setCodtrib(String value) {
        codtrib.set(value);
    }

    public StringProperty codtribProperty() {
        return codtrib;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getUnidade() {
        return unidade.get();
    }

    public final void setUnidade(String value) {
        unidade.set(value);
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public final double getLargura() {
        return largura.get();
    }

    public final void setLargura(double value) {
        largura.set(value);
    }

    public DoubleProperty larguraProperty() {
        return largura;
    }

    public final double getCompri() {
        return compri.get();
    }

    public final void setCompri(double value) {
        compri.set(value);
    }

    public DoubleProperty compriProperty() {
        return compri;
    }

    public final double getEspessura() {
        return espessura.get();
    }

    public final void setEspessura(double value) {
        espessura.set(value);
    }

    public DoubleProperty espessuraProperty() {
        return espessura;
    }

    public final double getDensidade() {
        return densidade.get();
    }

    public final void setDensidade(double value) {
        densidade.set(value);
    }

    public DoubleProperty densidadeProperty() {
        return densidade;
    }

    public final String getLocal() {
        return local.get();
    }

    public final void setLocal(String value) {
        local.set(value);
    }

    public StringProperty localProperty() {
        return local;
    }

    public final String getFornecedor() {
        return fornecedor.get();
    }

    public final void setFornecedor(String value) {
        fornecedor.set(value);
    }

    public StringProperty fornecedorProperty() {
        return fornecedor;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getCodigo2() {
        return codigo2.get();
    }

    public final void setCodigo2(String value) {
        codigo2.set(value);
    }

    public StringProperty codigo2Property() {
        return codigo2;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getComp1() {
        return comp1.get();
    }

    public final void setComp1(String value) {
        comp1.set(value);
    }

    public StringProperty comp1Property() {
        return comp1;
    }

    public final String getComp2() {
        return comp2.get();
    }

    public final void setComp2(String value) {
        comp2.set(value);
    }

    public StringProperty comp2Property() {
        return comp2;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getDescricao2() {
        return descricao2.get();
    }

    public final void setDescricao2(String value) {
        descricao2.set(value);
    }

    public StringProperty descricao2Property() {
        return descricao2;
    }

    public final String getFicha() {
        return ficha.get();
    }

    public final void setFicha(String value) {
        ficha.set(value);
    }

    public StringProperty fichaProperty() {
        return ficha;
    }

    public final String getTabela() {
        return tabela.get();
    }

    public final void setTabela(String value) {
        tabela.set(value);
    }

    public StringProperty tabelaProperty() {
        return tabela;
    }

    public final String getFaixa() {
        return faixa.get();
    }

    public final void setFaixa(String value) {
        faixa.set(value);
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final double getPreco_com() {
        return preco_com.get();
    }

    public final void setPreco_com(double value) {
        preco_com.set(value);
    }

    public DoubleProperty preco_comProperty() {
        return preco_com;
    }

    public final double getDesp_cort() {
        return desp_cort.get();
    }

    public final void setDesp_cort(double value) {
        desp_cort.set(value);
    }

    public DoubleProperty desp_cortProperty() {
        return desp_cort;
    }

    public final double getPeso_talhado() {
        return peso_talhado.get();
    }

    public final void setPeso_talhado(double value) {
        peso_talhado.set(value);
    }

    public DoubleProperty peso_talhadoProperty() {
        return peso_talhado;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final String getObs1() {
        return obs1.get();
    }

    public final void setObs1(String value) {
        obs1.set(value);
    }

    public StringProperty obs1Property() {
        return obs1;
    }

    public final String getPrototipo() {
        return prototipo.get();
    }

    public final void setPrototipo(String value) {
        prototipo.set(value);
    }

    public StringProperty prototipoProperty() {
        return prototipo;
    }

    public final double getIcms() {
        return icms.get();
    }

    public final void setIcms(double value) {
        icms.set(value);
    }

    public DoubleProperty icmsProperty() {
        return icms;
    }

    public final String getDt_entrega() {
        return dt_entrega.get();
    }

    public final void setDt_entrega(String value) {
        dt_entrega.set(value);
    }

    public StringProperty dt_entregaProperty() {
        return dt_entrega;
    }

    public final String getDt_producao() {
        return dt_producao.get();
    }

    public final void setDt_producao(String value) {
        dt_producao.set(value);
    }

    public StringProperty dt_producaoProperty() {
        return dt_producao;
    }

    public final double getCusto_imp() {
        return custo_imp.get();
    }

    public final void setCusto_imp(double value) {
        custo_imp.set(value);
    }

    public DoubleProperty custo_impProperty() {
        return custo_imp;
    }

    public final String getUni_venda() {
        return uni_venda.get();
    }

    public final void setUni_venda(String value) {
        uni_venda.set(value);
    }

    public StringProperty uni_vendaProperty() {
        return uni_venda;
    }

    public final double getPeso_max() {
        return peso_max.get();
    }

    public final void setPeso_max(double value) {
        peso_max.set(value);
    }

    public DoubleProperty peso_maxProperty() {
        return peso_max;
    }

    public final int getValidade() {
        return validade.get();
    }

    public final void setValidade(int value) {
        validade.set(value);
    }

    public IntegerProperty validadeProperty() {
        return validade;
    }

    public final String getOrigem() {
        return origem.get();
    }

    public final void setOrigem(String value) {
        origem.set(value);
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public final String getObs2() {
        return obs2.get();
    }

    public final void setObs2(String value) {
        obs2.set(value);
    }

    public StringProperty obs2Property() {
        return obs2;
    }

    public final String getEtiqueta() {
        return etiqueta.get();
    }

    public final void setEtiqueta(String value) {
        etiqueta.set(value);
    }

    public StringProperty etiquetaProperty() {
        return etiqueta;
    }

    public final String getObs3() {
        return obs3.get();
    }

    public final void setObs3(String value) {
        obs3.set(value);
    }

    public StringProperty obs3Property() {
        return obs3;
    }

    public final String getTributacao() {
        return tributacao.get();
    }

    public final void setTributacao(String value) {
        tributacao.set(value);
    }

    public StringProperty tributacaoProperty() {
        return tributacao;
    }

    public final double getPreco_for() {
        return preco_for.get();
    }

    public final void setPreco_for(double value) {
        preco_for.set(value);
    }

    public DoubleProperty preco_forProperty() {
        return preco_for;
    }

    public final double getPreco_repos() {
        return preco_repos.get();
    }

    public final void setPreco_repos(double value) {
        preco_repos.set(value);
    }

    public DoubleProperty preco_reposProperty() {
        return preco_repos;
    }

    public final String getEstoque() {
        return estoque.get();
    }

    public final void setEstoque(String value) {
        estoque.set(value);
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public final String getEstilista() {
        return estilista.get();
    }

    public final void setEstilista(String value) {
        estilista.set(value);
    }

    public StringProperty estilistaProperty() {
        return estilista;
    }

    public final String getModelista() {
        return modelista.get();
    }

    public final void setModelista(String value) {
        modelista.set(value);
    }

    public StringProperty modelistaProperty() {
        return modelista;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getCodsped() {
        return codsped.get();
    }

    public final void setCodsped(String value) {
        codsped.set(value);
    }

    public StringProperty codspedProperty() {
        return codsped;
    }

    public final double getCubagem() {
        return cubagem.get();
    }

    public final void setCubagem(double value) {
        cubagem.set(value);
    }

    public DoubleProperty cubagemProperty() {
        return cubagem;
    }

    public final String getTrib_ipi() {
        return trib_ipi.get();
    }

    public final void setTrib_ipi(String value) {
        trib_ipi.set(value);
    }

    public StringProperty trib_ipiProperty() {
        return trib_ipi;
    }

    public final String getTrib_pis() {
        return trib_pis.get();
    }

    public final void setTrib_pis(String value) {
        trib_pis.set(value);
    }

    public StringProperty trib_pisProperty() {
        return trib_pis;
    }

    public final String getTrib_cofins() {
        return trib_cofins.get();
    }

    public final void setTrib_cofins(String value) {
        trib_cofins.set(value);
    }

    public StringProperty trib_cofinsProperty() {
        return trib_cofins;
    }

    public final String getConta() {
        return conta.get();
    }

    public final void setConta(String value) {
        conta.set(value);
    }

    public StringProperty contaProperty() {
        return conta;
    }

    public final String getMd5() {
        return md5.get();
    }

    public final void setMd5(String value) {
        md5.set(value);
    }

    public StringProperty md5Property() {
        return md5;
    }

    public final String getPag_catalogo() {
        return pag_catalogo.get();
    }

    public final void setPag_catalogo(String value) {
        pag_catalogo.set(value);
    }

    public StringProperty pag_catalogoProperty() {
        return pag_catalogo;
    }

    public final String getAtualiza() {
        return atualiza.get();
    }

    public final void setAtualiza(String value) {
        atualiza.set(value);
    }

    public StringProperty atualizaProperty() {
        return atualiza;
    }

    public final String getLinha2() {
        return linha2.get();
    }

    public final void setLinha2(String value) {
        linha2.set(value);
    }

    public StringProperty linha2Property() {
        return linha2;
    }

    public final String getCelula() {
        return celula.get();
    }

    public final void setCelula(String value) {
        celula.set(value);
    }

    public StringProperty celulaProperty() {
        return celula;
    }

    public final String getFamilia() {
        return familia.get();
    }

    public final void setFamilia(String value) {
        familia.set(value);
    }

    public StringProperty familiaProperty() {
        return familia;
    }

    public final Familia001 getRelFamilia() throws SQLException {
        relFamilia.set(DAOFactory.getFamilia001DAO().getByCode(this.familia.get()));
        return relFamilia.get();
    }

    public final void setRelFamilia(Familia001 value) {
        relFamilia.set(value);
    }

    public ObjectProperty<Familia001> relFamiliaProperty() throws SQLException {
        relFamilia.set(DAOFactory.getFamilia001DAO().getByCode(this.familia.get()));
        return relFamilia;
    }

    public final int getUsu_seqatu() {
        return usu_seqatu.get();
    }

    public final void setUsu_seqatu(int value) {
        usu_seqatu.set(value);
    }

    public IntegerProperty usu_seqatuProperty() {
        return usu_seqatu;
    }

}
