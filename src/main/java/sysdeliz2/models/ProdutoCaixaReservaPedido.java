/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class ProdutoCaixaReservaPedido {

    private final StringProperty strPedido = new SimpleStringProperty();
    private final StringProperty strReserva = new SimpleStringProperty();
    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strCor = new SimpleStringProperty();
    private final StringProperty strTam = new SimpleStringProperty();
    private final StringProperty strBarra = new SimpleStringProperty();
    private final StringProperty strDescricao = new SimpleStringProperty();
    private final StringProperty strTipo = new SimpleStringProperty();

    public ProdutoCaixaReservaPedido() {
    }

    public ProdutoCaixaReservaPedido(String pPedido, String pReserva, String pCodigo, String pCor, String pTam, String pBarra, String pDescricao, String pTipo) {
        this.strPedido.set(pPedido);
        this.strReserva.set(pReserva);
        this.strBarra.set(pBarra);
        this.strCodigo.set(pCodigo);
        this.strCor.set(pCor);
        this.strDescricao.set(pDescricao);
        this.strTam.set(pTam);
        this.strTipo.set(pTipo);
    }

    public final String getStrCodigo() {
        return strCodigo.get();
    }

    public final void setStrCodigo(String value) {
        strCodigo.set(value);
    }

    public StringProperty strCodigoProperty() {
        return strCodigo;
    }

    public final String getStrCor() {
        return strCor.get();
    }

    public final void setStrCor(String value) {
        strCor.set(value);
    }

    public StringProperty strCorProperty() {
        return strCor;
    }

    public final String getStrTam() {
        return strTam.get();
    }

    public final void setStrTam(String value) {
        strTam.set(value);
    }

    public StringProperty strTamProperty() {
        return strTam;
    }

    public final String getStrBarra() {
        return strBarra.get();
    }

    public final void setStrBarra(String value) {
        strBarra.set(value);
    }

    public StringProperty strBarraProperty() {
        return strBarra;
    }

    public final String getStrDescricao() {
        return strDescricao.get();
    }

    public final void setStrDescricao(String value) {
        strDescricao.set(value);
    }

    public StringProperty strDescricaoProperty() {
        return strDescricao;
    }

    public final String getStrTipo() {
        return strTipo.get();
    }

    public final void setStrTipo(String value) {
        strTipo.set(value);
    }

    public StringProperty strTipoProperty() {
        return strTipo;
    }
    
    public String getStrPedido() {
        return strPedido.get();
    }
    
    public StringProperty strPedidoProperty() {
        return strPedido;
    }
    
    public void setStrPedido(String strPedido) {
        this.strPedido.set(strPedido);
    }
    
    public String getStrReserva() {
        return strReserva.get();
    }
    
    public StringProperty strReservaProperty() {
        return strReserva;
    }
    
    public void setStrReserva(String strReserva) {
        this.strReserva.set(strReserva);
    }
    
    @Override
    public String toString() {
        return "ProdutoCaixaReservaPedido{" + "strCodigo=" + strCodigo + ", strCor=" + strCor + ", strTam=" + strTam + ", strBarra=" + strBarra + ", strDescricao=" + strDescricao + ", strTipo=" + strTipo + '}';
    }

}
