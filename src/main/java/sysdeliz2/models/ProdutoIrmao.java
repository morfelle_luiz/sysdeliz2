package sysdeliz2.models;

import sysdeliz2.models.ti.ProdCombo;
import sysdeliz2.models.view.VSdDadosProduto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoIrmao {

    private VSdDadosProduto produtoOriginal = new VSdDadosProduto();
    private String produtoIrmao = "";
    private List<ProdCombo> combos = new ArrayList<>();

    public ProdutoIrmao(VSdDadosProduto produtoOriginal, String produtoIrmao, List<ProdCombo> combos) {
        this.produtoOriginal = produtoOriginal;
        this.produtoIrmao = produtoIrmao;
        this.combos = combos;
    }

    public VSdDadosProduto getProdutoOriginal() {
        return produtoOriginal;
    }

    public void setProdutoOriginal(VSdDadosProduto produtoOriginal) {
        this.produtoOriginal = produtoOriginal;
    }

    public String getProdutoIrmao() {
        return produtoIrmao;
    }

    public void setProdutoIrmao(String produtoIrmao) {
        this.produtoIrmao = produtoIrmao;
    }

    public List<ProdCombo> getCombos() {
        return combos;
    }

    public void setCombos(List<ProdCombo> combos) {
        this.combos = combos;
    }
}
