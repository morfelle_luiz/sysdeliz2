/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class ProdutoLancamentoReserva {

    private final StringProperty strPedido = new SimpleStringProperty();
    private final StringProperty strReserva = new SimpleStringProperty();
    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strDescricao = new SimpleStringProperty();
    private final StringProperty strCor = new SimpleStringProperty();
    private final StringProperty strTam = new SimpleStringProperty();
    private final StringProperty strTipo = new SimpleStringProperty();
    private final StringProperty strDeposito = new SimpleStringProperty();
    private final BooleanProperty boolExiste = new SimpleBooleanProperty();
    private final BooleanProperty boolDoPedido = new SimpleBooleanProperty();
    private final IntegerProperty intSaldo = new SimpleIntegerProperty();
    private final IntegerProperty intOrdem = new SimpleIntegerProperty();
    private final IntegerProperty intQtde = new SimpleIntegerProperty();
    private final StringProperty infoExped = new SimpleStringProperty();

    public ProdutoLancamentoReserva() {
    }

    public ProdutoLancamentoReserva(String pPedido, String pReserva, String pCodigo, String pDescricao, String pCor, String pTam, String pTipo,
                                    String pDeposito, Integer pOrdem, String pDoPedido, String pExiste, Integer pSaldo, Integer pQtde, String infoExped) {
        this.intSaldo.set(pSaldo);
        this.strPedido.set(pPedido);
        this.strReserva.set(pReserva);
        this.strCodigo.set(pCodigo);
        this.strCor.set(pCor);
        this.strDescricao.set(pDescricao);
        this.strTipo.set(pTipo);
        this.strDeposito.set(pDeposito);
        this.intOrdem.set(pOrdem);
        this.boolExiste.set(pExiste.equals("S"));
        this.strTam.set(pTam);
        this.boolDoPedido.set(pDoPedido.equals("S"));
        this.intQtde.set(pQtde);
        this.infoExped.set(infoExped);
    }

    public final String getStrCodigo() {
        return strCodigo.get();
    }

    public final void setStrCodigo(String value) {
        strCodigo.set(value);
    }

    public StringProperty strCodigoProperty() {
        return strCodigo;
    }

    public final String getStrDescricao() {
        return strDescricao.get();
    }

    public final void setStrDescricao(String value) {
        strDescricao.set(value);
    }

    public StringProperty strDescricaoProperty() {
        return strDescricao;
    }

    public final String getStrCor() {
        return strCor.get();
    }

    public final void setStrCor(String value) {
        strCor.set(value);
    }

    public StringProperty strCorProperty() {
        return strCor;
    }

    public final String getStrTam() {
        return strTam.get();
    }

    public final void setStrTam(String value) {
        strTam.set(value);
    }

    public StringProperty strTamProperty() {
        return strTam;
    }

    public final int getIntSaldo() {
        return intSaldo.get();
    }

    public final void setIntSaldo(int value) {
        intSaldo.set(value);
    }

    public IntegerProperty intSaldoProperty() {
        return intSaldo;
    }

    public final boolean isBoolExiste() {
        return boolExiste.get();
    }

    public final void setBoolExiste(boolean value) {
        boolExiste.set(value);
    }

    public BooleanProperty boolExisteProperty() {
        return boolExiste;
    }

    public final boolean isBoolDoPedido() {
        return boolDoPedido.get();
    }

    public final void setBoolDoPedido(boolean value) {
        boolDoPedido.set(value);
    }

    public BooleanProperty boolDoPedidoProperty() {
        return boolDoPedido;
    }

    public final String getStrTipo() {
        return strTipo.get();
    }

    public final void setStrTipo(String value) {
        strTipo.set(value);
    }

    public StringProperty strTipoProperty() {
        return strTipo;
    }

    public final String getStrDeposito() {
        return strDeposito.get();
    }

    public final void setStrDeposito(String value) {
        strDeposito.set(value);
    }

    public StringProperty strDepositoProperty() {
        return strDeposito;
    }

    public final int getIntOrdem() {
        return intOrdem.get();
    }

    public final void setIntOrdem(int value) {
        intOrdem.set(value);
    }

    public IntegerProperty intOrdemProperty() {
        return intOrdem;
    }

    public final int getIntQtde() {
        return intQtde.get();
    }

    public final void setIntQtde(int value) {
        intQtde.set(value);
    }

    public IntegerProperty intQtdeProperty() {
        return intQtde;
    }

    public String getInfoExped() {
        return infoExped.get();
    }

    public StringProperty infoExpedProperty() {
        return infoExped;
    }

    public void setInfoExped(String infoExped) {
        this.infoExped.set(infoExped);
    }
    
    public String getStrPedido() {
        return strPedido.get();
    }
    
    public StringProperty strPedidoProperty() {
        return strPedido;
    }
    
    public void setStrPedido(String strPedido) {
        this.strPedido.set(strPedido);
    }
    
    public String getStrReserva() {
        return strReserva.get();
    }
    
    public StringProperty strReservaProperty() {
        return strReserva;
    }
    
    public void setStrReserva(String strReserva) {
        this.strReserva.set(strReserva);
    }
    
    @Override
    public String toString() {
        return "ProdutoLancamentoReserva{" + "strCodigo=" + strCodigo + ", strDescricao=" + strDescricao + ", strCor=" + strCor + ", strTam=" + strTam + ", strTipo=" + strTipo + ", strDeposito=" + strDeposito + ", boolExiste=" + boolExiste + ", boolDoPedido=" + boolDoPedido + ", intSaldo=" + intSaldo + ", intOrdem=" + intOrdem + ", intQtde=" + intQtde + '}';
    }

}
