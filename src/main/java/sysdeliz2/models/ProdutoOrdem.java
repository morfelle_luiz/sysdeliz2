/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

/**
 *
 * @author lima.joao
 */
public class ProdutoOrdem {

    private String codigo;
    private String cor;
    private String tamanho;
    private Double percentual;

    public ProdutoOrdem(String codigo, String cor, String tamanho, Double percentual){
        this.codigo = codigo;
        this.cor = cor;
        this.tamanho = tamanho;
        this.percentual = percentual;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public Double getPercentual() {
        return percentual;
    }

    public void setPercentual(Double percentual) {
        this.percentual = percentual;
    }

    public String getInsertUpdateSQL(){
        StringBuilder sb = new StringBuilder();

        sb
                .append("MERGE INTO produto_ordem_001 por")
                .append(" USING DUAL ON ( ")
                .append(" por.codigo = '" ).append(this.codigo ).append("'")  // -- tbTamanho.getValueAt(k, 0)
                .append(" AND por.cor = '").append(this.cor    ).append("'")  // -- tbCor.getValueAt(i, 2)
                .append(" AND por.tam = '").append(this.tamanho).append("'") // -- tbTamanho.getValueAt(k, 2)
                .append(" AND por.tipo = 'V'")
                .append(" )")
                .append(" WHEN MATCHED THEN ")
                .append(" UPDATE SET  ")
                .append(" por.percentual = ").append( percentual ).append("")
                .append(" ,por.ordem = ").append(Math.floor( percentual )).append("")
                .append(" ,por.desconsidera = ' '")
                .append(" WHEN NOT MATCHED THEN")
                .append(" INSERT (")
                .append(" CODIGO,")
                .append(" COR,")
                .append(" TAM,")
                .append(" ORDEM,")
                .append(" PERCENTUAL,")
                .append(" SEQUENCIA,")
                .append(" TIPO,")
                .append(" DESCONSIDERA,")
                .append(" NUM_OF,")
                .append(" ATIVO,")
                .append(" PARTIDA")
                .append(" )")
                .append(" VALUES( ")
                .append(" '").append(this.codigo).append("',")  // CODIGO
                .append(" '").append(this.cor).append("',")     // COR
                .append(" '").append(this.tamanho).append("',") // TAMANHO
                .append(" ").append(Math.floor( percentual )).append(",") // ORDEM
                .append(" ").append( percentual ).append(",")               // PERCENTUAL
                .append(" '").append("0").append("',") // SEQUENCIA
                .append(" '").append("V").append("',") // TIPO
                .append(" '").append(" ").append("',") // DESCONSIDERA
                .append(" '").append(" ").append("',") // NUM_OF
                .append(" '").append("S").append("',") // ATIVO
                .append(" '").append("0").append("'") // PARTIDA
                .append(")");
        System.out.println(sb.toString());
        return sb.toString();
    }
    
}
