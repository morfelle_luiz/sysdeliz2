/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class ProdutosMarketing {
    
    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strDescricao = new SimpleStringProperty();
    private final StringProperty strQtdEstoque = new SimpleStringProperty();

    public ProdutosMarketing() {
    }
    
    public ProdutosMarketing(String pCodigo, String pDescricao, String pQtdEstoque) {
        this.strCodigo.set(pCodigo);
        this.strDescricao.set(pDescricao);
        this.strQtdEstoque.set(pQtdEstoque);
    }

    public final String getStrCodigo() {
        return strCodigo.get();
    }

    public final void setStrCodigo(String value) {
        strCodigo.set(value);
    }

    public StringProperty strCodigoProperty() {
        return strCodigo;
    }

    public final String getStrDescricao() {
        return strDescricao.get();
    }

    public final void setStrDescricao(String value) {
        strDescricao.set(value);
    }

    public StringProperty strDescricaoProperty() {
        return strDescricao;
    }

    public final void setStrQtdEstoque(String value) {
        strQtdEstoque.set(value);
    }

    public StringProperty strQtdEstoqueProperty() {
        return strQtdEstoque;
    }

    @Override
    public String toString() {
        return "ProdutosMarketing{" + "strCodigo=" + strCodigo + ", strDescricao=" + strDescricao + ", strQtdEstoque=" + strQtdEstoque + '}';
    }
    
    
    
}
