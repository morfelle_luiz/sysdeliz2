/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class ProdutosMarketingPedido {

    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strUsuario = new SimpleStringProperty();
    private final StringProperty strStatus = new SimpleStringProperty();
    private final StringProperty strReserva = new SimpleStringProperty();
    private final StringProperty strDataCad = new SimpleStringProperty();
    private final StringProperty strDescProduto = new SimpleStringProperty();
    private final StringProperty strObs = new SimpleStringProperty();
    private final StringProperty intQtde = new SimpleStringProperty();

    public ProdutosMarketingPedido() {
    }

    public ProdutosMarketingPedido(String pCodigo, String pReserva, String pStatus, String pUsuario, String pDataCad, String pDescProduto, String pObs, Integer pQtde) {
        this.strCodigo.set(pCodigo);
        this.strDescProduto.set(pDescProduto);
        this.strObs.set(pObs);
        this.strDataCad.set(pDataCad);
        this.strUsuario.set(pUsuario);
        this.intQtde.set(pQtde.toString());
        this.strReserva.set(pReserva);
        this.strStatus.set(pStatus);
    }

    public final String getStrCodigo() {
        return strCodigo.get();
    }

    public final void setStrCodigo(String value) {
        strCodigo.set(value);
    }

    public StringProperty strCodigoProperty() {
        return strCodigo;
    }

    public final String getStrUsuario() {
        return strUsuario.get();
    }

    public final void setStrUsuario(String value) {
        strUsuario.set(value);
    }

    public StringProperty strUsuarioProperty() {
        return strUsuario;
    }

    public final String getStrDataCad() {
        return strDataCad.get();
    }

    public final void setStrDataCad(String value) {
        strDataCad.set(value);
    }

    public StringProperty strDataCadProperty() {
        return strDataCad;
    }

    public final String getStrDescProduto() {
        return strDescProduto.get();
    }

    public final void setStrDescProduto(String value) {
        strDescProduto.set(value);
    }

    public StringProperty strDescProdutoProperty() {
        return strDescProduto;
    }

    public final String getStrObs() {
        return strObs.get();
    }

    public final void setStrObs(String value) {
        strObs.set(value);
    }

    public StringProperty strObsProperty() {
        return strObs;
    }

    public final String getIntQtde() {
        return intQtde.get();
    }

    public final void setIntQtde(Integer value) {
        intQtde.set(value.toString());
    }

    public StringProperty intQtdeProperty() {
        return intQtde;
    }

    public final String getStrStatus() {
        return strStatus.get();
    }

    public final void setStrStatus(String value) {
        strStatus.set(value);
    }

    public StringProperty strStatusProperty() {
        return strStatus;
    }

    public final String getStrReserva() {
        return strReserva.get();
    }

    public final void setStrReserva(String value) {
        strReserva.set(value);
    }

    public StringProperty strReservaProperty() {
        return strReserva;
    }

    @Override
    public String toString() {
        return "ProdutosMarketingPedido{" + "strCodigo=" + strCodigo + ", strUsuario=" + strUsuario + ", strStatus=" + strStatus + ", strReserva=" + strReserva + ", strDataCad=" + strDataCad + ", strDescProduto=" + strDescProduto + ", strObs=" + strObs + ", intQtde=" + intQtde + '}';
    }

}
