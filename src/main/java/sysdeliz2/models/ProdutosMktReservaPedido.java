/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

/**
 *
 * @author cristiano.diego
 */
public class ProdutosMktReservaPedido {

    private String strLocal;
    private String strCodigo;
    private String strDescCodigo;
    private Integer intQtde;
    private String strObs;

    public ProdutosMktReservaPedido() {
    }

    public ProdutosMktReservaPedido(String strLocal, String strCodigo, Integer intQtde, String strDescCodigo, String strObs) {
        this.strLocal = strLocal;
        this.strCodigo = strCodigo;
        this.strDescCodigo = strDescCodigo;
        this.intQtde = intQtde;
        this.strObs = strObs;
    }

    public String getStrLocal() {
        return strLocal;
    }

    public void setStrLocal(String strLocal) {
        this.strLocal = strLocal;
    }

    public String getStrCodigo() {
        return strCodigo;
    }

    public void setStrCodigo(String strCodigo) {
        this.strCodigo = strCodigo;
    }

    public String getStrDescCodigo() {
        return strDescCodigo;
    }

    public void setStrDescCodigo(String strDescCodigo) {
        this.strDescCodigo = strDescCodigo;
    }

    public Integer getIntQtde() {
        return intQtde;
    }

    public void setIntQtde(Integer intQtde) {
        this.intQtde = intQtde;
    }

    public String getStrObs() {
        return strObs;
    }

    public void setStrObs(String strObs) {
        this.strObs = strObs;
    }

    public ProdutosMktReservaPedido getMe() {
        return this;
    }

    @Override
    public String toString() {
        return "ProdutosMktReservaPedido{" + "strLocal=" + strLocal + ", strCodigo=" + strCodigo + ", strDescCodigo=" + strDescCodigo + ", intQtde=" + intQtde + ", strObs=" + strObs + '}';
    }

}
