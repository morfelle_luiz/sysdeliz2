/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class ProdutosReservaPedido {

    private final StringProperty strCodigo = new SimpleStringProperty();
    private final StringProperty strPedido = new SimpleStringProperty();
    private final StringProperty strReserva = new SimpleStringProperty();
    private final StringProperty strCor = new SimpleStringProperty();
    private final StringProperty strTam = new SimpleStringProperty();
    private final StringProperty strLocal = new SimpleStringProperty();
    private final StringProperty strDeposito = new SimpleStringProperty();
    private final IntegerProperty intQtde = new SimpleIntegerProperty();
    private final IntegerProperty intSaldo = new SimpleIntegerProperty();
    private final IntegerProperty intFaixa = new SimpleIntegerProperty();

    public ProdutosReservaPedido() {
    }

    public ProdutosReservaPedido(String pCodigo, String pCor, String pTam, Integer pQtde, Integer pFaixa, String pLocal, String pDeposito) {
        this.strCodigo.set(pCodigo);
        this.strCor.set(pCor);
        this.strTam.set(pTam);
        this.intQtde.set(pQtde);
        this.intFaixa.set(pFaixa);
        this.strLocal.set(pLocal);
        this.strDeposito.set(pDeposito);
    }

    public ProdutosReservaPedido(String pCodigo, String pCor, String pTam, Integer pQtde, Integer pFaixa, String pLocal, String pDeposito, Integer pSaldo) {
        this.strCodigo.set(pCodigo);
        this.strCor.set(pCor);
        this.strTam.set(pTam);
        this.intQtde.set(pQtde);
        this.intFaixa.set(pFaixa);
        this.strLocal.set(pLocal);
        this.strDeposito.set(pDeposito);
        this.intSaldo.set(pSaldo);
    }

    public final String getStrCodigo() {
        return strCodigo.get();
    }

    public final void setStrCodigo(String value) {
        strCodigo.set(value);
    }

    public StringProperty strCodigoProperty() {
        return strCodigo;
    }

    public final String getStrCor() {
        return strCor.get();
    }

    public final void setStrCor(String value) {
        strCor.set(value);
    }

    public StringProperty strCorProperty() {
        return strCor;
    }

    public final String getStrTam() {
        return strTam.get();
    }

    public final void setStrTam(String value) {
        strTam.set(value);
    }

    public StringProperty strTamProperty() {
        return strTam;
    }

    public final int getIntQtde() {
        return intQtde.get();
    }

    public final void setIntQtde(int value) {
        intQtde.set(value);
    }

    public IntegerProperty intQtdeProperty() {
        return intQtde;
    }

    public final int getIntFaixa() {
        return intFaixa.get();
    }

    public final void setIntFaixa(int value) {
        intFaixa.set(value);
    }

    public IntegerProperty intFaixaProperty() {
        return intFaixa;
    }

    public final String getStrLocal() {
        return strLocal.get();
    }

    public final void setStrLocal(String value) {
        strLocal.set(value);
    }

    public StringProperty strLocalProperty() {
        return strLocal;
    }

    public final int getIntSaldo() {
        return intSaldo.get();
    }

    public final void setIntSaldo(int value) {
        intSaldo.set(value);
    }

    public IntegerProperty intSaldoProperty() {
        return intSaldo;
    }

    public final String getStrDeposito() {
        return strDeposito.get();
    }

    public final void setStrDeposito(String value) {
        strDeposito.set(value);
    }

    public StringProperty strDepositoProperty() {
        return strDeposito;
    }
    
    public String getStrPedido() {
        return strPedido.get();
    }
    
    public StringProperty strPedidoProperty() {
        return strPedido;
    }
    
    public void setStrPedido(String strPedido) {
        this.strPedido.set(strPedido);
    }
    
    public String getStrReserva() {
        return strReserva.get();
    }
    
    public StringProperty strReservaProperty() {
        return strReserva;
    }
    
    public void setStrReserva(String strReserva) {
        this.strReserva.set(strReserva);
    }
    
    @Override
    public String toString() {
        return "ProdutosReservaPedido{" + "strCodigo=" + strCodigo + ", strCor=" + strCor + ", strTam=" + strTam + ", strLocal=" + strLocal + ", strDeposito=" + strDeposito + ", intQtde=" + intQtde + ", intSaldo=" + intSaldo + ", intFaixa=" + intFaixa + '}';
    }
}
