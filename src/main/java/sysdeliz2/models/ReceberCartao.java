/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class ReceberCartao {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty payment_id = new SimpleStringProperty();
    private final StringProperty authorization_code = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final DoubleProperty valor = new SimpleDoubleProperty();
    private final StringProperty card_number = new SimpleStringProperty();
    private final StringProperty brand = new SimpleStringProperty();
    private final IntegerProperty parcelas = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty data_receber = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();

    public ReceberCartao() {
    }

    public ReceberCartao(String numero, String payment_id, String auth_code, String pedido, Double valor,
            String card_number, String brand, Integer parcelas, String status, String data_receber,
            String codcli, String nome, String agrupado) {
        this.numero.set(numero);
        this.payment_id.set(payment_id);
        this.authorization_code.set(auth_code);
        this.pedido.set(agrupado != null ? "AGRUP [" + pedido + "," + agrupado + "]" : pedido);
        this.valor.set(valor);
        this.card_number.set(card_number);
        this.brand.set(brand);
        this.parcelas.set(parcelas);
        this.status.set(status);
        this.data_receber.set(data_receber);
        this.codcli.set(codcli);
        this.nome.set(nome);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getPayment_id() {
        return payment_id.get();
    }

    public final void setPayment_id(String value) {
        payment_id.set(value);
    }

    public StringProperty payment_idProperty() {
        return payment_id;
    }

    public final String getAuthorization_code() {
        return authorization_code.get();
    }

    public final void setAuthorization_code(String value) {
        authorization_code.set(value);
    }

    public StringProperty authorization_codeProperty() {
        return authorization_code;
    }

    public final String getPedido() {
        return pedido.get();
    }

    public final void setPedido(String value) {
        pedido.set(value);
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public final double getValor() {
        return valor.get();
    }

    public final void setValor(double value) {
        valor.set(value);
    }

    public DoubleProperty valorProperty() {
        return valor;
    }

    public final String getCard_number() {
        return card_number.get();
    }

    public final void setCard_number(String value) {
        card_number.set(value);
    }

    public StringProperty card_numberProperty() {
        return card_number;
    }

    public final String getBrand() {
        return brand.get();
    }

    public final void setBrand(String value) {
        brand.set(value);
    }

    public StringProperty brandProperty() {
        return brand;
    }

    public final int getParcelas() {
        return parcelas.get();
    }

    public final void setParcelas(int value) {
        parcelas.set(value);
    }

    public IntegerProperty parcelasProperty() {
        return parcelas;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getData_receber() {
        return data_receber.get();
    }

    public final void setData_receber(String value) {
        data_receber.set(value);
    }

    public StringProperty data_receberProperty() {
        return data_receber;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

}
