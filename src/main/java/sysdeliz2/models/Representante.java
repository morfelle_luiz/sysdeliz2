/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class Representante {

    BooleanProperty selected = new SimpleBooleanProperty(false);
    StringProperty codigo = new SimpleStringProperty();
    StringProperty representante = new SimpleStringProperty();
    StringProperty responsavel = new SimpleStringProperty();
    StringProperty regiao = new SimpleStringProperty();
    StringProperty cidade = new SimpleStringProperty();
    StringProperty gerente = new SimpleStringProperty();

    public Representante(String codigo, String representante, String responsavel, String regiao, String cidade, String gerente) {
        this.codigo.set(codigo);
        this.representante.set(representante);
        this.responsavel.set(responsavel);
        this.regiao.set(regiao);
        this.cidade.set(cidade);
        this.gerente.set(gerente);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getRepresentante() {
        return representante.get();
    }

    public final void setRepresentante(String value) {
        representante.set(value);
    }

    public StringProperty representanteProperty() {
        return representante;
    }

    public final String getResponsavel() {
        return responsavel.get();
    }

    public final void setResponsavel(String value) {
        responsavel.set(value);
    }

    public StringProperty responsavelProperty() {
        return responsavel;
    }

    public final String getRegiao() {
        return regiao.get();
    }

    public final void setRegiao(String value) {
        regiao.set(value);
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getGerente() {
        return gerente.get();
    }

    public final void setGerente(String value) {
        gerente.set(value);
    }

    public StringProperty gerenteProperty() {
        return gerente;
    }

    @Override
    public String toString() {
        return "Representantes{" + "codigo=" + codigo + ", representante=" + representante + ", responsavel=" + responsavel + ", regiao=" + regiao + ", cidade=" + cidade + ", gerente=" + gerente + '}';
    }

}
