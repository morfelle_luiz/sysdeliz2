/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.utils.GUIUtils;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
public class ReservaPedido {

    private final StringProperty strNumeroPedido = new SimpleStringProperty();
    private final StringProperty strCliente = new SimpleStringProperty();
    private final StringProperty strNumeroReserva = new SimpleStringProperty();
    private final StringProperty strDeposito = new SimpleStringProperty();
    private final StringProperty strPercenteDesc = new SimpleStringProperty();
    private final StringProperty strMarca = new SimpleStringProperty();
    private final StringProperty strPedAgrupador = new SimpleStringProperty();
    private final StringProperty strStatus = new SimpleStringProperty();
    private final StringProperty strDescStatus = new SimpleStringProperty();
    private final IntegerProperty intQtde = new SimpleIntegerProperty();
    private final StringProperty strTransportadora = new SimpleStringProperty();
    private final StringProperty obsExpedicao = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty pgto = new SimpleStringProperty();
    private final ListProperty<ProdutosReservaPedido> listProdutos = new SimpleListProperty<>();

    public ReservaPedido() {
    }

    public ReservaPedido(String pNumeroPedido, String pNumeroReserva, String pPedidoAgrupador, String pDeposito, String pStatus, Integer pQtde, String pCliente, String pMarca, String pPerDesc, String pRepresentante, String pPrazo) {
        this.strNumeroReserva.set(pNumeroReserva);
        this.strNumeroPedido.set(pNumeroPedido);
        this.strPedAgrupador.set(pPedidoAgrupador);
        this.strDeposito.set(pDeposito);
        this.strStatus.set(pStatus);
        this.intQtde.set(pQtde);
        this.strCliente.set(pCliente);
        this.strPercenteDesc.set(pPerDesc);
        this.strDescStatus.set(GUIUtils.getStatusReserva(pStatus));
        this.strMarca.set(pMarca);
        this.codrep.set(pRepresentante);
        this.pgto.set(pPrazo);
    }

    public ReservaPedido(String pNumeroPedido, String pNumeroReserva, String pPedidoAgrupador, String pDeposito, String pStatus, Integer pQtde, String pCliente, String pMarca, String pPerDesc, String pTransportadora, String obsExpedicao, String pRepresentante, String pPrazo) {
        this.strNumeroReserva.set(pNumeroReserva);
        this.strNumeroPedido.set(pNumeroPedido);
        this.strPedAgrupador.set(pPedidoAgrupador);
        this.strDeposito.set(pDeposito);
        this.strStatus.set(pStatus);
        this.intQtde.set(pQtde);
        this.strCliente.set(pCliente);
        this.strMarca.set(pMarca);
        this.codrep.set(pRepresentante);
        this.pgto.set(pPrazo);
        this.strPercenteDesc.set(pPerDesc);
        this.strTransportadora.set(pTransportadora);
        this.obsExpedicao.set(obsExpedicao);
        this.strDescStatus.set(GUIUtils.getStatusReserva(pStatus));
    }

    @Deprecated
    public ReservaPedido(String pNumeroPedido, String pNumeroReserva, String pDeposito, String pStatus, Integer pQtde, String pCliente, List<ProdutosReservaPedido> pProdutos) {
        this.strNumeroReserva.set(pNumeroReserva);
        this.strNumeroPedido.set(pNumeroPedido);
        this.strDeposito.set(pDeposito);
        this.strStatus.set(pStatus);
        this.strDescStatus.set(GUIUtils.getStatusReserva(pStatus));
        this.intQtde.set(pQtde);
        this.strCliente.set(pCliente);
        this.listProdutos.set(FXCollections.observableArrayList(pProdutos));
    }

    @Deprecated
    public ReservaPedido(String pNumeroPedido, String pNumeroReserva, String pDeposito, String pStatus, Integer pQtde, String pCliente, String pTransportadora, List<ProdutosReservaPedido> pProdutos) {
        this.strNumeroReserva.set(pNumeroReserva);
        this.strNumeroPedido.set(pNumeroPedido);
        this.strDeposito.set(pDeposito);
        this.strStatus.set(pStatus);
        this.strDescStatus.set(GUIUtils.getStatusReserva(pStatus));
        this.intQtde.set(pQtde);
        this.strCliente.set(pCliente);
        this.strTransportadora.set(pTransportadora);
        this.listProdutos.set(FXCollections.observableArrayList(pProdutos));
    }

    public final String getStrNumeroPedido() {
        return strNumeroPedido.get();
    }

    public final void setStrNumeroPedido(String value) {
        strNumeroPedido.set(value);
    }

    public StringProperty strNumeroPedidoProperty() {
        return strNumeroPedido;
    }

    public final String getStrNumeroReserva() {
        return strNumeroReserva.get();
    }

    public final void setStrNumeroReserva(String value) {
        strNumeroReserva.set(value);
    }

    public StringProperty strNumeroReservaProperty() {
        return strNumeroReserva;
    }

    public final String getStrDeposito() {
        return strDeposito.get();
    }

    public final void setStrDeposito(String value) {
        strDeposito.set(value);
    }

    public StringProperty strDepositoProperty() {
        return strDeposito;
    }

    public final String getStrStatus() {
        return strStatus.get();
    }

    public final void setStrStatus(String value) {
        strStatus.set(value);
    }

    public StringProperty strStatusProperty() {
        return strStatus;
    }

    public final String getStrCliente() {
        return strCliente.get();
    }

    public final void setStrCliente(String value) {
        strCliente.set(value);
    }

    public StringProperty strClienteProperty() {
        return strCliente;
    }

    public final int getIntQtde() {
        return intQtde.get();
    }

    public final void setIntQtde(int value) {
        intQtde.set(value);
    }

    public IntegerProperty intQtdeProperty() {
        return intQtde;
    }

    public final ObservableList<ProdutosReservaPedido> getListProdutos() {
        return listProdutos.get();
    }

    public final void setListProdutos(ObservableList<ProdutosReservaPedido> value) {
        listProdutos.set(value);
    }

    public ListProperty<ProdutosReservaPedido> listProdutosProperty() {
        return listProdutos;
    }

    public final String getStrDescStatus() {
        return strDescStatus.get();
    }

    public final void setStrDescStatus(String value) {
        strDescStatus.set(value);
    }

    public StringProperty strDescStatusProperty() {
        return strDescStatus;
    }

    public final String getStrTransportadora() {
        return strTransportadora.get();
    }

    public final void setStrTransportadora(String value) {
        strTransportadora.set(value);
    }

    public StringProperty strTransportadoraProperty() {
        return strTransportadora;
    }

    public String getObsExpedicao() {
        return obsExpedicao.get();
    }

    public StringProperty obsExpedicaoProperty() {
        return obsExpedicao;
    }

    public void setObsExpedicao(String obsExpedicao) {
        this.obsExpedicao.set(obsExpedicao);
    }
    
    public String getStrMarca() {
        return strMarca.get();
    }
    
    public StringProperty strMarcaProperty() {
        return strMarca;
    }
    
    public void setStrMarca(String strMarca) {
        this.strMarca.set(strMarca);
    }
    
    public String getStrPedAgrupador() {
        return strPedAgrupador.get();
    }
    
    public StringProperty strPedAgrupadorProperty() {
        return strPedAgrupador;
    }
    
    public void setStrPedAgrupador(String strPedAgrupador) {
        this.strPedAgrupador.set(strPedAgrupador);
    }
    
    public String getStrPercenteDesc() {
        return strPercenteDesc.get();
    }
    
    public StringProperty strPercenteDescProperty() {
        return strPercenteDesc;
    }
    
    public void setStrPercenteDesc(String strPercenteDesc) {
        this.strPercenteDesc.set(strPercenteDesc);
    }
    
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    public String getPgto() {
        return pgto.get();
    }
    
    public StringProperty pgtoProperty() {
        return pgto;
    }
    
    public void setPgto(String pgto) {
        this.pgto.set(pgto);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.strNumeroPedido);
        hash = 11 * hash + Objects.hashCode(this.strNumeroReserva);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReservaPedido other = (ReservaPedido) obj;
        if (!Objects.equals(this.strNumeroPedido, other.strNumeroPedido)) {
            return false;
        }
        return Objects.equals(this.strNumeroReserva, other.strNumeroReserva);
    }

    @Override
    public String toString() {
        return "ReservaPedido{" + "strNumeroPedido=" + strNumeroPedido + ", strCliente=" + strCliente + ", strNumeroReserva=" + strNumeroReserva + ", strDeposito=" + strDeposito + ", strStatus=" + strStatus + ", intQtde=" + intQtde + '}';
    }

}
