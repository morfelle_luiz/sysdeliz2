/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author cristiano.diego
 */
public class SdAgendaServico001 {

    private final ObjectProperty<LocalDateTime> dtCadastro = new SimpleObjectProperty<LocalDateTime>();
    private final ObjectProperty<LocalDate> dtAgenda = new SimpleObjectProperty<LocalDate>();
    private final StringProperty hrAgenda = new SimpleStringProperty();
    private final StringProperty dtFullAgenda = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty campo1 = new SimpleStringProperty();
    private final StringProperty campo2 = new SimpleStringProperty();
    private final StringProperty campo3 = new SimpleStringProperty();
    private final StringProperty campo4 = new SimpleStringProperty();
    private final StringProperty campo5 = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();

    public SdAgendaServico001() {
    }

    public SdAgendaServico001(String dtCadastro, String dtAgenda, String hrAgenda, String tipo, String campo1,
            String campo2, String campo3, String campo4, String campo5, String usuario, String dtFullAgenda) {
        this.dtCadastro.set(LocalDateTime.parse(dtCadastro != null ? dtCadastro : "01/01/1889", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        this.dtAgenda.set(LocalDate.parse(dtAgenda != null ? dtAgenda : "01/01/1889", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.hrAgenda.set(hrAgenda);
        this.tipo.set(tipo);
        this.campo1.set(campo1);
        this.campo2.set(campo2);
        this.campo3.set(campo3);
        this.campo4.set(campo4);
        this.campo5.set(campo5);
        this.usuario.set(usuario);
        this.dtFullAgenda.set(dtFullAgenda);
    }

    public SdAgendaServico001(SdAgendaServico001 toCopy) {
        this.dtCadastro.set(toCopy.getDtCadastro());
        this.dtAgenda.set(toCopy.getDtAgenda());
        this.hrAgenda.set(toCopy.hrAgenda.get());
        this.tipo.set(toCopy.tipo.get());
        this.campo1.set(toCopy.campo1.get());
        this.campo2.set(toCopy.campo2.get());
        this.campo3.set(toCopy.campo3.get());
        this.campo4.set(toCopy.campo4.get());
        this.campo5.set(toCopy.campo5.get());
        this.usuario.set(toCopy.usuario.get());
        this.dtFullAgenda.set(toCopy.dtFullAgenda.get());
    }

    public final LocalDateTime getDtCadastro() {
        return dtCadastro.get();
    }

    public final String getDtCadastroAsString() {
        String strDate = (null == dtCadastro || null == dtCadastro.get())
                ? "" : dtCadastro.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

        return strDate;
    }

    public final void setDtCadastro(LocalDateTime value) {
        dtCadastro.set(value);
    }

    public ObjectProperty<LocalDateTime> dtCadastroProperty() {
        return dtCadastro;
    }

    public StringProperty dtCadastroPropertyAsString() {
        return new SimpleStringProperty(dtCadastro.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }

    public final LocalDate getDtAgenda() {
        return dtAgenda.get();
    }

    public final String getDtAgendaAsString() {
        String strDate = (null == dtAgenda || null == dtAgenda.get())
                ? "" : dtAgenda.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return strDate;
    }

    public final void setDtAgenda(LocalDate value) {
        dtAgenda.set(value);
    }

    public ObjectProperty<LocalDate> dtAgendaProperty() {
        return dtAgenda;
    }

    public final String getHrAgenda() {
        return hrAgenda.get();
    }

    public final void setHrAgenda(String value) {
        hrAgenda.set(value);
    }

    public StringProperty hrAgendaProperty() {
        return hrAgenda;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final String getCampo1() {
        return campo1.get();
    }

    public final void setCampo1(String value) {
        campo1.set(value);
    }

    public StringProperty campo1Property() {
        return campo1;
    }

    public final String getCampo2() {
        return campo2.get();
    }

    public final void setCampo2(String value) {
        campo2.set(value);
    }

    public StringProperty campo2Property() {
        return campo2;
    }

    public final String getCampo3() {
        return campo3.get();
    }

    public final void setCampo3(String value) {
        campo3.set(value);
    }

    public StringProperty campo3Property() {
        return campo3;
    }

    public final String getCampo4() {
        return campo4.get();
    }

    public final void setCampo4(String value) {
        campo4.set(value);
    }

    public StringProperty campo4Property() {
        return campo4;
    }

    public final String getCampo5() {
        return campo5.get();
    }

    public final void setCampo5(String value) {
        campo5.set(value);
    }

    public StringProperty campo5Property() {
        return campo5;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getDtFullAgenda() {
        return dtFullAgenda.get();
    }

    public final void setDtFullAgenda(String value) {
        dtFullAgenda.set(value);
    }

    public StringProperty dtFullAgendaProperty() {
        return dtFullAgenda;
    }

}
