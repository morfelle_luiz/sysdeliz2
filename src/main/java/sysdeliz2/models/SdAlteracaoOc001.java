/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author cristiano.diego
 */
public class SdAlteracaoOc001 {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtAnterior = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtNovo = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDateTime> dtAtualizacao = new SimpleObjectProperty<LocalDateTime>();

    public SdAlteracaoOc001() {
    }

    public SdAlteracaoOc001(String numero, String codigo, String cor, String tipo, String status,
            String deposito, Integer ordem, LocalDateTime dtAtualizacao, LocalDate dtAnterior, LocalDate dtNovo) {
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.deposito.set(deposito);
        this.ordem.set(ordem);
        this.tipo.set(tipo);
        this.status.set(status);
        this.dtAtualizacao.set(dtAtualizacao);
        this.dtAnterior.set(dtAnterior);
        this.dtNovo.set(dtNovo);
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getDeposito() {
        return deposito.get();
    }

    public final void setDeposito(String value) {
        deposito.set(value);
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final int getOrdem() {
        return ordem.get();
    }

    public final void setOrdem(int value) {
        ordem.set(value);
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public final LocalDate getDtAnterior() {
        return dtAnterior.get();
    }

    public final void setDtAnterior(LocalDate value) {
        dtAnterior.set(value);
    }

    public ObjectProperty<LocalDate> dtAnteriorProperty() {
        return dtAnterior;
    }

    public final LocalDate getDtNovo() {
        return dtNovo.get();
    }

    public final void setDtNovo(LocalDate value) {
        dtNovo.set(value);
    }

    public ObjectProperty<LocalDate> dtNovoProperty() {
        return dtNovo;
    }

    public final LocalDateTime getDtAtualizacao() {
        return dtAtualizacao.get();
    }

    public final void setDtAtualizacao(LocalDateTime value) {
        dtAtualizacao.set(value);
    }

    public ObjectProperty<LocalDateTime> dtAtualizacaoProperty() {
        return dtAtualizacao;
    }

}
