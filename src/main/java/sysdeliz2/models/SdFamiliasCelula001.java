/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class SdFamiliasCelula001 {

    private final BooleanProperty selected = new SimpleBooleanProperty();
    private final IntegerProperty celula = new SimpleIntegerProperty();
    private final StringProperty familiaGrp = new SimpleStringProperty();
    private final ObjectProperty<FamiliaGrupo001> familia = new SimpleObjectProperty<>();

    public SdFamiliasCelula001() {
    }

    public SdFamiliasCelula001(Integer celula, String familiaGrp) {
        this.celula.set(celula);
        this.familiaGrp.set(familiaGrp);
    }

    public SdFamiliasCelula001(Integer celula, String familiaGrp, FamiliaGrupo001 familia) {
        this.celula.set(celula);
        this.familiaGrp.set(familiaGrp);
        this.familia.set(familia);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final int getCelula() {
        return celula.get();
    }

    public final void setCelula(int value) {
        celula.set(value);
    }

    public IntegerProperty celulaProperty() {
        return celula;
    }

    public final String getFamiliaGrp() {
        return familiaGrp.get();
    }

    public final void setFamiliaGrp(String value) {
        familiaGrp.set(value);
    }

    public StringProperty familiaGrpProperty() {
        return familiaGrp;
    }

    public final FamiliaGrupo001 getFamilia() {
        return familia.get();
    }

    public final void setFamilia(FamiliaGrupo001 value) {
        familia.set(value);
    }

    public ObjectProperty<FamiliaGrupo001> familiaProperty() {
        return familia;
    }

}
