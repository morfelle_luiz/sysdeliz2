/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
public class SdFuncao001 {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();

    public SdFuncao001() {
        this.codigo.set(0);
    }

    public SdFuncao001(Integer codigo, String descricao) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
    }

    public final int getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(int value) {
        codigo.set(value);
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void copy(SdFuncao001 function) {
        this.codigo.set(function.codigo.get());
        this.descricao.set(function.descricao.get());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdFuncao001 other = (SdFuncao001) obj;
        if (!Objects.equals(this.codigo.get(), other.codigo.get())) {
            return false;
        }
        return true;
    }

}
