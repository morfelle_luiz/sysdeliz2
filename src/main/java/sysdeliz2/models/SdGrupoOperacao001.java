/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
public class SdGrupoOperacao001 {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final ListProperty<SdOperacoesGrp001> operacoes = new SimpleListProperty<>();

    public SdGrupoOperacao001() {
    }

    public SdGrupoOperacao001(Integer codigo, String descricao, String ativo) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.ativo.set(ativo.equals("S"));
    }

    public SdGrupoOperacao001(Integer codigo, String descricao, String ativo, ObservableList<SdOperacoesGrp001> operacoes) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.ativo.set(ativo.equals("S"));
        this.operacoes.set(operacoes);
    }

    public final int getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(int value) {
        codigo.set(value);
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final boolean isAtivo() {
        return ativo.get();
    }

    public final void setAtivo(boolean value) {
        ativo.set(value);
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public final ObservableList<SdOperacoesGrp001> getOperacoes() {
        return operacoes.get();
    }

    public final void setOperacoes(ObservableList<SdOperacoesGrp001> value) {
        operacoes.set(value);
    }

    public ListProperty<SdOperacoesGrp001> operacoesProperty() {
        return operacoes;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void copy(SdGrupoOperacao001 operationGroup) {
        this.codigo.set(operationGroup.codigo.get());
        this.descricao.set(operationGroup.descricao.get());
        this.ativo.set(operationGroup.ativo.get());
        this.operacoes.set(operationGroup.operacoes.get());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdGrupoOperacao001 other = (SdGrupoOperacao001) obj;
        if (!Objects.equals(this.codigo.get(), other.codigo.get())) {
            return false;
        }
        return true;
    }

}
