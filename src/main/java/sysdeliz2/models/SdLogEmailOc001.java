/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class SdLogEmailOc001 {

    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty dtEnvio = new SimpleStringProperty();
    private final StringProperty dtFaturamento = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();

    public SdLogEmailOc001() {
    }

    public SdLogEmailOc001(String codcli, String numero, String codigo, String cor,
            String dtEnvio, String dtFaturamento, String email) {
        this.codcli.set(codcli);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.dtEnvio.set(dtEnvio);
        this.dtFaturamento.set(dtFaturamento);
        this.email.set(email);
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getDtEnvio() {
        return dtEnvio.get();
    }

    public final void setDtEnvio(String value) {
        dtEnvio.set(value);
    }

    public StringProperty dtEnvioProperty() {
        return dtEnvio;
    }

    public final String getDtFaturamento() {
        return dtFaturamento.get();
    }

    public final void setDtFaturamento(String value) {
        dtFaturamento.set(value);
    }

    public StringProperty dtFaturamentoProperty() {
        return dtFaturamento;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

}
