/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;

import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
public class SdOperacoesGrp001 {

    private final IntegerProperty grupo = new SimpleIntegerProperty();
    private final IntegerProperty codigoOperacao = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();

    public SdOperacoesGrp001() {
    }

    public SdOperacoesGrp001(Integer grupo, Integer operacao) {
        this.grupo.set(grupo);
        this.codigoOperacao.set(operacao);
    }

    public SdOperacoesGrp001(Integer grupo, Integer codigoOperacao, SdOperacaoOrganize001 operacao) {
        this.grupo.set(grupo);
        this.codigoOperacao.set(codigoOperacao);
        this.operacao.set(operacao);
    }

    public final int getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(int value) {
        grupo.set(value);
    }

    public IntegerProperty grupoProperty() {
        return grupo;
    }

    public final int getCodigoOperacao() {
        return codigoOperacao.get();
    }

    public final void setCodigoOperacao(int value) {
        codigoOperacao.set(value);
    }

    public IntegerProperty codigoOperacaoProperty() {
        return codigoOperacao;
    }

    public final SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }

    public final void setOperacao(SdOperacaoOrganize001 value) {
        operacao.set(value);
    }

    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdOperacoesGrp001 other = (SdOperacoesGrp001) obj;
        if (!Objects.equals(this.grupo, other.grupo)) {
            return false;
        }
        if (!Objects.equals(this.codigoOperacao.get(), other.codigoOperacao.get())) {
            return false;
        }
        if (!Objects.equals(this.operacao.get().getCodigo(), other.operacao.get().getCodigo())) {
            return false;
        }
        return true;
    }

}
