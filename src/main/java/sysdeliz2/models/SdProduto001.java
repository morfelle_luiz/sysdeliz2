/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class SdProduto001 {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty observacaoPcp = new SimpleStringProperty();

    public SdProduto001() {
    }

    public SdProduto001(String codigo, String observacaoPcp) {
        this.codigo.set(codigo);
        this.observacaoPcp.set(observacaoPcp);
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getObservacaoPcp() {
        return observacaoPcp.get();
    }

    public final void setObservacaoPcp(String value) {
        observacaoPcp.set(value);
    }

    public StringProperty observacaoPcpProperty() {
        return observacaoPcp;
    }

    public void copy(SdProduto001 toCopy) {
        if (toCopy != null) {
            this.codigo.set(toCopy.getCodigo());
            this.observacaoPcp.set(toCopy.getObservacaoPcp());
        }
    }

}
