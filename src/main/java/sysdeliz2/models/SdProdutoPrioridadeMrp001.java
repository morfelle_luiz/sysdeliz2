/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class SdProdutoPrioridadeMrp001 {

    private final BooleanProperty selected = new SimpleBooleanProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty prioridade = new SimpleIntegerProperty();

    public SdProdutoPrioridadeMrp001() {
    }

    public SdProdutoPrioridadeMrp001(String codigo, String descricao, Integer prioridade) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.prioridade.set(prioridade);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final int getPrioridade() {
        return prioridade.get();
    }

    public final void setPrioridade(int value) {
        prioridade.set(value);
    }

    public IntegerProperty prioridadeProperty() {
        return prioridade;
    }

}
