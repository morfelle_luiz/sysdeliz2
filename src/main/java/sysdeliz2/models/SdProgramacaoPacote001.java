/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.*;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author cristiano.diego
 */
public class SdProgramacaoPacote001 {
    
    private final StringProperty pacote = new SimpleStringProperty();
    private final IntegerProperty operacao = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacaoObject = new SimpleObjectProperty<SdOperacaoOrganize001>();
    @Deprecated private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();
    @Deprecated private final StringProperty independencia = new SimpleStringProperty();
    private final IntegerProperty agrupador = new SimpleIntegerProperty();
    @Deprecated private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<LocalDateTime>();
    @Deprecated private final ObjectProperty<LocalDateTime> dhIb = new SimpleObjectProperty<LocalDateTime>();
    @Deprecated private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<LocalDateTime>();
    private final IntegerProperty celula = new SimpleIntegerProperty();
    private final ObjectProperty<SdCelula> celulaObject = new SimpleObjectProperty<SdCelula>();
    private final IntegerProperty setorOp = new SimpleIntegerProperty();
    private final ObjectProperty<SdSetorOp001> setorOpObject = new SimpleObjectProperty<SdSetorOp001>();
    private final IntegerProperty maquina = new SimpleIntegerProperty();
    private final ObjectProperty<SdEquipamentosOrganize001> maquinaObject = new SimpleObjectProperty<SdEquipamentosOrganize001>();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final ObjectProperty<SdColaborador> colaboradorObject = new SimpleObjectProperty<SdColaborador>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    @Deprecated private final IntegerProperty qtdeIb = new SimpleIntegerProperty();
    @Deprecated private final DoubleProperty ib = new SimpleDoubleProperty();
    private final DoubleProperty tempoOperacao = new SimpleDoubleProperty();
    
    private final DoubleProperty tempoTotal = new SimpleDoubleProperty();
    private final ObjectProperty<SdOperRoteiroOrganize001> roteiroOrganize = new SimpleObjectProperty<SdOperRoteiroOrganize001>();
    private final BooleanProperty quebraManual = new SimpleBooleanProperty(false);
    private final BooleanProperty programacaoAnalisada = new SimpleBooleanProperty(false);
    
    private final ObjectProperty<SdProgramacaoPacote001> operacaoAnterior = new SimpleObjectProperty<>(null);
    private final ObjectProperty<SdProgramacaoPacote001> operacaoPosterior = new SimpleObjectProperty<>(null);
    
    @Deprecated private final BooleanProperty hasConflito = new SimpleBooleanProperty(false);
    private final BooleanProperty programacaoApontada = new SimpleBooleanProperty(false);
    
    
    public SdProgramacaoPacote001() {
    }
    
    public SdProgramacaoPacote001(String pacote, Integer operacao, Integer ordem, LocalDateTime dhInicio, LocalDateTime dhIb, LocalDateTime dhFim,
                                  Integer celula, Integer setorOp, Integer maquina, Integer colaborador, Integer qtde, String independencia, Integer agrupador,
                                  Double ib, Integer quebra, Integer qtdeIb, Double tempoOperacao) throws SQLException {
        this.pacote.set(pacote);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.dhInicio.set(dhInicio);
        this.dhIb.set(dhIb);
        this.dhFim.set(dhFim);
        this.celula.set(celula);
        this.setorOp.set(setorOp);
        this.maquina.set(maquina);
        this.colaborador.set(colaborador);
        this.qtde.set(qtde);
        this.qtdeIb.set(qtdeIb);
        this.independencia.set(independencia);
        this.agrupador.set(agrupador);
        this.ib.set(ib);
        this.quebra.set(quebra);
        this.tempoOperacao.set(tempoOperacao);
        operacaoObject.set(DAOFactory.getSdOperacaoOrganize001DAO().getByCode(this.operacao.getValue()));
        celulaObject.set(DAOFactory.getSdCelula001DAO().getByCode(this.celula.getValue()));
        setorOpObject.set(DAOFactory.getSdSetorOp001DAO().getByCode(this.setorOp.getValue()));
        maquinaObject.set(DAOFactory.getSdEquipamentosOrganize001DAO().getByCode(this.maquina.getValue()));
        colaboradorObject.set(DAOFactory.getSdColaborador001DAO().getByCode(this.colaborador.getValue()));
        this.tempoTotal.set(this.qtde.get() * tempoOperacao);
    }
    
    public SdProgramacaoPacote001(String pacote, Integer operacao, Integer ordem, LocalDateTime dhInicio, LocalDateTime dhIb, LocalDateTime dhFim,
                                  Integer celula, Integer setorOp, Integer maquina, Integer colaborador, Integer qtde, String independencia, Integer agrupador,
                                  Double ib, Integer quebra, Integer qtdeIb, Double tempoOperacao, Boolean programacaoApontada) throws SQLException {
        this.pacote.set(pacote);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.dhInicio.set(dhInicio);
        this.dhIb.set(dhIb);
        this.dhFim.set(dhFim);
        this.celula.set(celula);
        this.setorOp.set(setorOp);
        this.maquina.set(maquina);
        this.colaborador.set(colaborador);
        this.qtde.set(qtde);
        this.qtdeIb.set(qtdeIb);
        this.independencia.set(independencia);
        this.agrupador.set(agrupador);
        this.ib.set(ib);
        this.quebra.set(quebra);
        this.programacaoApontada.set(programacaoApontada);
        this.tempoOperacao.set(tempoOperacao);
        operacaoObject.set(DAOFactory.getSdOperacaoOrganize001DAO().getByCode(this.operacao.getValue()));
        celulaObject.set(DAOFactory.getSdCelula001DAO().getByCode(this.celula.getValue()));
        setorOpObject.set(DAOFactory.getSdSetorOp001DAO().getByCode(this.setorOp.getValue()));
        maquinaObject.set(DAOFactory.getSdEquipamentosOrganize001DAO().getByCode(this.maquina.getValue()));
        colaboradorObject.set(DAOFactory.getSdColaborador001DAO().getByCode(this.colaborador.getValue()));
        this.tempoTotal.set(this.qtde.get() * tempoOperacao);
    }
    
    public SdProgramacaoPacote001(String pacote, Integer operacao, Integer ordem, LocalDateTime dhInicio, LocalDateTime dhIb, LocalDateTime dhFim,
                                  Integer celula, Integer setorOp, Integer maquina, Integer colaborador, Integer qtde, String independencia, Integer agrupador,
                                  Double ib, Integer quebra, Integer qtdeIb, Double tempoOperacao, SdOperRoteiroOrganize001 roteiroOrganize) throws SQLException {
        this.pacote.set(pacote);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.dhInicio.set(dhInicio);
        this.dhIb.set(dhIb);
        this.dhFim.set(dhFim);
        this.celula.set(celula);
        this.setorOp.set(setorOp);
        this.maquina.set(maquina);
        this.colaborador.set(colaborador);
        this.qtde.set(qtde);
        this.qtdeIb.set(qtdeIb);
        this.independencia.set(independencia);
        this.agrupador.set(agrupador);
        this.ib.set(ib);
        this.quebra.set(quebra);
        this.tempoOperacao.set(tempoOperacao);
        this.roteiroOrganize.set(roteiroOrganize);
        operacaoObject.set(DAOFactory.getSdOperacaoOrganize001DAO().getByCode(this.operacao.getValue()));
        celulaObject.set(DAOFactory.getSdCelula001DAO().getByCode(this.celula.getValue()));
        setorOpObject.set(DAOFactory.getSdSetorOp001DAO().getByCode(this.setorOp.getValue()));
        maquinaObject.set(DAOFactory.getSdEquipamentosOrganize001DAO().getByCode(this.maquina.getValue()));
        colaboradorObject.set(DAOFactory.getSdColaborador001DAO().getByCode(this.colaborador.getValue()));
        this.tempoTotal.set(this.qtde.get() * tempoOperacao);
    }
    
    public SdProgramacaoPacote001(SdProgramacaoPacote001 toCopy) throws SQLException {
        this.pacote.set(toCopy.pacote.get());
        this.operacao.set(toCopy.operacao.get());
        this.ordem.set(toCopy.ordem.get());
        this.dhInicio.set(toCopy.dhInicio.get());
        this.dhIb.set(toCopy.dhIb.get());
        this.dhFim.set(toCopy.dhFim.get());
        this.celula.set(toCopy.celula.get());
        this.setorOp.set(toCopy.setorOp.get());
        this.maquina.set(toCopy.maquina.get());
        this.colaborador.set(toCopy.colaborador.get());
        this.qtde.set(toCopy.qtde.get());
        this.qtdeIb.set(toCopy.qtdeIb.get());
        this.independencia.set(toCopy.independencia.get());
        this.agrupador.set(toCopy.agrupador.get());
        this.ib.set(toCopy.ib.get());
        this.quebra.set(toCopy.quebra.get());
        this.roteiroOrganize.set(toCopy.roteiroOrganize.get());
        this.operacaoObject.set(toCopy.operacaoObject.get());
        this.celulaObject.set(toCopy.celulaObject.get());
        this.setorOpObject.set(toCopy.setorOpObject.get());
        this.maquinaObject.set(toCopy.maquinaObject.get());
        this.colaboradorObject.set(toCopy.colaboradorObject.get());
        this.tempoTotal.set(toCopy.tempoTotal.get());
        this.tempoOperacao.set(toCopy.tempoOperacao.get());
        
        this.quebraManual.set(toCopy.quebraManual.get());
        this.programacaoAnalisada.set(toCopy.programacaoAnalisada.get());
        this.operacaoAnterior.set(toCopy.operacaoAnterior.get());
        this.operacaoPosterior.set(toCopy.operacaoPosterior.get());
        
    }
    
    public final String getPacote() {
        return pacote.get();
    }
    
    public final void setPacote(String value) {
        pacote.set(value);
    }
    
    public StringProperty pacoteProperty() {
        return pacote;
    }
    
    public final int getOperacao() {
        return operacao.get();
    }
    
    public final void setOperacao(int value) {
        operacao.set(value);
    }
    
    public IntegerProperty operacaoProperty() {
        return operacao;
    }
    
    public final int getOrdem() {
        return ordem.get();
    }
    
    public final void setOrdem(int value) {
        ordem.set(value);
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    @Deprecated
    public final LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    @Deprecated
    public final String getDhInicioAsString() {
        return dhInicio.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
    @Deprecated
    public final void setDhInicio(LocalDateTime value) {
        dhInicio.set(value);
    }
    @Deprecated
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    @Deprecated
    public StringProperty dhInicioAsStringProperty() {
        return new SimpleStringProperty(dhInicio.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }
    @Deprecated
    public final LocalDateTime getDhIb() {
        return dhIb.get();
    }
    @Deprecated
    public final String getDhIbAsString() {
        return dhIb.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
    @Deprecated
    public final void setDhIb(LocalDateTime value) {
        dhIb.set(value);
    }
    @Deprecated
    public ObjectProperty<LocalDateTime> dhIbProperty() {
        return dhIb;
    }
    @Deprecated
    public StringProperty dhIbAsStringProperty() {
        return new SimpleStringProperty(dhIb.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }
    @Deprecated
    public final LocalDateTime getDhFim() {
        return dhFim.get();
    }
    @Deprecated
    public final String getDhFimAsString() {
        return dhFim.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
    @Deprecated
    public final void setDhFim(LocalDateTime value) {
        dhFim.set(value);
    }
    @Deprecated
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    @Deprecated
    public StringProperty dhFimAsStringProperty() {
        return new SimpleStringProperty(dhFim.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }
    
    public final int getCelula() {
        return celula.get();
    }
    
    public final void setCelula(int value) {
        celula.set(value);
    }
    
    public IntegerProperty celulaProperty() {
        return celula;
    }
    
    public final int getSetorOp() {
        return setorOp.get();
    }
    
    public final void setSetorOp(int value) {
        setorOp.set(value);
    }
    
    public IntegerProperty setorOpProperty() {
        return setorOp;
    }
    
    public final int getMaquina() {
        return maquina.get();
    }
    
    public final void setMaquina(int value) {
        maquina.set(value);
    }
    
    public IntegerProperty maquinaProperty() {
        return maquina;
    }
    
    public final int getColaborador() {
        return colaborador.get();
    }
    
    public final void setColaborador(int value) {
        colaborador.set(value);
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public final int getQtde() {
        return qtde.get();
    }
    
    public final void setQtde(int value) {
        qtde.set(value);
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public final SdOperacaoOrganize001 getOperacaoObject() {
        return operacaoObject.get();
    }
    
    public final void setOperacaoObject(SdOperacaoOrganize001 value) {
        operacaoObject.set(value);
    }
    
    public ObjectProperty<SdOperacaoOrganize001> operacaoObjectProperty() {
        return operacaoObject;
    }
    
    public final SdCelula getCelulaObject() {
        return celulaObject.get();
    }
    
    public final void setCelulaObject(SdCelula value) {
        celulaObject.set(value);
    }
    
    public ObjectProperty<SdCelula> celulaObjectProperty() {
        return celulaObject;
    }
    
    public final SdSetorOp001 getSetorOpObject() {
        return setorOpObject.get();
    }
    
    public final void setSetorOpObject(SdSetorOp001 value) {
        setorOpObject.set(value);
    }
    
    public ObjectProperty<SdSetorOp001> setorOpObjectProperty() {
        return setorOpObject;
    }
    
    public final SdEquipamentosOrganize001 getMaquinaObject() {
        return maquinaObject.get();
    }
    
    public final void setMaquinaObject(SdEquipamentosOrganize001 value) {
        maquinaObject.set(value);
    }
    
    public ObjectProperty<SdEquipamentosOrganize001> maquinaObjectProperty() {
        return maquinaObject;
    }
    
    public final SdColaborador getColaboradorObject() {
        return colaboradorObject.get();
    }
    
    public final void setColaboradorObject(SdColaborador value) {
        colaboradorObject.set(value);
    }
    
    public ObjectProperty<SdColaborador> colaboradorObjectProperty() {
        return colaboradorObject;
    }
    @Deprecated
    public final String getIndependencia() {
        return independencia.get();
    }
    @Deprecated
    public final void setIndependencia(String value) {
        independencia.set(value);
    }
    @Deprecated
    public StringProperty independenciaProperty() {
        return independencia;
    }
    
    public final int getAgrupador() {
        return agrupador.get();
    }
    
    public final void setAgrupador(int value) {
        agrupador.set(value);
    }
    
    public IntegerProperty agrupadorProperty() {
        return agrupador;
    }
    @Deprecated
    public final double getIb() {
        return ib.get();
    }
    @Deprecated
    public final void setIb(double value) {
        ib.set(value);
    }
    @Deprecated
    public DoubleProperty ibProperty() {
        return ib;
    }
    
    public final SdOperRoteiroOrganize001 getRoteiroOrganize() {
        return roteiroOrganize.get();
    }
    
    public final void setRoteiroOrganize(SdOperRoteiroOrganize001 value) {
        roteiroOrganize.set(value);
    }
    
    public ObjectProperty<SdOperRoteiroOrganize001> roteiroOrganizeProperty() {
        return roteiroOrganize;
    }
    
    public final int getQuebra() {
        return quebra.get();
    }
    
    public final void setQuebra(int value) {
        quebra.set(value);
    }
    
    public IntegerProperty quebraProperty() {
        return quebra;
    }
    
    public final boolean isQuebraManual() {
        return quebraManual.get();
    }
    
    public final void setQuebraManual(boolean value) {
        quebraManual.set(value);
    }
    
    public BooleanProperty quebraManualProperty() {
        return quebraManual;
    }
    
    public final double getTempoTotal() {
        return tempoTotal.get();
    }
    
    public final void setTempoTotal(double value) {
        tempoTotal.set(value);
    }
    
    public DoubleProperty tempoTotalProperty() {
        return tempoTotal;
    }
    
    public final SdProgramacaoPacote001 getOperacaoAnterior() {
        return operacaoAnterior.get();
    }
    
    public final void setOperacaoAnterior(SdProgramacaoPacote001 value) {
        operacaoAnterior.set(value);
    }
    
    public ObjectProperty<SdProgramacaoPacote001> operacaoAnteriorProperty() {
        return operacaoAnterior;
    }
    
    public final SdProgramacaoPacote001 getOperacaoPosterior() {
        return operacaoPosterior.get();
    }
    
    public final void setOperacaoPosterior(SdProgramacaoPacote001 value) {
        operacaoPosterior.set(value);
    }
    
    public ObjectProperty<SdProgramacaoPacote001> operacaoPosteriorProperty() {
        return operacaoPosterior;
    }
    
    public final boolean isProgramacaoAnalisada() {
        return programacaoAnalisada.get();
    }
    
    public final void setProgramacaoAnalisada(boolean value) {
        programacaoAnalisada.set(value);
    }
    
    public BooleanProperty programacaoAnalisadaProperty() {
        return programacaoAnalisada;
    }
    
    public final boolean isHasConflito() {
        return hasConflito.get();
    }
    
    public final void setHasConflito(boolean value) {
        hasConflito.set(value);
    }
    
    public BooleanProperty hasConflitoProperty() {
        return hasConflito;
    }
    
    public int getQtdeIb() {
        return qtdeIb.get();
    }
    
    public IntegerProperty qtdeIbProperty() {
        return qtdeIb;
    }
    
    public void setQtdeIb(int qtdeIb) {
        this.qtdeIb.set(qtdeIb);
    }
    
    public boolean getProgramacaoApontada() {
        return programacaoApontada.get();
    }
    
    public BooleanProperty programacaoApontadaProperty() {
        return programacaoApontada;
    }
    
    public void setProgramacaoApontada(boolean programacaoApontada) {
        this.programacaoApontada.set(programacaoApontada);
    }
    
    public double getTempoOperacao() {
        return tempoOperacao.get();
    }
    
    public DoubleProperty tempoOperacaoProperty() {
        return tempoOperacao;
    }
    
    public void setTempoOperacao(double tempoOperacao) {
        this.tempoOperacao.set(tempoOperacao);
    }
    
    public String toErrorPrint() {
        return "SdProgramacaoPacote001{" +
                "pacote=" + pacote +
                ", operacao=" + operacao +
                ", operacaoObject=" + operacaoObject +
                ", ordem=" + ordem +
                ", quebra=" + quebra +
                ", independencia=" + independencia +
                ", agrupador=" + agrupador +
                ", dhInicio=" + dhInicio +
                ", dhIb=" + dhIb +
                ", dhFim=" + dhFim +
                ", celula=" + celula +
                ", celulaObject=" + celulaObject +
                ", setorOp=" + setorOp +
                ", setorOpObject=" + setorOpObject +
                ", maquina=" + maquina +
                ", maquinaObject=" + maquinaObject +
                ", colaborador=" + colaborador +
                ", colaboradorObject=" + colaboradorObject +
                ", qtde=" + qtde +
                ", qtdeIb=" + qtdeIb +
                ", ib=" + ib +
                ", tempoTotal=" + tempoTotal +
                ", roteiroOrganize=" + roteiroOrganize +
                ", quebraManual=" + quebraManual +
                ", programacaoAnalisada=" + programacaoAnalisada +
                ", operacaoAnterior=" + operacaoAnterior +
                ", operacaoPosterior=" + operacaoPosterior +
                ", hasConflito=" + hasConflito +
                '}';
    }
    
    
}
