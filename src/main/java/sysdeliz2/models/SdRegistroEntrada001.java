/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.sys.DateUtils;

/**
 *
 * @author lima.joao
 */
public class SdRegistroEntrada001 {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty nomeResponsavel = new SimpleStringProperty();
    private final StringProperty emailResponsavel = new SimpleStringProperty();
    private final StringProperty dataEntrada = new SimpleStringProperty();
    private final StringProperty dataSaida = new SimpleStringProperty();
    private final StringProperty pedirLanche = new SimpleStringProperty();
    private final StringProperty tipoLanche = new SimpleStringProperty();
    private final StringProperty situacao = new SimpleStringProperty();
    private final StringProperty impresso = new SimpleStringProperty();
    private final StringProperty dataEmissao = new SimpleStringProperty();
    private final StringProperty dataImpressao = new SimpleStringProperty();
    private final StringProperty nomeFuncionario = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty comCafe = new SimpleStringProperty();
    // observacao
    private final StringProperty observacao = new SimpleStringProperty(this, "observacao");

    public SdRegistroEntrada001(){
        this.codigo.set("");
        this.nomeResponsavel.set("");
        this.emailResponsavel.set("");
        this.dataEntrada.set("");
        this.dataSaida.set("");
        this.pedirLanche.set("");
        this.tipoLanche.set("");
        this.situacao.set("");
        this.impresso.set("");
        this.dataEmissao.set("");
        this.dataImpressao.set("");
        this.nomeFuncionario.set("");
        this.setor.set("");
        this.observacao.set("");
    }

    public SdRegistroEntrada001(
            String codigo, String nomeResponsavel, String emailResponsavel, String dataEntrada, String dataSaida,
            String pedirLanche, String tipoLanche, String situacao, String impresso, 
            String dataEmissao, String dataImpressao, String nomeFuncionario, String setor, String observacao, String comCafe ){

        this.codigo.set(codigo);
        this.nomeResponsavel.set(nomeResponsavel);
        this.emailResponsavel.set(emailResponsavel);
        this.dataEntrada.set(dataEntrada);
        this.dataSaida.set(dataSaida);
        this.pedirLanche.set(pedirLanche);
        this.tipoLanche.set(tipoLanche);
        this.situacao.set(situacao);
        this.impresso.set(impresso);
        this.dataEmissao.set(dataEmissao);
        this.observacao.set(observacao);
        this.comCafe.set(comCafe);

        if(dataImpressao != null) {
            this.dataImpressao.set(dataImpressao);
        }
        this.nomeFuncionario.set(nomeFuncionario);
        this.setor.set(setor);

        if(pedirLanche.equals("N")){
            tipoLancheString.set("Sem Lanche");
        } else {
            if( tipoLanche.equals("0") ){
                tipoLancheString.set("1 Salgado 1 Doce");
            } else {
                tipoLancheString.set("1 Salgado 2 Doces");
            }
        }

        if(impresso.equals("N")){
            impressoString.set("Pendente");
        } else {
            impressoString.set("Impresso");
        }

        comCafeString.set(comCafe.equals("N") ? "Não" : "Sim");
    }

    // tipoLancheString
    private final StringProperty tipoLancheString = new SimpleStringProperty(this, "tipoLancheString");
    //comCafeString
    private final StringProperty comCafeString = new SimpleStringProperty(this, "comCafeString");
    // impressoString
    private final StringProperty impressoString = new SimpleStringProperty(this, "impressoString");
    // selectedProperty
    private final BooleanProperty selected = new SimpleBooleanProperty(this, "selected");

    // dataEntradaOnly
    private final StringProperty dataEntradaOnly = new SimpleStringProperty(this, "dataEntradaOnly");
    // horaEntradaProperty
    private final StringProperty horaEntrada = new SimpleStringProperty(this, "horaEntrada");

    // horaSaidaProperty
    private final StringProperty horaSaida = new SimpleStringProperty(this, "horaSaida");

    public final String getHoraSaida() {
        return DateUtils.getInstance().getSimpleTime(this.getDataSaida());
    }

    public final String getHoraEntrada() {
        return DateUtils.getInstance().getSimpleTime(this.getDataEntrada());
    }

    public final String getDataEntradaOnly() {
        return DateUtils.getInstance().getSimpleDate(this.getDataEntrada());
    }

    public final StringProperty observacao() {
        return observacao;
    }

    public final String getObservacao() {
        return observacao.get();
    }

    public final void setObservacao(String value) {
        observacao.set(value);
    }

    public final BooleanProperty selectedProperty() {
        return selected;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public final StringProperty impressoString() {
        return impressoString;
    }

    public final String getImpressoString() {
        return impressoString.get();
    }

    public final void setImpressoString(String value) {
        impressoString.set(value);
    }

    public final StringProperty tipoLancheString() {
        return tipoLancheString;
    }

    public final String getTipoLancheString() {
        return tipoLancheString.get();
    }

    public final void setTipoLancheString(String value) {
        tipoLancheString.set(value);
    }

    public String getComCafeString() {
        return comCafeString.get();
    }

    public StringProperty comCafeStringProperty() {
        return comCafeString;
    }

    public void setComCafeString(String comCafeString) {
        this.comCafeString.set(comCafeString);
    }

    public String getSetor() {
        return setor.get();
    }

    public void setSetor(String value) {
        setor.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty setorProperty() {
        return setor;
    }
    
    public String getNomeFuncionario() {
        return nomeFuncionario.get();
    }

    public void setNomeFuncionario(String value) {
        nomeFuncionario.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty nomeFuncionarioProperty() {
        return nomeFuncionario;
    }

    public String getDataImpressao() {
        return dataImpressao.get();
    }

    public void setDataImpressao(String value) {
        dataImpressao.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty dataImpressaoProperty() {
        return dataImpressao;
    }

    public String getDataEmissao() {
        return dataEmissao.get();
    }

    public void setDataEmissao(String value) {
        dataEmissao.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty dataEmissaoProperty() {
        return dataEmissao;
    }

    public String getImpresso() {
        return impresso.get();
    }

    public void setImpresso(String value) {
        impresso.set(value);

        if(impresso.get().equals("N")){
            impressoString.set("Pendente");
        } else {
            impressoString.set("Impresso");
        }
    }

    @SuppressWarnings("unused")
    public StringProperty impressoProperty() {
        return impresso;
    }
    
    public String getSituacao() {
        return situacao.get();
    }

    public void setSituacao(String value) {
        situacao.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty situacaoProperty() {
        return situacao;
    }
        
    public String getTipoLanche(){
        return tipoLanche.get();
    }

    public void setTipoLanche(String value) {
        tipoLanche.set(value);

        if(pedirLanche.get().equals("N")){
            tipoLancheString.set("Sem Lanche");
        } else {
            if( tipoLanche.get().equals("0") ){
                tipoLancheString.set("1 Salgado 1 Doce");
            } else {
                tipoLancheString.set("1 Salgado 2 Doces");
            }
        }
    }

    @SuppressWarnings("unused")
    public StringProperty tipoLancheProperty() {
        return tipoLanche;
    }
    
    public String getPedirLancheUpd() {
        return pedirLanche.get();
    }

    public String getPedirLanche(){
        if(pedirLanche.get().equals("S")){
            return "SIM";
        } else {
            return "NÃO";
        }
    }

    public void setPedirLanche(String value) {
        pedirLanche.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty perdirLancheProperty() {
        return pedirLanche;
    }
    
    public String getDataSaida() {
        return dataSaida.get();
    }

    public void setDataSaida(String value) {
        dataSaida.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty dataSaidaProperty() {
        return dataSaida;
    }

    public String getDataEntrada() {
        return dataEntrada.get();
    }

    public void setDataEntrada(String value) {
        dataEntrada.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty dataEntradaProperty() {
        return dataEntrada;
    }
        
    public String getEmailResponsavel() {
        return emailResponsavel.get();
    }

    public void setEmailResponsavel(String value) {
        emailResponsavel.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty emailResponsavelProperty() {
        return emailResponsavel;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel.get();
    }

    public void setNomeResponsavel(String value) {
        nomeResponsavel.set(value);
    }

    @SuppressWarnings("unused")
    public StringProperty nomeResponsavelProperty() {
        return nomeResponsavel;
    }
    
    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public StringProperty comCafeProperty() {
        return comCafe;
    }

    public String getComCafe() {
        return comCafe.get();
    }

    public void setComCafe(String comCafe) {
        this.comCafe.set(comCafe);
    }

    public String buildInsertSQL(){
        return "INSERT INTO SD_REGISTROS_ENTRADAS_001(\n" +
                "     NOME_RESPONSAVEL,\n" +
                "     EMAIL_RESPONSAVEL,\n" +
                "     dh_entrada,\n" +
                "     dh_saida,\n" +
                "     pedir_lanche,\n" +
                "     tipo_lanche,\n" +
                "     situacao,\n" +
                "     impresso,\n" +
                "     dh_emissao,\n" +
                "     dh_impresso,\n" +
                "     nome_funcionario,\n" +
                "     setor,\n" +
                "     observacao,\n" +
                "     com_cafe\n" +
                ") VALUES( \n" +
                "'" + this.getNomeResponsavel() + "',\n '" +
                this.getEmailResponsavel() + "',\n " +
                DateUtils.getInstance().getUpdateTimestamp(this.getDataEntrada()) + ",\n " +
                DateUtils.getInstance().getUpdateTimestamp(this.getDataSaida()) + ",\n '" +
                this.getPedirLancheUpd() + "',\n " +
                this.getTipoLanche() + ", \n '" +
                this.getSituacao() + "',\n '" +
                this.getImpresso() + "',\n" +
                DateUtils.getInstance().getToDate(this.getDataEmissao()) + ",\n" +
                DateUtils.getInstance().getToDate(this.getDataImpressao()) + ",\n '" +
                this.getNomeFuncionario() + "',\n '" +
                this.getSetor() + "',\n '" +
                this.getObservacao() + "',\n '" +
                this.getComCafe() + "'\n" +
                ")";
    }

    @SuppressWarnings("unused")
    public String buildUpdateSQL(){
        return "UPDATE SD_REGISTROS_ENTRADAS_001 " +
                "    SET NOME_RESPONSAVEL = '" + this.getNomeResponsavel() + "'," +
                "     EMAIL_RESPONSAVEL = '" + this.getEmailResponsavel() + "'," +
                "     dh_entrada = " + DateUtils.getInstance().getUpdateTimestamp(this.getDataEntrada()) + "," +
                "     dh_saida = " + DateUtils.getInstance().getUpdateTimestamp(this.getDataSaida()) + "," +
                "     pedir_lanche = '" + this.getPedirLancheUpd() + "'," +
                "     tipo_lanche = " + this.getTipoLanche() + "," +
                "     situacao = '" + this.getSituacao() + "'," +
                "     impresso = '" + this.getImpresso() + "'," +
                "     dh_impresso = " + DateUtils.getInstance().getUpdateTimestamp(this.getDataImpressao()) + ", " +
                "     observacao = '" + this.getObservacao() + "', " +
                "     com_cafe = '" + this.getComCafe() + "' " +
                "WHERE codigo = " + this.getCodigo();
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada.set(horaEntrada);
    }

    public void setHoraSaida(String horaSaida) {
        this.horaSaida.set(horaSaida);
    }
}
