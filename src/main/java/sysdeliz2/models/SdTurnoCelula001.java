/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.SdTurno;

import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
public class SdTurnoCelula001 {

    private final IntegerProperty codigoCelula = new SimpleIntegerProperty();
    private final IntegerProperty codigoTurno = new SimpleIntegerProperty();
    private final ObjectProperty<SdTurno> turno = new SimpleObjectProperty<SdTurno>();

    public SdTurnoCelula001() {
    }

    public SdTurnoCelula001(Integer codigoCelula, Integer codigoTurno) throws SQLException {
        this.codigoCelula.set(codigoCelula);
        this.codigoTurno.set(codigoTurno);
        this.turno.set(DAOFactory.getSdTurnoDAO().getByCode(codigoTurno));
    }

    public SdTurnoCelula001(Integer codigoCelula, Integer codigoTurno, SdTurno turno) throws SQLException {
        this.codigoCelula.set(codigoCelula);
        this.codigoTurno.set(codigoTurno);
        this.turno.set(turno);
    }

    public final int getCodigoCelula() {
        return codigoCelula.get();
    }

    public final void setCodigoCelula(int value) {
        codigoCelula.set(value);
    }

    public IntegerProperty codigoCelulaProperty() {
        return codigoCelula;
    }

    public final int getCodigoTurno() {
        return codigoTurno.get();
    }

    public final void setCodigoTurno(int value) {
        codigoTurno.set(value);
    }

    public IntegerProperty codigoTurnoProperty() {
        return codigoTurno;
    }

    public final SdTurno getTurno() {
        return turno.get();
    }

    public final void setTurno(SdTurno value) {
        turno.set(value);
    }

    public ObjectProperty<SdTurno> turnoProperty() {
        return turno;
    }

}
