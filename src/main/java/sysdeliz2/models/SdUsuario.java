package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.models.sysdeliz.SdTela;

import java.util.List;

public class SdUsuario implements Comparable<SdUsuario> {

    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty codUsu = new SimpleStringProperty();
    private List<SdTela> telas;

    public SdUsuario(String nome, String codUsu, List<SdTela> telas) {
        this.nome.set(nome);
        this.codUsu.set(codUsu);
        this.telas = telas;
    }

    public SdUsuario() {

    }

    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    public String getCodUsu() {
        return codUsu.get();
    }

    public StringProperty codUsuProperty() {
        return codUsu;
    }

    public void setCodUsu(String codUsu) {
        this.codUsu.set(codUsu);
    }

    public List<SdTela> getTelas() {
        return telas;
    }

    public void setTelas(List<SdTela> telas) {
        this.telas = telas;
    }

    @Override
    public String toString() {
        return this.nome.get();
    }

    @Override
    public int compareTo(@NotNull SdUsuario o) {
        return this.nome.get().compareTo(o.getNome());
    }
}
