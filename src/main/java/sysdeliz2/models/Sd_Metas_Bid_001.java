/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class Sd_Metas_Bid_001 {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty codmar = new SimpleStringProperty();
    private final StringProperty codcol = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty mostr = new SimpleIntegerProperty();
    private final DoubleProperty pm = new SimpleDoubleProperty();
    private final IntegerProperty cd = new SimpleIntegerProperty();
    private final IntegerProperty pv = new SimpleIntegerProperty();
    private final IntegerProperty ka = new SimpleIntegerProperty();
    private final IntegerProperty pp = new SimpleIntegerProperty();
    private final DoubleProperty vp = new SimpleDoubleProperty();
    private final IntegerProperty tp = new SimpleIntegerProperty();
    private final DoubleProperty tf = new SimpleDoubleProperty();
    private final DoubleProperty pg = new SimpleDoubleProperty();
    private final DoubleProperty pr = new SimpleDoubleProperty();
    private final IntegerProperty cln = new SimpleIntegerProperty();

    public Sd_Metas_Bid_001(String codrep, String tipo, String codmar, String codcol, Integer mostr,
            Double pm, Integer cd, Integer pv, Integer ka, Integer pp, Double vp, Integer tp, Double tf,
            Double pg, Double pr, Integer cln) {
        this.codrep.set(codrep);
        this.tipo.set(tipo);
        this.codmar.set(codmar);
        this.codcol.set(codcol);
        this.mostr.set(mostr);
        this.pm.set(pm);
        this.cd.set(cd);
        this.pv.set(pv);
        this.ka.set(ka);
        this.pp.set(pp);
        this.vp.set(vp);
        this.tp.set(tp);
        this.tf.set(tf);
        this.pg.set(pg);
        this.pr.set(pr);
        this.cln.set(cln);
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getCodmar() {
        return codmar.get();
    }

    public final void setCodmar(String value) {
        codmar.set(value);
    }

    public StringProperty codmarProperty() {
        return codmar;
    }

    public final String getCodcol() {
        return codcol.get();
    }

    public final void setCodcol(String value) {
        codcol.set(value);
    }

    public StringProperty codcolProperty() {
        return codcol;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final int getMostr() {
        return mostr.get();
    }

    public final void setMostr(int value) {
        mostr.set(value);
    }

    public IntegerProperty mostrProperty() {
        return mostr;
    }

    public final double getPm() {
        return pm.get();
    }

    public final void setPm(double value) {
        pm.set(value);
    }

    public DoubleProperty pmProperty() {
        return pm;
    }

    public final int getCd() {
        return cd.get();
    }

    public final void setCd(int value) {
        cd.set(value);
    }

    public IntegerProperty cdProperty() {
        return cd;
    }

    public final int getPv() {
        return pv.get();
    }

    public final void setPv(int value) {
        pv.set(value);
    }

    public IntegerProperty pvProperty() {
        return pv;
    }

    public final int getKa() {
        return ka.get();
    }

    public final void setKa(int value) {
        ka.set(value);
    }

    public IntegerProperty kaProperty() {
        return ka;
    }

    public final int getPp() {
        return pp.get();
    }

    public final void setPp(int value) {
        pp.set(value);
    }

    public IntegerProperty ppProperty() {
        return pp;
    }

    public final double getVp() {
        return vp.get();
    }

    public final void setVp(double value) {
        vp.set(value);
    }

    public DoubleProperty vpProperty() {
        return vp;
    }

    public final int getTp() {
        return tp.get();
    }

    public final void setTp(int value) {
        tp.set(value);
    }

    public IntegerProperty tpProperty() {
        return tp;
    }

    public final double getTf() {
        return tf.get();
    }

    public final void setTf(double value) {
        tf.set(value);
    }

    public DoubleProperty tfProperty() {
        return tf;
    }

    public final double getPg() {
        return pg.get();
    }

    public final void setPg(double value) {
        pg.set(value);
    }

    public DoubleProperty pgProperty() {
        return pg;
    }

    public final double getPr() {
        return pr.get();
    }

    public final void setPr(double value) {
        pr.set(value);
    }

    public DoubleProperty prProperty() {
        return pr;
    }

    public final int getCln() {
        return cln.get();
    }

    public final void setCln(int value) {
        cln.set(value);
    }

    public IntegerProperty clnProperty() {
        return cln;
    }

    @Override
    public String toString() {
        return "Sd_Metas_Bid_001{" + "codrep=" + codrep + ", codmar=" + codmar + ", codcol=" + codcol + ", tipo=" + tipo + ", mostr=" + mostr + ", pm=" + pm + ", cd=" + cd + ", pv=" + pv + ", ka=" + ka + ", pp=" + pp + ", vp=" + vp + ", tp=" + tp + ", tf=" + tf + ", pg=" + pg + ", pr=" + pr + ", cln=" + cln + '}';
    }

}
