/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdStatusProduto;

/**
 *
 * @author cristiano.diego
 */
public class SomatorioCoresGestao {

    private final BooleanProperty selected = new SimpleBooleanProperty(true);
    private final StringProperty cod_cor = new SimpleStringProperty();
    private final IntegerProperty em_producao = new SimpleIntegerProperty();
    private final IntegerProperty produzido = new SimpleIntegerProperty();
    private final IntegerProperty planej_of = new SimpleIntegerProperty();
    private final IntegerProperty venda_total = new SimpleIntegerProperty();
    private final IntegerProperty multiplo = new SimpleIntegerProperty(0);

    private final ObjectProperty<SdStatusProduto> statusPcp = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusProduto> statusComercial = new SimpleObjectProperty<>();

    public SomatorioCoresGestao() {
    }

    public SomatorioCoresGestao(String cod_cor, Integer em_producao, Integer produzido,
            Integer planej_of, Integer venda_total) {
        this.cod_cor.set(cod_cor);
        this.em_producao.set(em_producao);
        this.produzido.set(produzido);
        this.planej_of.set(planej_of);
        this.venda_total.set(venda_total);
    }

    public SomatorioCoresGestao(String cod_cor, Integer em_producao, Integer produzido,
            Integer planej_of, Integer venda_total, Integer multiplo, SdStatusProduto statusComercial, SdStatusProduto statusPcp) {
        this.cod_cor.set(cod_cor);
        this.em_producao.set(em_producao);
        this.produzido.set(produzido);
        this.planej_of.set(planej_of);
        this.venda_total.set(venda_total);
        this.multiplo.set(multiplo);
        this.statusComercial.set(statusComercial);
        this.statusPcp.set(statusPcp);
    }

    public final String getCod_cor() {
        return cod_cor.get();
    }

    public final void setCod_cor(String value) {
        cod_cor.set(value);
    }

    public StringProperty cod_corProperty() {
        return cod_cor;
    }

    public final int getEm_producao() {
        return em_producao.get();
    }

    public final void setEm_producao(int value) {
        em_producao.set(value);
    }

    public IntegerProperty em_producaoProperty() {
        return em_producao;
    }

    public final int getProduzido() {
        return produzido.get();
    }

    public final void setProduzido(int value) {
        produzido.set(value);
    }

    public IntegerProperty produzidoProperty() {
        return produzido;
    }

    public final int getPlanej_of() {
        return planej_of.get();
    }

    public final void setPlanej_of(int value) {
        planej_of.set(value);
    }

    public IntegerProperty planej_ofProperty() {
        return planej_of;
    }

    public final int getVenda_total() {
        return venda_total.get();
    }

    public final void setVenda_total(int value) {
        venda_total.set(value);
    }

    public IntegerProperty venda_totalProperty() {
        return venda_total;
    }

    public final int getMultiplo() {
        return multiplo.get();
    }

    public final void setMultiplo(int value) {
        multiplo.set(value);
    }

    public IntegerProperty multiploProperty() {
        return multiplo;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public SdStatusProduto getStatusPcp() {
        return statusPcp.get();
    }

    public ObjectProperty<SdStatusProduto> statusPcpProperty() {
        return statusPcp;
    }

    public void setStatusPcp(SdStatusProduto statusPcp) {
        this.statusPcp.set(statusPcp);
    }

    public SdStatusProduto getStatusComercial() {
        return statusComercial.get();
    }

    public ObjectProperty<SdStatusProduto> statusComercialProperty() {
        return statusComercial;
    }

    public void setStatusComercial(SdStatusProduto statusComercial) {
        this.statusComercial.set(statusComercial);
    }

    @Override
    public String toString() {
        return "SomatorioCoresGestao{" + "cod_cor=" + cod_cor + ", em_producao=" + em_producao + ", produzido=" + produzido + ", planej_of=" + planej_of + ", venda_total=" + venda_total + ", multiplo=" + multiplo + '}';
    }

}
