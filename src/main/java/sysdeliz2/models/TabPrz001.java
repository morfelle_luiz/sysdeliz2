/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
@Deprecated
public class TabPrz001 {

    private final StringProperty prazo = new SimpleStringProperty();
    private final StringProperty dtFim = new SimpleStringProperty();
    private final StringProperty dtInicio = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty minuto = new SimpleIntegerProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public TabPrz001(String prazo, String dtFim, String dtInicio, String descricao, Integer minuto, String ativo, String tipo) {
        this.prazo.set(prazo);
        this.dtFim.set(dtFim);
        this.dtInicio.set(dtInicio);
        this.descricao.set(descricao);
        this.minuto.set(minuto);
        this.ativo.set(ativo.equals("S"));
        this.tipo.set(tipo);
    }

    public final String getPrazo() {
        return prazo.get();
    }

    public final void setPrazo(String value) {
        prazo.set(value);
    }

    public StringProperty prazoProperty() {
        return prazo;
    }

    public final String getDtFim() {
        return dtFim.get();
    }

    public final void setDtFim(String value) {
        dtFim.set(value);
    }

    public StringProperty dtFimProperty() {
        return dtFim;
    }

    public final String getDtInicio() {
        return dtInicio.get();
    }

    public final void setDtInicio(String value) {
        dtInicio.set(value);
    }

    public StringProperty dtInicioProperty() {
        return dtInicio;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final int getMinuto() {
        return minuto.get();
    }

    public final void setMinuto(int value) {
        minuto.set(value);
    }

    public IntegerProperty minutoProperty() {
        return minuto;
    }

    public final boolean isAtivo() {
        return ativo.get();
    }

    public final void setAtivo(boolean value) {
        ativo.set(value);
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

}
