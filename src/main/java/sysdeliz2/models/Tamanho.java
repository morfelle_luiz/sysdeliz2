/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author lima.joao
 */
public class Tamanho {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty perctam = new SimpleStringProperty();

    public Tamanho(String codigo, String tam, String colecao, String perctam){
        this.codigo.set(codigo);
        this.tam.set(tam);
        this.colecao.set(colecao);
        this.perctam.set(perctam);
    }
    
    public String getPerctam() {
        return perctam.get();
    }

    public void setPerctam(String value) {
        perctam.set(value);
    }

    public StringProperty perctamProperty() {
        return perctam;
    }
        
    public String getColecao() {
        return colecao.get();
    }

    public void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }
    
    
    public String getTam() {
        return tam.get();
    }

    public void setTam(String value) {
        tam.set(value);
    }

    public StringProperty tamProperty() {
        return tam;
    }
        
    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public Double getPercTamAsDouble(){
        if(this.getPerctam() == null || this.getPerctam().equals("0") || this.getPerctam().equals("")){
            return 0.0;
        } else {
            return Double.parseDouble(this.getPerctam());
        }
    }
}
