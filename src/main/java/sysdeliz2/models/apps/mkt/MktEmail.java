package sysdeliz2.models.apps.mkt;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MKT_EMAIL_001")
@IdClass(MktEmail.MktEmailPk.class)
public class MktEmail implements Serializable {
    private final StringProperty email = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public MktEmail() {
    }

    @Id
    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Id
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    static class MktEmailPk implements Serializable{
        private final StringProperty email = new SimpleStringProperty();
        private final StringProperty tipo = new SimpleStringProperty();

        public MktEmailPk() {
        }

        public String getEmail() {
            return email.get();
        }

        public StringProperty emailProperty() {
            return email;
        }

        public void setEmail(String email) {
            this.email.set(email);
        }


        public String getTipo() {
            return tipo.get();
        }

        public StringProperty tipoProperty() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo.set(tipo);
        }
    }

    public enum TipoEmail {
        C("Comercial", "success"),
        MD("Marketing DLZ", "primary"),
        MF("Marketing Flor de Lis", "warning"),
        F("Fornecedor", "info");


        private final String descricao;
        private final String style;


        TipoEmail(String descricao, String style) {
            this.descricao = descricao;
            this.style = style;
        }

        public String getDescricao() {
            return descricao;
        }

        public String getStyle() {
            return style;
        }


        @Override
        public String toString() {
            return "[" + this.name() + "] " + descricao;
        }
    }
}
