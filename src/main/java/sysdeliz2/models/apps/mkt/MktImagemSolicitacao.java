package sysdeliz2.models.apps.mkt;

import javafx.beans.property.*;
import javafx.scene.image.Image;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "MKT_IMAGEM_SOLICITACAO_001")
public class MktImagemSolicitacao {
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty url = new SimpleStringProperty();
    private final StringProperty direcao = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<Image> imagem = new SimpleObjectProperty<>();

    public MktImagemSolicitacao() {
    }

    public MktImagemSolicitacao(String url, Marca marca, int ordem) {
        this.url.set(url);
        this.marca.set(marca);
        this.ordem.set(ordem);
        this.direcao.set("H");
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MKT_IMAGEM")
    @SequenceGenerator(sequenceName = "SEQ_SD_MKT_IMAGEM", name = "SEQ_SD_MKT_IMAGEM", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @Column(name = "URL")
    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @OneToOne
    @JoinColumn(name = "MARCA", referencedColumnName = "CODIGO")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "DIRECAO")
    public String getDirecao() {
        return direcao.get();
    }

    public StringProperty direcaoProperty() {
        return direcao;
    }

    public void setDirecao(String direcao) {
        this.direcao.set(direcao);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Transient
    public Image getImagem() {
        return imagem.get();
    }

    public ObjectProperty<Image> imagemProperty() {
        return imagem;
    }

    public void setImagem(Image imagem) {
        this.imagem.set(imagem);
    }

    @PostLoad
    public void loadImage() {
        this.setImagem(new Image(this.getUrl(), 160, 120, true, true, false));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MktImagemSolicitacao that = (MktImagemSolicitacao) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
