package sysdeliz2.models.apps.mkt;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "MKT_ITEM_SOLICITACAO_001")
@IdClass(MktItemSolicitacaoPk.class)
public class MktItemSolicitacao extends BasicModel implements Serializable {
    private final ObjectProperty<MktSolicitacao> solicitacao = new SimpleObjectProperty<>();
    private final ObjectProperty<MktTipoSolicitacao> tipo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> altura = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty codModelagem = new SimpleIntegerProperty();
    private final ObjectProperty<MktImagemSolicitacao> imagem = new SimpleObjectProperty<>();
    private final StringProperty pathImagem = new SimpleStringProperty();
    private final BooleanProperty enviadoFornecedor = new SimpleBooleanProperty();

    public MktItemSolicitacao() {
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITACAO", referencedColumnName = "ID")
    public MktSolicitacao getSolicitacao() {
        return solicitacao.get();
    }

    public ObjectProperty<MktSolicitacao> solicitacaoProperty() {
        return solicitacao;
    }

    public void setSolicitacao(MktSolicitacao solicitacao) {
        this.solicitacao.set(solicitacao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO", referencedColumnName = "ID")
    public MktTipoSolicitacao getTipo() {
        return tipo.get();
    }

    public ObjectProperty<MktTipoSolicitacao> tipoProperty() {
        return tipo;
    }

    public void setTipo(MktTipoSolicitacao tipo) {
        this.tipo.set(tipo);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "ALTURA")
    public BigDecimal getAltura() {
        return altura.get();
    }

    public ObjectProperty<BigDecimal> alturaProperty() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura.set(altura);
    }

    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }

    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Id
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "CODMODELAGEM")
    public int getCodModelagem() {
        return codModelagem.get();
    }

    public IntegerProperty codModelagemProperty() {
        return codModelagem;
    }

    public void setCodModelagem(int codModelagem) {
        this.codModelagem.set(codModelagem);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IMAGEM", referencedColumnName = "ID")
    public MktImagemSolicitacao getImagem() {
        return imagem.get();
    }

    public ObjectProperty<MktImagemSolicitacao> imagemProperty() {
        return imagem;
    }

    public void setImagem(MktImagemSolicitacao imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "PATH_IMAGEM")
    public String getPathImagem() {
        return pathImagem.get();
    }

    public StringProperty pathImagemProperty() {
        return pathImagem;
    }

    public void setPathImagem(String pathImagem) {
        this.pathImagem.set(pathImagem);
    }

    @Column(name = "ENVIADO_FORNECEDOR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviadoFornecedor() {
        return enviadoFornecedor.get();
    }

    public BooleanProperty enviadoFornecedorProperty() {
        return enviadoFornecedor;
    }

    public void setEnviadoFornecedor(boolean enviadoFornecedor) {
        this.enviadoFornecedor.set(enviadoFornecedor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MktItemSolicitacao that = (MktItemSolicitacao) o;
        return Objects.equals(solicitacao, that.solicitacao) && Objects.equals(ordem, that.ordem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(solicitacao, ordem);
    }
}
