package sysdeliz2.models.apps.mkt;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.io.Serializable;
import java.util.Objects;

public class MktItemSolicitacaoPk implements Serializable {

    private final ObjectProperty<MktSolicitacao> solicitacao = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public MktItemSolicitacaoPk() {
    }

    public MktSolicitacao getSolicitacao() {
        return solicitacao.get();
    }

    public ObjectProperty<MktSolicitacao> solicitacaoProperty() {
        return solicitacao;
    }

    public void setSolicitacao(MktSolicitacao solicitacao) {
        this.solicitacao.set(solicitacao);
    }

    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MktItemSolicitacaoPk that = (MktItemSolicitacaoPk) o;
        return Objects.equals(solicitacao, that.solicitacao) && Objects.equals(ordem, that.ordem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(solicitacao, ordem);
    }
}
