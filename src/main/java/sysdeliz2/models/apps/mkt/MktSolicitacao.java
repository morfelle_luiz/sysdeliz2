package sysdeliz2.models.apps.mkt;

import javafx.beans.property.*;
import org.hibernate.annotations.WhereJoinTable;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "MKT_SOLICITACAO_001")
public class MktSolicitacao {


    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosCliente> codcli = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<MktStatusSolicitacao> status = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dataAtualizacao = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final BooleanProperty ofVerificada = new SimpleBooleanProperty();
    private List<MktItemSolicitacao> itens = new ArrayList<>();

    public MktSolicitacao() {
    }

    @Id
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }

    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }

    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public VSdDadosCliente getCodcli() {
        return codcli.get();
    }

    public ObjectProperty<VSdDadosCliente> codcliProperty() {
        return codcli;
    }

    public void setCodcli(VSdDadosCliente codcli) {
        this.codcli.set(codcli);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public MktStatusSolicitacao getStatus() {
        return status.get();
    }

    public ObjectProperty<MktStatusSolicitacao> statusProperty() {
        return status;
    }

    public void setStatus(MktStatusSolicitacao status) {
        this.status.set(status);
    }


    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }


    @Column(name = "DATA_ATUALIZACAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataAtualizacao() {
        return dataAtualizacao.get();
    }

    public ObjectProperty<LocalDate> dataAtualizacaoProperty() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDate dataAtualizacao) {
        this.dataAtualizacao.set(dataAtualizacao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "OF_VERIFICADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isOfVerificada() {
        return ofVerificada.get();
    }

    public BooleanProperty ofVerificadaProperty() {
        return ofVerificada;
    }

    public void setOfVerificada(boolean ofVerificada) {
        this.ofVerificada.set(ofVerificada);
    }

    @OneToMany
    @JoinColumn(name = "SOLICITACAO", referencedColumnName = "ID")
    public List<MktItemSolicitacao> getItens() {
        return itens;
    }

    public void setItens(List<MktItemSolicitacao> itens) {
        this.itens = itens;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MktSolicitacao that = (MktSolicitacao) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
