package sysdeliz2.models.apps.mkt;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "MKT_STATUS_SOLICITACAO_001")
public class MktStatusSolicitacao {
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final IntegerProperty mediaDias = new SimpleIntegerProperty();
    private final IntegerProperty limiteDias = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty style = new SimpleStringProperty();
    private final BooleanProperty visivel = new SimpleBooleanProperty();


    public MktStatusSolicitacao() {
    }

    public MktStatusSolicitacao(int codigo) {
        this.codigo.set(codigo);
        this.status.set("Todos os Status");
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "MEDIA_DIAS")
    public int getMediaDias() {
        return mediaDias.get();
    }

    public IntegerProperty mediaDiasProperty() {
        return mediaDias;
    }

    public void setMediaDias(int mediaDias) {
        this.mediaDias.set(mediaDias);
    }

    @Column(name = "LIMITE_DIAS")
    public int getLimiteDias() {
        return limiteDias.get();
    }

    public IntegerProperty limiteDiasProperty() {
        return limiteDias;
    }

    public void setLimiteDias(int limiteDias) {
        this.limiteDias.set(limiteDias);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "STYLE")
    public String getStyle() {
        return style.get();
    }

    public StringProperty styleProperty() {
        return style;
    }

    public void setStyle(String style) {
        this.style.set(style);
    }

    @Column(name = "VISIVEL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isVisivel() {
        return visivel.get();
    }

    public BooleanProperty visivelProperty() {
        return visivel;
    }

    public void setVisivel(boolean visivel) {
        this.visivel.set(visivel);
    }

    @Override
    public String toString() {
        return codigo.get() + " - " + status.get();
    }
}
