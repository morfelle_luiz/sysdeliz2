package sysdeliz2.models.apps.mkt;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

@Entity
@TelaSysDeliz(descricao = "Materiais", icon = "marketing_50.png")
@Table(name = "MKT_TIPOS_SOLICITACAO_001")
public class MktTipoSolicitacao extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "id")
    private final IntegerProperty id = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Descrição", width = 270)
    @ColunaFilter(descricao = "Descrição", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    private final BooleanProperty largura = new SimpleBooleanProperty(false);
    private final BooleanProperty altura = new SimpleBooleanProperty(false);
    private final BooleanProperty qtde = new SimpleBooleanProperty(false);
    private final BooleanProperty imagem = new SimpleBooleanProperty(false);
    private final BooleanProperty fornPadrao = new SimpleBooleanProperty(false);
    private final BooleanProperty kits = new SimpleBooleanProperty(false);
    private final BooleanProperty fornecedor = new SimpleBooleanProperty(false);
    @ExibeTableView(descricao = "Ativo", width = 70)
    @ColunaFilter(descricao = "Ativo", coluna = "ativo")
    private final BooleanProperty ativo = new SimpleBooleanProperty(null,"Ativo", false);
    private final BooleanProperty comCodigoMkt = new SimpleBooleanProperty(null,"C/CodigoMkt", false);
    @Transient
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final StringProperty codMktD = new SimpleStringProperty();
    private final StringProperty codMKtF = new SimpleStringProperty();
    private final StringProperty imgPadraoD = new SimpleStringProperty();
    private final StringProperty imgPadraoF = new SimpleStringProperty();

    public MktTipoSolicitacao() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MKT_TIPOS")
    @SequenceGenerator(sequenceName = "SEQ_SD_MKT_TIPOS", name = "SEQ_SD_MKT_TIPOS", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.getValue();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
        setCodigoFilter(String.valueOf(id));
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
        setDescricaoFilter(tipo);
    }

    @Column(name = "LARGURA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLargura() {
        return largura.get();
    }

    public BooleanProperty larguraProperty() {
        return largura;
    }

    public void setLargura(boolean largura) {
        this.largura.set(largura);
    }

    @Column(name = "ALTURA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAltura() {
        return altura.get();
    }

    public BooleanProperty alturaProperty() {
        return altura;
    }

    public void setAltura(boolean altura) {
        this.altura.set(altura);
    }

    @Column(name = "QTDE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isQtde() {
        return qtde.get();
    }

    public BooleanProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(boolean qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "IMAGEM")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImagem() {
        return imagem.get();
    }

    public BooleanProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(boolean imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "KITS")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isKits() {
        return kits.get();
    }

    public BooleanProperty kitsProperty() {
        return kits;
    }

    public void setKits(boolean kits) {
        this.kits.set(kits);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "FORN_PADRAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFornPadrao() {
        return fornPadrao.get();
    }

    public BooleanProperty fornPadraoProperty() {
        return fornPadrao;
    }

    public void setFornPadrao(boolean fornPadrao) {
        this.fornPadrao.set(fornPadrao);
    }

    @Column(name = "COM_CODIGO_MKT")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isComCodigoMkt() {
        return comCodigoMkt.get();
    }

    public BooleanProperty comCodigoMktProperty() {
        return comCodigoMkt;
    }

    public void setComCodigoMkt(boolean comCodigoMkt) {
        this.comCodigoMkt.set(comCodigoMkt);
    }

    @Column(name = "FORNECEDOR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFornecedor() {
        return fornecedor.get();
    }

    public BooleanProperty fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(boolean fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    @Column(name = "COD_MKT_D")
    public String getCodMktD() {
        return codMktD.get();
    }

    public StringProperty codMktDProperty() {
        return codMktD;
    }

    public void setCodMktD(String codMktD) {
        this.codMktD.set(codMktD);
    }

    @Column(name = "COD_MKT_F")
    public String getCodMKtF() {
        return codMKtF.get();
    }

    public StringProperty codMKtFProperty() {
        return codMKtF;
    }

    public void setCodMKtF(String codMKtF) {
        this.codMKtF.set(codMKtF);
    }

    @Column(name = "IMG_PADRAO_D")
    public String getImgPadraoD() {
        return imgPadraoD.get();
    }

    public StringProperty imgPadraoDProperty() {
        return imgPadraoD;
    }

    public void setImgPadraoD(String imgPadraoD) {
        this.imgPadraoD.set(imgPadraoD);
    }

    @Column(name = "IMG_PADRAO_F")
    public String getImgPadraoF() {
        return imgPadraoF.get();
    }

    public StringProperty imgPadraoFProperty() {
        return imgPadraoF;
    }

    public void setImgPadraoF(String imgPadraoF) {
        this.imgPadraoF.set(imgPadraoF);
    }

    @Transient
    public boolean isEmEdicao() {
        return emEdicao.get();
    }

    public BooleanProperty emEdicaoProperty() {
        return emEdicao;
    }

    public void setEmEdicao(boolean emEdicao) {
        this.emEdicao.set(emEdicao);
    }
}
