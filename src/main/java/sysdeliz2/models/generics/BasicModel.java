package sysdeliz2.models.generics;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.beans.Transient;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 10/09/2019 16:18
 */
public class BasicModel implements Serializable {

    public final BooleanProperty selected = new SimpleBooleanProperty(this, "selected", false);
    private final StringProperty codigoFilter = new SimpleStringProperty();
    private final StringProperty descricaoFilter = new SimpleStringProperty();

    @Transient
    public boolean isSelected() {
        return selected.get();
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
    
    @Transient
    public String getCodigoFilter() {
        return codigoFilter.get();
    }
    
    public StringProperty codigoFilterProperty() {
        return codigoFilter;
    }
    
    public void setCodigoFilter(String codigoFilter) {
        this.codigoFilter.set(codigoFilter);
    }
    
    @Transient
    public String getDescricaoFilter() {
        return descricaoFilter.get();
    }
    
    public StringProperty descricaoFilterProperty() {
        return descricaoFilter;
    }
    
    public void setDescricaoFilter(String descricaoFilter) {
        this.descricaoFilter.set(descricaoFilter);
    }

    @Transient
    public void unselect() {
        selected.set(false);
    }

    @Transient
    public void select() {
        selected.set(true);
    }
}
