package sysdeliz2.models.properties;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.math.BigDecimal;

public class AgregateMrpTinturaria {
    
    private final StringProperty deposito = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> sumConsumido = new SimpleObjectProperty<>();
    private final ObjectProperty<Integer> countConsumido = new SimpleObjectProperty<>();
    
    public AgregateMrpTinturaria() {
    }
    
    public AgregateMrpTinturaria(String deposito, BigDecimal sumConsumido, Integer countConsumido) {
        this.deposito.set(deposito);
        this.sumConsumido.set(sumConsumido);
        this.countConsumido.set(countConsumido);
    }
    
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    public BigDecimal getSumConsumido() {
        return sumConsumido.get();
    }
    
    public ObjectProperty<BigDecimal> sumConsumidoProperty() {
        return sumConsumido;
    }
    
    public void setSumConsumido(BigDecimal sumConsumido) {
        this.sumConsumido.set(sumConsumido);
    }
    
    public Integer getCountConsumido() {
        return countConsumido.get();
    }
    
    public ObjectProperty<Integer> countConsumidoProperty() {
        return countConsumido;
    }
    
    public void setCountConsumido(Integer countConsumido) {
        this.countConsumido.set(countConsumido);
    }
}
