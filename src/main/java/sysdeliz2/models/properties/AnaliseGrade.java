package sysdeliz2.models.properties;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdAnaliseGrade;
import sysdeliz2.models.ti.Faixa;
import sysdeliz2.models.ti.FaixaItem;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lima.joao
 * @since 02/09/2019 16:53
 */
public class AnaliseGrade {

    private final StringProperty colecao = new SimpleStringProperty(this, "colecao");
    private final IntegerProperty posicao = new SimpleIntegerProperty(this, "posicao");
    private final StringProperty linha = new SimpleStringProperty(this, "linha");
    private final StringProperty modelagem = new SimpleStringProperty(this, "modelagem");
    private final StringProperty classe = new SimpleStringProperty(this, "classe");

    private final ObservableList<AnaliseGradeDetalhe> detalhe = FXCollections.observableArrayList();

    public AnaliseGrade() {
    }

    public AnaliseGrade(String colecao, Integer posicao, String linha, String modelagem, String classe) {
        this.colecao.set(colecao);
        this.posicao.set(posicao);
        this.linha.set(linha);
        this.modelagem.set(modelagem);
        this.classe.set(classe);
    }

    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    public int getPosicao() {
        return posicao.get();
    }

    public IntegerProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao.set(posicao);
    }

    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }

    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    public ObservableList<AnaliseGradeDetalhe> getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(ObservableList<AnaliseGradeDetalhe> detalhe) {
        this.detalhe.setAll(detalhe);
    }

    public static ObservableList<AnaliseGrade> buildBySdAnaliseGrade(List<SdAnaliseGrade> list) throws SQLException {
        String colecao = "";
        AnaliseGrade analise = null;
        AnaliseGrade analiseMedia = null;
        GenericDao<Faixa> faixaDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), Faixa.class);
        Faixa tmpFaixa = null;

        ObservableList<AnaliseGrade> observable = FXCollections.observableArrayList();

        for (SdAnaliseGrade sdAnaliseGrade : list) {

            if(!colecao.equals(sdAnaliseGrade.getColecao())){
                if(analise != null){
                    observable.add(analise);
                }
                analise = new AnaliseGrade();

                if(tmpFaixa == null){
                    tmpFaixa = faixaDao.load(sdAnaliseGrade.getFaixa());
                }

                if(analiseMedia == null){
                    analiseMedia = new AnaliseGrade();

                    analiseMedia.setColecao("Média");
                    analiseMedia.setLinha(sdAnaliseGrade.getLinha());
                    analiseMedia.setModelagem(sdAnaliseGrade.getModelagem());
                    analiseMedia.setClasse(sdAnaliseGrade.getClasse());

                    for (FaixaItem iten : tmpFaixa.getItens()) {
                        // Gera o detalhe da média
                        analiseMedia.getDetalhe().add(new AnaliseGradeDetalhe(
                                iten.getFaixaItemId().getTamanho(),
                                sdAnaliseGrade.getFaixa(),
                                0,
                                BigDecimal.ZERO
                        ));
                    }
                }

                analise.setColecao(sdAnaliseGrade.getColecao());
                analise.setLinha(sdAnaliseGrade.getLinha());
                analise.setModelagem(sdAnaliseGrade.getModelagem());
                analise.setClasse(sdAnaliseGrade.getClasse());

                for (FaixaItem iten : tmpFaixa.getItens()) {
                    analise.getDetalhe().add(new AnaliseGradeDetalhe(
                            iten.getFaixaItemId().getTamanho(),
                            sdAnaliseGrade.getFaixa(),
                            0,
                            BigDecimal.ZERO
                    ));
                }

                for (AnaliseGradeDetalhe analiseGradeDetalhe : analise.getDetalhe()) {
                    if(analiseGradeDetalhe.getTamanho().equals(sdAnaliseGrade.getTamanho())){
                        analiseGradeDetalhe.setTotalTamanho(sdAnaliseGrade.getTotalTamanho().intValue());
                        analiseGradeDetalhe.setPercTamanho(sdAnaliseGrade.getPercTamanho());
                    }
                }

                colecao = sdAnaliseGrade.getColecao();
            } else {
                assert analise != null;
                for (AnaliseGradeDetalhe analiseGradeDetalhe : analise.getDetalhe()) {
                    if(analiseGradeDetalhe.getTamanho().equals(sdAnaliseGrade.getTamanho())){
                        analiseGradeDetalhe.setTotalTamanho(sdAnaliseGrade.getTotalTamanho().intValue());
                        analiseGradeDetalhe.setPercTamanho(sdAnaliseGrade.getPercTamanho());
                    }
                }
            }
        }

        if(analise != null){
            observable.add(analise);
        }

        if(observable.size() > 0) {
            int count = observable.size();
            for (AnaliseGrade analiseGrade : observable) {
                for (AnaliseGradeDetalhe analiseGradeDetalhe : analiseGrade.getDetalhe()) {

                    for (AnaliseGradeDetalhe gradeDetalhe : analiseMedia.getDetalhe()) {
                        if (gradeDetalhe.getTamanho().equals(analiseGradeDetalhe.getTamanho())) {

                            gradeDetalhe.setTotalTamanho(gradeDetalhe.getTotalTamanho() + analiseGradeDetalhe.getTotalTamanho());
                            gradeDetalhe.setPercTamanho(gradeDetalhe.getPercTamanho().add(analiseGradeDetalhe.getPercTamanho()));
                        }
                    }
                }
            }
            Double diff = 0.0;
            for (AnaliseGradeDetalhe analiseGradeDetalhe : analiseMedia.getDetalhe()) {
                analiseGradeDetalhe.setTotalTamanho( analiseGradeDetalhe.getTotalTamanho() / count );
                BigDecimal valor = new BigDecimal(analiseGradeDetalhe.getPercTamanho().doubleValue() / count);
                valor = valor.setScale(0, BigDecimal.ROUND_HALF_UP);
                analiseGradeDetalhe.setPercTamanho( valor );
                diff += valor.doubleValue();
            }

            if(diff > 100.0){
                diff = diff - 100.0;
                analiseMedia.getDetalhe()
                        .get(analiseMedia.getDetalhe().size()-1)
                        .setPercTamanho(
                                analiseMedia.getDetalhe()
                                        .get(analiseMedia.getDetalhe().size()-1)
                                        .getPercTamanho()
                                        .subtract(new BigDecimal(diff)));
            } else {
                if (diff < 100.0) {
                    diff = 100.0 - diff;
                    analiseMedia.getDetalhe()
                            .get(analiseMedia.getDetalhe().size()-1)
                            .setPercTamanho(
                                    analiseMedia.getDetalhe()
                                            .get(analiseMedia.getDetalhe().size()-1)
                                            .getPercTamanho()
                                            .add(new BigDecimal(diff)));
                }
            }

            observable.add(analiseMedia);
        }
        return observable;
    }
}
