package sysdeliz2.models.properties;

import javafx.beans.property.*;

import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 03/09/2019 09:51
 */
public class AnaliseGradeDetalhe {

    private final StringProperty tamanho = new SimpleStringProperty(this, "tamanho");
    private final StringProperty faixa = new SimpleStringProperty(this, "faixa");
    private final IntegerProperty totalTamanho = new SimpleIntegerProperty(this, "totalTamanho");
    private final IntegerProperty totalFaixa = new SimpleIntegerProperty(this, "totalFaixa");
    private final ObjectProperty<BigDecimal> percTamanho = new SimpleObjectProperty<>(this, "percTamanho");

    public AnaliseGradeDetalhe() {
    }

    public AnaliseGradeDetalhe(String tamanho, String faixa, Integer totalTamanho, BigDecimal percTamanho) {
        this.tamanho.set(tamanho);
        this.faixa.set(faixa);
        this.totalTamanho.set(totalTamanho);
        this.percTamanho.set(percTamanho);
    }

    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    public int getTotalTamanho() {
        return totalTamanho.get();
    }

    public IntegerProperty totalTamanhoProperty() {
        return totalTamanho;
    }

    public void setTotalTamanho(int totalTamanho) {
        this.totalTamanho.set(totalTamanho);
    }

    public BigDecimal getPercTamanho() {
        return percTamanho.get();
    }

    public ObjectProperty<BigDecimal> percTamanhoProperty() {
        return percTamanho;
    }

    public void setPercTamanho(BigDecimal percTamanho) {
        this.percTamanho.set(percTamanho);
    }

    public int getTotalFaixa() {
        return totalFaixa.get();
    }

    public IntegerProperty totalFaixaProperty() {
        return totalFaixa;
    }

    public void setTotalFaixa(int totalFaixa) {
        this.totalFaixa.set(totalFaixa);
    }
}
