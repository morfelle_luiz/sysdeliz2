/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import sysdeliz2.models.IndicadorMetaBid;

/**
 *
 * @author cristiano.diego
 */
public class ComercialMetasBid {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty codmar = new SimpleStringProperty();
    private final StringProperty codcol = new SimpleStringProperty();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty respons = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty desc_col = new SimpleStringProperty();
    private final StringProperty desc_mar = new SimpleStringProperty();
    private final ListProperty<IndicadorMetaBid> metas = new SimpleListProperty<>();

    public ComercialMetasBid(String codrep, String codmar, String codcol,
            String represent, String respons, String cidade, String desc_col,
            String desc_mar, ObservableList<IndicadorMetaBid> indicadores) {
        this.codrep.set(codrep);
        this.codmar.set(codmar);
        this.codcol.set(codcol);
        this.represent.set(represent);
        this.respons.set(respons);
        this.cidade.set(cidade);
        this.desc_col.set(desc_col);
        this.desc_mar.set(desc_mar);
        this.metas.set(indicadores);
    }

    public final String getRepresent() {
        return represent.get();
    }

    public final void setRepresent(String value) {
        represent.set(value);
    }

    public StringProperty representProperty() {
        return represent;
    }

    public final String getRespons() {
        return respons.get();
    }

    public final void setRespons(String value) {
        respons.set(value);
    }

    public StringProperty responsProperty() {
        return respons;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getDesc_col() {
        return desc_col.get();
    }

    public final void setDesc_col(String value) {
        desc_col.set(value);
    }

    public StringProperty desc_colProperty() {
        return desc_col;
    }

    public final String getDesc_mar() {
        return desc_mar.get();
    }

    public final void setDesc_mar(String value) {
        desc_mar.set(value);
    }

    public StringProperty desc_marProperty() {
        return desc_mar;
    }

    public final String getCodrep() {
        return codrep.get();
    }

    public final void setCodrep(String value) {
        codrep.set(value);
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public final String getCodmar() {
        return codmar.get();
    }

    public final void setCodmar(String value) {
        codmar.set(value);
    }

    public StringProperty codmarProperty() {
        return codmar;
    }

    public final String getCodcol() {
        return codcol.get();
    }

    public final void setCodcol(String value) {
        codcol.set(value);
    }

    public StringProperty codcolProperty() {
        return codcol;
    }

    public final ObservableList<IndicadorMetaBid> getMetas() {
        return metas.get();
    }

    public final void setMetas(ObservableList<IndicadorMetaBid> value) {
        metas.set(value);
    }

    public ListProperty<IndicadorMetaBid> metasProperty() {
        return metas;
    }

    @Override
    public String toString() {
        return "ComercialMetasBid{" + "codrep=" + codrep + ", codmar=" + codmar + ", codcol=" + codcol + ", represent=" + represent + ", respons=" + respons + ", cidade=" + cidade + ", desc_col=" + desc_col + ", desc_mar=" + desc_mar + ", metas=" + metas + '}';
    }

}
