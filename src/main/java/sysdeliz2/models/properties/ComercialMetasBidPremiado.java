/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import sysdeliz2.models.IndicadorMetaBidPremiado;

/**
 *
 * @author cristiano.diego
 */
public class ComercialMetasBidPremiado {

    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty desc_colecao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty desc_marca = new SimpleStringProperty();
    private final ListProperty<IndicadorMetaBidPremiado> indicadores = new SimpleListProperty<>();

    public ComercialMetasBidPremiado(String colecao, String desc_colecao, String marca, String desc_marca, ObservableList<IndicadorMetaBidPremiado> indicadores) {
        this.colecao.set(colecao);
        this.desc_colecao.set(desc_colecao);
        this.marca.set(marca);
        this.desc_marca.set(desc_marca);
        this.indicadores.set(indicadores);
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getDesc_colecao() {
        return desc_colecao.get();
    }

    public final void setDesc_colecao(String value) {
        desc_colecao.set(value);
    }

    public StringProperty desc_colecaoProperty() {
        return desc_colecao;
    }

    public final String getMarca() {
        return marca.get();
    }

    public final void setMarca(String value) {
        marca.set(value);
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public final String getDesc_marca() {
        return desc_marca.get();
    }

    public final void setDesc_marca(String value) {
        desc_marca.set(value);
    }

    public StringProperty desc_marcaProperty() {
        return desc_marca;
    }

    public final ObservableList<IndicadorMetaBidPremiado> getIndicadores() {
        return indicadores.get();
    }

    public final void setIndicadores(ObservableList<IndicadorMetaBidPremiado> value) {
        indicadores.set(value);
    }

    public ListProperty<IndicadorMetaBidPremiado> indicadoresProperty() {
        return indicadores;
    }

    @Override
    public String toString() {
        return "ComercialMetasBidPremiado{" + "colecao=" + colecao + ", desc_colecao=" + desc_colecao + ", marca=" + marca + ", desc_marca=" + desc_marca + ", indicadores=" + indicadores + '}';
    }

}
