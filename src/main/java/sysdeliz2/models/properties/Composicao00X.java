/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class Composicao00X {

    private final StringProperty aplicacao = new SimpleStringProperty();
    private final StringProperty composicao = new SimpleStringProperty();

    public Composicao00X() {
    }

    public Composicao00X(String aplicacao, String composicao) {
        this.aplicacao.set(aplicacao);
        this.composicao.set(composicao);
    }

    public final String getAplicacao() {
        return aplicacao.get();
    }

    public final void setAplicacao(String value) {
        aplicacao.set(value);
    }

    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }

    public final String getComposicao() {
        return composicao.get();
    }

    public final void setComposicao(String value) {
        composicao.set(value);
    }

    public StringProperty composicaoProperty() {
        return composicao;
    }

}
