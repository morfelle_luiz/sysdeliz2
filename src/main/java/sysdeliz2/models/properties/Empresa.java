package sysdeliz2.models.properties;

import javafx.scene.image.Image;
import sysdeliz2.models.generics.BasicModel;

public class Empresa extends BasicModel {
    
    private String codigo;
    private String nome;
    private String cnpj;
    private Image iconeEmpresa;
    private Image altIconeEmpresa;

    public Empresa() {
    }
    
    public Empresa(String codigo, String nome, Image iconeEmpresa, String cnpj, Image altIconeEmpresa) {
        setCodigo(codigo);
        setNome(nome);
        setCnpj(cnpj);
        setIconeEmpresa(iconeEmpresa);
        setAltIconeEmpresa(altIconeEmpresa);
    }
    
    public String getCodigo() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
        setCodigoFilter(codigo);
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
        setDescricaoFilter(nome);
    }
    
    public String getCnpj() {
        return cnpj;
    }
    
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    public Image getIconeEmpresa() {
        return iconeEmpresa;
    }

    public void setIconeEmpresa(Image iconeEmpresa) {
        this.iconeEmpresa = iconeEmpresa;
    }

    public Image getAltIconeEmpresa() {
        return altIconeEmpresa;
    }

    public void setAltIconeEmpresa(Image altIconeEmpresa) {
        this.altIconeEmpresa = altIconeEmpresa;
    }
}
