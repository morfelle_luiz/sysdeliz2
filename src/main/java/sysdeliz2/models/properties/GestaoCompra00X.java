/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ExportExcel;
import sysdeliz2.utils.sys.annotations.HideExportExcel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * @author cristiano.diego
 */
public class GestaoCompra00X {

    private final StringProperty cnpj = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Fornecedor")
    private final StringProperty nome = new SimpleStringProperty();
    @ExportExcel(nameToShow = "O.C.")
    private final StringProperty numero = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Lote")
    private final StringProperty periodo = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Dt. Lote")
    private final StringProperty dtLote = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Lote Orig")
    private final StringProperty periodoOrig = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Dt. Lote Orig")
    private final StringProperty dtLoteOrig = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Pedido")
    private final StringProperty pedFornecedor = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Material")
    private final StringProperty codigo = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Descrição")
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Unid.")
    private final StringProperty unidade = new SimpleStringProperty();
    private final DoubleProperty qtde = new SimpleDoubleProperty();
    private final DoubleProperty custo = new SimpleDoubleProperty();
    private final DoubleProperty valor = new SimpleDoubleProperty();
    @ExportExcel(nameToShow = "Condição")
    private final StringProperty condicao = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Depósito")
    private final StringProperty deposito = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Dt. Entrega")
    private final ObjectProperty<LocalDate> dtEntrega = new SimpleObjectProperty<LocalDate>();
    @ExportExcel(nameToShow = "Dt. Emissão")
    private final ObjectProperty<LocalDate> dtEmissao = new SimpleObjectProperty<LocalDate>();
    @ExportExcel(nameToShow = "Dt. Fatura")
    private final ObjectProperty<LocalDate> dtFatura = new SimpleObjectProperty<LocalDate>();
    @ExportExcel(nameToShow = "Nota Fiscal")
    private final StringProperty fatura = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Especificação")
    private final StringProperty especifica = new SimpleStringProperty();

    @HideExportExcel
    private final StringProperty diasEntrega = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty minFornecedor = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty tam = new SimpleStringProperty();
    @HideExportExcel
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    @HideExportExcel
    private final StringProperty grupo = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty subGrupo = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty status = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty obsRecebimento = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty status_envio = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty corFornecedor = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty origem = new SimpleStringProperty();
    @HideExportExcel
    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    @HideExportExcel
    private final StringProperty codcli = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty email = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty tipo = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty ncm = new SimpleStringProperty();
    @HideExportExcel
    private final StringProperty dias = new SimpleStringProperty();

    public GestaoCompra00X() {
    }

    public GestaoCompra00X(String codcli, String cnpj, String nome, String diasEntrega, String minFornecedor, String email, String numero, String pedFornecedor,
                           String tipo, String codigo, String descricao, String cor, String tam, Integer ordem, String grupo,
                           String subGrupo, String unidade, Double qtde, Double custo, Double valor,
                           String deposito, String dtEntrega, String dtFatura, String fatura, String status,
                           String obsRecebimento, String condicao, String especifica, String status_envio, String ncm, String corFornecedor, String dtEmissao, String periodo, String periodoOrig, String origem) {
        this.codcli.set(codcli);
        this.cnpj.set(cnpj);
        this.nome.set(nome);
        this.diasEntrega.set(diasEntrega);
        this.minFornecedor.set(minFornecedor);
        this.email.set(email);
        this.numero.set(numero);
        this.pedFornecedor.set(pedFornecedor);
        this.tipo.set(tipo);
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.cor.set(cor);
        this.tam.set(tam);
        this.ordem.set(ordem);
        this.grupo.set(grupo);
        this.subGrupo.set(subGrupo);
        this.unidade.set(unidade);
        this.qtde.set(qtde);
        this.custo.set(custo);
        this.valor.set(valor);
        this.deposito.set(deposito);
        this.dtEntrega.set(LocalDate.parse(dtEntrega != null ? dtEntrega : "01/01/1889", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.dtFatura.set(LocalDate.parse(dtFatura != null ? dtFatura : "01/01/1889", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.fatura.set(fatura);
        this.status.set(status);
        this.obsRecebimento.set(obsRecebimento);
        this.condicao.set(condicao);
        this.especifica.set(especifica);
        this.status_envio.set(status_envio);
        this.ncm.set(ncm);
        this.corFornecedor.set(corFornecedor);
        this.dtEmissao.set(LocalDate.parse(dtEmissao != null ? dtEmissao : "01/01/1889", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.periodo.set(periodo);
        this.periodoOrig.set(periodoOrig);
        this.origem.set(origem);
        carregaPeriodos();
    }

    private void carregaPeriodos() {

        TabPrz lote = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", this.periodo.get())).singleResult();
        if (lote != null) {
            this.dias.set(String.valueOf(ChronoUnit.DAYS.between(LocalDate.now(), lote.getDtInicio().minusDays(3L))));
            this.dtLote.set(StringUtils.toShortDateFormat(lote.getDtInicio()));
        }

        TabPrz loteOrig = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", this.periodoOrig.get())).singleResult();
        if (loteOrig != null) {
            this.dtLoteOrig.set(StringUtils.toShortDateFormat(loteOrig.getDtInicio()));
        }
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public String getCnpj() {
        return cnpj.get();
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getPedFornecedor() {
        return pedFornecedor.get();
    }

    public final void setPedFornecedor(String value) {
        pedFornecedor.set(value);
    }

    public StringProperty pedFornecedorProperty() {
        return pedFornecedor;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getSubGrupo() {
        return subGrupo.get();
    }

    public final void setSubGrupo(String value) {
        subGrupo.set(value);
    }

    public StringProperty subGrupoProperty() {
        return subGrupo;
    }

    public final String getUnidade() {
        return unidade.get();
    }

    public final void setUnidade(String value) {
        unidade.set(value);
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public final double getQtde() {
        return qtde.get();
    }

    public final void setQtde(double value) {
        qtde.set(value);
    }

    public DoubleProperty qtdeProperty() {
        return qtde;
    }

    public final double getCusto() {
        return custo.get();
    }

    public final void setCusto(double value) {
        custo.set(value);
    }

    public DoubleProperty custoProperty() {
        return custo;
    }

    public final double getValor() {
        return valor.get();
    }

    public final void setValor(double value) {
        valor.set(value);
    }

    public DoubleProperty valorProperty() {
        return valor;
    }

    public final String getDeposito() {
        return deposito.get();
    }

    public final void setDeposito(String value) {
        deposito.set(value);
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public final String getDtEntregaAsString() {
        String strDate = (null == dtEntrega || null == dtEntrega.get())
                ? "" : dtEntrega.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return strDate;
    }

    public final LocalDate getDtEntrega() {
        return dtEntrega.get();
    }

    public final void setDtEntrega(LocalDate value) {
        dtEntrega.set(value);
    }

    public ObjectProperty<LocalDate> dtEntregaProperty() {
        return dtEntrega;
    }

    public final LocalDate getDtFatura() {
        return dtFatura.get();
    }

    public final String getDtFaturaAsString() {
        String strDate = (null == dtFatura || null == dtFatura.get())
                ? "" : dtFatura.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return strDate;
    }

    public final void setDtFatura(LocalDate value) {
        dtFatura.set(value);
    }

    public ObjectProperty<LocalDate> dtFaturaProperty() {
        return dtFatura;
    }

    public final String getFatura() {
        return fatura.get();
    }

    public final void setFatura(String value) {
        fatura.set(value);
    }

    public StringProperty faturaProperty() {
        return fatura;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getObsRecebimento() {
        return obsRecebimento.get();
    }

    public final void setObsRecebimento(String value) {
        obsRecebimento.set(value);
    }

    public StringProperty obsRecebimentoProperty() {
        return obsRecebimento;
    }

    public final String getCondicao() {
        return condicao.get();
    }

    public final void setCondicao(String value) {
        condicao.set(value);
    }

    public StringProperty condicaoProperty() {
        return condicao;
    }

    public final String getTam() {
        return tam.get();
    }

    public final void setTam(String value) {
        tam.set(value);
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public final int getOrdem() {
        return ordem.get();
    }

    public final void setOrdem(int value) {
        ordem.set(value);
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public final String getEspecifica() {
        return especifica.get();
    }

    public final void setEspecifica(String value) {
        especifica.set(value);
    }

    public StringProperty especificaProperty() {
        return especifica;
    }

    public final String getStatus_envio() {
        return status_envio.get();
    }

    public final void setStatus_envio(String value) {
        status_envio.set(value);
    }

    public StringProperty status_envioProperty() {
        return status_envio;
    }

    public final String getNcm() {
        return ncm.get();
    }

    public final void setNcm(String value) {
        ncm.set(value);
    }

    public StringProperty ncmProperty() {
        return ncm;
    }

    public final String getCorFornecedor() {
        return corFornecedor.get();
    }

    public final void setCorFornecedor(String value) {
        corFornecedor.set(value);
    }

    public StringProperty corFornecedorProperty() {
        return corFornecedor;
    }

    public String getDiasEntrega() {
        return diasEntrega.get();
    }

    public StringProperty diasEntregaProperty() {
        return diasEntrega;
    }

    public void setDiasEntrega(String diasEntrega) {
        this.diasEntrega.set(diasEntrega);
    }

    public String getMinFornecedor() {
        return minFornecedor.get();
    }

    public StringProperty minFornecedorProperty() {
        return minFornecedor;
    }

    public void setMinFornecedor(String minFornecedor) {
        this.minFornecedor.set(minFornecedor);
    }

    public LocalDate getDtEmissao() {
        return dtEmissao.get();
    }

    public final String getDtEmissaoAsString() {
        String strDate = (null == dtEmissao || null == dtEmissao.get())
                ? "" : dtEmissao.get().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return strDate;
    }

    public ObjectProperty<LocalDate> dtEmissaoProperty() {
        return dtEmissao;
    }

    public void setDtEmissao(LocalDate dtEmissao) {
        this.dtEmissao.set(dtEmissao);
    }

    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    public String getOrigem() {
        return origem.get();
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem.set(origem);
    }

    public String getDias() {
        return dias.get();
    }

    public StringProperty diasProperty() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias.set(dias);
    }

    public String getDtLote() {
        return dtLote.get();
    }

    public StringProperty dtLoteProperty() {
        return dtLote;
    }

    public void setDtLote(String dtLote) {
        this.dtLote.set(dtLote);
    }

    public String getPeriodoOrig() {
        return periodoOrig.get();
    }

    public StringProperty periodoOrigProperty() {
        return periodoOrig;
    }

    public void setPeriodoOrig(String periodoOrig) {
        this.periodoOrig.set(periodoOrig);
    }

    public String getDtLoteOrig() {
        return dtLoteOrig.get();
    }

    public StringProperty dtLoteOrigProperty() {
        return dtLoteOrig;
    }

    public void setDtLoteOrig(String dtLoteOrig) {
        this.dtLoteOrig.set(dtLoteOrig);
    }
}
