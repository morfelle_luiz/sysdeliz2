/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

/**
 *
 * @author cristiano.diego
 */
public class GestaoDeLote {

    private final StringProperty of = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty data_inicio = new SimpleStringProperty();
    private final StringProperty data_fim = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final IntegerProperty planejado = new SimpleIntegerProperty();
    private final IntegerProperty planejado_orig = new SimpleIntegerProperty();
    private final BooleanProperty locked = new SimpleBooleanProperty(false);
    private final BooleanProperty movimentarSetor = new SimpleBooleanProperty(false);
    private final ListProperty<GestaoDeLoteGrade> grade = new SimpleListProperty<>();

    public GestaoDeLote() {
    }

    public GestaoDeLote(String of, String lote, String data_inicio, String data_fim, String setor, Integer planejado, ObservableList<GestaoDeLoteGrade> grade, String colecao) {
        this.of.set(of);
        this.lote.set(lote);
        this.data_inicio.set(data_inicio);
        this.data_fim.set(data_fim);
        this.planejado.set(planejado);
        this.planejado_orig.set(planejado);
        this.grade.set(grade);
        this.setor.set(setor);
        this.colecao.set(colecao);
    }

    public final String getLote() {
        return lote.get();
    }

    public final void setLote(String value) {
        lote.set(value);
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public final String getData_inicio() {
        return data_inicio.get();
    }

    public final void setData_inicio(String value) {
        data_inicio.set(value);
    }

    public StringProperty data_inicioProperty() {
        return data_inicio;
    }

    public final String getData_fim() {
        return data_fim.get();
    }

    public final void setData_fim(String value) {
        data_fim.set(value);
    }

    public StringProperty data_fimProperty() {
        return data_fim;
    }

    public final int getPlanejado() {
        return planejado.get();
    }

    public final void setPlanejado(int value) {
        planejado.set(value);
    }

    public IntegerProperty planejadoProperty() {
        return planejado;
    }

    public final ObservableList<GestaoDeLoteGrade> getGrade() {
        return grade.get();
    }

    public final void setGrade(ObservableList<GestaoDeLoteGrade> value) {
        grade.set(value);
    }

    public ListProperty<GestaoDeLoteGrade> gradeProperty() {
        return grade;
    }

    public final boolean isLocked() {
        return locked.get();
    }

    public final void setLocked(boolean value) {
        locked.set(value);
    }

    public BooleanProperty lockedProperty() {
        return locked;
    }

    public final String getOf() {
        return of.get();
    }

    public final void setOf(String value) {
        of.set(value);
    }

    public StringProperty ofProperty() {
        return of;
    }

    public final String getSetor() {
        return setor.get();
    }

    public final void setSetor(String value) {
        setor.set(value);
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public final int getPlanejado_orig() {
        return planejado_orig.get();
    }

    public final void setPlanejado_orig(int value) {
        planejado_orig.set(value);
    }

    public IntegerProperty planejado_origProperty() {
        return planejado_orig;
    }

    public final boolean isMovimentarSetor() {
        return movimentarSetor.get();
    }

    public final void setMovimentarSetor(boolean value) {
        movimentarSetor.set(value);
    }

    public BooleanProperty movimentarSetorProperty() {
        return movimentarSetor;
    }

    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Override
    public String toString() {
        return "GestaoDeLote{" + "of=" + of + ", lote=" + lote + ", data_inicio=" + data_inicio + ", data_fim=" + data_fim + ", planejado=" + planejado + ", locked=" + locked + ", grade=" + grade + '}';
    }

}
