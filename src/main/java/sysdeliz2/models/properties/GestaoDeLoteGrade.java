/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class GestaoDeLoteGrade {

    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty cod_cor = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty planejado = new SimpleIntegerProperty();
    private final IntegerProperty planejado_orig = new SimpleIntegerProperty();

    private final BooleanProperty calcularGrade = new SimpleBooleanProperty(true);

    public GestaoDeLoteGrade() {
    }

    public GestaoDeLoteGrade(String lote, String cod_cor, String cor, String tam, Integer planejado) {
        this.lote.set(lote);
        this.cod_cor.set(cod_cor);
        this.cor.set(cor);
        this.tam.set(tam);
        this.planejado.set(planejado);
        this.planejado_orig.set(planejado);
    }

    public final String getLote() {
        return lote.get();
    }

    public final void setLote(String value) {
        lote.set(value);
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public final String getCod_cor() {
        return cod_cor.get();
    }

    public final void setCod_cor(String value) {
        cod_cor.set(value);
    }

    public StringProperty cod_corProperty() {
        return cod_cor;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getTam() {
        return tam.get();
    }

    public final void setTam(String value) {
        tam.set(value);
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public final int getPlanejado() {
        return planejado.get();
    }

    public final void setPlanejado(int value) {
        planejado.set(value);
    }

    public IntegerProperty planejadoProperty() {
        return planejado;
    }

    public final boolean isCalculaGrade() {
        return calcularGrade.get();
    }

    public final void setCalculaGrade(boolean value) {
        calcularGrade.set(value);
    }

    public BooleanProperty calculaGradeProperty() {
        return calcularGrade;
    }

    public final int getPlanejado_orig() {
        return planejado_orig.get();
    }

    public final void setPlanejado_orig(int value) {
        planejado_orig.set(value);
    }

    public IntegerProperty planejado_origProperty() {
        return planejado_orig;
    }

    public final boolean isCalcularGrade() {
        return calcularGrade.get();
    }

    public final void setCalcularGrade(boolean value) {
        calcularGrade.set(value);
    }

    public BooleanProperty calcularGradeProperty() {
        return calcularGrade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public String toString() {
        return "GestaoDeLoteGrade{" + "lote=" + lote + ", cod_cor=" + cod_cor + ", cor=" + cor + ", tam=" + tam + ", planejado=" + planejado + '}';
    }

}
