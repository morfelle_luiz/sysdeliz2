/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdStatusProduto;

/**
 *
 * @author cristiano.diego
 */
public class GestaoDeProduto {

    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty estoque = new SimpleIntegerProperty();
    private final IntegerProperty mostruario = new SimpleIntegerProperty();
    private final IntegerProperty em_producao = new SimpleIntegerProperty();
    private final IntegerProperty produzido = new SimpleIntegerProperty();
    private final IntegerProperty seg_qualidade = new SimpleIntegerProperty();
    private final IntegerProperty planej_of = new SimpleIntegerProperty();
    private final IntegerProperty simulacao = new SimpleIntegerProperty(0);
    private final IntegerProperty carteira = new SimpleIntegerProperty();
    private final IntegerProperty concentrado_pend = new SimpleIntegerProperty();
    private final IntegerProperty concentrado = new SimpleIntegerProperty();
    private final IntegerProperty venda_total = new SimpleIntegerProperty();
    private final IntegerProperty saldo_estoque = new SimpleIntegerProperty();

    private final IntegerProperty saldo_producao = new SimpleIntegerProperty();
    private final IntegerProperty saldo_producao_calc = new SimpleIntegerProperty();
    private final IntegerProperty saldo_producao_orig = new SimpleIntegerProperty();
    private final IntegerProperty saldo_planej = new SimpleIntegerProperty();
    private final DoubleProperty p_saldo_planej = new SimpleDoubleProperty(0.0);

    private final DoubleProperty p_prod_tamcor = new SimpleDoubleProperty();
    private final DoubleProperty p_prod_cor = new SimpleDoubleProperty();
    private final DoubleProperty p_prod_tam = new SimpleDoubleProperty();

    private final DoubleProperty p_venda_tamcor = new SimpleDoubleProperty();
    private final DoubleProperty p_venda_cor = new SimpleDoubleProperty();
    private final DoubleProperty p_venda_tam = new SimpleDoubleProperty();

    private final IntegerProperty status_pcp = new SimpleIntegerProperty();
    private final IntegerProperty status_comercial = new SimpleIntegerProperty();

    private final ObjectProperty<SdStatusProduto> statusPcp = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusProduto> statusComercial = new SimpleObjectProperty<>();

    public GestaoDeProduto() {
    }

    public GestaoDeProduto(String tam, String cor, Integer estoque, Integer mostruario, Integer em_producao, Integer produzido, Integer seg_qualidade, Integer planej_of,
            Integer carteira, Integer concentrado_pend, Integer concentrado, Integer venda_total, Integer saldo_estoque, Integer saldo_producao, Integer saldo_planej,
            Double p_venda_tam, Double p_venda_cor, Double p_prod_tam, Double p_prod_cor, Double p_prod_tamcor, Double p_venda_tamcor, Integer status_pcp, Integer status_comercial) {
        this.tam.set(tam);
        this.cor.set(cor);
        this.estoque.set(estoque);
        this.mostruario.set(mostruario);
        this.em_producao.set(em_producao);
        this.produzido.set(produzido);
        this.seg_qualidade.set(seg_qualidade);
        this.planej_of.set(planej_of);
        this.carteira.set(carteira);
        this.concentrado_pend.set(concentrado_pend);
        this.concentrado.set(concentrado);
        this.venda_total.set(venda_total);
        this.saldo_estoque.set(saldo_estoque);
        this.saldo_producao.set(saldo_producao);
        this.saldo_planej.set(saldo_planej);
        this.p_prod_cor.set(p_prod_cor);
        this.p_prod_tam.set(p_prod_tam);
        this.p_venda_cor.set(p_venda_cor);
        this.p_venda_tam.set(p_venda_tam);
        this.p_prod_tamcor.set(p_prod_tamcor);
        this.p_venda_tamcor.set(p_venda_tamcor);
        this.status_pcp.set(status_pcp);
        this.status_comercial.set(status_comercial);
        this.saldo_producao_orig.set(this.saldo_producao.get());
        this.saldo_producao_calc.set(this.saldo_producao.get());
        this.statusPcp.set(new FluentDao().selectFrom(SdStatusProduto.class).where(eb -> eb.equal("codigo", status_pcp)).singleResult());
        this.statusComercial.set(new FluentDao().selectFrom(SdStatusProduto.class).where(eb -> eb.equal("codigo", status_comercial)).singleResult());
    }

    public final String getTam() {
        return tam.get();
    }

    public final void setTam(String value) {
        tam.set(value);
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final int getEstoque() {
        return estoque.get();
    }

    public final void setEstoque(int value) {
        estoque.set(value);
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public final int getMostruario() {
        return mostruario.get();
    }

    public final void setMostruario(int value) {
        mostruario.set(value);
    }

    public IntegerProperty mostruarioProperty() {
        return mostruario;
    }

    public final int getEm_producao() {
        return em_producao.get();
    }

    public final void setEm_producao(int value) {
        em_producao.set(value);
    }

    public IntegerProperty em_producaoProperty() {
        return em_producao;
    }

    public final int getProduzido() {
        return produzido.get();
    }

    public final void setProduzido(int value) {
        produzido.set(value);
    }

    public IntegerProperty produzidoProperty() {
        return produzido;
    }

    public final int getSeg_qualidade() {
        return seg_qualidade.get();
    }

    public final void setSeg_qualidade(int value) {
        seg_qualidade.set(value);
    }

    public IntegerProperty seg_qualidadeProperty() {
        return seg_qualidade;
    }

    public final int getPlanej_of() {
        return planej_of.get();
    }

    public final void setPlanej_of(int value) {
        planej_of.set(value);
    }

    public IntegerProperty planej_ofProperty() {
        return planej_of;
    }

    public final int getSimulacao() {
        return simulacao.get();
    }

    public final void setSimulacao(int value) {
        simulacao.set(value);
    }

    public IntegerProperty simulacaoProperty() {
        return simulacao;
    }

    public final int getCarteira() {
        return carteira.get();
    }

    public final void setCarteira(int value) {
        carteira.set(value);
    }

    public IntegerProperty carteiraProperty() {
        return carteira;
    }

    public final int getConcentrado_pend() {
        return concentrado_pend.get();
    }

    public final void setConcentrado_pend(int value) {
        concentrado_pend.set(value);
    }

    public IntegerProperty concentrado_pendProperty() {
        return concentrado_pend;
    }

    public final int getConcentrado() {
        return concentrado.get();
    }

    public final void setConcentrado(int value) {
        concentrado.set(value);
    }

    public IntegerProperty concentradoProperty() {
        return concentrado;
    }

    public final int getVenda_total() {
        return venda_total.get();
    }

    public final void setVenda_total(int value) {
        venda_total.set(value);
    }

    public IntegerProperty venda_totalProperty() {
        return venda_total;
    }

    public final int getSaldo_estoque() {
        return saldo_estoque.get();
    }

    public final void setSaldo_estoque(int value) {
        saldo_estoque.set(value);
    }

    public IntegerProperty saldo_estoqueProperty() {
        return saldo_estoque;
    }

    public final int getSaldo_producao() {
        return saldo_producao.get();
    }

    public final void setSaldo_producao(int value) {
        saldo_producao.set(value);
    }

    public IntegerProperty saldo_producaoProperty() {
        return saldo_producao;
    }

    public final int getSaldo_planej() {
        return saldo_planej.get();
    }

    public final void setSaldo_planej(int value) {
        saldo_planej.set(value);
    }

    public IntegerProperty saldo_planejProperty() {
        return saldo_planej;
    }

    public final double getP_prod_cor() {
        return p_prod_cor.get();
    }

    public final void setP_prod_cor(double value) {
        p_prod_cor.set(value);
    }

    public DoubleProperty p_prod_corProperty() {
        return p_prod_cor;
    }

    public final double getP_prod_tam() {
        return p_prod_tam.get();
    }

    public final void setP_prod_tam(double value) {
        p_prod_tam.set(value);
    }

    public DoubleProperty p_prod_tamProperty() {
        return p_prod_tam;
    }

    public final double getP_venda_cor() {
        return p_venda_cor.get();
    }

    public final void setP_venda_cor(double value) {
        p_venda_cor.set(value);
    }

    public DoubleProperty p_venda_corProperty() {
        return p_venda_cor;
    }

    public final double getP_venda_tam() {
        return p_venda_tam.get();
    }

    public final void setP_venda_tam(double value) {
        p_venda_tam.set(value);
    }

    public DoubleProperty p_venda_tamProperty() {
        return p_venda_tam;
    }

    public final double getP_prod_tamcor() {
        return p_prod_tamcor.get();
    }

    public final void setP_prod_tamcor(double value) {
        p_prod_tamcor.set(value);
    }

    public DoubleProperty p_prod_tamcorProperty() {
        return p_prod_tamcor;
    }

    public final double getP_venda_tamcor() {
        return p_venda_tamcor.get();
    }

    public final void setP_venda_tamcor(double value) {
        p_venda_tamcor.set(value);
    }

    public DoubleProperty p_venda_tamcorProperty() {
        return p_venda_tamcor;
    }

    public final int getSaldo_producao_orig() {
        return saldo_producao_orig.get();
    }

    public final void setSaldo_producao_orig(int value) {
        saldo_producao_orig.set(value);
    }

    public IntegerProperty saldo_producao_origProperty() {
        return saldo_producao_orig;
    }

    public final int getSaldo_producao_calc() {
        return saldo_producao_calc.get();
    }

    public final void setSaldo_producao_calc(int value) {
        saldo_producao_calc.set(value);
    }

    public IntegerProperty saldo_producao_calcProperty() {
        return saldo_producao_calc;
    }

    public final double getP_saldo_planej() {
        return p_saldo_planej.get();
    }

    public final void setP_saldo_planej(double value) {
        p_saldo_planej.set(value);
    }

    public DoubleProperty p_saldo_planejProperty() {
        return p_saldo_planej;
    }

    public int getStatus_pcp() {
        return status_pcp.get();
    }

    public IntegerProperty status_pcpProperty() {
        return status_pcp;
    }

    public void setStatus_pcp(int status_pcp) {
        this.status_pcp.set(status_pcp);
    }

    public int getStatus_comercial() {
        return status_comercial.get();
    }

    public IntegerProperty status_comercialProperty() {
        return status_comercial;
    }

    public void setStatus_comercial(int status_comercial) {
        this.status_comercial.set(status_comercial);
    }

    public SdStatusProduto getStatusPcp() {
        return statusPcp.get();
    }

    public ObjectProperty<SdStatusProduto> statusPcpProperty() {
        return statusPcp;
    }

    public void setStatusPcp(SdStatusProduto statusPcp) {
        this.statusPcp.set(statusPcp);
    }

    public SdStatusProduto getStatusComercial() {
        return statusComercial.get();
    }

    public ObjectProperty<SdStatusProduto> statusComercialProperty() {
        return statusComercial;
    }

    public void setStatusComercial(SdStatusProduto statusComercial) {
        this.statusComercial.set(statusComercial);
    }

    @Override
    public String toString() {
        return "GestaoDeProduto{" + "tam=" + tam + ", cor=" + cor + ", estoque=" + estoque + ", mostruario=" + mostruario + ", em_producao=" + em_producao + ", produzido=" + produzido + ", seg_qualidade=" + seg_qualidade + ", planej_of=" + planej_of + ", simulacao=" + simulacao + ", carteira=" + carteira + ", concentrado_pend=" + concentrado_pend + ", concentrado=" + concentrado + ", venda_total=" + venda_total + ", saldo_estoque=" + saldo_estoque + ", saldo_producao=" + saldo_producao + ", saldo_producao_calc=" + saldo_producao_calc + ", saldo_producao_orig=" + saldo_producao_orig + ", saldo_planej=" + saldo_planej + ", p_prod_tamcor=" + p_prod_tamcor + ", p_prod_cor=" + p_prod_cor + ", p_prod_tam=" + p_prod_tam + ", p_venda_tamcor=" + p_venda_tamcor + ", p_venda_cor=" + p_venda_cor + ", p_venda_tam=" + p_venda_tam + '}';
    }

}
