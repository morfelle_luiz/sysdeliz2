/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;

/**
 *
 * @author cristiano.diego
 */
public class IndicadoresBidLinha {

    private final StringProperty linha = new SimpleStringProperty();
    private final IntegerProperty qtde_tp = new SimpleIntegerProperty();
    private final IntegerProperty meta_tp = new SimpleIntegerProperty();
    private final DoubleProperty att_tp = new SimpleDoubleProperty();
    private final IntegerProperty ly_tp = new SimpleIntegerProperty();
    private final DoubleProperty qtde_tf = new SimpleDoubleProperty();
    private final DoubleProperty meta_tf = new SimpleDoubleProperty();
    private final DoubleProperty att_tf = new SimpleDoubleProperty();
    private final DoubleProperty ly_tf = new SimpleDoubleProperty();
    private final DoubleProperty pg = new SimpleDoubleProperty();
    private final DoubleProperty pr = new SimpleDoubleProperty();

    public IndicadoresBidLinha() {
    }

    public IndicadoresBidLinha(String linha, Integer qtde_tp, Integer meta_tp, Double att_tp,
            Integer ly_tp, Double qtde_tf, Double meta_tf, Double att_tf, Double ly_tf, Double pg, Double pr) {
        this.att_tf.set(att_tf);
        this.att_tp.set(att_tp);
        this.linha.set(linha);
        this.ly_tf.set(ly_tf);
        this.ly_tp.set(ly_tp);
        this.meta_tf.set(meta_tf);
        this.meta_tp.set(meta_tp);
        this.pg.set(pg);
        this.pr.set(pr);
        this.qtde_tf.set(qtde_tf);
        this.qtde_tp.set(qtde_tp);
    }

    public final String getLinha() {
        return linha.get();
    }

    public final void setLinha(String value) {
        linha.set(value);
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public final int getQtde_tp() {
        return qtde_tp.get();
    }

    public final void setQtde_tp(int value) {
        qtde_tp.set(value);
    }

    public IntegerProperty qtde_tpProperty() {
        return qtde_tp;
    }

    public final int getMeta_tp() {
        return meta_tp.get();
    }

    public final void setMeta_tp(int value) {
        meta_tp.set(value);
    }

    public IntegerProperty meta_tpProperty() {
        return meta_tp;
    }

    public final double getAtt_tp() {
        return att_tp.get();
    }

    public final void setAtt_tp(double value) {
        att_tp.set(value);
    }

    public DoubleProperty att_tpProperty() {
        return att_tp;
    }

    public final int getLy_tp() {
        return ly_tp.get();
    }

    public final void setLy_tp(int value) {
        ly_tp.set(value);
    }

    public IntegerProperty ly_tpProperty() {
        return ly_tp;
    }

    public final double getQtde_tf() {
        return qtde_tf.get();
    }

    public final void setQtde_tf(double value) {
        qtde_tf.set(value);
    }

    public DoubleProperty qtde_tfProperty() {
        return qtde_tf;
    }

    public final double getMeta_tf() {
        return meta_tf.get();
    }

    public final void setMeta_tf(double value) {
        meta_tf.set(value);
    }

    public DoubleProperty meta_tfProperty() {
        return meta_tf;
    }

    public final double getAtt_tf() {
        return att_tf.get();
    }

    public final void setAtt_tf(double value) {
        att_tf.set(value);
    }

    public DoubleProperty att_tfProperty() {
        return att_tf;
    }

    public final double getLy_tf() {
        return ly_tf.get();
    }

    public final void setLy_tf(double value) {
        ly_tf.set(value);
    }

    public DoubleProperty ly_tfProperty() {
        return ly_tf;
    }

    public final double getPg() {
        return pg.get();
    }

    public final void setPg(double value) {
        pg.set(value);
    }

    public DoubleProperty pgProperty() {
        return pg;
    }

    public final double getPr() {
        return pr.get();
    }

    public final void setPr(double value) {
        pr.set(value);
    }

    public DoubleProperty prProperty() {
        return pr;
    }

    @Override
    public String toString() {
        return "IndicadoresBidLinha{" + "linha=" + linha + ", qtde_tp=" + qtde_tp + ", meta_tp=" + meta_tp + ", att_tp=" + att_tp + ", ly_tp=" + ly_tp + ", qtde_tf=" + qtde_tf + ", meta_tf=" + meta_tf + ", att_tf=" + att_tf + ", ly_tf=" + ly_tf + ", pg=" + pg + ", pr=" + pr + '}';
    }

}
