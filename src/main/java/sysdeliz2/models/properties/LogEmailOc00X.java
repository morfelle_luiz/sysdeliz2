/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class LogEmailOc00X {

    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty dt_envio = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();

    public LogEmailOc00X(String codcli, String nome, String numero, String codigo,
            String descricao, String cor, String dt_envio, String usuario) {
        this.codcli.set(codcli);
        this.nome.set(nome);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.cor.set(cor);
        this.dt_envio.set(dt_envio);
        this.usuario.set(usuario);
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getDescricao() {
        return descricao.get();
    }

    public final void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getDt_envio() {
        return dt_envio.get();
    }

    public final void setDt_envio(String value) {
        dt_envio.set(value);
    }

    public StringProperty dt_envioProperty() {
        return dt_envio;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

}
