package sysdeliz2.models.properties;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 16/09/2019 08:53
 */
public class OfGrade implements Serializable {
    private final StringProperty cor = new SimpleStringProperty(this, "cor");
    private final StringProperty nomeCor = new SimpleStringProperty(this, "nomeCor");
    private final ObservableList<OfGradeItem> ofGradeItens = FXCollections.observableArrayList();

    public OfGrade(String cor, String nomeCor) {
        this.cor.set(cor);
        this.nomeCor.set(nomeCor);
    }

    public OfGrade(String cor, String nomeCor, OfGradeItem ofGradeItem) {
        this.cor.set(cor);
        this.nomeCor.set(nomeCor);
        this.ofGradeItens.add(ofGradeItem);
    }

    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    public String getNomeCor() {
        return nomeCor.get();
    }

    public StringProperty nomeCorProperty() {
        return nomeCor;
    }

    public void setNomeCor(String nomeCor) {
        this.nomeCor.set(nomeCor);
    }

    public ObservableList<OfGradeItem> getOfGradeItens() {
        return ofGradeItens;
    }
}
