package sysdeliz2.models.properties;

import javafx.beans.property.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 16/09/2019 08:34
 */
public class OfGradeItem implements Serializable {

    private final IntegerProperty ordem = new SimpleIntegerProperty(this, "ordem");
    private final StringProperty tamanho = new SimpleStringProperty(this, "tamanho");
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>(this, "qtde");
    private final ObjectProperty<BigDecimal> qtdeB = new SimpleObjectProperty<>(this, "qtdeB");
    private final ObjectProperty<BigDecimal> qtdeC = new SimpleObjectProperty<>(this, "qtdeC");

    public OfGradeItem() {
    }

    public OfGradeItem(Integer ordem, String tamanho, BigDecimal qtde, BigDecimal qtdeB, BigDecimal qtdeC) {
        this.ordem.set(ordem);
        this.tamanho.set(tamanho);
        this.qtde.set(qtde);
        this.qtdeB.set(qtdeB);
        this.qtdeC.set(qtdeC);
    }

    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    public BigDecimal getQtdeB() {
        return qtdeB.get();
    }

    public ObjectProperty<BigDecimal> qtdeBProperty() {
        return qtdeB;
    }

    public void setQtdeB(BigDecimal qtdeB) {
        this.qtdeB.set(qtdeB);
    }

    public BigDecimal getQtdeC() {
        return qtdeC.get();
    }

    public ObjectProperty<BigDecimal> qtdeCProperty() {
        return qtdeC;
    }

    public void setQtdeC(BigDecimal qtdeC) {
        this.qtdeC.set(qtdeC);
    }
}
