/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.beans.property.*;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.utils.GUIUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cristiano.diego
 */
public class Roteiros {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty solicitante = new SimpleStringProperty();
    private final StringProperty dt_solicitacao = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dt_roteiro = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty mercadoria = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty codfor = new SimpleStringProperty();
    private final StringProperty endereco = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty uf = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty fornecedor = new SimpleStringProperty();

    public Roteiros() {
    }

    public Roteiros(Integer codigo, String solicitante, String dt_solicitacao, String dt_roteiro,
            String tipo, String mercadoria, String observacao, String codfor, String endereco,
            String bairro, String cidade, String uf, String referencia, String telefone, String status) {
        try {
            this.codigo.set(codigo);
            this.solicitante.set(solicitante);
            this.dt_solicitacao.set(dt_solicitacao);
            this.dt_roteiro.set(LocalDate.parse(dt_roteiro, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            this.tipo.set(tipo);
            this.mercadoria.set(mercadoria);
            this.observacao.set(observacao);
            this.codfor.set(codfor);
            this.endereco.set(endereco);
            this.bairro.set(bairro);
            this.cidade.set(cidade);
            this.uf.set(uf);
            this.referencia.set(referencia);
            this.telefone.set(telefone);
            this.status.set(status);
            this.fornecedor.set(DAOFactory.getFornecedorDAO().getByCodigo(codfor).getNome());
        } catch (SQLException ex) {
            Logger.getLogger(Roteiros.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final int getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(int value) {
        codigo.set(value);
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public final String getStatus() {
        return status.get();
    }

    public final void setStatus(String value) {
        status.set(value);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public final String getSolicitante() {
        return solicitante.get();
    }

    public final void setSolicitante(String value) {
        solicitante.set(value);
    }

    public StringProperty solicitanteProperty() {
        return solicitante;
    }

    public final String getDt_solicitacao() {
        return dt_solicitacao.get();
    }

    public final void setDt_solicitacao(String value) {
        dt_solicitacao.set(value);
    }

    public StringProperty dt_solicitacaoProperty() {
        return dt_solicitacao;
    }

    public final LocalDate getDt_roteiro() {
        return dt_roteiro.get();
    }

    public final void setDt_roteiro(LocalDate value) {
        dt_roteiro.set(value);
    }

    public ObjectProperty<LocalDate> dt_roteiroProperty() {
        return dt_roteiro;
    }

    public final String getTipo() {
        return tipo.get();
    }

    public final void setTipo(String value) {
        tipo.set(value);
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public final String getMercadoria() {
        return mercadoria.get();
    }

    public final void setMercadoria(String value) {
        mercadoria.set(value);
    }

    public StringProperty mercadoriaProperty() {
        return mercadoria;
    }

    public final String getObservacao() {
        return observacao.get();
    }

    public final void setObservacao(String value) {
        observacao.set(value);
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public final String getCodfor() {
        return codfor.get();
    }

    public final void setCodfor(String value) {
        codfor.set(value);
    }

    public StringProperty codforProperty() {
        return codfor;
    }

    public final String getEndereco() {
        return endereco.get();
    }

    public final void setEndereco(String value) {
        endereco.set(value);
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public final String getBairro() {
        return bairro.get();
    }

    public final void setBairro(String value) {
        bairro.set(value);
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public final String getCidade() {
        return cidade.get();
    }

    public final void setCidade(String value) {
        cidade.set(value);
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public final String getUf() {
        return uf.get();
    }

    public final void setUf(String value) {
        uf.set(value);
    }

    public StringProperty ufProperty() {
        return uf;
    }

    public final String getReferencia() {
        return referencia.get();
    }

    public final void setReferencia(String value) {
        referencia.set(value);
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public final String getFornecedor() {
        return fornecedor.get();
    }

    public final void setFornecedor(String value) {
        fornecedor.set(value);
    }

    public StringProperty fornecedorProperty() {
        return fornecedor;
    }

    @Override
    public String toString() {
        return "Roteiros{" + "selected=" + selected + ", codigo=" + codigo + ", status=" + status + ", solicitante=" + solicitante + ", dt_solicitacao=" + dt_solicitacao + ", dt_roteiro=" + dt_roteiro + ", tipo=" + tipo + ", mercadoria=" + mercadoria + ", observacao=" + observacao + ", codfor=" + codfor + ", endereco=" + endereco + ", bairro=" + bairro + ", cidade=" + cidade + ", uf=" + uf + ", referencia=" + referencia + ", telefone=" + telefone + '}';
    }

}
