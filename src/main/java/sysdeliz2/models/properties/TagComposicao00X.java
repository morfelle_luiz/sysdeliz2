/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.properties;

import javafx.collections.ObservableList;

/**
 *
 * @author cristiano.diego
 */
public class TagComposicao00X {

    private String codigo;
    private String numero;
    private String cor;
    private String tam;
    private ObservableList<Composicao00X> composicao;

    public TagComposicao00X() {
    }

    public TagComposicao00X(String codigo, String numero, String cor, String tam, ObservableList<Composicao00X> composicao) {
        this.codigo = codigo;
        this.numero = numero;
        this.cor = cor;
        this.tam = tam;
        this.composicao = composicao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTam() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam = tam;
    }

    public ObservableList<Composicao00X> getComposicao() {
        return composicao;
    }

    public void setComposicao(ObservableList<Composicao00X> composicao) {
        this.composicao = composicao;
    }

}
