package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Defeito;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdDadosOfPendenteSKU;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Table(name = "SD_BARRA_CAIXA_PA_001")
@Entity
public class SdBarraCaixaPA extends BasicModel implements Serializable {

    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<SdItemCaixaPA> itemcaixa = new SimpleObjectProperty<>();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final BooleanProperty perdida = new SimpleBooleanProperty(false);
    private final BooleanProperty incompleta = new SimpleBooleanProperty(false);
    private final BooleanProperty segunda = new SimpleBooleanProperty(false);
    private final ObjectProperty<Defeito> defeito = new SimpleObjectProperty<>();
    private final BooleanProperty removida = new SimpleBooleanProperty(false);
    private final ObjectProperty<LocalDate> dataRemocao = new SimpleObjectProperty<>();

    public SdBarraCaixaPA(VSdDadosProdutoBarra pBarra, String barra, SdItemCaixaPA itemcaixa) {
        this.barra.set(barra);
        this.itemcaixa.set(itemcaixa);
        this.produto.set(pBarra.getCodigo());
        this.cor.set(pBarra.getCor());
        this.tam.set(pBarra.getTam());
    }

    public SdBarraCaixaPA(PaIten paIten, String barra, SdItemCaixaPA itemcaixa) {
        this.barra.set(barra);
        this.itemcaixa.set(itemcaixa);
        this.produto.set(paIten.getId().getCodigo());
        this.cor.set(paIten.getId().getCor());
        this.tam.set(paIten.getId().getTam());
    }

    public SdBarraCaixaPA(VSdDadosOfPendenteSKU of) {
        this.produto.set(of.getId().getCodigo());
        this.cor.set(of.getId().getCor());
        this.tam.set(of.getId().getTam());
    }

    public SdBarraCaixaPA() {
    }

    public SdBarraCaixaPA(String codigo, String cor, String tam) {
        this.produto.set(codigo);
        this.cor.set(cor);
        this.tam.set(tam);
    }

    @Id
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        setCodigoFilter(barra);
        this.barra.set(barra);
    }

    @OneToOne
    @JoinColumn(name = "ITEMCAIXA")
    public SdItemCaixaPA getItemcaixa() {
        return itemcaixa.get();
    }

    public ObjectProperty<SdItemCaixaPA> itemcaixaProperty() {
        return itemcaixa;
    }

    public void setItemcaixa(SdItemCaixaPA itemcaixa) {
        this.itemcaixa.set(itemcaixa);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "SEGUNDA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSegunda() {
        return segunda.get();
    }

    public BooleanProperty segundaProperty() {
        return segunda;
    }

    public void setSegunda(boolean segunda) {
        this.segunda.set(segunda);
    }

    @OneToOne
    @JoinColumn(name = "DEFEITO")
    public Defeito getDefeito() {
        return defeito.get();
    }

    public ObjectProperty<Defeito> defeitoProperty() {
        return defeito;
    }

    public void setDefeito(Defeito defeito) {
        this.defeito.set(defeito);
    }

    @Column(name = "REMOVIDA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isRemovida() {
        return removida.get();
    }

    public BooleanProperty removidaProperty() {
        return removida;
    }

    public void setRemovida(boolean removida) {
        this.removida.set(removida);
    }

    @Column(name = "DATA_REMOCAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataRemocao() {
        return dataRemocao.get();
    }

    public ObjectProperty<LocalDate> dataRemocaoProperty() {
        return dataRemocao;
    }

    public void setDataRemocao(LocalDate dataRemocao) {
        this.dataRemocao.set(dataRemocao);
    }


    @Transient
    public boolean isPerdida() {
        return perdida.get();
    }

    public BooleanProperty perdidaProperty() {
        return perdida;
    }

    public void setPerdida(boolean perdida) {
        this.perdida.set(perdida);
    }

    @Transient
    public boolean isIncompleta() {
        return incompleta.get();
    }

    public BooleanProperty incompletaProperty() {
        return incompleta;
    }

    public void setIncompleta(boolean incompleta) {
        this.incompleta.set(incompleta);
    }
}