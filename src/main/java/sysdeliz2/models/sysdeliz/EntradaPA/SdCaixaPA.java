package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Table(name = "SD_CAIXA_PA_001")
@Entity
public class SdCaixaPA extends BasicModel implements Serializable {
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final BooleanProperty fechada = new SimpleBooleanProperty(true);
    private final ObjectProperty<SdItemLotePA> itemlote = new SimpleObjectProperty<>();
    private final StringProperty produto = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty volume = new SimpleIntegerProperty(0);
    private final BooleanProperty segunda = new SimpleBooleanProperty(false);
    private final BooleanProperty incompleta = new SimpleBooleanProperty(false);
    private final BooleanProperty perdida = new SimpleBooleanProperty(false);
    private final BooleanProperty recebida = new SimpleBooleanProperty(false);
    private final BooleanProperty inspecao = new SimpleBooleanProperty(false);


    private List<SdItemCaixaPA> itensCaixa = new ArrayList<>();
    private List<SdBarraCaixaPA> barrasInspecao = new ArrayList<>();

    public SdCaixaPA() {
    }

    public SdCaixaPA(SdItemLotePA ordemEscolhida) {
        this.itemlote.set(ordemEscolhida);
        this.produto.set(ordemEscolhida.getProduto());
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_CAIXA_PA")
    @SequenceGenerator(sequenceName = "SEQ_SD_CAIXA_PA", name = "SEQ_SD_CAIXA_PA", initialValue = 1, allocationSize = 1)
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        setCodigoFilter(String.valueOf(id));
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "ITEM_LOTE")
    public SdItemLotePA getItemlote() {
        return itemlote.get();
    }

    public ObjectProperty<SdItemLotePA> itemloteProperty() {
        return itemlote;
    }

    public void setItemlote(SdItemLotePA itemlote) {
        this.itemlote.set(itemlote);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "VOLUME")
    public Integer getVolume() {
        return volume.get();
    }

    public IntegerProperty volumeProperty() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume.set(volume);
    }

    @Column(name = "FECHADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFechada() {
        return fechada.get();
    }

    public BooleanProperty fechadaProperty() {
        return fechada;
    }

    public void setFechada(boolean fechada) {
        this.fechada.set(fechada);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CAIXA")
    public List<SdItemCaixaPA> getItensCaixa() {
        return itensCaixa;
    }

    public void setItensCaixa(List<SdItemCaixaPA> itensCaixa) {
        this.itensCaixa = itensCaixa;
    }

    @Column(name = "SEGUNDA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSegunda() {
        return segunda.get();
    }

    public BooleanProperty segundaProperty() {
        return segunda;
    }

    public void setSegunda(boolean segunda) {
        this.segunda.set(segunda);
    }

    @Column(name = "INCOMPLETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isIncompleta() {
        return incompleta.get();
    }

    public BooleanProperty incompletaProperty() {
        return incompleta;
    }

    public void setIncompleta(boolean incompleta) {
        this.incompleta.set(incompleta);
    }

    @Column(name = "PERDIDA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPerdida() {
        return perdida.get();
    }

    public BooleanProperty perdidaProperty() {
        return perdida;
    }

    public void setPerdida(boolean perdida) {
        this.perdida.set(perdida);
    }

    @Column(name = "RECEBIDA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isRecebida() {
        return recebida.get();
    }

    public BooleanProperty recebidaProperty() {
        return recebida;
    }

    public void setRecebida(boolean recebida) {
        this.recebida.set(recebida);
    }

    @Column(name = "INSPECAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isInspecao() {
        return inspecao.get();
    }

    public BooleanProperty inspecaoProperty() {
        return inspecao;
    }

    public void setInspecao(boolean inspecao) {
        this.inspecao.set(inspecao);
    }


    @Transient
    public List<SdBarraCaixaPA> getBarrasInspecao() {
        return barrasInspecao;
    }

    public void setBarrasInspecao(List<SdBarraCaixaPA> barrasInspecao) {
        this.barrasInspecao = barrasInspecao;
    }
}