package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdDadosProdutoBarra;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table(name = "SD_ITEM_CAIXA_PA_001")
@Entity
public class SdItemCaixaPA extends BasicModel implements Serializable  {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<SdCaixaPA> caixa = new SimpleObjectProperty<>();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final StringProperty local = new SimpleStringProperty();
    private List<SdBarraCaixaPA> listBarra = new ArrayList<>();

    public SdItemCaixaPA() {
    }


    public SdItemCaixaPA(VSdDadosProdutoBarra produtoBarra, SdCaixaPA caixaEscolhida) {
        this.caixa.set(caixaEscolhida);
        this.produto.set(produtoBarra.getCodigo());
        this.cor.set(produtoBarra.getCor());
        this.tam.set(produtoBarra.getTam());
    }
    public SdItemCaixaPA(PaIten paIten, SdCaixaPA caixaEscolhida) {
        this.caixa.set(caixaEscolhida);
        this.produto.set(paIten.getId().getCodigo());
        this.cor.set(paIten.getId().getCor());
        this.tam.set(paIten.getId().getTam());
    }

    public SdItemCaixaPA(SdCaixaPA caixaIncompleta, int qtde, String produto, String tam, String cor) {
        this.caixa.set(caixaIncompleta);
        this.produto.set(produto);
        this.cor.set(cor);
        this.tam.set(tam);
        this.qtde.set(qtde);
    }

    public SdItemCaixaPA(String cor, String tam, int qtde) {
        this.cor.set(cor);
        this.tam.set(tam);
        this.qtde.set(qtde);
    }

    public SdItemCaixaPA(String produto, String cor, String tam, int qtde) {
        this.produto.set(produto);
        this.cor.set(cor);
        this.tam.set(tam);
        this.qtde.set(qtde);
    }

    public SdItemCaixaPA(Object produto, Object cor, Object tam, Object qtde) {
        this.cor.set(cor.toString());
        this.tam.set(tam.toString());
        this.qtde.set(Integer.parseInt(qtde.toString()));
        this.produto.set(produto.toString());
    }

    public SdItemCaixaPA(String produto, String cor){
        this.cor.set(cor);
        this.produto.set(produto);
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_ITEM_CAIXA_PA")
    @SequenceGenerator(sequenceName = "SEQ_SD_ITEM_CAIXA_PA", name = "SEQ_SD_ITEM_CAIXA_PA", initialValue = 1, allocationSize = 1)
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        setCodigoFilter(String.valueOf(id));
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "CAIXA")
    public SdCaixaPA getCaixa() {
        return caixa.get();
    }

    public ObjectProperty<SdCaixaPA> caixaProperty() {
        return caixa;
    }

    public void setCaixa(SdCaixaPA caixa) {
        this.caixa.set(caixa);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Transient
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ITEMCAIXA")
    public List<SdBarraCaixaPA> getListBarra() {
        return listBarra;
    }

    public void setListBarra(List<SdBarraCaixaPA> listBarra) {
        this.listBarra = listBarra;
    }
}