package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdSolicitacaoExp;
import sysdeliz2.models.view.VSdDadosOfOsPendente;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Table(name = "SD_ITEM_LOTE_PA_001")
@Entity
public class SdItemLotePA extends BasicModel implements Serializable {
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<SdLotePa> lote = new SimpleObjectProperty<>();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final IntegerProperty qtdecaixas = new SimpleIntegerProperty(0);
    private final IntegerProperty volume = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdeTotal = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdeLida = new SimpleIntegerProperty(0);
    private final BooleanProperty selecionado = new SimpleBooleanProperty();
    private final ObjectProperty<SdStatusItemLotePA> status = new SimpleObjectProperty<>();
    private final StringProperty setor = new SimpleStringProperty();
    private final BooleanProperty ordemServico = new SimpleBooleanProperty();
    private List<SdCaixaPA> listCaixas = new ArrayList<>();

    public SdItemLotePA() {
    }

    public SdItemLotePA(SdLotePa lote, VSdDadosOfOsPendente of) {
        this.lote.set(lote);
        this.numero.set(of.getNumero());
        this.produto.set(of.getCodigo());
        this.qtdeTotal.set(of.getQtde());
        this.status.set(new FluentDao().selectFrom(SdStatusItemLotePA.class).where(it -> it.equal("codigo", 1)).singleResult());
        this.setor.set(of.getSetor().getCodigo());
        this.ordemServico.set(of.isOrdem());
    }

    public SdItemLotePA(SdLotePa lote, SdSolicitacaoExp solicitacao) {
        this.lote.set(lote);
        this.numero.set(String.valueOf(solicitacao.getId()));
        this.status.set(new FluentDao().selectFrom(SdStatusItemLotePA.class).where(it -> it.equal("codigo", 1)).singleResult());
        this.setor.set("118");
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_ITEM_LOTE_PA")
    @SequenceGenerator(sequenceName = "SEQ_SD_ITEM_LOTE_PA", name = "SEQ_SD_ITEM_LOTE_PA", initialValue = 1, allocationSize = 1)
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        setCodigoFilter(String.valueOf(id));
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "LOTE")
    public SdLotePa getLote() {
        return lote.get();
    }

    public ObjectProperty<SdLotePa> loteProperty() {
        return lote;
    }

    public void setLote(SdLotePa lote) {
        this.lote.set(lote);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "QTDE_CAIXAS")
    public Integer getQtdecaixas() {
        return qtdecaixas.get();
    }

    public IntegerProperty qtdecaixasProperty() {
        return qtdecaixas;
    }

    public void setQtdecaixas(Integer qtdecaixas) {
        this.qtdecaixas.set(qtdecaixas);
    }

    @Column(name = "VOLUME")
    public Integer getVolume() {
        return volume.get();
    }

    public IntegerProperty volumeProperty() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume.set(volume);
    }

    @Column(name = "QTDE_TOTAL")
    public int getQtdeTotal() {
        return qtdeTotal.get();
    }

    public IntegerProperty qtdeTotalProperty() {
        return qtdeTotal;
    }

    public void setQtdeTotal(int qtdeTotal) {
        this.qtdeTotal.set(qtdeTotal);
    }

    @Column(name = "QTDE_LIDA")
    public int getQtdeLida() {
        return qtdeLida.get();
    }

    public IntegerProperty qtdeLidaProperty() {
        return qtdeLida;
    }

    public void setQtdeLida(int qtdeLida) {
        this.qtdeLida.set(qtdeLida);
    }

    @Transient
    public boolean isSelecionado() {
        return selecionado.get();
    }

    public BooleanProperty selecionadoProperty() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado.set(selecionado);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ITEM_LOTE")
    public List<SdCaixaPA> getListCaixas() {
        return listCaixas;
    }

    public void setListCaixas(List<SdCaixaPA> listCaixas) {
        this.listCaixas = listCaixas;
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusItemLotePA getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusItemLotePA> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusItemLotePA status) {
        this.status.set(status);
    }

    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Column(name = "ORDEM_SERVICO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isOrdemServico() {
        return ordemServico.get();
    }

    public BooleanProperty ordemServicoProperty() {
        return ordemServico;
    }

    public void setOrdemServico(boolean ordemServico) {
        this.ordemServico.set(ordemServico);
    }
}