package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Table(name = "SD_LOTE_PA_001")
@Entity
@TelaSysDeliz(descricao = "Lote Produto Acabado", icon = "produto_50.png")
public class SdLotePa extends BasicModel implements Serializable{

    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "id")
    private final IntegerProperty id = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Status", width = 100)
    @ColunaFilter(descricao = "Status", coluna = "status.codigo")
    private final ObjectProperty<SdStatusLotePA> status = new SimpleObjectProperty<>();

    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    @ExibeTableView(descricao = "Volumes", width = 80)
    private final IntegerProperty volumes = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>(BigDecimal.ZERO);
    @ExibeTableView(descricao = "CodFor", width = 300)
    @ColunaFilter(descricao = "CodFor", coluna = "codfor.codcli")
    private final ObjectProperty<Entidade> codfor = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<>(LocalDate.now());
    @ExibeTableView(descricao = "Dt Envio", width = 100)
    private final ObjectProperty<LocalDate> dtenvio = new SimpleObjectProperty<>();
    private final StringProperty usuario = new SimpleStringProperty();
    private List<SdItemLotePA> itensLote = new ArrayList<>();

    public SdLotePa() {
    }

    public SdLotePa(String usuario) {
        this.usuario.set(usuario);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_LOTE_PA")
    @SequenceGenerator(sequenceName = "SEQ_SD_LOTE_PA", name = "SEQ_SD_LOTE_PA", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        setCodigoFilter(String.valueOf(id));
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusLotePA getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusLotePA> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusLotePA status) {
        this.status.set(status);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "VOLUMES")
    public Integer getVolumes() {
        return volumes.get();
    }

    public IntegerProperty volumesProperty() {
        return volumes;
    }

    public void setVolumes(Integer volumes) {
        this.volumes.set(volumes);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @OneToOne
    @JoinColumn(name = "CODFOR")
    public Entidade getCodfor() {
        return codfor.get();
    }

    public ObjectProperty<Entidade> codforProperty() {
        return codfor;
    }

    public void setCodfor(Entidade codfor) {
        this.codfor.set(codfor);
    }


    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "DT_EMISSAO")
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "DT_ENVIO")
    public LocalDate getDtenvio() {
        return dtenvio.get();
    }

    public ObjectProperty<LocalDate> dtenvioProperty() {
        return dtenvio;
    }

    public void setDtenvio(LocalDate dtenvio) {
        this.dtenvio.set(dtenvio);
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "LOTE")
    public List<SdItemLotePA> getItensLote() {
        return itensLote;
    }

    public void setItensLote(List<SdItemLotePA> itensLote) {
        this.itensLote = itensLote;
    }
}