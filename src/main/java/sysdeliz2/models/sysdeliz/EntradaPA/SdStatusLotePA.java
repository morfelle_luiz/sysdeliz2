package sysdeliz2.models.sysdeliz.EntradaPA;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Table(name = "SD_STATUS_LOTE_PA_001")
@Entity
public class SdStatusLotePA implements Serializable {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();

    public SdStatusLotePA() {
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Override
    public String toString() {
        return getCodigo() + " - " + getStatus();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdStatusLotePA)) return false;
        SdStatusLotePA that = (SdStatusLotePA) o;
        return Objects.equals(getCodigo(), that.getCodigo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }
}