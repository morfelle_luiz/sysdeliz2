package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "SD_BARRA_ITEM_INVENTARIO_001")
@IdClass(SdBarraItemInventarioId.class)
public class SdBarraItemInventario implements Serializable {

    private final ObjectProperty<SdGradeItemInventario> itemgrade = new SimpleObjectProperty<>();
    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtLeitura = new SimpleObjectProperty<>();


    public SdBarraItemInventario() {

    }

    public SdBarraItemInventario(SdGradeItemInventario itemGrade, String barra) {
        this.itemgrade.set(itemGrade);
        this.barra.set(barra);
        this.dtLeitura.set(LocalDate.now());
    }

    @Id
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "INVENTARIO", referencedColumnName = "INVENTARIO"),
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR"),
            @JoinColumn(name = "TAM", referencedColumnName = "TAM")
    })
    public SdGradeItemInventario getItemgrade() {
        return itemgrade.get();
    }

    public ObjectProperty<SdGradeItemInventario> itemgradeProperty() {
        return itemgrade;
    }

    public void setItemgrade(SdGradeItemInventario itemgrade) {
        this.itemgrade.set(itemgrade);
    }

    @Column(name = "DATA_LEITURA")
    public LocalDate getDtLeitura() {
        return dtLeitura.get();
    }

    public ObjectProperty<LocalDate> dtLeituraProperty() {
        return dtLeitura;
    }

    public void setDtLeitura(LocalDate dtLeitura) {
        this.dtLeitura.set(dtLeitura);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdBarraItemInventario)) return false;
        SdBarraItemInventario that = (SdBarraItemInventario) o;
        return getBarra().equals(that.getBarra()) && getItemgrade().equals(that.getItemgrade());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBarra(), getItemgrade());
    }
}
