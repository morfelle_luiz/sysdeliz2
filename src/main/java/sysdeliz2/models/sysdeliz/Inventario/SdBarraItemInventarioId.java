package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;
import java.util.Objects;

public class SdBarraItemInventarioId implements Serializable {

    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<SdGradeItemInventario> itemgrade = new SimpleObjectProperty<>();


    public SdBarraItemInventarioId() {

    }

    public SdBarraItemInventarioId(SdGradeItemInventario itemGrade, String barra) {
        this.itemgrade.set(itemGrade);
        this.barra.set(barra);
    }


    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }


    public SdGradeItemInventario getItemgrade() {
        return itemgrade.get();
    }

    public ObjectProperty<SdGradeItemInventario> itemgradeProperty() {
        return itemgrade;
    }

    public void setItemgrade(SdGradeItemInventario itemgrade) {
        this.itemgrade.set(itemgrade);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdBarraItemInventarioId)) return false;
        SdBarraItemInventarioId that = (SdBarraItemInventarioId) o;
        return getBarra().equals(that.getBarra()) && getItemgrade().equals(that.getItemgrade());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBarra(), getItemgrade());
    }
}
