package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.PaIten;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_GRADE_ITEM_INVENTARIO_001")
@IdClass(sdGradeItemInventarioId.class)
public class SdGradeItemInventario implements Serializable {

    private final ObjectProperty<SdItemInventario> item = new SimpleObjectProperty<>();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty qtdelida = new SimpleIntegerProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private List<SdBarraItemInventario> listBarra = new ArrayList<>();

    public SdGradeItemInventario() {
    }

    public SdGradeItemInventario(SdItemInventario item, int qtde, String tam) {
        this.item.set(item);
        this.qtde.set(qtde);
        this.tam.set(tam);
    }

    @Id
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "INVENTARIO", referencedColumnName = "INVENTARIO"),
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR")
    })
    public SdItemInventario getItem() {
        return item.get();
    }

    public ObjectProperty<SdItemInventario> itemProperty() {
        return item;
    }

    public void setItem(SdItemInventario item) {
        this.item.set(item);
    }

    @Column(name = "QTDE_LIDA")
    public Integer getQtdelida() {
        return qtdelida.get();
    }

    public IntegerProperty qtdelidaProperty() {
        return qtdelida;
    }

    public void setQtdelida(Integer qtdelida) {
        this.qtdelida.set(qtdelida);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @OneToMany(mappedBy = "itemgrade", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<SdBarraItemInventario> getListBarra() {
        return listBarra;
    }

    public void setListBarra(List<SdBarraItemInventario> listBarra) {
        this.listBarra = listBarra;
    }

    public void atualizaQuantidadeAtual() {
        List<PaIten> itensList = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                .where(it -> it
                        .equal("id.codigo", this.getItem().getCodigo().getCodigo())
                        .equal("id.cor", this.getItem().getCodigo().getCor())
                        .equal("id.tam", this.getTam())
                        .isIn("id.deposito", new String[]{"0005", "0023"})
                ).resultList();
        if (itensList != null) {
            this.qtde.set(itensList.stream().mapToInt(it -> it.getQuantidade().intValue()).sum());
        } else {
            this.qtde.set(0);
        }
    }


}
