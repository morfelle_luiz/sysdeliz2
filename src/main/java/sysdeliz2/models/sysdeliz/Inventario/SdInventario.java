package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

@Table(name = "SD_INVENTARIO_001")
@Entity
public class SdInventario implements Serializable {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtcriacao = new SimpleObjectProperty<>(LocalDate.now());
    private final ObjectProperty<LocalDate> dtleitura = new SimpleObjectProperty<>(LocalDate.now());
    private final ObjectProperty<SdStatusInventario> status = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeprodutos = new SimpleIntegerProperty(0);
    private final IntegerProperty erros = new SimpleIntegerProperty(0);
    private final IntegerProperty acertos = new SimpleIntegerProperty(0);
    private Set<SdItemInventario> itens = new HashSet<>();
    private final BooleanProperty mobile = new SimpleBooleanProperty(false);

    public SdInventario() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_INVENTARIO")
    @SequenceGenerator(name = "SEQ_SD_INVENTARIO", sequenceName = "SEQ_SD_INVENTARIO", allocationSize = 1)
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    @Column(name = "DT_CRIACAO")
    public LocalDate getDtcriacao() {
        return dtcriacao.get();
    }

    public ObjectProperty<LocalDate> dtcriacaoProperty() {
        return dtcriacao;
    }

    public void setDtcriacao(LocalDate dtcriacao) {
        this.dtcriacao.set(dtcriacao);
    }

    @Column(name = "DT_LEITURA")
    public LocalDate getDtleitura() {
        return dtleitura.get();
    }

    public ObjectProperty<LocalDate> dtleituraProperty() {
        return dtleitura;
    }

    public void setDtleitura(LocalDate dtleitura) {
        this.dtleitura.set(dtleitura);
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusInventario getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusInventario> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusInventario status) {
        this.status.set(status);
    }

    @Column(name = "QTDE_PRODUTOS")
    public Integer getQtdeprodutos() {
        return qtdeprodutos.get();
    }

    public IntegerProperty qtdeprodutosProperty() {
        return qtdeprodutos;
    }

    public void setQtdeprodutos(Integer qtdeprodutos) {
        this.qtdeprodutos.set(qtdeprodutos);
    }

    @Column(name = "ERROS")
    public Integer getErros() {
        return erros.get();
    }

    public IntegerProperty errosProperty() {
        return erros;
    }

    public void setErros(Integer erros) {
        this.erros.set(erros);
    }

    @Column(name = "ACERTOS")
    public Integer getAcertos() {
        return acertos.get();
    }

    public IntegerProperty acertosProperty() {
        return acertos;
    }

    public void setAcertos(Integer acertos) {
        this.acertos.set(acertos);
    }

    @OneToMany(mappedBy = "inventario", cascade = CascadeType.MERGE, orphanRemoval = true)
    public Set<SdItemInventario> getItens() {
        return itens;
    }

    public void setItens(Set<SdItemInventario> itens) {
        this.itens = itens;
    }

    @Column(name = "MOBILE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isMobile() {
        return mobile.get();
    }

    public BooleanProperty mobileProperty() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile.set(mobile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdInventario)) return false;
        SdInventario that = (SdInventario) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}