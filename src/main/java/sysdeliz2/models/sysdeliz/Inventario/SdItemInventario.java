package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdProdutoInventario;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "SD_ITEM_INVENTARIO_001")
@IdClass(SdItemInventarioId.class)
public class SdItemInventario implements Serializable {

    private final ObjectProperty<SdInventario> inventario = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdProdutoInventario> codigo = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeesperada = new SimpleIntegerProperty();
    private final IntegerProperty qtdelida = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> horaleitura = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusItemInventario> status = new SimpleObjectProperty<>();
    private Set<SdGradeItemInventario> listGrade = new HashSet<>();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();
    private final IntegerProperty erros = new SimpleIntegerProperty(0);
    private final IntegerProperty acertos = new SimpleIntegerProperty(0);

    public SdItemInventario() {
    }

    public SdItemInventario(SdInventario inventario, VSdProdutoInventario codigo) {
        this.inventario.set(inventario);
        this.codigo.set(codigo);
        this.qtdeesperada.set(listGrade.stream().mapToInt(SdGradeItemInventario::getQtde).sum());
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "INVENTARIO")
    public SdInventario getInventario() {
        return inventario.get();
    }

    public ObjectProperty<SdInventario> inventarioProperty() {
        return inventario;
    }

    public void setInventario(SdInventario inventario) {
        this.inventario.set(inventario);
    }

    @Id
    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR"),
    })
    public VSdProdutoInventario getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<VSdProdutoInventario> codigoProperty() {
        return codigo;
    }

    public void setCodigo(VSdProdutoInventario codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "QTDE_ESPERADA")
    public Integer getQtdeesperada() {
        return qtdeesperada.get();
    }

    public IntegerProperty qtdeesperadaProperty() {
        return qtdeesperada;
    }

    public void setQtdeesperada(Integer qtdeesperada) {
        this.qtdeesperada.set(qtdeesperada);
    }

    @Column(name = "QTDE_LIDA")
    public Integer getQtdelida() {
        return qtdelida.get();
    }

    public IntegerProperty qtdelidaProperty() {
        return qtdelida;
    }

    public void setQtdelida(Integer qtdelida) {
        this.qtdelida.set(qtdelida);
    }

    @Column(name = "HORA_LEITURA")
    public LocalDateTime getHoraleitura() {
        return horaleitura.get();
    }

    public ObjectProperty<LocalDateTime> horaleituraProperty() {
        return horaleitura;
    }

    public void setHoraleitura(LocalDateTime horaleitura) {
        this.horaleitura.set(horaleitura);
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusItemInventario getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusItemInventario> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusItemInventario status) {
        this.status.set(status);
    }

    @OneToMany(mappedBy = "item", orphanRemoval = true, cascade = CascadeType.ALL)
    public Set<SdGradeItemInventario> getListGrade() {
        return listGrade;
    }

    public void setListGrade(Set<SdGradeItemInventario> listGrade) {
        this.listGrade = listGrade;
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @Transient
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "ERROS")
    public int getErros() {
        return erros.get();
    }

    public IntegerProperty errosProperty() {
        return erros;
    }

    public void setErros(int erros) {
        this.erros.set(erros);
    }

    @Column(name = "ACERTOS")
    public int getAcertos() {
        return acertos.get();
    }

    public IntegerProperty acertosProperty() {
        return acertos;
    }

    public void setAcertos(int acertos) {
        this.acertos.set(acertos);
    }

    public void generateListGrade() {
        this.getListGrade().clear();
        List<PaIten> itensList = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                .where(it -> it
                        .equal("id.codigo", this.getCodigo().getCodigo())
                        .equal("id.cor", this.getCodigo().getCor())
                        .isIn("id.deposito", new String[]{"0005", "0023"})
                ).resultList();

        itensList.stream().collect(Collectors.groupingBy(it -> it.getId().getTam())).forEach((tam, list) -> this.getListGrade().add(new SdGradeItemInventario(this, list.stream().mapToInt(eb -> eb.getQuantidade().intValue()).sum(), tam)));
    }

}
