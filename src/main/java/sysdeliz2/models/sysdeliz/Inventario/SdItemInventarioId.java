package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdProdutoInventario;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SdItemInventarioId implements Serializable {

    private final ObjectProperty<SdInventario> inventario = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdProdutoInventario> codigo = new SimpleObjectProperty<>();

    public SdItemInventarioId() {
    }

    public SdItemInventarioId(SdInventario inventario, VSdProdutoInventario codigo) {
        this.inventario.set(inventario);
        this.codigo.set(codigo);
    }

    public SdInventario getInventario() {
        return inventario.get();
    }

    public ObjectProperty<SdInventario> inventarioProperty() {
        return inventario;
    }

    public void setInventario(SdInventario inventario) {
        this.inventario.set(inventario);
    }

    public VSdProdutoInventario getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<VSdProdutoInventario> codigoProperty() {
        return codigo;
    }

    public void setCodigo(VSdProdutoInventario codigo) {
        this.codigo.set(codigo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdItemInventarioId)) return false;
        SdItemInventarioId that = (SdItemInventarioId) o;
        return getInventario().equals(that.getInventario()) && getCodigo().equals(that.getCodigo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInventario(), getCodigo());
    }
}
