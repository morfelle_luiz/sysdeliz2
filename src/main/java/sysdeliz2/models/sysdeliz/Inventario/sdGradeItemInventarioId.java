package sysdeliz2.models.sysdeliz.Inventario;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.PaIten;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class sdGradeItemInventarioId implements Serializable {

    private final ObjectProperty<SdItemInventario> item = new SimpleObjectProperty<>();
    private final StringProperty tam = new SimpleStringProperty();

    public sdGradeItemInventarioId() {
    }

    public sdGradeItemInventarioId(SdItemInventario item, String tam) {
        this.item.set(item);
        this.tam.set(tam);
    }

    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    public SdItemInventario getItem() {
        return item.get();
    }

    public ObjectProperty<SdItemInventario> itemProperty() {
        return item;
    }

    public void setItem(SdItemInventario item) {
        this.item.set(item);
    }
}
