package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_ACESSO_TELAS_001")
public class SdAcessoTela extends BasicModel implements Serializable {

    private final ObjectProperty<SdAcessoTelaPK> id = new SimpleObjectProperty<>();
    private final BooleanProperty favorita = new SimpleBooleanProperty(false);
    private final StringProperty codTela = new SimpleStringProperty();
    public SdAcessoTela() {
    }

    public SdAcessoTela(SdTela codTela, String codUsu) {
        this.id.set(new SdAcessoTelaPK(codTela,codUsu));
        this.codTela.set(codTela.getCodTela());
    }

    @EmbeddedId
    public SdAcessoTelaPK getId() {
        return id.get();
    }

    public ObjectProperty<SdAcessoTelaPK> idProperty() {
        return id;
    }

    public void setId(SdAcessoTelaPK id) {
        this.id.set(id);
    }

    @Column(name = "FAVORITA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean getFavorita() {
        return favorita.get();
    }

    public BooleanProperty favoritaProperty() {
        return favorita;
    }

    public void setFavorita(boolean favorita) {
        this.favorita.set(favorita);
    }

    @Nullable
    @Column(name = "COD_TELA")
    public String getCodTela() {
        return codTela.get();
    }

    public StringProperty codTelaProperty() {
        return codTela;
    }

    public void setCodTela(String codTela) {
        this.codTela.set(codTela);
    }
}
