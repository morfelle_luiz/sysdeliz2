package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdAcessoTelaPK implements Serializable {

    private final ObjectProperty<SdTela> idTela = new SimpleObjectProperty<>();
    private final StringProperty nomeUsu = new SimpleStringProperty();


    public SdAcessoTelaPK() {
    }

    public SdAcessoTelaPK(SdTela idTela, String codUsu) {
        this.idTela.set(idTela);
        this.nomeUsu.set(codUsu);

    }

    @OneToOne
    @JoinColumn(name = "ID_TELA")
    public SdTela getIdTela() {
        return idTela.get();
    }

    public ObjectProperty<SdTela> idTelaProperty() {
        return idTela;
    }

    public void setIdTela(SdTela idTela) {
        this.idTela.set(idTela);
    }

    @Column(name = "NOME_USU")
    public String getNomeUsu() {
        return nomeUsu.get();
    }

    public StringProperty nomeUsuProperty() {
        return nomeUsu;
    }

    public void setNomeUsu(String nomeUsu) {
        this.nomeUsu.set(nomeUsu);
    }

}
