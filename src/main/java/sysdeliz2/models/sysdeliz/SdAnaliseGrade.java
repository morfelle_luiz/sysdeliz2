package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 02/09/2019 16:53
 */
@Entity
@Table(name = "SD_ANALISE_GRADE_001")
public class SdAnaliseGrade {

    private final IntegerProperty codigo = new SimpleIntegerProperty(this, "codigo");
    private final StringProperty colecao = new SimpleStringProperty(this, "colecao");
    private final StringProperty linha = new SimpleStringProperty(this, "linha");
    private final StringProperty modelagem = new SimpleStringProperty(this, "modelagem");
    private final StringProperty classe = new SimpleStringProperty(this, "classe");
    private final StringProperty marca = new SimpleStringProperty(this, "marca");
    private final StringProperty tamanho = new SimpleStringProperty(this, "tamanho");
    private final ObjectProperty<BigDecimal> totalTamanho = new SimpleObjectProperty<>(this, "totalTamanho");
    private final ObjectProperty<BigDecimal> percTamanho = new SimpleObjectProperty<>(this, "percTamanho");
    private final StringProperty faixa = new SimpleStringProperty(this, "faixa");
    private final ObjectProperty<BigDecimal> totalFaixa = new SimpleObjectProperty<>(this, "totalFaixa");

    public SdAnaliseGrade() {
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    @Column(name = "MODELAGEM")
    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }

    @Column(name = "CLASSE")
    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @Column(name = "TOTAL_TAMANHO")
    public BigDecimal getTotalTamanho() {
        return totalTamanho.get();
    }

    public ObjectProperty<BigDecimal> totalTamanhoProperty() {
        return totalTamanho;
    }

    public void setTotalTamanho(BigDecimal totalTamanho) {
        this.totalTamanho.set(totalTamanho);
    }

    @Column(name = "PERC_TAMANHO")
    public BigDecimal getPercTamanho() {
        return percTamanho.get();
    }

    public ObjectProperty<BigDecimal> percTamanhoProperty() {
        return percTamanho;
    }

    public void setPercTamanho(BigDecimal percTamanho) {
        this.percTamanho.set(percTamanho);
    }

    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    @Column(name = "TOTAL_FAIXA")
    public BigDecimal getTotalFaixa() {
        return totalFaixa.get();
    }

    public ObjectProperty<BigDecimal> totalFaixaProperty() {
        return totalFaixa;
    }

    public void setTotalFaixa(BigDecimal totalFaixa) {
        this.totalFaixa.set(totalFaixa);
    }

}
