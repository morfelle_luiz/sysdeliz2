package sysdeliz2.models.sysdeliz;

import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;


public class SdBidRep extends BasicModel {

    private final ObjectProperty<Represen> representante = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<Cidade> cidade = new SimpleObjectProperty<>();
    private final LongProperty qtdCidades = new SimpleLongProperty();
    private final LongProperty cidadesColecaoAtual = new SimpleLongProperty();
    private final LongProperty cidadesColecaoAnterior = new SimpleLongProperty();
    private final LongProperty cidadesColecaoPassada = new SimpleLongProperty();
    private final LongProperty cidadesColecaoRef = new SimpleLongProperty();
    private final LongProperty cidadesSemClientes = new SimpleLongProperty();
    private final LongProperty cidadesConflito = new SimpleLongProperty();

    public SdBidRep() {
    }

    public Represen getRepresentante() {
        return representante.get();
    }

    public ObjectProperty<Represen> representanteProperty() {
        return representante;
    }

    public void setRepresentante(Represen representante) {
        this.representante.set(representante);
    }

    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    public Cidade getCidade() {
        return cidade.get();
    }

    public ObjectProperty<Cidade> cidadeProperty() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade.set(cidade);
    }

    public long getQtdCidades() {
        return qtdCidades.get();
    }

    public LongProperty qtdCidadesProperty() {
        return qtdCidades;
    }

    public void setQtdCidades(long qtdCidades) {
        this.qtdCidades.set(qtdCidades);
    }

    public long getCidadesColecaoAtual() {
        return cidadesColecaoAtual.get();
    }

    public LongProperty cidadesColecaoAtualProperty() {
        return cidadesColecaoAtual;
    }

    public void setCidadesColecaoAtual(long cidadesColecaoAtual) {
        this.cidadesColecaoAtual.set(cidadesColecaoAtual);
    }

    public long getCidadesColecaoAnterior() {
        return cidadesColecaoAnterior.get();
    }

    public LongProperty cidadesColecaoAnteriorProperty() {
        return cidadesColecaoAnterior;
    }

    public void setCidadesColecaoAnterior(long cidadesColecaoAnterior) {
        this.cidadesColecaoAnterior.set(cidadesColecaoAnterior);
    }

    public long getCidadesColecaoPassada() {
        return cidadesColecaoPassada.get();
    }

    public LongProperty cidadesColecaoPassadaProperty() {
        return cidadesColecaoPassada;
    }

    public void setCidadesColecaoPassada(long cidadesColecaoPassada) {
        this.cidadesColecaoPassada.set(cidadesColecaoPassada);
    }

    public long getCidadesColecaoRef() {
        return cidadesColecaoRef.get();
    }

    public LongProperty cidadesColecaoRefProperty() {
        return cidadesColecaoRef;
    }

    public void setCidadesColecaoRef(long cidadesColecaoRef) {
        this.cidadesColecaoRef.set(cidadesColecaoRef);
    }

    public long getCidadesSemClientes() {
        return cidadesSemClientes.get();
    }

    public LongProperty cidadesSemClientesProperty() {
        return cidadesSemClientes;
    }

    public void setCidadesSemClientes(long cidadesSemClientes) {
        this.cidadesSemClientes.set(cidadesSemClientes);
    }

    public long getCidadesConflito() {
        return cidadesConflito.get();
    }

    public LongProperty cidadesConflitoProperty() {
        return cidadesConflito;
    }

    public void setCidadesConflito(long cidadesConflito) {
        this.cidadesConflito.set(cidadesConflito);
    }
}
