package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.RemessaAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_CAIXA_REMESSA_001")
public class SdCaixaRemessa {
    
    private final IntegerProperty numero = new SimpleIntegerProperty();
    private final ObjectProperty<SdRemessaCliente> remessa = new SimpleObjectProperty<>();
    private final BooleanProperty fechada = new SimpleBooleanProperty(false);
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private List<SdItensCaixaRemessa> itens = new ArrayList<>();
    private final StringProperty nota = new SimpleStringProperty();
    private final IntegerProperty volume = new SimpleIntegerProperty();
    
    public SdCaixaRemessa() {
    }
    
    public SdCaixaRemessa(Integer numero, SdRemessaCliente remessa) {
        this.numero.set(numero);
        this.remessa.set(remessa);
    }
    
    @Id
    @Column(name = "NUMERO")
    public Integer getNumero() {
        return numero.get();
    }
    
    public IntegerProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(Integer numero) {
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REMESSA")
    @Convert(converter = RemessaAttributeConverter.class)
    public SdRemessaCliente getRemessa() {
        return remessa.get();
    }
    
    public ObjectProperty<SdRemessaCliente> remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(SdRemessaCliente remessa) {
        this.remessa.set(remessa);
    }
    
    @Column(name = "FECHADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getFechada() {
        return fechada.get();
    }
    
    public BooleanProperty fechadaProperty() {
        return fechada;
    }
    
    public void setFechada(Boolean fechada) {
        this.fechada.set(fechada);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }
    
    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }
    
    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "NUMERO")
    public List<SdItensCaixaRemessa> getItens() {
        return itens;
    }
    
    public void setItens(List<SdItensCaixaRemessa> itens) {
        this.itens = itens;
    }
    
    @Column(name = "NOTA")
    public String getNota() {
        return nota.get();
    }
    
    public StringProperty notaProperty() {
        return nota;
    }
    
    public void setNota(String nota) {
        this.nota.set(nota);
    }
    
    @Column(name = "VOLUME")
    public Integer getVolume() {
        return volume.get();
    }
    
    public IntegerProperty volumeProperty() {
        return volume;
    }
    
    public void setVolume(Integer volume) {
        this.volume.set(volume);
    }
    
    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}