/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdFamiliasCelula001;
import sysdeliz2.models.SdTurnoCelula001;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_CELULA_001")
@TelaSysDeliz(descricao = "Célula", icon = "celula (4).png")
public class SdCelula extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 200)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty("SELECIONE A CÉLULA");
    @Transient
    private final IntegerProperty codigoLider = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Líder", width = 200)
    @ColunaFilter(descricao = "Líder", coluna = "lider.codigo", filterClass = "sysdeliz2.models.sysdeliz.SdColaborador001")
    private final ObjectProperty<SdColaborador> lider = new SimpleObjectProperty<>(new SdColaborador());
    @Transient
    private final IntegerProperty codigoSetorOp = new SimpleIntegerProperty();
    @Transient
    @ColunaFilter(descricao = "Setor Operação", coluna = "setorOp.codigo", filterClass = "sysdeliz2.models.sysdeliz.SdSetorOp001")
    private final ObjectProperty<SdSetorOp001> setorOp = new SimpleObjectProperty<>(new SdSetorOp001());
    @Transient
    private final IntegerProperty qtdeOperadores = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    @Transient
    private final ListProperty<SdFamiliasCelula001> familias = new SimpleListProperty<>();
    @Transient
    private final ListProperty<SdColabCelula001> colaboradores = new SimpleListProperty<>();
    @Transient
    private final ListProperty<SdTurnoCelula001> turnos = new SimpleListProperty<>();
    @Transient
    private final ListProperty<Map<String, Object>> intervalos = new SimpleListProperty<>();

    public SdCelula() {
    }

    public SdCelula(SdCelula toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.descricao.set(toCopy.descricao.get());
        this.codigoLider.set(toCopy.codigoLider.get());
        this.lider.set(toCopy.lider.get());
        this.codigoSetorOp.set(toCopy.codigoSetorOp.get());
        this.setorOp.set(toCopy.setorOp.get());
        this.qtdeOperadores.set(toCopy.qtdeOperadores.get());
        this.ativo.set(toCopy.ativo.get());
        setSelected(toCopy.isSelected());
        this.colaboradores.set(toCopy.colaboradores.get());
        this.familias.set(toCopy.familias.get());
        this.turnos.set(toCopy.turnos.get());
        this.intervalos.set(toCopy.intervalos.get());
    }

    public SdCelula(Integer codigo, String descricao,
                    Integer codigoLider, SdColaborador lider,
                    Integer codigoSetorOp, SdSetorOp001 setorOp,
                    Integer qtdeOperadores, String ativo) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.codigoLider.set(codigoLider);
        this.lider.set(lider);
        this.codigoSetorOp.set(codigoSetorOp);
        this.setorOp.set(setorOp);
        this.qtdeOperadores.set(qtdeOperadores);
        this.ativo.set(ativo.equals("S"));
    }

    public SdCelula(Integer codigo, String descricao,
                    Integer codigoLider, SdColaborador lider,
                    Integer codigoSetorOp, SdSetorOp001 setorOp,
                    Integer qtdeOperadores, String ativo,
                    ObservableList<SdFamiliasCelula001> familias,
                    ObservableList<SdColabCelula001> colaboradores,
                    ObservableList<SdTurnoCelula001> turnos,
                    ObservableList<Map<String, Object>> intervalos) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.codigoLider.set(codigoLider);
        this.lider.set(lider);
        this.codigoSetorOp.set(codigoSetorOp);
        this.setorOp.set(setorOp);
        this.qtdeOperadores.set(qtdeOperadores);
        this.ativo.set(ativo.equals("S"));
        this.familias.set(familias);
        this.colaboradores.set(colaboradores);
        this.turnos.set(turnos);
        this.intervalos.set(intervalos);

    }

    
    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    public void setCodigo(int value) {
        setCodigoFilter(""+value);
        codigo.set(value);
    }
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    public void setDescricao(String value) {
        setDescricaoFilter(value);
        descricao.set(value);
    }
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Transient
    public final int getCodigoLider() {
        return codigoLider.get();
    }
    public final void setCodigoLider(int value) {
        codigoLider.set(value);
    }
    public IntegerProperty codigoLiderProperty() {
        return codigoLider;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LIDER")
    public SdColaborador getLider() {
        return lider.get();
    }
    public void setLider(SdColaborador value) {
        setCodigoLider(value != null ? value.getCodigo() : 0);
        lider.set(value);
    }
    public ObjectProperty<SdColaborador> liderProperty() {
        return lider;
    }
    
    @Transient
    public final int getCodigoSetorOp() {
        return codigoSetorOp.get();
    }
    public final void setCodigoSetorOp(int value) {
        codigoSetorOp.set(value);
    }
    public IntegerProperty codigoSetorOpProperty() {
        return codigoSetorOp;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SETOR_OP")
    public SdSetorOp001 getSetorOp() {
        return setorOp.get();
    }
    public void setSetorOp(SdSetorOp001 value) {
        setorOp.set(value);
    }
    public ObjectProperty<SdSetorOp001> setorOpProperty() {
        return setorOp;
    }
    
    @Column(name = "QTDE_OPERADORES")
    public int getQtdeOperadores() {
        return qtdeOperadores.get();
    }
    public void setQtdeOperadores(int value) {
        qtdeOperadores.set(value);
    }
    public IntegerProperty qtdeOperadoresProperty() {
        return qtdeOperadores;
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    public void setAtivo(boolean value) {
        ativo.set(value);
    }
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    @Transient
    public final ObservableList<SdFamiliasCelula001> getFamilias() {
        return familias.get();
    }
    public final void setFamilias(ObservableList<SdFamiliasCelula001> value) {
        familias.set(value);
    }
    public ListProperty<SdFamiliasCelula001> familiasProperty() {
        return familias;
    }
    
    @Transient
    public final ObservableList<SdColabCelula001> getColaboradores() {
        return colaboradores.get();
    }
    public final void setColaboradores(ObservableList<SdColabCelula001> value) {
        colaboradores.set(value);
    }
    public ListProperty<SdColabCelula001> colaboradoresProperty() {
        return colaboradores;
    }
    
    @Transient
    public final ObservableList<SdTurnoCelula001> getTurnos() {
        return turnos.get();
    }
    public final void setTurnos(ObservableList<SdTurnoCelula001> value) {
        turnos.set(value);
    }
    public ListProperty<SdTurnoCelula001> turnosProperty() {
        return turnos;
    }
    
    @Transient
    public final ObservableList<Map<String, Object>> getIntervalos() {
        return intervalos.get();
    }
    public final void setIntervalos(ObservableList<Map<String, Object>> value) {
        intervalos.set(value);
    }
    public ListProperty<Map<String, Object>> intervalosProperty() {
        return intervalos;
    }

    @Transient
    public void clear() {
        this.codigo.set(0);
        this.descricao.set("SELECIONE A CÉLULA");
        this.codigoLider.set(0);
        this.lider.set(null);
        this.codigoSetorOp.set(0);
        this.setorOp.set(null);
        this.qtdeOperadores.set(0);
        this.ativo.set(true);
        setSelected(false);
        this.colaboradores.clear();
        this.familias.clear();
        this.turnos.clear();
        this.intervalos.clear();
    }
    
    @Transient
    public void copy(SdCelula toCopy) {
        if(toCopy != null) {
            this.codigo.set(toCopy.codigo.get());
            this.descricao.set(toCopy.descricao.get());
            this.codigoLider.set(toCopy.codigoLider.get());
            this.lider.set(toCopy.lider.get());
            this.codigoSetorOp.set(toCopy.codigoSetorOp.get());
            this.setorOp.set(toCopy.setorOp.get());
            this.qtdeOperadores.set(toCopy.qtdeOperadores.get());
            this.ativo.set(toCopy.ativo.get());
            setSelected(toCopy.isSelected());
            this.colaboradores.set(toCopy.colaboradores.get());
            this.familias.set(toCopy.familias.get());
            this.turnos.set(toCopy.turnos.get());
            this.intervalos.set(toCopy.intervalos.get());
        }
    }
    
    @Transient
    public void copySetorOp(SdSetorOp001 setorOp) {
        this.codigoSetorOp.set(setorOp.getCodigo());
        this.setorOp.get().setCodigo(setorOp.getCodigo());
        this.setorOp.get().setDescricao(setorOp.getDescricao());
        this.setorOp.get().setAtivo(setorOp.isAtivo());
        this.setorOp.get().setSelected(setorOp.isSelected());
    }
    
    @Transient
    public void copyLider(SdColaborador lider) {
        this.codigoLider.set(lider.getCodigo());
        this.lider.get().setAtivo(lider.isAtivo());
        this.lider.get().setCodigo(lider.getCodigo());
        this.lider.get().setCodigoFuncao(lider.getCodigoFuncao());
        this.lider.get().setCodigoRH(lider.getCodigoRH());
        this.lider.get().setCodigoTurno(lider.getCodigoTurno());
        this.lider.get().setNome(lider.getNome());
        this.lider.get().setSelected(lider.isSelected());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdCelula other = (SdCelula) obj;
        if (!Objects.equals(this.codigo.get(), other.codigo.get())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

}
