package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author lima.joao
 * @since 17/07/2019 08:35
 */
@Entity
@Table(name="SD_CIDADES_001")
@SuppressWarnings("unused")
@TelaSysDeliz(descricao = "Cidade", icon = "cidade (4).png")
public class SdCidade extends BasicModel {

    //<editor-fold defaultstate="collapsed" desc="Declaração das properties">
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codcli")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty(this, "nome");
    @Transient
    private final StringProperty uf = new SimpleStringProperty(this, "uf");
    @Transient
    @ExibeTableView(descricao = "UF", width = 30)
    @ColunaFilter(descricao = "UF", coluna = "siglaUf")
    private final StringProperty siglaUf = new SimpleStringProperty(this, "siglaUf");
    @Transient
    @ExibeTableView(descricao = "País", width = 40)
    private final StringProperty pais = new SimpleStringProperty(this, "pais");
    @Transient
    private final ObjectProperty<BigDecimal> ipc = new SimpleObjectProperty<>(this, "ipc");
    @Transient
    private final IntegerProperty populacao = new SimpleIntegerProperty(this, "populacao");
    @Transient
    private final IntegerProperty populacao0_4 = new SimpleIntegerProperty(this, "populacao0_4");
    @Transient
    private final IntegerProperty populacao5_9 = new SimpleIntegerProperty(this, "populacao5_9");
    @Transient
    private final IntegerProperty populacao10_14 = new SimpleIntegerProperty(this, "populacao10_14");
    @Transient
    private final IntegerProperty populacao15_19 = new SimpleIntegerProperty(this, "populacao15_19");
    @Transient
    private final IntegerProperty populacao20_29 = new SimpleIntegerProperty(this, "populacao20_29");
    @Transient
    private final IntegerProperty populacao30_49 = new SimpleIntegerProperty(this, "populacao30_49");
    @Transient
    private final IntegerProperty populacao50 = new SimpleIntegerProperty(this, "populacao50");
    @Transient
    private final ObjectProperty<BigDecimal> homens = new SimpleObjectProperty<>(this, "homens");
    @Transient
    private final ObjectProperty<BigDecimal> mulheres = new SimpleObjectProperty<>(this, "mulheres");
    @Transient
    private final ObjectProperty<BigDecimal> urbana = new SimpleObjectProperty<>(this, "urbana");
    @Transient
    private final ObjectProperty<BigDecimal> rural = new SimpleObjectProperty<>(this, "rural");
    @Transient
    private final ObjectProperty<BigDecimal> alfabetizacao = new SimpleObjectProperty<>(this, "alfabetizacao");
    @Transient
    private final ObjectProperty<BigDecimal> perCapita = new SimpleObjectProperty<>(this, "perCapita");
    @Transient
    private final ObjectProperty<BigDecimal> vestuario = new SimpleObjectProperty<>(this, "vestuario");
    @Transient
    private final IntegerProperty classeA1 = new SimpleIntegerProperty(this, "classeA1");
    @Transient
    private final IntegerProperty classeA2 = new SimpleIntegerProperty(this, "classeA2");
    @Transient
    private final IntegerProperty classeB1 = new SimpleIntegerProperty(this, "classeB1");
    @Transient
    private final IntegerProperty classeB2 = new SimpleIntegerProperty(this, "classeB2");
    @Transient
    private final IntegerProperty classeC1 = new SimpleIntegerProperty(this, "classeC1");
    @Transient
    private final IntegerProperty classeC2 = new SimpleIntegerProperty(this, "classeC2");
    @Transient
    private final IntegerProperty classeD = new SimpleIntegerProperty(this, "classeD");
    @Transient
    private final IntegerProperty classeE = new SimpleIntegerProperty(this, "classeE");
    @Transient
    private final ObjectProperty<BigDecimal> icc = new SimpleObjectProperty<>(this, "icc");
    @Transient
    private final IntegerProperty shoppings = new SimpleIntegerProperty(this, "shoppings");
    @Transient
    private final StringProperty micro = new SimpleStringProperty(this, "micro");
    @Transient
    private final StringProperty irradiacao = new SimpleStringProperty(this, "irradiacao");
    @Transient
    private final StringProperty informacoesMercado = new SimpleStringProperty(this, "informacoesMercado");
    @Transient
    private final ObjectProperty<BigDecimal> densidadeDemografica = new SimpleObjectProperty<>(this, "densidadeDemografica");
    @Transient
    private final ObjectProperty<BigDecimal> taxaCrescimento = new SimpleObjectProperty<>(this, "taxaCrescimento");
    @Transient
    private final IntegerProperty veiculos = new SimpleIntegerProperty(this, "veiculos");
    @Transient
    private final IntegerProperty agenciaBancaria = new SimpleIntegerProperty(this, "agenciaBancaria");
    @Transient
    private final IntegerProperty industrias = new SimpleIntegerProperty(this, "industrias");
    @Transient
    private final IntegerProperty estabelecimentoComercial = new SimpleIntegerProperty(this, "estabelecimentoComercial");
    @Transient
    private final IntegerProperty rankingNacional = new SimpleIntegerProperty(this, "rankingNacional");
    @Transient
    private final IntegerProperty rankingEstadual = new SimpleIntegerProperty(this, "rankingEstadual");
    @Transient
    private final IntegerProperty cdwk = new SimpleIntegerProperty(this, "cdwk");
    @Transient
    private final IntegerProperty consumoVestuarioA1 = new SimpleIntegerProperty(this, "consumoVestuarioA1");
    @Transient
    private final IntegerProperty consumoVestuarioA2 = new SimpleIntegerProperty(this, "consumoVestuarioA2");
    @Transient
    private final IntegerProperty consumoVestuarioB1 = new SimpleIntegerProperty(this, "consumoVestuarioB1");
    @Transient
    private final IntegerProperty consumoVestuarioB2 = new SimpleIntegerProperty(this, "consumoVestuarioB2");
    @Transient
    private final IntegerProperty consumoVestuarioC1 = new SimpleIntegerProperty(this, "consumoVestuarioC1");
    @Transient
    private final IntegerProperty consumoVestuarioC2 = new SimpleIntegerProperty(this, "consumoVestuarioC2");
    @Transient
    private final IntegerProperty consumoVestuarioD = new SimpleIntegerProperty(this, "consumoVestuarioD");
    @Transient
    private final IntegerProperty consumoVestuarioE = new SimpleIntegerProperty(this, "consumoVestuarioE");
    @Transient
    private final IntegerProperty distanciaCapital = new SimpleIntegerProperty(this, "distanciaCapital");
    @Transient
    private final ObjectProperty<BigInteger> vendidoVestuario = new SimpleObjectProperty<>(this, "vendidoVestuario");
    @Transient
    private final IntegerProperty cotaClientes = new SimpleIntegerProperty(this, "cotaClientes");
//    @Transient
//    private final StringProperty dtAlteracao = new SimpleStringProperty(this, "dtAlteracao");
    @Transient
    private final StringProperty zonaFranca = new SimpleStringProperty(this, "zonaFranca");
    @Transient
    private final ObjectProperty<BigDecimal> latitude = new SimpleObjectProperty<>(this, "latitude");
    @Transient
    private final ObjectProperty<BigDecimal> longitude = new SimpleObjectProperty<>(this, "longitude");

    // selected
    @Transient
    private final BooleanProperty selected = new SimpleBooleanProperty(this, "selected");

    public final BooleanProperty selectedProperty() {
       return selected;
    }

    @Transient
    public final boolean isSelected() {
       return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters com annotação JPA">
    @Id
    @Column(name="CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    @Column(name="UF")
    public String getUf() {
        return uf.get();
    }

    @Column(name="SIGLA_UF")
    public String getSiglaUf() {
        return siglaUf.get();
    }

    @Column(name="PAIS")
    public String getPais() {
        return pais.get();
    }

    @Column(name="IPC")
    public BigDecimal getIpc() {
        return ipc.get();
    }

    @Column(name="POPULACAO")
    public Integer getPopulacao() {
        return populacao.get();
    }

    @Column(name="POPULACAO_0_4")
    public Integer getPopulacao0_4() {
        return populacao0_4.get();
    }

    @Column(name="POPULACAO_5_9")
    public Integer getPopulacao5_9() {
        return populacao5_9.get();
    }

    @Column(name="POPULACAO_10_14")
    public Integer getPopulacao10_14() {
        return populacao10_14.get();
    }

    @Column(name="POPULACAO_15_19")
    public Integer getPopulacao15_19() {
        return populacao15_19.get();
    }

    @Column(name="POPULACAO_20_29")
    public Integer getPopulacao20_29() {
        return populacao20_29.get();
    }

    @Column(name="POPULACAO_30_49")
    public Integer getPopulacao30_49() {
        return populacao30_49.get();
    }

    @Column(name="POPULACAO_50")
    public Integer getPopulacao50() {
        return populacao50.get();
    }

    @Column(name="HOMENS")
    public BigDecimal getHomens() {
        return homens.get();
    }

    @Column(name="MULHERES")
    public BigDecimal getMulheres() {
        return mulheres.get();
    }

    @Column(name="URBANA")
    public BigDecimal getUrbana() {
        return urbana.get();
    }

    @Column(name="RURAL")
    public BigDecimal getRural() {
        return rural.get();
    }

    @Column(name="ALFABETIZACAO")
    public BigDecimal getAlfabetizacao() {
        return alfabetizacao.get();
    }

    @Column(name="PER_CAPITA")
    public BigDecimal getPerCapita() {
        return perCapita.get();
    }

    @Column(name="VESTUARIO")
    public BigDecimal getVestuario() {
        return vestuario.get();
    }

    @Column(name="CLASSE_A1")
    public Integer getClasseA1() {
        return classeA1.get();
    }

    @Column(name="CLASSE_A2")
    public Integer getClasseA2() {
        return classeA2.get();
    }

    @Column(name="CLASSE_B1")
    public Integer getClasseB1() {
        return classeB1.get();
    }

    @Column(name="CLASSE_B2")
    public Integer getClasseB2() {
        return classeB2.get();
    }

    @Column(name="CLASSE_C1")
    public Integer getClasseC1() {
        return classeC1.get();
    }

    @Column(name="CLASSE_C2")
    public Integer getClasseC2() {
        return classeC2.get();
    }

    @Column(name="CLASSE_D")
    public Integer getClasseD() {
        return classeD.get();
    }

    @Column(name="CLASSE_E")
    public Integer getClasseE() {
        return classeE.get();
    }

    @Column(name="ICC")
    public BigDecimal getIcc() {
        return icc.get();
    }

    @Column(name="SHOPPINGS")
    public Integer getShoppings() {
        return shoppings.get();
    }

    @Column(name="MICRO")
    public String getMicro() {
        return micro.get();
    }

    @Column(name="IRRADIACAO")
    public String getIrradiacao() {
        return irradiacao.get();
    }

    @Column(name="INFORMACOES_MERCADO")
    public String getInformacoesMercado() {
        return informacoesMercado.get();
    }

    @Column(name="DENSIDADE_DEMOGRAFICA")
    public BigDecimal getDensidadeDemografica() {
        return densidadeDemografica.get();
    }

    @Column(name="TAXA_CRESCIMENTO")
    public BigDecimal getTaxaCrescimento() {
        return taxaCrescimento.get();
    }

    @Column(name="VEICULOS")
    public Integer getVeiculos() {
        return veiculos.get();
    }

    @Column(name="AGENCIA_BANCARIA")
    public Integer getAgenciaBancaria() {
        return agenciaBancaria.get();
    }

    @Column(name="INDUSTRIAS")
    public Integer getIndustrias() {
        return industrias.get();
    }

    @Column(name="ESTABELECIMENTO_COMERCIAL")
    public Integer getEstabelecimentoComercial() {
        return estabelecimentoComercial.get();
    }

    @Column(name="RANKING_NACIONAL")
    public Integer getRankingNacional() {
        return rankingNacional.get();
    }

    @Column(name="RANKING_ESTADUAL")
    public Integer getRankingEstadual() {
        return rankingEstadual.get();
    }

    @Column(name="CDWK")
    public Integer getCdwk() {
        return cdwk.get();
    }

    @Column(name="CONSUMO_VEST_A1")
    public Integer getConsumoVestuarioA1() {
        return consumoVestuarioA1.get();
    }

    @Column(name="CONSUMO_VEST_A2")
    public Integer getConsumoVestuarioA2() {
        return consumoVestuarioA2.get();
    }

    @Column(name="CONSUMO_VEST_B1")
    public Integer getConsumoVestuarioB1() {
        return consumoVestuarioB1.get();
    }

    @Column(name="CONSUMO_VEST_B2")
    public Integer getConsumoVestuarioB2() {
        return consumoVestuarioB2.get();
    }

    @Column(name="CONSUMO_VEST_C1")
    public Integer getConsumoVestuarioC1() {
        return consumoVestuarioC1.get();
    }

    @Column(name="CONSUMO_VEST_C2")
    public Integer getConsumoVestuarioC2() {
        return consumoVestuarioC2.get();
    }

    @Column(name="CONSUMO_VEST_D")
    public Integer getConsumoVestuarioD() {
        return consumoVestuarioD.get();
    }

    @Column(name="CONSUMO_VEST_E")
    public Integer getConsumoVestuarioE() {
        return consumoVestuarioE.get();
    }

    @Column(name="DISTANCIA_CAPITAL")
    public Integer getDistanciaCapital() {
        return distanciaCapital.get();
    }

    @Column(name="VENDIDO_VESTUARIO")
    public BigInteger getVendidoVestuario() {
        return vendidoVestuario.get();
    }

    @Column(name="COTA_CLIENTES")
    public Integer getCotaClientes() {
        return cotaClientes.get();
    }

    @Column(name="ZONA_FRANCA")
    public String getZonaFranca() {
        return zonaFranca.get();
    }

    @Column(name="LATITUDE")
    public BigDecimal getLatitude() {
        return latitude.get();
    }

    @Column(name="LONGITUDE")
    public BigDecimal getLongitude() {
        return longitude.get();
    }

    @Column(name="NOME")
    public String getNome() {
        return nome.get();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Acesso as properties">
    public final StringProperty codigoProperty() {
       return codigo;
    }

    public final StringProperty ufProperty() {
        return uf;
    }

    public final StringProperty siglaUfProperty() {
        return siglaUf;
    }

    public final StringProperty paisProperty() {
        return pais;
    }

    public final ObjectProperty<BigDecimal> ipcProperty() {
        return ipc;
    }

    public final IntegerProperty populacaoProperty() {
        return populacao;
    }

    public final IntegerProperty populacao0_4Property() {
        return populacao0_4;
    }

    public final IntegerProperty populacao5_9Property() {
        return populacao5_9;
    }

    public final IntegerProperty populacao10_14Property() {
        return populacao10_14;
    }

    public final IntegerProperty populacao15_19Property() {
        return populacao15_19;
    }

    public final IntegerProperty populacao20_29Property() {
        return populacao20_29;
    }

    public final IntegerProperty populacao30_49Property() {
        return populacao30_49;
    }

    public final IntegerProperty populacao50Property() {
        return populacao50;
    }

    public final ObjectProperty<BigDecimal> homensProperty() {
        return homens;
    }

    public final ObjectProperty<BigDecimal> mulheresProperty() {
        return mulheres;
    }

    public final ObjectProperty<BigDecimal> urbanaProperty() {
        return urbana;
    }

    public final ObjectProperty<BigDecimal> ruralProperty() {
        return rural;
    }

    public final ObjectProperty<BigDecimal> alfabetizacaoProperty() {
        return alfabetizacao;
    }

    public final ObjectProperty<BigDecimal> perCapitaProperty() {
        return perCapita;
    }

    public final ObjectProperty<BigDecimal> vestuarioProperty() {
        return vestuario;
    }

    public final IntegerProperty classeA1Property() {
        return classeA1;
    }

    public final IntegerProperty classeA2Property() {
        return classeA2;
    }

    public final IntegerProperty classeB1Property() {
        return classeB1;
    }

    public final IntegerProperty classeB2Property() {
        return classeB2;
    }

    public final IntegerProperty classeC1Property() {
        return classeC1;
    }

    public final IntegerProperty classeC2Property() {
        return classeC2;
    }

    public final IntegerProperty classeDProperty() {
        return classeD;
    }

    public final IntegerProperty classeEProperty() {
        return classeE;
    }

    public final ObjectProperty<BigDecimal> iccProperty() {
        return icc;
    }

    public final IntegerProperty shoppingsProperty() {
        return shoppings;
    }

    public final StringProperty microProperty() {
        return micro;
    }

    public final StringProperty irradiacaoProperty() {
        return irradiacao;
    }

    public final StringProperty informacoesMercadoProperty() {
        return informacoesMercado;
    }

    public final ObjectProperty<BigDecimal> densidadeDemograficaProperty() {
        return densidadeDemografica;
    }

    public final ObjectProperty<BigDecimal> taxaCrescimentoProperty() {
        return taxaCrescimento;
    }

    public final IntegerProperty veiculosProperty() {
        return veiculos;
    }

    public final IntegerProperty agenciaBancariaProperty() {
        return agenciaBancaria;
    }

    public final IntegerProperty industriasProperty() {
        return industrias;
    }

    public final IntegerProperty estabelecimentoComercialProperty() {
        return estabelecimentoComercial;
    }

    public final IntegerProperty rankingNacionalProperty() {
        return rankingNacional;
    }

    public final IntegerProperty rankingEstadualProperty() {
        return rankingEstadual;
    }

    public final IntegerProperty cdwkProperty() {
        return cdwk;
    }

    public final IntegerProperty consumoVestuarioA1Property() {
        return consumoVestuarioA1;
    }

    public final IntegerProperty consumoVestuarioA2Property() {
        return consumoVestuarioA2;
    }

    public final IntegerProperty consumoVestuarioB1Property() {
        return consumoVestuarioB1;
    }

    public final IntegerProperty consumoVestuarioB2Property() {
        return consumoVestuarioB2;
    }

    public final IntegerProperty consumoVestuarioC1Property() {
        return consumoVestuarioC1;
    }

    public final IntegerProperty consumoVestuarioC2Property() {
        return consumoVestuarioC2;
    }

    public final IntegerProperty consumoVestuarioDProperty() {
        return consumoVestuarioD;
    }

    public final IntegerProperty consumoVestuarioEProperty() {
        return consumoVestuarioE;
    }

    public final IntegerProperty distanciaCapitalProperty() {
        return distanciaCapital;
    }

    public final ObjectProperty<BigInteger> vendidoVestuarioProperty() {
        return vendidoVestuario;
    }

    public final IntegerProperty cotaClientesProperty() {
        return cotaClientes;
    }

//    public final StringProperty dtAlteracaoProperty() {
//        return dtAlteracao;
//    }

    public final StringProperty zonaFrancaProperty() {
        return zonaFranca;
    }

    public final ObjectProperty<BigDecimal> latitudeProperty() {
        return latitude;
    }

    public final ObjectProperty<BigDecimal> longitudeProperty() {
        return longitude;
    }

    public final StringProperty nomeProperty() {
        return nome;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Setters">
    public void setCodigo(String value) {
        setCodigoFilter(value);
        codigo.set(value);
    }

    public void setUf(String value) {
        uf.set(value);
    }

    public void setSiglaUf(String value) {
        siglaUf.set(value);
    }

    public void setPais(String value) {
        pais.set(value);
    }

    public void setIpc(BigDecimal value) {
        ipc.set(value);
    }

    public void setPopulacao(Integer value) {
        populacao.set(value);
    }

    public void setPopulacao0_4(Integer value) {
        populacao0_4.set(value);
    }

    public void setPopulacao5_9(Integer value) {
        populacao5_9.set(value);
    }

    public void setPopulacao10_14(Integer value) {
        populacao10_14.set(value);
    }

    public void setPopulacao15_19(Integer value) {
        populacao15_19.set(value);
    }

    public void setPopulacao20_29(Integer value) {
        populacao20_29.set(value);
    }

    public void setPopulacao30_49(Integer value) {
        populacao30_49.set(value);
    }

    public void setPopulacao50(Integer value) {
        populacao50.set(value);
    }

    public void setHomens(BigDecimal value) {
        homens.set(value);
    }

    public void setMulheres(BigDecimal value) {
        mulheres.set(value);
    }

    public void setUrbana(BigDecimal value) {
        urbana.set(value);
    }

    public void setRural(BigDecimal value) {
        rural.set(value);
    }

    public void setAlfabetizacao(BigDecimal value) {
        alfabetizacao.set(value);
    }

    public void setPerCapita(BigDecimal value) {
        perCapita.set(value);
    }

    public void setVestuario(BigDecimal value) {
        vestuario.set(value);
    }

    public void setClasseA1(Integer value) {
        classeA1.set(value);
    }

    public void setClasseA2(Integer value) {
        classeA2.set(value);
    }

    public void setClasseB1(Integer value) {
        classeB1.set(value);
    }

    public void setClasseB2(Integer value) {
        classeB2.set(value);
    }

    public void setClasseC1(Integer value) {
        classeC1.set(value);
    }

    public void setClasseC2(Integer value) {
        classeC2.set(value);
    }

    public void setClasseD(Integer value) {
        classeD.set(value);
    }

    public void setClasseE(Integer value) {
        classeE.set(value);
    }

    public void setIcc(BigDecimal value) {
        icc.set(value);
    }

    public void setShoppings(Integer value) {
        shoppings.set(value);
    }

    public void setMicro(String value) {
        micro.set(value);
    }

    public void setIrradiacao(String value) {
        irradiacao.set(value);
    }

    public void setInformacoesMercado(String value) {
        informacoesMercado.set(value);
    }

    public void setDensidadeDemografica(BigDecimal value) {
        densidadeDemografica.set(value);
    }

    public void setTaxaCrescimento(BigDecimal value) {
        taxaCrescimento.set(value);
    }

    public void setVeiculos(Integer value) {
        veiculos.set(value);
    }

    public void setAgenciaBancaria(Integer value) {
        agenciaBancaria.set(value);
    }

    public void setIndustrias(Integer value) {
        industrias.set(value);
    }

    public void setEstabelecimentoComercial(Integer value) {
        estabelecimentoComercial.set(value);
    }

    public void setRankingNacional(Integer value) {
        rankingNacional.set(value);
    }

    public void setRankingEstadual(Integer value) {
        rankingEstadual.set(value);
    }

    public void setCdwk(Integer value) {
        cdwk.set(value);
    }

    public void setConsumoVestuarioA1(Integer value) {
        consumoVestuarioA1.set(value);
    }

    public void setConsumoVestuarioA2(Integer value) {
        consumoVestuarioA2.set(value);
    }

    public void setConsumoVestuarioB1(Integer value) {
        consumoVestuarioB1.set(value);
    }

    public void setConsumoVestuarioB2(Integer value) {
        consumoVestuarioB2.set(value);
    }

    public void setConsumoVestuarioC1(Integer value) {
        consumoVestuarioC1.set(value);
    }

    public void setConsumoVestuarioC2(Integer value) {
        consumoVestuarioC2.set(value);
    }

    public void setConsumoVestuarioD(Integer value) {
        consumoVestuarioD.set(value);
    }

    public void setConsumoVestuarioE(Integer value) {
        consumoVestuarioE.set(value);
    }

    public void setDistanciaCapital(Integer value) {
        distanciaCapital.set(value);
    }

    public void setVendidoVestuario(BigInteger value) {
        vendidoVestuario.set(value);
    }

    public void setCotaClientes(Integer value) {
        cotaClientes.set(value);
    }

    public void setZonaFranca(String value) {
        zonaFranca.set(value);
    }

    public void setLatitude(BigDecimal value) {
        latitude.set(value);
    }

    public void setLongitude(BigDecimal value) {
        longitude.set(value);
    }

    public void setNome(String value) {
        setDescricaoFilter(value);
        nome.set(value);
    }
    //</editor-fold>


    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + nome.get();
    }
}
