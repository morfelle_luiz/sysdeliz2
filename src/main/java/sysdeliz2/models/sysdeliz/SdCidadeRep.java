package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_CIDADE_REP_001")
public class SdCidadeRep extends BasicModel implements Serializable {

    private final ObjectProperty<SdCidadeRepPK> id = new SimpleObjectProperty();

    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();

    private final StringProperty status = new SimpleStringProperty();

    public SdCidadeRep() {

    }

    @EmbeddedId
    public SdCidadeRepPK getId() {
        return id.get();
    }

    public ObjectProperty<SdCidadeRepPK> idProperty() {
        return id;
    }

    public void setId(SdCidadeRepPK id) {
        this.id.set(id);
    }


    @OneToOne
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Transient
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }


}
