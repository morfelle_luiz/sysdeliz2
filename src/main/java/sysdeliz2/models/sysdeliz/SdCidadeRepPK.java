package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Represen;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdCidadeRepPK implements Serializable {

    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();

    private final ObjectProperty<Represen> codRep = new SimpleObjectProperty<>();

    public SdCidadeRepPK() {
    }

    public SdCidadeRepPK(Cidade codCid, Represen codRep) {
        this.codCid.set(codCid);
        this.codRep.set(codRep);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COD_CID")
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP")
    public Represen getCodRep() {
        return codRep.get();
    }

    public ObjectProperty<Represen> codRepProperty() {
        return codRep;
    }

    public void setCodRep(Represen codRep) {
        this.codRep.set(codRep);
    }

    @Override
    public String toString() {
        return codRep.getValue().getCodRep() + codCid.getValue().getCodCid();
    }
}
