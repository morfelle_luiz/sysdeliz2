package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ClientesEtiqueta;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_CLI_DULLIUS")
public class SdCliDullius extends ClientesEtiqueta {

    private final StringProperty coditem = new SimpleStringProperty();
    private final StringProperty descitem = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty coritem = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valorvista = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> parcelavista = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty numparcelavuita = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valorprazo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> parcelaprazo = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty numparcelaprazo = new SimpleIntegerProperty();
    private final StringProperty txtpagto = new SimpleStringProperty();
    private final StringProperty txttroca = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty barraean = new SimpleStringProperty();

    public SdCliDullius() {
    }

    public SdCliDullius(String codItem, String descricao, String codigo, String corItem, BigDecimal valorVendaAVista, BigDecimal valorParcelaAVista, int qtdeParcelaAVista, BigDecimal valorVendaAPrazo, BigDecimal valorParcelaPrazo, int qtdeParcelaPrazo, String textoFPgto, String textoTroca, BigDecimal qtde, String tam) {
        this.coditem.set(codItem);
        this.descitem.set(descricao);
        this.codigo.set(codigo);
        this.coritem.set(corItem);
        this.valorvista.set(valorVendaAVista);
        this.parcelavista.set(valorParcelaAVista);
        this.numparcelavuita.set(qtdeParcelaAVista);
        this.valorprazo.set(valorVendaAPrazo);
        this.parcelaprazo.set(valorParcelaPrazo);
        this.numparcelaprazo.set(qtdeParcelaPrazo);
        this.txtpagto.set(textoFPgto);
        this.txttroca.set(textoTroca);
        this.qtde.set(qtde);
        this.tam.set(tam);
    }

    @Id
    @Column(name = "CODITEM")
    public String getCoditem() {
        return coditem.get();
    }

    public StringProperty coditemProperty() {
        return coditem;
    }

    public void setCoditem(String coditem) {
        this.coditem.set(coditem);
    }

    @Column(name = "DESCITEM")
    public String getDescitem() {
        return descitem.get();
    }

    public StringProperty descitemProperty() {
        return descitem;
    }

    public void setDescitem(String descitem) {
        this.descitem.set(descitem);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "CORITEM")
    public String getCoritem() {
        return coritem.get();
    }

    public StringProperty coritemProperty() {
        return coritem;
    }

    public void setCoritem(String coritem) {
        this.coritem.set(coritem);
    }

    @Column(name = "VALORVISTA")
    public BigDecimal getValorvista() {
        return valorvista.get();
    }

    public ObjectProperty<BigDecimal> valorvistaProperty() {
        return valorvista;
    }

    public void setValorvista(BigDecimal valorvista) {
        this.valorvista.set(valorvista);
    }

    @Column(name = "PARCELAVISTA")
    public BigDecimal getParcelavista() {
        return parcelavista.get();
    }

    public ObjectProperty<BigDecimal> parcelavistaProperty() {
        return parcelavista;
    }

    public void setParcelavista(BigDecimal parcelavista) {
        this.parcelavista.set(parcelavista);
    }

    @Column(name = "NUMPARCELAVUITA")
    public Integer getNumparcelavuita() {
        return numparcelavuita.get();
    }

    public IntegerProperty numparcelavuitaProperty() {
        return numparcelavuita;
    }

    public void setNumparcelavuita(Integer numparcelavuita) {
        this.numparcelavuita.set(numparcelavuita);
    }

    @Column(name = "VALORPRAZO")
    public BigDecimal getValorprazo() {
        return valorprazo.get();
    }

    public ObjectProperty<BigDecimal> valorprazoProperty() {
        return valorprazo;
    }

    public void setValorprazo(BigDecimal valorprazo) {
        this.valorprazo.set(valorprazo);
    }

    @Column(name = "PARCELAPRAZO")
    public BigDecimal getParcelaprazo() {
        return parcelaprazo.get();
    }

    public ObjectProperty<BigDecimal> parcelaprazoProperty() {
        return parcelaprazo;
    }

    public void setParcelaprazo(BigDecimal parcelaprazo) {
        this.parcelaprazo.set(parcelaprazo);
    }

    @Column(name = "NUMPARCELAPRAZO")
    public Integer getNumparcelaprazo() {
        return numparcelaprazo.get();
    }

    public IntegerProperty numparcelaprazoProperty() {
        return numparcelaprazo;
    }

    public void setNumparcelaprazo(Integer numparcelaprazo) {
        this.numparcelaprazo.set(numparcelaprazo);
    }

    @Column(name = "TXTPAGTO")
    public String getTxtpagto() {
        return txtpagto.get();
    }

    public StringProperty txtpagtoProperty() {
        return txtpagto;
    }

    public void setTxtpagto(String txtpagto) {
        this.txtpagto.set(txtpagto);
    }

    @Column(name = "TXTTROCA")
    public String getTxttroca() {
        return txttroca.get();
    }

    public StringProperty txttrocaProperty() {
        return txttroca;
    }

    public void setTxttroca(String txttroca) {
        this.txttroca.set(txttroca);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "BARRAEAN")
    public String getBarraean() {
        return barraean.get();
    }

    public StringProperty barraeanProperty() {
        return barraean;
    }

    public void setBarraean(String barraean) {
        this.barraean.set(barraean);
    }

}
