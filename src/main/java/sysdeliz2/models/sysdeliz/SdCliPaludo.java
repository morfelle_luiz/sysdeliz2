package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ClientesEtiqueta;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_CLI_PALUDO")
public class SdCliPaludo extends ClientesEtiqueta {

    private final ObjectProperty<SdCliPaludoPK> id = new SimpleObjectProperty<>();
    private final StringProperty barracli = new SimpleStringProperty();
    private final StringProperty descitem = new SimpleStringProperty();
    private final StringProperty np = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valparcela = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valtotal = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty pedidoerp = new SimpleStringProperty();

    public SdCliPaludo() {
    }

    public SdCliPaludo(String coditem, String barraCli, String descItem, String codigo, String corItem, String tamItem, String np,BigDecimal valParcela,BigDecimal valTotal, String lote){
        SdCliPaludoPK idPK = new SdCliPaludoPK();
        idPK.setCodItem(coditem);
        idPK.setCodigo(codigo);
        idPK.setCoritem(corItem);
        idPK.setTamitem(tamItem);


        this.id.set(idPK);
        this.barracli.set(barraCli);
        this.descitem.set(descItem);
        this.np.set(np);
        this.valparcela.set(valParcela);
        this.valtotal.set(valTotal);
        this.lote.set(lote);
    }

    @EmbeddedId
    public SdCliPaludoPK getId() {
        return id.get();
    }

    public ObjectProperty<SdCliPaludoPK> idProperty() {
        return id;
    }

    public void setId(SdCliPaludoPK id) {
        this.id.set(id);
    }

    @Column(name = "BARRACLI")
    public String getBarracli() {
        return barracli.get();
    }

    public StringProperty barracliProperty() {
        return barracli;
    }

    public void setBarracli(String barracli) {
        this.barracli.set(barracli);
    }

    @Column(name = "DESCITEM")
    public String getDescitem() {
        return descitem.get();
    }

    public StringProperty descitemProperty() {
        return descitem;
    }

    public void setDescitem(String descitem) {
        this.descitem.set(descitem);
    }

    @Column(name = "NP")
    public String getNp() {
        return np.get();
    }

    public StringProperty npProperty() {
        return np;
    }

    public void setNp(String np) {
        this.np.set(np);
    }

    @Column(name = "VALPARCELA")
    public BigDecimal getValparcela() {
        return valparcela.get();
    }

    public ObjectProperty<BigDecimal> valparcelaProperty() {
        return valparcela;
    }

    public void setValparcela(BigDecimal valparcela) {
        this.valparcela.set(valparcela);
    }

    @Column(name = "VALTOTAL")
    public BigDecimal getValtotal() {
        return valtotal.get();
    }

    public ObjectProperty<BigDecimal> valtotalProperty() {
        return valtotal;
    }

    public void setValtotal(BigDecimal valtotal) {
        this.valtotal.set(valtotal);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "PEDIDO_ERP")
    public String getPedidoerp() {
        return pedidoerp.get();
    }

    public StringProperty pedidoerpProperty() {
        return pedidoerp;
    }

    public void setPedidoerp(String pedidoerp) {
        this.pedidoerp.set(pedidoerp);
    }

}
