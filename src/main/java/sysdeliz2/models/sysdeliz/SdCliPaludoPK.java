package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SdCliPaludoPK extends BasicModel {

    private final StringProperty codItem = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty coritem = new SimpleStringProperty();
    private final StringProperty tamitem = new SimpleStringProperty();


    public SdCliPaludoPK() {
    }


    @Column(name = "CODITEM")
    public String getCodItem() {
        return codItem.get();
    }

    public StringProperty codItemProperty() {
        return codItem;
    }

    public void setCodItem(String codItem) {
        this.codItem.set(codItem);
    }


    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "CORITEM")
    public String getCoritem() {
        return coritem.get();
    }

    public StringProperty coritemProperty() {
        return coritem;
    }

    public void setCoritem(String coritem) {
        this.coritem.set(coritem);
    }

    @Column(name = "TAMITEM")
    public String getTamitem() {
        return tamitem.get();
    }

    public StringProperty tamitemProperty() {
        return tamitem;
    }

    public void setTamitem(String tamitem) {
        this.tamitem.set(tamitem);
    }

}
