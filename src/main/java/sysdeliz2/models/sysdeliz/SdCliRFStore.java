package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ClientesEtiqueta;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_CLI_RFSTORE")
public class SdCliRFStore extends ClientesEtiqueta {

    private final StringProperty codproduto = new SimpleStringProperty();
    private final StringProperty codinterno = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valorvenda = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty descricaoproduto = new SimpleStringProperty();

    public SdCliRFStore() {
    }

    public SdCliRFStore(String referencia, String codProduto, String colecao, String descProd, String descCor, String codCor, String barra, String valor, String tam) {
        this.codproduto.set(codProduto);
        this.codinterno.set(referencia);
        this.referencia.set(referencia);
        this.descricao.set(descCor);
        this.cor.set(codCor);
        this.tam.set(tam);
        this.colecao.set(colecao);
        this.valorvenda.set(new BigDecimal(valor.replace(",",".")));
        this.descricaoproduto.set(descProd);
        this.barra.set(barra);
    }

    @Id
    @Column(name = "CODPRODUTO")
    public String getCodproduto() {
        return codproduto.get();
    }

    public StringProperty codprodutoProperty() {
        return codproduto;
    }

    public void setCodproduto(String codproduto) {
        this.codproduto.set(codproduto);
    }

    @Column(name = "CODINTERNO")
    public String getCodinterno() {
        return codinterno.get();
    }

    public StringProperty codinternoProperty() {
        return codinterno;
    }

    public void setCodinterno(String codinterno) {
        this.codinterno.set(codinterno);
    }

    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "VALORVENDA")
    public BigDecimal getValorvenda() {
        return valorvenda.get();
    }

    public ObjectProperty<BigDecimal> valorvendaProperty() {
        return valorvenda;
    }

    public void setValorvenda(BigDecimal valorvenda) {
        this.valorvenda.set(valorvenda);
    }

    @Column(name = "DESCRICAOPRODUTO")
    public String getDescricaoproduto() {
        return descricaoproduto.get();
    }

    public StringProperty descricaoprodutoProperty() {
        return descricaoproduto;
    }

    public void setDescricaoproduto(String descricaoproduto) {
        this.descricaoproduto.set(descricaoproduto);
    }

    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }
}
