/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_COLAB_CELULA_001")
public class SdColabCelula001 implements Serializable {

    private final IntegerProperty codigoCelula = new SimpleIntegerProperty();
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<SdCelula>();
    private final IntegerProperty codigoColaborador = new SimpleIntegerProperty();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<SdColaborador>();

    public SdColabCelula001() {
    }

    public SdColabCelula001(Integer codigoCelula, Integer codigoColaborador, SdColaborador colaborador) {
        this.codigoCelula.set(codigoCelula);
        this.codigoColaborador.set(codigoColaborador);
        this.colaborador.set(colaborador);
    }

    public SdColabCelula001(Integer codigoCelula, Integer codigoColaborador, SdCelula celula) {
        this.codigoCelula.set(codigoCelula);
        this.codigoColaborador.set(codigoColaborador);
        this.celula.set(celula);
    }

    @Transient
    public final int getCodigoCelula() {
        return codigoCelula.get();
    }

    public final void setCodigoCelula(int value) {
        codigoCelula.set(value);
    }

    public IntegerProperty codigoCelulaProperty() {
        return codigoCelula;
    }
    
    @Transient
    public final int getCodigoColaborador() {
        return codigoColaborador.get();
    }

    public final void setCodigoColaborador(int value) {
        codigoColaborador.set(value);
    }

    public IntegerProperty codigoColaboradorProperty() {
        return codigoColaborador;
    }

    @Id
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }

    public void setColaborador(SdColaborador value) {
        colaborador.set(value);
    }

    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CELULA")
    public SdCelula getCelula() {
        return celula.get();
    }

    public void setCelula(SdCelula value) {
        celula.set(value);
    }

    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
