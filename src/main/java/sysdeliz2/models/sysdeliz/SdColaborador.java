/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdFuncao001;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

/**
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_COLABORADOR_001")
@TelaSysDeliz(descricao = "Colaborador", icon = "colaborador (4).png")
public class SdColaborador extends BasicModel {

    //Properties Table
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty();
    @Transient
    private final IntegerProperty codigoFuncao = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty codigoTurno = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty codigoRH = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    @Transient
    private final StringProperty usuario = new SimpleStringProperty();

    //Properties Controllers
    @Transient
    private final ListProperty<SdPolivalencia001> operacoes = new SimpleListProperty<>();
    @Transient
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<SdTurno> turno = new SimpleObjectProperty<>(new SdTurno());
    @Transient
    private final ObjectProperty<SdFuncao001> funcao = new SimpleObjectProperty<>(new SdFuncao001());
    @Transient
    private final ListProperty<SdProgramacaoPacote001> programacaoColaborador = new SimpleListProperty<>();
    @Transient
    private final StringProperty statusOcupacao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty codfor = new SimpleStringProperty();
    @Transient
    private final BooleanProperty impressoraPadrao = new SimpleBooleanProperty(false);
    @Transient
    private final BooleanProperty perdidasAtivas = new SimpleBooleanProperty(false);
    @Transient
    private final BooleanProperty incompletasAtivas = new SimpleBooleanProperty(false);
    @Transient
    private final IntegerProperty pisoColeta = new SimpleIntegerProperty(0);
    @Transient
    private final BooleanProperty segundoColetor = new SimpleBooleanProperty(false);
    @Transient
    private final BooleanProperty entradaDireta = new SimpleBooleanProperty(false);
    public SdColaborador() {
    }

    public SdColaborador(Integer codigo, String nome, Integer codigoFuncao,
                         Integer codigoTurno, Integer codigoRH, String ativo) {
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.codigoFuncao.set(codigoFuncao);
        this.codigoTurno.set(codigoTurno);
        this.codigoRH.set(codigoRH);
        this.ativo.set(ativo.equals("S"));
    }

    public SdColaborador(Integer codigo, String nome, Integer codigoFuncao,
                         Integer codigoTurno, Integer codigoRH, String ativo, String usuario, String codfor,
                         String impressoaPadrao, String perdidasAtivas, String incompletasAtivas, String entradaDireta, Integer pisoColeta, Boolean segundoColetor,
                         SdFuncao001 funcao, SdTurno turno) {
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.codigoFuncao.set(codigoFuncao);
        this.codigoTurno.set(codigoTurno);
        this.codigoRH.set(codigoRH);
        this.ativo.set(ativo.equals("S"));
        this.usuario.set(usuario);
        this.codfor.set(codfor);
        this.impressoraPadrao.set(impressoaPadrao != null && impressoaPadrao.equals("S"));
        this.perdidasAtivas.set(perdidasAtivas != null && perdidasAtivas.equals("S"));
        this.incompletasAtivas.set(incompletasAtivas != null && incompletasAtivas.equals("S"));
        this.entradaDireta.set(entradaDireta != null && entradaDireta.equals("S"));
        this.funcao.set(funcao);
        this.turno.set(turno);
        this.pisoColeta.set(pisoColeta);
        this.setSegundoColetor(segundoColetor);
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public void setCodigo(int value) {
        setCodigoFilter("" + value);
        codigo.set(value);
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    public void setNome(String value) {
        setDescricaoFilter(value);
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    @Column(name = "FUNCAO")
    public int getCodigoFuncao() {
        return codigoFuncao.get();
    }

    public void setCodigoFuncao(int value) {
        codigoFuncao.set(value);
    }

    public IntegerProperty codigoFuncaoProperty() {
        return codigoFuncao;
    }

    @Transient
    public final SdFuncao001 getFuncao() {
        return funcao.get();
    }

    public final void setFuncao(SdFuncao001 value) {
        funcao.set(value);
    }

    public ObjectProperty<SdFuncao001> funcaoProperty() {
        return funcao;
    }

    @Transient
    public final int getCodigoTurno() {
        return codigoTurno.get();
    }

    public final void setCodigoTurno(int value) {
        codigoTurno.set(value);
    }

    public IntegerProperty codigoTurnoProperty() {
        return codigoTurno;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TURNO")
    public SdTurno getTurno() {
        return turno.get();
    }

    public void setTurno(SdTurno value) {
        turno.set(value);
        this.codigoTurno.set(value.getCodigo());
    }

    public ObjectProperty<SdTurno> turnoProperty() {
        return turno;
    }

    @Column(name = "CODIGO_RH")
    public int getCodigoRH() {
        return codigoRH.get();
    }

    public void setCodigoRH(int value) {
        codigoRH.set(value);
    }

    public IntegerProperty codigoRHProperty() {
        return codigoRH;
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public void setAtivo(boolean value) {
        ativo.set(value);
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }

    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }

    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "CODFOR")
    public String getCodfor() {
        return codfor.get();
    }

    public StringProperty codforProperty() {
        return codfor;
    }

    public void setCodfor(String codfor) {
        this.codfor.set(codfor);
    }

    @Column(name = "IMPRESSORA_PADRAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImpressoraPadrao() {
        return impressoraPadrao.get();
    }

    public BooleanProperty impressoraPadraoProperty() {
        return impressoraPadrao;
    }

    public void setImpressoraPadrao(boolean impressoraPadrao) {
        this.impressoraPadrao.set(impressoraPadrao);
    }

    @Column(name = "PERDIDAS_ATIVAS")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPerdidasAtivas() {
        return perdidasAtivas.get();
    }

    public BooleanProperty perdidasAtivasProperty() {
        return perdidasAtivas;
    }

    public void setPerdidasAtivas(boolean perdidasAtivas) {
        this.perdidasAtivas.set(perdidasAtivas);
    }

    @Column(name = "INCOMPLETAS_ATIVAS")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isIncompletasAtivas() {
        return incompletasAtivas.get();
    }

    public BooleanProperty incompletasAtivasProperty() {
        return incompletasAtivas;
    }

    public void setIncompletasAtivas(boolean incompletasAtivas) {
        this.incompletasAtivas.set(incompletasAtivas);
    }

    @Column(name = "PISO_COLETA")
    public int getPisoColeta() {
        return pisoColeta.get();
    }

    public IntegerProperty pisoColetaProperty() {
        return pisoColeta;
    }

    public void setPisoColeta(int pisoColeta) {
        this.pisoColeta.set(pisoColeta);
    }

    @Column(name = "SEGUNDO_COLETOR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSegundoColetor() {
        return segundoColetor.get();
    }

    public BooleanProperty segundoColetorProperty() {
        return segundoColetor;
    }

    public void setSegundoColetor(boolean segundoColetor) {
        this.segundoColetor.set(segundoColetor);
    }

    @Column(name = "ENTRADA_DIRETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEntradaDireta() {
        return entradaDireta.get();
    }

    public BooleanProperty entradaDiretaProperty() {
        return entradaDireta;
    }

    public void setEntradaDireta(boolean entradaDireta) {
        this.entradaDireta.set(entradaDireta);
    }


    @Transient
    public final ObservableList<SdPolivalencia001> getOperacoes() {
        return operacoes.get();
    }

    public final void setOperacoes(ObservableList<SdPolivalencia001> value) {
        operacoes.set(value);
    }

    public ListProperty<SdPolivalencia001> operacoesProperty() {
        return operacoes;
    }

    @Transient
    public final SdCelula getCelula() {
        return celula.get();
    }

    public final void setCelula(SdCelula value) {
        celula.set(value);
    }

    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }

    @Transient
    public final String getStatusOcupacao() {
        return statusOcupacao.get();
    }

    public final void setStatusOcupacao(String value) {
        statusOcupacao.set(value);
    }

    public StringProperty statusOcupacaoProperty() {
        return statusOcupacao;
    }

    public void copy(SdColaborador toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.nome.set(toCopy.nome.get());
        this.codigoFuncao.set(toCopy.codigoFuncao.get());
        this.codigoTurno.set(toCopy.codigoTurno.get());
        this.codigoRH.set(toCopy.codigoRH.get());
        this.ativo.set(toCopy.ativo.get());
        this.funcao.set(toCopy.funcao.get());
        this.turno.set(toCopy.turno.get());
        this.celula.set(toCopy.celula.get());
    }

    public void copyTurno(SdTurno period) {
        getTurno().setCodigo(period.getCodigo());
        getTurno().setDescricao(period.getDescricao());
        getTurno().setInicioTurno(period.getInicioTurno());
        getTurno().setFimTurno(period.getFimTurno());
        getTurno().setAtivo(period.isAtivo());
        getTurno().setSelected(period.isSelected());

        setCodigoTurno(period.getCodigo());
    }

    public void copyFuncao(SdFuncao001 function) {
        getFuncao().setCodigo(function.getCodigo());
        getFuncao().setDescricao(function.getDescricao());
        getFuncao().setSelected(function.isSelected());

        setCodigoFuncao(function.getCodigo());
    }

    @Transient
    public final ObservableList<SdProgramacaoPacote001> getProgramacaoColaborador() {
        return programacaoColaborador.get();
    }

    public final void setProgramacaoColaborador(ObservableList<SdProgramacaoPacote001> value) {
        programacaoColaborador.set(value);
    }

    public ListProperty<SdProgramacaoPacote001> programacaoColaboradorProperty() {
        return programacaoColaborador;
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + nome.get();
    }

    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

}
