package sysdeliz2.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@SuppressWarnings("unused")
public class SdColaboradorSenior001 {

    // numCadProperty
    private final StringProperty numCad = new SimpleStringProperty(this, "numCad");
    // numEmp
    private final StringProperty numEmp = new SimpleStringProperty(this, "numEmp");
    // nomeEmp
    private final StringProperty nomeEmp = new SimpleStringProperty(this, "nomeEmp");
    // tipoCol
    private final StringProperty tipoCol = new SimpleStringProperty(this, "tipoCol");
    // numLoc
    private final StringProperty numLoc = new SimpleStringProperty(this, "numLoc");
    // nomeSetor
    private final StringProperty nomeSetor = new SimpleStringProperty(this, "nomeSetor");
    // nomeFuncionario
    private final StringProperty nomeFuncionario = new SimpleStringProperty(this, "nomeFuncionario");

    public SdColaboradorSenior001(){
        this("", "", "", "", "", "", "");
    }

    public SdColaboradorSenior001(String numCad, String numEmp, String nomeEmp, String tipoCol, String numLoc, String nomeSetor, String nomeFuncionario){
        this.numCad.set(numCad);
        this.numEmp.set(numEmp);
        this.nomeEmp.set(nomeEmp);
        this.tipoCol.set(tipoCol);
        this.numLoc.set(numLoc);
        this.nomeSetor.set(nomeSetor);
        this.nomeFuncionario.set(nomeFuncionario);
    }

    public final StringProperty nomeSetorProperty() {
        return nomeSetor;
    }

    public final String getNomeSetor() {
        return nomeSetor.get();
    }

    public final void setNomeSetor(String value) {
        nomeSetor.set(value);
    }

    public final StringProperty nomeFuncionarioProperty() {
        return nomeFuncionario;
    }

    public final String getNomeFuncionario() {
        return nomeFuncionario.get();
    }

    public final void setNomeFuncionario(String value) {
        nomeFuncionario.set(value);
    }

    public final StringProperty numLocProperty() {
        return numLoc;
    }

    public final String getNumLoc() {
        return numLoc.get();
    }

    public final void setNumLoc(String value) {
        numLoc.set(value);
    }

    public final StringProperty tipoColProperty() {
        return tipoCol;
    }

    public final String getTipoCol() {
        return tipoCol.get();
    }

    public final void setTipoCol(String value) {
        tipoCol.set(value);
    }

    public final StringProperty nomeEmpProperty() {
        return nomeEmp;
    }

    public final String getNomeEmp() {
        return nomeEmp.get();
    }

    public final void setNomeEmp(String value) {
        nomeEmp.set(value);
    }

    public final StringProperty numEmpProperty() {
        return numEmp;
    }

    public final String getNumEmp() {
        return numEmp.get();
    }

    public final void setNumEmp(String value) {
        numEmp.set(value);
    }

    public final StringProperty numCadProperty() {
        return numCad;
    }

    public final String getNumCad() {
        return numCad.get();
    }

    public final void setNumCad(String value) {
        numCad.set(value);
    }

}
