package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_COLECAO_MARCA_001")
public class SdColecaoMarca001 extends BasicModel {
    
    private final ObjectProperty<SdColecaoMarca001PK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> inicioVend = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> fimVend = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> inicioProd = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> fimProd = new SimpleObjectProperty<>();
    private final StringProperty colecaoAnterior = new SimpleStringProperty();
    private final StringProperty colecaoLastYear = new SimpleStringProperty();

    public SdColecaoMarca001() {
    }
    
    @EmbeddedId
    public SdColecaoMarca001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdColecaoMarca001PK> idProperty() {
        return id;
    }
    
    public void setId(SdColecaoMarca001PK id) {
        setCodigoFilter(id.getColecao().getCodigo());
        setDescricaoFilter(id.getColecao().getDescricao());
        this.id.set(id);
    }
    
    @Column(name = "INICIO_VEND")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getInicioVend() {
        return inicioVend.get();
    }
    
    public ObjectProperty<LocalDateTime> inicioVendProperty() {
        return inicioVend;
    }
    
    public void setInicioVend(LocalDateTime inicioVend) {
        this.inicioVend.set(inicioVend);
    }
    
    @Column(name = "FIM_VEND")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getFimVend() {
        return fimVend.get();
    }
    
    public ObjectProperty<LocalDateTime> fimVendProperty() {
        return fimVend;
    }
    
    public void setFimVend(LocalDateTime fimVend) {
        this.fimVend.set(fimVend);
    }
    
    @Column(name = "INICIO_PROD")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getInicioProd() {
        return inicioProd.get();
    }
    
    public ObjectProperty<LocalDateTime> inicioProdProperty() {
        return inicioProd;
    }
    
    public void setInicioProd(LocalDateTime inicioProd) {
        this.inicioProd.set(inicioProd);
    }
    
    @Column(name = "FIM_PROD")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getFimProd() {
        return fimProd.get();
    }
    
    public ObjectProperty<LocalDateTime> fimProdProperty() {
        return fimProd;
    }
    
    public void setFimProd(LocalDateTime fimProd) {
        this.fimProd.set(fimProd);
    }

    @Column(name = "COL_ANT")
    public String getColecaoAnterior() {
        return colecaoAnterior.get();
    }

    public StringProperty colecaoAnteriorProperty() {
        return colecaoAnterior;
    }

    public void setColecaoAnterior(String colecaoAnterior) {
        this.colecaoAnterior.set(colecaoAnterior);
    }

    @Column(name = "COL_LY")
    public String getColecaoLastYear() {
        return colecaoLastYear.get();
    }

    public StringProperty colecaoLastYearProperty() {
        return colecaoLastYear;
    }

    public void setColecaoLastYear(String colecaoLastYear) {
        this.colecaoLastYear.set(colecaoLastYear);
    }
}
