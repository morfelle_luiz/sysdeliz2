package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdColecaoMarca001PK extends BasicModel implements Serializable {
    
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    
    public SdColecaoMarca001PK() {
    
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
