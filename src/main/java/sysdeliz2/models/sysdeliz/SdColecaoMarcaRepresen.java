package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Entidade;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_COLECAO_MARCA_REPRESEN_001")
public class SdColecaoMarcaRepresen implements Serializable {

    @Embeddable
    public static class Pk implements Serializable{
        private final ObjectProperty<SdMarcasRepresen> marcaRepresen = new SimpleObjectProperty<>();
        private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();

        public Pk() {
        }

        public Pk(SdMarcasRepresen marca, Colecao colecaoNova) {
            this.marcaRepresen.set(marca);
            this.colecao.set(colecaoNova);
        }

        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumns({
                @JoinColumn(name = "CODREP"),
                @JoinColumn(name = "MARCA")
        })
        public SdMarcasRepresen getMarcaRepresen() {
            return marcaRepresen.get();
        }

        public ObjectProperty<SdMarcasRepresen> marcaRepresenProperty() {
            return marcaRepresen;
        }

        public void setMarcaRepresen(SdMarcasRepresen marcaRepresen) {
            this.marcaRepresen.set(marcaRepresen);
        }

        @OneToOne
        @JoinColumn(name = "COLECAO")
        public Colecao getColecao() {
            return colecao.get();
        }

        public ObjectProperty<Colecao> colecaoProperty() {
            return colecao;
        }

        public void setColecao(Colecao colecao) {
            this.colecao.set(colecao);
        }

    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> gerente = new SimpleObjectProperty<>();

    public SdColecaoMarcaRepresen() {
    }

    public SdColecaoMarcaRepresen(SdMarcasRepresen marca, Colecao colecaoNova, Entidade gerente) {
        this.id.set(new Pk(marca,colecaoNova));
        this.gerente.set(gerente);
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gerente")
    public Entidade getGerente() {
        return gerente.get();
    }

    public ObjectProperty<Entidade> gerenteProperty() {
        return gerente;
    }

    public void setGerente(Entidade gerente) {
        this.gerente.set(gerente);
    }
}

