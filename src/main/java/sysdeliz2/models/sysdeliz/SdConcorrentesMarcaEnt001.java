package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_CONCORRENTES_MARCA_ENT_001")
public class SdConcorrentesMarcaEnt001 {
    
    private final ObjectProperty<SdConcorrentesMarcaEnt001PK> id = new SimpleObjectProperty<>();
    private final IntegerProperty nivel = new SimpleIntegerProperty();
    
    public SdConcorrentesMarcaEnt001() {
    }
    
    public SdConcorrentesMarcaEnt001(SdMarcasEntidadePK marcaEntidade, SdMarcasConcorrentes001 concorrente) {
        this.id.set(new SdConcorrentesMarcaEnt001PK(marcaEntidade.getCodcli(), marcaEntidade.getMarca(), concorrente));
    }
    
    @EmbeddedId
    public SdConcorrentesMarcaEnt001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdConcorrentesMarcaEnt001PK> idProperty() {
        return id;
    }
    
    public void setId(SdConcorrentesMarcaEnt001PK id) {
        this.id.set(id);
    }
    
    @Column(name = "NIVEL")
    public int getNivel() {
        return nivel.get();
    }
    
    public IntegerProperty nivelProperty() {
        return nivel;
    }
    
    public void setNivel(int nivel) {
        this.nivel.set(nivel);
    }
    
    @Override
    public String toString() {
        return "[" + id.get().getMarcaConcorrente().getCodigo() + "] " + id.get().getMarcaConcorrente().getNome();
    }
}
