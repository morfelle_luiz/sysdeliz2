package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.CodcliAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdConcorrentesMarcaEnt001PK implements Serializable {
    
    private final IntegerProperty codcli = new SimpleIntegerProperty();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<SdMarcasConcorrentes001> marcaConcorrente = new SimpleObjectProperty<>();
    
    public SdConcorrentesMarcaEnt001PK() {
    }
    
    public SdConcorrentesMarcaEnt001PK(Integer codcli, Marca marca, SdMarcasConcorrentes001 marcaConcorrente) {
        this.codcli.set(codcli);
        this.marca.set(marca);
        this.marcaConcorrente.set(marcaConcorrente);
    }
    
    @Column(name = "CODCLI")
    @Convert(converter = CodcliAttributeConverter.class)
    public Integer getCodcli() {
        return codcli.get();
    }
    
    public IntegerProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Integer codcli) {
        this.codcli.set(codcli);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MARCA_CONCORRENTE")
    public SdMarcasConcorrentes001 getMarcaConcorrente() {
        return marcaConcorrente.get();
    }
    
    public ObjectProperty<SdMarcasConcorrentes001> marcaConcorrenteProperty() {
        return marcaConcorrente;
    }
    
    public void setMarcaConcorrente(SdMarcasConcorrentes001 marcaConcorrente) {
        this.marcaConcorrente.set(marcaConcorrente);
    }
}
