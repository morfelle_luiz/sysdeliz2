package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_CONDICAO_001")
@TelaSysDeliz(descricao = "Condição Pagamento", icon = "condicao pagamento (4).png")
public class SdCondicao001 extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Nr. Parcelas", width = 80)
    private final IntegerProperty nrpar = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty diave = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty nrdia = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty entrada = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Prazo Inicial", width = 80)
    private final IntegerProperty prazoini = new SimpleIntegerProperty();
    @Transient
    private final ObjectProperty<BigDecimal> taxa = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> descmax = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty tabela = new SimpleStringProperty();
    @Transient
    private final StringProperty tipo = new SimpleStringProperty();
    
    public SdCondicao001() {
    }
    
    @Id
    @Column(name = "CODIGO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONDICAO_001")
    @SequenceGenerator(sequenceName = "SD_SEQ_SD_CONDICAO_001", name = "SEQ_CONDICAO_001", initialValue = 1, allocationSize = 1)
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "NRPAR")
    public int getNrpar() {
        return nrpar.get();
    }
    
    public IntegerProperty nrparProperty() {
        return nrpar;
    }
    
    public void setNrpar(int nrpar) {
        this.nrpar.set(nrpar);
    }
    
    @Column(name = "DIAVE")
    public int getDiave() {
        return diave.get();
    }
    
    public IntegerProperty diaveProperty() {
        return diave;
    }
    
    public void setDiave(int diave) {
        this.diave.set(diave);
    }
    
    @Column(name = "NRDIA")
    public int getNrdia() {
        return nrdia.get();
    }
    
    public IntegerProperty nrdiaProperty() {
        return nrdia;
    }
    
    public void setNrdia(int nrdia) {
        this.nrdia.set(nrdia);
    }
    
    @Column(name = "ENTRADA")
    public int getEntrada() {
        return entrada.get();
    }
    
    public IntegerProperty entradaProperty() {
        return entrada;
    }
    
    public void setEntrada(int entrada) {
        this.entrada.set(entrada);
    }
    
    @Column(name = "PRAZOINI")
    public int getPrazoini() {
        return prazoini.get();
    }
    
    public IntegerProperty prazoiniProperty() {
        return prazoini;
    }
    
    public void setPrazoini(int prazoini) {
        this.prazoini.set(prazoini);
    }
    
    @Column(name = "TAXA")
    public BigDecimal getTaxa() {
        return taxa.get();
    }
    
    public ObjectProperty<BigDecimal> taxaProperty() {
        return taxa;
    }
    
    public void setTaxa(BigDecimal taxa) {
        this.taxa.set(taxa);
    }
    
    @Column(name = "DESCMAX")
    public BigDecimal getDescmax() {
        return descmax.get();
    }
    
    public ObjectProperty<BigDecimal> descmaxProperty() {
        return descmax;
    }
    
    public void setDescmax(BigDecimal descmax) {
        this.descmax.set(descmax);
    }
    
    @Column(name = "TABELA")
    public String getTabela() {
        return tabela.get();
    }
    
    public StringProperty tabelaProperty() {
        return tabela;
    }
    
    public void setTabela(String tabela) {
        this.tabela.set(tabela);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
