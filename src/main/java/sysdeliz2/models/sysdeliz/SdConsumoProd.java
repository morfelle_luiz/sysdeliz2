package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.ti.Unidade;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_CONSUMO_PROD_001")
@IdClass(SdConsumoProdId.class)
public class SdConsumoProd implements Serializable {
    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Material> material = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacao = new SimpleObjectProperty<>();
    private final ObjectProperty<Unidade> unidade = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> largmod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> grammod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consuMetroMod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consuKiloMod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> largmost = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> grammost = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumost = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> largprod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consuprod = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> porcentagem = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> porcentagemVariacao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty ordem = new SimpleIntegerProperty(0);
    private final BooleanProperty linhaItem = new SimpleBooleanProperty(true);

    public SdConsumoProd() {
    }

    public SdConsumoProd(Produto produto) {
        this.codigo.set(produto);
        this.setLinhaItem(false);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATERIAL")
    public Material getMaterial() {
        return material.get();
    }

    public ObjectProperty<Material> materialProperty() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material.set(material);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "APLICACAO")
    public Pcpapl getAplicacao() {
        return aplicacao.get();
    }

    public ObjectProperty<Pcpapl> aplicacaoProperty() {
        return aplicacao;
    }

    public void setAplicacao(Pcpapl aplicacao) {
        this.aplicacao.set(aplicacao);
    }



    @Column(name = "LARG_MOD")
    public BigDecimal getLargmod() {
        return largmod.get();
    }

    public ObjectProperty<BigDecimal> largmodProperty() {
        return largmod;
    }

    public void setLargmod(BigDecimal largmod) {
        this.largmod.set(largmod);
    }

    @Column(name = "GRAM_MOD")
    public BigDecimal getGrammod() {
        return grammod.get();
    }

    public ObjectProperty<BigDecimal> grammodProperty() {
        return grammod;
    }

    public void setGrammod(BigDecimal grammod) {
        this.grammod.set(grammod);
    }

    @Column(name = "CONSU_METRO_MOD")
    public BigDecimal getConsuMetroMod() {
        return consuMetroMod.get();
    }

    public ObjectProperty<BigDecimal> consuMetroModProperty() {
        return consuMetroMod;
    }

    public void setConsuMetroMod(BigDecimal consuMetroMod) {
        this.consuMetroMod.set(consuMetroMod);
    }

    @Column(name = "LARG_MOST")
    public BigDecimal getLargmost() {
        return largmost.get();
    }

    public ObjectProperty<BigDecimal> largmostProperty() {
        return largmost;
    }

    public void setLargmost(BigDecimal largmost) {
        this.largmost.set(largmost);
    }

    @Column(name = "GRAM_MOST")
    public BigDecimal getGrammost() {
        return grammost.get();
    }

    public ObjectProperty<BigDecimal> grammostProperty() {
        return grammost;
    }

    public void setGrammost(BigDecimal grammost) {
        this.grammost.set(grammost);
    }

    @Column(name = "CONSU_MOST")
    public BigDecimal getConsumost() {
        return consumost.get();
    }

    public ObjectProperty<BigDecimal> consumostProperty() {
        return consumost;
    }

    public void setConsumost(BigDecimal consumost) {
        this.consumost.set(consumost);
    }

    @Column(name = "LARG_PROD")
    public BigDecimal getLargprod() {
        return largprod.get();
    }

    public ObjectProperty<BigDecimal> largprodProperty() {
        return largprod;
    }

    public void setLargprod(BigDecimal largprod) {
        this.largprod.set(largprod);
    }

    @Column(name = "CONSU_PROD")
    public BigDecimal getConsuprod() {
        return consuprod.get();
    }

    public ObjectProperty<BigDecimal> consuprodProperty() {
        return consuprod;
    }

    public void setConsuprod(BigDecimal consuprod) {
        this.consuprod.set(consuprod);
    }

    @OneToOne
    @JoinColumn(name = "UNIDADE")
    public Unidade getUnidade() {
        return unidade.get();
    }

    public ObjectProperty<Unidade> unidadeProperty() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade.set(unidade);
    }

    @Column(name = "CONSU_KILO_MOD")
    public BigDecimal getConsuKiloMod() {
        return consuKiloMod.get();
    }

    public ObjectProperty<BigDecimal> consuKiloModProperty() {
        return consuKiloMod;
    }

    public void setConsuKiloMod(BigDecimal consuKiloMod) {
        this.consuKiloMod.set(consuKiloMod);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Transient
    public BigDecimal getPorcentagem() {
        return porcentagem.get();
    }

    public ObjectProperty<BigDecimal> porcentagemProperty() {
        return porcentagem;
    }

    public void setPorcentagem(BigDecimal porcentagem) {
        this.porcentagem.set(porcentagem);
    }

    @Transient
    public BigDecimal getPorcentagemVariacao() {
        return porcentagemVariacao.get();
    }

    public ObjectProperty<BigDecimal> porcentagemVariacaoProperty() {
        return porcentagemVariacao;
    }

    public void setPorcentagemVariacao(BigDecimal porcentagemVariacao) {
        this.porcentagemVariacao.set(porcentagemVariacao);
    }

    @Transient
    public boolean isLinhaItem() {
        return linhaItem.get();
    }

    public BooleanProperty linhaItemProperty() {
        return linhaItem;
    }

    public void setLinhaItem(boolean linhaItem) {
        this.linhaItem.set(linhaItem);
    }
}

