package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.models.ti.Produto;

import java.io.Serializable;
import java.util.Objects;

public class SdConsumoProdId implements Serializable {

    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Material> material = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacao = new SimpleObjectProperty<>();

    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    public Material getMaterial() {
        return material.get();
    }

    public ObjectProperty<Material> materialProperty() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material.set(material);
    }

    public Pcpapl getAplicacao() {
        return aplicacao.get();
    }

    public ObjectProperty<Pcpapl> aplicacaoProperty() {
        return aplicacao;
    }

    public void setAplicacao(Pcpapl aplicacao) {
        this.aplicacao.set(aplicacao);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdConsumoProdId that = (SdConsumoProdId) o;
        return Objects.equals(codigo, that.codigo) && Objects.equals(material, that.material) && Objects.equals(aplicacao, that.aplicacao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, material, aplicacao);
    }
}
