package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.IntegerAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_COR_RISCO_001")
public class SdCorRisco implements Serializable {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final IntegerProperty idrisco = new SimpleIntegerProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty seq = new SimpleIntegerProperty();
    private final StringProperty descrisco = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> comprimento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty folhas = new SimpleIntegerProperty(0);
    private final IntegerProperty folhasrc = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> consumototal = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumopeca = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumopecakg = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty totalSaldoOf = new SimpleIntegerProperty(0);
    private final IntegerProperty totalRealizado = new SimpleIntegerProperty(0);
    private final IntegerProperty totalRealizadoRc = new SimpleIntegerProperty(0);
    private final IntegerProperty totalGrade = new SimpleIntegerProperty(0);
    private final IntegerProperty totalParte = new SimpleIntegerProperty(0);
    private final IntegerProperty totalSomCor = new SimpleIntegerProperty(0);
    private final ListProperty<SdGradeRiscoCor> gradeObservable = new SimpleListProperty<>();
    private List<SdGradeRiscoCor> grade = new ArrayList<>();
    private final ListProperty<SdMateriaisRiscoCor> materiaisObservable = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdMateriaisRiscoCor> materiais = new ArrayList<>();
    private final ListProperty<SdCorRisco> riscoCorpo = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty corAgrupada = new SimpleBooleanProperty(false);
    private final BooleanProperty tubular = new SimpleBooleanProperty(false);
    private final BooleanProperty gradeFixada = new SimpleBooleanProperty(false);
    private final BooleanProperty escada = new SimpleBooleanProperty(false);
    private final BooleanProperty grupoCor = new SimpleBooleanProperty(false);
    
    public SdCorRisco() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_COR_RISCO")
    @SequenceGenerator(name = "SEQ_SD_COR_RISCO", sequenceName = "SEQ_SD_COR_RISCO", allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(int id) {
        this.id.set(id);
    }
    
    @Column(name = "ID_RISCO")
    public int getIdrisco() {
        return idrisco.get();
    }
    
    public IntegerProperty idriscoProperty() {
        return idrisco;
    }
    
    public void setIdrisco(int idrisco) {
        this.idrisco.set(idrisco);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "SEQ")
    public int getSeq() {
        return seq.get();
    }
    
    public IntegerProperty seqProperty() {
        return seq;
    }
    
    public void setSeq(int seq) {
        this.seq.set(seq);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescrisco() {
        return descrisco.get();
    }
    
    public StringProperty descriscoProperty() {
        return descrisco;
    }
    
    public void setDescrisco(String descrisco) {
        this.descrisco.set(descrisco);
    }
    
    @Column(name = "COMPRIMENTO")
    public BigDecimal getComprimento() {
        return comprimento.get();
    }
    
    public ObjectProperty<BigDecimal> comprimentoProperty() {
        return comprimento;
    }
    
    public void setComprimento(BigDecimal comprimento) {
        this.comprimento.set(comprimento);
    }
    
    @Column(name = "FOLHAS")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getFolhas() {
        return folhas.get();
    }
    
    public IntegerProperty folhasProperty() {
        return folhas;
    }
    
    public void setFolhas(int folhas) {
        this.folhas.set(folhas);
    }
    
    @Column(name = "CONSUMO_TOTAL")
    public BigDecimal getConsumototal() {
        return consumototal.get();
    }
    
    public ObjectProperty<BigDecimal> consumototalProperty() {
        return consumototal;
    }
    
    public void setConsumototal(BigDecimal consumototal) {
        this.consumototal.set(consumototal);
    }
    
    @Column(name = "CONSUMO_PC")
    public BigDecimal getConsumopeca() {
        return consumopeca.get();
    }
    
    public ObjectProperty<BigDecimal> consumopecaProperty() {
        return consumopeca;
    }
    
    public void setConsumopeca(BigDecimal consumopeca) {
        this.consumopeca.set(consumopeca);
    }
    
    @Column(name = "TOTAL_SALDO_OF")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getTotalSaldoOf() {
        return totalSaldoOf.get();
    }
    
    public IntegerProperty totalSaldoOfProperty() {
        return totalSaldoOf;
    }
    
    public void setTotalSaldoOf(int totalSaldoOf) {
        this.totalSaldoOf.set(totalSaldoOf);
    }
    
    @Column(name = "TOTAL_REALIZADO")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getTotalRealizado() {
        return totalRealizado.get();
    }
    
    public IntegerProperty totalRealizadoProperty() {
        return totalRealizado;
    }
    
    public void setTotalRealizado(int totalRealizado) {
        this.totalRealizado.set(totalRealizado);
    }
    
    @Column(name = "TOTAL_GRADE")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getTotalGrade() {
        return totalGrade.get();
    }
    
    public IntegerProperty totalGradeProperty() {
        return totalGrade;
    }
    
    public void setTotalGrade(int totalGrade) {
        this.totalGrade.set(totalGrade);
    }
    
    @Column(name = "TOTAL_PARTE")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getTotalParte() {
        return totalParte.get();
    }
    
    public IntegerProperty totalParteProperty() {
        return totalParte;
    }
    
    public void setTotalParte(int totalParte) {
        this.totalParte.set(totalParte);
    }
    
    @Transient
    public ObservableList<SdGradeRiscoCor> getGradeObservable() {
        gradeObservable.set(FXCollections.observableList(grade));
        return gradeObservable.get();
    }
    
    public ListProperty<SdGradeRiscoCor> gradeObservableProperty() {
        gradeObservable.set(FXCollections.observableList(grade));
        return gradeObservable;
    }
    
    public void setGradeObservable(ObservableList<SdGradeRiscoCor> gradeObservable) {
        this.gradeObservable.set(gradeObservable);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "ID_RISCO", referencedColumnName = "ID")
    public List<SdGradeRiscoCor> getGrade() {
        return grade;
    }
    
    public void setGrade(List<SdGradeRiscoCor> grade) {
        this.grade = grade;
    }
    
    @Transient
    public ObservableList<SdMateriaisRiscoCor> getMateriaisObservable() {
        return materiaisObservable.get();
    }
    
    public ListProperty<SdMateriaisRiscoCor> materiaisObservableProperty() {
        return materiaisObservable;
    }
    
    public void setMateriaisObservable(ObservableList<SdMateriaisRiscoCor> materiaisObservable) {
        this.materiaisObservable.set(materiaisObservable);
    }
    
    @Column(name = "CONSUMO_PC_KG")
    public BigDecimal getConsumopecakg() {
        return consumopecakg.get();
    }
    
    public ObjectProperty<BigDecimal> consumopecakgProperty() {
        return consumopecakg;
    }
    
    public void setConsumopecakg(BigDecimal consumopecakg) {
        this.consumopecakg.set(consumopecakg);
    }
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "ID_RISCO", referencedColumnName = "ID")
    @OrderBy(value = "SEQ_ENFESTO ASC")
    public List<SdMateriaisRiscoCor> getMateriais() {
        return materiais;
    }
    
    public void setMateriais(List<SdMateriaisRiscoCor> materiais) {
        this.materiais = materiais;
    }
    
    @Transient
    public ObservableList<SdCorRisco> getRiscoCorpo() {
        return riscoCorpo.get();
    }
    
    public ListProperty<SdCorRisco> riscoCorpoProperty() {
        return riscoCorpo;
    }
    
    public void setRiscoCorpo(ObservableList<SdCorRisco> riscoCorpo) {
        this.riscoCorpo.set(riscoCorpo);
    }
    
    @Transient
    public boolean isCorAgrupada() {
        return corAgrupada.get();
    }
    
    public BooleanProperty corAgrupadaProperty() {
        return corAgrupada;
    }
    
    public void setCorAgrupada(boolean corAgrupada) {
        this.corAgrupada.set(corAgrupada);
    }

    @Transient
    public boolean isGrupoCor() {
        return grupoCor.get();
    }

    public BooleanProperty grupoCorProperty() {
        return grupoCor;
    }

    public void setGrupoCor(boolean grupoCor) {
        this.grupoCor.set(grupoCor);
    }

    @Column(name = "FOLHAS_RC")
    public int getFolhasrc() {
        return folhasrc.get();
    }
    
    public IntegerProperty folhasrcProperty() {
        return folhasrc;
    }
    
    public void setFolhasrc(Integer folhasrc) {
            this.folhasrc.set(folhasrc);
    }
    
    @Column(name = "TOTAL_REALIZADO_RC")
    public int getTotalRealizadoRc() {
        return totalRealizadoRc.get();
    }
    
    public IntegerProperty totalRealizadoRcProperty() {
        return totalRealizadoRc;
    }
    
    public void setTotalRealizadoRc(Integer totalRealizadoRc) {
            this.totalRealizadoRc.set(totalRealizadoRc);
    }
    
    @Column(name = "TUBULAR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isTubular() {
        return tubular.get();
    }
    
    public BooleanProperty tubularProperty() {
        return tubular;
    }
    
    public void setTubular(boolean tubular) {
        this.tubular.set(tubular);
    }
    
    @Column(name = "GRADE_FIXADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isGradeFixada() {
        return gradeFixada.get();
    }
    
    public BooleanProperty gradeFixadaProperty() {
        return gradeFixada;
    }
    
    public void setGradeFixada(boolean gradeFixada) {
        this.gradeFixada.set(gradeFixada);
    }
    
    @Column(name = "ESCADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEscada() {
        return escada.get();
    }
    
    public BooleanProperty escadaProperty() {
        return escada;
    }
    
    public void setEscada(boolean escada) {
        this.escada.set(escada);
    }
    
    @Transient
    public int getTotalSomCor() {
        return totalSomCor.get();
    }
    
    public IntegerProperty totalSomCorProperty() {
        return totalSomCor;
    }
    
    public void setTotalSomCor(int totalSomCor) {
        this.totalSomCor.set(totalSomCor);
    }
    
    @PostLoad
    private void onPostLoad() {
    
    }
}
