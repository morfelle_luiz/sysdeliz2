package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "SD_DATA_TABPRZ_001")
@IdClass(SdDataTabPrzPK.class)
public class SdDataTabPrz implements Serializable {
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dataInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dataFim = new SimpleObjectProperty<>();

    public SdDataTabPrz() {
    }

    public SdDataTabPrz(String lote, String setor) {
        this.codigo.set(lote);
        this.setor.set(setor);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Column(name = "DATA_INICIO")
    public LocalDate getDataInicio() {
        return dataInicio.get();
    }

    public ObjectProperty<LocalDate> dataInicioProperty() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio.set(dataInicio);
    }

    @Column(name = "DATA_FIM")
    public LocalDate getDataFim() {
        return dataFim.get();
    }

    public ObjectProperty<LocalDate> dataFimProperty() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim.set(dataFim);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdDataTabPrz that = (SdDataTabPrz) o;
        return Objects.equals(codigo, that.codigo) && Objects.equals(setor, that.setor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, setor);
    }
}
