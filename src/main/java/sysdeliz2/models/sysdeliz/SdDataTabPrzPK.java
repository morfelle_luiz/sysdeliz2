package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class SdDataTabPrzPK implements Serializable {
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();

    public SdDataTabPrzPK() {
    }

    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdDataTabPrzPK that = (SdDataTabPrzPK) o;
        return Objects.equals(codigo, that.codigo) && Objects.equals(setor, that.setor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, setor);
    }
}
