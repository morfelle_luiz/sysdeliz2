package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_DIARIO_COLABORADOR_001")
public class SdDiarioColaborador001 {
    
    private final ObjectProperty<SdDiarioColaborador001PK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> minOcupados = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> eficiencia = new SimpleObjectProperty<>();
    
    public SdDiarioColaborador001() {
    }
    
    public SdDiarioColaborador001(SdDiarioColaborador001PK id, BigDecimal minOcupados, BigDecimal eficiencia) {
        this.id.set(id);
        this.minOcupados.set(minOcupados);
        this.eficiencia.set(eficiencia);
    }
    
    @EmbeddedId
    public SdDiarioColaborador001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdDiarioColaborador001PK> idProperty() {
        return id;
    }
    
    public void setId(SdDiarioColaborador001PK id) {
        this.id.set(id);
    }
    
    @Column(name = "MIN_OCUPADOS")
    public BigDecimal getMinOcupados() {
        return minOcupados.get();
    }
    
    public ObjectProperty<BigDecimal> minOcupadosProperty() {
        return minOcupados;
    }
    
    public void setMinOcupados(BigDecimal minOcupados) {
        this.minOcupados.set(minOcupados);
    }
    
    @Column(name = "EFICIENCIA")
    public BigDecimal getEficiencia() {
        return eficiencia.get();
    }
    
    public ObjectProperty<BigDecimal> eficienciaProperty() {
        return eficiencia;
    }
    
    public void setEficiencia(BigDecimal eficiencia) {
        this.eficiencia.set(eficiencia);
    }
}
