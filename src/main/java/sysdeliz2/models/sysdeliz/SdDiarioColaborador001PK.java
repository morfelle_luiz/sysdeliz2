package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class SdDiarioColaborador001PK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtDiario = new SimpleObjectProperty<>();
    
    public SdDiarioColaborador001PK() {
    }
    
    public SdDiarioColaborador001PK(Integer colaborador, LocalDate dtDiario) {
        this.colaborador.set(colaborador);
        this.dtDiario.set(dtDiario);
    }
    
    @Column(name = "COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name = "DT_DIARIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtDiario() {
        return dtDiario.get();
    }
    
    public ObjectProperty<LocalDate> dtDiarioProperty() {
        return dtDiario;
    }
    
    public void setDtDiario(LocalDate dtDiario) {
        this.dtDiario.set(dtDiario);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
