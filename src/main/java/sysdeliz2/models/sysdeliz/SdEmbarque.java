package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_EMBARQUE_001")
@TelaSysDeliz(descricao = "Embarque", icon = "globe_50.png")
public class SdEmbarque extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();

    @ExibeTableView(descricao = "Data", width = 150)
    @ColunaFilter(descricao = "Data", coluna = "data")
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<LocalDate>();

    @ExibeTableView(descricao = "Tipo", width = 100)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Status", width = 100)
    @ColunaFilter(descricao = "Status", coluna = "status")
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty codProforma = new SimpleIntegerProperty();
    private final StringProperty faturas = new SimpleStringProperty();
    private List<SdItemEmbarque> listItens = new ArrayList<>();

    public SdEmbarque() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_EMBARQUE")
    @SequenceGenerator(name = "SEQ_SD_EMBARQUE", sequenceName = "SEQ_SD_EMBARQUE", allocationSize = 1)
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        setCodigoFilter(codigo.toString());
        this.codigo.set(codigo);
    }

    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "COD_PROFORMA")
    public Integer getCodProforma() {
        return codProforma.get();
    }

    public IntegerProperty codProformaProperty() {
        return codProforma;
    }

    public void setCodProforma(Integer codproforma) {
        codproforma = codproforma == null ? 0 : codproforma;
        this.codProforma.set(codproforma);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CODIGO_EMBARQUE")
    public List<SdItemEmbarque> getListItens() {
        return listItens;
    }

    public void setListItens(List<SdItemEmbarque> listItens) {
        this.listItens = listItens;
    }

    @Column(name = "status")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "FATURAS")
    public String getFaturas() {
        return faturas.get();
    }

    public StringProperty faturasProperty() {
        return faturas;
    }

    public void setFaturas(String faturas) {
        this.faturas.set(faturas);
    }

    public enum TipoEmbarque {
        IMPORTACAO, EXPORTACAO
    }

    public enum StatusEmbarque {
        CRIADO, EM_EDICAO, CONCLUIDO, FINALIZADO
    }

}


