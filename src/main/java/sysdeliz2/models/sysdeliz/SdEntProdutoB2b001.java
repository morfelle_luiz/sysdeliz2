package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_ENT_PRODUTO_B2B_001")
public class SdEntProdutoB2b001 {
    
    private final ObjectProperty<SdEntProdutoB2b001PK> id = new SimpleObjectProperty<>();
    
    public SdEntProdutoB2b001() {
    }
    
    public SdEntProdutoB2b001(String codigo, String cor, String entrega) {
        id.set(new SdEntProdutoB2b001PK(codigo, cor, entrega));
    }
    
    @EmbeddedId
    public SdEntProdutoB2b001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdEntProdutoB2b001PK> idProperty() {
        return id;
    }
    
    public void setId(SdEntProdutoB2b001PK id) {
        this.id.set(id);
    }
}
