package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SdEntProdutoB2b001PK implements Serializable {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty entrega = new SimpleStringProperty();
    
    public SdEntProdutoB2b001PK() {
    }
    
    public SdEntProdutoB2b001PK(String codigo, String cor, String entrega) {
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.entrega.set(entrega);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "ENTREGA")
    public String getEntrega() {
        return entrega.get();
    }
    
    public StringProperty entregaProperty() {
        return entrega;
    }
    
    public void setEntrega(String entrega) {
        this.entrega.set(entrega);
    }
}
