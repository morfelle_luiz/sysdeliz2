package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import kotlin.Suppress;
import org.apache.commons.lang.builder.ToStringBuilder;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "SD_ENTIDADE_001")
@Suppress(names = "unused")
public class SdEntidade {
    
    private final IntegerProperty codcli = new SimpleIntegerProperty();
    private final StringProperty origem = new SimpleStringProperty();
    private final BooleanProperty liberadoB2b = new SimpleBooleanProperty();
    private final BooleanProperty liberadoCatalogo = new SimpleBooleanProperty(false);
    private final BooleanProperty atualizaGrade = new SimpleBooleanProperty(true);
    private final BooleanProperty avisaBoleto = new SimpleBooleanProperty(false);
    private final ObjectProperty<SdCondicao001> condPagto = new SimpleObjectProperty<>();
    private final StringProperty perfil = new SimpleStringProperty();
    private final BooleanProperty sincB2b = new SimpleBooleanProperty();
    private final BooleanProperty imprimeConteudoCaixa = new SimpleBooleanProperty(false);
    private final BooleanProperty atendeRobo = new SimpleBooleanProperty(false);
    private final BooleanProperty antecipaProduto = new SimpleBooleanProperty(false);
    private final ObjectProperty<SdMagentoSitCli> sitcliB2b = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> minimoB2b = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> minimoReserva = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> maximoReserva = new SimpleObjectProperty<>(new BigDecimal("999999"));
    private final BooleanProperty separaMarcasFatura = new SimpleBooleanProperty(false);
    private final BooleanProperty imprimeRemessa = new SimpleBooleanProperty(false);
    private final BooleanProperty atualizaGradeRemessa = new SimpleBooleanProperty(false);
    private final IntegerProperty faturamentosMensais = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dataRevisao = new SimpleObjectProperty<>();
    private final BooleanProperty dividaSerasa = new SimpleBooleanProperty(false);

    // <editor-fold defaultstate="collapsed" desc="mt">
    public SdEntidade() {
    }

    public SdEntidade(SdEntidade toCopy) {
        this.codcli.set(toCopy.codcli.get());
        this.origem.set(toCopy.origem.get());
        this.liberadoB2b.set(toCopy.liberadoB2b.get());
        this.liberadoCatalogo.set(toCopy.liberadoCatalogo.get());
        this.atualizaGrade.set(toCopy.atualizaGrade.get());
        this.avisaBoleto.set(toCopy.avisaBoleto.get());
        this.condPagto.set(toCopy.condPagto.get());
        this.perfil.set(toCopy.perfil.get());
        this.sincB2b.set(toCopy.sincB2b.get());
        this.imprimeConteudoCaixa.set(toCopy.imprimeConteudoCaixa.get());
        this.atendeRobo.set(toCopy.atendeRobo.get());
        this.antecipaProduto.set(toCopy.antecipaProduto.get());
        this.sitcliB2b.set(toCopy.sitcliB2b.get());
        this.minimoB2b.set(toCopy.minimoB2b.get());
        this.minimoReserva.set(toCopy.minimoReserva.get());
        this.maximoReserva.set(toCopy.maximoReserva.get());
        this.separaMarcasFatura.set(toCopy.separaMarcasFatura.get());
        this.imprimeRemessa.set(toCopy.imprimeRemessa.get());
        this.atualizaGradeRemessa.set(toCopy.atualizaGradeRemessa.get());
        this.faturamentosMensais.set(toCopy.faturamentosMensais.get());
        this.dataRevisao.set(toCopy.dataRevisao.get());
    }

    @Id
    @Column(name = "CODCLI", columnDefinition = "varchar2()")
    @Convert(converter = CodcliAttributeConverter.class)
    public Integer getCodcli() {
        return codcli.get();
    }

    public IntegerProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(Integer codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem.set(origem);
    }

    @Column(name = "LIBERADO_B2B")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLiberadoB2b() {
        return liberadoB2b.get();
    }

    public BooleanProperty liberadoB2bProperty() {
        return liberadoB2b;
    }

    public void setLiberadoB2b(boolean liberadoB2b) {
        this.liberadoB2b.set(liberadoB2b);
    }

    @Column(name = "LIBERADO_CATALOGO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLiberadoCatalogo() {
        return liberadoCatalogo.get();
    }

    public BooleanProperty liberadoCatalogoProperty() {
        return liberadoCatalogo;
    }

    public void setLiberadoCatalogo(boolean liberadoCatalogo) {
        this.liberadoCatalogo.set(liberadoCatalogo);
    }

    @Column(name = "ATUALIZA_GRADE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtualizaGrade() {
        return atualizaGrade.get();
    }

    public BooleanProperty atualizaGradeProperty() {
        return atualizaGrade;
    }

    public void setAtualizaGrade(boolean atualizaGrade) {
        this.atualizaGrade.set(atualizaGrade);
    }

    @Column(name = "AVISA_BOLETO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAvisaBoleto() {
        return avisaBoleto.get();
    }

    public BooleanProperty avisaBoletoProperty() {
        return avisaBoleto;
    }

    public void setAvisaBoleto(boolean avisaBoleto) {
        this.avisaBoleto.set(avisaBoleto);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COND_PAGTO")
    public SdCondicao001 getCondPagto() {
        return condPagto.get();
    }

    public ObjectProperty<SdCondicao001> condPagtoProperty() {
        return condPagto;
    }

    public void setCondPagto(SdCondicao001 condPagto) {
        this.condPagto.set(condPagto);
    }

    @Column(name = "PERFIL_ENTIDADE")
    public String getPerfil() {
        return perfil.get();
    }

    public StringProperty perfilProperty() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil.set(perfil);
    }

    @Column(name = "SINC_B2B")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSincB2b() {
        return sincB2b.get();
    }

    public BooleanProperty sincB2bProperty() {
        return sincB2b;
    }

    public void setSincB2b(boolean sincB2b) {
        this.sincB2b.set(sincB2b);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SITCLI_B2B")
    public SdMagentoSitCli getSitcliB2b() {
        return sitcliB2b.get();
    }

    public ObjectProperty<SdMagentoSitCli> sitcliB2bProperty() {
        return sitcliB2b;
    }

    public void setSitcliB2b(SdMagentoSitCli sitcliB2b) {
        this.sitcliB2b.set(sitcliB2b);
    }

    @Column(name = "IMPRIMI_CONTEUDO_CX")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImprimeConteudoCaixa() {
        return imprimeConteudoCaixa.get();
    }

    public BooleanProperty imprimeConteudoCaixaProperty() {
        return imprimeConteudoCaixa;
    }

    public void setImprimeConteudoCaixa(boolean imprimeConteudoCaixa) {
        this.imprimeConteudoCaixa.set(imprimeConteudoCaixa);
    }

    @Column(name = "ATENDE_ROBO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtendeRobo() {
        return atendeRobo.get();
    }

    public BooleanProperty atendeRoboProperty() {
        return atendeRobo;
    }

    public void setAtendeRobo(boolean atendeRobo) {
        this.atendeRobo.set(atendeRobo);
    }

    @Column(name = "ANTECIPA_PRODUTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAntecipaProduto() {
        return antecipaProduto.get();
    }

    public BooleanProperty antecipaProdutoProperty() {
        return antecipaProduto;
    }

    public void setAntecipaProduto(boolean antecipaProduto) {
        this.antecipaProduto.set(antecipaProduto);
    }

    @Column(name = "MINIMO_B2B")
    public BigDecimal getMinimoB2b() {
        return minimoB2b.get();
    }

    public ObjectProperty<BigDecimal> minimoB2bProperty() {
        return minimoB2b;
    }

    public void setMinimoB2b(BigDecimal minimoB2b) {
        this.minimoB2b.set(minimoB2b);
    }

    @Column(name = "MINIMO_RESERVA")
    public BigDecimal getMinimoReserva() {
        return minimoReserva.get();
    }

    public ObjectProperty<BigDecimal> minimoReservaProperty() {
        return minimoReserva;
    }

    public void setMinimoReserva(BigDecimal minimoReserva) {
        this.minimoReserva.set(minimoReserva);
    }

    @Column(name = "MAXIMO_RESERVA")
    public BigDecimal getMaximoReserva() {
        return maximoReserva.get();
    }

    public ObjectProperty<BigDecimal> maximoReservaProperty() {
        return maximoReserva;
    }

    public void setMaximoReserva(BigDecimal maximoReserva) {
        this.maximoReserva.set(maximoReserva);
    }

    @Column(name = "SEPARA_MARCAS_FATURA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSeparaMarcasFatura() {
        return separaMarcasFatura.get();
    }

    public BooleanProperty separaMarcasFaturaProperty() {
        return separaMarcasFatura;
    }

    public void setSeparaMarcasFatura(boolean separaMarcasFatura) {
        this.separaMarcasFatura.set(separaMarcasFatura);
    }

    @Column(name = "IMPRIME_REMESSA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImprimeRemessa() {
        return imprimeRemessa.get();
    }

    public BooleanProperty imprimeRemessaProperty() {
        return imprimeRemessa;
    }

    public void setImprimeRemessa(boolean imprimeRemessa) {
        this.imprimeRemessa.set(imprimeRemessa);
    }

    @Column(name = "ATUALIZA_GRADE_REMESSA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtualizaGradeRemessa() {
        return atualizaGradeRemessa.get();
    }

    public BooleanProperty atualizaGradeRemessaProperty() {
        return atualizaGradeRemessa;
    }

    public void setAtualizaGradeRemessa(boolean atualizaGradeRemessa) {
        this.atualizaGradeRemessa.set(atualizaGradeRemessa);
    }

    @Column(name = "FAT_MENSAL")
    public int getFaturamentosMensais() {
        return faturamentosMensais.get();
    }

    public IntegerProperty faturamentosMensaisProperty() {
        return faturamentosMensais;
    }

    public void setFaturamentosMensais(int faturamentosMensais) {
        this.faturamentosMensais.set(faturamentosMensais);
    }

    @Column(name = "DATA_REVISAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataRevisao() {
        return dataRevisao.get();
    }

    public ObjectProperty<LocalDate> dataRevisaoProperty() {
        return dataRevisao;
    }

    public void setDataRevisao(LocalDate dataRevisao) {
        this.dataRevisao.set(dataRevisao);
    }

    @Column(name = "DIVIDA_SERASA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isDividaSerasa() {
        return dividaSerasa.get();
    }

    public BooleanProperty dividaSerasaProperty() {
        return dividaSerasa;
    }

    public void setDividaSerasa(boolean dividaSerasa) {
        this.dividaSerasa.set(dividaSerasa);
    }

    // </editor-fold>

    @Transient
    public String stringToLog(){
        return new ToStringBuilder(this)
                .append("codcli", codcli.get())
                .append("origem", origem.get())
                .append("liberadob2b", liberadoB2b.get())
                .append("atualizagrade", atualizaGrade.get())
                .append("avisaboleto", avisaBoleto.get())
                .append("condpagto", condPagto.get())
                .append("perfilentidade", perfil.get())
                .append("sincb2b", sincB2b.get())
                .append("separaMarcasFatura", separaMarcasFatura.get())
                .append("imprimeRemessa", imprimeRemessa.get())
                .append("atualizaGradeRemessa", atualizaGradeRemessa.get())
                .append("faturamentosMensais", faturamentosMensais.get())
                .append("maximoReserva", maximoReserva.get())
                .append("minimoB2b", minimoB2b.get())
                .append("minimoReserva", minimoReserva.get())
                .append("atendeRobo", atendeRobo.get())
                .toString();
    }

    @Transient
    public String diffObject(SdEntidade entidade) {
        String diffs = "Complemento: [\n";
        if (this.origem.getValue() == null && entidade.origem.getValue() != null) diffs = diffs.concat("origem: NULL>" + entidade.origem.getValue().toString() + "\n");
        else if (this.origem.getValue() != null && entidade.origem.getValue() == null) diffs = diffs.concat("origem:" + this.origem.getValue().toString() + ">NULL\n");
        else if (this.origem.getValue() != null && entidade.origem.getValue() != null)
            diffs = diffs.concat(!this.origem.getValue().equals(entidade.origem.getValue()) ? "origem:" + this.origem.getValue().toString() + ">" + entidade.origem.getValue().toString() + "\n" : "");
        if (this.liberadoB2b.getValue() == null && entidade.liberadoB2b.getValue() != null) diffs = diffs.concat("liberadoB2b: NULL>" + entidade.liberadoB2b.getValue().toString() + "\n");
        else if (this.liberadoB2b.getValue() != null && entidade.liberadoB2b.getValue() == null) diffs = diffs.concat("liberadoB2b:" + this.liberadoB2b.getValue().toString() + ">NULL\n");
        else if (this.liberadoB2b.getValue() != null && entidade.liberadoB2b.getValue() != null)
            diffs = diffs.concat(!this.liberadoB2b.getValue().equals(entidade.liberadoB2b.getValue()) ? "liberadoB2b:" + this.liberadoB2b.getValue().toString() + ">" + entidade.liberadoB2b.getValue().toString() + "\n" : "");
        if (this.liberadoCatalogo.getValue() == null && entidade.liberadoCatalogo.getValue() != null) diffs = diffs.concat("liberadoCatalogo: NULL>" + entidade.liberadoCatalogo.getValue().toString() + "\n");
        else if (this.liberadoCatalogo.getValue() != null && entidade.liberadoCatalogo.getValue() == null) diffs = diffs.concat("liberadoCatalogo:" + this.liberadoCatalogo.getValue().toString() + ">NULL\n");
        else if (this.liberadoCatalogo.getValue() != null && entidade.liberadoCatalogo.getValue() != null)
            diffs = diffs.concat(!this.liberadoCatalogo.getValue().equals(entidade.liberadoCatalogo.getValue()) ? "liberadoCatalogo:" + this.liberadoCatalogo.getValue().toString() + ">" + entidade.liberadoCatalogo.getValue().toString() + "\n" : "");
        if (this.atualizaGrade.getValue() == null && entidade.atualizaGrade.getValue() != null) diffs = diffs.concat("atualizaGrade: NULL>" + entidade.atualizaGrade.getValue().toString() + "\n");
        else if (this.atualizaGrade.getValue() != null && entidade.atualizaGrade.getValue() == null) diffs = diffs.concat("atualizaGrade:" + this.atualizaGrade.getValue().toString() + ">NULL\n");
        else if (this.atualizaGrade.getValue() != null && entidade.atualizaGrade.getValue() != null)
            diffs = diffs.concat(!this.atualizaGrade.getValue().equals(entidade.atualizaGrade.getValue()) ? "atualizaGrade:" + this.atualizaGrade.getValue().toString() + ">" + entidade.atualizaGrade.getValue().toString() + "\n" : "");
        if (this.avisaBoleto.getValue() == null && entidade.avisaBoleto.getValue() != null) diffs = diffs.concat("avisaBoleto: NULL>" + entidade.avisaBoleto.getValue().toString() + "\n");
        else if (this.avisaBoleto.getValue() != null && entidade.avisaBoleto.getValue() == null) diffs = diffs.concat("avisaBoleto:" + this.avisaBoleto.getValue().toString() + ">NULL\n");
        else if (this.avisaBoleto.getValue() != null && entidade.avisaBoleto.getValue() != null)
            diffs = diffs.concat(!this.avisaBoleto.getValue().equals(entidade.avisaBoleto.getValue()) ? "avisaBoleto:" + this.avisaBoleto.getValue().toString() + ">" + entidade.avisaBoleto.getValue().toString() + "\n" : "");
        if (this.condPagto.getValue() == null && entidade.condPagto.getValue() != null) diffs = diffs.concat("condPagto: NULL>" + entidade.condPagto.getValue().toString() + "\n");
        else if (this.condPagto.getValue() != null && entidade.condPagto.getValue() == null) diffs = diffs.concat("condPagto:" + this.condPagto.getValue().toString() + ">NULL\n");
        else if (this.condPagto.getValue() != null && entidade.condPagto.getValue() != null)
            diffs = diffs.concat(!this.condPagto.getValue().equals(entidade.condPagto.getValue()) ? "condPagto:" + this.condPagto.getValue().toString() + ">" + entidade.condPagto.getValue().toString() + "\n" : "");
        if (this.perfil.getValue() == null && entidade.perfil.getValue() != null) diffs = diffs.concat("perfil: NULL>" + entidade.perfil.getValue().toString() + "\n");
        else if (this.perfil.getValue() != null && entidade.perfil.getValue() == null) diffs = diffs.concat("perfil:" + this.perfil.getValue().toString() + ">NULL\n");
        else if (this.perfil.getValue() != null && entidade.perfil.getValue() != null)
            diffs = diffs.concat(!this.perfil.getValue().equals(entidade.perfil.getValue()) ? "perfil:" + this.perfil.getValue().toString() + ">" + entidade.perfil.getValue().toString() + "\n" : "");
        if (this.sincB2b.getValue() == null && entidade.sincB2b.getValue() != null) diffs = diffs.concat("sincB2b: NULL>" + entidade.sincB2b.getValue().toString() + "\n");
        else if (this.sincB2b.getValue() != null && entidade.sincB2b.getValue() == null) diffs = diffs.concat("sincB2b:" + this.sincB2b.getValue().toString() + ">NULL\n");
        else if (this.sincB2b.getValue() != null && entidade.sincB2b.getValue() != null)
            diffs = diffs.concat(!this.sincB2b.getValue().equals(entidade.sincB2b.getValue()) ? "sincB2b:" + this.sincB2b.getValue().toString() + ">" + entidade.sincB2b.getValue().toString() + "\n" : "");
        if (this.imprimeConteudoCaixa.getValue() == null && entidade.imprimeConteudoCaixa.getValue() != null) diffs = diffs.concat("imprimeConteudoCaixa: NULL>" + entidade.imprimeConteudoCaixa.getValue().toString() + "\n");
        else if (this.imprimeConteudoCaixa.getValue() != null && entidade.imprimeConteudoCaixa.getValue() == null) diffs = diffs.concat("imprimeConteudoCaixa:" + this.imprimeConteudoCaixa.getValue().toString() + ">NULL\n");
        else if (this.imprimeConteudoCaixa.getValue() != null && entidade.imprimeConteudoCaixa.getValue() != null)
            diffs = diffs.concat(!this.imprimeConteudoCaixa.getValue().equals(entidade.imprimeConteudoCaixa.getValue()) ? "imprimeConteudoCaixa:" + this.imprimeConteudoCaixa.getValue().toString() + ">" + entidade.imprimeConteudoCaixa.getValue().toString() + "\n" : "");
        if (this.atendeRobo.getValue() == null && entidade.atendeRobo.getValue() != null) diffs = diffs.concat("atendeRobo: NULL>" + entidade.atendeRobo.getValue().toString() + "\n");
        else if (this.atendeRobo.getValue() != null && entidade.atendeRobo.getValue() == null) diffs = diffs.concat("atendeRobo:" + this.atendeRobo.getValue().toString() + ">NULL\n");
        else if (this.atendeRobo.getValue() != null && entidade.atendeRobo.getValue() != null)
            diffs = diffs.concat(!this.atendeRobo.getValue().equals(entidade.atendeRobo.getValue()) ? "atendeRobo:" + this.atendeRobo.getValue().toString() + ">" + entidade.atendeRobo.getValue().toString() + "\n" : "");
        if (this.antecipaProduto.getValue() == null && entidade.antecipaProduto.getValue() != null) diffs = diffs.concat("antecipaProduto: NULL>" + entidade.antecipaProduto.getValue().toString() + "\n");
        else if (this.antecipaProduto.getValue() != null && entidade.antecipaProduto.getValue() == null) diffs = diffs.concat("antecipaProduto:" + this.antecipaProduto.getValue().toString() + ">NULL\n");
        else if (this.antecipaProduto.getValue() != null && entidade.antecipaProduto.getValue() != null)
            diffs = diffs.concat(!this.antecipaProduto.getValue().equals(entidade.antecipaProduto.getValue()) ? "antecipaProduto:" + this.antecipaProduto.getValue().toString() + ">" + entidade.antecipaProduto.getValue().toString() + "\n" : "");
        if (this.sitcliB2b.getValue() == null && entidade.sitcliB2b.getValue() != null) diffs = diffs.concat("sitcliB2b: NULL>" + entidade.sitcliB2b.getValue().toString() + "\n");
        else if (this.sitcliB2b.getValue() != null && entidade.sitcliB2b.getValue() == null) diffs = diffs.concat("sitcliB2b:" + this.sitcliB2b.getValue().toString() + ">NULL\n");
        else if (this.sitcliB2b.getValue() != null && entidade.sitcliB2b.getValue() != null)
            diffs = diffs.concat(!this.sitcliB2b.getValue().equals(entidade.sitcliB2b.getValue()) ? "sitcliB2b:" + this.sitcliB2b.getValue().toString() + ">" + entidade.sitcliB2b.getValue().toString() + "\n" : "");
        if (this.minimoB2b.getValue() == null && entidade.minimoB2b.getValue() != null) diffs = diffs.concat("minimoB2b: NULL>" + entidade.minimoB2b.getValue().toString() + "\n");
        else if (this.minimoB2b.getValue() != null && entidade.minimoB2b.getValue() == null) diffs = diffs.concat("minimoB2b:" + this.minimoB2b.getValue().toString() + ">NULL\n");
        else if (this.minimoB2b.getValue() != null && entidade.minimoB2b.getValue() != null)
            diffs = diffs.concat(!this.minimoB2b.getValue().equals(entidade.minimoB2b.getValue()) ? "minimoB2b:" + this.minimoB2b.getValue().toString() + ">" + entidade.minimoB2b.getValue().toString() + "\n" : "");
        if (this.minimoReserva.getValue() == null && entidade.minimoReserva.getValue() != null) diffs = diffs.concat("minimoReserva: NULL>" + entidade.minimoReserva.getValue().toString() + "\n");
        else if (this.minimoReserva.getValue() != null && entidade.minimoReserva.getValue() == null) diffs = diffs.concat("minimoReserva:" + this.minimoReserva.getValue().toString() + ">NULL\n");
        else if (this.minimoReserva.getValue() != null && entidade.minimoReserva.getValue() != null)
            diffs = diffs.concat(!this.minimoReserva.getValue().equals(entidade.minimoReserva.getValue()) ? "minimoReserva:" + this.minimoReserva.getValue().toString() + ">" + entidade.minimoReserva.getValue().toString() + "\n" : "");
        if (this.maximoReserva.getValue() == null && entidade.maximoReserva.getValue() != null) diffs = diffs.concat("maximoReserva: NULL>" + entidade.maximoReserva.getValue().toString() + "\n");
        else if (this.maximoReserva.getValue() != null && entidade.maximoReserva.getValue() == null) diffs = diffs.concat("maximoReserva:" + this.maximoReserva.getValue().toString() + ">NULL\n");
        else if (this.maximoReserva.getValue() != null && entidade.maximoReserva.getValue() != null)
            diffs = diffs.concat(!this.maximoReserva.getValue().equals(entidade.maximoReserva.getValue()) ? "maximoReserva:" + this.maximoReserva.getValue().toString() + ">" + entidade.maximoReserva.getValue().toString() + "\n" : "");
        if (this.separaMarcasFatura.getValue() == null && entidade.separaMarcasFatura.getValue() != null) diffs = diffs.concat("separaMarcasFatura: NULL>" + entidade.separaMarcasFatura.getValue().toString() + "\n");
        else if (this.separaMarcasFatura.getValue() != null && entidade.separaMarcasFatura.getValue() == null) diffs = diffs.concat("separaMarcasFatura:" + this.separaMarcasFatura.getValue().toString() + ">NULL\n");
        else if (this.separaMarcasFatura.getValue() != null && entidade.separaMarcasFatura.getValue() != null)
            diffs = diffs.concat(!this.separaMarcasFatura.getValue().equals(entidade.separaMarcasFatura.getValue()) ? "separaMarcasFatura:" + this.separaMarcasFatura.getValue().toString() + ">" + entidade.separaMarcasFatura.getValue().toString() + "\n" : "");
        if (this.imprimeRemessa.getValue() == null && entidade.imprimeRemessa.getValue() != null) diffs = diffs.concat("imprimeRemessa: NULL>" + entidade.imprimeRemessa.getValue().toString() + "\n");
        else if (this.imprimeRemessa.getValue() != null && entidade.imprimeRemessa.getValue() == null) diffs = diffs.concat("imprimeRemessa:" + this.imprimeRemessa.getValue().toString() + ">NULL\n");
        else if (this.imprimeRemessa.getValue() != null && entidade.imprimeRemessa.getValue() != null)
            diffs = diffs.concat(!this.imprimeRemessa.getValue().equals(entidade.imprimeRemessa.getValue()) ? "imprimeRemessa:" + this.imprimeRemessa.getValue().toString() + ">" + entidade.imprimeRemessa.getValue().toString() + "\n" : "");
        if (this.faturamentosMensais.getValue() == null && entidade.faturamentosMensais.getValue() != null) diffs = diffs.concat("faturamentosMensais: NULL>" + entidade.faturamentosMensais.getValue().toString() + "\n");
        else if (this.faturamentosMensais.getValue() != null && entidade.faturamentosMensais.getValue() == null) diffs = diffs.concat("faturamentosMensais:" + this.faturamentosMensais.getValue().toString() + ">NULL\n");
        else if (this.faturamentosMensais.getValue() != null && entidade.faturamentosMensais.getValue() != null)
            diffs = diffs.concat(!this.faturamentosMensais.getValue().equals(entidade.faturamentosMensais.getValue()) ? "faturamentosMensais:" + this.faturamentosMensais.getValue().toString() + ">" + entidade.faturamentosMensais.getValue().toString() + "\n" : "");
        if (this.atualizaGradeRemessa.getValue() == null && entidade.atualizaGradeRemessa.getValue() != null) diffs = diffs.concat("atualizaGradeRemessa: NULL>" + entidade.atualizaGradeRemessa.getValue().toString() + "\n");
        else if (this.atualizaGradeRemessa.getValue() != null && entidade.atualizaGradeRemessa.getValue() == null) diffs = diffs.concat("atualizaGradeRemessa:" + this.atualizaGradeRemessa.getValue().toString() + ">NULL\n");
        else if (this.atualizaGradeRemessa.getValue() != null && entidade.atualizaGradeRemessa.getValue() != null)
            diffs = diffs.concat(!this.atualizaGradeRemessa.getValue().equals(entidade.atualizaGradeRemessa.getValue()) ? "atualizaGradeRemessa:" + this.atualizaGradeRemessa.getValue().toString() + ">" + entidade.atualizaGradeRemessa.getValue().toString() + "\n" : "");

        return diffs + "]";
    }


    
}
