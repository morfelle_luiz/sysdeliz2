package sysdeliz2.models.sysdeliz;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "SD_ENTREGA_PRODUCAO_001")
public class SdEntregaProducao001 {
    
    private final ObjectProperty<SdEntregaProducao001PK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtFim = new SimpleObjectProperty<>();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    
    public SdEntregaProducao001() {
    }
    
    public SdEntregaProducao001(Colecao colecao, Marca marca, TabPrz loteEntrega, TabPrz loteProducao,
                                LocalDate dtInicio, LocalDate dtFim, Boolean ativo) {
        this.id.set(new SdEntregaProducao001PK(colecao, marca, loteEntrega, loteProducao));
        this.dtInicio.set(dtInicio);
        this.dtFim.set(dtFim);
        this.ativo.set(ativo);
    }
    
    @EmbeddedId
    public SdEntregaProducao001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdEntregaProducao001PK> idProperty() {
        return id;
    }
    
    public void setId(SdEntregaProducao001PK id) {
        this.id.set(id);
    }
    
    @Column(name = "DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtInicio() {
        return dtInicio.get();
    }
    
    public ObjectProperty<LocalDate> dtInicioProperty() {
        return dtInicio;
    }
    
    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio.set(dtInicio);
    }
    
    @Column(name = "DT_FIM")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtFim() {
        return dtFim.get();
    }
    
    public ObjectProperty<LocalDate> dtFimProperty() {
        return dtFim;
    }
    
    public void setDtFim(LocalDate dtFim) {
        this.dtFim.set(dtFim);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
}
