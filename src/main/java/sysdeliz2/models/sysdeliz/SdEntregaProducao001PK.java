package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.TabPrz;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdEntregaProducao001PK implements Serializable {
    
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final ObjectProperty<TabPrz> entrega = new SimpleObjectProperty<>();
    private final ObjectProperty<TabPrz> producao = new SimpleObjectProperty<>();
    
    public SdEntregaProducao001PK() {
    }
    
    public SdEntregaProducao001PK(Colecao colecao, Marca marca, TabPrz loteEntrega, TabPrz loteProducao) {
        this.colecao.set(colecao);
        this.marca.set(marca);
        this.entrega.set(loteEntrega);
        this.producao.set(loteProducao);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTREGA")
    public TabPrz getEntrega() {
        return entrega.get();
    }
    
    public ObjectProperty<TabPrz> entregaProperty() {
        return entrega;
    }
    
    public void setEntrega(TabPrz entrega) {
        this.entrega.set(entrega);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCAO")
    public TabPrz getProducao() {
        return producao.get();
    }
    
    public ObjectProperty<TabPrz> producaoProperty() {
        return producao;
    }
    
    public void setProducao(TabPrz producao) {
        this.producao.set(producao);
    }
}
