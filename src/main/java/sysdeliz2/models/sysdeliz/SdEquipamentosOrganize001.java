/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_EQUIPAMENTOS_ORGANIZE_001")
@TelaSysDeliz(descricao = "Equipamentos Organize", icon = "equipamento (4).png")
public class SdEquipamentosOrganize001 {

    @Transient
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codorg")
    private final IntegerProperty codorg = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 200)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> interferencia = new SimpleObjectProperty<BigDecimal>();
    
    public SdEquipamentosOrganize001() {
    }
    
    public SdEquipamentosOrganize001(String codigo, Integer codorg, String descricao, Double interferencia) {
        this.codigo.set(codigo);
        this.codorg.set(codorg);
        this.descricao.set(descricao);
        this.interferencia.set(BigDecimal.valueOf(interferencia));
    }
    
    @Id
    @Column(name = "CODORG")
    public int getCodorg() {
        return codorg.get();
    }
    public void setCodorg(int value) {
        codorg.set(value);
    }
    public IntegerProperty codorgProperty() {
        return codorg;
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    public void setCodigo(String value) {
        codigo.set(value);
    }
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    public void setDescricao(String value) {
        descricao.set(value);
    }
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Column(name = "INTERFERENCIA")
    public BigDecimal getInterferencia() {
        return interferencia.get();
    }
    public ObjectProperty<BigDecimal> interferenciaProperty() {
        return interferencia;
    }
    public void setInterferencia(BigDecimal interferencia) {
        this.interferencia.set(interferencia);
    }
    
    @Override
    public String toString() {
        return "[" + codorg.get() + "] " + descricao.get();
    }

}
