package sysdeliz2.models.sysdeliz;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_FECHAMENTO_DIARIO_REP_001")
public class SdFechamentoDiarioRep {
    
    private final ObjectProperty<SdFechamentoDiarioRepPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> faturamento = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> recebimento = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> devolucaotot = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> devolucaopar = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> lanccredito = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> lancdebito = new SimpleObjectProperty<BigDecimal>();
    private final BooleanProperty fechado = new SimpleBooleanProperty();
    
    public SdFechamentoDiarioRep() {
    }
    
    @EmbeddedId
    public SdFechamentoDiarioRepPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdFechamentoDiarioRepPK> idProperty() {
        return id;
    }
    
    public void setId(SdFechamentoDiarioRepPK id) {
        this.id.set(id);
    }
    
    @Column(name = "FATURAMENTO")
    public BigDecimal getFaturamento() {
        return faturamento.get();
    }
    
    public ObjectProperty<BigDecimal> faturamentoProperty() {
        return faturamento;
    }
    
    public void setFaturamento(BigDecimal faturamento) {
        this.faturamento.set(faturamento);
    }
    
    @Column(name = "RECEBIMENTO")
    public BigDecimal getRecebimento() {
        return recebimento.get();
    }
    
    public ObjectProperty<BigDecimal> recebimentoProperty() {
        return recebimento;
    }
    
    public void setRecebimento(BigDecimal recebimento) {
        this.recebimento.set(recebimento);
    }
    
    @Column(name = "DEVOLUCAO_TOT")
    public BigDecimal getDevolucaotot() {
        return devolucaotot.get();
    }
    
    public ObjectProperty<BigDecimal> devolucaototProperty() {
        return devolucaotot;
    }
    
    public void setDevolucaotot(BigDecimal devolucaotot) {
        this.devolucaotot.set(devolucaotot);
    }
    
    @Column(name = "DEVOLUCAO_PAR")
    public BigDecimal getDevolucaopar() {
        return devolucaopar.get();
    }
    
    public ObjectProperty<BigDecimal> devolucaoparProperty() {
        return devolucaopar;
    }
    
    public void setDevolucaopar(BigDecimal devolucaopar) {
        this.devolucaopar.set(devolucaopar);
    }
    
    @Column(name = "LANC_CREDITO")
    public BigDecimal getLanccredito() {
        return lanccredito.get();
    }
    
    public ObjectProperty<BigDecimal> lanccreditoProperty() {
        return lanccredito;
    }
    
    public void setLanccredito(BigDecimal lanccredito) {
        this.lanccredito.set(lanccredito);
    }
    
    @Column(name = "LANC_DEBITO")
    public BigDecimal getLancdebito() {
        return lancdebito.get();
    }
    
    public ObjectProperty<BigDecimal> lancdebitoProperty() {
        return lancdebito;
    }
    
    public void setLancdebito(BigDecimal lancdebito) {
        this.lancdebito.set(lancdebito);
    }
    
    @Column(name = "FECHADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isFechado() {
        return fechado.get();
    }
    
    public BooleanProperty fechadoProperty() {
        return fechado;
    }
    
    public void setFechado(Boolean fechado) {
        this.fechado.set(fechado);
    }
    
}
