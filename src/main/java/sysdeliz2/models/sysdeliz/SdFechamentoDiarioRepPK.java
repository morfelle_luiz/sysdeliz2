package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class SdFechamentoDiarioRepPK implements Serializable {
    
    private final StringProperty codrep = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtfecha = new SimpleObjectProperty<LocalDate>();
    
    public SdFechamentoDiarioRepPK() {
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "DT_FECHA")
    public LocalDate getDtfecha() {
        return dtfecha.get();
    }
    
    public ObjectProperty<LocalDate> dtfechaProperty() {
        return dtfecha;
    }
    
    public void setDtfecha(LocalDate dtfecha) {
        this.dtfecha.set(dtfecha);
    }
    
}
