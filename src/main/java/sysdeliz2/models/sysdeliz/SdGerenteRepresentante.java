package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_GERENTE_REPRESENTANTE_001")
public class SdGerenteRepresentante {
    
    private final ObjectProperty<SdGerenteRepresentantePK> id = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty();
    
    public SdGerenteRepresentante() {
    }
    
    @EmbeddedId
    public SdGerenteRepresentantePK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdGerenteRepresentantePK> idProperty() {
        return id;
    }
    
    public void setId(SdGerenteRepresentantePK id) {
        this.id.set(id);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
}
