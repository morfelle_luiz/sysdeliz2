package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdGerenteRepresentantePK implements Serializable {
    
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> codcli = new SimpleObjectProperty();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty();
    
    public SdGerenteRepresentantePK() {
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }
    
    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODCLI")
    public Entidade getCodcli() {
        return codcli.get();
    }
    
    public ObjectProperty<Entidade> codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Entidade codcli) {
        this.codcli.set(codcli);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
}
