package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_GRADE_ITEM_PED_REM_001")
public class SdGradeItemPedRem {
    
    private final ObjectProperty<SdGradeItemPedRemPK> id = new SimpleObjectProperty<>();
    private final StringProperty statusitem = new SimpleStringProperty("P");
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdec = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdel = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tipo = new SimpleStringProperty("COM");
    private final IntegerProperty qtdep = new SimpleIntegerProperty(0);
    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final IntegerProperty piso = new SimpleIntegerProperty(0);
    private final ObjectProperty<LocalDateTime> dhColeta = new SimpleObjectProperty<>();

    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> perDesc = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valDesc = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);

    public SdGradeItemPedRem() {
    }
    
    public SdGradeItemPedRem(Integer remessa, String numero, String codigo, String cor, String tam,
                             String statusitem, LocalDate dtentrega, Integer qtde, BigDecimal valor,
                             String tipo, Integer ordem, BigDecimal desconto) {
        this.id.set(new SdGradeItemPedRemPK(remessa, numero, codigo, cor, tam));
        this.statusitem.set(statusitem);
        this.dtentrega.set(dtentrega);
        this.qtde.set(qtde);
        this.valor.set(valor);
        this.tipo.set(tipo);
        this.ordem.set(ordem);
        this.desconto.set(desconto);
    }
    
    @EmbeddedId
    public SdGradeItemPedRemPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdGradeItemPedRemPK> idProperty() {
        return id;
    }
    
    public void setId(SdGradeItemPedRemPK id) {
        this.id.set(id);
    }
    
    @Column(name = "STATUS_ITEM")
    public String getStatusitem() {
        return statusitem.get();
    }
    
    public StringProperty statusitemProperty() {
        return statusitem;
    }
    
    public void setStatusitem(String statusitem) {
        this.statusitem.set(statusitem);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }
    
    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }
    
    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "QTDE_L")
    public Integer getQtdel() {
        return qtdel.get();
    }
    
    public IntegerProperty qtdelProperty() {
        return qtdel;
    }
    
    public void setQtdel(Integer qtdel) {
        this.qtdel.set(qtdel);
    }
    
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }
    
    public StringProperty barra28Property() {
        return barra28;
    }
    
    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "PER_DESC")
    public BigDecimal getPerDesc() {
        return perDesc.get();
    }

    public ObjectProperty<BigDecimal> perDescProperty() {
        return perDesc;
    }

    public void setPerDesc(BigDecimal perDesc) {
        this.perDesc.set(perDesc);
    }

    @Column(name = "VAL_DESC")
    public BigDecimal getValDesc() {
        return valDesc.get();
    }

    public ObjectProperty<BigDecimal> valDescProperty() {
        return valDesc;
    }

    public void setValDesc(BigDecimal valDesc) {
        this.valDesc.set(valDesc);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }

    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }

    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }

    @Column(name = "PISO")
    public int getPiso() {
        return piso.get();
    }

    public IntegerProperty pisoProperty() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso.set(piso);
    }

    @Column(name = "DH_COLETA")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhColeta() {
        return dhColeta.get();
    }

    public ObjectProperty<LocalDateTime> dhColetaProperty() {
        return dhColeta;
    }

    public void setDhColeta(LocalDateTime dhColeta) {
        this.dhColeta.set(dhColeta);
    }

    @Transient
    public Integer getQtdep() {
        return qtdep.get();
    }
    
    public IntegerProperty qtdepProperty() {
        return qtdep;
    }
    
    public void setQtdep(Integer qtdep) {
        this.qtdep.set(qtdep);
    }
    
    @PostLoad
    private void onPostLoad(){
        setQtdep(getQtde() - getQtdel());
    }
    
    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}