package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.RemessaAttributeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SdGradeItemPedRemPK implements Serializable {
    
    private final IntegerProperty remessa = new SimpleIntegerProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    
    public SdGradeItemPedRemPK() {
    
    }
    
    public SdGradeItemPedRemPK(Integer remessa, String numero, String codigo, String cor, String tam) {
        this.remessa.set(remessa);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.tam.set(tam);
    }
    
    @Column(name = "REMESSA")
    @Convert(converter = RemessaAttributeConverter.class)
    public Integer getRemessa() {
        return remessa.get();
    }
    
    public IntegerProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(Integer remessa) {
        this.remessa.set(remessa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
}