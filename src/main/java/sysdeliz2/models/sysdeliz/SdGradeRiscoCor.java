package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.IntegerAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_GRADE_RISCO_COR_001")
public class SdGradeRiscoCor implements Serializable {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final IntegerProperty idrisco = new SimpleIntegerProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty saldoOf = new SimpleIntegerProperty(0);
    private final IntegerProperty realizado = new SimpleIntegerProperty(0);
    private final IntegerProperty realizadorc = new SimpleIntegerProperty(0);
    private final IntegerProperty grade = new SimpleIntegerProperty(0);
    private final IntegerProperty parte = new SimpleIntegerProperty(0);
    private final IntegerProperty somatorioCor = new SimpleIntegerProperty(0);
    
    public SdGradeRiscoCor() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_GRADE_RISCO_COR")
    @SequenceGenerator(name = "SEQ_SD_GRADE_RISCO_COR", sequenceName = "SEQ_SD_GRADE_RISCO_COR", allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(int id) {
        this.id.set(id);
    }
    
    @Column(name = "ID_RISCO")
    public int getIdrisco() {
        return idrisco.get();
    }
    
    public IntegerProperty idriscoProperty() {
        return idrisco;
    }
    
    public void setIdrisco(int idrisco) {
        this.idrisco.set(idrisco);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name = "SALDO_OF", columnDefinition = "NUMBER")
    @Convert(converter = IntegerAttributeConverter.class)
    public Integer getSaldoOf() {
        return saldoOf.get();
    }
    
    public IntegerProperty saldoOfProperty() {
        return saldoOf;
    }
    
    public void setSaldoOf(Integer saldoOf) {
        this.saldoOf.set(saldoOf);
    }
    
    @Column(name = "REALIZADO")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getRealizado() {
        return realizado.get();
    }
    
    public IntegerProperty realizadoProperty() {
        return realizado;
    }
    
    public void setRealizado(int realizado) {
        this.realizado.set(realizado);
        this.setRealizadorc(realizado);
    }
    
    @Column(name = "GRADE")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getGrade() {
        return grade.get();
    }
    
    public IntegerProperty gradeProperty() {
        return grade;
    }
    
    public void setGrade(int grade) {
        this.grade.set(grade);
    }
    
    @Column(name = "PARTE")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getParte() {
        return parte.get();
    }
    
    public IntegerProperty parteProperty() {
        return parte;
    }
    
    public void setParte(int parte) {
        this.parte.set(parte);
    }
    
    @Column(name = "REALIZADO_RC")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getRealizadorc() {
        return realizadorc.get();
    }
    
    public IntegerProperty realizadorcProperty() {
        return realizadorc;
    }
    
    public void setRealizadorc(Integer realizadorc) {
            this.realizadorc.set(realizadorc);
    }
    
    @Transient
    public int getSomatorioCor() {
        return somatorioCor.get();
    }
    
    public IntegerProperty somatorioCorProperty() {
        return somatorioCor;
    }
    
    public void setSomatorioCor(int somatorioCor) {
        this.somatorioCor.set(somatorioCor);
    }
    
    @PostLoad
    private void onPostLoad() {
    }
}
