package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_GRUPO_MODELAGEM_001")
@TelaSysDeliz(descricao = "Grupo Modelagem", icon = "produto (4).png")
public class SdGrupoModelagem extends BasicModel implements Serializable {


    @ExibeTableView(descricao = "Código", width = 150)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    @Transient
    private final IntegerProperty codigo = new SimpleIntegerProperty();

    @ExibeTableView(descricao = "Grupo", width = 250)
    @ColunaFilter(descricao = "Grupo", coluna = "grupo")
    @Transient
    private final StringProperty grupo = new SimpleStringProperty();

    public SdGrupoModelagem() {
    }

    @Id
    @Column(name = "codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }


    @Column(name = "grupo")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        setDescricaoFilter(grupo);
        this.grupo.set(grupo);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + grupo.getValue();
    }
}
