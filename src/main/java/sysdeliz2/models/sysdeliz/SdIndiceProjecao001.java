package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeXConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_INDICE_PROJECAO_001")
public class SdIndiceProjecao001 {
    
    
    private final ObjectProperty<SdIndiceProjecao001PK> id = new SimpleObjectProperty<>();
    private final BooleanProperty byMarca = new SimpleBooleanProperty();
    private final BooleanProperty byLinha = new SimpleBooleanProperty();
    private final ObjectProperty<BigDecimal> byFixo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> byMeta = new SimpleObjectProperty<>();
    private final StringProperty linhaMeta = new SimpleStringProperty();
    
    public SdIndiceProjecao001() {
    }
    
    public SdIndiceProjecao001(SdIndiceProjecao001PK id, Boolean byMarca, Boolean byLinha, BigDecimal byFixo, BigDecimal byMeta, String linhaMeta){
        this.id.set(id);
        this.byMarca.set(byMarca);
        this.byLinha.set(byLinha);
        this.byFixo.set(byFixo);
        this.byMeta.set(byMeta);
        this.linhaMeta.set(linhaMeta);
    }
    
    @EmbeddedId
    public SdIndiceProjecao001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdIndiceProjecao001PK> idProperty() {
        return id;
    }
    
    public void setId(SdIndiceProjecao001PK id) {
        this.id.set(id);
    }
    
    @Column(name = "BYMARCA", columnDefinition = "VARCHAR2")
    @Convert(converter = BooleanAttributeXConverter.class)
    public boolean isByMarca() {
        return byMarca.get();
    }
    
    public BooleanProperty byMarcaProperty() {
        return byMarca;
    }
    
    public void setByMarca(boolean byMarca) {
        this.byMarca.set(byMarca);
    }
    
    @Column(name = "BYLINHA", columnDefinition = "VARCHAR2")
    @Convert(converter = BooleanAttributeXConverter.class)
    public boolean isByLinha() {
        return byLinha.get();
    }
    
    public BooleanProperty byLinhaProperty() {
        return byLinha;
    }
    
    public void setByLinha(boolean byLinha) {
        this.byLinha.set(byLinha);
    }
    
    @Column(name = "BYFIXO")
    public BigDecimal getByFixo() {
        return byFixo.get();
    }
    
    public ObjectProperty<BigDecimal> byFixoProperty() {
        return byFixo;
    }
    
    public void setByFixo(BigDecimal byFixo) {
        this.byFixo.set(byFixo);
    }
    
    @Column(name = "BYMETA")
    public BigDecimal getByMeta() {
        return byMeta.get();
    }
    
    public ObjectProperty<BigDecimal> byMetaProperty() {
        return byMeta;
    }
    
    public void setByMeta(BigDecimal byMeta) {
        this.byMeta.set(byMeta);
    }
    
    @Column(name = "LINHAMETA")
    public String getLinhaMeta() {
        return linhaMeta.get();
    }
    
    public StringProperty linhaMetaProperty() {
        return linhaMeta;
    }
    
    public void setLinhaMeta(String linhaMeta) {
        this.linhaMeta.set(linhaMeta);
    }
}