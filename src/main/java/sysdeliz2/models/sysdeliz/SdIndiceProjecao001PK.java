package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdIndiceProjecao001PK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final StringProperty descMarca = new SimpleStringProperty();
    
    public SdIndiceProjecao001PK() {
    }
    
    public SdIndiceProjecao001PK(Colecao colecao, Marca marca) {
        this.colecao.set(colecao);
        this.marca.set(marca);
        this.descMarca.set(marca.getDescricao());
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COD_MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "MARCA")
    public String getDescMarca() {
        return descMarca.get();
    }
    
    public StringProperty descMarcaProperty() {
        return descMarca;
    }
    
    public void setDescMarca(String descMarca) {
        this.descMarca.set(descMarca);
    }
    
    @Override
    public String toString() {
        return colecao.get() + "-" + marca.get();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
