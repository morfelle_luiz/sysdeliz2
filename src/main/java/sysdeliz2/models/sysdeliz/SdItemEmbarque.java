package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdDadosProdutoImportacao;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SD_ITEM_EMBARQUE_001")
@TelaSysDeliz(descricao = "Entidade", icon = "cliente (4).png")
public class SdItemEmbarque extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "id")
    private final IntegerProperty id = new SimpleIntegerProperty();

    @ExibeTableView(descricao = "Cod Embarque", width = 100)
    @ColunaFilter(descricao = "Cod Embarque", coluna = "codigoEmbarque")
    private final ObjectProperty<SdEmbarque> codigoEmbarque = new SimpleObjectProperty<>();

    @ExibeTableView(descricao = "Cod Produto", width = 100)
    @ColunaFilter(descricao = "Cod Produto", coluna = "produto")
    private final ObjectProperty<VSdDadosProdutoImportacao> produto = new SimpleObjectProperty<>();

    @ExibeTableView(descricao = "Nota", width = 100)
    @ColunaFilter(descricao = "Nota", coluna = "numero")
    private final StringProperty numero = new SimpleStringProperty();

    private final ObjectProperty<BigDecimal> pesoTotal = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pesoUnidade = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pesoOf = new SimpleObjectProperty<>();

    private final StringProperty unidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> precoUnidade = new SimpleObjectProperty<BigDecimal>();

    private final StringProperty unidadeConv = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdeConv = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> precoUnidadeConv = new SimpleObjectProperty<BigDecimal>();

    private final ObjectProperty<BigDecimal> precoTotal = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty ncm = new SimpleStringProperty();

    private final StringProperty chaveNfe = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();

    private List<SdItemGradeEmbarque> listGrade = new ArrayList<>();

    private Material material = new Material();

    public SdItemEmbarque() {
    }

    public SdItemEmbarque(SdEmbarque embarque, VSdDadosOfPendente of, VSdDadosProdutoImportacao produtoImportacao, int qtde) {
        this.codigoEmbarque.set(embarque);
        this.produto.set(produtoImportacao);
        this.numero.set(of.getId().getNumero());
        this.qtde.set(new BigDecimal(qtde));
        this.precoUnidade.set(produtoImportacao.getPrecodolar());
        this.precoTotal.set(this.getPrecoUnidade().multiply(this.getQtde()));
        this.unidade.set(produtoImportacao.getUnidade());
        this.ncm.set(produtoImportacao.getCodfis().getDescricao());
        this.descricao.set(produtoImportacao.getDescricao());
    }

    public SdItemEmbarque( VSdDadosOfPendente of, VSdDadosProdutoImportacao produtoImportacao, int qtde) {
        this.produto.set(produtoImportacao);
        this.numero.set(of.getId().getNumero());
        this.qtde.set(new BigDecimal(qtde));
        this.precoUnidade.set(produtoImportacao.getPrecodolar());
        this.precoTotal.set(this.getPrecoUnidade().multiply(this.getQtde()));
        this.unidade.set(produtoImportacao.getUnidade());
        this.ncm.set(produtoImportacao.getCodfis().getDescricao());
        this.descricao.set(produtoImportacao.getDescricao());
    }

//    public SdItemEmbarque(SdEmbarque embarque, NotaFiscalXML.Det det, NotaFiscalXML nota) {
//        this.codigoEmbarque.set(embarque);
//        this.codProduto.set(det.getProd().getcProd());
//        this.numero.set(nota.getNFe().getInfNFe().getIde().getnNF());
//        this.qtde.set(new BigDecimal(det.getProd().getqCom()));
//        this.precoUnidade.set(new BigDecimal(det.getProd().getvUnCom()));
//        this.unidade.set(det.getProd().getuCom());
//        this.qtdeConv.set(new BigDecimal(det.getProd().getqTrib()));
//        this.precoUnidadeConv.set(new BigDecimal(det.getProd().getvUnTrib()));
//        this.unidadeConv.set(det.getProd().getuTrib());
//        this.precoTotal.set(new BigDecimal(det.getProd().getvProd()));
//        this.chaveNfe.set(nota.getNFe().getInfNFe().getId().replace("NFe", ""));
//        this.ordem.set(Integer.parseInt(det.getnItem()));
//        this.ncm.set(getNcm(det.getProd().getcProd()));
//        this.descricao.set(det.getProd().getxProd().replace(" - 0-ZERO", ""));
//    }

    private String getNcm(String getcProd) {
        try {
            List<Object> objects = new NativeDAO().runNativeQuery("SELECT TAB.DESCRICAO FROM MATERIAL_001 MAT JOIN TABFIS_001 TAB ON TAB.CODIGO = MAT.CODFIS WHERE MAT.CODIGO = '" + getcProd + "'");
            return (String) ((Map<String, Object>) objects.get(0)).get("DESCRICAO");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_ITEM_EMBARQUE")
    @SequenceGenerator(name = "SEQ_SD_ITEM_EMBARQUE", sequenceName = "SEQ_SD_ITEM_EMBARQUE", allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "CODIGO_EMBARQUE")
    public SdEmbarque getCodigoEmbarque() {
        return codigoEmbarque.get();
    }

    public ObjectProperty<SdEmbarque> codigoEmbarqueProperty() {
        return codigoEmbarque;
    }

    public void setCodigoEmbarque(SdEmbarque codigoEmbarque) {
        this.codigoEmbarque.set(codigoEmbarque);
    }

    @OneToOne
    @JoinColumn(name = "PRODUTO")
    public VSdDadosProdutoImportacao getProduto() {
        return produto.get();
    }

    public ObjectProperty<VSdDadosProdutoImportacao> produtoProperty() {
        return produto;
    }

    public void setProduto(VSdDadosProdutoImportacao produto) {
        this.produto.set(produto);
    }

    @Column(name = "PRECO_TOTAL")
    public BigDecimal getPrecoTotal() {
        return precoTotal.get();
    }

    public ObjectProperty<BigDecimal> precoTotalProperty() {
        return precoTotal;
    }

    public void setPrecoTotal(BigDecimal precoTotal) {
        this.precoTotal.set(precoTotal);
    }

    @Column(name = "PRECO_UN")
    public BigDecimal getPrecoUnidade() {
        return precoUnidade.get();
    }

    public ObjectProperty<BigDecimal> precoUnidadeProperty() {
        return precoUnidade;
    }

    public void setPrecoUnidade(BigDecimal precoUnidade) {
        this.precoUnidade.set(precoUnidade);
    }

    @Column(name = "PESO")
    public BigDecimal getPesoTotal() {
        return pesoTotal.get();
    }

    public ObjectProperty<BigDecimal> pesoTotalProperty() {
        return pesoTotal;
    }

    public void setPesoTotal(BigDecimal pesoTotal) {
        this.pesoTotal.set(pesoTotal);
    }

    @Column(name = "PESO_UN")
    public BigDecimal getPesoUnidade() {
        return pesoUnidade.get();
    }

    public ObjectProperty<BigDecimal> pesoUnidadeProperty() {
        return pesoUnidade;
    }

    public void setPesoUnidade(BigDecimal pesoUnidade) {
        this.pesoUnidade.set(pesoUnidade);
    }

    @Column(name = "PESO_OF")
    public BigDecimal getPesoOf() {
        return pesoOf.get();
    }

    public ObjectProperty<BigDecimal> pesoOfProperty() {
        return pesoOf;
    }

    public void setPesoOf(BigDecimal pesoOf) {
        this.pesoOf.set(pesoOf);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "COD_ITEM")
    public List<SdItemGradeEmbarque> getListGrade() {
        return listGrade;
    }

    public void setListGrade(List<SdItemGradeEmbarque> listGrade) {
        this.listGrade = listGrade;
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }

    @Column(name = "UNIDADE_CONV")
    public String getUnidadeConv() {
        return unidadeConv.get();
    }

    public StringProperty unidadeConvProperty() {
        return unidadeConv;
    }

    public void setUnidadeConv(String unidadeConv) {
        this.unidadeConv.set(unidadeConv);
    }

    @Column(name = "QTDE_CONV")
    public BigDecimal getQtdeConv() {
        return qtdeConv.get();
    }

    public ObjectProperty<BigDecimal> qtdeConvProperty() {
        return qtdeConv;
    }

    public void setQtdeConv(BigDecimal qtdeConv) {
        this.qtdeConv.set(qtdeConv);
    }

    @Column(name = "PRECO_UN_CONV")
    public BigDecimal getPrecoUnidadeConv() {
        return precoUnidadeConv.get();
    }

    public ObjectProperty<BigDecimal> precoUnidadeConvProperty() {
        return precoUnidadeConv;
    }

    public void setPrecoUnidadeConv(BigDecimal precoUnidadeConv) {
        this.precoUnidadeConv.set(precoUnidadeConv);
    }

    @Column(name = "CHAVE_NFE")
    public String getChaveNfe() {
        return chaveNfe.get();
    }

    public StringProperty chaveNfeProperty() {
        return chaveNfe;
    }

    public void setChaveNfe(String chaveNfe) {
        this.chaveNfe.set(chaveNfe);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "NCM")
    public String getNcm() {
        return ncm.get();
    }

    public StringProperty ncmProperty() {
        return ncm;
    }

    public void setNcm(String ncm) {
        this.ncm.set(ncm);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }


    @Transient
    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}

