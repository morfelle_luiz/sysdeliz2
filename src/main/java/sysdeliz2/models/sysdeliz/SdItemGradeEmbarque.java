package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdDadosOfPendenteSKU;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_ITEM_GRADE_EMBARQUE_001")
public class SdItemGradeEmbarque extends BasicModel {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<SdItemEmbarque> codItem = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pesoLiquido = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoBruto = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoUnidade = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tamanho = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty volumes = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> cubagem = new SimpleObjectProperty<BigDecimal>(new BigDecimal("0.1"));

    public SdItemGradeEmbarque() {
    }

    public SdItemGradeEmbarque(VSdDadosOfPendenteSKU ofSku, SdItemEmbarque itemEmbarque, int qtde) {
        this.codItem.set(itemEmbarque);
        this.tamanho.set(ofSku.getId().getTam());
        this.qtde.set(qtde);
        this.volumes.set(ofSku.getQtde() % 20 != 0 ? ofSku.getQtde() / 20 + 1 : ofSku.getQtde() / 20);
    }

    public SdItemGradeEmbarque(VSdDadosOfPendenteSKU ofSku, SdItemEmbarque itemEmbarque) {
        this.codItem.set(itemEmbarque);
        this.tamanho.set(ofSku.getId().getTam());
        this.qtde.set(ofSku.getQtde());
        this.volumes.set(ofSku.getQtde() % 20 != 0 ? ofSku.getQtde() / 20 + 1 : ofSku.getQtde() / 20);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_ITEM_GRADE_EMBARQUE")
    @SequenceGenerator(name = "SEQ_SD_ITEM_GRADE_EMBARQUE", sequenceName = "SEQ_SD_ITEM_GRADE_EMBARQUE", allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "COD_ITEM")
    public SdItemEmbarque getCodItem() {
        return codItem.get();
    }

    public ObjectProperty<SdItemEmbarque> codItemProperty() {
        return codItem;
    }

    public void setCodItem(SdItemEmbarque codItem) {
        this.codItem.set(codItem);
    }

    @Column(name = "PESO_LIQUIDO")
    public BigDecimal getPesoLiquido() {
        return pesoLiquido.get();
    }

    public ObjectProperty<BigDecimal> pesoLiquidoProperty() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido.set(pesoLiquido);
    }

    @Column(name = "PESO_BRUTO")
    public BigDecimal getPesoBruto() {
        return pesoBruto.get();
    }

    public ObjectProperty<BigDecimal> pesoBrutoProperty() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto.set(pesoBruto);
    }

    @Column(name = "PESO_UNIDADE")
    public BigDecimal getPesoUnidade() {
        return pesoUnidade.get();
    }

    public ObjectProperty<BigDecimal> pesoUnidadeProperty() {
        return pesoUnidade;
    }

    public void setPesoUnidade(BigDecimal pesoUnidade) {
        this.pesoUnidade.set(pesoUnidade);
    }

    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "VOLUMES")
    public int getVolumes() {
        return volumes.get();
    }

    public IntegerProperty volumesProperty() {
        return volumes;
    }

    public void setVolumes(int volumes) {
        this.volumes.set(volumes);
    }

    @Column(name = "CUBAGEM")
    public BigDecimal getCubagem() {
        return cubagem.get();
    }

    public ObjectProperty<BigDecimal> cubagemProperty() {
        return cubagem;
    }

    public void setCubagem(BigDecimal cubagem) {
        this.cubagem.set(cubagem);
    }

    @Override
    public String toString() {
        return
                " \n\n Tamanho:  " + tamanho.get() +
                        " \npesoLiquido = " + pesoLiquido.get() +
                        " \n pesoBruto = " + pesoBruto.get() +
                        " \n pesoUnidade = " + pesoUnidade.get()
                ;
    }
}

