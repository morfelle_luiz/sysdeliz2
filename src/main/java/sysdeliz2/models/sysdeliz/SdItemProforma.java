package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.view.VSdDadosProdutoImportacao;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_ITEM_PROFORMA_001")
public class SdItemProforma {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<SdProforma> codProforma = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProdutoImportacao> codProduto = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> precoDolar = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoUnidade = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> precoUnidadeReal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> precoRealFinal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty pagina = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty volumes = new SimpleIntegerProperty();

    public SdItemProforma() {
    }

    public SdItemProforma(SdProforma proforma, SdItemEmbarque item, int ordem, BigDecimal dolar, BigDecimal valorItem, BigDecimal valorFinal) {
        this.setCodProduto(item.getProduto());
        this.setCodProforma(proforma);
        this.setPeso(item.getPesoTotal());
        this.setQtde(item.getQtde());
        this.setPrecoUnidadeReal(dolar.multiply(item.getPrecoUnidade()));
        this.setPrecoRealFinal(valorItem);
        this.setPreco(valorFinal);
        this.setPrecoDolar(item.getPrecoUnidade());
        this.setPesoUnidade(item.getPesoUnidade());
        this.setOrdem(ordem);
        this.volumes.set(item.getListGrade().stream().mapToInt(SdItemGradeEmbarque::getVolumes).sum());
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_ITEM_PROFORMA")
    @SequenceGenerator(name = "SEQ_SD_ITEM_PROFORMA", sequenceName = "SEQ_SD_ITEM_PROFORMA", allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "COD_PROFORMA")
    public SdProforma getCodProforma() {
        return codProforma.get();
    }

    public ObjectProperty<SdProforma> codProformaProperty() {
        return codProforma;
    }

    public void setCodProforma(SdProforma codProforma) {
        this.codProforma.set(codProforma);
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "COD_PRODUTO")
    public VSdDadosProdutoImportacao getCodProduto() {
        return codProduto.get();
    }

    public ObjectProperty<VSdDadosProdutoImportacao> codProdutoProperty() {
        return codProduto;
    }

    public void setCodProduto(VSdDadosProdutoImportacao codProduto) {
        this.codProduto.set(codProduto);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "PAGINA")
    public Integer getPagina() {
        return pagina.get();
    }

    public IntegerProperty paginaProperty() {
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina.set(pagina);
    }

    @Column(name = "PRECO_UN_DOLAR")
    public BigDecimal getPrecoDolar() {
        return precoDolar.get();
    }

    public ObjectProperty<BigDecimal> precoDolarProperty() {
        return precoDolar;
    }

    public void setPrecoDolar(BigDecimal precoDolar) {
        this.precoDolar.set(precoDolar);
    }

    @Column(name = "PESO_UN")
    public BigDecimal getPesoUnidade() {
        return pesoUnidade.get();
    }

    public ObjectProperty<BigDecimal> pesoUnidadeProperty() {
        return pesoUnidade;
    }

    public void setPesoUnidade(BigDecimal pesoUnidade) {
        this.pesoUnidade.set(pesoUnidade);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "PRECO_UN_REAL")
    public BigDecimal getPrecoUnidadeReal() {
        return precoUnidadeReal.get();
    }

    public ObjectProperty<BigDecimal> precoUnidadeRealProperty() {
        return precoUnidadeReal;
    }

    public void setPrecoUnidadeReal(BigDecimal precoUnidadeReal) {
        this.precoUnidadeReal.set(precoUnidadeReal);
    }

    @Column(name = "PRECO_FINAL_REAL")
    public BigDecimal getPrecoRealFinal() {
        return precoRealFinal.get();
    }

    public ObjectProperty<BigDecimal> precoRealFinalProperty() {
        return precoRealFinal;
    }

    public void setPrecoRealFinal(BigDecimal precoRealFinal) {
        this.precoRealFinal.set(precoRealFinal);
    }

    @Column(name = "VOLUMES")
    public int getVolumes() {
        return volumes.get();
    }

    public IntegerProperty volumesProperty() {
        return volumes;
    }

    public void setVolumes(int volumes) {
        this.volumes.set(volumes);
    }
}
