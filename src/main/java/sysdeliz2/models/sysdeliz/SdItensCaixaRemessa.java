package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.*;

@Entity
@Table(name = "SD_ITENS_CAIXA_REMESSA_001")
public class SdItensCaixaRemessa {
    
    private final ObjectProperty<SdItensCaixaRemessaPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    
    public SdItensCaixaRemessa() {
    }
    
    public SdItensCaixaRemessa(SdCaixaRemessa caixa, String barra, VSdDadosProduto codigo, String cor, String tam, String status, String pedido) {
        this.id.set(new SdItensCaixaRemessaPK(caixa, barra));
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.tam.set(tam);
        this.status.set(status);
        this.pedido.set(pedido);
    }
    
    @EmbeddedId
    public SdItensCaixaRemessaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdItensCaixaRemessaPK> idProperty() {
        return id;
    }
    
    public void setId(SdItensCaixaRemessaPK id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
}