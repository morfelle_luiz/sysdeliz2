package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdItensCaixaRemessaPK implements Serializable {
    
    private final ObjectProperty<SdCaixaRemessa> caixa = new SimpleObjectProperty<>();
    private final StringProperty barra = new SimpleStringProperty();
    
    public SdItensCaixaRemessaPK() {
    }
    
    public SdItensCaixaRemessaPK(SdCaixaRemessa caixa, String barra) {
        this.caixa.set(caixa);
        this.barra.set(barra);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    public SdCaixaRemessa getCaixa() {
        return caixa.get();
    }
    
    public ObjectProperty<SdCaixaRemessa> caixaProperty() {
        return caixa;
    }
    
    public void setCaixa(SdCaixaRemessa caixa) {
        this.caixa.set(caixa);
    }
    
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }
    
    public StringProperty barraProperty() {
        return barra;
    }
    
    public void setBarra(String barra) {
        this.barra.set(barra);
    }
}