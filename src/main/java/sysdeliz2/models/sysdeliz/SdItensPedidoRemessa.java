package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_ITENS_PEDIDO_REMESSA_001")
public class SdItensPedidoRemessa {
    
    private final ObjectProperty<SdItensPedidoRemessaPK> id = new SimpleObjectProperty<>();
    private final StringProperty statusitem = new SimpleStringProperty("P");
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdec = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty observacao = new SimpleStringProperty();
    private List<SdGradeItemPedRem> grade = new ArrayList<>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty piso = new SimpleIntegerProperty(0);

    public SdItensPedidoRemessa() {
    }
    
    public SdItensPedidoRemessa(Integer remessa, String numero, VSdDadosProduto codigo, String cor,
                                String statusitem, LocalDate dtentrega, Integer qtde, BigDecimal valor,
                                String tipo, Integer ordem) {
        this.id.set(new SdItensPedidoRemessaPK(remessa, numero, codigo, cor));
        this.statusitem.set(statusitem);
        this.dtentrega.set(dtentrega);
        this.qtde.set(qtde);
        this.valor.set(valor);
        this.tipo.set(tipo);
        this.ordem.set(ordem);
    }
    
    @EmbeddedId
    public SdItensPedidoRemessaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdItensPedidoRemessaPK> idProperty() {
        return id;
    }
    
    public void setId(SdItensPedidoRemessaPK id) {
        this.id.set(id);
    }
    
    @Column(name = "STATUS_ITEM")
    public String getStatusitem() {
        return statusitem.get();
    }
    
    public StringProperty statusitemProperty() {
        return statusitem;
    }
    
    public void setStatusitem(String statusitem) {
        this.statusitem.set(statusitem);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "REMESSA", referencedColumnName = "REMESSA"),
            @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO"),
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR")
    })
    @OrderBy("ordem ASC")
    public List<SdGradeItemPedRem> getGrade() {
        return grade;
    }
    
    public void setGrade(List<SdGradeItemPedRem> grade) {
        this.grade = grade;
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "PISO")
    public int getPiso() {
        return piso.get();
    }

    public IntegerProperty pisoProperty() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso.set(piso);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
    
}