package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.RemessaAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdItensPedidoRemessaPK implements Serializable {
    
    private final IntegerProperty remessa = new SimpleIntegerProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty();
    private final StringProperty cor = new SimpleStringProperty();
    
    public SdItensPedidoRemessaPK() {
    }
    
    public SdItensPedidoRemessaPK(Integer remessa, String numero, VSdDadosProduto codigo, String cor) {
        this.remessa.set(remessa);
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.cor.set(cor);
    }
    
    @Column(name = "REMESSA")
    @Convert(converter = RemessaAttributeConverter.class)
    public Integer getRemessa() {
        return remessa.get();
    }
    
    public IntegerProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(Integer remessa) {
        this.remessa.set(remessa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
}