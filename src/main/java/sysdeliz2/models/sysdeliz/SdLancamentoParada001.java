package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="SD_LANCAMENTO_PARADA_001")
public class SdLancamentoParada001 implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final ObjectProperty<SdLancamentoParada001PK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<>();
    
    public SdLancamentoParada001() {
    }
    
    public SdLancamentoParada001(SdLancamentoParada001PK id){
        this.id.set(id);
    }
    
    public SdLancamentoParada001(SdLancamentoParada001PK id, LocalDateTime dhFim, BigDecimal tempo){
        this.id.set(id);
        this.dhFim.set(dhFim);
        this.tempo.set(tempo);
    }
    
    @EmbeddedId
    public SdLancamentoParada001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdLancamentoParada001PK> idProperty() {
        return id;
    }
    
    public void setId(SdLancamentoParada001PK id) {
        this.id.set(id);
    }
    
    @Column(name="DH_FIM", columnDefinition = "DATE")
    @Convert(converter= LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.dhFim.set(dhFim);
    }
    
    @Column(name="TEMPO")
    public BigDecimal getTempo() {
        return tempo.get();
    }
    
    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }
    
    public void setTempo(BigDecimal tempo) {
        this.tempo.set(tempo);
    }
}
