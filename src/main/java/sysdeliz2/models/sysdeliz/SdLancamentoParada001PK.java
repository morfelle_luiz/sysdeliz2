package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Embeddable
public class SdLancamentoParada001PK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final ObjectProperty<SdTiposParada001> motivo = new SimpleObjectProperty();
    private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<>();
    
    public SdLancamentoParada001PK() {
    }
    
    public SdLancamentoParada001PK(Integer colaborador) {
        this.colaborador.set(colaborador);
    }
    
    public SdLancamentoParada001PK(SdTiposParada001 motivo) {
        this.motivo.set(motivo);
    }
    
    public SdLancamentoParada001PK(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    public SdLancamentoParada001PK(Integer colaborador, SdTiposParada001 motivo, LocalDateTime dhInicio) {
        this.colaborador.set(colaborador);
        this.motivo.set(motivo);
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name="COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="MOTIVO")
    public SdTiposParada001 getMotivo() {
        return motivo.get();
    }
    
    public ObjectProperty<SdTiposParada001> motivoProperty() {
        return motivo;
    }
    
    public void setMotivo(SdTiposParada001 motivo) {
        this.motivo.set(motivo);
    }
    
    @Column(name="DH_INICIO", columnDefinition = "DATE")
    @Convert(converter= LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdLancamentoParada001PK that = (SdLancamentoParada001PK) o;
        return colaborador.equals(that.colaborador) &&
                motivo.equals(that.motivo) &&
                dhInicio.equals(that.dhInicio);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(colaborador, motivo, dhInicio);
    }
}
