package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="SD_LANCAMENTO_PRODUCAO_001")
public class SdLancamentoProducao001 implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty lancAgrup = new SimpleStringProperty();
    private final StringProperty pacoteProg = new SimpleStringProperty();
    private final IntegerProperty operacao = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacaoOrganize = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final IntegerProperty pacoteLan = new SimpleIntegerProperty();
    private final ObjectProperty<SdPacoteLancamento001> pctLancamento = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoOper = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoProd = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> eficiencia = new SimpleObjectProperty<>();
    private final IntegerProperty celula = new SimpleIntegerProperty();
    
    
    public SdLancamentoProducao001() {
    }
    
    public SdLancamentoProducao001(String pacoteProg, Integer operacao, Integer ordem, Integer quebra, Integer colaborador, Integer pacoteLan,
            BigDecimal tempoOper, Integer qtde, LocalDateTime dhInicio, LocalDateTime dhFim, BigDecimal tempoProd,
                                   Integer celula, BigDecimal eficiencia, String lancAgrup) {
        this.pacoteProg.set(pacoteProg);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.quebra.set(quebra);
        this.colaborador.set(colaborador);
        this.pacoteLan.set(pacoteLan);
        this.tempoOper.set(tempoOper);
        this.qtde.set(qtde);
        this.dhInicio.set(dhInicio);
        this.dhFim.set(dhFim);
        this.tempoProd.set(tempoProd);
        this.celula.set(celula);
        this.eficiencia.set(eficiencia);
        this.lancAgrup.set(lancAgrup);
        this.pctLancamento.set(new SdPacoteLancamento001(pacoteProg, pacoteLan));
        this.operacaoOrganize.set(new SdOperacaoOrganize001(operacao));
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_lancamento_producao")
    @SequenceGenerator(name="seq_lancamento_producao", sequenceName="seq_sd_lancamento_producao", allocationSize = 1)
    @Column(name="SEQ_LANC")
    public Integer getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id.set(id);
    }
    
    //@Column(name="PACOTE_PROG")
    @Transient
    public String getPacoteProg() {
        return pacoteProg.get();
    }
    
    public StringProperty pacoteProgProperty() {
        return pacoteProg;
    }
    
    public void setPacoteProg(String pacoteProg) {
        this.pacoteProg.set(pacoteProg);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public SdOperacaoOrganize001 getOperacaoOrganize() {
        return operacaoOrganize.get();
    }
    
    public ObjectProperty<SdOperacaoOrganize001> operacaoOrganizeProperty() {
        return operacaoOrganize;
    }
    
    public void setOperacaoOrganize(SdOperacaoOrganize001 operacaoOrganize) {
        this.operacao.set(operacaoOrganize.getCodorg());
        this.operacaoOrganize.set(operacaoOrganize);
    }
    
    //@Column(name="OPERACAO")
    @Transient
    public int getOperacao() {
        return operacao.get();
    }
    
    public IntegerProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(int operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name="QUEBRA")
    public int getQuebra() {
        return quebra.get();
    }
    
    public IntegerProperty quebraProperty() {
        return quebra;
    }
    
    public void setQuebra(int quebra) {
        this.quebra.set(quebra);
    }
    
    @Column(name="COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "PACOTE_PROG"),
            @JoinColumn(name = "PACOTE_LANC")
    })
    public SdPacoteLancamento001 getPctLancamento() {
        return pctLancamento.get();
    }
    
    public ObjectProperty<SdPacoteLancamento001> pctLancamentoProperty() {
        return pctLancamento;
    }
    
    public void setPctLancamento(SdPacoteLancamento001 pctLancamento) {
        pacoteLan.set(pctLancamento.getId().getOrdem());
        pacoteProg.set(pctLancamento.getId().getCodigo());
        this.pctLancamento.set(pctLancamento);
    }
    
    //@Column(name="PACOTE_LANC")
    @Transient
    public int getPacoteLan() {
        return pacoteLan.get();
    }
    
    public IntegerProperty pacoteLanProperty() {
        return pacoteLan;
    }
    
    public void setPacoteLan(int pacoteLan) {
        this.pacoteLan.set(pacoteLan);
    }
    
    @Column(name="TEMPO_OPER")
    public BigDecimal getTempoOper() {
        return tempoOper.get();
    }
    
    public ObjectProperty<BigDecimal> tempoOperProperty() {
        return tempoOper;
    }
    
    public void setTempoOper(BigDecimal tempoOper) {
        this.tempoOper.set(tempoOper);
    }
    
    @Column(name="QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name="DH_INICIO", columnDefinition="DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name="DH_FIM", columnDefinition="DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.dhFim.set(dhFim);
    }
    
    @Column(name="TEMPO_PROD")
    public BigDecimal getTempoProd() {
        return tempoProd.get();
    }
    
    public ObjectProperty<BigDecimal> tempoProdProperty() {
        return tempoProd;
    }
    
    public void setTempoProd(BigDecimal tempoProd) {
        this.tempoProd.set(tempoProd);
    }
    
    @Column(name="CELULA")
    public int getCelula() {
        return celula.get();
    }
    
    public IntegerProperty celulaProperty() {
        return celula;
    }
    
    public void setCelula(int celula) {
        this.celula.set(celula);
    }
    
    @Column(name="EFICIENCIA")
    public BigDecimal getEficiencia() {
        return eficiencia.get();
    }
    
    public ObjectProperty<BigDecimal> eficienciaProperty() {
        return eficiencia;
    }
    
    public void setEficiencia(BigDecimal eficiencia) {
        this.eficiencia.set(eficiencia);
    }
    
    @Column(name = "LANC_AGRUP")
    public String getLancAgrup() {
        return lancAgrup.get();
    }
    
    public StringProperty lancAgrupProperty() {
        return lancAgrup;
    }
    
    public void setLancAgrup(String lancAgrup) {
        this.lancAgrup.set(lancAgrup);
    }
}
