package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Deprecated
@Embeddable
public class SdLancamentoProducao001PK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty seqLanc = new SimpleIntegerProperty();
    private final StringProperty pacoteProg = new SimpleStringProperty();
    private final IntegerProperty operacao = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final IntegerProperty pacoteLan = new SimpleIntegerProperty();
    
    public SdLancamentoProducao001PK() {
    }
    
    public SdLancamentoProducao001PK(String pacoteProg, Integer operacao, Integer ordem, Integer quebra, Integer colaborador, Integer pacoteLan) {
        this.pacoteProg.set(pacoteProg);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.quebra.set(quebra);
        this.colaborador.set(colaborador);
        this.pacoteLan.set(pacoteLan);
    }
    
    @Column(name="SEQ_LANC")
    public int getSeqLanc() {
        return seqLanc.get();
    }
    
    public IntegerProperty seqLancProperty() {
        return seqLanc;
    }
    
    public void setSeqLanc(int seqLanc) {
        this.seqLanc.set(seqLanc);
    }
    
    @Column(name="PACOTE_PROG")
    public String getPacoteProg() {
        return pacoteProg.get();
    }
    
    public StringProperty pacoteProgProperty() {
        return pacoteProg;
    }
    
    public void setPacoteProg(String pacoteProg) {
        this.pacoteProg.set(pacoteProg);
    }
    
    @Column(name="OPERACAO")
    public int getOperacao() {
        return operacao.get();
    }
    
    public IntegerProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(int operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name="QUEBRA")
    public int getQuebra() {
        return quebra.get();
    }
    
    public IntegerProperty quebraProperty() {
        return quebra;
    }
    
    public void setQuebra(int quebra) {
        this.quebra.set(quebra);
    }
    
    @Column(name="COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name="PACOTE_LANC")
    public int getPacoteLan() {
        return pacoteLan.get();
    }
    
    public IntegerProperty pacoteLanProperty() {
        return pacoteLan;
    }
    
    public void setPacoteLan(int pacoteLan) {
        this.pacoteLan.set(pacoteLan);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdLancamentoProducao001PK that = (SdLancamentoProducao001PK) o;
        return pacoteProg.equals(that.pacoteProg) &&
                operacao.equals(that.operacao) &&
                ordem.equals(that.ordem) &&
                colaborador.equals(that.colaborador) &&
                pacoteLan.equals(that.pacoteLan);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(pacoteProg, operacao, ordem, colaborador, pacoteLan);
    }
}
