package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_LOCAL_EXPEDICAO_001")
public class SdLocalExpedicao implements Serializable {
    private final ObjectProperty<SdRuaExpedicao> rua = new SimpleObjectProperty<>();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty posicao = new SimpleStringProperty();
    private final StringProperty lado = new SimpleStringProperty();
    private final IntegerProperty andar = new SimpleIntegerProperty();
    private final ObjectProperty<VSdDadosProduto> produto = new SimpleObjectProperty<>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();

    public SdLocalExpedicao() {
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "PISO", referencedColumnName = "PISO"),
            @JoinColumn(name = "RUA", referencedColumnName = "RUA")
    })
    public SdRuaExpedicao getRua() {
        return rua.get();
    }

    public ObjectProperty<SdRuaExpedicao> ruaProperty() {
        return rua;
    }

    public void setRua(SdRuaExpedicao rua) {
        this.rua.set(rua);
    }

    @Id
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "POSICAO")
    public String getPosicao() {
        return posicao.get();
    }

    public StringProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao.set(posicao);
    }

    @Column(name = "LADO")
    public String getLado() {
        return lado.get();
    }

    public StringProperty ladoProperty() {
        return lado;
    }

    public void setLado(String lado) {
        this.lado.set(lado);
    }

    @Column(name = "ANDAR")
    public int getAndar() {
        return andar.get();
    }

    public IntegerProperty andarProperty() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar.set(andar);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getProduto() {
        return produto.get();
    }

    public ObjectProperty<VSdDadosProduto> produtoProperty() {
        return produto;
    }

    public void setProduto(VSdDadosProduto produto) {
        this.produto.set(produto);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Id
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }
}
