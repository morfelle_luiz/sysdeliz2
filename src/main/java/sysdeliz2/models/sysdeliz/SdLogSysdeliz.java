package sysdeliz2.models.sysdeliz;


import java.io.Serializable;
import java.util.Date;

/**
 * @author lima.joao
 * @since 22/08/2019 09:34
 */
public class SdLogSysdeliz implements Serializable {
    private Integer seq;

    private Date dataLog;

    private String usuario;

    private String tela;

    private String acao;

    private String referencia;

    private String descricao;

    public SdLogSysdeliz() {
    }

    public SdLogSysdeliz(Date dataLog, String usuario, String tela, String acao, String referencia, String descricao) {
        this.dataLog = dataLog;
        this.usuario = usuario;
        this.tela = tela;
        this.acao = acao;
        this.referencia = referencia;
        this.descricao = descricao;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Date getDataLog() {
        return dataLog;
    }

    public void setDataLog(Date dataLog) {
        this.dataLog = dataLog;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTela() {
        return tela;
    }

    public void setTela(String tela) {
        this.tela = tela;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String buildNativeInsert(){
        return "INSERT INTO SD_LOG_SYSDELIZ_001(DATA_LOG, USUARIO, TELA, ACAO, REFERENCIA, DESCRICAO)" +
                "VALUES(SYSDATE,'" +
                this.usuario + "','" +
                this.tela + "','" +
                this.acao + "','" +
                this.referencia + "','" +
                this.descricao +
                "');";
    }
}
