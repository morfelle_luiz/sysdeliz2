package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;
import sysdeliz2.utils.enums.TipoAcao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_LOG_SYSDELIZ_001")
public class SdLogSysdeliz001 {
    
    private final IntegerProperty seq = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dataLog = new SimpleObjectProperty<>();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty tela = new SimpleStringProperty();
    private final StringProperty acao = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    
    public SdLogSysdeliz001() {
    }
    
    public SdLogSysdeliz001(LocalDateTime dataLog, String usuario, String tela, TipoAcao acao, String referencia, String descricao) {
        this.dataLog.set(dataLog);
        this.usuario.set(usuario);
        this.tela.set(tela);
        this.acao.set(acao.getValor());
        this.referencia.set(referencia);
        this.descricao.set(descricao);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_log_sysdeliz")
    @SequenceGenerator(name="seq_log_sysdeliz", sequenceName="sd_seq_log_sysdeliz", allocationSize = 1)
    @Column(name="SEQ")
    public int getSeq() {
        return seq.get();
    }
    
    public IntegerProperty seqProperty() {
        return seq;
    }
    
    public void setSeq(int seq) {
        this.seq.set(seq);
    }
    
    @Column(name="DATA_LOG")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDataLog() {
        return dataLog.get();
    }
    
    public ObjectProperty<LocalDateTime> dataLogProperty() {
        return dataLog;
    }
    
    public void setDataLog(LocalDateTime dataLog) {
        this.dataLog.set(dataLog);
    }
    
    @Column(name="USUARIO")
    public String getUsuario() {
        return usuario.get();
    }
    
    public StringProperty usuarioProperty() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }
    
    @Column(name="TELA")
    public String getTela() {
        return tela.get();
    }
    
    public StringProperty telaProperty() {
        return tela;
    }
    
    public void setTela(String tela) {
        this.tela.set(tela);
    }
    
    @Column(name="ACAO")
    public String getAcao() {
        return acao.get();
    }
    
    public StringProperty acaoProperty() {
        return acao;
    }
    
    public void setAcao(String acao) {
        this.acao.set(acao);
    }
    
    @Column(name="REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }
    
    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }
    
    @Column(name="DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
}
