package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

@Entity
@Table(name = "SD_MAGENTO_SITCLI_001")
@TelaSysDeliz(descricao = "Grupo Cliente B2B", icon = "situacao cliente (4).png")
public class SdMagentoSitCli extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 50, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    @Transient
    private final StringProperty tabPreco = new SimpleStringProperty();
    
    public SdMagentoSitCli() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
        this.setCodigoFilter(String.valueOf(codigo));
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        this.setDescricaoFilter(descricao);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(Boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "TAB_PRE")
    public String getTabPreco() {
        return tabPreco.get();
    }

    public StringProperty tabPrecoProperty() {
        return tabPreco;
    }

    public void setTabPreco(String tabPreco) {
        this.tabPreco.set(tabPreco);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}