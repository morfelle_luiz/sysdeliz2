package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_MARCAS_CONCORRENTES_001")
@TelaSysDeliz(descricao = "Marcas Concorrentes", icon = "marca (4).png")
public class SdMarcasConcorrentes001 extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    @Transient
    @ExibeTableView(descricao = "Nossa Marca", width = 200)
    @ColunaFilter(descricao = "Marca", coluna = "marca.codigo", filterClass = "sysdeliz2.models.ti.Marca")
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    
    public SdMarcasConcorrentes001() {
    }
    
    @Id
    @Column(name = "CODIGO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MARCAS_CONCORRENTES_001")
    @SequenceGenerator(sequenceName = "SD_SEQ_MARCAS_CONCORRENTES_001", name = "SEQ_MARCAS_CONCORRENTES_001", initialValue = 1, allocationSize = 1)
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        setDescricaoFilter(nome);
        this.nome.set(nome);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
}