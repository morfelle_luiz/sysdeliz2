package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang.builder.ToStringBuilder;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.ti.SitCli;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_MARCAS_ENTIDADE_001")
public class SdMarcasEntidade implements Serializable {

    private final ObjectProperty<SdMarcasEntidadePK> id = new SimpleObjectProperty<>();
    private final StringProperty prospeccao = new SimpleStringProperty("0");
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final ObjectProperty<SitCli> tipo = new SimpleObjectProperty<>();
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
    private final ObjectProperty<Represen> consultor = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> comissaoFat = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> comissaoRec = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ListProperty<SdConcorrentesMarcaEnt001> concorrentes = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdConcorrentesMarcaEnt001> marcasConcorrentes = new ArrayList<>();
    private final BooleanProperty marcaNova = new SimpleBooleanProperty(false);
    private final BooleanProperty ativoB2b = new SimpleBooleanProperty(false);
    private final ObjectProperty<SdMagentoSitCli> grupoB2b = new SimpleObjectProperty<>();

    public SdMarcasEntidade() {
    }

    public SdMarcasEntidade(SdMarcasEntidade toCopy) {
        this.id.set(toCopy.getId());
        this.prospeccao.set(toCopy.getProspeccao());
        this.ativo.set(toCopy.isAtivo());
        this.tipo.set(toCopy.getTipo());
        this.codrep.set(toCopy.getCodrep());
        this.comissaoFat.set(toCopy.getComissaoFat());
        this.comissaoRec.set(toCopy.getComissaoRec());
        this.concorrentes.set(toCopy.getConcorrentes());
        this.marcasConcorrentes = toCopy.getMarcasConcorrentes();
        this.marcaNova.set(toCopy.isMarcaNova());
    }

    public SdMarcasEntidade(Integer codcli, Marca marca) {
        this.id.set(new SdMarcasEntidadePK(codcli, marca));
        this.marcaNova.set(true);
    }

    @EmbeddedId
    public SdMarcasEntidadePK getId() {
        return id.get();
    }

    public ObjectProperty<SdMarcasEntidadePK> idProperty() {
        return id;
    }

    public void setId(SdMarcasEntidadePK id) {
        this.id.set(id);
    }

    @Column(name = "PROSPECCAO")
    public String getProspeccao() {
        return prospeccao.get();
    }

    public StringProperty prospeccaoProperty() {
        return prospeccao;
    }

    public void setProspeccao(String prospeccao) {
        this.prospeccao.set(prospeccao);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TIPO")
    public SitCli getTipo() {
        return tipo.get();
    }

    public ObjectProperty<SitCli> tipoProperty() {
        return tipo;
    }

    public void setTipo(SitCli tipo) {
        this.tipo.set(tipo);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }

    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }

    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CONSULTOR")
    public Represen getConsultor() {
        return consultor.get();
    }

    public ObjectProperty<Represen> consultorProperty() {
        return consultor;
    }

    public void setConsultor(Represen codrep) {
        this.consultor.set(codrep);
    }

    @Column(name = "COMISSAO_FAT")
    public BigDecimal getComissaoFat() {
        return comissaoFat.get();
    }

    public ObjectProperty<BigDecimal> comissaoFatProperty() {
        return comissaoFat;
    }

    public void setComissaoFat(BigDecimal comissaoFat) {
        this.comissaoFat.set(comissaoFat);
    }

    @Column(name = "COMISSAO_REC")
    public BigDecimal getComissaoRec() {
        return comissaoRec.get();
    }

    public ObjectProperty<BigDecimal> comissaoRecProperty() {
        return comissaoRec;
    }

    public void setComissaoRec(BigDecimal comissaoRec) {
        this.comissaoRec.set(comissaoRec);
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "MARCA", referencedColumnName = "MARCA"),
            @JoinColumn(name = "CODCLI", referencedColumnName = "CODCLI")
    })
    public List<SdConcorrentesMarcaEnt001> getMarcasConcorrentes() {
        return marcasConcorrentes;
    }

    public void setMarcasConcorrentes(List<SdConcorrentesMarcaEnt001> marcasConcorrentes) {
        this.concorrentes.set(FXCollections.observableList(marcasConcorrentes));
        this.marcasConcorrentes = marcasConcorrentes;
    }

    @Column(name = "ATIVO_B2B")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivoB2b() {
        return ativoB2b.get();
    }

    public BooleanProperty ativoB2bProperty() {
        return ativoB2b;
    }

    public void setAtivoB2b(boolean ativoB2b) {
        this.ativoB2b.set(ativoB2b);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO_B2B")
    public SdMagentoSitCli getGrupoB2b() {
        return grupoB2b.get();
    }

    public ObjectProperty<SdMagentoSitCli> grupoB2bProperty() {
        return grupoB2b;
    }

    public void setGrupoB2b(SdMagentoSitCli grupoB2b) {
        this.grupoB2b.set(grupoB2b);
    }

    @Transient
    public ObservableList<SdConcorrentesMarcaEnt001> getConcorrentes() {
        return concorrentes.get();
    }

    public ListProperty<SdConcorrentesMarcaEnt001> concorrentesProperty() {
        return concorrentes;
    }

    public void setConcorrentes(ObservableList<SdConcorrentesMarcaEnt001> concorrentes) {
        this.concorrentes.set(concorrentes);
    }

    @Transient
    public boolean isMarcaNova() {
        return marcaNova.get();
    }

    public BooleanProperty marcaNovaProperty() {
        return marcaNova;
    }

    public void setMarcaNova(boolean marcaNova) {
        this.marcaNova.set(marcaNova);
    }

    @Transient
    public String stringToLog() {
        return new ToStringBuilder(this)
                .append("codcli", id.get().getCodcli() == null ? "Null" : id.get().getCodcli())
                .append("marca", id.get().getMarca() == null ? "Null" : id.get().getMarca().getCodigo())
                .append("prospeccao", prospeccao.getValue() == null ? "Null" : prospeccao.getValue().toString())
                .append("ativo", ativo.getValue() == null ? "Null" : ativo.getValue().toString())
                .append("tipo", tipo.getValue() == null ? "Null" : tipo.getValue().toString())
                .append("codrep", codrep.getValue() == null ? "Null" : codrep.getValue().toString())
                .append("comissaofat", comissaoFat.getValue() == null ? "Null" : comissaoFat.getValue().toString())
                .append("comissaorec", comissaoRec.getValue() == null ? "Null" : comissaoRec.getValue().toString())
                .append("consultor", consultor.getValue() == null ? "Null" : consultor.getValue().toString())
                .toString();
    }

    @Transient
    public String diffObject(SdMarcasEntidade entidade) {
        String diffs = "Marca " + entidade.getId().getMarca().getDescricao() + ": [\n";

        if (this.prospeccao.getValue() == null && entidade.prospeccao.getValue() != null)
            diffs = diffs.concat("prospeccao: NULL>" + entidade.prospeccao.getValue().toString() + "\n");
        else if (this.prospeccao.getValue() != null && entidade.prospeccao.getValue() == null)
            diffs = diffs.concat("prospeccao:" + this.prospeccao.getValue().toString() + ">NULL\n");
        else if (this.prospeccao.getValue() != null && entidade.prospeccao.getValue() != null && !this.prospeccao.getValue().equals(entidade.prospeccao.getValue()))
            diffs = diffs.concat("prospeccao:" + this.prospeccao.getValue().toString() + ">" + entidade.prospeccao.getValue().toString() + "\n");

        if (this.ativo.getValue() == null && entidade.ativo.getValue() != null)
            diffs = diffs.concat("ativo: NULL>" + entidade.ativo.getValue().toString() + "\n");
        else if (this.ativo.getValue() != null && entidade.ativo.getValue() == null)
            diffs = diffs.concat("ativo:" + this.ativo.getValue().toString() + ">NULL\n");
        else if (this.ativo.getValue() != null && entidade.ativo.getValue() != null && !this.ativo.getValue().equals(entidade.ativo.getValue()))
            diffs = diffs.concat("ativo:" + this.ativo.getValue().toString() + ">" + entidade.ativo.getValue().toString() + "\n");

        if (this.tipo.getValue() == null && entidade.tipo.getValue() != null)
            diffs = diffs.concat("tipo: NULL>" + entidade.tipo.getValue().toString() + "\n");
        else if (this.tipo.getValue() != null && entidade.tipo.getValue() == null)
            diffs = diffs.concat("tipo:" + this.tipo.getValue().toString() + ">NULL\n");
        else if (this.tipo.getValue() != null && entidade.tipo.getValue() != null && !this.tipo.getValue().getCodigo().equals(entidade.tipo.getValue().getCodigo()))
            diffs = diffs.concat("tipo:" + this.tipo.getValue().toString() + ">" + entidade.tipo.getValue().toString() + "\n");

        if (this.codrep.getValue() == null && entidade.codrep.getValue() != null)
            diffs = diffs.concat("codrep: NULL>" + entidade.codrep.getValue().toString() + "\n");
        else if (this.codrep.getValue() != null && entidade.codrep.getValue() == null)
            diffs = diffs.concat("codrep:" + this.codrep.getValue().toString() + ">NULL\n");
        else if (this.codrep.getValue() != null && entidade.codrep.getValue() != null && !this.codrep.getValue().getCodRep().equals(entidade.codrep.getValue().getCodRep()))
            diffs = diffs.concat("codrep:" + this.codrep.getValue().toString() + ">" + entidade.codrep.getValue().toString() + "\n");

        if (this.comissaoFat.getValue() == null && entidade.comissaoFat.getValue() != null)
            diffs = diffs.concat("comissaofat: NULL>" + entidade.comissaoFat.getValue().toString() + "\n");
        else if (this.comissaoFat.getValue() != null && entidade.comissaoFat.getValue() == null)
            diffs = diffs.concat("comissaofat:" + this.comissaoFat.getValue().toString() + ">NULL\n");
        else if (this.comissaoFat.getValue() != null && entidade.comissaoFat.getValue() != null && this.comissaoFat.getValue().doubleValue() != entidade.comissaoFat.getValue().doubleValue())
            diffs = diffs.concat("comissaofat:" + this.comissaoFat.getValue().toString() + ">" + entidade.comissaoFat.getValue().toString() + "\n");

        if (this.comissaoRec.getValue() == null && entidade.comissaoRec.getValue() != null)
            diffs = diffs.concat("comissaorec: NULL>" + entidade.comissaoRec.getValue().toString() + "\n");
        else if (this.comissaoRec.getValue() != null && entidade.comissaoRec.getValue() == null)
            diffs = diffs.concat("comissaorec:" + this.comissaoRec.getValue().toString() + ">NULL\n");
        else if (this.comissaoRec.getValue() != null && entidade.comissaoRec.getValue() != null && this.comissaoRec.getValue().doubleValue() != entidade.comissaoRec.getValue().doubleValue())
            diffs = diffs.concat("comissaorec:" + this.comissaoRec.getValue().toString() + ">" + entidade.comissaoRec.getValue().toString() + "\n");

        if (this.consultor.getValue() == null && entidade.consultor.getValue() != null)
            diffs = diffs.concat("consultor: NULL>" + entidade.consultor.getValue().toString() + "\n");
        else if (this.consultor.getValue() != null && entidade.consultor.getValue() == null)
            diffs = diffs.concat("consultor:" + this.consultor.getValue().toString() + ">NULL\n");
        else if (this.consultor.getValue() != null && entidade.consultor.getValue() != null && !this.consultor.getValue().getCodRep().equals(entidade.consultor.getValue().getCodRep()))
            diffs = diffs.concat("consultor:" + this.consultor.getValue().toString() + ">" + entidade.consultor.getValue().toString() + "\n");

        return diffs + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdMarcasEntidade)) return false;
        SdMarcasEntidade that = (SdMarcasEntidade) o;
        return
                Objects.equals(getProspeccao(), that.getProspeccao()) &&
                        Objects.equals(isAtivo(), that.isAtivo()) &&
                        Objects.equals(getTipo(), that.getTipo()) &&
                        Objects.equals(getCodrep(), that.getCodrep()) &&
                        Objects.equals(getConsultor(), that.getConsultor()) &&
                        Objects.equals(getComissaoFat(), that.getComissaoFat()) &&
                        Objects.equals(getComissaoRec(), that.getComissaoRec());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProspeccao(), isAtivo(), getTipo(), getCodrep(), getConsultor(), getComissaoFat(), getComissaoRec(), getConcorrentes(), getMarcasConcorrentes(), isMarcaNova());
    }

}
