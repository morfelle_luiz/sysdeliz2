package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.CodcliAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdMarcasEntidadePK implements Serializable {
    
    private final IntegerProperty codcli = new SimpleIntegerProperty();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    
    public SdMarcasEntidadePK() {
    }
    
    public SdMarcasEntidadePK(Integer codcli, Marca marca) {
        setCodcli(codcli);
        setMarca(marca);
//        this.codcli.set(codcli);
//        this.marca.set(marca);
    }
    
    @Column(name = "CODCLI", columnDefinition = "varchar2()")
    @Convert(converter = CodcliAttributeConverter.class)
    public Integer getCodcli() {
        return codcli.get();
    }
    
    public IntegerProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Integer codcli) {
        this.codcli.set(codcli);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
}
