package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_MARCAS_REPRESEN_001")
public class SdMarcasRepresen implements Serializable {
    @Embeddable
    public static class Pk implements Serializable {
        private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
        private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();

        public Pk() {
        }

        public Pk(Represen codrep, Marca marca) {
            this.codrep.set(codrep);
            this.marca.set(marca);
        }

        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "CODREP", nullable = false)
        public Represen getCodrep() {
            return codrep.get();
        }

        public ObjectProperty<Represen> codrepProperty() {
            return codrep;
        }

        public void setCodrep(Represen codrep) {
            this.codrep.set(codrep);
        }

        @OneToOne
        @JoinColumn(name = "MARCA")
        public Marca getMarca() {
            return marca.get();
        }

        public ObjectProperty<Marca> marcaProperty() {
            return marca;
        }

        public void setMarca(Marca marca) {
            this.marca.set(marca);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private List<SdColecaoMarcaRepresen> colecoes = new ArrayList<>();
    private final IntegerProperty qtdeMostruario = new SimpleIntegerProperty(1);

    public SdMarcasRepresen() {
    }

    public SdMarcasRepresen(Represen codrep, Marca marca) {
        this.id.set(new Pk(codrep, marca));
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @OneToMany(mappedBy = "id.marcaRepresen", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<SdColecaoMarcaRepresen> getColecoes() {
        return colecoes;
    }

    public void setColecoes(List<SdColecaoMarcaRepresen> colecoes) {
        this.colecoes = colecoes;
    }

    @Transient
    public int getQtdeMostruario() {
        return qtdeMostruario.get();
    }

    public IntegerProperty qtdeMostruarioProperty() {
        return qtdeMostruario;
    }

    public void setQtdeMostruario(int qtdeMostruario) {
        this.qtdeMostruario.set(qtdeMostruario);
    }
}
