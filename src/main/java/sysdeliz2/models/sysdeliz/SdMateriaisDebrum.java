package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_MATERIAIS_DEBRUM_001")
public class SdMateriaisDebrum {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final IntegerProperty idplano = new SimpleIntegerProperty();
    private final IntegerProperty idrisco = new SimpleIntegerProperty();
    private final StringProperty aplicacao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty material = new SimpleStringProperty();
    private final StringProperty cori = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final StringProperty partida = new SimpleStringProperty();
    private final StringProperty sequencia = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumopc = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumido = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty qtdepecas = new SimpleIntegerProperty();
    
    public SdMateriaisDebrum() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MATERIAIS_DEBRUM")
    @SequenceGenerator(name = "SEQ_SD_MATERIAIS_DEBRUM", sequenceName = "SEQ_SD_MATERIAIS_DEBRUM", allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id.set(id);
    }
    
    @Column(name = "ID_PLANO")
    public Integer getIdplano() {
        return idplano.get();
    }
    
    public IntegerProperty idplanoProperty() {
        return idplano;
    }
    
    public void setIdplano(Integer idplano) {
        this.idplano.set(idplano);
    }
    
    @Column(name = "ID_RISCO")
    public int getIdrisco() {
        return idrisco.get();
    }
    
    public IntegerProperty idriscoProperty() {
        return idrisco;
    }
    
    public void setIdrisco(int idrisco) {
        this.idrisco.set(idrisco);
    }
    
    @Column(name = "APLICACAO")
    public String getAplicacao() {
        return aplicacao.get();
    }
    
    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(String aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "MATERIAL")
    public String getMaterial() {
        return material.get();
    }
    
    public StringProperty materialProperty() {
        return material;
    }
    
    public void setMaterial(String material) {
        this.material.set(material);
    }
    
    @Column(name = "COR_I")
    public String getCori() {
        return cori.get();
    }
    
    public StringProperty coriProperty() {
        return cori;
    }
    
    public void setCori(String cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }
    
    public StringProperty partidaProperty() {
        return partida;
    }
    
    public void setPartida(String partida) {
        this.partida.set(partida);
    }
    
    @Column(name = "SEQUENCIA")
    public String getSequencia() {
        return sequencia.get();
    }
    
    public StringProperty sequenciaProperty() {
        return sequencia;
    }
    
    public void setSequencia(String sequencia) {
        this.sequencia.set(sequencia);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }
    
    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "CONSUMO_PC")
    public BigDecimal getConsumopc() {
        return consumopc.get();
    }
    
    public ObjectProperty<BigDecimal> consumopcProperty() {
        return consumopc;
    }
    
    public void setConsumopc(BigDecimal consumopc) {
        this.consumopc.set(consumopc);
    }
    
    @Column(name = "CONSUMIDO")
    public BigDecimal getConsumido() {
        return consumido.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoProperty() {
        return consumido;
    }
    
    public void setConsumido(BigDecimal consumido) {
        this.consumido.set(consumido);
    }
    
    @Column(name = "QTDE_PECAS")
    public Integer getQtdepecas() {
        return qtdepecas.get();
    }
    
    public IntegerProperty qtdepecasProperty() {
        return qtdepecas;
    }
    
    public void setQtdepecas(Integer qtdepecas) {
        this.qtdepecas.set(qtdepecas);
    }
    
}