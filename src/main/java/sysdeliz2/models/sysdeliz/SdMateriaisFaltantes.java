package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_MATERIAIS_FALTANTES_001")
public class SdMateriaisFaltantes {
    
    private final ObjectProperty<SdMateriaisFaltantesPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhAnalise = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhCriacao = new SimpleObjectProperty<>();
    private final StringProperty status = new SimpleStringProperty();
    private final BooleanProperty substituto = new SimpleBooleanProperty();
    
    public SdMateriaisFaltantes() {
    }
    
    public SdMateriaisFaltantes(String numero, Material insumo, Cor corI, String setor, String faixa, Integer idPcpftof, BigDecimal qtde, LocalDateTime dhAnalise, String status, Boolean substituto) {
        this.id.set(new SdMateriaisFaltantesPK(numero, insumo, corI, setor, faixa, idPcpftof));
        this.qtde.set(qtde);
        this.dhAnalise.set(dhAnalise);
        this.status.set(status);
        this.substituto.set(substituto);
        this.dhCriacao.set(LocalDateTime.now());
    }
    
    @EmbeddedId
    public SdMateriaisFaltantesPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdMateriaisFaltantesPK> idProperty() {
        return id;
    }
    
    public void setId(SdMateriaisFaltantesPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "DH_ANALISE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhAnalise() {
        return dhAnalise.get();
    }
    
    public ObjectProperty<LocalDateTime> dhAnaliseProperty() {
        return dhAnalise;
    }
    
    public void setDhAnalise(LocalDateTime dhAnalise) {
        this.dhAnalise.set(dhAnalise);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "SUBSTITUTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSubstituto() {
        return substituto.get();
    }
    
    public BooleanProperty substitutoProperty() {
        return substituto;
    }
    
    public void setSubstituto(boolean substituto) {
        this.substituto.set(substituto);
    }
    
    @Column(name = "DH_CRIACAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhCriacao() {
        return dhCriacao.get();
    }
    
    public ObjectProperty<LocalDateTime> dhCriacaoProperty() {
        return dhCriacao;
    }
    
    public void setDhCriacao(LocalDateTime dhCriacao) {
        this.dhCriacao.set(dhCriacao);
    }
}
