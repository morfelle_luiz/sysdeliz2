package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdMateriaisFaltantesPK implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corI = new SimpleObjectProperty<>();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final IntegerProperty idPcpftof = new SimpleIntegerProperty();
    
    public SdMateriaisFaltantesPK() {
    }
    
    public SdMateriaisFaltantesPK(String numero, Material insumo, Cor corI, String setor, String faixa, Integer idPcpftof) {
        this.numero.set(numero);
        this.insumo.set(insumo);
        this.corI.set(corI);
        this.setor.set(setor);
        this.faixa.set(faixa);
        this.idPcpftof.set(idPcpftof);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR_I")
    public Cor getCorI() {
        return corI.get();
    }
    
    public ObjectProperty<Cor> corIProperty() {
        return corI;
    }
    
    public void setCorI(Cor corI) {
        this.corI.set(corI);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }
    
    public StringProperty faixaProperty() {
        return faixa;
    }
    
    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }
    
    @Column(name = "ID_PCPFTOF")
    public int getIdPcpftof() {
        return idPcpftof.get();
    }
    
    public IntegerProperty idPcpftofProperty() {
        return idPcpftof;
    }
    
    public void setIdPcpftof(int idPcpftof) {
        this.idPcpftof.set(idPcpftof);
    }
}
