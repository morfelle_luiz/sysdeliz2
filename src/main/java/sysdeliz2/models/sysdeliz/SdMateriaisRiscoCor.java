package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdMateriaisCorte;
import sysdeliz2.utils.converters.IntegerAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_MATERIAIS_RISCO_COR_001")
public class SdMateriaisRiscoCor extends BasicModel implements Serializable {
    
    private final IntegerProperty id = new SimpleIntegerProperty(0);
    private final IntegerProperty idrisco = new SimpleIntegerProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty material = new SimpleStringProperty();
    private final StringProperty cori = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final StringProperty partida = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<>();
    private final StringProperty sequencia = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumido = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumidoDebrum = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty qtdefolhas = new SimpleIntegerProperty();
    private final IntegerProperty qtdepecas = new SimpleIntegerProperty();
    private final IntegerProperty seqEnfesto = new SimpleIntegerProperty();
    private final IntegerProperty qtdeFolhasRc = new SimpleIntegerProperty();
    
    public SdMateriaisRiscoCor() {
    }
    
    public SdMateriaisRiscoCor(Integer idrisco, String cor, String material, String cori,
                               String lote, String tonalidade, String partida, BigDecimal largura,
                               BigDecimal gramatura, String sequencia, BigDecimal consumido, Integer qtdefolhas,
                               Integer qtdepecas) {
        this.idrisco.set(idrisco);
        this.cor.set(cor);
        this.material.set(material);
        this.cori.set(cori);
        this.lote.set(lote);
        this.tonalidade.set(tonalidade);
        this.partida.set(partida);
        this.largura.set(largura);
        this.gramatura.set(gramatura);
        this.sequencia.set(sequencia);
        this.consumido.set(consumido);
        this.qtdefolhas.set(qtdefolhas);
        this.qtdepecas.set(qtdepecas);
    }
    
    public SdMateriaisRiscoCor(Integer idrisco, String cor, String material, String cori,
                               String lote, String tonalidade, String partida, BigDecimal largura,
                               BigDecimal gramatura, String sequencia, BigDecimal consumido, Integer qtdefolhas,
                               Integer qtdepecas, BigDecimal consumidoDebrum, Integer seqEnfesto) {
        this.idrisco.set(idrisco);
        this.cor.set(cor);
        this.material.set(material);
        this.cori.set(cori);
        this.lote.set(lote);
        this.tonalidade.set(tonalidade);
        this.partida.set(partida);
        this.largura.set(largura);
        this.gramatura.set(gramatura);
        this.sequencia.set(sequencia);
        this.consumido.set(consumido);
        this.qtdefolhas.set(qtdefolhas);
        this.qtdepecas.set(qtdepecas);
        this.consumidoDebrum.set(consumidoDebrum);
        this.seqEnfesto.set(seqEnfesto);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MATERIAIS_RISCO_COR")
    @SequenceGenerator(name = "SEQ_SD_MATERIAIS_RISCO_COR", sequenceName = "SEQ_SD_MATERIAIS_RISCO_COR", allocationSize = 1)
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id.set(id);
        setCodigoFilter(String.valueOf(id));
        setDescricaoFilter(String.valueOf(id));
    }
    
    @Column(name = "ID_RISCO")
    public int getIdrisco() {
        return idrisco.get();
    }
    
    public IntegerProperty idriscoProperty() {
        return idrisco;
    }
    
    public void setIdrisco(int idrisco) {
        this.idrisco.set(idrisco);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "MATERIAL")
    public String getMaterial() {
        return material.get();
    }
    
    public StringProperty materialProperty() {
        return material;
    }
    
    public void setMaterial(String material) {
        this.material.set(material);
    }
    
    @Column(name = "COR_I")
    public String getCori() {
        return cori.get();
    }
    
    public StringProperty coriProperty() {
        return cori;
    }
    
    public void setCori(String cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }
    
    public StringProperty partidaProperty() {
        return partida;
    }
    
    public void setPartida(String partida) {
        this.partida.set(partida);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }
    
    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "SEQUENCIA")
    public String getSequencia() {
        return sequencia.get();
    }
    
    public StringProperty sequenciaProperty() {
        return sequencia;
    }
    
    public void setSequencia(String sequencia) {
        this.sequencia.set(sequencia);
    }
    
    @Column(name = "CONSUMIDO")
    public BigDecimal getConsumido() {
        return consumido.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoProperty() {
        return consumido;
    }
    
    public void setConsumido(BigDecimal consumido) {
        this.consumido.set(consumido);
    }
    
    @Column(name = "CONS_DEBRUM")
    public BigDecimal getConsumidoDebrum() {
        return consumidoDebrum.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoDebrumProperty() {
        return consumidoDebrum;
    }
    
    public void setConsumidoDebrum(BigDecimal consumidoDebrum) {
        this.consumidoDebrum.set(consumidoDebrum);
    }
    
    @Column(name = "QTDE_FOLHAS")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getQtdefolhas() {
        return qtdefolhas.get();
    }
    
    public IntegerProperty qtdefolhasProperty() {
        return qtdefolhas;
    }
    
    public void setQtdefolhas(int qtdefolhas) {
        this.qtdefolhas.set(qtdefolhas);
    }
    
    @Column(name = "QTDE_PECAS")
    @Convert(converter = IntegerAttributeConverter.class)
    public int getQtdepecas() {
        return qtdepecas.get();
    }
    
    public IntegerProperty qtdepecasProperty() {
        return qtdepecas;
    }
    
    public void setQtdepecas(int qtdepecas) {
        this.qtdepecas.set(qtdepecas);
    }
    
    @Column(name = "SEQ_ENFESTO")
    public int getSeqEnfesto() {
        return seqEnfesto.get();
    }
    
    public IntegerProperty seqEnfestoProperty() {
        return seqEnfesto;
    }
    
    public void setSeqEnfesto(int seqEnfesto) {
        this.seqEnfesto.set(seqEnfesto);
    }
    
    @Column(name = "QTDE_FOLHAS_RC")
    @Convert(converter = IntegerAttributeConverter.class)
    public Integer getQtdeFolhasRc() {
        return qtdeFolhasRc.get();
    }
    
    public IntegerProperty qtdeFolhasRcProperty() {
        return qtdeFolhasRc;
    }
    
    public void setQtdeFolhasRc(Integer qtdeFolhasRc) {
        this.qtdeFolhasRc.set(qtdeFolhasRc);
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.getId());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj))
            return true;
        
        if (obj instanceof VSdMateriaisCorte) {
            VSdMateriaisCorte roloReserva = (VSdMateriaisCorte) obj;
            if (roloReserva.getCodigo().getCodigo().equals(this.getMaterial()) &&
                    roloReserva.getCori().getCor().equals(this.getCori()) &&
                    roloReserva.getTonalidade().equals(this.getTonalidade()) &&
                    roloReserva.getLote().equals(this.getLote()) &&
                    roloReserva.getPartida().equals(this.getPartida()) &&
                    roloReserva.getSequencia().equals(this.getSequencia()))
                return true;
        } else {
            SdMateriaisRiscoCor roloConsumido = (SdMateriaisRiscoCor) obj;
            if (roloConsumido.getMaterial().equals(this.getMaterial()) &&
                    roloConsumido.getCori().equals(this.getCori()) &&
                    roloConsumido.getTonalidade().equals(this.getTonalidade()) &&
                    roloConsumido.getLote().equals(this.getLote()) &&
                    roloConsumido.getPartida().equals(this.getPartida()) &&
                    roloConsumido.getSequencia().equals(this.getSequencia()))
                return true;
        }
        
        return false;
    }
}
