package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;

@Entity
@Table(name = "SD_MINUTA_EXPEDICAO_001")
public class SdMinutaExpedicao {
    
    private final ObjectProperty<SdMinutaExpedicaoPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeVolumes = new SimpleIntegerProperty();
    private final IntegerProperty volumesLidos = new SimpleIntegerProperty();
    private final IntegerProperty pendente = new SimpleIntegerProperty();
    private final StringProperty caixas = new SimpleStringProperty();
    private final BooleanProperty conferido = new SimpleBooleanProperty(false);
    
    public SdMinutaExpedicao() {
    }
    
    @EmbeddedId
    public SdMinutaExpedicaoPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdMinutaExpedicaoPK> idProperty() {
        return id;
    }
    
    public void setId(SdMinutaExpedicaoPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE_VOLUMES")
    public int getQtdeVolumes() {
        return qtdeVolumes.get();
    }
    
    public IntegerProperty qtdeVolumesProperty() {
        return qtdeVolumes;
    }
    
    public void setQtdeVolumes(int qtdeVolumes) {
        this.qtdeVolumes.set(qtdeVolumes);
    }
    
    @Column(name = "QTDE_LIDOS")
    public int getVolumesLidos() {
        return volumesLidos.get();
    }
    
    public IntegerProperty volumesLidosProperty() {
        return volumesLidos;
    }
    
    public void setVolumesLidos(int volumesLidos) {
        this.volumesLidos.set(volumesLidos);
    }
    
    @Transient
    public int getPendente() {
        return pendente.get();
    }
    
    public IntegerProperty pendenteProperty() {
        return pendente;
    }
    
    public void setPendente(int pendente) {
        this.pendente.set(pendente);
    }
    
    @Column(name = "CAIXAS")
    public String getCaixas() {
        return caixas.get();
    }
    
    public StringProperty caixasProperty() {
        return caixas;
    }
    
    public void setCaixas(String caixas) {
        this.caixas.set(caixas);
    }

    @Column(name = "CONFERIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isConferido() {
        return conferido.get();
    }

    public BooleanProperty conferidoProperty() {
        return conferido;
    }

    public void setConferido(boolean conferido) {
        this.conferido.set(conferido);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
    
    @PostLoad
    private void postLoad() {
        pendente.set(qtdeVolumes.subtract(volumesLidos).intValue());
    }
}