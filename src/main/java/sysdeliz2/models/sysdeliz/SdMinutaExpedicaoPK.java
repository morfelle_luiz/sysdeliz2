package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Nota;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class SdMinutaExpedicaoPK implements Serializable {
    
    private final ObjectProperty<LocalDate> dtMinuta = new SimpleObjectProperty<>();
    private final StringProperty transportadora = new SimpleStringProperty();
    private final ObjectProperty<Nota> nota = new SimpleObjectProperty<>();
    
    public SdMinutaExpedicaoPK() {
    }
    
    @Column(name = "DT_MINUTA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtMinuta() {
        return dtMinuta.get();
    }
    
    public ObjectProperty<LocalDate> dtMinutaProperty() {
        return dtMinuta;
    }
    
    public void setDtMinuta(LocalDate dtMinuta) {
        this.dtMinuta.set(dtMinuta);
    }
    
    @Column(name = "TRANSPORTADORA")
    public String getTransportadora() {
        return transportadora.get();
    }
    
    public StringProperty transportadoraProperty() {
        return transportadora;
    }
    
    public void setTransportadora(String transportadora) {
        this.transportadora.set(transportadora);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "NOTA")
    public Nota getNota() {
        return nota.get();
    }
    
    public ObjectProperty<Nota> notaProperty() {
        return nota;
    }
    
    public void setNota(Nota nota) {
        this.nota.set(nota);
    }
}