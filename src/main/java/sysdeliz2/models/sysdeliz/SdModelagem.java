package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_MODELAGEM_001")
@TelaSysDeliz(descricao = "Modelagem", icon = "produto (4).png")
public class SdModelagem extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 150)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    @Transient
    private final IntegerProperty codigo = new SimpleIntegerProperty();

    @ExibeTableView(descricao = "Modelagem", width = 250)
    @ColunaFilter(descricao = "Modelagem", coluna = "modelagem")
    @Transient
    private final StringProperty modelagem = new SimpleStringProperty();

    public SdModelagem() {
    }

    @Id
    @Column(name = "codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }


    @Column(name = "modelagem")
    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        setDescricaoFilter(modelagem);
        this.modelagem.set(modelagem);
    }

    @Override
    public String toString() {
        return modelagem.getValue();
    }
}
