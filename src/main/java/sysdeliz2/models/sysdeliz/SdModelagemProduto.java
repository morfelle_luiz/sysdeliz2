package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_MODELAGEM_PRODUTO_001")
public class SdModelagemProduto {
    
    private final ObjectProperty<SdModelagemProdutoPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty partes = new SimpleIntegerProperty();
    
    public SdModelagemProduto() {
    }
    
    @EmbeddedId
    public SdModelagemProdutoPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdModelagemProdutoPK> idProperty() {
        return id;
    }
    
    public void setId(SdModelagemProdutoPK id) {
        this.id.set(id);
    }
    
    @Column(name = "PARTES")
    public Integer getPartes() {
        return partes.get();
    }
    
    public IntegerProperty partesProperty() {
        return partes;
    }
    
    public void setPartes(Integer partes) {
        this.partes.set(partes);
    }
    
}
