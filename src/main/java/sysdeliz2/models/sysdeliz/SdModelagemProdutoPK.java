package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdModelagemProdutoPK implements Serializable {
    
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacao = new SimpleObjectProperty<>();
    
    public SdModelagemProdutoPK() {
    }
    
    public SdModelagemProdutoPK(VSdDadosProduto codigo, Pcpapl aplicacao) {
        this.codigo.set(codigo);
        this.aplicacao.set(aplicacao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APLICACAO")
    public Pcpapl getAplicacao() {
        return aplicacao.get();
    }
    
    public ObjectProperty<Pcpapl> aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(Pcpapl aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    
}
