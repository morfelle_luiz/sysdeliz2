package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SD_MOSTR_REP_BARRA_RETORNO_001")
public class SdMostrRepBarraRetorno implements Serializable {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty barra = new SimpleStringProperty();

    public SdMostrRepBarraRetorno() {
    }

    @Id
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }

    @Id
    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Id
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }
}