package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "SD_MRP_PRODUTO_001")
public class SdMrpProduto {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty ordemp = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentop = new SimpleObjectProperty<LocalDate>();
    
    public SdMrpProduto() {
    }
    
    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id.set(id);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }
    
    public StringProperty estoqueProperty() {
        return estoque;
    }
    
    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }
    
    @Column(name = "ORDEM_P")
    public String getOrdemp() {
        return ordemp.get();
    }
    
    public StringProperty ordempProperty() {
        return ordemp;
    }
    
    public void setOrdemp(String ordemp) {
        this.ordemp.set(ordemp);
    }
    
    @Column(name = "DT_ENT_OP")
    public LocalDate getDtentop() {
        return dtentop.get();
    }
    
    public ObjectProperty<LocalDate> dtentopProperty() {
        return dtentop;
    }
    
    public void setDtentop(LocalDate dtentop) {
        this.dtentop.set(dtentop);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
}