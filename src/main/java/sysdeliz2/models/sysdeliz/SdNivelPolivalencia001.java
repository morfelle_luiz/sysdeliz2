/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_NIVEL_POLIVALENCIA_001")
public class SdNivelPolivalencia001 extends BasicModel {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> metaInicio = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> metaFim = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> premio = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty sigla = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public SdNivelPolivalencia001() {
    }

    public SdNivelPolivalencia001(SdNivelPolivalencia001 toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.descricao.set(toCopy.descricao.get());
        this.metaInicio.set(toCopy.metaInicio.get());
        this.metaFim.set(toCopy.metaFim.get());
        this.premio.set(toCopy.premio.get());
        this.sigla.set(toCopy.sigla.get());
        setSelected(toCopy.isSelected());
    }

    public SdNivelPolivalencia001(Integer codigo, String descricao, Double metaInicio, Double metaFim, Double premio, String sigla) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.metaInicio.set(BigDecimal.valueOf(metaInicio));
        this.metaFim.set(BigDecimal.valueOf(metaFim));
        this.premio.set(BigDecimal.valueOf(premio));
        this.sigla.set(sigla);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ai_codigo_nivel_polivalencia")
    @SequenceGenerator(name="ai_codigo_nivel_polivalencia", sequenceName="SEQ_SD_NIVEL_POLIVALENCIA_001", allocationSize = 1)
    @Column(name="CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    public void setCodigo(int value) {
        setCodigoFilter(value+"");
        codigo.set(value);
    }
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    @Column(name="DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    public void setDescricao(String value) {
        descricao.set(value);
    }
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Column(name="META_INICIO")
    public BigDecimal getMetaInicio() {
        return metaInicio.get();
    }
    public void setMetaInicio(BigDecimal value) {
        metaInicio.set(value);
    }
    public ObjectProperty<BigDecimal> metaInicioProperty() {
        return metaInicio;
    }
    
    @Column(name="META_FIM")
    public BigDecimal getMetaFim() {
        return metaFim.get();
    }
    public void setMetaFim(BigDecimal value) {
        metaFim.set(value);
    }
    public ObjectProperty<BigDecimal> metaFimProperty() {
        return metaFim;
    }
    
    @Column(name="PREMIO")
    public BigDecimal getPremio() {
        return premio.get();
    }
    public void setPremio(BigDecimal value) {
        premio.set(value);
    }
    public ObjectProperty<BigDecimal> premioProperty() {
        return premio;
    }
    
    @Column(name="SIGLA")
    public String getSigla() {
        return sigla.get();
    }
    public void setSigla(String value) {
        sigla.set(value);
    }
    public StringProperty siglaProperty() {
        return sigla;
    }
    
    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Transient
    public void copy(SdNivelPolivalencia001 level) {
        this.codigo.set(level.codigo.get());
        this.descricao.set(level.descricao.get());
        this.metaInicio.set(level.metaInicio.get());
        this.metaFim.set(level.metaFim.get());
        this.premio.set(level.premio.get());
        this.sigla.set(level.sigla.get());
        setSelected(level.isSelected());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdNivelPolivalencia001 other = (SdNivelPolivalencia001) obj;
        if (!Objects.equals(this.codigo.get(), other.codigo.get())) {
            return false;
        }
        return true;
    }

}
