package sysdeliz2.models.sysdeliz;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "SD_NOTA_001")
public class SdNota {
    
    private final StringProperty fatura = new SimpleStringProperty();
    private final BooleanProperty enviadoEmailFaturado = new SimpleBooleanProperty(false);
    private final BooleanProperty impressoBoleto = new SimpleBooleanProperty(false);
    
    public SdNota() {
    }
    
    @Id
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "ENVIADO_EMAIL_FATURADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviadoEmailFaturado() {
        return enviadoEmailFaturado.get();
    }
    
    public BooleanProperty enviadoEmailFaturadoProperty() {
        return enviadoEmailFaturado;
    }
    
    public void setEnviadoEmailFaturado(boolean enviadoEmailFaturado) {
        this.enviadoEmailFaturado.set(enviadoEmailFaturado);
    }
    
    @Column(name = "IMPRESSO_BOLETO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImpressoBoleto() {
        return impressoBoleto.get();
    }
    
    public BooleanProperty impressoBoletoProperty() {
        return impressoBoleto;
    }
    
    public void setImpressoBoleto(boolean impressoBoleto) {
        this.impressoBoleto.set(impressoBoleto);
    }
}