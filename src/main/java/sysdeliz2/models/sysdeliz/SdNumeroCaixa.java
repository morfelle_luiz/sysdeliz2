package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "SD_NUMERO_CAIXA_001")
public class SdNumeroCaixa {
    
    private final ObjectProperty<SdNumeroCaixaPK> id = new SimpleObjectProperty<>();
    private final StringProperty usuario = new SimpleStringProperty(Globals.getUsuarioLogado().getUsuario());
    private final ObjectProperty<LocalDate> dtcriacao = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    
    public SdNumeroCaixa() {
    }
    
    public SdNumeroCaixa(String tipo, Integer inicio, Integer fim) {
        this.id.set(new SdNumeroCaixaPK(tipo, inicio, fim));
    }
    
    @EmbeddedId
    public SdNumeroCaixaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdNumeroCaixaPK> idProperty() {
        return id;
    }
    
    public void setId(SdNumeroCaixaPK id) {
        this.id.set(id);
    }
    
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }
    
    public StringProperty usuarioProperty() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }
    
    @Column(name = "DT_CRIACAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtcriacao() {
        return dtcriacao.get();
    }
    
    public ObjectProperty<LocalDate> dtcriacaoProperty() {
        return dtcriacao;
    }
    
    public void setDtcriacao(LocalDate dtcriacao) {
        this.dtcriacao.set(dtcriacao);
    }
    
}