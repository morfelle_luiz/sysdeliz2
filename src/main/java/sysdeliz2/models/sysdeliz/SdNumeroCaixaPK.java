package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SdNumeroCaixaPK implements Serializable {
    
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty inicio = new SimpleIntegerProperty();
    private final IntegerProperty fim = new SimpleIntegerProperty();
    
    public SdNumeroCaixaPK() {
    }
    
    public SdNumeroCaixaPK(String tipo, Integer inicio, Integer fim) {
        this.tipo.set(tipo);
        this.inicio.set(inicio);
        this.fim.set(fim);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "INICIO")
    public Integer getInicio() {
        return inicio.get();
    }
    
    public IntegerProperty inicioProperty() {
        return inicio;
    }
    
    public void setInicio(Integer inicio) {
        this.inicio.set(inicio);
    }
    
    @Column(name = "FIM")
    public Integer getFim() {
        return fim.get();
    }
    
    public IntegerProperty fimProperty() {
        return fim;
    }
    
    public void setFim(Integer fim) {
        this.fim.set(fim);
    }
}