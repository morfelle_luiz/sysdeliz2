package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_OFS_COLETOR_001")
public class SdOfsColetor {
    
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty("N");
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty("A");
    private final StringProperty periodo = new SimpleStringProperty();
    private final IntegerProperty programacao = new SimpleIntegerProperty();
    private final ObjectProperty<Entidade> faccao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtcoleta = new SimpleObjectProperty<>();
    private final BooleanProperty impresso = new SimpleBooleanProperty(false);
    private final BooleanProperty pintarOf = new SimpleBooleanProperty(false);
    
    public SdOfsColetor() {
    }
    
    public SdOfsColetor(SdColaborador coletor, String numero, String setor, Deposito deposito, Integer ordem, Entidade faccao, String tipo, Boolean impresso) {
        this.coletor.set(coletor);
        this.numero.set(numero);
        this.setor.set(setor);
        this.deposito.set(deposito);
        this.ordem.set(ordem);
        this.faccao.set(faccao);
        this.tipo.set(tipo);
        this.impresso.set(impresso);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }
    
    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }
    
    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @OneToOne
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }
    
    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ai_ofs_coletor")
    @SequenceGenerator(name = "ai_ofs_coletor", sequenceName = "sd_seq_of_coletor", allocationSize = 1)
    @Column(name = "PROGRAMACAO")
    public Integer getProgramacao() {
        return programacao.get();
    }
    
    public IntegerProperty programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(Integer programacao) {
        this.programacao.set(programacao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FACCAO")
    @Convert(converter = CodcliAttributeConverter.class)
    public Entidade getFaccao() {
        return faccao.get();
    }
    
    public ObjectProperty<Entidade> faccaoProperty() {
        return faccao;
    }
    
    public void setFaccao(Entidade faccao) {
        this.faccao.set(faccao);
    }
    
    @Column(name = "DT_COLETA")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtcoleta() {
        return dtcoleta.get();
    }
    
    public ObjectProperty<LocalDateTime> dtcoletaProperty() {
        return dtcoleta;
    }
    
    public void setDtcoleta(LocalDateTime dtcoleta) {
        this.dtcoleta.set(dtcoleta);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "IMPRESSO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImpresso() {
        return impresso.get();
    }
    
    public BooleanProperty impressoProperty() {
        return impresso;
    }
    
    public void setImpresso(boolean impresso) {
        this.impresso.set(impresso);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @Transient
    public boolean isPintarOf() {
        return pintarOf.get();
    }
    
    public BooleanProperty pintarOfProperty() {
        return pintarOf;
    }
    
    public void setPintarOf(boolean pintarOf) {
        this.pintarOf.set(pintarOf);
    }
}
