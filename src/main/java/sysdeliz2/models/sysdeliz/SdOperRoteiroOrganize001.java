/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_OPER_ROTEIRO_ORGANIZE_001")
public class SdOperRoteiroOrganize001 extends BasicModel {
    
    private final ObjectProperty<SdOperRoteiroOrganize001PK> id = new SimpleObjectProperty<>();
    private final StringProperty setor = new SimpleStringProperty();
    private final ObjectProperty<SdSetorOp001> setorOp = new SimpleObjectProperty<>();
    private final StringProperty setorOperacao = new SimpleStringProperty();
    private final StringProperty descSetOp = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> tempoOp = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty independencia = new SimpleStringProperty();
    
    public SdOperRoteiroOrganize001() {
    }
    
    public SdOperRoteiroOrganize001(Integer roteiro, SdOperacaoOrganize001 operacao, Integer ordem,
                                    String setor, SdSetorOp001 setorOp, Double tempoOp, String independencia) {
        this.id.set(new SdOperRoteiroOrganize001PK(roteiro, ordem, operacao));
        this.setor.set(setor);
        this.setorOp.set(setorOp);
        this.setorOperacao.set(String.valueOf(setorOp.getCodigo()));
        this.descSetOp.set(setor.concat(" - ").concat(setorOp.getDescricao().toUpperCase()));
        this.tempoOp.set(BigDecimal.valueOf(tempoOp));
        this.independencia.set(independencia);
    }
    
    public SdOperRoteiroOrganize001(Integer roteiro, Integer codigoOperacao, Integer ordem, String setor,
                                    String setorOperacao, String descSetOp, Double tempoOp, String independencia, SdOperacaoOrganize001 operacao) {
        this.id.set(new SdOperRoteiroOrganize001PK(roteiro, ordem, operacao));
        this.setor.set(setor);
        this.setorOperacao.set(setorOperacao);
        this.descSetOp.set(descSetOp);
        this.tempoOp.set(BigDecimal.valueOf(tempoOp));
        this.independencia.set(independencia);
    }
    
    @EmbeddedId
    public SdOperRoteiroOrganize001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdOperRoteiroOrganize001PK> idProperty() {
        return id;
    }
    
    public void setId(SdOperRoteiroOrganize001PK id) {
        this.id.set(id);
    }

    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public void setSetor(String value) {
        setor.set(value);
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SETOR_OPERACAO")
    public SdSetorOp001 getSetorOp() {
        return setorOp.get();
    }
    
    public ObjectProperty<SdSetorOp001> setorOpProperty() {
        return setorOp;
    }
    
    public void setSetorOp(SdSetorOp001 setorOp) {
        this.setorOp.set(setorOp);
        this.setorOperacao.set(String.valueOf(setorOp.getCodigo()));
    }
    
    @Transient
    public final String getSetorOperacao() {
        return setorOperacao.get();
    }

    public final void setSetorOperacao(String value) {
        setorOperacao.set(value);
    }

    public StringProperty setorOperacaoProperty() {
        return setorOperacao;
    }
    
    @Column(name = "DESC_SET_OP")
    public String getDescSetOp() {
        return descSetOp.get();
    }

    public void setDescSetOp(String value) {
        descSetOp.set(value);
    }

    public StringProperty descSetOpProperty() {
        return descSetOp;
    }
    
    @Column(name = "TEMPO_OP")
    public BigDecimal getTempoOp() {
        return tempoOp.get();
    }

    public void setTempoOp(BigDecimal value) {
        tempoOp.set(value);
    }

    public ObjectProperty<BigDecimal> tempoOpProperty() {
        return tempoOp;
    }
    
    @Column(name = "INDEPENDENCIA")
    public String getIndependencia() {
        return independencia.get();
    }

    public void setIndependencia(String value) {
        independencia.set(value);
    }

    public StringProperty independenciaProperty() {
        return independencia;
    }

}
