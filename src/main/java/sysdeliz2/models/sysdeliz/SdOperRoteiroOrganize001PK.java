/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author cristiano.diego
 */
@Embeddable
public class SdOperRoteiroOrganize001PK extends BasicModel implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final IntegerProperty roteiro = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<SdOperacaoOrganize001>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    
    public SdOperRoteiroOrganize001PK() {
    }
    
    public SdOperRoteiroOrganize001PK(Integer roteiro, Integer ordem, SdOperacaoOrganize001 operacao) {
        this.roteiro.set(roteiro);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
    }

    @Column(name = "ROTEIRO")
    public final int getRoteiro() {
        return roteiro.get();
    }

    public final void setRoteiro(int value) {
        roteiro.set(value);
    }

    public IntegerProperty roteiroProperty() {
        return roteiro;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public final SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }

    public final void setOperacao(SdOperacaoOrganize001 value) {
        operacao.set(value);
    }

    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }
    
    @Column(name = "ORDEM")
    public final int getOrdem() {
        return ordem.get();
    }

    public final void setOrdem(int value) {
        ordem.set(value);
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
