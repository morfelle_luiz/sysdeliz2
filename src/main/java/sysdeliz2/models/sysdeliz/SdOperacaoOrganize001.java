/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_OPERACAO_ORGANIZE_001")
@TelaSysDeliz(descricao = "Operações Organize", icon = "operacao costura (4).png")
public class SdOperacaoOrganize001 extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codorg")
    private final IntegerProperty codorg = new SimpleIntegerProperty();
    @Transient
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 400)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Tempo", width = 80)
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> tempoHora = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> concessao = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty equipamento = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Equipamento", width = 150)
    @ColunaFilter(descricao = "Equipamento", coluna = "equipamentoOrganize.codigo", filterClass = "sysdeliz2.models.sysdeliz.SdEquipamentosOrganize001")
    private final ObjectProperty<SdEquipamentosOrganize001> equipamentoOrganize = new SimpleObjectProperty<SdEquipamentosOrganize001>();
    @Transient
    private final StringProperty dtInclusao = new SimpleStringProperty();
    @Transient
    private final StringProperty dtAlteracao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> compCustura = new SimpleObjectProperty<BigDecimal>();

    public SdOperacaoOrganize001() {
    }
    
    public SdOperacaoOrganize001(Integer codorg) {
        this.codorg.set(codorg);
    }
    
    public SdOperacaoOrganize001(Integer codorg, String codigo, String descricao, Double tempo, Double tempoHora,
            Double custo, Double concessao, String equipamento, String dtInclusao, String dtAlteracao, Double compCustura) {
        this.codorg.set(codorg);
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.tempo.set(BigDecimal.valueOf(tempo));
        this.tempoHora.set(BigDecimal.valueOf(tempoHora));
        this.custo.set(BigDecimal.valueOf(custo));
        this.concessao.set(BigDecimal.valueOf(concessao));
        this.equipamento.set(equipamento);
        this.dtInclusao.set(dtInclusao);
        this.dtAlteracao.set(dtAlteracao == null ? dtInclusao : dtAlteracao);
        this.compCustura.set(BigDecimal.valueOf(compCustura));
    }

    public SdOperacaoOrganize001(Integer codorg, String codigo, String descricao, Double tempo, Double tempoHora,
            Double custo, Double concessao, String equipamento, String dtInclusao, String dtAlteracao, Double compCustura, SdEquipamentosOrganize001 equipamentoOrganize) {
        this.codorg.set(codorg);
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.tempo.set(BigDecimal.valueOf(tempo));
        this.tempoHora.set(BigDecimal.valueOf(tempoHora));
        this.custo.set(BigDecimal.valueOf(custo));
        this.concessao.set(BigDecimal.valueOf(concessao));
        this.equipamento.set(equipamento);
        this.dtInclusao.set(dtInclusao);
        this.dtAlteracao.set(dtAlteracao == null ? dtInclusao : dtAlteracao);
        this.compCustura.set(BigDecimal.valueOf(compCustura));
        this.equipamentoOrganize.set(equipamentoOrganize);
    }

    @Id
    @Column(name = "CODORG")
    public int getCodorg() {
        return codorg.get();
    }

    public void setCodorg(int value) {
        codorg.set(value);
        this.setCodigoFilter(String.valueOf(value));
    }

    public IntegerProperty codorgProperty() {
        return codorg;
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String value) {
        descricao.set(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Column(name = "TEMPO")
    public BigDecimal getTempo() {
        return tempo.get();
    }

    public void setTempo(BigDecimal value) {
        tempo.set(value);
    }

    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }
    
    @Column(name = "TEMPO_HORA")
    public BigDecimal getTempoHora() {
        return tempoHora.get();
    }

    public void setTempoHora(BigDecimal value) {
        tempoHora.set(value);
    }

    public ObjectProperty<BigDecimal> tempoHoraProperty() {
        return tempoHora;
    }
    
    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public void setCusto(BigDecimal value) {
        custo.set(value);
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }
    
    @Column(name = "CONCESSAO")
    public BigDecimal getConcessao() {
        return concessao.get();
    }

    public void setConcessao(BigDecimal value) {
        concessao.set(value);
    }

    public ObjectProperty<BigDecimal> concessaoProperty() {
        return concessao;
    }
    
    @Transient
    public final String getEquipamento() {
        return equipamento.get();
    }

    public final void setEquipamento(String value) {
        equipamento.set(value);
    }

    public StringProperty equipamentoProperty() {
        return equipamento;
    }
    
    @Column(name = "DT_INCLUSAO")
    public String getDtInclusao() {
        return dtInclusao.get();
    }

    public void setDtInclusao(String value) {
        dtInclusao.set(value);
    }

    public StringProperty dtInclusaoProperty() {
        return dtInclusao;
    }
    
    @Column(name = "DT_ALTERACAO")
    public String getDtAlteracao() {
        return dtAlteracao.get();
    }

    public void setDtAlteracao(String value) {
        dtAlteracao.set(value);
    }

    public StringProperty dtAlteracaoProperty() {
        return dtAlteracao;
    }
    
    @Column(name = "COMP_CUSTURA")
    public BigDecimal getCompCustura() {
        return compCustura.get();
    }

    public void setCompCustura(BigDecimal value) {
        compCustura.set(value);
    }

    public ObjectProperty<BigDecimal> compCusturaProperty() {
        return compCustura;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="EQUIPAMENTO")
    public SdEquipamentosOrganize001 getEquipamentoOrganize() {
        return equipamentoOrganize.get();
    }

    public void setEquipamentoOrganize(SdEquipamentosOrganize001 value) {
        equipamentoOrganize.set(value);
    }

    public ObjectProperty<SdEquipamentosOrganize001> equipamentoOrganizeProperty() {

        return equipamentoOrganize;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdOperacaoOrganize001 other = (SdOperacaoOrganize001) obj;
        if (!Objects.equals(this.codorg, other.codorg)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + codorg.get() + "] " + descricao.get();
    }

}
