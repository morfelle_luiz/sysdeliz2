/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;

/**
 * @author cristiano.diego
 */
@Entity
@Table(name="SD_PACOTE_001")
public class SdPacote001 {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty tipoLancamento = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<SdProgramacaoOf001> sdProgramacaoOf = new SimpleObjectProperty<>();
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
    
    private final BooleanProperty pacoteProgramado = new SimpleBooleanProperty(false);
    
    public SdPacote001() {
    }
    
    public SdPacote001(String codigo, Integer ordem, String numero, String referencia, String cor, String tam, Integer qtde, String tipo, String tipoLancamento) {
        this.ordem.set(ordem);
        this.numero.set(numero);
        this.referencia.set(referencia);
        this.cor.set(cor);
        this.tam.set(tam);
        this.qtde.set(qtde);
        this.codigo.set(codigo);
        this.tipo.set(tipo);
        this.tipoLancamento.set(tipoLancamento);
    }
    
    public SdPacote001(String codigo, Integer ordem, String numero, String referencia, String cor, String descCor, String tam, Integer qtde, String tipo, String tipoLancamento) {
        this.ordem.set(ordem);
        this.numero.set(numero);
        this.referencia.set(referencia);
        this.cor.set(cor);
        this.descCor.set("[" + cor + "] " + descCor);
        this.tam.set(tam);
        this.qtde.set(qtde);
        this.codigo.set(codigo);
        this.tipo.set(tipo);
        this.tipoLancamento.set(tipoLancamento);
    }
    
    public SdPacote001(Integer ordem, String numero, String referencia, String cor, String tam, Integer qtde, String tipo, String tipoLancamento, String setor) {
        this.ordem.set(ordem);
        this.numero.set(numero);
        this.referencia.set(referencia);
        this.cor.set(cor);
        this.tam.set(tam);
        this.qtde.set(qtde);
        this.codigo.set(numero + StringUtils.leftPad(setor, 3, "0") + StringUtils.leftPad(ordem.toString(), 2, "0"));
        this.tipo.set(tipo);
        this.tipoLancamento.set(tipoLancamento);
    }
    
    @Id
    @Column(name="CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public void setCodigo(String value) {
        codigo.set(value);
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "NUMERO")
    public SdProgramacaoOf001 getSdProgramacaoOf() {
        return sdProgramacaoOf.get();
    }
    
    public ObjectProperty<SdProgramacaoOf001> sdProgramacaoOfProperty() {
        return sdProgramacaoOf;
    }
    
    public void setSdProgramacaoOf(SdProgramacaoOf001 sdProgramacaoOf) {
        setNumero(sdProgramacaoOf.getOrdemProd());
        this.sdProgramacaoOf.set(sdProgramacaoOf);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public void setOrdem(int value) {
        ordem.set(value);
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    @Transient
    public final String getNumero() {
        return numero.get();
    }
    
    public final void setNumero(String value) {
        numero.set(value);
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "REFERENCIA")
    public Produto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        setReferencia(produto.getCodigo());
        this.produto.set(produto);
    }
    
    @Transient
    public final String getReferencia() {
        return referencia.get();
    }
    
    public final void setReferencia(String value) {
        referencia.set(value);
    }
    
    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    @Column(name="COR")
    public String getCor() {
        return cor.get();
    }
    
    public void setCor(String value) {
        cor.set(value);
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    @Column(name="TAM")
    public String getTam() {
        return tam.get();
    }
    
    public void setTam(String value) {
        tam.set(value);
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    @Column(name="QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public void setQtde(int value) {
        qtde.set(value);
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    @Column(name="TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public void setTipo(String value) {
        tipo.set(value);
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    @Column(name="TIPO_LANC")
    public String getTipoLancamento() {
        return tipoLancamento.get();
    }
    
    public void setTipoLancamento(String value) {
        tipoLancamento.set(value);
    }
    
    public StringProperty tipoLancamentoProperty() {
        return tipoLancamento;
    }
    
    @Transient
    public String getDescCor() {
        return descCor.get();
    }
    
    public StringProperty descCorProperty() {
        return descCor;
    }
    
    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }
    
    @Transient
    public boolean isPacoteProgramado() {
        return pacoteProgramado.get();
    }
    
    public BooleanProperty pacoteProgramadoProperty() {
        return pacoteProgramado;
    }
    
    public void setPacoteProgramado(boolean pacoteProgramado) {
        this.pacoteProgramado.set(pacoteProgramado);
    }
    
    @Override
    public String toString() {
        return "PCT " + ordem.get() + " [" + numero.get() + "]";
    }
    
    @Transient
    public String getDescricao() {
        return "PCT " + this.ordem.get() + ": "
                + this.referencia.get()
                + (this.cor.isNotNull().get() ? " - " + this.cor.get() : "")
                + (this.tam.isNotNull().get() ? " - " + this.tam.get() : "");
    }
}
