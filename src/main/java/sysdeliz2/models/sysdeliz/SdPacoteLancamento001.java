/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cristiano.diego
 */
@Entity
@Table(name="SD_PACOTE_LANCAMENTO_001")
public class SdPacoteLancamento001 implements Serializable {
    
    private final ObjectProperty<SdPacoteLancamento001PK> id = new SimpleObjectProperty<>();
    private final StringProperty referencia = new SimpleStringProperty();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    
    public SdPacoteLancamento001() {
    }
    
    public SdPacoteLancamento001(String pacoteProgramacao, Integer pacoteLancamento) {
        this.id.set(new SdPacoteLancamento001PK(pacoteProgramacao, pacoteLancamento));
    }
    
    @EmbeddedId
    public SdPacoteLancamento001PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdPacoteLancamento001PK> idProperty() {
        return id;
    }
    
    public void setId(SdPacoteLancamento001PK id) {
        this.id.set(id);
    }
    
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }
    
    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR", nullable = true)
    public Cor getCor() {
        return cor.get();
    }
    
    public ObjectProperty<Cor> corProperty() {
        return cor;
    }
    
    public void setCor(Cor cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Override
    public String toString() {
        return String.valueOf(id.get().getOrdem());
    }
}
