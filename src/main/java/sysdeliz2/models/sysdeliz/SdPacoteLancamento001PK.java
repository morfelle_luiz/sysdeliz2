/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author cristiano.diego
 */
@Embeddable
public class SdPacoteLancamento001PK implements Serializable {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    
    public SdPacoteLancamento001PK() {
    }
    
    public SdPacoteLancamento001PK(String pacoteProgramacao, Integer pacoteLancamento) {
        this.codigo.set(pacoteProgramacao);
        this.ordem.set(pacoteLancamento);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
