package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_PARADAS_TURNO_001")
public class SdParadasTurno001 {
    
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final IntegerProperty turno = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> inicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> fim = new SimpleObjectProperty<>();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty tempoIntervalo = new SimpleIntegerProperty();
    
    public SdParadasTurno001() {
    }
    
    public SdParadasTurno001(LocalDateTime inicio, LocalDateTime fim, String descricao, Integer tempoIntervalo) {
        this.inicio.set(inicio);
        this.fim.set(fim);
        this.descricao.set(descricao);
        this.tempoIntervalo.set(tempoIntervalo);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ai_codigo_paradas_turno")
    @SequenceGenerator(name="ai_codigo_paradas_turno", sequenceName="seq_sd_paradas_turno", allocationSize = 1)
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "TURNO")
    public int getTurno() {
        return turno.get();
    }
    
    public IntegerProperty turnoProperty() {
        return turno;
    }
    
    public void setTurno(int turno) {
        this.turno.set(turno);
    }
    
    @Column(name = "INICIO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getInicio() {
        return inicio.get();
    }
    
    public ObjectProperty<LocalDateTime> inicioProperty() {
        return inicio;
    }
    
    public void setInicio(LocalDateTime inicio) {
        this.inicio.set(inicio);
    }
    
    @Column(name = "FIM")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getFim() {
        return fim.get();
    }
    
    public ObjectProperty<LocalDateTime> fimProperty() {
        return fim;
    }
    
    public void setFim(LocalDateTime fim) {
        this.fim.set(fim);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name = "TEMPO_INTERVALO")
    public int getTempoIntervalo() {
        return tempoIntervalo.get();
    }
    
    public IntegerProperty tempoIntervaloProperty() {
        return tempoIntervalo;
    }
    
    public void setTempoIntervalo(int tempoIntervalo) {
        this.tempoIntervalo.set(tempoIntervalo);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] "+ descricao.get() + " - TEMPO: " + tempoIntervalo.get();
    }
}
