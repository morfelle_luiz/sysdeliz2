package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "SD_PCA_CODREP_MARCA_001")
public class SdPcaCodRepMarca implements Serializable {

    private final StringProperty codRep = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final IntegerProperty pCurtoMes = new SimpleIntegerProperty(0);
    private final IntegerProperty pCurtoCid = new SimpleIntegerProperty(0);
    private final IntegerProperty pMedioMes = new SimpleIntegerProperty(0);
    private final IntegerProperty pMedioCid = new SimpleIntegerProperty(0);
    private final IntegerProperty pLongoMes = new SimpleIntegerProperty(0);
    private final IntegerProperty pLongoCid = new SimpleIntegerProperty(0);
    private final ObjectProperty<LocalDate> dtCriacao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtUltAtualizacao = new SimpleObjectProperty<>();

    public SdPcaCodRepMarca() {
    }

    public SdPcaCodRepMarca(String codrep, int pCurtoMes, int pCurtoCid, int pMedioMes, int pMedioCid, int pLongoMes, int pLongoCid) {
        this.codRep.set(codrep);
        this.pCurtoMes.set(pCurtoMes);
        this.pCurtoCid.set(pCurtoCid);
        this.pMedioMes.set(pMedioMes);
        this.pMedioCid.set(pMedioCid);
        this.pLongoMes.set(pLongoMes);
        this.pLongoCid.set(pLongoCid);
    }

    @Id
    @Column(name = "CODREP")
    public String getCodRep() {
        return codRep.get();
    }

    public StringProperty codRepProperty() {
        return codRep;
    }

    public void setCodRep(String codRep) {
        this.codRep.set(codRep);
    }

    @Id
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "P_CURTO_MES")
    public Integer getpCurtoMes() {
        return pCurtoMes.get();
    }

    public IntegerProperty pCurtoMesProperty() {
        return pCurtoMes;
    }

    public void setpCurtoMes(Integer pCurtoMes) {
        this.pCurtoMes.set(pCurtoMes);
    }

    @Column(name = "P_CURTO_CID")
    public Integer getpCurtoCid() {
        return pCurtoCid.get();
    }

    public IntegerProperty pCurtoCidProperty() {
        return pCurtoCid;
    }

    public void setpCurtoCid(Integer pCurtoCid) {
        this.pCurtoCid.set(pCurtoCid);
    }

    @Column(name = "P_MEDIO_MES")
    public Integer getpMedioMes() {
        return pMedioMes.get();
    }

    public IntegerProperty pMedioMesProperty() {
        return pMedioMes;
    }

    public void setpMedioMes(Integer pMedioMes) {
        this.pMedioMes.set(pMedioMes);
    }

    @Column(name = "P_MEDIO_CID")
    public Integer getpMedioCid() {
        return pMedioCid.get();
    }

    public IntegerProperty pMedioCidProperty() {
        return pMedioCid;
    }

    public void setpMedioCid(Integer pMedioCid) {
        this.pMedioCid.set(pMedioCid);
    }

    @Column(name = "P_LONGO_MES")
    public Integer getpLongoMes() {
        return pLongoMes.get();
    }

    public IntegerProperty pLongoMesProperty() {
        return pLongoMes;
    }

    public void setpLongoMes(Integer pLongoMes) {
        this.pLongoMes.set(pLongoMes);
    }

    @Column(name = "P_LONGO_CID")
    public Integer getpLongoCid() {
        return pLongoCid.get();
    }

    public IntegerProperty pLongoCidProperty() {
        return pLongoCid;
    }

    public void setpLongoCid(Integer pLongoCid) {
        this.pLongoCid.set(pLongoCid);
    }

    @Column(name = "DT_CRIACAO")
    public LocalDate getDtCriacao() {
        return dtCriacao.get();
    }

    public ObjectProperty<LocalDate> dtCriacaoProperty() {
        return dtCriacao;
    }

    public void setDtCriacao(LocalDate dtCriacao) {
        this.dtCriacao.set(dtCriacao);
    }

    @Column(name = "DT_ULT_ATUALIZACAO")
    public LocalDate getDtUltAtualizacao() {
        return dtUltAtualizacao.get();
    }

    public ObjectProperty<LocalDate> dtUltAtualizacaoProperty() {
        return dtUltAtualizacao;
    }

    public void setDtUltAtualizacao(LocalDate dtUltAtualizacao) {
        this.dtUltAtualizacao.set(dtUltAtualizacao);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdPcaCodRepMarca that = (SdPcaCodRepMarca) o;
        return Objects.equals(codRep, that.codRep) && Objects.equals(marca, that.marca);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codRep, marca);
    }
}
