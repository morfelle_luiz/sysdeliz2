package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Nota;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_PEDIDOS_REMESSA_001")
public class SdPedidosRemessa {
    
    private final ObjectProperty<SdPedidosRemessaPK> id = new SimpleObjectProperty<>();
    private final StringProperty marca = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdec = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private List<SdItensPedidoRemessa> itens = new ArrayList<>();
    private final ObjectProperty<Nota> nota = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtBase = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> vlrDesc = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    
    public SdPedidosRemessa() {
    }
    
    public SdPedidosRemessa(Integer remessa, Pedido numero, String marca, LocalDate dtentrega, Integer qtde, BigDecimal valor) {
        this.id.set(new SdPedidosRemessaPK(remessa, numero));
        this.marca.set(marca);
        this.dtentrega.set(dtentrega);
        this.qtde.set(qtde);
        this.valor.set(valor);
    }
    
    @EmbeddedId
    public SdPedidosRemessaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdPedidosRemessaPK> idProperty() {
        return id;
    }
    
    public void setId(SdPedidosRemessaPK id) {
        this.id.set(id);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "REMESSA", referencedColumnName = "REMESSA"),
            @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO")
    })
    @OrderBy("id.codigo.codigo ASC, id.cor ASC")
    public List<SdItensPedidoRemessa> getItens() {
        return itens;
    }
    
    public void setItens(List<SdItensPedidoRemessa> itens) {
        this.itens = itens;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NOTA")
    public Nota getNota() {
        return nota.get();
    }
    
    public ObjectProperty<Nota> notaProperty() {
        return nota;
    }
    
    public void setNota(Nota nota) {
        this.nota.set(nota);
    }
    
    @Column(name = "DT_BASE")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtBase() {
        return dtBase.get();
    }
    
    public ObjectProperty<LocalDate> dtBaseProperty() {
        return dtBase;
    }
    
    public void setDtBase(LocalDate dtBase) {
        this.dtBase.set(dtBase);
    }
    
    @Column(name = "VLR_DESC")
    public BigDecimal getVlrDesc() {
        return vlrDesc.get();
    }
    
    public ObjectProperty<BigDecimal> vlrDescProperty() {
        return vlrDesc;
    }
    
    public void setVlrDesc(BigDecimal vlrDesc) {
        this.vlrDesc.set(vlrDesc);
    }
    
    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}