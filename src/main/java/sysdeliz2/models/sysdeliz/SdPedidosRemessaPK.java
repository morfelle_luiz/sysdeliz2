package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.converters.RemessaAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdPedidosRemessaPK implements Serializable {
    
    private final IntegerProperty remessa = new SimpleIntegerProperty();
    private final ObjectProperty<Pedido> numero = new SimpleObjectProperty<>();
    
    public SdPedidosRemessaPK() {
    }
    
    public SdPedidosRemessaPK(Integer remessa, Pedido numero) {
        this.remessa.set(remessa);
        this.numero.set(numero);
    }
    
    @Column(name = "REMESSA")
    @Convert(converter = RemessaAttributeConverter.class)
    public Integer getRemessa() {
        return remessa.get();
    }
    
    public IntegerProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(Integer remessa) {
        this.remessa.set(remessa);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    public Pedido getNumero() {
        return numero.get();
    }
    
    public ObjectProperty<Pedido> numeroProperty() {
        return numero;
    }
    
    public void setNumero(Pedido numero) {
        this.numero.set(numero);
    }
}