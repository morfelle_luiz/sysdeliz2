package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sd_pesos_campos_ord_001")
@IdClass(SdPesosCamposOrdPk.class)
public class SdPesosCamposOrd implements Serializable {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty atributo = new SimpleStringProperty();
    private final StringProperty modulo = new SimpleStringProperty();
    private final StringProperty classe = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty peso = new SimpleIntegerProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public SdPesosCamposOrd() {
    }

    @Id
    @Column(name = "codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "atributo")
    public String getAtributo() {
        return atributo.get();
    }

    public StringProperty atributoProperty() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo.set(atributo);
    }

    @Id
    @Column(name = "classe")
    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    @Id
    @Column(name = "modulo")
    public String getModulo() {
        return modulo.get();
    }

    public StringProperty moduloProperty() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo.set(modulo);
    }

    @Column(name = "descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "peso")
    public int getPeso() {
        return peso.get();
    }

    public IntegerProperty pesoProperty() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso.set(peso);
    }

    @Column(name = "tipo")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
}
