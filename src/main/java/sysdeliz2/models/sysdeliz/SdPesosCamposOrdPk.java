package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;

public class SdPesosCamposOrdPk implements Serializable {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty atributo = new SimpleStringProperty();
    private final StringProperty classe = new SimpleStringProperty();
    private final StringProperty modulo = new SimpleStringProperty();

    public SdPesosCamposOrdPk() {
    }

    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    public String getAtributo() {
        return atributo.get();
    }

    public StringProperty atributoProperty() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo.set(atributo);
    }

    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    public String getModulo() {
        return modulo.get();
    }

    public StringProperty moduloProperty() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo.set(modulo);
    }
}
