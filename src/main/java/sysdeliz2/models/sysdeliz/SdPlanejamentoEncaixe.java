package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ti.Of2;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_PLANEJAMENTO_ENCAIXE_001")
public class SdPlanejamentoEncaixe implements Serializable {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<Of2> numero = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtplano = new SimpleObjectProperty<>(LocalDateTime.now());
    private final StringProperty usuario = new SimpleStringProperty(Globals.getUsuarioLogado() != null ? Globals.getUsuarioLogado().getUsuario() : null);
    private final BooleanProperty gabarito = new SimpleBooleanProperty(false);
    private final ListProperty<SdRiscoPlanejamentoEncaixe> riscosObservable = new SimpleListProperty<>();
    private List<SdRiscoPlanejamentoEncaixe> riscos = new ArrayList<>();
    private final BooleanProperty realcortado = new SimpleBooleanProperty();
    private final BooleanProperty tubular = new SimpleBooleanProperty();
    private final BooleanProperty finalizado = new SimpleBooleanProperty(false);
    
    public SdPlanejamentoEncaixe() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_PLANEJAMENTO_ENCAIXE")
    @SequenceGenerator(name = "SEQ_SD_PLANEJAMENTO_ENCAIXE", sequenceName = "SEQ_SD_PLANEJAMENTO_ENCAIXE", allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(int id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "NUMERO")
    public Of2 getNumero() {
        return numero.get();
    }
    
    public ObjectProperty<Of2> numeroProperty() {
        return numero;
    }
    
    public void setNumero(Of2 numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "DT_PLANO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtplano() {
        return dtplano.get();
    }
    
    public ObjectProperty<LocalDateTime> dtplanoProperty() {
        return dtplano;
    }
    
    public void setDtplano(LocalDateTime dtplano) {
        this.dtplano.set(dtplano);
    }
    
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }
    
    public StringProperty usuarioProperty() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }
    
    @Column(name = "GABARITO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isGabarito() {
        return gabarito.get();
    }
    
    public BooleanProperty gabaritoProperty() {
        return gabarito;
    }
    
    public void setGabarito(boolean gabarito) {
        this.gabarito.set(gabarito);
    }
    
    @Transient
    public ObservableList<SdRiscoPlanejamentoEncaixe> getRiscosObservable() {
        riscosObservable.set(FXCollections.observableList(riscos));
        return riscosObservable.get();
    }
    
    public ListProperty<SdRiscoPlanejamentoEncaixe> riscosObservableProperty() {
        riscosObservable.set(FXCollections.observableList(riscos));
        return riscosObservable;
    }
    
    public void setRiscosObservable(ObservableList<SdRiscoPlanejamentoEncaixe> riscosObservable) {
        this.riscosObservable.set(riscosObservable);
    }
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLANEJAMENTO", referencedColumnName = "ID")
    public List<SdRiscoPlanejamentoEncaixe> getRiscos() {
        return riscos;
    }
    
    public void setRiscos(List<SdRiscoPlanejamentoEncaixe> riscos) {
        this.riscos = riscos;
    }
    
    @Column(name = "REAL_CORTADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isRealcortado() {
        return realcortado.get();
    }
    
    public BooleanProperty realcortadoProperty() {
        return realcortado;
    }
    
    public void setRealcortado(boolean realcortado) {
        this.realcortado.set(realcortado);
    }
    
    @Column(name = "TUBULAR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isTubular() {
        return tubular.get();
    }
    
    public BooleanProperty tubularProperty() {
        return tubular;
    }
    
    public void setTubular(boolean tubular) {
        this.tubular.set(tubular);
    }
    
    @Column(name = "FINALIZADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFinalizado() {
        return finalizado.get();
    }
    
    public BooleanProperty finalizadoProperty() {
        return finalizado;
    }
    
    public void setFinalizado(boolean finalizado) {
        this.finalizado.set(finalizado);
    }
}
