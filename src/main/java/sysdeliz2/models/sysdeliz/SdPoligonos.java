package sysdeliz2.models.sysdeliz;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 25/07/2019 08:13
 */
@Entity
@Table(name="SD_POLIGONOS_001")
public class SdPoligonos {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SD_SEQ_POLIGONOS_001")
    @SequenceGenerator(name="SD_SEQ_POLIGONOS_001", sequenceName="SD_SEQ_POLIGONOS_001", allocationSize=1)
    @Column(name="CODIGO")
    private Integer codigo;

    @Column(name="LATITUDE")
    private BigDecimal latitude;
    @Column(name="LONGITUDE")
    private BigDecimal longitude;
    @Column(name="REGIAO_ID")
    private Integer regiaoId;
    @Column(name="CIDADE_ID")
    private Integer cidadeId;
    @Column(name="UF_ID")
    private String ufId;
    @Column(name="UF_SIGLA")
    private String ufSigla;
    @Column(name="POSICAO")
    private Integer posicao;

    public SdPoligonos() {
    }

    public SdPoligonos(BigDecimal latitude, BigDecimal longitude, Integer regiaoId, Integer cidadeId, String ufId, String ufSigla, Integer posicao) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.regiaoId = regiaoId;
        this.cidadeId = cidadeId;
        this.ufId = ufId;
        this.ufSigla = ufSigla;
        this.posicao = posicao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Integer getRegiaoId() {
        return regiaoId;
    }

    public void setRegiaoId(Integer regiaoId) {
        this.regiaoId = regiaoId;
    }

    public Integer getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(Integer cidadeId) {
        this.cidadeId = cidadeId;
    }

    public String getUfId() {
        return ufId;
    }

    public void setUfId(String ufId) {
        this.ufId = ufId;
    }

    public String getUfSigla() {
        return ufSigla;
    }

    public void setUfSigla(String ufSigla) {
        this.ufSigla = ufSigla;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public String toSqlString(){
        return "INSERT INTO SD_POLIGONOS_001 (LATITUDE, LONGITUDE, POSICAO, UF_ID, UF_SIGLA) VALUES (" +
                this.getLatitude() + "," +
                this.getLongitude() + "," +
                this.getPosicao() + ",'" +
                this.getUfId() + "','" +
                this.getUfSigla() + "');\n";
    }

}
