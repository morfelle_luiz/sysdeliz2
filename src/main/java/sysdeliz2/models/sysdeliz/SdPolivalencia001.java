/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_POLIVALENCIA_001")
public class SdPolivalencia001 {
    
    private final ObjectProperty<SdPolivalencia001PK> id = new SimpleObjectProperty<>();
    private final IntegerProperty codigoColaborador = new SimpleIntegerProperty();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final IntegerProperty codigoOperacao = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();
    private final IntegerProperty codigoNivel = new SimpleIntegerProperty();
    private final ObjectProperty<SdNivelPolivalencia001> nivel = new SimpleObjectProperty<>();
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);

    public SdPolivalencia001() {
    }

    public SdPolivalencia001(Integer colaborador, Integer operacao, Integer nivel, String ativo) {
        this.codigoColaborador.set(colaborador);
        this.codigoOperacao.set(operacao);
        this.codigoNivel.set(nivel);
        this.ativo.set(ativo.equals("S"));
    }

    public SdPolivalencia001(Integer codigoColaborador, Integer codigoOperacao, Integer codigoNivel, String ativo,
                             SdColaborador colaborador, SdOperacaoOrganize001 operacao, SdNivelPolivalencia001 nivel) {
        this.codigoColaborador.set(codigoColaborador);
        this.codigoOperacao.set(codigoOperacao);
        this.codigoNivel.set(codigoNivel);
        this.ativo.set(ativo.equals("S"));
        this.colaborador.set(colaborador);
        this.operacao.set(operacao);
        this.nivel.set(nivel);
    }
    
    @EmbeddedId
    public SdPolivalencia001PK getId() {
        return id.get();
    }
    public ObjectProperty<SdPolivalencia001PK> idProperty() {
        return id;
    }
    public void setId(SdPolivalencia001PK id) {
        this.id.set(id);
    }
    
    @Transient
    public final int getCodigoColaborador() {
        return codigoColaborador.get();
    }
    public final void setCodigoColaborador(int value) {
        codigoColaborador.set(value);
    }
    public IntegerProperty codigoColaboradorProperty() {
        return codigoColaborador;
    }

    @Transient
    public final SdColaborador getColaborador() {
        return colaborador.get();
    }
    public final void setColaborador(SdColaborador value) {
        colaborador.set(value);
    }
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    @Transient
    public final int getCodigoOperacao() {
        return codigoOperacao.get();
    }
    public final void setCodigoOperacao(int value) {
        codigoOperacao.set(value);
    }
    public IntegerProperty codigoOperacaoProperty() {
        return codigoOperacao;
    }
    
    @Transient
    public final SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }
    public final void setOperacao(SdOperacaoOrganize001 value) {
        operacao.set(value);
    }
    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }

    @Transient
    public final int getCodigoNivel() {
        return codigoNivel.get();
    }
    public final void setCodigoNivel(int value) {
        codigoNivel.set(value);
    }
    public IntegerProperty codigoNivelProperty() {
        return codigoNivel;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "NIVEL")
    public SdNivelPolivalencia001 getNivel() {
        return nivel.get();
    }
    public void setNivel(SdNivelPolivalencia001 value) {
        nivel.set(value);
    }
    public ObjectProperty<SdNivelPolivalencia001> nivelProperty() {
        return nivel;
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public void setAtivo(boolean value) {
        ativo.set(value);
    }
    public BooleanProperty ativoProperty() {
        return ativo;
    }

}
