package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdPolivalencia001PK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();
    
    public SdPolivalencia001PK() {
    }
    
    public SdPolivalencia001PK(SdColaborador colaborador, SdOperacaoOrganize001 operacao) {
        this.colaborador.set(colaborador);
        this.operacao.set(operacao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }
    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }
    public void setOperacao(SdOperacaoOrganize001 operacao) {
        this.operacao.set(operacao);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
