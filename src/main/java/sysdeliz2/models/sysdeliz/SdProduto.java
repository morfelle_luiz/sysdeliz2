package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "SD_PRODUTO_001")
public class SdProduto {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty observacaopcp = new SimpleStringProperty();
    private final StringProperty expedicao = new SimpleStringProperty();
    private final BooleanProperty unicocaixa = new SimpleBooleanProperty();
    private final StringProperty lojastatus = new SimpleStringProperty();
    private final StringProperty metatitle = new SimpleStringProperty();
    private final StringProperty metakeyword = new SimpleStringProperty();
    private final StringProperty metadescription = new SimpleStringProperty();
    private final StringProperty description = new SimpleStringProperty();
    private final StringProperty shortdescription = new SimpleStringProperty();
    private final StringProperty statusprocessamento = new SimpleStringProperty();
    private final StringProperty longdescription = new SimpleStringProperty();
    private final StringProperty tabelamedida = new SimpleStringProperty();
    private final StringProperty video = new SimpleStringProperty();
    private final StringProperty statusb2b = new SimpleStringProperty();
    private final IntegerProperty partesmodelagem = new SimpleIntegerProperty();
    private final BooleanProperty situacaoLoja = new SimpleBooleanProperty(true);
    private final BooleanProperty configuravel = new SimpleBooleanProperty(false);
    private final BooleanProperty validaPedido = new SimpleBooleanProperty(false);
    private final BooleanProperty baixaEstoque = new SimpleBooleanProperty(true);

    public SdProduto() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "OBSERVACAO_PCP")
    @Lob
    public String getObservacaopcp() {
        return observacaopcp.get();
    }
    
    public StringProperty observacaopcpProperty() {
        return observacaopcp;
    }
    
    public void setObservacaopcp(String observacaopcp) {
        this.observacaopcp.set(observacaopcp);
    }
    
    @Column(name = "EXPEDICAO")
    @Lob
    public String getExpedicao() {
        return expedicao.get();
    }
    
    public StringProperty expedicaoProperty() {
        return expedicao;
    }
    
    public void setExpedicao(String expedicao) {
        this.expedicao.set(expedicao);
    }
    
    @Column(name = "UNICO_CAIXA")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getUnicocaixa() {
        return unicocaixa.get();
    }
    
    public BooleanProperty unicocaixaProperty() {
        return unicocaixa;
    }
    
    public void setUnicocaixa(Boolean unicocaixa) {
        this.unicocaixa.set(unicocaixa);
    }
    
    @Column(name = "LOJA_STATUS")
    public String getLojastatus() {
        return lojastatus.get();
    }
    
    public StringProperty lojastatusProperty() {
        return lojastatus;
    }
    
    public void setLojastatus(String lojastatus) {
        this.lojastatus.set(lojastatus);
    }
    
    @Column(name = "META_TITLE")
    @Lob
    public String getMetatitle() {
        return metatitle.get();
    }
    
    public StringProperty metatitleProperty() {
        return metatitle;
    }
    
    public void setMetatitle(String metatitle) {
        this.metatitle.set(metatitle);
    }
    
    @Column(name = "META_KEYWORD")
    @Lob
    public String getMetakeyword() {
        return metakeyword.get();
    }
    
    public StringProperty metakeywordProperty() {
        return metakeyword;
    }
    
    public void setMetakeyword(String metakeyword) {
        this.metakeyword.set(metakeyword);
    }
    
    @Column(name = "META_DESCRIPTION")
    @Lob
    public String getMetadescription() {
        return metadescription.get();
    }
    
    public StringProperty metadescriptionProperty() {
        return metadescription;
    }
    
    public void setMetadescription(String metadescription) {
        this.metadescription.set(metadescription);
    }
    
    @Column(name = "DESCRIPTION")
    @Lob
    public String getDescription() {
        return description.get();
    }
    
    public StringProperty descriptionProperty() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description.set(description);
    }
    
    @Column(name = "SHORT_DESCRIPTION")
    @Lob
    public String getShortdescription() {
        return shortdescription.get();
    }
    
    public StringProperty shortdescriptionProperty() {
        return shortdescription;
    }
    
    public void setShortdescription(String shortdescription) {
        this.shortdescription.set(shortdescription);
    }
    
    @Column(name = "STATUS_PROCESSAMENTO")
    public String getStatusprocessamento() {
        return statusprocessamento.get();
    }
    
    public StringProperty statusprocessamentoProperty() {
        return statusprocessamento;
    }
    
    public void setStatusprocessamento(String statusprocessamento) {
        this.statusprocessamento.set(statusprocessamento);
    }

    @Column(name = "LONG_DESCRIPTION")
    @Lob
    public String getLongdescription() {
        return longdescription.get();
    }
    
    public StringProperty longdescriptionProperty() {
        return longdescription;
    }
    
    public void setLongdescription(String longdescription) {
        this.longdescription.set(longdescription);
    }
    
    @Column(name = "TABELA_MEDIDA")
    public String getTabelamedida() {
        return tabelamedida.get();
    }
    
    public StringProperty tabelamedidaProperty() {
        return tabelamedida;
    }
    
    public void setTabelamedida(String tabelamedida) {
        this.tabelamedida.set(tabelamedida);
    }

    @Column(name = "VIDEO")
    @Lob
    public String getVideo() {
        return video.get();
    }

    public StringProperty videoProperty() {
        return video;
    }

    public void setVideo(String video) {
        this.video.set(video);
    }

    @Column(name = "STATUS_B2B")
    public String getStatusb2b() {
        return statusb2b.get();
    }
    
    public StringProperty statusb2bProperty() {
        return statusb2b;
    }
    
    public void setStatusb2b(String statusb2b) {
        this.statusb2b.set(statusb2b);
    }
    
    @Column(name = "PARTES_MODELAGEM")
    public Integer getPartesmodelagem() {
        return partesmodelagem.get();
    }
    
    public IntegerProperty partesmodelagemProperty() {
        return partesmodelagem;
    }
    
    public void setPartesmodelagem(Integer partesmodelagem) {
        this.partesmodelagem.set(partesmodelagem);
    }

    @Column(name = "SITUACAO_LOJA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSituacaoLoja() {
        return situacaoLoja.get();
    }

    public BooleanProperty situacaoLojaProperty() {
        return situacaoLoja;
    }

    public void setSituacaoLoja(boolean situacaoLoja) {
        this.situacaoLoja.set(situacaoLoja);
    }

    @Column(name = "CONFIGURAVEL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isConfiguravel() {
        return configuravel.get();
    }

    public BooleanProperty configuravelProperty() {
        return configuravel;
    }

    public void setConfiguravel(boolean configuravel) {
        this.configuravel.set(configuravel);
    }

    @Column(name = "VALIDA_PEDIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isValidaPedido() {
        return validaPedido.get();
    }

    public BooleanProperty validaPedidoProperty() {
        return validaPedido;
    }

    public void setValidaPedido(boolean validaPedido) {
        this.validaPedido.set(validaPedido);
    }

    @Column(name = "BAIXA_ESTOQUE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isBaixaEstoque() {
        return baixaEstoque.get();
    }

    public BooleanProperty baixaEstoqueProperty() {
        return baixaEstoque;
    }

    public void setBaixaEstoque(boolean baixaEstoque) {
        this.baixaEstoque.set(baixaEstoque);
    }
}