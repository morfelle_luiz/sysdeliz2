package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_PRODUTO_COLECAO_001")
public class SdProdutoColecao extends BasicModel implements Serializable {
    
    private final ObjectProperty<SdProdutoColecaoPK> id = new SimpleObjectProperty<>();
    private final BooleanProperty pc = new SimpleBooleanProperty(true);

    private final IntegerProperty qtdeMostruario = new SimpleIntegerProperty(1);
    
    public SdProdutoColecao() {
    }
    
    public SdProdutoColecao(VSdDadosProduto codigo, Colecao colecao){
        this.setId(new SdProdutoColecaoPK(codigo, colecao));
    }
    
    @EmbeddedId
    public SdProdutoColecaoPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdProdutoColecaoPK> idProperty() {
        return id;
    }
    
    public void setId(SdProdutoColecaoPK id) {
        this.id.set(id);
    }
    
    @Column(name = "PC")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPc() {
        return pc.get();
    }
    
    public BooleanProperty pcProperty() {
        return pc;
    }
    
    public void setPc(boolean pc) {
        this.pc.set(pc);
    }

    @Transient
    public int getQtdeMostruario() {
        return qtdeMostruario.get();
    }

    public IntegerProperty qtdeMostruarioProperty() {
        return qtdeMostruario;
    }

    public void setQtdeMostruario(int qtdeMostruario) {
        this.qtdeMostruario.set(qtdeMostruario);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}
