package sysdeliz2.models.sysdeliz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SdProdutoColecaoPK implements Serializable {
    
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    
    public SdProdutoColecaoPK() {
    }
    
    public SdProdutoColecaoPK(VSdDadosProduto codigo, Colecao colecao) {
        this.setColecao(colecao);
        this.setCodigo(codigo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
}
