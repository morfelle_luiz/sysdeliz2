package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "SD_PROFORMA_001")
public class SdProforma {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty numeroNota = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pesoBruto = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty volumes = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> pesoLiquido = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valorProdutos = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorNota = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cofins = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> syscomex = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> dolar = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> totalDespesas = new SimpleObjectProperty<BigDecimal>();
    private List<SdItemProforma> itens = new ArrayList<>();
    private ObjectProperty<LocalDate> dataEmissao = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> dataSaida = new SimpleObjectProperty<>();
    private ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private StringProperty numeroDi = new SimpleStringProperty();
    private StringProperty refDi = new SimpleStringProperty();

    public SdProforma() {
    }

    public SdProforma(SdEmbarque embarque, BigDecimal pis, BigDecimal cofins, BigDecimal syscomex, BigDecimal frete, BigDecimal dolar, String numeroDi, String refDi) {
        this.pesoLiquido.set(embarque.getPeso());
        this.pesoBruto.set(this.pesoLiquido.getValue().add(new BigDecimal("0.02").multiply(embarque.getListItens().stream().reduce(BigDecimal.ZERO, (partial, it) -> partial.add(new BigDecimal(it.getListGrade().size())), BigDecimal::add))));
        this.tipo.set(embarque.getTipo());
        this.pis.set(pis);
        this.cofins.set(cofins);
        this.syscomex.set(syscomex);
        this.frete.set(frete);
        this.dolar.set(dolar);
        this.totalDespesas.set(pis.add(cofins).add(syscomex));

        this.dataEmissao.set(LocalDate.now());
        this.dataSaida.set(LocalDate.now());
        this.qtde.set(embarque.getQtde());
        this.numeroDi.set(numeroDi);
        this.refDi.set(refDi);
        this.setItens(carregaItens(embarque));
        this.volumes.set(this.getItens().stream().mapToInt(SdItemProforma::getVolumes).sum());
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_PROFORMA")
    @SequenceGenerator(name = "SEQ_SD_PROFORMA", sequenceName = "SEQ_SD_PROFORMA", allocationSize = 1)
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "NUMERO_NOTA")
    public String getNumeroNota() {
        return numeroNota.get();
    }

    public StringProperty numeroNotaProperty() {
        return numeroNota;
    }

    public void setNumeroNota(String numeroNota) {
        this.numeroNota.set(numeroNota);
    }

    @Column(name = "PESO_BRUTO")
    public BigDecimal getPesoBruto() {
        return pesoBruto.get();
    }

    public ObjectProperty<BigDecimal> pesoBrutoProperty() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto.set(pesoBruto);
    }

    @Column(name = "VOLUMES")
    public Integer getVolumes() {
        return volumes.get();
    }

    public IntegerProperty volumesProperty() {
        return volumes;
    }

    public void setVolumes(Integer volumes) {
        this.volumes.set(volumes);
    }

    @Column(name = "PESO_LIQUIDO")
    public BigDecimal getPesoLiquido() {
        return pesoLiquido.get();
    }

    public ObjectProperty<BigDecimal> pesoLiquidoProperty() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido.set(pesoLiquido);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "VALOR_PRODUTOS")
    public BigDecimal getValorProdutos() {
        return valorProdutos.get();
    }

    public ObjectProperty<BigDecimal> valorProdutosProperty() {
        return valorProdutos;
    }

    public void setValorProdutos(BigDecimal valorProdutos) {
        this.valorProdutos.set(valorProdutos);
    }

    @Column(name = "VALOR_NOTA")
    public BigDecimal getValorNota() {
        return valorNota.get();
    }

    public ObjectProperty<BigDecimal> valorNotaProperty() {
        return valorNota;
    }

    public void setValorNota(BigDecimal valorNota) {
        this.valorNota.set(valorNota);
    }

    @Column(name = "PIS")
    public BigDecimal getPis() {
        return pis.get();
    }

    public ObjectProperty<BigDecimal> pisProperty() {
        return pis;
    }

    public void setPis(BigDecimal pis) {
        this.pis.set(pis);
    }

    @Column(name = "COFINS")
    public BigDecimal getCofins() {
        return cofins.get();
    }

    public ObjectProperty<BigDecimal> cofinsProperty() {
        return cofins;
    }

    public void setCofins(BigDecimal cofins) {
        this.cofins.set(cofins);
    }

    @Column(name = "SYSCOMEX")
    public BigDecimal getSyscomex() {
        return syscomex.get();
    }

    public ObjectProperty<BigDecimal> syscomexProperty() {
        return syscomex;
    }

    public void setSyscomex(BigDecimal syscomex) {
        this.syscomex.set(syscomex);
    }

    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }

    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }

    @Column(name = "DOLAR")
    public BigDecimal getDolar() {
        return dolar.get();
    }

    public ObjectProperty<BigDecimal> dolarProperty() {
        return dolar;
    }

    public void setDolar(BigDecimal dolar) {
        this.dolar.set(dolar);
    }

    @Column(name = "TOTAL_DESPESAS")
    public BigDecimal getTotalDespesas() {
        return totalDespesas.get();
    }

    public ObjectProperty<BigDecimal> totalDespesasProperty() {
        return totalDespesas;
    }

    public void setTotalDespesas(BigDecimal totalDespesas) {
        this.totalDespesas.set(totalDespesas);
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "COD_PROFORMA")
    public List<SdItemProforma> getItens() {
        return itens;
    }

    public void setItens(List<SdItemProforma> itens) {
        this.itens = itens;
    }

    @Column(name = "DATA_EMISSAO")
    public LocalDate getDataEmissao() {
        return dataEmissao.get();
    }

    public ObjectProperty<LocalDate> dataEmissaoProperty() {
        return dataEmissao;
    }

    public void setDataEmissao(LocalDate dataEmissao) {
        this.dataEmissao.set(dataEmissao);
    }

    @Column(name = "DATA_SAIDA")
    public LocalDate getDataSaida() {
        return dataSaida.get();
    }

    public ObjectProperty<LocalDate> dataSaidaProperty() {
        return dataSaida;
    }

    public void setDataSaida(LocalDate dataSaida) {
        this.dataSaida.set(dataSaida);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "NUMERO_DI")
    public String getNumeroDi() {
        return numeroDi.get();
    }

    public StringProperty numeroDiProperty() {
        return numeroDi;
    }

    public void setNumeroDi(String numeroDi) {
        this.numeroDi.set(numeroDi);
    }

    @Column(name = "REFERENCIA_DI")
    public String getRefDi() {
        return refDi.get();
    }

    public StringProperty refDiProperty() {
        return refDi;
    }

    public void setRefDi(String refDi) {
        this.refDi.set(refDi);
    }

//    private BigDecimal getVolumes(SdEmbarque embarque) throws SQLException {
//        return (BigDecimal) ((Map<String, Object>) new NativeDAO().runNativeQuery(String.format("select sum(grd.volumes) volumes\n" +
//                "  from sd_embarque_001 emb\n" +
//                "  join sd_item_embarque_001 item\n" +
//                "    on item.codigo_embarque = emb.codigo\n" +
//                "  join sd_item_grade_embarque_001 grd\n" +
//                "    on grd.cod_item = item.id\n" +
//                "    \n" +
//                " where emb.codigo = '%s'", embarque.getCodigo())).get(0)).get("VOLUMES");
//    }

    private List<SdItemProforma> carregaItens(SdEmbarque embarque) {
        List<SdItemProforma> listItensProforma = new ArrayList<>();
        int ordem = 1;
        BigDecimal montante = BigDecimal.ZERO;
        embarque.setListItens(embarque.getListItens().stream().sorted(Comparator.comparing(obj -> obj.getProduto().getCodigo())).collect(Collectors.toList()));

        for (SdItemEmbarque item : embarque.getListItens()) {
            BigDecimal valorItem = (item.getPrecoUnidade().multiply(dolar.get()).add(frete.get().multiply(dolar.get()).divide(this.qtde.get(), 15, RoundingMode.HALF_UP)));
            valorItem = valorItem.setScale(6, RoundingMode.HALF_UP);

            BigDecimal valorFinalItem = valorItem.multiply(item.getQtde());

            listItensProforma.add(new SdItemProforma(this, item, ordem, dolar.get(), valorItem, valorFinalItem));

            montante = montante.add(valorFinalItem);
            ordem++;
        }
        this.valorProdutos.set(montante);
        this.valorNota.set(montante.add(this.pis.get()).add(this.cofins.get()).add(this.syscomex.get()));

        return listItensProforma;
    }

}
