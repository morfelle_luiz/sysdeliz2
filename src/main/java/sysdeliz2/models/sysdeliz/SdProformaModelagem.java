package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_PROFORMA_MODELAGEM_001")
public class SdProformaModelagem extends BasicModel {
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<SdGrupoModelagem> codGrupo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty genero = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>(new BigDecimal("0.350"));
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();

    public SdProformaModelagem() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_PROFORMA_MODELAGEM")
    @SequenceGenerator(name = "SEQ_SD_PROFORMA_MODELAGEM", sequenceName = "SEQ_SD_PROFORMA_MODELAGEM", allocationSize = 1)
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @OneToOne
    @JoinColumn(name = "CODGRUPO")
    public SdGrupoModelagem getCodGrupo() {
        return codGrupo.get();
    }

    public ObjectProperty<SdGrupoModelagem> codGrupoProperty() {
        return codGrupo;
    }

    public void setCodGrupo(SdGrupoModelagem codGrupo) {
        this.codGrupo.set(codGrupo);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @OneToOne
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
}
