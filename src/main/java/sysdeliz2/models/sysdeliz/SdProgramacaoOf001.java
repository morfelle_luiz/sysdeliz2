/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Of1001;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name="SD_PROGRAMACAO_OF_001")
public class SdProgramacaoOf001 {

    @Transient
    private final StringProperty dataCad = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<LocalDateTime> dataProg = new SimpleObjectProperty<>();
    @Transient
    private final IntegerProperty programador = new SimpleIntegerProperty();
    @Transient
    private final StringProperty setor = new SimpleStringProperty();
    @Transient
    private final StringProperty ordemProd = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Of1001> of1 = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty tipoPacote = new SimpleStringProperty();
    @Transient
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    @Transient
    private final BooleanProperty prodFinalizada = new SimpleBooleanProperty(false);
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="NUMERO")
    private List<SdPacote001> pacotesProgramacao;
    
    public SdProgramacaoOf001() {
    }
    
    public SdProgramacaoOf001(String dataCad, Integer programador, String setor, String ordemProd, String tipoPacote, Integer qtde, String prodFinalizada) {
        this.dataCad.set(dataCad);
        this.programador.set(programador);
        this.setor.set(setor);
        this.ordemProd.set(ordemProd);
        this.tipoPacote.set(tipoPacote);
        this.qtde.set(qtde);
        this.prodFinalizada.set(prodFinalizada.equals("S"));
    }
    
    @Id
    @Column(name = "ORDEM_PROD")
    public String getOrdemProd() {
        return ordemProd.get();
    }
    
    public void setOrdemProd(String value) {
        ordemProd.set(value);
    }
    
    public StringProperty ordemProdProperty() {
        return ordemProd;
    }
    
    @Transient
    public final String getDataCad() {
        return dataCad.get();
    }

    public final void setDataCad(String value) {
        dataCad.set(value);
    }

    public StringProperty dataCadProperty() {
        return dataCad;
    }
    
    @Column(name = "PROGRAMADOR")
    public int getProgramador() {
        return programador.get();
    }

    public void setProgramador(int value) {
        programador.set(value);
    }

    public IntegerProperty programadorProperty() {
        return programador;
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public void setSetor(String value) {
        setor.set(value);
    }

    public StringProperty setorProperty() {
        return setor;
    }
    
    @Column(name = "TIPO_PACOTE", columnDefinition = "CHAR")
    public String getTipoPacote() {
        return tipoPacote.get();
    }

    public void setTipoPacote(String value) {
        tipoPacote.set(value);
    }

    public StringProperty tipoPacoteProperty() {
        return tipoPacote;
    }
    
    @Transient
    public final Of1001 getOf1() throws SQLException {
        return DAOFactory.getOf1001DAO().getOfByNumero(this.ordemProd.get());
    }

    public ObjectProperty<Of1001> of1Property() throws SQLException {
        of1.set(DAOFactory.getOf1001DAO().getOfByNumero(this.ordemProd.get()));
        return of1;
    }

    public final void setOf1(Of1001 value) {
        of1.set(value);
    }
    
    @Column(name = "QTDE_PROG")
    public int getQtde() {
        return qtde.get();
    }

    public void setQtde(int value) {
        qtde.set(value);
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    @Column(name = "PROD_FINALIZADA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isProdFinalizada() {
        return prodFinalizada.get();
    }

    public void setProdFinalizada(boolean value) {
        prodFinalizada.set(value);
    }

    public BooleanProperty prodFinalizadaProperty() {
        return prodFinalizada;
    }
    
    @Column(name = "DATA_CAD", columnDefinition = "DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDataProg() {
        return dataProg.get();
    }
    
    public ObjectProperty<LocalDateTime> dataProgProperty() {
        return dataProg;
    }
    
    public void setDataProg(LocalDateTime dataProg) {
        this.dataProg.set(dataProg);
    }
    
    @Transient
    public List<SdPacote001> getPacotesProgramacao() {
        return pacotesProgramacao;
    }
    
    public void setPacotesProgramacao(List<SdPacote001> pacotesProgramacao) {
        this.pacotesProgramacao = pacotesProgramacao;
    }
}
