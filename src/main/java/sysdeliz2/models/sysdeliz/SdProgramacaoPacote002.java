package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang.StringUtils;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdColabCelProg;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;
import sysdeliz2.utils.enums.StatusProgramacao;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "SD_PROGRAMACAO_PACOTE_002")
public class SdProgramacaoPacote002 extends BasicModel implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    // Table atributes
    private final ObjectProperty<SdProgramacaoPacote002PK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<>();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<SdEquipamentosOrganize001> maquina = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoOp = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty agrupador = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dtProgramacao = new SimpleObjectProperty<>();
    private final IntegerProperty seqColaborador = new SimpleIntegerProperty();
    private final IntegerProperty statusProg = new SimpleIntegerProperty();
    
    // Logic atributes
    private final ObjectProperty<SdProgramacaoPacote002> operacaoAnterior = new SimpleObjectProperty<>();
    private final ObjectProperty<SdProgramacaoPacote002> operacaoPosterior = new SimpleObjectProperty<>();
    private final ObjectProperty<SdProgramacaoPacote002> operacaoAgrupador = new SimpleObjectProperty<>();
    private final ListProperty<SdProgramacaoPacote002> operacoesAgrupadas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ObjectProperty<BigDecimal> tempoTotal = new SimpleObjectProperty<>();
    private final ObjectProperty<StatusProgramacao> statusProgramacao = new SimpleObjectProperty<>(StatusProgramacao.LIVRE);
    private final ObjectProperty<BigDecimal> tempoOpAgrupada = new SimpleObjectProperty<>();
    private final BooleanProperty qtdeAlterada = new SimpleBooleanProperty(false);
    private final ObjectProperty<VSdColabCelProg> colaboradorCelula = new SimpleObjectProperty<>();
    private final IntegerProperty ordemOriginal = new SimpleIntegerProperty();
    private final BooleanProperty operacaoLiberada = new SimpleBooleanProperty(false);
    private final StringProperty posicaoOriginal = new SimpleStringProperty();
    
    // Logic
    private final GenericDao<SdProgramacaoPacote002> daoProgramacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdProgramacaoPacote002.class);
    private final GenericDao<SdLancamentoProducao001> daoProducao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLancamentoProducao001.class);
    
    public SdProgramacaoPacote002() {
    }
    
    public SdProgramacaoPacote002(SdProgramacaoPacote002 toCopy) {
        if(toCopy != null) {
            this.id.set(new SdProgramacaoPacote002PK(toCopy.getId()));
            this.maquina.set(toCopy.getMaquina());
            this.tempoOp.set(toCopy.getTempoOp());
            this.colaborador.set(toCopy.getColaborador());
            this.celula.set(toCopy.getCelula());
            this.qtde.set(toCopy.getQtde());
            this.agrupador.set(toCopy.getAgrupador());
            this.dtProgramacao.set(toCopy.getDtProgramacao());
            this.seqColaborador.set(toCopy.getSeqColaborador());
            this.statusProg.set(toCopy.getStatusProg());
            this.tempoTotal.set(toCopy.getTempoTotal());
            this.statusProgramacao.set(toCopy.getStatusProgramacao());
            this.operacaoAgrupador.set(toCopy.getOperacaoAgrupador());
            this.operacoesAgrupadas.set(toCopy.getOperacoesAgrupadas());
            this.tempoOpAgrupada.set(toCopy.getTempoOpAgrupada());
            this.colaboradorCelula.set(toCopy.getColaboradorCelula());
            this.ordemOriginal.set(toCopy.getId().getOrdem());
            this.posicaoOriginal.set(toCopy.getPosicaoOriginal());
        }
    }
    
    public SdProgramacaoPacote002(String pacote, SdSetorOp001 setorOp, SdOperacaoOrganize001 operacao, Integer ordem, Integer quebra,
                                  SdEquipamentosOrganize001 maquina, BigDecimal tempoOp, Integer qtde, Integer agrupador, LocalDateTime dtProgramacao,
                                  Integer seqColaborador, BigDecimal tempoTotal, StatusProgramacao statusProgramacao) {
        this.id.set(new SdProgramacaoPacote002PK(pacote, setorOp, operacao, ordem, quebra));
        this.maquina.set(maquina);
        this.tempoOp.set(tempoOp);
        this.tempoOpAgrupada.set(tempoOp);
        this.qtde.set(qtde);
        this.agrupador.set(agrupador);
        this.dtProgramacao.set(dtProgramacao);
        this.seqColaborador.set(seqColaborador);
        this.tempoTotal.set(tempoTotal);
        this.statusProgramacao.set(statusProgramacao);
        this.ordemOriginal.set(ordem);
        this.posicaoOriginal.set(StringUtils.leftPad(String.valueOf(ordem), 3,'0')+"1");
    }
    
    @EmbeddedId
    public SdProgramacaoPacote002PK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdProgramacaoPacote002PK> idProperty() {
        return id;
    }
    
    public void setId(SdProgramacaoPacote002PK id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CELULA")
    public SdCelula getCelula() {
        return celula.get();
    }
    
    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }
    
    public void setCelula(SdCelula celula) {
        this.celula.set(celula);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MAQUINA")
    public SdEquipamentosOrganize001 getMaquina() {
        return maquina.get();
    }
    
    public ObjectProperty<SdEquipamentosOrganize001> maquinaProperty() {
        return maquina;
    }
    
    public void setMaquina(SdEquipamentosOrganize001 maquina) {
        this.maquina.set(maquina);
    }
    
    @Column(name = "TEMPO_OP")
    public BigDecimal getTempoOp() {
        return tempoOp.get();
    }
    
    public ObjectProperty<BigDecimal> tempoOpProperty() {
        return tempoOp;
    }
    
    public void setTempoOp(BigDecimal tempoOp) {
        this.tempoOp.set(tempoOp);
        this.tempoOpAgrupada.set(tempoOp);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "AGRUPADOR")
    public int getAgrupador() {
        return agrupador.get();
    }
    
    public IntegerProperty agrupadorProperty() {
        return agrupador;
    }
    
    public void setAgrupador(int agrupador) {
        this.agrupador.set(agrupador);
    }
    
    @Column(name = "DT_PROGRAMACAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtProgramacao() {
        return dtProgramacao.get();
    }
    
    public ObjectProperty<LocalDateTime> dtProgramacaoProperty() {
        return dtProgramacao;
    }
    
    public void setDtProgramacao(LocalDateTime dtProgramacao) {
        this.dtProgramacao.set(dtProgramacao);
    }
    
    @Column(name = "SEQ_COLABORADOR")
    public int getSeqColaborador() {
        return seqColaborador.get();
    }
    
    public IntegerProperty seqColaboradorProperty() {
        return seqColaborador;
    }
    
    public void setSeqColaborador(int seqColaborador) {
        this.seqColaborador.set(seqColaborador);
    }
    
    @Column(name = "STATUS_PROG")
    public int getStatusProg() {
        return statusProg.get();
    }
    
    public IntegerProperty statusProgProperty() {
        return statusProg;
    }
    
    public void setStatusProg(int statusProg) {
        if (statusProg == 0) {
            this.setStatusProgramacao(StatusProgramacao.LIVRE);
        } else if (statusProg == 1) {
            this.setStatusProgramacao(StatusProgramacao.LANCADO);
        } else if (statusProg == 2) {
            this.setStatusProgramacao(StatusProgramacao.FINALIZADO);
        }
        this.statusProg.set(statusProg);
    }
    
    @Transient
    public SdProgramacaoPacote002 getOperacaoAnterior() {
        return operacaoAnterior.get();
    }
    
    public ObjectProperty<SdProgramacaoPacote002> operacaoAnteriorProperty() {
        return operacaoAnterior;
    }
    
    public void setOperacaoAnterior(SdProgramacaoPacote002 operacaoAnterior) {
        this.operacaoAnterior.set(operacaoAnterior);
    }
    
    @Transient
    public SdProgramacaoPacote002 getOperacaoPosterior() {
        return operacaoPosterior.get();
    }
    
    public ObjectProperty<SdProgramacaoPacote002> operacaoPosteriorProperty() {
        return operacaoPosterior;
    }
    
    public void setOperacaoPosterior(SdProgramacaoPacote002 operacaoPosterior) {
        this.operacaoPosterior.set(operacaoPosterior);
    }
    
    @Transient
    public BigDecimal getTempoTotal() {
        return tempoTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tempoTotalProperty() {
        return tempoTotal;
    }
    
    public void setTempoTotal(BigDecimal tempoTotal) {
        this.tempoTotal.set(tempoTotal);
    }
    
    @Transient
    public StatusProgramacao getStatusProgramacao() {
        return statusProgramacao.get();
    }
    
    public ObjectProperty<StatusProgramacao> statusProgramacaoProperty() {
        return statusProgramacao;
    }
    
    public void setStatusProgramacao(StatusProgramacao statusProgramacao) {
        this.statusProgramacao.set(statusProgramacao);
    }
    
    @Transient
    public BigDecimal getTempoOpAgrupada() {
        return tempoOpAgrupada.get();
    }
    
    public ObjectProperty<BigDecimal> tempoOpAgrupadaProperty() {
        return tempoOpAgrupada;
    }
    
    public void setTempoOpAgrupada(BigDecimal tempoOpAgrupada) {
        this.tempoOpAgrupada.set(tempoOpAgrupada);
    }
    
    @Transient
    public boolean isQtdeAlterada() {
        return qtdeAlterada.get();
    }
    
    public BooleanProperty qtdeAlteradaProperty() {
        return qtdeAlterada;
    }
    
    public void setQtdeAlterada(boolean qtdeAlterada) {
        this.qtdeAlterada.set(qtdeAlterada);
    }
    
    @Transient
    public VSdColabCelProg getColaboradorCelula() {
        return colaboradorCelula.get();
    }
    
    public ObjectProperty<VSdColabCelProg> colaboradorCelulaProperty() {
        return colaboradorCelula;
    }
    
    public void setColaboradorCelula(VSdColabCelProg colaboradorCelula) {
        this.colaboradorCelula.set(colaboradorCelula);
    }
    
    @Transient
    public SdProgramacaoPacote002 getOperacaoAgrupador() {
        return operacaoAgrupador.get();
    }
    
    public ObjectProperty<SdProgramacaoPacote002> operacaoAgrupadorProperty() {
        return operacaoAgrupador;
    }
    
    public void setOperacaoAgrupador(SdProgramacaoPacote002 operacaoAgrupador) {
        this.operacaoAgrupador.set(operacaoAgrupador);
    }
    
    @Transient
    public int getOrdemOriginal() {
        return ordemOriginal.get();
    }
    
    public IntegerProperty ordemOriginalProperty() {
        return ordemOriginal;
    }
    
    public void setOrdemOriginal(int ordemOriginal) {
        this.ordemOriginal.set(ordemOriginal);
    }
    
    @Transient
    public ObservableList<SdProgramacaoPacote002> getOperacoesAgrupadas() {
        return operacoesAgrupadas.get();
    }
    
    public ListProperty<SdProgramacaoPacote002> operacoesAgrupadasProperty() {
        return operacoesAgrupadas;
    }
    
    public void setOperacoesAgrupadas(ObservableList<SdProgramacaoPacote002> operacoesAgrupadas) {
        this.operacoesAgrupadas.set(operacoesAgrupadas);
    }
    
    @Transient
    public boolean isOperacaoLiberada() {
        return operacaoLiberada.get();
    }
    
    public BooleanProperty operacaoLiberadaProperty() {
        return operacaoLiberada;
    }
    
    public void setOperacaoLiberada(boolean operacaoLiberada) {
        this.operacaoLiberada.set(operacaoLiberada);
    }
    
    @Transient
    public String getPosicaoOriginal() {
        return posicaoOriginal.get();
    }
    
    public StringProperty posicaoOriginalProperty() {
        return posicaoOriginal;
    }
    
    public void setPosicaoOriginal(String posicaoOriginal) {
        this.posicaoOriginal.set(posicaoOriginal);
    }
    
    @Override
    public String toString() {
        return this != null ? "SdProgramacaoPacote002 ["
                + "\npacote=" + this.getId().getPacote()
                + "\nqtde=" + this.getQtde()
                + "\nstatusProgramacao=" + this.getStatusProgramacao()
                + "\nsetorOp=" + this.getId().getSetorOp().toString()
                + "\ncelula=" + (this.getCelula() != null ? getCelula().toString() : "null")
                + "\nordem=" + this.getId().getOrdem()
                + "\nquebra=" + this.getId().getQuebra()
                + "\noperacao=" + this.getId().getOperacao().toString()
                + "\ntempoOp=" + this.getTempoOp()
                + "\ntempoTotal=" + this.getTempoTotal()
                + "\nmaquina=" + this.getMaquina().toString()
                + "\nagrupador=" + this.getAgrupador()
                + "\ncolaborador=" + (this.getColaborador() != null ? getColaborador().toString() : "null")
                + ']' : "null";
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdProgramacaoPacote002 other = (SdProgramacaoPacote002) obj;
        if (!this.id.get().equals(other.id.get())) {
            return false;
        }
        return true;
    }
    
    @PostLoad
    private void onPostLoad(){
        try {
            if(this.statusProg.get() == 0){
                if(this.colaborador.get() != null)
                    this.statusProgramacao.set(StatusProgramacao.PROGRAMADO);
                if(this.id.get().getQuebra() > 1)
                    this.statusProgramacao.set(StatusProgramacao.QUEBRADA);
            }
            
            this.setOrdemOriginal(this.getId().getOrdem());
            this.setPosicaoOriginal(StringUtils.leftPad(String.valueOf(this.getId().getOrdem()), 3,'0')+"1");
            List<SdProgramacaoPacote002> operacoesAgrupadas = daoProgramacao
                    .initCriteria()
                    .addPredicateEq("agrupador", this.id.get().getOperacao().getCodorg())
                    .addPredicateEqPkEmbedded("id","pacote", this.id.get().getPacote(), false)
                    .addPredicateEqPkEmbedded("id","setorOp", this.id.get().getSetorOp().getCodigo(), false)
                    .loadListByPredicate();
            this.tempoOpAgrupada.set(this.tempoOp.get());
            if (operacoesAgrupadas.size() > 0) {
                if(!this.statusProgramacao.get().equals(StatusProgramacao.LANCADO) && !this.statusProgramacao.get().equals(StatusProgramacao.FINALIZADO))
                    this.statusProgramacao.set(StatusProgramacao.AGRUPADA);
                this.tempoOpAgrupada.set(BigDecimal.valueOf(operacoesAgrupadas.stream().mapToDouble(operacao -> operacao.getTempoOp().doubleValue()).sum() + this.tempoOp.get().doubleValue()));
                this.tempoTotal.set(BigDecimal.valueOf(this.tempoOpAgrupada.get().doubleValue() * this.qtde.get()));
                this.operacoesAgrupadas.addAll(operacoesAgrupadas);
            }else if (this.agrupador.get() != 0){
                if(!this.statusProgramacao.get().equals(StatusProgramacao.LANCADO) && !this.statusProgramacao.get().equals(StatusProgramacao.FINALIZADO))
                    this.statusProgramacao.set(StatusProgramacao.AGRUPADA);
                this.tempoTotal.set(BigDecimal.valueOf(this.tempoOp.get().doubleValue() * this.qtde.get()));
            } else {
                this.tempoTotal.set(BigDecimal.valueOf(this.tempoOp.get().doubleValue() * this.qtde.get()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
