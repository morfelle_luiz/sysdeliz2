package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SdProgramacaoPacote002PK extends BasicModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private final StringProperty pacote = new SimpleStringProperty();
    private final ObjectProperty<SdSetorOp001> setorOp = new SimpleObjectProperty<>();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();

    public SdProgramacaoPacote002PK() {
    }
    
    public SdProgramacaoPacote002PK(SdProgramacaoPacote002PK toCopy) {
        this.pacote.set(toCopy.getPacote());
        this.setorOp.set(toCopy.getSetorOp());
        this.operacao.set(toCopy.getOperacao());
        this.ordem.set(toCopy.getOrdem());
        this.quebra.set(toCopy.getQuebra());
    }

    public SdProgramacaoPacote002PK(String pacote, SdSetorOp001 setorOp, SdOperacaoOrganize001 operacao, Integer ordem, Integer quebra) {
        this.pacote.set(pacote);
        this.setorOp.set(setorOp);
        this.operacao.set(operacao);
        this.ordem.set(ordem);
        this.quebra.set(quebra);
    }

    @Column(name = "PACOTE")
    public String getPacote() {
        return pacote.get();
    }

    public StringProperty pacoteProperty() {
        return pacote;
    }

    public void setPacote(String pacote) {
        this.pacote.set(pacote);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SETOR_OP")
    public SdSetorOp001 getSetorOp() {
        return setorOp.get();
    }

    public ObjectProperty<SdSetorOp001> setorOpProperty() {
        return setorOp;
    }

    public void setSetorOp(SdSetorOp001 setorOp) {
        this.setorOp.set(setorOp);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }

    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }

    public void setOperacao(SdOperacaoOrganize001 operacao) {
        this.operacao.set(operacao);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "QUEBRA")
    public int getQuebra() {
        return quebra.get();
    }

    public IntegerProperty quebraProperty() {
        return quebra;
    }

    public void setQuebra(int quebra) {
        this.quebra.set(quebra);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdProgramacaoPacote002PK other = (SdProgramacaoPacote002PK) obj;
        if (!Objects.equals(this.pacote.get(), other.pacote.get())) {
            return false;
        }
        if (!Objects.equals(this.setorOp.get().getCodigo(), other.setorOp.get().getCodigo())) {
            return false;
        }
        if (!Objects.equals(this.operacao.get().getCodorg(), other.operacao.get().getCodorg())) {
            return false;
        }
        if (!Objects.equals(this.ordem.get(), other.ordem.get())) {
            return false;
        }
        if (!Objects.equals(this.quebra.get(), other.quebra.get())) {
            return false;
        }
        return true;
    }

}
