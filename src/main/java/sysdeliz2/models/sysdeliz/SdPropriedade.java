package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_PROPRIEDADE_001")
public class SdPropriedade implements Serializable {

    public final StringProperty usuario = new SimpleStringProperty();
    public final IntegerProperty idTela = new SimpleIntegerProperty();
    public final StringProperty propriedade = new SimpleStringProperty();
    public final StringProperty valor = new SimpleStringProperty();

    public SdPropriedade() {
    }

    public SdPropriedade(String nomeUsuario, String id, String propriedade, String valor) {
        this.usuario.set(nomeUsuario);
        this.idTela.set(Integer.parseInt(id));
        this.propriedade.set(propriedade.toUpperCase());
        this.valor.set(valor);
    }

    @Id
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @Id
    @Column(name = "IDTELA")
    public int getIdTela() {
        return idTela.get();
    }

    public IntegerProperty idTelaProperty() {
        return idTela;
    }

    public void setIdTela(int idTela) {
        this.idTela.set(idTela);
    }

    @Id
    @Column(name = "PROPRIEDADE")
    public String getPropriedade() {
        return propriedade.get();
    }

    public StringProperty propriedadeProperty() {
        return propriedade;
    }

    public void setPropriedade(String propriedade) {
        this.propriedade.set(propriedade);
    }

    @Lob
    @Column(name = "VALOR")
    public String getValor() {
        return valor.get();
    }

    public StringProperty valorProperty() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor.set(valor);
    }
}
