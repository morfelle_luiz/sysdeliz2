package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 23/08/2019 09:58
 */
@Entity
@Table(name = "SD_REDES_SOCIAIS_001")
@TelaSysDeliz(descricao = "Redes Sociais", icon = "avisar_100.png")
public class SdRedesSociais extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Rede Social", width = 300)
    @ColunaFilter(descricao = "Rede Social", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty(this, "nome");
    @Transient
    @ExibeTableView(descricao = "Link", width = 250)
    private final StringProperty url = new SimpleStringProperty(this, "url");

    public SdRedesSociais() {
    }

    public SdRedesSociais(String nome, String url) {
        this.nome.set(nome);
        this.url.set(url);
    }

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="SEQ_REDES_SOCIAIS_001")
    @SequenceGenerator(name="SEQ_REDES_SOCIAIS_001", sequenceName="SD_SEQ_REDES_SOCIAIS_001", allocationSize=1, initialValue = 1)
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    @Column(name = "URL")
    public String getUrl() {
        return url.get();
    }

    public IntegerProperty codigoProperty() {
       return codigo;
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setCodigo(int value) {
        codigo.set(value);
        setCodigoFilter(String.valueOf(value));
    }

    public void setNome(String value) {
        nome.set(value);
        setDescricaoFilter(value);
    }

    public void setUrl(String value) {
        url.set(value);
    }

}
