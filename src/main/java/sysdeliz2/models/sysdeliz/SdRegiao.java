package sysdeliz2.models.sysdeliz;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.ti.TabUf;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 12/07/2019 13:39
 */
@Entity
@Table(name="SD_REGIOES_001")
public class SdRegiao implements Serializable {

    @Transient
    private final IntegerProperty codigo = new SimpleIntegerProperty(this, "codigo");
    @Transient
    private final StringProperty siglaTi = new SimpleStringProperty(this, "siglaTi");
    @Transient
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final StringProperty status = new SimpleStringProperty(this, "status");
    @Transient
    private final StringProperty obs = new SimpleStringProperty(this, "obs");

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="SD_REGIOES_UF_001",
            joinColumns = {@JoinColumn(name="REGIAO_CODIGO", referencedColumnName = "CODIGO")},
            inverseJoinColumns ={
                    @JoinColumn(name = "CODIGO_UF", referencedColumnName = "CODIGO"),
                    @JoinColumn(name = "SIGLA_UF", referencedColumnName = "SIGLA_EST")
            }
    )
    @Access(AccessType.FIELD)
    private List<TabUf> ufList;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name =  "SD_REGIOES_CIDADE_001",
            joinColumns        = {@JoinColumn(name = "REGIAO_CODIGO", referencedColumnName = "CODIGO")},
            inverseJoinColumns = {@JoinColumn(name = "CIDADE_CODIGO", referencedColumnName = "CODIGO")}
    )
    @Access(AccessType.FIELD)
    private List<SdCidade> cidadeList;

    @Transient
    private boolean addCidade = false;

    public SdRegiao(){
        this.status.setValue("A");
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SD_SEQ_REGIOES_001")
    @SequenceGenerator(name="SD_SEQ_REGIOES_001", sequenceName="SD_SEQ_REGIOES_001", allocationSize=1)
    public int getCodigo() {
       return codigo.get();
    }
    @Column(name="sigla_ti")
    public String getSiglaTi() {
        return siglaTi.get();
    }
    @Column(name="descricao")
    public String getDescricao() {
        return descricao.get();
    }
    @Column(name="status")
    public String getStatus() {
        return status.get();
    }
    @Column(name="obs")
    public String getObs() {
        return obs.get();
    }

    public List<TabUf> getUfList(){
        return ufList;
    }

    public List<SdCidade> getCidadeList(){
        return cidadeList;
    }

    public void addUF(TabUf uf){
        this.ufList.add(uf);
    }

    public void addCidades(List<SdCidade> cidades){
        if(cidades != null && cidades.size() > 0){
            cidades.forEach(cidade -> {
                addCidade = true;
                this.cidadeList.forEach(city -> {
                    if(city.getCodigo().equals(cidade.getCodigo())){
                        addCidade = false;
                    }
                });

                if(addCidade){
                    this.cidadeList.add(cidade);
                }
            });
        }
    }

    public void addCidades(ObservableList<SdCidade> cidades){
        if(cidades != null && cidades.size() > 0){
            cidades.forEach(cidade -> {
                addCidade = true;
                this.cidadeList.forEach(city -> {
                    if(city.getCodigo().equals(cidade.getCodigo())){
                        addCidade = false;
                    }
                });

                if(addCidade){
                    this.cidadeList.add(cidade);
                }
            });
        }
    }

    @SuppressWarnings("unused")
    public void setUfList(List<TabUf> value){
        ufList = value;
    }

    public void setCidadeList(List<SdCidade> value){
        cidadeList = value;
    }

    public final IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int value) {
        codigo.set(value);
    }

    public final StringProperty siglaTiProperty() {
        return siglaTi;
    }

    @SuppressWarnings("unused")
    public void setSiglaTi(String value) {
        siglaTi.set(value);
    }

    public final StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String value) {
        descricao.set(value);
    }

    public final StringProperty statusProperty() {
       return status;
    }

    public void setStatus(String value) {
        status.set(value);
    }

    public final StringProperty obsProperty() {
       return obs;
    }

    public void setObs(String value) {
        obs.set(value);
    }

    @Transient
    public ObservableList<TabUf> getUfObservableList(){
        if(this.ufList != null) {
            return FXCollections.observableArrayList(this.ufList);
        } else {
            return FXCollections.observableArrayList();
        }
    }

    @Transient
    public ObservableList<SdCidade> getCidadeObservableList(String uf){
        if(this.cidadeList != null){
            ObservableList<SdCidade> lista = FXCollections.observableArrayList(this.cidadeList);
            if(!uf.isEmpty()){
                lista = lista.filtered(cidade -> cidade.getSiglaUf().equals(uf));
            } else {
                lista.clear();
            }
            return lista;
        } else {
            return FXCollections.observableArrayList();
        }
    }

    @Override
    public String toString() {
        return this.descricao.get() + " - " + this.siglaTi.get();
    }
}
