package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;
import sysdeliz2.utils.converters.RemessaAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_REMESSA_CLIENTE_001")
public class SdRemessaCliente extends BasicModel {
    
    private final IntegerProperty remessa = new SimpleIntegerProperty();
    private final ObjectProperty<SdStatusRemessa> status = new SimpleObjectProperty<>();
    private final BooleanProperty automatica = new SimpleBooleanProperty();
    private final ObjectProperty<VSdDadosCliente> codcli = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtemissao = new SimpleObjectProperty<>(LocalDateTime.now());
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdec = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty observacao = new SimpleStringProperty();
    private final IntegerProperty ordemcoleta = new SimpleIntegerProperty(999);
    private final StringProperty usuario = new SimpleStringProperty(Globals.getUsuarioLogado() != null ? Globals.getUsuarioLogado().getUsuario() : null);
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtIniColeta = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtFimColeta = new SimpleObjectProperty<>();
    private List<SdPedidosRemessa> pedidos = new ArrayList<>();
    private final StringProperty deposito = new SimpleStringProperty();
    private final BooleanProperty liberaMinimo = new SimpleBooleanProperty(false);
    private final BooleanProperty coletaDupla = new SimpleBooleanProperty(false);
    private final BooleanProperty pisoFinalizado = new SimpleBooleanProperty(false);
    private final IntegerProperty piso = new SimpleIntegerProperty(0);
    private final ObjectProperty<LocalDate> dtFatura = new SimpleObjectProperty<>();
    
    public SdRemessaCliente() {
    }
    
    public SdRemessaCliente(SdStatusRemessa status, Boolean automatica, VSdDadosCliente codcli, LocalDate dtentrega, Integer qtde, BigDecimal valor, String deposito) {
        this.status.set(status);
        this.automatica.set(automatica);
        this.codcli.set(codcli);
        this.dtentrega.set(dtentrega);
        this.qtde.set(qtde);
        this.valor.set(valor);
        this.deposito.set(deposito);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TEM_PED_RESERVA")
    @SequenceGenerator(name = "SEQ_TEM_PED_RESERVA", sequenceName = "SEQ_TEM_PED_RESERVA", allocationSize = 1)
    @Column(name = "REMESSA", columnDefinition = "VARCHAR2()")
    @Convert(converter = RemessaAttributeConverter.class)
    public Integer getRemessa() {
        return remessa.get();
    }
    
    public IntegerProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(Integer remessa) {
        this.remessa.set(remessa);
        setCodigoFilter(String.valueOf(remessa));
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public SdStatusRemessa getStatus() {
        return status.get();
    }
    
    public ObjectProperty<SdStatusRemessa> statusProperty() {
        return status;
    }
    
    public void setStatus(SdStatusRemessa status) {
        this.status.set(status);
    }
    
    @Column(name = "AUTOMATICA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAutomatica() {
        return automatica.get();
    }
    
    public BooleanProperty automaticaProperty() {
        return automatica;
    }
    
    public void setAutomatica(boolean automatica) {
        this.automatica.set(automatica);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public VSdDadosCliente getCodcli() {
        return codcli.get();
    }
    
    public ObjectProperty<VSdDadosCliente> codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(VSdDadosCliente codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtemissao() {
        return dtemissao.get();
    }
    
    public ObjectProperty<LocalDateTime> dtemissaoProperty() {
        return dtemissao;
    }
    
    public void setDtemissao(LocalDateTime dtemissao) {
        this.dtemissao.set(dtemissao);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public int getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(int qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Lob
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "ORDEM_COLETA")
    public int getOrdemcoleta() {
        return ordemcoleta.get();
    }
    
    public IntegerProperty ordemcoletaProperty() {
        return ordemcoleta;
    }
    
    public void setOrdemcoleta(int ordemcoleta) {
        this.ordemcoleta.set(ordemcoleta);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "REMESSA", referencedColumnName = "REMESSA")
    @OrderBy("id.numero.numero ASC")
    public List<SdPedidosRemessa> getPedidos() {
        return pedidos;
    }
    
    public void setPedidos(List<SdPedidosRemessa> pedidos) {
        this.pedidos = pedidos;
    }
    
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }
    
    public StringProperty usuarioProperty() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }
    
    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }
    
    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "DT_INI_COLETA")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtIniColeta() {
        return dtIniColeta.get();
    }
    
    public ObjectProperty<LocalDateTime> dtIniColetaProperty() {
        return dtIniColeta;
    }
    
    public void setDtIniColeta(LocalDateTime dtIniColeta) {
        this.dtIniColeta.set(dtIniColeta);
    }
    
    @Column(name = "DT_FIM_COLETA")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtFimColeta() {
        return dtFimColeta.get();
    }
    
    public ObjectProperty<LocalDateTime> dtFimColetaProperty() {
        return dtFimColeta;
    }
    
    public void setDtFimColeta(LocalDateTime dtFimColeta) {
        this.dtFimColeta.set(dtFimColeta);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "LIBERA_MINIMO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLiberaMinimo() {
        return liberaMinimo.get();
    }
    
    public BooleanProperty liberaMinimoProperty() {
        return liberaMinimo;
    }
    
    public void setLiberaMinimo(boolean liberaMinimo) {
        this.liberaMinimo.set(liberaMinimo);
    }
    
    @Column(name = "DT_FATURA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtFatura() {
        return dtFatura.get();
    }
    
    public ObjectProperty<LocalDate> dtFaturaProperty() {
        return dtFatura;
    }
    
    public void setDtFatura(LocalDate dtFatura) {
        this.dtFatura.set(dtFatura);
    }

    @Column(name = "COLETA_DUPLA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isColetaDupla() {
        return coletaDupla.get();
    }

    public BooleanProperty coletaDuplaProperty() {
        return coletaDupla;
    }

    public void setColetaDupla(boolean coletaDupla) {
        this.coletaDupla.set(coletaDupla);
    }

    @Column(name = "PISO_FINALIZADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPisoFinalizado() {
        return pisoFinalizado.get();
    }

    public BooleanProperty pisoFinalizadoProperty() {
        return pisoFinalizado;
    }

    public void setPisoFinalizado(boolean pisoFinalizado) {
        this.pisoFinalizado.set(pisoFinalizado);
    }

    @Column(name = "PISO")
    public int getPiso() {
        return piso.get();
    }

    public IntegerProperty pisoProperty() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso.set(piso);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}