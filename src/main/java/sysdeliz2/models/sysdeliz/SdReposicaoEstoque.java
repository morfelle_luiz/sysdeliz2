package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_REPOSICAO_ESTOQUE_001")
public class SdReposicaoEstoque {
    
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final ObjectProperty<VSdDadosProduto> produto = new SimpleObjectProperty<>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtemissao = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.now());
    private final ObjectProperty<SdColaborador>  repositor = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtresolvido = new SimpleObjectProperty<LocalDateTime>();
    private final BooleanProperty resolvido = new SimpleBooleanProperty();
    private final BooleanProperty furoEstoque = new SimpleBooleanProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    
    public SdReposicaoEstoque() {
    }
    
    public SdReposicaoEstoque(VSdDadosProduto produto, String cor, String local, String status, SdColaborador coletor, String remessa) {
        this.produto.set(produto);
        this.cor.set(cor);
        this.local.set(local);
        this.status.set(status);
        this.coletor.set(coletor);
        this.remessa.set(remessa);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_REPOSICAO_ESTOQUE")
    @SequenceGenerator(name = "SEQ_SD_REPOSICAO_ESTOQUE", sequenceName = "SEQ_SD_REPOSICAO_ESTOQUE", allocationSize = 1)
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne
    @JoinColumn(name = "PRODUTO")
    public VSdDadosProduto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<VSdDadosProduto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(VSdDadosProduto produto) {
        this.produto.set(produto);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @OneToOne
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }
    
    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }
    
    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtemissao() {
        return dtemissao.get();
    }
    
    public ObjectProperty<LocalDateTime> dtemissaoProperty() {
        return dtemissao;
    }
    
    public void setDtemissao(LocalDateTime dtemissao) {
        this.dtemissao.set(dtemissao);
    }
    
    @OneToOne
    @JoinColumn(name = "REPOSITOR")
    public SdColaborador getRepositor() {
        return repositor.get();
    }
    
    public ObjectProperty<SdColaborador> repositorProperty() {
        return repositor;
    }
    
    public void setRepositor(SdColaborador repositor) {
        this.repositor.set(repositor);
    }
    
    @Column(name = "DT_RESOLVIDO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtresolvido() {
        return dtresolvido.get();
    }
    
    public ObjectProperty<LocalDateTime> dtresolvidoProperty() {
        return dtresolvido;
    }
    
    public void setDtresolvido(LocalDateTime dtresolvido) {
        this.dtresolvido.set(dtresolvido);
    }
    
    @Column(name = "RESOLVIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isResolvido() {
        return resolvido.get();
    }
    
    public BooleanProperty resolvidoProperty() {
        return resolvido;
    }
    
    public void setResolvido(boolean resolvido) {
        this.resolvido.set(resolvido);
    }
    
    @Column(name = "FURO_ESTOQUE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFuroEstoque() {
        return furoEstoque.get();
    }
    
    public BooleanProperty furoEstoqueProperty() {
        return furoEstoque;
    }
    
    public void setFuroEstoque(boolean furoEstoque) {
        this.furoEstoque.set(furoEstoque);
    }
    
    @Column(name = "REMESSA")
    public String getRemessa() {
        return remessa.get();
    }
    
    public StringProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }
}