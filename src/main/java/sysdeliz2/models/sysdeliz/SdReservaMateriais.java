package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "SD_RESERVA_MATERIAIS_001")
public class SdReservaMateriais extends BasicModel {
    
    private final ObjectProperty<SdReservaMateriaisPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> qtdeB = new SimpleObjectProperty<>();
    private final StringProperty situacao = new SimpleStringProperty();
    private final BooleanProperty baixado = new SimpleBooleanProperty();
    private final BooleanProperty reserva = new SimpleBooleanProperty();
    private final BooleanProperty coleta = new SimpleBooleanProperty();
    private final BooleanProperty substituto = new SimpleBooleanProperty();
    private final IntegerProperty coletor = new SimpleIntegerProperty();
    private final IntegerProperty movimento = new SimpleIntegerProperty();
    private final IntegerProperty movEstorno = new SimpleIntegerProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty depagrupador = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty("N");
    
    public SdReservaMateriais() {
    }
    
    public SdReservaMateriais(String numero, Material insumo, Cor corI, String setor, String deposito, String lote, String faixa, Integer idPcpftof, LocalDateTime dhRequisicao,
                              Integer ordem, BigDecimal qtdeB, String situacao, Boolean baixado, Boolean reserva, Boolean coleta, Boolean substituto,
                              Integer coletor, String local, String depagrupador) {
        this.id.set(new SdReservaMateriaisPK(numero, insumo, corI, setor, deposito, lote, faixa, idPcpftof, dhRequisicao));
        this.ordem.set(ordem);
        this.qtdeB.set(qtdeB);
        this.situacao.set(situacao);
        this.baixado.set(baixado);
        this.reserva.set(reserva);
        this.coleta.set(coleta);
        this.substituto.set(substituto);
        this.coletor.set(coletor);
        this.local.set(local);
        this.depagrupador.set(depagrupador);
    }
    
    @EmbeddedId
    public SdReservaMateriaisPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdReservaMateriaisPK> idProperty() {
        return id;
    }
    
    public void setId(SdReservaMateriaisPK id) {
        this.id.set(id);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "QTDE_B")
    public BigDecimal getQtdeB() {
        return qtdeB.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeBProperty() {
        return qtdeB;
    }
    
    public void setQtdeB(BigDecimal qtdeB) {
        this.qtdeB.set(qtdeB);
    }
    
    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }
    
    public StringProperty situacaoProperty() {
        return situacao;
    }
    
    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }
    
    @Column(name = "BAIXADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isBaixado() {
        return baixado.get();
    }
    
    public BooleanProperty baixadoProperty() {
        return baixado;
    }
    
    public void setBaixado(boolean baixado) {
        this.baixado.set(baixado);
    }
    
    @Column(name = "RESERVA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isReserva() {
        return reserva.get();
    }
    
    public BooleanProperty reservaProperty() {
        return reserva;
    }
    
    public void setReserva(boolean reserva) {
        this.reserva.set(reserva);
    }
    
    @Column(name = "COLETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isColeta() {
        return coleta.get();
    }
    
    public BooleanProperty coletaProperty() {
        return coleta;
    }
    
    public void setColeta(boolean coleta) {
        this.coleta.set(coleta);
    }
    
    @Column(name = "SUBSTITUTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSubstituto() {
        return substituto.get();
    }
    
    public BooleanProperty substitutoProperty() {
        return substituto;
    }
    
    public void setSubstituto(boolean substituto) {
        this.substituto.set(substituto);
    }
    
    @Column(name = "COLETOR")
    public int getColetor() {
        return coletor.get();
    }
    
    public IntegerProperty coletorProperty() {
        return coletor;
    }
    
    public void setColetor(int coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "DEP_AGRUPADOR")
    public String getDepagrupador() {
        return depagrupador.get();
    }
    
    public StringProperty depagrupadorProperty() {
        return depagrupador;
    }
    
    public void setDepagrupador(String depagrupador) {
        this.depagrupador.set(depagrupador);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "MOVIMENTO")
    public int getMovimento() {
        return movimento.get();
    }
    
    public IntegerProperty movimentoProperty() {
        return movimento;
    }
    
    public void setMovimento(int movimento) {
        this.movimento.set(movimento);
    }
    
    @Column(name = "MOV_ESTORNO")
    public int getMovEstorno() {
        return movEstorno.get();
    }
    
    public IntegerProperty movEstornoProperty() {
        return movEstorno;
    }
    
    public void setMovEstorno(int movEstorno) {
        this.movEstorno.set(movEstorno);
    }
}
