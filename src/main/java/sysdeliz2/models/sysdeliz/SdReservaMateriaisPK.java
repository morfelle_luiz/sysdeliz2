package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
public class SdReservaMateriaisPK implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty();
    private final ObjectProperty<Cor> corI = new SimpleObjectProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty("0000");
    private final IntegerProperty idPcpftof = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dhRequisicao = new SimpleObjectProperty<>();
    
    public SdReservaMateriaisPK() {
    }
    
    public SdReservaMateriaisPK(String numero, Material insumo, Cor corI, String setor, String deposito, String lote, String faixa, Integer idPcpftof, LocalDateTime dhRequisicao) {
        this.numero.set(numero);
        this.insumo.set(insumo);
        this.corI.set(corI);
        this.setor.set(setor);
        this.deposito.set(deposito);
        this.lote.set(lote);
        this.faixa.set(faixa);
        this.idPcpftof.set(idPcpftof);
        this.dhRequisicao.set(dhRequisicao);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR_I")
    public Cor getCorI() {
        return corI.get();
    }
    
    public ObjectProperty<Cor> corIProperty() {
        return corI;
    }
    
    public void setCorI(Cor corI) {
        this.corI.set(corI);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }
    
    public StringProperty faixaProperty() {
        return faixa;
    }
    
    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }
    
    @Column(name = "DH_REQUISICAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhRequisicao() {
        return dhRequisicao.get();
    }
    
    public ObjectProperty<LocalDateTime> dhRequisicaoProperty() {
        return dhRequisicao;
    }
    
    public void setDhRequisicao(LocalDateTime dhRequisicao) {
        this.dhRequisicao.set(dhRequisicao);
    }
    
    @Column(name = "ID_PCPFTOF")
    public Integer getIdPcpftof() {
        return idPcpftof.get();
    }
    
    public IntegerProperty idPcpftofProperty() {
        return idPcpftof;
    }
    
    public void setIdPcpftof(Integer idPcpftof) {
        this.idPcpftof.set(idPcpftof);
    }
}
