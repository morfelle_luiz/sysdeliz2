package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_RISCO_PLAN_ENCAIXE_001")
public class SdRiscoPlanejamentoEncaixe extends BasicModel implements Serializable {
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<SdPlanejamentoEncaixe> idplanejamento = new SimpleObjectProperty<>();
    private final ObjectProperty<SdTipoRisco> tiporisco = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacaoft = new SimpleObjectProperty<>();
    private final IntegerProperty seq = new SimpleIntegerProperty();
    private final IntegerProperty seqTipo = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> consumopc = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty partes = new SimpleIntegerProperty(0);
    private final StringProperty arqdiamino = new SimpleStringProperty();
    private final ListProperty<SdCorRisco> coresRiscoObservable = new SimpleListProperty<>();
    private List<SdCorRisco> coresRisco = new ArrayList<>();
    private final BooleanProperty riscoApro = new SimpleBooleanProperty(false);
    
    private final StringProperty stringCoresAgrupadas = new SimpleStringProperty();
    private final StringProperty idsCoresAgrupadas = new SimpleStringProperty();
    
    public SdRiscoPlanejamentoEncaixe() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_RISCO_PLAN_ENCAIXE")
    @SequenceGenerator(name = "SEQ_SD_RISCO_PLAN_ENCAIXE", sequenceName = "SEQ_SD_RISCO_PLAN_ENCAIXE", allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(int id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLANEJAMENTO")
    public SdPlanejamentoEncaixe getIdplanejamento() {
        return idplanejamento.get();
    }
    
    public ObjectProperty<SdPlanejamentoEncaixe> idplanejamentoProperty() {
        return idplanejamento;
    }
    
    public void setIdplanejamento(SdPlanejamentoEncaixe idplanejamento) {
        this.idplanejamento.set(idplanejamento);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TIPO_RISCO")
    public SdTipoRisco getTiporisco() {
        return tiporisco.get();
    }
    
    public ObjectProperty<SdTipoRisco> tiporiscoProperty() {
        return tiporisco;
    }
    
    public void setTiporisco(SdTipoRisco tiporisco) {
        this.tiporisco.set(tiporisco);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APLICACAO_FT")
    public Pcpapl getAplicacaoft() {
        return aplicacaoft.get();
    }
    
    public ObjectProperty<Pcpapl> aplicacaoftProperty() {
        return aplicacaoft;
    }
    
    public void setAplicacaoft(Pcpapl aplicacaoft) {
        this.aplicacaoft.set(aplicacaoft);
    }
    
    @Column(name = "SEQ")
    public int getSeq() {
        return seq.get();
    }
    
    public IntegerProperty seqProperty() {
        return seq;
    }
    
    public void setSeq(int seq) {
        this.seq.set(seq);
    }
    
    @Column(name = "CONSUMO_PC")
    public BigDecimal getConsumopc() {
        return consumopc.get();
    }
    
    public ObjectProperty<BigDecimal> consumopcProperty() {
        return consumopc;
    }
    
    public void setConsumopc(BigDecimal consumopc) {
        this.consumopc.set(consumopc);
    }
    
    @Column(name = "PARTES")
    public int getPartes() {
        return partes.get();
    }
    
    public IntegerProperty partesProperty() {
        return partes;
    }
    
    public void setPartes(int partes) {
        this.partes.set(partes);
    }
    
    @Column(name = "ARQ_DIAMINO")
    public String getArqdiamino() {
        return arqdiamino.get();
    }
    
    public StringProperty arqdiaminoProperty() {
        return arqdiamino;
    }
    
    public void setArqdiamino(String arqdiamino) {
        this.arqdiamino.set(arqdiamino);
    }
    
    @Transient
    public ObservableList<SdCorRisco> getCoresRiscoObservable() {
        coresRiscoObservable.set(FXCollections.observableList(coresRisco));
        return coresRiscoObservable.get();
    }
    
    public ListProperty<SdCorRisco> coresRiscoObservableProperty() {
        coresRiscoObservable.set(FXCollections.observableList(coresRisco));
        return coresRiscoObservable;
    }
    
    public void setCoresRisco(ObservableList<SdCorRisco> coresRiscoObservable) {
        this.coresRiscoObservable.set(coresRiscoObservable);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_RISCO", referencedColumnName = "ID")
    public List<SdCorRisco> getCores() {
        return coresRisco;
    }
    
    public void setCores(List<SdCorRisco> coresRisco) {
        this.coresRisco = coresRisco;
    }
    
    @Column(name = "RISCO_APRO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isRiscoApro() {
        return riscoApro.get();
    }
    
    public BooleanProperty riscoAproProperty() {
        return riscoApro;
    }
    
    public void setRiscoApro(boolean riscoApro) {
        this.riscoApro.set(riscoApro);
    }
    
    @Transient
    public String getStringCoresAgrupadas() {
        return stringCoresAgrupadas.get();
    }
    
    public StringProperty stringCoresAgrupadasProperty() {
        return stringCoresAgrupadas;
    }
    
    public void setStringCoresAgrupadas(String stringCoresAgrupadas) {
        this.stringCoresAgrupadas.set(stringCoresAgrupadas);
    }
    
    @Transient
    public String getIdsCoresAgrupadas() {
        return idsCoresAgrupadas.get();
    }
    
    public StringProperty idsCoresAgrupadasProperty() {
        return idsCoresAgrupadas;
    }
    
    public void setIdsCoresAgrupadas(String idsCoresAgrupadas) {
        this.idsCoresAgrupadas.set(idsCoresAgrupadas);
    }
    
    @Column(name = "SEQ_TIPO")
    public int getSeqTipo() {
        return seqTipo.get();
    }
    
    public IntegerProperty seqTipoProperty() {
        return seqTipo;
    }
    
    public void setSeqTipo(int seqTipo) {
        this.seqTipo.set(seqTipo);
    }
}
