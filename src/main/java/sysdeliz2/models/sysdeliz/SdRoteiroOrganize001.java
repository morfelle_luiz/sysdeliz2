/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.SQLException;

/**
 *
 * @author cristiano.diego
 */
@Entity
@TelaSysDeliz(descricao = "Roteiros", icon = "ordem compra_100.png")
@Table(name = "SD_ROTEIRO_ORGANIZE_001")
public class SdRoteiroOrganize001 extends BasicModel {
    
    // table attributes
    private final IntegerProperty codorg = new SimpleIntegerProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 300)
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty parte = new SimpleIntegerProperty();
    private final StringProperty descPart = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> percentual = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty setor = new SimpleIntegerProperty();
    private final StringProperty sdSetor = new SimpleStringProperty();
    @ExibeTableView(descricao = "Setor", width = 180)
    private final StringProperty descSetor = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tempoHora = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> custo1 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> custo2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> custo3 = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty dtInclusao = new SimpleStringProperty();
    private final StringProperty dtAlteracao = new SimpleStringProperty();
    private final BooleanProperty alteracaoMan = new SimpleBooleanProperty();
    
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
    
    // logic attributes
    private final GenericDao<SdOperRoteiroOrganize001> daoOpersRoteiros = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdOperRoteiroOrganize001.class);
    private final StringProperty dtAlteracaoOper = new SimpleStringProperty();
    private final ListProperty<SdOperRoteiroOrganize001> operacoes = new SimpleListProperty<SdOperRoteiroOrganize001>(FXCollections.observableArrayList());
    
    public SdRoteiroOrganize001() {
    }

    public SdRoteiroOrganize001(Integer codorg, String referencia, String descricao, Integer parte, String descPart,
            Double percentual, Integer setor, String descSetor, Double tempo, Double tempoHora, Double custo1, Double custo2,
            Double custo3, String dtInclusao, String dtAlteracao, String dtAlteracaoOper) {
        this.codorg.set(codorg);
        this.referencia.set(referencia);
        this.descricao.set(descricao);
        this.parte.set(parte);
        this.descPart.set(descPart);
        this.percentual.set(BigDecimal.valueOf(percentual));
        this.setor.set(setor);
        this.descSetor.set(descSetor);
        this.tempo.set(BigDecimal.valueOf(tempo));
        this.tempoHora.set(BigDecimal.valueOf(tempoHora));
        this.custo1.set(BigDecimal.valueOf(custo1));
        this.custo2.set(BigDecimal.valueOf(custo2));
        this.custo3.set(BigDecimal.valueOf(custo3));
        this.dtInclusao.set(dtInclusao);
        this.dtAlteracao.set(dtAlteracao);
        this.dtAlteracaoOper.set(dtAlteracaoOper);
    }

    public SdRoteiroOrganize001(Integer codorg, String referencia, String descricao, Integer parte, String descPart,
            Double percentual, Integer setor, String descSetor, Double tempo, Double tempoHora, Double custo1, Double custo2,
            Double custo3, String dtInclusao, String dtAlteracao, String dtAlteracaoOper, ObservableList<SdOperRoteiroOrganize001> operacoes) {
        this.codorg.set(codorg);
        this.referencia.set(referencia);
        this.descricao.set(descricao);
        this.parte.set(parte);
        this.descPart.set(descPart);
        this.percentual.set(BigDecimal.valueOf(percentual));
        this.setor.set(setor);
        this.descSetor.set(descSetor);
        this.tempo.set(BigDecimal.valueOf(tempo));
        this.tempoHora.set(BigDecimal.valueOf(tempoHora));
        this.custo1.set(BigDecimal.valueOf(custo1));
        this.custo2.set(BigDecimal.valueOf(custo2));
        this.custo3.set(BigDecimal.valueOf(custo3));
        this.dtInclusao.set(dtInclusao);
        this.dtAlteracao.set(dtAlteracao);
        this.dtAlteracaoOper.set(dtAlteracaoOper);
        this.operacoes.set(operacoes);
    }

    @Id
    @Column(name = "CODORG")
    public int getCodorg() {
        return codorg.get();
    }

    public void setCodorg(int value) {
        codorg.set(value);
        this.setCodigoFilter(String.valueOf(value));
    }

    public IntegerProperty codorgProperty() {
        return codorg;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REFERENCIA", nullable = true)
    public Produto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        this.produto.set(produto);
        this.referencia.set(produto.getCodigo());
    }
    
    @Transient
    public String getReferencia() {
        return referencia.get();
    }

    public void setReferencia(String value) {
        referencia.set(value);
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String value) {
        descricao.set(value);
        setDescricaoFilter(value);
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Column(name = "PARTE")
    public int getParte() {
        return parte.get();
    }

    public void setParte(int value) {
        parte.set(value);
    }

    public IntegerProperty parteProperty() {
        return parte;
    }
    
    @Column(name = "DESC_PART")
    public String getDescPart() {
        return descPart.get();
    }

    public void setDescPart(String value) {
        descPart.set(value);
    }

    public StringProperty descPartProperty() {
        return descPart;
    }
    
    @Column(name = "PERCENTUAL")
    public BigDecimal getPercentual() {
        return percentual.get();
    }

    public void setPercentual(BigDecimal value) {
        percentual.set(value);
    }

    public ObjectProperty<BigDecimal> percentualProperty() {
        return percentual;
    }
    
    @Column(name = "SETOR")
    public int getSetor() {
        return setor.get();
    }

    public void setSetor(int value) {
        setor.set(value);
    }

    public IntegerProperty setorProperty() {
        return setor;
    }

    @Transient
    public String getSdSetor() {
        return sdSetor.get();
    }

    public StringProperty sdSetorProperty() {
        return sdSetor;
    }

    public void setSdSetor(String sdSetor) {
        setCodigoFilter(sdSetor);
        this.sdSetor.set(sdSetor);
    }

    @Column(name = "DESC_SETOR")
    public String getDescSetor() {
        return descSetor.get();
    }

    public void setDescSetor(String value) {
        sdSetor.set(value.substring(0,value.indexOf("|")));
        descSetor.set(value);
    }

    public StringProperty descSetorProperty() {
        return descSetor;
    }
    
    @Column(name = "TEMPO")
    public BigDecimal getTempo() {
        return tempo.get();
    }

    public void setTempo(BigDecimal value) {
        tempo.set(value);
    }

    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }
    
    @Column(name = "TEMPO_HORA")
    public BigDecimal getTempoHora() {
        return tempoHora.get();
    }

    public void setTempoHora(BigDecimal value) {
        tempoHora.set(value);
    }

    public ObjectProperty<BigDecimal> tempoHoraProperty() {
        return tempoHora;
    }
    
    @Column(name = "CUSTO1")
    public BigDecimal getCusto1() {
        return custo1.get();
    }

    public void setCusto1(BigDecimal value) {
        custo1.set(value);
    }

    public ObjectProperty<BigDecimal> custo1Property() {
        return custo1;
    }
    
    @Column(name = "CUSTO2")
    public BigDecimal getCusto2() {
        return custo2.get();
    }

    public void setCusto2(BigDecimal value) {
        custo2.set(value);
    }

    public ObjectProperty<BigDecimal> custo2Property() {
        return custo2;
    }
    
    @Column(name = "CUSTO3")
    public BigDecimal getCusto3() {
        return custo3.get();
    }

    public void setCusto3(BigDecimal value) {
        custo3.set(value);
    }

    public ObjectProperty<BigDecimal> custo3Property() {
        return custo3;
    }
    
    @Column(name = "DT_INCLUSAO")
    public String getDtInclusao() {
        return dtInclusao.get();
    }

    public void setDtInclusao(String value) {
        dtInclusao.set(value);
    }

    public StringProperty dtInclusaoProperty() {
        return dtInclusao;
    }
    
    @Column(name = "DT_ALTERACAO")
    public String getDtAlteracao() {
        return dtAlteracao.get();
    }

    public void setDtAlteracao(String value) {
        dtAlteracao.set(value);
    }

    public StringProperty dtAlteracaoProperty() {
        return dtAlteracao;
    }
    
    @Column(name = "ALTERACAO_MAN")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAlteracaoMan() {
        return alteracaoMan.get();
    }
    
    public BooleanProperty alteracaoManProperty() {
        return alteracaoMan;
    }
    
    public void setAlteracaoMan(boolean alteracaoMan) {
        this.alteracaoMan.set(alteracaoMan);
    }
    
    @Transient
    public String getDtAlteracaoOper() {
        return dtAlteracaoOper.get();
    }
    
    public StringProperty dtAlteracaoOperProperty() {
        return dtAlteracaoOper;
    }
    
    public void setDtAlteracaoOper(String dtAlteracaoOper) {
        this.dtAlteracaoOper.set(dtAlteracaoOper);
    }
    
    @Transient
    public ObservableList<SdOperRoteiroOrganize001> getOperacoes() {
        return operacoes.get();
    }

    public void setOperacoes(ObservableList<SdOperRoteiroOrganize001> value) {
        operacoes.set(value);
    }
    
    public ListProperty<SdOperRoteiroOrganize001> operacoesProperty() {
        return operacoes;
    }
    
    @PostLoad
    private void onPostLoad() throws SQLException {
//        operacoes.set(daoOpersRoteiros.initCriteria().addPredicateEqPkEmbedded("id","roteiro", this.codorg.get(), false).loadListByPredicate());
//        Comparator<SdOperRoteiroOrganize001> comparator = Comparator.comparing(programacao -> programacao.getId().getOrdem());
//        operacoes.sort(comparator);
    }
}
