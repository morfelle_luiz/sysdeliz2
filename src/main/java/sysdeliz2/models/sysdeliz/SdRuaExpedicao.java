package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_RUA_EXPEDICAO_001")
public class SdRuaExpedicao implements Serializable {
    private final StringProperty piso = new SimpleStringProperty();
    private final StringProperty rua = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final IntegerProperty indice = new SimpleIntegerProperty();
    private List<SdLocalExpedicao> locais = new ArrayList<>();

    public SdRuaExpedicao() {
    }

    @Id
    @Column(name = "PISO")
    public String getPiso() {
        return piso.get();
    }

    public StringProperty pisoProperty() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso.set(piso);
    }

    @Id
    @Column(name = "RUA")
    public String getRua() {
        return rua.get();
    }

    public StringProperty ruaProperty() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua.set(rua);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "INDICE")
    public int getIndice() {
        return indice.get();
    }

    public IntegerProperty indiceProperty() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice.set(indice);
    }

    @OneToMany(mappedBy = "rua")
    public List<SdLocalExpedicao> getLocais() {
        return locais;
    }

    public void setLocais(List<SdLocalExpedicao> locais) {
        this.locais = locais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdRuaExpedicao that = (SdRuaExpedicao) o;
        return Objects.equals(piso, that.piso) && Objects.equals(rua, that.rua);
    }

    @Override
    public int hashCode() {
        return Objects.hash(piso, rua);
    }
}
