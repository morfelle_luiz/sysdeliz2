/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

/**
 *
 * @author cristiano.diego
 */
@Entity
@Table(name = "SD_SETOR_OP_001")
@TelaSysDeliz(descricao = "Setor Operação", icon = "setor operacao (4).png")
public class SdSetorOp001 extends BasicModel {

    //Table atributes
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    
    
    public SdSetorOp001() {
    }

    public SdSetorOp001(Integer codigo, String descricao, String ativo) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.ativo.set(ativo.equals("S"));
    }
    
    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    public void setCodigo(int value) {
        setCodigoFilter(""+value);
        codigo.set(value);
    }
    public IntegerProperty codigoProperty() {
        return codigo;
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    public void setDescricao(String value) {
        setDescricaoFilter(value);
        descricao.set(value);
    }
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    public void setAtivo(boolean value) {
        ativo.set(value);
    }
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    @Transient
    public void copy(SdSetorOp001 toCopy) {
        this.codigo.set(toCopy.codigo.get());
        this.descricao.set(toCopy.descricao.get());
        this.ativo.set(toCopy.ativo.get());
        super.setSelected(toCopy.isSelected());
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

}
