package sysdeliz2.models.sysdeliz;

import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "SD_STATUS_PEDIDO_B2C_001")
public class SdStatusPedidoB2C {
    private Integer codigo;
    private String status;
    private boolean padrao;
    private String codMagento;

    public SdStatusPedidoB2C() {
    }

    public SdStatusPedidoB2C(Integer codigo, String status) {
        this.codigo = codigo;
        this.status = status;
    }

    @Id
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "PADRAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPadrao() {
        return padrao;
    }

    public void setPadrao(boolean padrao) {
        this.padrao = padrao;
    }

    @Column(name = "COD_MAGENTO")
    public String getCodMagento() {
        return codMagento;
    }

    public void setCodMagento(String codMagento) {
        this.codMagento = codMagento;
    }

    @Override
    public String toString() {
        return "[" + getCodigo() + "] " + getStatus();
    }
}