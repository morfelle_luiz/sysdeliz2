package sysdeliz2.models.sysdeliz;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

@Entity
@Table(name = "SD_STATUS_REMESSA_001")
@TelaSysDeliz(descricao = "Status Remessa", icon = "status_100.png")
public class SdStatusRemessa extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final BooleanProperty exclusao = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty cancelamento = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty faltagrade = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty reserva = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty excremessa = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty exibeLiberacao = new SimpleBooleanProperty();
    @Transient
    private final StringProperty cor = new SimpleStringProperty();
    
    public SdStatusRemessa() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        super.setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        super.setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "EXCLUSAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isExclusao() {
        return exclusao.get();
    }
    
    public BooleanProperty exclusaoProperty() {
        return exclusao;
    }
    
    public void setExclusao(boolean exclusao) {
        this.exclusao.set(exclusao);
    }
    
    @Column(name = "CANCELAMENTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isCancelamento() {
        return cancelamento.get();
    }
    
    public BooleanProperty cancelamentoProperty() {
        return cancelamento;
    }
    
    public void setCancelamento(boolean cancelamento) {
        this.cancelamento.set(cancelamento);
    }
    
    @Column(name = "FALTA_GRADE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFaltagrade() {
        return faltagrade.get();
    }
    
    public BooleanProperty faltagradeProperty() {
        return faltagrade;
    }
    
    public void setFaltagrade(boolean faltagrade) {
        this.faltagrade.set(faltagrade);
    }
    
    @Column(name = "RESERVA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isReserva() {
        return reserva.get();
    }
    
    public BooleanProperty reservaProperty() {
        return reserva;
    }
    
    public void setReserva(boolean reserva) {
        this.reserva.set(reserva);
    }
    
    @Column(name = "EXC_REMESSA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isExcremessa() {
        return excremessa.get();
    }
    
    public BooleanProperty excremessaProperty() {
        return excremessa;
    }
    
    public void setExcremessa(boolean excremessa) {
        this.excremessa.set(excremessa);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "EXIBE_LIBERACAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isExibeLiberacao() {
        return exibeLiberacao.get();
    }
    
    public BooleanProperty exibeLiberacaoProperty() {
        return exibeLiberacao;
    }
    
    public void setExibeLiberacao(boolean exibeLiberacao) {
        this.exibeLiberacao.set(exibeLiberacao);
    }
}