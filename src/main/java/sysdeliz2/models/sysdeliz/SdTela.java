package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.TipoTelaAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SD_TELAS_001")
@TelaSysDeliz(descricao = "Telas", icon = "cliente (4).png")
public class SdTela extends BasicModel implements Serializable, Comparable<SdTela> {

    public enum TipoTela {JV, KT, FX}

    private final IntegerProperty id = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codTela")
    private final StringProperty codTela = new SimpleStringProperty();
    @ExibeTableView(descricao = "Nome", width = 300)
    @ColunaFilter(descricao = "Nome", coluna = "nomeTela")
    private final StringProperty nomeTela = new SimpleStringProperty();
    private final StringProperty icone = new SimpleStringProperty();
    private final StringProperty path = new SimpleStringProperty();
    private final BooleanProperty menu = new SimpleBooleanProperty();
    private final StringProperty grupoAldap = new SimpleStringProperty();
    private final BooleanProperty mobile = new SimpleBooleanProperty();
    private final StringProperty style = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final ObjectProperty<TipoTela> tipo = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<SdTela> idMenu = new SimpleObjectProperty<>();

    public SdTela() {
    }

    public SdTela(SdTela codMenu, String codTela) {
        this.idMenu.set(codMenu);
        this.codTela.set(codTela);
        this.idMenu.set(codMenu);
    }

    public SdTela(String codTela, String nomeTela) {
        this.codTela.set(codTela);
        this.nomeTela.set(nomeTela);
    }

    public SdTela(String codTela, String nomeTela, String icone, String path, TipoTela tipo) {
        this.codTela.set(codTela);
        this.nomeTela.set(nomeTela);
        this.icone.set(icone);
        this.path.set(path);
        this.tipo.set(tipo);
    }

    @Id
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @Column(name = "COD_TELA")
    public String getCodTela() {
        return codTela.get();
    }

    public StringProperty codTelaProperty() {
        return codTela;
    }

    public void setCodTela(String codTela) {
        setCodigoFilter(codTela);
        this.codTela.set(codTela);
    }

    @Column(name = "NOME_TELA")
    public String getNomeTela() {
        return nomeTela.get();
    }

    public StringProperty nomeTelaProperty() {
        return nomeTela;
    }

    public void setNomeTela(String nomeTela) {
        setDescricaoFilter(nomeTela);
        this.nomeTela.set(nomeTela);
    }

    @Column(name = "ICONE")
    public String getIcone() {
        return icone.get();
    }

    public StringProperty iconeProperty() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone.set(icone);
    }

    @Column(name = "PATH")
    public String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }

    public void setPath(String path) {
        this.path.set(path);
    }

    @Column(name = "MENU")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isMenu() {
        return menu.get();
    }

    public BooleanProperty menuProperty() {
        return menu;
    }

    public void setMenu(boolean menu) {
        this.menu.set(menu);
    }

    @Column(name = "GRUPOALDAP")
    public String getGrupoAldap() {
        return grupoAldap.get();
    }

    public StringProperty grupoAldapProperty() {
        return grupoAldap;
    }

    public void setGrupoAldap(String grupoAldap) {
        this.grupoAldap.set(grupoAldap);
    }

    @Column(name = "MOBILE")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isMobile() {
        return mobile.get();
    }

    public BooleanProperty mobileProperty() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile.set(mobile);
    }

    @Column(name = "STYLE")
    public String getStyle() {
        return style.get();
    }

    public StringProperty styleProperty() {
        return style;
    }

    public void setStyle(String style) {
        this.style.set(style);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "TIPO")
    @Convert(converter = TipoTelaAttributeConverter.class)
    public TipoTela getTipo() {
        return tipo.get();
    }

    public ObjectProperty<TipoTela> tipoProperty() {
        return tipo;
    }

    public void setTipo(TipoTela tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @OneToOne
    @JoinColumn(name = "ID_MENU")
    public SdTela getIdMenu() {
        return idMenu.get();
    }

    public ObjectProperty<SdTela> idMenuProperty() {
        return idMenu;
    }

    public void setIdMenu(SdTela idMenu) {
        this.idMenu.set(idMenu);
    }

    @Override
    public String toString() {
        return this.getCodTela() + " " + this.getNomeTela();
    }

    @Override
    public int compareTo(@NotNull SdTela o) {
        return this.getCodTela().compareTo(o.getCodTela());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdTela sdTela = (SdTela) o;
        return Objects.equals(id, sdTela.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
