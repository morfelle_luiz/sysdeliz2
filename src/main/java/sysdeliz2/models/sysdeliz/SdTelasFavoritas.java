package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SD_TELAS_FAVORITAS")
public class SdTelasFavoritas {

    private final StringProperty codTela = new SimpleStringProperty();
    private final StringProperty nomeUsu = new SimpleStringProperty();

    public SdTelasFavoritas() {
    }

    @Id
    @Column(name = "COD_TELA")
    public String getCodTela() {
        return codTela.get();
    }

    public StringProperty codTelaProperty() {
        return codTela;
    }

    public void setCodTela(String codTela) {
        this.codTela.set(codTela);
    }

    @Column(name = "NOME_USU")
    public String getNomeUsu() {
        return nomeUsu.get();
    }

    public StringProperty nomeUsuProperty() {
        return nomeUsu;
    }

    public void setNomeUsu(String nomeUsu) {
        this.nomeUsu.set(nomeUsu);
    }
}
