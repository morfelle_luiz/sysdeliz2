package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_TEMP_MRP")
@Immutable
public class SdTempMrp {
    
    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty descsetor = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty statusof = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descproduto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty desccor = new SimpleStringProperty();
    private final StringProperty desclinha = new SimpleStringProperty();
    private final StringProperty insumo = new SimpleStringProperty();
    private final StringProperty descinsumo = new SimpleStringProperty();
    private final StringProperty aplicacao = new SimpleStringProperty();
    private final StringProperty grpcomprador = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty corinsumo = new SimpleStringProperty();
    private final StringProperty desccorinsumo = new SimpleStringProperty();
    private final StringProperty codfornecedor = new SimpleStringProperty();
    private final StringProperty descfornecedor = new SimpleStringProperty();
    private final StringProperty setorinsumo = new SimpleStringProperty();
    private final StringProperty faccaoinsumo = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty ordcompra = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dataent = new SimpleObjectProperty<LocalDate>();
    private final StringProperty descfamilia = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumoestoque = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty unidadeestoque = new SimpleStringProperty();
    private final StringProperty dataFat = new SimpleStringProperty();
    private final StringProperty bNaoConf = new SimpleStringProperty();


    public SdTempMrp() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public Integer getIdjpa() {
        return idjpa.get();
    }
    
    public IntegerProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(int idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "DESC_SETOR")
    public String getDescsetor() {
        return descsetor.get();
    }
    
    public StringProperty descsetorProperty() {
        return descsetor;
    }
    
    public void setDescsetor(String descsetor) {
        this.descsetor.set(descsetor);
    }
    
    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }
    
    public StringProperty parteProperty() {
        return parte;
    }
    
    public void setParte(String parte) {
        this.parte.set(parte);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusof() {
        return statusof.get();
    }
    
    public StringProperty statusofProperty() {
        return statusof;
    }
    
    public void setStatusof(String statusof) {
        this.statusof.set(statusof);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESC_PRODUTO")
    public String getDescproduto() {
        return descproduto.get();
    }
    
    public StringProperty descprodutoProperty() {
        return descproduto;
    }
    
    public void setDescproduto(String descproduto) {
        this.descproduto.set(descproduto);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "DESC_COR")
    public String getDesccor() {
        return desccor.get();
    }
    
    public StringProperty desccorProperty() {
        return desccor;
    }
    
    public void setDesccor(String desccor) {
        this.desccor.set(desccor);
    }
    
    @Column(name = "DESC_LINHA")
    public String getDesclinha() {
        return desclinha.get();
    }
    
    public StringProperty desclinhaProperty() {
        return desclinha;
    }
    
    public void setDesclinha(String desclinha) {
        this.desclinha.set(desclinha);
    }
    
    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }
    
    public StringProperty insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }
    
    @Column(name = "DESC_INSUMO")
    public String getDescinsumo() {
        return descinsumo.get();
    }
    
    public StringProperty descinsumoProperty() {
        return descinsumo;
    }
    
    public void setDescinsumo(String descinsumo) {
        this.descinsumo.set(descinsumo);
    }
    
    @Column(name = "APLICACAO")
    public String getAplicacao() {
        return aplicacao.get();
    }
    
    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(String aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @Column(name = "GRP_COMPRADOR")
    public String getGrpcomprador() {
        return grpcomprador.get();
    }
    
    public StringProperty grpcompradorProperty() {
        return grpcomprador;
    }
    
    public void setGrpcomprador(String grpcomprador) {
        this.grpcomprador.set(grpcomprador);
    }
    
    @Column(name = "CONSUMO")
    public BigDecimal getConsumo() {
        return consumo.get();
    }
    
    public ObjectProperty<BigDecimal> consumoProperty() {
        return consumo;
    }
    
    public void setConsumo(BigDecimal consumo) {
        this.consumo.set(consumo);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "COR_INSUMO")
    public String getCorinsumo() {
        return corinsumo.get();
    }
    
    public StringProperty corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(String corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @Column(name = "DESC_COR_INSUMO")
    public String getDesccorinsumo() {
        return desccorinsumo.get();
    }
    
    public StringProperty desccorinsumoProperty() {
        return desccorinsumo;
    }
    
    public void setDesccorinsumo(String desccorinsumo) {
        this.desccorinsumo.set(desccorinsumo);
    }
    
    @Column(name = "COD_FORNECEDOR")
    public String getCodfornecedor() {
        return codfornecedor.get();
    }
    
    public StringProperty codfornecedorProperty() {
        return codfornecedor;
    }
    
    public void setCodfornecedor(String codfornecedor) {
        this.codfornecedor.set(codfornecedor);
    }
    
    @Column(name = "DESC_FORNECEDOR")
    public String getDescfornecedor() {
        return descfornecedor.get();
    }
    
    public StringProperty descfornecedorProperty() {
        return descfornecedor;
    }
    
    public void setDescfornecedor(String descfornecedor) {
        this.descfornecedor.set(descfornecedor);
    }
    
    @Column(name = "SETOR_INSUMO")
    public String getSetorinsumo() {
        return setorinsumo.get();
    }
    
    public StringProperty setorinsumoProperty() {
        return setorinsumo;
    }
    
    public void setSetorinsumo(String setorinsumo) {
        this.setorinsumo.set(setorinsumo);
    }
    
    @Column(name = "FACCAO_INSUMO")
    public String getFaccaoinsumo() {
        return faccaoinsumo.get();
    }
    
    public StringProperty faccaoinsumoProperty() {
        return faccaoinsumo;
    }
    
    public void setFaccaoinsumo(String faccaoinsumo) {
        this.faccaoinsumo.set(faccaoinsumo);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }
    
    public StringProperty estoqueProperty() {
        return estoque;
    }
    
    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }
    
    @Column(name = "ORD_COMPRA")
    public String getOrdcompra() {
        return ordcompra.get();
    }
    
    public StringProperty ordcompraProperty() {
        return ordcompra;
    }
    
    public void setOrdcompra(String ordcompra) {
        this.ordcompra.set(ordcompra);
    }
    
    @Column(name = "DATA_ENT")
    public LocalDate getDataent() {
        return dataent.get();
    }
    
    public ObjectProperty<LocalDate> dataentProperty() {
        return dataent;
    }
    
    public void setDataent(LocalDate dataent) {
        this.dataent.set(dataent);
    }
    
    @Column(name = "DESC_FAMILIA")
    public String getDescfamilia() {
        return descfamilia.get();
    }
    
    public StringProperty descfamiliaProperty() {
        return descfamilia;
    }
    
    public void setDescfamilia(String descfamilia) {
        this.descfamilia.set(descfamilia);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }
    
    public StringProperty faixaProperty() {
        return faixa;
    }
    
    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }
    
    @Column(name = "CONSUMO_ESTOQUE")
    public BigDecimal getConsumoestoque() {
        return consumoestoque.get();
    }
    
    public ObjectProperty<BigDecimal> consumoestoqueProperty() {
        return consumoestoque;
    }
    
    public void setConsumoestoque(BigDecimal consumoestoque) {
        this.consumoestoque.set(consumoestoque);
    }
    
    @Column(name = "UNIDADE_ESTOQUE")
    public String getUnidadeestoque() {
        return unidadeestoque.get();
    }
    
    public StringProperty unidadeestoqueProperty() {
        return unidadeestoque;
    }
    
    public void setUnidadeestoque(String unidadeestoque) {
        this.unidadeestoque.set(unidadeestoque);
    }

    @Column(name = "DATA_FAT")
    public String getDataFat() {
        return dataFat.get();
    }

    public StringProperty dataFatProperty() {
        return dataFat;
    }

    public void setDataFat(String dataFat) {
        this.dataFat.set(dataFat);
    }



    @Column(name = "B_NAO_CONF")
    public String getbNaoConf() {
        return bNaoConf.get();
    }

    public StringProperty bNaoConfProperty() {
        return bNaoConf;
    }

    public void setbNaoConf(String bNaoConf) {
        this.bNaoConf.set(bNaoConf);
    }
}
