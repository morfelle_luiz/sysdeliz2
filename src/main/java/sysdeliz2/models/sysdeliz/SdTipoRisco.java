package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Pcpapl;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "SD_TIPO_RISCO_001")
public class SdTipoRisco extends BasicModel {
    
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty codrisco = new SimpleStringProperty();
    private final StringProperty color = new SimpleStringProperty();
    private final ObjectProperty<Pcpapl> aplicacao = new SimpleObjectProperty<>();
    private final BooleanProperty usacor = new SimpleBooleanProperty();
    
    public SdTipoRisco() {
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SD_SEQ_TIPO_RISCO")
    @SequenceGenerator(name="SD_SEQ_TIPO_RISCO", sequenceName="SEQ_SD_TIPO_RISCO_001", allocationSize=1)
    @Column(name = "CODIGO")
    public Integer getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(String.valueOf(codigo));
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "CODRISCO")
    public String getCodrisco() {
        return codrisco.get();
    }
    
    public StringProperty codriscoProperty() {
        return codrisco;
    }
    
    public void setCodrisco(String codrisco) {
        this.codrisco.set(codrisco);
    }
    
    @Column(name = "COLOR")
    public String getColor() {
        return color.get();
    }
    
    public StringProperty colorProperty() {
        return color;
    }
    
    public void setColor(String color) {
        this.color.set(color);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APLICACAO")
    public Pcpapl getAplicacao() {
        return aplicacao.get();
    }
    
    public ObjectProperty<Pcpapl> aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(Pcpapl aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @Column(name = "USA_COR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isUsacor() {
        return usacor.get();
    }
    
    public BooleanProperty usacorProperty() {
        return usacor;
    }
    
    public void setUsacor(boolean usacor) {
        this.usacor.set(usacor);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}