package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SD_TIPO_TRIBUTACAO_001")
public class SdTipoTributacao001 {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    
    public SdTipoTributacao001() {
    
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
