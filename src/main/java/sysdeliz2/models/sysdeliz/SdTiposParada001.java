package sysdeliz2.models.sysdeliz;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.BuscaPadrao;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

@Entity
@Table(name = "SD_TIPOS_PARADA_001")
@TelaSysDeliz(descricao = "TIPOS PARADA", icon = "paradas (4).png")
public class SdTiposParada001 extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @BuscaPadrao(descricao = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "efic.", width = 50, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty dedutivel = new SimpleBooleanProperty(false);
    @Transient
    @ExibeTableView(descricao = "prod.", width = 50, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty deduzProd = new SimpleBooleanProperty(false);
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 50, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty comTempo = new SimpleBooleanProperty();
    
    public SdTiposParada001() {
    }
    
    public SdTiposParada001(Integer codigo) {
        this.codigo.set(codigo);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ai_codigo_tipos_parada")
    @SequenceGenerator(name = "ai_codigo_tipos_parada", sequenceName = "seq_sd_tipos_parada", allocationSize = 1)
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        super.setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "DEDUTIVEL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isDedutivel() {
        return dedutivel.get();
    }
    
    public BooleanProperty dedutivelProperty() {
        return dedutivel;
    }
    
    public void setDedutivel(boolean dedutivel) {
        this.dedutivel.set(dedutivel);
    }
    
    @Column(name = "DEDUZ_PROD")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean getDeduzProd() {
        return deduzProd.get();
    }
    
    public BooleanProperty deduzProdProperty() {
        return deduzProd;
    }
    
    public void setDeduzProd(boolean deduzProd) {
        this.deduzProd.set(deduzProd);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
    
    @Column(name = "COM_TEMPO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isComTempo() {
        return comTempo.get();
    }
    
    public BooleanProperty comTempoProperty() {
        return comTempo;
    }
    
    public void setComTempo(boolean comTempo) {
        this.comTempo.set(comTempo);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.getValue();
    }
}
