package sysdeliz2.models.sysdeliz;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

@Entity
@Table(name = "SD_TRANSP_PEDIDO_B2C_001")
public class SdTranspPedidoB2C {
    private StringProperty codigo = new SimpleStringProperty();
    private StringProperty transporte = new SimpleStringProperty();
    private StringProperty mensagemPadrao = new SimpleStringProperty();
    private StringProperty codTrans = new SimpleStringProperty();
    private StringProperty carrierCode = new SimpleStringProperty();

    public SdTranspPedidoB2C() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "TRANSPORTE")
    public String getTransporte() {
        return transporte.get();
    }

    public StringProperty transporteProperty() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte.set(transporte);
    }

    @Lob
    @Column(name = "MENSAGEM_PADRAO")
    public String getMensagemPadrao() {
        return mensagemPadrao.get();
    }

    public StringProperty mensagemPadraoProperty() {
        return mensagemPadrao;
    }

    public void setMensagemPadrao(String mensagemPadrao) {
        this.mensagemPadrao.set(mensagemPadrao);
    }

    @Column(name = "COD_TRANS")
    public String getCodTrans() {
        return codTrans.get();
    }

    public StringProperty codTransProperty() {
        return codTrans;
    }

    public void setCodTrans(String codTrans) {
        this.codTrans.set(codTrans);
    }

    @Column(name = "CARRIER_CODE")
    public String getCarrierCode() {
        return carrierCode.get();
    }

    public StringProperty carrierCodeProperty() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode.set(carrierCode);
    }

    @Override
    public String toString() {
        return transporte.get();
    }


}
