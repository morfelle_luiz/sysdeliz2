/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.sysdeliz;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

/**
 * @author cristiano.diego
 */

@Entity
@Table(name = "SD_TURNO_001")
@TelaSysDeliz(descricao = "Coleção", icon = "turno_100.png")
public class SdTurno extends BasicModel implements Serializable {
    
    // Properties table
    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 320)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty inicioTurno = new SimpleStringProperty();
    @Transient
    private final StringProperty fimTurno = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Início Turno", width = 100, columnType = ColumnType.DATETIME)
    private final ObjectProperty<LocalDateTime> dhInicioTurno = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Fim Turno", width = 100, columnType = ColumnType.DATETIME)
    private final ObjectProperty<LocalDateTime> dhFimTurno = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    @Transient
    private final IntegerProperty tempoInterjornada = new SimpleIntegerProperty();

    // Properties Controllers
    @Transient
    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    
    public SdTurno() {
    }
    
    public SdTurno(Integer codigo, String descricao,
                   String inicioTurno, String fimTurno,
                   String ativo, LocalDateTime dhInicioTurno, LocalDateTime dhFimTurno) {
        this.codigo.set(codigo);
        this.descricao.set(descricao);
        this.inicioTurno.set(inicioTurno);
        this.fimTurno.set(fimTurno);
        this.dhFimTurno.set(dhFimTurno);
        this.dhInicioTurno.set(dhInicioTurno);
        this.ativo.set(ativo.equals("S"));
    }
    
    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }
    
    public void setCodigo(int value) {
        codigo.set(value);
        setCodigoFilter(String.valueOf(value));
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public void setDescricao(String value) {
        descricao.set(value);
        setDescricaoFilter(value);
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    @Transient
    public final String getInicioTurno() {
        return inicioTurno.get();
    }
    
    @Transient
    @JsonIgnore
    public final LocalTime getInicioTurnoAsLocaTime() {
        return this.getDhInicioTurno().toLocalTime();
    }
    
    public final void setInicioTurno(String value) {
        inicioTurno.set(value);
    }
    
    public StringProperty inicioTurnoProperty() {
        return inicioTurno;
    }
    
    @Transient
    public final String getFimTurno() {
        return fimTurno.get();
    }
    
    @Transient
    @JsonIgnore
    public final LocalTime getFimTurnoAsLocaTime() {
        return this.getDhFimTurno().toLocalTime();
    }
    
    public final void setFimTurno(String value) {
        fimTurno.set(value);
    }
    
    public StringProperty fimTurnoProperty() {
        return fimTurno;
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public void setAtivo(boolean value) {
        ativo.set(value);
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    @Transient
    @JsonIgnore
    public final boolean isSelected() {
        return selected.get();
    }
    
    public final void setSelected(boolean value) {
        selected.set(value);
    }
    
    public BooleanProperty selectedProperty() {
        return selected;
    }
    
    @Column(name = "HR_INICIO", columnDefinition = "DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @JsonIgnore
    public LocalDateTime getDhInicioTurno() {
        return dhInicioTurno.get();
    }
    
    public void setDhInicioTurno(LocalDateTime value) {
        dhInicioTurno.set(value);
    }
    
    public ObjectProperty<LocalDateTime> dhInicioTurnoProperty() {
        return dhInicioTurno;
    }
    
    @Column(name = "HR_FIM", columnDefinition = "DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @JsonIgnore
    public LocalDateTime getDhFimTurno() {
        return dhFimTurno.get();
    }
    
    public void setDhFimTurno(LocalDateTime value) {
        dhFimTurno.set(value);
    }
    
    public ObjectProperty<LocalDateTime> dhFimTurnoProperty() {
        return dhFimTurno;
    }
    
    @Column(name = "TEMPO_INTERJORNADA")
    @JsonIgnore
    public int getTempoInterjornada() {
        return tempoInterjornada.get();
    }
    
    public IntegerProperty tempoInterjornadaProperty() {
        return tempoInterjornada;
    }
    
    public void setTempoInterjornada(int tempoInterjornada) {
        this.tempoInterjornada.set(tempoInterjornada);
    }
    
    public void copy(SdTurno period) {
        this.codigo.set(period.codigo.get());
        this.descricao.set(period.descricao.get());
        this.inicioTurno.set(period.inicioTurno.get());
        this.fimTurno.set(period.fimTurno.get());
        this.ativo.set(period.ativo.get());
        this.selected.set(period.selected.get());
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SdTurno other = (SdTurno) obj;
        if (!Objects.equals(this.codigo.get(), other.codigo.get())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
