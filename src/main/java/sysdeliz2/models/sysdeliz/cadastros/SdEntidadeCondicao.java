package sysdeliz2.models.sysdeliz.cadastros;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Condicao;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_ENTIDADE_CONDICAO_001")
public class SdEntidadeCondicao implements Serializable {

    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<Condicao> codcond = new SimpleObjectProperty<>();

    public SdEntidadeCondicao() {
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "CODCOND")
    public Condicao getCodcond() {
        return codcond.get();
    }

    public ObjectProperty<Condicao> codcondProperty() {
        return codcond;
    }

    public void setCodcond(Condicao codcond) {
        this.codcond.set(codcond);
    }
}