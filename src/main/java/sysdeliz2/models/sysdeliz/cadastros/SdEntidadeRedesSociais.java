package sysdeliz2.models.sysdeliz.cadastros;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdRedesSociais;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_ENTIDADE_REDES_SOCIAIS_001")
public class SdEntidadeRedesSociais implements Serializable {

    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<SdRedesSociais> redesocial = new SimpleObjectProperty<>();
    private final StringProperty link = new SimpleStringProperty();
    private final IntegerProperty id = new SimpleIntegerProperty();

    public SdEntidadeRedesSociais() {
    }

    public SdEntidadeRedesSociais(String codcli, SdRedesSociais redesocial, String link) {
        this.codcli.set(codcli);
        this.redesocial.set(redesocial);
        this.link.set(link);
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "REDE_SOCIAL")
    public SdRedesSociais getRedesocial() {
        return redesocial.get();
    }

    public ObjectProperty<SdRedesSociais> redesocialProperty() {
        return redesocial;
    }

    public void setRedesocial(SdRedesSociais redesocial) {
        this.redesocial.set(redesocial);
    }

    @Column(name = "LINK")
    public String getLink() {
        return link.get();
    }

    public StringProperty linkProperty() {
        return link;
    }

    public void setLink(String link) {
        this.link.set(link);
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SD_SEQ_SD_ENT_REDES_SOCIAIS")
    @SequenceGenerator(sequenceName = "SD_SEQ_SD_ENT_REDES_SOCIAIS", name = "SD_SEQ_SD_ENT_REDES_SOCIAIS", allocationSize = 1)
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }
}