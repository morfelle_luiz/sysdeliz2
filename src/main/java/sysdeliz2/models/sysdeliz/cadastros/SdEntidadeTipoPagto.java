package sysdeliz2.models.sysdeliz.cadastros;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_ENTIDADE_TIPO_PAGTO_001")
public class SdEntidadeTipoPagto implements Serializable {

    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<SdTipoPagto> codtipo = new SimpleObjectProperty<>();

    public SdEntidadeTipoPagto() {
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "CODTIPO")
    public SdTipoPagto getCodtipo() {
        return codtipo.get();
    }

    public ObjectProperty<SdTipoPagto> codtipoProperty() {
        return codtipo;
    }

    public void setCodtipo(SdTipoPagto codtipo) {
        this.codtipo.set(codtipo);
    }
}