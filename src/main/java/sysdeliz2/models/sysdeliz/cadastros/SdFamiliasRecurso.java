package sysdeliz2.models.sysdeliz.cadastros;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Familia;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sd_familias_recurso_001")
public class SdFamiliasRecurso implements Serializable {

    private final ObjectProperty<SdFamiliasRecursoPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty prioridade = new SimpleIntegerProperty();

    public SdFamiliasRecurso() {
    }

    public SdFamiliasRecurso(SdRecursoProducao recurso, Familia familia, Integer prioridade) {
        setId(new SdFamiliasRecursoPK(recurso, familia));
        setPrioridade(prioridade);
    }

    @EmbeddedId
    public SdFamiliasRecursoPK getId() {
        return id.get();
    }

    public ObjectProperty<SdFamiliasRecursoPK> idProperty() {
        return id;
    }

    public void setId(SdFamiliasRecursoPK id) {
        this.id.set(id);
    }

    @Column(name = "prioridade")
    @JsonProperty
    public Integer getPrioridade() {
        return prioridade.get();
    }

    public IntegerProperty prioridadeProperty() {
        return prioridade;
    }

    public void setPrioridade(Integer prioridade) {
        this.prioridade.set(prioridade);
    }

    @Transient
    @JsonIgnore
    public String toLog() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error to string object";
        }
    }
}
