package sysdeliz2.models.sysdeliz.cadastros;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Familia;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdFamiliasRecursoPK implements Serializable {

    private final ObjectProperty<Familia> familia = new SimpleObjectProperty<>();
    private final ObjectProperty<SdRecursoProducao> recurso = new SimpleObjectProperty<>();

    public SdFamiliasRecursoPK() {
    }

    public SdFamiliasRecursoPK(SdRecursoProducao recurso, Familia familia) {
        setFamilia(familia);
        setRecurso(recurso);
    }

    @OneToOne
    @JoinColumn(name = "familia")
    @JsonProperty
    public Familia getFamilia() {
        return familia.get();
    }

    public ObjectProperty<Familia> familiaProperty() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia.set(familia);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "SETOR_EXCIA", referencedColumnName = "SETOR_EXCIA"),
            @JoinColumn(name = "FORNECEDOR", referencedColumnName = "FORNECEDOR"),
            @JoinColumn(name = "TURNO", referencedColumnName = "TURNO"),
            @JoinColumn(name = "COLECAO", referencedColumnName = "COLECAO"),
    })
    @JsonIgnore
    public SdRecursoProducao getRecurso() {
        return recurso.get();
    }

    public ObjectProperty<SdRecursoProducao> recursoProperty() {
        return recurso;
    }

    public void setRecurso(SdRecursoProducao recurso) {
        this.recurso.set(recurso);
    }
}
