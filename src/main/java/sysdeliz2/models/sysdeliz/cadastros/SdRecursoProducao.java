package sysdeliz2.models.sysdeliz.cadastros;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdTurno;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sd_recurso_producao_002")
@IdClass(SdRecursoProducaoPK.class)
public class SdRecursoProducao implements Serializable {

    public enum TipoCapacidade {M, P}

    private final ObjectProperty<CadFluxo> setorExcia = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosEntidade> fornecedor = new SimpleObjectProperty<>();
    private final ObjectProperty<SdTurno> turno = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final IntegerProperty equipamentos = new SimpleIntegerProperty(1);
    private final ObjectProperty<BigDecimal> tempoEquipamento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tempoFila = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> eficiencia = new SimpleObjectProperty<>(BigDecimal.ONE);
    private final ObjectProperty<BigDecimal> margem = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final ObjectProperty<SdColaborador> controlador = new SimpleObjectProperty<>();
    private final ObjectProperty<TipoCapacidade> tipoCapacidade = new SimpleObjectProperty<>(TipoCapacidade.M);
    private List<SdFamiliasRecurso> familias = new ArrayList<>();

    public SdRecursoProducao() {
    }

    @Id
    @OneToOne
    @JoinColumn(name = "SETOR_EXCIA")
    @JsonProperty
    public CadFluxo getSetorExcia() {
        return setorExcia.get();
    }

    public ObjectProperty<CadFluxo> setorExciaProperty() {
        return setorExcia;
    }

    public void setSetorExcia(CadFluxo setorExcia) {
        this.setorExcia.set(setorExcia);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "FORNECEDOR")
    @JsonProperty
    public VSdDadosEntidade getFornecedor() {
        return fornecedor.get();
    }

    public ObjectProperty<VSdDadosEntidade> fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(VSdDadosEntidade fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "TURNO")
    @JsonProperty
    public SdTurno getTurno() {
        return turno.get();
    }

    public ObjectProperty<SdTurno> turnoProperty() {
        return turno;
    }

    public void setTurno(SdTurno turno) {
        this.turno.set(turno);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "COLECAO")
    @JsonProperty
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "equipamentos")
    @JsonProperty
    public int getEquipamentos() {
        return equipamentos.get();
    }

    public IntegerProperty equipamentosProperty() {
        return equipamentos;
    }

    public void setEquipamentos(int equipamentos) {
        this.equipamentos.set(equipamentos);
    }

    @Column(name = "tempo_equipamentos")
    @JsonProperty
    public BigDecimal getTempoEquipamento() {
        return tempoEquipamento.get();
    }

    public ObjectProperty<BigDecimal> tempoEquipamentoProperty() {
        return tempoEquipamento;
    }

    public void setTempoEquipamento(BigDecimal tempoEquipamento) {
        this.tempoEquipamento.set(tempoEquipamento);
    }

    @Column(name = "tempo_fila")
    @JsonProperty
    public BigDecimal getTempoFila() {
        return tempoFila.get();
    }

    public ObjectProperty<BigDecimal> tempoFilaProperty() {
        return tempoFila;
    }

    public void setTempoFila(BigDecimal tempoFila) {
        this.tempoFila.set(tempoFila);
    }

    @Column(name = "eficiencia")
    @JsonProperty
    public BigDecimal getEficiencia() {
        return eficiencia.get();
    }

    public ObjectProperty<BigDecimal> eficienciaProperty() {
        return eficiencia;
    }

    public void setEficiencia(BigDecimal eficiencia) {
        this.eficiencia.set(eficiencia);
    }

    @Column(name = "margem")
    @JsonProperty
    public BigDecimal getMargem() {
        return margem.get();
    }

    public ObjectProperty<BigDecimal> margemProperty() {
        return margem;
    }

    public void setMargem(BigDecimal margem) {
        this.margem.set(margem);
    }

    @Column(name = "ativo")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonProperty
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @OneToOne
    @JoinColumn(name = "controlador")
    public SdColaborador getControlador() {
        return controlador.get();
    }

    public ObjectProperty<SdColaborador> controladorProperty() {
        return controlador;
    }

    public void setControlador(SdColaborador controlador) {
        this.controlador.set(controlador);
    }

    @Column(name = "tipo_capacidade")
    @Enumerated(EnumType.STRING)
    public TipoCapacidade getTipoCapacidade() {
        return tipoCapacidade.get();
    }

    public ObjectProperty<TipoCapacidade> tipoCapacidadeProperty() {
        return tipoCapacidade;
    }

    public void setTipoCapacidade(TipoCapacidade tipoCapacidade) {
        this.tipoCapacidade.set(tipoCapacidade);
    }

    @OneToMany(mappedBy = "id.recurso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonProperty
    public List<SdFamiliasRecurso> getFamilias() {
        return familias;
    }

    public void setFamilias(List<SdFamiliasRecurso> familias) {
        this.familias = familias;
    }

    @Transient
    @JsonIgnore
    public String codigo() {
        return getColecao().getCodigo() + "-" + getTurno().getCodigo() + "-" + getFornecedor().getCodcli() + "-" + getSetorExcia().getCodigo();
    }

    @Transient
    @JsonIgnore
    public String toLog() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error to string object";
        }
    }
}