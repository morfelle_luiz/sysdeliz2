package sysdeliz2.models.sysdeliz.cadastros;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.sysdeliz.SdTurno;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.view.VSdDadosEntidade;

import java.io.Serializable;

public class SdRecursoProducaoPK implements Serializable {

    private final ObjectProperty<CadFluxo> setorExcia = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosEntidade> fornecedor = new SimpleObjectProperty<>();
    private final ObjectProperty<SdTurno> turno = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();

    public SdRecursoProducaoPK() {
    }

    public CadFluxo getSetorExcia() {
        return setorExcia.get();
    }

    public ObjectProperty<CadFluxo> setorExciaProperty() {
        return setorExcia;
    }

    public void setSetorExcia(CadFluxo setorExcia) {
        this.setorExcia.set(setorExcia);
    }

    public VSdDadosEntidade getFornecedor() {
        return fornecedor.get();
    }

    public ObjectProperty<VSdDadosEntidade> fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(VSdDadosEntidade fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    public SdTurno getTurno() {
        return turno.get();
    }

    public ObjectProperty<SdTurno> turnoProperty() {
        return turno;
    }

    public void setTurno(SdTurno turno) {
        this.turno.set(turno);
    }

    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

}
