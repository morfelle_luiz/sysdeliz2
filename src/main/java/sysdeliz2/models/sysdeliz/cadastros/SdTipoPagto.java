package sysdeliz2.models.sysdeliz.cadastros;

import javafx.beans.property.*;
import org.hibernate.Hibernate;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SD_TIPO_PAGTO_001")
@TelaSysDeliz(descricao = "Tipos de Pagamento", icon = "pagto cartao_50.png")
public class SdTipoPagto extends BasicModel {

    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 50)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);

    public SdTipoPagto() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_TIPO_PAGTO_001")
    @SequenceGenerator(name = "SEQ_SD_TIPO_PAGTO_001", sequenceName = "SEQ_SD_TIPO_PAGTO_001", allocationSize = 1)
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
        super.setCodigoFilter(String.valueOf(codigo));
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        super.setDescricaoFilter(descricao);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SdTipoPagto that = (SdTipoPagto) o;
        return Objects.equals(getCodigo(), that.getCodigo());
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
