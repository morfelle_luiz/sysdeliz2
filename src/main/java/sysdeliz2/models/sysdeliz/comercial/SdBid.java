package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.hibernate.annotations.Formula;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_BID_001")
public class SdBid extends BasicModel implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final StringProperty codrep = new SimpleStringProperty();
        private final StringProperty codmar = new SimpleStringProperty();
        private final StringProperty codcol = new SimpleStringProperty();

        public Pk() {
        }

        @Column(name = "CODREP")
        public String getCodrep() {
            return codrep.get();
        }

        public StringProperty codrepProperty() {
            return codrep;
        }

        public void setCodrep(String codrep) {
            this.codrep.set(codrep);
        }

        @Column(name = "CODMAR")
        public String getCodmar() {
            return codmar.get();
        }

        public StringProperty codmarProperty() {
            return codmar;
        }

        public void setCodmar(String codmar) {
            this.codmar.set(codmar);
        }

        @Column(name = "CODCOL")
        public String getCodcol() {
            return codcol.get();
        }

        public StringProperty codcolProperty() {
            return codcol;
        }

        public void setCodcol(String codcol) {
            this.codcol.set(codcol);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty emailrep = new SimpleStringProperty();
    private final StringProperty ufrep = new SimpleStringProperty();
    private final StringProperty codreg = new SimpleStringProperty();
    private final StringProperty regiao = new SimpleStringProperty();
    private final StringProperty codger = new SimpleStringProperty();
    private final StringProperty gerente = new SimpleStringProperty();
    private final StringProperty emailger = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> semtotal = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> semhoje = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> mostr = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pmrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pmrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pmmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pmly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pmRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pmRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> cdrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cdrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cdmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cdly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cdRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> cdRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pvrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pvrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pvmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pvly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pvRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pvRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> karcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> karpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> kamta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> kaly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> kaRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> kaRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pprcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pprpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> ppmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pply = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> ppRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> ppRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> vprcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vprpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vpmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vply = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vpRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> vpRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tprcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tprpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tpmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tply = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tpRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tpRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tfrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tfrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tfmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tfly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> tfRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tfRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pgrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pgrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pgmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pgly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pgRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pgRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> prrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> prrpm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> prmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> prly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> prRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> prRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> clnrcm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> clnmta = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> clnly = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> clnRelMta = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> clnRelLy = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> ratrcm = new SimpleObjectProperty<BigDecimal>();

    private List<SdBil> bil = new ArrayList<>();
    private List<VSdBidCidade> cidades = new ArrayList<>();

    public SdBid() {
    }

    // <editor-fold defaultstate="collapsed" desc="columns">
    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
        super.setCodigoFilter(String.valueOf(id.hashCode()));
    }

    @Column(name = "REPRESENT")
    public String getRepresent() {
        return represent.get();
    }

    public StringProperty representProperty() {
        return represent;
    }

    public void setRepresent(String represent) {
        this.represent.set(represent);
        super.setDescricaoFilter(represent);
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    @Column(name = "EMAIL_REP")
    public String getEmailrep() {
        return emailrep.get();
    }

    public StringProperty emailrepProperty() {
        return emailrep;
    }

    public void setEmailrep(String emailrep) {
        this.emailrep.set(emailrep);
    }

    @Column(name = "UF_REP")
    public String getUfrep() {
        return ufrep.get();
    }

    public StringProperty ufrepProperty() {
        return ufrep;
    }

    public void setUfrep(String ufrep) {
        this.ufrep.set(ufrep);
    }

    @Column(name = "CODREG")
    public String getCodreg() {
        return codreg.get();
    }

    public StringProperty codregProperty() {
        return codreg;
    }

    public void setCodreg(String codreg) {
        this.codreg.set(codreg);
    }

    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }

    @Column(name = "CODGER")
    public String getCodger() {
        return codger.get();
    }

    public StringProperty codgerProperty() {
        return codger;
    }

    public void setCodger(String codger) {
        this.codger.set(codger);
    }

    @Column(name = "GERENTE")
    public String getGerente() {
        return gerente.get();
    }

    public StringProperty gerenteProperty() {
        return gerente;
    }

    public void setGerente(String gerente) {
        this.gerente.set(gerente);
    }

    @Column(name = "EMAIL_GER")
    public String getEmailger() {
        return emailger.get();
    }

    public StringProperty emailgerProperty() {
        return emailger;
    }

    public void setEmailger(String emailger) {
        this.emailger.set(emailger);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "SEM_TOTAL")
    public BigDecimal getSemtotal() {
        return semtotal.get();
    }

    public ObjectProperty<BigDecimal> semtotalProperty() {
        return semtotal;
    }

    public void setSemtotal(BigDecimal semtotal) {
        this.semtotal.set(semtotal);
    }

    @Column(name = "SEM_HOJE")
    public BigDecimal getSemhoje() {
        return semhoje.get();
    }

    public ObjectProperty<BigDecimal> semhojeProperty() {
        return semhoje;
    }

    public void setSemhoje(BigDecimal semhoje) {
        this.semhoje.set(semhoje);
    }

    @Column(name = "MOSTR")
    @Formula("COALESCE(MOSTR, 0)")
    public BigDecimal getMostr() {
        return mostr.get();
    }

    public ObjectProperty<BigDecimal> mostrProperty() {
        return mostr;
    }

    public void setMostr(BigDecimal mostr) {
        this.mostr.set(mostr);
    }

    @Column(name = "PM_RCM")
    public BigDecimal getPmrcm() {
        return pmrcm.get();
    }

    public ObjectProperty<BigDecimal> pmrcmProperty() {
        return pmrcm;
    }

    public void setPmrcm(BigDecimal pmrcm) {
        this.pmrcm.set(pmrcm);
    }

    @Column(name = "PM_RPM")
    @Formula("COALESCE(PM_RPM, 0)")
    public BigDecimal getPmrpm() {
        return pmrpm.get();
    }

    public ObjectProperty<BigDecimal> pmrpmProperty() {
        return pmrpm;
    }

    public void setPmrpm(BigDecimal pmrpm) {
        this.pmrpm.set(pmrpm);
    }

    @Column(name = "PM_MTA")
    @Formula("COALESCE(PM_MTA, 0)")
    public BigDecimal getPmmta() {
        return pmmta.get();
    }

    public ObjectProperty<BigDecimal> pmmtaProperty() {
        return pmmta;
    }

    public void setPmmta(BigDecimal pmmta) {
        this.pmmta.set(pmmta);
    }

    @Column(name = "PM_LY")
    public BigDecimal getPmly() {
        return pmly.get();
    }

    public ObjectProperty<BigDecimal> pmlyProperty() {
        return pmly;
    }

    public void setPmly(BigDecimal pmly) {
        this.pmly.set(pmly);
    }

    @Column(name = "CD_RCM")
    public BigDecimal getCdrcm() {
        return cdrcm.get();
    }

    public ObjectProperty<BigDecimal> cdrcmProperty() {
        return cdrcm;
    }

    public void setCdrcm(BigDecimal cdrcm) {
        this.cdrcm.set(cdrcm);
    }

    @Column(name = "CD_RPM")
    @Formula("COALESCE(CD_RPM, 0)")
    public BigDecimal getCdrpm() {
        return cdrpm.get();
    }

    public ObjectProperty<BigDecimal> cdrpmProperty() {
        return cdrpm;
    }

    public void setCdrpm(BigDecimal cdrpm) {
        this.cdrpm.set(cdrpm);
    }

    @Column(name = "CD_MTA")
    @Formula("COALESCE(CD_MTA, 0)")
    public BigDecimal getCdmta() {
        return cdmta.get();
    }

    public ObjectProperty<BigDecimal> cdmtaProperty() {
        return cdmta;
    }

    public void setCdmta(BigDecimal cdmta) {
        this.cdmta.set(cdmta);
    }

    @Column(name = "CD_LY")
    public BigDecimal getCdly() {
        return cdly.get();
    }

    public ObjectProperty<BigDecimal> cdlyProperty() {
        return cdly;
    }

    public void setCdly(BigDecimal cdly) {
        this.cdly.set(cdly);
    }

    @Column(name = "PV_RCM")
    public BigDecimal getPvrcm() {
        return pvrcm.get();
    }

    public ObjectProperty<BigDecimal> pvrcmProperty() {
        return pvrcm;
    }

    public void setPvrcm(BigDecimal pvrcm) {
        this.pvrcm.set(pvrcm);
    }

    @Column(name = "PV_RPM")
    @Formula("COALESCE(PV_RPM, 0)")
    public BigDecimal getPvrpm() {
        return pvrpm.get();
    }

    public ObjectProperty<BigDecimal> pvrpmProperty() {
        return pvrpm;
    }

    public void setPvrpm(BigDecimal pvrpm) {
        this.pvrpm.set(pvrpm);
    }

    @Column(name = "PV_MTA")
    @Formula("COALESCE(PV_MTA, 0)")
    public BigDecimal getPvmta() {
        return pvmta.get();
    }

    public ObjectProperty<BigDecimal> pvmtaProperty() {
        return pvmta;
    }

    public void setPvmta(BigDecimal pvmta) {
        this.pvmta.set(pvmta);
    }

    @Column(name = "PV_LY")
    public BigDecimal getPvly() {
        return pvly.get();
    }

    public ObjectProperty<BigDecimal> pvlyProperty() {
        return pvly;
    }

    public void setPvly(BigDecimal pvly) {
        this.pvly.set(pvly);
    }

    @Column(name = "KA_RCM")
    public BigDecimal getKarcm() {
        return karcm.get();
    }

    public ObjectProperty<BigDecimal> karcmProperty() {
        return karcm;
    }

    public void setKarcm(BigDecimal karcm) {
        this.karcm.set(karcm);
    }

    @Column(name = "KA_RPM")
    @Formula("COALESCE(KA_RPM, 0)")
    public BigDecimal getKarpm() {
        return karpm.get();
    }

    public ObjectProperty<BigDecimal> karpmProperty() {
        return karpm;
    }

    public void setKarpm(BigDecimal karpm) {
        this.karpm.set(karpm);
    }

    @Column(name = "KA_MTA")
    @Formula("COALESCE(KA_MTA, 0)")
    public BigDecimal getKamta() {
        return kamta.get();
    }

    public ObjectProperty<BigDecimal> kamtaProperty() {
        return kamta;
    }

    public void setKamta(BigDecimal kamta) {
        this.kamta.set(kamta);
    }

    @Column(name = "KA_LY")
    public BigDecimal getKaly() {
        return kaly.get();
    }

    public ObjectProperty<BigDecimal> kalyProperty() {
        return kaly;
    }

    public void setKaly(BigDecimal kaly) {
        this.kaly.set(kaly);
    }

    @Column(name = "PP_RCM")
    public BigDecimal getPprcm() {
        return pprcm.get();
    }

    public ObjectProperty<BigDecimal> pprcmProperty() {
        return pprcm;
    }

    public void setPprcm(BigDecimal pprcm) {
        this.pprcm.set(pprcm);
    }

    @Column(name = "PP_RPM")
    @Formula("COALESCE(PP_RPM, 0)")
    public BigDecimal getPprpm() {
        return pprpm.get();
    }

    public ObjectProperty<BigDecimal> pprpmProperty() {
        return pprpm;
    }

    public void setPprpm(BigDecimal pprpm) {
        this.pprpm.set(pprpm);
    }

    @Column(name = "PP_MTA")
    @Formula("COALESCE(PP_MTA, 0)")
    public BigDecimal getPpmta() {
        return ppmta.get();
    }

    public ObjectProperty<BigDecimal> ppmtaProperty() {
        return ppmta;
    }

    public void setPpmta(BigDecimal ppmta) {
        this.ppmta.set(ppmta);
    }

    @Column(name = "PP_LY")
    public BigDecimal getPply() {
        return pply.get();
    }

    public ObjectProperty<BigDecimal> pplyProperty() {
        return pply;
    }

    public void setPply(BigDecimal pply) {
        this.pply.set(pply);
    }

    @Column(name = "VP_RCM")
    public BigDecimal getVprcm() {
        return vprcm.get();
    }

    public ObjectProperty<BigDecimal> vprcmProperty() {
        return vprcm;
    }

    public void setVprcm(BigDecimal vprcm) {
        this.vprcm.set(vprcm);
    }

    @Column(name = "VP_RPM")
    @Formula("COALESCE(VP_RPM, 0)")
    public BigDecimal getVprpm() {
        return vprpm.get();
    }

    public ObjectProperty<BigDecimal> vprpmProperty() {
        return vprpm;
    }

    public void setVprpm(BigDecimal vprpm) {
        this.vprpm.set(vprpm);
    }

    @Column(name = "VP_MTA")
    @Formula("COALESCE(VP_MTA, 0)")
    public BigDecimal getVpmta() {
        return vpmta.get();
    }

    public ObjectProperty<BigDecimal> vpmtaProperty() {
        return vpmta;
    }

    public void setVpmta(BigDecimal vpmta) {
        this.vpmta.set(vpmta);
    }

    @Column(name = "VP_LY")
    public BigDecimal getVply() {
        return vply.get();
    }

    public ObjectProperty<BigDecimal> vplyProperty() {
        return vply;
    }

    public void setVply(BigDecimal vply) {
        this.vply.set(vply);
    }

    @Column(name = "TP_RCM")
    public BigDecimal getTprcm() {
        return tprcm.get();
    }

    public ObjectProperty<BigDecimal> tprcmProperty() {
        return tprcm;
    }

    public void setTprcm(BigDecimal tprcm) {
        this.tprcm.set(tprcm);
    }

    @Column(name = "TP_RPM")
    @Formula("COALESCE(TP_RPM, 0)")
    public BigDecimal getTprpm() {
        return tprpm.get();
    }

    public ObjectProperty<BigDecimal> tprpmProperty() {
        return tprpm;
    }

    public void setTprpm(BigDecimal tprpm) {
        this.tprpm.set(tprpm);
    }

    @Column(name = "TP_MTA")
    @Formula("COALESCE(TP_MTA, 0)")
    public BigDecimal getTpmta() {
        return tpmta.get();
    }

    public ObjectProperty<BigDecimal> tpmtaProperty() {
        return tpmta;
    }

    public void setTpmta(BigDecimal tpmta) {
        this.tpmta.set(tpmta);
    }

    @Column(name = "TP_LY")
    public BigDecimal getTply() {
        return tply.get();
    }

    public ObjectProperty<BigDecimal> tplyProperty() {
        return tply;
    }

    public void setTply(BigDecimal tply) {
        this.tply.set(tply);
    }

    @Column(name = "TF_RCM")
    public BigDecimal getTfrcm() {
        return tfrcm.get();
    }

    public ObjectProperty<BigDecimal> tfrcmProperty() {
        return tfrcm;
    }

    public void setTfrcm(BigDecimal tfrcm) {
        this.tfrcm.set(tfrcm);
    }

    @Column(name = "TF_RPM")
    @Formula("COALESCE(TF_RPM, 0)")
    public BigDecimal getTfrpm() {
        return tfrpm.get();
    }

    public ObjectProperty<BigDecimal> tfrpmProperty() {
        return tfrpm;
    }

    public void setTfrpm(BigDecimal tfrpm) {
        this.tfrpm.set(tfrpm);
    }

    @Column(name = "TF_MTA")
    @Formula("COALESCE(TF_MTA, 0)")
    public BigDecimal getTfmta() {
        return tfmta.get();
    }

    public ObjectProperty<BigDecimal> tfmtaProperty() {
        return tfmta;
    }

    public void setTfmta(BigDecimal tfmta) {
        this.tfmta.set(tfmta);
    }

    @Column(name = "TF_LY")
    public BigDecimal getTfly() {
        return tfly.get();
    }

    public ObjectProperty<BigDecimal> tflyProperty() {
        return tfly;
    }

    public void setTfly(BigDecimal tfly) {
        this.tfly.set(tfly);
    }

    @Column(name = "PG_RCM")
    public BigDecimal getPgrcm() {
        return pgrcm.get();
    }

    public ObjectProperty<BigDecimal> pgrcmProperty() {
        return pgrcm;
    }

    public void setPgrcm(BigDecimal pgrcm) {
        this.pgrcm.set(pgrcm);
    }

    @Column(name = "PG_RPM")
    @Formula("COALESCE(PG_RPM, 0)")
    public BigDecimal getPgrpm() {
        return pgrpm.get();
    }

    public ObjectProperty<BigDecimal> pgrpmProperty() {
        return pgrpm;
    }

    public void setPgrpm(BigDecimal pgrpm) {
        this.pgrpm.set(pgrpm);
    }

    @Column(name = "PG_MTA")
    @Formula("COALESCE(PG_MTA, 0)")
    public BigDecimal getPgmta() {
        return pgmta.get();
    }

    public ObjectProperty<BigDecimal> pgmtaProperty() {
        return pgmta;
    }

    public void setPgmta(BigDecimal pgmta) {
        this.pgmta.set(pgmta);
    }

    @Column(name = "PG_LY")
    public BigDecimal getPgly() {
        return pgly.get();
    }

    public ObjectProperty<BigDecimal> pglyProperty() {
        return pgly;
    }

    public void setPgly(BigDecimal pgly) {
        this.pgly.set(pgly);
    }

    @Column(name = "PR_RCM")
    @Formula("COALESCE(PR_RCM, 0) / 100")
    public BigDecimal getPrrcm() {
        return prrcm.get();
    }

    public ObjectProperty<BigDecimal> prrcmProperty() {
        return prrcm;
    }

    public void setPrrcm(BigDecimal prrcm) {
        this.prrcm.set(prrcm);
    }

    @Column(name = "PR_RPM")
    @Formula("COALESCE(PR_RPM, 0)")
    public BigDecimal getPrrpm() {
        return prrpm.get();
    }

    public ObjectProperty<BigDecimal> prrpmProperty() {
        return prrpm;
    }

    public void setPrrpm(BigDecimal prrpm) {
        this.prrpm.set(prrpm);
    }

    @Column(name = "PR_MTA")
    @Formula("COALESCE(PR_MTA, 0)")
    public BigDecimal getPrmta() {
        return prmta.get();
    }

    public ObjectProperty<BigDecimal> prmtaProperty() {
        return prmta;
    }

    public void setPrmta(BigDecimal prmta) {
        this.prmta.set(prmta);
    }

    @Column(name = "PR_LY")
    @Formula("COALESCE(PR_LY, 0) / 100")
    public BigDecimal getPrly() {
        return prly.get();
    }

    public ObjectProperty<BigDecimal> prlyProperty() {
        return prly;
    }

    public void setPrly(BigDecimal prly) {
        this.prly.set(prly);
    }

    @Column(name = "CLN_RCM")
    public BigDecimal getClnrcm() {
        return clnrcm.get();
    }

    public ObjectProperty<BigDecimal> clnrcmProperty() {
        return clnrcm;
    }

    public void setClnrcm(BigDecimal clnrcm) {
        this.clnrcm.set(clnrcm);
    }

    @Column(name = "CLN_MTA")
    @Formula("COALESCE(CLN_MTA, 0)")
    public BigDecimal getClnmta() {
        return clnmta.get();
    }

    public ObjectProperty<BigDecimal> clnmtaProperty() {
        return clnmta;
    }

    public void setClnmta(BigDecimal clnmta) {
        this.clnmta.set(clnmta);
    }

    @Column(name = "CLN_LY")
    public BigDecimal getClnly() {
        return clnly.get();
    }

    public ObjectProperty<BigDecimal> clnlyProperty() {
        return clnly;
    }

    public void setClnly(BigDecimal clnly) {
        this.clnly.set(clnly);
    }

    @Column(name = "RAT_RCM")
    public BigDecimal getRatrcm() {
        return ratrcm.get();
    }

    public ObjectProperty<BigDecimal> ratrcmProperty() {
        return ratrcm;
    }

    public void setRatrcm(BigDecimal ratrcm) {
        this.ratrcm.set(ratrcm);
    }

    @OneToMany(mappedBy = "id.bid")
    public List<SdBil> getBil() {
        return bil;
    }

    public void setBil(List<SdBil> bil) {
        this.bil = bil;
    }

    @OneToMany(mappedBy = "id.bid")
    public List<VSdBidCidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<VSdBidCidade> cidades) {
        this.cidades = cidades;
    }

    // </editor-fold
    // <editor-fold defaultstate="collapsed" desc="transient">
    @Transient
    public BigDecimal getPmRelMta() {
        return pmRelMta.get();
    }

    public ObjectProperty<BigDecimal> pmRelMtaProperty() {
        return pmRelMta;
    }

    public void setPmRelMta(BigDecimal pmRelMta) {
        this.pmRelMta.set(pmRelMta);
    }

    @Transient
    public BigDecimal getPmRelLy() {
        return pmRelLy.get();
    }

    public ObjectProperty<BigDecimal> pmRelLyProperty() {
        return pmRelLy;
    }

    public void setPmRelLy(BigDecimal pmRelLy) {
        this.pmRelLy.set(pmRelLy);
    }

    @Transient
    public BigDecimal getCdRelMta() {
        return cdRelMta.get();
    }

    public ObjectProperty<BigDecimal> cdRelMtaProperty() {
        return cdRelMta;
    }

    public void setCdRelMta(BigDecimal cdRelMta) {
        this.cdRelMta.set(cdRelMta);
    }

    @Transient
    public BigDecimal getCdRelLy() {
        return cdRelLy.get();
    }

    public ObjectProperty<BigDecimal> cdRelLyProperty() {
        return cdRelLy;
    }

    public void setCdRelLy(BigDecimal cdRelLy) {
        this.cdRelLy.set(cdRelLy);
    }

    @Transient
    public BigDecimal getPvRelMta() {
        return pvRelMta.get();
    }

    public ObjectProperty<BigDecimal> pvRelMtaProperty() {
        return pvRelMta;
    }

    public void setPvRelMta(BigDecimal pvRelMta) {
        this.pvRelMta.set(pvRelMta);
    }

    @Transient
    public BigDecimal getPvRelLy() {
        return pvRelLy.get();
    }

    public ObjectProperty<BigDecimal> pvRelLyProperty() {
        return pvRelLy;
    }

    public void setPvRelLy(BigDecimal pvRelLy) {
        this.pvRelLy.set(pvRelLy);
    }

    @Transient
    public BigDecimal getKaRelMta() {
        return kaRelMta.get();
    }

    public ObjectProperty<BigDecimal> kaRelMtaProperty() {
        return kaRelMta;
    }

    public void setKaRelMta(BigDecimal kaRelMta) {
        this.kaRelMta.set(kaRelMta);
    }

    @Transient
    public BigDecimal getKaRelLy() {
        return kaRelLy.get();
    }

    public ObjectProperty<BigDecimal> kaRelLyProperty() {
        return kaRelLy;
    }

    public void setKaRelLy(BigDecimal kaRelLy) {
        this.kaRelLy.set(kaRelLy);
    }

    @Transient
    public BigDecimal getPpRelMta() {
        return ppRelMta.get();
    }

    public ObjectProperty<BigDecimal> ppRelMtaProperty() {
        return ppRelMta;
    }

    public void setPpRelMta(BigDecimal ppRelMta) {
        this.ppRelMta.set(ppRelMta);
    }

    @Transient
    public BigDecimal getPpRelLy() {
        return ppRelLy.get();
    }

    public ObjectProperty<BigDecimal> ppRelLyProperty() {
        return ppRelLy;
    }

    public void setPpRelLy(BigDecimal ppRelLy) {
        this.ppRelLy.set(ppRelLy);
    }

    @Transient
    public BigDecimal getVpRelMta() {
        return vpRelMta.get();
    }

    public ObjectProperty<BigDecimal> vpRelMtaProperty() {
        return vpRelMta;
    }

    public void setVpRelMta(BigDecimal vpRelMta) {
        this.vpRelMta.set(vpRelMta);
    }

    @Transient
    public BigDecimal getVpRelLy() {
        return vpRelLy.get();
    }

    public ObjectProperty<BigDecimal> vpRelLyProperty() {
        return vpRelLy;
    }

    public void setVpRelLy(BigDecimal vpRelLy) {
        this.vpRelLy.set(vpRelLy);
    }

    @Transient
    public BigDecimal getTpRelMta() {
        return tpRelMta.get();
    }

    public ObjectProperty<BigDecimal> tpRelMtaProperty() {
        return tpRelMta;
    }

    public void setTpRelMta(BigDecimal tpRelMta) {
        this.tpRelMta.set(tpRelMta);
    }

    @Transient
    public BigDecimal getTpRelLy() {
        return tpRelLy.get();
    }

    public ObjectProperty<BigDecimal> tpRelLyProperty() {
        return tpRelLy;
    }

    public void setTpRelLy(BigDecimal tpRelLy) {
        this.tpRelLy.set(tpRelLy);
    }

    @Transient
    public BigDecimal getTfRelMta() {
        return tfRelMta.get();
    }

    public ObjectProperty<BigDecimal> tfRelMtaProperty() {
        return tfRelMta;
    }

    public void setTfRelMta(BigDecimal tfRelMta) {
        this.tfRelMta.set(tfRelMta);
    }

    @Transient
    public BigDecimal getTfRelLy() {
        return tfRelLy.get();
    }

    public ObjectProperty<BigDecimal> tfRelLyProperty() {
        return tfRelLy;
    }

    public void setTfRelLy(BigDecimal tfRelLy) {
        this.tfRelLy.set(tfRelLy);
    }

    @Transient
    public BigDecimal getPgRelMta() {
        return pgRelMta.get();
    }

    public ObjectProperty<BigDecimal> pgRelMtaProperty() {
        return pgRelMta;
    }

    public void setPgRelMta(BigDecimal pgRelMta) {
        this.pgRelMta.set(pgRelMta);
    }

    @Transient
    public BigDecimal getPgRelLy() {
        return pgRelLy.get();
    }

    public ObjectProperty<BigDecimal> pgRelLyProperty() {
        return pgRelLy;
    }

    public void setPgRelLy(BigDecimal pgRelLy) {
        this.pgRelLy.set(pgRelLy);
    }

    @Transient
    public BigDecimal getPrRelMta() {
        return prRelMta.get();
    }

    public ObjectProperty<BigDecimal> prRelMtaProperty() {
        return prRelMta;
    }

    public void setPrRelMta(BigDecimal prRelMta) {
        this.prRelMta.set(prRelMta);
    }

    @Transient
    public BigDecimal getPrRelLy() {
        return prRelLy.get();
    }

    public ObjectProperty<BigDecimal> prRelLyProperty() {
        return prRelLy;
    }

    public void setPrRelLy(BigDecimal prRelLy) {
        this.prRelLy.set(prRelLy);
    }

    @Transient
    public BigDecimal getClnRelMta() {
        return clnRelMta.get();
    }

    public ObjectProperty<BigDecimal> clnRelMtaProperty() {
        return clnRelMta;
    }

    public void setClnRelMta(BigDecimal clnRelMta) {
        this.clnRelMta.set(clnRelMta);
    }

    @Transient
    public BigDecimal getClnRelLy() {
        return clnRelLy.get();
    }

    public ObjectProperty<BigDecimal> clnRelLyProperty() {
        return clnRelLy;
    }

    public void setClnRelLy(BigDecimal clnRelLy) {
        this.clnRelLy.set(clnRelLy);
    }
    // </editor-fold>

    @PostLoad
    private void postLoad() {
        // relação real com meta
        setPmRelMta(getPmmta() == null || getPmmta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPmrcm().divide(getPmmta(), 4, RoundingMode.HALF_UP));
        setCdRelMta(getCdmta() == null || getCdmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getCdrcm().divide(getCdmta(),4, RoundingMode.HALF_UP));
        setPvRelMta(getPvmta() == null || getPvmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getPvrcm().divide(getPvmta(),4, RoundingMode.HALF_UP));
        setKaRelMta(getKamta() == null || getKamta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getKarcm().divide(getKamta(),4, RoundingMode.HALF_UP));
        setPpRelMta(getPpmta() == null || getPpmta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPprcm().divide(getPpmta(),4, RoundingMode.HALF_UP));
        setVpRelMta(getVpmta() == null || getVpmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getVprcm().divide(getVpmta(),4, RoundingMode.HALF_UP));
        setTpRelMta(getTpmta() == null || getTpmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getTprcm().divide(getTpmta(),4, RoundingMode.HALF_UP));
        setTfRelMta(getTfmta() == null || getTfmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getTfrcm().divide(getTfmta(),4, RoundingMode.HALF_UP));
        setPgRelMta(getPgmta() == null || getPgmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getPgrcm().divide(getPgmta(),4, RoundingMode.HALF_UP));
        setPrRelMta(getPrmta() == null || getPrmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getPrrcm().divide(getPrmta(),4, RoundingMode.HALF_UP));
        setClnRelMta(getClnmta() == null || getClnmta().compareTo(BigDecimal.ZERO) == 0 ?  BigDecimal.ONE : getClnrcm().divide(getClnmta(),4, RoundingMode.HALF_UP));
        // relação real com ly
        setPmRelLy(getPmly() == null || getPmly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPmrcm().divide(getPmly(),4, RoundingMode.HALF_UP));
        setCdRelLy(getCdly() == null || getCdly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getCdrcm().divide(getCdly(),4, RoundingMode.HALF_UP));
        setPvRelLy(getPvly() == null || getPvly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPvrcm().divide(getPvly(),4, RoundingMode.HALF_UP));
        setKaRelLy(getKaly() == null || getKaly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getKarcm().divide(getKaly(),4, RoundingMode.HALF_UP));
        setPpRelLy(getPply() == null || getPply().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPprcm().divide(getPply(),4, RoundingMode.HALF_UP));
        setVpRelLy(getVply() == null || getVply().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getVprcm().divide(getVply(),4, RoundingMode.HALF_UP));
        setTpRelLy(getTply() == null || getTply().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTprcm().divide(getTply(),4, RoundingMode.HALF_UP));
        setTfRelLy(getTfly() == null || getTfly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTfrcm().divide(getTfly(),4, RoundingMode.HALF_UP));
        setPgRelLy(getPgly() == null || getPgly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPgrcm().divide(getPgly(),4, RoundingMode.HALF_UP));
        setPrRelLy(getPrly() == null || getPrly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPrrcm().divide(getPrly(),4, RoundingMode.HALF_UP));
        setClnRelLy(getClnly() == null || getClnly().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getClnrcm().divide(getClnly(),4, RoundingMode.HALF_UP));
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}
