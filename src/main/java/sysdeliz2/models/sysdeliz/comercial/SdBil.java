package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import org.hibernate.annotations.Formula;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
@Table(name = "sd_bil_001")
public class SdBil implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<SdBid> bid = new SimpleObjectProperty<>();
        private final ObjectProperty<Linha> linha = new SimpleObjectProperty<>();

        public Pk() {
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "codrep", referencedColumnName = "codrep"),
                @JoinColumn(name = "colecao", referencedColumnName = "codcol"),
                @JoinColumn(name = "marca", referencedColumnName = "codmar")
        })
        public SdBid getBid() {
            return bid.get();
        }

        public ObjectProperty<SdBid> bidProperty() {
            return bid;
        }

        public void setBid(SdBid bid) {
            this.bid.set(bid);
        }

        @OneToOne
        @JoinColumn(name = "linha")
        public Linha getLinha() {
            return linha.get();
        }

        public ObjectProperty<Linha> linhaProperty() {
            return linha;
        }

        public void setLinha(Linha linha) {
            this.linha.set(linha);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final IntegerProperty seqLinha = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> mostruario = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> referencias = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pmRcm = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pmMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pmLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pmRelMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pmRelLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tpRcm = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tpMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tpLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tpRelMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tpRelLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tfRcm = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tfMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tfLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tfRelMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tfRelLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pgRcm = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pgMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pgLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pgRelMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pgRelLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prRcm = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prRelMta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prRelLy = new SimpleObjectProperty<>();

    public SdBil() {
    }

    // <editor-fold defaultstate="collapsed" desc="column">
    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "seq_linha")
    public int getSeqLinha() {
        return seqLinha.get();
    }

    public IntegerProperty seqLinhaProperty() {
        return seqLinha;
    }

    public void setSeqLinha(int seqLinha) {
        this.seqLinha.set(seqLinha);
    }

    @Column(name = "mostruario")
    public BigDecimal getMostruario() {
        return mostruario.get();
    }

    public ObjectProperty<BigDecimal> mostruarioProperty() {
        return mostruario;
    }

    public void setMostruario(BigDecimal mostruario) {
        this.mostruario.set(mostruario);
    }

    @Column(name = "referencias")
    public BigDecimal getReferencias() {
        return referencias.get();
    }

    public ObjectProperty<BigDecimal> referenciasProperty() {
        return referencias;
    }

    public void setReferencias(BigDecimal referencias) {
        this.referencias.set(referencias);
    }

    @Column(name = "pm_rcm")
    public BigDecimal getPmRcm() {
        return pmRcm.get();
    }

    public ObjectProperty<BigDecimal> pmRcmProperty() {
        return pmRcm;
    }

    public void setPmRcm(BigDecimal pmRcm) {
        this.pmRcm.set(pmRcm);
    }

    @Column(name = "pm_mta")
    @Formula("COALESCE(pm_mta, 0)")
    public BigDecimal getPmMta() {
        return pmMta.get();
    }

    public ObjectProperty<BigDecimal> pmMtaProperty() {
        return pmMta;
    }

    public void setPmMta(BigDecimal pmMta) {
        this.pmMta.set(pmMta);
    }

    @Column(name = "pm_ly")
    @Formula("COALESCE(pm_ly, 0)")
    public BigDecimal getPmLy() {
        return pmLy.get();
    }

    public ObjectProperty<BigDecimal> pmLyProperty() {
        return pmLy;
    }

    public void setPmLy(BigDecimal pmLy) {
        this.pmLy.set(pmLy);
    }

    @Column(name = "tp_rcm")
    public BigDecimal getTpRcm() {
        return tpRcm.get();
    }

    public ObjectProperty<BigDecimal> tpRcmProperty() {
        return tpRcm;
    }

    public void setTpRcm(BigDecimal tpRcm) {
        this.tpRcm.set(tpRcm);
    }

    @Column(name = "tp_mta")
    @Formula("COALESCE(tp_mta, 0)")
    public BigDecimal getTpMta() {
        return tpMta.get();
    }

    public ObjectProperty<BigDecimal> tpMtaProperty() {
        return tpMta;
    }

    public void setTpMta(BigDecimal tpMta) {
        this.tpMta.set(tpMta);
    }

    @Column(name = "tp_ly")
    @Formula("COALESCE(tp_ly, 0)")
    public BigDecimal getTpLy() {
        return tpLy.get();
    }

    public ObjectProperty<BigDecimal> tpLyProperty() {
        return tpLy;
    }

    public void setTpLy(BigDecimal tpLy) {
        this.tpLy.set(tpLy);
    }

    @Column(name = "tf_rcm")
    public BigDecimal getTfRcm() {
        return tfRcm.get();
    }

    public ObjectProperty<BigDecimal> tfRcmProperty() {
        return tfRcm;
    }

    public void setTfRcm(BigDecimal tfRcm) {
        this.tfRcm.set(tfRcm);
    }

    @Column(name = "tf_mta")
    @Formula("COALESCE(tf_mta, 0)")
    public BigDecimal getTfMta() {
        return tfMta.get();
    }

    public ObjectProperty<BigDecimal> tfMtaProperty() {
        return tfMta;
    }

    public void setTfMta(BigDecimal tfMta) {
        this.tfMta.set(tfMta);
    }

    @Column(name = "tf_ly")
    @Formula("COALESCE(tf_ly, 0)")
    public BigDecimal getTfLy() {
        return tfLy.get();
    }

    public ObjectProperty<BigDecimal> tfLyProperty() {
        return tfLy;
    }

    public void setTfLy(BigDecimal tfLy) {
        this.tfLy.set(tfLy);
    }

    @Column(name = "pg_rcm")
    public BigDecimal getPgRcm() {
        return pgRcm.get();
    }

    public ObjectProperty<BigDecimal> pgRcmProperty() {
        return pgRcm;
    }

    public void setPgRcm(BigDecimal pgRcm) {
        this.pgRcm.set(pgRcm);
    }

    @Column(name = "pg_mta")
    @Formula("COALESCE(pg_mta, 0)")
    public BigDecimal getPgMta() {
        return pgMta.get();
    }

    public ObjectProperty<BigDecimal> pgMtaProperty() {
        return pgMta;
    }

    public void setPgMta(BigDecimal pgMta) {
        this.pgMta.set(pgMta);
    }

    @Column(name = "pg_ly")
    @Formula("COALESCE(pg_ly, 0)")
    public BigDecimal getPgLy() {
        return pgLy.get();
    }

    public ObjectProperty<BigDecimal> pgLyProperty() {
        return pgLy;
    }

    public void setPgLy(BigDecimal pgLy) {
        this.pgLy.set(pgLy);
    }

    @Column(name = "pr_rcm")
    public BigDecimal getPrRcm() {
        return prRcm.get();
    }

    public ObjectProperty<BigDecimal> prRcmProperty() {
        return prRcm;
    }

    public void setPrRcm(BigDecimal prRcm) {
        this.prRcm.set(prRcm);
    }

    @Column(name = "pr_mta")
    @Formula("COALESCE(pr_mta, 0)")
    public BigDecimal getPrMta() {
        return prMta.get();
    }

    public ObjectProperty<BigDecimal> prMtaProperty() {
        return prMta;
    }

    public void setPrMta(BigDecimal prMta) {
        this.prMta.set(prMta);
    }

    @Column(name = "pr_ly")
    @Formula("COALESCE(pr_ly, 0)")
    public BigDecimal getPrLy() {
        return prLy.get();
    }

    public ObjectProperty<BigDecimal> prLyProperty() {
        return prLy;
    }

    public void setPrLy(BigDecimal prLy) {
        this.prLy.set(prLy);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transient">
    @Transient
    public BigDecimal getPmRelMta() {
        return pmRelMta.get();
    }

    public ObjectProperty<BigDecimal> pmRelMtaProperty() {
        return pmRelMta;
    }

    public void setPmRelMta(BigDecimal pmRelMta) {
        this.pmRelMta.set(pmRelMta);
    }

    @Transient
    public BigDecimal getPmRelLy() {
        return pmRelLy.get();
    }

    public ObjectProperty<BigDecimal> pmRelLyProperty() {
        return pmRelLy;
    }

    public void setPmRelLy(BigDecimal pmRelLy) {
        this.pmRelLy.set(pmRelLy);
    }

    @Transient
    public BigDecimal getTpRelMta() {
        return tpRelMta.get();
    }

    public ObjectProperty<BigDecimal> tpRelMtaProperty() {
        return tpRelMta;
    }

    public void setTpRelMta(BigDecimal tpRelMta) {
        this.tpRelMta.set(tpRelMta);
    }

    @Transient
    public BigDecimal getTpRelLy() {
        return tpRelLy.get();
    }

    public ObjectProperty<BigDecimal> tpRelLyProperty() {
        return tpRelLy;
    }

    public void setTpRelLy(BigDecimal tpRelLy) {
        this.tpRelLy.set(tpRelLy);
    }

    @Transient
    public BigDecimal getTfRelMta() {
        return tfRelMta.get();
    }

    public ObjectProperty<BigDecimal> tfRelMtaProperty() {
        return tfRelMta;
    }

    public void setTfRelMta(BigDecimal tfRelMta) {
        this.tfRelMta.set(tfRelMta);
    }

    @Transient
    public BigDecimal getTfRelLy() {
        return tfRelLy.get();
    }

    public ObjectProperty<BigDecimal> tfRelLyProperty() {
        return tfRelLy;
    }

    public void setTfRelLy(BigDecimal tfRelLy) {
        this.tfRelLy.set(tfRelLy);
    }

    @Transient
    public BigDecimal getPgRelMta() {
        return pgRelMta.get();
    }

    public ObjectProperty<BigDecimal> pgRelMtaProperty() {
        return pgRelMta;
    }

    public void setPgRelMta(BigDecimal pgRelMta) {
        this.pgRelMta.set(pgRelMta);
    }

    @Transient
    public BigDecimal getPgRelLy() {
        return pgRelLy.get();
    }

    public ObjectProperty<BigDecimal> pgRelLyProperty() {
        return pgRelLy;
    }

    public void setPgRelLy(BigDecimal pgRelLy) {
        this.pgRelLy.set(pgRelLy);
    }

    @Transient
    public BigDecimal getPrRelMta() {
        return prRelMta.get();
    }

    public ObjectProperty<BigDecimal> prRelMtaProperty() {
        return prRelMta;
    }

    public void setPrRelMta(BigDecimal prRelMta) {
        this.prRelMta.set(prRelMta);
    }

    @Transient
    public BigDecimal getPrRelLy() {
        return prRelLy.get();
    }

    public ObjectProperty<BigDecimal> prRelLyProperty() {
        return prRelLy;
    }

    public void setPrRelLy(BigDecimal prRelLy) {
        this.prRelLy.set(prRelLy);
    }
    // </editor-fold>

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

    @PostLoad
    private void postLoad() {
        // mta
        setPmRelMta(getPmMta() == null || getPmMta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPmRcm().divide(getPmMta(), 4, RoundingMode.HALF_UP));
        setTpRelMta(getTpMta() == null || getTpMta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTpRcm().divide(getTpMta(), 4, RoundingMode.HALF_UP));
        setTfRelMta(getTfMta() == null || getTfMta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTfRcm().divide(getTfMta(), 4, RoundingMode.HALF_UP));
        setPgRelMta(getPgMta() == null || getPgMta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPgRcm().divide(getPgMta(), 4, RoundingMode.HALF_UP));
        setPrRelMta(getPrMta() == null || getPrMta().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPrRcm().divide(getPrMta(), 4, RoundingMode.HALF_UP));
        // ly
        setPmRelLy(getPmLy() == null || getPmLy().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPmRcm().divide(getPmLy(), 4, RoundingMode.HALF_UP));
        setTpRelLy(getTpLy() == null || getTpLy().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTpRcm().divide(getTpLy(), 4, RoundingMode.HALF_UP));
        setTfRelLy(getTfLy() == null || getTfLy().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getTfRcm().divide(getTfLy(), 4, RoundingMode.HALF_UP));
        setPgRelLy(getPgLy() == null || getPgLy().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPgRcm().divide(getPgLy(), 4, RoundingMode.HALF_UP));
        setPrRelLy(getPrLy() == null || getPrLy().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : getPrRcm().divide(getPrLy(), 4, RoundingMode.HALF_UP));
    }
}