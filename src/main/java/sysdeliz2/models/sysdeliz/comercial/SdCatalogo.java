package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.*;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_CATALOGO_001")
@TelaSysDeliz(descricao = "Catálogos", icon = "catalogo_50.png")
public class SdCatalogo extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    @Transient private final IntegerProperty codigo = new SimpleIntegerProperty(0);
    @ExibeTableView(descricao = "Nome", width = 250)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    @Transient private final StringProperty nome = new SimpleStringProperty();
    @Transient private final StringProperty jsonMarca = new SimpleStringProperty();
    @ExibeTableView(descricao = "Marca", width = 180)
    @ColunaFilter(descricao = "Marca", coluna = "marca.codigo", filterClass = "sysdeliz2.models.sysdeliz.Marca")
    @Transient private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>(null);
    @ExibeTableView(descricao = "Coleção", width = 200)
    @ColunaFilter(descricao = "Coleção", coluna = "colecao.codigo", filterClass = "sysdeliz2.models.sysdeliz.Colecao")
    @Transient private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>(null);
    @Transient private final StringProperty descricao = new SimpleStringProperty();
    @Transient private final StringProperty imagem = new SimpleStringProperty();
    @Transient private final StringProperty imagemLocal = new SimpleStringProperty();
    @Transient private final StringProperty video = new SimpleStringProperty();
    @Transient private final BooleanProperty destaque = new SimpleBooleanProperty(false);
    @Transient private final StringProperty ano = new SimpleStringProperty();
    @Transient private final ObjectProperty<LocalDate> novidadeDe = new SimpleObjectProperty<>();
    @Transient private final ObjectProperty<LocalDate> novidadeAte = new SimpleObjectProperty<>();
    @Transient private final StringProperty ordem = new SimpleStringProperty("1");
    @ExibeTableView(descricao = "Status", width = 40, columnType = ColumnType.BOOLEAN)
    @Transient private final BooleanProperty status = new SimpleBooleanProperty(true);
    @ExibeTableView(descricao = "Envio", width = 40, columnType = ColumnType.BOOLEAN)
    @Transient private final BooleanProperty statusEnvio = new SimpleBooleanProperty(false);
    @Transient private List<SdLookbookCatalogo> lookbooks = new ArrayList<>();
    @Transient private List<SdConceitoCatalogo> conceitos = new ArrayList<>();

    public SdCatalogo() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CODIGO_SD_CATALOGO")
    @SequenceGenerator(name = "SEQ_CODIGO_SD_CATALOGO", sequenceName = "SEQ_CODIGO_SD_CATALOGO", allocationSize = 1)
    @Column(name = "CODIGO")
    @JsonProperty("cat_codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
        super.setCodigoFilter(String.valueOf(codigo));
    }

    @Column(name = "NOME")
    @JsonProperty("cat_nome")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
        super.setDescricaoFilter(nome);
    }

    @Transient
    @JsonProperty("cat_marca")
    public String getJsonMarca() {
        return jsonMarca.get();
    }

    public StringProperty jsonMarcaProperty() {
        return jsonMarca;
    }

    public void setJsonMarca(String jsonMarca) {
        this.jsonMarca.set(jsonMarca);
    }

    @OneToOne
    @JoinColumn(name = "MARCA")
    @JsonIgnore
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @OneToOne
    @JoinColumn(name = "COLECAO")
    @JsonIgnore
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "DESCRICAO")
    @Lob
    @JsonProperty("cat_descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "IMAGEM")
    @JsonProperty("cat_image")
    public String getImagem() {
        return imagem.get();
    }

    public StringProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "IMAGEM_LOCAL")
    @JsonIgnore
    public String getImagemLocal() {
        return imagemLocal.get();
    }

    public StringProperty imagemLocalProperty() {
        return imagemLocal;
    }

    public void setImagemLocal(String imagemLocal) {
        this.imagemLocal.set(imagemLocal);
    }

    @Column(name = "VIDEO")
    @JsonProperty("cat_video")
    public String getVideo() {
        return video.get();
    }

    public StringProperty videoProperty() {
        return video;
    }

    public void setVideo(String video) {
        this.video.set(video);
    }

    @Column(name = "DESTAQUE")
    @Convert(converter = BooleanAttributeZeroUmConverter.class)
    @JsonProperty("cat_destaque")
    @JsonSerialize(using = BooleanZeroEUmAttributeSerializer.class)
    @JsonDeserialize(using = BooleanZeroEUmAttributeDeserializer.class)
    public Boolean getDestaque() {
        return destaque.get();
    }

    public BooleanProperty destaqueProperty() {
        return destaque;
    }

    public void setDestaque(Boolean destaque) {
        this.destaque.set(destaque);
    }

    @Column(name = "ANO")
    @JsonProperty("cat_ano")
    public String getAno() {
        return ano.get();
    }

    public StringProperty anoProperty() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano.set(ano);
    }

    @Column(name = "NOVIDADE_DE")
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonProperty("cat_novidade_de")
    @JsonSerialize(using = LocalDateAttributeSerializer.class)
    @JsonDeserialize(using = LocalDateAttributeDeserializer.class)
    public LocalDate getNovidadeDe() {
        return novidadeDe.get();
    }

    public ObjectProperty<LocalDate> novidadeDeProperty() {
        return novidadeDe;
    }

    public void setNovidadeDe(LocalDate novidadeDe) {
        this.novidadeDe.set(novidadeDe);
    }

    @Column(name = "NOVIDADE_ATE")
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonProperty("cat_novidade_ate")
    @JsonSerialize(using = LocalDateAttributeSerializer.class)
    @JsonDeserialize(using = LocalDateAttributeDeserializer.class)
    public LocalDate getNovidadeAte() {
        return novidadeAte.get();
    }

    public ObjectProperty<LocalDate> novidadeAteProperty() {
        return novidadeAte;
    }

    public void setNovidadeAte(LocalDate novidadeAte) {
        this.novidadeAte.set(novidadeAte);
    }

    @Column(name = "ORDEM")
    @JsonProperty("cat_ordem")
    public String getOrdem() {
        return ordem.get();
    }

    public StringProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "STATUS")
    @Convert(converter = BooleanAttributeZeroUmConverter.class)
    @JsonProperty("cat_status")
    @JsonSerialize(using = BooleanZeroEUmAttributeSerializer.class)
    @JsonDeserialize(using = BooleanZeroEUmAttributeDeserializer.class)
    public Boolean getStatus() {
        return status.get();
    }

    public BooleanProperty statusProperty() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status.set(status);
    }

    @Column(name = "STATUS_ENVIO")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    public boolean isStatusEnvio() {
        return statusEnvio.get();
    }

    public BooleanProperty statusEnvioProperty() {
        return statusEnvio;
    }

    public void setStatusEnvio(boolean statusEnvio) {
        this.statusEnvio.set(statusEnvio);
    }

    @OneToMany(mappedBy = "catalogo", cascade = CascadeType.ALL)
    @JsonProperty("lookbook")
    public List<SdLookbookCatalogo> getLookbooks() {
        return lookbooks;
    }

    public void setLookbooks(List<SdLookbookCatalogo> lookbooks) {
        this.lookbooks = lookbooks;
    }

    @OneToMany(mappedBy = "catalogo", cascade = CascadeType.ALL)
    @JsonProperty("slides")
    public List<SdConceitoCatalogo> getConceitos() {
        return conceitos;
    }

    public void setConceitos(List<SdConceitoCatalogo> conceitos) {
        this.conceitos = conceitos;
    }

    @PostLoad
    private void postLoad() {
        setJsonMarca(getMarca().getCodigo());
    }

    @PostPersist
    private void postPersist() {
        setJsonMarca(getMarca().getCodigo());
    }

    @Override
    public String toString() {
        return "["+ codigo.get() + "] " + nome.get();
    }
}