package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javafx.beans.property.*;
import sysdeliz2.models.view.VSdLinhaCatalogo;
import sysdeliz2.utils.converters.BooleanAttributeZeroUmConverter;
import sysdeliz2.utils.converters.BooleanZeroEUmAttributeDeserializer;
import sysdeliz2.utils.converters.BooleanZeroEUmAttributeSerializer;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "SD_CONCEITO_CATALOGO_001")
@IdClass(SdConceitoCatalogoPK.class)
public class SdConceitoCatalogo implements Serializable {

    private final ObjectProperty<SdCatalogo> catalogo = new SimpleObjectProperty<>();
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty titulo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty imagem = new SimpleStringProperty();
    private final StringProperty imagemLocal = new SimpleStringProperty();
    private final StringProperty imagemMobile = new SimpleStringProperty();
    private final StringProperty imagemMobileLocal = new SimpleStringProperty();
    private final ObjectProperty<VSdLinhaCatalogo> linha = new SimpleObjectProperty<>();
    private final ObjectProperty<LinhaCatalogo> linhaJson = new SimpleObjectProperty<>();
    private final StringProperty ordem = new SimpleStringProperty("1");
    private final BooleanProperty status = new SimpleBooleanProperty(true);
    private List<SdProdutosConceito> produtos = new ArrayList<>();
    private List<SdProdutosConceito> pinsDesktop = new ArrayList<>();
    private List<SdProdutosConceito> pinsMobile = new ArrayList<>();

    public SdConceitoCatalogo() {
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "CATALOGO", referencedColumnName = "CODIGO")
    @JsonIgnore
    public SdCatalogo getCatalogo() {
        return catalogo.get();
    }

    public ObjectProperty<SdCatalogo> catalogoProperty() {
        return catalogo;
    }

    public void setCatalogo(SdCatalogo catalogo) {
        this.catalogo.set(catalogo);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CODIGO_SD_CONCEITO_CAT")
    @SequenceGenerator(name = "SEQ_CODIGO_SD_CONCEITO_CAT", sequenceName = "SEQ_CODIGO_SD_CONCEITO_CAT", allocationSize = 1)
    @Column(name = "CODIGO")
    @JsonProperty("ban_codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "TITULO")
    @JsonProperty("ban_titulo")
    public String getTitulo() {
        return titulo.get();
    }

    public StringProperty tituloProperty() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo.set(titulo);
    }

    @Column(name = "DESCRICAO")
    @Lob
    @JsonProperty("ban_descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "IMAGEM")
    @JsonProperty("ban_image")
    public String getImagem() {
        return imagem.get();
    }

    public StringProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "IMAGEM_LOCAL")
    @JsonIgnore
    public String getImagemLocal() {
        return imagemLocal.get();
    }

    public StringProperty imagemLocalProperty() {
        return imagemLocal;
    }

    public void setImagemLocal(String imagemLocal) {
        this.imagemLocal.set(imagemLocal);
    }

    @Column(name = "IMAGEM_MOBILE")
    @JsonProperty("ban_image_mobile")
    public String getImagemMobile() {
        return imagemMobile.get();
    }

    public StringProperty imagemMobileProperty() {
        return imagemMobile;
    }

    public void setImagemMobile(String imagemMobile) {
        this.imagemMobile.set(imagemMobile);
    }

    @Column(name = "IMAGEM_MOBILE_LOCAL")
    @JsonIgnore
    public String getImagemMobileLocal() {
        return imagemMobileLocal.get();
    }

    public StringProperty imagemMobileLocalProperty() {
        return imagemMobileLocal;
    }

    public void setImagemMobileLocal(String imagemMobileLocal) {
        this.imagemMobileLocal.set(imagemMobileLocal);
    }

    @OneToOne
    @JoinColumn(name = "LINHA")
    @JsonIgnore
    public VSdLinhaCatalogo getLinha() {
        return linha.get();
    }

    public ObjectProperty<VSdLinhaCatalogo> linhaProperty() {
        return linha;
    }

    public void setLinha(VSdLinhaCatalogo linha) {
        this.linha.set(linha);
    }

    @Transient
    @JsonProperty("ban_linha")
    public LinhaCatalogo getLinhaJson() {
        return linhaJson.get();
    }

    public ObjectProperty<LinhaCatalogo> linhaJsonProperty() {
        return linhaJson;
    }

    public void setLinhaJson(LinhaCatalogo linhaJson) {
        this.linhaJson.set(linhaJson);
    }

    @Column(name = "ORDEM")
    @JsonProperty("ban_ordem")
    public String getOrdem() {
        return ordem.get();
    }

    public StringProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "STATUS")
    @Convert(converter = BooleanAttributeZeroUmConverter.class)
    @JsonProperty("ban_status")
    @JsonSerialize(using = BooleanZeroEUmAttributeSerializer.class)
    @JsonDeserialize(using = BooleanZeroEUmAttributeDeserializer.class)
    public Boolean getStatus() {
        return status.get();
    }

    public BooleanProperty statusProperty() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status.set(status);
    }

    @OneToMany(mappedBy = "id.conceito", cascade = CascadeType.ALL)
    @JsonIgnore
    public List<SdProdutosConceito> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<SdProdutosConceito> produtos) {
        this.produtos = produtos;
    }

    @Transient
    @JsonProperty("pins_desktop")
    public List<SdProdutosConceito> getPinsDesktop() {
        return pinsDesktop;
    }

    public void setPinsDesktop(List<SdProdutosConceito> pinsDesktop) {
        this.pinsDesktop = pinsDesktop;
    }

    @Transient
    @JsonProperty("pins_mobile")
    public List<SdProdutosConceito> getPinsMobile() {
        return pinsMobile;
    }

    public void setPinsMobile(List<SdProdutosConceito> pinsMobile) {
        this.pinsMobile = pinsMobile;
    }

    @PostLoad
    public void postLoad() {
        if (getLinha() != null) {
            setLinhaJson(new LinhaCatalogo("LIN" + getCatalogo().getMarca().getCodigo() + getLinha().getCodigo(),
                    getLinha().getDescricao(),
                    "https://imagens.deliz.com.br/catalogo/"
                            .concat(getCatalogo().getColecao().getCodigo())
                            .concat("/")
                            .concat(getCatalogo().getMarca().getCodigo())
                            .concat("/")
                            .concat("Linha")
                            .concat("/")
                            .concat("LIN" + getCatalogo().getMarca().getCodigo() + getLinha().getCodigo())
                            .concat(".png")));
        }
        setPinsDesktop(getProdutos().stream().filter(produto -> produto.getId().getTipo().equals("D")).collect(Collectors.toList()));
        setPinsMobile(getProdutos().stream().filter(produto -> produto.getId().getTipo().equals("M")).collect(Collectors.toList()));
    }

    @PostPersist
    private void postPersist() {
        postLoad();
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

    private static class LinhaCatalogo {

        public String code;
        public String descricao;
        public String imagem;

        public LinhaCatalogo() {

        }

        public LinhaCatalogo(String code, String descricao, String imagem) {
            this.code = code;
            this.descricao = descricao;
            this.imagem = imagem;
        }

        @Override
        public String toString() {
            return "[" + code + "] " + descricao;
        }
    }
}