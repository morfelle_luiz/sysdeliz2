package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_IMAGENS_LOOKBOOK_001")
public class SdImagensLookbook implements Serializable {

    private final ObjectProperty<SdImagensLookbookPK> id = new SimpleObjectProperty<>();
    private final StringProperty ordem = new SimpleStringProperty();
    private final StringProperty imagem = new SimpleStringProperty();
    private final StringProperty imagemLocal = new SimpleStringProperty();

    public SdImagensLookbook() {
    }

    @EmbeddedId
    @JsonIgnore
    public SdImagensLookbookPK getId() {
        return id.get();
    }

    public ObjectProperty<SdImagensLookbookPK> idProperty() {
        return id;
    }

    public void setId(SdImagensLookbookPK id) {
        this.id.set(id);
    }

    @Transient
    @JsonProperty("ordem")
    public String getOrdem() {
        return ordem.get();
    }

    public StringProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "IMAGEM")
    @JsonProperty("imagem")
    public String getImagem() {
        return imagem.get();
    }

    public StringProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "IMAGEM_LOCAL")
    @JsonIgnore
    public String getImagemLocal() {
        return imagemLocal.get();
    }

    public StringProperty imagemLocalProperty() {
        return imagemLocal;
    }

    public void setImagemLocal(String imagemLocal) {
        this.imagemLocal.set(imagemLocal);
    }

    @PostLoad
    public void postLoad() {
        setOrdem(getId().getOrdem());
    }

    @PostPersist
    private void postPersist() {
        setOrdem(getId().getOrdem());
    }
}