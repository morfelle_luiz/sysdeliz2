package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdImagensLookbookPK implements Serializable {

    private final StringProperty ordem = new SimpleStringProperty();
    private final ObjectProperty<SdLookbookCatalogo> lookbook = new SimpleObjectProperty<>();

    public SdImagensLookbookPK() {
    }

    @Column(name = "ORDEM")
    public String getOrdem() {
        return ordem.get();
    }

    public StringProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem.set(ordem);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "CATALOGO", referencedColumnName = "CATALOGO"),
            @JoinColumn(name = "LOOKBOOK", referencedColumnName = "CODIGO")
    })
    public SdLookbookCatalogo getLookbook() {
        return lookbook.get();
    }

    public ObjectProperty<SdLookbookCatalogo> lookbookProperty() {
        return lookbook;
    }

    public void setLookbook(SdLookbookCatalogo lookbook) {
        this.lookbook.set(lookbook);
    }

}