package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sd_indicador_comercial_001")
@TelaSysDeliz(descricao = "Indicador Comercial", icon = "regras comerciais_100.png")
public class SdIndicadorComercial extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Sigla", width = 80)
    @ColunaFilter(descricao = "Sigla", coluna = "sigla")
    private final StringProperty sigla = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty acao = new SimpleStringProperty();
    @Transient
    private final BooleanProperty bid = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    @Transient
    private final ObjectProperty<SdGrupoIndicadorCom> grupo = new SimpleObjectProperty<>();
    @Transient
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public SdIndicadorComercial() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_INDICADOR_COMERCIAL")
    @SequenceGenerator(name = "SEQ_SD_INDICADOR_COMERCIAL", sequenceName = "SEQ_SD_INDICADOR_COMERCIAL", allocationSize = 1)
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(String.valueOf(codigo));
    }

    @Column(name = "sigla")
    public String getSigla() {
        return sigla.get();
    }

    public StringProperty siglaProperty() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla.set(sigla);
    }

    @Column(name = "descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Column(name = "acao")
    @Lob
    public String getAcao() {
        return acao.get();
    }

    public StringProperty acaoProperty() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao.set(acao);
    }

    @Column(name = "bid")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isBid() {
        return bid.get();
    }

    public BooleanProperty bidProperty() {
        return bid;
    }

    public void setBid(boolean bid) {
        this.bid.set(bid);
    }

    @Column(name = "ativo")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grupo")
    public SdGrupoIndicadorCom getGrupo() {
        return grupo.get();
    }

    public ObjectProperty<SdGrupoIndicadorCom> grupoProperty() {
        return grupo;
    }

    public void setGrupo(SdGrupoIndicadorCom grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Transient
    public String toView() {
        return "["+sigla.get()+"] "+descricao.get();
    }

    @Override
    public String toString() {
        return "["+codigo.get()+"] "+descricao.get();
    }
}
