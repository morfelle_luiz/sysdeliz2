package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sd_itens_lista_fav_cli_001")
public class SdItensListaFavCli implements Serializable {

    private final ObjectProperty<SdListaFavoritosCliente> lista = new SimpleObjectProperty<>();
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();

    public SdItensListaFavCli() {
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "codcli", referencedColumnName = "codcli"),
            @JoinColumn(name = "catalogo", referencedColumnName = "catalogo"),
            @JoinColumn(name = "lista", referencedColumnName = "lista")
    })
    public SdListaFavoritosCliente getLista() {
        return lista.get();
    }

    public ObjectProperty<SdListaFavoritosCliente> listaProperty() {
        return lista;
    }

    public void setLista(SdListaFavoritosCliente lista) {
        this.lista.set(lista);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codigo")
    public Produto getProduto() {
        return produto.get();
    }

    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto.set(produto);
    }
}