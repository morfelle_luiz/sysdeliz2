package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Produto;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_ITENS_MKT_PEDIDO_001")
public class SdItensMktPedido {

    private final ObjectProperty<SdItensMktPedidoPk> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();

    public SdItensMktPedido() {
    }

    public SdItensMktPedido(String pedido, String codigo, String reserva, Produto produto, String cor, String tam, Integer qtde) {
        this.id.set(new SdItensMktPedidoPk(pedido, codigo, reserva, produto, cor, tam));
        this.qtde.set(qtde);
    }

    @EmbeddedId
    public SdItensMktPedidoPk getId() {
        return id.get();
    }

    public ObjectProperty<SdItensMktPedidoPk> idProperty() {
        return id;
    }

    public void setId(SdItensMktPedidoPk id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
}