package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdItensMktPedidoPk implements Serializable {

    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty reserva = new SimpleStringProperty();
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();

    public SdItensMktPedidoPk() {
    }

    public SdItensMktPedidoPk(String pedido, String codigo, String reserva, Produto produto, String cor, String tam) {
        this.pedido.set(pedido);
        this.codigo.set(codigo);
        this.reserva.set(reserva);
        this.produto.set(produto);
        this.cor.set(cor);
        this.tam.set(tam);
    }

    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "RESERVA")
    public String getReserva() {
        return reserva.get();
    }

    public StringProperty reservaProperty() {
        return reserva;
    }

    public void setReserva(String reserva) {
        this.reserva.set(reserva);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUTO")
    public Produto getProduto() {
        return produto.get();
    }

    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }
}