package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import org.hibernate.validator.constraints.CodePointLength;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sd_lista_favoritos_cliente_001")
public class SdListaFavoritosCliente extends BasicModel implements Serializable {

    private final ObjectProperty<SdListaFavoritosClientePK> id = new SimpleObjectProperty<>();
    private final StringProperty nomeLista = new SimpleStringProperty();
    private final BooleanProperty enviado = new SimpleBooleanProperty(false);
    private final ObjectProperty<LocalDateTime> dtEnvio = new SimpleObjectProperty<>();
    private List<SdItensListaFavCli> itens = new ArrayList<>();

    public SdListaFavoritosCliente() {
    }

    @EmbeddedId
    public SdListaFavoritosClientePK getId() {
        return id.get();
    }

    public ObjectProperty<SdListaFavoritosClientePK> idProperty() {
        return id;
    }

    public void setId(SdListaFavoritosClientePK id) {
        this.id.set(id);
    }

    @Column(name = "nome_lista")
    public String getNomeLista() {
        return nomeLista.get();
    }

    public StringProperty nomeListaProperty() {
        return nomeLista;
    }

    public void setNomeLista(String nomeLista) {
        this.nomeLista.set(nomeLista);
    }

    @OneToMany(mappedBy = "lista", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<SdItensListaFavCli> getItens() {
        return itens;
    }

    public void setItens(List<SdItensListaFavCli> itens) {
        this.itens = itens;
    }

    @Column(name = "enviado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviado() {
        return enviado.get();
    }

    public BooleanProperty enviadoProperty() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado.set(enviado);
    }

    @Column(name = "dt_envio")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtEnvio() {
        return dtEnvio.get();
    }

    public ObjectProperty<LocalDateTime> dtEnvioProperty() {
        return dtEnvio;
    }

    public void setDtEnvio(LocalDateTime dtEnvio) {
        this.dtEnvio.set(dtEnvio);
    }
}