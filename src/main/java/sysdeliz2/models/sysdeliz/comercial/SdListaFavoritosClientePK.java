package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Entidade;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdListaFavoritosClientePK implements Serializable {

    private final ObjectProperty<Entidade> cliente = new SimpleObjectProperty<>();
    private final ObjectProperty<SdCatalogo> catalogo = new SimpleObjectProperty<>();
    private final IntegerProperty lista = new SimpleIntegerProperty();

    public SdListaFavoritosClientePK() {
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codcli")
    public Entidade getCliente() {
        return cliente.get();
    }

    public ObjectProperty<Entidade> clienteProperty() {
        return cliente;
    }

    public void setCliente(Entidade cliente) {
        this.cliente.set(cliente);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "catalogo")
    public SdCatalogo getCatalogo() {
        return catalogo.get();
    }

    public ObjectProperty<SdCatalogo> catalogoProperty() {
        return catalogo;
    }

    public void setCatalogo(SdCatalogo catalogo) {
        this.catalogo.set(catalogo);
    }

    @Column(name = "lista")
    public int getLista() {
        return lista.get();
    }

    public IntegerProperty listaProperty() {
        return lista;
    }

    public void setLista(int lista) {
        this.lista.set(lista);
    }
}