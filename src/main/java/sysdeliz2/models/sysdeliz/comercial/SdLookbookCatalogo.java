package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javafx.beans.property.*;
import org.apache.xpath.operations.Bool;
import sysdeliz2.models.view.VSdLinhaCatalogo;
import sysdeliz2.utils.converters.BooleanAttributeZeroUmConverter;
import sysdeliz2.utils.converters.BooleanZeroEUmAttributeDeserializer;
import sysdeliz2.utils.converters.BooleanZeroEUmAttributeSerializer;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_LOOKBOOK_CATALOGO_001")
@IdClass(SdLookbookCatalogoPK.class)
public class SdLookbookCatalogo implements Serializable {

    private final ObjectProperty<SdCatalogo> catalogo = new SimpleObjectProperty<>();
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty titulo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final BooleanProperty destaque = new SimpleBooleanProperty(false);
    private final StringProperty video = new SimpleStringProperty();
    private final StringProperty ordem = new SimpleStringProperty();
    private final BooleanProperty status = new SimpleBooleanProperty(true);
    private final ObjectProperty<VSdLinhaCatalogo> linha = new SimpleObjectProperty<>();
    private final StringProperty linhaJson = new SimpleStringProperty();
    private List<SdImagensLookbook> imagens = new ArrayList<>();
    private List<SdProdutosLookbook> produtos = new ArrayList<>();

    public SdLookbookCatalogo() {
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATALOGO", referencedColumnName = "CODIGO")
    @JsonIgnore
    public SdCatalogo getCatalogo() {
        return catalogo.get();
    }

    public ObjectProperty<SdCatalogo> catalogoProperty() {
        return catalogo;
    }

    public void setCatalogo(SdCatalogo catalogo) {
        this.catalogo.set(catalogo);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CODIGO_SD_LOOKBOOK_CAT")
    @SequenceGenerator(name = "SEQ_CODIGO_SD_LOOKBOOK_CAT", sequenceName = "SEQ_CODIGO_SD_LOOKBOOK_CAT", allocationSize = 1)
    @Column(name = "CODIGO", nullable = false)
    @JsonProperty("look_codigo")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "TITULO")
    @JsonProperty("look_titulo")
    public String getTitulo() {
        return titulo.get();
    }

    public StringProperty tituloProperty() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo.set(titulo);
    }

    @Column(name = "DESCRICAO")
    @Lob
    @JsonProperty("look_descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "DESTAQUE")
    @Convert(converter = BooleanAttributeZeroUmConverter.class)
    @JsonProperty("look_destaque")
    @JsonSerialize(using = BooleanZeroEUmAttributeSerializer.class)
    @JsonDeserialize(using = BooleanZeroEUmAttributeDeserializer.class)
    public Boolean getDestaque() {
        return destaque.get();
    }

    public BooleanProperty destaqueProperty() {
        return destaque;
    }

    public void setDestaque(Boolean destaque) {
        this.destaque.set(destaque);
    }

    @Column(name = "VIDEO")
    @JsonProperty("look_video")
    public String getVideo() {
        return video.get();
    }

    public StringProperty videoProperty() {
        return video;
    }

    public void setVideo(String video) {
        this.video.set(video);
    }

    @Column(name = "ORDEM")
    @JsonProperty("look_ordem")
    public String getOrdem() {
        return ordem.get();
    }

    public StringProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "STATUS")
    @Convert(converter = BooleanAttributeZeroUmConverter.class)
    @JsonProperty("look_status")
    @JsonSerialize(using = BooleanZeroEUmAttributeSerializer.class)
    @JsonDeserialize(using = BooleanZeroEUmAttributeDeserializer.class)
    public Boolean getStatus() {
        return status.get();
    }

    public BooleanProperty statusProperty() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status.set(status);
    }

    @OneToMany(mappedBy = "id.lookbook", cascade = CascadeType.ALL)
    @JsonProperty("look_imagens")
    public List<SdImagensLookbook> getImagens() {
        return imagens;
    }

    public void setImagens(List<SdImagensLookbook> imagens) {
        this.imagens = imagens;
    }

    @OneToMany(mappedBy = "id.lookbook", cascade = CascadeType.ALL)
    @JsonProperty("produtos")
    public List<SdProdutosLookbook> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<SdProdutosLookbook> produtos) {
        this.produtos = produtos;
    }

    @OneToOne
    @JoinColumn(name = "LINHA")
    @JsonIgnore
    public VSdLinhaCatalogo getLinha() {
        return linha.get();
    }

    public ObjectProperty<VSdLinhaCatalogo> linhaProperty() {
        return linha;
    }

    public void setLinha(VSdLinhaCatalogo linha) {
        this.linha.set(linha);
    }

    @Transient
    @JsonProperty("look_linha")
    public String getLinhaJson() {
        return linhaJson.get();
    }

    public StringProperty linhaJsonProperty() {
        return linhaJson;
    }

    public void setLinhaJson(String linhaJson) {
        this.linhaJson.set(linhaJson);
    }

    @PostLoad
    public void postLoad() {
        if (getLinha() != null)
            setLinhaJson("LIN"+getCatalogo().getMarca().getCodigo()+getLinha().getCodigo());
    }

    @PostPersist
    private void postPersist() {
        postLoad();
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}