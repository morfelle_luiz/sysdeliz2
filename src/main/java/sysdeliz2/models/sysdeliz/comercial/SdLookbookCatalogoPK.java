package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.io.Serializable;


public class SdLookbookCatalogoPK implements Serializable {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final ObjectProperty<SdCatalogo> catalogo = new SimpleObjectProperty<>();

    public SdLookbookCatalogoPK() {
    }

    public Integer getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo.set(codigo);
    }

    public SdCatalogo getCatalogo() {
        return catalogo.get();
    }

    public ObjectProperty<SdCatalogo> catalogoProperty() {
        return catalogo;
    }

    public void setCatalogo(SdCatalogo catalogo) {
        this.catalogo.set(catalogo);
    }
}