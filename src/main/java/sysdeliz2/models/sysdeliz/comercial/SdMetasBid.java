package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "sd_metas_bid_001")
public class SdMetasBid implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<Represen> representante = new SimpleObjectProperty<>();
        private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
        private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
        private final StringProperty tipo = new SimpleStringProperty();

        public Pk() {
        }

        public Pk(Represen representante, Colecao colecao, Marca marca, String tipo) {
            this.representante.set(representante);
            this.colecao.set(colecao);
            this.marca.set(marca);
            this.tipo.set(tipo);
        }

        @OneToOne
        @JoinColumn(name = "codrep")
        public Represen getRepresentante() {
            return representante.get();
        }

        public ObjectProperty<Represen> representanteProperty() {
            return representante;
        }

        public void setRepresentante(Represen representante) {
            this.representante.set(representante);
        }

        @OneToOne
        @JoinColumn(name = "colecao")
        public Colecao getColecao() {
            return colecao.get();
        }

        public ObjectProperty<Colecao> colecaoProperty() {
            return colecao;
        }

        public void setColecao(Colecao colecao) {
            this.colecao.set(colecao);
        }

        @OneToOne
        @JoinColumn(name = "marca")
        public Marca getMarca() {
            return marca.get();
        }

        public ObjectProperty<Marca> marcaProperty() {
            return marca;
        }

        public void setMarca(Marca marca) {
            this.marca.set(marca);
        }

        @Column(name = "tipo")
        public String getTipo() {
            return tipo.get();
        }

        public StringProperty tipoProperty() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo.set(tipo);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> pm = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> cd = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pv = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> ka = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pp = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> vp = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tp = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> tf = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pg = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pr = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> mostr = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> cln = new SimpleObjectProperty<>(BigDecimal.ZERO);

    public SdMetasBid() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "pm")
    public BigDecimal getPm() {
        return pm.get();
    }

    public ObjectProperty<BigDecimal> pmProperty() {
        return pm;
    }

    public void setPm(BigDecimal pm) {
        this.pm.set(pm);
    }

    @Column(name = "cd")
    public BigDecimal getCd() {
        return cd.get();
    }

    public ObjectProperty<BigDecimal> cdProperty() {
        return cd;
    }

    public void setCd(BigDecimal cd) {
        this.cd.set(cd);
    }

    @Column(name = "pv")
    public BigDecimal getPv() {
        return pv.get();
    }

    public ObjectProperty<BigDecimal> pvProperty() {
        return pv;
    }

    public void setPv(BigDecimal pv) {
        this.pv.set(pv);
    }

    @Column(name = "ka")
    public BigDecimal getKa() {
        return ka.get();
    }

    public ObjectProperty<BigDecimal> kaProperty() {
        return ka;
    }

    public void setKa(BigDecimal ka) {
        this.ka.set(ka);
    }

    @Column(name = "pp")
    public BigDecimal getPp() {
        return pp.get();
    }

    public ObjectProperty<BigDecimal> ppProperty() {
        return pp;
    }

    public void setPp(BigDecimal pp) {
        this.pp.set(pp);
    }

    @Column(name = "vp")
    public BigDecimal getVp() {
        return vp.get();
    }

    public ObjectProperty<BigDecimal> vpProperty() {
        return vp;
    }

    public void setVp(BigDecimal vp) {
        this.vp.set(vp);
    }

    @Column(name = "tp")
    public BigDecimal getTp() {
        return tp.get();
    }

    public ObjectProperty<BigDecimal> tpProperty() {
        return tp;
    }

    public void setTp(BigDecimal tp) {
        this.tp.set(tp);
    }

    @Column(name = "tf")
    public BigDecimal getTf() {
        return tf.get();
    }

    public ObjectProperty<BigDecimal> tfProperty() {
        return tf;
    }

    public void setTf(BigDecimal tf) {
        this.tf.set(tf);
    }

    @Column(name = "pg")
    public BigDecimal getPg() {
        return pg.get();
    }

    public ObjectProperty<BigDecimal> pgProperty() {
        return pg;
    }

    public void setPg(BigDecimal pg) {
        this.pg.set(pg);
    }

    @Column(name = "pr")
    public BigDecimal getPr() {
        return pr.get();
    }

    public ObjectProperty<BigDecimal> prProperty() {
        return pr;
    }

    public void setPr(BigDecimal pr) {
        this.pr.set(pr);
    }

    @Column(name = "mostr")
    public BigDecimal getMostr() {
        return mostr.get();
    }

    public ObjectProperty<BigDecimal> mostrProperty() {
        return mostr;
    }

    public void setMostr(BigDecimal mostr) {
        this.mostr.set(mostr);
    }

    @Column(name = "cln")
    public BigDecimal getCln() {
        return cln.get();
    }

    public ObjectProperty<BigDecimal> clnProperty() {
        return cln;
    }

    public void setCln(BigDecimal cln) {
        this.cln.set(cln);
    }

    @Transient
    public String toLog() {
        return "Meta: " + id.get().getColecao().getCodigo() + "/" + id.get().getMarca().getCodigo() + " - " + id.get().getTipo() +
                " =>  PM = " + pm.get() + ", CD = " + cd.get() + ", PV = " + pv.get() + ", KA = " + ka.get() + ", PP = " + pp.get() + ", VP = " + vp.get() +
                ", TP = " + tp.get() + ", TF = " + tf.get() + ", PG = " + pg.get() + ", PR = " + pr.get() + ", CLN = " + cln.get();
    }
}
