package sysdeliz2.models.sysdeliz.comercial;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.validator.bean.annotation.CpfOuCnpj;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sd_mix_colecao_001")
public class SdMixColecao implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
        private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
        private final ObjectProperty<Linha> linha = new SimpleObjectProperty<>();
        private final ObjectProperty<SdGrupoModelagem> grupoModelagem = new SimpleObjectProperty<>();
        private final IntegerProperty mix = new SimpleIntegerProperty();

        public Pk() {
        }

        public Pk(Colecao colecao, Marca marca, Linha linha, SdGrupoModelagem grupoModelagem, Integer mix) {
            this.colecao.set(colecao);
            this.marca.set(marca);
            this.linha.set(linha);
            this.grupoModelagem.set(grupoModelagem);
            this.mix.set(mix);
        }

        @OneToOne
        @JoinColumn(name = "colecao")
        public Colecao getColecao() {
            return colecao.get();
        }

        public ObjectProperty<Colecao> colecaoProperty() {
            return colecao;
        }

        public void setColecao(Colecao colecao) {
            this.colecao.set(colecao);
        }

        @OneToOne
        @JoinColumn(name = "marca")
        public Marca getMarca() {
            return marca.get();
        }

        public ObjectProperty<Marca> marcaProperty() {
            return marca;
        }

        public void setMarca(Marca marca) {
            this.marca.set(marca);
        }

        @OneToOne
        @JoinColumn(name = "linha")
        public Linha getLinha() {
            return linha.get();
        }

        public ObjectProperty<Linha> linhaProperty() {
            return linha;
        }

        public void setLinha(Linha linha) {
            this.linha.set(linha);
        }

        @OneToOne
        @JoinColumn(name = "grupo_mod")
        public SdGrupoModelagem getGrupoModelagem() {
            return grupoModelagem.get();
        }

        public ObjectProperty<SdGrupoModelagem> grupoModelagemProperty() {
            return grupoModelagem;
        }

        public void setGrupoModelagem(SdGrupoModelagem grupoModelagem) {
            this.grupoModelagem.set(grupoModelagem);
        }

        @Column(name = "mix")
        public int getMix() {
            return mix.get();
        }

        public IntegerProperty mixProperty() {
            return mix;
        }

        public void setMix(int mix) {
            this.mix.set(mix);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorInicial = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorFinal = new SimpleObjectProperty<>();
    private final IntegerProperty meta = new SimpleIntegerProperty();
    private final IntegerProperty referencias = new SimpleIntegerProperty();
    private final IntegerProperty mostruario = new SimpleIntegerProperty();
    private final BooleanProperty enviadoFluxogama = new SimpleBooleanProperty(false);
    private List<SdMixProdutos> familias = new ArrayList<>();

    public SdMixColecao() {
    }

    public SdMixColecao(Colecao colecao, Marca marca, Linha linha, SdGrupoModelagem grupoModelagem, Integer mix) {
        this.id.set(new Pk(colecao, marca, linha, grupoModelagem, mix));
    }

    public SdMixColecao(Colecao colecao, Marca marca, Linha linha, SdGrupoModelagem grupoModelagem, Integer mix,
                        BigDecimal valorInicial, BigDecimal valorFinal, Integer meta, Integer referencias, Integer mostruario) {
        this.id.set(new Pk(colecao, marca, linha, grupoModelagem, mix));
        this.valorInicial.set(valorInicial);
        this.valorFinal.set(valorFinal);
        this.meta.set(meta);
        this.referencias.set(referencias);
        this.mostruario.set(mostruario);
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "valor_medio_ini")
    public BigDecimal getValorInicial() {
        return valorInicial.get();
    }

    public ObjectProperty<BigDecimal> valorInicialProperty() {
        return valorInicial;
    }

    public void setValorInicial(BigDecimal valorInicial) {
        this.valorInicial.set(valorInicial);
    }

    @Column(name = "valor_medio_fim")
    public BigDecimal getValorFinal() {
        return valorFinal.get();
    }

    public ObjectProperty<BigDecimal> valorFinalProperty() {
        return valorFinal;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal.set(valorFinal);
    }

    @Column(name = "meta")
    public int getMeta() {
        return meta.get();
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

    public void setMeta(int meta) {
        this.meta.set(meta);
    }

    @Column(name = "referencias")
    public int getReferencias() {
        return referencias.get();
    }

    public IntegerProperty referenciasProperty() {
        return referencias;
    }

    public void setReferencias(int referencias) {
        this.referencias.set(referencias);
    }

    @Column(name = "mostruario")
    public int getMostruario() {
        return mostruario.get();
    }

    public IntegerProperty mostruarioProperty() {
        return mostruario;
    }

    public void setMostruario(int mostruario) {
        this.mostruario.set(mostruario);
    }

    @Column(name = "enviado_fluxogama")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviadoFluxogama() {
        return enviadoFluxogama.get();
    }

    public BooleanProperty enviadoFluxogamaProperty() {
        return enviadoFluxogama;
    }

    public void setEnviadoFluxogama(boolean enviadoFluxogama) {
        this.enviadoFluxogama.set(enviadoFluxogama);
    }

    @Transient
    public void sendMixFluxogama() {
        setEnviadoFluxogama(true);
    }

    @OneToMany(mappedBy = "id.mix")
    public List<SdMixProdutos> getFamilias() {
        return familias;
    }

    public void setFamilias(List<SdMixProdutos> familias) {
        this.familias = familias;
    }

    @Transient
    @JsonIgnore
    public String toLog() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error to string object";
        }
    }
}
