package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "sd_mix_produtos_001")
public class SdMixProdutos implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<SdMixColecao> mix = new SimpleObjectProperty<>();
        private final ObjectProperty<Familia> familia = new SimpleObjectProperty<>();

        public Pk() {
        }

        public Pk(SdMixColecao mix, Familia familia) {
            setMix(mix);
            setFamilia(familia);
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "mix", referencedColumnName = "mix"),
                @JoinColumn(name = "colecao", referencedColumnName = "colecao"),
                @JoinColumn(name = "marca", referencedColumnName = "marca"),
                @JoinColumn(name = "linha", referencedColumnName = "linha"),
                @JoinColumn(name = "grupo_mod", referencedColumnName = "grupo_mod")
        })
        @JsonIgnore
        public SdMixColecao getMix() {
            return mix.get();
        }

        public ObjectProperty<SdMixColecao> mixProperty() {
            return mix;
        }

        public void setMix(SdMixColecao mix) {
            this.mix.set(mix);
        }

        @OneToOne
        @JoinColumn(name = "familia")
        public Familia getFamilia() {
            return familia.get();
        }

        public ObjectProperty<Familia> familiaProperty() {
            return familia;
        }

        public void setFamilia(Familia familia) {
            this.familia.set(familia);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeProdutos = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tempoMedio = new SimpleObjectProperty<>();
    private final IntegerProperty meta = new SimpleIntegerProperty();
    private final BooleanProperty enviadoFluxogama = new SimpleBooleanProperty(false);

    public SdMixProdutos() {
    }

    public SdMixProdutos(SdMixColecao mix, Familia familia) {
        setId(new Pk(mix, familia));
    }

    public SdMixProdutos(SdMixColecao mix, Familia familia,
                         Integer qtdeProdutos, Integer meta, BigDecimal tempoMedio) {
        setId(new Pk(mix, familia));
        setQtdeProdutos(qtdeProdutos);
        setMeta(meta);
        setTempoMedio(tempoMedio);
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "qtde_produtos")
    public Integer getQtdeProdutos() {
        return qtdeProdutos.get();
    }

    public IntegerProperty qtdeProdutosProperty() {
        return qtdeProdutos;
    }

    public void setQtdeProdutos(Integer qtdeProdutos) {
        this.qtdeProdutos.set(qtdeProdutos);
    }

    @Column(name = "tempo_medio")
    public BigDecimal getTempoMedio() {
        return tempoMedio.get();
    }

    public ObjectProperty<BigDecimal> tempoMedioProperty() {
        return tempoMedio;
    }

    public void setTempoMedio(BigDecimal tempoMedio) {
        this.tempoMedio.set(tempoMedio);
    }

    @Column(name = "meta")
    public Integer getMeta() {
        return meta.get();
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta.set(meta);
    }

    @Column(name = "enviado_fluxogama")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviadoFluxogama() {
        return enviadoFluxogama.get();
    }

    public BooleanProperty enviadoFluxogamaProperty() {
        return enviadoFluxogama;
    }

    public void setEnviadoFluxogama(boolean enviadoFluxogama) {
        this.enviadoFluxogama.set(enviadoFluxogama);
    }

    @Transient
    @JsonIgnore
    public String toLog() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error to string object";
        }
    }
}