package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosPedido;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_MKT_PEDIDO_001")
public class SdMktPedido implements Serializable {
    
    private final ObjectProperty<SdMktPedidoPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> datacad = new SimpleObjectProperty<LocalDateTime>();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private List<SdItensMktPedido> itens = new ArrayList<>();
    
    public SdMktPedido() {
    }

    public SdMktPedido(VSdDadosPedido pedido, String codigoMkt, int qtde, String nomeUsuario) {
        this.id.set(new SdMktPedidoPK(pedido.getNumero().getNumero(), new VSdDadosProduto(codigoMkt),"0"));
        this.qtde.set(qtde);
        this.datacad.set(LocalDateTime.now());
         this.usuario.set(nomeUsuario);
    }

    @EmbeddedId
    public SdMktPedidoPK getId() {
        return id.get();
    }
    
    public ObjectProperty<SdMktPedidoPK> idProperty() {
        return id;
    }
    
    public void setId(SdMktPedidoPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "DATA_CAD")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDatacad() {
        return datacad.get();
    }
    
    public ObjectProperty<LocalDateTime> datacadProperty() {
        return datacad;
    }
    
    public void setDatacad(LocalDateTime datacad) {
        this.datacad.set(datacad);
    }
    
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }
    
    public StringProperty usuarioProperty() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }
    
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "PEDIDO", referencedColumnName = "PEDIDO"),
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "RESERVA", referencedColumnName = "RESERVA")
    })
    public List<SdItensMktPedido> getItens() {
        return itens;
    }

    public void setItens(List<SdItensMktPedido> itens) {
        this.itens = itens;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdMktPedido that = (SdMktPedido) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}