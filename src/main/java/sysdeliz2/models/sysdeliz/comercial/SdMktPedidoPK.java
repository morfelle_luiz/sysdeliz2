package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SdMktPedidoPK implements Serializable {
    
    private final StringProperty pedido = new SimpleStringProperty();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final StringProperty reserva = new SimpleStringProperty();
    
    public SdMktPedidoPK() {
    }

    public SdMktPedidoPK(String pedido, VSdDadosProduto codigo, String reserva) {
        this.pedido.set(pedido);
        this.codigo.set(codigo);
        this.reserva.set(reserva);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "RESERVA")
    public String getReserva() {
        return reserva.get();
    }
    
    public StringProperty reservaProperty() {
        return reserva;
    }
    
    public void setReserva(String reserva) {
        this.reserva.set(reserva);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdMktPedidoPK that = (SdMktPedidoPK) o;
        return Objects.equals(pedido, that.pedido) && Objects.equals(codigo, that.codigo) && Objects.equals(reserva, that.reserva);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pedido, codigo, reserva);
    }
}