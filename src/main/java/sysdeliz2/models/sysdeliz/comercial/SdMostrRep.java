package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_MOSTR_REP_001")
public class SdMostrRep implements Serializable {

    private final IntegerProperty mostruario = new SimpleIntegerProperty();
    private final StringProperty colvenda = new SimpleStringProperty();
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty("P");
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    private List<SdMostrRepItens> itens = new ArrayList<>();

    private final StringProperty statusExpedicao = new SimpleStringProperty();

    public SdMostrRep() {
    }

    @Id
    @Column(name = "MOSTRUARIO", nullable = false)
    public Integer getMostruario() {
        return mostruario.get();
    }

    public IntegerProperty mostruarioProperty() {
        return mostruario;
    }

    public void setMostruario(Integer mostruario) {
        this.mostruario.set(mostruario);
    }

    @Id
    @Column(name = "COL_VENDA")
    public String getColvenda() {
        return colvenda.get();
    }

    public StringProperty colvendaProperty() {
        return colvenda;
    }

    public void setColvenda(String colvenda) {
        this.colvenda.set(colvenda);
    }

    @OneToOne
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }

    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }

    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }

    @Id
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Id
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "REMESSA")
    public String getRemessa() {
        return remessa.get();
    }

    public StringProperty remessaProperty() {
        return remessa;
    }

    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }

    @OneToMany(mappedBy = "mostruario", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public List<SdMostrRepItens> getItens() {
        return itens;
    }

    public void setItens(List<SdMostrRepItens> itens) {
        this.itens = itens;
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

    @Transient
    public String getStatusExpedicao() {
        return statusExpedicao.get();
    }

    public StringProperty statusExpedicaoProperty() {
        return statusExpedicao;
    }

    public void setStatusExpedicao(String statusExpedicao) {
        this.statusExpedicao.set(statusExpedicao);
    }
}