package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_MOSTR_REP_ITENS_001")
public class SdMostrRepItens implements Serializable {

    private final ObjectProperty<SdMostrRep> mostruario = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdelido = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdeDevoldida = new SimpleIntegerProperty(0);
    private final StringProperty status = new SimpleStringProperty("P");

    public SdMostrRepItens() {
    }

    public SdMostrRepItens(SdMostrRep mostruario, VSdDadosProduto codigo, Integer qtde) {
        this.mostruario.set(mostruario);
        this.codigo.set(codigo);
        this.qtde.set(qtde);
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "MOSTRUARIO", referencedColumnName = "mostruario"),
            @JoinColumn(name = "TIPO", referencedColumnName = "tipo"),
            @JoinColumn(name = "MARCA", referencedColumnName = "marca"),
            @JoinColumn(name = "COL_VENDA", referencedColumnName = "col_venda")
    })
    public SdMostrRep getMostruario() {
        return mostruario.get();
    }

    public ObjectProperty<SdMostrRep> mostruarioProperty() {
        return mostruario;
    }

    public void setMostruario(SdMostrRep mostruario) {
        this.mostruario.set(mostruario);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "QTDE_LIDA")
    public int getQtdelido() {
        return qtdelido.get();
    }

    public IntegerProperty qtdelidoProperty() {
        return qtdelido;
    }

    public void setQtdelido(int qtdelido) {
        this.qtdelido.set(qtdelido);
    }

    @Column(name = "QTDE_DEVOLVIDA")
    public int getQtdeDevoldida() {
        return qtdeDevoldida.get();
    }

    public IntegerProperty qtdeDevoldidaProperty() {
        return qtdeDevoldida;
    }

    public void setQtdeDevoldida(int qtdeDevoldida) {
        this.qtdeDevoldida.set(qtdeDevoldida);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}
