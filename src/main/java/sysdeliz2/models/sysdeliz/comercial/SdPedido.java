package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.utils.sys.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Types;
import java.time.LocalDate;

@Entity
@Table(name = "SD_PEDIDO_001")
public class SdPedido implements Serializable {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty envioespelho = new SimpleStringProperty("S");
    private final ObjectProperty<LocalDate> dtbloqueio = new SimpleObjectProperty<LocalDate>();
    private final StringProperty enviobid = new SimpleStringProperty("S");
    private final StringProperty acaotomadacrm = new SimpleStringProperty();
    private final StringProperty analisecomissao = new SimpleStringProperty("N");
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty aplicoupack = new SimpleStringProperty("N");
    private final StringProperty origem = new SimpleStringProperty("ERP");
    private final StringProperty colecao = new SimpleStringProperty();

    public SdPedido() {
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "ENVIO_ESPELHO")
    public String getEnvioespelho() {
        return envioespelho.get();
    }

    public StringProperty envioespelhoProperty() {
        return envioespelho;
    }

    public void setEnvioespelho(String envioespelho) {
        this.envioespelho.set(envioespelho);
    }

    @Column(name = "DT_BLOQUEIO")
    public LocalDate getDtbloqueio() {
        return dtbloqueio.get();
    }

    public ObjectProperty<LocalDate> dtbloqueioProperty() {
        return dtbloqueio;
    }

    public void setDtbloqueio(LocalDate dtbloqueio) {
        this.dtbloqueio.set(dtbloqueio);
    }

    @Column(name = "ENVIO_BID")
    public String getEnviobid() {
        return enviobid.get();
    }

    public StringProperty enviobidProperty() {
        return enviobid;
    }

    public void setEnviobid(String enviobid) {
        this.enviobid.set(enviobid);
    }

    @Column(name = "ACAO_TOMADA_CRM")
    public String getAcaotomadacrm() {
        return acaotomadacrm.get();
    }

    public StringProperty acaotomadacrmProperty() {
        return acaotomadacrm;
    }

    public void setAcaotomadacrm(String acaotomadacrm) {
        this.acaotomadacrm.set(acaotomadacrm);
    }

    @Column(name = "ANALISE_COMISSAO")
    public String getAnalisecomissao() {
        return analisecomissao.get();
    }

    public StringProperty analisecomissaoProperty() {
        return analisecomissao;
    }

    public void setAnalisecomissao(String analisecomissao) {
        this.analisecomissao.set(analisecomissao);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "APLICOU_PACK")
    public String getAplicoupack() {
        return aplicoupack.get();
    }

    public StringProperty aplicoupackProperty() {
        return aplicoupack;
    }

    public void setAplicoupack(String aplicoupack) {
        this.aplicoupack.set(aplicoupack);
    }

    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem.set(origem);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

}