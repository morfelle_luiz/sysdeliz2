package sysdeliz2.models.sysdeliz.comercial;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_PRODUTOS_CONCEITO_001")
public class SdProdutosConceito implements Serializable {

    private final ObjectProperty<SdProdutosConceitoPK> id = new SimpleObjectProperty<>();
    private final StringProperty codigo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> eixoX = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> eixoY = new SimpleObjectProperty<>();

    public SdProdutosConceito() {
    }

    @EmbeddedId
    @JsonIgnore
    public SdProdutosConceitoPK getId() {
        return id.get();
    }

    public ObjectProperty<SdProdutosConceitoPK> idProperty() {
        return id;
    }

    public void setId(SdProdutosConceitoPK id) {
        this.id.set(id);
    }

    @Transient
    @JsonProperty("produto")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "EIXO_X")
    @JsonProperty("x")
    public BigDecimal getEixoX() {
        return eixoX.get();
    }

    public ObjectProperty<BigDecimal> eixoXProperty() {
        return eixoX;
    }

    public void setEixoX(BigDecimal eixoX) {
        this.eixoX.set(eixoX);
    }

    @Column(name = "EIXO_Y")
    @JsonProperty("y")
    public BigDecimal getEixoY() {
        return eixoY.get();
    }

    public ObjectProperty<BigDecimal> eixoYProperty() {
        return eixoY;
    }

    public void setEixoY(BigDecimal eixoY) {
        this.eixoY.set(eixoY);
    }

    @PostLoad
    private void postLoad() {
        setCodigo(getId().getCodigo().getCodigo());
    }

    @PostPersist
    private void postPersist() {
        setCodigo(getId().getCodigo().getCodigo());
    }
}