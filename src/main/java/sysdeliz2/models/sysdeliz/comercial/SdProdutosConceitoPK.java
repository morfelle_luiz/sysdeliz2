package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdProdutosConceitoPK implements Serializable {

    private final ObjectProperty<SdConceitoCatalogo> conceito = new SimpleObjectProperty<>();
    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public SdProdutosConceitoPK() {
    }

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "CATALOGO", referencedColumnName = "CATALOGO"),
            @JoinColumn(name = "CONCEITO", referencedColumnName = "CODIGO")
    })
    public SdConceitoCatalogo getConceito() {
        return conceito.get();
    }

    public ObjectProperty<SdConceitoCatalogo> conceitoProperty() {
        return conceito;
    }

    public void setConceito(SdConceitoCatalogo conceito) {
        this.conceito.set(conceito);
    }

    @OneToOne
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
}