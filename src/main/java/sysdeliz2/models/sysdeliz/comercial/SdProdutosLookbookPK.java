package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;
import org.hibernate.annotations.CollectionType;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdProdutosLookbookPK implements Serializable {

    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<SdLookbookCatalogo> lookbook = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public SdProdutosLookbookPK() {
    }

    @OneToOne
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "CATALOGO", referencedColumnName = "CATALOGO"),
            @JoinColumn(name = "LOOKBOOK", referencedColumnName = "CODIGO")
    })
    public SdLookbookCatalogo getLookbook() {
        return lookbook.get();
    }

    public ObjectProperty<SdLookbookCatalogo> lookbookProperty() {
        return lookbook;
    }

    public void setLookbook(SdLookbookCatalogo lookbook) {
        this.lookbook.set(lookbook);
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
}