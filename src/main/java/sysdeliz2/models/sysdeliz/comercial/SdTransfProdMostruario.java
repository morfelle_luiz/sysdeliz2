package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_TRANSF_PROD_MOSTRUARIO_001")
public class SdTransfProdMostruario implements Serializable {

    @Embeddable
    public static class SdTransfProdMostruarioPK implements Serializable {

        private final StringProperty colecao = new SimpleStringProperty();
        private final StringProperty representante = new SimpleStringProperty();
        private final StringProperty produto = new SimpleStringProperty();
        private final IntegerProperty mostruario = new SimpleIntegerProperty();
        private final StringProperty barra = new SimpleStringProperty();

        public SdTransfProdMostruarioPK() {
        }
        public SdTransfProdMostruarioPK(String colecao, String representante, String produto, Integer mostruario, String barra) {
            setColecao(colecao);
            setProduto(produto);
            setRepresentante(representante);
            setMostruario(mostruario);
            setBarra(barra);
        }

        // <editor-fold defaultstate="collapsed" desc="Getter/Setter">
        @Column(name = "COLECAO")
        public String getColecao() {
            return colecao.get();
        }

        public StringProperty colecaoProperty() {
            return colecao;
        }

        public void setColecao(String colecao) {
            this.colecao.set(colecao);
        }

        @Column(name = "REPRESENTANTE")
        public String getRepresentante() {
            return representante.get();
        }

        public StringProperty representanteProperty() {
            return representante;
        }

        public void setRepresentante(String representante) {
            this.representante.set(representante);
        }

        @Column(name = "PRODUTO")
        public String getProduto() {
            return produto.get();
        }

        public StringProperty produtoProperty() {
            return produto;
        }

        public void setProduto(String produto) {
            this.produto.set(produto);
        }

        @Column(name = "MOSTRUARIO")
        public int getMostruario() {
            return mostruario.get();
        }

        public IntegerProperty mostruarioProperty() {
            return mostruario;
        }

        public void setMostruario(int mostruario) {
            this.mostruario.set(mostruario);
        }

        @Column(name = "BARRA")
        public String getBarra() {
            return barra.get();
        }

        public StringProperty barraProperty() {
            return barra;
        }

        public void setBarra(String barra) {
            this.barra.set(barra);
        }
        // </editor-fold>
    }

    private final ObjectProperty<SdTransfProdMostruarioPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty("P");

    public SdTransfProdMostruario() {
    }
    public SdTransfProdMostruario(String colecao, String representante, String produto, Integer mostruario, Integer qtde, String status, String barra) {
        setId(new SdTransfProdMostruarioPK(colecao, representante, produto, mostruario, barra));
        setQtde(qtde);
        setStatus(status);
    }

    @EmbeddedId
    public SdTransfProdMostruarioPK getId() {
        return id.get();
    }

    public ObjectProperty<SdTransfProdMostruarioPK> idProperty() {
        return id;
    }

    public void setId(SdTransfProdMostruarioPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }
}