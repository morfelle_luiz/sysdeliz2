package sysdeliz2.models.sysdeliz.comercial;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Colecao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "v_sd_bid_cidade")
public class VSdBidCidade implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<SdBid> bid = new SimpleObjectProperty<>();
        private final ObjectProperty<Cidade> cidade = new SimpleObjectProperty<>();

        public Pk() {
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "codrep", referencedColumnName = "codrep"),
                @JoinColumn(name = "colecao", referencedColumnName = "codcol"),
                @JoinColumn(name = "marca", referencedColumnName = "codmar")
        })
        public SdBid getBid() {
            return bid.get();
        }

        public ObjectProperty<SdBid> bidProperty() {
            return bid;
        }

        public void setBid(SdBid bid) {
            this.bid.set(bid);
        }

        @OneToOne
        @JoinColumn(name = "codcid")
        public Cidade getCidade() {
            return cidade.get();
        }

        public ObjectProperty<Cidade> cidadeProperty() {
            return cidade;
        }

        public void setCidade(Cidade cidade) {
            this.cidade.set(cidade);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colLy = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colAnt = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colPass = new SimpleObjectProperty<>();
    private final IntegerProperty clientesAtual = new SimpleIntegerProperty();
    private final IntegerProperty clientesLy = new SimpleIntegerProperty();
    private final IntegerProperty clientesAnt = new SimpleIntegerProperty();
    private final IntegerProperty clientesPass = new SimpleIntegerProperty();
    private final IntegerProperty atendimentoClientes = new SimpleIntegerProperty();
    private final IntegerProperty gruposAtual = new SimpleIntegerProperty();
    private final IntegerProperty gruposLy = new SimpleIntegerProperty();
    private final IntegerProperty gruposAnt = new SimpleIntegerProperty();
    private final IntegerProperty gruposPass = new SimpleIntegerProperty();
    private final IntegerProperty atendimentoGrupos = new SimpleIntegerProperty();
    private final IntegerProperty pecasAtual = new SimpleIntegerProperty();
    private final IntegerProperty pecasLy = new SimpleIntegerProperty();
    private final IntegerProperty pecasAnt = new SimpleIntegerProperty();
    private final IntegerProperty pecasPass = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valorAtual = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorLy = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorAnt = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorPass = new SimpleObjectProperty<>();

    public VSdBidCidade() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "colly")
    public Colecao getColLy() {
        return colLy.get();
    }

    public ObjectProperty<Colecao> colLyProperty() {
        return colLy;
    }

    public void setColLy(Colecao colLy) {
        this.colLy.set(colLy);
    }

    @OneToOne
    @JoinColumn(name = "colant")
    public Colecao getColAnt() {
        return colAnt.get();
    }

    public ObjectProperty<Colecao> colAntProperty() {
        return colAnt;
    }

    public void setColAnt(Colecao colAnt) {
        this.colAnt.set(colAnt);
    }

    @OneToOne
    @JoinColumn(name = "colpas")
    public Colecao getColPass() {
        return colPass.get();
    }

    public ObjectProperty<Colecao> colPassProperty() {
        return colPass;
    }

    public void setColPass(Colecao colPass) {
        this.colPass.set(colPass);
    }

    @Column(name = "cliatual")
    public int getClientesAtual() {
        return clientesAtual.get();
    }

    public IntegerProperty clientesAtualProperty() {
        return clientesAtual;
    }

    public void setClientesAtual(int clientesAtual) {
        this.clientesAtual.set(clientesAtual);
    }

    @Column(name = "clily")
    public int getClientesLy() {
        return clientesLy.get();
    }

    public IntegerProperty clientesLyProperty() {
        return clientesLy;
    }

    public void setClientesLy(int clientesLy) {
        this.clientesLy.set(clientesLy);
    }

    @Column(name = "cli1")
    public int getClientesAnt() {
        return clientesAnt.get();
    }

    public IntegerProperty clientesAntProperty() {
        return clientesAnt;
    }

    public void setClientesAnt(int clientesAnt) {
        this.clientesAnt.set(clientesAnt);
    }

    @Column(name = "cli2")
    public int getClientesPass() {
        return clientesPass.get();
    }

    public IntegerProperty clientesPassProperty() {
        return clientesPass;
    }

    public void setClientesPass(int clientesPass) {
        this.clientesPass.set(clientesPass);
    }

    @Column(name = "atendcli")
    public int getAtendimentoClientes() {
        return atendimentoClientes.get();
    }

    public IntegerProperty atendimentoClientesProperty() {
        return atendimentoClientes;
    }

    public void setAtendimentoClientes(int atendimentoClientes) {
        this.atendimentoClientes.set(atendimentoClientes);
    }

    @Column(name = "grpatual")
    public int getGruposAtual() {
        return gruposAtual.get();
    }

    public IntegerProperty gruposAtualProperty() {
        return gruposAtual;
    }

    public void setGruposAtual(int gruposAtual) {
        this.gruposAtual.set(gruposAtual);
    }

    @Column(name = "grply")
    public int getGruposLy() {
        return gruposLy.get();
    }

    public IntegerProperty gruposLyProperty() {
        return gruposLy;
    }

    public void setGruposLy(int gruposLy) {
        this.gruposLy.set(gruposLy);
    }

    @Column(name = "grp1")
    public int getGruposAnt() {
        return gruposAnt.get();
    }

    public IntegerProperty gruposAntProperty() {
        return gruposAnt;
    }

    public void setGruposAnt(int gruposAnt) {
        this.gruposAnt.set(gruposAnt);
    }

    @Column(name = "grp2")
    public int getGruposPass() {
        return gruposPass.get();
    }

    public IntegerProperty gruposPassProperty() {
        return gruposPass;
    }

    public void setGruposPass(int gruposPass) {
        this.gruposPass.set(gruposPass);
    }

    @Column(name = "atendgrp")
    public int getAtendimentoGrupos() {
        return atendimentoGrupos.get();
    }

    public IntegerProperty atendimentoGruposProperty() {
        return atendimentoGrupos;
    }

    public void setAtendimentoGrupos(int atendimentoGrupos) {
        this.atendimentoGrupos.set(atendimentoGrupos);
    }

    @Column(name = "pcatual")
    public int getPecasAtual() {
        return pecasAtual.get();
    }

    public IntegerProperty pecasAtualProperty() {
        return pecasAtual;
    }

    public void setPecasAtual(int pecasAtual) {
        this.pecasAtual.set(pecasAtual);
    }

    @Column(name = "pcly")
    public int getPecasLy() {
        return pecasLy.get();
    }

    public IntegerProperty pecasLyProperty() {
        return pecasLy;
    }

    public void setPecasLy(int pecasLy) {
        this.pecasLy.set(pecasLy);
    }

    @Column(name = "pc1")
    public int getPecasAnt() {
        return pecasAnt.get();
    }

    public IntegerProperty pecasAntProperty() {
        return pecasAnt;
    }

    public void setPecasAnt(int pecasAnt) {
        this.pecasAnt.set(pecasAnt);
    }

    @Column(name = "pc2")
    public int getPecasPass() {
        return pecasPass.get();
    }

    public IntegerProperty pecasPassProperty() {
        return pecasPass;
    }

    public void setPecasPass(int pecasPass) {
        this.pecasPass.set(pecasPass);
    }

    @Column(name = "vlratual")
    public BigDecimal getValorAtual() {
        return valorAtual.get();
    }

    public ObjectProperty<BigDecimal> valorAtualProperty() {
        return valorAtual;
    }

    public void setValorAtual(BigDecimal valorAtual) {
        this.valorAtual.set(valorAtual);
    }

    @Column(name = "vlrly")
    public BigDecimal getValorLy() {
        return valorLy.get();
    }

    public ObjectProperty<BigDecimal> valorLyProperty() {
        return valorLy;
    }

    public void setValorLy(BigDecimal valorLy) {
        this.valorLy.set(valorLy);
    }

    @Column(name = "vlr1")
    public BigDecimal getValorAnt() {
        return valorAnt.get();
    }

    public ObjectProperty<BigDecimal> valorAntProperty() {
        return valorAnt;
    }

    public void setValorAnt(BigDecimal valorAnt) {
        this.valorAnt.set(valorAnt);
    }

    @Column(name = "vlr2")
    public BigDecimal getValorPass() {
        return valorPass.get();
    }

    public ObjectProperty<BigDecimal> valorPassProperty() {
        return valorPass;
    }

    public void setValorPass(BigDecimal valorPass) {
        this.valorPass.set(valorPass);
    }
}
