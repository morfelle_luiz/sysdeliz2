package sysdeliz2.models.sysdeliz.ecommerce.b2b;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SD_PRODUTO_COR_TABELA_001")
public class SdProdutoCorTabela {

    private final ObjectProperty<SdProdutoCorTabelaPK> id = new SimpleObjectProperty<>();

    public SdProdutoCorTabela() {
    }

    @EmbeddedId
    public SdProdutoCorTabelaPK getId() {
        return id.get();
    }

    public ObjectProperty<SdProdutoCorTabelaPK> idProperty() {
        return id;
    }

    public void setId(SdProdutoCorTabelaPK id) {
        this.id.set(id);
    }
}