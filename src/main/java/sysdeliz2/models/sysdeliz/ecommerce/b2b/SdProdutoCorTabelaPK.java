package sysdeliz2.models.sysdeliz.ecommerce.b2b;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Regiao;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdProdutoCorTabelaPK implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final ObjectProperty<Regiao> tabela = new SimpleObjectProperty<>();

    public SdProdutoCorTabelaPK() {
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TABELA")
    public Regiao getTabela() {
        return tabela.get();
    }

    public ObjectProperty<Regiao> tabelaProperty() {
        return tabela;
    }

    public void setTabela(Regiao tabela) {
        this.tabela.set(tabela);
    }
}