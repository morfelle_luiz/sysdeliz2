package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Bordero;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_CONTRATO_FIDIC_001")
public class SdContratoFidic implements Serializable {
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final ObjectProperty<Bordero> bordero = new SimpleObjectProperty<>(null);
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty usuario = new SimpleStringProperty();
    private final BooleanProperty enviado = new SimpleBooleanProperty(false);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>(BigDecimal.ZERO);

    private List<SdItemContratoFidic> items = new ArrayList<>();

    public SdContratoFidic() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_CONTRATO_FIDIC")
    @SequenceGenerator(sequenceName = "SEQ_SD_CONTRATO_FIDIC", name = "SEQ_SD_CONTRATO_FIDIC", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "BORDERO", referencedColumnName = "NUMERO")
    public Bordero getBordero() {
        return bordero.get();
    }

    public ObjectProperty<Bordero> borderoProperty() {
        return bordero;
    }

    public void setBordero(Bordero bordero) {
        this.bordero.set(bordero);
    }

    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @Column(name = "ENVIADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviado() {
        return enviado.get();
    }

    public BooleanProperty enviadoProperty() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado.set(enviado);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato", orphanRemoval = true, fetch = FetchType.LAZY)
    public List<SdItemContratoFidic> getItems() {
        return items;
    }

    public void setItems(List<SdItemContratoFidic> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdContratoFidic that = (SdContratoFidic) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
