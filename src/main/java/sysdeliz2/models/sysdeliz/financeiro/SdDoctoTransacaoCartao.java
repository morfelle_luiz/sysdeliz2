package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_DOCTO_TRANSACAO_CARTAO_001")
public class SdDoctoTransacaoCartao implements Serializable {

    private final ObjectProperty<SdDoctoTransacaoCartaoPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();

    public SdDoctoTransacaoCartao() {
    }

    public SdDoctoTransacaoCartao(SdTransacaoCartao transacao, String docto, BigDecimal valor) {
        this.id.set(new SdDoctoTransacaoCartaoPK(transacao, docto));
        this.valor.set(valor);
    }

    @EmbeddedId
    public SdDoctoTransacaoCartaoPK getId() {
        return id.get();
    }

    public ObjectProperty<SdDoctoTransacaoCartaoPK> idProperty() {
        return id;
    }

    public void setId(SdDoctoTransacaoCartaoPK id) {
        this.id.set(id);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
}
