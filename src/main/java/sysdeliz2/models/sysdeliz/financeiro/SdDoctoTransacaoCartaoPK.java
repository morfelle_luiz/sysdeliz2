package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
public class SdDoctoTransacaoCartaoPK implements Serializable {


    private final StringProperty docto = new SimpleStringProperty();
    private final ObjectProperty<SdTransacaoCartao> transacao = new SimpleObjectProperty<>();

    public SdDoctoTransacaoCartaoPK() {
    }

    public SdDoctoTransacaoCartaoPK(SdTransacaoCartao transacao, String docto) {
        this.transacao.set(transacao);
        this.docto.set(docto);
    }

    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }

    public StringProperty doctoProperty() {
        return docto;
    }

    public void setDocto(String docto) {
        this.docto.set(docto);
    }

    @ManyToOne
    @JoinColumn(name = "TRANSACAO")
    public SdTransacaoCartao getTransacao() {
        return transacao.get();
    }

    public ObjectProperty<SdTransacaoCartao> transacaoProperty() {
        return transacao;
    }

    public void setTransacao(SdTransacaoCartao transacao) {
        this.transacao.set(transacao);
    }

}