package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SD_ERRO_ABECS_001")
public class SdErroAbecs {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty codigoAbecs = new SimpleStringProperty();
    private final StringProperty bandeira = new SimpleStringProperty();
    private final StringProperty definicao = new SimpleStringProperty();
    private final StringProperty acao = new SimpleStringProperty();
    private final StringProperty significado = new SimpleStringProperty();
    private final StringProperty retentativa = new SimpleStringProperty();

    public SdErroAbecs() {
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "CODIGO_ABECS")
    public String getCodigoAbecs() {
        return codigoAbecs.get();
    }

    public StringProperty codigoAbecsProperty() {
        return codigoAbecs;
    }

    public void setCodigoAbecs(String codigoAbecs) {
        this.codigoAbecs.set(codigoAbecs);
    }

    @Column(name = "BANDEIRA")
    public String getBandeira() {
        return bandeira.get();
    }

    public StringProperty bandeiraProperty() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira.set(bandeira);
    }

    @Column(name = "DEFINICAO")
    public String getDefinicao() {
        return definicao.get();
    }

    public StringProperty definicaoProperty() {
        return definicao;
    }

    public void setDefinicao(String definicao) {
        this.definicao.set(definicao);
    }

    @Column(name = "ACAO")
    public String getAcao() {
        return acao.get();
    }

    public StringProperty acaoProperty() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao.set(acao);
    }

    @Column(name = "SIGNIFICADO")
    public String getSignificado() {
        return significado.get();
    }

    public StringProperty significadoProperty() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado.set(significado);
    }

    @Column(name = "RETENTATIVA")
    public String getRetentativa() {
        return retentativa.get();
    }

    public StringProperty retentativaProperty() {
        return retentativa;
    }

    public void setRetentativa(String retentativa) {
        this.retentativa.set(retentativa);
    }
}