package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SD_ERRO_API_CIELO_001")
public class SdErroApiCielo {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty mensagem = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();

    public SdErroApiCielo() {
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "MENSAGEM")
    public String getMensagem() {
        return mensagem.get();
    }

    public StringProperty mensagemProperty() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem.set(mensagem);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
}