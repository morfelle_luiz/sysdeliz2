package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Receber;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Sd_ITEM_CONTRATO_FIDIC_001")
public class SdItemContratoFidic implements Serializable {
    private final ObjectProperty<SdContratoFidic> contrato = new SimpleObjectProperty<>();
    private final ObjectProperty<Receber> titulo = new SimpleObjectProperty<>();
    private final BooleanProperty recomprado = new SimpleBooleanProperty(false);

    public SdItemContratoFidic() {
    }

    public SdItemContratoFidic(SdContratoFidic contrato, Receber titulo) {
        this.contrato.set(contrato);
        this.titulo.set(titulo);
    }

    @JoinColumn(name = "CONTRATO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    public SdContratoFidic getContrato() {
        return contrato.get();
    }

    public ObjectProperty<SdContratoFidic> contratoProperty() {
        return contrato;
    }

    public void setContrato(SdContratoFidic contrato) {
        this.contrato.set(contrato);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "TITULO", referencedColumnName = "NUMERO")
    public Receber getTitulo() {
        return titulo.get();
    }

    public ObjectProperty<Receber> tituloProperty() {
        return titulo;
    }

    public void setTitulo(Receber titulo) {
        this.titulo.set(titulo);
    }

    @Column(name = "RECOMPRADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isRecomprado() {
        return recomprado.get();
    }

    public BooleanProperty recompradoProperty() {
        return recomprado;
    }

    public void setRecomprado(boolean recomprado) {
        this.recomprado.set(recomprado);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdItemContratoFidic that = (SdItemContratoFidic) o;
        return Objects.equals(contrato, that.contrato) && Objects.equals(titulo, that.titulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contrato, titulo);
    }
}
