package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Receber;

import java.io.Serializable;
import java.util.Objects;


public class SdItemContratoFidicPk implements Serializable {
    private final ObjectProperty<SdContratoFidic> contrato = new SimpleObjectProperty<>();
    private final ObjectProperty<Receber> titulo = new SimpleObjectProperty<>();

    public SdItemContratoFidicPk() {
    }

    public SdContratoFidic getContrato() {
        return contrato.get();
    }

    public ObjectProperty<SdContratoFidic> contratoProperty() {
        return contrato;
    }

    public void setContrato(SdContratoFidic contrato) {
        this.contrato.set(contrato);
    }


    public Receber getTitulo() {
        return titulo.get();
    }

    public ObjectProperty<Receber> tituloProperty() {
        return titulo;
    }

    public void setTitulo(Receber titulo) {
        this.titulo.set(titulo);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdItemContratoFidicPk that = (SdItemContratoFidicPk) o;
        return Objects.equals(contrato, that.contrato) && Objects.equals(titulo, that.titulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contrato, titulo);
    }
}
