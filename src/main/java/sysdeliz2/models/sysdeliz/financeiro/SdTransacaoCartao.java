package sysdeliz2.models.sysdeliz.financeiro;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_TRANSACAO_CARTAO_001")
public class SdTransacaoCartao implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty();
    private final BooleanProperty capturado = new SimpleBooleanProperty();
    private final StringProperty tid = new SimpleStringProperty();
    private final StringProperty proofofsale = new SimpleStringProperty();
    private final StringProperty authorizationcode = new SimpleStringProperty();
    private final StringProperty paymentid = new SimpleStringProperty();
    private final IntegerProperty valor = new SimpleIntegerProperty();
    private final IntegerProperty parcelas = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dttransacao = new SimpleObjectProperty<LocalDateTime>();
    private final StringProperty numerocartao = new SimpleStringProperty();
    private final StringProperty validade = new SimpleStringProperty();
    private final StringProperty bandeira = new SimpleStringProperty();
    private final StringProperty titular = new SimpleStringProperty();
    private final IntegerProperty valorcancelado = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> dtcancelamento = new SimpleObjectProperty<LocalDateTime>();
    private final ObjectProperty<Entidade> codcli = new SimpleObjectProperty<Entidade>();
    private List<SdDoctoTransacaoCartao> doctos = new ArrayList<>();

    public SdTransacaoCartao() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "CAPTURADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isCapturado() {
        return capturado.get();
    }

    public BooleanProperty capturadoProperty() {
        return capturado;
    }

    public void setCapturado(Boolean capturado) {
        this.capturado.set(capturado);
    }

    @Column(name = "TID")
    public String getTid() {
        return tid.get();
    }

    public StringProperty tidProperty() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid.set(tid);
    }

    @Column(name = "PROOF_OF_SALE")
    public String getProofofsale() {
        return proofofsale.get();
    }

    public StringProperty proofofsaleProperty() {
        return proofofsale;
    }

    public void setProofofsale(String proofofsale) {
        this.proofofsale.set(proofofsale);
    }

    @Column(name = "AUTHORIZATION_CODE")
    public String getAuthorizationcode() {
        return authorizationcode.get();
    }

    public StringProperty authorizationcodeProperty() {
        return authorizationcode;
    }

    public void setAuthorizationcode(String authorizationcode) {
        this.authorizationcode.set(authorizationcode);
    }

    @Column(name = "PAYMENT_ID")
    public String getPaymentid() {
        return paymentid.get();
    }

    public StringProperty paymentidProperty() {
        return paymentid;
    }

    public void setPaymentid(String paymentid) {
        this.paymentid.set(paymentid);
    }

    @Column(name = "VALOR")
    public Integer getValor() {
        return valor.get();
    }

    public IntegerProperty valorProperty() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor.set(valor);
    }

    @Column(name = "PARCELAS")
    public Integer getParcelas() {
        return parcelas.get();
    }

    public IntegerProperty parcelasProperty() {
        return parcelas;
    }

    public void setParcelas(Integer parcelas) {
        this.parcelas.set(parcelas);
    }

    @Column(name = "DT_TRANSACAO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDttransacao() {
        return dttransacao.get();
    }

    public ObjectProperty<LocalDateTime> dttransacaoProperty() {
        return dttransacao;
    }

    public void setDttransacao(LocalDateTime dttransacao) {
        this.dttransacao.set(dttransacao);
    }

    @Column(name = "NUMERO_CARTAO")
    public String getNumerocartao() {
        return numerocartao.get();
    }

    public StringProperty numerocartaoProperty() {
        return numerocartao;
    }

    public void setNumerocartao(String numerocartao) {
        this.numerocartao.set(numerocartao);
    }

    @Column(name = "VALIDADE")
    public String getValidade() {
        return validade.get();
    }

    public StringProperty validadeProperty() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade.set(validade);
    }

    @Column(name = "BANDEIRA")
    public String getBandeira() {
        return bandeira.get();
    }

    public StringProperty bandeiraProperty() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira.set(bandeira);
    }

    @Column(name = "TITULAR")
    public String getTitular() {
        return titular.get();
    }

    public StringProperty titularProperty() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular.set(titular);
    }

    @Column(name = "VALOR_CANCELADO")
    public Integer getValorcancelado() {
        return valorcancelado.get();
    }

    public IntegerProperty valorcanceladoProperty() {
        return valorcancelado;
    }

    public void setValorcancelado(Integer valorcancelado) {
        this.valorcancelado.set(valorcancelado);
    }

    @Column(name = "DT_CANCELAMENTO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtcancelamento() {
        return dtcancelamento.get();
    }

    public ObjectProperty<LocalDateTime> dtcancelamentoProperty() {
        return dtcancelamento;
    }

    public void setDtcancelamento(LocalDateTime dtcancelamento) {
        this.dtcancelamento.set(dtcancelamento);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public Entidade getCodcli() {
        return codcli.get();
    }

    public ObjectProperty<Entidade> codcliProperty() {
        return codcli;
    }

    public void setCodcli(Entidade codcli) {
        this.codcli.set(codcli);
    }

    @OneToMany(mappedBy = "id.transacao", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<SdDoctoTransacaoCartao> getDoctos() {
        return doctos;
    }

    public void setDoctos(List<SdDoctoTransacaoCartao> doctos) {
        this.doctos = doctos;
    }
}