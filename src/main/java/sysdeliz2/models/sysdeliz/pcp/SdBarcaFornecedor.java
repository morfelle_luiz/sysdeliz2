package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.BuscaPadrao;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_BARCA_FORNECEDOR_001")
@TelaSysDeliz(descricao = "Barcas Fornecedor", icon = "lavanderia_100.png")
public class SdBarcaFornecedor implements Serializable {

    @Transient
    private final ObjectProperty<SdBarcaFornecedorPK> id = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Fornecedor", width = 300)
    private final ObjectProperty<Entidade> fornecedor = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Máquina", width = 180)
    @BuscaPadrao(descricao = "Máquina")
    private final StringProperty maquina = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Min.", width = 80)
    private final IntegerProperty minimo = new SimpleIntegerProperty(0);
    @Transient
    @ExibeTableView(descricao = "Max.", width = 80)
    private final IntegerProperty maximo = new SimpleIntegerProperty(0);
    @Transient
    @ExibeTableView(descricao = "Custo", width = 40)
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<>(BigDecimal.ZERO);
    @Transient
    @ExibeTableView(descricao = "Prazo", width = 60)
    private final IntegerProperty prazo = new SimpleIntegerProperty(0);
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 50, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    @Transient
    private final StringProperty obs = new SimpleStringProperty();

    public SdBarcaFornecedor() {
        this.id.set(new SdBarcaFornecedorPK());
    }

    public SdBarcaFornecedor(Entidade fornecedor, String barca, Integer minimo, Integer maximo, BigDecimal custo, Integer prazo, Boolean ativo) {
        this.id.set(new SdBarcaFornecedorPK(fornecedor, barca));
        this.minimo.set(minimo);
        this.maximo.set(maximo);
        this.custo.set(custo);
        this.prazo.set(prazo);
        this.ativo.set(ativo);
    }

    @EmbeddedId
    public SdBarcaFornecedorPK getId() {
        return id.get();
    }

    public ObjectProperty<SdBarcaFornecedorPK> idProperty() {
        return id;
    }

    public void setId(SdBarcaFornecedorPK id) {
        this.id.set(id);
    }

    @Column(name = "MINIMO")
    public int getMinimo() {
        return minimo.get();
    }

    public IntegerProperty minimoProperty() {
        return minimo;
    }

    public void setMinimo(int minimo) {
        this.minimo.set(minimo);
    }

    @Column(name = "MAXIMO")
    public int getMaximo() {
        return maximo.get();
    }

    public IntegerProperty maximoProperty() {
        return maximo;
    }

    public void setMaximo(int maximo) {
        this.maximo.set(maximo);
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Column(name = "PRAZO")
    public int getPrazo() {
        return prazo.get();
    }

    public IntegerProperty prazoProperty() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo.set(prazo);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo.set(ativo);
    }

    @Lob
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Transient
    public Entidade getFornecedor() {
        return fornecedor.get();
    }

    public ObjectProperty<Entidade> fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(Entidade fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    @Transient
    public String getMaquina() {
        return maquina.get();
    }

    public StringProperty maquinaProperty() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina.set(maquina);
    }

    @PostLoad
    private void postLoad() {
        setMaquina(id.get().getMaquina());
        setFornecedor(id.get().getFornecedor());
    }
}