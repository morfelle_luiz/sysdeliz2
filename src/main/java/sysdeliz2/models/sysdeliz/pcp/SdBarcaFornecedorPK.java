package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.CodcliAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SdBarcaFornecedorPK implements Serializable {

    private final ObjectProperty<Entidade> fornecedor = new SimpleObjectProperty<>();
    private final StringProperty maquina = new SimpleStringProperty();

    public SdBarcaFornecedorPK() {
    }

    public SdBarcaFornecedorPK(Entidade fornecedor, String maquina) {
        this.fornecedor.set(fornecedor);
        this.maquina.set(maquina);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORNECEDOR")
    @Convert(converter = CodcliAttributeConverter.class)
    public Entidade getFornecedor() {
        return fornecedor.get();
    }

    public ObjectProperty<Entidade> fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(Entidade fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    @Column(name = "MAQUINA")
    public String getMaquina() {
        return maquina.get();
    }

    public StringProperty maquinaProperty() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina.set(maquina);
    }
}