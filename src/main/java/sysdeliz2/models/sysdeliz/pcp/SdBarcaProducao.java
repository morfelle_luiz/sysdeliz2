package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_BARCA_PRODUCAO_001")
public class SdBarcaProducao implements Serializable {

    private final ObjectProperty<SdProgramacaoBarca> programacao = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<SdBarcaFornecedor> maquina = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtenvio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtretorno = new SimpleObjectProperty<>();
    private List<SdItensBarca> itens = new ArrayList<>();

    private final IntegerProperty andar = new SimpleIntegerProperty();

    public SdBarcaProducao() {
    }

    public SdBarcaProducao(SdBarcaFornecedor maquina, Integer ordem, BigDecimal qtde) {
        this.maquina.set(maquina);
        this.ordem.set(ordem);
        this.qtde.set(qtde);
    }

    public SdBarcaProducao(SdProgramacaoBarca programacao, SdBarcaFornecedor maquina, Integer ordem,
                           BigDecimal qtde, String status) {
        this.programacao.set(programacao);
        this.maquina.set(maquina);
        this.ordem.set(ordem);
        this.qtde.set(qtde);
        this.status.set(status);
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "PROGRAMACAO")
    public SdProgramacaoBarca getProgramacao() {
        return programacao.get();
    }

    public ObjectProperty<SdProgramacaoBarca> programacaoProperty() {
        return programacao;
    }

    public void setProgramacao(SdProgramacaoBarca programacao) {
        this.programacao.set(programacao);
    }

    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "FORNECEDOR"),
            @JoinColumn(name = "MAQUINA")
    })
    public SdBarcaFornecedor getMaquina() {
        return maquina.get();
    }

    public ObjectProperty<SdBarcaFornecedor> maquinaProperty() {
        return maquina;
    }

    public void setMaquina(SdBarcaFornecedor maquina) {
        this.maquina.set(maquina);
    }

    @Id
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "DT_ENVIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtenvio() {
        return dtenvio.get();
    }

    public ObjectProperty<LocalDate> dtenvioProperty() {
        return dtenvio;
    }

    public void setDtenvio(LocalDate dtenvio) {
        this.dtenvio.set(dtenvio);
    }

    @Column(name = "DT_RETORNO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtretorno() {
        return dtretorno.get();
    }

    public ObjectProperty<LocalDate> dtretornoProperty() {
        return dtretorno;
    }

    public void setDtretorno(LocalDate dtretorno) {
        this.dtretorno.set(dtretorno);
    }

    @OneToMany(mappedBy = "barca", cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumns({
//            @JoinColumn(name = "PROGRAMACAO"),
//            @JoinColumn(name = "ORDEM")
//    })
    public List<SdItensBarca> getItens() {
        return itens;
    }

    public void setItens(List<SdItensBarca> itens) {
        this.itens = itens;
    }

    @Transient
    public int getAndar() {
        return andar.get();
    }

    public IntegerProperty andarProperty() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar.set(andar);
    }
}
