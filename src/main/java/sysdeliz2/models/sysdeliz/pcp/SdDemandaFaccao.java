package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Ano;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sd_demanda_faccao_001")
public class SdDemandaFaccao implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final ObjectProperty<Ano> dataDemanda = new SimpleObjectProperty<>();

        public Pk() {
        }

        @OneToOne
        @JoinColumn(name = "faccao")
        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }

        @OneToOne
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @OneToOne
        @JoinColumn(name = "data_demanda")
        public Ano getDataDemanda() {
            return dataDemanda.get();
        }

        public ObjectProperty<Ano> dataDemandaProperty() {
            return dataDemanda;
        }

        public void setDataDemanda(Ano dataDemanda) {
            this.dataDemanda.set(dataDemanda);
        }
    }

    public SdDemandaFaccao() {
    }

    // ------------------- JPA ATTRIBUTES --------------------------------

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final BooleanProperty setorAtual = new SimpleBooleanProperty();
    private final BooleanProperty emProducao = new SimpleBooleanProperty();
    private final BooleanProperty atrasado = new SimpleBooleanProperty();
    private final IntegerProperty diasAtraso = new SimpleIntegerProperty();
    private final IntegerProperty qtdePecas = new SimpleIntegerProperty();
    private final IntegerProperty leadTotal = new SimpleIntegerProperty();

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "setor_atual")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSetorAtual() {
        return setorAtual.get();
    }

    public BooleanProperty setorAtualProperty() {
        return setorAtual;
    }

    public void setSetorAtual(boolean setorAtual) {
        this.setorAtual.set(setorAtual);
    }

    @Column(name = "em_producao")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEmProducao() {
        return emProducao.get();
    }

    public BooleanProperty emProducaoProperty() {
        return emProducao;
    }

    public void setEmProducao(boolean emProducao) {
        this.emProducao.set(emProducao);
    }

    @Column(name = "atrasado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtrasado() {
        return atrasado.get();
    }

    public BooleanProperty atrasadoProperty() {
        return atrasado;
    }

    public void setAtrasado(boolean atrasado) {
        this.atrasado.set(atrasado);
    }

    @Column(name = "dd_atraso")
    public int getDiasAtraso() {
        return diasAtraso.get();
    }

    public IntegerProperty diasAtrasoProperty() {
        return diasAtraso;
    }

    public void setDiasAtraso(int diasAtraso) {
        this.diasAtraso.set(diasAtraso);
    }

    @Column(name = "qtde")
    public int getQtdePecas() {
        return qtdePecas.get();
    }

    public IntegerProperty qtdePecasProperty() {
        return qtdePecas;
    }

    public void setQtdePecas(int qtdePecas) {
        this.qtdePecas.set(qtdePecas);
    }

    @Column(name = "lead_total")
    public int getLeadTotal() {
        return leadTotal.get();
    }

    public IntegerProperty leadTotalProperty() {
        return leadTotal;
    }

    public void setLeadTotal(int leadTotal) {
        this.leadTotal.set(leadTotal);
    }

    // ------------------- TRANSIENTS ATTRIBUTES --------------------------------

    private final ObjectProperty<BigDecimal> capacidade = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> ocupacao = new SimpleObjectProperty<>();
    private List<SdFluxoAtualOf> ofsDia = new ArrayList<>();

    @Transient
    public BigDecimal getCapacidade() {
        return capacidade.get();
    }

    public ObjectProperty<BigDecimal> capacidadeProperty() {
        return capacidade;
    }

    public void setCapacidade(BigDecimal capacidade) {
        this.capacidade.set(capacidade);
    }

    @Transient
    public BigDecimal getOcupacao() {
        return ocupacao.get();
    }

    public ObjectProperty<BigDecimal> ocupacaoProperty() {
        return ocupacao;
    }

    public void setOcupacao(BigDecimal ocupacao) {
        this.ocupacao.set(ocupacao);
    }

    @Transient
    public List<SdFluxoAtualOf> getOfsDia() {
        return ofsDia;
    }

    public void setOfsDia(List<SdFluxoAtualOf> ofsDia) {
        this.ofsDia = ofsDia;
    }
}
