package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "sd_demanda_of_001")
public class SdDemandaOf implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<LocalDate> dtDemanda = new SimpleObjectProperty<>();
        private final StringProperty numero = new SimpleStringProperty();
        private final StringProperty faccao = new SimpleStringProperty();
        private final StringProperty setor = new SimpleStringProperty();

        public Pk() {
        }

        public Pk(LocalDate dtDemanda, String numero, String faccao, String setor) {
            this.dtDemanda.set(dtDemanda);
            this.numero.set(numero);
            this.faccao.set(faccao);
            this.setor.set(setor);
        }

        @Column(name = "dt_demanda")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getDtDemanda() {
            return dtDemanda.get();
        }

        public ObjectProperty<LocalDate> dtDemandaProperty() {
            return dtDemanda;
        }

        public void setDtDemanda(LocalDate dtDemanda) {
            this.dtDemanda.set(dtDemanda);
        }

        @Column(name = "numero")
        public String getNumero() {
            return numero.get();
        }

        public StringProperty numeroProperty() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero.set(numero);
        }

        @Column(name = "faccao")
        public String getFaccao() {
            return faccao.get();
        }

        public StringProperty faccaoProperty() {
            return faccao;
        }

        public void setFaccao(String faccao) {
            this.faccao.set(faccao);
        }

        @Column(name = "setor")
        public String getSetor() {
            return setor.get();
        }

        public StringProperty setorProperty() {
            return setor;
        }

        public void setSetor(String setor) {
            this.setor.set(setor);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<>();

    public SdDemandaOf() {
    }

    public SdDemandaOf(LocalDate dtDemanda, String numero, String faccao, String setor, BigDecimal qtde, BigDecimal tempo) {
        this.id.set(new Pk(dtDemanda, numero, faccao, setor));
        this.qtde.set(qtde);
        this.tempo.set(tempo);
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "qtde")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "tempo")
    public BigDecimal getTempo() {
        return tempo.get();
    }

    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }

    public void setTempo(BigDecimal tempo) {
        this.tempo.set(tempo);
    }
}
