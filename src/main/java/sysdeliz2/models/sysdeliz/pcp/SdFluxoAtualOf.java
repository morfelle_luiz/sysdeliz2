package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "sd_fluxo_atual_of_001")
public class SdFluxoAtualOf implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<Of1> numero = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final BooleanProperty setorAtual = new SimpleBooleanProperty();
        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();
        private final ObjectProperty<LocalDate> dtEnvio = new SimpleObjectProperty<>();
        private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>();
        private final ObjectProperty<LocalDate> dtRetorno = new SimpleObjectProperty<>();

        public Pk() {
        }

        @OneToOne
        @JoinColumn(name = "numero")
        public Of1 getNumero() {
            return numero.get();
        }

        public ObjectProperty<Of1> numeroProperty() {
            return numero;
        }

        public void setNumero(Of1 numero) {
            this.numero.set(numero);
        }

        @OneToOne
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @Column(name = "setor_atual")
        @Convert(converter = BooleanAttributeConverter.class)
        public boolean isSetorAtual() {
            return setorAtual.get();
        }

        public BooleanProperty setorAtualProperty() {
            return setorAtual;
        }

        public void setSetorAtual(boolean setorAtual) {
            this.setorAtual.set(setorAtual);
        }

        @OneToOne
        @JoinColumn(name = "faccao")
        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }

        @Column(name = "dt_envio")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getDtEnvio() {
            return dtEnvio.get();
        }

        public ObjectProperty<LocalDate> dtEnvioProperty() {
            return dtEnvio;
        }

        public void setDtEnvio(LocalDate dtEnvio) {
            this.dtEnvio.set(dtEnvio);
        }

        @Column(name = "dt_inicio")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getDtInicio() {
            return dtInicio.get();
        }

        public ObjectProperty<LocalDate> dtInicioProperty() {
            return dtInicio;
        }

        public void setDtInicio(LocalDate dtInicio) {
            this.dtInicio.set(dtInicio);
        }

        @Column(name = "dt_retorno_prog")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getDtRetorno() {
            return dtRetorno.get();
        }

        public ObjectProperty<LocalDate> dtRetornoProperty() {
            return dtRetorno;
        }

        public void setDtRetorno(LocalDate dtRetorno) {
            this.dtRetorno.set(dtRetorno);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final BooleanProperty emProducao = new SimpleBooleanProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty leadSetor = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtRetornoProgamado = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeOf = new SimpleIntegerProperty();
    private final BooleanProperty atrasado = new SimpleBooleanProperty();
    private final StringProperty tipoOf = new SimpleStringProperty();
    private final IntegerProperty diasAtraso = new SimpleIntegerProperty();
    private final ObjectProperty<CadFluxo> setorEmProducao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRetornoProducao = new SimpleObjectProperty<>();
    private final IntegerProperty proximoSetor = new SimpleIntegerProperty();
    private final ObjectProperty<SdColaborador> controlador = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoOf = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoDia = new SimpleObjectProperty<>();
    private final IntegerProperty venda = new SimpleIntegerProperty();
    private final StringProperty ordemPeriodo = new SimpleStringProperty();
    private final StringProperty tipoProg = new SimpleStringProperty();
    private final BooleanProperty finalizado = new SimpleBooleanProperty();
    private final BooleanProperty prioridade = new SimpleBooleanProperty();
    private final BooleanProperty comProgramacaoRetorno = new SimpleBooleanProperty();

    public SdFluxoAtualOf() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "em_producao")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEmProducao() {
        return emProducao.get();
    }

    public BooleanProperty emProducaoProperty() {
        return emProducao;
    }

    public void setEmProducao(boolean emProducao) {
        this.emProducao.set(emProducao);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "lead_setor")
    public int getLeadSetor() {
        return leadSetor.get();
    }

    public IntegerProperty leadSetorProperty() {
        return leadSetor;
    }

    public void setLeadSetor(int leadSetor) {
        this.leadSetor.set(leadSetor);
    }

    @Column(name = "dt_retorno_orig")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRetornoProgamado() {
        return dtRetornoProgamado.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProgamadoProperty() {
        return dtRetornoProgamado;
    }

    public void setDtRetornoProgamado(LocalDate dtRetornoProgamado) {
        this.dtRetornoProgamado.set(dtRetornoProgamado);
    }

    @Column(name = "qtde_of")
    public int getQtdeOf() {
        return qtdeOf.get();
    }

    public IntegerProperty qtdeOfProperty() {
        return qtdeOf;
    }

    public void setQtdeOf(int qtdeOf) {
        this.qtdeOf.set(qtdeOf);
    }

    @Column(name = "atrasado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtrasado() {
        return atrasado.get();
    }

    public BooleanProperty atrasadoProperty() {
        return atrasado;
    }

    public void setAtrasado(boolean atrasado) {
        this.atrasado.set(atrasado);
    }

    @Column(name = "tipo_op")
    public String getTipoOf() {
        return tipoOf.get();
    }

    public StringProperty tipoOfProperty() {
        return tipoOf;
    }

    public void setTipoOf(String tipoOf) {
        this.tipoOf.set(tipoOf);
    }

    @Column(name = "dd_atraso")
    public int getDiasAtraso() {
        return diasAtraso.get();
    }

    public IntegerProperty diasAtrasoProperty() {
        return diasAtraso;
    }

    public void setDiasAtraso(int diasAtraso) {
        this.diasAtraso.set(diasAtraso);
    }

    @OneToOne
    @JoinColumn(name = "setor_prod")
    public CadFluxo getSetorEmProducao() {
        return setorEmProducao.get();
    }

    public ObjectProperty<CadFluxo> setorEmProducaoProperty() {
        return setorEmProducao;
    }

    public void setSetorEmProducao(CadFluxo setorEmProducao) {
        this.setorEmProducao.set(setorEmProducao);
    }

    @Column(name = "dt_retorno_prod")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRetornoProducao() {
        return dtRetornoProducao.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProducaoProperty() {
        return dtRetornoProducao;
    }

    public void setDtRetornoProducao(LocalDate dtRetornoProducao) {
        this.dtRetornoProducao.set(dtRetornoProducao);
    }

    @Column(name = "prox_setor")
    public Integer getProximoSetor() {
        return proximoSetor.get();
    }

    public IntegerProperty proximoSetorProperty() {
        return proximoSetor;
    }

    public void setProximoSetor(Integer proximoSetor) {
        this.proximoSetor.set(proximoSetor);
    }

    @OneToOne
    @JoinColumn(name = "controlador")
    public SdColaborador getControlador() {
        return controlador.get();
    }

    public ObjectProperty<SdColaborador> controladorProperty() {
        return controlador;
    }

    public void setControlador(SdColaborador controlador) {
        this.controlador.set(controlador);
    }

    @Column(name = "tempo_of")
    public BigDecimal getTempoOf() {
        return tempoOf.get();
    }

    public ObjectProperty<BigDecimal> tempoOfProperty() {
        return tempoOf;
    }

    public void setTempoOf(BigDecimal tempoOf) {
        this.tempoOf.set(tempoOf);
    }

    @Column(name = "tempo_dia")
    public BigDecimal getTempoDia() {
        return tempoDia.get();
    }

    public ObjectProperty<BigDecimal> tempoDiaProperty() {
        return tempoDia;
    }

    public void setTempoDia(BigDecimal tempoDia) {
        this.tempoDia.set(tempoDia);
    }

    @Column(name = "venda")
    public int getVenda() {
        return venda.get();
    }

    public IntegerProperty vendaProperty() {
        return venda;
    }

    public void setVenda(int venda) {
        this.venda.set(venda);
    }

    @Column(name = "ordem_Periodo")
    public String getOrdemPeriodo() {
        return ordemPeriodo.get();
    }

    public StringProperty ordemPeriodoProperty() {
        return ordemPeriodo;
    }

    public void setOrdemPeriodo(String ordemPeriodo) {
        this.ordemPeriodo.set(ordemPeriodo);
    }

    @Column(name = "tipo_prog")
    public String getTipoProg() {
        return tipoProg.get();
    }

    public StringProperty tipoProgProperty() {
        return tipoProg;
    }

    public void setTipoProg(String tipoProg) {
        this.tipoProg.set(tipoProg);
    }

    @Column(name = "finalizado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFinalizado() {
        return finalizado.get();
    }

    public BooleanProperty finalizadoProperty() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado.set(finalizado);
    }

    @Column(name = "prioridade")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPrioridade() {
        return prioridade.get();
    }

    public BooleanProperty prioridadeProperty() {
        return prioridade;
    }

    public void setPrioridade(boolean prioridade) {
        this.prioridade.set(prioridade);
    }

    @Column(name = "com_prog_retorno")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isComProgramacaoRetorno() {
        return comProgramacaoRetorno.get();
    }

    public BooleanProperty comProgramacaoRetornoProperty() {
        return comProgramacaoRetorno;
    }

    public void setComProgramacaoRetorno(boolean comProgramacaoRetorno) {
        this.comProgramacaoRetorno.set(comProgramacaoRetorno);
    }

    @PostLoad
    private void postLoad() {
        this.setDtInicioProg(this.getId().getDtInicio());
        this.setDtRetornoProg(this.getId().getDtRetorno());
        this.setMarcadoFinalizado(this.isFinalizado());
    }

    //-------------------------- TRANSIENTE ---------------------------
    private final ObjectProperty<BigDecimal> saldoTempoOf = new SimpleObjectProperty<>();
    private List<SdDemandaOf> ocupacaoOf = new ArrayList<>();
    private final ObjectProperty<LocalDate> dtInicioProg = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRetornoProg = new SimpleObjectProperty<>();
    private final BooleanProperty marcadoFinalizado = new SimpleBooleanProperty();

    @Transient
    public BigDecimal getSaldoTempoOf() {
        return saldoTempoOf.get();
    }

    public ObjectProperty<BigDecimal> saldoTempoOfProperty() {
        return saldoTempoOf;
    }

    public void setSaldoTempoOf(BigDecimal saldoTempoOf) {
        this.saldoTempoOf.set(saldoTempoOf);
    }

    @Transient
    public List<SdDemandaOf> getOcupacaoOf() {
        return ocupacaoOf;
    }

    public void setOcupacaoOf(List<SdDemandaOf> ocupacaoOf) {
        this.ocupacaoOf = ocupacaoOf;
    }

    @Transient
    public LocalDate getDtInicioProg() {
        return dtInicioProg.get();
    }

    public ObjectProperty<LocalDate> dtInicioProgProperty() {
        return dtInicioProg;
    }

    public void setDtInicioProg(LocalDate dtInicioProg) {
        this.dtInicioProg.set(dtInicioProg);
    }

    @Transient
    public LocalDate getDtRetornoProg() {
        return dtRetornoProg.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProgProperty() {
        return dtRetornoProg;
    }

    public void setDtRetornoProg(LocalDate dtRetornoProg) {
        this.dtRetornoProg.set(dtRetornoProg);
    }

    @Transient
    public boolean isMarcadoFinalizado() {
        return marcadoFinalizado.get();
    }

    public BooleanProperty marcadoFinalizadoProperty() {
        return marcadoFinalizado;
    }

    public void setMarcadoFinalizado(boolean marcadoFinalizado) {
        this.marcadoFinalizado.set(marcadoFinalizado);
    }
}
