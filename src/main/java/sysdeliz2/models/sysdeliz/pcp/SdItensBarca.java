package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "SD_ITENS_BARCA_001")
public class SdItensBarca implements Serializable {

    private final ObjectProperty<SdBarcaProducao> barca = new SimpleObjectProperty<>();
    private final StringProperty numeroof = new SimpleStringProperty();
    private final StringProperty corProduto = new SimpleStringProperty();
    private final StringProperty loteof = new SimpleStringProperty();
    private final StringProperty insumo = new SimpleStringProperty();
    private final StringProperty insumocru = new SimpleStringProperty();
    private final StringProperty corfornecedor = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> custofornecedor = new SimpleObjectProperty<>();
    private final StringProperty corinsumo = new SimpleStringProperty();
    private final StringProperty corinsumocru = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final BooleanProperty principal = new SimpleBooleanProperty();
    private final BooleanProperty contrastante = new SimpleBooleanProperty();
    private final StringProperty combinado = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdeEstoque = new SimpleObjectProperty<>();
    private final StringProperty firmadoEm = new SimpleStringProperty();

    public SdItensBarca() {
    }

    public SdItensBarca(SdBarcaProducao barca, String numeroof, String corProduto, String loteof,
                        String corfornecedor, BigDecimal custofornecedor, String insumo,
                        String insumocru, String corinsumo, String corinsumocru, String deposito,
                        BigDecimal qtde, BigDecimal qtdeEstoque, Boolean principal, Boolean contrastante,
                        String combinado) {
        this.barca.set(barca);
        this.numeroof.set(numeroof);
        this.corProduto.set(corProduto);
        this.loteof.set(loteof);
        this.corfornecedor.set(corfornecedor);
        this.custofornecedor.set(custofornecedor);
        this.insumo.set(insumo);
        this.insumocru.set(insumocru);
        this.corinsumo.set(corinsumo);
        this.corinsumocru.set(corinsumocru);
        this.deposito.set(deposito);
        this.qtde.set(qtde);
        this.qtdeEstoque.set(qtdeEstoque);
        this.principal.set(principal);
        this.contrastante.set(contrastante);
        this.combinado.set(combinado);
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "PROGRAMACAO"),
            @JoinColumn(name = "ORDEM")
    })
    public SdBarcaProducao getBarca() {
        return barca.get();
    }

    public ObjectProperty<SdBarcaProducao> barcaProperty() {
        return barca;
    }

    public void setBarca(SdBarcaProducao barca) {
        this.barca.set(barca);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumeroof() {
        return numeroof.get();
    }

    public StringProperty numeroofProperty() {
        return numeroof;
    }

    public void setNumeroof(String numeroof) {
        this.numeroof.set(numeroof);
    }

    @Id
    @Column(name = "COR_PRODUTO")
    public String getCorProduto() {
        return corProduto.get();
    }

    public StringProperty corProdutoProperty() {
        return corProduto;
    }

    public void setCorProduto(String corProduto) {
        this.corProduto.set(corProduto);
    }

    @Column(name = "LOTE")
    public String getLoteof() {
        return loteof.get();
    }

    public StringProperty loteofProperty() {
        return loteof;
    }

    public void setLoteof(String loteof) {
        this.loteof.set(loteof);
    }

    @Id
    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }

    public StringProperty insumoProperty() {
        return insumo;
    }

    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }

    @Column(name = "INSUMO_CRU")
    public String getInsumocru() {
        return insumocru.get();
    }

    public StringProperty insumocruProperty() {
        return insumocru;
    }

    public void setInsumocru(String insumocru) {
        this.insumocru.set(insumocru);
    }

    @Column(name = "COR_INSUMO")
    public String getCorinsumo() {
        return corinsumo.get();
    }

    public StringProperty corinsumoProperty() {
        return corinsumo;
    }

    public void setCorinsumo(String corinsumo) {
        this.corinsumo.set(corinsumo);
    }

    @Column(name = "COR_INSUMO_CRU")
    public String getCorinsumocru() {
        return corinsumocru.get();
    }

    public StringProperty corinsumocruProperty() {
        return corinsumocru;
    }

    public void setCorinsumocru(String corinsumocru) {
        this.corinsumocru.set(corinsumocru);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "PRINCIPAL")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isPrincipal() {
        return principal.get();
    }

    public BooleanProperty principalProperty() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal.set(principal);
    }

    @Column(name = "CONTRASTANTE")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isContrastante() {
        return contrastante.get();
    }

    public BooleanProperty contrastanteProperty() {
        return contrastante;
    }

    public void setContrastante(Boolean contrastante) {
        this.contrastante.set(contrastante);
    }

    @Column(name = "COMBINADO")
    public String getCombinado() {
        return combinado.get();
    }

    public StringProperty combinadoProperty() {
        return combinado;
    }

    public void setCombinado(String combinado) {
        this.combinado.set(combinado);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "COR_FORNECEDOR")
    public String getCorfornecedor() {
        return corfornecedor.get();
    }

    public StringProperty corfornecedorProperty() {
        return corfornecedor;
    }

    public void setCorfornecedor(String corfornecedor) {
        this.corfornecedor.set(corfornecedor);
    }

    @Column(name = "CUSTO_FORNECEDOR")
    public BigDecimal getCustofornecedor() {
        return custofornecedor.get();
    }

    public ObjectProperty<BigDecimal> custofornecedorProperty() {
        return custofornecedor;
    }

    public void setCustofornecedor(BigDecimal custofornecedor) {
        this.custofornecedor.set(custofornecedor);
    }

    @Column(name = "QTDE_ESTQ")
    public BigDecimal getQtdeEstoque() {
        return qtdeEstoque.get();
    }

    public ObjectProperty<BigDecimal> qtdeEstoqueProperty() {
        return qtdeEstoque;
    }

    public void setQtdeEstoque(BigDecimal qtdeEstoque) {
        this.qtdeEstoque.set(qtdeEstoque);
    }

    @Column(name = "FIRMADO_EM")
    public String getFirmadoEm() {
        return firmadoEm.get();
    }

    public StringProperty firmadoEmProperty() {
        return firmadoEm;
    }

    public void setFirmadoEm(String firmadoEm) {
        this.firmadoEm.set(firmadoEm);
    }
}
