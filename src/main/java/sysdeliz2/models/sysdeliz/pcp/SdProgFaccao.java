package sysdeliz2.models.sysdeliz.pcp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javafx.beans.property.*;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeDeserializer;
import sysdeliz2.utils.converters.LocalDateAttributeSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "sd_prog_faccao_001")
public class SdProgFaccao implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final StringProperty numero = new SimpleStringProperty();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();

        public Pk() {
        }

        public Pk(VSdDadosEntidade faccao, CadFluxo setor, String numero) {
            this.faccao.set(faccao);
            this.setor.set(setor);
            this.numero.set(numero);
        }

        @Column(name = "numero")
        public String getNumero() {
            return numero.get();
        }

        public StringProperty numeroProperty() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero.set(numero);
        }

        @OneToOne
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @OneToOne
        @JoinColumn(name = "faccao")
        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtInicioProd = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRetornoProg = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtProgramacaoOriginal = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final BooleanProperty finalizado = new SimpleBooleanProperty();
    private final BooleanProperty prioridade = new SimpleBooleanProperty();

    public SdProgFaccao() {
    }

    public SdProgFaccao(VSdDadosEntidade faccao, CadFluxo setor, String numero) {
        id.set(new Pk(faccao, setor, numero));
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "dt_inicio_prod")
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonDeserialize(using = LocalDateAttributeDeserializer.class)
    @JsonSerialize(using = LocalDateAttributeSerializer.class)
    public LocalDate getDtInicioProd() {
        return dtInicioProd.get();
    }

    public ObjectProperty<LocalDate> dtInicioProdProperty() {
        return dtInicioProd;
    }

    public void setDtInicioProd(LocalDate dtInicioProd) {
        this.dtInicioProd.set(dtInicioProd);
    }

    @Column(name = "dt_retorno_prog")
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonDeserialize(using = LocalDateAttributeDeserializer.class)
    @JsonSerialize(using = LocalDateAttributeSerializer.class)
    public LocalDate getDtRetornoProg() {
        return dtRetornoProg.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProgProperty() {
        return dtRetornoProg;
    }

    public void setDtRetornoProg(LocalDate dtRetornoProg) {
        this.dtRetornoProg.set(dtRetornoProg);
    }

    @Column(name = "dt_prog_orig")
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonDeserialize(using = LocalDateAttributeDeserializer.class)
    @JsonSerialize(using = LocalDateAttributeSerializer.class)
    public LocalDate getDtProgramacaoOriginal() {
        return dtProgramacaoOriginal.get();
    }

    public ObjectProperty<LocalDate> dtProgramacaoOriginalProperty() {
        return dtProgramacaoOriginal;
    }

    public void setDtProgramacaoOriginal(LocalDate dtProgramacaoOriginal) {
        this.dtProgramacaoOriginal.set(dtProgramacaoOriginal);
    }

    @Column(name = "tipo")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "finalizado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFinalizado() {
        return finalizado.get();
    }

    public BooleanProperty finalizadoProperty() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado.set(finalizado);
    }

    @Column(name = "prioridade")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPrioridade() {
        return prioridade.get();
    }

    public BooleanProperty prioridadeProperty() {
        return prioridade;
    }

    public void setPrioridade(boolean prioridade) {
        this.prioridade.set(prioridade);
    }

    @Transient
    @JsonIgnore
    public String toLog() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error to string object";
        }
    }
}
