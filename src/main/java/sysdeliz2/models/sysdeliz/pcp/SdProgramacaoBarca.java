package sysdeliz2.models.sysdeliz.pcp;

import javafx.beans.property.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.converters.CodcliAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SD_PROGRAMACAO_BARCA_001")
public class SdProgramacaoBarca implements Serializable {

    private final IntegerProperty programacao = new SimpleIntegerProperty();
    private final IntegerProperty fornecedor = new SimpleIntegerProperty();
    private final StringProperty corbarca = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtprogramacao = new SimpleObjectProperty<>(LocalDate.now());
    private final StringProperty usuario = new SimpleStringProperty(Globals.getUsuarioLogado() == null ? null : Globals.getUsuarioLogado().getUsuario());
    private final StringProperty lote = new SimpleStringProperty();
    private List<SdBarcaProducao> barcas = new ArrayList<>();

    public SdProgramacaoBarca() {
    }

    public SdProgramacaoBarca(Integer fornecedor, String corbarca, String lote) {
        this.fornecedor.set(fornecedor);
        this.corbarca.set(corbarca);
        this.lote.set(lote);
    }

    public SdProgramacaoBarca(Integer programacao, Integer fornecedor, String corbarca) {
        this.programacao.set(programacao);
        this.fornecedor.set(fornecedor);
        this.corbarca.set(corbarca);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_PROGRAMACAO_BARCA")
    @SequenceGenerator(name = "SEQ_SD_PROGRAMACAO_BARCA", sequenceName = "SEQ_SD_PROGRAMACAO_BARCA", allocationSize = 1)
    @Column(name = "PROGRAMACAO")
    public int getProgramacao() {
        return programacao.get();
    }

    public IntegerProperty programacaoProperty() {
        return programacao;
    }

    public void setProgramacao(int programacao) {
        this.programacao.set(programacao);
    }

    @Column(name = "FORNECEDOR")
    @Convert(converter = CodcliAttributeConverter.class)
    public int getFornecedor() {
        return fornecedor.get();
    }

    public IntegerProperty fornecedorProperty() {
        return fornecedor;
    }

    public void setFornecedor(int fornecedor) {
        this.fornecedor.set(fornecedor);
    }

    @Column(name = "COR_BARCA")
    public String getCorbarca() {
        return corbarca.get();
    }

    public StringProperty corbarcaProperty() {
        return corbarca;
    }

    public void setCorbarca(String corbarca) {
        this.corbarca.set(corbarca);
    }

    @Column(name = "DT_PROGRAMACAO")
    public LocalDate getDtprogramacao() {
        return dtprogramacao.get();
    }

    public ObjectProperty<LocalDate> dtprogramacaoProperty() {
        return dtprogramacao;
    }

    public void setDtprogramacao(LocalDate dtprogramacao) {
        this.dtprogramacao.set(dtprogramacao);
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @OneToMany(mappedBy = "programacao", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "PROGRAMACAO")
    public List<SdBarcaProducao> getBarcas() {
        return barcas;
    }

    public void setBarcas(List<SdBarcaProducao> barcas) {
        this.barcas = barcas;
    }
}
