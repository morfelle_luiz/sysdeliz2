package sysdeliz2.models.sysdeliz.pcp.almoxarifado;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SD_PRODUTO_PA_SEQUENCIA_001")
@IdClass(SdProdutoPASequenciaPK.class)
public class SdProdutoPASequencia implements Serializable {

    private final StringProperty sequence = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tamanho = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();

    public SdProdutoPASequencia() {
    }

    public SdProdutoPASequencia(String cor, String tamanho, String codigoProduto) {
        setSequence("0001");
        setCor(cor);
        setTamanho(tamanho);
        setProduto(codigoProduto);
    }

    @Column(name = "SEQUENCE")
    public String getSequence() {
        return sequence.get();
    }

    public StringProperty sequenceProperty() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence.set(sequence);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Id
    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @Id
    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

}
