package sysdeliz2.models.sysdeliz.pcp.almoxarifado;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

public class SdProdutoPASequenciaPK implements Serializable {

    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tamanho = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();

    public SdProdutoPASequenciaPK() {
    }

    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdProdutoPASequenciaPK that = (SdProdutoPASequenciaPK) o;
        return Objects.equals(cor, that.cor) && Objects.equals(tamanho, that.tamanho) && Objects.equals(produto, that.produto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cor, tamanho, produto);
    }
}
