package sysdeliz2.models.sysdeliz.sistema;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "SD_PARAMETROS_001")
public class SdParametros extends BasicModel implements Serializable {

    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<SdGrupoParametros> grupo = new SimpleObjectProperty<>();
    private final StringProperty valor = new SimpleStringProperty();
    private final StringProperty valores = new SimpleStringProperty();
    private List<String> valoresA = new ArrayList<>();

    public SdParametros() {
    }

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO")
    public SdGrupoParametros getGrupo() {
        return grupo.get();
    }

    public ObjectProperty<SdGrupoParametros> grupoProperty() {
        return grupo;
    }

    public void setGrupo(SdGrupoParametros grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "VALOR")
    public String getValor() {
        return valor.get();
    }

    public StringProperty valorProperty() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor.set(valor);
    }

    @Column(name = "VALORES")
    public String getValores() {
        return valores.get();
    }

    public StringProperty valoresProperty() {
        return valores;
    }

    public void setValores(String valores) {
        this.valores.set(valores);
    }


    @Transient
    public List<String> getValoresA() {
        return valoresA;
    }

    public void setValoresA(List<String> valoresA) {
        this.valoresA = valoresA;
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }


    @PostLoad
    private void postLoad() {
        if (getValores() != null)
            setValoresA(Arrays.asList(getValores().split("\\+")));
    }
}
