package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;

import javax.persistence.*;
import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SD_BARRA_ITEM_SOLIC_EXP_001")
@IdClass(SdBarraItemSolicExpId.class)
public class SdBarraItemSolicExp implements Serializable {

    private final ObjectProperty<SdItemSolicitacaoExp> itemSolicitacao = new SimpleObjectProperty<>();
    private final StringProperty barra = new SimpleStringProperty();

    public SdBarraItemSolicExp() {
    }

    public SdBarraItemSolicExp(SdItemSolicitacaoExp itemSolicitacaoExp, String barra) {
        this.itemSolicitacao.set(itemSolicitacaoExp);
        this.barra.set(barra);
    }

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    @JoinColumns({
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR"),
            @JoinColumn(name = "TAM", referencedColumnName = "TAM"),
            @JoinColumn(name = "SOLICITACAO", referencedColumnName = "SOLICITACAO")
    })
    public SdItemSolicitacaoExp getItemSolicitacao() {
        return itemSolicitacao.get();
    }

    public ObjectProperty<SdItemSolicitacaoExp> itemSolicitacaoProperty() {
        return itemSolicitacao;
    }

    public void setItemSolicitacao(SdItemSolicitacaoExp itemSolicitacao) {
        this.itemSolicitacao.set(itemSolicitacao);
    }

    @Id
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdBarraItemSolicExp)) return false;
        SdBarraItemSolicExp that = (SdBarraItemSolicExp) o;
        return Objects.equals(getItemSolicitacao(), that.getItemSolicitacao()) && Objects.equals(getBarra(), that.getBarra());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemSolicitacao(), getBarra());
    }
}
