package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class SdBarraItemSolicExpId implements Serializable {

    private final ObjectProperty<SdItemSolicitacaoExp> itemSolicitacao = new SimpleObjectProperty<>();
    private final StringProperty barra = new SimpleStringProperty();

    public SdBarraItemSolicExpId() {
    }

    public SdBarraItemSolicExpId(SdItemSolicitacaoExp itemSolicitacaoExp, String barra) {
        this.itemSolicitacao.set(itemSolicitacaoExp);
        this.barra.set(barra);
    }

    public SdItemSolicitacaoExp getItemSolicitacao() {
        return itemSolicitacao.get();
    }

    public ObjectProperty<SdItemSolicitacaoExp> itemSolicitacaoProperty() {
        return itemSolicitacao;
    }

    public void setItemSolicitacao(SdItemSolicitacaoExp itemSolicitacao) {
        this.itemSolicitacao.set(itemSolicitacao);
    }

    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdBarraItemSolicExpId)) return false;
        SdBarraItemSolicExpId that = (SdBarraItemSolicExpId) o;
        return Objects.equals(getItemSolicitacao(), that.getItemSolicitacao()) && Objects.equals(getBarra(), that.getBarra());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemSolicitacao(), getBarra());
    }
}
