package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.*;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_ITEM_SOLICITACAO_EXP_001")
@IdClass(SdItemSolicitacaoExpId.class)
public class SdItemSolicitacaoExp implements Serializable {

    private final ObjectProperty<SdSolicitacaoExp> solicitacao = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProdutoCorTam> produto = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private List<SdBarraItemSolicExp> barras = new ArrayList<>();

    public SdItemSolicitacaoExp() {
    }

    public SdItemSolicitacaoExp(VSdDadosProdutoCorTam item, int qtde, SdSolicitacaoExp solicitacaoEscolhida) {
        this.solicitacao.set(solicitacaoEscolhida);
        this.produto.set(item);
        this.qtde.set(qtde);
    }

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SOLICITACAO")
    public SdSolicitacaoExp getSolicitacao() {
        return solicitacao.get();
    }

    public ObjectProperty<SdSolicitacaoExp> solicitacaoProperty() {
        return solicitacao;
    }

    public void setSolicitacao(SdSolicitacaoExp solicitacao) {
        this.solicitacao.set(solicitacao);
    }

    @Id
    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR"),
            @JoinColumn(name = "TAM", referencedColumnName = "TAM"),
    })
    public VSdDadosProdutoCorTam getProduto() {
        return produto.get();
    }

    public ObjectProperty<VSdDadosProdutoCorTam> produtoProperty() {
        return produto;
    }

    public void setProduto(VSdDadosProdutoCorTam produto) {
        this.produto.set(produto);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "itemSolicitacao")
    public List<SdBarraItemSolicExp> getBarras() {
        return barras;
    }

    public void setBarras(List<SdBarraItemSolicExp> barras) {
        this.barras = barras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdItemSolicitacaoExp)) return false;
        SdItemSolicitacaoExp that = (SdItemSolicitacaoExp) o;
        return Objects.equals(getSolicitacao(), that.getSolicitacao()) && Objects.equals(getProduto(), that.getProduto()) && Objects.equals(getQtde(), that.getQtde());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSolicitacao(), getProduto(), getQtde());
    }
}
