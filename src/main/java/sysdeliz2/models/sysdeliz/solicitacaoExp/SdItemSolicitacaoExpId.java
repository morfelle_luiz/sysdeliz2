package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SdItemSolicitacaoExpId implements Serializable {

    private final ObjectProperty<SdSolicitacaoExp> solicitacao = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProdutoCorTam> produto = new SimpleObjectProperty<>();

    public SdItemSolicitacaoExpId() {
    }

    public SdSolicitacaoExp getSolicitacao() {
        return solicitacao.get();
    }

    public ObjectProperty<SdSolicitacaoExp> solicitacaoProperty() {
        return solicitacao;
    }

    public void setSolicitacao(SdSolicitacaoExp solicitacao) {
        this.solicitacao.set(solicitacao);
    }

    public VSdDadosProdutoCorTam getProduto() {
        return produto.get();
    }

    public ObjectProperty<VSdDadosProdutoCorTam> produtoProperty() {
        return produto;
    }

    public void setProduto(VSdDadosProdutoCorTam produto) {
        this.produto.set(produto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdItemSolicitacaoExpId)) return false;
        SdItemSolicitacaoExpId that = (SdItemSolicitacaoExpId) o;
        return Objects.equals(getSolicitacao(), that.getSolicitacao()) && Objects.equals(getProduto(), that.getProduto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSolicitacao(), getProduto());
    }
}
