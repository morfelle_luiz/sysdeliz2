package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SD_SOLICITACAO_EXP_001")
@TelaSysDeliz(descricao = "Solicitação de Peças", icon = "expedicao_50.png")
public class SdSolicitacaoExp extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "id")
    private final StringProperty id = new SimpleStringProperty();
    private final ObjectProperty<SdTipoSolicitacaoExp> tipo = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Dt Solicitação", width = 150)
    @ColunaFilter(descricao = "Dt Solicitação", coluna = "dtSolicitacao")
    private final ObjectProperty<LocalDate> dtSolicitacao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtEnvioProduto = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtDevolucao = new SimpleObjectProperty<LocalDate>();
    @ExibeTableView(descricao = "Status", width = 220)
    @ColunaFilter(descricao = "Status", coluna = "status.status")
    private final ObjectProperty<SdStatusSolicitacaoExp> status = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Usuário", width = 150)
    @ColunaFilter(descricao = "Usuário", coluna = "usuario")
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private List<SdItemSolicitacaoExp> itens = new ArrayList<>();


    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_SOLICITACAO_EXP")
//    @SequenceGenerator(sequenceName = "SEQ_SD_SOLICITACAO_EXP", name = "SEQ_SD_SOLICITACAO_EXP", initialValue = 1, allocationSize = 1)
    @Column(name = "ID")
    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    @OneToOne
    @JoinColumn(name = "TIPO")
    public SdTipoSolicitacaoExp getTipo() {
        return tipo.get();
    }

    public ObjectProperty<SdTipoSolicitacaoExp> tipoProperty() {
        return tipo;
    }

    public void setTipo(SdTipoSolicitacaoExp tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "DT_SOLICITACAO")
    public LocalDate getDtSolicitacao() {
        return dtSolicitacao.get();
    }

    public ObjectProperty<LocalDate> dtSolicitacaoProperty() {
        return dtSolicitacao;
    }

    public void setDtSolicitacao(LocalDate dtSolicitacao) {
        this.dtSolicitacao.set(dtSolicitacao);
    }

    @Column(name = "DT_ENVIO_PRODUTO")
    public LocalDate getDtEnvioProduto() {
        return dtEnvioProduto.get();
    }

    public ObjectProperty<LocalDate> dtEnvioProdutoProperty() {
        return dtEnvioProduto;
    }

    public void setDtEnvioProduto(LocalDate dtEnvioProduto) {
        this.dtEnvioProduto.set(dtEnvioProduto);
    }

    @Column(name = "DT_DEVOLUCAO")
    public LocalDate getDtDevolucao() {
        return dtDevolucao.get();
    }

    public ObjectProperty<LocalDate> dtDevolucaoProperty() {
        return dtDevolucao;
    }

    public void setDtDevolucao(LocalDate dtDevolucao) {
        this.dtDevolucao.set(dtDevolucao);
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusSolicitacaoExp getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusSolicitacaoExp> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusSolicitacaoExp status) {
        this.status.set(status);
    }

    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    @Lob
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "solicitacao")
    public List<SdItemSolicitacaoExp> getItens() {
        return itens;
    }

    public void setItens(List<SdItemSolicitacaoExp> itens) {
        this.itens = itens;
    }

    @PostLoad
    private void criaDescricaoFitler() {
        setDescricaoFilter(getId() + " - " + getTipo().getTipo() + " - " + getStatus().getStatus());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdSolicitacaoExp)) return false;
        SdSolicitacaoExp that = (SdSolicitacaoExp) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
