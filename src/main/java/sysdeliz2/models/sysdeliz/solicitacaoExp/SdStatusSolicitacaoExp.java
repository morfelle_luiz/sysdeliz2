package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "SD_STATUS_SOLICITACAO_EXP_001")
@TelaSysDeliz(descricao = "Status Solicitação", icon = "pedido_100.png")
public class SdStatusSolicitacaoExp extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Status", width = 300)
    @ColunaFilter(descricao = "Status", coluna = "status")
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty style = new SimpleStringProperty();

    @Id
    @Column(name = "CODIGO")
    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        setDescricaoFilter(status);
        this.status.set(status);
    }

    @Column(name = "STYLE")
    public String getStyle() {
        return style.get();
    }

    public StringProperty styleProperty() {
        return style;
    }

    public void setStyle(String style) {
        this.style.set(style);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SdStatusSolicitacaoExp)) return false;
        SdStatusSolicitacaoExp that = (SdStatusSolicitacaoExp) o;
        return Objects.equals(getCodigo(), that.getCodigo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }

    @Override
    public String toString() {
        return this.codigo.getValue() + " - " + this.status.getValue();
    }
}
