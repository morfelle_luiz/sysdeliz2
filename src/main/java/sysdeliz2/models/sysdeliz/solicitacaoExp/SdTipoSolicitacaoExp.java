package sysdeliz2.models.sysdeliz.solicitacaoExp;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SD_TIPO_SOLICITACAO_EXP_001")
@TelaSysDeliz(descricao = "Tipo de Solicitação para Expedição", icon = "expedicao_50.png")
public class SdTipoSolicitacaoExp extends BasicModel {

    @ExibeTableView(descricao = "Tipo", width = 150)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();

    public SdTipoSolicitacaoExp() {
    }

    public SdTipoSolicitacaoExp (String tipo, String descricao) {
        this.tipo.set(tipo);
        this.descricao.set(descricao);
    }

    @Id
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        setCodigoFilter(tipo);
        this.tipo.set(tipo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Override
    public String toString() {
        if(getTipo() == null) return "";
        return getTipo();
    }
}
