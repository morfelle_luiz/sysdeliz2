/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.table;

import javafx.beans.property.*;
import sysdeliz2.models.ClienteFaturaAReceber;
import sysdeliz2.models.FaturaAReceber;

/**
 *
 * @author cristiano.diego
 */
public class TableBoletosClientes {

    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty tipo_cli = new SimpleStringProperty();
    private final StringProperty represent = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final StringProperty duplicata = new SimpleStringProperty();
    private final DoubleProperty valor_fatura = new SimpleDoubleProperty();
    private final DoubleProperty valor_duplicata = new SimpleDoubleProperty();
    private final StringProperty dt_vencto = new SimpleStringProperty();
    private final StringProperty dt_emissao = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();

    private final ObjectProperty<ClienteFaturaAReceber> obj_cliente = new SimpleObjectProperty<>();

    public TableBoletosClientes() {
    }

    public TableBoletosClientes(ClienteFaturaAReceber obj_cliente) {
        for (FaturaAReceber faturas : obj_cliente.getFaturas()) {
            this.cliente.set(obj_cliente.getCliente());
            this.email.set(obj_cliente.getEmail());
            this.telefone.set(obj_cliente.getTelefone());
            this.tipo_cli.set(obj_cliente.getTipo_cli());
            this.represent.set(obj_cliente.getRepresent());
            this.fatura.set(faturas.getFatura());
            this.duplicata.set(faturas.getDuplicata());
            this.valor_duplicata.set(faturas.getValor_duplicata());
            this.valor_fatura.set(faturas.getValor_fatura());
            this.dt_vencto.set(faturas.getDt_vencto());
            this.dt_emissao.set(faturas.getDt_emissao());
            this.pedido.set(faturas.getPedido());
            this.obs.set(faturas.getObs());
        }
        this.obj_cliente.set(obj_cliente);
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public final String getTelefone() {
        return telefone.get();
    }

    public final void setTelefone(String value) {
        telefone.set(value);
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public final String getTipo_cli() {
        return tipo_cli.get();
    }

    public final void setTipo_cli(String value) {
        tipo_cli.set(value);
    }

    public StringProperty tipo_cliProperty() {
        return tipo_cli;
    }

    public final String getRepresent() {
        return represent.get();
    }

    public final void setRepresent(String value) {
        represent.set(value);
    }

    public StringProperty representProperty() {
        return represent;
    }

    public final String getFatura() {
        return fatura.get();
    }

    public final void setFatura(String value) {
        fatura.set(value);
    }

    public StringProperty faturaProperty() {
        return fatura;
    }

    public final String getDuplicata() {
        return duplicata.get();
    }

    public final void setDuplicata(String value) {
        duplicata.set(value);
    }

    public StringProperty duplicataProperty() {
        return duplicata;
    }

    public final double getValor_fatura() {
        return valor_fatura.get();
    }

    public final void setValor_fatura(double value) {
        valor_fatura.set(value);
    }

    public DoubleProperty valor_faturaProperty() {
        return valor_fatura;
    }

    public final double getValor_duplicata() {
        return valor_duplicata.get();
    }

    public final void setValor_duplicata(double value) {
        valor_duplicata.set(value);
    }

    public DoubleProperty valor_duplicataProperty() {
        return valor_duplicata;
    }

    public final String getDt_vencto() {
        return dt_vencto.get();
    }

    public final void setDt_vencto(String value) {
        dt_vencto.set(value);
    }

    public StringProperty dt_venctoProperty() {
        return dt_vencto;
    }

    public final String getDt_emissao() {
        return dt_emissao.get();
    }

    public final void setDt_emissao(String value) {
        dt_emissao.set(value);
    }

    public StringProperty dt_emissaoProperty() {
        return dt_emissao;
    }

    public final String getPedido() {
        return pedido.get();
    }

    public final void setPedido(String value) {
        pedido.set(value);
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public final String getObs() {
        return obs.get();
    }

    public final void setObs(String value) {
        obs.set(value);
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public final ClienteFaturaAReceber getObj_cliente() {
        return obj_cliente.get();
    }

    public final void setObj_cliente(ClienteFaturaAReceber value) {
        obj_cliente.set(value);
    }

    public ObjectProperty<ClienteFaturaAReceber> obj_clienteProperty() {
        return obj_cliente;
    }

    @Override
    public String toString() {
        return "TreeTableBoletosClientes{" + "cliente=" + cliente + ", email=" + email + ", telefone=" + telefone + ", tipo_cli=" + tipo_cli + ", represent=" + represent + ", fatura=" + fatura + ", duplicata=" + duplicata + ", valor_fatura=" + valor_fatura + ", valor_duplicata=" + valor_duplicata + ", dt_vencto=" + dt_vencto + ", dt_emissao=" + dt_emissao + ", pedido=" + pedido + ", obs=" + obs + '}';
    }

}
