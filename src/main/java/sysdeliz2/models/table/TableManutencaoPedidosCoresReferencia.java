/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.table;

import javafx.beans.property.*;
import javafx.collections.ObservableMap;

/**
 *
 * @author cristiano.diego
 */
public class TableManutencaoPedidosCoresReferencia {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final MapProperty<String, Integer> tamanhos = new SimpleMapProperty<String, Integer>();
    private final MapProperty<Integer, String> headersTamanhos = new SimpleMapProperty<Integer, String>();

    public TableManutencaoPedidosCoresReferencia() {
    }

    public TableManutencaoPedidosCoresReferencia(String codigo, String cor, ObservableMap<String, Integer> tamanhos, ObservableMap<Integer, String> headersTamanhos) {
        this.codigo.set(codigo);
        this.cor.set(cor);
        this.tamanhos.set(tamanhos);
        this.headersTamanhos.set(headersTamanhos);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final ObservableMap<String, Integer> getTamanhos() {
        return tamanhos.get();
    }

    public final void setTamanhos(ObservableMap<String, Integer> value) {
        tamanhos.set(value);
    }

    public MapProperty<String, Integer> tamanhosProperty() {
        return tamanhos;
    }

    public final ObservableMap<Integer, String> getHeadersTamanhos() {
        return headersTamanhos.get();
    }

    public final void setHeadersTamanhos(ObservableMap<Integer, String> value) {
        headersTamanhos.set(value);
    }

    public MapProperty<Integer, String> headersTamanhosProperty() {
        return headersTamanhos;
    }

}
