package sysdeliz2.models.table;

import javafx.beans.property.*;
import javafx.collections.ObservableMap;

import java.math.BigDecimal;

public class TableManutencaoPedidosPedido {

    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    private final BooleanProperty edited = new SimpleBooleanProperty(false);
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty item = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty entrega = new SimpleStringProperty();
    private final StringProperty dtEntrega = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty representante = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty qtdePecas = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> percCancelado = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorBruto = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorUnit = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> percDesconto = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty financeiro = new SimpleStringProperty();
    private final StringProperty comercial = new SimpleStringProperty();
    private final StringProperty npa = new SimpleStringProperty();
    private final StringProperty pagtoAntecipado = new SimpleStringProperty();
    private final MapProperty<String, Integer> tamanhos = new SimpleMapProperty<String, Integer>();
    private final MapProperty<Integer, String> headersTamanhos = new SimpleMapProperty<Integer, String>();

    public TableManutencaoPedidosPedido() {
    }

    public TableManutencaoPedidosPedido(String numero, String codigo, String item, String colecao, String entrega, String dtEntrega, String cliente,
            String nome, String grupo, String representante, String cor, Integer qtdePecas, BigDecimal valorBruto, BigDecimal valorUnit, BigDecimal percDesconto,
            String financeiro, String comercial, String npa, String pagtoAntecipado, BigDecimal percCancelado, ObservableMap<String, Integer> tamanhos, ObservableMap<Integer, String> headersTamanhos) {
        this.numero.set(numero);
        this.codigo.set(codigo);
        this.item.set(item);
        this.colecao.set(colecao);
        this.entrega.set(entrega);
        this.dtEntrega.set(dtEntrega);
        this.cliente.set(cliente);
        this.nome.set(nome);
        this.grupo.set(grupo);
        this.representante.set(representante);
        this.cor.set(cor);
        this.qtdePecas.set(qtdePecas);
        this.valorBruto.set(valorBruto);
        this.valorUnit.set(valorUnit);
        this.percDesconto.set(percDesconto);
        this.percCancelado.set(percCancelado);
        this.financeiro.set(financeiro);
        this.comercial.set(comercial);
        this.npa.set(npa);
        this.pagtoAntecipado.set(pagtoAntecipado);
        this.tamanhos.set(tamanhos);
        this.headersTamanhos.set(headersTamanhos);
    }

    public final boolean isSelected() {
        return selected.get();
    }

    public final void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public final boolean isEdited() {
        return edited.get();
    }

    public final void setEdited(boolean value) {
        edited.set(value);
    }

    public BooleanProperty editedProperty() {
        return edited;
    }

    public final String getNumero() {
        return numero.get();
    }

    public final void setNumero(String value) {
        numero.set(value);
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public final String getItem() {
        return item.get();
    }

    public final void setItem(String value) {
        item.set(value);
    }

    public StringProperty itemProperty() {
        return item;
    }

    public final String getColecao() {
        return colecao.get();
    }

    public final void setColecao(String value) {
        colecao.set(value);
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public final String getEntrega() {
        return entrega.get();
    }

    public final void setEntrega(String value) {
        entrega.set(value);
    }

    public StringProperty entregaProperty() {
        return entrega;
    }

    public final String getDtEntrega() {
        return dtEntrega.get();
    }

    public final void setDtEntrega(String value) {
        dtEntrega.set(value);
    }

    public StringProperty dtEntregaProperty() {
        return dtEntrega;
    }

    public final String getCliente() {
        return cliente.get();
    }

    public final void setCliente(String value) {
        cliente.set(value);
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getGrupo() {
        return grupo.get();
    }

    public final void setGrupo(String value) {
        grupo.set(value);
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public final String getRepresentante() {
        return representante.get();
    }

    public final void setRepresentante(String value) {
        representante.set(value);
    }

    public StringProperty representanteProperty() {
        return representante;
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final int getQtdePecas() {
        return qtdePecas.get();
    }

    public final void setQtdePecas(int value) {
        qtdePecas.set(value);
    }

    public IntegerProperty qtdePecasProperty() {
        return qtdePecas;
    }

    public final BigDecimal getValorBruto() {
        return valorBruto.get();
    }

    public final void setValorBruto(BigDecimal value) {
        valorBruto.set(value);
    }

    public ObjectProperty<BigDecimal> valorBrutoProperty() {
        return valorBruto;
    }

    public final BigDecimal getPercDesconto() {
        return percDesconto.get();
    }

    public final void setPercDesconto(BigDecimal value) {
        percDesconto.set(value);
    }

    public ObjectProperty<BigDecimal> percDescontoProperty() {
        return percDesconto;
    }

    public final String getFinanceiro() {
        return financeiro.get();
    }

    public final void setFinanceiro(String value) {
        financeiro.set(value);
    }

    public StringProperty financeiroProperty() {
        return financeiro;
    }
    
    public String getComercial() {
        return comercial.get();
    }
    
    public StringProperty comercialProperty() {
        return comercial;
    }
    
    public void setComercial(String comercial) {
        this.comercial.set(comercial);
    }
    
    public final String getNpa() {
        return npa.get();
    }

    public final void setNpa(String value) {
        npa.set(value);
    }

    public StringProperty npaProperty() {
        return npa;
    }

    public final String getPagtoAntecipado() {
        return pagtoAntecipado.get();
    }

    public final void setPagtoAntecipado(String value) {
        pagtoAntecipado.set(value);
    }

    public StringProperty pagtoAntecipadoProperty() {
        return pagtoAntecipado;
    }

    public final ObservableMap<String, Integer> getTamanhos() {
        return tamanhos.get();
    }

    public final void setTamanhos(ObservableMap<String, Integer> value) {
        tamanhos.set(value);
    }

    public MapProperty<String, Integer> tamanhosProperty() {
        return tamanhos;
    }

    public final ObservableMap<Integer, String> getHeadersTamanhos() {
        return headersTamanhos.get();
    }

    public final void setHeadersTamanhos(ObservableMap<Integer, String> value) {
        headersTamanhos.set(value);
    }

    public MapProperty<Integer, String> headersTamanhosProperty() {
        return headersTamanhos;
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final BigDecimal getValorUnit() {
        return valorUnit.get();
    }

    public final void setValorUnit(BigDecimal value) {
        valorUnit.set(value);
    }

    public ObjectProperty<BigDecimal> valorUnitProperty() {
        return valorUnit;
    }
    
    public BigDecimal getPercCancelado() {
        return percCancelado.get();
    }
    
    public ObjectProperty<BigDecimal> percCanceladoProperty() {
        return percCancelado;
    }
    
    public void setPercCancelado(BigDecimal percCancelado) {
        this.percCancelado.set(percCancelado);
    }
    
    @Override
    public String toString() {
        return "TableManutencaoPedidosPedido{" + "selected=" + selected + ", edited=" + edited + ", numero=" + numero + ", codigo=" + codigo + ", item=" + item + ", colecao=" + colecao + ", entrega=" + entrega + ", dtEntrega=" + dtEntrega + ", cliente=" + cliente + ", nome=" + nome + ", grupo=" + grupo + ", representante=" + representante + ", cor=" + cor + ", qtdePecas=" + qtdePecas + ", valorBruto=" + valorBruto + ", percDesconto=" + percDesconto + ", financeiro=" + financeiro + ", npa=" + npa + ", pagtoAntecipado=" + pagtoAntecipado + ", tamanhos=" + tamanhos + ", headersTamanhos=" + headersTamanhos + '}';
    }

}
