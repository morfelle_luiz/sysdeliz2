/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.table;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author cristiano.diego
 */
public class TableProdutoGradeProduto {

    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty planejado = new SimpleIntegerProperty();
    private final IntegerProperty emProducao = new SimpleIntegerProperty();
    private final IntegerProperty produzido = new SimpleIntegerProperty();
    private final IntegerProperty estoque = new SimpleIntegerProperty();
    private final IntegerProperty expedicao = new SimpleIntegerProperty();
    private final IntegerProperty carteira = new SimpleIntegerProperty();
    private final IntegerProperty saldo = new SimpleIntegerProperty();

    public TableProdutoGradeProduto() {
    }

    public TableProdutoGradeProduto(String cor, String tam, Integer planejado, Integer emProducao, Integer produzido, Integer estoque,
            Integer expedicao, Integer carteira, Integer saldo) {
        this.carteira.set(carteira);
        this.cor.set(cor);
        this.emProducao.set(emProducao);
        this.estoque.set(estoque);
        this.expedicao.set(expedicao);
        this.planejado.set(planejado);
        this.produzido.set(produzido);
        this.saldo.set(saldo);
        this.tam.set(tam);
    }

    public final String getCor() {
        return cor.get();
    }

    public final void setCor(String value) {
        cor.set(value);
    }

    public StringProperty corProperty() {
        return cor;
    }

    public final String getTam() {
        return tam.get();
    }

    public final void setTam(String value) {
        tam.set(value);
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public final int getPlanejado() {
        return planejado.get();
    }

    public final void setPlanejado(int value) {
        planejado.set(value);
    }

    public IntegerProperty planejadoProperty() {
        return planejado;
    }

    public final int getEmProducao() {
        return emProducao.get();
    }

    public final void setEmProducao(int value) {
        emProducao.set(value);
    }

    public IntegerProperty emProducaoProperty() {
        return emProducao;
    }

    public final int getProduzido() {
        return produzido.get();
    }

    public final void setProduzido(int value) {
        produzido.set(value);
    }

    public IntegerProperty produzidoProperty() {
        return produzido;
    }

    public final int getEstoque() {
        return estoque.get();
    }

    public final void setEstoque(int value) {
        estoque.set(value);
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public final int getExpedicao() {
        return expedicao.get();
    }

    public final void setExpedicao(int value) {
        expedicao.set(value);
    }

    public IntegerProperty expedicaoProperty() {
        return expedicao;
    }

    public final int getCarteira() {
        return carteira.get();
    }

    public final void setCarteira(int value) {
        carteira.set(value);
    }

    public IntegerProperty carteiraProperty() {
        return carteira;
    }

    public final int getSaldo() {
        return saldo.get();
    }

    public final void setSaldo(int value) {
        saldo.set(value);
    }

    public IntegerProperty saldoProperty() {
        return saldo;
    }

}
