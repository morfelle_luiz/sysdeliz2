package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "ano_001")
public class Ano implements Serializable {

    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final BooleanProperty diaUtil = new SimpleBooleanProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty sdVenPer = new SimpleStringProperty();
    private final StringProperty sdQuinzena = new SimpleStringProperty();

    public Ano() {
    }

    @Id
    @Column(name = "data")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }

    @Column(name = "dia_util")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isDiaUtil() {
        return diaUtil.get();
    }

    public BooleanProperty diaUtilProperty() {
        return diaUtil;
    }

    public void setDiaUtil(boolean diaUtil) {
        this.diaUtil.set(diaUtil);
    }

    @Column(name = "periodo")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Column(name = "sd_ven_per")
    public String getSdVenPer() {
        return sdVenPer.get();
    }

    public StringProperty sdVenPerProperty() {
        return sdVenPer;
    }

    public void setSdVenPer(String sdVenPer) {
        this.sdVenPer.set(sdVenPer);
    }

    @Column(name = "sd_quinzena")
    public String getSdQuinzena() {
        return sdQuinzena.get();
    }

    public StringProperty sdQuinzenaProperty() {
        return sdQuinzena;
    }

    public void setSdQuinzena(String sdQuinzena) {
        this.sdQuinzena.set(sdQuinzena);
    }
}