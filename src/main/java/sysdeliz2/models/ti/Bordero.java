package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BORDERO_001")
@TelaSysDeliz(descricao = "Bordero", icon = "banco (4).png")
public class Bordero extends BasicModel {

    @ColunaFilter(descricao = "Número", coluna = "numero")
    @ExibeTableView(descricao = "Número", width = 100)
    private final IntegerProperty numero = new SimpleIntegerProperty();
    private final StringProperty variacao = new SimpleStringProperty();
    @ColunaFilter(descricao = "Carteira", coluna = "carteira")
    @ExibeTableView(descricao = "Carteira", width = 100)
    private final StringProperty carteira = new SimpleStringProperty();
    @ColunaFilter(descricao = "Conta", coluna = "conta")
    @ExibeTableView(descricao = "Conta", width = 100)
    private final StringProperty conta = new SimpleStringProperty();
    private final IntegerProperty diasPro = new SimpleIntegerProperty(0);
    private final StringProperty ocorrencia = new SimpleStringProperty();
    @ColunaFilter(descricao = "Data Criação", coluna = "data")
    @ExibeTableView(descricao = "Data Criação", width = 100)
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty tipo = new SimpleStringProperty();

    private final ObjectProperty<Carteira> carteiraObj = new SimpleObjectProperty<>();
    private final ObjectProperty<CadConta> contaObj = new SimpleObjectProperty<>();
    private List<Receber> titulos = new ArrayList<>();

    public Bordero() {
    }

    public Bordero(String numero, CadConta conta, Carteira carteira, LocalDate data) {
        this.numero.set(Integer.parseInt(numero));
        this.carteira.set(carteira.getCarteira());
        this.conta.set(conta.getConta());
        this.data.set(data);
        this.tipo.set("R");
    }


    @Id
    @Column(name = "NUMERO")
    public int getNumero() {
        return numero.get();
    }

    public IntegerProperty numeroProperty() {
        return numero;
    }

    public void setNumero(int numero) {
        setCodigoFilter(String.valueOf(numero));
        this.numero.set(numero);
    }

    @Column(name = "VARIACAO")
    public String getVariacao() {
        return variacao.get();
    }

    public StringProperty variacaoProperty() {
        return variacao;
    }

    public void setVariacao(String variacao) {
        this.variacao.set(variacao);
    }

    @Column(name = "DIAS_PRO")
    public int getDiasPro() {
        return diasPro.get();
    }

    public IntegerProperty diasProProperty() {
        return diasPro;
    }

    public void setDiasPro(int diasPro) {
        this.diasPro.set(diasPro);
    }

    @Column(name = "OCORRENCIA")
    public String getOcorrencia() {
        return ocorrencia.get();
    }

    public StringProperty ocorrenciaProperty() {
        return ocorrencia;
    }

    public void setOcorrencia(String ocorrencia) {
        this.ocorrencia.set(ocorrencia);
    }

    @Column(name = "CARTEIRA")
    public String getCarteira() {
        return carteira.get();
    }

    public StringProperty carteiraProperty() {
        return carteira;
    }

    public void setCarteira(String carteira) {
        this.carteira.set(carteira);
    }

    @Column(name = "CONTA")
    public String getConta() {
        return conta.get();
    }

    public StringProperty contaProperty() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta.set(conta);
    }

    @Column(name = "DATA")
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @OneToMany
    @JoinColumn(name = "BORDERO", referencedColumnName = "NUMERO")
    public List<Receber> getTitulos() {
        return titulos;
    }

    public void setTitulos(List<Receber> titulos) {
        this.titulos = titulos;
    }


    @Transient
    public Carteira getCarteiraObj() {
        return carteiraObj.get();
    }

    public ObjectProperty<Carteira> carteiraObjProperty() {
        return carteiraObj;
    }

    public void setCarteiraObj(Carteira carteiraObj) {
        this.carteiraObj.set(carteiraObj);
    }

    @Transient
    public CadConta getContaObj() {
        return contaObj.get();
    }

    public ObjectProperty<CadConta> contaObjProperty() {
        return contaObj;
    }

    public void setContaObj(CadConta contaObj) {
        this.contaObj.set(contaObj);
    }

    @PostLoad
    private void postLoadFunctions() {

        setDescricaoFilter(getNumero() + " - " + StringUtils.toShortDateFormat(getData()));

        if (!conta.getValueSafe().equals(""))
            contaObj.set(new FluentDao().selectFrom(CadConta.class).where(it -> it.equal("conta", conta.getValueSafe())).singleResult());

        if (!carteira.getValueSafe().equals(""))
            carteiraObj.set(new FluentDao().selectFrom(Carteira.class).where(it -> it.equal("carteira", carteira.getValueSafe()).equal("conta.conta", conta.getValueSafe(), TipoExpressao.AND, when -> !conta.getValueSafe().equals(""))).singleResult());
    }

}
