package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "CADBAN_001")
@TelaSysDeliz(descricao = "Banco", icon = "banco (4).png")
public class CadBan extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "banco")
    private final StringProperty banco = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Banco", width = 350)
    @ColunaFilter(descricao = "Banco", coluna = "nomeBanco")
    private final StringProperty nomeBanco = new SimpleStringProperty();
    @Transient
    private final StringProperty local = new SimpleStringProperty();
    @Transient
    private final StringProperty entrada = new SimpleStringProperty();
    @Transient
    private final StringProperty layout = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> valContrato = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty nomeArq = new SimpleStringProperty();
    @Transient
    private final StringProperty layoutPag = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> tamBordero = new SimpleObjectProperty<>();
    @Transient
    private final BooleanProperty geraContrato = new SimpleBooleanProperty();

    private final ObjectProperty<VSdDadosEntidade> codFor = new SimpleObjectProperty<>();

    public CadBan() {
    }

    @Id
    @Column(name = "BANCO")
    public String getBanco() {
        return banco.get();
    }

    public StringProperty bancoProperty() {
        return banco;
    }

    public void setBanco(String banco) {
        setCodigoFilter(banco);
        this.banco.set(banco);
    }

    @Column(name = "NOME_BANCO")
    public String getNomeBanco() {
        return nomeBanco.get();
    }

    public StringProperty nomeBancoProperty() {
        return nomeBanco;
    }

    public void setNomeBanco(String nomeBanco) {
        setDescricaoFilter(nomeBanco);
        this.nomeBanco.set(nomeBanco);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "ENTRADA")
    public String getEntrada() {
        return entrada.get();
    }

    public StringProperty entradaProperty() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada.set(entrada);
    }

    @Column(name = "LAYOUT")
    public String getLayout() {
        return layout.get();
    }

    public StringProperty layoutProperty() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout.set(layout);
    }

    @Column(name = "VAL_CONTRATO")
    public BigDecimal getValContrato() {
        return valContrato.get();
    }

    public ObjectProperty<BigDecimal> valContratoProperty() {
        return valContrato;
    }

    public void setValContrato(BigDecimal valContrato) {
        this.valContrato.set(valContrato);
    }

    @Column(name = "NOME_ARQ")
    public String getNomeArq() {
        return nomeArq.get();
    }

    public StringProperty nomeArqProperty() {
        return nomeArq;
    }

    public void setNomeArq(String nomeArq) {
        this.nomeArq.set(nomeArq);
    }

    @Column(name = "LAYOUT_PAG")
    public String getLayoutPag() {
        return layoutPag.get();
    }

    public StringProperty layoutPagProperty() {
        return layoutPag;
    }

    public void setLayoutPag(String layoutPag) {
        this.layoutPag.set(layoutPag);
    }


    @Column(name = "TAM_BORDERO")
    public BigDecimal getTamBordero() {
        return tamBordero.get();
    }

    public ObjectProperty<BigDecimal> tamBorderoProperty() {
        return tamBordero;
    }

    public void setTamBordero(BigDecimal tamBordero) {
        this.tamBordero.set(tamBordero);
    }

    @Column(name = "GERA_CONTRATO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isGeraContrato() {
        return geraContrato.get();
    }

    public BooleanProperty geraContratoProperty() {
        return geraContrato;
    }

    public void setGeraContrato(boolean geraContrato) {
        this.geraContrato.set(geraContrato);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODFOR")
    public VSdDadosEntidade getCodFor() {
        return codFor.get();
    }

    public ObjectProperty<VSdDadosEntidade> codForProperty() {
        return codFor;
    }

    public void setCodFor(VSdDadosEntidade codFor) {
        this.codFor.set(codFor);
    }


    @Override
    public String toString() {
        return "[" + banco.get() + "] " + nomeBanco.get();
    }
}
