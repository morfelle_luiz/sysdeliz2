package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 27/08/2019 17:27
 */
@Entity
@Table(name = "CADCEP_001")
@TelaSysDeliz(descricao = "CEPs", icon = "cep (4).png")
public class CadCep extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "CEP", width = 60)
    @ColunaFilter(descricao = "CEP", coluna = "cep")
    private StringProperty cep = new SimpleStringProperty(this, "cep");
    
    @Transient
    @ExibeTableView(descricao = "Cidade", width = 300)
    @ColunaFilter(descricao = "Cidade", coluna = "cidade.codCid", filterClass = "sysdeliz2.models.ti.Cidade")
    private ObjectProperty<Cidade> cidade = new SimpleObjectProperty<>();
    //private StringProperty codCidade = new SimpleStringProperty(this, "codCidade");
    
    @Transient
    private StringProperty logrCep = new SimpleStringProperty(this, "logrCep");
    @Transient
    private StringProperty bairro = new SimpleStringProperty(this, "bairro");

    public CadCep() {
    }

    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "CEP")
    public String getCep() {
        return cep.get();
    }

    public StringProperty cepProperty() {
        return cep;
    }

    public void setCep(String cep) {
        setCodigoFilter(cep);
        this.cep.set(cep);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_CID")
    public Cidade getCidade() {
        return cidade.get();
    }

    public ObjectProperty<Cidade> cidadeProperty() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        setDescricaoFilter(cidade.getNomeCid());
        this.cidade.set(cidade);
    }

    @Column(name ="LOGR_CEP")
    public String getLogrCep() {
        return logrCep.get();
    }

    public StringProperty logrCepProperty() {
        return logrCep;
    }

    public void setLogrCep(String logrCep) {
        this.logrCep.set(logrCep);
    }

    @Column(name ="BAIRRO")
    public String getBairro() {
        return bairro.get();
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }

    @Override
    public String toString() {
        return cep.get();
    }

}
