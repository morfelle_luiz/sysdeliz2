package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "CADCONTA_001")
@TelaSysDeliz(descricao = "CONTA", icon = "comercial_100.png")
public class CadConta extends BasicModel implements Serializable {
    private final IntegerProperty contador = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Banco", width = 200)
    @ColunaFilter(descricao = "Banco", coluna = "banco.banco")
    private final ObjectProperty<CadBan> banco = new SimpleObjectProperty<>();
    private final StringProperty agencia = new SimpleStringProperty();
    private final StringProperty inscricao = new SimpleStringProperty();
    private final StringProperty cnpj = new SimpleStringProperty();
    private final StringProperty outros = new SimpleStringProperty();
    private final StringProperty empresa = new SimpleStringProperty();
    private final StringProperty mensagem1 = new SimpleStringProperty();
    private final StringProperty mensagem2 = new SimpleStringProperty();
    private final StringProperty loteservico = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> limite = new SimpleObjectProperty<>();
    private final StringProperty padrao = new SimpleStringProperty();
    private final StringProperty avalista = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtcaixa = new SimpleObjectProperty<>();
    private final IntegerProperty diascomp = new SimpleIntegerProperty();
    private final StringProperty impfluxo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Conta", width = 100)
    @ColunaFilter(descricao = "Conta", coluna = "conta")
    private final StringProperty conta = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 100)
    @ColunaFilter(descricao = "Descriação", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty pendente = new SimpleStringProperty();
    private final StringProperty nrbanco = new SimpleStringProperty();

    public CadConta() {
    }

    @Column(name = "CONTADOR")
    public Integer getContador() {
        return contador.get();
    }

    public IntegerProperty contadorProperty() {
        return contador;
    }

    public void setContador(Integer contador) {
        this.contador.set(contador);
    }

    @OneToOne
    @JoinColumn(name = "BANCO")
    public CadBan getBanco() {
        return banco.get();
    }

    public ObjectProperty<CadBan> bancoProperty() {
        return banco;
    }

    public void setBanco(CadBan banco) {
        this.banco.set(banco);
    }

    @Column(name = "AGENCIA")
    public String getAgencia() {
        return agencia.get();
    }

    public StringProperty agenciaProperty() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia.set(agencia);
    }

    @Id
    @Column(name = "CONTA")
    public String getConta() {
        return conta.get();
    }

    public StringProperty contaProperty() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta.set(conta);
        setCodigoFilter(conta);
    }

    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }

    public StringProperty inscricaoProperty() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }

    @Column(name = "CNPJ")
    public String getCnpj() {
        return cnpj.get();
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }

    @Column(name = "OUTROS")
    public String getOutros() {
        return outros.get();
    }

    public StringProperty outrosProperty() {
        return outros;
    }

    public void setOutros(String outros) {
        this.outros.set(outros);
    }

    @Column(name = "EMPRESA")
    public String getEmpresa() {
        return empresa.get();
    }

    public StringProperty empresaProperty() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa.set(empresa);
    }

    @Column(name = "MENSAGEM_1")
    public String getMensagem1() {
        return mensagem1.get();
    }

    public StringProperty mensagem1Property() {
        return mensagem1;
    }

    public void setMensagem1(String mensagem1) {
        this.mensagem1.set(mensagem1);
    }

    @Column(name = "MENSAGEM_2")
    public String getMensagem2() {
        return mensagem2.get();
    }

    public StringProperty mensagem2Property() {
        return mensagem2;
    }

    public void setMensagem2(String mensagem2) {
        this.mensagem2.set(mensagem2);
    }

    @Column(name = "LOTE_SERVICO")
    public String getLoteservico() {
        return loteservico.get();
    }

    public StringProperty loteservicoProperty() {
        return loteservico;
    }

    public void setLoteservico(String loteservico) {
        this.loteservico.set(loteservico);
    }

    @Column(name = "LIMITE")
    public BigDecimal getLimite() {
        return limite.get();
    }

    public ObjectProperty<BigDecimal> limiteProperty() {
        return limite;
    }

    public void setLimite(BigDecimal limite) {
        this.limite.set(limite);
    }

    @Column(name = "PADRAO")
    public String getPadrao() {
        return padrao.get();
    }

    public StringProperty padraoProperty() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao.set(padrao);
    }

    @Column(name = "AVALISTA")
    public String getAvalista() {
        return avalista.get();
    }

    public StringProperty avalistaProperty() {
        return avalista;
    }

    public void setAvalista(String avalista) {
        this.avalista.set(avalista);
    }

    @Column(name = "DT_CAIXA")
    public LocalDate getDtcaixa() {
        return dtcaixa.get();
    }

    public ObjectProperty<LocalDate> dtcaixaProperty() {
        return dtcaixa;
    }

    public void setDtcaixa(LocalDate dtcaixa) {
        this.dtcaixa.set(dtcaixa);
    }

    @Column(name = "DIAS_COMP")
    public Integer getDiascomp() {
        return diascomp.get();
    }

    public IntegerProperty diascompProperty() {
        return diascomp;
    }

    public void setDiascomp(Integer diascomp) {
        this.diascomp.set(diascomp);
    }

    @Column(name = "IMP_FLUXO")
    public String getImpfluxo() {
        return impfluxo.get();
    }

    public StringProperty impfluxoProperty() {
        return impfluxo;
    }

    public void setImpfluxo(String impfluxo) {
        this.impfluxo.set(impfluxo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "PENDENTE")
    public String getPendente() {
        return pendente.get();
    }

    public StringProperty pendenteProperty() {
        return pendente;
    }

    public void setPendente(String pendente) {
        this.pendente.set(pendente);
    }

    @Column(name = "NR_BANCO")
    public String getNrbanco() {
        return nrbanco.get();
    }

    public StringProperty nrbancoProperty() {
        return nrbanco;
    }

    public void setNrbanco(String nrbanco) {
        this.nrbanco.set(nrbanco);
    }

    @Override
    public String toString() {
        return "[" + conta.get() + "] " + descricao.get();
    }
}
