package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "CADFLUXO_001")
@TelaSysDeliz(descricao = "Setores", icon = "setor_50.png")
public class CadFluxo extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    @Transient
    private final StringProperty inventario = new SimpleStringProperty();
    @Transient
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> indinv = new SimpleObjectProperty<BigDecimal>();
    @Transient
    @ExibeTableView(descricao = "Lead", width = 40)
    private final IntegerProperty dias = new SimpleIntegerProperty();
    @Transient
    private final StringProperty altpreco = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty baixamaior = new SimpleStringProperty();
    @Transient
    private final StringProperty baixamov = new SimpleStringProperty();
    @Transient
    private final StringProperty baixalivre = new SimpleStringProperty();
    @Transient
    private final IntegerProperty pecasdia = new SimpleIntegerProperty();
    @Transient
    private final StringProperty impfluxo = new SimpleStringProperty();
    @Transient
    private final StringProperty grupo = new SimpleStringProperty();
    @Transient
    private final StringProperty baixaparcial = new SimpleStringProperty();
    @Transient
    private final IntegerProperty diassemana = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty indpag = new SimpleIntegerProperty();
    @Transient
    private final StringProperty baixaconsumo = new SimpleStringProperty();
    @Transient
    private final StringProperty baixadefeito = new SimpleStringProperty();
    @Transient
    private final StringProperty codsped = new SimpleStringProperty();
    @Transient
    private final StringProperty turno = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 50)
    @ColunaFilter(descricao = "Grupo", coluna = "sdgrupo")
    private final StringProperty sdgrupo = new SimpleStringProperty();
    @Transient
    private final BooleanProperty controleData = new SimpleBooleanProperty(false);
    @Transient
    private final StringProperty sdDescricao = new SimpleStringProperty();
    @Transient
    private final StringProperty sdLinha = new SimpleStringProperty();

    public CadFluxo() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }
    
    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }
    
    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "INVENTARIO")
    public String getInventario() {
        return inventario.get();
    }
    
    public StringProperty inventarioProperty() {
        return inventario;
    }
    
    public void setInventario(String inventario) {
        this.inventario.set(inventario);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "IND_INV")
    public BigDecimal getIndinv() {
        return indinv.get();
    }
    
    public ObjectProperty<BigDecimal> indinvProperty() {
        return indinv;
    }
    
    public void setIndinv(BigDecimal indinv) {
        this.indinv.set(indinv);
    }
    
    @Column(name = "DIAS")
    public Integer getDias() {
        return dias.get();
    }
    
    public IntegerProperty diasProperty() {
        return dias;
    }
    
    public void setDias(Integer dias) {
        this.dias.set(dias);
    }
    
    @Column(name = "ALT_PRECO")
    public String getAltpreco() {
        return altpreco.get();
    }
    
    public StringProperty altprecoProperty() {
        return altpreco;
    }
    
    public void setAltpreco(String altpreco) {
        this.altpreco.set(altpreco);
    }
    
    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }
    
    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }
    
    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }
    
    @Column(name = "BAIXAMAIOR")
    public String getBaixamaior() {
        return baixamaior.get();
    }
    
    public StringProperty baixamaiorProperty() {
        return baixamaior;
    }
    
    public void setBaixamaior(String baixamaior) {
        this.baixamaior.set(baixamaior);
    }
    
    @Column(name = "BAIXAMOV")
    public String getBaixamov() {
        return baixamov.get();
    }
    
    public StringProperty baixamovProperty() {
        return baixamov;
    }
    
    public void setBaixamov(String baixamov) {
        this.baixamov.set(baixamov);
    }
    
    @Column(name = "BAIXALIVRE")
    public String getBaixalivre() {
        return baixalivre.get();
    }
    
    public StringProperty baixalivreProperty() {
        return baixalivre;
    }
    
    public void setBaixalivre(String baixalivre) {
        this.baixalivre.set(baixalivre);
    }
    
    @Column(name = "PECAS_DIA")
    public Integer getPecasdia() {
        return pecasdia.get();
    }
    
    public IntegerProperty pecasdiaProperty() {
        return pecasdia;
    }
    
    public void setPecasdia(Integer pecasdia) {
        this.pecasdia.set(pecasdia);
    }
    
    @Column(name = "IMP_FLUXO")
    public String getImpfluxo() {
        return impfluxo.get();
    }
    
    public StringProperty impfluxoProperty() {
        return impfluxo;
    }
    
    public void setImpfluxo(String impfluxo) {
        this.impfluxo.set(impfluxo);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "BAIXAPARCIAL")
    public String getBaixaparcial() {
        return baixaparcial.get();
    }
    
    public StringProperty baixaparcialProperty() {
        return baixaparcial;
    }
    
    public void setBaixaparcial(String baixaparcial) {
        this.baixaparcial.set(baixaparcial);
    }
    
    @Column(name = "DIAS_SEMANA")
    public Integer getDiassemana() {
        return diassemana.get();
    }
    
    public IntegerProperty diassemanaProperty() {
        return diassemana;
    }
    
    public void setDiassemana(Integer diassemana) {
        this.diassemana.set(diassemana);
    }
    
    @Column(name = "IND_PAG")
    public Integer getIndpag() {
        return indpag.get();
    }
    
    public IntegerProperty indpagProperty() {
        return indpag;
    }
    
    public void setIndpag(Integer indpag) {
        this.indpag.set(indpag);
    }
    
    @Column(name = "BAIXACONSUMO")
    public String getBaixaconsumo() {
        return baixaconsumo.get();
    }
    
    public StringProperty baixaconsumoProperty() {
        return baixaconsumo;
    }
    
    public void setBaixaconsumo(String baixaconsumo) {
        this.baixaconsumo.set(baixaconsumo);
    }
    
    @Column(name = "BAIXADEFEITO")
    public String getBaixadefeito() {
        return baixadefeito.get();
    }
    
    public StringProperty baixadefeitoProperty() {
        return baixadefeito;
    }
    
    public void setBaixadefeito(String baixadefeito) {
        this.baixadefeito.set(baixadefeito);
    }
    
    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }
    
    public StringProperty codspedProperty() {
        return codsped;
    }
    
    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }
    
    @Column(name = "TURNO")
    public String getTurno() {
        return turno.get();
    }
    
    public StringProperty turnoProperty() {
        return turno;
    }
    
    public void setTurno(String turno) {
        this.turno.set(turno);
    }
    
    @Column(name = "SD_GRUPO")
    public String getSdgrupo() {
        return sdgrupo.get();
    }
    
    public StringProperty sdgrupoProperty() {
        return sdgrupo;
    }
    
    public void setSdgrupo(String sdgrupo) {
        this.sdgrupo.set(sdgrupo);
    }

    @Column(name = "CONTROLE_DATA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isControleData() {
        return controleData.get();
    }

    public BooleanProperty controleDataProperty() {
        return controleData;
    }

    public void setControleData(boolean controleData) {
        this.controleData.set(controleData);
    }

    @Column(name = "SD_DESCRICAO")
    public String getSdDescricao() {
        return sdDescricao.get();
    }

    public StringProperty sdDescricaoProperty() {
        return sdDescricao;
    }

    public void setSdDescricao(String sdDescricao) {
        this.sdDescricao.set(sdDescricao);
    }

    @Column(name = "SD_LINHA")
    public String getSdLinha() {
        return sdLinha.get();
    }

    public StringProperty sdLinhaProperty() {
        return sdLinha;
    }

    public void setSdLinha(String sdLinha) {
        this.sdLinha.set(sdLinha);
    }

    @Override
    public String toString() {
        return "["+this.codigo.get()+"] "+this.descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CadFluxo cadFluxo = (CadFluxo) o;
        return Objects.equals(codigo.get(), cadFluxo.codigo.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo.get());
    }
}
