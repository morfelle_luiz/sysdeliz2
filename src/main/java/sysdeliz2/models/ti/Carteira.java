package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "CARTEIRA_001")
@TelaSysDeliz(descricao = "Carteira", icon = "faturamento_100.png")
@IdClass(CarteiraPK.class)
public class Carteira extends BasicModel implements Serializable {
    @ExibeTableView(descricao = "Carteira", width = 80)
    @ColunaFilter(descricao = "Carteira", coluna = "carteira")
    private final StringProperty carteira = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 80)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty diasbaixar = new SimpleStringProperty();
    private final StringProperty convenio = new SimpleStringProperty();
    private final StringProperty loteservico = new SimpleStringProperty();
    private final StringProperty instrucao1 = new SimpleStringProperty();
    private final StringProperty instrucao2 = new SimpleStringProperty();
    private final StringProperty aceite = new SimpleStringProperty();
    @ExibeTableView(descricao = "Situação", width = 80)
    @ColunaFilter(descricao = "Situação", coluna = "situacao")
    private final StringProperty situacao = new SimpleStringProperty();
    private final StringProperty situabco = new SimpleStringProperty();
    private final StringProperty banco = new SimpleStringProperty();
    private final StringProperty descricao2 = new SimpleStringProperty();
    private final StringProperty descricao3 = new SimpleStringProperty();
    private final StringProperty numcontrato = new SimpleStringProperty();
    private final IntegerProperty diasprotesto = new SimpleIntegerProperty();
    @ExibeTableView(descricao = "Conta", width = 80)
    @ColunaFilter(descricao = "Conta", coluna = "conta")
    private final ObjectProperty<CadConta> conta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> multa = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty geranosso = new SimpleStringProperty();
    private final StringProperty nrbanco = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> taxa1 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> taxa2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtvendor = new SimpleObjectProperty<LocalDate>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty layout = new SimpleStringProperty();
    private final StringProperty impboleto = new SimpleStringProperty();
    private final StringProperty cobliteral = new SimpleStringProperty();
    private final StringProperty diasdevol = new SimpleStringProperty();
    private final StringProperty tpcob = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty chavepix = new SimpleStringProperty();
    private final StringProperty chaveaut = new SimpleStringProperty();
    private final StringProperty chaveapl = new SimpleStringProperty();

    public Carteira() {
    }

    @Column(name = "DIAS_BAIXAR")
    public String getDiasbaixar() {
        return diasbaixar.get();
    }

    public StringProperty diasbaixarProperty() {
        return diasbaixar;
    }

    public void setDiasbaixar(String diasbaixar) {
        this.diasbaixar.set(diasbaixar);
    }

    @Column(name = "CONVENIO")
    public String getConvenio() {
        return convenio.get();
    }

    public StringProperty convenioProperty() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio.set(convenio);
    }

    @Column(name = "LOTE_SERVICO")
    public String getLoteservico() {
        return loteservico.get();
    }

    public StringProperty loteservicoProperty() {
        return loteservico;
    }

    public void setLoteservico(String loteservico) {
        this.loteservico.set(loteservico);
    }

    @Column(name = "INSTRUCAO_1")
    public String getInstrucao1() {
        return instrucao1.get();
    }

    public StringProperty instrucao1Property() {
        return instrucao1;
    }

    public void setInstrucao1(String instrucao1) {
        this.instrucao1.set(instrucao1);
    }

    @Column(name = "INSTRUCAO_2")
    public String getInstrucao2() {
        return instrucao2.get();
    }

    public StringProperty instrucao2Property() {
        return instrucao2;
    }

    public void setInstrucao2(String instrucao2) {
        this.instrucao2.set(instrucao2);
    }

    @Column(name = "ACEITE")
    public String getAceite() {
        return aceite.get();
    }

    public StringProperty aceiteProperty() {
        return aceite;
    }

    public void setAceite(String aceite) {
        this.aceite.set(aceite);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Column(name = "SITUA_BCO")
    public String getSituabco() {
        return situabco.get();
    }

    public StringProperty situabcoProperty() {
        return situabco;
    }

    public void setSituabco(String situabco) {
        this.situabco.set(situabco);
    }

    @Id
    @Column(name = "CARTEIRA")
    public String getCarteira() {
        return carteira.get();
    }

    public StringProperty carteiraProperty() {
        return carteira;
    }

    public void setCarteira(String carteira) {
        this.carteira.set(carteira);
        setCodigoFilter(carteira);
    }

    @Column(name = "BANCO")
    public String getBanco() {
        return banco.get();
    }

    public StringProperty bancoProperty() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco.set(banco);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Column(name = "DESCRICAO2")
    public String getDescricao2() {
        return descricao2.get();
    }

    public StringProperty descricao2Property() {
        return descricao2;
    }

    public void setDescricao2(String descricao2) {
        this.descricao2.set(descricao2);
    }

    @Column(name = "DESCRICAO3")
    public String getDescricao3() {
        return descricao3.get();
    }

    public StringProperty descricao3Property() {
        return descricao3;
    }

    public void setDescricao3(String descricao3) {
        this.descricao3.set(descricao3);
    }

    @Column(name = "NUMCONTRATO")
    public String getNumcontrato() {
        return numcontrato.get();
    }

    public StringProperty numcontratoProperty() {
        return numcontrato;
    }

    public void setNumcontrato(String numcontrato) {
        this.numcontrato.set(numcontrato);
    }

    @Column(name = "DIASPROTESTO")
    public Integer getDiasprotesto() {
        return diasprotesto.get();
    }

    public IntegerProperty diasprotestoProperty() {
        return diasprotesto;
    }

    public void setDiasprotesto(Integer diasprotesto) {
        this.diasprotesto.set(diasprotesto);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "CONTA")
    public CadConta getConta() {
        return conta.get();
    }

    public ObjectProperty<CadConta> contaProperty() {
        return conta;
    }

    public void setConta(CadConta conta) {
        this.conta.set(conta);
    }

    @Column(name = "MULTA")
    public BigDecimal getMulta() {
        return multa.get();
    }

    public ObjectProperty<BigDecimal> multaProperty() {
        return multa;
    }

    public void setMulta(BigDecimal multa) {
        this.multa.set(multa);
    }

    @Column(name = "GERA_NOSSO")
    public String getGeranosso() {
        return geranosso.get();
    }

    public StringProperty geranossoProperty() {
        return geranosso;
    }

    public void setGeranosso(String geranosso) {
        this.geranosso.set(geranosso);
    }

    @Column(name = "NR_BANCO")
    public String getNrbanco() {
        return nrbanco.get();
    }

    public StringProperty nrbancoProperty() {
        return nrbanco;
    }

    public void setNrbanco(String nrbanco) {
        this.nrbanco.set(nrbanco);
    }

    @Column(name = "TAXA1")
    public BigDecimal getTaxa1() {
        return taxa1.get();
    }

    public ObjectProperty<BigDecimal> taxa1Property() {
        return taxa1;
    }

    public void setTaxa1(BigDecimal taxa1) {
        this.taxa1.set(taxa1);
    }

    @Column(name = "TAXA2")
    public BigDecimal getTaxa2() {
        return taxa2.get();
    }

    public ObjectProperty<BigDecimal> taxa2Property() {
        return taxa2;
    }

    public void setTaxa2(BigDecimal taxa2) {
        this.taxa2.set(taxa2);
    }

    @Column(name = "DT_VENDOR")
    public LocalDate getDtvendor() {
        return dtvendor.get();
    }

    public ObjectProperty<LocalDate> dtvendorProperty() {
        return dtvendor;
    }

    public void setDtvendor(LocalDate dtvendor) {
        this.dtvendor.set(dtvendor);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "LAYOUT")
    public String getLayout() {
        return layout.get();
    }

    public StringProperty layoutProperty() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout.set(layout);
    }

    @Column(name = "IMP_BOLETO")
    public String getImpboleto() {
        return impboleto.get();
    }

    public StringProperty impboletoProperty() {
        return impboleto;
    }

    public void setImpboleto(String impboleto) {
        this.impboleto.set(impboleto);
    }

    @Column(name = "COB_LITERAL")
    public String getCobliteral() {
        return cobliteral.get();
    }

    public StringProperty cobliteralProperty() {
        return cobliteral;
    }

    public void setCobliteral(String cobliteral) {
        this.cobliteral.set(cobliteral);
    }

    @Column(name = "DIAS_DEVOL")
    public String getDiasdevol() {
        return diasdevol.get();
    }

    public StringProperty diasdevolProperty() {
        return diasdevol;
    }

    public void setDiasdevol(String diasdevol) {
        this.diasdevol.set(diasdevol);
    }

    @Column(name = "TP_COB")
    public String getTpcob() {
        return tpcob.get();
    }

    public StringProperty tpcobProperty() {
        return tpcob;
    }

    public void setTpcob(String tpcob) {
        this.tpcob.set(tpcob);
    }

    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "CHAVE_PIX")
    public String getChavepix() {
        return chavepix.get();
    }

    public StringProperty chavepixProperty() {
        return chavepix;
    }

    public void setChavepix(String chavepix) {
        this.chavepix.set(chavepix);
    }

    @Column(name = "CHAVE_AUT")
    public String getChaveaut() {
        return chaveaut.get();
    }

    public StringProperty chaveautProperty() {
        return chaveaut;
    }

    public void setChaveaut(String chaveaut) {
        this.chaveaut.set(chaveaut);
    }

    @Column(name = "CHAVE_APL")
    public String getChaveapl() {
        return chaveapl.get();
    }

    public StringProperty chaveaplProperty() {
        return chaveapl;
    }

    public void setChaveapl(String chaveapl) {
        this.chaveapl.set(chaveapl);
    }
}
