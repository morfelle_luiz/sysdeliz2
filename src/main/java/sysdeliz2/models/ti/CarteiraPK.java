package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;
import java.util.Objects;

public class CarteiraPK implements Serializable {
    private final StringProperty carteira = new SimpleStringProperty();
    private final ObjectProperty<CadConta> conta = new SimpleObjectProperty<>();

    public CarteiraPK() {
    }

    public String getCarteira() {
        return carteira.get();
    }

    public StringProperty carteiraProperty() {
        return carteira;
    }

    public void setCarteira(String carteira) {
        this.carteira.set(carteira);
    }

    public CadConta getConta() {
        return conta.get();
    }

    public ObjectProperty<CadConta> contaProperty() {
        return conta;
    }

    public void setConta(CadConta conta) {
        this.conta.set(conta);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarteiraPK that = (CarteiraPK) o;
        return Objects.equals(carteira, that.carteira) && Objects.equals(conta, that.conta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carteira, conta);
    }
}
