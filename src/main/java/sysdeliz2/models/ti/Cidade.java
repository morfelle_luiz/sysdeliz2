package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 17/07/2019 08:35
 */
@Entity
@Table(name="CIDADE_001")
@SuppressWarnings("unused")
@TelaSysDeliz(descricao = "Cidade", icon = "cidade (4).png")
public class Cidade extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codCid")
    private final StringProperty codCid = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nomeCid")
    private final StringProperty nomeCid = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "UF", width = 30)
    private final ObjectProperty<TabUf> codEst = new SimpleObjectProperty<>();
    @Transient
    private final IntegerProperty popCid = new SimpleIntegerProperty();
    @Transient
    private final StringProperty regiao = new SimpleStringProperty();
    @Transient
    private final StringProperty codRep = new SimpleStringProperty();
    @Transient
    private final StringProperty classe = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> ipc = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty nomeRep = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<SdCidade> sdCidade = new SimpleObjectProperty<>();
    
    
    public Cidade() {
    }
    
    public Cidade(String codCid) {
        this.codCid.set(codCid);
    }
    
    @Id
    @Column(name = "COD_CID")
    public String getCodCid() {
        return codCid.get();
    }
    
    public StringProperty codCidProperty() {
        return codCid;
    }
    
    public void setCodCid(String codCid) {
        setCodigoFilter(codCid);
        this.codCid.set(codCid);
    }
    
    @Column(name = "NOME_CID")
    public String getNomeCid() {
        return nomeCid.get();
    }
    
    public StringProperty nomeCidProperty() {
        return nomeCid;
    }
    
    public void setNomeCid(String nomeCid) {
        this.nomeCid.set(nomeCid);
        setDescricaoFilter(nomeCid);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "COD_EST", referencedColumnName = "SIGLA_EST"),
            @JoinColumn(name = "COD_UF", referencedColumnName = "CODIGO")
    })
    public TabUf getCodEst() {
        return codEst.get();
    }
    
    public ObjectProperty<TabUf> codEstProperty() {
        return codEst;
    }
    
    public void setCodEst(TabUf codEst) {
        this.codEst.set(codEst);
    }
    
    @Column(name = "POP_CID")
    public Integer getPopCid() {
        return popCid.get();
    }
    
    public IntegerProperty popCidProperty() {
        return popCid;
    }
    
    public void setPopCid(Integer popCid) {
        this.popCid.set(popCid);
    }
    
    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }
    
    public StringProperty regiaoProperty() {
        return regiao;
    }
    
    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }
    
    @Column(name = "CODREP")
    public String getCodRep() {
        return codRep.get();
    }
    
    public StringProperty codRepProperty() {
        return codRep;
    }
    
    public void setCodRep(String codRep) {
        this.codRep.set(codRep);
    }
    
    @Column(name = "CLASSE")
    public String getClasse() {
        return classe.get();
    }
    
    public StringProperty classeProperty() {
        return classe;
    }
    
    public void setClasse(String classe) {
        this.classe.set(classe);
    }
    
    @Column(name = "IPC")
    public BigDecimal getIpc() {
        return ipc.get();
    }
    
    public ObjectProperty<BigDecimal> ipcProperty() {
        return ipc;
    }
    
    public void setIpc(BigDecimal ipc) {
        this.ipc.set(ipc);
    }
    
    @Column(name = "NOME_REP")
    public String getNomeRep() {
        return nomeRep.get();
    }
    
    public StringProperty nomeRepProperty() {
        return nomeRep;
    }
    
    public void setNomeRep(String nomeRep) {
        this.nomeRep.set(nomeRep);
    }
    
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "COD_CID")
    @Transient
    public SdCidade getSdCidade() {
        return sdCidade.get();
    }
    
    public ObjectProperty<SdCidade> sdCidadeProperty() {
        return sdCidade;
    }
    
    public void setSdCidade(SdCidade sdCidade) {
        this.sdCidade.set(sdCidade);
    }
    
    @Override
    public String toString() {
        return "[" + codCid.get() + "] " + nomeCid.get();
    }
}
