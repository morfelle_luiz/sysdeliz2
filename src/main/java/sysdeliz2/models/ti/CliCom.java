package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "CLI_COM_001")
public class CliCom {
    
    private final ObjectProperty<CliComPK> id = new SimpleObjectProperty<>();
    private final StringProperty marca = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<>();
    
    public CliCom() {
    }
    
    public CliCom(String codcli, String codrep, String marca, BigDecimal com1, BigDecimal com2) {
        this.id.set(new CliComPK(codcli, codrep));
        this.marca.set(marca);
        this.com1.set(com1);
        this.com2.set(com2);
    }
    
    @EmbeddedId
    public CliComPK getId() {
        return id.get();
    }
    
    public ObjectProperty<CliComPK> idProperty() {
        return id;
    }
    
    public void setId(CliComPK id) {
        this.id.set(id);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }
    
    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }
    
    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }
    
    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }
    
    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }
    
    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }
}
