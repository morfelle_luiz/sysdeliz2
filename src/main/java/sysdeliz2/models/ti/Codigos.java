package sysdeliz2.models.ti;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 01/08/2019 09:16
 */
@Entity
@Table(name = "CODIGOS_001")
public class Codigos {

    @EmbeddedId
    private CodigosId codigosId;

    @Column(name = "TAMANHO", insertable = false, updatable = false)
    private Integer tamanho;

    @Column(name = "PROXIMO")
    private BigDecimal proximo;

    public CodigosId getCodigosId() {
        return codigosId;
    }

    public void setCodigosId(CodigosId codigosId) {
        this.codigosId = codigosId;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public BigDecimal getProximo() {
        return proximo;
    }

    public void setProximo(BigDecimal proximo) {
        this.proximo = proximo;
    }
}
