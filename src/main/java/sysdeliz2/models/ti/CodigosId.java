package sysdeliz2.models.ti;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 01/08/2019 09:18
 */
@Embeddable
public class CodigosId implements Serializable {
    @Column(name = "TABELA")
    private String tabela;

    @Column(name = "CAMPO")
    private String campo;

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
