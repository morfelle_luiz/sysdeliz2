package sysdeliz2.models.ti;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 31/07/2019 15:23
 */
@Entity
@Table(name = "COLECAO_001")
@TelaSysDeliz(descricao = "Coleção", icon = "colecao (4).png")
public class Colecao extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final IntegerProperty indice = new SimpleIntegerProperty(this, "indice");
    @Transient
    private final StringProperty sequencia = new SimpleStringProperty(this, "sequencia");
    @Transient
    private final ObjectProperty<Date> inicioVig = new SimpleObjectProperty<>(this, "inicioVig");
    @Transient
    private final ObjectProperty<Date> fimVig = new SimpleObjectProperty<>(this, "fimVig");
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final StringProperty sdAtivo = new SimpleStringProperty(this, "sdAtivo");
    @Transient
    private final StringProperty sdAbreviacao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Colecao> colAnt = new SimpleObjectProperty<>();


    public Colecao() {
    }

    public Colecao(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    @Column(name = "INDICE")
    @JsonIgnore
    public int getIndice() {
        return indice.get();
    }

    @Column(name = "SEQUENCIA")
    @JsonIgnore
    public String getSequencia() {
        return sequencia.get();
    }

    @Column(name = "INICIO_VIG")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    public Date getInicioVig() {
        return inicioVig.get();
    }

    @Column(name = "FIM_VIG")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    public Date getFimVig() {
        return fimVig.get();
    }

    @Column(name = "SD_ATIVO", columnDefinition = "char")
    @JsonIgnore
    public String getSdAtivo() {
        return sdAtivo.get();
    }

    @Column(name = "SD_ABREVIACAO")
    public String getSdAbreviacao() {
        return sdAbreviacao.get();
    }

    @OneToOne
    @JoinColumn(name = "COL_ANT", referencedColumnName = "CODIGO")
    @JsonIgnore
    public Colecao getColAnt() {
        return colAnt.get();
    }


    public void setCodigo(String value) {
        setCodigoFilter(value);
        codigo.set(value);
    }

    public void setDescricao(String value) {
        setDescricaoFilter(value);
        descricao.set(value);
    }

    public void setIndice(int value) {
        indice.set(value);
    }

    public void setSequencia(String value) {
        sequencia.set(value);
    }

    public void setInicioVig(Date value) {
        inicioVig.set(value);
    }

    public void setFimVig(Date value) {
        fimVig.set(value);
    }

    public void setSdAtivo(String value) {
        sdAtivo.set(value);
    }

    public void setSdAbreviacao(String sdAbreviacao) {
        this.sdAbreviacao.set(sdAbreviacao);
    }

    public void setColAnt(Colecao colAnt) {
        this.colAnt.set(colAnt);
    }


    public final StringProperty codigoProperty() {
        return codigo;
    }

    public final StringProperty descricaoProperty() {
        return descricao;
    }

    public final IntegerProperty indiceProperty() {
        return indice;
    }

    public final StringProperty sequenciaProperty() {
        return sequencia;
    }

    public final ObjectProperty<Date> inicioVigProperty() {
        return inicioVig;
    }

    public final ObjectProperty<Date> fimVigProperty() {
        return fimVig;
    }

    public final StringProperty sdAtivoProperty() {
        return sdAtivo;
    }

    public StringProperty sdAbreviacaoProperty() {
        return sdAbreviacao;
    }

    public ObjectProperty<Colecao> colAntProperty() {
        return colAnt;
    }


    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Colecao colecao = (Colecao) o;
        return Objects.equals(codigo, colecao.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
