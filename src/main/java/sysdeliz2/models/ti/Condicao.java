package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "CONDICAO_001")
@TelaSysDeliz(descricao = "Condição de Pagamento", icon = "condicao pagamento_50.png")
public class Condicao extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codcond")
    private final IntegerProperty codcond = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 200)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "N. Parc.", width = 60)
    @ColunaFilter(descricao = "N. Parc.", coluna = "nrpar")
    private final IntegerProperty nrpar = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty diave = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "N. Dias", width = 60)
    @ColunaFilter(descricao = "N. Dias", coluna = "nrdia")
    private final IntegerProperty nrdia = new SimpleIntegerProperty();
    @Transient
    private final IntegerProperty entrada = new SimpleIntegerProperty();
    @Transient
    @ExibeTableView(descricao = "P. Inicial", width = 80)
    @ColunaFilter(descricao = "P. Inicial", coluna = "prazoini")
    private final IntegerProperty prazoini = new SimpleIntegerProperty();
    @Transient
    private final ObjectProperty<BigDecimal> taxa = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> descmax = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty tabela = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "P. Médio", width = 80)
    @ColunaFilter(descricao = "P. Médio", coluna = "prazoMedio")
    private final IntegerProperty prazoMedio = new SimpleIntegerProperty();
    
    public Condicao() {
    }
    
    @Id
    @Column(name = "CODCOND")
    public Integer getCodcond() {
        return codcond.get();
    }
    
    public IntegerProperty codcondProperty() {
        return codcond;
    }
    
    public void setCodcond(Integer codcond) {
        this.codcond.set(codcond);
        setCodigoFilter(codcond.toString());
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "NRPAR")
    public Integer getNrpar() {
        return nrpar.get();
    }
    
    public IntegerProperty nrparProperty() {
        return nrpar;
    }
    
    public void setNrpar(Integer nrpar) {
        this.nrpar.set(nrpar);
    }
    
    @Column(name = "DIAVE")
    public Integer getDiave() {
        return diave.get();
    }
    
    public IntegerProperty diaveProperty() {
        return diave;
    }
    
    public void setDiave(Integer diave) {
        this.diave.set(diave);
    }
    
    @Column(name = "NRDIA")
    public Integer getNrdia() {
        return nrdia.get();
    }
    
    public IntegerProperty nrdiaProperty() {
        return nrdia;
    }
    
    public void setNrdia(Integer nrdia) {
        this.nrdia.set(nrdia);
    }
    
    @Column(name = "ENTRADA")
    public Integer getEntrada() {
        return entrada.get();
    }
    
    public IntegerProperty entradaProperty() {
        return entrada;
    }
    
    public void setEntrada(Integer entrada) {
        this.entrada.set(entrada);
    }
    
    @Column(name = "PRAZOINI")
    public Integer getPrazoini() {
        return prazoini.get();
    }
    
    public IntegerProperty prazoiniProperty() {
        return prazoini;
    }
    
    public void setPrazoini(Integer prazoini) {
        this.prazoini.set(prazoini);
    }
    
    @Column(name = "TAXA")
    public BigDecimal getTaxa() {
        return taxa.get();
    }
    
    public ObjectProperty<BigDecimal> taxaProperty() {
        return taxa;
    }
    
    public void setTaxa(BigDecimal taxa) {
        this.taxa.set(taxa);
    }
    
    @Column(name = "DESCMAX")
    public BigDecimal getDescmax() {
        return descmax.get();
    }
    
    public ObjectProperty<BigDecimal> descmaxProperty() {
        return descmax;
    }
    
    public void setDescmax(BigDecimal descmax) {
        this.descmax.set(descmax);
    }
    
    @Column(name = "TABELA")
    public String getTabela() {
        return tabela.get();
    }
    
    public StringProperty tabelaProperty() {
        return tabela;
    }
    
    public void setTabela(String tabela) {
        this.tabela.set(tabela);
    }
    
    @Column(name = "PRAZO_MEDIO")
    public int getPrazoMedio() {
        return prazoMedio.get();
    }
    
    public IntegerProperty prazoMedioProperty() {
        return prazoMedio;
    }
    
    public void setPrazoMedio(int prazoMedio) {
        this.prazoMedio.set(prazoMedio);
    }
}