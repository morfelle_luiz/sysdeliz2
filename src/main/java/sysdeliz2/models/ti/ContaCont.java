package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONTA_CONT_001")
@TelaSysDeliz(descricao = "Conta Contábil", icon = "conta contabil (4).png")
public class ContaCont extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Conta", width = 80)
    @ColunaFilter(descricao = "Conta", coluna = "conta")
    private final StringProperty conta = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty operacao = new SimpleStringProperty();
    @Transient
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final StringProperty tpConta = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 35)
    private final StringProperty grupo = new SimpleStringProperty();
    @Transient
    private final StringProperty ctResult = new SimpleStringProperty();
    
    public ContaCont() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }
    
    @Column(name = "CONTA")
    public String getConta() {
        return conta.get();
    }
    
    public StringProperty contaProperty() {
        return conta;
    }
    
    public void setConta(String conta) {
        this.conta.set(conta);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "OPERACAO")
    public String getOperacao() {
        return operacao.get();
    }
    
    public StringProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(String operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "TP_CONTA")
    public String getTpConta() {
        return tpConta.get();
    }
    
    public StringProperty tpContaProperty() {
        return tpConta;
    }
    
    public void setTpConta(String tpConta) {
        this.tpConta.set(tpConta);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "CT_RESULT")
    public String getCtResult() {
        return ctResult.get();
    }
    
    public StringProperty ctResultProperty() {
        return ctResult;
    }
    
    public void setCtResult(String ctResult) {
        this.ctResult.set(ctResult);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
