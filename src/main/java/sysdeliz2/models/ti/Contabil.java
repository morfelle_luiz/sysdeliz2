package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "CONTABIL_001")
public class Contabil {
    
    private final ObjectProperty<ContabilPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty operacao = new SimpleStringProperty();
    private final StringProperty contac = new SimpleStringProperty();
    private final StringProperty contad = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty docto = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> datalan = new SimpleObjectProperty<LocalDate>();
    private final StringProperty contar = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    
    public Contabil() {
    }
    
    @EmbeddedId
    public ContabilPK getId() {
        return id.get();
    }
    
    public ObjectProperty<ContabilPK> idProperty() {
        return id;
    }
    
    public void setId(ContabilPK id) {
        this.id.set(id);
    }
    
    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }
    
    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }
    
    public void setData(LocalDate data) {
        this.data.set(data);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "OPERACAO")
    public String getOperacao() {
        return operacao.get();
    }
    
    public StringProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(String operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name = "CONTA_C")
    public String getContac() {
        return contac.get();
    }
    
    public StringProperty contacProperty() {
        return contac;
    }
    
    public void setContac(String contac) {
        this.contac.set(contac);
    }
    
    @Column(name = "CONTA_D")
    public String getContad() {
        return contad.get();
    }
    
    public StringProperty contadProperty() {
        return contad;
    }
    
    public void setContad(String contad) {
        this.contad.set(contad);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }
    
    public StringProperty doctoProperty() {
        return docto;
    }
    
    public void setDocto(String docto) {
        this.docto.set(docto);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "DATA_LAN")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDatalan() {
        return datalan.get();
    }
    
    public ObjectProperty<LocalDate> datalanProperty() {
        return datalan;
    }
    
    public void setDatalan(LocalDate datalan) {
        this.datalan.set(datalan);
    }
    
    @Column(name = "CONTA_R")
    public String getContar() {
        return contar.get();
    }
    
    public StringProperty contarProperty() {
        return contar;
    }
    
    public void setContar(String contar) {
        this.contar.set(contar);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
}