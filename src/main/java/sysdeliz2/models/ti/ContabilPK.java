package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ContabilPK implements Serializable {
    
    private final StringProperty lancamento = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    
    public ContabilPK() {
    }
    
    @Column(name = "LANCAMENTO")
    public String getLancamento() {
        return lancamento.get();
    }
    
    public StringProperty lancamentoProperty() {
        return lancamento;
    }
    
    public void setLancamento(String lancamento) {
        this.lancamento.set(lancamento);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
}
