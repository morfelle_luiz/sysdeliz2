package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdTipoContato001;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "CONTATO_001")
public class Contato implements Serializable {
    
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final ObjectProperty<SdTipoContato001> tipo = new SimpleObjectProperty<>();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty fone = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtNasc = new SimpleObjectProperty<>();
    private final IntegerProperty codcli = new SimpleIntegerProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty texto = new SimpleStringProperty();
    
    public Contato() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SD_SEQ_CONTATO_001")
    @SequenceGenerator(name = "SD_SEQ_CONTATO_001", sequenceName = "SD_SEQ_CONTATO", allocationSize = 1)
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TIPO")
    public SdTipoContato001 getTipo() {
        return tipo.get();
    }
    
    public ObjectProperty<SdTipoContato001> tipoProperty() {
        return tipo;
    }
    
    public void setTipo(SdTipoContato001 tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "FONE")
    public String getFone() {
        return fone.get();
    }
    
    public StringProperty foneProperty() {
        return fone;
    }
    
    public void setFone(String fone) {
        this.fone.set(fone);
    }
    
    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email.set(email);
    }
    
    @Column(name = "DT_NASC")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtNasc() {
        return dtNasc.get();
    }
    
    public ObjectProperty<LocalDate> dtNascProperty() {
        return dtNasc;
    }
    
    public void setDtNasc(LocalDate dtNasc) {
        this.dtNasc.set(dtNasc);
    }
    
    @Column(name = "CODCLI")
    @Convert(converter = CodcliAttributeConverter.class)
    public Integer getCodcli() {
        return codcli.get();
    }
    
    public IntegerProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Integer codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }
    
    @Lob
    @Column(name = "TEXTO")
    public String getTexto() {
        return texto.get();
    }
    
    public StringProperty textoProperty() {
        return texto;
    }
    
    public void setTexto(String texto) {
        this.texto.set(texto);
    }
    
    @Transient
    public String toLog() {
        return '{' +
                "codigo=" + codigo.get() +
                ", tipo=" + tipo.get() +
                ", nome=" + nome.get() +
                ", fone=" + fone.get() +
                ", email=" + email.get() +
                ", dtNasc=" + dtNasc.get() +
                ", codcli=" + codcli.get() +
                ", obs=" + obs.get() +
                ", texto=" + texto.get() +
                '}';
    }
}
