package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 18/09/2019 17:10
 */
@Entity
@Table(name = "CADCOR_001")
@TelaSysDeliz(descricao = "Cores", icon = "cor_50.png")
public class Cor extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "cor")
    private final StringProperty cor = new SimpleStringProperty(this, "cor");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final StringProperty grupo = new SimpleStringProperty(this, "grupo");
    @Transient
    private final StringProperty pantone = new SimpleStringProperty(this, "pantone");
    @Transient
    private final StringProperty corbase = new SimpleStringProperty(this, "corbase");

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
        setCodigoFilter(cor);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "PANTONE")
    public String getPantone() {
        return pantone.get();
    }

    public StringProperty pantoneProperty() {
        return pantone;
    }

    public void setPantone(String pantone) {
        this.pantone.set(pantone);
    }

    @Column(name = "CORBASE")
    public String getCorbase() {
        return corbase.get();
    }

    public StringProperty corbaseProperty() {
        return corbase;
    }

    public void setCorbase(String corbase) {
        this.corbase.set(corbase);
    }
    
    @Override
    public String toString() {
        return "["+this.cor.get()+"] "+this.descricao.get();
    }
}
