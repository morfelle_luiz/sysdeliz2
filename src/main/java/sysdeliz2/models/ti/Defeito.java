package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "DEFEITO_001")
@Entity
@TelaSysDeliz(descricao = "Defeitos", icon = "defeito produto_100.png")
public class Defeito extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 150)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    @Transient
    private final StringProperty codigo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    @Transient
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty qualidade = new SimpleStringProperty();
    @Transient
    private final StringProperty setor = new SimpleStringProperty();
    @Transient
    private final StringProperty segunda = new SimpleStringProperty();
    @Transient
    private final StringProperty desconto = new SimpleStringProperty();
    @Transient
    private final StringProperty reproc = new SimpleStringProperty();
    @ExibeTableView(descricao = "Grupo", width = 200)
    @ColunaFilter(descricao = "Grupo", coluna = "grupo")
    @Transient
    private final StringProperty grupo = new SimpleStringProperty();
    @Transient
    private final StringProperty complemento = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> valdesc = new SimpleObjectProperty<BigDecimal>();

    public Defeito() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "QUALIDADE")
    public String getQualidade() {
        return qualidade.get();
    }

    public StringProperty qualidadeProperty() {
        return qualidade;
    }

    public void setQualidade(String qualidade) {
        this.qualidade.set(qualidade);
    }

    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Column(name = "SEGUNDA")
    public String getSegunda() {
        return segunda.get();
    }

    public StringProperty segundaProperty() {
        return segunda;
    }

    public void setSegunda(String segunda) {
        this.segunda.set(segunda);
    }

    @Column(name = "DESCONTO")
    public String getDesconto() {
        return desconto.get();
    }

    public StringProperty descontoProperty() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "REPROC")
    public String getReproc() {
        return reproc.get();
    }

    public StringProperty reprocProperty() {
        return reproc;
    }

    public void setReproc(String reproc) {
        this.reproc.set(reproc);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "COMPLEMENTO")
    public String getComplemento() {
        return complemento.get();
    }

    public StringProperty complementoProperty() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento.set(complemento);
    }

    @Column(name = "VAL_DESC")
    public BigDecimal getValdesc() {
        return valdesc.get();
    }

    public ObjectProperty<BigDecimal> valdescProperty() {
        return valdesc;
    }

    public void setValdesc(BigDecimal valdesc) {
        this.valdesc.set(valdesc);
    }

}
