package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;

@Entity
@Table(name = "DEPOSITO_001")
@TelaSysDeliz(descricao = "Depósitos", icon = "deposito_50.png")
public class Deposito extends BasicModel {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty prioridade = new SimpleStringProperty();
    @Transient
    private final StringProperty codcli = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "País", width = 50)
    @ColunaFilter(descricao = "País", coluna = "pais")
    private final StringProperty pais = new SimpleStringProperty();
    @Transient
    private final StringProperty posse = new SimpleStringProperty();
    @Transient
    private final StringProperty coddep = new SimpleStringProperty();
    @Transient
    private final StringProperty tipest = new SimpleStringProperty();
    @Transient
    private final StringProperty tipoMat = new SimpleStringProperty();
    @Transient
    private final StringProperty grupoDep = new SimpleStringProperty();
    
    public Deposito() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
        
    }
    
    @Column(name = "PRIORIDADE")
    public String getPrioridade() {
        return prioridade.get();
    }
    
    public StringProperty prioridadeProperty() {
        return prioridade;
    }
    
    public void setPrioridade(String prioridade) {
        this.prioridade.set(prioridade);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @Column(name = "POSSE")
    public String getPosse() {
        return posse.get();
    }
    
    public StringProperty posseProperty() {
        return posse;
    }
    
    public void setPosse(String posse) {
        this.posse.set(posse);
    }
    
    @Column(name = "CODDEP")
    public String getCoddep() {
        return coddep.get();
    }
    
    public StringProperty coddepProperty() {
        return coddep;
    }
    
    public void setCoddep(String coddep) {
        this.coddep.set(coddep);
    }
    
    @Column(name = "TIPEST")
    public String getTipest() {
        return tipest.get();
    }
    
    public StringProperty tipestProperty() {
        return tipest;
    }
    
    public void setTipest(String tipest) {
        this.tipest.set(tipest);
    }
    
    @Column(name = "TIPO_MAT")
    public String getTipoMat() {
        return tipoMat.get();
    }
    
    public StringProperty tipoMatProperty() {
        return tipoMat;
    }
    
    public void setTipoMat(String tipoMat) {
        this.tipoMat.set(tipoMat);
    }
    
    @Column(name = "GRUPO_DEP")
    public String getGrupoDep() {
        return grupoDep.get();
    }
    
    public StringProperty grupoDepProperty() {
        return grupoDep;
    }
    
    public void setGrupoDep(String grupoDep) {
        this.grupoDep.set(grupoDep);
    }
    
    @Override
    public String toString() {
        return "[" + this.codigo.get() + "] " + this.descricao.get();
    }
}


