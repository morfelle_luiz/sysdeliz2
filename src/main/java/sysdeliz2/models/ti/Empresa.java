package sysdeliz2.models.ti;

import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "EMPRESA")
public class Empresa {
    
    private final ObjectProperty<BigDecimal> empi1 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empi2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> emptaxa = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empminfat = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empvalnota = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> empdata = new SimpleObjectProperty<LocalDate>();
    private final StringProperty empartigo = new SimpleStringProperty();
    private final StringProperty empqualidade = new SimpleStringProperty();
    private final StringProperty empbaixa = new SimpleStringProperty();
    private final StringProperty empestado = new SimpleStringProperty();
    private final StringProperty empcep = new SimpleStringProperty();
    private final StringProperty emptelefone = new SimpleStringProperty();
    private final StringProperty empfax = new SimpleStringProperty();
    private final StringProperty emppat = new SimpleStringProperty();
    private final StringProperty empcnpj = new SimpleStringProperty();
    private final StringProperty empinscricao = new SimpleStringProperty();
    private final StringProperty empcidade = new SimpleStringProperty();
    private final StringProperty empemail = new SimpleStringProperty();
    private final StringProperty empendereco = new SimpleStringProperty();
    private final StringProperty empnome = new SimpleStringProperty();
    private final StringProperty empobs = new SimpleStringProperty();
    private final StringProperty empsituacao = new SimpleStringProperty();
    private final StringProperty empespecie = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> emparquivo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty empserienf = new SimpleStringProperty();
    private final StringProperty empnumero = new SimpleStringProperty();
    private final StringProperty empbairro = new SimpleStringProperty();
    private final StringProperty empresponsavel = new SimpleStringProperty();
    private final StringProperty empbanco = new SimpleStringProperty();
    private final StringProperty empdemo = new SimpleStringProperty();
    private final StringProperty empduplidemo = new SimpleStringProperty();
    private final IntegerProperty empindice = new SimpleIntegerProperty();
    private final StringProperty emplogo = new SimpleStringProperty();
    private final StringProperty emptiponf = new SimpleStringProperty();
    private final StringProperty empcontribuinte = new SimpleStringProperty();
    private final StringProperty empcontrserie = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> empdtcont = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> empdtcaixa = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> emppis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empcofins = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empimpesp = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty empmesesp = new SimpleStringProperty();
    private final StringProperty empcert = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> empirpj = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> empcontrsoc = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty empsepdupli = new SimpleStringProperty();
    private final StringProperty empemailsenha = new SimpleStringProperty();
    private final StringProperty empemailserv = new SimpleStringProperty();
    private final StringProperty empambnfe = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> empdtinv = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty empemailporta = new SimpleIntegerProperty();
    private final StringProperty empcor = new SimpleStringProperty();
    private final StringProperty empinscrmunicipal = new SimpleStringProperty();
    private final StringProperty faixaean13 = new SimpleStringProperty();
    private final StringProperty empean13 = new SimpleStringProperty();
    private final StringProperty empregimetrib = new SimpleStringProperty();
    private final IntegerProperty codemp = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> empdtfiscal = new SimpleObjectProperty<LocalDate>();
    
    public Empresa() {
    }
    
    @Column(name = "EMP_I_1")
    public BigDecimal getEmpi1() {
        return empi1.get();
    }
    
    public ObjectProperty<BigDecimal> empi1Property() {
        return empi1;
    }
    
    public void setEmpi1(BigDecimal empi1) {
        this.empi1.set(empi1);
    }
    
    @Column(name = "EMP_I_2")
    public BigDecimal getEmpi2() {
        return empi2.get();
    }
    
    public ObjectProperty<BigDecimal> empi2Property() {
        return empi2;
    }
    
    public void setEmpi2(BigDecimal empi2) {
        this.empi2.set(empi2);
    }
    
    @Column(name = "EMP_TAXA")
    public BigDecimal getEmptaxa() {
        return emptaxa.get();
    }
    
    public ObjectProperty<BigDecimal> emptaxaProperty() {
        return emptaxa;
    }
    
    public void setEmptaxa(BigDecimal emptaxa) {
        this.emptaxa.set(emptaxa);
    }
    
    @Column(name = "EMP_MINFAT")
    public BigDecimal getEmpminfat() {
        return empminfat.get();
    }
    
    public ObjectProperty<BigDecimal> empminfatProperty() {
        return empminfat;
    }
    
    public void setEmpminfat(BigDecimal empminfat) {
        this.empminfat.set(empminfat);
    }
    
    @Column(name = "EMP_VAL_NOTA")
    public BigDecimal getEmpvalnota() {
        return empvalnota.get();
    }
    
    public ObjectProperty<BigDecimal> empvalnotaProperty() {
        return empvalnota;
    }
    
    public void setEmpvalnota(BigDecimal empvalnota) {
        this.empvalnota.set(empvalnota);
    }
    
    @Column(name = "EMP_DATA")
    public LocalDate getEmpdata() {
        return empdata.get();
    }
    
    public ObjectProperty<LocalDate> empdataProperty() {
        return empdata;
    }
    
    public void setEmpdata(LocalDate empdata) {
        this.empdata.set(empdata);
    }
    
    @Column(name = "EMP_ARTIGO")
    public String getEmpartigo() {
        return empartigo.get();
    }
    
    public StringProperty empartigoProperty() {
        return empartigo;
    }
    
    public void setEmpartigo(String empartigo) {
        this.empartigo.set(empartigo);
    }
    
    @Column(name = "EMP_QUALIDADE")
    public String getEmpqualidade() {
        return empqualidade.get();
    }
    
    public StringProperty empqualidadeProperty() {
        return empqualidade;
    }
    
    public void setEmpqualidade(String empqualidade) {
        this.empqualidade.set(empqualidade);
    }
    
    @Column(name = "EMP_BAIXA")
    public String getEmpbaixa() {
        return empbaixa.get();
    }
    
    public StringProperty empbaixaProperty() {
        return empbaixa;
    }
    
    public void setEmpbaixa(String empbaixa) {
        this.empbaixa.set(empbaixa);
    }
    
    @Column(name = "EMP_ESTADO")
    public String getEmpestado() {
        return empestado.get();
    }
    
    public StringProperty empestadoProperty() {
        return empestado;
    }
    
    public void setEmpestado(String empestado) {
        this.empestado.set(empestado);
    }
    
    @Column(name = "EMP_CEP")
    public String getEmpcep() {
        return empcep.get();
    }
    
    public StringProperty empcepProperty() {
        return empcep;
    }
    
    public void setEmpcep(String empcep) {
        this.empcep.set(empcep);
    }
    
    @Column(name = "EMP_TELEFONE")
    public String getEmptelefone() {
        return emptelefone.get();
    }
    
    public StringProperty emptelefoneProperty() {
        return emptelefone;
    }
    
    public void setEmptelefone(String emptelefone) {
        this.emptelefone.set(emptelefone);
    }
    
    @Column(name = "EMP_FAX")
    public String getEmpfax() {
        return empfax.get();
    }
    
    public StringProperty empfaxProperty() {
        return empfax;
    }
    
    public void setEmpfax(String empfax) {
        this.empfax.set(empfax);
    }
    
    @Id
    @Column(name = "EMP_PAT")
    public String getEmppat() {
        return emppat.get();
    }
    
    public StringProperty emppatProperty() {
        return emppat;
    }
    
    public void setEmppat(String emppat) {
        this.emppat.set(emppat);
    }
    
    @Column(name = "EMP_CNPJ")
    public String getEmpcnpj() {
        return empcnpj.get();
    }
    
    public StringProperty empcnpjProperty() {
        return empcnpj;
    }
    
    public void setEmpcnpj(String empcnpj) {
        this.empcnpj.set(empcnpj);
    }
    
    @Column(name = "EMP_INSCRICAO")
    public String getEmpinscricao() {
        return empinscricao.get();
    }
    
    public StringProperty empinscricaoProperty() {
        return empinscricao;
    }
    
    public void setEmpinscricao(String empinscricao) {
        this.empinscricao.set(empinscricao);
    }
    
    @Column(name = "EMP_CIDADE")
    public String getEmpcidade() {
        return empcidade.get();
    }
    
    public StringProperty empcidadeProperty() {
        return empcidade;
    }
    
    public void setEmpcidade(String empcidade) {
        this.empcidade.set(empcidade);
    }
    
    @Column(name = "EMP_EMAIL")
    public String getEmpemail() {
        return empemail.get();
    }
    
    public StringProperty empemailProperty() {
        return empemail;
    }
    
    public void setEmpemail(String empemail) {
        this.empemail.set(empemail);
    }
    
    @Column(name = "EMP_ENDERECO")
    public String getEmpendereco() {
        return empendereco.get();
    }
    
    public StringProperty empenderecoProperty() {
        return empendereco;
    }
    
    public void setEmpendereco(String empendereco) {
        this.empendereco.set(empendereco);
    }
    
    @Column(name = "EMP_NOME")
    public String getEmpnome() {
        return empnome.get();
    }
    
    public StringProperty empnomeProperty() {
        return empnome;
    }
    
    public void setEmpnome(String empnome) {
        this.empnome.set(empnome);
    }
    
    @Column(name = "EMP_OBS")
    public String getEmpobs() {
        return empobs.get();
    }
    
    public StringProperty empobsProperty() {
        return empobs;
    }
    
    public void setEmpobs(String empobs) {
        this.empobs.set(empobs);
    }
    
    @Column(name = "EMP_SITUACAO")
    public String getEmpsituacao() {
        return empsituacao.get();
    }
    
    public StringProperty empsituacaoProperty() {
        return empsituacao;
    }
    
    public void setEmpsituacao(String empsituacao) {
        this.empsituacao.set(empsituacao);
    }
    
    @Column(name = "EMP_ESPECIE")
    public String getEmpespecie() {
        return empespecie.get();
    }
    
    public StringProperty empespecieProperty() {
        return empespecie;
    }
    
    public void setEmpespecie(String empespecie) {
        this.empespecie.set(empespecie);
    }
    
    @Column(name = "EMP_ARQUIVO")
    public BigDecimal getEmparquivo() {
        return emparquivo.get();
    }
    
    public ObjectProperty<BigDecimal> emparquivoProperty() {
        return emparquivo;
    }
    
    public void setEmparquivo(BigDecimal emparquivo) {
        this.emparquivo.set(emparquivo);
    }
    
    @Column(name = "EMP_SERIENF")
    public String getEmpserienf() {
        return empserienf.get();
    }
    
    public StringProperty empserienfProperty() {
        return empserienf;
    }
    
    public void setEmpserienf(String empserienf) {
        this.empserienf.set(empserienf);
    }
    
    @Column(name = "EMP_NUMERO")
    public String getEmpnumero() {
        return empnumero.get();
    }
    
    public StringProperty empnumeroProperty() {
        return empnumero;
    }
    
    public void setEmpnumero(String empnumero) {
        this.empnumero.set(empnumero);
    }
    
    @Column(name = "EMP_BAIRRO")
    public String getEmpbairro() {
        return empbairro.get();
    }
    
    public StringProperty empbairroProperty() {
        return empbairro;
    }
    
    public void setEmpbairro(String empbairro) {
        this.empbairro.set(empbairro);
    }
    
    @Column(name = "EMP_RESPONSAVEL")
    public String getEmpresponsavel() {
        return empresponsavel.get();
    }
    
    public StringProperty empresponsavelProperty() {
        return empresponsavel;
    }
    
    public void setEmpresponsavel(String empresponsavel) {
        this.empresponsavel.set(empresponsavel);
    }
    
    @Column(name = "EMP_BANCO")
    public String getEmpbanco() {
        return empbanco.get();
    }
    
    public StringProperty empbancoProperty() {
        return empbanco;
    }
    
    public void setEmpbanco(String empbanco) {
        this.empbanco.set(empbanco);
    }
    
    @Column(name = "EMP_DEMO")
    public String getEmpdemo() {
        return empdemo.get();
    }
    
    public StringProperty empdemoProperty() {
        return empdemo;
    }
    
    public void setEmpdemo(String empdemo) {
        this.empdemo.set(empdemo);
    }
    
    @Column(name = "EMP_DUPLI_DEMO")
    public String getEmpduplidemo() {
        return empduplidemo.get();
    }
    
    public StringProperty empduplidemoProperty() {
        return empduplidemo;
    }
    
    public void setEmpduplidemo(String empduplidemo) {
        this.empduplidemo.set(empduplidemo);
    }
    
    @Column(name = "EMP_INDICE")
    public Integer getEmpindice() {
        return empindice.get();
    }
    
    public IntegerProperty empindiceProperty() {
        return empindice;
    }
    
    public void setEmpindice(Integer empindice) {
        this.empindice.set(empindice);
    }
    
    @Column(name = "EMP_LOGO")
    @Lob
    public String getEmplogo() {
        return emplogo.get();
    }
    
    public StringProperty emplogoProperty() {
        return emplogo;
    }
    
    public void setEmplogo(String emplogo) {
        this.emplogo.set(emplogo);
    }
    
    @Column(name = "EMP_TIPONF")
    public String getEmptiponf() {
        return emptiponf.get();
    }
    
    public StringProperty emptiponfProperty() {
        return emptiponf;
    }
    
    public void setEmptiponf(String emptiponf) {
        this.emptiponf.set(emptiponf);
    }
    
    @Column(name = "EMP_CONTRIBUINTE")
    public String getEmpcontribuinte() {
        return empcontribuinte.get();
    }
    
    public StringProperty empcontribuinteProperty() {
        return empcontribuinte;
    }
    
    public void setEmpcontribuinte(String empcontribuinte) {
        this.empcontribuinte.set(empcontribuinte);
    }
    
    @Column(name = "EMP_CONTR_SERIE")
    public String getEmpcontrserie() {
        return empcontrserie.get();
    }
    
    public StringProperty empcontrserieProperty() {
        return empcontrserie;
    }
    
    public void setEmpcontrserie(String empcontrserie) {
        this.empcontrserie.set(empcontrserie);
    }
    
    @Column(name = "EMP_DTCONT")
    public LocalDate getEmpdtcont() {
        return empdtcont.get();
    }
    
    public ObjectProperty<LocalDate> empdtcontProperty() {
        return empdtcont;
    }
    
    public void setEmpdtcont(LocalDate empdtcont) {
        this.empdtcont.set(empdtcont);
    }
    
    @Column(name = "EMP_DTCAIXA")
    public LocalDate getEmpdtcaixa() {
        return empdtcaixa.get();
    }
    
    public ObjectProperty<LocalDate> empdtcaixaProperty() {
        return empdtcaixa;
    }
    
    public void setEmpdtcaixa(LocalDate empdtcaixa) {
        this.empdtcaixa.set(empdtcaixa);
    }
    
    @Column(name = "EMP_PIS")
    public BigDecimal getEmppis() {
        return emppis.get();
    }
    
    public ObjectProperty<BigDecimal> emppisProperty() {
        return emppis;
    }
    
    public void setEmppis(BigDecimal emppis) {
        this.emppis.set(emppis);
    }
    
    @Column(name = "EMP_COFINS")
    public BigDecimal getEmpcofins() {
        return empcofins.get();
    }
    
    public ObjectProperty<BigDecimal> empcofinsProperty() {
        return empcofins;
    }
    
    public void setEmpcofins(BigDecimal empcofins) {
        this.empcofins.set(empcofins);
    }
    
    @Column(name = "EMP_IMP_ESP")
    public BigDecimal getEmpimpesp() {
        return empimpesp.get();
    }
    
    public ObjectProperty<BigDecimal> empimpespProperty() {
        return empimpesp;
    }
    
    public void setEmpimpesp(BigDecimal empimpesp) {
        this.empimpesp.set(empimpesp);
    }
    
    @Column(name = "EMP_MES_ESP")
    public String getEmpmesesp() {
        return empmesesp.get();
    }
    
    public StringProperty empmesespProperty() {
        return empmesesp;
    }
    
    public void setEmpmesesp(String empmesesp) {
        this.empmesesp.set(empmesesp);
    }
    
    @Column(name = "EMP_CERT")
    public String getEmpcert() {
        return empcert.get();
    }
    
    public StringProperty empcertProperty() {
        return empcert;
    }
    
    public void setEmpcert(String empcert) {
        this.empcert.set(empcert);
    }
    
    @Column(name = "EMP_IRPJ")
    public BigDecimal getEmpirpj() {
        return empirpj.get();
    }
    
    public ObjectProperty<BigDecimal> empirpjProperty() {
        return empirpj;
    }
    
    public void setEmpirpj(BigDecimal empirpj) {
        this.empirpj.set(empirpj);
    }
    
    @Column(name = "EMP_CONTR_SOC")
    public BigDecimal getEmpcontrsoc() {
        return empcontrsoc.get();
    }
    
    public ObjectProperty<BigDecimal> empcontrsocProperty() {
        return empcontrsoc;
    }
    
    public void setEmpcontrsoc(BigDecimal empcontrsoc) {
        this.empcontrsoc.set(empcontrsoc);
    }
    
    @Column(name = "EMP_SEP_DUPLI")
    public String getEmpsepdupli() {
        return empsepdupli.get();
    }
    
    public StringProperty empsepdupliProperty() {
        return empsepdupli;
    }
    
    public void setEmpsepdupli(String empsepdupli) {
        this.empsepdupli.set(empsepdupli);
    }
    
    @Column(name = "EMP_EMAIL_SENHA")
    public String getEmpemailsenha() {
        return empemailsenha.get();
    }
    
    public StringProperty empemailsenhaProperty() {
        return empemailsenha;
    }
    
    public void setEmpemailsenha(String empemailsenha) {
        this.empemailsenha.set(empemailsenha);
    }
    
    @Column(name = "EMP_EMAIL_SERV")
    public String getEmpemailserv() {
        return empemailserv.get();
    }
    
    public StringProperty empemailservProperty() {
        return empemailserv;
    }
    
    public void setEmpemailserv(String empemailserv) {
        this.empemailserv.set(empemailserv);
    }
    
    @Column(name = "EMP_AMB_NFE")
    public String getEmpambnfe() {
        return empambnfe.get();
    }
    
    public StringProperty empambnfeProperty() {
        return empambnfe;
    }
    
    public void setEmpambnfe(String empambnfe) {
        this.empambnfe.set(empambnfe);
    }
    
    @Column(name = "EMP_DTINV")
    public LocalDate getEmpdtinv() {
        return empdtinv.get();
    }
    
    public ObjectProperty<LocalDate> empdtinvProperty() {
        return empdtinv;
    }
    
    public void setEmpdtinv(LocalDate empdtinv) {
        this.empdtinv.set(empdtinv);
    }
    
    @Column(name = "EMP_EMAIL_PORTA")
    public Integer getEmpemailporta() {
        return empemailporta.get();
    }
    
    public IntegerProperty empemailportaProperty() {
        return empemailporta;
    }
    
    public void setEmpemailporta(Integer empemailporta) {
        this.empemailporta.set(empemailporta);
    }
    
    @Column(name = "EMP_COR")
    public String getEmpcor() {
        return empcor.get();
    }
    
    public StringProperty empcorProperty() {
        return empcor;
    }
    
    public void setEmpcor(String empcor) {
        this.empcor.set(empcor);
    }
    
    @Column(name = "EMP_INSCR_MUNICIPAL")
    public String getEmpinscrmunicipal() {
        return empinscrmunicipal.get();
    }
    
    public StringProperty empinscrmunicipalProperty() {
        return empinscrmunicipal;
    }
    
    public void setEmpinscrmunicipal(String empinscrmunicipal) {
        this.empinscrmunicipal.set(empinscrmunicipal);
    }
    
    @Column(name = "FAIXA_EAN13")
    public String getFaixaean13() {
        return faixaean13.get();
    }
    
    public StringProperty faixaean13Property() {
        return faixaean13;
    }
    
    public void setFaixaean13(String faixaean13) {
        this.faixaean13.set(faixaean13);
    }
    
    @Column(name = "EMP_EAN13")
    public String getEmpean13() {
        return empean13.get();
    }
    
    public StringProperty empean13Property() {
        return empean13;
    }
    
    public void setEmpean13(String empean13) {
        this.empean13.set(empean13);
    }
    
    @Column(name = "EMP_REGIME_TRIB")
    public String getEmpregimetrib() {
        return empregimetrib.get();
    }
    
    public StringProperty empregimetribProperty() {
        return empregimetrib;
    }
    
    public void setEmpregimetrib(String empregimetrib) {
        this.empregimetrib.set(empregimetrib);
    }
    
    @Column(name = "CODEMP")
    public Integer getCodemp() {
        return codemp.get();
    }
    
    public IntegerProperty codempProperty() {
        return codemp;
    }
    
    public void setCodemp(Integer codemp) {
        this.codemp.set(codemp);
    }
    
    @Column(name = "EMP_DTFISCAL")
    public LocalDate getEmpdtfiscal() {
        return empdtfiscal.get();
    }
    
    public ObjectProperty<LocalDate> empdtfiscalProperty() {
        return empdtfiscal;
    }
    
    public void setEmpdtfiscal(LocalDate empdtfiscal) {
        this.empdtfiscal.set(empdtfiscal);
    }
    
}
