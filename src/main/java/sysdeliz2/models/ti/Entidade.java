package sysdeliz2.models.ti;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kotlin.Suppress;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import sysdeliz2.daos.generics.helpers.BooleanSimNaoConverter;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdEntidade;
import sysdeliz2.models.sysdeliz.SdMarcasEntidade;
import sysdeliz2.models.sysdeliz.SdPagadorFrete001;
import sysdeliz2.models.sysdeliz.SdTipoTributacao001;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;
import sysdeliz2.utils.validator.bean.annotation.CpfOuCnpj;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 31/07/2019 15:23
 */
@Entity
@Table(name = "ENTIDADE_001")
@Suppress(names = "unused")
@TelaSysDeliz(descricao = "Entidade", icon = "cliente (4).png")
public class Entidade extends BasicModel implements Serializable {
    
    //<editor-fold desc="JavaFX properties">
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codcli")
    private final IntegerProperty codcli = new SimpleIntegerProperty(this, "codcli");
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty(this, "nome");
    @Transient
    private final StringProperty inscricao = new SimpleStringProperty(this, "inscricao");
    @Transient
    @ExibeTableView(descricao = "CNPJ", width = 80)
    @ColunaFilter(descricao = "CNPJ", coluna = "cnpj")
    private final StringProperty cnpj = new SimpleStringProperty(this, "cnpj");
    @Transient
    private final StringProperty endereco = new SimpleStringProperty(this, "endereco");
    
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(this, "ativo");
    
    @Transient
    private final StringProperty bairro = new SimpleStringProperty(this, "bairro");
    @Transient
    private final ObjectProperty<CadBan> banco = new SimpleObjectProperty<>(this, "banco");
    
    @Transient
    private final BooleanProperty bloqueio = new SimpleBooleanProperty(this, "bloqueio");
    
    @Transient
    private final ObjectProperty<CadCep> cep = new SimpleObjectProperty<>(this, "cep");
    
    @Transient
    private final ObjectProperty<CadCep> cepCob = new SimpleObjectProperty<>(this, "cepCob");
    @Transient
    private final ObjectProperty<CadCep> cepEnt = new SimpleObjectProperty<>(this, "cepEnt");
    @Transient
    private final StringProperty cnpjCob = new SimpleStringProperty(this, "cnpjCob");
    @Transient
    private final StringProperty cnpjEnt = new SimpleStringProperty(this, "cnpjEnt");
    @Transient
    private final StringProperty codrep = new SimpleStringProperty(this, "codrep");
    @Transient
    private final StringProperty condicao = new SimpleStringProperty(this, "condicao");
    
    @Transient
    private final ObjectProperty<BigDecimal> credito = new SimpleObjectProperty<>(this, "credito");
    
    @Transient
    private final ObjectProperty<LocalDate> dataAcumulo = new SimpleObjectProperty<>(this, "dataAcumulo");
    @Transient
    private final ObjectProperty<LocalDate> dataCad = new SimpleObjectProperty<>(this, "dataCad");
    @Transient
    private final ObjectProperty<LocalDate> dtNasc = new SimpleObjectProperty<>(this, "dtNasc");
    @Transient
    private final ObjectProperty<LocalDate> dtUltimoFat = new SimpleObjectProperty<>(this, "dtUltimoFat");
    
    @Transient
    private final StringProperty email = new SimpleStringProperty(this, "email");
    @Transient
    private final StringProperty endCob = new SimpleStringProperty(this, "endCob");
    @Transient
    private final StringProperty endEnt = new SimpleStringProperty(this, "endEnt");
    @Transient
    private final StringProperty fax = new SimpleStringProperty(this, "fax");
    
    @Transient
    private final ObjectProperty<GrupoCli> grupo = new SimpleObjectProperty<>(this, "grupo");
    
    @Transient
    private final StringProperty inscCob = new SimpleStringProperty(this, "inscCob");
    @Transient
    private final StringProperty inscEnt = new SimpleStringProperty(this, "inscEnt");
    
    @Transient
    private final ObjectProperty<BigDecimal> maiorAcumulo = new SimpleObjectProperty<>(this, "maiorAcumulo");
    
    @Transient
    private final ObjectProperty<Natureza> natureza = new SimpleObjectProperty<>(this, "natureza");
    @Transient
    private final StringProperty nrloja = new SimpleStringProperty(this, "nrloja");
    
    @Transient
    private final ObjectProperty<SitCli> sitCli = new SimpleObjectProperty<>(this, "sitCSli");
    
    @Transient
    private final ObjectProperty<Regiao> tabela = new SimpleObjectProperty<>(this, "tabela");
    @Transient
    private final StringProperty telefone = new SimpleStringProperty(this, "telefone");
    @Transient
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
    @Transient
    private final ObjectProperty<TabTran> transporte = new SimpleObjectProperty<>(this, "transporte");
    
    @Transient
    private final ObjectProperty<BigDecimal> ultimoFat = new SimpleObjectProperty<>(this, "ultimoFat");
    
    @Transient
    private final StringProperty obs = new SimpleStringProperty(this, "obs");
    @Transient
    private final ObjectProperty<ContaCont> contac = new SimpleObjectProperty<>(this, "contac");
    @Transient
    private final StringProperty fantasia = new SimpleStringProperty(this, "fantasia");
    
    @Transient
    private final ObjectProperty<BigDecimal> consumos = new SimpleObjectProperty<>(this, "consumos");
    @Transient
    private final ObjectProperty<BigDecimal> royal = new SimpleObjectProperty<>(this, "royal");
    
    @Transient
    private final ObjectProperty<Subgrupo> classe = new SimpleObjectProperty<>(this, "classe");
    @Transient
    private final StringProperty tipoEntidade = new SimpleStringProperty(this, "tipoEntidade");
    @Transient
    private final ObjectProperty<ContaCont> contad = new SimpleObjectProperty<>(this, "contad");
    
    @Transient
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<>(this, "com1");
    @Transient
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<>(this, "com2");
    
    @Transient
    private final IntegerProperty dias = new SimpleIntegerProperty(this, "dias");
    
    @Transient
    private final StringProperty bairroEnt = new SimpleStringProperty(this, "bairroEnt");
    @Transient
    private final StringProperty bairroCob = new SimpleStringProperty(this, "bairroCob");
    @Transient
    private final StringProperty suframa = new SimpleStringProperty(this, "suframa");
    @Transient
    private final StringProperty nomePai = new SimpleStringProperty(this, "nomePai");
    @Transient
    private final StringProperty nomeMae = new SimpleStringProperty(this, "nomeMae");
    @Transient
    private final StringProperty sexo = new SimpleStringProperty(this, "sexo");
    @Transient
    private final StringProperty numRg = new SimpleStringProperty(this, "numRg");
    @Transient
    private final StringProperty emiRg = new SimpleStringProperty(this, "emiRg");
    @Transient
    private final ObjectProperty<SdTipoTributacao001> simples = new SimpleObjectProperty<>(this, "simples");
    @Transient
    private final ObjectProperty<TabTran> redesp = new SimpleObjectProperty<>(this, "redesp");
    @Transient
    private final StringProperty conta = new SimpleStringProperty(this, "conta");
    @Transient
    private final StringProperty agencia = new SimpleStringProperty(this, "agencia");
    
    @Transient
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<>(this, "desconto");
    @Transient
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<>(this, "frete");
    
    @Transient
    private final StringProperty site = new SimpleStringProperty(this, "site");
    @Transient
    private final StringProperty inscEst = new SimpleStringProperty(this, "inscEst");
    
    @Transient
    private final ObjectProperty<LocalDate> dataFund = new SimpleObjectProperty<>(this, "dataFund");
    
    @Transient
    private final IntegerProperty atrasoMedio = new SimpleIntegerProperty(this, "atrasoMedio");
    
    @Transient
    private final StringProperty iof = new SimpleStringProperty(this, "iof");
    @Transient
    private final StringProperty foneCob = new SimpleStringProperty(this, "foneCob");
    @Transient
    private final ObjectProperty<Histcp> historico = new SimpleObjectProperty<>(this, "historico");
    @Transient
    private final ObjectProperty<TabSit> sitDup = new SimpleObjectProperty<>(this, "sitDup");
    @Transient
    private final StringProperty prazo = new SimpleStringProperty(this, "prazo");
    @Transient
    private final StringProperty tpPag = new SimpleStringProperty(this, "tpPag");
    
    @Transient
    private final ObjectProperty<LocalDate> dtAnalise = new SimpleObjectProperty<>(this, "dtAnalise");
    @Transient
    private final ObjectProperty<LocalDate> dtPrevAnalise = new SimpleObjectProperty<>(this, "dtPrevAnalise");
    
    @Transient
    private final StringProperty classifica = new SimpleStringProperty(this, "classifica");
    @Transient
    private final StringProperty obsNota = new SimpleStringProperty(this, "obsNota");
    @Transient
    private final StringProperty obsFinan = new SimpleStringProperty(this, "obsFinan");
    
    @Transient
    private final ObjectProperty<Mensagem> codMot = new SimpleObjectProperty<>(this, "codMot");
    
    @Transient
    private final ObjectProperty<Atividade> atividade = new SimpleObjectProperty<>(this, "atividade");
    
    @Transient
    private final StringProperty codPais = new SimpleStringProperty(this, "codPais");
    @Transient
    private final ObjectProperty<SdPagadorFrete001> cif = new SimpleObjectProperty<>(this, "cif");
    @Transient
    private final ObjectProperty<SdPagadorFrete001> redespCif = new SimpleObjectProperty<>(this, "redespCif");
    @Transient
    private final StringProperty complemento = new SimpleStringProperty(this, "complemento");
    
    @Transient
    private final ObjectProperty<BigDecimal> maiorFat = new SimpleObjectProperty<>(this, "maiorFat");
    
    @Transient
    private final ObjectProperty<LocalDate> dtMaiorFat = new SimpleObjectProperty<>(this, "dtMaiorFat");
    
    @Transient
    private final StringProperty numEnd = new SimpleStringProperty(this, "numEnd");
    @Transient
    private final StringProperty numEnt = new SimpleStringProperty(this, "numEnt");
    @Transient
    private final StringProperty numCob = new SimpleStringProperty(this, "numCob");
    
    @Transient
    private final ObjectProperty<BigDecimal> aliqSt = new SimpleObjectProperty<>(this, "aliqSt");
    @Transient
    private final ObjectProperty<BigDecimal> bonif = new SimpleObjectProperty<>(this, "bonif");
    
    @Transient
    private final StringProperty compEnt = new SimpleStringProperty(this, "compEnt");
    
    @Transient
    private final ObjectProperty<SdEntidade> sdEntidade = new SimpleObjectProperty<>();
    @Transient
    private final ListProperty<SdMarcasEntidade> marcas = new SimpleListProperty<>(FXCollections.observableArrayList());
    @Transient
    private final ListProperty<Contato> contatos = new SimpleListProperty<>(FXCollections.observableArrayList());
    @Transient
    private List<SdMarcasEntidade> marcasEntidade = new ArrayList<>();
    @Transient
    private List<Contato> contatosEntidade = new ArrayList<>();
    //</editor-fold>
    
    public Entidade() {
    }
    
    public Entidade(Entidade toCopy) {
        this.codcli.set(toCopy.codcli.get());
        this.nome.set(toCopy.nome.get());
        this.inscricao.set(toCopy.inscricao.get());
        this.cnpj.set(toCopy.cnpj.get());
        this.endereco.set(toCopy.endereco.get());
        this.ativo.set(toCopy.ativo.get());
        this.bairro.set(toCopy.bairro.get());
        this.banco.set(toCopy.banco.get());
        this.bloqueio.set(toCopy.bloqueio.get());
        this.cep.set(toCopy.cep.get());
        this.cepCob.set(toCopy.cepCob.get());
        this.cepEnt.set(toCopy.cepEnt.get());
        this.cnpjCob.set(toCopy.cnpjCob.get());
        this.cnpjEnt.set(toCopy.cnpjEnt.get());
        this.codrep.set(toCopy.codrep.get());
        this.condicao.set(toCopy.condicao.get());
        this.credito.set(toCopy.credito.get());
        this.dataAcumulo.set(toCopy.dataAcumulo.get());
        this.dataCad.set(toCopy.dataCad.get());
        this.dtNasc.set(toCopy.dtNasc.get());
        this.dtUltimoFat.set(toCopy.dtUltimoFat.get());
        this.email.set(toCopy.email.get());
        this.endCob.set(toCopy.endCob.get());
        this.endEnt.set(toCopy.endEnt.get());
        this.fax.set(toCopy.fax.get());
        this.grupo.set(toCopy.grupo.get());
        this.inscCob.set(toCopy.inscCob.get());
        this.inscEnt.set(toCopy.inscEnt.get());
        this.maiorAcumulo.set(toCopy.maiorAcumulo.get());
        this.natureza.set(toCopy.natureza.get());
        this.nrloja.set(toCopy.nrloja.get());
        this.sitCli.set(toCopy.sitCli.get());
        this.tabela.set(toCopy.tabela.get());
        this.telefone.set(toCopy.telefone.get());
        this.tipo.set(toCopy.tipo.get());
        this.transporte.set(toCopy.transporte.get());
        this.ultimoFat.set(toCopy.ultimoFat.get());
        this.obs.set(toCopy.obs.get());
        this.contac.set(toCopy.contac.get());
        this.fantasia.set(toCopy.fantasia.get());
        this.consumos.set(toCopy.consumos.get());
        this.royal.set(toCopy.royal.get());
        this.classe.set(toCopy.classe.get());
        this.tipoEntidade.set(toCopy.tipoEntidade.get());
        this.contad.set(toCopy.contad.get());
        this.com1.set(toCopy.com1.get());
        this.com2.set(toCopy.com2.get());
        this.dias.set(toCopy.dias.get());
        this.bairroEnt.set(toCopy.bairroEnt.get());
        this.bairroCob.set(toCopy.bairroCob.get());
        this.suframa.set(toCopy.suframa.get());
        this.nomePai.set(toCopy.nomePai.get());
        this.nomeMae.set(toCopy.nomeMae.get());
        this.sexo.set(toCopy.sexo.get());
        this.numRg.set(toCopy.numRg.get());
        this.emiRg.set(toCopy.emiRg.get());
        this.simples.set(toCopy.simples.get());
        this.redesp.set(toCopy.redesp.get());
        this.conta.set(toCopy.conta.get());
        this.agencia.set(toCopy.agencia.get());
        this.desconto.set(toCopy.desconto.get());
        this.frete.set(toCopy.frete.get());
        this.site.set(toCopy.site.get());
        this.inscEst.set(toCopy.inscEst.get());
        this.dataFund.set(toCopy.dataFund.get());
        this.atrasoMedio.set(toCopy.atrasoMedio.get());
        this.iof.set(toCopy.iof.get());
        this.foneCob.set(toCopy.foneCob.get());
        this.historico.set(toCopy.historico.get());
        this.sitDup.set(toCopy.sitDup.get());
        this.prazo.set(toCopy.prazo.get());
        this.tpPag.set(toCopy.tpPag.get());
        this.dtAnalise.set(toCopy.dtAnalise.get());
        this.dtPrevAnalise.set(toCopy.dtPrevAnalise.get());
        this.classifica.set(toCopy.classifica.get());
        this.obsNota.set(toCopy.obsNota.get());
        this.obsFinan.set(toCopy.obsFinan.get());
        this.codMot.set(toCopy.codMot.get());
        this.atividade.set(toCopy.atividade.get());
        this.codPais.set(toCopy.codPais.get());
        this.cif.set(toCopy.cif.get());
        this.redespCif.set(toCopy.redespCif.get());
        this.complemento.set(toCopy.complemento.get());
        this.maiorFat.set(toCopy.maiorFat.get());
        this.dtMaiorFat.set(toCopy.dtMaiorFat.get());
        this.numEnd.set(toCopy.numEnd.get());
        this.numEnt.set(toCopy.numEnt.get());
        this.numCob.set(toCopy.numCob.get());
        this.aliqSt.set(toCopy.aliqSt.get());
        this.bonif.set(toCopy.bonif.get());
        this.compEnt.set(toCopy.compEnt.get());
        this.sdEntidade.set(toCopy.sdEntidade.get());
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "entidade001")
    @TableGenerator(
            name = "entidade001",
            table = "CODIGOS_001",
            pkColumnName = "TABELA",
            valueColumnName = "PROXIMO",
            pkColumnValue = "ENTIDADE",
            allocationSize = 1
    )
    @Column(name = "CODCLI", columnDefinition = "VARCHAR2()")
    @Convert(converter = CodcliAttributeConverter.class)
    public Integer getCodcli() {
        return codcli.get();
    }
    
    @Transient
    public String getStringCodcli() {
        return StringUtils.lpad(String.valueOf(codcli.get()), 5, "0");
    }
    
    public IntegerProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Integer codcli) {
        super.setCodigoFilter(String.valueOf(codcli));
        this.codcli.set(codcli);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
        setDescricaoFilter(nome);
    }
    
    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }
    
    public StringProperty inscricaoProperty() {
        return inscricao;
    }
    
    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }
    
    @Column(name = "CNPJ")
    @CpfOuCnpj(message = "Campos CNPJ/CPF não é válido")
    public String getCnpj() {
        return cnpj.get();
    }
    
    public StringProperty cnpjProperty() {
        return cnpj;
    }
    
    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }
    
    @Column(name = "ENDERECO")
    public String getEndereco() {
        return endereco.get();
    }
    
    public StringProperty enderecoProperty() {
        return endereco;
    }
    
    public void setEndereco(String endereco) {
        this.endereco.set(endereco);
    }
    
    @Column(name = "ATIVO", columnDefinition = "VARCHAR2(1)")
    @Convert(converter = BooleanSimNaoConverter.class)
    public Boolean getAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(Boolean ativo) {
        this.ativo.set(ativo);
    }
    
    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }
    
    public StringProperty bairroProperty() {
        return bairro;
    }
    
    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANCO")
    public CadBan getBanco() {
        return banco.get();
    }
    
    public ObjectProperty<CadBan> bancoProperty() {
        return banco;
    }
    
    public void setBanco(CadBan banco) {
        this.banco.set(banco);
    }
    
    @Column(name = "BLOQUEIO", columnDefinition = "VARCHAR2(1)")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getBloqueio() {
        return bloqueio.get();
    }
    
    public BooleanProperty bloqueioProperty() {
        return bloqueio;
    }
    
    public void setBloqueio(Boolean bloqueio) {
        this.bloqueio.set(bloqueio);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "CEP")
    public CadCep getCep() {
        return cep.get();
    }
    
    public ObjectProperty<CadCep> cepProperty() {
        return cep;
    }
    
    public void setCep(CadCep cep) {
        this.cep.set(cep);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "CEP_COB")
    public CadCep getCepCob() {
        return cepCob.get();
    }
    
    public ObjectProperty<CadCep> cepCobProperty() {
        return cepCob;
    }
    
    public void setCepCob(CadCep cepCob) {
        this.cepCob.set(cepCob);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "CEP_ENT")
    public CadCep getCepEnt() {
        return cepEnt.get();
    }
    
    public ObjectProperty<CadCep> cepEntProperty() {
        return cepEnt;
    }
    
    public void setCepEnt(CadCep cepEnt) {
        this.cepEnt.set(cepEnt);
    }
    
    @Column(name = "CNPJ_COB")
    public String getCnpjCob() {
        return cnpjCob.get();
    }
    
    public StringProperty cnpjCobProperty() {
        return cnpjCob;
    }
    
    public void setCnpjCob(String cnpjCob) {
        this.cnpjCob.set(cnpjCob);
    }
    
    @Column(name = "CNPJ_ENT")
    public String getCnpjEnt() {
        return cnpjEnt.get();
    }
    
    public StringProperty cnpjEntProperty() {
        return cnpjEnt;
    }
    
    public void setCnpjEnt(String cnpjEnt) {
        this.cnpjEnt.set(cnpjEnt);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "CONDICAO")
    public String getCondicao() {
        return condicao.get();
    }
    
    public StringProperty condicaoProperty() {
        return condicao;
    }
    
    public void setCondicao(String condicao) {
        this.condicao.set(condicao);
    }
    
    @Column(name = "CREDITO")
    public BigDecimal getCredito() {
        return credito.get();
    }
    
    public ObjectProperty<BigDecimal> creditoProperty() {
        return credito;
    }
    
    public void setCredito(BigDecimal credito) {
        this.credito.set(credito);
    }
    
    @Column(name = "DATA_ACUMULO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataAcumulo() {
        return dataAcumulo.get();
    }
    
    public ObjectProperty<LocalDate> dataAcumuloProperty() {
        return dataAcumulo;
    }
    
    public void setDataAcumulo(LocalDate dataAcumulo) {
        this.dataAcumulo.set(dataAcumulo);
    }
    
    @Column(name = "DATA_CAD")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataCad() {
        if (dataCad.get() == null) {
            dataCad.set(LocalDate.now());
        }
        return dataCad.get();
    }
    
    public ObjectProperty<LocalDate> dataCadProperty() {
        return dataCad;
    }
    
    public void setDataCad(LocalDate dataCad) {
        this.dataCad.set(dataCad);
    }
    
    @Column(name = "DT_NASC")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtNasc() {
        return dtNasc.get();
    }
    
    public ObjectProperty<LocalDate> dtNascProperty() {
        return dtNasc;
    }
    
    public void setDtNasc(LocalDate dtNasc) {
        this.dtNasc.set(dtNasc);
    }
    
    @Column(name = "DT_ULTIMO_FAT")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtUltimoFat() {
        return dtUltimoFat.get();
    }
    
    public ObjectProperty<LocalDate> dtUltimoFatProperty() {
        return dtUltimoFat;
    }
    
    public void setDtUltimoFat(LocalDate dtUltimoFat) {
        this.dtUltimoFat.set(dtUltimoFat);
    }
    
    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email.set(email);
    }
    
    @Column(name = "END_COB")
    public String getEndCob() {
        return endCob.get();
    }
    
    public StringProperty endCobProperty() {
        return endCob;
    }
    
    public void setEndCob(String endCob) {
        this.endCob.set(endCob);
    }
    
    @Column(name = "END_ENT")
    public String getEndEnt() {
        return endEnt.get();
    }
    
    public StringProperty endEntProperty() {
        return endEnt;
    }
    
    public void setEndEnt(String endEnt) {
        this.endEnt.set(endEnt);
    }
    
    @Column(name = "FAX")
    public String getFax() {
        return fax.get();
    }
    
    public StringProperty faxProperty() {
        return fax;
    }
    
    public void setFax(String fax) {
        this.fax.set(fax);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO")
    public GrupoCli getGrupo() {
        return grupo.get();
    }
    
    public ObjectProperty<GrupoCli> grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(GrupoCli grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "INSC_COB")
    public String getInscCob() {
        return inscCob.get();
    }
    
    public StringProperty inscCobProperty() {
        return inscCob;
    }
    
    public void setInscCob(String inscCob) {
        this.inscCob.set(inscCob);
    }
    
    @Column(name = "INSC_ENT")
    public String getInscEnt() {
        return inscEnt.get();
    }
    
    public StringProperty inscEntProperty() {
        return inscEnt;
    }
    
    public void setInscEnt(String inscEnt) {
        this.inscEnt.set(inscEnt);
    }
    
    @Column(name = "MAIOR_ACUMULO")
    public BigDecimal getMaiorAcumulo() {
        return maiorAcumulo.get();
    }
    
    public ObjectProperty<BigDecimal> maiorAcumuloProperty() {
        return maiorAcumulo;
    }
    
    public void setMaiorAcumulo(BigDecimal maiorAcumulo) {
        this.maiorAcumulo.set(maiorAcumulo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NATUREZA")
    public Natureza getNatureza() {
        return natureza.get();
    }
    
    public ObjectProperty<Natureza> naturezaProperty() {
        return natureza;
    }
    
    public void setNatureza(Natureza natureza) {
        this.natureza.set(natureza);
    }
    
    @Column(name = "NRLOJA")
    public String getNrloja() {
        return nrloja.get();
    }
    
    public StringProperty nrlojaProperty() {
        return nrloja;
    }
    
    public void setNrloja(String nrloja) {
        this.nrloja.set(nrloja);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIT_CLI")
    public SitCli getSitCli() {
        return sitCli.get();
    }
    
    public ObjectProperty<SitCli> sitCliProperty() {
        return sitCli;
    }
    
    public void setSitCli(SitCli sitCli) {
        this.sitCli.set(sitCli);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TABELA")
    public Regiao getTabela() {
        return tabela.get();
    }
    
    public ObjectProperty<Regiao> tabelaProperty() {
        return tabela;
    }
    
    public void setTabela(Regiao tabela) {
        this.tabela.set(tabela);
    }
    
    @Column(name = "TELEFONE")
    public String getTelefone() {
        return telefone.get();
    }
    
    public StringProperty telefoneProperty() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRANSPORTE")
    public TabTran getTransporte() {
        return transporte.get();
    }
    
    public ObjectProperty<TabTran> transporteProperty() {
        return transporte;
    }
    
    public void setTransporte(TabTran transporte) {
        this.transporte.set(transporte);
    }
    
    @Column(name = "ULTIMO_FAT")
    public BigDecimal getUltimoFat() {
        return ultimoFat.get();
    }
    
    public ObjectProperty<BigDecimal> ultimoFatProperty() {
        return ultimoFat;
    }
    
    public void setUltimoFat(BigDecimal ultimoFat) {
        this.ultimoFat.set(ultimoFat);
    }
    
    @Lob
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTAC")
    public ContaCont getContac() {
        return contac.get();
    }
    
    public ObjectProperty<ContaCont> contacProperty() {
        return contac;
    }
    
    public void setContac(ContaCont contac) {
        this.contac.set(contac);
    }
    
    @Column(name = "FANTASIA")
    public String getFantasia() {
        return fantasia.get();
    }
    
    public StringProperty fantasiaProperty() {
        return fantasia;
    }
    
    public void setFantasia(String fantasia) {
        this.fantasia.set(fantasia);
    }
    
    @Column(name = "CONSUMOS")
    public BigDecimal getConsumos() {
        return consumos.get();
    }
    
    public ObjectProperty<BigDecimal> consumosProperty() {
        return consumos;
    }
    
    public void setConsumos(BigDecimal consumos) {
        this.consumos.set(consumos);
    }
    
    @Column(name = "ROYAL")
    public BigDecimal getRoyal() {
        return royal.get();
    }
    
    public ObjectProperty<BigDecimal> royalProperty() {
        return royal;
    }
    
    public void setRoyal(BigDecimal royal) {
        this.royal.set(royal);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLASSE")
    public Subgrupo getClasse() {
        return classe.get();
    }
    
    public ObjectProperty<Subgrupo> classeProperty() {
        return classe;
    }
    
    public void setClasse(Subgrupo classe) {
        this.classe.set(classe);
    }
    
    @Column(name = "TIPO_ENTIDADE")
    public String getTipoEntidade() {
        return tipoEntidade.get();
    }
    
    public StringProperty tipoEntidadeProperty() {
        return tipoEntidade;
    }
    
    public void setTipoEntidade(String tipoEntidade) {
        this.tipoEntidade.set(tipoEntidade);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTAD")
    public ContaCont getContad() {
        return contad.get();
    }
    
    public ObjectProperty<ContaCont> contadProperty() {
        return contad;
    }
    
    public void setContad(ContaCont contad) {
        this.contad.set(contad);
    }
    
    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }
    
    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }
    
    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }
    
    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }
    
    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }
    
    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }
    
    @Column(name = "DIAS")
    public Integer getDias() {
        return dias.get();
    }
    
    public IntegerProperty diasProperty() {
        return dias;
    }
    
    public void setDias(Integer dias) {
        if (dias == null) {
            this.dias.set(0);
        } else {
            this.dias.set(dias);
        }
    }
    
    @Column(name = "BAIRRO_ENT")
    public String getBairroEnt() {
        return bairroEnt.get();
    }
    
    public StringProperty bairroEntProperty() {
        return bairroEnt;
    }
    
    public void setBairroEnt(String bairroEnt) {
        this.bairroEnt.set(bairroEnt);
    }
    
    @Column(name = "BAIRRO_COB")
    public String getBairroCob() {
        return bairroCob.get();
    }
    
    public StringProperty bairroCobProperty() {
        return bairroCob;
    }
    
    public void setBairroCob(String bairroCob) {
        this.bairroCob.set(bairroCob);
    }
    
    @Column(name = "SUFRAMA")
    public String getSuframa() {
        return suframa.get();
    }
    
    public StringProperty suframaProperty() {
        return suframa;
    }
    
    public void setSuframa(String suframa) {
        this.suframa.set(suframa);
    }
    
    @Column(name = "NOME_PAI")
    public String getNomePai() {
        return nomePai.get();
    }
    
    public StringProperty nomePaiProperty() {
        return nomePai;
    }
    
    public void setNomePai(String nomePai) {
        this.nomePai.set(nomePai);
    }
    
    @Column(name = "NOME_MAE")
    public String getNomeMae() {
        return nomeMae.get();
    }
    
    public StringProperty nomeMaeProperty() {
        return nomeMae;
    }
    
    public void setNomeMae(String nomeMae) {
        this.nomeMae.set(nomeMae);
    }
    
    @Column(name = "SEXO")
    public String getSexo() {
        return sexo.get();
    }
    
    public StringProperty sexoProperty() {
        return sexo;
    }
    
    public void setSexo(String sexo) {
        this.sexo.set(sexo);
    }
    
    @Column(name = "NUM_RG")
    public String getNumRg() {
        return numRg.get();
    }
    
    public StringProperty numRgProperty() {
        return numRg;
    }
    
    public void setNumRg(String numRg) {
        this.numRg.set(numRg);
    }
    
    @Column(name = "EMI_RG")
    public String getEmiRg() {
        return emiRg.get();
    }
    
    public StringProperty emiRgProperty() {
        return emiRg;
    }
    
    public void setEmiRg(String emiRg) {
        this.emiRg.set(emiRg);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SIMPLES")
    public SdTipoTributacao001 getSimples() {
        return simples.get();
    }
    
    public ObjectProperty<SdTipoTributacao001> simplesProperty() {
        return simples;
    }
    
    public void setSimples(SdTipoTributacao001 simples) {
        this.simples.set(simples);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REDESP")
    public TabTran getRedesp() {
        return redesp.get();
    }
    
    public ObjectProperty<TabTran> redespProperty() {
        return redesp;
    }
    
    public void setRedesp(TabTran redesp) {
        this.redesp.set(redesp);
    }
    
    @Column(name = "CONTA")
    public String getConta() {
        return conta.get();
    }
    
    public StringProperty contaProperty() {
        return conta;
    }
    
    public void setConta(String conta) {
        this.conta.set(conta);
    }
    
    @Column(name = "AGENCIA")
    public String getAgencia() {
        return agencia.get();
    }
    
    public StringProperty agenciaProperty() {
        return agencia;
    }
    
    public void setAgencia(String agencia) {
        this.agencia.set(agencia);
    }
    
    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }
    
    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }
    
    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }
    
    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }
    
    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }
    
    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }
    
    @Column(name = "SITE")
    public String getSite() {
        return site.get();
    }
    
    public StringProperty siteProperty() {
        return site;
    }
    
    public void setSite(String site) {
        this.site.set(site);
    }
    
    @Column(name = "INSC_EST")
    public String getInscEst() {
        return inscEst.get();
    }
    
    public StringProperty inscEstProperty() {
        return inscEst;
    }
    
    public void setInscEst(String inscEst) {
        this.inscEst.set(inscEst);
    }
    
    @Column(name = "DATA_FUND")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataFund() {
        return dataFund.get();
    }
    
    public ObjectProperty<LocalDate> dataFundProperty() {
        return dataFund;
    }
    
    public void setDataFund(LocalDate dataFund) {
        this.dataFund.set(dataFund);
    }
    
    @Column(name = "ATRASO_MEDIO")
    public Integer getAtrasoMedio() {
        return atrasoMedio.get();
    }
    
    public IntegerProperty atrasoMedioProperty() {
        return atrasoMedio;
    }
    
    public void setAtrasoMedio(Integer atrasoMedio) {
        if (atrasoMedio == null) {
            this.atrasoMedio.set(0);
        } else {
            this.atrasoMedio.set(atrasoMedio);
        }
        
    }
    
    @Column(name = "IOF")
    public String getIof() {
        return iof.get();
    }
    
    public StringProperty iofProperty() {
        return iof;
    }
    
    public void setIof(String iof) {
        this.iof.set(iof);
    }
    
    @Column(name = "FONE_COB")
    public String getFoneCob() {
        return foneCob.get();
    }
    
    public StringProperty foneCobProperty() {
        return foneCob;
    }
    
    public void setFoneCob(String foneCob) {
        this.foneCob.set(foneCob);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HISTORICO")
    public Histcp getHistorico() {
        return historico.get();
    }
    
    public ObjectProperty<Histcp> historicoProperty() {
        return historico;
    }
    
    public void setHistorico(Histcp historico) {
        this.historico.set(historico);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIT_DUP")
    public TabSit getSitDup() {
        return sitDup.get();
    }
    
    public ObjectProperty<TabSit> sitDupProperty() {
        return sitDup;
    }
    
    public void setSitDup(TabSit sitDup) {
        this.sitDup.set(sitDup);
    }
    
    @Column(name = "PRAZO")
    public String getPrazo() {
        return prazo.get();
    }
    
    public StringProperty prazoProperty() {
        return prazo;
    }
    
    public void setPrazo(String prazo) {
        this.prazo.set(prazo);
    }
    
    @Column(name = "TP_PAG")
    public String getTpPag() {
        return tpPag.get();
    }
    
    public StringProperty tpPagProperty() {
        return tpPag;
    }
    
    public void setTpPag(String tpPag) {
        this.tpPag.set(tpPag);
    }
    
    @Column(name = "DT_ANALISE")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtAnalise() {
        return dtAnalise.get();
    }
    
    public ObjectProperty<LocalDate> dtAnaliseProperty() {
        return dtAnalise;
    }
    
    public void setDtAnalise(LocalDate dtAnalise) {
        this.dtAnalise.set(dtAnalise);
    }
    
    @Column(name = "DT_PREV_ANALISE")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtPrevAnalise() {
        return dtPrevAnalise.get();
    }
    
    public ObjectProperty<LocalDate> dtPrevAnaliseProperty() {
        return dtPrevAnalise;
    }
    
    public void setDtPrevAnalise(LocalDate dtPrevAnalise) {
        this.dtPrevAnalise.set(dtPrevAnalise);
    }
    
    @Column(name = "CLASSIFICA")
    public String getClassifica() {
        return classifica.get();
    }
    
    public StringProperty classificaProperty() {
        return classifica;
    }
    
    public void setClassifica(String classifica) {
        this.classifica.set(classifica);
    }
    
    @Lob
    @Column(name = "OBS_NOTA")
    public String getObsNota() {
        return obsNota.get();
    }
    
    public StringProperty obsNotaProperty() {
        return obsNota;
    }
    
    public void setObsNota(String obsNota) {
        this.obsNota.set(obsNota);
    }
    
    @Lob
    @Column(name = "OBS_FINAN")
    public String getObsFinan() {
        return obsFinan.get();
    }
    
    public StringProperty obsFinanProperty() {
        return obsFinan;
    }
    
    public void setObsFinan(String obsFinan) {
        this.obsFinan.set(obsFinan);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COD_MOT")
    public Mensagem getCodMot() {
        return codMot.get();
    }
    
    public ObjectProperty<Mensagem> codMotProperty() {
        return codMot;
    }
    
    public void setCodMot(Mensagem codMot) {
        this.codMot.set(codMot);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_ATVD")
    public Atividade getAtividade() {
        return atividade.get();
    }
    
    public ObjectProperty<Atividade> atividadeProperty() {
        return atividade;
    }
    
    public void setAtividade(Atividade atividade) {
        this.atividade.set(atividade);
    }
    
    @Column(name = "COD_PAIS")
    public String getCodPais() {
        return codPais.get();
    }
    
    public StringProperty codPaisProperty() {
        return codPais;
    }
    
    public void setCodPais(String codPais) {
        this.codPais.set(codPais);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIF")
    public SdPagadorFrete001 getCif() {
        return cif.get();
    }
    
    public ObjectProperty<SdPagadorFrete001> cifProperty() {
        return cif;
    }
    
    public void setCif(SdPagadorFrete001 cif) {
        this.cif.set(cif);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REDESP_CIF")
    public SdPagadorFrete001 getRedespCif() {
        return redespCif.get();
    }
    
    public ObjectProperty<SdPagadorFrete001> redespCifProperty() {
        return redespCif;
    }
    
    public void setRedespCif(SdPagadorFrete001 redespCif) {
        this.redespCif.set(redespCif);
    }
    
    @Column(name = "COMPLEMENTO")
    public String getComplemento() {
        return complemento.get();
    }
    
    public StringProperty complementoProperty() {
        return complemento;
    }
    
    public void setComplemento(String complemento) {
        this.complemento.set(complemento);
    }
    
    @Column(name = "MAIOR_FAT")
    public BigDecimal getMaiorFat() {
        return maiorFat.get();
    }
    
    public ObjectProperty<BigDecimal> maiorFatProperty() {
        return maiorFat;
    }
    
    public void setMaiorFat(BigDecimal maiorFat) {
        this.maiorFat.set(maiorFat);
    }
    
    @Column(name = "DT_MAIOR_FAT")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtMaiorFat() {
        return dtMaiorFat.get();
    }
    
    public ObjectProperty<LocalDate> dtMaiorFatProperty() {
        return dtMaiorFat;
    }
    
    public void setDtMaiorFat(LocalDate dtMaiorFat) {
        this.dtMaiorFat.set(dtMaiorFat);
    }
    
    @Column(name = "NUM_END")
    public String getNumEnd() {
        return numEnd.get();
    }
    
    public StringProperty numEndProperty() {
        return numEnd;
    }
    
    public void setNumEnd(String numEnd) {
        this.numEnd.set(numEnd);
    }
    
    @Column(name = "NUM_ENT")
    public String getNumEnt() {
        return numEnt.get();
    }
    
    public StringProperty numEntProperty() {
        return numEnt;
    }
    
    public void setNumEnt(String numEnt) {
        this.numEnt.set(numEnt);
    }
    
    @Column(name = "NUM_COB")
    public String getNumCob() {
        return numCob.get();
    }
    
    public StringProperty numCobProperty() {
        return numCob;
    }
    
    public void setNumCob(String numCob) {
        this.numCob.set(numCob);
    }
    
    @Column(name = "ALIQ_ST")
    public BigDecimal getAliqSt() {
        return aliqSt.get();
    }
    
    public ObjectProperty<BigDecimal> aliqStProperty() {
        return aliqSt;
    }
    
    public void setAliqSt(BigDecimal aliqSt) {
        this.aliqSt.set(aliqSt);
    }
    
    @Column(name = "BONIF")
    public BigDecimal getBonif() {
        return bonif.get();
    }
    
    public ObjectProperty<BigDecimal> bonifProperty() {
        return bonif;
    }
    
    public void setBonif(BigDecimal bonif) {
        this.bonif.set(bonif);
    }
    
    @Column(name = "COMP_ENT")
    public String getCompEnt() {
        return compEnt.get();
    }
    
    public StringProperty compEntProperty() {
        return compEnt;
    }
    
    public void setCompEnt(String compEnt) {
        this.compEnt.set(compEnt);
    }
    
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "CODCLI")
    public SdEntidade getSdEntidade() {
        return sdEntidade.get();
    }
    
    public ObjectProperty<SdEntidade> sdEntidadeProperty() {
        return sdEntidade;
    }
    
    public void setSdEntidade(SdEntidade sdEntidade) {
        this.sdEntidade.set(sdEntidade);
    }
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    @Convert(converter = CodcliAttributeConverter.class)
    public List<SdMarcasEntidade> getMarcasEntidade() {
        return marcasEntidade;
    }
    
    public void setMarcasEntidade(List<SdMarcasEntidade> marcasEntidade) {
        this.marcasEntidade = marcasEntidade;
        this.marcas.addAll(marcasEntidade);
    }
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public List<Contato> getContatosEntidade() {
        return contatosEntidade;
    }
    
    public void setContatosEntidade(List<Contato> contatosEntidade) {
        this.contatosEntidade = contatosEntidade;
        this.contatos.clear();
        this.contatos.addAll(contatosEntidade);
    }
    
    @Transient
    public ObservableList<SdMarcasEntidade> getMarcas() {
        return marcas.get();
    }
    
    public ListProperty<SdMarcasEntidade> marcasProperty() {
        return marcas;
    }
    
    public void setMarcas(ObservableList<SdMarcasEntidade> marcas) {
        this.marcas.set(marcas);
    }
    
    @Transient
    public ObservableList<Contato> getContatos() {
        return contatos.get();
    }
    
    public ListProperty<Contato> contatosProperty() {
        return contatos;
    }
    
    public void setContatos(ObservableList<Contato> contatos) {
        this.contatos.set(contatos);
    }
    
    @Transient
    public String diffObject(Entidade entidade) {
        String diffs = "Entidade: [\n";
        if (this.codcli.getValue() == null && entidade.codcli.getValue() != null) diffs = diffs.concat("codcli: NULL>" + entidade.codcli.getValue().toString() + "\n");
        else if (this.codcli.getValue() != null && entidade.codcli.getValue() == null) diffs = diffs.concat("codcli:" + this.codcli.getValue().toString() + ">NULL\n");
        else if (this.codcli.getValue() != null && entidade.codcli.getValue() != null)
            diffs = diffs.concat(!this.codcli.getValue().equals(entidade.codcli.getValue()) ? "codcli:" + this.codcli.getValue().toString() + ">" + entidade.codcli.getValue().toString() + "\n" : "");
        if (this.nome.getValue() == null && entidade.nome.getValue() != null) diffs = diffs.concat("nome: NULL>" + entidade.nome.getValue().toString() + "\n");
        else if (this.nome.getValue() != null && entidade.nome.getValue() == null) diffs = diffs.concat("nome:" + this.nome.getValue().toString() + ">NULL\n");
        else if (this.nome.getValue() != null && entidade.nome.getValue() != null)
            diffs = diffs.concat(!this.nome.getValue().equals(entidade.nome.getValue()) ? "nome:" + this.nome.getValue().toString() + ">" + entidade.nome.getValue().toString() + "\n" : "");
        if (this.inscricao.getValue() == null && entidade.inscricao.getValue() != null) diffs = diffs.concat("inscricao: NULL>" + entidade.inscricao.getValue().toString() + "\n");
        else if (this.inscricao.getValue() != null && entidade.inscricao.getValue() == null) diffs = diffs.concat("inscricao:" + this.inscricao.getValue().toString() + ">NULL\n");
        else if (this.inscricao.getValue() != null && entidade.inscricao.getValue() != null)
            diffs = diffs.concat(!this.inscricao.getValue().equals(entidade.inscricao.getValue()) ? "inscricao:" + this.inscricao.getValue().toString() + ">" + entidade.inscricao.getValue().toString() + "\n" : "");
        if (this.cnpj.getValue() == null && entidade.cnpj.getValue() != null) diffs = diffs.concat("cnpj: NULL>" + entidade.cnpj.getValue().toString() + "\n");
        else if (this.cnpj.getValue() != null && entidade.cnpj.getValue() == null) diffs = diffs.concat("cnpj:" + this.cnpj.getValue().toString() + ">NULL\n");
        else if (this.cnpj.getValue() != null && entidade.cnpj.getValue() != null)
            diffs = diffs.concat(!this.cnpj.getValue().equals(entidade.cnpj.getValue()) ? "cnpj:" + this.cnpj.getValue().toString() + ">" + entidade.cnpj.getValue().toString() + "\n" : "");
        if (this.endereco.getValue() == null && entidade.endereco.getValue() != null) diffs = diffs.concat("endereco: NULL>" + entidade.endereco.getValue().toString() + "\n");
        else if (this.endereco.getValue() != null && entidade.endereco.getValue() == null) diffs = diffs.concat("endereco:" + this.endereco.getValue().toString() + ">NULL\n");
        else if (this.endereco.getValue() != null && entidade.endereco.getValue() != null)
            diffs = diffs.concat(!this.endereco.getValue().equals(entidade.endereco.getValue()) ? "endereco:" + this.endereco.getValue().toString() + ">" + entidade.endereco.getValue().toString() + "\n" : "");
        if (this.ativo.getValue() == null && entidade.ativo.getValue() != null) diffs = diffs.concat("ativo: NULL>" + entidade.ativo.getValue().toString() + "\n");
        else if (this.ativo.getValue() != null && entidade.ativo.getValue() == null) diffs = diffs.concat("ativo:" + this.ativo.getValue().toString() + ">NULL\n");
        else if (this.ativo.getValue() != null && entidade.ativo.getValue() != null)
            diffs = diffs.concat(!this.ativo.getValue().equals(entidade.ativo.getValue()) ? "ativo:" + this.ativo.getValue().toString() + ">" + entidade.ativo.getValue().toString() + "\n" : "");
        if (this.bairro.getValue() == null && entidade.bairro.getValue() != null) diffs = diffs.concat("bairro: NULL>" + entidade.bairro.getValue().toString() + "\n");
        else if (this.bairro.getValue() != null && entidade.bairro.getValue() == null) diffs = diffs.concat("bairro:" + this.bairro.getValue().toString() + ">NULL\n");
        else if (this.bairro.getValue() != null && entidade.bairro.getValue() != null)
            diffs = diffs.concat(!this.bairro.getValue().equals(entidade.bairro.getValue()) ? "bairro:" + this.bairro.getValue().toString() + ">" + entidade.bairro.getValue().toString() + "\n" : "");
        if (this.banco.getValue() == null && entidade.banco.getValue() != null) diffs = diffs.concat("banco: NULL>" + entidade.banco.getValue().toString() + "\n");
        else if (this.banco.getValue() != null && entidade.banco.getValue() == null) diffs = diffs.concat("banco:" + this.banco.getValue().toString() + ">NULL\n");
        else if (this.banco.getValue() != null && entidade.banco.getValue() != null)
            diffs = diffs.concat(!this.banco.getValue().equals(entidade.banco.getValue()) ? "banco:" + this.banco.getValue().toString() + ">" + entidade.banco.getValue().toString() + "\n" : "");
        if (this.bloqueio.getValue() == null && entidade.bloqueio.getValue() != null) diffs = diffs.concat("bloqueio: NULL>" + entidade.bloqueio.getValue().toString() + "\n");
        else if (this.bloqueio.getValue() != null && entidade.bloqueio.getValue() == null) diffs = diffs.concat("bloqueio:" + this.bloqueio.getValue().toString() + ">NULL\n");
        else if (this.bloqueio.getValue() != null && entidade.bloqueio.getValue() != null)
            diffs = diffs.concat(!this.bloqueio.getValue().equals(entidade.bloqueio.getValue()) ? "bloqueio:" + this.bloqueio.getValue().toString() + ">" + entidade.bloqueio.getValue().toString() + "\n" : "");
        if (this.cep.getValue() == null && entidade.cep.getValue() != null) diffs = diffs.concat("cep: NULL>" + entidade.cep.getValue().toString() + "\n");
        else if (this.cep.getValue() != null && entidade.cep.getValue() == null) diffs = diffs.concat("cep:" + this.cep.getValue().toString() + ">NULL\n");
        else if (this.cep.getValue() != null && entidade.cep.getValue() != null)
            diffs = diffs.concat(!this.cep.getValue().equals(entidade.cep.getValue()) ? "cep:" + this.cep.getValue().toString() + ">" + entidade.cep.getValue().toString() + "\n" : "");
        if (this.cepCob.getValue() == null && entidade.cepCob.getValue() != null) diffs = diffs.concat("cepCob: NULL>" + entidade.cepCob.getValue().toString() + "\n");
        else if (this.cepCob.getValue() != null && entidade.cepCob.getValue() == null) diffs = diffs.concat("cepCob:" + this.cepCob.getValue().toString() + ">NULL\n");
        else if (this.cepCob.getValue() != null && entidade.cepCob.getValue() != null)
            diffs = diffs.concat(!this.cepCob.getValue().equals(entidade.cepCob.getValue()) ? "cepCob:" + this.cepCob.getValue().toString() + ">" + entidade.cepCob.getValue().toString() + "\n" : "");
        if (this.cepEnt.getValue() == null && entidade.cepEnt.getValue() != null) diffs = diffs.concat("cepEnt: NULL>" + entidade.cepEnt.getValue().toString() + "\n");
        else if (this.cepEnt.getValue() != null && entidade.cepEnt.getValue() == null) diffs = diffs.concat("cepEnt:" + this.cepEnt.getValue().toString() + ">NULL\n");
        else if (this.cepEnt.getValue() != null && entidade.cepEnt.getValue() != null)
            diffs = diffs.concat(!this.cepEnt.getValue().equals(entidade.cepEnt.getValue()) ? "cepEnt:" + this.cepEnt.getValue().toString() + ">" + entidade.cepEnt.getValue().toString() + "\n" : "");
        if (this.cnpjCob.getValue() == null && entidade.cnpjCob.getValue() != null) diffs = diffs.concat("cnpjCob: NULL>" + entidade.cnpjCob.getValue().toString() + "\n");
        else if (this.cnpjCob.getValue() != null && entidade.cnpjCob.getValue() == null) diffs = diffs.concat("cnpjCob:" + this.cnpjCob.getValue().toString() + ">NULL\n");
        else if (this.cnpjCob.getValue() != null && entidade.cnpjCob.getValue() != null)
            diffs = diffs.concat(!this.cnpjCob.getValue().equals(entidade.cnpjCob.getValue()) ? "cnpjCob:" + this.cnpjCob.getValue().toString() + ">" + entidade.cnpjCob.getValue().toString() + "\n" : "");
        if (this.cnpjEnt.getValue() == null && entidade.cnpjEnt.getValue() != null) diffs = diffs.concat("cnpjEnt: NULL>" + entidade.cnpjEnt.getValue().toString() + "\n");
        else if (this.cnpjEnt.getValue() != null && entidade.cnpjEnt.getValue() == null) diffs = diffs.concat("cnpjEnt:" + this.cnpjEnt.getValue().toString() + ">NULL\n");
        else if (this.cnpjEnt.getValue() != null && entidade.cnpjEnt.getValue() != null)
            diffs = diffs.concat(!this.cnpjEnt.getValue().equals(entidade.cnpjEnt.getValue()) ? "cnpjEnt:" + this.cnpjEnt.getValue().toString() + ">" + entidade.cnpjEnt.getValue().toString() + "\n" : "");
        if (this.codrep.getValue() == null && entidade.codrep.getValue() != null) diffs = diffs.concat("codrep: NULL>" + entidade.codrep.getValue().toString() + "\n");
        else if (this.codrep.getValue() != null && entidade.codrep.getValue() == null) diffs = diffs.concat("codrep:" + this.codrep.getValue().toString() + ">NULL\n");
        else if (this.codrep.getValue() != null && entidade.codrep.getValue() != null)
            diffs = diffs.concat(!this.codrep.getValue().equals(entidade.codrep.getValue()) ? "codrep:" + this.codrep.getValue().toString() + ">" + entidade.codrep.getValue().toString() + "\n" : "");
        if (this.condicao.getValue() == null && entidade.condicao.getValue() != null) diffs = diffs.concat("condicao: NULL>" + entidade.condicao.getValue().toString() + "\n");
        else if (this.condicao.getValue() != null && entidade.condicao.getValue() == null) diffs = diffs.concat("condicao:" + this.condicao.getValue().toString() + ">NULL\n");
        else if (this.condicao.getValue() != null && entidade.condicao.getValue() != null)
            diffs = diffs.concat(!this.condicao.getValue().equals(entidade.condicao.getValue()) ? "condicao:" + this.condicao.getValue().toString() + ">" + entidade.condicao.getValue().toString() + "\n" : "");
        if (this.credito.getValue() == null && entidade.credito.getValue() != null) diffs = diffs.concat("credito: NULL>" + entidade.credito.getValue().toString() + "\n");
        else if (this.credito.getValue() != null && entidade.credito.getValue() == null) diffs = diffs.concat("credito:" + this.credito.getValue().toString() + ">NULL\n");
        else if (this.credito.getValue() != null && entidade.credito.getValue() != null)
            diffs = diffs.concat(!this.credito.getValue().equals(entidade.credito.getValue()) ? "credito:" + this.credito.getValue().toString() + ">" + entidade.credito.getValue().toString() + "\n" : "");
        if (this.dataAcumulo.getValue() == null && entidade.dataAcumulo.getValue() != null) diffs = diffs.concat("dataAcumulo: NULL>" + entidade.dataAcumulo.getValue().toString() + "\n");
        else if (this.dataAcumulo.getValue() != null && entidade.dataAcumulo.getValue() == null) diffs = diffs.concat("dataAcumulo:" + this.dataAcumulo.getValue().toString() + ">NULL\n");
        else if (this.dataAcumulo.getValue() != null && entidade.dataAcumulo.getValue() != null)
            diffs = diffs.concat(!this.dataAcumulo.getValue().equals(entidade.dataAcumulo.getValue()) ? "dataAcumulo:" + this.dataAcumulo.getValue().toString() + ">" + entidade.dataAcumulo.getValue().toString() + "\n" : "");
        if (this.dataCad.getValue() == null && entidade.dataCad.getValue() != null) diffs = diffs.concat("dataCad: NULL>" + entidade.dataCad.getValue().toString() + "\n");
        else if (this.dataCad.getValue() != null && entidade.dataCad.getValue() == null) diffs = diffs.concat("dataCad:" + this.dataCad.getValue().toString() + ">NULL\n");
        else if (this.dataCad.getValue() != null && entidade.dataCad.getValue() != null)
            diffs = diffs.concat(!this.dataCad.getValue().equals(entidade.dataCad.getValue()) ? "dataCad:" + this.dataCad.getValue().toString() + ">" + entidade.dataCad.getValue().toString() + "\n" : "");
        if (this.dtNasc.getValue() == null && entidade.dtNasc.getValue() != null) diffs = diffs.concat("dtNasc: NULL>" + entidade.dtNasc.getValue().toString() + "\n");
        else if (this.dtNasc.getValue() != null && entidade.dtNasc.getValue() == null) diffs = diffs.concat("dtNasc:" + this.dtNasc.getValue().toString() + ">NULL\n");
        else if (this.dtNasc.getValue() != null && entidade.dtNasc.getValue() != null)
            diffs = diffs.concat(!this.dtNasc.getValue().equals(entidade.dtNasc.getValue()) ? "dtNasc:" + this.dtNasc.getValue().toString() + ">" + entidade.dtNasc.getValue().toString() + "\n" : "");
        if (this.dtUltimoFat.getValue() == null && entidade.dtUltimoFat.getValue() != null) diffs = diffs.concat("dtUltimoFat: NULL>" + entidade.dtUltimoFat.getValue().toString() + "\n");
        else if (this.dtUltimoFat.getValue() != null && entidade.dtUltimoFat.getValue() == null) diffs = diffs.concat("dtUltimoFat:" + this.dtUltimoFat.getValue().toString() + ">NULL\n");
        else if (this.dtUltimoFat.getValue() != null && entidade.dtUltimoFat.getValue() != null)
            diffs = diffs.concat(!this.dtUltimoFat.getValue().equals(entidade.dtUltimoFat.getValue()) ? "dtUltimoFat:" + this.dtUltimoFat.getValue().toString() + ">" + entidade.dtUltimoFat.getValue().toString() + "\n" : "");
        if (this.email.getValue() == null && entidade.email.getValue() != null) diffs = diffs.concat("email: NULL>" + entidade.email.getValue().toString() + "\n");
        else if (this.email.getValue() != null && entidade.email.getValue() == null) diffs = diffs.concat("email:" + this.email.getValue().toString() + ">NULL\n");
        else if (this.email.getValue() != null && entidade.email.getValue() != null)
            diffs = diffs.concat(!this.email.getValue().equals(entidade.email.getValue()) ? "email:" + this.email.getValue().toString() + ">" + entidade.email.getValue().toString() + "\n" : "");
        if (this.endCob.getValue() == null && entidade.endCob.getValue() != null) diffs = diffs.concat("endCob: NULL>" + entidade.endCob.getValue().toString() + "\n");
        else if (this.endCob.getValue() != null && entidade.endCob.getValue() == null) diffs = diffs.concat("endCob:" + this.endCob.getValue().toString() + ">NULL\n");
        else if (this.endCob.getValue() != null && entidade.endCob.getValue() != null)
            diffs = diffs.concat(!this.endCob.getValue().equals(entidade.endCob.getValue()) ? "endCob:" + this.endCob.getValue().toString() + ">" + entidade.endCob.getValue().toString() + "\n" : "");
        if (this.endEnt.getValue() == null && entidade.endEnt.getValue() != null) diffs = diffs.concat("endEnt: NULL>" + entidade.endEnt.getValue().toString() + "\n");
        else if (this.endEnt.getValue() != null && entidade.endEnt.getValue() == null) diffs = diffs.concat("endEnt:" + this.endEnt.getValue().toString() + ">NULL\n");
        else if (this.endEnt.getValue() != null && entidade.endEnt.getValue() != null)
            diffs = diffs.concat(!this.endEnt.getValue().equals(entidade.endEnt.getValue()) ? "endEnt:" + this.endEnt.getValue().toString() + ">" + entidade.endEnt.getValue().toString() + "\n" : "");
        if (this.fax.getValue() == null && entidade.fax.getValue() != null) diffs = diffs.concat("fax: NULL>" + entidade.fax.getValue().toString() + "\n");
        else if (this.fax.getValue() != null && entidade.fax.getValue() == null) diffs = diffs.concat("fax:" + this.fax.getValue().toString() + ">NULL\n");
        else if (this.fax.getValue() != null && entidade.fax.getValue() != null)
            diffs = diffs.concat(!this.fax.getValue().equals(entidade.fax.getValue()) ? "fax:" + this.fax.getValue().toString() + ">" + entidade.fax.getValue().toString() + "\n" : "");
        if (this.grupo.getValue() == null && entidade.grupo.getValue() != null) diffs = diffs.concat("grupo: NULL>" + entidade.grupo.getValue().toString() + "\n");
        else if (this.grupo.getValue() != null && entidade.grupo.getValue() == null) diffs = diffs.concat("grupo:" + this.grupo.getValue().toString() + ">NULL\n");
        else if (this.grupo.getValue() != null && entidade.grupo.getValue() != null)
            diffs = diffs.concat(!this.grupo.getValue().equals(entidade.grupo.getValue()) ? "grupo:" + this.grupo.getValue().toString() + ">" + entidade.grupo.getValue().toString() + "\n" : "");
        if (this.inscCob.getValue() == null && entidade.inscCob.getValue() != null) diffs = diffs.concat("inscCob: NULL>" + entidade.inscCob.getValue().toString() + "\n");
        else if (this.inscCob.getValue() != null && entidade.inscCob.getValue() == null) diffs = diffs.concat("inscCob:" + this.inscCob.getValue().toString() + ">NULL\n");
        else if (this.inscCob.getValue() != null && entidade.inscCob.getValue() != null)
            diffs = diffs.concat(!this.inscCob.getValue().equals(entidade.inscCob.getValue()) ? "inscCob:" + this.inscCob.getValue().toString() + ">" + entidade.inscCob.getValue().toString() + "\n" : "");
        if (this.inscEnt.getValue() == null && entidade.inscEnt.getValue() != null) diffs = diffs.concat("inscEnt: NULL>" + entidade.inscEnt.getValue().toString() + "\n");
        else if (this.inscEnt.getValue() != null && entidade.inscEnt.getValue() == null) diffs = diffs.concat("inscEnt:" + this.inscEnt.getValue().toString() + ">NULL\n");
        else if (this.inscEnt.getValue() != null && entidade.inscEnt.getValue() != null)
            diffs = diffs.concat(!this.inscEnt.getValue().equals(entidade.inscEnt.getValue()) ? "inscEnt:" + this.inscEnt.getValue().toString() + ">" + entidade.inscEnt.getValue().toString() + "\n" : "");
        if (this.maiorAcumulo.getValue() == null && entidade.maiorAcumulo.getValue() != null) diffs = diffs.concat("maiorAcumulo: NULL>" + entidade.maiorAcumulo.getValue().toString() + "\n");
        else if (this.maiorAcumulo.getValue() != null && entidade.maiorAcumulo.getValue() == null) diffs = diffs.concat("maiorAcumulo:" + this.maiorAcumulo.getValue().toString() + ">NULL\n");
        else if (this.maiorAcumulo.getValue() != null && entidade.maiorAcumulo.getValue() != null)
            diffs = diffs.concat(!this.maiorAcumulo.getValue().equals(entidade.maiorAcumulo.getValue()) ? "maiorAcumulo:" + this.maiorAcumulo.getValue().toString() + ">" + entidade.maiorAcumulo.getValue().toString() + "\n" : "");
        if (this.natureza.getValue() == null && entidade.natureza.getValue() != null) diffs = diffs.concat("natureza: NULL>" + entidade.natureza.getValue().toString() + "\n");
        else if (this.natureza.getValue() != null && entidade.natureza.getValue() == null) diffs = diffs.concat("natureza:" + this.natureza.getValue().toString() + ">NULL\n");
        else if (this.natureza.getValue() != null && entidade.natureza.getValue() != null)
            diffs = diffs.concat(!this.natureza.getValue().equals(entidade.natureza.getValue()) ? "natureza:" + this.natureza.getValue().toString() + ">" + entidade.natureza.getValue().toString() + "\n" : "");
        if (this.nrloja.getValue() == null && entidade.nrloja.getValue() != null) diffs = diffs.concat("nrloja: NULL>" + entidade.nrloja.getValue().toString() + "\n");
        else if (this.nrloja.getValue() != null && entidade.nrloja.getValue() == null) diffs = diffs.concat("nrloja:" + this.nrloja.getValue().toString() + ">NULL\n");
        else if (this.nrloja.getValue() != null && entidade.nrloja.getValue() != null)
            diffs = diffs.concat(!this.nrloja.getValue().equals(entidade.nrloja.getValue()) ? "nrloja:" + this.nrloja.getValue().toString() + ">" + entidade.nrloja.getValue().toString() + "\n" : "");
        if (this.sitCli.getValue() == null && entidade.sitCli.getValue() != null) diffs = diffs.concat("sitCli: NULL>" + entidade.sitCli.getValue().toString() + "\n");
        else if (this.sitCli.getValue() != null && entidade.sitCli.getValue() == null) diffs = diffs.concat("sitCli:" + this.sitCli.getValue().toString() + ">NULL\n");
        else if (this.sitCli.getValue() != null && entidade.sitCli.getValue() != null)
            diffs = diffs.concat(!this.sitCli.getValue().equals(entidade.sitCli.getValue()) ? "sitCli:" + this.sitCli.getValue().toString() + ">" + entidade.sitCli.getValue().toString() + "\n" : "");
        if (this.tabela.getValue() == null && entidade.tabela.getValue() != null) diffs = diffs.concat("tabela: NULL>" + entidade.tabela.getValue().toString() + "\n");
        else if (this.tabela.getValue() != null && entidade.tabela.getValue() == null) diffs = diffs.concat("tabela:" + this.tabela.getValue().toString() + ">NULL\n");
        else if (this.tabela.getValue() != null && entidade.tabela.getValue() != null)
            diffs = diffs.concat(!this.tabela.getValue().equals(entidade.tabela.getValue()) ? "tabela:" + this.tabela.getValue().toString() + ">" + entidade.tabela.getValue().toString() + "\n" : "");
        if (this.telefone.getValue() == null && entidade.telefone.getValue() != null) diffs = diffs.concat("telefone: NULL>" + entidade.telefone.getValue().toString() + "\n");
        else if (this.telefone.getValue() != null && entidade.telefone.getValue() == null) diffs = diffs.concat("telefone:" + this.telefone.getValue().toString() + ">NULL\n");
        else if (this.telefone.getValue() != null && entidade.telefone.getValue() != null)
            diffs = diffs.concat(!this.telefone.getValue().equals(entidade.telefone.getValue()) ? "telefone:" + this.telefone.getValue().toString() + ">" + entidade.telefone.getValue().toString() + "\n" : "");
        if (this.tipo.getValue() == null && entidade.tipo.getValue() != null) diffs = diffs.concat("tipo: NULL>" + entidade.tipo.getValue().toString() + "\n");
        else if (this.tipo.getValue() != null && entidade.tipo.getValue() == null) diffs = diffs.concat("tipo:" + this.tipo.getValue().toString() + ">NULL\n");
        else if (this.tipo.getValue() != null && entidade.tipo.getValue() != null)
            diffs = diffs.concat(!this.tipo.getValue().equals(entidade.tipo.getValue()) ? "tipo:" + this.tipo.getValue().toString() + ">" + entidade.tipo.getValue().toString() + "\n" : "");
        if (this.transporte.getValue() == null && entidade.transporte.getValue() != null) diffs = diffs.concat("transporte: NULL>" + entidade.transporte.getValue().toString() + "\n");
        else if (this.transporte.getValue() != null && entidade.transporte.getValue() == null) diffs = diffs.concat("transporte:" + this.transporte.getValue().toString() + ">NULL\n");
        else if (this.transporte.getValue() != null && entidade.transporte.getValue() != null)
            diffs = diffs.concat(!this.transporte.getValue().equals(entidade.transporte.getValue()) ? "transporte:" + this.transporte.getValue().toString() + ">" + entidade.transporte.getValue().toString() + "\n" : "");
        if (this.ultimoFat.getValue() == null && entidade.ultimoFat.getValue() != null) diffs = diffs.concat("ultimoFat: NULL>" + entidade.ultimoFat.getValue().toString() + "\n");
        else if (this.ultimoFat.getValue() != null && entidade.ultimoFat.getValue() == null) diffs = diffs.concat("ultimoFat:" + this.ultimoFat.getValue().toString() + ">NULL\n");
        else if (this.ultimoFat.getValue() != null && entidade.ultimoFat.getValue() != null)
            diffs = diffs.concat(!this.ultimoFat.getValue().equals(entidade.ultimoFat.getValue()) ? "ultimoFat:" + this.ultimoFat.getValue().toString() + ">" + entidade.ultimoFat.getValue().toString() + "\n" : "");
        if (this.obs.getValue() == null && entidade.obs.getValue() != null) diffs = diffs.concat("obs: NULL>" + entidade.obs.getValue().toString() + "\n");
        else if (this.obs.getValue() != null && entidade.obs.getValue() == null) diffs = diffs.concat("obs:" + this.obs.getValue().toString() + ">NULL\n");
        else if (this.obs.getValue() != null && entidade.obs.getValue() != null)
            diffs = diffs.concat(!this.obs.getValue().equals(entidade.obs.getValue()) ? "obs:" + this.obs.getValue().toString() + ">" + entidade.obs.getValue().toString() + "\n" : "");
        if (this.contac.getValue() == null && entidade.contac.getValue() != null) diffs = diffs.concat("contac: NULL>" + entidade.contac.getValue().toString() + "\n");
        else if (this.contac.getValue() != null && entidade.contac.getValue() == null) diffs = diffs.concat("contac:" + this.contac.getValue().toString() + ">NULL\n");
        else if (this.contac.getValue() != null && entidade.contac.getValue() != null)
            diffs = diffs.concat(!this.contac.getValue().equals(entidade.contac.getValue()) ? "contac:" + this.contac.getValue().toString() + ">" + entidade.contac.getValue().toString() + "\n" : "");
        if (this.fantasia.getValue() == null && entidade.fantasia.getValue() != null) diffs = diffs.concat("fantasia: NULL>" + entidade.fantasia.getValue().toString() + "\n");
        else if (this.fantasia.getValue() != null && entidade.fantasia.getValue() == null) diffs = diffs.concat("fantasia:" + this.fantasia.getValue().toString() + ">NULL\n");
        else if (this.fantasia.getValue() != null && entidade.fantasia.getValue() != null)
            diffs = diffs.concat(!this.fantasia.getValue().equals(entidade.fantasia.getValue()) ? "fantasia:" + this.fantasia.getValue().toString() + ">" + entidade.fantasia.getValue().toString() + "\n" : "");
        if (this.consumos.getValue() == null && entidade.consumos.getValue() != null) diffs = diffs.concat("consumos: NULL>" + entidade.consumos.getValue().toString() + "\n");
        else if (this.consumos.getValue() != null && entidade.consumos.getValue() == null) diffs = diffs.concat("consumos:" + this.consumos.getValue().toString() + ">NULL\n");
        else if (this.consumos.getValue() != null && entidade.consumos.getValue() != null)
            diffs = diffs.concat(!this.consumos.getValue().equals(entidade.consumos.getValue()) ? "consumos:" + this.consumos.getValue().toString() + ">" + entidade.consumos.getValue().toString() + "\n" : "");
        if (this.royal.getValue() == null && entidade.royal.getValue() != null) diffs = diffs.concat("royal: NULL>" + entidade.royal.getValue().toString() + "\n");
        else if (this.royal.getValue() != null && entidade.royal.getValue() == null) diffs = diffs.concat("royal:" + this.royal.getValue().toString() + ">NULL\n");
        else if (this.royal.getValue() != null && entidade.royal.getValue() != null)
            diffs = diffs.concat(!this.royal.getValue().equals(entidade.royal.getValue()) ? "royal:" + this.royal.getValue().toString() + ">" + entidade.royal.getValue().toString() + "\n" : "");
        if (this.classe.getValue() == null && entidade.classe.getValue() != null) diffs = diffs.concat("classe: NULL>" + entidade.classe.getValue().toString() + "\n");
        else if (this.classe.getValue() != null && entidade.classe.getValue() == null) diffs = diffs.concat("classe:" + this.classe.getValue().toString() + ">NULL\n");
        else if (this.classe.getValue() != null && entidade.classe.getValue() != null)
            diffs = diffs.concat(!this.classe.getValue().equals(entidade.classe.getValue()) ? "classe:" + this.classe.getValue().toString() + ">" + entidade.classe.getValue().toString() + "\n" : "");
        if (this.tipoEntidade.getValue() == null && entidade.tipoEntidade.getValue() != null) diffs = diffs.concat("tipoEntidade: NULL>" + entidade.tipoEntidade.getValue().toString() + "\n");
        else if (this.tipoEntidade.getValue() != null && entidade.tipoEntidade.getValue() == null) diffs = diffs.concat("tipoEntidade:" + this.tipoEntidade.getValue().toString() + ">NULL\n");
        else if (this.tipoEntidade.getValue() != null && entidade.tipoEntidade.getValue() != null)
            diffs = diffs.concat(!this.tipoEntidade.getValue().equals(entidade.tipoEntidade.getValue()) ? "tipoEntidade:" + this.tipoEntidade.getValue().toString() + ">" + entidade.tipoEntidade.getValue().toString() + "\n" : "");
        if (this.contad.getValue() == null && entidade.contad.getValue() != null) diffs = diffs.concat("contad: NULL>" + entidade.contad.getValue().toString() + "\n");
        else if (this.contad.getValue() != null && entidade.contad.getValue() == null) diffs = diffs.concat("contad:" + this.contad.getValue().toString() + ">NULL\n");
        else if (this.contad.getValue() != null && entidade.contad.getValue() != null)
            diffs = diffs.concat(!this.contad.getValue().equals(entidade.contad.getValue()) ? "contad:" + this.contad.getValue().toString() + ">" + entidade.contad.getValue().toString() + "\n" : "");
        if (this.com1.getValue() == null && entidade.com1.getValue() != null) diffs = diffs.concat("com1: NULL>" + entidade.com1.getValue().toString() + "\n");
        else if (this.com1.getValue() != null && entidade.com1.getValue() == null) diffs = diffs.concat("com1:" + this.com1.getValue().toString() + ">NULL\n");
        else if (this.com1.getValue() != null && entidade.com1.getValue() != null)
            diffs = diffs.concat(!this.com1.getValue().equals(entidade.com1.getValue()) ? "com1:" + this.com1.getValue().toString() + ">" + entidade.com1.getValue().toString() + "\n" : "");
        if (this.com2.getValue() == null && entidade.com2.getValue() != null) diffs = diffs.concat("com2: NULL>" + entidade.com2.getValue().toString() + "\n");
        else if (this.com2.getValue() != null && entidade.com2.getValue() == null) diffs = diffs.concat("com2:" + this.com2.getValue().toString() + ">NULL\n");
        else if (this.com2.getValue() != null && entidade.com2.getValue() != null)
            diffs = diffs.concat(!this.com2.getValue().equals(entidade.com2.getValue()) ? "com2:" + this.com2.getValue().toString() + ">" + entidade.com2.getValue().toString() + "\n" : "");
        if (this.dias.getValue() == null && entidade.dias.getValue() != null) diffs = diffs.concat("dias: NULL>" + entidade.dias.getValue().toString() + "\n");
        else if (this.dias.getValue() != null && entidade.dias.getValue() == null) diffs = diffs.concat("dias:" + this.dias.getValue().toString() + ">NULL\n");
        else if (this.dias.getValue() != null && entidade.dias.getValue() != null)
            diffs = diffs.concat(!this.dias.getValue().equals(entidade.dias.getValue()) ? "dias:" + this.dias.getValue().toString() + ">" + entidade.dias.getValue().toString() + "\n" : "");
        if (this.bairroEnt.getValue() == null && entidade.bairroEnt.getValue() != null) diffs = diffs.concat("bairroEnt: NULL>" + entidade.bairroEnt.getValue().toString() + "\n");
        else if (this.bairroEnt.getValue() != null && entidade.bairroEnt.getValue() == null) diffs = diffs.concat("bairroEnt:" + this.bairroEnt.getValue().toString() + ">NULL\n");
        else if (this.bairroEnt.getValue() != null && entidade.bairroEnt.getValue() != null)
            diffs = diffs.concat(!this.bairroEnt.getValue().equals(entidade.bairroEnt.getValue()) ? "bairroEnt:" + this.bairroEnt.getValue().toString() + ">" + entidade.bairroEnt.getValue().toString() + "\n" : "");
        if (this.bairroCob.getValue() == null && entidade.bairroCob.getValue() != null) diffs = diffs.concat("bairroCob: NULL>" + entidade.bairroCob.getValue().toString() + "\n");
        else if (this.bairroCob.getValue() != null && entidade.bairroCob.getValue() == null) diffs = diffs.concat("bairroCob:" + this.bairroCob.getValue().toString() + ">NULL\n");
        else if (this.bairroCob.getValue() != null && entidade.bairroCob.getValue() != null)
            diffs = diffs.concat(!this.bairroCob.getValue().equals(entidade.bairroCob.getValue()) ? "bairroCob:" + this.bairroCob.getValue().toString() + ">" + entidade.bairroCob.getValue().toString() + "\n" : "");
        if (this.suframa.getValue() == null && entidade.suframa.getValue() != null) diffs = diffs.concat("suframa: NULL>" + entidade.suframa.getValue().toString() + "\n");
        else if (this.suframa.getValue() != null && entidade.suframa.getValue() == null) diffs = diffs.concat("suframa:" + this.suframa.getValue().toString() + ">NULL\n");
        else if (this.suframa.getValue() != null && entidade.suframa.getValue() != null)
            diffs = diffs.concat(!this.suframa.getValue().equals(entidade.suframa.getValue()) ? "suframa:" + this.suframa.getValue().toString() + ">" + entidade.suframa.getValue().toString() + "\n" : "");
        if (this.nomePai.getValue() == null && entidade.nomePai.getValue() != null) diffs = diffs.concat("nomePai: NULL>" + entidade.nomePai.getValue().toString() + "\n");
        else if (this.nomePai.getValue() != null && entidade.nomePai.getValue() == null) diffs = diffs.concat("nomePai:" + this.nomePai.getValue().toString() + ">NULL\n");
        else if (this.nomePai.getValue() != null && entidade.nomePai.getValue() != null)
            diffs = diffs.concat(!this.nomePai.getValue().equals(entidade.nomePai.getValue()) ? "nomePai:" + this.nomePai.getValue().toString() + ">" + entidade.nomePai.getValue().toString() + "\n" : "");
        if (this.nomeMae.getValue() == null && entidade.nomeMae.getValue() != null) diffs = diffs.concat("nomeMae: NULL>" + entidade.nomeMae.getValue().toString() + "\n");
        else if (this.nomeMae.getValue() != null && entidade.nomeMae.getValue() == null) diffs = diffs.concat("nomeMae:" + this.nomeMae.getValue().toString() + ">NULL\n");
        else if (this.nomeMae.getValue() != null && entidade.nomeMae.getValue() != null)
            diffs = diffs.concat(!this.nomeMae.getValue().equals(entidade.nomeMae.getValue()) ? "nomeMae:" + this.nomeMae.getValue().toString() + ">" + entidade.nomeMae.getValue().toString() + "\n" : "");
        if (this.sexo.getValue() == null && entidade.sexo.getValue() != null) diffs = diffs.concat("sexo: NULL>" + entidade.sexo.getValue().toString() + "\n");
        else if (this.sexo.getValue() != null && entidade.sexo.getValue() == null) diffs = diffs.concat("sexo:" + this.sexo.getValue().toString() + ">NULL\n");
        else if (this.sexo.getValue() != null && entidade.sexo.getValue() != null)
            diffs = diffs.concat(!this.sexo.getValue().equals(entidade.sexo.getValue()) ? "sexo:" + this.sexo.getValue().toString() + ">" + entidade.sexo.getValue().toString() + "\n" : "");
        if (this.numRg.getValue() == null && entidade.numRg.getValue() != null) diffs = diffs.concat("numRg: NULL>" + entidade.numRg.getValue().toString() + "\n");
        else if (this.numRg.getValue() != null && entidade.numRg.getValue() == null) diffs = diffs.concat("numRg:" + this.numRg.getValue().toString() + ">NULL\n");
        else if (this.numRg.getValue() != null && entidade.numRg.getValue() != null)
            diffs = diffs.concat(!this.numRg.getValue().equals(entidade.numRg.getValue()) ? "numRg:" + this.numRg.getValue().toString() + ">" + entidade.numRg.getValue().toString() + "\n" : "");
        if (this.emiRg.getValue() == null && entidade.emiRg.getValue() != null) diffs = diffs.concat("emiRg: NULL>" + entidade.emiRg.getValue().toString() + "\n");
        else if (this.emiRg.getValue() != null && entidade.emiRg.getValue() == null) diffs = diffs.concat("emiRg:" + this.emiRg.getValue().toString() + ">NULL\n");
        else if (this.emiRg.getValue() != null && entidade.emiRg.getValue() != null)
            diffs = diffs.concat(!this.emiRg.getValue().equals(entidade.emiRg.getValue()) ? "emiRg:" + this.emiRg.getValue().toString() + ">" + entidade.emiRg.getValue().toString() + "\n" : "");
        if (this.simples.getValue() == null && entidade.simples.getValue() != null) diffs = diffs.concat("simples: NULL>" + entidade.simples.getValue().toString() + "\n");
        else if (this.simples.getValue() != null && entidade.simples.getValue() == null) diffs = diffs.concat("simples:" + this.simples.getValue().toString() + ">NULL\n");
        else if (this.simples.getValue() != null && entidade.simples.getValue() != null)
            diffs = diffs.concat(!this.simples.getValue().equals(entidade.simples.getValue()) ? "simples:" + this.simples.getValue().toString() + ">" + entidade.simples.getValue().toString() + "\n" : "");
        if (this.redesp.getValue() == null && entidade.redesp.getValue() != null) diffs = diffs.concat("redesp: NULL>" + entidade.redesp.getValue().toString() + "\n");
        else if (this.redesp.getValue() != null && entidade.redesp.getValue() == null) diffs = diffs.concat("redesp:" + this.redesp.getValue().toString() + ">NULL\n");
        else if (this.redesp.getValue() != null && entidade.redesp.getValue() != null)
            diffs = diffs.concat(!this.redesp.getValue().equals(entidade.redesp.getValue()) ? "redesp:" + this.redesp.getValue().toString() + ">" + entidade.redesp.getValue().toString() + "\n" : "");
        if (this.conta.getValue() == null && entidade.conta.getValue() != null) diffs = diffs.concat("conta: NULL>" + entidade.conta.getValue().toString() + "\n");
        else if (this.conta.getValue() != null && entidade.conta.getValue() == null) diffs = diffs.concat("conta:" + this.conta.getValue().toString() + ">NULL\n");
        else if (this.conta.getValue() != null && entidade.conta.getValue() != null)
            diffs = diffs.concat(!this.conta.getValue().equals(entidade.conta.getValue()) ? "conta:" + this.conta.getValue().toString() + ">" + entidade.conta.getValue().toString() + "\n" : "");
        if (this.agencia.getValue() == null && entidade.agencia.getValue() != null) diffs = diffs.concat("agencia: NULL>" + entidade.agencia.getValue().toString() + "\n");
        else if (this.agencia.getValue() != null && entidade.agencia.getValue() == null) diffs = diffs.concat("agencia:" + this.agencia.getValue().toString() + ">NULL\n");
        else if (this.agencia.getValue() != null && entidade.agencia.getValue() != null)
            diffs = diffs.concat(!this.agencia.getValue().equals(entidade.agencia.getValue()) ? "agencia:" + this.agencia.getValue().toString() + ">" + entidade.agencia.getValue().toString() + "\n" : "");
        if (this.desconto.getValue() == null && entidade.desconto.getValue() != null) diffs = diffs.concat("desconto: NULL>" + entidade.desconto.getValue().toString() + "\n");
        else if (this.desconto.getValue() != null && entidade.desconto.getValue() == null) diffs = diffs.concat("desconto:" + this.desconto.getValue().toString() + ">NULL\n");
        else if (this.desconto.getValue() != null && entidade.desconto.getValue() != null)
            diffs = diffs.concat(!this.desconto.getValue().equals(entidade.desconto.getValue()) ? "desconto:" + this.desconto.getValue().toString() + ">" + entidade.desconto.getValue().toString() + "\n" : "");
        if (this.frete.getValue() == null && entidade.frete.getValue() != null) diffs = diffs.concat("frete: NULL>" + entidade.frete.getValue().toString() + "\n");
        else if (this.frete.getValue() != null && entidade.frete.getValue() == null) diffs = diffs.concat("frete:" + this.frete.getValue().toString() + ">NULL\n");
        else if (this.frete.getValue() != null && entidade.frete.getValue() != null)
            diffs = diffs.concat(!this.frete.getValue().equals(entidade.frete.getValue()) ? "frete:" + this.frete.getValue().toString() + ">" + entidade.frete.getValue().toString() + "\n" : "");
        if (this.site.getValue() == null && entidade.site.getValue() != null) diffs = diffs.concat("site: NULL>" + entidade.site.getValue().toString() + "\n");
        else if (this.site.getValue() != null && entidade.site.getValue() == null) diffs = diffs.concat("site:" + this.site.getValue().toString() + ">NULL\n");
        else if (this.site.getValue() != null && entidade.site.getValue() != null)
            diffs = diffs.concat(!this.site.getValue().equals(entidade.site.getValue()) ? "site:" + this.site.getValue().toString() + ">" + entidade.site.getValue().toString() + "\n" : "");
        if (this.inscEst.getValue() == null && entidade.inscEst.getValue() != null) diffs = diffs.concat("inscEst: NULL>" + entidade.inscEst.getValue().toString() + "\n");
        else if (this.inscEst.getValue() != null && entidade.inscEst.getValue() == null) diffs = diffs.concat("inscEst:" + this.inscEst.getValue().toString() + ">NULL\n");
        else if (this.inscEst.getValue() != null && entidade.inscEst.getValue() != null)
            diffs = diffs.concat(!this.inscEst.getValue().equals(entidade.inscEst.getValue()) ? "inscEst:" + this.inscEst.getValue().toString() + ">" + entidade.inscEst.getValue().toString() + "\n" : "");
        if (this.dataFund.getValue() == null && entidade.dataFund.getValue() != null) diffs = diffs.concat("dataFund: NULL>" + entidade.dataFund.getValue().toString() + "\n");
        else if (this.dataFund.getValue() != null && entidade.dataFund.getValue() == null) diffs = diffs.concat("dataFund:" + this.dataFund.getValue().toString() + ">NULL\n");
        else if (this.dataFund.getValue() != null && entidade.dataFund.getValue() != null)
            diffs = diffs.concat(!this.dataFund.getValue().equals(entidade.dataFund.getValue()) ? "dataFund:" + this.dataFund.getValue().toString() + ">" + entidade.dataFund.getValue().toString() + "\n" : "");
        if (this.atrasoMedio.getValue() == null && entidade.atrasoMedio.getValue() != null) diffs = diffs.concat("atrasoMedio: NULL>" + entidade.atrasoMedio.getValue().toString() + "\n");
        else if (this.atrasoMedio.getValue() != null && entidade.atrasoMedio.getValue() == null) diffs = diffs.concat("atrasoMedio:" + this.atrasoMedio.getValue().toString() + ">NULL\n");
        else if (this.atrasoMedio.getValue() != null && entidade.atrasoMedio.getValue() != null)
            diffs = diffs.concat(!this.atrasoMedio.getValue().equals(entidade.atrasoMedio.getValue()) ? "atrasoMedio:" + this.atrasoMedio.getValue().toString() + ">" + entidade.atrasoMedio.getValue().toString() + "\n" : "");
        if (this.iof.getValue() == null && entidade.iof.getValue() != null) diffs = diffs.concat("iof: NULL>" + entidade.iof.getValue().toString() + "\n");
        else if (this.iof.getValue() != null && entidade.iof.getValue() == null) diffs = diffs.concat("iof:" + this.iof.getValue().toString() + ">NULL\n");
        else if (this.iof.getValue() != null && entidade.iof.getValue() != null)
            diffs = diffs.concat(!this.iof.getValue().equals(entidade.iof.getValue()) ? "iof:" + this.iof.getValue().toString() + ">" + entidade.iof.getValue().toString() + "\n" : "");
        if (this.foneCob.getValue() == null && entidade.foneCob.getValue() != null) diffs = diffs.concat("foneCob: NULL>" + entidade.foneCob.getValue().toString() + "\n");
        else if (this.foneCob.getValue() != null && entidade.foneCob.getValue() == null) diffs = diffs.concat("foneCob:" + this.foneCob.getValue().toString() + ">NULL\n");
        else if (this.foneCob.getValue() != null && entidade.foneCob.getValue() != null)
            diffs = diffs.concat(!this.foneCob.getValue().equals(entidade.foneCob.getValue()) ? "foneCob:" + this.foneCob.getValue().toString() + ">" + entidade.foneCob.getValue().toString() + "\n" : "");
        if (this.historico.getValue() == null && entidade.historico.getValue() != null) diffs = diffs.concat("historico: NULL>" + entidade.historico.getValue().toString() + "\n");
        else if (this.historico.getValue() != null && entidade.historico.getValue() == null) diffs = diffs.concat("historico:" + this.historico.getValue().toString() + ">NULL\n");
        else if (this.historico.getValue() != null && entidade.historico.getValue() != null)
            diffs = diffs.concat(!this.historico.getValue().equals(entidade.historico.getValue()) ? "historico:" + this.historico.getValue().toString() + ">" + entidade.historico.getValue().toString() + "\n" : "");
        if (this.sitDup.getValue() == null && entidade.sitDup.getValue() != null) diffs = diffs.concat("sitDup: NULL>" + entidade.sitDup.getValue().toString() + "\n");
        else if (this.sitDup.getValue() != null && entidade.sitDup.getValue() == null) diffs = diffs.concat("sitDup:" + this.sitDup.getValue().toString() + ">NULL\n");
        else if (this.sitDup.getValue() != null && entidade.sitDup.getValue() != null)
            diffs = diffs.concat(!this.sitDup.getValue().equals(entidade.sitDup.getValue()) ? "sitDup:" + this.sitDup.getValue().toString() + ">" + entidade.sitDup.getValue().toString() + "\n" : "");
        if (this.prazo.getValue() == null && entidade.prazo.getValue() != null) diffs = diffs.concat("prazo: NULL>" + entidade.prazo.getValue().toString() + "\n");
        else if (this.prazo.getValue() != null && entidade.prazo.getValue() == null) diffs = diffs.concat("prazo:" + this.prazo.getValue().toString() + ">NULL\n");
        else if (this.prazo.getValue() != null && entidade.prazo.getValue() != null)
            diffs = diffs.concat(!this.prazo.getValue().equals(entidade.prazo.getValue()) ? "prazo:" + this.prazo.getValue().toString() + ">" + entidade.prazo.getValue().toString() + "\n" : "");
        if (this.tpPag.getValue() == null && entidade.tpPag.getValue() != null) diffs = diffs.concat("tpPag: NULL>" + entidade.tpPag.getValue().toString() + "\n");
        else if (this.tpPag.getValue() != null && entidade.tpPag.getValue() == null) diffs = diffs.concat("tpPag:" + this.tpPag.getValue().toString() + ">NULL\n");
        else if (this.tpPag.getValue() != null && entidade.tpPag.getValue() != null)
            diffs = diffs.concat(!this.tpPag.getValue().equals(entidade.tpPag.getValue()) ? "tpPag:" + this.tpPag.getValue().toString() + ">" + entidade.tpPag.getValue().toString() + "\n" : "");
        if (this.dtAnalise.getValue() == null && entidade.dtAnalise.getValue() != null) diffs = diffs.concat("dtAnalise: NULL>" + entidade.dtAnalise.getValue().toString() + "\n");
        else if (this.dtAnalise.getValue() != null && entidade.dtAnalise.getValue() == null) diffs = diffs.concat("dtAnalise:" + this.dtAnalise.getValue().toString() + ">NULL\n");
        else if (this.dtAnalise.getValue() != null && entidade.dtAnalise.getValue() != null)
            diffs = diffs.concat(!this.dtAnalise.getValue().equals(entidade.dtAnalise.getValue()) ? "dtAnalise:" + this.dtAnalise.getValue().toString() + ">" + entidade.dtAnalise.getValue().toString() + "\n" : "");
        if (this.dtPrevAnalise.getValue() == null && entidade.dtPrevAnalise.getValue() != null) diffs = diffs.concat("dtPrevAnalise: NULL>" + entidade.dtPrevAnalise.getValue().toString() + "\n");
        else if (this.dtPrevAnalise.getValue() != null && entidade.dtPrevAnalise.getValue() == null) diffs = diffs.concat("dtPrevAnalise:" + this.dtPrevAnalise.getValue().toString() + ">NULL\n");
        else if (this.dtPrevAnalise.getValue() != null && entidade.dtPrevAnalise.getValue() != null)
            diffs = diffs.concat(!this.dtPrevAnalise.getValue().equals(entidade.dtPrevAnalise.getValue()) ? "dtPrevAnalise:" + this.dtPrevAnalise.getValue().toString() + ">" + entidade.dtPrevAnalise.getValue().toString() + "\n" : "");
        if (this.classifica.getValue() == null && entidade.classifica.getValue() != null) diffs = diffs.concat("classifica: NULL>" + entidade.classifica.getValue().toString() + "\n");
        else if (this.classifica.getValue() != null && entidade.classifica.getValue() == null) diffs = diffs.concat("classifica:" + this.classifica.getValue().toString() + ">NULL\n");
        else if (this.classifica.getValue() != null && entidade.classifica.getValue() != null)
            diffs = diffs.concat(!this.classifica.getValue().equals(entidade.classifica.getValue()) ? "classifica:" + this.classifica.getValue().toString() + ">" + entidade.classifica.getValue().toString() + "\n" : "");
        if (this.obsNota.getValue() == null && entidade.obsNota.getValue() != null) diffs = diffs.concat("obsNota: NULL>" + entidade.obsNota.getValue().toString() + "\n");
        else if (this.obsNota.getValue() != null && entidade.obsNota.getValue() == null) diffs = diffs.concat("obsNota:" + this.obsNota.getValue().toString() + ">NULL\n");
        else if (this.obsNota.getValue() != null && entidade.obsNota.getValue() != null)
            diffs = diffs.concat(!this.obsNota.getValue().equals(entidade.obsNota.getValue()) ? "obsNota:" + this.obsNota.getValue().toString() + ">" + entidade.obsNota.getValue().toString() + "\n" : "");
        if (this.obsFinan.getValue() == null && entidade.obsFinan.getValue() != null) diffs = diffs.concat("obsFinan: NULL>" + entidade.obsFinan.getValue().toString() + "\n");
        else if (this.obsFinan.getValue() != null && entidade.obsFinan.getValue() == null) diffs = diffs.concat("obsFinan:" + this.obsFinan.getValue().toString() + ">NULL\n");
        else if (this.obsFinan.getValue() != null && entidade.obsFinan.getValue() != null)
            diffs = diffs.concat(!this.obsFinan.getValue().equals(entidade.obsFinan.getValue()) ? "obsFinan:" + this.obsFinan.getValue().toString() + ">" + entidade.obsFinan.getValue().toString() + "\n" : "");
        if (this.codMot.getValue() == null && entidade.codMot.getValue() != null) diffs = diffs.concat("codMot: NULL>" + entidade.codMot.getValue().toString() + "\n");
        else if (this.codMot.getValue() != null && entidade.codMot.getValue() == null) diffs = diffs.concat("codMot:" + this.codMot.getValue().toString() + ">NULL\n");
        else if (this.codMot.getValue() != null && entidade.codMot.getValue() != null)
            diffs = diffs.concat(!this.codMot.getValue().equals(entidade.codMot.getValue()) ? "codMot:" + this.codMot.getValue().toString() + ">" + entidade.codMot.getValue().toString() + "\n" : "");
        if (this.atividade.getValue() == null && entidade.atividade.getValue() != null) diffs = diffs.concat("atividade: NULL>" + entidade.atividade.getValue().toString() + "\n");
        else if (this.atividade.getValue() != null && entidade.atividade.getValue() == null) diffs = diffs.concat("atividade:" + this.atividade.getValue().toString() + ">NULL\n");
        else if (this.atividade.getValue() != null && entidade.atividade.getValue() != null)
            diffs = diffs.concat(!this.atividade.getValue().equals(entidade.atividade.getValue()) ? "atividade:" + this.atividade.getValue().toString() + ">" + entidade.atividade.getValue().toString() + "\n" : "");
        if (this.codPais.getValue() == null && entidade.codPais.getValue() != null) diffs = diffs.concat("codPais: NULL>" + entidade.codPais.getValue().toString() + "\n");
        else if (this.codPais.getValue() != null && entidade.codPais.getValue() == null) diffs = diffs.concat("codPais:" + this.codPais.getValue().toString() + ">NULL\n");
        else if (this.codPais.getValue() != null && entidade.codPais.getValue() != null)
            diffs = diffs.concat(!this.codPais.getValue().equals(entidade.codPais.getValue()) ? "codPais:" + this.codPais.getValue().toString() + ">" + entidade.codPais.getValue().toString() + "\n" : "");
        if (this.cif.getValue() == null && entidade.cif.getValue() != null) diffs = diffs.concat("cif: NULL>" + entidade.cif.getValue().toString() + "\n");
        else if (this.cif.getValue() != null && entidade.cif.getValue() == null) diffs = diffs.concat("cif:" + this.cif.getValue().toString() + ">NULL\n");
        else if (this.cif.getValue() != null && entidade.cif.getValue() != null)
            diffs = diffs.concat(!this.cif.getValue().equals(entidade.cif.getValue()) ? "cif:" + this.cif.getValue().toString() + ">" + entidade.cif.getValue().toString() + "\n" : "");
        if (this.redespCif.getValue() == null && entidade.redespCif.getValue() != null) diffs = diffs.concat("redespCif: NULL>" + entidade.redespCif.getValue().toString() + "\n");
        else if (this.redespCif.getValue() != null && entidade.redespCif.getValue() == null) diffs = diffs.concat("redespCif:" + this.redespCif.getValue().toString() + ">NULL\n");
        else if (this.redespCif.getValue() != null && entidade.redespCif.getValue() != null)
            diffs = diffs.concat(!this.redespCif.getValue().equals(entidade.redespCif.getValue()) ? "redespCif:" + this.redespCif.getValue().toString() + ">" + entidade.redespCif.getValue().toString() + "\n" : "");
        if (this.complemento.getValue() == null && entidade.complemento.getValue() != null) diffs = diffs.concat("complemento: NULL>" + entidade.complemento.getValue().toString() + "\n");
        else if (this.complemento.getValue() != null && entidade.complemento.getValue() == null) diffs = diffs.concat("complemento:" + this.complemento.getValue().toString() + ">NULL\n");
        else if (this.complemento.getValue() != null && entidade.complemento.getValue() != null)
            diffs = diffs.concat(!this.complemento.getValue().equals(entidade.complemento.getValue()) ? "complemento:" + this.complemento.getValue().toString() + ">" + entidade.complemento.getValue().toString() + "\n" : "");
        if (this.maiorFat.getValue() == null && entidade.maiorFat.getValue() != null) diffs = diffs.concat("maiorFat: NULL>" + entidade.maiorFat.getValue().toString() + "\n");
        else if (this.maiorFat.getValue() != null && entidade.maiorFat.getValue() == null) diffs = diffs.concat("maiorFat:" + this.maiorFat.getValue().toString() + ">NULL\n");
        else if (this.maiorFat.getValue() != null && entidade.maiorFat.getValue() != null)
            diffs = diffs.concat(!this.maiorFat.getValue().equals(entidade.maiorFat.getValue()) ? "maiorFat:" + this.maiorFat.getValue().toString() + ">" + entidade.maiorFat.getValue().toString() + "\n" : "");
        if (this.dtMaiorFat.getValue() == null && entidade.dtMaiorFat.getValue() != null) diffs = diffs.concat("dtMaiorFat: NULL>" + entidade.dtMaiorFat.getValue().toString() + "\n");
        else if (this.dtMaiorFat.getValue() != null && entidade.dtMaiorFat.getValue() == null) diffs = diffs.concat("dtMaiorFat:" + this.dtMaiorFat.getValue().toString() + ">NULL\n");
        else if (this.dtMaiorFat.getValue() != null && entidade.dtMaiorFat.getValue() != null)
            diffs = diffs.concat(!this.dtMaiorFat.getValue().equals(entidade.dtMaiorFat.getValue()) ? "dtMaiorFat:" + this.dtMaiorFat.getValue().toString() + ">" + entidade.dtMaiorFat.getValue().toString() + "\n" : "");
        if (this.numEnd.getValue() == null && entidade.numEnd.getValue() != null) diffs = diffs.concat("numEnd: NULL>" + entidade.numEnd.getValue().toString() + "\n");
        else if (this.numEnd.getValue() != null && entidade.numEnd.getValue() == null) diffs = diffs.concat("numEnd:" + this.numEnd.getValue().toString() + ">NULL\n");
        else if (this.numEnd.getValue() != null && entidade.numEnd.getValue() != null)
            diffs = diffs.concat(!this.numEnd.getValue().equals(entidade.numEnd.getValue()) ? "numEnd:" + this.numEnd.getValue().toString() + ">" + entidade.numEnd.getValue().toString() + "\n" : "");
        if (this.numEnt.getValue() == null && entidade.numEnt.getValue() != null) diffs = diffs.concat("numEnt: NULL>" + entidade.numEnt.getValue().toString() + "\n");
        else if (this.numEnt.getValue() != null && entidade.numEnt.getValue() == null) diffs = diffs.concat("numEnt:" + this.numEnt.getValue().toString() + ">NULL\n");
        else if (this.numEnt.getValue() != null && entidade.numEnt.getValue() != null)
            diffs = diffs.concat(!this.numEnt.getValue().equals(entidade.numEnt.getValue()) ? "numEnt:" + this.numEnt.getValue().toString() + ">" + entidade.numEnt.getValue().toString() + "\n" : "");
        if (this.numCob.getValue() == null && entidade.numCob.getValue() != null) diffs = diffs.concat("numCob: NULL>" + entidade.numCob.getValue().toString() + "\n");
        else if (this.numCob.getValue() != null && entidade.numCob.getValue() == null) diffs = diffs.concat("numCob:" + this.numCob.getValue().toString() + ">NULL\n");
        else if (this.numCob.getValue() != null && entidade.numCob.getValue() != null)
            diffs = diffs.concat(!this.numCob.getValue().equals(entidade.numCob.getValue()) ? "numCob:" + this.numCob.getValue().toString() + ">" + entidade.numCob.getValue().toString() + "\n" : "");
        if (this.aliqSt.getValue() == null && entidade.aliqSt.getValue() != null) diffs = diffs.concat("aliqSt: NULL>" + entidade.aliqSt.getValue().toString() + "\n");
        else if (this.aliqSt.getValue() != null && entidade.aliqSt.getValue() == null) diffs = diffs.concat("aliqSt:" + this.aliqSt.getValue().toString() + ">NULL\n");
        else if (this.aliqSt.getValue() != null && entidade.aliqSt.getValue() != null)
            diffs = diffs.concat(!this.aliqSt.getValue().equals(entidade.aliqSt.getValue()) ? "aliqSt:" + this.aliqSt.getValue().toString() + ">" + entidade.aliqSt.getValue().toString() + "\n" : "");
        if (this.bonif.getValue() == null && entidade.bonif.getValue() != null) diffs = diffs.concat("bonif: NULL>" + entidade.bonif.getValue().toString() + "\n");
        else if (this.bonif.getValue() != null && entidade.bonif.getValue() == null) diffs = diffs.concat("bonif:" + this.bonif.getValue().toString() + ">NULL\n");
        else if (this.bonif.getValue() != null && entidade.bonif.getValue() != null)
            diffs = diffs.concat(!this.bonif.getValue().equals(entidade.bonif.getValue()) ? "bonif:" + this.bonif.getValue().toString() + ">" + entidade.bonif.getValue().toString() + "\n" : "");
        if (this.compEnt.getValue() == null && entidade.compEnt.getValue() != null) diffs = diffs.concat("compEnt: NULL>" + entidade.compEnt.getValue().toString() + "\n");
        else if (this.compEnt.getValue() != null && entidade.compEnt.getValue() == null) diffs = diffs.concat("compEnt:" + this.compEnt.getValue().toString() + ">NULL\n");
        else if (this.compEnt.getValue() != null && entidade.compEnt.getValue() != null)
            diffs = diffs.concat(!this.compEnt.getValue().equals(entidade.compEnt.getValue()) ? "compEnt:" + this.compEnt.getValue().toString() + ">" + entidade.compEnt.getValue().toString() + "\n" : "");
        return diffs + "]";
    }
    
    @Transient
    public String stringToLog() {
        String toLog = new ToStringBuilder(this)
                .append("codcli", codcli.getValue() == null ? "Null" : codcli.getValue().toString())
                .append("nome", nome.getValue() == null ? "Null" : nome.getValue().toString())
                .append("inscricao", inscricao.getValue() == null ? "Null" : inscricao.getValue().toString())
                .append("cnpj", cnpj.getValue() == null ? "Null" : cnpj.getValue().toString())
                .append("endereco", endereco.getValue() == null ? "Null" : endereco.getValue().toString())
                .append("ativo", ativo.getValue() == null ? "Null" : ativo.getValue().toString())
                .append("bairro", bairro.getValue() == null ? "Null" : bairro.getValue().toString())
                .append("banco", banco.getValue() == null ? "Null" : banco.getValue().toString())
                .append("bloqueio", bloqueio.getValue() == null ? "Null" : bloqueio.getValue().toString())
                .append("cep", cep.getValue() == null ? "Null" : cep.getValue().toString())
                .append("cepCob", cepCob.getValue() == null ? "Null" : cepCob.getValue().toString())
                .append("cepEnt", cepEnt.getValue() == null ? "Null" : cepEnt.getValue().toString())
                .append("cnpjCob", cnpjCob.getValue() == null ? "Null" : cnpjCob.getValue().toString())
                .append("cnpjEnt", cnpjEnt.getValue() == null ? "Null" : cnpjEnt.getValue().toString())
                .append("codrep", codrep.getValue() == null ? "Null" : codrep.getValue().toString())
                .append("condicao", condicao.getValue() == null ? "Null" : condicao.getValue().toString())
                .append("credito", credito.getValue() == null ? "Null" : credito.getValue().toString())
                .append("dataAcumulo", dataAcumulo.getValue() == null ? "Null" : dataAcumulo.getValue().toString())
                .append("dataCad", dataCad.getValue() == null ? "Null" : dataCad.getValue().toString())
                .append("dtNasc", dtNasc.getValue() == null ? "Null" : dtNasc.getValue().toString())
                .append("dtUltimoFat", dtUltimoFat.getValue() == null ? "Null" : dtUltimoFat.getValue().toString())
                .append("email", email.getValue() == null ? "Null" : email.getValue().toString())
                .append("endCob", endCob.getValue() == null ? "Null" : endCob.getValue().toString())
                .append("endEnt", endEnt.getValue() == null ? "Null" : endEnt.getValue().toString())
                .append("fax", fax.getValue() == null ? "Null" : fax.getValue().toString())
                .append("grupo", grupo.getValue() == null ? "Null" : grupo.getValue().toString())
                .append("inscCob", inscCob.getValue() == null ? "Null" : inscCob.getValue().toString())
                .append("inscEnt", inscEnt.getValue() == null ? "Null" : inscEnt.getValue().toString())
                .append("maiorAcumulo", maiorAcumulo.getValue() == null ? "Null" : maiorAcumulo.getValue().toString())
                .append("natureza", natureza.getValue() == null ? "Null" : natureza.getValue().toString())
                .append("nrloja", nrloja.getValue() == null ? "Null" : nrloja.getValue().toString())
                .append("sitCli", sitCli.getValue() == null ? "Null" : sitCli.getValue().toString())
                .append("tabela", tabela.getValue() == null ? "Null" : tabela.getValue().toString())
                .append("telefone", telefone.getValue() == null ? "Null" : telefone.getValue().toString())
                .append("tipo", tipo.getValue() == null ? "Null" : tipo.getValue().toString())
                .append("transporte", transporte.getValue() == null ? "Null" : transporte.getValue().toString())
                .append("ultimoFat", ultimoFat.getValue() == null ? "Null" : ultimoFat.getValue().toString())
                .append("obs", obs.getValue() == null ? "Null" : obs.getValue().toString())
                .append("contac", contac.getValue() == null ? "Null" : contac.getValue().toString())
                .append("fantasia", fantasia.getValue() == null ? "Null" : fantasia.getValue().toString())
                .append("consumos", consumos.getValue() == null ? "Null" : consumos.getValue().toString())
                .append("royal", royal.getValue() == null ? "Null" : royal.getValue().toString())
                .append("classe", classe.getValue() == null ? "Null" : classe.getValue().toString())
                .append("tipoEntidade", tipoEntidade.getValue() == null ? "Null" : tipoEntidade.getValue().toString())
                .append("contad", contad.getValue() == null ? "Null" : contad.getValue().toString())
                .append("com1", com1.getValue() == null ? "Null" : com1.getValue().toString())
                .append("com2", com2.getValue() == null ? "Null" : com2.getValue().toString())
                .append("dias", dias.getValue() == null ? "Null" : dias.getValue().toString())
                .append("bairroEnt", bairroEnt.getValue() == null ? "Null" : bairroEnt.getValue().toString())
                .append("bairroCob", bairroCob.getValue() == null ? "Null" : bairroCob.getValue().toString())
                .append("suframa", suframa.getValue() == null ? "Null" : suframa.getValue().toString())
                .append("nomePai", nomePai.getValue() == null ? "Null" : nomePai.getValue().toString())
                .append("nomeMae", nomeMae.getValue() == null ? "Null" : nomeMae.getValue().toString())
                .append("sexo", sexo.getValue() == null ? "Null" : sexo.getValue().toString())
                .append("numRg", numRg.getValue() == null ? "Null" : numRg.getValue().toString())
                .append("emiRg", emiRg.getValue() == null ? "Null" : emiRg.getValue().toString())
                .append("simples", simples.getValue() == null ? "Null" : simples.getValue().toString())
                .append("redesp", redesp.getValue() == null ? "Null" : redesp.getValue().toString())
                .append("conta", conta.getValue() == null ? "Null" : conta.getValue().toString())
                .append("agencia", agencia.getValue() == null ? "Null" : agencia.getValue().toString())
                .append("desconto", desconto.getValue() == null ? "Null" : desconto.getValue().toString())
                .append("frete", frete.getValue() == null ? "Null" : frete.getValue().toString())
                .append("site", site.getValue() == null ? "Null" : site.getValue().toString())
                .append("inscEst", inscEst.getValue() == null ? "Null" : inscEst.getValue().toString())
                .append("dataFund", dataFund.getValue() == null ? "Null" : dataFund.getValue().toString())
                .append("atrasoMedio", atrasoMedio.getValue() == null ? "Null" : atrasoMedio.getValue().toString())
                .append("iof", iof.getValue() == null ? "Null" : iof.getValue().toString())
                .append("foneCob", foneCob.getValue() == null ? "Null" : foneCob.getValue().toString())
                .append("historico", historico.getValue() == null ? "Null" : historico.getValue().toString())
                .append("sitDup", sitDup.getValue() == null ? "Null" : sitDup.getValue().toString())
                .append("prazo", prazo.getValue() == null ? "Null" : prazo.getValue().toString())
                .append("tpPag", tpPag.getValue() == null ? "Null" : tpPag.getValue().toString())
                .append("dtAnalise", dtAnalise.getValue() == null ? "Null" : dtAnalise.getValue().toString())
                .append("dtPrevAnalise", dtPrevAnalise.getValue() == null ? "Null" : dtPrevAnalise.getValue().toString())
                .append("classifica", classifica.getValue() == null ? "Null" : classifica.getValue().toString())
                .append("obsNota", obsNota.getValue() == null ? "Null" : obsNota.getValue().toString())
                .append("obsFinan", obsFinan.getValue() == null ? "Null" : obsFinan.getValue().toString())
                .append("codMot", codMot.getValue() == null ? "Null" : codMot.getValue().toString())
                .append("atividade", atividade.getValue() == null ? "Null" : atividade.getValue().toString())
                .append("codPais", codPais.getValue() == null ? "Null" : codPais.getValue().toString())
                .append("cif", cif.getValue() == null ? "Null" : cif.getValue().toString())
                .append("redespCif", redespCif.getValue() == null ? "Null" : redespCif.getValue().toString())
                .append("complemento", complemento.getValue() == null ? "Null" : complemento.getValue().toString())
                .append("maiorFat", maiorFat.getValue() == null ? "Null" : maiorFat.getValue().toString())
                .append("dtMaiorFat", dtMaiorFat.getValue() == null ? "Null" : dtMaiorFat.getValue().toString())
                .append("numEnd", numEnd.getValue() == null ? "Null" : numEnd.getValue().toString())
                .append("numEnt", numEnt.getValue() == null ? "Null" : numEnt.getValue().toString())
                .append("numCob", numCob.getValue() == null ? "Null" : numCob.getValue().toString())
                .append("aliqSt", aliqSt.getValue() == null ? "Null" : aliqSt.getValue().toString())
                .append("bonif", bonif.getValue() == null ? "Null" : bonif.getValue().toString())
                .append("compEnt", compEnt.getValue() == null ? "Null" : compEnt.getValue().toString())
                .toString();
        marcasEntidade.forEach(marca -> {
            toLog.concat(marca.stringToLog());
        });
        
        return toLog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entidade entidade = (Entidade) o;
        return Objects.equals(codcli, entidade.codcli);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codcli);
    }

    @Override
    public String toString() {
        return "[" + this.codcli.get() + "] " + this.nome.get();
    }
}
