package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.sysdeliz.SdModelagem;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/09/2019 10:06
 */
@Entity
@Table(name = "ETQ_PROD_001")
@TelaSysDeliz(descricao = "Etiqueta Produto", icon = "produto (4).png")
public class EtqProd extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 250)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty obs = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 200)
    @ColunaFilter(descricao = "Grupo", coluna = "grupo")
    private final StringProperty grupo = new SimpleStringProperty();
    @Transient
    private final StringProperty modelagem = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<SdGrupoModelagem> codGrupo = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<SdModelagem> codModelagem = new SimpleObjectProperty<>();

    public EtqProd() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(String.valueOf(codigo));
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "OBS")
    @Lob
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "MODELAGEM")
    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }

    @OneToOne
    @JoinColumn(name = "COD_GRUPO")
    public SdGrupoModelagem getCodGrupo() {
        return codGrupo.get();
    }

    public ObjectProperty<SdGrupoModelagem> codGrupoProperty() {
        return codGrupo;
    }

    public void setCodGrupo(SdGrupoModelagem codGrupo) {
        this.codGrupo.set(codGrupo);
    }

    @OneToOne
    @JoinColumn(name = "COD_MODELAGEM")
    public SdModelagem getCodModelagem() {
        return codModelagem.get();
    }

    public ObjectProperty<SdModelagem> codModelagemProperty() {
        return codModelagem;
    }

    public void setCodModelagem(SdModelagem codModelagem) {
        this.codModelagem.set(codModelagem);
    }
}
