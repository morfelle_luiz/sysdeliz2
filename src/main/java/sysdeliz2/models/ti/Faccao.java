package sysdeliz2.models.ti;

import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "FACCAO_001")
@Entity
public class Faccao {

    private final ObjectProperty<FaccaoPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtorig = new SimpleIntegerProperty();
    private final IntegerProperty quant = new SimpleIntegerProperty();
    private final IntegerProperty quant2 = new SimpleIntegerProperty();
    private final IntegerProperty quanti = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> unitario = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dts = new SimpleObjectProperty<LocalDate>();
    private final StringProperty fluxo = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtr = new SimpleObjectProperty<LocalDate>();
    private final StringProperty obs = new SimpleStringProperty();
    private final IntegerProperty idant = new SimpleIntegerProperty();
    private final IntegerProperty quantf = new SimpleIntegerProperty();
    private final StringProperty reprocesso = new SimpleStringProperty();
    private final StringProperty notarem = new SimpleStringProperty();

    public Faccao() {
    }

    @EmbeddedId
    public FaccaoPK getId() {
        return id.get();
    }

    public ObjectProperty<FaccaoPK> idProperty() {
        return id;
    }

    public void setId(FaccaoPK id) {
        this.id.set(id);
    }

    @Column(name = "QT_ORIG")
    public Integer getQtorig() {
        return qtorig.get();
    }

    public IntegerProperty qtorigProperty() {
        return qtorig;
    }

    public void setQtorig(Integer qtorig) {
        this.qtorig.set(qtorig);
    }

    @Column(name = "QUANT")
    public Integer getQuant() {
        return quant.get();
    }

    public IntegerProperty quantProperty() {
        return quant;
    }

    public void setQuant(Integer quant) {
        this.quant.set(quant);
    }

    @Column(name = "QUANT_2")
    public Integer getQuant2() {
        return quant2.get();
    }

    public IntegerProperty quant2Property() {
        return quant2;
    }

    public void setQuant2(Integer quant2) {
        this.quant2.set(quant2);
    }

    @Column(name = "QUANT_I")
    public Integer getQuanti() {
        return quanti.get();
    }

    public IntegerProperty quantiProperty() {
        return quanti;
    }

    public void setQuanti(Integer quanti) {
        this.quanti.set(quanti);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "UNITARIO")
    public BigDecimal getUnitario() {
        return unitario.get();
    }

    public ObjectProperty<BigDecimal> unitarioProperty() {
        return unitario;
    }

    public void setUnitario(BigDecimal unitario) {
        this.unitario.set(unitario);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "DT_S")
    public LocalDate getDts() {
        return dts.get();
    }

    public ObjectProperty<LocalDate> dtsProperty() {
        return dts;
    }

    public void setDts(LocalDate dts) {
        this.dts.set(dts);
    }

    @Column(name = "FLUXO")
    public String getFluxo() {
        return fluxo.get();
    }

    public StringProperty fluxoProperty() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo.set(fluxo);
    }

    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Column(name = "DT_R")
    public LocalDate getDtr() {
        return dtr.get();
    }

    public ObjectProperty<LocalDate> dtrProperty() {
        return dtr;
    }

    public void setDtr(LocalDate dtr) {
        this.dtr.set(dtr);
    }

    @Column(name = "OBS")
    @Lob
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "ID_ANT")
    public Integer getIdant() {
        return idant.get();
    }

    public IntegerProperty idantProperty() {
        return idant;
    }

    public void setIdant(Integer idant) {
        this.idant.set(idant);
    }

    @Column(name = "QUANT_F")
    public Integer getQuantf() {
        return quantf.get();
    }

    public IntegerProperty quantfProperty() {
        return quantf;
    }

    public void setQuantf(Integer quantf) {
        this.quantf.set(quantf);
    }

    @Column(name = "REPROCESSO")
    public String getReprocesso() {
        return reprocesso.get();
    }

    public StringProperty reprocessoProperty() {
        return reprocesso;
    }

    public void setReprocesso(String reprocesso) {
        this.reprocesso.set(reprocesso);
    }

    @Column(name = "NOTA_REM")
    public String getNotarem() {
        return notarem.get();
    }

    public StringProperty notaremProperty() {
        return notarem;
    }

    public void setNotarem(String notarem) {
        this.notarem.set(notarem);
    }

}