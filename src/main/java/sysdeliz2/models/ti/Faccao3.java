package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "FACCAO3_001")
@Entity
public class Faccao3 {

    private final ObjectProperty<Faccao3PK> id = new SimpleObjectProperty<>();
    private final IntegerProperty quanti = new SimpleIntegerProperty(0);
    private final IntegerProperty quant2 = new SimpleIntegerProperty(0);
    private final IntegerProperty quant = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> dtlan = new SimpleObjectProperty<>();
    private final StringProperty notarem = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> unitario = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty qtorig = new SimpleIntegerProperty();
    private final StringProperty codcliant = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty numap = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty pago = new SimpleStringProperty();
    private final IntegerProperty idant = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtpagto = new SimpleObjectProperty<>();
    private final IntegerProperty quantf = new SimpleIntegerProperty();
    private final StringProperty nota = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty qual = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty reprocesso = new SimpleStringProperty();
    private final StringProperty sysdeliz = new SimpleStringProperty();

    public Faccao3() {
    }

    public Faccao3(Faccao faccao, String deposito, SdItemCaixaPA itemCaixa) {
        this.id.set(new Faccao3PK(faccao));
        this.quanti.set(faccao.getQuanti());

        if (itemCaixa.getCaixa().isSegunda())
            this.quant2.set(itemCaixa.getQtde());
        else if (itemCaixa.getCaixa().isIncompleta())
            this.quanti.set(itemCaixa.getQtde());
        else if (itemCaixa.getCaixa().isPerdida())
            this.quantf.set(itemCaixa.getQtde());
        else
            this.quant.set(itemCaixa.getQtde());

        this.preco.set(faccao.getPreco());
        this.valor.set(faccao.getValor());
        this.dtlan.set(LocalDate.now());
        this.notarem.set(faccao.getNotarem());
        this.unitario.set(faccao.getUnitario());
        this.qtorig.set(faccao.getQtorig());
        this.codcliant.set(faccao.getId().getCodcli());
        this.pedido.set(faccao.getPedido());
        this.obs.set(faccao.getObs());
        this.idant.set(faccao.getId().getIdentificador());
        this.estoque.set("S");
        this.pago.set("N");
        this.qual.set("1");
        this.deposito.set(deposito.equals("0011") ? "0011" : "0005");
        this.reprocesso.set(faccao.getReprocesso());
    }

    @EmbeddedId
    public Faccao3PK getId() {
        return id.get();
    }

    public ObjectProperty<Faccao3PK> idProperty() {
        return id;
    }

    public void setId(Faccao3PK id) {
        this.id.set(id);
    }

    @Column(name = "QUANT_I")
    public Integer getQuanti() {
        return quanti.get();
    }

    public IntegerProperty quantiProperty() {
        return quanti;
    }

    public void setQuanti(Integer quanti) {
        this.quanti.set(quanti);
    }

    @Column(name = "QUANT_2")
    public Integer getQuant2() {
        return quant2.get();
    }

    public IntegerProperty quant2Property() {
        return quant2;
    }

    public void setQuant2(Integer quant2) {
        this.quant2.set(quant2);
    }

    @Column(name = "QUANT")
    public Integer getQuant() {
        return quant.get();
    }

    public IntegerProperty quantProperty() {
        return quant;
    }

    public void setQuant(Integer quant) {
        this.quant.set(quant);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "DT_LAN")
    public LocalDate getDtlan() {
        return dtlan.get();
    }

    public ObjectProperty<LocalDate> dtlanProperty() {
        return dtlan;
    }

    public void setDtlan(LocalDate dtlan) {
        this.dtlan.set(dtlan);
    }

    @Column(name = "NOTA_REM")
    public String getNotarem() {
        return notarem.get();
    }

    public StringProperty notaremProperty() {
        return notarem;
    }

    public void setNotarem(String notarem) {
        this.notarem.set(notarem);
    }

    @Column(name = "UNITARIO")
    public BigDecimal getUnitario() {
        return unitario.get();
    }

    public ObjectProperty<BigDecimal> unitarioProperty() {
        return unitario;
    }

    public void setUnitario(BigDecimal unitario) {
        this.unitario.set(unitario);
    }

    @Column(name = "QT_ORIG")
    public Integer getQtorig() {
        return qtorig.get();
    }

    public IntegerProperty qtorigProperty() {
        return qtorig;
    }

    public void setQtorig(Integer qtorig) {
        this.qtorig.set(qtorig);
    }

    @Column(name = "CODCLI_ANT")
    public String getCodcliant() {
        return codcliant.get();
    }

    public StringProperty codcliantProperty() {
        return codcliant;
    }

    public void setCodcliant(String codcliant) {
        this.codcliant.set(codcliant);
    }

    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Column(name = "NUM_AP")
    public String getNumap() {
        return numap.get();
    }

    public StringProperty numapProperty() {
        return numap;
    }

    public void setNumap(String numap) {
        this.numap.set(numap);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "PAGO")
    public String getPago() {
        return pago.get();
    }

    public StringProperty pagoProperty() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago.set(pago);
    }

    @Column(name = "ID_ANT")
    public Integer getIdant() {
        return idant.get();
    }

    public IntegerProperty idantProperty() {
        return idant;
    }

    public void setIdant(Integer idant) {
        this.idant.set(idant);
    }

    @Column(name = "DT_PAGTO")
    public LocalDate getDtpagto() {
        return dtpagto.get();
    }

    public ObjectProperty<LocalDate> dtpagtoProperty() {
        return dtpagto;
    }

    public void setDtpagto(LocalDate dtpagto) {
        this.dtpagto.set(dtpagto);
    }

    @Column(name = "QUANT_F")
    public Integer getQuantf() {
        return quantf.get();
    }

    public IntegerProperty quantfProperty() {
        return quantf;
    }

    public void setQuantf(Integer quantf) {
        this.quantf.set(quantf);
    }

    @Column(name = "NOTA")
    public String getNota() {
        return nota.get();
    }

    public StringProperty notaProperty() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota.set(nota);
    }

    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "QUAL")
    public String getQual() {
        return qual.get();
    }

    public StringProperty qualProperty() {
        return qual;
    }

    public void setQual(String qual) {
        this.qual.set(qual);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "REPROCESSO")
    public String getReprocesso() {
        return reprocesso.get();
    }

    public StringProperty reprocessoProperty() {
        return reprocesso;
    }

    public void setReprocesso(String reprocesso) {
        this.reprocesso.set(reprocesso);
    }

    @Column(name = "SYSDELIZ")
    public String getSysdeliz() {
        return sysdeliz.get();
    }

    public StringProperty sysdelizProperty() {
        return sysdeliz;
    }

    public void setSysdeliz(String sysdeliz) {
        this.sysdeliz.set(sysdeliz);
    }

}