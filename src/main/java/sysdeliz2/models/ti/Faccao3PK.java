package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Faccao3PK implements Serializable {

    private final StringProperty op = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty identificador = new SimpleIntegerProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    private final IntegerProperty mov = new SimpleIntegerProperty();

    public Faccao3PK() {
    }

    public Faccao3PK(Faccao faccao) {
        this.op.set(faccao.getId().getOp());
        this.tam.set(faccao.getId().getTam());
        this.cor.set(faccao.getId().getCor());
        this.codcli.set(faccao.getId().getCodcli());
        this.numero.set(faccao.getId().getNumero());
        this.codigo.set(faccao.getId().getCodigo());
        this.parte.set(faccao.getId().getParte());
        this.mov.set(faccao.getId().getMov());
    }

    @Column(name = "OP")
    public String getOp() {
        return op.get();
    }

    public StringProperty opProperty() {
        return op;
    }

    public void setOp(String op) {
        this.op.set(op);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "IDENTIFICADOR")
    public Integer getIdentificador() {
        return identificador.get();
    }

    public IntegerProperty identificadorProperty() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador.set(identificador);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

    @Column(name = "MOV")
    public Integer getMov() {
        return mov.get();
    }

    public IntegerProperty movProperty() {
        return mov;
    }

    public void setMov(Integer mov) {
        this.mov.set(mov);
    }

}