package sysdeliz2.models.ti;

import javafx.beans.property.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 02/09/2019 08:30
 */
@Entity
@Table(name = "FAIXA_001")
public class Faixa implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    private final IntegerProperty tamanho = new SimpleIntegerProperty(this, "tamanho");

    private final ObjectProperty<List<FaixaItem>> itens = new SimpleObjectProperty<>(this, "itens");

    public Faixa() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "TAMANHO")
    public int getTamanho() {
        return tamanho.get();
    }

    public IntegerProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho.set(tamanho);
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAIXA")
    @OrderBy("posicao ASC")
    public List<FaixaItem> getItens() {
        return itens.get();
    }

    public ObjectProperty<List<FaixaItem>> itensProperty() {
        return itens;
    }

    public void setItens(List<FaixaItem> itens) {
        this.itens.set(itens);
    }
}
