package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/09/2019 08:45
 */
@Entity
@Table(name = "FAIXA_ITEN_001")
public class FaixaItem implements Serializable {

    private final ObjectProperty<FaixaItemPK> faixaItemId = new SimpleObjectProperty<>(this, "faixaItemId");
    private final IntegerProperty posicao = new SimpleIntegerProperty(this, "posicao");

    public FaixaItem() {
    }

    @EmbeddedId
    public FaixaItemPK getFaixaItemId() {
        return faixaItemId.get();
    }

    public ObjectProperty<FaixaItemPK> faixaItemIdProperty() {
        return faixaItemId;
    }

    public void setFaixaItemId(FaixaItemPK faixaItemPK) {
        this.faixaItemId.set(faixaItemPK);
    }

    @Column(name = "POSICAO")
    @OrderBy("POSICAO ASC")
    public int getPosicao() {
        return posicao.get();
    }

    public IntegerProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao.set(posicao);
    }
}
