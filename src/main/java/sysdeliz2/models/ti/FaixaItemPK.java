package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/09/2019 08:40
 */
@Embeddable
public class FaixaItemPK implements Serializable {

    private final StringProperty faixa = new SimpleStringProperty(this, "faixa");
    private final StringProperty tamanho = new SimpleStringProperty(this, "tamanho");

    public FaixaItemPK() {
    }

    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
