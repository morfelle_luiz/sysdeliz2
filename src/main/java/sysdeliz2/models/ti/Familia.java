package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/09/2019 08:24
 */
@Entity
@Table(name = "FAMILIA_001")
@TelaSysDeliz(descricao = "Família", icon = "familia (4).png")
public class Familia extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 200)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final IntegerProperty cor = new SimpleIntegerProperty(this, "cor");
    @Transient
    @ExibeTableView(descricao = "Tipo", width = 40)
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
    @Transient
    private final StringProperty grupo = new SimpleStringProperty(this, "grupo");
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 40)
    @ColunaFilter(descricao = "Grupo", coluna = "famGrp")
    private final StringProperty famGrp = new SimpleStringProperty(this, "famGrp");
    @Transient
    @ExibeTableView(descricao = "Meta", width = 60)
    private final IntegerProperty meta = new SimpleIntegerProperty(this, "meta");

    public Familia() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "COR")
    public int getCor() {
        return cor.get();
    }

    public IntegerProperty corProperty() {
        return cor;
    }

    public void setCor(int cor) {
        this.cor.set(cor);
    }

    @Column(name = "TIPO", insertable = false, updatable = false)
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "TIPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "FAM_GRP")
    public String getFamGrp() {
        return famGrp.get();
    }

    public StringProperty famGrpProperty() {
        return famGrp;
    }

    public void setFamGrp(String famGrp) {
        this.famGrp.set(famGrp);
    }

    @Column(name = "META")
    public int getMeta() {
        return meta.get();
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

    public void setMeta(int meta) {
        this.meta.set(meta);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
