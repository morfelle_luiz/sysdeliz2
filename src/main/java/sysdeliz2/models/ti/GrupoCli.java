package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 28/08/2019 16:41
 */
@Entity
@Table(name = "GRUPO_CLI_001")
@TelaSysDeliz(descricao = "Grupo Cliente/Fornecedor", icon = "grupo cliente (4).png")
public class GrupoCli extends BasicModel {

    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 250)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<>(this, "desconto");
    @Transient
    private final StringProperty analise = new SimpleStringProperty(this, "analise");

    public GrupoCli() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "ANALISE")
    public String getAnalise() {
        return analise.get();
    }

    public StringProperty analiseProperty() {
        return analise;
    }

    public void setAnalise(String analise) {
        this.analise.set(analise);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
    
}
