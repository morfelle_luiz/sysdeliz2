package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "GRUPO_MA_001")
@TelaSysDeliz(descricao = "Grupos", icon = "grupo_50.png")
public class GrupoMa extends BasicModel implements Serializable {
    
    @Transient
    private final StringProperty estampa = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    private final StringProperty ccontabil = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty desp = new SimpleStringProperty();
    @Transient
    private final StringProperty inventario = new SimpleStringProperty();
    @Transient
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final StringProperty lote = new SimpleStringProperty();
    @Transient
    private final StringProperty sdgrupo = new SimpleStringProperty();
    
    public GrupoMa() {
    }
    
    @Column(name = "ESTAMPA")
    public String getEstampa() {
        return estampa.get();
    }
    
    public StringProperty estampaProperty() {
        return estampa;
    }
    
    public void setEstampa(String estampa) {
        this.estampa.set(estampa);
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "C_CONTABIL")
    public String getCcontabil() {
        return ccontabil.get();
    }
    
    public StringProperty ccontabilProperty() {
        return ccontabil;
    }
    
    public void setCcontabil(String ccontabil) {
        this.ccontabil.set(ccontabil);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "DESP")
    public String getDesp() {
        return desp.get();
    }
    
    public StringProperty despProperty() {
        return desp;
    }
    
    public void setDesp(String desp) {
        this.desp.set(desp);
    }
    
    @Column(name = "INVENTARIO")
    public String getInventario() {
        return inventario.get();
    }
    
    public StringProperty inventarioProperty() {
        return inventario;
    }
    
    public void setInventario(String inventario) {
        this.inventario.set(inventario);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "SD_GRUPO")
    public String getSdgrupo() {
        return sdgrupo.get();
    }
    
    public StringProperty sdgrupoProperty() {
        return sdgrupo;
    }
    
    public void setSdgrupo(String sdgrupo) {
        this.sdgrupo.set(sdgrupo);
    }
    
    @Override
    public String toString(){
        return "["+this.codigo.get()+"] "+this.descricao.get();
    }
}
