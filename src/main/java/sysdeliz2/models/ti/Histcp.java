package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "HISTCP_001")
@TelaSysDeliz(descricao = "Histórico", icon = "historico cont (4).png")
public class Histcp extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Histórico", width = 50)
    @ColunaFilter(descricao = "Histórico", coluna = "historico")
    private final StringProperty historico = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty observacao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Tipo", width = 80)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final StringProperty situacao = new SimpleStringProperty();
    @Transient
    private final StringProperty sdAtivo = new SimpleStringProperty();
    @Transient
    private final StringProperty sdHistes = new SimpleStringProperty();
    @Transient
    private final StringProperty sdIndcomp = new SimpleStringProperty();
    
    public Histcp() {
    }
    
    @Id
    @Column(name = "HISTORICO")
    public String getHistorico() {
        return historico.get();
    }
    
    public StringProperty historicoProperty() {
        return historico;
    }
    
    public void setHistorico(String historico) {
        setCodigoFilter(historico);
        this.historico.set(historico);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }
    
    public StringProperty situacaoProperty() {
        return situacao;
    }
    
    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }
    
    @Column(name = "SD_ATIVO")
    public String getSdAtivo() {
        return sdAtivo.get();
    }
    
    public StringProperty sdAtivoProperty() {
        return sdAtivo;
    }
    
    public void setSdAtivo(String sdAtivo) {
        this.sdAtivo.set(sdAtivo);
    }
    
    @Column(name = "SD_HISTES")
    public String getSdHistes() {
        return sdHistes.get();
    }
    
    public StringProperty sdHistesProperty() {
        return sdHistes;
    }
    
    public void setSdHistes(String sdHistes) {
        this.sdHistes.set(sdHistes);
    }
    
    @Column(name = "SD_INDCOMP")
    public String getSdIndcomp() {
        return sdIndcomp.get();
    }
    
    public StringProperty sdIndcompProperty() {
        return sdIndcomp;
    }
    
    public void setSdIndcomp(String sdIndcomp) {
        this.sdIndcomp.set(sdIndcomp);
    }


    @Override
    public String toString() {
        return "[" + historico.get() + "] " + descricao.get();
    }
}
