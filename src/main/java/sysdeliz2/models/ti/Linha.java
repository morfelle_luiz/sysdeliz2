package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/09/2019 08:34
 */
@Entity
@Table(name = "TABLIN_001")
@TelaSysDeliz(descricao = "Linha", icon = "linha (4).png")
public class Linha extends BasicModel implements Serializable{
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    private final IntegerProperty concentrado = new SimpleIntegerProperty(this, "concentrado");
    @Transient
    private final StringProperty genero = new SimpleStringProperty(this, "genero");
    @Transient
    private final StringProperty sdMarca = new SimpleStringProperty(this, "sdMarca");
    @Transient
    private final StringProperty sdLinha = new SimpleStringProperty(this, "sdLinha");
    @Transient
    private final StringProperty sdLinhaDesc = new SimpleStringProperty(this, "sdLinhaDesc");
    @Transient
    private final StringProperty tipoLinha = new SimpleStringProperty(this, "tipoLinha");
    @Transient
    private final StringProperty grpIndice = new SimpleStringProperty(this, "grpIndice");
    @Transient
    private final BooleanProperty sdAtivo = new SimpleBooleanProperty(this, "sdAtivo");
    @Transient
    private final IntegerProperty indice = new SimpleIntegerProperty();
    public Linha() {
    }

    public Linha(int indice) {
        this.indice.set(indice);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "CONCENTRADO")
    public int getConcentrado() {
        return concentrado.get();
    }

    public IntegerProperty concentradoProperty() {
        return concentrado;
    }

    public void setConcentrado(int concentrado) {
        this.concentrado.set(concentrado);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "SD_MARCA")
    public String getSdMarca() {
        return sdMarca.get();
    }

    public StringProperty sdMarcaProperty() {
        return sdMarca;
    }

    public void setSdMarca(String sdMarca) {
        this.sdMarca.set(sdMarca);
    }

    @Column(name = "SD_LINHA")
    public String getSdLinha() {
        return sdLinha.get();
    }

    public StringProperty sdLinhaProperty() {
        return sdLinha;
    }

    public void setSdLinha(String sdLinha) {
        this.sdLinha.set(sdLinha);
    }

    @Column(name = "SD_LINHADESC")
    public String getSdLinhaDesc() {
        return sdLinhaDesc.get();
    }

    public StringProperty sdLinhaDescProperty() {
        return sdLinhaDesc;
    }

    public void setSdLinhaDesc(String sdLinhaDesc) {
        this.sdLinhaDesc.set(sdLinhaDesc);
    }

    @Column(name = "TIPO_LINHA")
    public String getTipoLinha() {
        return tipoLinha.get();
    }

    public StringProperty tipoLinhaProperty() {
        return tipoLinha;
    }

    public void setTipoLinha(String tipoLinha) {
        this.tipoLinha.set(tipoLinha);
    }

    @Column(name = "GRP_INDICE")
    public String getGrpIndice() {
        return grpIndice.get();
    }

    public StringProperty grpIndiceProperty() {
        return grpIndice;
    }

    public void setGrpIndice(String grpIndice) {
        this.grpIndice.set(grpIndice);
    }

    @Column(name = "SD_ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSdAtivo() {
        return sdAtivo.get();
    }

    public BooleanProperty sdAtivoProperty() {
        return sdAtivo;
    }

    public void setSdAtivo(boolean sdAtivo) {
        this.sdAtivo.set(sdAtivo);
    }

    @Transient
    public int getIndice() {
        return indice.get();
    }

    public IntegerProperty indiceProperty() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice.set(indice);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
