package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.DefaultFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 31/07/2019 15:45
 */
@Entity
@Table(name = "MARCA_001")
@TelaSysDeliz(descricao = "Marca", icon = "marca (4).png")
public class Marca extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 40)
    private final StringProperty sdGrupo = new SimpleStringProperty(this, "sdGrupo");
    @Transient
    private final StringProperty sdGerente = new SimpleStringProperty(this, "sdGerente");
    @Transient
    @DefaultFilter(column = "sdAtivo", value = "S")
    private final StringProperty sdAtivo = new SimpleStringProperty(this, "sdAtivo");
    @Transient
    private final StringProperty sdAbreviacao = new SimpleStringProperty();


    public Marca() {
    }

    public Marca (String codigo) {
        this.codigo.set(codigo);
        this.descricao.set("DLZ + FLOR");
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    @Column(name = "SD_GRUPO")
    public String getSdGrupo() {
        return sdGrupo.get();
    }

    @Column(name = "SD_GERENTE")
    public String getSdGerente() {
        return sdGerente.get();
    }

    @Column(name = "SD_ATIVO")
    public String getSdAtivo() {
        return sdAtivo.get();
    }

    @Column(name = "SD_ABREVIACAO")
    public String getSdAbreviacao() {
        return sdAbreviacao.get();
    }

    public StringProperty sdAbreviacaoProperty() {
        return sdAbreviacao;
    }

    public void setSdAbreviacao(String sdAbreviacao) {
        this.sdAbreviacao.set(sdAbreviacao);
    }

    public void setCodigo(String value) {
        setCodigoFilter(value);
        codigo.set(value);
    }

    public void setDescricao(String value) {
        setDescricaoFilter(value);
        descricao.set(value);
    }

    public void setSdGrupo(String value) {
        sdGrupo.set(value);
    }

    public void setSdGerente(String value) {
        sdGerente.set(value);
    }

    public void setSdAtivo(String value) {
        sdAtivo.set(value);
    }


    public final StringProperty codigoProperty() {
       return codigo;
    }

    public final StringProperty descricaoProperty() {
        return descricao;
    }

    public final StringProperty sdGrupoProperty() {
        return sdGrupo;
    }

    public final StringProperty sdGerenteProperty() {
        return sdGerente;
    }

    public final StringProperty sdAtivoProperty() {
        return sdAtivo;
    }

    @Transient
    public String getDescricaoPath() {
        return this.getCodigo().equals("D") ? "DLZ" : this.getCodigo().equals("F") ? "Flor de Lis" : this.getDescricao();
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marca marca = (Marca) o;
        return Objects.equals(codigo, marca.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
