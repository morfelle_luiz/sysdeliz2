package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "MAT_ITEN_001")
public class MatIten {
    
    private final ObjectProperty<MatItenPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> qtder = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> qtdminima = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> precomed = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty local = new SimpleStringProperty("AINSP");
    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdmaxima = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty ativo = new SimpleStringProperty("S");
    private final StringProperty dtleitura = new SimpleStringProperty();
    
    public MatIten() {
    }
    
    public MatIten(String cor,
                   String lote,
                   String codigo,
                   Deposito deposito) {
        this.id.set(new MatItenPK(cor, lote, codigo, deposito, "1"));
    }
    
    public MatIten(BigDecimal qtde,
                   BigDecimal qtder,
                   BigDecimal qtdminima,
                   String cor,
                   String lote,
                   String codigo,
                   Deposito deposito,
                   BigDecimal precomed,
                   String local,
                   String barra,
                   BigDecimal qtdmaxima,
                   BigDecimal largura,
                   String qualidade,
                   String ativo,
                   String dtleitura) {
        this.id.set(new MatItenPK(cor, lote, codigo, deposito, qualidade));
        this.qtde.set(qtde);
        this.qtder.set(qtder);
        this.qtdminima.set(qtdminima);
        this.precomed.set(precomed);
        this.local.set(local);
        this.barra.set(barra);
        this.qtdmaxima.set(qtdmaxima);
        this.largura.set(largura);
        this.ativo.set(ativo);
        this.dtleitura.set(dtleitura);
    }
    
    @EmbeddedId
    public MatItenPK getId() {
        return id.get();
    }
    
    public ObjectProperty<MatItenPK> idProperty() {
        return id;
    }
    
    public void setId(MatItenPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_R")
    public BigDecimal getQtder() {
        return qtder.get();
    }
    
    public ObjectProperty<BigDecimal> qtderProperty() {
        return qtder;
    }
    
    public void setQtder(BigDecimal qtder) {
        this.qtder.set(qtder);
    }
    
    @Column(name = "QTD_MINIMA")
    public BigDecimal getQtdminima() {
        return qtdminima.get();
    }
    
    public ObjectProperty<BigDecimal> qtdminimaProperty() {
        return qtdminima;
    }
    
    public void setQtdminima(BigDecimal qtdminima) {
        this.qtdminima.set(qtdminima);
    }
    
    @Column(name = "PRECO_MED")
    public BigDecimal getPrecomed() {
        return precomed.get();
    }
    
    public ObjectProperty<BigDecimal> precomedProperty() {
        return precomed;
    }
    
    public void setPrecomed(BigDecimal precomed) {
        this.precomed.set(precomed);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }
    
    public StringProperty barraProperty() {
        return barra;
    }
    
    public void setBarra(String barra) {
        this.barra.set(barra);
    }
    
    @Column(name = "QTD_MAXIMA")
    public BigDecimal getQtdmaxima() {
        return qtdmaxima.get();
    }
    
    public ObjectProperty<BigDecimal> qtdmaximaProperty() {
        return qtdmaxima;
    }
    
    public void setQtdmaxima(BigDecimal qtdmaxima) {
        this.qtdmaxima.set(qtdmaxima);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "ATIVO")
    public String getAtivo() {
        return ativo.get();
    }
    
    public StringProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(String ativo) {
        this.ativo.set(ativo);
    }
    
    @Column(name = "DT_LEITURA")
    public String getDtleitura() {
        return dtleitura.get();
    }
    
    public StringProperty dtleituraProperty() {
        return dtleitura;
    }
    
    public void setDtleitura(String dtleitura) {
        this.dtleitura.set(dtleitura);
    }
    
}
