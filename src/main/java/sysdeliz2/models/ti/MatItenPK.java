package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class MatItenPK implements Serializable {
    
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<Deposito>();
    //private final StringProperty qualidade = new SimpleStringProperty();
    
    public MatItenPK() {
    }
    
    public MatItenPK(String cor,
                     String lote,
                     String codigo,
                     Deposito deposito,
                     String qualidade) {
        this.cor.set(cor);
        this.lote.set(lote);
        this.codigo.set(codigo);
        this.deposito.set(deposito);
        //this.qualidade.set(qualidade);
    }
    
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }
    
    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }
    
//    @Column(name = "QUALIDADE")
//    public String getQualidade() {
//        return qualidade.get();
//    }
//
//    public StringProperty qualidadeProperty() {
//        return qualidade;
//    }
//
//    public void setQualidade(String qualidade) {
//        this.qualidade.set(qualidade);
//    }
}
