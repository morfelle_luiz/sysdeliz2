package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "MAT_MOV_001")
public class MatMov {
    
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty operacao = new SimpleStringProperty();
    private final StringProperty codcre = new SimpleStringProperty();
    private final StringProperty numdocto = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty ccusto = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtmovto = new SimpleObjectProperty<LocalDate>();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty maquina = new SimpleStringProperty();
    private final StringProperty funcionario = new SimpleStringProperty();
    private final StringProperty tpmov = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDateTime> hora = new SimpleObjectProperty<LocalDateTime>();
    
    public MatMov() {
    
    }
    
    public MatMov(String codigo, String operacao, String numdocto, BigDecimal qtde,
                  BigDecimal preco, BigDecimal custo, String cor, String tipo, String deposito,
                  LocalDate dtmovto, String lote, String descricao, String tpmov, LocalDateTime hora) {
        this.codigo.set(codigo);
        this.operacao.set(operacao);
        this.numdocto.set(numdocto);
        this.qtde.set(qtde);
        this.preco.set(preco);
        this.custo.set(custo);
        this.cor.set(cor);
        this.tipo.set(tipo);
        this.deposito.set(deposito);
        this.dtmovto.set(dtmovto);
        this.lote.set(lote);
        this.descricao.set(descricao);
        this.tpmov.set(tpmov);
        this.hora.set(hora);
    }
    
    public MatMov(String codigo, String operacao, String codcre, String numdocto, BigDecimal qtde,
                  BigDecimal preco, BigDecimal custo, String cor, String tipo, String ccusto, String deposito,
                  String grupo, LocalDate dtmovto, String lote, String descricao, String maquina,
                  String funcionario, String tpmov, LocalDateTime hora) {
        this.codigo.set(codigo);
        this.operacao.set(operacao);
        this.codcre.set(codcre);
        this.numdocto.set(numdocto);
        this.qtde.set(qtde);
        this.preco.set(preco);
        this.custo.set(custo);
        this.cor.set(cor);
        this.tipo.set(tipo);
        this.ccusto.set(ccusto);
        this.deposito.set(deposito);
        this.grupo.set(grupo);
        this.dtmovto.set(dtmovto);
        this.lote.set(lote);
        this.descricao.set(descricao);
        this.maquina.set(maquina);
        this.funcionario.set(funcionario);
        this.tpmov.set(tpmov);
        this.hora.set(hora);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "OPERACAO")
    public String getOperacao() {
        return operacao.get();
    }
    
    public StringProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(String operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name = "CODCRE")
    public String getCodcre() {
        return codcre.get();
    }
    
    public StringProperty codcreProperty() {
        return codcre;
    }
    
    public void setCodcre(String codcre) {
        this.codcre.set(codcre);
    }
    
    @Column(name = "NUM_DOCTO")
    public String getNumdocto() {
        return numdocto.get();
    }
    
    public StringProperty numdoctoProperty() {
        return numdocto;
    }
    
    public void setNumdocto(String numdocto) {
        this.numdocto.set(numdocto);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }
    
    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }
    
    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }
    
    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }
    
    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }
    
    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "C_CUSTO")
    public String getCcusto() {
        return ccusto.get();
    }
    
    public StringProperty ccustoProperty() {
        return ccusto;
    }
    
    public void setCcusto(String ccusto) {
        this.ccusto.set(ccusto);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "DT_MOVTO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtmovto() {
        return dtmovto.get();
    }
    
    public ObjectProperty<LocalDate> dtmovtoProperty() {
        return dtmovto;
    }
    
    public void setDtmovto(LocalDate dtmovto) {
        this.dtmovto.set(dtmovto);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name = "MAQUINA")
    public String getMaquina() {
        return maquina.get();
    }
    
    public StringProperty maquinaProperty() {
        return maquina;
    }
    
    public void setMaquina(String maquina) {
        this.maquina.set(maquina);
    }
    
    @Column(name = "FUNCIONARIO")
    public String getFuncionario() {
        return funcionario.get();
    }
    
    public StringProperty funcionarioProperty() {
        return funcionario;
    }
    
    public void setFuncionario(String funcionario) {
        this.funcionario.set(funcionario);
    }
    
    @Column(name = "TP_MOV")
    public String getTpmov() {
        return tpmov.get();
    }
    
    public StringProperty tpmovProperty() {
        return tpmov;
    }
    
    public void setTpmov(String tpmov) {
        this.tpmov.set(tpmov);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AI_MAT_MOV_ORDEM")
    @SequenceGenerator(name = "AI_MAT_MOV_ORDEM", sequenceName = "MAT_MOV_ORDEM", allocationSize = 1)
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "HORA")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getHora() {
        return hora.get();
    }
    
    public ObjectProperty<LocalDateTime> horaProperty() {
        return hora;
    }
    
    public void setHora(LocalDateTime hora) {
        this.hora.set(hora);
    }
    
}
