package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "MAT_RESERVA_001")
public class MatReserva {
    
    private final ObjectProperty<MatReservaPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> qtdeb = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final IntegerProperty mov = new SimpleIntegerProperty();
    private final StringProperty romaneio = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtcad = new SimpleObjectProperty<LocalDate>();
    private final StringProperty tipo = new SimpleStringProperty();
    
    public MatReserva() {
    }
    
    @EmbeddedId
    public MatReservaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<MatReservaPK> idProperty() {
        return id;
    }
    
    public void setId(MatReservaPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_B")
    public BigDecimal getQtdeb() {
        return qtdeb.get();
    }
    
    public ObjectProperty<BigDecimal> qtdebProperty() {
        return qtdeb;
    }
    
    public void setQtdeb(BigDecimal qtdeb) {
        this.qtdeb.set(qtdeb);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }
    
    @Column(name = "MOV")
    public Integer getMov() {
        return mov.get();
    }
    
    public IntegerProperty movProperty() {
        return mov;
    }
    
    public void setMov(Integer mov) {
        this.mov.set(mov);
    }
    
    @Column(name = "ROMANEIO")
    public String getRomaneio() {
        return romaneio.get();
    }
    
    public StringProperty romaneioProperty() {
        return romaneio;
    }
    
    public void setRomaneio(String romaneio) {
        this.romaneio.set(romaneio);
    }
    
    @Column(name = "DT_CAD")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtcad() {
        return dtcad.get();
    }
    
    public ObjectProperty<LocalDate> dtcadProperty() {
        return dtcad;
    }
    
    public void setDtcad(LocalDate dtcad) {
        this.dtcad.set(dtcad);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
}