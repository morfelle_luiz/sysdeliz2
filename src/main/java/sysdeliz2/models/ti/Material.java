package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "MATERIAL_001")
@TelaSysDeliz(descricao = "Material", icon = "material_50.png")
public class Material extends BasicModel {
    
    @Transient
    private final StringProperty estampa = new SimpleStringProperty();
    @Transient
    private final IntegerProperty ne = new SimpleIntegerProperty();
    @Transient
    private final ObjectProperty<LocalDate> dtcompra = new SimpleObjectProperty<LocalDate>();
    @Transient
    private final ObjectProperty<BigDecimal> premedio = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> reposicao = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> kghora = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<SubGrupoMa> subgrupo = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty codfis = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<GrupoMa> grupo = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty codtri = new SimpleStringProperty();
    @Transient
    private final StringProperty cdcentro = new SimpleStringProperty();
    @Transient
    private final StringProperty codcli = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    private final StringProperty composicao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "UN", width = 30)
    private final ObjectProperty<Unidade> unidade = new SimpleObjectProperty<Unidade>();
    @Transient
    private final StringProperty descricao2 = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> ipi = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> precompra = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty local = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> indtin = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    @Transient
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    @Transient
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<Unidade> unicom = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> divisor = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty observacao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> custoimp = new SimpleObjectProperty<BigDecimal>();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 30)
    private final StringProperty ativo = new SimpleStringProperty();
    @Transient
    private final StringProperty linha = new SimpleStringProperty();
    @Transient
    private final StringProperty origem = new SimpleStringProperty();
    @Transient
    private final StringProperty estoque = new SimpleStringProperty();
    @Transient
    private final StringProperty codsped = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> concentracao = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty tribipi = new SimpleStringProperty();
    @Transient
    private final StringProperty tribpis = new SimpleStringProperty();
    @Transient
    private final StringProperty tribcofins = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> minimo = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> maximo = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> cubagem = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final ObjectProperty<BigDecimal> pesoliq = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final IntegerProperty ususeqatu = new SimpleIntegerProperty();
    @Transient
    private final ObjectProperty<BigDecimal> concentraest = new SimpleObjectProperty<BigDecimal>();
    @Transient
    private final StringProperty obs1 = new SimpleStringProperty();
    
    public Material() {
    }
    
    @Column(name = "ESTAMPA")
    public String getEstampa() {
        return estampa.get();
    }
    
    public StringProperty estampaProperty() {
        return estampa;
    }
    
    public void setEstampa(String estampa) {
        this.estampa.set(estampa);
    }
    
    @Column(name = "NE")
    public Integer getNe() {
        return ne.get();
    }
    
    public IntegerProperty neProperty() {
        return ne;
    }
    
    public void setNe(Integer ne) {
        if(ne == null) ne = 0;
        this.ne.set(ne);
    }
    
    @Column(name = "DT_COMPRA")
    public LocalDate getDtcompra() {
        return dtcompra.get();
    }
    
    public ObjectProperty<LocalDate> dtcompraProperty() {
        return dtcompra;
    }
    
    public void setDtcompra(LocalDate dtcompra) {
        this.dtcompra.set(dtcompra);
    }
    
    @Column(name = "PRE_MEDIO")
    public BigDecimal getPremedio() {
        return premedio.get();
    }
    
    public ObjectProperty<BigDecimal> premedioProperty() {
        return premedio;
    }
    
    public void setPremedio(BigDecimal premedio) {
        this.premedio.set(premedio);
    }
    
    @Column(name = "REPOSICAO")
    public BigDecimal getReposicao() {
        return reposicao.get();
    }
    
    public ObjectProperty<BigDecimal> reposicaoProperty() {
        return reposicao;
    }
    
    public void setReposicao(BigDecimal reposicao) {
        this.reposicao.set(reposicao);
    }
    
    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }
    
    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }
    
    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }
    
    @Column(name = "KG_HORA")
    public BigDecimal getKghora() {
        return kghora.get();
    }
    
    public ObjectProperty<BigDecimal> kghoraProperty() {
        return kghora;
    }
    
    public void setKghora(BigDecimal kghora) {
        this.kghora.set(kghora);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SUB_GRUPO")
    public SubGrupoMa getSubgrupo() {
        return subgrupo.get();
    }
    
    public ObjectProperty<SubGrupoMa> subgrupoProperty() {
        return subgrupo;
    }
    
    public void setSubgrupo(SubGrupoMa subgrupo) {
        this.subgrupo.set(subgrupo);
    }
    
    @Column(name = "CODFIS")
    public String getCodfis() {
        return codfis.get();
    }
    
    public StringProperty codfisProperty() {
        return codfis;
    }
    
    public void setCodfis(String codfis) {
        this.codfis.set(codfis);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GRUPO")
    public GrupoMa getGrupo() {
        return grupo.get();
    }
    
    public ObjectProperty<GrupoMa> grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(GrupoMa grupo) {
        this.grupo.set(grupo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIDADE")
    public Unidade getUnidade() {
        return unidade.get();
    }
    
    public ObjectProperty<Unidade> unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(Unidade unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "CODTRI")
    public String getCodtri() {
        return codtri.get();
    }
    
    public StringProperty codtriProperty() {
        return codtri;
    }
    
    public void setCodtri(String codtri) {
        this.codtri.set(codtri);
    }
    
    @Column(name = "CDCENTRO")
    public String getCdcentro() {
        return cdcentro.get();
    }
    
    public StringProperty cdcentroProperty() {
        return cdcentro;
    }
    
    public void setCdcentro(String cdcentro) {
        this.cdcentro.set(cdcentro);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "COMPOSICAO")
    public String getComposicao() {
        return composicao.get();
    }
    
    public StringProperty composicaoProperty() {
        return composicao;
    }
    
    public void setComposicao(String composicao) {
        this.composicao.set(composicao);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "DESCRICAO2")
    public String getDescricao2() {
        return descricao2.get();
    }
    
    public StringProperty descricao2Property() {
        return descricao2;
    }
    
    public void setDescricao2(String descricao2) {
        this.descricao2.set(descricao2);
    }
    
    @Column(name = "IPI")
    public BigDecimal getIpi() {
        return ipi.get();
    }
    
    public ObjectProperty<BigDecimal> ipiProperty() {
        return ipi;
    }
    
    public void setIpi(BigDecimal ipi) {
        this.ipi.set(ipi);
    }
    
    @Column(name = "PRE_COMPRA")
    public BigDecimal getPrecompra() {
        return precompra.get();
    }
    
    public ObjectProperty<BigDecimal> precompraProperty() {
        return precompra;
    }
    
    public void setPrecompra(BigDecimal precompra) {
        this.precompra.set(precompra);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "IND_TIN")
    public BigDecimal getIndtin() {
        return indtin.get();
    }
    
    public ObjectProperty<BigDecimal> indtinProperty() {
        return indtin;
    }
    
    public void setIndtin(BigDecimal indtin) {
        this.indtin.set(indtin);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }
    
    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }
    
    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }
    
    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNI_COM")
    public Unidade getUnicom() {
        return unicom.get();
    }
    
    public ObjectProperty<Unidade> unicomProperty() {
        return unicom;
    }
    
    public void setUnicom(Unidade unicom) {
        this.unicom.set(unicom);
    }
    
    @Column(name = "DIVISOR")
    public BigDecimal getDivisor() {
        return divisor.get();
    }
    
    public ObjectProperty<BigDecimal> divisorProperty() {
        return divisor;
    }
    
    public void setDivisor(BigDecimal divisor) {
        this.divisor.set(divisor);
    }
    
    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "CUSTO_IMP")
    public BigDecimal getCustoimp() {
        return custoimp.get();
    }
    
    public ObjectProperty<BigDecimal> custoimpProperty() {
        return custoimp;
    }
    
    public void setCustoimp(BigDecimal custoimp) {
        this.custoimp.set(custoimp);
    }
    
    @Column(name = "ATIVO")
    public String getAtivo() {
        return ativo.get();
    }
    
    public StringProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(String ativo) {
        this.ativo.set(ativo);
    }
    
    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }
    
    public StringProperty linhaProperty() {
        return linha;
    }
    
    public void setLinha(String linha) {
        this.linha.set(linha);
    }
    
    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }
    
    public StringProperty origemProperty() {
        return origem;
    }
    
    public void setOrigem(String origem) {
        this.origem.set(origem);
    }
    
    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }
    
    public StringProperty estoqueProperty() {
        return estoque;
    }
    
    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }
    
    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }
    
    public StringProperty codspedProperty() {
        return codsped;
    }
    
    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }
    
    @Column(name = "CONCENTRACAO")
    public BigDecimal getConcentracao() {
        return concentracao.get();
    }
    
    public ObjectProperty<BigDecimal> concentracaoProperty() {
        return concentracao;
    }
    
    public void setConcentracao(BigDecimal concentracao) {
        this.concentracao.set(concentracao);
    }
    
    @Column(name = "TRIB_IPI")
    public String getTribipi() {
        return tribipi.get();
    }
    
    public StringProperty tribipiProperty() {
        return tribipi;
    }
    
    public void setTribipi(String tribipi) {
        this.tribipi.set(tribipi);
    }
    
    @Column(name = "TRIB_PIS")
    public String getTribpis() {
        return tribpis.get();
    }
    
    public StringProperty tribpisProperty() {
        return tribpis;
    }
    
    public void setTribpis(String tribpis) {
        this.tribpis.set(tribpis);
    }
    
    @Column(name = "TRIB_COFINS")
    public String getTribcofins() {
        return tribcofins.get();
    }
    
    public StringProperty tribcofinsProperty() {
        return tribcofins;
    }
    
    public void setTribcofins(String tribcofins) {
        this.tribcofins.set(tribcofins);
    }
    
    @Column(name = "MINIMO")
    public BigDecimal getMinimo() {
        return minimo.get();
    }
    
    public ObjectProperty<BigDecimal> minimoProperty() {
        return minimo;
    }
    
    public void setMinimo(BigDecimal minimo) {
        this.minimo.set(minimo);
    }
    
    @Column(name = "MAXIMO")
    public BigDecimal getMaximo() {
        return maximo.get();
    }
    
    public ObjectProperty<BigDecimal> maximoProperty() {
        return maximo;
    }
    
    public void setMaximo(BigDecimal maximo) {
        this.maximo.set(maximo);
    }
    
    @Column(name = "CUBAGEM")
    public BigDecimal getCubagem() {
        return cubagem.get();
    }
    
    public ObjectProperty<BigDecimal> cubagemProperty() {
        return cubagem;
    }
    
    public void setCubagem(BigDecimal cubagem) {
        this.cubagem.set(cubagem);
    }
    
    @Column(name = "PESO_LIQ")
    public BigDecimal getPesoliq() {
        return pesoliq.get();
    }
    
    public ObjectProperty<BigDecimal> pesoliqProperty() {
        return pesoliq;
    }
    
    public void setPesoliq(BigDecimal pesoliq) {
        this.pesoliq.set(pesoliq);
    }
    
    @Column(name = "USU_SEQATU")
    public Integer getUsuseqatu() {
        return ususeqatu.get();
    }
    
    public IntegerProperty ususeqatuProperty() {
        return ususeqatu;
    }
    
    public void setUsuseqatu(Integer ususeqatu) {
        this.ususeqatu.set(ususeqatu == null ? 0 : ususeqatu);
    }
    
    @Column(name = "CONCENTRA_EST")
    public BigDecimal getConcentraest() {
        return concentraest.get();
    }
    
    public ObjectProperty<BigDecimal> concentraestProperty() {
        return concentraest;
    }
    
    public void setConcentraest(BigDecimal concentraest) {
        this.concentraest.set(concentraest);
    }
    
    @Column(name = "OBS1")
    @Lob
    public String getObs1() {
        return obs1.get();
    }
    
    public StringProperty obs1Property() {
        return obs1;
    }
    
    public void setObs1(String obs1) {
        this.obs1.set(obs1);
    }
    
    @Override
    public String toString() {
        return "["+this.codigo.get()+"] "+this.descricao.get();
    }
}
