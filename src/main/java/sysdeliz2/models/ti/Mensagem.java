package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MENSAGEM_001")
@TelaSysDeliz(descricao = "Motivos", icon = "motivo (4).png")
public class Mensagem extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codmen")
    private final StringProperty codmen = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty padrao = new SimpleStringProperty();
    @Transient
    private final StringProperty venda = new SimpleStringProperty();
    
    public Mensagem() {
    }
    
    @Id
    @Column(name = "CODMEN")
    public String getCodmen() {
        return codmen.get();
    }
    
    public StringProperty codmenProperty() {
        return codmen;
    }
    
    public void setCodmen(String codmen) {
        setCodigoFilter(codmen);
        this.codmen.set(codmen);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "PADRAO")
    public String getPadrao() {
        return padrao.get();
    }
    
    public StringProperty padraoProperty() {
        return padrao;
    }
    
    public void setPadrao(String padrao) {
        this.padrao.set(padrao);
    }
    
    @Column(name = "VENDA")
    public String getVenda() {
        return venda.get();
    }
    
    public StringProperty vendaProperty() {
        return venda;
    }
    
    public void setVenda(String venda) {
        this.venda.set(venda);
    }
    
    @Override
    public String toString() {
        return "[" + codmen.get() + "] " + descricao.get();
    }
}
