package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "NAT_CONTA_001")
@TelaSysDeliz(descricao = "NatConta", icon = "fragment_100.png")
public class NatConta extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "id.codigo")
    private final ObjectProperty<NatContaPK> id = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Descrição", width = 450)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty contad = new SimpleStringProperty();
    private final StringProperty contac = new SimpleStringProperty();
    
    public NatConta() {
    
    }
    
    @EmbeddedId
    public NatContaPK getId() {
        return id.get();
    }
    
    public ObjectProperty<NatContaPK> idProperty() {
        return id;
    }
    
    public void setId(NatContaPK id) {
        this.id.set(id);
        setCodigoFilter(id.getCodigo());
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "CONTA_D")
    public String getContad() {
        return contad.get();
    }
    
    public StringProperty contadProperty() {
        return contad;
    }
    
    public void setContad(String contad) {
        this.contad.set(contad);
    }
    
    @Column(name = "CONTA_C")
    public String getContac() {
        return contac.get();
    }
    
    public StringProperty contacProperty() {
        return contac;
    }
    
    public void setContac(String contac) {
        this.contac.set(contac);
    }
}