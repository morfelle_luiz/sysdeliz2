package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "NATUREZA_001")
@TelaSysDeliz(descricao = "Entidade", icon = "natureza (4).png")
public class Natureza extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Natureza", width = 50)
    @ColunaFilter(descricao = "Natureza", coluna = "natureza")
    private final StringProperty natureza = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> aliquota = new SimpleObjectProperty<>(BigDecimal.ZERO);
    @Transient
    private final StringProperty tpBase = new SimpleStringProperty();
    @Transient
    private final BooleanProperty dupli = new SimpleBooleanProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty ipi = new SimpleStringProperty();
    @Transient
    private final StringProperty pisCofins = new SimpleStringProperty();
    @Transient
    private final StringProperty icms = new SimpleStringProperty();
    @Transient
    private final StringProperty clafis = new SimpleStringProperty();
    @Transient
    private final StringProperty clatrib = new SimpleStringProperty();
    @Transient
    private final StringProperty mensagem = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Tipo", width = 100)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    @ColunaFilter(descricao = "Ativo", coluna = "ativo")
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    @Transient
    private final StringProperty natCont = new SimpleStringProperty();
    @Transient
    private final StringProperty substTrib = new SimpleStringProperty();
    @Transient
    private final StringProperty complemento = new SimpleStringProperty();
    @Transient
    private final StringProperty tribIpi = new SimpleStringProperty();
    @Transient
    private final StringProperty tribPis = new SimpleStringProperty();
    @Transient
    private final StringProperty tribCofins = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> aliqPis = new SimpleObjectProperty<>(BigDecimal.ZERO);
    @Transient
    private final ObjectProperty<BigDecimal> aliqCofins = new SimpleObjectProperty<>(BigDecimal.ZERO);
    @Transient
    private final StringProperty codsped = new SimpleStringProperty();
    @Transient
    private final StringProperty cfop = new SimpleStringProperty();
    @Transient
    private final StringProperty compBaseIcms = new SimpleStringProperty();
    @Transient
    private final StringProperty compBasePico = new SimpleStringProperty();
    @Transient
    private final StringProperty codAjSped = new SimpleStringProperty();
    @Transient
    private final StringProperty consFinal = new SimpleStringProperty();
    @Transient
    private final StringProperty movEst = new SimpleStringProperty();
    @Transient
    private final StringProperty codTns = new SimpleStringProperty();

    public enum TipoNatureza {
        V("Venda/Compra"),
        D("Devolução"),
        R("Retorno/Remessa"),
        C("Consignação"),
        A("Antecipação"),
        S("Serviço"),
        B("Bonificação/Brindes"),
        I("Imposto Complementar");

        private final String desc;

        TipoNatureza(String tipo) {
            this.desc = tipo;
        }

        public String getDesc() {
            return desc;
        }

        @Override
        public String toString() {
            return this.name() + " - " + desc;
        }
    }



    public Natureza() {
    }

    @Id
    @Column(name = "NATUREZA")
    public String getNatureza() {
        return natureza.get();
    }

    public StringProperty naturezaProperty() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        setCodigoFilter(natureza);
        this.natureza.set(natureza);
    }

    @Column(name = "ALIQUOTA")
    public BigDecimal getAliquota() {
        return aliquota.get();
    }

    public ObjectProperty<BigDecimal> aliquotaProperty() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota.set(aliquota);
    }

    @Column(name = "TP_BASE")
    public String getTpBase() {
        return tpBase.get();
    }

    public StringProperty tpBaseProperty() {
        return tpBase;
    }

    public void setTpBase(String tpBase) {
        this.tpBase.set(tpBase);
    }

    @Column(name = "DUPLI")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isDupli() {
        return dupli.get();
    }

    public BooleanProperty dupliProperty() {
        return dupli;
    }

    public void setDupli(Boolean dupli) {
        this.dupli.set(dupli);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "IPI")
    public String getIpi() {
        return ipi.get();
    }

    public StringProperty ipiProperty() {
        return ipi;
    }

    public void setIpi(String ipi) {
        this.ipi.set(ipi);
    }

    @Column(name = "PIS_COFINS")
    public String getPisCofins() {
        return pisCofins.get();
    }

    public StringProperty pisCofinsProperty() {
        return pisCofins;
    }

    public void setPisCofins(String pisCofins) {
        this.pisCofins.set(pisCofins);
    }

    @Column(name = "ICMS")
    public String getIcms() {
        return icms.get();
    }

    public StringProperty icmsProperty() {
        return icms;
    }

    public void setIcms(String icms) {
        this.icms.set(icms);
    }

    @Column(name = "CLAFIS")
    public String getClafis() {
        return clafis.get();
    }

    public StringProperty clafisProperty() {
        return clafis;
    }

    public void setClafis(String clafis) {
        this.clafis.set(clafis);
    }

    @Column(name = "CLATRIB")
    public String getClatrib() {
        return clatrib.get();
    }

    public StringProperty clatribProperty() {
        return clatrib;
    }

    public void setClatrib(String clatrib) {
        this.clatrib.set(clatrib);
    }

    @Column(name = "MENSAGEM")
    public String getMensagem() {
        return mensagem.get();
    }

    public StringProperty mensagemProperty() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem.set(mensagem);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "NAT_CONT")
    public String getNatCont() {
        return natCont.get();
    }

    public StringProperty natContProperty() {
        return natCont;
    }

    public void setNatCont(String natCont) {
        this.natCont.set(natCont);
    }

    @Column(name = "SUBST_TRIB")
    public String getSubstTrib() {
        return substTrib.get();
    }

    public StringProperty substTribProperty() {
        return substTrib;
    }

    public void setSubstTrib(String substTrib) {
        this.substTrib.set(substTrib);
    }

    @Column(name = "COMPLEMENTO")
    public String getComplemento() {
        return complemento.get();
    }

    public StringProperty complementoProperty() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento.set(complemento);
    }

    @Column(name = "TRIB_IPI")
    public String getTribIpi() {
        return tribIpi.get();
    }

    public StringProperty tribIpiProperty() {
        return tribIpi;
    }

    public void setTribIpi(String tribIpi) {
        this.tribIpi.set(tribIpi);
    }

    @Column(name = "TRIB_PIS")
    public String getTribPis() {
        return tribPis.get();
    }

    public StringProperty tribPisProperty() {
        return tribPis;
    }

    public void setTribPis(String tribPis) {
        this.tribPis.set(tribPis);
    }

    @Column(name = "TRIB_COFINS")
    public String getTribCofins() {
        return tribCofins.get();
    }

    public StringProperty tribCofinsProperty() {
        return tribCofins;
    }

    public void setTribCofins(String tribCofins) {
        this.tribCofins.set(tribCofins);
    }

    @Column(name = "ALIQ_PIS")
    public BigDecimal getAliqPis() {
        return aliqPis.get();
    }

    public ObjectProperty<BigDecimal> aliqPisProperty() {
        return aliqPis;
    }

    public void setAliqPis(BigDecimal aliqPis) {
        this.aliqPis.set(aliqPis);
    }

    @Column(name = "ALIQ_COFINS")
    public BigDecimal getAliqCofins() {
        return aliqCofins.get();
    }

    public ObjectProperty<BigDecimal> aliqCofinsProperty() {
        return aliqCofins;
    }

    public void setAliqCofins(BigDecimal aliqCofins) {
        this.aliqCofins.set(aliqCofins);
    }

    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }

    public StringProperty codspedProperty() {
        return codsped;
    }

    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }

    @Column(name = "CFOP")
    public String getCfop() {
        return cfop.get();
    }

    public StringProperty cfopProperty() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop.set(cfop);
    }

    @Column(name = "COMP_BASE_ICMS")
    public String getCompBaseIcms() {
        return compBaseIcms.get();
    }

    public StringProperty compBaseIcmsProperty() {
        return compBaseIcms;
    }

    public void setCompBaseIcms(String compBaseIcms) {
        this.compBaseIcms.set(compBaseIcms);
    }

    @Column(name = "COMP_BASE_PICO")
    public String getCompBasePico() {
        return compBasePico.get();
    }

    public StringProperty compBasePicoProperty() {
        return compBasePico;
    }

    public void setCompBasePico(String compBasePico) {
        this.compBasePico.set(compBasePico);
    }

    @Column(name = "COD_AJ_SPED")
    public String getCodAjSped() {
        return codAjSped.get();
    }

    public StringProperty codAjSpedProperty() {
        return codAjSped;
    }

    public void setCodAjSped(String codAjSped) {
        this.codAjSped.set(codAjSped);
    }

    @Column(name = "CONS_FINAL")
    public String getConsFinal() {
        return consFinal.get();
    }

    public StringProperty consFinalProperty() {
        return consFinal;
    }

    public void setConsFinal(String consFinal) {
        this.consFinal.set(consFinal);
    }

    @Column(name = "MOV_EST")
    public String getMovEst() {
        return movEst.get();
    }

    public StringProperty movEstProperty() {
        return movEst;
    }

    public void setMovEst(String movEst) {
        this.movEst.set(movEst);
    }

    @Column(name = "CODTNS")
    public String getCodTns() {
        return codTns.get();
    }

    public StringProperty codTnsProperty() {
        return codTns;
    }

    public void setCodTns(String codTns) {
        this.codTns.set(codTns);
    }

    @Override
    public String toString() {
        return natureza.get();
    }
}
