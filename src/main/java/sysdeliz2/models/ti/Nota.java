package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdNota;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "NOTA_001")
public class Nota {
    
    private final IntegerProperty volumes = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> aliquota = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pecas = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pesol = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pesob = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<LocalDate> dtsaida = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> basecalc = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> baseipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valfrete = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valseguro = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tipfat = new SimpleStringProperty("P");
    private final StringProperty cif = new SimpleStringProperty();
    private final StringProperty impresso = new SimpleStringProperty();
    private final StringProperty transport = new SimpleStringProperty();
    private final StringProperty redesp = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final ObjectProperty<Natureza> natureza = new SimpleObjectProperty<>();
    private final StringProperty especie = new SimpleStringProperty("CAIXAS");
    private final ObjectProperty<BigDecimal> valicms = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valprodutos = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty protocolocorr = new SimpleStringProperty();
    private final StringProperty placa = new SimpleStringProperty();
    private final IntegerProperty codven = new SimpleIntegerProperty(0);
    private final StringProperty codcli2 = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty servico = new SimpleStringProperty();
    private final StringProperty natserv = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> aliqserv = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valserv = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> despesas = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty cobcli = new SimpleStringProperty();
    private final StringProperty notadev = new SimpleStringProperty();
    private final StringProperty mensagem = new SimpleStringProperty();
    private final StringProperty serie = new SimpleStringProperty("1");
    private final StringProperty nffrete = new SimpleStringProperty();
    private final StringProperty motivo = new SimpleStringProperty();
    private final StringProperty redespcif = new SimpleStringProperty();
    private final StringProperty docto = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valsubst = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> basesubst = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty baiest = new SimpleStringProperty("N");
    private final ObjectProperty<BigDecimal> valpis = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valcofins = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valimport = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty lotenfe = new SimpleStringProperty();
    private final StringProperty recibonfe = new SimpleStringProperty();
    private final StringProperty protocolonfe = new SimpleStringProperty();
    private final StringProperty chavenfe = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pedagio = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> fretepesov = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> freteseguro = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> fretedespacho = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> freteoutros = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valdesonera = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty nrvolumes = new SimpleStringProperty();
    private final StringProperty tipofiscal = new SimpleStringProperty();
    private final StringProperty docto2 = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valfcp = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valicmsremet = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valicmsdest = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final IntegerProperty usunumreg = new SimpleIntegerProperty();
    private final StringProperty importsap = new SimpleStringProperty("N");
    private final IntegerProperty enviada = new SimpleIntegerProperty(0);
    private final ObjectProperty<SdNota> sdNota = new SimpleObjectProperty<>();
    private List<NotaIten> itens = new ArrayList<>();
    
    public Nota() {
    }




    @Column(name = "VOLUMES")
    public Integer getVolumes() {
        return volumes.get();
    }
    
    public IntegerProperty volumesProperty() {
        return volumes;
    }
    
    public void setVolumes(Integer volumes) {
        this.volumes.set(volumes);
    }
    
    @Column(name = "ALIQUOTA")
    public BigDecimal getAliquota() {
        return aliquota.get();
    }
    
    public ObjectProperty<BigDecimal> aliquotaProperty() {
        return aliquota;
    }
    
    public void setAliquota(BigDecimal aliquota) {
        this.aliquota.set(aliquota);
    }
    
    @Column(name = "PECAS")
    public BigDecimal getPecas() {
        return pecas.get();
    }
    
    public ObjectProperty<BigDecimal> pecasProperty() {
        return pecas;
    }
    
    public void setPecas(BigDecimal pecas) {
        this.pecas.set(pecas);
    }
    
    @Column(name = "PESOL")
    public BigDecimal getPesol() {
        return pesol.get();
    }
    
    public ObjectProperty<BigDecimal> pesolProperty() {
        return pesol;
    }
    
    public void setPesol(BigDecimal pesol) {
        this.pesol.set(pesol);
    }
    
    @Column(name = "PESOB")
    public BigDecimal getPesob() {
        return pesob.get();
    }
    
    public ObjectProperty<BigDecimal> pesobProperty() {
        return pesob;
    }
    
    public void setPesob(BigDecimal pesob) {
        this.pesob.set(pesob);
    }
    
    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }
    
    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }
    
    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }
    
    @Column(name = "DT_SAIDA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtsaida() {
        return dtsaida.get();
    }
    
    public ObjectProperty<LocalDate> dtsaidaProperty() {
        return dtsaida;
    }
    
    public void setDtsaida(LocalDate dtsaida) {
        this.dtsaida.set(dtsaida);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }
    
    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }
    
    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }
    
    @Column(name = "BASE_CALC")
    public BigDecimal getBasecalc() {
        return basecalc.get();
    }
    
    public ObjectProperty<BigDecimal> basecalcProperty() {
        return basecalc;
    }
    
    public void setBasecalc(BigDecimal basecalc) {
        this.basecalc.set(basecalc);
    }
    
    @Column(name = "BASE_IPI")
    public BigDecimal getBaseipi() {
        return baseipi.get();
    }
    
    public ObjectProperty<BigDecimal> baseipiProperty() {
        return baseipi;
    }
    
    public void setBaseipi(BigDecimal baseipi) {
        this.baseipi.set(baseipi);
    }
    
    @Column(name = "VAL_IPI")
    public BigDecimal getValipi() {
        return valipi.get();
    }
    
    public ObjectProperty<BigDecimal> valipiProperty() {
        return valipi;
    }
    
    public void setValipi(BigDecimal valipi) {
        this.valipi.set(valipi);
    }
    
    @Column(name = "VAL_FRETE")
    public BigDecimal getValfrete() {
        return valfrete.get();
    }
    
    public ObjectProperty<BigDecimal> valfreteProperty() {
        return valfrete;
    }
    
    public void setValfrete(BigDecimal valfrete) {
        this.valfrete.set(valfrete);
    }
    
    @Column(name = "VAL_SEGURO")
    public BigDecimal getValseguro() {
        return valseguro.get();
    }
    
    public ObjectProperty<BigDecimal> valseguroProperty() {
        return valseguro;
    }
    
    public void setValseguro(BigDecimal valseguro) {
        this.valseguro.set(valseguro);
    }
    
    @Column(name = "TIPFAT")
    public String getTipfat() {
        return tipfat.get();
    }
    
    public StringProperty tipfatProperty() {
        return tipfat;
    }
    
    public void setTipfat(String tipfat) {
        this.tipfat.set(tipfat);
    }
    
    @Column(name = "CIF")
    public String getCif() {
        return cif.get();
    }
    
    public StringProperty cifProperty() {
        return cif;
    }
    
    public void setCif(String cif) {
        this.cif.set(cif);
    }
    
    @Column(name = "IMPRESSO")
    public String getImpresso() {
        return impresso.get();
    }
    
    public StringProperty impressoProperty() {
        return impresso;
    }
    
    public void setImpresso(String impresso) {
        this.impresso.set(impresso);
    }
    
    @Column(name = "TRANSPORT")
    public String getTransport() {
        return transport.get();
    }
    
    public StringProperty transportProperty() {
        return transport;
    }
    
    public void setTransport(String transport) {
        this.transport.set(transport);
    }
    
    @Column(name = "REDESP")
    public String getRedesp() {
        return redesp.get();
    }
    
    public StringProperty redespProperty() {
        return redesp;
    }
    
    public void setRedesp(String redesp) {
        this.redesp.set(redesp);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Id
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @OneToOne
    @JoinColumn(name = "NATUREZA")
    public Natureza getNatureza() {
        return natureza.get();
    }
    
    public ObjectProperty<Natureza> naturezaProperty() {
        return natureza;
    }
    
    public void setNatureza(Natureza natureza) {
        this.natureza.set(natureza);
    }
    
    @Column(name = "ESPECIE")
    public String getEspecie() {
        return especie.get();
    }
    
    public StringProperty especieProperty() {
        return especie;
    }
    
    public void setEspecie(String especie) {
        this.especie.set(especie);
    }
    
    @Column(name = "VAL_ICMS")
    public BigDecimal getValicms() {
        return valicms.get();
    }
    
    public ObjectProperty<BigDecimal> valicmsProperty() {
        return valicms;
    }
    
    public void setValicms(BigDecimal valicms) {
        this.valicms.set(valicms);
    }
    
    @Column(name = "VAL_PRODUTOS")
    public BigDecimal getValprodutos() {
        return valprodutos.get();
    }
    
    public ObjectProperty<BigDecimal> valprodutosProperty() {
        return valprodutos;
    }
    
    public void setValprodutos(BigDecimal valprodutos) {
        this.valprodutos.set(valprodutos);
    }
    
    @Column(name = "PROTOCOLO_CORR")
    public String getProtocolocorr() {
        return protocolocorr.get();
    }
    
    public StringProperty protocolocorrProperty() {
        return protocolocorr;
    }
    
    public void setProtocolocorr(String protocolocorr) {
        this.protocolocorr.set(protocolocorr);
    }
    
    @Column(name = "PLACA")
    public String getPlaca() {
        return placa.get();
    }
    
    public StringProperty placaProperty() {
        return placa;
    }
    
    public void setPlaca(String placa) {
        this.placa.set(placa);
    }
    
    @Column(name = "CODVEN")
    public Integer getCodven() {
        return codven.get();
    }
    
    public IntegerProperty codvenProperty() {
        return codven;
    }
    
    public void setCodven(Integer codven) {
        this.codven.set(codven);
    }
    
    @Column(name = "CODCLI2")
    public String getCodcli2() {
        return codcli2.get();
    }
    
    public StringProperty codcli2Property() {
        return codcli2;
    }
    
    public void setCodcli2(String codcli2) {
        this.codcli2.set(codcli2);
    }
    
    @Column(name = "OBS")
    @Lob
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }
    
    @Column(name = "SERVICO")
    @Lob
    public String getServico() {
        return servico.get();
    }
    
    public StringProperty servicoProperty() {
        return servico;
    }
    
    public void setServico(String servico) {
        this.servico.set(servico);
    }
    
    @Column(name = "NAT_SERV")
    public String getNatserv() {
        return natserv.get();
    }
    
    public StringProperty natservProperty() {
        return natserv;
    }
    
    public void setNatserv(String natserv) {
        this.natserv.set(natserv);
    }
    
    @Column(name = "ALIQ_SERV")
    public BigDecimal getAliqserv() {
        return aliqserv.get();
    }
    
    public ObjectProperty<BigDecimal> aliqservProperty() {
        return aliqserv;
    }
    
    public void setAliqserv(BigDecimal aliqserv) {
        this.aliqserv.set(aliqserv);
    }
    
    @Column(name = "VAL_SERV")
    public BigDecimal getValserv() {
        return valserv.get();
    }
    
    public ObjectProperty<BigDecimal> valservProperty() {
        return valserv;
    }
    
    public void setValserv(BigDecimal valserv) {
        this.valserv.set(valserv);
    }
    
    @Column(name = "DESPESAS")
    public BigDecimal getDespesas() {
        return despesas.get();
    }
    
    public ObjectProperty<BigDecimal> despesasProperty() {
        return despesas;
    }
    
    public void setDespesas(BigDecimal despesas) {
        this.despesas.set(despesas);
    }
    
    @Column(name = "COBCLI")
    public String getCobcli() {
        return cobcli.get();
    }
    
    public StringProperty cobcliProperty() {
        return cobcli;
    }
    
    public void setCobcli(String cobcli) {
        this.cobcli.set(cobcli);
    }
    
    @Column(name = "NOTA_DEV")
    public String getNotadev() {
        return notadev.get();
    }
    
    public StringProperty notadevProperty() {
        return notadev;
    }
    
    public void setNotadev(String notadev) {
        this.notadev.set(notadev);
    }
    
    @Column(name = "MENSAGEM")
    @Lob
    public String getMensagem() {
        return mensagem.get();
    }
    
    public StringProperty mensagemProperty() {
        return mensagem;
    }
    
    public void setMensagem(String mensagem) {
        this.mensagem.set(mensagem);
    }
    
    @Column(name = "SERIE")
    public String getSerie() {
        return serie.get();
    }
    
    public StringProperty serieProperty() {
        return serie;
    }
    
    public void setSerie(String serie) {
        this.serie.set(serie);
    }
    
    @Column(name = "NF_FRETE")
    public String getNffrete() {
        return nffrete.get();
    }
    
    public StringProperty nffreteProperty() {
        return nffrete;
    }
    
    public void setNffrete(String nffrete) {
        this.nffrete.set(nffrete);
    }
    
    @Column(name = "MOTIVO")
    public String getMotivo() {
        return motivo.get();
    }
    
    public StringProperty motivoProperty() {
        return motivo;
    }
    
    public void setMotivo(String motivo) {
        this.motivo.set(motivo);
    }
    
    @Column(name = "REDESP_CIF")
    public String getRedespcif() {
        return redespcif.get();
    }
    
    public StringProperty redespcifProperty() {
        return redespcif;
    }
    
    public void setRedespcif(String redespcif) {
        this.redespcif.set(redespcif);
    }
    
    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }
    
    public StringProperty doctoProperty() {
        return docto;
    }
    
    public void setDocto(String docto) {
        this.docto.set(docto);
    }
    
    @Column(name = "VAL_SUBST")
    public BigDecimal getValsubst() {
        return valsubst.get();
    }
    
    public ObjectProperty<BigDecimal> valsubstProperty() {
        return valsubst;
    }
    
    public void setValsubst(BigDecimal valsubst) {
        this.valsubst.set(valsubst);
    }
    
    @Column(name = "BASE_SUBST")
    public BigDecimal getBasesubst() {
        return basesubst.get();
    }
    
    public ObjectProperty<BigDecimal> basesubstProperty() {
        return basesubst;
    }
    
    public void setBasesubst(BigDecimal basesubst) {
        this.basesubst.set(basesubst);
    }
    
    @Column(name = "BAI_EST")
    public String getBaiest() {
        return baiest.get();
    }
    
    public StringProperty baiestProperty() {
        return baiest;
    }
    
    public void setBaiest(String baiest) {
        this.baiest.set(baiest);
    }
    
    @Column(name = "VAL_PIS")
    public BigDecimal getValpis() {
        return valpis.get();
    }
    
    public ObjectProperty<BigDecimal> valpisProperty() {
        return valpis;
    }
    
    public void setValpis(BigDecimal valpis) {
        this.valpis.set(valpis);
    }
    
    @Column(name = "VAL_COFINS")
    public BigDecimal getValcofins() {
        return valcofins.get();
    }
    
    public ObjectProperty<BigDecimal> valcofinsProperty() {
        return valcofins;
    }
    
    public void setValcofins(BigDecimal valcofins) {
        this.valcofins.set(valcofins);
    }
    
    @Column(name = "VAL_IMPORT")
    public BigDecimal getValimport() {
        return valimport.get();
    }
    
    public ObjectProperty<BigDecimal> valimportProperty() {
        return valimport;
    }
    
    public void setValimport(BigDecimal valimport) {
        this.valimport.set(valimport);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "LOTE_NFE")
    public String getLotenfe() {
        return lotenfe.get();
    }
    
    public StringProperty lotenfeProperty() {
        return lotenfe;
    }
    
    public void setLotenfe(String lotenfe) {
        this.lotenfe.set(lotenfe);
    }
    
    @Column(name = "RECIBO_NFE")
    public String getRecibonfe() {
        return recibonfe.get();
    }
    
    public StringProperty recibonfeProperty() {
        return recibonfe;
    }
    
    public void setRecibonfe(String recibonfe) {
        this.recibonfe.set(recibonfe);
    }
    
    @Column(name = "PROTOCOLO_NFE")
    public String getProtocolonfe() {
        return protocolonfe.get();
    }
    
    public StringProperty protocolonfeProperty() {
        return protocolonfe;
    }
    
    public void setProtocolonfe(String protocolonfe) {
        this.protocolonfe.set(protocolonfe);
    }
    
    @Column(name = "CHAVE_NFE")
    public String getChavenfe() {
        return chavenfe.get();
    }
    
    public StringProperty chavenfeProperty() {
        return chavenfe;
    }
    
    public void setChavenfe(String chavenfe) {
        this.chavenfe.set(chavenfe);
    }
    
    @Column(name = "PEDAGIO")
    public BigDecimal getPedagio() {
        return pedagio.get();
    }
    
    public ObjectProperty<BigDecimal> pedagioProperty() {
        return pedagio;
    }
    
    public void setPedagio(BigDecimal pedagio) {
        this.pedagio.set(pedagio);
    }
    
    @Column(name = "FRETE_PESOV")
    public BigDecimal getFretepesov() {
        return fretepesov.get();
    }
    
    public ObjectProperty<BigDecimal> fretepesovProperty() {
        return fretepesov;
    }
    
    public void setFretepesov(BigDecimal fretepesov) {
        this.fretepesov.set(fretepesov);
    }
    
    @Column(name = "FRETE_SEGURO")
    public BigDecimal getFreteseguro() {
        return freteseguro.get();
    }
    
    public ObjectProperty<BigDecimal> freteseguroProperty() {
        return freteseguro;
    }
    
    public void setFreteseguro(BigDecimal freteseguro) {
        this.freteseguro.set(freteseguro);
    }
    
    @Column(name = "FRETE_DESPACHO")
    public BigDecimal getFretedespacho() {
        return fretedespacho.get();
    }
    
    public ObjectProperty<BigDecimal> fretedespachoProperty() {
        return fretedespacho;
    }
    
    public void setFretedespacho(BigDecimal fretedespacho) {
        this.fretedespacho.set(fretedespacho);
    }
    
    @Column(name = "FRETE_OUTROS")
    public BigDecimal getFreteoutros() {
        return freteoutros.get();
    }
    
    public ObjectProperty<BigDecimal> freteoutrosProperty() {
        return freteoutros;
    }
    
    public void setFreteoutros(BigDecimal freteoutros) {
        this.freteoutros.set(freteoutros);
    }
    
    @Column(name = "VAL_DESONERA")
    public BigDecimal getValdesonera() {
        return valdesonera.get();
    }
    
    public ObjectProperty<BigDecimal> valdesoneraProperty() {
        return valdesonera;
    }
    
    public void setValdesonera(BigDecimal valdesonera) {
        this.valdesonera.set(valdesonera);
    }
    
    @Column(name = "NR_VOLUMES")
    public String getNrvolumes() {
        return nrvolumes.get();
    }
    
    public StringProperty nrvolumesProperty() {
        return nrvolumes;
    }
    
    public void setNrvolumes(String nrvolumes) {
        this.nrvolumes.set(nrvolumes);
    }
    
    @Column(name = "TIPO_FISCAL")
    public String getTipofiscal() {
        return tipofiscal.get();
    }
    
    public StringProperty tipofiscalProperty() {
        return tipofiscal;
    }
    
    public void setTipofiscal(String tipofiscal) {
        this.tipofiscal.set(tipofiscal);
    }
    
    @Column(name = "DOCTO2")
    public String getDocto2() {
        return docto2.get();
    }
    
    public StringProperty docto2Property() {
        return docto2;
    }
    
    public void setDocto2(String docto2) {
        this.docto2.set(docto2);
    }
    
    @Column(name = "VAL_FCP")
    public BigDecimal getValfcp() {
        return valfcp.get();
    }
    
    public ObjectProperty<BigDecimal> valfcpProperty() {
        return valfcp;
    }
    
    public void setValfcp(BigDecimal valfcp) {
        this.valfcp.set(valfcp);
    }
    
    @Column(name = "VAL_ICMS_REMET")
    public BigDecimal getValicmsremet() {
        return valicmsremet.get();
    }
    
    public ObjectProperty<BigDecimal> valicmsremetProperty() {
        return valicmsremet;
    }
    
    public void setValicmsremet(BigDecimal valicmsremet) {
        this.valicmsremet.set(valicmsremet);
    }
    
    @Column(name = "VAL_ICMS_DEST")
    public BigDecimal getValicmsdest() {
        return valicmsdest.get();
    }
    
    public ObjectProperty<BigDecimal> valicmsdestProperty() {
        return valicmsdest;
    }
    
    public void setValicmsdest(BigDecimal valicmsdest) {
        this.valicmsdest.set(valicmsdest);
    }
    
    @Column(name = "USU_NUMREG")
    public Integer getUsunumreg() {
        return usunumreg.get();
    }
    
    public IntegerProperty usunumregProperty() {
        return usunumreg;
    }
    
    public void setUsunumreg(Integer usunumreg) {
        this.usunumreg.set(usunumreg);
    }
    
    @Column(name = "IMPORT_SAP")
    public String getImportsap() {
        return importsap.get();
    }
    
    public StringProperty importsapProperty() {
        return importsap;
    }
    
    public void setImportsap(String importsap) {
        this.importsap.set(importsap);
    }
    
    @Column(name = "ENVIADA")
    public Integer getEnviada() {
        return enviada.get();
    }
    
    public IntegerProperty enviadaProperty() {
        return enviada;
    }
    
    public void setEnviada(Integer enviada) {
        this.enviada.set(enviada);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FATURA")
    public SdNota getSdNota() {
        return sdNota.get();
    }
    
    public ObjectProperty<SdNota> sdNotaProperty() {
        return sdNota;
    }
    
    public void setSdNota(SdNota sdNota) {
        this.sdNota.set(sdNota);
    }
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FATURA")
    public List<NotaIten> getItens() {
        return itens;
    }
    
    public void setItens(List<NotaIten> itens) {
        this.itens = itens;
    }


}
