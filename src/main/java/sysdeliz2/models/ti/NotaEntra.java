package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "NOTA_ENTRA_004")
@TelaSysDeliz(descricao = "Notas Fiscais", icon = "nota fiscal_100.png")
public class NotaEntra extends BasicModel {

    @ExibeTableView(descricao = "Dados da Nota", width = 250)
    private final ObjectProperty<NotaEntraPK> id = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Data de Entrada", width = 100)
    private final ObjectProperty<LocalDate> dtentrada = new SimpleObjectProperty<LocalDate>();
    private final StringProperty natureza = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> aliquota = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseicm = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Valor", width = 80)
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseipi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valoricm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valoripi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> icmIsenta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> ipiIsenta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> outrasicm = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> outrasipi = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty condicao = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>();
    private final StringProperty grupo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> seguro = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valprodutos = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> despesa = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> baseicmsdif = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> fretenf = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty codajusdif = new SimpleStringProperty();
    private final StringProperty nsu = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cofins = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valcardex = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty docto = new SimpleStringProperty();
    private final StringProperty doctopag = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtdigitacao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> valorirrf = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseirrf = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorcsll = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> basecsll = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty tpfrete = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> aliqdif = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valicmsdif = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseiss = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valoriss = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorinss = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseinss = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty motivo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> basest = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorst = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty mensagem = new SimpleStringProperty();
    private final StringProperty importsap = new SimpleStringProperty();
    private final IntegerProperty usunumreg = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valimport = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pisret = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cofinsret = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty sdStatusExportacao = new SimpleStringProperty();
    private List<NotaEntraIten> listItens = new ArrayList<>();

    public NotaEntra() {
    }

    @EmbeddedId
    public NotaEntraPK getId() {
        return id.get();
    }

    public ObjectProperty<NotaEntraPK> idProperty() {
        return id;
    }

    public void setId(NotaEntraPK id) {
        this.id.set(id);
    }

    @Column(name = "DT_ENTRADA")
    public LocalDate getDtentrada() {
        return dtentrada.get();
    }

    public ObjectProperty<LocalDate> dtentradaProperty() {
        return dtentrada;
    }

    public void setDtentrada(LocalDate dtentrada) {
        this.dtentrada.set(dtentrada);
    }

    @Column(name = "NATUREZA")
    public String getNatureza() {
        return natureza.get();
    }

    public StringProperty naturezaProperty() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        this.natureza.set(natureza);
    }

    @Column(name = "ALIQUOTA")
    public BigDecimal getAliquota() {
        return aliquota.get();
    }

    public ObjectProperty<BigDecimal> aliquotaProperty() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota.set(aliquota);
    }

    @Column(name = "BASE_ICM")
    public BigDecimal getBaseicm() {
        return baseicm.get();
    }

    public ObjectProperty<BigDecimal> baseicmProperty() {
        return baseicm;
    }

    public void setBaseicm(BigDecimal baseicm) {
        this.baseicm.set(baseicm);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "BASE_IPI")
    public BigDecimal getBaseipi() {
        return baseipi.get();
    }

    public ObjectProperty<BigDecimal> baseipiProperty() {
        return baseipi;
    }

    public void setBaseipi(BigDecimal baseipi) {
        this.baseipi.set(baseipi);
    }

    @Column(name = "VALOR_ICM")
    public BigDecimal getValoricm() {
        return valoricm.get();
    }

    public ObjectProperty<BigDecimal> valoricmProperty() {
        return valoricm;
    }

    public void setValoricm(BigDecimal valoricm) {
        this.valoricm.set(valoricm);
    }

    @Column(name = "VALOR_IPI")
    public BigDecimal getValoripi() {
        return valoripi.get();
    }

    public ObjectProperty<BigDecimal> valoripiProperty() {
        return valoripi;
    }

    public void setValoripi(BigDecimal valoripi) {
        this.valoripi.set(valoripi);
    }

    @Column(name = "ISENTA_ICM")
    public BigDecimal getIcmIsenta() {
        return icmIsenta.get();
    }

    public ObjectProperty<BigDecimal> icmIsentaProperty() {
        return icmIsenta;
    }

    public void setIcmIsenta(BigDecimal icmIsenta) {
        this.icmIsenta.set(icmIsenta);
    }

    @Column(name = "ISENTA_IPI")
    public BigDecimal getIpiIsenta() {
        return ipiIsenta.get();
    }

    public ObjectProperty<BigDecimal> ipiIsentaProperty() {
        return ipiIsenta;
    }

    public void setIpiIsenta(BigDecimal ipiIsenta) {
        this.ipiIsenta.set(ipiIsenta);
    }

    @Column(name = "OUTRAS_ICM")
    public BigDecimal getOutrasicm() {
        return outrasicm.get();
    }

    public ObjectProperty<BigDecimal> outrasicmProperty() {
        return outrasicm;
    }

    public void setOutrasicm(BigDecimal outrasicm) {
        this.outrasicm.set(outrasicm);
    }

    @Column(name = "OUTRAS_IPI")
    public BigDecimal getOutrasipi() {
        return outrasipi.get();
    }

    public ObjectProperty<BigDecimal> outrasipiProperty() {
        return outrasipi;
    }

    public void setOutrasipi(BigDecimal outrasipi) {
        this.outrasipi.set(outrasipi);
    }

    @Column(name = "CONDICAO")
    public String getCondicao() {
        return condicao.get();
    }

    public StringProperty condicaoProperty() {
        return condicao;
    }

    public void setCondicao(String condicao) {
        this.condicao.set(condicao);
    }

    @Column(name = "DT_EMISSAO")
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "SEGURO")
    public BigDecimal getSeguro() {
        return seguro.get();
    }

    public ObjectProperty<BigDecimal> seguroProperty() {
        return seguro;
    }

    public void setSeguro(BigDecimal seguro) {
        this.seguro.set(seguro);
    }

    @Column(name = "VAL_PRODUTOS")
    public BigDecimal getValprodutos() {
        return valprodutos.get();
    }

    public ObjectProperty<BigDecimal> valprodutosProperty() {
        return valprodutos;
    }

    public void setValprodutos(BigDecimal valprodutos) {
        this.valprodutos.set(valprodutos);
    }

    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }

    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "DESPESA")
    public BigDecimal getDespesa() {
        return despesa.get();
    }

    public ObjectProperty<BigDecimal> despesaProperty() {
        return despesa;
    }

    public void setDespesa(BigDecimal despesa) {
        this.despesa.set(despesa);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "BASE_ICMS_DIF")
    public BigDecimal getBaseicmsdif() {
        return baseicmsdif.get();
    }

    public ObjectProperty<BigDecimal> baseicmsdifProperty() {
        return baseicmsdif;
    }

    public void setBaseicmsdif(BigDecimal baseicmsdif) {
        this.baseicmsdif.set(baseicmsdif);
    }

    @Column(name = "FRETENF")
    public BigDecimal getFretenf() {
        return fretenf.get();
    }

    public ObjectProperty<BigDecimal> fretenfProperty() {
        return fretenf;
    }

    public void setFretenf(BigDecimal fretenf) {
        this.fretenf.set(fretenf);
    }

    @Column(name = "COD_AJUS_DIF")
    public String getCodajusdif() {
        return codajusdif.get();
    }

    public StringProperty codajusdifProperty() {
        return codajusdif;
    }

    public void setCodajusdif(String codajusdif) {
        this.codajusdif.set(codajusdif);
    }

    @Column(name = "NSU")
    public String getNsu() {
        return nsu.get();
    }

    public StringProperty nsuProperty() {
        return nsu;
    }

    public void setNsu(String nsu) {
        this.nsu.set(nsu);
        setCodigoFilter(nsu);
    }

    @Column(name = "PIS")
    public BigDecimal getPis() {
        return pis.get();
    }

    public ObjectProperty<BigDecimal> pisProperty() {
        return pis;
    }

    public void setPis(BigDecimal pis) {
        this.pis.set(pis);
    }

    @Column(name = "COFINS")
    public BigDecimal getCofins() {
        return cofins.get();
    }

    public ObjectProperty<BigDecimal> cofinsProperty() {
        return cofins;
    }

    public void setCofins(BigDecimal cofins) {
        this.cofins.set(cofins);
    }

    @Column(name = "VAL_CARDEX")
    public BigDecimal getValcardex() {
        return valcardex.get();
    }

    public ObjectProperty<BigDecimal> valcardexProperty() {
        return valcardex;
    }

    public void setValcardex(BigDecimal valcardex) {
        this.valcardex.set(valcardex);
    }

    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }

    public StringProperty doctoProperty() {
        return docto;
    }

    public void setDocto(String docto) {
        this.docto.set(docto);
    }

    @Column(name = "DOCTO_PAG")
    public String getDoctopag() {
        return doctopag.get();
    }

    public StringProperty doctopagProperty() {
        return doctopag;
    }

    public void setDoctopag(String doctopag) {
        this.doctopag.set(doctopag);
    }

    @Column(name = "DT_DIGITACAO")
    public LocalDate getDtdigitacao() {
        return dtdigitacao.get();
    }

    public ObjectProperty<LocalDate> dtdigitacaoProperty() {
        return dtdigitacao;
    }

    public void setDtdigitacao(LocalDate dtdigitacao) {
        this.dtdigitacao.set(dtdigitacao);
    }

    @Column(name = "VALOR_IRRF")
    public BigDecimal getValorirrf() {
        return valorirrf.get();
    }

    public ObjectProperty<BigDecimal> valorirrfProperty() {
        return valorirrf;
    }

    public void setValorirrf(BigDecimal valorirrf) {
        this.valorirrf.set(valorirrf);
    }

    @Column(name = "BASE_IRRF")
    public BigDecimal getBaseirrf() {
        return baseirrf.get();
    }

    public ObjectProperty<BigDecimal> baseirrfProperty() {
        return baseirrf;
    }

    public void setBaseirrf(BigDecimal baseirrf) {
        this.baseirrf.set(baseirrf);
    }

    @Column(name = "VALOR_CSLL")
    public BigDecimal getValorcsll() {
        return valorcsll.get();
    }

    public ObjectProperty<BigDecimal> valorcsllProperty() {
        return valorcsll;
    }

    public void setValorcsll(BigDecimal valorcsll) {
        this.valorcsll.set(valorcsll);
    }

    @Column(name = "BASE_CSLL")
    public BigDecimal getBasecsll() {
        return basecsll.get();
    }

    public ObjectProperty<BigDecimal> basecsllProperty() {
        return basecsll;
    }

    public void setBasecsll(BigDecimal basecsll) {
        this.basecsll.set(basecsll);
    }

    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "TP_FRETE")
    public String getTpfrete() {
        return tpfrete.get();
    }

    public StringProperty tpfreteProperty() {
        return tpfrete;
    }

    public void setTpfrete(String tpfrete) {
        this.tpfrete.set(tpfrete);
    }

    @Column(name = "ALIQ_DIF")
    public BigDecimal getAliqdif() {
        return aliqdif.get();
    }

    public ObjectProperty<BigDecimal> aliqdifProperty() {
        return aliqdif;
    }

    public void setAliqdif(BigDecimal aliqdif) {
        this.aliqdif.set(aliqdif);
    }

    @Column(name = "VAL_ICMS_DIF")
    public BigDecimal getValicmsdif() {
        return valicmsdif.get();
    }

    public ObjectProperty<BigDecimal> valicmsdifProperty() {
        return valicmsdif;
    }

    public void setValicmsdif(BigDecimal valicmsdif) {
        this.valicmsdif.set(valicmsdif);
    }

    @Column(name = "BASE_ISS")
    public BigDecimal getBaseiss() {
        return baseiss.get();
    }

    public ObjectProperty<BigDecimal> baseissProperty() {
        return baseiss;
    }

    public void setBaseiss(BigDecimal baseiss) {
        this.baseiss.set(baseiss);
    }

    @Column(name = "VALOR_ISS")
    public BigDecimal getValoriss() {
        return valoriss.get();
    }

    public ObjectProperty<BigDecimal> valorissProperty() {
        return valoriss;
    }

    public void setValoriss(BigDecimal valoriss) {
        this.valoriss.set(valoriss);
    }

    @Column(name = "VALOR_INSS")
    public BigDecimal getValorinss() {
        return valorinss.get();
    }

    public ObjectProperty<BigDecimal> valorinssProperty() {
        return valorinss;
    }

    public void setValorinss(BigDecimal valorinss) {
        this.valorinss.set(valorinss);
    }

    @Column(name = "BASE_INSS")
    public BigDecimal getBaseinss() {
        return baseinss.get();
    }

    public ObjectProperty<BigDecimal> baseinssProperty() {
        return baseinss;
    }

    public void setBaseinss(BigDecimal baseinss) {
        this.baseinss.set(baseinss);
    }

    @Column(name = "MOTIVO")
    public String getMotivo() {
        return motivo.get();
    }

    public StringProperty motivoProperty() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo.set(motivo);
    }

    @Column(name = "BASE_ST")
    public BigDecimal getBasest() {
        return basest.get();
    }

    public ObjectProperty<BigDecimal> basestProperty() {
        return basest;
    }

    public void setBasest(BigDecimal basest) {
        this.basest.set(basest);
    }

    @Column(name = "VALOR_ST")
    public BigDecimal getValorst() {
        return valorst.get();
    }

    public ObjectProperty<BigDecimal> valorstProperty() {
        return valorst;
    }

    public void setValorst(BigDecimal valorst) {
        this.valorst.set(valorst);
    }

    @Column(name = "MENSAGEM")
    @Lob
    public String getMensagem() {
        return mensagem.get();
    }

    public StringProperty mensagemProperty() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem.set(mensagem);
    }

    @Column(name = "IMPORT_SAP")
    public String getImportsap() {
        return importsap.get();
    }

    public StringProperty importsapProperty() {
        return importsap;
    }

    public void setImportsap(String importsap) {
        this.importsap.set(importsap);
    }

    @Column(name = "USU_NUMREG")
    public Integer getUsunumreg() {
        return usunumreg.get();
    }

    public IntegerProperty usunumregProperty() {
        return usunumreg;
    }

    public void setUsunumreg(Integer usunumreg) {
        this.usunumreg.set(usunumreg == null ? 0 : usunumreg);
    }

    @Column(name = "VAL_IMPORT")
    public BigDecimal getValimport() {
        return valimport.get();
    }

    public ObjectProperty<BigDecimal> valimportProperty() {
        return valimport;
    }

    public void setValimport(BigDecimal valimport) {
        this.valimport.set(valimport);
    }

    @Column(name = "PIS_RET")
    public BigDecimal getPisret() {
        return pisret.get();
    }

    public ObjectProperty<BigDecimal> pisretProperty() {
        return pisret;
    }

    public void setPisret(BigDecimal pisret) {
        this.pisret.set(pisret);
    }

    @Column(name = "COFINS_RET")
    public BigDecimal getCofinsret() {
        return cofinsret.get();
    }

    public ObjectProperty<BigDecimal> cofinsretProperty() {
        return cofinsret;
    }

    public void setCofinsret(BigDecimal cofinsret) {
        this.cofinsret.set(cofinsret);
    }

    @Column(name = "SD_STATUS_EXPORTACAO")
    public String getSdStatusExportacao() {
        return sdStatusExportacao.get();
    }

    public StringProperty sdStatusExportacaoProperty() {
        return sdStatusExportacao;
    }

    public void setSdStatusExportacao(String sdStatusExportacao) {
        this.sdStatusExportacao.set(sdStatusExportacao);
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "NUMERO"),
            @JoinColumn(name = "SERIE"),
            @JoinColumn(name = "CODCRE")
    })
    public List<NotaEntraIten> getListItens() {
        return listItens;
    }

    public void setListItens(List<NotaEntraIten> listItens) {
        this.listItens = listItens;
    }
}
