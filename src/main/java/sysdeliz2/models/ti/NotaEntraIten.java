package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "NF_ENTRA_ITEN_004")
public class NotaEntraIten {

    private final ObjectProperty<NotaEntraItenPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> basecofins = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vlripi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> basepis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cofins = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty natureza = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> icms = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty origem = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty clafis = new SimpleStringProperty();
    private final StringProperty clatrib = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();
    private final StringProperty codsped = new SimpleStringProperty();
    private final StringProperty tribipi = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> baseipi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baseicms = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty tribpis = new SimpleStringProperty();
    private final StringProperty tribcofins = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valicms = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valipi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valfrete = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valdesc = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valortot = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> basest = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorst = new SimpleObjectProperty<>();

    public NotaEntraIten() {
    }

    @EmbeddedId
    public NotaEntraItenPK getId() {
        return id.get();
    }

    public ObjectProperty<NotaEntraItenPK> idProperty() {
        return id;
    }

    public void setId(NotaEntraItenPK id) {
        this.id.set(id);
    }

    @Column(name = "BASE_COFINS")
    public BigDecimal getBasecofins() {
        return basecofins.get();
    }

    public ObjectProperty<BigDecimal> basecofinsProperty() {
        return basecofins;
    }

    public void setBasecofins(BigDecimal basecofins) {
        this.basecofins.set(basecofins);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "VLR_IPI")
    public BigDecimal getVlripi() {
        return vlripi.get();
    }

    public ObjectProperty<BigDecimal> vlripiProperty() {
        return vlripi;
    }

    public void setVlripi(BigDecimal vlripi) {
        this.vlripi.set(vlripi);
    }

    @Column(name = "BASE_PIS")
    public BigDecimal getBasepis() {
        return basepis.get();
    }

    public ObjectProperty<BigDecimal> basepisProperty() {
        return basepis;
    }

    public void setBasepis(BigDecimal basepis) {
        this.basepis.set(basepis);
    }

    @Column(name = "PIS")
    public BigDecimal getPis() {
        return pis.get();
    }

    public ObjectProperty<BigDecimal> pisProperty() {
        return pis;
    }

    public void setPis(BigDecimal pis) {
        this.pis.set(pis);
    }

    @Column(name = "COFINS")
    public BigDecimal getCofins() {
        return cofins.get();
    }

    public ObjectProperty<BigDecimal> cofinsProperty() {
        return cofins;
    }

    public void setCofins(BigDecimal cofins) {
        this.cofins.set(cofins);
    }

    @Column(name = "NATUREZA")
    public String getNatureza() {
        return natureza.get();
    }

    public StringProperty naturezaProperty() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        this.natureza.set(natureza);
    }

    @Column(name = "ICMS")
    public BigDecimal getIcms() {
        return icms.get();
    }

    public ObjectProperty<BigDecimal> icmsProperty() {
        return icms;
    }

    public void setIcms(BigDecimal icms) {
        this.icms.set(icms);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }

    public StringProperty origemProperty() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem.set(origem);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "CLAFIS")
    public String getClafis() {
        return clafis.get();
    }

    public StringProperty clafisProperty() {
        return clafis;
    }

    public void setClafis(String clafis) {
        this.clafis.set(clafis);
    }

    @Column(name = "CLATRIB")
    public String getClatrib() {
        return clatrib.get();
    }

    public StringProperty clatribProperty() {
        return clatrib;
    }

    public void setClatrib(String clatrib) {
        this.clatrib.set(clatrib);
    }

    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }

    public StringProperty codspedProperty() {
        return codsped;
    }

    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }

    @Column(name = "TRIB_IPI")
    public String getTribipi() {
        return tribipi.get();
    }

    public StringProperty tribipiProperty() {
        return tribipi;
    }

    public void setTribipi(String tribipi) {
        this.tribipi.set(tribipi);
    }

    @Column(name = "BASE_IPI")
    public BigDecimal getBaseipi() {
        return baseipi.get();
    }

    public ObjectProperty<BigDecimal> baseipiProperty() {
        return baseipi;
    }

    public void setBaseipi(BigDecimal baseipi) {
        this.baseipi.set(baseipi);
    }

    @Column(name = "BASE_ICMS")
    public BigDecimal getBaseicms() {
        return baseicms.get();
    }

    public ObjectProperty<BigDecimal> baseicmsProperty() {
        return baseicms;
    }

    public void setBaseicms(BigDecimal baseicms) {
        this.baseicms.set(baseicms);
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }

    @Column(name = "TRIB_PIS")
    public String getTribpis() {
        return tribpis.get();
    }

    public StringProperty tribpisProperty() {
        return tribpis;
    }

    public void setTribpis(String tribpis) {
        this.tribpis.set(tribpis);
    }

    @Column(name = "TRIB_COFINS")
    public String getTribcofins() {
        return tribcofins.get();
    }

    public StringProperty tribcofinsProperty() {
        return tribcofins;
    }

    public void setTribcofins(String tribcofins) {
        this.tribcofins.set(tribcofins);
    }

    @Column(name = "VAL_ICMS")
    public BigDecimal getValicms() {
        return valicms.get();
    }

    public ObjectProperty<BigDecimal> valicmsProperty() {
        return valicms;
    }

    public void setValicms(BigDecimal valicms) {
        this.valicms.set(valicms);
    }

    @Column(name = "VAL_IPI")
    public BigDecimal getValipi() {
        return valipi.get();
    }

    public ObjectProperty<BigDecimal> valipiProperty() {
        return valipi;
    }

    public void setValipi(BigDecimal valipi) {
        this.valipi.set(valipi);
    }

    @Column(name = "VAL_FRETE")
    public BigDecimal getValfrete() {
        return valfrete.get();
    }

    public ObjectProperty<BigDecimal> valfreteProperty() {
        return valfrete;
    }

    public void setValfrete(BigDecimal valfrete) {
        this.valfrete.set(valfrete);
    }

    @Column(name = "VAL_DESC")
    public BigDecimal getValdesc() {
        return valdesc.get();
    }

    public ObjectProperty<BigDecimal> valdescProperty() {
        return valdesc;
    }

    public void setValdesc(BigDecimal valdesc) {
        this.valdesc.set(valdesc);
    }

    @Column(name = "VALOR_TOT")
    public BigDecimal getValortot() {
        return valortot.get();
    }

    public ObjectProperty<BigDecimal> valortotProperty() {
        return valortot;
    }

    public void setValortot(BigDecimal valortot) {
        this.valortot.set(valortot);
    }

    @Column(name = "BASE_ST")
    public BigDecimal getBasest() {
        return basest.get();
    }

    public ObjectProperty<BigDecimal> basestProperty() {
        return basest;
    }

    public void setBasest(BigDecimal basest) {
        this.basest.set(basest);
    }

    @Column(name = "VALOR_ST")
    public BigDecimal getValorst() {
        return valorst.get();
    }

    public ObjectProperty<BigDecimal> valorstProperty() {
        return valorst;
    }

    public void setValorst(BigDecimal valorst) {
        this.valorst.set(valorst);
    }

}
