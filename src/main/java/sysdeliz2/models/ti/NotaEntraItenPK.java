package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotaEntraItenPK implements Serializable {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty serie = new SimpleStringProperty();
    private final StringProperty codcre = new SimpleStringProperty();
    private final StringProperty docto = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public NotaEntraItenPK() {
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "CODCRE")
    public String getCodcre() {
        return codcre.get();
    }

    public StringProperty codcreProperty() {
        return codcre;
    }

    public void setCodcre(String codcre) {
        this.codcre.set(codcre);
    }

    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }

    public StringProperty doctoProperty() {
        return docto;
    }

    public void setDocto(String docto) {
        this.docto.set(docto);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "SERIE")
    public String getSerie() {
        return serie.get();
    }

    public StringProperty serieProperty() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie.set(serie);
    }
}
