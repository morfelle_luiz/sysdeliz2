package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotaEntraPK implements Serializable {

    @ExibeTableView(descricao = "Número da Nota", width = 100)
    @ColunaFilter(descricao = "Nº Nota", coluna = "notafiscal")
    private final StringProperty notafiscal = new SimpleStringProperty();
    @ExibeTableView(descricao = "Série", width = 80)
    @ColunaFilter(descricao = "Série", coluna = "serie")
    private final StringProperty serie = new SimpleStringProperty();
    @ExibeTableView(descricao = "Código Fornecedor", width = 100)
    @ColunaFilter(descricao = "Cod. Fornecedor", coluna = "credor")
    private final StringProperty credor = new SimpleStringProperty();

    public NotaEntraPK() {
    }

    @Column(name = "NOTAFISCAL")
    public String getNotafiscal() {
        return notafiscal.get();
    }

    public StringProperty notafiscalProperty() {
        return notafiscal;
    }

    public void setNotafiscal(String notafiscal) {
        this.notafiscal.set(notafiscal);
    }

    @Column(name = "SERIE")
    public String getSerie() {
        return serie.get();
    }

    public StringProperty serieProperty() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie.set(serie);
    }

    @Column(name = "CREDOR")
    public String getCredor() {
        return credor.get();
    }

    public StringProperty credorProperty() {
        return credor;
    }

    public void setCredor(String credor) {
        this.credor.set(credor);
    }

    @Override
    public String toString() {
        return "Nº " + notafiscal.get() + " - " + serie.get() + " - " + credor.get();
    }
}
