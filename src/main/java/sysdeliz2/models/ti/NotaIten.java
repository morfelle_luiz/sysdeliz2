package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "NOTAITEN_001")
public class NotaIten {
    
    private final ObjectProperty<NotaItenPK> id = new SimpleObjectProperty<>(new NotaItenPK());
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> datamvto = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty tamanho = new SimpleStringProperty();
    private final StringProperty clafis = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty clatrib = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty notacliente = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> percicms = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty lote = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<Natureza> natureza = new SimpleObjectProperty<Natureza>();
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tipoit = new SimpleStringProperty();
    private final IntegerProperty ordped = new SimpleIntegerProperty(0);
    private final StringProperty origem = new SimpleStringProperty();
    private final StringProperty codsped = new SimpleStringProperty();
    private final IntegerProperty volumes = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> percred = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percst = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorst = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> basest = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tribipi = new SimpleStringProperty();
    private final StringProperty tribpis = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> percpis = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tribcofins = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> perccofins = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> baseicms = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> baseipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> basepis = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> basecofins = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percii = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> baseii = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valordif = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percdif = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorstfcp = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> qtdetrib = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty chaveref = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valfcp = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valicmsremet = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valicmsdest = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);

    private final ObjectProperty<BigDecimal> valfrete = new SimpleObjectProperty<>();
    
    public NotaIten() {
    }

    
    @EmbeddedId
    public NotaItenPK getId() {
        return id.get();
    }
    
    public ObjectProperty<NotaItenPK> idProperty() {
        return id;
    }
    
    public void setId(NotaItenPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "PERC_IPI")
    public BigDecimal getPercipi() {
        return percipi.get();
    }
    
    public ObjectProperty<BigDecimal> percipiProperty() {
        return percipi;
    }
    
    public void setPercipi(BigDecimal percipi) {
        this.percipi.set(percipi);
    }
    
    @Column(name = "DATA_MVTO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDatamvto() {
        return datamvto.get();
    }
    
    public ObjectProperty<LocalDate> datamvtoProperty() {
        return datamvto;
    }
    
    public void setDatamvto(LocalDate datamvto) {
        this.datamvto.set(datamvto);
    }
    
    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }
    
    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }
    
    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @OneToOne
    @JoinColumn(name = "NATUREZA")
    public Natureza getNatureza() {
        return natureza.get();
    }
    
    public ObjectProperty<Natureza> naturezaProperty() {
        return natureza;
    }
    
    public void setNatureza(Natureza natureza) {
        this.natureza.set(natureza);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }
    
    public StringProperty tamanhoProperty() {
        return tamanho;
    }
    
    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }
    
    @Column(name = "CLA_FIS")
    public String getClafis() {
        return clafis.get();
    }
    
    public StringProperty clafisProperty() {
        return clafis;
    }
    
    public void setClafis(String clafis) {
        this.clafis.set(clafis);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "CLA_TRIB")
    public String getClatrib() {
        return clatrib.get();
    }
    
    public StringProperty clatribProperty() {
        return clatrib;
    }
    
    public void setClatrib(String clatrib) {
        this.clatrib.set(clatrib);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name = "NOTA_CLIENTE")
    public String getNotacliente() {
        return notacliente.get();
    }
    
    public StringProperty notaclienteProperty() {
        return notacliente;
    }
    
    public void setNotacliente(String notacliente) {
        this.notacliente.set(notacliente);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "PERC_ICMS")
    public BigDecimal getPercicms() {
        return percicms.get();
    }
    
    public ObjectProperty<BigDecimal> percicmsProperty() {
        return percicms;
    }
    
    public void setPercicms(BigDecimal percicms) {
        this.percicms.set(percicms);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }
    
    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }
    
    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }
    
    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }
    
    @Column(name = "TIPO_IT")
    public String getTipoit() {
        return tipoit.get();
    }
    
    public StringProperty tipoitProperty() {
        return tipoit;
    }
    
    public void setTipoit(String tipoit) {
        this.tipoit.set(tipoit);
    }
    
    @Column(name = "ORD_PED")
    public Integer getOrdped() {
        return ordped.get();
    }
    
    public IntegerProperty ordpedProperty() {
        return ordped;
    }
    
    public void setOrdped(Integer ordped) {
        this.ordped.set(ordped);
    }
    
    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }
    
    public StringProperty origemProperty() {
        return origem;
    }
    
    public void setOrigem(String origem) {
        this.origem.set(origem);
    }
    
    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }
    
    public StringProperty codspedProperty() {
        return codsped;
    }
    
    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }
    
    @Column(name = "VOLUMES")
    public Integer getVolumes() {
        return volumes.get();
    }
    
    public IntegerProperty volumesProperty() {
        return volumes;
    }
    
    public void setVolumes(Integer volumes) {
        this.volumes.set(volumes);
    }
    
    @Column(name = "PERC_RED")
    public BigDecimal getPercred() {
        return percred.get();
    }
    
    public ObjectProperty<BigDecimal> percredProperty() {
        return percred;
    }
    
    public void setPercred(BigDecimal percred) {
        this.percred.set(percred);
    }
    
    @Column(name = "PERC_ST")
    public BigDecimal getPercst() {
        return percst.get();
    }
    
    public ObjectProperty<BigDecimal> percstProperty() {
        return percst;
    }
    
    public void setPercst(BigDecimal percst) {
        this.percst.set(percst);
    }
    
    @Column(name = "VALOR_ST")
    public BigDecimal getValorst() {
        return valorst.get();
    }
    
    public ObjectProperty<BigDecimal> valorstProperty() {
        return valorst;
    }
    
    public void setValorst(BigDecimal valorst) {
        this.valorst.set(valorst);
    }
    
    @Column(name = "BASE_ST")
    public BigDecimal getBasest() {
        return basest.get();
    }
    
    public ObjectProperty<BigDecimal> basestProperty() {
        return basest;
    }
    
    public void setBasest(BigDecimal basest) {
        this.basest.set(basest);
    }
    
    @Column(name = "TRIB_IPI")
    public String getTribipi() {
        return tribipi.get();
    }
    
    public StringProperty tribipiProperty() {
        return tribipi;
    }
    
    public void setTribipi(String tribipi) {
        this.tribipi.set(tribipi);
    }
    
    @Column(name = "TRIB_PIS")
    public String getTribpis() {
        return tribpis.get();
    }
    
    public StringProperty tribpisProperty() {
        return tribpis;
    }
    
    public void setTribpis(String tribpis) {
        this.tribpis.set(tribpis);
    }
    
    @Column(name = "PERC_PIS")
    public BigDecimal getPercpis() {
        return percpis.get();
    }
    
    public ObjectProperty<BigDecimal> percpisProperty() {
        return percpis;
    }
    
    public void setPercpis(BigDecimal percpis) {
        this.percpis.set(percpis);
    }
    
    @Column(name = "TRIB_COFINS")
    public String getTribcofins() {
        return tribcofins.get();
    }
    
    public StringProperty tribcofinsProperty() {
        return tribcofins;
    }
    
    public void setTribcofins(String tribcofins) {
        this.tribcofins.set(tribcofins);
    }
    
    @Column(name = "PERC_COFINS")
    public BigDecimal getPerccofins() {
        return perccofins.get();
    }
    
    public ObjectProperty<BigDecimal> perccofinsProperty() {
        return perccofins;
    }
    
    public void setPerccofins(BigDecimal perccofins) {
        this.perccofins.set(perccofins);
    }
    
    @Column(name = "BASE_ICMS")
    public BigDecimal getBaseicms() {
        return baseicms.get();
    }
    
    public ObjectProperty<BigDecimal> baseicmsProperty() {
        return baseicms;
    }
    
    public void setBaseicms(BigDecimal baseicms) {
        this.baseicms.set(baseicms);
    }
    
    @Column(name = "BASE_IPI")
    public BigDecimal getBaseipi() {
        return baseipi.get();
    }
    
    public ObjectProperty<BigDecimal> baseipiProperty() {
        return baseipi;
    }
    
    public void setBaseipi(BigDecimal baseipi) {
        this.baseipi.set(baseipi);
    }
    
    @Column(name = "BASE_PIS")
    public BigDecimal getBasepis() {
        return basepis.get();
    }
    
    public ObjectProperty<BigDecimal> basepisProperty() {
        return basepis;
    }
    
    public void setBasepis(BigDecimal basepis) {
        this.basepis.set(basepis);
    }
    
    @Column(name = "BASE_COFINS")
    public BigDecimal getBasecofins() {
        return basecofins.get();
    }
    
    public ObjectProperty<BigDecimal> basecofinsProperty() {
        return basecofins;
    }
    
    public void setBasecofins(BigDecimal basecofins) {
        this.basecofins.set(basecofins);
    }
    
    @Column(name = "PERC_II")
    public BigDecimal getPercii() {
        return percii.get();
    }
    
    public ObjectProperty<BigDecimal> perciiProperty() {
        return percii;
    }
    
    public void setPercii(BigDecimal percii) {
        this.percii.set(percii);
    }
    
    @Column(name = "BASE_II")
    public BigDecimal getBaseii() {
        return baseii.get();
    }
    
    public ObjectProperty<BigDecimal> baseiiProperty() {
        return baseii;
    }
    
    public void setBaseii(BigDecimal baseii) {
        this.baseii.set(baseii);
    }
    
    @Column(name = "VALOR_DIF")
    public BigDecimal getValordif() {
        return valordif.get();
    }
    
    public ObjectProperty<BigDecimal> valordifProperty() {
        return valordif;
    }
    
    public void setValordif(BigDecimal valordif) {
        this.valordif.set(valordif);
    }
    
    @Column(name = "PERC_DIF")
    public BigDecimal getPercdif() {
        return percdif.get();
    }
    
    public ObjectProperty<BigDecimal> percdifProperty() {
        return percdif;
    }
    
    public void setPercdif(BigDecimal percdif) {
        this.percdif.set(percdif);
    }
    
    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }
    
    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }
    
    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }
    
    @Column(name = "VALOR_ST_FCP")
    public BigDecimal getValorstfcp() {
        return valorstfcp.get();
    }
    
    public ObjectProperty<BigDecimal> valorstfcpProperty() {
        return valorstfcp;
    }
    
    public void setValorstfcp(BigDecimal valorstfcp) {
        this.valorstfcp.set(valorstfcp);
    }
    
    @Column(name = "QTDE_TRIB")
    public BigDecimal getQtdetrib() {
        return qtdetrib.get();
    }
    
    public ObjectProperty<BigDecimal> qtdetribProperty() {
        return qtdetrib;
    }
    
    public void setQtdetrib(BigDecimal qtdetrib) {
        this.qtdetrib.set(qtdetrib);
    }
    
    @Column(name = "CHAVE_REF")
    public String getChaveref() {
        return chaveref.get();
    }
    
    public StringProperty chaverefProperty() {
        return chaveref;
    }
    
    public void setChaveref(String chaveref) {
        this.chaveref.set(chaveref);
    }
    
    @Column(name = "VAL_FCP")
    public BigDecimal getValfcp() {
        return valfcp.get();
    }
    
    public ObjectProperty<BigDecimal> valfcpProperty() {
        return valfcp;
    }
    
    public void setValfcp(BigDecimal valfcp) {
        this.valfcp.set(valfcp);
    }
    
    @Column(name = "VAL_ICMS_REMET")
    public BigDecimal getValicmsremet() {
        return valicmsremet.get();
    }
    
    public ObjectProperty<BigDecimal> valicmsremetProperty() {
        return valicmsremet;
    }
    
    public void setValicmsremet(BigDecimal valicmsremet) {
        this.valicmsremet.set(valicmsremet);
    }
    
    @Column(name = "VAL_ICMS_DEST")
    public BigDecimal getValicmsdest() {
        return valicmsdest.get();
    }
    
    public ObjectProperty<BigDecimal> valicmsdestProperty() {
        return valicmsdest;
    }
    
    public void setValicmsdest(BigDecimal valicmsdest) {
        this.valicmsdest.set(valicmsdest);
    }

    @Transient
    public BigDecimal getValfrete() {
        return valfrete.get();
    }

    public ObjectProperty<BigDecimal> valfreteProperty() {
        return valfrete;
    }

    public void setValfrete(BigDecimal valfrete) {
        this.valfrete.set(valfrete);
    }
}