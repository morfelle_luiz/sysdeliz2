package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotaItenPK implements Serializable {
    
    private final StringProperty fatura = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty(0);
    private final StringProperty empenho = new SimpleStringProperty();
    
    public NotaItenPK() {
    }
    
    @Column(name = "EMPENHO")
    public String getEmpenho() {
        return empenho.get();
    }
    
    public StringProperty empenhoProperty() {
        return empenho;
    }
    
    public void setEmpenho(String empenho) {
        this.empenho.set(empenho);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
}