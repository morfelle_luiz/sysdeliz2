package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "NOTA_NUMERO_001")
public class NotaNumero {
    
    private final ObjectProperty<NotaNumeroPK> id = new SimpleObjectProperty<>();
    
    public NotaNumero() {
    }
    
    public NotaNumero(String numero, String serie) {
        this.id.set(new NotaNumeroPK(numero, serie));
    }
    
    @EmbeddedId
    public NotaNumeroPK getId() {
        return id.get();
    }
    
    public ObjectProperty<NotaNumeroPK> idProperty() {
        return id;
    }
    
    public void setId(NotaNumeroPK id) {
        this.id.set(id);
    }
}