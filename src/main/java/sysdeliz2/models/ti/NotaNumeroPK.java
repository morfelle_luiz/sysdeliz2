package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotaNumeroPK implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty serie = new SimpleStringProperty();
    
    public NotaNumeroPK() {
    }
    
    public NotaNumeroPK(String numero, String serie) {
        this.numero.set(numero);
        this.serie.set(serie);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "SERIE")
    public String getSerie() {
        return serie.get();
    }
    
    public StringProperty serieProperty() {
        return serie;
    }
    
    public void setSerie(String serie) {
        this.serie.set(serie);
    }
}