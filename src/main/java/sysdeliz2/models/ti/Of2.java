package sysdeliz2.models.ti;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdCorOf;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author lima.joao
 * @since 12/09/2019
 */
@Entity
@Table(name = "OF1_001")
public class Of2 extends BasicModel implements Serializable {
    
    //<editor-fold desc="Properties">
    private final StringProperty numero = new SimpleStringProperty(this, "numero");
    
    private final IntegerProperty impOf = new SimpleIntegerProperty(this, "impOf");
    
    private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>(this, "dtInicio");
    private final ObjectProperty<LocalDate> dtFinal = new SimpleObjectProperty<>(this, "dtFinal");
    
    private final StringProperty periodo = new SimpleStringProperty(this, "periodo");
    private final StringProperty pedido = new SimpleStringProperty(this, "pedido");
    
    private final ObjectProperty<VSdDadosProduto> produto = new SimpleObjectProperty<>(this, "produto");
    
    private final ObjectProperty<LocalDate> dtPrev = new SimpleObjectProperty<>(this, "dtPrev");
    
    private final StringProperty codCli = new SimpleStringProperty(this, "codCli");
    
    private final ObjectProperty<BigDecimal> ativos = new SimpleObjectProperty<>(this, "ativos");
    
    private final StringProperty consumo = new SimpleStringProperty(this, "consumo");
    private final StringProperty parte = new SimpleStringProperty(this, "parte");
    private final StringProperty observacao = new SimpleStringProperty(this, "observacao");
    private final StringProperty facQtde = new SimpleStringProperty(this, "facQtde");
    
    private final ObjectProperty<BigDecimal> qtdeB = new SimpleObjectProperty<>(this, "qtdeB");
    private final ObjectProperty<BigDecimal> pesoBr = new SimpleObjectProperty<>(this, "pesoBr");
    private final ObjectProperty<BigDecimal> pesoLiq = new SimpleObjectProperty<>(this, "pesoLiq");
    
    private final StringProperty maquina = new SimpleStringProperty(this, "maquina");
    
    private final ObjectProperty<BigDecimal> perc = new SimpleObjectProperty<>(this, "perc");
    
    private final StringProperty facPreco = new SimpleStringProperty(this, "facPreco");
    private final StringProperty programacao = new SimpleStringProperty(this, "programacao");
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
    
    private final IntegerProperty sequencial = new SimpleIntegerProperty(this, "sequencial");
    private final StringProperty tabela = new SimpleStringProperty(this, "tabela");
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    
    private final ListProperty<VSdCorOf> itensObservable = new SimpleListProperty<>();
    private List<VSdCorOf> itens;
    //</editor-fold>
    
    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        setCodigoFilter(numero);
        this.numero.set(numero);
    }
    
    @Column(name = "IMPOF")
    public int getImpOf() {
        return impOf.get();
    }
    
    public IntegerProperty impOfProperty() {
        return impOf;
    }
    
    public void setImpOf(int impOf) {
        this.impOf.set(impOf);
    }
    
    @Column(name = "DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtInicio() {
        return dtInicio.get();
    }
    
    public ObjectProperty<LocalDate> dtInicioProperty() {
        return dtInicio;
    }
    
    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio.set(dtInicio);
    }
    
    @Column(name = "DT_FINAL")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtFinal() {
        return dtFinal.get();
    }
    
    public ObjectProperty<LocalDate> dtFinalProperty() {
        return dtFinal;
    }
    
    public void setDtFinal(LocalDate dtFinal) {
        this.dtFinal.set(dtFinal);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<VSdDadosProduto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(VSdDadosProduto produto) {
        this.produto.set(produto);
    }
    
    @Column(name = "DT_PREV")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtPrev() {
        return dtPrev.get();
    }
    
    public ObjectProperty<LocalDate> dtPrevProperty() {
        return dtPrev;
    }
    
    public void setDtPrev(LocalDate dtPrev) {
        this.dtPrev.set(dtPrev);
    }
    
    @Column(name = "CODCLI")
    public String getCodCli() {
        return codCli.get();
    }
    
    public StringProperty codCliProperty() {
        return codCli;
    }
    
    public void setCodCli(String codCli) {
        this.codCli.set(codCli);
    }
    
    @Column(name = "ATIVOS")
    public BigDecimal getAtivos() {
        return ativos.get();
    }
    
    public ObjectProperty<BigDecimal> ativosProperty() {
        return ativos;
    }
    
    public void setAtivos(BigDecimal ativos) {
        this.ativos.set(ativos);
    }
    
    @Column(name = "CONSUMO")
    public String getConsumo() {
        return consumo.get();
    }
    
    public StringProperty consumoProperty() {
        return consumo;
    }
    
    public void setConsumo(String consumo) {
        this.consumo.set(consumo);
    }
    
    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }
    
    public StringProperty parteProperty() {
        return parte;
    }
    
    public void setParte(String parte) {
        this.parte.set(parte);
    }
    
    @Lob
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "FAC_QTDE")
    public String getFacQtde() {
        return facQtde.get();
    }
    
    public StringProperty facQtdeProperty() {
        return facQtde;
    }
    
    public void setFacQtde(String facQtde) {
        this.facQtde.set(facQtde);
    }
    
    @Column(name = "QTDE_B")
    public BigDecimal getQtdeB() {
        return qtdeB.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeBProperty() {
        return qtdeB;
    }
    
    public void setQtdeB(BigDecimal qtdeB) {
        this.qtdeB.set(qtdeB);
    }
    
    @Column(name = "PESO_BR")
    public BigDecimal getPesoBr() {
        return pesoBr.get();
    }
    
    public ObjectProperty<BigDecimal> pesoBrProperty() {
        return pesoBr;
    }
    
    public void setPesoBr(BigDecimal pesoBr) {
        this.pesoBr.set(pesoBr);
    }
    
    @Column(name = "PESO_LIQ")
    public BigDecimal getPesoLiq() {
        return pesoLiq.get();
    }
    
    public ObjectProperty<BigDecimal> pesoLiqProperty() {
        return pesoLiq;
    }
    
    public void setPesoLiq(BigDecimal pesoLiq) {
        this.pesoLiq.set(pesoLiq);
    }
    
    @Column(name = "MAQUINA")
    public String getMaquina() {
        return maquina.get();
    }
    
    public StringProperty maquinaProperty() {
        return maquina;
    }
    
    public void setMaquina(String maquina) {
        this.maquina.set(maquina);
    }
    
    @Column(name = "PERC")
    public BigDecimal getPerc() {
        return perc.get();
    }
    
    public ObjectProperty<BigDecimal> percProperty() {
        return perc;
    }
    
    public void setPerc(BigDecimal perc) {
        this.perc.set(perc);
    }
    
    @Column(name = "FAC_PRECO")
    public String getFacPreco() {
        return facPreco.get();
    }
    
    public StringProperty facPrecoProperty() {
        return facPreco;
    }
    
    public void setFacPreco(String facPreco) {
        this.facPreco.set(facPreco);
    }
    
    @Column(name = "PROGRAMACAO")
    public String getProgramacao() {
        return programacao.get();
    }
    
    public StringProperty programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(String programacao) {
        this.programacao.set(programacao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "SEQUENCIAL")
    public int getSequencial() {
        return sequencial.get();
    }
    
    public IntegerProperty sequencialProperty() {
        return sequencial;
    }
    
    public void setSequencial(int sequencial) {
        this.sequencial.set(sequencial);
    }
    
    @Column(name = "TABELA")
    public String getTabela() {
        return tabela.get();
    }
    
    public StringProperty tabelaProperty() {
        return tabela;
    }
    
    public void setTabela(String tabela) {
        this.tabela.set(tabela);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "NUMERO")
    @OrderBy("COR")
    public List<VSdCorOf> getItens() {
        return itens;
    }

    public void setItens(List<VSdCorOf> value) {
        this.itens = value;
    }

    @Transient
    public ObservableList<VSdCorOf> getItensObservable() {
        itensObservable.set(FXCollections.observableList(this.itens));
        return itensObservable.get();
    }

    public ListProperty<VSdCorOf> itensObservableProperty() {
        itensObservable.set(FXCollections.observableList(this.itens));
        return itensObservable;
    }

    public void setItensObservable(ObservableList<VSdCorOf> itensObservable) {
        this.itensObservable.set(itensObservable);
    }
}
