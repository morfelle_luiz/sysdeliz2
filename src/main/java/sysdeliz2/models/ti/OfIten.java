package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 12/09/2019
 */

@Entity
@Table(name = "OF_ITEN_001")
public class OfIten implements Serializable {

    private final ObjectProperty<OfItenId> id = new SimpleObjectProperty<>(this, "ofItenId");
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>(this, "qtde", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> qtdeB = new SimpleObjectProperty<>(this, "qtdeB", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> qtdeC = new SimpleObjectProperty<>(this, "qtdeC", BigDecimal.ZERO);

    public OfIten() {

    }

    public OfIten(Cor cor, String tam, String numero) {
        setId(new OfItenId(cor, tam, numero));
    }

    @Id
    public OfItenId getId() {
        return id.get();
    }

    public ObjectProperty<OfItenId> idProperty() {
        return id;
    }

    public void setId(OfItenId id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "QTDE_B")
    public BigDecimal getQtdeB() {
        return qtdeB.get();
    }

    public ObjectProperty<BigDecimal> qtdeBProperty() {
        return qtdeB;
    }

    public void setQtdeB(BigDecimal qtdeB) {
        this.qtdeB.set(qtdeB);
    }

    @Column(name = "QTDE_C")
    public BigDecimal getQtdeC() {
        return qtdeC.get();
    }

    public ObjectProperty<BigDecimal> qtdeCProperty() {
        return qtdeC;
    }

    public void setQtdeC(BigDecimal qtdeC) {
        this.qtdeC.set(qtdeC);
    }

//    private final MapProperty<String, Integer> tamanhos = new SimpleMapProperty<String, Integer>();
//    private final MapProperty<Integer, String> headersTamanhos = new SimpleMapProperty<Integer, String>();
//
//    public final ObservableMap<String, Integer> getTamanhos() {
//        return tamanhos.get();
//    }
//
//    public final void setTamanhos(ObservableMap<String, Integer> value) {
//        tamanhos.set(value);
//    }
//
//    public MapProperty<String, Integer> tamanhosProperty() {
//        return tamanhos;
//    }
//
//    public final ObservableMap<Integer, String> getHeadersTamanhos() {
//        return headersTamanhos.get();
//    }
//
//    public final void setHeadersTamanhos(ObservableMap<Integer, String> value) {
//        headersTamanhos.set(value);
//    }
//
//    public MapProperty<Integer, String> headersTamanhosProperty() {
//        return headersTamanhos;
//    }

}
