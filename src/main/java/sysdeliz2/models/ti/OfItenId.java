package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 12/09/2019 16:51
 */
@Embeddable
public class OfItenId implements Serializable {

    private final StringProperty numero = new SimpleStringProperty(this, "numero");
    private final StringProperty tamanho = new SimpleStringProperty(this, "tamanho");
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>(this, "cor");

    public OfItenId() {
    }

    public OfItenId(Cor cor, String tam, String numero) {
        setCor(cor);
        setTamanho(tam);
        setNumero(numero);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "TAM")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }

    public ObjectProperty<Cor> corProperty() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor.set(cor);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
