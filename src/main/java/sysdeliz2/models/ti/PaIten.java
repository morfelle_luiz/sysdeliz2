package sysdeliz2.models.ti;

import javafx.beans.property.*;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.sysdeliz.Inventario.SdGradeItemInventario;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Entity
@Table(name = "PA_ITEN_001")
public class PaIten {

    private final ObjectProperty<PaItenPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> quantidade = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percentual = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty barra = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdebruto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final BooleanProperty ativo = new SimpleBooleanProperty(true);
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty composicao = new SimpleStringProperty();
    private final StringProperty barracli = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdemin = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty md5 = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty idecom = new SimpleStringProperty();

    public PaIten() {
    }

    public PaIten(int qtde, String tam, String codigo, String cor) {
        this.id.set(new PaItenPK(tam, cor, codigo));
        this.quantidade.set(new BigDecimal(qtde));
    }

    // Importação Produtos B2C
    public PaIten(int qtde, String tam, String codigo, String cor, String deposito) {
        this.id.set(new PaItenPK(tam, cor, codigo, deposito));
        this.quantidade.set(new BigDecimal(qtde));
        VSdDadosProdutoBarra barraProduto = getBarraProduto(codigo, cor, tam);
        this.barra28.set(barraProduto.getBarra28());
        this.barra.set(barraProduto.getBarra());
        this.local.set(barraProduto.getLocal());
        this.ordem.set(getMaxOrdem(codigo, cor, tam, "0023"));
    }

    // Entrada PA
    public PaIten(SdItemCaixaPA itemCaixa, String deposito) {
        this.id.set(new PaItenPK(itemCaixa, deposito));
        this.quantidade.set(new BigDecimal(itemCaixa.getQtde()));
        VSdDadosProdutoBarra barraProduto = getBarraProduto(itemCaixa.getProduto(), itemCaixa.getCor(), itemCaixa.getTam());
        AtomicReference<String> local = new AtomicReference<>(barraProduto.getLocal());
        if (barraProduto.getLocal().equals("AINSP")) getBarraProduto(itemCaixa.getProduto(), itemCaixa.getCor()).stream().filter(it -> !it.getLocal().equals("AINSP")).findFirst().ifPresent(eb -> local.set(eb.getLocal()));
        this.barra28.set(barraProduto.getBarra28());
        this.barra.set(barraProduto.getBarra());
        this.local.set(local.get());
        this.ordem.set(getMaxOrdem(itemCaixa.getProduto(), itemCaixa.getCor(), itemCaixa.getTam(), itemCaixa.getCaixa().isSegunda() ? "0007" : "0005"));
    }

    // Inventário
    public PaIten(SdGradeItemInventario itemGrade, String deposito) {
        this.id.set(new PaItenPK(itemGrade, deposito));
        VSdDadosProdutoBarra barraProduto = getBarraProduto(itemGrade.getItem().getCodigo().getCodigo(), itemGrade.getItem().getCodigo().getCor(), itemGrade.getTam());
        this.barra28.set(barraProduto.getBarra28());
        this.barra.set(barraProduto.getBarra());
        this.local.set(barraProduto.getLocal());
        this.ordem.set(getMaxOrdem(itemGrade.getItem().getCodigo().getCodigo(), itemGrade.getItem().getCodigo().getCor(), itemGrade.getTam(), deposito));
    }

    public PaIten(String cor) {
        this.id.set(new PaItenPK(cor));
    }

    @EmbeddedId
    public PaItenPK getId() {
        return id.get();
    }

    public ObjectProperty<PaItenPK> idProperty() {
        return id;
    }

    public void setId(PaItenPK id) {
        this.id.set(id);
    }

    @Column(name = "QUANTIDADE")
    public BigDecimal getQuantidade() {
        return quantidade.get();
    }

    public ObjectProperty<BigDecimal> quantidadeProperty() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade.set(quantidade);
    }

    @Column(name = "PERCENTUAL")
    public BigDecimal getPercentual() {
        return percentual.get();
    }

    public ObjectProperty<BigDecimal> percentualProperty() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual.set(percentual);
    }

    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }

    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    @Column(name = "QTDE_BRUTO")
    public BigDecimal getQtdebruto() {
        return qtdebruto.get();
    }

    public ObjectProperty<BigDecimal> qtdebrutoProperty() {
        return qtdebruto;
    }

    public void setQtdebruto(BigDecimal qtdebruto) {
        this.qtdebruto.set(qtdebruto);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "COMPOSICAO")
    public String getComposicao() {
        return composicao.get();
    }

    public StringProperty composicaoProperty() {
        return composicao;
    }

    public void setComposicao(String composicao) {
        this.composicao.set(composicao);
    }

    @Column(name = "BARRACLI")
    public String getBarracli() {
        return barracli.get();
    }

    public StringProperty barracliProperty() {
        return barracli;
    }

    public void setBarracli(String barracli) {
        this.barracli.set(barracli);
    }

    @Column(name = "QTDE_MIN")
    public BigDecimal getQtdemin() {
        return qtdemin.get();
    }

    public ObjectProperty<BigDecimal> qtdeminProperty() {
        return qtdemin;
    }

    public void setQtdemin(BigDecimal qtdemin) {
        this.qtdemin.set(qtdemin);
    }

    @Column(name = "MD5")
    public String getMd5() {
        return md5.get();
    }

    public StringProperty md5Property() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5.set(md5);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "ID_ECOM")
    public String getIdecom() {
        return idecom.get();
    }

    public StringProperty idecomProperty() {
        return idecom;
    }

    public void setIdecom(String idecom) {
        this.idecom.set(idecom);
    }

    @Nullable
    private VSdDadosProdutoBarra getBarraProduto(String codigo, String cor, String tam) {
        return new FluentDao()
                .selectFrom(VSdDadosProdutoBarra.class)
                .where(it -> it
                        .equal("codigo", codigo)
                        .equal("cor", cor)
                        .equal("tam", tam)
                ).singleResult();
    }

    private List<VSdDadosProdutoBarra> getBarraProduto(String codigo, String cor) {
        return (List<VSdDadosProdutoBarra>) new FluentDao()
                .selectFrom(VSdDadosProdutoBarra.class)
                .where(it -> it
                        .equal("codigo", codigo)
                        .equal("cor", cor)
                ).resultList();
    }

    public int getMaxOrdem(String codigo, String cor, String tam, String deposito) {
        List<PaIten> itensPAIten = ((List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it
                .equal("id.codigo", codigo)
                .equal("id.tam", cor)
                .equal("id.cor", tam)
                .equal("id.deposito", deposito)
        ).resultList());

        if (itensPAIten != null && itensPAIten.stream().mapToInt(PaIten::getOrdem).max().isPresent()) {
            return itensPAIten.stream().mapToInt(PaIten::getOrdem).max().getAsInt() + 1;
        } else {
            return 1;
        }
    }

}