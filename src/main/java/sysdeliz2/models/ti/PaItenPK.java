package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.sysdeliz.Inventario.SdGradeItemInventario;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PaItenPK implements Serializable {

    private final StringProperty tipo = new SimpleStringProperty("1");
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty("000000");
    private final StringProperty deposito = new SimpleStringProperty();

    public PaItenPK() {
    }

    public PaItenPK(String tam, String cor, String codigo) {
        this.tam.set(tam);
        this.cor.set(cor);
        this.codigo.set(codigo);
    }

    public PaItenPK(String tam, String cor, String codigo, String deposito) {
        this.tam.set(tam);
        this.cor.set(cor);
        this.codigo.set(codigo);
        this.deposito.set(deposito);
    }

    public PaItenPK(SdItemCaixaPA itemCaixa, String deposito) {
        this.tam.set(itemCaixa.getTam());
        this.cor.set(itemCaixa.getCor());
        this.codigo.set(itemCaixa.getProduto());
        this.deposito.set(deposito);
    }

    public PaItenPK(SdGradeItemInventario itemGrade, String deposito) {
        this.tam.set(itemGrade.getTam());
        this.cor.set(itemGrade.getItem().getCodigo().getCor());
        this.codigo.set(itemGrade.getItem().getCodigo().getCodigo());
        this.deposito.set(deposito);
    }

    public PaItenPK(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
}