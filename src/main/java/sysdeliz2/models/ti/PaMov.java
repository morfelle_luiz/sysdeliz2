package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.sysdeliz.Inventario.SdGradeItemInventario;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdItemSolicitacaoExp;
import sysdeliz2.utils.Globals;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "PA_MOV_001")
@Entity
public class PaMov {

    private final ObjectProperty<PaMovPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtmvto = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tipo = new SimpleStringProperty("1");
    private final StringProperty turno = new SimpleStringProperty("1");
    private final StringProperty tipbai = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty tpmov = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty codfun = new SimpleStringProperty();
    private final StringProperty md5 = new SimpleStringProperty();
    private final ObjectProperty<LocalDateTime> hora = new SimpleObjectProperty<>(LocalDateTime.now());

    public PaMov() {
    }

    public PaMov(SdItemCaixaPA itemCaixa, String usuario, String deposito, String tpMov) {
        this.id.set(new PaMovPK(itemCaixa.getProduto(), itemCaixa.getCor(), itemCaixa.getTam(), deposito, "E"));
        this.qtde.set(new BigDecimal(itemCaixa.getQtde()));
        this.tipbai.set("3");
        this.descricao.set(usuario);
        this.preco.set(getCustoProduto(itemCaixa.getProduto()));
        this.custo.set(getPreco().multiply(getQtde()));
        this.tpmov.set(tpMov);
        this.observacao.set("Lote: " + itemCaixa.getCaixa().getItemlote().getLote().getId() + " - Caixa: " + itemCaixa.getCaixa().getId());
        this.codfun.set("00037");
    }

    public PaMov(SdGradeItemInventario itemGrade, String deposito, BigDecimal qtde, String operacao, String observacao) {
        CustoProduto custo = new FluentDao().selectFrom(CustoProduto.class).where(it -> it.equal("codigo", itemGrade.getItem().getCodigo().getCodigo())).singleResult();
        this.id.set(new PaMovPK(itemGrade.getItem().getCodigo().getCodigo(), itemGrade.getItem().getCodigo().getCor(), itemGrade.getTam(), deposito, operacao));
        this.qtde.set(qtde);
        this.preco.set(getCustoProduto(itemGrade.getItem().getCodigo().getCodigo()));
        this.custo.set(getPreco().multiply(getQtde()).setScale(2, RoundingMode.HALF_UP));
        this.tipbai.set("0");
        this.descricao.set(Globals.getNomeUsuario());
        this.tpmov.set("MN");
        this.observacao.set(observacao);
        this.codfun.set("00038");
    }

    public PaMov(SdItemSolicitacaoExp itemSolicitacao, String deposito, String operacao, String observacao) {
        this.id.set(new PaMovPK(itemSolicitacao.getProduto().getCodigo(), itemSolicitacao.getProduto().getCor(), itemSolicitacao.getProduto().getTam(), deposito, operacao));
        this.qtde.set(new BigDecimal(itemSolicitacao.getQtde()));
        this.preco.set(getCustoProduto(itemSolicitacao.getProduto().getCodigo()));
        this.custo.set(getPreco().multiply(getQtde()));
        this.tipbai.set("3");
        this.descricao.set(Globals.getNomeUsuario());
        this.tpmov.set("MN");
        this.observacao.set(observacao);
        this.codfun.set("00037");
    }

    @EmbeddedId
    public PaMovPK getId() {
        return id.get();
    }

    public ObjectProperty<PaMovPK> idProperty() {
        return id;
    }

    public void setId(PaMovPK id) {
        this.id.set(id);
    }

    @Column(name = "DT_MVTO")
    public LocalDate getDtmvto() {
        return dtmvto.get();
    }

    public ObjectProperty<LocalDate> dtmvtoProperty() {
        return dtmvto;
    }

    public void setDtmvto(LocalDate dtmvto) {
        this.dtmvto.set(dtmvto);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "TURNO")
    public String getTurno() {
        return turno.get();
    }

    public StringProperty turnoProperty() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno.set(turno);
    }

    @Column(name = "TIP_BAI")
    public String getTipbai() {
        return tipbai.get();
    }

    public StringProperty tipbaiProperty() {
        return tipbai;
    }

    public void setTipbai(String tipbai) {
        this.tipbai.set(tipbai);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "TP_MOV")
    public String getTpmov() {
        return tpmov.get();
    }

    public StringProperty tpmovProperty() {
        return tpmov;
    }

    public void setTpmov(String tpmov) {
        this.tpmov.set(tpmov);
    }

    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "CODFUN")
    public String getCodfun() {
        return codfun.get();
    }

    public StringProperty codfunProperty() {
        return codfun;
    }

    public void setCodfun(String codfun) {
        this.codfun.set(codfun);
    }

    @Column(name = "MD5")
    public String getMd5() {
        return md5.get();
    }

    public StringProperty md5Property() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5.set(md5);
    }

    @Column(name = "HORA")
    public LocalDateTime getHora() {
        return hora.get();
    }

    public ObjectProperty<LocalDateTime> horaProperty() {
        return hora;
    }

    public void setHora(LocalDateTime hora) {
        this.hora.set(hora);
    }

    private BigDecimal getCustoProduto(String codigo) {
        CustoProduto custo = new FluentDao().selectFrom(CustoProduto.class).where(it -> it.equal("codigo", codigo)).singleResult();
        return custo == null ? BigDecimal.ZERO : custo.getCusto().setScale(2, RoundingMode.HALF_UP);
    }
}