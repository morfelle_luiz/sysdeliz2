package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.Globals;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PaMovPK implements Serializable {

    private final StringProperty operacao = new SimpleStringProperty();
    private final StringProperty tamanho = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty numdocto = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty("000000");
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public PaMovPK() {
    }

    public PaMovPK(String produto, String cor, String tam, String deposito, String operacao) {
        this.operacao.set(operacao);
        this.tamanho.set(tam);
        this.cor.set(cor);
        this.numdocto.set(Globals.getProximoCodigo("PA_MOV","NUM_DOCTO"));
        this.codigo.set(produto);
        this.deposito.set(deposito);
    }

    @Column(name = "OPERACAO")
    public String getOperacao() {
        return operacao.get();
    }

    public StringProperty operacaoProperty() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao.set(operacao);
    }

    @Column(name = "TAMANHO")
    public String getTamanho() {
        return tamanho.get();
    }

    public StringProperty tamanhoProperty() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho.set(tamanho);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "NUM_DOCTO")
    public String getNumdocto() {
        return numdocto.get();
    }

    public StringProperty numdoctoProperty() {
        return numdocto;
    }

    public void setNumdocto(String numdocto) {
        this.numdocto.set(numdocto);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }

}