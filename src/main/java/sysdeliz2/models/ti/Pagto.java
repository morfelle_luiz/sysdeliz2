package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.financeiro.SdItemContratoFidic;
import sysdeliz2.utils.Globals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "PAGTO_001")
public class Pagto implements Serializable {
    private final ObjectProperty<BigDecimal> taxa = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> icm = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> ipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> dataorig = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<LocalDate> dtemi = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<LocalDate> dtven = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<LocalDate> dtatual = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final ObjectProperty<BigDecimal> valororig = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valicm = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valpag = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> juros = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty bloqueio = new SimpleStringProperty("N");
    private final StringProperty tipo = new SimpleStringProperty(null);
    private final StringProperty historico = new SimpleStringProperty();
    private final StringProperty banco = new SimpleStringProperty();
    private final StringProperty numos = new SimpleStringProperty();
    private final StringProperty codcre = new SimpleStringProperty();
    private final StringProperty numap = new SimpleStringProperty();
    private final StringProperty docto = new SimpleStringProperty();
    private final StringProperty dupli = new SimpleStringProperty();
    private final StringProperty nrbanco = new SimpleStringProperty(null);
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty situacao = new SimpleStringProperty();
    private final StringProperty orcamento = new SimpleStringProperty(null);
    private final StringProperty barra = new SimpleStringProperty(null);
    private final ObjectProperty<BigDecimal> valdev = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty lote = new SimpleStringProperty(null);
    private final ObjectProperty<BigDecimal> valnota = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtprevisao = new SimpleObjectProperty<LocalDate>(null);
    private final IntegerProperty vezes = new SimpleIntegerProperty();
    private final StringProperty tppag = new SimpleStringProperty(null);
    private final StringProperty serie = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtdigita = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    private final StringProperty lancamento = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> usunumreg = new SimpleObjectProperty<>(null);
    private final StringProperty importsap = new SimpleStringProperty();
    private final StringProperty gnre = new SimpleStringProperty(null);
    private final StringProperty registro = new SimpleStringProperty(null);

    public Pagto() {

    }

    public Pagto(SdItemContratoFidic item, CadBan banco) {
        this.valororig.set(item.getTitulo().getValorigin());
        this.valor.set(item.getTitulo().getValorTitulo());
        this.valnota.set(item.getTitulo().getValorTitulo());
        this.historico.set("0500");
        this.banco.set("341");
        if (banco.getCodFor() != null)
            this.codcre.set(banco.getCodFor().getCodcli());
        this.numap.set(Globals.getProximoCodigo("PAGTO", "NUM_AP"));
        this.numos.set("0130");
        this.docto.set(item.getTitulo().getNumero());
        this.dupli.set(item.getTitulo().getNumero());
        this.situacao.set("11");
        this.status.set("N");
        this.vezes.set(0);
        this.serie.set("U");
        this.importsap.set("N");
    }

    @Column(name = "TAXA")
    public BigDecimal getTaxa() {
        return taxa.get();
    }

    public ObjectProperty<BigDecimal> taxaProperty() {
        return taxa;
    }

    public void setTaxa(BigDecimal taxa) {
        this.taxa.set(taxa);
    }

    @Column(name = "ICM")
    public BigDecimal getIcm() {
        return icm.get();
    }

    public ObjectProperty<BigDecimal> icmProperty() {
        return icm;
    }

    public void setIcm(BigDecimal icm) {
        this.icm.set(icm);
    }

    @Column(name = "IPI")
    public BigDecimal getIpi() {
        return ipi.get();
    }

    public ObjectProperty<BigDecimal> ipiProperty() {
        return ipi;
    }

    public void setIpi(BigDecimal ipi) {
        this.ipi.set(ipi);
    }

    @Column(name = "DATA_ORIG")
    public LocalDate getDataorig() {
        return dataorig.get();
    }

    public ObjectProperty<LocalDate> dataorigProperty() {
        return dataorig;
    }

    public void setDataorig(LocalDate dataorig) {
        this.dataorig.set(dataorig);
    }

    @Column(name = "DT_EMI")
    public LocalDate getDtemi() {
        return dtemi.get();
    }

    public ObjectProperty<LocalDate> dtemiProperty() {
        return dtemi;
    }

    public void setDtemi(LocalDate dtemi) {
        this.dtemi.set(dtemi);
    }

    @Column(name = "DT_VEN")
    public LocalDate getDtven() {
        return dtven.get();
    }

    public ObjectProperty<LocalDate> dtvenProperty() {
        return dtven;
    }

    public void setDtven(LocalDate dtven) {
        this.dtven.set(dtven);
    }

    @Column(name = "DT_ATUAL")
    public LocalDate getDtatual() {
        return dtatual.get();
    }

    public ObjectProperty<LocalDate> dtatualProperty() {
        return dtatual;
    }

    public void setDtatual(LocalDate dtatual) {
        this.dtatual.set(dtatual);
    }

    @Column(name = "VALOR_ORIG")
    public BigDecimal getValororig() {
        return valororig.get();
    }

    public ObjectProperty<BigDecimal> valororigProperty() {
        return valororig;
    }

    public void setValororig(BigDecimal valororig) {
        this.valororig.set(valororig);
    }

    @Column(name = "VAL_IPI")
    public BigDecimal getValipi() {
        return valipi.get();
    }

    public ObjectProperty<BigDecimal> valipiProperty() {
        return valipi;
    }

    public void setValipi(BigDecimal valipi) {
        this.valipi.set(valipi);
    }

    @Column(name = "VAL_ICM")
    public BigDecimal getValicm() {
        return valicm.get();
    }

    public ObjectProperty<BigDecimal> valicmProperty() {
        return valicm;
    }

    public void setValicm(BigDecimal valicm) {
        this.valicm.set(valicm);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "VAL_PAG")
    public BigDecimal getValpag() {
        return valpag.get();
    }

    public ObjectProperty<BigDecimal> valpagProperty() {
        return valpag;
    }

    public void setValpag(BigDecimal valpag) {
        this.valpag.set(valpag);
    }

    @Column(name = "JUROS")
    public BigDecimal getJuros() {
        return juros.get();
    }

    public ObjectProperty<BigDecimal> jurosProperty() {
        return juros;
    }

    public void setJuros(BigDecimal juros) {
        this.juros.set(juros);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "BLOQUEIO")
    public String getBloqueio() {
        return bloqueio.get();
    }

    public StringProperty bloqueioProperty() {
        return bloqueio;
    }

    public void setBloqueio(String bloqueio) {
        this.bloqueio.set(bloqueio);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "HISTORICO")
    public String getHistorico() {
        return historico.get();
    }

    public StringProperty historicoProperty() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico.set(historico);
    }

    @Column(name = "BANCO")
    public String getBanco() {
        return banco.get();
    }

    public StringProperty bancoProperty() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco.set(banco);
    }

    @Column(name = "NUM_OS")
    public String getNumos() {
        return numos.get();
    }

    public StringProperty numosProperty() {
        return numos;
    }

    public void setNumos(String numos) {
        this.numos.set(numos);
    }

    @Column(name = "CODCRE")
    public String getCodcre() {
        return codcre.get();
    }

    public StringProperty codcreProperty() {
        return codcre;
    }

    public void setCodcre(String codcre) {
        this.codcre.set(codcre);
    }

    @Id
    @Column(name = "NUM_AP")
    public String getNumap() {
        return numap.get();
    }

    public StringProperty numapProperty() {
        return numap;
    }

    public void setNumap(String numap) {
        this.numap.set(numap);
    }

    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }

    public StringProperty doctoProperty() {
        return docto;
    }

    public void setDocto(String docto) {
        this.docto.set(docto);
    }

    @Column(name = "DUPLI")
    public String getDupli() {
        return dupli.get();
    }

    public StringProperty dupliProperty() {
        return dupli;
    }

    public void setDupli(String dupli) {
        this.dupli.set(dupli);
    }

    @Column(name = "NRBANCO")
    public String getNrbanco() {
        return nrbanco.get();
    }

    public StringProperty nrbancoProperty() {
        return nrbanco;
    }

    public void setNrbanco(String nrbanco) {
        this.nrbanco.set(nrbanco);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Column(name = "ORCAMENTO")
    public String getOrcamento() {
        return orcamento.get();
    }

    public StringProperty orcamentoProperty() {
        return orcamento;
    }

    public void setOrcamento(String orcamento) {
        this.orcamento.set(orcamento);
    }

    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    @Column(name = "VAL_DEV")
    public BigDecimal getValdev() {
        return valdev.get();
    }

    public ObjectProperty<BigDecimal> valdevProperty() {
        return valdev;
    }

    public void setValdev(BigDecimal valdev) {
        this.valdev.set(valdev);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "VAL_NOTA")
    public BigDecimal getValnota() {
        return valnota.get();
    }

    public ObjectProperty<BigDecimal> valnotaProperty() {
        return valnota;
    }

    public void setValnota(BigDecimal valnota) {
        this.valnota.set(valnota);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "DT_PREVISAO")
    public LocalDate getDtprevisao() {
        return dtprevisao.get();
    }

    public ObjectProperty<LocalDate> dtprevisaoProperty() {
        return dtprevisao;
    }

    public void setDtprevisao(LocalDate dtprevisao) {
        this.dtprevisao.set(dtprevisao);
    }

    @Column(name = "VEZES")
    public Integer getVezes() {
        return vezes.get();
    }

    public IntegerProperty vezesProperty() {
        return vezes;
    }

    public void setVezes(Integer vezes) {
        this.vezes.set(vezes);
    }

    @Column(name = "TP_PAG")
    public String getTppag() {
        return tppag.get();
    }

    public StringProperty tppagProperty() {
        return tppag;
    }

    public void setTppag(String tppag) {
        this.tppag.set(tppag);
    }

    @Column(name = "SERIE")
    public String getSerie() {
        return serie.get();
    }

    public StringProperty serieProperty() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie.set(serie);
    }

    @Column(name = "DT_DIGITA")
    public LocalDate getDtdigita() {
        return dtdigita.get();
    }

    public ObjectProperty<LocalDate> dtdigitaProperty() {
        return dtdigita;
    }

    public void setDtdigita(LocalDate dtdigita) {
        this.dtdigita.set(dtdigita);
    }

    @Column(name = "LANCAMENTO")
    public String getLancamento() {
        return lancamento.get();
    }

    public StringProperty lancamentoProperty() {
        return lancamento;
    }

    public void setLancamento(String lancamento) {
        this.lancamento.set(lancamento);
    }

    @Column(name = "USU_NUMREG")
    public BigDecimal getUsunumreg() {
        return usunumreg.get();
    }

    public ObjectProperty<BigDecimal> usunumregProperty() {
        return usunumreg;
    }

    public void setUsunumreg(BigDecimal usunumreg) {
        this.usunumreg.set(usunumreg);
    }

    @Column(name = "IMPORT_SAP")
    public String getImportsap() {
        return importsap.get();
    }

    public StringProperty importsapProperty() {
        return importsap;
    }

    public void setImportsap(String importsap) {
        this.importsap.set(importsap);
    }

    @Column(name = "GNRE")
    public String getGnre() {
        return gnre.get();
    }

    public StringProperty gnreProperty() {
        return gnre;
    }

    public void setGnre(String gnre) {
        this.gnre.set(gnre);
    }

    @Column(name = "REGISTRO")
    public String getRegistro() {
        return registro.get();
    }

    public StringProperty registroProperty() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro.set(registro);
    }

}
