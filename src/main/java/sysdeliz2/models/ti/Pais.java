package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PAIS_001")
public class Pais extends BasicModel implements Serializable {
    
    private final StringProperty codPais = new SimpleStringProperty();
    private final StringProperty nomePais = new SimpleStringProperty();
    
    public Pais() {
    
    }
    
    @Id
    @Column(name = "COD_PAIS")
    public String getCodPais() {
        return codPais.get();
    }
    
    public StringProperty codPaisProperty() {
        return codPais;
    }
    
    public void setCodPais(String codPais) {
        setCodigoFilter(codPais);
        this.codPais.set(codPais);
    }
    
    @Column(name = "NOME_PAIS")
    public String getNomePais() {
        return nomePais.get();
    }
    
    public StringProperty nomePaisProperty() {
        return nomePais;
    }
    
    public void setNomePais(String nomePais) {
        setDescricaoFilter(nomePais);
        this.nomePais.set(nomePais);
    }
}
