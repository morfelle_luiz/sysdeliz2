package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lima.joao
 * @since 19/09/2019 14:27
 */
@Entity
@Table(name = "PCPFTOF_001")

@NamedNativeQuery(
        name = "Funcoes.f_get_valor_consumo_of",
        query = "SELECT F_PCP_GET_VALOR_CONSUMO_OF(:P_NUMERO_OF, :P_INSUMO, :P_COR, :P_COR_I, :P_FAIXA, :P_TAMANHO, :P_APLICACAO  ) FROM dual;")

public class PcpFtOf implements Serializable {

    private final ObjectProperty<PcpFtOfID> pcpFtOfID = new SimpleObjectProperty<>(this, "pcpFtOfID");
    private final StringProperty parte = new SimpleStringProperty(this, "parte");
    private final ObjectProperty<BigDecimal> consumo = new SimpleObjectProperty<>(this, "consumo");
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<>(this, "custo");
    private final StringProperty situacao = new SimpleStringProperty(this, "situacao");
    private final ObjectProperty<Date> data = new SimpleObjectProperty<>(this, "data");
    private final StringProperty corIOrig = new SimpleStringProperty(this, "corIOrig");
    private final StringProperty insumoOrig = new SimpleStringProperty(this, "insumoOrig");
    private final StringProperty deposito = new SimpleStringProperty(this, "deposito");

    private final ObjectProperty<BigDecimal> valorConsumo = new SimpleObjectProperty<>(this, "valorConsumo");

    @EmbeddedId
    public PcpFtOfID getPcpFtOfID() {
        return pcpFtOfID.get();
    }

    public ObjectProperty<PcpFtOfID> pcpFtOfIDProperty() {
        return pcpFtOfID;
    }

    public void setPcpFtOfID(PcpFtOfID pcpFtOfID) {
        this.pcpFtOfID.set(pcpFtOfID);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

    @Column(name = "CONSUMO")
    public BigDecimal getConsumo() {
        return consumo.get();
    }

    public ObjectProperty<BigDecimal> consumoProperty() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo.set(consumo);
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Column(name = "DATA")
    public Date getData() {
        return data.get();
    }

    public ObjectProperty<Date> dataProperty() {
        return data;
    }

    public void setData(Date data) {
        this.data.set(data);
    }

    @Column(name = "COR_I_ORIG")
    public String getCorIOrig() {
        return corIOrig.get();
    }

    public StringProperty corIOrigProperty() {
        return corIOrig;
    }

    public void setCorIOrig(String corIOrig) {
        this.corIOrig.set(corIOrig);
    }

    @Column(name = "INSUMO_ORIG")
    public String getInsumoOrig() {
        return insumoOrig.get();
    }

    public StringProperty insumoOrigProperty() {
        return insumoOrig;
    }

    public void setInsumoOrig(String insumoOrig) {
        this.insumoOrig.set(insumoOrig);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Transient
    public BigDecimal getValorConsumo(String tamanho){
        Query query = JPAUtils.getEntityManager().createNamedQuery("Funcoes.f_get_valor_consumo_of");
        query.setParameter("P_NUMERO_OF", this.getPcpFtOfID().getNumero());
        query.setParameter("P_INSUMO", this.getPcpFtOfID().getInsumo());
        query.setParameter("P_COR", this.getPcpFtOfID().getCor());
        query.setParameter("P_COR_I", this.getPcpFtOfID().getCorI());
        query.setParameter("P_FAIXA", this.getPcpFtOfID().getFaixa());
        query.setParameter("P_TAMANHO", tamanho);
        query.setParameter("P_APLICACAO", this.getPcpFtOfID().getAplicacao());

        return (BigDecimal) query.getSingleResult();
    }
    
    
}
