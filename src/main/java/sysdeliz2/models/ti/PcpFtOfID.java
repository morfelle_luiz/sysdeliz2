package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author lima.joao
 * @since 19/09/2019 14:27
 */
@Embeddable
public class PcpFtOfID implements Serializable {

    private final StringProperty numero = new SimpleStringProperty(this, "numero");
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
    private final StringProperty insumo = new SimpleStringProperty(this, "insumo");
    private final StringProperty setor = new SimpleStringProperty(this, "setor");
    private final StringProperty aplicacao = new SimpleStringProperty(this, "aplicacao");
    private final StringProperty faixa = new SimpleStringProperty(this, "faixa");
    private final StringProperty cor = new SimpleStringProperty(this, "cor");
    private final StringProperty corI = new SimpleStringProperty(this, "corI");
    private final IntegerProperty id = new SimpleIntegerProperty(this, "id");
    
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public void setId(int id) {
        this.id.set(id);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }

    public StringProperty insumoProperty() {
        return insumo;
    }

    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }

    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Column(name = "APLICACAO")
    public String getAplicacao() {
        return aplicacao.get();
    }

    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }

    public void setAplicacao(String aplicacao) {
        this.aplicacao.set(aplicacao);
    }

    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "COR_I")
    public String getCorI() {
        return corI.get();
    }

    public StringProperty corIProperty() {
        return corI;
    }

    public void setCorI(String corI) {
        this.corI.set(corI);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
