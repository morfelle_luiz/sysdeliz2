package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PCPAPL_001")
@TelaSysDeliz(descricao = "Aplicação PCP", icon = "gestao producao_100.png")
public class  Pcpapl extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao", defaultFilter = true)
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty tipoapl = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty sdriscoapl = new SimpleStringProperty();
    
    public Pcpapl() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "TIPOAPL")
    public String getTipoapl() {
        return tipoapl.get();
    }
    
    public StringProperty tipoaplProperty() {
        return tipoapl;
    }
    
    public void setTipoapl(String tipoapl) {
        this.tipoapl.set(tipoapl);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "SD_RISCO_APL")
    public String getSdriscoapl() {
        return sdriscoapl.get();
    }
    
    public StringProperty sdriscoaplProperty() {
        return sdriscoapl;
    }
    
    public void setSdriscoapl(String sdriscoapl) {
        this.sdriscoapl.set(sdriscoapl);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "]" + descricao.get();
    }
}