package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCliPaludo;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "PED_ITEN_001")
public class PedIten extends BasicModel {

    private final ObjectProperty<PedItenPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final IntegerProperty qtde_f = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty qtde_canc = new SimpleIntegerProperty(0);
    private final IntegerProperty qtde_packs = new SimpleIntegerProperty(1);
    private final StringProperty qualidade = new SimpleStringProperty("1");
    private final IntegerProperty qtde_orig = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> preco_orig = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty indice2 = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> perc_comissao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty desconto = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty("P");
    private final ObjectProperty<BigDecimal> ipi = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> margem = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty bonif = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty indice = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> impostos = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty embalagem = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dt_entrega = new SimpleObjectProperty<>();
    private final StringProperty desc_pack = new SimpleStringProperty();
    private final StringProperty estampa = new SimpleStringProperty("0");
    private final IntegerProperty nr_item = new SimpleIntegerProperty(0);
    private final StringProperty motivo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valor_st = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private IntegerProperty qtdeEtq = new SimpleIntegerProperty(0);
    private StringProperty corEtq = new SimpleStringProperty();

    public PedIten() {
    }

    public PedIten(PedIten item, SdCliPaludo etq) {
        this.id.set(new PedItenPK(item));
        this.corEtq.set(etq.getId().getCoritem());
        this.qtde.set(item.getQtde());
    }

    @EmbeddedId
    public PedItenPK getId() {
        return id.get();
    }

    public ObjectProperty<PedItenPK> idProperty() {
        return id;
    }

    public void setId(PedItenPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "QTDE_F")
    public int getQtde_f() {
        return qtde_f.get();
    }

    public IntegerProperty qtde_fProperty() {
        return qtde_f;
    }

    public void setQtde_f(int qtde_f) {
        this.qtde_f.set(qtde_f);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "QTDE_CANC")
    public int getQtde_canc() {
        return qtde_canc.get();
    }

    public IntegerProperty qtde_cancProperty() {
        return qtde_canc;
    }

    public void setQtde_canc(int qtde_canc) {
        this.qtde_canc.set(qtde_canc);
    }

    @Column(name = "QTDE_PACKS")
    public int getQtde_packs() {
        return qtde_packs.get();
    }

    public IntegerProperty qtde_packsProperty() {
        return qtde_packs;
    }

    public void setQtde_packs(int qtde_packs) {
        this.qtde_packs.set(qtde_packs);
    }

    @Column(name = "QUALIDADE")
    public String getQualidade() {
        return qualidade.get();
    }

    public StringProperty qualidadeProperty() {
        return qualidade;
    }

    public void setQualidade(String qualidade) {
        this.qualidade.set(qualidade);
    }

    @Column(name = "QTDE_ORIG")
    public int getQtde_orig() {
        return qtde_orig.get();
    }

    public IntegerProperty qtde_origProperty() {
        return qtde_orig;
    }

    public void setQtde_orig(int qtde_orig) {
        this.qtde_orig.set(qtde_orig);
    }

    @Column(name = "PRECO_ORIG")
    public BigDecimal getPreco_orig() {
        return preco_orig.get();
    }

    public ObjectProperty<BigDecimal> preco_origProperty() {
        return preco_orig;
    }

    public void setPreco_orig(BigDecimal preco_orig) {
        this.preco_orig.set(preco_orig);
    }

    @Column(name = "INDICE2")
    public int getIndice2() {
        return indice2.get();
    }

    public IntegerProperty indice2Property() {
        return indice2;
    }

    public void setIndice2(int indice2) {
        this.indice2.set(indice2);
    }

    @Column(name = "PERC_COMISSAO")
    public BigDecimal getPerc_comissao() {
        return perc_comissao.get();
    }

    public ObjectProperty<BigDecimal> perc_comissaoProperty() {
        return perc_comissao;
    }

    public void setPerc_comissao(BigDecimal perc_comissao) {
        this.perc_comissao.set(perc_comissao);
    }

    @Column(name = "DESCONTO")
    public String getDesconto() {
        return desconto.get();
    }

    public StringProperty descontoProperty() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "IPI")
    public BigDecimal getIpi() {
        return ipi.get();
    }

    public ObjectProperty<BigDecimal> ipiProperty() {
        return ipi;
    }

    public void setIpi(BigDecimal ipi) {
        this.ipi.set(ipi);
    }

    @Column(name = "MARGEM")
    public BigDecimal getMargem() {
        return margem.get();
    }

    public ObjectProperty<BigDecimal> margemProperty() {
        return margem;
    }

    public void setMargem(BigDecimal margem) {
        this.margem.set(margem);
    }

    @Column(name = "BONIF")
    public int getBonif() {
        return bonif.get();
    }

    public IntegerProperty bonifProperty() {
        return bonif;
    }

    public void setBonif(int bonif) {
        this.bonif.set(bonif);
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Column(name = "INDICE")
    public int getIndice() {
        return indice.get();
    }

    public IntegerProperty indiceProperty() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice.set(indice);
    }

    @Column(name = "IMPOSTOS")
    public BigDecimal getImpostos() {
        return impostos.get();
    }

    public ObjectProperty<BigDecimal> impostosProperty() {
        return impostos;
    }

    public void setImpostos(BigDecimal impostos) {
        this.impostos.set(impostos);
    }

    @Column(name = "EMBALAGEM")
    public String getEmbalagem() {
        return embalagem.get();
    }

    public StringProperty embalagemProperty() {
        return embalagem;
    }

    public void setEmbalagem(String embalagem) {
        this.embalagem.set(embalagem);
    }

    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "DT_ENTREGA")
    public LocalDate getDt_entrega() {
        return dt_entrega.get();
    }

    public ObjectProperty<LocalDate> dt_entregaProperty() {
        return dt_entrega;
    }

    public void setDt_entrega(LocalDate dt_entrega) {
        this.dt_entrega.set(dt_entrega);
    }

    @Column(name = "DESC_PACK")
    public String getDesc_pack() {
        return desc_pack.get();
    }

    public StringProperty desc_packProperty() {
        return desc_pack;
    }

    public void setDesc_pack(String desc_pack) {
        this.desc_pack.set(desc_pack);
    }

    @Column(name = "ESTAMPA")
    public String getEstampa() {
        return estampa.get();
    }

    public StringProperty estampaProperty() {
        return estampa;
    }

    public void setEstampa(String estampa) {
        this.estampa.set(estampa);
    }

    @Column(name = "NR_ITEM")
    public int getNr_item() {
        return nr_item.get();
    }

    public IntegerProperty nr_itemProperty() {
        return nr_item;
    }

    public void setNr_item(int nr_item) {
        this.nr_item.set(nr_item);
    }

    @Column(name = "MOTIVO")
    public String getMotivo() {
        return motivo.get();
    }

    public StringProperty motivoProperty() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo.set(motivo);
    }

    @Column(name = "VALOR_ST")
    public BigDecimal getValor_st() {
        return valor_st.get();
    }

    public ObjectProperty<BigDecimal> valor_stProperty() {
        return valor_st;
    }

    public void setValor_st(BigDecimal valor_st) {
        this.valor_st.set(valor_st);
    }

    @Transient
    public int getQtdeEtq() {
        return qtdeEtq.get();
    }

    public IntegerProperty qtdeEtqProperty() {
        return qtdeEtq;
    }

    public void setQtdeEtq(int qtdeEtq) {
        this.qtdeEtq.set(qtdeEtq);
    }

    @Transient
    public String getCorEtq() {
        if(corEtq.getValue() == null || corEtq.getValue().equals(""))
            return id.get().getCor().getCor();
        return corEtq.get();
    }

    public StringProperty corEtqProperty() {
        return corEtq;
    }

    public void setCorEtq(String corEtq) {
        this.corEtq.set(corEtq);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PedIten)) return false;
        PedIten pedIten = (PedIten) o;
        return Objects.equals(getId(), pedIten.getId()) &&
                Objects.equals(getCorEtq(), pedIten.getCorEtq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCorEtq());
    }
}
