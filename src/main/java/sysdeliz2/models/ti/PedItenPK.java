package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdCliPaludo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PedItenPK implements Serializable {

    private final IntegerProperty ordem = new SimpleIntegerProperty(0);
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty();
    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty();

    public PedItenPK() {
    }

    public PedItenPK(PedIten item) {
        this.tam.set(item.getId().getTam());
        this.codigo.set(item.getId().getCodigo());
    }

    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }

    public ObjectProperty<Cor> corProperty() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor.set(cor);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    @Transient
    public boolean isEqual(String numero, String codigo, String cor, String tam) {
        if (this.getNumero().equals(numero)
                && this.getCodigo().getCodigo().equals(codigo)
                && this.getCor().getCor().equals(cor)
                && this.getTam().equals(tam))
            return true;

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PedItenPK)) return false;
        PedItenPK pedItenPK = (PedItenPK) o;
        return Objects.equals(getTam(), pedItenPK.getTam()) && Objects.equals(getCodigo().getCodigo(), pedItenPK.getCodigo().getCodigo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTam(), getCodigo());
    }
}
