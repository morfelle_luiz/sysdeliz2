package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.comercial.SdPedido;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lima.joao
 * @since 17/10/2019 08:06
 */
@Entity
@Table(name = "PEDIDO_001")
@TelaSysDeliz(descricao = "Pedidos", icon = "pedido_50.png")
@SuppressWarnings("unused")
public class Pedido extends BasicModel implements Serializable {

    private final IntegerProperty fatura = new SimpleIntegerProperty(this, "fatura", 100);
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<>(this, "com1", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<>(this, "com2", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> percDesc = new SimpleObjectProperty<>(this, "percDesc", BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> dtdigita = new SimpleObjectProperty<>(this, "dtdigita");
    @ExibeTableView(descricao = "DT. Emissão", width = 70, columnType = ColumnType.DATE)
    private final ObjectProperty<LocalDate> dt_emissao = new SimpleObjectProperty<>(this, "dt_emissao");
    private final ObjectProperty<LocalDate> dt_fatura = new SimpleObjectProperty<>(this, "dt_fatura");
    private final StringProperty bloqueio = new SimpleStringProperty(this, "bloqueio");
    private final StringProperty cif = new SimpleStringProperty(this, "cif");
    @ExibeTableView(descricao = "Período", width = 100)
    @ColunaFilter(descricao = "Período", coluna = "periodo", filterClass = "sysdeliz2.models.ti.TabPrz")
    private final ObjectProperty<TabPrz> periodo = new SimpleObjectProperty<>(this, "periodo");
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>(this, "colecao");
    private final StringProperty codrep2 = new SimpleStringProperty(this, "codrep2");
    private final ObjectProperty<Regiao> tabPre = new SimpleObjectProperty(this, "tabPre");
    private final ObjectProperty<TabTran> tabTrans = new SimpleObjectProperty(this, "tabTrans");
    private final StringProperty tabRedes = new SimpleStringProperty(this, "tabRedes");
    @ExibeTableView(descricao = "Representante", width = 200)
    @ColunaFilter(descricao = "Representante", coluna = "codrep", filterClass = "sysdeliz2.models.ti.Represen")
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>(this, "codrep");
    private final StringProperty tabPre2 = new SimpleStringProperty(this, "tabPre2", "00");
    @ExibeTableView(descricao = "Número", width = 70)
    @ColunaFilter(descricao = "Número", coluna = "numero")
    private final StringProperty numero = new SimpleStringProperty(this, "numero");
    @ExibeTableView(descricao = "Cliente", width = 280)
    @ColunaFilter(descricao = "Cliente", coluna = "codcli", filterClass = "sysdeliz2.models.ti.Entidade")
    private final ObjectProperty<Entidade> codcli = new SimpleObjectProperty<>(this, "codcli");
    private final StringProperty pedCli = new SimpleStringProperty(this, "pedCli");
    private final StringProperty pagto = new SimpleStringProperty(this, "pagto");
    private final StringProperty obs = new SimpleStringProperty(this, "obs");
    private final StringProperty artCli = new SimpleStringProperty(this, "artCli");
    private final StringProperty ris = new SimpleStringProperty(this, "ris");
    private final ObjectProperty<LocalDate> entrerga = new SimpleObjectProperty<>(this, "entrerga");
    private final StringProperty moeda = new SimpleStringProperty(this, "moeda", "0");
    private final StringProperty nota = new SimpleStringProperty(this, "nota");
    private final IntegerProperty tempo = new SimpleIntegerProperty(this, "tempo", 0);
    private final ObjectProperty<BigDecimal> com3 = new SimpleObjectProperty<>(this, "com3", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> com4 = new SimpleObjectProperty<>(this, "com4", BigDecimal.ZERO);
    private final StringProperty motivo = new SimpleStringProperty(this, "motivo");
    private final StringProperty programacao = new SimpleStringProperty(this, "programacao");
    private final StringProperty desconto = new SimpleStringProperty(this, "desconto", "0,00 0,00 0,00 0,00");
    private final StringProperty financeiro = new SimpleStringProperty(this, "financeiro");
    private final StringProperty sitDup = new SimpleStringProperty(this, "sitDup");
    private final StringProperty redespCif = new SimpleStringProperty(this, "redespCif","4");
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<>(this, "frete", BigDecimal.ZERO);
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo", "P");
    private final ObjectProperty<BigDecimal> bonif = new SimpleObjectProperty<>(this, "bonif", BigDecimal.ZERO);
    private final IntegerProperty impresso = new SimpleIntegerProperty(this, "impresso",0);
    private final ObjectProperty<BigDecimal> vlrDesc = new SimpleObjectProperty<>(this, "vlrDesc", BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> taxa = new SimpleObjectProperty<>(this, "taxa", BigDecimal.ZERO);
    private final StringProperty locado = new SimpleStringProperty(this, "locado");
    private final StringProperty periodoProd = new SimpleStringProperty(this, "periodoProd");
    private final StringProperty progSetor = new SimpleStringProperty(this, "progSetor");
    private final StringProperty contato = new SimpleStringProperty(this, "contato");
    private final StringProperty envioEspelho = new SimpleStringProperty(this, "envioEspelho", "X");
    private List<PedIten> itens = new ArrayList<>();
    private final ObjectProperty<SdPedido> sdPedido = new SimpleObjectProperty<>();
    private Long qtdeItens = 0L;

    public Pedido() {
    }

    public Pedido(String numero, Long qtdeItens) {
        this.numero.set(numero);
        this.qtdeItens = qtdeItens;
    }
    
    @Column(name = "FATURA")
    public int getFatura() {
        return fatura.get();
    }

    public IntegerProperty faturaProperty() {
        return fatura;
    }

    public void setFatura(int fatura) {
        this.fatura.set(fatura);
    }

    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }

    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }

    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }

    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }

    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }

    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }

    @Column(name = "PER_DESC")
    public BigDecimal getPercDesc() {
        return percDesc.get();
    }

    public ObjectProperty<BigDecimal> percDescProperty() {
        return percDesc;
    }

    public void setPercDesc(BigDecimal percDesc) {
        this.percDesc.set(percDesc);
    }

    @Column(name = "DTDIGITA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtdigita() {
        return dtdigita.get();
    }

    public ObjectProperty<LocalDate> dtdigitaProperty() {
        return dtdigita;
    }

    public void setDtdigita(LocalDate dtDigita) {
        this.dtdigita.set(dtDigita);
    }

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDt_emissao() {
        return dt_emissao.get();
    }

    public ObjectProperty<LocalDate> dt_emissaoProperty() {
        return dt_emissao;
    }

    public void setDt_emissao(LocalDate dtEmissao) {
        this.dt_emissao.set(dtEmissao);
    }

    @Column(name = "DT_FATURA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDt_fatura() {
        return dt_fatura.get();
    }

    public ObjectProperty<LocalDate> dt_faturaProperty() {
        return dt_fatura;
    }

    public void setDt_fatura(LocalDate dtFatura) {
        this.dt_fatura.set(dtFatura);
    }

    @Column(name = "BLOQUEIO")
    public String getBloqueio() {
        return bloqueio.get();
    }

    public StringProperty bloqueioProperty() {
        return bloqueio;
    }

    public void setBloqueio(String bloqueio) {
        this.bloqueio.set(bloqueio);
    }

    @Column(name = "CIF")
    public String getCif() {
        return cif.get();
    }

    public StringProperty cifProperty() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif.set(cif);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERIODO")
    public TabPrz getPeriodo() {
        return periodo.get();
    }

    public ObjectProperty<TabPrz> periodoProperty() {
        return periodo;
    }

    public void setPeriodo(TabPrz periodo) {
        this.periodo.set(periodo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "CODREP2")
    public String getCodrep2() {
        return codrep2.get();
    }

    public StringProperty codrep2Property() {
        return codrep2;
    }

    public void setCodrep2(String codrep2) {
        this.codrep2.set(codrep2);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TAB_PRE")
    public Regiao getTabPre() {
        return tabPre.get();
    }

    public ObjectProperty<Regiao> tabPreProperty() {
        return tabPre;
    }

    public void setTabPre(Regiao tabPre) {
        this.tabPre.set(tabPre);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAB_TRANS")
    public TabTran getTabTrans() {
        return tabTrans.get();
    }

    public ObjectProperty<TabTran> tabTransProperty() {
        return tabTrans;
    }

    public void setTabTrans(TabTran tabTrans) {
        this.tabTrans.set(tabTrans);
    }

    @Column(name = "TAB_REDES")
    public String getTabRedes() {
        return tabRedes.get();
    }

    public StringProperty tabRedesProperty() {
        return tabRedes;
    }

    public void setTabRedes(String tabRedes) {
        this.tabRedes.set(tabRedes);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }
    
    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }

    @Column(name = "TAB_PRE2")
    public String getTabPre2() {
        return tabPre2.get();
    }

    public StringProperty tabPre2Property() {
        return tabPre2;
    }

    public void setTabPre2(String tabPre2) {
        this.tabPre2.set(tabPre2);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.setCodigoFilter(numero);
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODCLI", columnDefinition = "VARCHAR2()", referencedColumnName = "codcli")
    @Convert(converter = CodcliAttributeConverter.class)
    public Entidade getCodcli() {
        return codcli.get();
    }
    
    public ObjectProperty<Entidade> codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Entidade codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "PED_CLI")
    public String getPedCli() {
        return pedCli.get();
    }

    public StringProperty pedCliProperty() {
        return pedCli;
    }

    public void setPedCli(String pedCli) {
        this.pedCli.set(pedCli);
    }

    @Column(name = "PGTO")
    public String getPagto() {
        return pagto.get();
    }

    public StringProperty pagtoProperty() {
        return pagto;
    }

    public void setPagto(String pagto) {
        this.pagto.set(pagto);
    }

    @Lob
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "ART_CLI")
    public String getArtCli() {
        return artCli.get();
    }

    public StringProperty artCliProperty() {
        return artCli;
    }

    public void setArtCli(String artCli) {
        this.artCli.set(artCli);
    }

    @Column(name = "RIS")
    public String getRis() {
        return ris.get();
    }

    public StringProperty risProperty() {
        return ris;
    }

    public void setRis(String ris) {
        this.ris.set(ris);
    }

    @Column(name = "ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getEntrerga() {
        return entrerga.get();
    }

    public ObjectProperty<LocalDate> entrergaProperty() {
        return entrerga;
    }

    public void setEntrerga(LocalDate entrerga) {
        this.entrerga.set(entrerga);
    }

    @Column(name = "MOEDA")
    public String getMoeda() {
        return moeda.get();
    }

    public StringProperty moedaProperty() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda.set(moeda);
    }

    @Column(name = "NOTA")
    public String getNota() {
        return nota.get();
    }

    public StringProperty notaProperty() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota.set(nota);
    }

    @Column(name = "TEMPO")
    public int getTempo() {
        return tempo.get();
    }

    public IntegerProperty tempoProperty() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo.set(tempo);
    }

    @Column(name = "COM3")
    public BigDecimal getCom3() {
        return com3.get();
    }

    public ObjectProperty<BigDecimal> com3Property() {
        return com3;
    }

    public void setCom3(BigDecimal com3) {
        this.com3.set(com3);
    }

    @Column(name = "COM4")
    public BigDecimal getCom4() {
        return com4.get();
    }

    public ObjectProperty<BigDecimal> com4Property() {
        return com4;
    }

    public void setCom4(BigDecimal com4) {
        this.com4.set(com4);
    }

    @Column(name = "MOTIVO")
    public String getMotivo() {
        return motivo.get();
    }

    public StringProperty motivoProperty() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo.set(motivo);
    }

    @Column(name = "PROGRAMACAO")
    public String getProgramacao() {
        return programacao.get();
    }

    public StringProperty programacaoProperty() {
        return programacao;
    }

    public void setProgramacao(String programacao) {
        this.programacao.set(programacao);
    }

    @Column(name = "DESCONTO")
    public String getDesconto() {
        return desconto.get();
    }

    public StringProperty descontoProperty() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "FINANCEIRO")
    public String getFinanceiro() {
        return financeiro.get();
    }

    public StringProperty financeiroProperty() {
        return financeiro;
    }

    public void setFinanceiro(String financeiro) {
        this.financeiro.set(financeiro);
    }

    @Column(name = "SIT_DUP")
    public String getSitDup() {
        return sitDup.get();
    }

    public StringProperty sitDupProperty() {
        return sitDup;
    }

    public void setSitDup(String sitDup) {
        this.sitDup.set(sitDup);
    }

    @Column(name = "REDESP_CIF")
    public String getRedespCif() {
        return redespCif.get();
    }

    public StringProperty redespCifProperty() {
        return redespCif;
    }

    public void setRedespCif(String redespCif) {
        this.redespCif.set(redespCif);
    }

    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }

    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "BONIF")
    public BigDecimal getBonif() {
        return bonif.get();
    }

    public ObjectProperty<BigDecimal> bonifProperty() {
        return bonif;
    }

    public void setBonif(BigDecimal bonif) {
        this.bonif.set(bonif);
    }

    @Column(name = "IMPRESSO")
    public int getImpresso() {
        return impresso.get();
    }

    public IntegerProperty impressoProperty() {
        return impresso;
    }

    public void setImpresso(int impresso) {
        this.impresso.set(impresso);
    }

    @Column(name = "VLR_DESC")
    public BigDecimal getVlrDesc() {
        return vlrDesc.get();
    }

    public ObjectProperty<BigDecimal> vlrDescProperty() {
        return vlrDesc;
    }

    public void setVlrDesc(BigDecimal vlrDesc) {
        this.vlrDesc.set(vlrDesc);
    }

    @Column(name = "TAXA")
    public BigDecimal getTaxa() {
        return taxa.get();
    }

    public ObjectProperty<BigDecimal> taxaProperty() {
        return taxa;
    }

    public void setTaxa(BigDecimal taxa) {
        this.taxa.set(taxa);
    }

    @Column(name = "LOCADO")
    public String getLocado() {
        return locado.get();
    }

    public StringProperty locadoProperty() {
        return locado;
    }

    public void setLocado(String locado) {
        this.locado.set(locado);
    }

    @Column(name = "PERIODO_PROD")
    public String getPeriodoProd() {
        return periodoProd.get();
    }

    public StringProperty periodoProdProperty() {
        return periodoProd;
    }

    public void setPeriodoProd(String periodoProd) {
        this.periodoProd.set(periodoProd);
    }

    @Column(name = "PROG_SETOR")
    public String getProgSetor() {
        return progSetor.get();
    }

    public StringProperty progSetorProperty() {
        return progSetor;
    }

    public void setProgSetor(String progSetor) {
        this.progSetor.set(progSetor);
    }

    @Column(name = "CONTATO")
    public String getContato() {
        return contato.get();
    }

    public StringProperty contatoProperty() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato.set(contato);
    }

    @Column(name = "ENVIO_ESPELHO")
    public String getEnvioEspelho() {
        return envioEspelho.get();
    }

    public StringProperty envioEspelhoProperty() {
        return envioEspelho;
    }

    public void setEnvioEspelho(String envioEspelho) {
        this.envioEspelho.set(envioEspelho);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    public SdPedido getSdPedido() {
        return sdPedido.get();
    }

    public ObjectProperty<SdPedido> sdPedidoProperty() {
        return sdPedido;
    }

    public void setSdPedido(SdPedido sdPedido) {
        this.sdPedido.set(sdPedido);
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    public List<PedIten> getItens() {
        return itens;
    }
    
    public void setItens(List<PedIten> items){
        this.itens = items;
    }

    @Transient
    public Long getQtdeItens() {
        return qtdeItens;
    }

    public void setQtdeItens(Long qtdeItens) {
        this.qtdeItens = qtdeItens;
    }

    @Override
    public String toString() {
        return numero.get();
    }

    @PostLoad
    private void postLoad() throws SQLException {
        try {
            String _colecao = (String) new NativeDAO().runNativeQueryFunction(Types.VARCHAR,
                    "f_sd_get_col_ped_marca('%s','%s')",
                    StringUtils.toDateFormat(this.dt_emissao.get()), sdPedido.get().getMarca());
            sdPedido.get().setColecao(_colecao);
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
