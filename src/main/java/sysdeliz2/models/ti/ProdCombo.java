package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Table(name = "PROD_COMBO_001")
@Entity
public class ProdCombo extends BasicModel {

    private final ObjectProperty<ProdComboPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecombo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> precopeca = new SimpleObjectProperty<>();
    private final StringProperty somatorio = new SimpleStringProperty("X");
    private final StringProperty inativar = new SimpleStringProperty("N");
    private final ObjectProperty<BigDecimal> qtdeate = new SimpleObjectProperty<>();

    public ProdCombo() {
    }

    public ProdCombo(String produto, String produtoIrmao, String tabela, BigDecimal qtde, BigDecimal preco, BigDecimal qtdeAte) {
        this.id.set(new ProdComboPK(produto, produtoIrmao, tabela));
        this.qtdecombo.set(qtde);
        this.precopeca.set(preco);
        this.qtdeate.set(qtdeAte);
    }

    @EmbeddedId
    public ProdComboPK getId() {
        return id.get();
    }

    public ObjectProperty<ProdComboPK> idProperty() {
        return id;
    }

    public void setId(ProdComboPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE_COMBO")
    public BigDecimal getQtdecombo() {
        return qtdecombo.get();
    }

    public ObjectProperty<BigDecimal> qtdecomboProperty() {
        return qtdecombo;
    }

    public void setQtdecombo(BigDecimal qtdecombo) {
        this.qtdecombo.set(qtdecombo);
    }

    @Column(name = "PRECO_PECA")
    public BigDecimal getPrecopeca() {
        return precopeca.get();
    }

    public ObjectProperty<BigDecimal> precopecaProperty() {
        return precopeca;
    }

    public void setPrecopeca(BigDecimal precopeca) {
        this.precopeca.set(precopeca);
    }

    @Column(name = "SOMATORIO")
    public String getSomatorio() {
        return somatorio.get();
    }

    public StringProperty somatorioProperty() {
        return somatorio;
    }

    public void setSomatorio(String somatorio) {
        this.somatorio.set(somatorio);
    }

    @Column(name = "INATIVAR")
    public String getInativar() {
        return inativar.get();
    }

    public StringProperty inativarProperty() {
        return inativar;
    }

    public void setInativar(String inativar) {
        this.inativar.set(inativar);
    }

    @Column(name = "QTDE_ATE")
    public BigDecimal getQtdeate() {
        return qtdeate.get();
    }

    public ObjectProperty<BigDecimal> qtdeateProperty() {
        return qtdeate;
    }

    public void setQtdeate(BigDecimal qtdeate) {
        this.qtdeate.set(qtdeate);
    }

}