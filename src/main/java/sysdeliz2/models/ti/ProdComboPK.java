package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProdComboPK implements Serializable {

    private final StringProperty prodori = new SimpleStringProperty();
    private final StringProperty prodirm = new SimpleStringProperty();
    private final StringProperty tabela = new SimpleStringProperty();

    public ProdComboPK() {
    }

    public ProdComboPK(String produto, String produtoIrmao, String tabela) {
        this.prodori.set(produto);
        this.prodirm.set(produtoIrmao);
        this.tabela.set(tabela);
    }

    @Column(name = "PROD_ORI")
    public String getProdori() {
        return prodori.get();
    }

    public StringProperty prodoriProperty() {
        return prodori;
    }

    public void setProdori(String prodori) {
        this.prodori.set(prodori);
    }

    @Column(name = "PROD_IRM")
    public String getProdirm() {
        return prodirm.get();
    }

    public StringProperty prodirmProperty() {
        return prodirm;
    }

    public void setProdirm(String prodirm) {
        this.prodirm.set(prodirm);
    }

    @Column(name = "TABELA")
    public String getTabela() {
        return tabela.get();
    }

    public StringProperty tabelaProperty() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela.set(tabela);
    }

}