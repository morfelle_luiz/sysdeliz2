package sysdeliz2.models.ti;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.models.Cor;
import sysdeliz2.models.Tamanho;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 30/08/2019 17:36
 */
@Entity
@Table(name = "PRODUTO_001")
@TelaSysDeliz(descricao = "Produto", icon = "produto (4).png")
public class Produto extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 250)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 30)
    private final StringProperty ativo = new SimpleStringProperty(this, "ativo");
    
    @Transient
    @ExibeTableView(descricao = "Coleção", width = 150)
    @ColunaFilter(descricao = "Coleção", coluna = "colecao.codigo", filterClass = "sysdeliz2.models.ti.Colecao")
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>(this, "colecao");
    @Transient
    private final ObjectProperty<Linha> linha = new SimpleObjectProperty<>(this, "linha");
    @Transient
    private final ObjectProperty<Faixa> faixa = new SimpleObjectProperty<>(this, "faixa");
    @Transient
    @ExibeTableView(descricao = "Marca", width = 100)
    @ColunaFilter(descricao = "Marca", coluna = "marca.codigo", filterClass = "sysdeliz2.models.ti.Marca")
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>(this, "marca");
    @Transient
    private final ObjectProperty<Familia> familia = new SimpleObjectProperty<>(this, "familia");
    @Transient
    private final ObjectProperty<EtqProd> etiqueta = new SimpleObjectProperty<>(this, "etiqueta");
    
    @Transient
    private final ListProperty<Cor> coresDemanda = new SimpleListProperty<>();
    @Transient
    private final ListProperty<Tamanho> tamanhosDemanda = new SimpleListProperty<>();
    
    @Transient
    private final BooleanProperty gravar = new SimpleBooleanProperty(this, "gravar");
    @Transient
    private final BooleanProperty alterou = new SimpleBooleanProperty(this, "alterou", false);
    @Transient
    private final ObjectProperty<BigDecimal> percIpi = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty codSped = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<SdProduto> sdProduto = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty estilista = new SimpleStringProperty();
    @Transient
    private final StringProperty modelista = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<LocalDate> dtEntrega = new SimpleObjectProperty<>();
    
    public Produto() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LINHA")
    public Linha getLinha() {
        return linha.get();
    }

    public ObjectProperty<Linha> linhaProperty() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha.set(linha);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAIXA")
    public Faixa getFaixa() {
        return faixa.get();
    }

    public ObjectProperty<Faixa> faixaProperty() {
        return faixa;
    }

    public void setFaixa(Faixa faixa) {
        this.faixa.set(faixa);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAMILIA")
    public Familia getFamilia() {
        return familia.get();
    }

    public ObjectProperty<Familia> familiaProperty() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia.set(familia);
    }

    @Column(name = "ATIVO")
    public String getAtivo() {
        return ativo.get();
    }

    public StringProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo.set(ativo);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ETIQUETA", nullable = true)
    public EtqProd getEtiqueta() {
        return etiqueta.get();
    }

    public ObjectProperty<EtqProd> etiquetaProperty() {
        return etiqueta;
    }

    public void setEtiqueta(EtqProd etiqueta) {
        this.etiqueta.set(etiqueta);
    }
    
    @Column(name = "IPI")
    public BigDecimal getPercIpi() {
        return percIpi.get();
    }
    
    public ObjectProperty<BigDecimal> percIpiProperty() {
        return percIpi;
    }
    
    public void setPercIpi(BigDecimal percIpi) {
        this.percIpi.set(percIpi);
    }

    @Column(name = "CODSPED")
    public String getCodSped() {
        return codSped.get();
    }

    public StringProperty codSpedProperty() {
        return codSped;
    }

    public void setCodSped(String codSped) {
        this.codSped.set(codSped);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public SdProduto getSdProduto() {
        return sdProduto.get();
    }

    public ObjectProperty<SdProduto> sdProdutoProperty() {
        return sdProduto;
    }

    public void setSdProduto(SdProduto sdProduto) {
        this.sdProduto.set(sdProduto);
    }

    @Transient
    public ObservableList<Cor> getCoresDemanda() {
        return coresDemanda.get();
    }

    public ListProperty<Cor> coresDemandaProperty() {
        return coresDemanda;
    }

    public void setCoresDemanda(ObservableList<Cor> coresDemanda) {
        this.coresDemanda.set(coresDemanda);
    }

    @Transient
    public ObservableList<Tamanho> getTamanhosDemanda() {
        return tamanhosDemanda.get();
    }

    public ListProperty<Tamanho> tamanhosDemandaProperty() {
        return tamanhosDemanda;
    }

    public void setTamanhosDemanda(ObservableList<Tamanho> tamanhosDemanda) {
        this.tamanhosDemanda.set(tamanhosDemanda);
    }

    @Transient
    public boolean getGravar(){
        return this.gravar.get();
    }

    public BooleanProperty gravarProperty(){
        return gravar;
    }

    public void setGravar(Boolean gravar){
        this.gravar.set(gravar);
    }

    @Transient
    public boolean getAlterou(){
        return this.alterou.get();
    }

    public BooleanProperty alterouProperty(){
        return alterou;
    }

    public void setAlterou(Boolean alterou){
        this.alterou.set(alterou);
    }

    @Column(name = "ESTILISTA")
    public String getEstilista() {
        return estilista.get();
    }

    public StringProperty estilistaProperty() {
        return estilista;
    }

    public void setEstilista(String estilista) {
        this.estilista.set(estilista);
    }

    @Column(name = "MODELISTA")
    public String getModelista() {
        return modelista.get();
    }

    public StringProperty modelistaProperty() {
        return modelista;
    }

    public void setModelista(String modelista) {
        this.modelista.set(modelista);
    }

    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEntrega() {
        return dtEntrega.get();
    }

    public ObjectProperty<LocalDate> dtEntregaProperty() {
        return dtEntrega;
    }

    public void setDtEntrega(LocalDate dtEntrega) {
        this.dtEntrega.set(dtEntrega);
    }

    @Transient
    public boolean temCores(){
        return this.getCoresDemanda() == null || this.getCoresDemanda().isEmpty();
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produto produto = (Produto) o;
        return Objects.equals(codigo, produto.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
