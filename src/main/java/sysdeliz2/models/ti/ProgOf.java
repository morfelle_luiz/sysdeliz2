package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "PROG_OF_001")
public class ProgOf implements Serializable {

    private final StringProperty tipo = new SimpleStringProperty("T");
    private final StringProperty numdoc = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty partida = new SimpleStringProperty("1");
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();

    public ProgOf() {
    }

    public ProgOf(String tipo, String numdoc, String numero, String partida, BigDecimal qtde,
                  String codigo, String cor) {
        this.tipo.set(tipo);
        this.numdoc.set(numdoc);
        this.numero.set(numero);
        this.partida.set(partida);
        this.qtde.set(qtde);
        this.codigo.set(codigo);
        this.cor.set(cor);
    }

    @Id
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Id
    @Column(name = "NUM_DOC")
    public String getNumdoc() {
        return numdoc.get();
    }

    public StringProperty numdocProperty() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc.set(numdoc);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }

    public StringProperty partidaProperty() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida.set(partida);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }
}
