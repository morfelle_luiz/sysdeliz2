package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "REC_COM_001")
public class RecCom {
    
    private final ObjectProperty<RecComPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<>();
    
    @EmbeddedId
    public RecComPK getId() {
        return id.get();
    }
    
    public ObjectProperty<RecComPK> idProperty() {
        return id;
    }
    
    public void setId(RecComPK id) {
        this.id.set(id);
    }
    
    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }
    
    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }
    
    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }
    
    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }
    
    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }
    
    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }
}