package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "RECEBER_001")
public class Receber extends BasicModel {

    private final IntegerProperty vezes = new SimpleIntegerProperty(0);
    private final IntegerProperty numcx = new SimpleIntegerProperty(0);
    private final IntegerProperty bordero = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> taxa = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> aliquota = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    @ColunaFilter(descricao = "Dt Vencimento", coluna = "dtvencto")
    private final ObjectProperty<LocalDate> dtvencto = new SimpleObjectProperty<LocalDate>();
    @ColunaFilter(descricao = "Dt Emissao", coluna = "dtemissao")
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> original = new SimpleObjectProperty<LocalDate>();
    @ColunaFilter(descricao = "Valor", coluna = "valor")
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valor2 = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorpago = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> quant = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pesol = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pesob = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> juros = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valdev = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorigin = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> despesas = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valipi = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty tipo = new SimpleStringProperty();
    @ColunaFilter(descricao = "Situação", coluna = "situacao")
    private final StringProperty situacao = new SimpleStringProperty();
    private final StringProperty tipfat = new SimpleStringProperty();
    private final StringProperty banco = new SimpleStringProperty();
    private final StringProperty transport = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty codrep2 = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty nrbanco = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final IntegerProperty nrocupom = new SimpleIntegerProperty(0);
    private final StringProperty classe = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtcomissao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> valcom = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty pedido = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtserasa = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtenvio = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> com3 = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> com4 = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty md5 = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtprevisao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtstatus = new SimpleObjectProperty<LocalDate>();
    private final StringProperty historico = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valsubst = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final StringProperty numeroch = new SimpleStringProperty();
    private final StringProperty contach = new SimpleStringProperty();
    private final StringProperty agenciach = new SimpleStringProperty();
    private final StringProperty emissorch = new SimpleStringProperty();
    private final StringProperty bancoch = new SimpleStringProperty();
    private final StringProperty lancamento = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> usunumreg = new SimpleObjectProperty<>();
    private final StringProperty importsap = new SimpleStringProperty("N");
    private final ObjectProperty<LocalDate> datadag = new SimpleObjectProperty<LocalDate>();
//    private final ObjectProperty<VSdTitulosContrato> tituloContrato = new SimpleObjectProperty<>();

    private final ObjectProperty<BigDecimal> valorRecebido = new SimpleObjectProperty<BigDecimal>(BigDecimal.ZERO);
    private final ObjectProperty<VSdDadosEntidade> entidade = new SimpleObjectProperty<>();
    private final ObjectProperty<Represen> representante = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorTitulo = new SimpleObjectProperty<>(BigDecimal.ZERO);


    public Receber() {
    }

    @Column(name = "VEZES")
    public Integer getVezes() {
        return vezes.get();
    }

    public IntegerProperty vezesProperty() {
        return vezes;
    }

    public void setVezes(Integer vezes) {
        this.vezes.set(vezes);
    }

    @Column(name = "NUM_CX")
    public Integer getNumcx() {
        return numcx.get();
    }

    public IntegerProperty numcxProperty() {
        return numcx;
    }

    public void setNumcx(Integer numcx) {
        this.numcx.set(numcx);
    }

    @Column(name = "BORDERO")
    public Integer getBordero() {
        return bordero.get();
    }

    public IntegerProperty borderoProperty() {
        return bordero;
    }

    public void setBordero(Integer bordero) {
        this.bordero.set(bordero);
    }

    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }

    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }

    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }

    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }

    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }

    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }

    @Column(name = "TAXA")
    public BigDecimal getTaxa() {
        return taxa.get();
    }

    public ObjectProperty<BigDecimal> taxaProperty() {
        return taxa;
    }

    public void setTaxa(BigDecimal taxa) {
        this.taxa.set(taxa);
    }

    @Column(name = "ALIQUOTA")
    public BigDecimal getAliquota() {
        return aliquota.get();
    }

    public ObjectProperty<BigDecimal> aliquotaProperty() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota.set(aliquota);
    }

    @Column(name = "DT_VENCTO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtvencto() {
        return dtvencto.get();
    }

    public ObjectProperty<LocalDate> dtvenctoProperty() {
        return dtvencto;
    }

    public void setDtvencto(LocalDate dtvencto) {
        this.dtvencto.set(dtvencto);
    }

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "ORIGINAL")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getOriginal() {
        return original.get();
    }

    public ObjectProperty<LocalDate> originalProperty() {
        return original;
    }

    public void setOriginal(LocalDate original) {
        this.original.set(original);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "VALOR2")
    public BigDecimal getValor2() {
        return valor2.get();
    }

    public ObjectProperty<BigDecimal> valor2Property() {
        return valor2;
    }

    public void setValor2(BigDecimal valor2) {
        this.valor2.set(valor2);
    }

    @Column(name = "VALOR_PAGO")
    public BigDecimal getValorpago() {
        return valorpago.get();
    }

    public ObjectProperty<BigDecimal> valorpagoProperty() {
        return valorpago;
    }

    public void setValorpago(BigDecimal valorpago) {
        this.valorpago.set(valorpago);
    }

    @Column(name = "QUANT")
    public BigDecimal getQuant() {
        return quant.get();
    }

    public ObjectProperty<BigDecimal> quantProperty() {
        return quant;
    }

    public void setQuant(BigDecimal quant) {
        this.quant.set(quant);
    }

    @Column(name = "PESOL")
    public BigDecimal getPesol() {
        return pesol.get();
    }

    public ObjectProperty<BigDecimal> pesolProperty() {
        return pesol;
    }

    public void setPesol(BigDecimal pesol) {
        this.pesol.set(pesol);
    }

    @Column(name = "PESOB")
    public BigDecimal getPesob() {
        return pesob.get();
    }

    public ObjectProperty<BigDecimal> pesobProperty() {
        return pesob;
    }

    public void setPesob(BigDecimal pesob) {
        this.pesob.set(pesob);
    }

    @Column(name = "JUROS")
    public BigDecimal getJuros() {
        return juros.get();
    }

    public ObjectProperty<BigDecimal> jurosProperty() {
        return juros;
    }

    public void setJuros(BigDecimal juros) {
        this.juros.set(juros);
    }

    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }

    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }

    @Column(name = "VAL_DEV")
    public BigDecimal getValdev() {
        return valdev.get();
    }

    public ObjectProperty<BigDecimal> valdevProperty() {
        return valdev;
    }

    public void setValdev(BigDecimal valdev) {
        this.valdev.set(valdev);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "VAL_ORIGIN")
    public BigDecimal getValorigin() {
        return valorigin.get();
    }

    public ObjectProperty<BigDecimal> valoriginProperty() {
        return valorigin;
    }

    public void setValorigin(BigDecimal valorigin) {
        this.valorigin.set(valorigin);
    }

    @Column(name = "DESPESAS")
    public BigDecimal getDespesas() {
        return despesas.get();
    }

    public ObjectProperty<BigDecimal> despesasProperty() {
        return despesas;
    }

    public void setDespesas(BigDecimal despesas) {
        this.despesas.set(despesas);
    }

    @Column(name = "VAL_IPI")
    public BigDecimal getValipi() {
        return valipi.get();
    }

    public ObjectProperty<BigDecimal> valipiProperty() {
        return valipi;
    }

    public void setValipi(BigDecimal valipi) {
        this.valipi.set(valipi);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Column(name = "TIPFAT")
    public String getTipfat() {
        return tipfat.get();
    }

    public StringProperty tipfatProperty() {
        return tipfat;
    }

    public void setTipfat(String tipfat) {
        this.tipfat.set(tipfat);
    }

    @Column(name = "BANCO")
    public String getBanco() {
        return banco.get();
    }

    public StringProperty bancoProperty() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco.set(banco);
    }

    @Column(name = "TRANSPORT")
    public String getTransport() {
        return transport.get();
    }

    public StringProperty transportProperty() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport.set(transport);
    }

    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }

    @Column(name = "CODREP2")
    public String getCodrep2() {
        return codrep2.get();
    }

    public StringProperty codrep2Property() {
        return codrep2;
    }

    public void setCodrep2(String codrep2) {
        this.codrep2.set(codrep2);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }

    public StringProperty faturaProperty() {
        return fatura;
    }

    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
        setCodigoFilter(numero);
    }

    @Column(name = "NRBANCO")
    public String getNrbanco() {
        return nrbanco.get();
    }

    public StringProperty nrbancoProperty() {
        return nrbanco;
    }

    public void setNrbanco(String nrbanco) {
        this.nrbanco.set(nrbanco);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "NRO_CUPOM")
    public Integer getNrocupom() {
        return nrocupom.get();
    }

    public IntegerProperty nrocupomProperty() {
        return nrocupom;
    }

    public void setNrocupom(Integer nrocupom) {
        this.nrocupom.set(nrocupom);
    }

    @Column(name = "CLASSE")
    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    @Column(name = "DT_COMISSAO")
    public LocalDate getDtcomissao() {
        return dtcomissao.get();
    }

    public ObjectProperty<LocalDate> dtcomissaoProperty() {
        return dtcomissao;
    }

    public void setDtcomissao(LocalDate dtcomissao) {
        this.dtcomissao.set(dtcomissao);
    }

    @Column(name = "VAL_COM")
    public BigDecimal getValcom() {
        return valcom.get();
    }

    public ObjectProperty<BigDecimal> valcomProperty() {
        return valcom;
    }

    public void setValcom(BigDecimal valcom) {
        this.valcom.set(valcom);
    }

    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Column(name = "DT_SERASA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtserasa() {
        return dtserasa.get();
    }

    public ObjectProperty<LocalDate> dtserasaProperty() {
        return dtserasa;
    }

    public void setDtserasa(LocalDate dtserasa) {
        this.dtserasa.set(dtserasa);
    }

    @Column(name = "DT_ENVIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtenvio() {
        return dtenvio.get();
    }

    public ObjectProperty<LocalDate> dtenvioProperty() {
        return dtenvio;
    }

    public void setDtenvio(LocalDate dtenvio) {
        this.dtenvio.set(dtenvio);
    }

    @Column(name = "COM3")
    public BigDecimal getCom3() {
        return com3.get();
    }

    public ObjectProperty<BigDecimal> com3Property() {
        return com3;
    }

    public void setCom3(BigDecimal com3) {
        this.com3.set(com3);
    }

    @Column(name = "COM4")
    public BigDecimal getCom4() {
        return com4.get();
    }

    public ObjectProperty<BigDecimal> com4Property() {
        return com4;
    }

    public void setCom4(BigDecimal com4) {
        this.com4.set(com4);
    }

    @Column(name = "MD5")
    public String getMd5() {
        return md5.get();
    }

    public StringProperty md5Property() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5.set(md5);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "DT_PREVISAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtprevisao() {
        return dtprevisao.get();
    }

    public ObjectProperty<LocalDate> dtprevisaoProperty() {
        return dtprevisao;
    }

    public void setDtprevisao(LocalDate dtprevisao) {
        this.dtprevisao.set(dtprevisao);
    }

    @Column(name = "DT_STATUS")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtstatus() {
        return dtstatus.get();
    }

    public ObjectProperty<LocalDate> dtstatusProperty() {
        return dtstatus;
    }

    public void setDtstatus(LocalDate dtstatus) {
        this.dtstatus.set(dtstatus);
    }

    @Column(name = "HISTORICO")
    public String getHistorico() {
        return historico.get();
    }

    public StringProperty historicoProperty() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico.set(historico);
    }

    @Column(name = "VAL_SUBST")
    public BigDecimal getValsubst() {
        return valsubst.get();
    }

    public ObjectProperty<BigDecimal> valsubstProperty() {
        return valsubst;
    }

    public void setValsubst(BigDecimal valsubst) {
        this.valsubst.set(valsubst);
    }

    @Column(name = "NUMERO_CH")
    public String getNumeroch() {
        return numeroch.get();
    }

    public StringProperty numerochProperty() {
        return numeroch;
    }

    public void setNumeroch(String numeroch) {
        this.numeroch.set(numeroch);
    }

    @Column(name = "CONTA_CH")
    public String getContach() {
        return contach.get();
    }

    public StringProperty contachProperty() {
        return contach;
    }

    public void setContach(String contach) {
        this.contach.set(contach);
    }

    @Column(name = "AGENCIA_CH")
    public String getAgenciach() {
        return agenciach.get();
    }

    public StringProperty agenciachProperty() {
        return agenciach;
    }

    public void setAgenciach(String agenciach) {
        this.agenciach.set(agenciach);
    }

    @Column(name = "EMISSOR_CH")
    public String getEmissorch() {
        return emissorch.get();
    }

    public StringProperty emissorchProperty() {
        return emissorch;
    }

    public void setEmissorch(String emissorch) {
        this.emissorch.set(emissorch);
    }

    @Column(name = "BANCO_CH")
    public String getBancoch() {
        return bancoch.get();
    }

    public StringProperty bancochProperty() {
        return bancoch;
    }

    public void setBancoch(String bancoch) {
        this.bancoch.set(bancoch);
    }

    @Column(name = "LANCAMENTO")
    public String getLancamento() {
        return lancamento.get();
    }

    public StringProperty lancamentoProperty() {
        return lancamento;
    }

    public void setLancamento(String lancamento) {
        this.lancamento.set(lancamento);
    }

    @Column(name = "USU_NUMREG")
    public BigDecimal getUsunumreg() {
        return usunumreg.get();
    }

    public ObjectProperty<BigDecimal> usunumregProperty() {
        return usunumreg;
    }

    public void setUsunumreg(BigDecimal usunumreg) {
        this.usunumreg.set(usunumreg);
    }

    @Column(name = "IMPORT_SAP")
    public String getImportsap() {
        return importsap.get();
    }

    public StringProperty importsapProperty() {
        return importsap;
    }

    public void setImportsap(String importsap) {
        this.importsap.set(importsap);
    }

    @Column(name = "DATA_DAG")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDatadag() {
        return datadag.get();
    }

    public ObjectProperty<LocalDate> datadagProperty() {
        return datadag;
    }

    public void setDatadag(LocalDate datadag) {
        this.datadag.set(datadag);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI", insertable = false, updatable = false)
    public VSdDadosEntidade getEntidade() {
        return entidade.get();
    }

    public ObjectProperty<VSdDadosEntidade> entidadeProperty() {
        return entidade;
    }

    public void setEntidade(VSdDadosEntidade entidade) {
        this.entidade.set(entidade);
    }


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP", referencedColumnName = "CODREP", insertable = false, updatable = false)
    public Represen getRepresentante() {
        return representante.get();
    }

    public ObjectProperty<Represen> representanteProperty() {
        return representante;
    }

    public void setRepresentante(Represen representante) {
        this.representante.set(representante);
    }

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO")
//    public VSdTitulosContrato getTituloContrato() {
//        return tituloContrato.get();
//    }
//
//    public ObjectProperty<VSdTitulosContrato> tituloContratoProperty() {
//        return tituloContrato;
//    }
//
//    public void setTituloContrato(VSdTitulosContrato tituloContrato) {
//        this.tituloContrato.set(tituloContrato);
//    }


    @Transient
    public BigDecimal getValorTitulo() {
        return valorTitulo.get();
    }

    public ObjectProperty<BigDecimal> valorTituloProperty() {
        return valorTitulo;
    }

    public void setValorTitulo(BigDecimal valorTitulo) {
        this.valorTitulo.set(valorTitulo);
    }

    @PostLoad
    private void setValorTituloPL() {
        this.setValorTitulo(this.getValor2().add(this.getJuros()).subtract(this.getValorpago().add(this.getDesconto().add(this.getValdev()))));
    }

    @Transient
    public BigDecimal getValorRecebido() {
        return valorRecebido.get();
    }

    public ObjectProperty<BigDecimal> valorRecebidoProperty() {
        return valorRecebido;
    }

    public void setValorRecebido(BigDecimal valorRecebido) {
        this.valorRecebido.set(valorRecebido);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receber receber = (Receber) o;
        return Objects.equals(numero, receber.numero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero);
    }
}