package sysdeliz2.models.ti;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "RECEBERB_001")
public class ReceberB implements Serializable {
    private final ObjectProperty<BigDecimal> qtdev = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtpagto = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtcont = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> valor2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpago = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> juros = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valdev = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty comissao = new SimpleStringProperty();
    private final StringProperty lancamento = new SimpleStringProperty();
    private final StringProperty classe = new SimpleStringProperty();
    private final StringProperty historico = new SimpleStringProperty();
    private final StringProperty docto = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> desconto2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> desconto3 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtcomissao = new SimpleObjectProperty<LocalDate>();
    private final StringProperty portador = new SimpleStringProperty();
    private final IntegerProperty usunumreg = new SimpleIntegerProperty();
    private final StringProperty importsap = new SimpleStringProperty();


    public ReceberB() {
    }

    @Column(name = "QT_DEV")
    public BigDecimal getQtdev() {
        return qtdev.get();
    }

    public ObjectProperty<BigDecimal> qtdevProperty() {
        return qtdev;
    }

    public void setQtdev(BigDecimal qtdev) {
        this.qtdev.set(qtdev);
    }

    @Column(name = "DT_PAGTO")
    public LocalDate getDtpagto() {
        return dtpagto.get();
    }

    public ObjectProperty<LocalDate> dtpagtoProperty() {
        return dtpagto;
    }

    public void setDtpagto(LocalDate dtpagto) {
        this.dtpagto.set(dtpagto);
    }

    @Column(name = "DT_CONT")
    public LocalDate getDtcont() {
        return dtcont.get();
    }

    public ObjectProperty<LocalDate> dtcontProperty() {
        return dtcont;
    }

    public void setDtcont(LocalDate dtcont) {
        this.dtcont.set(dtcont);
    }

    @Column(name = "VALOR2")
    public BigDecimal getValor2() {
        return valor2.get();
    }

    public ObjectProperty<BigDecimal> valor2Property() {
        return valor2;
    }

    public void setValor2(BigDecimal valor2) {
        this.valor2.set(valor2);
    }

    @Column(name = "VALOR_PAGO")
    public BigDecimal getValorpago() {
        return valorpago.get();
    }

    public ObjectProperty<BigDecimal> valorpagoProperty() {
        return valorpago;
    }

    public void setValorpago(BigDecimal valorpago) {
        this.valorpago.set(valorpago);
    }

    @Column(name = "JUROS")
    public BigDecimal getJuros() {
        return juros.get();
    }

    public ObjectProperty<BigDecimal> jurosProperty() {
        return juros;
    }

    public void setJuros(BigDecimal juros) {
        this.juros.set(juros);
    }

    @Column(name = "VAL_DEV")
    public BigDecimal getValdev() {
        return valdev.get();
    }

    public ObjectProperty<BigDecimal> valdevProperty() {
        return valdev;
    }

    public void setValdev(BigDecimal valdev) {
        this.valdev.set(valdev);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "COMISSAO")
    public String getComissao() {
        return comissao.get();
    }

    public StringProperty comissaoProperty() {
        return comissao;
    }

    public void setComissao(String comissao) {
        this.comissao.set(comissao);
    }

    @Id
    @Column(name = "LANCAMENTO")
    public String getLancamento() {
        return lancamento.get();
    }

    public StringProperty lancamentoProperty() {
        return lancamento;
    }

    public void setLancamento(String lancamento) {
        this.lancamento.set(lancamento);
    }

    @Column(name = "CLASSE")
    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    @Column(name = "HISTORICO")
    public String getHistorico() {
        return historico.get();
    }

    public StringProperty historicoProperty() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico.set(historico);
    }

    @Column(name = "DOCTO")
    public String getDocto() {
        return docto.get();
    }

    public StringProperty doctoProperty() {
        return docto;
    }

    public void setDocto(String docto) {
        this.docto.set(docto);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "DESCONTO2")
    public BigDecimal getDesconto2() {
        return desconto2.get();
    }

    public ObjectProperty<BigDecimal> desconto2Property() {
        return desconto2;
    }

    public void setDesconto2(BigDecimal desconto2) {
        this.desconto2.set(desconto2);
    }

    @Column(name = "DESCONTO3")
    public BigDecimal getDesconto3() {
        return desconto3.get();
    }

    public ObjectProperty<BigDecimal> desconto3Property() {
        return desconto3;
    }

    public void setDesconto3(BigDecimal desconto3) {
        this.desconto3.set(desconto3);
    }

    @Column(name = "DT_COMISSAO")
    public LocalDate getDtcomissao() {
        return dtcomissao.get();
    }

    public ObjectProperty<LocalDate> dtcomissaoProperty() {
        return dtcomissao;
    }

    public void setDtcomissao(LocalDate dtcomissao) {
        this.dtcomissao.set(dtcomissao);
    }

    @Column(name = "PORTADOR")
    public String getPortador() {
        return portador.get();
    }

    public StringProperty portadorProperty() {
        return portador;
    }

    public void setPortador(String portador) {
        this.portador.set(portador);
    }

    @Column(name = "USU_NUMREG")
    public Integer getUsunumreg() {
        return usunumreg.get();
    }

    public IntegerProperty usunumregProperty() {
        return usunumreg;
    }

    public void setUsunumreg(Integer usunumreg) {
        this.usunumreg.set(usunumreg);
    }

    @Column(name = "IMPORT_SAP")
    public String getImportsap() {
        return importsap.get();
    }

    public StringProperty importsapProperty() {
        return importsap;
    }

    public void setImportsap(String importsap) {
        this.importsap.set(importsap);
    }
}
