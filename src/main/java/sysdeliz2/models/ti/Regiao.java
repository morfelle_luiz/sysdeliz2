package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "REGIAO_001")
@TelaSysDeliz(descricao = "Tabela Preço", icon = "tabela preco (4).png")
public class Regiao extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "regiao")
    private final StringProperty regiao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Código", width = 300)
    @ColunaFilter(descricao = "Código", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final BooleanProperty padrao = new SimpleBooleanProperty();
    @Transient
    private final ObjectProperty<BigDecimal> indice = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Liberado", width = 50)
    @ColunaFilter(descricao = "Liberado", coluna = "liberado")
    private final BooleanProperty liberado = new SimpleBooleanProperty();
    @Transient
    private final StringProperty obs2503 = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty obs = new SimpleStringProperty();
    @Transient
    private final StringProperty sdRegiao = new SimpleStringProperty();
    @Transient
    private final StringProperty sdSubstituta = new SimpleStringProperty();
    @Transient
    private final BooleanProperty sdAtivo = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty usaReserva = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty usaMkt = new SimpleBooleanProperty();
    @Transient
    private final BooleanProperty usaFv = new SimpleBooleanProperty();

    public Regiao() {
    }

    @Id
    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        setCodigoFilter(regiao);
        this.regiao.set(regiao);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "PADRAO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPadrao() {
        return padrao.get();
    }

    public BooleanProperty padraoProperty() {
        return padrao;
    }

    public void setPadrao(boolean padrao) {
        this.padrao.set(padrao);
    }


    @Column(name = "INDICE")
    public BigDecimal getIndice() {
        return indice.get();
    }

    public ObjectProperty<BigDecimal> indiceProperty() {
        return indice;
    }

    public void setIndice(BigDecimal indice) {
        this.indice.set(indice);
    }

    @Column(name = "LIBERADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLiberado() {
        return liberado.get();
    }

    public BooleanProperty liberadoProperty() {
        return liberado;
    }

    public void setLiberado(boolean liberado) {
        this.liberado.set(liberado);
    }

    @Column(name = "OBS_2503")
    public String getObs2503() {
        return obs2503.get();
    }

    public StringProperty obs2503Property() {
        return obs2503;
    }

    public void setObs2503(String obs2503) {
        this.obs2503.set(obs2503);
    }

    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }

    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }

    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }

    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }

    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }

    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }

    @Lob
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "SD_REGIAO")
    public String getSdRegiao() {
        return sdRegiao.get();
    }

    public StringProperty sdRegiaoProperty() {
        return sdRegiao;
    }

    public void setSdRegiao(String sdRegiao) {
        this.sdRegiao.set(sdRegiao);
    }

    @Column(name = "SD_SUBSTITUTA")
    public String getSdSubstituta() {
        return sdSubstituta.get();
    }

    public StringProperty sdSubstitutaProperty() {
        return sdSubstituta;
    }

    public void setSdSubstituta(String sdSubstituta) {
        this.sdSubstituta.set(sdSubstituta);
    }

    @Column(name = "SD_ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSdAtivo() {
        return sdAtivo.get();
    }

    public BooleanProperty sdAtivoProperty() {
        return sdAtivo;
    }

    public void setSdAtivo(boolean sdAtivo) {
        this.sdAtivo.set(sdAtivo);
    }
    
    
    @Column(name = "USA_MKT")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isUsaMkt() {
        return usaMkt.get();
    }
    
    public BooleanProperty usaMktProperty() {
        return usaMkt;
    }
    
    public void setUsaMkt(boolean usaMkt) {
        this.usaMkt.set(usaMkt);
    }
    

    @Column(name = "USA_RESERVA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isUsaReserva() {
        return usaReserva.get();
    }

    public BooleanProperty usaReservaProperty() {
        return usaReserva;
    }

    public void setUsaReserva(boolean usaReserva) {
        this.usaReserva.set(usaReserva);
    }

    @Column(name = "USA_FV")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isUsaFv() {
        return usaFv.get();
    }

    public BooleanProperty usaFvProperty() {
        return usaFv;
    }

    public void setUsaFv(boolean usaFv) {
        this.usaFv.set(usaFv);
    }

    @Override
    public String toString() {
        return "[" + regiao.get() + "] " + descricao.get();
    }


}
