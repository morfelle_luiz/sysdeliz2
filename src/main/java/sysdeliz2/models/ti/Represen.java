package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdMarcasRepresen;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;
import sysdeliz2.utils.validator.bean.annotation.CpfOuCnpj;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 31/07/2019 16:08
 */
@Entity
@Table(name = "REPRESEN_001")
@TelaSysDeliz(descricao = "Representante", icon = "representante (4).png")
@SuppressWarnings("unused")
public class Represen extends BasicModel implements Serializable, Cloneable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codRep")
    private final StringProperty codRep = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty();

    @Transient
    private final ObjectProperty<CadCep> cep = new SimpleObjectProperty<>();

    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    @ColunaFilter(descricao = "Ativo", coluna = "ativo")
    private final BooleanProperty ativo = new SimpleBooleanProperty();

    @Transient
    @ColunaFilter(descricao = "Região", coluna = "regiao")
    private final StringProperty regiao = new SimpleStringProperty();

    @Transient
    @ColunaFilter(descricao = "CNPJ", coluna = "CNPJ")
    @CpfOuCnpj(message = "Campo CNPJ/CPF não é válido")
    private final StringProperty cnpj = new SimpleStringProperty();

    @Transient
    private final StringProperty cpf = new SimpleStringProperty();

    @Transient
    private final ObjectProperty<BigDecimal> comissaoFat = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> comissaoRec = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> comissaoFat2 = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> comissaoRec2 = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty telefone = new SimpleStringProperty();
    @Transient
    private final StringProperty fax = new SimpleStringProperty();
    @Transient
    private final StringProperty inscricao = new SimpleStringProperty();
    @Transient
    private final StringProperty endereco = new SimpleStringProperty();

    @Transient
    @ExibeTableView(descricao = "Responsável", width = 80)
    @ColunaFilter(descricao = "Responsável", coluna = "respon")
    private final StringProperty respon = new SimpleStringProperty();

    @Transient
    private final StringProperty obs = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Email", width = 80)
    @ColunaFilter(descricao = "Email", coluna = "email")
    private final StringProperty email = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> impRenda = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty bairro = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<LocalDate> dtNascto = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty codCli = new SimpleStringProperty();
    @Transient
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<LocalDate> admissao = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty grupo = new SimpleStringProperty("3");
    @Transient
    private final StringProperty celular = new SimpleStringProperty();

    private List<SdMarcasRepresen> marcas = new ArrayList<>();

    public Represen() {
    }

    @Id
    @Column(name = "CODREP")
    public String getCodRep() {
        return codRep.get();
    }

    public StringProperty codRepProperty() {
        return codRep;
    }

    public void setCodRep(String codRep) {
        setCodigoFilter(codRep);
        this.codRep.set(codRep);
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        setDescricaoFilter(nome);
        this.nome.set(nome);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CEP")
    public CadCep getCep() {
        return cep.get();
    }

    public ObjectProperty<CadCep> cepProperty() {
        return cep;
    }

    public void setCep(CadCep cep) {
        this.cep.set(cep);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }

    @Column(name = "CNPJ")
    public String getCnpj() {
        return cnpj.get();
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }

    @Column(name = "CPF")
    public String getCpf() {
        return cpf.get();
    }

    public StringProperty cpfProperty() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf.set(cpf);
    }

    @Column(name = "COMISSAO")
    public BigDecimal getComissaoFat() {
        return comissaoFat.get();
    }

    public ObjectProperty<BigDecimal> comissaoFatProperty() {
        return comissaoFat;
    }

    public void setComissaoFat(BigDecimal comissaoFat) {
        this.comissaoFat.set(comissaoFat);
    }

    @Column(name = "COMISSAO2")
    public BigDecimal getComissaoRec() {
        return comissaoRec.get();
    }

    public ObjectProperty<BigDecimal> comissaoRecProperty() {
        return comissaoRec;
    }

    public void setComissaoRec(BigDecimal comissaoRec) {
        this.comissaoRec.set(comissaoRec);
    }

    @Column(name = "COMISSAO3")
    public BigDecimal getComissaoFat2() {
        return comissaoFat2.get();
    }

    public ObjectProperty<BigDecimal> comissaoFat2Property() {
        return comissaoFat2;
    }

    public void setComissaoFat2(BigDecimal comissaoFat2) {
        this.comissaoFat2.set(comissaoFat2);
    }

    @Column(name = "COMISSAO4")
    public BigDecimal getComissaoRec2() {
        return comissaoRec2.get();
    }

    public ObjectProperty<BigDecimal> comissaoRec2Property() {
        return comissaoRec2;
    }

    public void setComissaoRec2(BigDecimal comissaoRec2) {
        this.comissaoRec2.set(comissaoRec2);
    }

    @Column(name = "TELEFONE")
    public String getTelefone() {
        return telefone.get();
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }

    @Column(name = "FAX")
    public String getFax() {
        return fax.get();
    }

    public StringProperty faxProperty() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax.set(fax);
    }

    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }

    public StringProperty inscricaoProperty() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }

    @Column(name = "ENDERECO")
    public String getEndereco() {
        return endereco.get();
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco.set(endereco);
    }

    @Column(name = "RESPON")
    public String getRespon() {
        return respon.get();
    }

    public StringProperty responProperty() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon.set(respon);
    }

    @Lob
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Column(name = "IMP_RENDA")
    public BigDecimal getImpRenda() {
        return impRenda.get();
    }

    public ObjectProperty<BigDecimal> impRendaProperty() {
        return impRenda;
    }

    public void setImpRenda(BigDecimal impRenda) {
        this.impRenda.set(impRenda);
    }

    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }

    @Column(name = "DTNASCTO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtNascto() {
        return dtNascto.get();
    }

    public ObjectProperty<LocalDate> dtNasctoProperty() {
        return dtNascto;
    }

    public void setDtNascto(LocalDate dtNascto) {
        this.dtNascto.set(dtNascto);
    }

    @Column(name = "CODCLI")
    public String getCodCli() {
        return codCli.get();
    }

    public StringProperty codCliProperty() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli.set(codCli);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ADMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getAdmissao() {
        return admissao.get();
    }

    public ObjectProperty<LocalDate> admissaoProperty() {
        return admissao;
    }

    public void setAdmissao(LocalDate admissao) {
        this.admissao.set(admissao);
    }

    // Sempre preencher com 3 = Representante
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "CELULAR")
    public String getCelular() {
        return celular.get();
    }

    public StringProperty celularProperty() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular.set(celular);
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id.codrep")
    public List<SdMarcasRepresen> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<SdMarcasRepresen> marcas) {
        this.marcas = marcas;
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

    @Override
    public String toString() {
        return "[" + this.codRep.get() + "] " + this.nome.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Represen represen = (Represen) o;
        return Objects.equals(codRep, represen.codRep);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codRep);
    }

    @Override
    public Represen clone() {
        try {
            return (Represen) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}