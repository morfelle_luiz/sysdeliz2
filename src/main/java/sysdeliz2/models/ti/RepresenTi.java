package sysdeliz2.models.ti;

import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lima.joao
 * @since 31/07/2019 16:08
 */
@Entity
@Table(name = "REPRESEN_001")
@TelaSysDeliz(descricao = "Representante", icon = "representante (4).png")
@SuppressWarnings("unused")
@Deprecated
public class RepresenTi extends BasicModel {
    
    @Id
    @Column(name = "CODREP")
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codRep")
    private String codRep;
    @Column(name = "NOME")
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private String nome;

    @Column(name = "CEP")
    private String cep;

    @Column(name = "ATIVO")
    @ExibeTableView(descricao = "Ativo", width = 40)
    @ColunaFilter(descricao = "Ativo", coluna = "ativo")
    private String ativo;

    @Column(name = "REGIAO")
    @ColunaFilter(descricao = "Região", coluna = "regiao")
    private String regiao;

    @Column(name = "CNPJ")
    @ColunaFilter(descricao = "CNPJ", coluna = "CNPJ")
    private String cnpj;

    @Column(name = "CPF")
    private String cpf;

    @Column(name = "COMISSAO")
    private BigDecimal comissaoFat;
    @Column(name = "COMISSAO2")
    private BigDecimal comissaoRec;
    @Column(name = "COMISSAO3")
    private BigDecimal comissaoFat2;
    @Column(name = "COMISSAO4")
    private BigDecimal comissaoRec2;
    @Column(name = "TELEFONE")
    private String telefone;
    @Column(name = "FAX")
    private String fax;
    @Column(name = "INSCRICAO")
    private String inscricao;
    @Column(name = "ENDERECO")
    private String endereco;

    @Column(name = "RESPON")
    @ExibeTableView(descricao = "Responsável", width = 80)
    @ColunaFilter(descricao = "Responsável", coluna = "respon")
    private String respon;

    @Lob
    @Column(name = "OBS")
    private String obs;
    @Column(name = "EMAIL")
    @ExibeTableView(descricao = "Email", width = 80)
    @ColunaFilter(descricao = "Email", coluna = "email")
    private String email;
    @Column(name = "IMP_RENDA")
    private BigDecimal impRenda;
    @Column(name = "BAIRRO")
    private String bairro;
    @Column(name = "DTNASCTO")
    @Temporal(TemporalType.DATE)
    private Date dtNascto;
    @Column(name = "CODCLI")
    private String codCli;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "ADMISSAO")
    @Temporal(TemporalType.DATE)
    private Date admissao;
    // Sempre preencher com 3 = Representante
    @Column(name = "GRUPO")
    private String grupo;
    @Column(name = "CELULAR")
    private String celular;

    public String getCodRep() {
        return codRep;
    }

    public void setCodRep(String codRep) {
        setCodigoFilter(codRep);
        this.codRep = codRep;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public BigDecimal getComissaoFat() {
        return comissaoFat;
    }

    public void setComissaoFat(BigDecimal comissaoFat) {
        this.comissaoFat = comissaoFat;
    }

    public BigDecimal getComissaoRec() {
        return comissaoRec;
    }

    public void setComissaoRec(BigDecimal comissaoRec) {
        this.comissaoRec = comissaoRec;
    }

    public BigDecimal getComissaoFat2() {
        return comissaoFat2;
    }

    public void setComissaoFat2(BigDecimal comissaoFat2) {
        this.comissaoFat2 = comissaoFat2;
    }

    public BigDecimal getComissaoRec2() {
        return comissaoRec2;
    }

    public void setComissaoRec2(BigDecimal comissaoRec2) {
        this.comissaoRec2 = comissaoRec2;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        setDescricaoFilter(nome);
        this.nome = nome;
    }

    public BigDecimal getImpRenda() {
        return impRenda;
    }

    public void setImpRenda(BigDecimal impRenda) {
        this.impRenda = impRenda;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Date getDtNascto() {
        return dtNascto;
    }

    public void setDtNascto(Date dtNascto) {
        this.dtNascto = dtNascto;
    }

    public String getCodCli() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli = codCli;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getAdmissao() {
        return admissao;
    }

    public void setAdmissao(Date admissao) {
        this.admissao = admissao;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
    
    @Override
    public String toString() {
        return "[" + codRep + "] "+ nome;
    }
}