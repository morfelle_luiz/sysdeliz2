package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SUBGRUPO_MA_001")
@TelaSysDeliz(descricao = "Subgrupos", icon = "subgrupo_50.png")
public class SubGrupoMa extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Grupo", width = 50)
    private final StringProperty grupo = new SimpleStringProperty();
    
    public SubGrupoMa() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Override
    public String toString(){
        return "["+this.codigo.get()+"] "+this.descricao.get();
    }
}
