package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "TABFIS_001")
@Entity
@TelaSysDeliz(descricao = "TabFis", icon = "fragment_100.png")
public class TabFis extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Descrição", width = 130)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @ExibeTableView(descricao = "Tipo", width = 400)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> icms = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> ipi = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty padrao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> percii = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> ibpt = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty cest = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty indescala = new SimpleStringProperty();
    private final StringProperty extipi = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pis = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> cofins = new SimpleObjectProperty<BigDecimal>();

    public TabFis() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "ICMS")
    public BigDecimal getIcms() {
        return icms.get();
    }

    public ObjectProperty<BigDecimal> icmsProperty() {
        return icms;
    }

    public void setIcms(BigDecimal icms) {
        this.icms.set(icms);
    }

    @Column(name = "IPI")
    public BigDecimal getIpi() {
        return ipi.get();
    }

    public ObjectProperty<BigDecimal> ipiProperty() {
        return ipi;
    }

    public void setIpi(BigDecimal ipi) {
        this.ipi.set(ipi);
    }

    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "PADRAO")
    public String getPadrao() {
        return padrao.get();
    }

    public StringProperty padraoProperty() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao.set(padrao);
    }

    @Column(name = "PERC_II")
    public BigDecimal getPercii() {
        return percii.get();
    }

    public ObjectProperty<BigDecimal> perciiProperty() {
        return percii;
    }

    public void setPercii(BigDecimal percii) {
        this.percii.set(percii);
    }

    @Column(name = "IBPT")
    public BigDecimal getIbpt() {
        return ibpt.get();
    }

    public ObjectProperty<BigDecimal> ibptProperty() {
        return ibpt;
    }

    public void setIbpt(BigDecimal ibpt) {
        this.ibpt.set(ibpt);
    }

    @Column(name = "CEST")
    public String getCest() {
        return cest.get();
    }

    public StringProperty cestProperty() {
        return cest;
    }

    public void setCest(String cest) {
        this.cest.set(cest);
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }

    @Column(name = "IND_ESCALA")
    public String getIndescala() {
        return indescala.get();
    }

    public StringProperty indescalaProperty() {
        return indescala;
    }

    public void setIndescala(String indescala) {
        this.indescala.set(indescala);
    }

    @Column(name = "EXTIPI")
    public String getExtipi() {
        return extipi.get();
    }

    public StringProperty extipiProperty() {
        return extipi;
    }

    public void setExtipi(String extipi) {
        this.extipi.set(extipi);
    }

    @Column(name = "PIS")
    public BigDecimal getPis() {
        return pis.get();
    }

    public ObjectProperty<BigDecimal> pisProperty() {
        return pis;
    }

    public void setPis(BigDecimal pis) {
        this.pis.set(pis);
    }

    @Column(name = "COFINS")
    public BigDecimal getCofins() {
        return cofins.get();
    }

    public ObjectProperty<BigDecimal> cofinsProperty() {
        return cofins;
    }

    public void setCofins(BigDecimal cofins) {
        this.cofins.set(cofins);
    }

}

