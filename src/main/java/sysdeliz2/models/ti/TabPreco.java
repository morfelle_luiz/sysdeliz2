package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "TABPRECO_001")
public class TabPreco {

    private final ObjectProperty<TabPrecoPK> id = new SimpleObjectProperty<>();
    private final StringProperty codigo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdeMin = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> preco00 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco07 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco14 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco21 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco28 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco35 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco42 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco49 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco56 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco63 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco70 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco77 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco84 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> preco91 = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> precoMedio = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty descProduto = new SimpleStringProperty();
    private final StringProperty variante = new SimpleStringProperty("0");
    private final ObjectProperty<BigDecimal> margem = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty grupoCor = new SimpleStringProperty("00");
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty cor = new SimpleStringProperty("0");
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty liberado = new SimpleStringProperty();
    private final StringProperty faixaPreco = new SimpleStringProperty("00");
    private final ObjectProperty<BigDecimal> ultPreco = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty promocao = new SimpleStringProperty();
    private final StringProperty servico = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtPromo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> precoPromo = new SimpleObjectProperty<>(BigDecimal.ZERO);

    public TabPreco() {
    }

    public TabPreco(VSdDadosProduto produto, Regiao regiao) {
        this.id.set(new TabPrecoPK(produto, regiao));
        this.descricao.set(regiao.getDescricao());
        this.preco00.setValue(BigDecimal.ZERO);
        this.descProduto.set(produto.getDescricao());
        this.data.set(LocalDate.now());
    }

    @EmbeddedId
    public TabPrecoPK getId() {
        return id.get();
    }

    public ObjectProperty<TabPrecoPK> idProperty() {
        return id;
    }

    public void setId(TabPrecoPK id) {
        this.id.set(id);
    }

    @Column(name = "CODIGO", insertable = false, updatable = false)
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "QTDE_MIN")
    public BigDecimal getQtdeMin() {
        return qtdeMin.get();
    }

    public ObjectProperty<BigDecimal> qtdeMinProperty() {
        return qtdeMin;
    }

    public void setQtdeMin(BigDecimal qtdeMin) {
        this.qtdeMin.set(qtdeMin);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "PRECO_00")
    public BigDecimal getPreco00() {
        return preco00.get();
    }

    public ObjectProperty<BigDecimal> preco00Property() {
        return preco00;
    }

    public void setPreco00(BigDecimal preco00) {
        this.preco00.set(preco00);
    }

    @Column(name = "PRECO_07")
    public BigDecimal getPreco07() {
        return preco07.get();
    }

    public ObjectProperty<BigDecimal> preco07Property() {
        return preco07;
    }

    public void setPreco07(BigDecimal preco07) {
        this.preco07.set(preco07);
    }

    @Column(name = "PRECO_14")
    public BigDecimal getPreco14() {
        return preco14.get();
    }

    public ObjectProperty<BigDecimal> preco14Property() {
        return preco14;
    }

    public void setPreco14(BigDecimal preco14) {
        this.preco14.set(preco14);
    }

    @Column(name = "PRECO_21")
    public BigDecimal getPreco21() {
        return preco21.get();
    }

    public ObjectProperty<BigDecimal> preco21Property() {
        return preco21;
    }

    public void setPreco21(BigDecimal preco21) {
        this.preco21.set(preco21);
    }

    @Column(name = "PRECO_28")
    public BigDecimal getPreco28() {
        return preco28.get();
    }

    public ObjectProperty<BigDecimal> preco28Property() {
        return preco28;
    }

    public void setPreco28(BigDecimal preco28) {
        this.preco28.set(preco28);
    }

    @Column(name = "PRECO_35")
    public BigDecimal getPreco35() {
        return preco35.get();
    }

    public ObjectProperty<BigDecimal> preco35Property() {
        return preco35;
    }

    public void setPreco35(BigDecimal preco35) {
        this.preco35.set(preco35);
    }

    @Column(name = "PRECO_42")
    public BigDecimal getPreco42() {
        return preco42.get();
    }

    public ObjectProperty<BigDecimal> preco42Property() {
        return preco42;
    }

    public void setPreco42(BigDecimal preco42) {
        this.preco42.set(preco42);
    }

    @Column(name = "PRECO_49")
    public BigDecimal getPreco49() {
        return preco49.get();
    }

    public ObjectProperty<BigDecimal> preco49Property() {
        return preco49;
    }

    public void setPreco49(BigDecimal preco49) {
        this.preco49.set(preco49);
    }

    @Column(name = "PRECO_56")
    public BigDecimal getPreco56() {
        return preco56.get();
    }

    public ObjectProperty<BigDecimal> preco56Property() {
        return preco56;
    }

    public void setPreco56(BigDecimal preco56) {
        this.preco56.set(preco56);
    }

    @Column(name = "PRECO_63")
    public BigDecimal getPreco63() {
        return preco63.get();
    }

    public ObjectProperty<BigDecimal> preco63Property() {
        return preco63;
    }

    public void setPreco63(BigDecimal preco63) {
        this.preco63.set(preco63);
    }

    @Column(name = "PRECO_70")
    public BigDecimal getPreco70() {
        return preco70.get();
    }

    public ObjectProperty<BigDecimal> preco70Property() {
        return preco70;
    }

    public void setPreco70(BigDecimal preco70) {
        this.preco70.set(preco70);
    }

    @Column(name = "PRECO_77")
    public BigDecimal getPreco77() {
        return preco77.get();
    }

    public ObjectProperty<BigDecimal> preco77Property() {
        return preco77;
    }

    public void setPreco77(BigDecimal preco77) {
        this.preco77.set(preco77);
    }

    @Column(name = "PRECO_84")
    public BigDecimal getPreco84() {
        return preco84.get();
    }

    public ObjectProperty<BigDecimal> preco84Property() {
        return preco84;
    }

    public void setPreco84(BigDecimal preco84) {
        this.preco84.set(preco84);
    }

    @Column(name = "PRECO_91")
    public BigDecimal getPreco91() {
        return preco91.get();
    }

    public ObjectProperty<BigDecimal> preco91Property() {
        return preco91;
    }

    public void setPreco91(BigDecimal preco91) {
        this.preco91.set(preco91);
    }

    @Column(name = "PRECO_MEDIO")
    public BigDecimal getPrecoMedio() {
        return precoMedio.get();
    }

    public ObjectProperty<BigDecimal> precoMedioProperty() {
        return precoMedio;
    }

    public void setPrecoMedio(BigDecimal precoMedio) {
        this.precoMedio.set(precoMedio);
    }

    @Column(name = "DESC_PRODUTO")
    public String getDescProduto() {
        return descProduto.get();
    }

    public StringProperty descProdutoProperty() {
        return descProduto;
    }

    public void setDescProduto(String descProduto) {
        this.descProduto.set(descProduto);
    }

    @Column(name = "VARIANTE")
    public String getVariante() {
        return variante.get();
    }

    public StringProperty varianteProperty() {
        return variante;
    }

    public void setVariante(String variante) {
        this.variante.set(variante);
    }

    @Column(name = "MARGEN")
    public BigDecimal getMargem() {
        return margem.get();
    }

    public ObjectProperty<BigDecimal> margemProperty() {
        return margem;
    }

    public void setMargem(BigDecimal margem) {
        this.margem.set(margem);
    }

    @Column(name = "GRUPO_COR")
    public String getGrupoCor() {
        return grupoCor.get();
    }

    public StringProperty grupoCorProperty() {
        return grupoCor;
    }

    public void setGrupoCor(String grupoCor) {
        this.grupoCor.set(grupoCor);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }

    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data.set(data);
    }

    @Column(name = "LIBERADO")
    public String getLiberado() {
        return liberado.get();
    }

    public StringProperty liberadoProperty() {
        return liberado;
    }

    public void setLiberado(String liberado) {
        this.liberado.set(liberado);
    }

    @Column(name = "FAIXA_PRECO")
    public String getFaixaPreco() {
        return faixaPreco.get();
    }

    public StringProperty faixaPrecoProperty() {
        return faixaPreco;
    }

    public void setFaixaPreco(String faixaPreco) {
        this.faixaPreco.set(faixaPreco);
    }

    @Column(name = "ULT_PRECO")
    public BigDecimal getUltPreco() {
        return ultPreco.get();
    }

    public ObjectProperty<BigDecimal> ultPrecoProperty() {
        return ultPreco;
    }

    public void setUltPreco(BigDecimal ultPreco) {
        this.ultPreco.set(ultPreco);
    }

    @Column(name = "PROMOCAO")
    public String getPromocao() {
        return promocao.get();
    }

    public StringProperty promocaoProperty() {
        return promocao;
    }

    public void setPromocao(String promocao) {
        this.promocao.set(promocao);
    }

    @Column(name = "SERVICO")
    public String getServico() {
        return servico.get();
    }

    public StringProperty servicoProperty() {
        return servico;
    }

    public void setServico(String servico) {
        this.servico.set(servico);
    }

    @Column(name = "DT_PROMO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtPromo() {
        return dtPromo.get();
    }

    public ObjectProperty<LocalDate> dtPromoProperty() {
        return dtPromo;
    }

    public void setDtPromo(LocalDate dtPromo) {
        this.dtPromo.set(dtPromo);
    }

    @Column(name = "PRECO_PROMO")
    public BigDecimal getPrecoPromo() {
        return precoPromo.get();
    }

    public ObjectProperty<BigDecimal> precoPromoProperty() {
        return precoPromo;
    }

    public void setPrecoPromo(BigDecimal precoPromo) {
        this.precoPromo.set(precoPromo);
    }

    @Override
    public String toString() {
        return "TabPreco{" +
                "id=" + id +
                ", qtdeMin=" + qtdeMin +
                ", descricao=" + descricao +
                ", preco00=" + preco00 +
                ", preco07=" + preco07 +
                ", preco14=" + preco14 +
                ", preco21=" + preco21 +
                ", preco28=" + preco28 +
                ", preco35=" + preco35 +
                ", preco42=" + preco42 +
                ", preco49=" + preco49 +
                ", preco56=" + preco56 +
                ", preco63=" + preco63 +
                ", preco70=" + preco70 +
                ", preco77=" + preco77 +
                ", preco84=" + preco84 +
                ", preco91=" + preco91 +
                ", precoMedio=" + precoMedio +
                ", descProduto=" + descProduto +
                ", variante=" + variante +
                ", margem=" + margem +
                ", grupoCor=" + grupoCor +
                ", desconto=" + desconto +
                ", cor=" + cor +
                ", data=" + data +
                ", liberado=" + liberado +
                ", faixaPreco=" + faixaPreco +
                ", ultPreco=" + ultPreco +
                ", promocao=" + promocao +
                ", servico=" + servico +
                ", dtPromo=" + dtPromo +
                ", precoPromo=" + precoPromo +
                '}';
    }
}
