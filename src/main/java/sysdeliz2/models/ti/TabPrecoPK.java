package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.view.VSdDadosProduto;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class TabPrecoPK implements Serializable {
    
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final StringProperty regiao = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public TabPrecoPK() {
    }

    public TabPrecoPK(VSdDadosProduto produto, Regiao regiao) {
        this.codigo.set(produto);
        this.regiao.set(regiao.getRegiao());
        this.tipo.set("P");
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }
    
    public StringProperty regiaoProperty() {
        return regiao;
    }
    
    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
}
