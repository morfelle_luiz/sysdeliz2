/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author cristiano.diego
 */
@Entity
@Table(name = "TABPRZ_001")
@TelaSysDeliz(descricao = "Período", icon = "periodo (4).png")
public class TabPrz extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Prazo", width = 60)
    @ColunaFilter(descricao = "Prazo", coluna = "prazo")
    private final StringProperty prazo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Dt Início", width = 80, columnType = ColumnType.DATE)
    private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Dt Fim", width = 80, columnType = ColumnType.DATE)
    private final ObjectProperty<LocalDate> dtFim = new SimpleObjectProperty<>();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 200)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final IntegerProperty minuto = new SimpleIntegerProperty(0);
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty(false);
    @Transient
    @ExibeTableView(descricao = "Tipo", width = 40)
    @ColunaFilter(descricao = "Tipo", coluna = "tipo")
    private final StringProperty tipo = new SimpleStringProperty();
    @Transient
    private final BooleanProperty faturaSeparado = new SimpleBooleanProperty(false);
    @Transient
    private final StringProperty tipoFaturamento = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty sdLinha = new SimpleStringProperty();


    public TabPrz() {
    }

    @Id
    @Column(name = "PRAZO")
    public String getPrazo() {
        return prazo.get();
    }

    public StringProperty prazoProperty() {
        return prazo;
    }

    public void setPrazo(String prazo) {
        this.setCodigoFilter(prazo);
        this.prazo.set(prazo);
    }

    @Column(name = "DT_FIM")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtFim() {
        return dtFim.get();
    }

    public ObjectProperty<LocalDate> dtFimProperty() {
        return dtFim;
    }

    public void setDtFim(LocalDate dtFim) {
        this.dtFim.set(dtFim);
    }

    @Column(name = "DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtInicio() {
        return dtInicio.get();
    }

    public ObjectProperty<LocalDate> dtInicioProperty() {
        return dtInicio;
    }

    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio.set(dtInicio);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "MINUTO")
    public int getMinuto() {
        return minuto.get();
    }

    public IntegerProperty minutoProperty() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto.set(minuto);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "FATURA_SEPARADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFaturaSeparado() {
        return faturaSeparado.get();
    }

    public BooleanProperty faturaSeparadoProperty() {
        return faturaSeparado;
    }

    public void setFaturaSeparado(boolean faturaSeparado) {
        this.faturaSeparado.set(faturaSeparado);
    }

    @Column(name = "TP_FATURAMENTO")
    public String getTipoFaturamento() {
        return tipoFaturamento.get();
    }

    public StringProperty tipoFaturamentoProperty() {
        return tipoFaturamento;
    }

    public void setTipoFaturamento(String tipoFaturamento) {
        this.tipoFaturamento.set(tipoFaturamento);
    }

    @OneToOne
    @JoinColumn(name = "COLECAO", referencedColumnName = "CODIGO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "SD_LINHA")
    public String getSdLinha() {
        return sdLinha.get();
    }

    public StringProperty sdLinhaProperty() {
        return sdLinha;
    }

    public void setSdLinha(String sdLinha) {
        this.sdLinha.set(sdLinha);
    }

    @Override
    public String toString() {
        return "[" + prazo.get() + "] " + descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabPrz tabPrz = (TabPrz) o;
        return Objects.equals(prazo, tabPrz.prazo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prazo);
    }

    public enum TipoLote {
        P("Produção"),
        E("Exceção"),
        V("Venda");

        private String descricao;

        TipoLote(String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.getDescricao();
        }
    }
}
