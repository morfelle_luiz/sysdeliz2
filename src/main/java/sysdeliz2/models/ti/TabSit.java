package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TABSIT_001")
@TelaSysDeliz(descricao = "Situação Duplicata", icon = "situacao duplicata (4).png")
public class TabSit extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Abreviação", width = 90)
    @ColunaFilter(descricao = "Abreviação", coluna = "abrevi")
    private final StringProperty abrevi = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final StringProperty padrao = new SimpleStringProperty();
    @Transient
    private final StringProperty pendente = new SimpleStringProperty();
    @Transient
    private final IntegerProperty dias = new SimpleIntegerProperty();
    @Transient
    private final StringProperty impFluxo = new SimpleStringProperty();
    @Transient
    private final StringProperty descontada = new SimpleStringProperty();
    
    public TabSit() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
        setCodigoFilter(codigo);
    }
    
    @Column(name = "ABREVI")
    public String getAbrevi() {
        return abrevi.get();
    }
    
    public StringProperty abreviProperty() {
        return abrevi;
    }
    
    public void setAbrevi(String abrevi) {
        this.abrevi.set(abrevi);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }
    
    @Column(name = "PADRAO")
    public String getPadrao() {
        return padrao.get();
    }
    
    public StringProperty padraoProperty() {
        return padrao;
    }
    
    public void setPadrao(String padrao) {
        this.padrao.set(padrao);
    }
    
    @Column(name = "PENDENTE")
    public String getPendente() {
        return pendente.get();
    }
    
    public StringProperty pendenteProperty() {
        return pendente;
    }
    
    public void setPendente(String pendente) {
        this.pendente.set(pendente);
    }
    
    @Column(name = "DIAS")
    public int getDias() {
        return dias.get();
    }
    
    public IntegerProperty diasProperty() {
        return dias;
    }
    
    public void setDias(int dias) {
        this.dias.set(dias);
    }
    
    @Column(name = "IMP_FLUXO")
    public String getImpFluxo() {
        return impFluxo.get();
    }
    
    public StringProperty impFluxoProperty() {
        return impFluxo;
    }
    
    public void setImpFluxo(String impFluxo) {
        this.impFluxo.set(impFluxo);
    }
    
    @Column(name = "DESCONTADA")
    public String getDescontada() {
        return descontada.get();
    }
    
    public StringProperty descontadaProperty() {
        return descontada;
    }
    
    public void setDescontada(String descontada) {
        this.descontada.set(descontada);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}
