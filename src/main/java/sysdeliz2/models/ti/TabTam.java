package sysdeliz2.models.ti;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TABTAM_001")
public class TabTam {

    private final IntegerProperty acrescimo = new SimpleIntegerProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty faixaPreco = new SimpleStringProperty();

    @Column(name = "ACRESCIMO")
    public int getAcrescimo() {
        return acrescimo.get();
    }

    public IntegerProperty acrescimoProperty() {
        return acrescimo;
    }

    public void setAcrescimo(int acrescimo) {
        this.acrescimo.set(acrescimo);
    }

    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    @Id
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "FAIXA_PRECO")
    public String getFaixaPreco() {
        return faixaPreco.get();
    }

    public StringProperty faixaPrecoProperty() {
        return faixaPreco;
    }

    public void setFaixaPreco(String faixaPreco) {
        this.faixaPreco.set(faixaPreco);
    }
}
