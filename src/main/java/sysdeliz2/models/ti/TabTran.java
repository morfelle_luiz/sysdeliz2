package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TABTRAN_001")
@TelaSysDeliz(descricao = "Transportadora", icon = "transportadora (4).png")
public class TabTran extends BasicModel implements Serializable {
    
    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<CadCep> cep = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty fax = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "CNPJ", width = 90)
    @ColunaFilter(descricao = "CNPJ", coluna = "cgc")
    private final StringProperty cgc = new SimpleStringProperty();
    @Transient
    private final StringProperty inscricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Nome", width = 300)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Telefone", width = 80)
    private final StringProperty telefone = new SimpleStringProperty();
    @Transient
    private final StringProperty endereco = new SimpleStringProperty();
    @Transient
    private final StringProperty email = new SimpleStringProperty();
    @Transient
    private final StringProperty observacao = new SimpleStringProperty();
    @Transient
    private final StringProperty contato = new SimpleStringProperty();
    @Transient
    private final StringProperty placa = new SimpleStringProperty();
    @Transient
    private final StringProperty bairro = new SimpleStringProperty();
    @Transient
    private final StringProperty rntc = new SimpleStringProperty();
    @Transient
    private final StringProperty codTran = new SimpleStringProperty();
    @Transient
    private final StringProperty ativo = new SimpleStringProperty();
    
    public TabTran() {
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CEP")
    public CadCep getCep() {
        return cep.get();
    }
    
    public ObjectProperty<CadCep> cepProperty() {
        return cep;
    }
    
    public void setCep(CadCep cep) {
        this.cep.set(cep);
    }
    
    @Column(name = "TELEFONE")
    public String getTelefone() {
        return telefone.get();
    }
    
    public StringProperty telefoneProperty() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }
    
    @Column(name = "FAX")
    public String getFax() {
        return fax.get();
    }
    
    public StringProperty faxProperty() {
        return fax;
    }
    
    public void setFax(String fax) {
        this.fax.set(fax);
    }
    
    @Column(name = "CGC")
    public String getCgc() {
        return cgc.get();
    }
    
    public StringProperty cgcProperty() {
        return cgc;
    }
    
    public void setCgc(String cgc) {
        this.cgc.set(cgc);
    }
    
    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }
    
    public StringProperty inscricaoProperty() {
        return inscricao;
    }
    
    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        setDescricaoFilter(nome);
        this.nome.set(nome);
    }
    
    @Column(name = "ENDERECO")
    public String getEndereco() {
        return endereco.get();
    }
    
    public StringProperty enderecoProperty() {
        return endereco;
    }
    
    public void setEndereco(String endereco) {
        this.endereco.set(endereco);
    }
    
    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email.set(email);
    }
    
    @Lob
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "CONTATO")
    public String getContato() {
        return contato.get();
    }
    
    public StringProperty contatoProperty() {
        return contato;
    }
    
    public void setContato(String contato) {
        this.contato.set(contato);
    }
    
    @Column(name = "PLACA")
    public String getPlaca() {
        return placa.get();
    }
    
    public StringProperty placaProperty() {
        return placa;
    }
    
    public void setPlaca(String placa) {
        this.placa.set(placa);
    }
    
    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }
    
    public StringProperty bairroProperty() {
        return bairro;
    }
    
    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }
    
    @Column(name = "RNTC")
    public String getRntc() {
        return rntc.get();
    }
    
    public StringProperty rntcProperty() {
        return rntc;
    }
    
    public void setRntc(String rntc) {
        this.rntc.set(rntc);
    }
    
    @Column(name = "CODTRAN")
    public String getCodTran() {
        return codTran.get();
    }
    
    public StringProperty codTranProperty() {
        return codTran;
    }
    
    public void setCodTran(String codTran) {
        this.codTran.set(codTran);
    }
    
    @Column(name = "ATIVO")
    public String getAtivo() {
        return ativo.get();
    }
    
    public StringProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(String ativo) {
        this.ativo.set(ativo);
    }
    
    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + nome.get();
    }
}
