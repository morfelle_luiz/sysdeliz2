package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TABTRI_001")
@TelaSysDeliz(descricao = "TabTri", icon = "fragment_100.png")
public class TabTri extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 100)
    @ColunaFilter(descricao = "Código", coluna = "id.codigo")
    private final ObjectProperty<TabTriPK> id = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Descrição", width = 520)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty padrao = new SimpleStringProperty();
    private final StringProperty tpcalc = new SimpleStringProperty();
    private final StringProperty operacao = new SimpleStringProperty();
    private final StringProperty codent = new SimpleStringProperty();

    public TabTri() {
    }

    @EmbeddedId
    public TabTriPK getId() {
        return id.get();
    }

    public ObjectProperty<TabTriPK> idProperty() {
        return id;
    }

    public void setId(TabTriPK id) {
        this.id.set(id);
        setCodigoFilter(id.getCodigo());
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "PADRAO")
    public String getPadrao() {
        return padrao.get();
    }

    public StringProperty padraoProperty() {
        return padrao;
    }

    public void setPadrao(String padrao) {
        this.padrao.set(padrao);
    }

    @Column(name = "TP_CALC")
    public String getTpcalc() {
        return tpcalc.get();
    }

    public StringProperty tpcalcProperty() {
        return tpcalc;
    }

    public void setTpcalc(String tpcalc) {
        this.tpcalc.set(tpcalc);
    }

    @Column(name = "OPERACAO")
    public String getOperacao() {
        return operacao.get();
    }

    public StringProperty operacaoProperty() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao.set(operacao);
    }

    @Column(name = "COD_ENT")
    public String getCodent() {
        return codent.get();
    }

    public StringProperty codentProperty() {
        return codent;
    }

    public void setCodent(String codent) {
        this.codent.set(codent);
    }

}
