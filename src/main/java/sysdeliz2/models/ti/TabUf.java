package sysdeliz2.models.ti;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author lima.joao
 * @since 16/07/2019 09:26
 */
@Entity
@Table(name = "TABUF_001")
@TelaSysDeliz(descricao = "Estados", icon = "cliente (4).png")
@SuppressWarnings("unused")
public class TabUf extends BasicModel implements Serializable {

    //    @EmbeddedId
//    private TabUfId tabUfID;
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    @Transient
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<TabUfId> id = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Descrição", width = 250)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    @Transient
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> aliquota = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty alerta = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> aliqInt = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<Pais> codPais = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty geraSt = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> percFcp = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty inscricao = new SimpleStringProperty();
    @Transient
    private final IntegerProperty diasEnt = new SimpleIntegerProperty();
    @Transient
    private final StringProperty regiao = new SimpleStringProperty();
    @Transient
    private final StringProperty polygon = new SimpleStringProperty();

    @ExibeTableView(descricao = "Sigla", width = 60)
    @ColunaFilter(descricao = "Sigla", coluna = "siglaEst")
    @Transient
    private final StringProperty sigla = new SimpleStringProperty();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "UF"),
            @JoinColumn(name = "SIGLA_UF")
    })
    @Access(AccessType.FIELD)
    private List<SdCidade> cidadeList;

//    @Transient
//    public TabUfId getTabUfID(){
//        return tabUfID;
//    }
//
//    public final StringProperty descricaoProperty() {
//        return descricao;
//    }
//
//    public void setDescricao(String value) {
//        descricao.set(value);
//    }

    @EmbeddedId
    public TabUfId getId() {
        return id.get();
    }

    public ObjectProperty<TabUfId> idProperty() {
        return id;
    }

    public void setId(TabUfId id) {
        this.id.set(id);
    }

    @Column(name = "DESCRICAO")
    @Access(AccessType.PROPERTY)
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "ALIQUOTA")
    public BigDecimal getAliquota() {
        return aliquota.get();
    }

    public ObjectProperty<BigDecimal> aliquotaProperty() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota.set(aliquota);
    }

    @Column(name = "ALERTA")
    public String getAlerta() {
        return alerta.get();
    }

    public StringProperty alertaProperty() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta.set(alerta);
    }

    @Column(name = "ALIQ_INT")
    public BigDecimal getAliqInt() {
        return aliqInt.get();
    }

    public ObjectProperty<BigDecimal> aliqIntProperty() {
        return aliqInt;
    }

    public void setAliqInt(BigDecimal aliqInt) {
        this.aliqInt.set(aliqInt);
    }

    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }

    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }

    @Column(name = "GERA_ST")
    public String getGeraSt() {
        return geraSt.get();
    }

    public StringProperty geraStProperty() {
        return geraSt;
    }

    public void setGeraSt(String geraSt) {
        this.geraSt.set(geraSt);
    }

    @Column(name = "PERC_FCP")
    public BigDecimal getPercFcp() {
        return percFcp.get();
    }

    public ObjectProperty<BigDecimal> percFcpProperty() {
        return percFcp;
    }

    public void setPercFcp(BigDecimal percFcp) {
        this.percFcp.set(percFcp);
    }

    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }

    public StringProperty inscricaoProperty() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }

    @Column(name = "DIAS_ENT")
    public int getDiasEnt() {
        return diasEnt.get();
    }

    public IntegerProperty diasEntProperty() {
        return diasEnt;
    }

    public void setDiasEnt(int diasEnt) {
        this.diasEnt.set(diasEnt);
    }

    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COD_PAIS")
    public Pais getCodPais() {
        return codPais.get();
    }

    public final ObjectProperty<Pais> codPaisProperty() {
        return codPais;
    }

    public void setCodPais(Pais value) {
        codPais.set(value);
    }

    @Column(name = "POLYGON")
    @Lob
    @Access(AccessType.PROPERTY)
    public String getPolygon() {
        return polygon.get();
    }

    public final StringProperty polygonPorperty() {
        return polygon;
    }

    public void setPolygon(String value) {
        polygon.set(value);
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------

    @Column(name = "CODIGO", updatable = false, insertable = false)
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "SIGLA_EST", updatable = false, insertable = false)
    public String getSigla() {
        return sigla.get();
    }

    public StringProperty siglaProperty() {
        return sigla;
    }

    public void setSigla(String sigla) {
        setCodigoFilter(sigla);
        this.sigla.set(sigla);
    }

    @Transient
    public List<SdCidade> getCidadeList() {
        return this.cidadeList;
    }

    public void addUF(SdCidade cidade001) {
        this.cidadeList.add(cidade001);
    }

    public void setCidadeList(List<SdCidade> value) {
        cidadeList = value;
    }

    @Transient
    public ObservableList<SdCidade> getCidadesObservableList() {
        return FXCollections.observableArrayList(this.cidadeList);
    }

    @Override
    public String toString() {
        return id.getValue().getSiglaEst();
    }
}
