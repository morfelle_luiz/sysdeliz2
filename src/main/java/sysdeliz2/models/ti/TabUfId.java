package sysdeliz2.models.ti;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 16/07/2019 10:30
 */
@Embeddable

public class TabUfId extends BasicModel implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");

    private final StringProperty siglaEst = new SimpleStringProperty(this, "siglaEst");

    @Column(name = "CODIGO")
    public String getCodigo() {
        super.setCodigoFilter(codigo.get());
        return codigo.get();
    }

    public final StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String value) {
        codigo.set(value);
    }

    @Column(name = "SIGLA_EST")
    @SuppressWarnings("unused")
    public String getSiglaEst() {
        return siglaEst.get();
    }

    @SuppressWarnings("unused")
    public final StringProperty siglaEstProperty() {
        return siglaEst;
    }

    @SuppressWarnings("unused")
    public void setSiglaEst(String value) {
        siglaEst.set(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TabUfId that = (TabUfId) o;

        return Objects.equals(this.codigo, that.codigo) &&
                Objects.equals(this.siglaEst, that.siglaEst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, siglaEst);
    }
}
