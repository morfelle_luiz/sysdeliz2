package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "TINTUR_001")
public class Tintur implements Serializable {

    private final ObjectProperty<LocalDate> dats = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> datr = new SimpleObjectProperty<LocalDate>();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty ficha = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty tinturaria = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> databaixa = new SimpleObjectProperty<LocalDate>();
    private final StringProperty reproc = new SimpleStringProperty();
    private final StringProperty motivo = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();

    public Tintur() {
    }

    public Tintur(LocalDate dats, LocalDate datr, String periodo, String tinturaria, String tipo) {
        this.dats.set(dats);
        this.datr.set(datr);
        this.periodo.set(periodo);
        this.tinturaria.set(tinturaria);
        this.tipo.set(tipo);
    }

    public Tintur(LocalDate dats, LocalDate datr, String cliente, String pedido, String ficha,
                  String codcli, String periodo, String tinturaria, String observacao,
                  LocalDate databaixa, String reproc, String motivo, String tipo) {
        this.dats.set(dats);
        this.datr.set(datr);
        this.cliente.set(cliente);
        this.pedido.set(pedido);
        this.ficha.set(ficha);
        this.codcli.set(codcli);
        this.periodo.set(periodo);
        this.tinturaria.set(tinturaria);
        this.observacao.set(observacao);
        this.databaixa.set(databaixa);
        this.reproc.set(reproc);
        this.motivo.set(motivo);
        this.tipo.set(tipo);
    }

    @Column(name = "DATS")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDats() {
        return dats.get();
    }

    public ObjectProperty<LocalDate> datsProperty() {
        return dats;
    }

    public void setDats(LocalDate dats) {
        this.dats.set(dats);
    }

    @Column(name = "DATR")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDatr() {
        return datr.get();
    }

    public ObjectProperty<LocalDate> datrProperty() {
        return datr;
    }

    public void setDatr(LocalDate datr) {
        this.datr.set(datr);
    }

    @Column(name = "CLIENTE")
    public String getCliente() {
        return cliente.get();
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente.set(cliente);
    }

    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "FICHA")
    public String getFicha() {
        return ficha.get();
    }

    public StringProperty fichaProperty() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha.set(ficha);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Column(name = "TINTURARIA")
    public String getTinturaria() {
        return tinturaria.get();
    }

    public StringProperty tinturariaProperty() {
        return tinturaria;
    }

    public void setTinturaria(String tinturaria) {
        this.tinturaria.set(tinturaria);
    }

    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "DATA_BAIXA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDatabaixa() {
        return databaixa.get();
    }

    public ObjectProperty<LocalDate> databaixaProperty() {
        return databaixa;
    }

    public void setDatabaixa(LocalDate databaixa) {
        this.databaixa.set(databaixa);
    }

    @Column(name = "REPROC")
    public String getReproc() {
        return reproc.get();
    }

    public StringProperty reprocProperty() {
        return reproc;
    }

    public void setReproc(String reproc) {
        this.reproc.set(reproc);
    }

    @Column(name = "MOTIVO")
    public String getMotivo() {
        return motivo.get();
    }

    public StringProperty motivoProperty() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo.set(motivo);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @PrePersist
    private void beginPersist() {
        setNumero(Globals.getProximoCodigo("TINTUR","NUMERO"));
    }

}
