package sysdeliz2.models.ti;

import javafx.beans.property.*;
import sysdeliz2.utils.Globals;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "TINTUR_ITEN_001")
public class TinturIten implements Serializable {

    private final StringProperty codigo2 = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty gramatura = new SimpleStringProperty();
    private final StringProperty estampa = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> rolos = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesobruto = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoliquido = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoretorno = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty situacao = new SimpleStringProperty();
    private final StringProperty fluxo = new SimpleStringProperty();
    private final StringProperty ficha = new SimpleStringProperty();
    private final StringProperty cort = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty cors = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pecas = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pesobaixado = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty obs = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> custofluxo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtretorno = new SimpleObjectProperty<LocalDate>();
    private final StringProperty nfcob = new SimpleStringProperty();
    private final StringProperty depretorno = new SimpleStringProperty();

    public TinturIten() {
    }

    public TinturIten(String codigo2, String codigo, String numero, String gramatura,String estampa,
                      String cor, BigDecimal largura, BigDecimal rolos, BigDecimal pesobruto,
                      BigDecimal pesoliquido, BigDecimal pesoretorno, String situacao, String fluxo,
                      String cort, BigDecimal custo, String cors, String deposito, BigDecimal pecas,
                      BigDecimal preco, Integer ordem, String lote, BigDecimal pesobaixado, String obs,
                      BigDecimal custofluxo, LocalDate dtretorno, String nfcob, String depretorno) {
        this.codigo2.set(codigo2);
        this.codigo.set(codigo);
        this.numero.set(numero);
        this.gramatura.set(gramatura);
        this.estampa.set(estampa);
        this.cor.set(cor);
        this.largura.set(largura);
        this.rolos.set(rolos);
        this.pesobruto.set(pesobruto);
        this.pesoliquido.set(pesoliquido);
        this.pesoretorno.set(pesoretorno);
        this.situacao.set(situacao);
        this.fluxo.set(fluxo);
        this.cort.set(cort);
        this.custo.set(custo);
        this.cors.set(cors);
        this.deposito.set(deposito);
        this.pecas.set(pecas);
        this.preco.set(preco);
        this.ordem.set(ordem);
        this.lote.set(lote);
        this.pesobaixado.set(pesobaixado);
        this.obs.set(obs);
        this.custofluxo.set(custofluxo);
        this.dtretorno.set(dtretorno);
        this.nfcob.set(nfcob);
        this.depretorno.set(depretorno);
    }

    @Id
    @Column(name = "CODIGO2")
    public String getCodigo2() {
        return codigo2.get();
    }

    public StringProperty codigo2Property() {
        return codigo2;
    }

    public void setCodigo2(String codigo2) {
        this.codigo2.set(codigo2);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "GRAMATURA")
    public String getGramatura() {
        return gramatura.get();
    }

    public StringProperty gramaturaProperty() {
        return gramatura;
    }

    public void setGramatura(String gramatura) {
        this.gramatura.set(gramatura);
    }

    @Column(name = "ESTAMPA")
    public String getEstampa() {
        return estampa.get();
    }

    public StringProperty estampaProperty() {
        return estampa;
    }

    public void setEstampa(String estampa) {
        this.estampa.set(estampa);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }

    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }

    @Column(name = "ROLOS")
    public BigDecimal getRolos() {
        return rolos.get();
    }

    public ObjectProperty<BigDecimal> rolosProperty() {
        return rolos;
    }

    public void setRolos(BigDecimal rolos) {
        this.rolos.set(rolos);
    }

    @Column(name = "PESO_BRUTO")
    public BigDecimal getPesobruto() {
        return pesobruto.get();
    }

    public ObjectProperty<BigDecimal> pesobrutoProperty() {
        return pesobruto;
    }

    public void setPesobruto(BigDecimal pesobruto) {
        this.pesobruto.set(pesobruto);
    }

    @Column(name = "PESO_LIQUIDO")
    public BigDecimal getPesoliquido() {
        return pesoliquido.get();
    }

    public ObjectProperty<BigDecimal> pesoliquidoProperty() {
        return pesoliquido;
    }

    public void setPesoliquido(BigDecimal pesoliquido) {
        this.pesoliquido.set(pesoliquido);
    }

    @Column(name = "PESO_RETORNO")
    public BigDecimal getPesoretorno() {
        return pesoretorno.get();
    }

    public ObjectProperty<BigDecimal> pesoretornoProperty() {
        return pesoretorno;
    }

    public void setPesoretorno(BigDecimal pesoretorno) {
        this.pesoretorno.set(pesoretorno);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Id
    @Column(name = "FLUXO")
    public String getFluxo() {
        return fluxo.get();
    }

    public StringProperty fluxoProperty() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo.set(fluxo);
    }

    @Id
    @Column(name = "FICHA")
    public String getFicha() {
        return ficha.get();
    }

    public StringProperty fichaProperty() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha.set(ficha);
    }

    @Id
    @Column(name = "CORT")
    public String getCort() {
        return cort.get();
    }

    public StringProperty cortProperty() {
        return cort;
    }

    public void setCort(String cort) {
        this.cort.set(cort);
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Column(name = "CORS")
    public String getCors() {
        return cors.get();
    }

    public StringProperty corsProperty() {
        return cors;
    }

    public void setCors(String cors) {
        this.cors.set(cors);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "PECAS")
    public BigDecimal getPecas() {
        return pecas.get();
    }

    public ObjectProperty<BigDecimal> pecasProperty() {
        return pecas;
    }

    public void setPecas(BigDecimal pecas) {
        this.pecas.set(pecas);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "PESO_BAIXADO")
    public BigDecimal getPesobaixado() {
        return pesobaixado.get();
    }

    public ObjectProperty<BigDecimal> pesobaixadoProperty() {
        return pesobaixado;
    }

    public void setPesobaixado(BigDecimal pesobaixado) {
        this.pesobaixado.set(pesobaixado);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "CUSTO_FLUXO")
    public BigDecimal getCustofluxo() {
        return custofluxo.get();
    }

    public ObjectProperty<BigDecimal> custofluxoProperty() {
        return custofluxo;
    }

    public void setCustofluxo(BigDecimal custofluxo) {
        this.custofluxo.set(custofluxo);
    }

    @Column(name = "DT_RETORNO")
    public LocalDate getDtretorno() {
        return dtretorno.get();
    }

    public ObjectProperty<LocalDate> dtretornoProperty() {
        return dtretorno;
    }

    public void setDtretorno(LocalDate dtretorno) {
        this.dtretorno.set(dtretorno);
    }

    @Column(name = "NF_COB")
    public String getNfcob() {
        return nfcob.get();
    }

    public StringProperty nfcobProperty() {
        return nfcob;
    }

    public void setNfcob(String nfcob) {
        this.nfcob.set(nfcob);
    }

    @Column(name = "DEP_RETORNO")
    public String getDepretorno() {
        return depretorno.get();
    }

    public StringProperty depretornoProperty() {
        return depretorno;
    }

    public void setDepretorno(String depretorno) {
        this.depretorno.set(depretorno);
    }

    @PrePersist
    private void beforePersist() {
        setFicha(Globals.getProximoCodigo("TINTUR_ITEN","FICHA"));
    }

}
