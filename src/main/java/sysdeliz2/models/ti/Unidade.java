package sysdeliz2.models.ti;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "UNIDADE_001")
public class Unidade {
    
    private final StringProperty unidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty abreviacao = new SimpleStringProperty();

    @Id
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "ABREVIACAO")
    public String getAbreviacao() {
        return abreviacao.get();
    }

    public StringProperty abreviacaoProperty() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao.set(abreviacao);
    }

    @Override
    public String toString() {
        return unidade.get();
    }
    
    @Transient
    public String description() {
        return "["+unidade.get()+"] "+descricao.get();
    }
}