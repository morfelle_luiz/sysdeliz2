package sysdeliz2.models.ti.materiais;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "CADCORTI_001")
public class Cadcorti implements Serializable {

    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty cor1 = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final StringProperty subgrupo = new SimpleStringProperty();

    public Cadcorti() {
    }

    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }

    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Id
    @Column(name = "COR1")
    public String getCor1() {
        return cor1.get();
    }

    public StringProperty cor1Property() {
        return cor1;
    }

    public void setCor1(String cor1) {
        this.cor1.set(cor1);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "SUB_GRUPO")
    public String getSubgrupo() {
        return subgrupo.get();
    }

    public StringProperty subgrupoProperty() {
        return subgrupo;
    }

    public void setSubgrupo(String subgrupo) {
        this.subgrupo.set(subgrupo);
    }

}