package sysdeliz2.models.ti.materiais;

import javafx.beans.property.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "CADMATTI_001")
public class Cadmatti implements Serializable {

    private final ObjectProperty<BigDecimal> lcru = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> ltin = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> lmax = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> kgrolo = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty estampa = new SimpleStringProperty();
    private final StringProperty pelucia = new SimpleStringProperty();
    private final StringProperty rama = new SimpleStringProperty();
    private final StringProperty uni = new SimpleStringProperty();
    private final StringProperty gramatura = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty codigo2 = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty fluxo = new SimpleStringProperty();
    private final StringProperty lfa = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largurafinal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> desperdicio = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty alimentadores = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> fatormal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> comprponto = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty qtagulhas = new SimpleIntegerProperty();
    private final StringProperty gramatcrua = new SimpleStringProperty();
    private final StringProperty navalhar = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> compmax = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> compmin = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> largrolomax = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> largrolomin = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> perctorcao = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> percenclarg = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> percenccomp = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> percumidade = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty cors = new SimpleStringProperty();

    public Cadmatti() {
    }

    @Column(name = "LCRU")
    public BigDecimal getLcru() {
        return lcru.get();
    }

    public ObjectProperty<BigDecimal> lcruProperty() {
        return lcru;
    }

    public void setLcru(BigDecimal lcru) {
        this.lcru.set(lcru);
    }

    @Column(name = "LTIN")
    public BigDecimal getLtin() {
        return ltin.get();
    }

    public ObjectProperty<BigDecimal> ltinProperty() {
        return ltin;
    }

    public void setLtin(BigDecimal ltin) {
        this.ltin.set(ltin);
    }

    @Column(name = "LMAX")
    public BigDecimal getLmax() {
        return lmax.get();
    }

    public ObjectProperty<BigDecimal> lmaxProperty() {
        return lmax;
    }

    public void setLmax(BigDecimal lmax) {
        this.lmax.set(lmax);
    }

    @Column(name = "KGROLO")
    public BigDecimal getKgrolo() {
        return kgrolo.get();
    }

    public ObjectProperty<BigDecimal> kgroloProperty() {
        return kgrolo;
    }

    public void setKgrolo(BigDecimal kgrolo) {
        this.kgrolo.set(kgrolo);
    }

    @Column(name = "ESTAMPA")
    public String getEstampa() {
        return estampa.get();
    }

    public StringProperty estampaProperty() {
        return estampa;
    }

    public void setEstampa(String estampa) {
        this.estampa.set(estampa);
    }

    @Column(name = "PELUCIA")
    public String getPelucia() {
        return pelucia.get();
    }

    public StringProperty peluciaProperty() {
        return pelucia;
    }

    public void setPelucia(String pelucia) {
        this.pelucia.set(pelucia);
    }

    @Column(name = "RAMA")
    public String getRama() {
        return rama.get();
    }

    public StringProperty ramaProperty() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama.set(rama);
    }

    @Column(name = "UNI")
    public String getUni() {
        return uni.get();
    }

    public StringProperty uniProperty() {
        return uni;
    }

    public void setUni(String uni) {
        this.uni.set(uni);
    }

    @Column(name = "GRAMATURA")
    public String getGramatura() {
        return gramatura.get();
    }

    public StringProperty gramaturaProperty() {
        return gramatura;
    }

    public void setGramatura(String gramatura) {
        this.gramatura.set(gramatura);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "CODIGO2")
    public String getCodigo2() {
        return codigo2.get();
    }

    public StringProperty codigo2Property() {
        return codigo2;
    }

    public void setCodigo2(String codigo2) {
        this.codigo2.set(codigo2);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "FLUXO")
    public String getFluxo() {
        return fluxo.get();
    }

    public StringProperty fluxoProperty() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo.set(fluxo);
    }

    @Column(name = "LFA")
    public String getLfa() {
        return lfa.get();
    }

    public StringProperty lfaProperty() {
        return lfa;
    }

    public void setLfa(String lfa) {
        this.lfa.set(lfa);
    }

    @Column(name = "OBSERVACAO")
    @Lob
    public String getObservacao() {
        return observacao.get();
    }

    public StringProperty observacaoProperty() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }

    @Column(name = "LARGURA_FINAL")
    public BigDecimal getLargurafinal() {
        return largurafinal.get();
    }

    public ObjectProperty<BigDecimal> largurafinalProperty() {
        return largurafinal;
    }

    public void setLargurafinal(BigDecimal largurafinal) {
        this.largurafinal.set(largurafinal);
    }

    @Column(name = "DESPERDICIO")
    public BigDecimal getDesperdicio() {
        return desperdicio.get();
    }

    public ObjectProperty<BigDecimal> desperdicioProperty() {
        return desperdicio;
    }

    public void setDesperdicio(BigDecimal desperdicio) {
        this.desperdicio.set(desperdicio);
    }

    @Column(name = "ALIMENTADORES")
    public Integer getAlimentadores() {
        return alimentadores.get();
    }

    public IntegerProperty alimentadoresProperty() {
        return alimentadores;
    }

    public void setAlimentadores(Integer alimentadores) {
        this.alimentadores.set(alimentadores);
    }

    @Column(name = "FATOR_MAL")
    public BigDecimal getFatormal() {
        return fatormal.get();
    }

    public ObjectProperty<BigDecimal> fatormalProperty() {
        return fatormal;
    }

    public void setFatormal(BigDecimal fatormal) {
        this.fatormal.set(fatormal);
    }

    @Column(name = "COMPR_PONTO")
    public BigDecimal getComprponto() {
        return comprponto.get();
    }

    public ObjectProperty<BigDecimal> comprpontoProperty() {
        return comprponto;
    }

    public void setComprponto(BigDecimal comprponto) {
        this.comprponto.set(comprponto);
    }

    @Column(name = "QT_AGULHAS")
    public Integer getQtagulhas() {
        return qtagulhas.get();
    }

    public IntegerProperty qtagulhasProperty() {
        return qtagulhas;
    }

    public void setQtagulhas(Integer qtagulhas) {
        this.qtagulhas.set(qtagulhas);
    }

    @Column(name = "GRAMAT_CRUA")
    public String getGramatcrua() {
        return gramatcrua.get();
    }

    public StringProperty gramatcruaProperty() {
        return gramatcrua;
    }

    public void setGramatcrua(String gramatcrua) {
        this.gramatcrua.set(gramatcrua);
    }

    @Column(name = "NAVALHAR")
    public String getNavalhar() {
        return navalhar.get();
    }

    public StringProperty navalharProperty() {
        return navalhar;
    }

    public void setNavalhar(String navalhar) {
        this.navalhar.set(navalhar);
    }

    @Column(name = "COMP_MAX")
    public BigDecimal getCompmax() {
        return compmax.get();
    }

    public ObjectProperty<BigDecimal> compmaxProperty() {
        return compmax;
    }

    public void setCompmax(BigDecimal compmax) {
        this.compmax.set(compmax);
    }

    @Column(name = "COMP_MIN")
    public BigDecimal getCompmin() {
        return compmin.get();
    }

    public ObjectProperty<BigDecimal> compminProperty() {
        return compmin;
    }

    public void setCompmin(BigDecimal compmin) {
        this.compmin.set(compmin);
    }

    @Column(name = "LARG_ROLO_MAX")
    public BigDecimal getLargrolomax() {
        return largrolomax.get();
    }

    public ObjectProperty<BigDecimal> largrolomaxProperty() {
        return largrolomax;
    }

    public void setLargrolomax(BigDecimal largrolomax) {
        this.largrolomax.set(largrolomax);
    }

    @Column(name = "LARG_ROLO_MIN")
    public BigDecimal getLargrolomin() {
        return largrolomin.get();
    }

    public ObjectProperty<BigDecimal> largrolominProperty() {
        return largrolomin;
    }

    public void setLargrolomin(BigDecimal largrolomin) {
        this.largrolomin.set(largrolomin);
    }

    @Column(name = "PERC_TORCAO")
    public BigDecimal getPerctorcao() {
        return perctorcao.get();
    }

    public ObjectProperty<BigDecimal> perctorcaoProperty() {
        return perctorcao;
    }

    public void setPerctorcao(BigDecimal perctorcao) {
        this.perctorcao.set(perctorcao);
    }

    @Column(name = "PERC_ENC_LARG")
    public BigDecimal getPercenclarg() {
        return percenclarg.get();
    }

    public ObjectProperty<BigDecimal> percenclargProperty() {
        return percenclarg;
    }

    public void setPercenclarg(BigDecimal percenclarg) {
        this.percenclarg.set(percenclarg);
    }

    @Column(name = "PERC_ENC_COMP")
    public BigDecimal getPercenccomp() {
        return percenccomp.get();
    }

    public ObjectProperty<BigDecimal> percenccompProperty() {
        return percenccomp;
    }

    public void setPercenccomp(BigDecimal percenccomp) {
        this.percenccomp.set(percenccomp);
    }

    @Column(name = "PERC_UMIDADE")
    public BigDecimal getPercumidade() {
        return percumidade.get();
    }

    public ObjectProperty<BigDecimal> percumidadeProperty() {
        return percumidade;
    }

    public void setPercumidade(BigDecimal percumidade) {
        this.percumidade.set(percumidade);
    }

    @Id
    @Column(name = "CORS")
    public String getCors() {
        return cors.get();
    }

    public StringProperty corsProperty() {
        return cors;
    }

    public void setCors(String cors) {
        this.cors.set(cors);
    }

}