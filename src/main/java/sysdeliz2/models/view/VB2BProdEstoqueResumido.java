package sysdeliz2.models.view;

import  javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "V_B2B_PROD_ESTOQUE_RESUMIDO")
public class VB2BProdEstoqueResumido extends BasicModel {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty descmarca = new SimpleStringProperty();
    private final StringProperty codCol = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty grupoModelagem = new SimpleStringProperty();
    private final BooleanProperty imagem = new SimpleBooleanProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final BooleanProperty enviado = new SimpleBooleanProperty();
    private final BooleanProperty status = new SimpleBooleanProperty();
    private final BooleanProperty entrega = new SimpleBooleanProperty();

    public VB2BProdEstoqueResumido() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "DESCMARCA")
    public String getDescmarca() {
        return descmarca.get();
    }

    public StringProperty descmarcaProperty() {
        return descmarca;
    }

    public void setDescmarca(String descmarca) {
        this.descmarca.set(descmarca);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "IMAGEM")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImagem() {
        return imagem.get();
    }

    public BooleanProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(boolean imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "ENVIADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviado() {
        return enviado.get();
    }

    public BooleanProperty enviadoProperty() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado.set(enviado);
    }

    @Column(name = "CODCOL")
    public String getCodCol() {
        return codCol.get();
    }

    public StringProperty codColProperty() {
        return codCol;
    }

    public void setCodCol(String codCol) {
        this.codCol.set(codCol);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "GRUPOMODELAGEM")
    public String getGrupoModelagem() {
        return grupoModelagem.get();
    }

    public StringProperty grupoModelagemProperty() {
        return grupoModelagem;
    }

    public void setGrupoModelagem(String grupoModelagem) {
        this.grupoModelagem.set(grupoModelagem);
    }

    @Column(name = "STATUS")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isStatus() {
        return status.get();
    }

    public BooleanProperty statusProperty() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status.set(status);
    }

    @Column(name = "ENTREGA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEntrega() {
        return entrega.get();
    }

    public BooleanProperty entregaProperty() {
        return entrega;
    }

    public void setEntrega(boolean entrega) {
        this.entrega.set(entrega);
    }
}
