package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_ANALISE_COMISSAO_ITEM")
@Immutable
public class VSdAnaliseComissaoItem {
    
    private final ObjectProperty<VSdAnaliseComissaoItemPK> id = new SimpleObjectProperty<>();
    private final StringProperty linha = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> comped = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> comcal = new SimpleObjectProperty<>();
    private final BooleanProperty verificar = new SimpleBooleanProperty();
    private final IntegerProperty qtdeorig = new SimpleIntegerProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdef = new SimpleIntegerProperty();
    private final IntegerProperty qtdec = new SimpleIntegerProperty();
    
    public VSdAnaliseComissaoItem() {
    }
    
    public VSdAnaliseComissaoItem(String numero,
                                  String codlin,
                                  String linha,
                                  BigDecimal comped,
                                  BigDecimal comcal,
                                  Boolean verificar,
                                  Integer qtdeorig,
                                  Integer qtde,
                                  Integer qtdef,
                                  Integer qtdec) {
        this.id.set(new VSdAnaliseComissaoItemPK(numero, codlin));
        this.linha.set(linha);
        this.comped.set(comped);
        this.comcal.set(comcal);
        this.verificar.set(verificar);
        this.qtdeorig.set(qtdeorig);
        this.qtde.set(qtde);
        this.qtdef.set(qtdef);
        this.qtdec.set(qtdec);
        
    }
    
    @EmbeddedId
    public VSdAnaliseComissaoItemPK getId() {
        return id.get();
    }
    
    public ObjectProperty<VSdAnaliseComissaoItemPK> idProperty() {
        return id;
    }
    
    public void setId(VSdAnaliseComissaoItemPK id) {
        this.id.set(id);
    }
    
    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }
    
    public StringProperty linhaProperty() {
        return linha;
    }
    
    public void setLinha(String linha) {
        this.linha.set(linha);
    }
    
    @Column(name = "COM_PED")
    public BigDecimal getComped() {
        return comped.get();
    }
    
    public ObjectProperty<BigDecimal> compedProperty() {
        return comped;
    }
    
    public void setComped(BigDecimal comped) {
        this.comped.set(comped);
    }
    
    @Column(name = "COM_CAL")
    public BigDecimal getComcal() {
        return comcal.get();
    }
    
    public ObjectProperty<BigDecimal> comcalProperty() {
        return comcal;
    }
    
    public void setComcal(BigDecimal comcal) {
        this.comcal.set(comcal);
    }
    
    @Column(name = "VERIFICAR")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean getVerificar() {
        return verificar.get();
    }
    
    public BooleanProperty verificarProperty() {
        return verificar;
    }
    
    public void setVerificar(Boolean verificar) {
        this.verificar.set(verificar);
    }
    
    @Column(name = "QTDE_ORIG")
    public Integer getQtdeorig() {
        return qtdeorig.get();
    }
    
    public IntegerProperty qtdeorigProperty() {
        return qtdeorig;
    }
    
    public void setQtdeorig(Integer qtdeorig) {
        this.qtdeorig.set(qtdeorig);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_F")
    public Integer getQtdef() {
        return qtdef.get();
    }
    
    public IntegerProperty qtdefProperty() {
        return qtdef;
    }
    
    public void setQtdef(Integer qtdef) {
        this.qtdef.set(qtdef);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}