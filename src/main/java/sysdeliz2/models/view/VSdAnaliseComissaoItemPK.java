package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VSdAnaliseComissaoItemPK implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codlin = new SimpleStringProperty();
    
    public VSdAnaliseComissaoItemPK() {
    }
    
    public VSdAnaliseComissaoItemPK(String numero,
                                    String codlin) {
        this.numero.set(numero);
        this.codlin.set(codlin);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "CODLIN")
    public String getCodlin() {
        return codlin.get();
    }
    
    public StringProperty codlinProperty() {
        return codlin;
    }
    
    public void setCodlin(String codlin) {
        this.codlin.set(codlin);
    }
    
    
}