package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdColaborador;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_APROV_DIARIA_COLAB")
public class VSdAprovDiariaColab implements Serializable {
    
    private final ObjectProperty<SdColaborador> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpRelogio = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpProduzido = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpParadoDeduz = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpParado = new SimpleObjectProperty<>();
    
    public VSdAprovDiariaColab() {
    }
    
    @Id
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public SdColaborador getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<SdColaborador> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(SdColaborador codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "TMP_RELOGIO")
    public BigDecimal getTmpRelogio() {
        return tmpRelogio.get();
    }
    
    public ObjectProperty<BigDecimal> tmpRelogioProperty() {
        return tmpRelogio;
    }
    
    public void setTmpRelogio(BigDecimal tmpRelogio) {
        this.tmpRelogio.set(tmpRelogio);
    }
    
    @Column(name = "TMP_PRODUZIDO")
    public BigDecimal getTmpProduzido() {
        return tmpProduzido.get();
    }
    
    public ObjectProperty<BigDecimal> tmpProduzidoProperty() {
        return tmpProduzido;
    }
    
    public void setTmpProduzido(BigDecimal tmpProduzido) {
        this.tmpProduzido.set(tmpProduzido);
    }
    
    @Column(name = "TMP_PARADO_DEDUZ")
    public BigDecimal getTmpParadoDeduz() {
        return tmpParadoDeduz.get();
    }
    
    public ObjectProperty<BigDecimal> tmpParadoDeduzProperty() {
        return tmpParadoDeduz;
    }
    
    public void setTmpParadoDeduz(BigDecimal tmpParadoDeduz) {
        this.tmpParadoDeduz.set(tmpParadoDeduz);
    }
    
    @Column(name = "TMP_PARADO")
    public BigDecimal getTmpParado() {
        return tmpParado.get();
    }
    
    public ObjectProperty<BigDecimal> tmpParadoProperty() {
        return tmpParado;
    }
    
    public void setTmpParado(BigDecimal tmpParado) {
        this.tmpParado.set(tmpParado);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
