package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_ATEND_CARTEIRA_REP")
public class VSdAtendCarteiraRep extends BasicModel {


    private final StringProperty idjpa = new SimpleStringProperty();
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> codcli = new SimpleObjectProperty<>();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty marcaCliente = new SimpleStringProperty();
    private final StringProperty endereco = new SimpleStringProperty();
    private final StringProperty num = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty fone = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty classeCidade = new SimpleStringProperty();
    private final ObjectProperty<Marca> codMarca = new SimpleObjectProperty();
    private final ObjectProperty<Represen> codRep = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dataAtualizacao = new SimpleObjectProperty<>();
    private final StringProperty sitCli = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty atend = new SimpleStringProperty();
    private final StringProperty ultimaColecao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdew20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorw20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdess21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdes21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valors21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdew21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorw21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecancw20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcancw20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecancss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcancss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecancs21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcancs21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecancw21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcancw21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde17cow20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor17cow20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde17coss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor17coss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde17cos21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor17cos21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde17cow21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor17cow21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecanc17cow20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcanc17cow20 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecanc17coss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcanc17coss21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecanc17cos21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcanc17cos21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdecanc17cow21 = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcanc17cow21 = new SimpleObjectProperty<>();

    public VSdAtendCarteiraRep() {
    }

    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }

    public StringProperty idjpaProperty() {
        return idjpa;
    }

    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_CID")
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public Entidade getCodcli() {
        return codcli.get();
    }

    public ObjectProperty<Entidade> codcliProperty() {
        return codcli;
    }

    public void setCodcli(Entidade codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "CLIENTE")
    public String getCliente() {
        return cliente.get();
    }

    public StringProperty clienteProperty() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente.set(cliente);
    }

    @Column(name = "MARCADOCLIENTE")
    public String getMarcaCliente() {
        return marcaCliente.get();
    }

    public StringProperty marcaClienteProperty() {
        return marcaCliente;
    }

    public void setMarcaCliente(String marcaCliente) {
        this.marcaCliente.set(marcaCliente);
    }

    @Column(name = "ENDERECO")
    public String getEndereco() {
        return endereco.get();
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco.set(endereco);
    }

    @Column(name = "NUM")
    public String getNum() {
        return num.get();
    }

    public StringProperty numProperty() {
        return num;
    }

    public void setNum(String num) {
        this.num.set(num);
    }

    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }

    @Column(name = "FONE")
    public String getFone() {
        return fone.get();
    }

    public StringProperty foneProperty() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone.set(fone);
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Column(name = "CLASSECIDADE")
    public String getClasseCidade() {
        return classeCidade.get();
    }

    public StringProperty classeCidadeProperty() {
        return classeCidade;
    }

    public void setClasseCidade(String classeCidade) {
        this.classeCidade.set(classeCidade);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODMAR")
    public Marca getCodMarca() {
        return codMarca.get();
    }

    public ObjectProperty<Marca> codMarcaProperty() {
        return codMarca;
    }

    public void setCodMarca(Marca codMarca) {
        this.codMarca.set(codMarca);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP")
    public Represen getCodRep() {
        return codRep.get();
    }

    public ObjectProperty<Represen> codRepProperty() {
        return codRep;
    }

    public void setCodRep(Represen codRep) {
        this.codRep.set(codRep);
    }

    @Column(name = "DATA_ATUALIZACAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataAtualizacao() {
        return dataAtualizacao.get();
    }

    public ObjectProperty<LocalDate> dataAtualizacaoProperty() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDate dataAtualizacao) {
        this.dataAtualizacao.set(dataAtualizacao);
    }

    @Column(name = "SIT_CLI")
    public String getSitCli() {
        return sitCli.get();
    }

    public StringProperty sitCliProperty() {
        return sitCli;
    }

    public void setSitCli(String sitCli) {
        this.sitCli.set(sitCli);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "ATEND")
    public String getAtend() {
        return atend.get();
    }

    public StringProperty atendProperty() {
        return atend;
    }

    public void setAtend(String atend) {
        this.atend.set(atend);
    }

    @Column(name = "ULTIMACOLECAO")
    public String getUltimaColecao() {
        return ultimaColecao.get();
    }

    public StringProperty ultimaColecaoProperty() {
        return ultimaColecao;
    }

    public void setUltimaColecao(String ultimaColecao) {
        this.ultimaColecao.set(ultimaColecao);
    }

    @Column(name = "QTDEW20")
    public BigDecimal getQtdew20() {
        return qtdew20.get();
    }

    public ObjectProperty<BigDecimal> qtdew20Property() {
        return qtdew20;
    }

    public void setQtdew20(BigDecimal qtdew20) {
        this.qtdew20.set(qtdew20);
    }

    @Column(name = "VALORW20")
    public BigDecimal getValorw20() {
        return valorw20.get();
    }

    public ObjectProperty<BigDecimal> valorw20Property() {
        return valorw20;
    }

    public void setValorw20(BigDecimal valorw20) {
        this.valorw20.set(valorw20);
    }

    @Column(name = "QTDESS21")
    public BigDecimal getQtdess21() {
        return qtdess21.get();
    }

    public ObjectProperty<BigDecimal> qtdess21Property() {
        return qtdess21;
    }

    public void setQtdess21(BigDecimal qtdess21) {
        this.qtdess21.set(qtdess21);
    }

    @Column(name = "VALORSS21")
    public BigDecimal getValorss21() {
        return valorss21.get();
    }

    public ObjectProperty<BigDecimal> valorss21Property() {
        return valorss21;
    }

    public void setValorss21(BigDecimal valorss21) {
        this.valorss21.set(valorss21);
    }

    @Column(name = "QTDES21")
    public BigDecimal getQtdes21() {
        return qtdes21.get();
    }

    public ObjectProperty<BigDecimal> qtdes21Property() {
        return qtdes21;
    }

    public void setQtdes21(BigDecimal qtdes21) {
        this.qtdes21.set(qtdes21);
    }

    @Column(name = "VALORS21")
    public BigDecimal getValors21() {
        return valors21.get();
    }

    public ObjectProperty<BigDecimal> valors21Property() {
        return valors21;
    }

    public void setValors21(BigDecimal valors21) {
        this.valors21.set(valors21);
    }

    @Column(name = "QTDEW21")
    public BigDecimal getQtdew21() {
        return qtdew21.get();
    }

    public ObjectProperty<BigDecimal> qtdew21Property() {
        return qtdew21;
    }

    public void setQtdew21(BigDecimal qtdew21) {
        this.qtdew21.set(qtdew21);
    }

    @Column(name = "VALORW21")
    public BigDecimal getValorw21() {
        return valorw21.get();
    }

    public ObjectProperty<BigDecimal> valorw21Property() {
        return valorw21;
    }

    public void setValorw21(BigDecimal valorw21) {
        this.valorw21.set(valorw21);
    }

    @Column(name = "QTDECANCW20")
    public BigDecimal getQtdecancw20() {
        return qtdecancw20.get();
    }

    public ObjectProperty<BigDecimal> qtdecancw20Property() {
        return qtdecancw20;
    }

    public void setQtdecancw20(BigDecimal qtdecancw20) {
        this.qtdecancw20.set(qtdecancw20);
    }

    @Column(name = "VALORCANCW20")
    public BigDecimal getValorcancw20() {
        return valorcancw20.get();
    }

    public ObjectProperty<BigDecimal> valorcancw20Property() {
        return valorcancw20;
    }

    public void setValorcancw20(BigDecimal valorcancw20) {
        this.valorcancw20.set(valorcancw20);
    }

    @Column(name = "QTDECANCSS21")
    public BigDecimal getQtdecancss21() {
        return qtdecancss21.get();
    }

    public ObjectProperty<BigDecimal> qtdecancss21Property() {
        return qtdecancss21;
    }

    public void setQtdecancss21(BigDecimal qtdecancss21) {
        this.qtdecancss21.set(qtdecancss21);
    }

    @Column(name = "VALORCANCSS21")
    public BigDecimal getValorcancss21() {
        return valorcancss21.get();
    }

    public ObjectProperty<BigDecimal> valorcancss21Property() {
        return valorcancss21;
    }

    public void setValorcancss21(BigDecimal valorcancss21) {
        this.valorcancss21.set(valorcancss21);
    }

    @Column(name = "QTDECANCS21")
    public BigDecimal getQtdecancs21() {
        return qtdecancs21.get();
    }

    public ObjectProperty<BigDecimal> qtdecancs21Property() {
        return qtdecancs21;
    }

    public void setQtdecancs21(BigDecimal qtdecancs21) {
        this.qtdecancs21.set(qtdecancs21);
    }

    @Column(name = "VALORCANCS21")
    public BigDecimal getValorcancs21() {
        return valorcancs21.get();
    }

    public ObjectProperty<BigDecimal> valorcancs21Property() {
        return valorcancs21;
    }

    public void setValorcancs21(BigDecimal valorcancs21) {
        this.valorcancs21.set(valorcancs21);
    }

    @Column(name = "QTDECANCW21")
    public BigDecimal getQtdecancw21() {
        return qtdecancw21.get();
    }

    public ObjectProperty<BigDecimal> qtdecancw21Property() {
        return qtdecancw21;
    }

    public void setQtdecancw21(BigDecimal qtdecancw21) {
        this.qtdecancw21.set(qtdecancw21);
    }

    @Column(name = "VALORCANCW21")
    public BigDecimal getValorcancw21() {
        return valorcancw21.get();
    }

    public ObjectProperty<BigDecimal> valorcancw21Property() {
        return valorcancw21;
    }

    public void setValorcancw21(BigDecimal valorcancw21) {
        this.valorcancw21.set(valorcancw21);
    }

    @Column(name = "QTDE17COW20")

    public BigDecimal getQtde17cow20() {
        return qtde17cow20.get();
    }

    public ObjectProperty<BigDecimal> qtde17cow20Property() {
        return qtde17cow20;
    }

    public void setQtde17cow20(BigDecimal qtde17cow20) {
        this.qtde17cow20.set(qtde17cow20);
    }

    @Column(name = "VALOR17COW20")
    public BigDecimal getValor17cow20() {
        return valor17cow20.get();
    }

    public ObjectProperty<BigDecimal> valor17cow20Property() {
        return valor17cow20;
    }

    public void setValor17cow20(BigDecimal valor17cow20) {
        this.valor17cow20.set(valor17cow20);
    }

    @Column(name = "QTDE17COSS21")
    public BigDecimal getQtde17coss21() {
        return qtde17coss21.get();
    }

    public ObjectProperty<BigDecimal> qtde17coss21Property() {
        return qtde17coss21;
    }

    public void setQtde17coss21(BigDecimal qtde17coss21) {
        this.qtde17coss21.set(qtde17coss21);
    }

    @Column(name = "VALOR17COSS21")
    public BigDecimal getValor17coss21() {
        return valor17coss21.get();
    }

    public ObjectProperty<BigDecimal> valor17coss21Property() {
        return valor17coss21;
    }

    public void setValor17coss21(BigDecimal valor17coss21) {
        this.valor17coss21.set(valor17coss21);
    }

    @Column(name = "QTDE17COS21")
    public BigDecimal getQtde17cos21() {
        return qtde17cos21.get();
    }

    public ObjectProperty<BigDecimal> qtde17cos21Property() {
        return qtde17cos21;
    }

    public void setQtde17cos21(BigDecimal qtde17cos21) {
        this.qtde17cos21.set(qtde17cos21);
    }

    @Column(name = "VALOR17COS21")
    public BigDecimal getValor17cos21() {
        return valor17cos21.get();
    }

    public ObjectProperty<BigDecimal> valor17cos21Property() {
        return valor17cos21;
    }

    public void setValor17cos21(BigDecimal valor17cos21) {
        this.valor17cos21.set(valor17cos21);
    }

    @Column(name = "QTDE17COW21")
    public BigDecimal getQtde17cow21() {
        return qtde17cow21.get();
    }

    public ObjectProperty<BigDecimal> qtde17cow21Property() {
        return qtde17cow21;
    }

    public void setQtde17cow21(BigDecimal qtde17cow21) {
        this.qtde17cow21.set(qtde17cow21);
    }

    @Column(name = "VALOR17COW21")
    public BigDecimal getValor17cow21() {
        return valor17cow21.get();
    }

    public ObjectProperty<BigDecimal> valor17cow21Property() {
        return valor17cow21;
    }

    public void setValor17cow21(BigDecimal valor17cow21) {
        this.valor17cow21.set(valor17cow21);
    }

    @Column(name = "QTDECANC17COW20")
    public BigDecimal getQtdecanc17cow20() {
        return qtdecanc17cow20.get();
    }

    public ObjectProperty<BigDecimal> qtdecanc17cow20Property() {
        return qtdecanc17cow20;
    }

    public void setQtdecanc17cow20(BigDecimal qtdecanc17cow20) {
        this.qtdecanc17cow20.set(qtdecanc17cow20);
    }

    @Column(name = "VALORCANC17COW20")
    public BigDecimal getValorcanc17cow20() {
        return valorcanc17cow20.get();
    }

    public ObjectProperty<BigDecimal> valorcanc17cow20Property() {
        return valorcanc17cow20;
    }

    public void setValorcanc17cow20(BigDecimal valorcanc17cow20) {
        this.valorcanc17cow20.set(valorcanc17cow20);
    }

    @Column(name = "QTDECANC17COSS21")
    public BigDecimal getQtdecanc17coss21() {
        return qtdecanc17coss21.get();
    }

    public ObjectProperty<BigDecimal> qtdecanc17coss21Property() {
        return qtdecanc17coss21;
    }

    public void setQtdecanc17coss21(BigDecimal qtdecanc17coss21) {
        this.qtdecanc17coss21.set(qtdecanc17coss21);
    }

    @Column(name = "VALORCANC17COSS21")
    public BigDecimal getValorcanc17coss21() {
        return valorcanc17coss21.get();
    }

    public ObjectProperty<BigDecimal> valorcanc17coss21Property() {
        return valorcanc17coss21;
    }

    public void setValorcanc17coss21(BigDecimal valorcanc17coss21) {
        this.valorcanc17coss21.set(valorcanc17coss21);
    }

    @Column(name = "QTDECANC17COS21")
    public BigDecimal getQtdecanc17cos21() {
        return qtdecanc17cos21.get();
    }

    public ObjectProperty<BigDecimal> qtdecanc17cos21Property() {
        return qtdecanc17cos21;
    }

    public void setQtdecanc17cos21(BigDecimal qtdecanc17cos21) {
        this.qtdecanc17cos21.set(qtdecanc17cos21);
    }

    @Column(name = "VALORCANC17COS21")
    public BigDecimal getValorcanc17cos21() {
        return valorcanc17cos21.get();
    }

    public ObjectProperty<BigDecimal> valorcanc17cos21Property() {
        return valorcanc17cos21;
    }

    public void setValorcanc17cos21(BigDecimal valorcanc17cos21) {
        this.valorcanc17cos21.set(valorcanc17cos21);
    }

    @Column(name = "QTDECANC17COW21")
    public BigDecimal getQtdecanc17cow21() {
        return qtdecanc17cow21.get();
    }

    public ObjectProperty<BigDecimal> qtdecanc17cow21Property() {
        return qtdecanc17cow21;
    }

    public void setQtdecanc17cow21(BigDecimal qtdecanc17cow21) {
        this.qtdecanc17cow21.set(qtdecanc17cow21);
    }

    @Column(name = "VALORCANC17COW21")
    public BigDecimal getValorcanc17cow21() {
        return valorcanc17cow21.get();
    }

    public ObjectProperty<BigDecimal> valorcanc17cow21Property() {
        return valorcanc17cow21;
    }

    public void setValorcanc17cow21(BigDecimal valorcanc17cow21) {
        this.valorcanc17cow21.set(valorcanc17cow21);
    }

    @Transient
    public BigDecimal getQtdeCanceladaw20() {
        return getQtdew20().add(getQtdecancw20());
    }

    @Transient
    public BigDecimal getQtdeCanceladass21() {
        return getQtdess21().add(getQtdecancss21());
    }

    @Transient
    public BigDecimal getQtdeCanceladas21() {
        return getQtdes21().add(getQtdecancs21());
    }

    @Transient
    public BigDecimal getQtdeCanceladaw21() {
        return getQtdew21().add(getQtdecancw21());
    }

    @Transient
    public BigDecimal getValorCanceladaw20() {
        return getValorw20().add(getValorcancw20());
    }

    @Transient
    public BigDecimal getValorCanceladass21() {
        return getValorss21().add(getValorcancss21());
    }

    @Transient
    public BigDecimal getValorCanceladas21() {
        return getValors21().add(getValorcancs21());
    }

    @Transient
    public BigDecimal getValorCanceladaw21() {
        return getValorw21().add(getValorcancw21());
    }

    @Transient
    public BigDecimal getValorCancelada17cow20() {
        return getValor17cow20().add(getValorcanc17cow20());
    }

    @Transient
    public BigDecimal getValorCancelada17coss21() {
        return getValor17coss21().add(getValorcanc17coss21());
    }

    @Transient
    public BigDecimal getValorCancelada17cos21() {
        return getValor17cos21().add(getValorcanc17cos21());
    }

    @Transient
    public BigDecimal getValorCancelada17cow21() {
        return getValor17cow21().add(getValorcanc17cow21());
    }

    @Transient
    public BigDecimal getQtdeCancelada17cow20() {
        return getQtde17cow20().add(getQtdecanc17cow20());
    }

    @Transient
    public BigDecimal getQtdeCancelada17coss21() {
        return getQtde17coss21().add(getQtdecanc17coss21());
    }

    @Transient
    public BigDecimal getQtdeCancelada17cos21() {
        return getQtde17cos21().add(getQtdecanc17cos21());
    }

    @Transient
    public BigDecimal getQtdeCancelada17cow21() {
        return getQtde17cow21().add(getQtdecanc17cow21());
    }

}

