package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BigDecimalAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_ATRASO_CLIENTE")
public class VSdAtrasoCliente {

    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> mediaDias = new SimpleObjectProperty<>();
    private final IntegerProperty minimo = new SimpleIntegerProperty();
    private final IntegerProperty maximo = new SimpleIntegerProperty();

    public VSdAtrasoCliente() {
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "MEDIA_DIAS")
    public BigDecimal getMediaDias() {
        return mediaDias.get();
    }

    public ObjectProperty<BigDecimal> mediaDiasProperty() {
        return mediaDias;
    }

    public void setMediaDias(BigDecimal mediaDias) {
        this.mediaDias.set(mediaDias);
    }

    @Column(name = "MINIMO")
    public int getMinimo() {
        return minimo.get();
    }

    public IntegerProperty minimoProperty() {
        return minimo;
    }

    public void setMinimo(int minimo) {
        this.minimo.set(minimo);
    }

    @Column(name = "MAXIMO")
    public int getMaximo() {
        return maximo.get();
    }

    public IntegerProperty maximoProperty() {
        return maximo;
    }

    public void setMaximo(int maximo) {
        this.maximo.set(maximo);
    }
}
