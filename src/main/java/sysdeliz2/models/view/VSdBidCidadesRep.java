package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;

@Entity
@Table(name = "V_SD_BID_CIDADES_REP_001")
public class VSdBidCidadesRep extends BasicModel {

    private final ObjectProperty<VSdBidCidadesRepPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty status = new SimpleIntegerProperty();

    public VSdBidCidadesRep() {
    }

    @EmbeddedId
    public VSdBidCidadesRepPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdBidCidadesRepPK> idProperty() {
        return id;
    }

    public void setId(VSdBidCidadesRepPK id) {
        this.id.set(id);
    }

    @Column(name = "STATUS")
    public int getStatus() {
        return status.get();
    }

    public IntegerProperty statusProperty() {
        return status;
    }

    public void setStatus(int status) {
        this.status.set(status);
    }

    @Transient
    public boolean naoTemRep(){
        return id.getValue().getCodRep().getCodRep().equals("0261");
    }
}
