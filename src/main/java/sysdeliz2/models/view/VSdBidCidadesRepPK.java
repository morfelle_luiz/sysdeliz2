package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class VSdBidCidadesRepPK implements Serializable {

    private final ObjectProperty<Represen> codRep = new SimpleObjectProperty<>();
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> codMarca = new SimpleObjectProperty<>();

    public VSdBidCidadesRepPK() {
    }

    @JoinColumn(name = "CODREP")
    @OneToOne(fetch = FetchType.LAZY)
    public Represen getCodRep() {
        return codRep.get();
    }

    public ObjectProperty<Represen> codRepProperty() {
        return codRep;
    }

    public void setCodRep(Represen codRep) {
        this.codRep.set(codRep);
    }

    @JoinColumn(name = "COD_CID")
    @OneToOne(fetch = FetchType.LAZY)
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @JoinColumn(name = "MARCA")
    @OneToOne(fetch = FetchType.LAZY)
    public Marca getCodMarca() {
        return codMarca.get();
    }

    public ObjectProperty<Marca> codMarcaProperty() {
        return codMarca;
    }

    public void setCodMarca(Marca codMarca) {
        this.codMarca.set(codMarca);
    }
}
