package sysdeliz2.models.view;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.annotations.Immutable;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdPolivalencia001;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.enums.StatusProgramacao;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.SQLException;

@Entity
@Immutable
@Table(name = "V_SD_COLAB_CEL_PROG")
public class VSdColabCelProg extends BasicModel {
    
    //View atributes
    private final StringProperty id = new SimpleStringProperty();
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<>();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final StringProperty nivelMedio = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> diasProg = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prodDiaAnt = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> operMes = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> eficMes = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> prodMes = new SimpleObjectProperty<>();
    private final IntegerProperty qtdePecas = new SimpleIntegerProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final ListProperty<SdPolivalencia001> polivalencia = new SimpleListProperty<>();
    private final ListProperty<VSdProgramacaoPendente> programacao = new SimpleListProperty<>(FXCollections.observableArrayList());
    
    //Logic
    private final GenericDao<SdPolivalencia001> daoPolivalencia = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdPolivalencia001.class);
    private final GenericDao<VSdProgramacaoPendente> daoProgramacoesPendentes = new GenericDaoImpl<>(JPAUtils.getEntityManager(), VSdProgramacaoPendente.class);
    private final ObjectProperty<StatusProgramacao> statusColaborador = new SimpleObjectProperty<>(StatusProgramacao.LIVRE);
    private final BooleanProperty hasAlteracao = new SimpleBooleanProperty(false);
    
    public VSdColabCelProg() {
    
    }
    
    @Id
    @Column(name = "ID")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CELULA")
    public SdCelula getCelula() {
        return celula.get();
    }
    
    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }
    
    public void setCelula(SdCelula celula) {
        this.celula.set(celula);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name = "NIV_MEDIO")
    public String getNivelMedio() {
        return nivelMedio.get();
    }
    
    public StringProperty nivelMedioProperty() {
        return nivelMedio;
    }
    
    public void setNivelMedio(String nivelMedio) {
        this.nivelMedio.set(nivelMedio);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "DIAS_PROG")
    public BigDecimal getDiasProg() {
        return diasProg.get();
    }
    
    public ObjectProperty<BigDecimal> diasProgProperty() {
        return diasProg;
    }
    
    public void setDiasProg(BigDecimal diasProg) {
        this.diasProg.set(diasProg);
    }
    
    @Column(name = "PROD_DIA_ANT")
    public BigDecimal getProdDiaAnt() {
        return prodDiaAnt.get();
    }
    
    public ObjectProperty<BigDecimal> prodDiaAntProperty() {
        return prodDiaAnt;
    }
    
    public void setProdDiaAnt(BigDecimal prodDiaAnt) {
        this.prodDiaAnt.set(prodDiaAnt);
    }
    
    @Column(name = "OPER_MES")
    public BigDecimal getOperMes() {
        return operMes.get();
    }
    
    public ObjectProperty<BigDecimal> operMesProperty() {
        return operMes;
    }
    
    public void setOperMes(BigDecimal operMes) {
        this.operMes.set(operMes);
    }
    
    @Column(name = "EFIC_MES")
    public BigDecimal getEficMes() {
        return eficMes.get();
    }
    
    public ObjectProperty<BigDecimal> eficMesProperty() {
        return eficMes;
    }
    
    public void setEficMes(BigDecimal eficMes) {
        this.eficMes.set(eficMes);
    }
    
    @Column(name = "PROD_MES")
    public BigDecimal getProdMes() {
        return prodMes.get();
    }
    
    public ObjectProperty<BigDecimal> prodMesProperty() {
        return prodMes;
    }
    
    public void setProdMes(BigDecimal prodMes) {
        this.prodMes.set(prodMes);
    }
    
    @Column(name = "QTDE")
    public int getQtdePecas() {
        return qtdePecas.get();
    }
    
    public IntegerProperty qtdePecasProperty() {
        return qtdePecas;
    }
    
    public void setQtdePecas(int qtdePecas) {
        this.qtdePecas.set(qtdePecas);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
    
    @Transient
    public ObservableList<SdPolivalencia001> getPolivalencia() {
        return polivalencia.get();
    }
    
    public ListProperty<SdPolivalencia001> polivalenciaProperty() {
        return polivalencia;
    }
    
    public void setPolivalencia(ObservableList<SdPolivalencia001> polivalencia) {
        this.polivalencia.set(polivalencia);
    }
    
    @Transient
    public StatusProgramacao getStatusColaborador() {
        return statusColaborador.get();
    }
    
    public ObjectProperty<StatusProgramacao> statusColaboradorProperty() {
        return statusColaborador;
    }
    
    public void setStatusColaborador(StatusProgramacao statusColaborador) {
        this.statusColaborador.set(statusColaborador);
    }
    
    @Transient
    public ObservableList<VSdProgramacaoPendente> getProgramacao() {
        return programacao.get();
    }
    
    public ListProperty<VSdProgramacaoPendente> programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(ObservableList<VSdProgramacaoPendente> programacao) {
        this.programacao.set(programacao);
    }
    
    @Transient
    public boolean getHasAlteracao() {
        return hasAlteracao.get();
    }
    
    public BooleanProperty hasAlteracaoProperty() {
        return hasAlteracao;
    }
    
    public void setHasAlteracao(boolean hasAlteracao) {
        this.hasAlteracao.set(hasAlteracao);
    }
    
    @PostLoad
    private void onPostLoad() {
        try {
            polivalencia.clear();
            polivalencia.set(daoPolivalencia
                    .initCriteria()
                    .addPredicateEqPkEmbedded("id", "colaborador", this.colaborador.get().getCodigo(), false)
                    .addPredicateEq("ativo", true, false)
                    .loadListByPredicate());
            programacao.clear();
            programacao.set(daoProgramacoesPendentes
                    .initCriteria()
                    .addPredicateEq("colaborador", this.colaborador.get().getCodigo())
                    .loadListByPredicate());
            
            int indexOper = 1;
            for (VSdProgramacaoPendente vSdProgramacaoPendente : programacao) {
                vSdProgramacaoPendente.getProgramacao().setSeqColaborador(indexOper);
                indexOper++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
