package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_COMPOSICAO_COR_OF")
public class VSdComposicaoCorOf implements Serializable {

    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigocomposicao = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty composicao = new SimpleStringProperty();

    public VSdComposicaoCorOf() {
    }

    @Id
    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Id
    @Column(name = "CODIGO_COMPOSICAO")
    public String getCodigocomposicao() {
        return codigocomposicao.get();
    }

    public StringProperty codigocomposicaoProperty() {
        return codigocomposicao;
    }

    public void setCodigocomposicao(String codigocomposicao) {
        this.codigocomposicao.set(codigocomposicao);
    }
    @Id
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Id
    @Column(name = "COMPOSICAO")
    public String getComposicao() {
        return composicao.get();
    }

    public StringProperty composicaoProperty() {
        return composicao;
    }

    public void setComposicao(String composicao) {
        this.composicao.set(composicao);
    }


}
