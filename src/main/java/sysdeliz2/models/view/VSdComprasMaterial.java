package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "V_SD_COMPRAS_MATERIAL")
public class VSdComprasMaterial {
    
    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtfatura = new SimpleObjectProperty<LocalDate>();
    private final StringProperty pedfornecedor = new SimpleStringProperty();
    private final StringProperty nffornecedor = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    
    public VSdComprasMaterial() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public Integer getIdjpa() {
        return idjpa.get();
    }
    
    public IntegerProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(Integer idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "DT_FATURA")
    public LocalDate getDtfatura() {
        return dtfatura.get();
    }
    
    public ObjectProperty<LocalDate> dtfaturaProperty() {
        return dtfatura;
    }
    
    public void setDtfatura(LocalDate dtfatura) {
        this.dtfatura.set(dtfatura);
    }
    
    @Column(name = "PED_FORNECEDOR")
    public String getPedfornecedor() {
        return pedfornecedor.get();
    }
    
    public StringProperty pedfornecedorProperty() {
        return pedfornecedor;
    }
    
    public void setPedfornecedor(String pedfornecedor) {
        this.pedfornecedor.set(pedfornecedor);
    }
    
    @Column(name = "NF_FORNECEDOR")
    public String getNffornecedor() {
        return nffornecedor.get();
    }
    
    public StringProperty nffornecedorProperty() {
        return nffornecedor;
    }
    
    public void setNffornecedor(String nffornecedor) {
        this.nffornecedor.set(nffornecedor);
    }
    
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
}
