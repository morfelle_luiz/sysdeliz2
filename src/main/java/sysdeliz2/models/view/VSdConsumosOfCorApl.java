package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.Pcpapl;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_CONSUMOS_OF_COR_APL")
@Immutable
public class VSdConsumosOfCorApl {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty aplicacao = new SimpleStringProperty();
    private final ObjectProperty<Pcpapl> aplicpcp = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumoMt = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumocor = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> percconsumo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cori = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumototal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumoTotalMt = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty qtdeof = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> minimoConsumo = new SimpleObjectProperty<BigDecimal>();
    
    public VSdConsumosOfCorApl() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "APLICACAO")
    public String getAplicacao() {
        return aplicacao.get();
    }
    
    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(String aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APLIC_PCP")
    public Pcpapl getAplicpcp() {
        return aplicpcp.get();
    }
    
    public ObjectProperty<Pcpapl> aplicpcpProperty() {
        return aplicpcp;
    }
    
    public void setAplicpcp(Pcpapl aplicpcp) {
        this.aplicpcp.set(aplicpcp);
    }
    
    @Column(name = "CONSUMO")
    public BigDecimal getConsumo() {
        return consumo.get();
    }
    
    public ObjectProperty<BigDecimal> consumoProperty() {
        return consumo;
    }
    
    public void setConsumo(BigDecimal consumo) {
        this.consumo.set(consumo);
    }
    
    @Column(name = "CONSUMO_MT")
    public BigDecimal getConsumoMt() {
        return consumoMt.get();
    }
    
    public ObjectProperty<BigDecimal> consumoMtProperty() {
        return consumoMt;
    }
    
    public void setConsumoMt(BigDecimal consumoMt) {
        this.consumoMt.set(consumoMt);
    }
    
    @Column(name = "CONSUMO_COR")
    public BigDecimal getConsumocor() {
        return consumocor.get();
    }
    
    public ObjectProperty<BigDecimal> consumocorProperty() {
        return consumocor;
    }
    
    public void setConsumocor(BigDecimal consumocor) {
        this.consumocor.set(consumocor);
    }
    
    @Column(name = "PERC_CONSUMO")
    public BigDecimal getPercconsumo() {
        return percconsumo.get();
    }
    
    public ObjectProperty<BigDecimal> percconsumoProperty() {
        return percconsumo;
    }
    
    public void setPercconsumo(BigDecimal percconsumo) {
        this.percconsumo.set(percconsumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR_I")
    public Cor getCori() {
        return cori.get();
    }
    
    public ObjectProperty<Cor> coriProperty() {
        return cori;
    }
    
    public void setCori(Cor cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "CONSUMO_TOTAL")
    public BigDecimal getConsumototal() {
        return consumototal.get();
    }
    
    public ObjectProperty<BigDecimal> consumototalProperty() {
        return consumototal;
    }
    
    public void setConsumototal(BigDecimal consumototal) {
        this.consumototal.set(consumototal);
    }
    
    @Column(name = "CONSUMO_TOTAL_MT")
    public BigDecimal getConsumoTotalMt() {
        return consumoTotalMt.get();
    }
    
    public ObjectProperty<BigDecimal> consumoTotalMtProperty() {
        return consumoTotalMt;
    }
    
    public void setConsumoTotalMt(BigDecimal consumoTotalMt) {
        this.consumoTotalMt.set(consumoTotalMt);
    }
    
    @Column(name = "QTDE_OF")
    public int getQtdeof() {
        return qtdeof.get();
    }
    
    public IntegerProperty qtdeofProperty() {
        return qtdeof;
    }
    
    public void setQtdeof(int qtdeof) {
        this.qtdeof.set(qtdeof);
    }

    @Column(name = "MINIMO_CONSUMO")
    public BigDecimal getMinimoConsumo() {
        return minimoConsumo.get();
    }

    public ObjectProperty<BigDecimal> minimoConsumoProperty() {
        return minimoConsumo;
    }

    public void setMinimoConsumo(BigDecimal minimoConsumo) {
        this.minimoConsumo.set(minimoConsumo);
    }
}