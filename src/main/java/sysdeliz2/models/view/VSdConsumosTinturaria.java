package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_SD_CONSUMOS_TINTURARIA")
public class VSdConsumosTinturaria {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty insumo = new SimpleStringProperty();
    private final StringProperty corinsumo = new SimpleStringProperty();
    private final StringProperty tipoinsumo = new SimpleStringProperty();
    
    public VSdConsumosTinturaria() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }
    
    public StringProperty insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }
    
    @Column(name = "COR_INSUMO")
    public String getCorinsumo() {
        return corinsumo.get();
    }
    
    public StringProperty corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(String corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @Column(name = "TIPO_INSUMO")
    public String getTipoinsumo() {
        return tipoinsumo.get();
    }
    
    public StringProperty tipoinsumoProperty() {
        return tipoinsumo;
    }
    
    public void setTipoinsumo(String tipoinsumo) {
        this.tipoinsumo.set(tipoinsumo);
    }
}
