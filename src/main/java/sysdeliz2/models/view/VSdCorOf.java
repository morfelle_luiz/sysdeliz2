package sysdeliz2.models.view;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "V_SD_COR_OF")
@Immutable
public class VSdCorOf {
    
    private final ObjectProperty<VSdCorOfPk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final ListProperty<VSdCortamOf> tamsObservable = new SimpleListProperty<>();
    private List<VSdCortamOf> tams;
    
    public VSdCorOf() {
    }
    
    @EmbeddedId
    public VSdCorOfPk getId() {
        return id.get();
    }
    
    public ObjectProperty<VSdCorOfPk> idProperty() {
        return id;
    }
    
    public void setId(VSdCorOfPk id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO"),
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR")
    })
    @OrderBy("ORDEM")
    public List<VSdCortamOf> getTams() {
        return tams;
    }
    
    public void setTams(List<VSdCortamOf> tams) {
        this.tams = tams;
    }
    
    @Transient
    public ObservableList<VSdCortamOf> getTamsObservable() {
        tamsObservable.set(FXCollections.observableList(this.tams));
        return tamsObservable.get();
    }
    
    public ListProperty<VSdCortamOf> tamsObservableProperty() {
        tamsObservable.set(FXCollections.observableList(this.tams));
        return tamsObservable;
    }
    
    public void setTamsObservable(ObservableList<VSdCortamOf> tamsObservable) {
        this.tamsObservable.set(tamsObservable);
    }
    
    @Override
    public String toString() {
        return this.id.get().getCor().getCor();
    }
}