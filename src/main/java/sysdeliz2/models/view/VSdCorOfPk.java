package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Cor;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class VSdCorOfPk implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
    private final StringProperty codigo = new SimpleStringProperty();
    
    public VSdCorOfPk() {
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }
    
    public ObjectProperty<Cor> corProperty() {
        return cor;
    }
    
    public void setCor(Cor cor) {
        this.cor.set(cor);
    }
}