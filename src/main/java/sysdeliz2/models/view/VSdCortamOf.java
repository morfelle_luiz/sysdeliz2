package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_CORTAM_OF")
@Immutable
public class VSdCortamOf {
    
    private final ObjectProperty<VSdCortamOfPk> id = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> qtdeof = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> vendpend = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> planejado = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> emprod = new SimpleObjectProperty<BigDecimal>();
    
    public VSdCortamOf() {
    }
    
    public VSdCortamOf(VSdCortamOf cortam) {
        this.id.set(new VSdCortamOfPk(cortam.getId().getNumero(), cortam.getId().getCodigo(), cortam.getId().getCor(), cortam.getId().getTam()));
        this.ordem.set(cortam.getOrdem());
        this.qtdeof.set(cortam.getQtdeof());
        this.vendpend.set(cortam.getVendpend());
        this.planejado.set(cortam.getPlanejado());
        this.emprod.set(cortam.getEmprod());
    }
    
    @EmbeddedId
    public VSdCortamOfPk getId() {
        return id.get();
    }
    
    public ObjectProperty<VSdCortamOfPk> idProperty() {
        return id;
    }
    
    public void setId(VSdCortamOfPk id) {
        this.id.set(id);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "QTDE_OF")
    public BigDecimal getQtdeof() {
        return qtdeof.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeofProperty() {
        return qtdeof;
    }
    
    public void setQtdeof(BigDecimal qtdeof) {
        this.qtdeof.set(qtdeof);
    }
    
    @Column(name = "VEND_PEND")
    public BigDecimal getVendpend() {
        return vendpend.get();
    }
    
    public ObjectProperty<BigDecimal> vendpendProperty() {
        return vendpend;
    }
    
    public void setVendpend(BigDecimal vendpend) {
        this.vendpend.set(vendpend);
    }
    
    @Column(name = "PLANEJADO")
    public BigDecimal getPlanejado() {
        return planejado.get();
    }
    
    public ObjectProperty<BigDecimal> planejadoProperty() {
        return planejado;
    }
    
    public void setPlanejado(BigDecimal planejado) {
        this.planejado.set(planejado);
    }
    
    @Column(name = "EM_PROD")
    public BigDecimal getEmprod() {
        return emprod.get();
    }
    
    public ObjectProperty<BigDecimal> emprodProperty() {
        return emprod;
    }
    
    public void setEmprod(BigDecimal emprod) {
        this.emprod.set(emprod);
    }
    
}
