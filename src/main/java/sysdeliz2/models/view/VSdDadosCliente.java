package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.CadCep;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.TabSit;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.CodcliAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_DADOS_CLIENTE")
public class VSdDadosCliente {
    
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty razaosocial = new SimpleStringProperty();
    private final StringProperty cidade = new SimpleStringProperty();
    private final StringProperty uf = new SimpleStringProperty();
    private final StringProperty endereço = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    private final StringProperty fantasia = new SimpleStringProperty();
    private final StringProperty classecliente = new SimpleStringProperty();
    private final StringProperty tipocidade = new SimpleStringProperty();
    private final StringProperty enderecoentrega = new SimpleStringProperty();
    private final StringProperty numeroentrega = new SimpleStringProperty();
    private final StringProperty bairroentrega = new SimpleStringProperty();
    private final StringProperty complementoentrega = new SimpleStringProperty();
    private final ObjectProperty<CadCep> cepentrega = new SimpleObjectProperty<>();
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();
    private final StringProperty suframa = new SimpleStringProperty();
    private final StringProperty obsNota = new SimpleStringProperty();
    private final StringProperty cnpj = new SimpleStringProperty();
    private final StringProperty inscricao = new SimpleStringProperty();
    private final ObjectProperty<TabSit> sitDup = new SimpleObjectProperty<>();
    private final BooleanProperty imprimeConteudoCaixa = new SimpleBooleanProperty(false);
    private final BooleanProperty separaMarcasFatura = new SimpleBooleanProperty(false);
    private final BooleanProperty imprimeRemessa = new SimpleBooleanProperty(false);
    private final ObjectProperty<BigDecimal> maximoReserva = new SimpleObjectProperty<>();

    public VSdDadosCliente() {
    }
    
    @Id
    @Column(name = "CODCLI")
    @Convert(converter = CodcliAttributeConverter.class)
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "RAZAOSOCIAL")
    public String getRazaosocial() {
        return razaosocial.get();
    }
    
    public StringProperty razaosocialProperty() {
        return razaosocial;
    }
    
    public void setRazaosocial(String razaosocial) {
        this.razaosocial.set(razaosocial);
    }
    
    @Column(name = "CIDADE")
    public String getCidade() {
        return cidade.get();
    }
    
    public StringProperty cidadeProperty() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade.set(cidade);
    }
    
    @Column(name = "UF")
    public String getUf() {
        return uf.get();
    }
    
    public StringProperty ufProperty() {
        return uf;
    }
    
    public void setUf(String uf) {
        this.uf.set(uf);
    }
    
    @Column(name = "ENDEREÇO")
    public String getEndereço() {
        return endereço.get();
    }
    
    public StringProperty endereçoProperty() {
        return endereço;
    }
    
    public void setEndereço(String endereço) {
        this.endereço.set(endereço);
    }
    
    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }
    
    public StringProperty bairroProperty() {
        return bairro;
    }
    
    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "TELEFONE")
    public String getTelefone() {
        return telefone.get();
    }
    
    public StringProperty telefoneProperty() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }
    
    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }
    
    public StringProperty emailProperty() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email.set(email);
    }
    
    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }
    
    public StringProperty grupoProperty() {
        return grupo;
    }
    
    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }
    
    @Column(name = "FANTASIA")
    public String getFantasia() {
        return fantasia.get();
    }
    
    public StringProperty fantasiaProperty() {
        return fantasia;
    }
    
    public void setFantasia(String fantasia) {
        this.fantasia.set(fantasia);
    }
    
    @Column(name = "CLASSECLIENTE")
    public String getClassecliente() {
        return classecliente.get();
    }
    
    public StringProperty classeclienteProperty() {
        return classecliente;
    }
    
    public void setClassecliente(String classecliente) {
        this.classecliente.set(classecliente);
    }
    
    @Column(name = "TIPOCIDADE")
    public String getTipocidade() {
        return tipocidade.get();
    }
    
    public StringProperty tipocidadeProperty() {
        return tipocidade;
    }
    
    public void setTipocidade(String tipocidade) {
        this.tipocidade.set(tipocidade);
    }
    
    @Column(name = "ENDERECOENTREGA")
    public String getEnderecoentrega() {
        return enderecoentrega.get();
    }
    
    public StringProperty enderecoentregaProperty() {
        return enderecoentrega;
    }
    
    public void setEnderecoentrega(String enderecoentrega) {
        this.enderecoentrega.set(enderecoentrega);
    }
    
    @Column(name = "NUMEROENTREGA")
    public String getNumeroentrega() {
        return numeroentrega.get();
    }
    
    public StringProperty numeroentregaProperty() {
        return numeroentrega;
    }
    
    public void setNumeroentrega(String numeroentrega) {
        this.numeroentrega.set(numeroentrega);
    }
    
    @Column(name = "BAIRROENTREGA")
    public String getBairroentrega() {
        return bairroentrega.get();
    }
    
    public StringProperty bairroentregaProperty() {
        return bairroentrega;
    }
    
    public void setBairroentrega(String bairroentrega) {
        this.bairroentrega.set(bairroentrega);
    }
    
    @Column(name = "COMPLEMENTOENTREGA")
    public String getComplementoentrega() {
        return complementoentrega.get();
    }
    
    public StringProperty complementoentregaProperty() {
        return complementoentrega;
    }
    
    public void setComplementoentrega(String complementoentrega) {
        this.complementoentrega.set(complementoentrega);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CEPENTREGA")
    public CadCep getCepentrega() {
        return cepentrega.get();
    }
    
    public ObjectProperty<CadCep> cepentregaProperty() {
        return cepentrega;
    }
    
    public void setCepentrega(CadCep cepentrega) {
        this.cepentrega.set(cepentrega);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_CID")
    public Cidade getCodCid() {
        return codCid.get();
    }
    
    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }
    
    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }
    
    @Column(name = "SUFRAMA")
    public String getSuframa() {
        return suframa.get();
    }
    
    public StringProperty suframaProperty() {
        return suframa;
    }
    
    public void setSuframa(String suframa) {
        this.suframa.set(suframa);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIT_DUP")
    public TabSit getSitDup() {
        return sitDup.get();
    }
    
    public ObjectProperty<TabSit> sitDupProperty() {
        return sitDup;
    }
    
    public void setSitDup(TabSit sitDup) {
        this.sitDup.set(sitDup);
    }
    
    @Lob
    @Column(name = "OBS_NOTA")
    public String getObsNota() {
        return obsNota.get();
    }
    
    public StringProperty obsNotaProperty() {
        return obsNota;
    }
    
    public void setObsNota(String obsNota) {
        this.obsNota.set(obsNota);
    }
    
    @Column(name = "CNPJ")
    public String getCnpj() {
        return cnpj.get();
    }
    
    public StringProperty cnpjProperty() {
        return cnpj;
    }
    
    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }
    
    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }
    
    public StringProperty inscricaoProperty() {
        return inscricao;
    }
    
    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }
    
    @Column(name = "IMPRIMI_CONTEUDO_CX")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImprimeConteudoCaixa() {
        return imprimeConteudoCaixa.get();
    }
    
    public BooleanProperty imprimeConteudoCaixaProperty() {
        return imprimeConteudoCaixa;
    }
    
    public void setImprimeConteudoCaixa(boolean imprimeConteudoCaixa) {
        this.imprimeConteudoCaixa.set(imprimeConteudoCaixa);
    }

    @Column(name = "SEPARA_MARCAS_FATURA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSeparaMarcasFatura() {
        return separaMarcasFatura.get();
    }

    public BooleanProperty separaMarcasFaturaProperty() {
        return separaMarcasFatura;
    }

    public void setSeparaMarcasFatura(boolean separaMarcasFatura) {
        this.separaMarcasFatura.set(separaMarcasFatura);
    }

    @Column(name = "IMPRIME_REMESSA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isImprimeRemessa() {
        return imprimeRemessa.get();
    }

    public BooleanProperty imprimeRemessaProperty() {
        return imprimeRemessa;
    }

    public void setImprimeRemessa(boolean imprimeRemessa) {
        this.imprimeRemessa.set(imprimeRemessa);
    }

    @Column(name = "MAXIMO_RESERVA")
    public BigDecimal getMaximoReserva() {
        return maximoReserva.get();
    }

    public ObjectProperty<BigDecimal> maximoReservaProperty() {
        return maximoReserva;
    }

    public void setMaximoReserva(BigDecimal maximoReserva) {
        this.maximoReserva.set(maximoReserva);
    }

    @Override
    public String toString() {
        return "[" + codcli.get() + "] " + razaosocial.get();
    }

}