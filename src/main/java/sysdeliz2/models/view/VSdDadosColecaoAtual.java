package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_DADOS_COLECAO_ATUAL")
public class VSdDadosColecaoAtual implements Serializable {
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final StringProperty marca = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> inicioVend = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> fimVend = new SimpleObjectProperty<>();

    public VSdDadosColecaoAtual() {
    }

    @Id
    @OneToOne
    @JoinColumn(name = "COLECAO", referencedColumnName = "CODIGO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Id
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "INICIO_VEND")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getInicioVend() {
        return inicioVend.get();
    }

    public ObjectProperty<LocalDate> inicioVendProperty() {
        return inicioVend;
    }

    public void setInicioVend(LocalDate inicioVend) {
        this.inicioVend.set(inicioVend);
    }

    @Column(name = "FIM_VEND")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getFimVend() {
        return fimVend.get();
    }

    public ObjectProperty<LocalDate> fimVendProperty() {
        return fimVend;
    }

    public void setFimVend(LocalDate fimVend) {
        this.fimVend.set(fimVend);
    }
}
