package sysdeliz2.models.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.*;
import sysdeliz2.models.ti.SitCli;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.CadCep;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.GrupoCli;
import sysdeliz2.models.ti.TabSit;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.CodcliAttributeConverter;
import sysdeliz2.utils.enums.ColumnType;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_DADOS_ENTIDADE")
@TelaSysDeliz(descricao = "Entidades", icon = "cliente (4).png")
public class VSdDadosEntidade extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codcli")
    private final StringProperty codcli = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "razaosocial")
    private final StringProperty razaosocial = new SimpleStringProperty();
    @Transient
    private final StringProperty cidade = new SimpleStringProperty();
    @Transient
    private final StringProperty uf = new SimpleStringProperty();
    @Transient
    private final StringProperty endereço = new SimpleStringProperty();
    @Transient
    private final StringProperty bairro = new SimpleStringProperty();
    @Transient
    private final StringProperty numero = new SimpleStringProperty();
    @Transient
    private final StringProperty telefone = new SimpleStringProperty();
    @Transient
    private final StringProperty email = new SimpleStringProperty();
    @Transient
    @ColunaFilter(descricao = "Grupo", coluna = "grupo.codigo", filterClass = "sysdeliz2.models.ti.GrupoCli")
    private final ObjectProperty<GrupoCli> grupo = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty fantasia = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<SitCli> classecliente = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty tipocidade = new SimpleStringProperty();
    @Transient
    private final StringProperty enderecoentrega = new SimpleStringProperty();
    @Transient
    private final StringProperty numeroentrega = new SimpleStringProperty();
    @Transient
    private final StringProperty bairroentrega = new SimpleStringProperty();
    @Transient
    private final StringProperty complementoentrega = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<CadCep> cepentrega = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty suframa = new SimpleStringProperty();
    @Transient
    private final StringProperty obsNota = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "CNPJ", width = 80)
    @ColunaFilter(descricao = "CNPJ", coluna = "cnpj")
    private final StringProperty cnpj = new SimpleStringProperty();
    @Transient
    private final StringProperty inscricao = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<TabSit> sitDup = new SimpleObjectProperty<>();
    @Transient
    private final BooleanProperty imprimeConteudoCaixa = new SimpleBooleanProperty(false);
    @Transient
    private final BooleanProperty separaMarcasFatura = new SimpleBooleanProperty(false);
    @Transient
    private final BooleanProperty imprimeRemessa = new SimpleBooleanProperty(false);
    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private final BooleanProperty ativo = new SimpleBooleanProperty();

    public VSdDadosEntidade() {
    }

    @Id
    @Column(name = "CODCLI")
    @Convert(converter = CodcliAttributeConverter.class)
    @JsonProperty
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
        setCodigoFilter(codcli);
    }

    @Column(name = "RAZAOSOCIAL")
    @JsonProperty
    public String getRazaosocial() {
        return razaosocial.get();
    }

    public StringProperty razaosocialProperty() {
        return razaosocial;
    }

    public void setRazaosocial(String razaosocial) {
        this.razaosocial.set(razaosocial);
        setDescricaoFilter(razaosocial);
    }

    @Column(name = "CIDADE")
    @JsonProperty
    public String getCidade() {
        return cidade.get();
    }

    public StringProperty cidadeProperty() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade.set(cidade);
    }

    @Column(name = "UF")
    @JsonProperty
    public String getUf() {
        return uf.get();
    }

    public StringProperty ufProperty() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf.set(uf);
    }

    @Column(name = "ENDEREÇO")
    @JsonProperty
    public String getEndereço() {
        return endereço.get();
    }

    public StringProperty endereçoProperty() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço.set(endereço);
    }

    @Column(name = "BAIRRO")
    @JsonProperty
    public String getBairro() {
        return bairro.get();
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }

    @Column(name = "NUMERO")
    @JsonProperty
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "TELEFONE")
    @JsonProperty
    public String getTelefone() {
        return telefone.get();
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }

    @Column(name = "EMAIL")
    @JsonProperty
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @OneToOne
    @JoinColumn(name = "GRUPO")
    @JsonProperty
    public GrupoCli getGrupo() {
        return grupo.get();
    }

    public ObjectProperty<GrupoCli> grupoProperty() {
        return grupo;
    }

    public void setGrupo(GrupoCli grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "FANTASIA")
    @JsonProperty
    public String getFantasia() {
        return fantasia.get();
    }

    public StringProperty fantasiaProperty() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia.set(fantasia);
    }

    @OneToOne
    @JoinColumn(name = "CLASSECLIENTE")
    @JsonProperty
    public SitCli getClassecliente() {
        return classecliente.get();
    }

    public ObjectProperty<SitCli> classeclienteProperty() {
        return classecliente;
    }

    public void setClassecliente(SitCli classecliente) {
        this.classecliente.set(classecliente);
    }


    @Column(name = "TIPOCIDADE")
    @JsonProperty
    public String getTipocidade() {
        return tipocidade.get();
    }

    public StringProperty tipocidadeProperty() {
        return tipocidade;
    }

    public void setTipocidade(String tipocidade) {
        this.tipocidade.set(tipocidade);
    }

    @Column(name = "ENDERECOENTREGA")
    @JsonProperty
    public String getEnderecoentrega() {
        return enderecoentrega.get();
    }

    public StringProperty enderecoentregaProperty() {
        return enderecoentrega;
    }

    public void setEnderecoentrega(String enderecoentrega) {
        this.enderecoentrega.set(enderecoentrega);
    }

    @Column(name = "NUMEROENTREGA")
    @JsonProperty
    public String getNumeroentrega() {
        return numeroentrega.get();
    }

    public StringProperty numeroentregaProperty() {
        return numeroentrega;
    }

    public void setNumeroentrega(String numeroentrega) {
        this.numeroentrega.set(numeroentrega);
    }

    @Column(name = "BAIRROENTREGA")
    @JsonProperty
    public String getBairroentrega() {
        return bairroentrega.get();
    }

    public StringProperty bairroentregaProperty() {
        return bairroentrega;
    }

    public void setBairroentrega(String bairroentrega) {
        this.bairroentrega.set(bairroentrega);
    }

    @Column(name = "COMPLEMENTOENTREGA")
    @JsonProperty
    public String getComplementoentrega() {
        return complementoentrega.get();
    }

    public StringProperty complementoentregaProperty() {
        return complementoentrega;
    }

    public void setComplementoentrega(String complementoentrega) {
        this.complementoentrega.set(complementoentrega);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CEPENTREGA")
    @JsonIgnore
    public CadCep getCepentrega() {
        return cepentrega.get();
    }

    public ObjectProperty<CadCep> cepentregaProperty() {
        return cepentrega;
    }

    public void setCepentrega(CadCep cepentrega) {
        this.cepentrega.set(cepentrega);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_CID")
    @JsonIgnore
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @Column(name = "SUFRAMA")
    @JsonIgnore
    public String getSuframa() {
        return suframa.get();
    }

    public StringProperty suframaProperty() {
        return suframa;
    }

    public void setSuframa(String suframa) {
        this.suframa.set(suframa);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIT_DUP")
    @JsonIgnore
    public TabSit getSitDup() {
        return sitDup.get();
    }

    public ObjectProperty<TabSit> sitDupProperty() {
        return sitDup;
    }

    public void setSitDup(TabSit sitDup) {
        this.sitDup.set(sitDup);
    }

//    @Lob
    @Column(name = "OBS_NOTA")
    @JsonIgnore
    public String getObsNota() {
        return obsNota.get();
    }

    public StringProperty obsNotaProperty() {
        return obsNota;
    }

    public void setObsNota(String obsNota) {
        this.obsNota.set(obsNota);
    }

    @Column(name = "CNPJ")
    @JsonProperty
    public String getCnpj() {
        return cnpj.get();
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }

    @Column(name = "INSCRICAO")
    @JsonIgnore
    public String getInscricao() {
        return inscricao.get();
    }

    public StringProperty inscricaoProperty() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }

    @Column(name = "IMPRIMI_CONTEUDO_CX")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    public boolean isImprimeConteudoCaixa() {
        return imprimeConteudoCaixa.get();
    }

    public BooleanProperty imprimeConteudoCaixaProperty() {
        return imprimeConteudoCaixa;
    }

    public void setImprimeConteudoCaixa(boolean imprimeConteudoCaixa) {
        this.imprimeConteudoCaixa.set(imprimeConteudoCaixa);
    }

    @Column(name = "SEPARA_MARCAS_FATURA")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    public boolean isSeparaMarcasFatura() {
        return separaMarcasFatura.get();
    }

    public BooleanProperty separaMarcasFaturaProperty() {
        return separaMarcasFatura;
    }

    public void setSeparaMarcasFatura(boolean separaMarcasFatura) {
        this.separaMarcasFatura.set(separaMarcasFatura);
    }

    @Column(name = "IMPRIME_REMESSA")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    public boolean isImprimeRemessa() {
        return imprimeRemessa.get();
    }

    public BooleanProperty imprimeRemessaProperty() {
        return imprimeRemessa;
    }

    public void setImprimeRemessa(boolean imprimeRemessa) {
        this.imprimeRemessa.set(imprimeRemessa);
    }

    @Column(name = "ativo")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Override
    public String toString() {
        return "[" + codcli.get() + "] " + razaosocial.get();
    }

}