package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Immutable
@TelaSysDeliz(descricao = "Cliente", icon = "cliente (4).png")
@Table(name = "V_SD_DADOS_ENTIDADE_MARCA")
public class VSdDadosEntidadeMarca extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codcli")
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty grupo = new SimpleStringProperty();
    @ExibeTableView(descricao = "Nome", width = 350)
    @ColunaFilter(descricao = "Nome", coluna = "nome")
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty fantasia = new SimpleStringProperty();
    private final StringProperty marcascliente = new SimpleStringProperty();
    private final StringProperty inscricao = new SimpleStringProperty();
    @ExibeTableView(descricao = "CNPJ", width = 80)
    @ColunaFilter(descricao = "CNPJ", coluna = "cnpj")
    private final StringProperty cnpj = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty cep = new SimpleStringProperty();
    private final StringProperty endereco = new SimpleStringProperty();
    private final StringProperty bairro = new SimpleStringProperty();
    private final StringProperty numend = new SimpleStringProperty();
    private final StringProperty classecliente = new SimpleStringProperty();
    private final StringProperty codcid = new SimpleStringProperty();
    private final StringProperty nomecid = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> ipc = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty classe = new SimpleStringProperty();
    private final StringProperty codest = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty representada = new SimpleStringProperty();
    private final StringProperty nomerep = new SimpleStringProperty();
    private final StringProperty classemarca = new SimpleStringProperty();
    private final StringProperty codmarca = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();


    public VSdDadosEntidadeMarca() {
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "GRUPO")
    public String getGrupo() {
        return grupo.get();
    }

    public StringProperty grupoProperty() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    @Column(name = "FANTASIA")
    public String getFantasia() {
        return fantasia.get();
    }

    public StringProperty fantasiaProperty() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia.set(fantasia);
    }

    @Column(name = "MARCAS_CLIENTE")
    public String getMarcascliente() {
        return marcascliente.get();
    }

    public StringProperty marcasclienteProperty() {
        return marcascliente;
    }

    public void setMarcascliente(String marcascliente) {
        this.marcascliente.set(marcascliente);
    }

    @Column(name = "INSCRICAO")
    public String getInscricao() {
        return inscricao.get();
    }

    public StringProperty inscricaoProperty() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao.set(inscricao);
    }

    @Column(name = "CNPJ")
    public String getCnpj() {
        return cnpj.get();
    }

    public StringProperty cnpjProperty() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj.set(cnpj);
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Column(name = "TELEFONE")
    public String getTelefone() {
        return telefone.get();
    }

    public StringProperty telefoneProperty() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }

    @Column(name = "CEP")
    public String getCep() {
        return cep.get();
    }

    public StringProperty cepProperty() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep.set(cep);
    }

    @Column(name = "ENDERECO")
    public String getEndereco() {
        return endereco.get();
    }

    public StringProperty enderecoProperty() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco.set(endereco);
    }

    @Column(name = "BAIRRO")
    public String getBairro() {
        return bairro.get();
    }

    public StringProperty bairroProperty() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro.set(bairro);
    }

    @Column(name = "NUM_END")
    public String getNumend() {
        return numend.get();
    }

    public StringProperty numendProperty() {
        return numend;
    }

    public void setNumend(String numend) {
        this.numend.set(numend);
    }

    @Column(name = "CLASSECLIENTE")
    public String getClassecliente() {
        return classecliente.get();
    }

    public StringProperty classeclienteProperty() {
        return classecliente;
    }

    public void setClassecliente(String classecliente) {
        this.classecliente.set(classecliente);
    }

    @Column(name = "COD_CID")
    public String getCodcid() {
        return codcid.get();
    }

    public StringProperty codcidProperty() {
        return codcid;
    }

    public void setCodcid(String codcid) {
        this.codcid.set(codcid);
    }

    @Column(name = "NOME_CID")
    public String getNomecid() {
        return nomecid.get();
    }

    public StringProperty nomecidProperty() {
        return nomecid;
    }

    public void setNomecid(String nomecid) {
        this.nomecid.set(nomecid);
    }

    @Column(name = "IPC")
    public BigDecimal getIpc() {
        return ipc.get();
    }

    public ObjectProperty<BigDecimal> ipcProperty() {
        return ipc;
    }

    public void setIpc(BigDecimal ipc) {
        this.ipc.set(ipc);
    }

    @Column(name = "CLASSE")
    public String getClasse() {
        return classe.get();
    }

    public StringProperty classeProperty() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe.set(classe);
    }

    @Column(name = "COD_EST")
    public String getCodest() {
        return codest.get();
    }

    public StringProperty codestProperty() {
        return codest;
    }

    public void setCodest(String codest) {
        this.codest.set(codest);
    }

    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }

    @Column(name = "REPRESENTADA")
    public String getRepresentada() {
        return representada.get();
    }

    public StringProperty representadaProperty() {
        return representada;
    }

    public void setRepresentada(String representada) {
        this.representada.set(representada);
    }

    @Column(name = "NOMEREP")
    public String getNomerep() {
        return nomerep.get();
    }

    public StringProperty nomerepProperty() {
        return nomerep;
    }

    public void setNomerep(String nomerep) {
        this.nomerep.set(nomerep);
    }

    @Column(name = "CLASSEMARCA")
    public String getClassemarca() {
        return classemarca.get();
    }

    public StringProperty classemarcaProperty() {
        return classemarca;
    }

    public void setClassemarca(String classemarca) {
        this.classemarca.set(classemarca);
    }

    @Column(name = "CODMARCA")
    public String getCodmarca() {
        return codmarca.get();
    }

    public StringProperty codmarcaProperty() {
        return codmarca;
    }

    public void setCodmarca(String codmarca) {
        this.codmarca.set(codmarca);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }


}