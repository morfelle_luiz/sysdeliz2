package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_DADOS_OF_FINALIZADAS")
public class VSdDadosOfFinalizadas implements Serializable {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty liberadocoleta = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty proxsetor = new SimpleStringProperty();
    private final StringProperty faccao = new SimpleStringProperty();
    private final StringProperty fluxo = new SimpleStringProperty();
    private final StringProperty descfluxo = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty lavand = new SimpleStringProperty();
    private final StringProperty coleta = new SimpleStringProperty();
    private final StringProperty cort = new SimpleStringProperty();
    private final StringProperty aplic = new SimpleStringProperty();
    private final StringProperty cost = new SimpleStringProperty();
    private final StringProperty acab = new SimpleStringProperty();

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Column(name = "LIBERADO_COLETA")
    public String getLiberadocoleta() {
        return liberadocoleta.get();
    }

    public StringProperty liberadocoletaProperty() {
        return liberadocoleta;
    }

    public void setLiberadocoleta(String liberadocoleta) {
        this.liberadocoleta.set(liberadocoleta);
    }

    @Id
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor.set(setor);
    }

    @Column(name = "PROX_SETOR")
    public String getProxsetor() {
        return proxsetor.get();
    }

    public StringProperty proxsetorProperty() {
        return proxsetor;
    }

    public void setProxsetor(String proxsetor) {
        this.proxsetor.set(proxsetor);
    }

    @Column(name = "FACCAO")
    public String getFaccao() {
        return faccao.get();
    }

    public StringProperty faccaoProperty() {
        return faccao;
    }

    public void setFaccao(String faccao) {
        this.faccao.set(faccao);
    }

    @Column(name = "FLUXO")
    public String getFluxo() {
        return fluxo.get();
    }

    public StringProperty fluxoProperty() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo.set(fluxo);
    }

    @Column(name = "DESC_FLUXO")
    public String getDescfluxo() {
        return descfluxo.get();
    }

    public StringProperty descfluxoProperty() {
        return descfluxo;
    }

    public void setDescfluxo(String descfluxo) {
        this.descfluxo.set(descfluxo);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }

    public StringProperty paisProperty() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais.set(pais);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "LAVAND")
    public String getLavand() {
        return lavand.get();
    }

    public StringProperty lavandProperty() {
        return lavand;
    }

    public void setLavand(String lavand) {
        this.lavand.set(lavand);
    }

    @Column(name = "COLETA")
    public String getColeta() {
        return coleta.get();
    }

    public StringProperty coletaProperty() {
        return coleta;
    }

    public void setColeta(String coleta) {
        this.coleta.set(coleta);
    }

    @Column(name = "CORT")
    public String getCort() {
        return cort.get();
    }

    public StringProperty cortProperty() {
        return cort;
    }

    public void setCort(String cort) {
        this.cort.set(cort);
    }

    @Column(name = "APLIC")
    public String getAplic() {
        return aplic.get();
    }

    public StringProperty aplicProperty() {
        return aplic;
    }

    public void setAplic(String aplic) {
        this.aplic.set(aplic);
    }

    @Column(name = "COST")
    public String getCost() {
        return cost.get();
    }

    public StringProperty costProperty() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost.set(cost);
    }

    @Column(name = "ACAB")
    public String getAcab() {
        return acab.get();
    }

    public StringProperty acabProperty() {
        return acab;
    }

    public void setAcab(String acab) {
        this.acab.set(acab);
    }

}
