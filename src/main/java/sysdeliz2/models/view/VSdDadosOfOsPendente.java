package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "V_SD_DADOS_OF_OS_PENDENTE")
public class VSdDadosOfOsPendente implements Serializable {

    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> faccao = new SimpleObjectProperty<>();
    private final BooleanProperty ordem = new SimpleBooleanProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private List<VSdDadosOfOsPendenteSKU> cores = new ArrayList<>();

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "SETOR")
    public CadFluxo getSetor() {
        return setor.get();
    }

    public ObjectProperty<CadFluxo> setorProperty() {
        return setor;
    }

    public void setSetor(CadFluxo setor) {
        this.setor.set(setor);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FACCAO")
    public Entidade getFaccao() {
        return faccao.get();
    }

    public ObjectProperty<Entidade> faccaoProperty() {
        return faccao;
    }

    public void setFaccao(Entidade faccao) {
        this.faccao.set(faccao);
    }

    @Column(name = "ORDEM")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isOrdem() {
        return ordem.get();
    }

    public BooleanProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(boolean ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Transient
    public List<VSdDadosOfOsPendenteSKU> getCores() {
        return cores;
    }

    public void setCores(List<VSdDadosOfOsPendenteSKU> cores) {
        this.cores = cores;
    }
}
