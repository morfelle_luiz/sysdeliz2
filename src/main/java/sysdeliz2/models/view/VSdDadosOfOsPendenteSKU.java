package sysdeliz2.models.view;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_DADOS_OF_OS_PENDENTE_SKU")
public class VSdDadosOfOsPendenteSKU {

    private final ObjectProperty<VSdDadosOfOsPendenteSKUPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty parte = new SimpleStringProperty();

    public VSdDadosOfOsPendenteSKU() {
    }

    @EmbeddedId
    public VSdDadosOfOsPendenteSKUPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdDadosOfOsPendenteSKUPK> idProperty() {
        return id;
    }

    public void setId(VSdDadosOfOsPendenteSKUPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

}
