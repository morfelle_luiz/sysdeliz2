package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "V_SD_DADOS_OF_PENDENTE")
@TelaSysDeliz(descricao = "Of's Pendentes", icon = "carregando pedido_100.png")
public class VSdDadosOfPendente extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Número", width = 100)
    @ColunaFilter(descricao = "Número", coluna = "id.numero")
    private final ObjectProperty<VSdDadosOfPendentePK> id = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Produto", width = 250)
    @ColunaFilter(descricao = "Produto", coluna = "codigo")
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final StringProperty periodo = new SimpleStringProperty();
    private final BooleanProperty liberadocoleta = new SimpleBooleanProperty();
    private final ObjectProperty<CadFluxo> proxsetor = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> faccao = new SimpleObjectProperty<>();
    private final StringProperty fluxo = new SimpleStringProperty();
    private final StringProperty descfluxo = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    @ExibeTableView(descricao = "País", width = 60)
    private final StringProperty pais = new SimpleStringProperty();
    @ExibeTableView(descricao = "Qtde", width = 50)
    private final IntegerProperty qtde = new SimpleIntegerProperty(0);
    private final BooleanProperty lavand = new SimpleBooleanProperty();
    private final BooleanProperty coleta = new SimpleBooleanProperty();
    private final StringProperty cort = new SimpleStringProperty();
    private final StringProperty aplic = new SimpleStringProperty();
    private final StringProperty cost = new SimpleStringProperty();
    private final StringProperty acab = new SimpleStringProperty();
    private List<VSdDadosOfPendenteSKU> cores = new ArrayList<>();
    private final ObjectProperty<Of1> of1 = new SimpleObjectProperty<>();

    public VSdDadosOfPendente() {
    }

    public VSdDadosOfPendente(VSdDadosOfFinalizadas ofFinalizada) {
        this.id.set(new VSdDadosOfPendentePK(ofFinalizada.getNumero(), new FluentDao().selectFrom(CadFluxo.class).where(it -> it.equal("codigo", ofFinalizada.getSetor())).singleResult()));
        this.periodo.set(ofFinalizada.getPeriodo());
        this.codigo.set(new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codigo", ofFinalizada.getCodigo())).singleResult());
        this.parte.set(ofFinalizada.getParte());
        this.of1.set(new FluentDao().selectFrom(Of1.class).where(it -> it.equal("numero", ofFinalizada.getNumero())).singleResult());
    }

    @EmbeddedId
    public VSdDadosOfPendentePK getId() {
        return id.get();
    }

    public ObjectProperty<VSdDadosOfPendentePK> idProperty() {
        return id;
    }

    public void setId(VSdDadosOfPendentePK id) {
        setCodigoFilter(id.getNumero());
        this.id.set(id);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROX_SETOR")
    public CadFluxo getProxsetor() {
        return proxsetor.get();
    }

    public ObjectProperty<CadFluxo> proxsetorProperty() {
        return proxsetor;
    }

    public void setProxsetor(CadFluxo proxsetor) {
        this.proxsetor.set(proxsetor);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FACCAO")
    public Entidade getFaccao() {
        return faccao.get();
    }

    public ObjectProperty<Entidade> faccaoProperty() {
        return faccao;
    }

    public void setFaccao(Entidade faccao) {
        this.faccao.set(faccao);
    }

    @Column(name = "FLUXO")
    public String getFluxo() {
        return fluxo.get();
    }

    public StringProperty fluxoProperty() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo.set(fluxo);
    }

    @Column(name = "DESC_FLUXO")
    public String getDescfluxo() {
        return descfluxo.get();
    }

    public StringProperty descfluxoProperty() {
        return descfluxo;
    }

    public void setDescfluxo(String descfluxo) {
        this.descfluxo.set(descfluxo);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }

    public StringProperty paisProperty() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais.set(pais);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "LAVAND")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isLavand() {
        return lavand.get();
    }

    public BooleanProperty lavandProperty() {
        return lavand;
    }

    public void setLavand(Boolean lavand) {
        this.lavand.set(lavand);
    }

    @Column(name = "COLETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isColeta() {
        return coleta.get();
    }

    public BooleanProperty coletaProperty() {
        return coleta;
    }

    public void setColeta(Boolean lavand) {
        this.coleta.set(lavand);
    }

    @Column(name = "CORT")
    public String getCort() {
        return cort.get();
    }

    public StringProperty cortProperty() {
        return cort;
    }

    public void setCort(String cort) {
        this.cort.set(cort);
    }

    @Column(name = "APLIC")
    public String getAplic() {
        return aplic.get();
    }

    public StringProperty aplicProperty() {
        return aplic;
    }

    public void setAplic(String aplic) {
        this.aplic.set(aplic);
    }

    @Column(name = "COST")
    public String getCost() {
        return cost.get();
    }

    public StringProperty costProperty() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost.set(cost);
    }

    @Column(name = "ACAB")
    public String getAcab() {
        return acab.get();
    }

    public StringProperty acabProperty() {
        return acab;
    }

    public void setAcab(String acab) {
        this.acab.set(acab);
    }

    @Column(name = "LIBERADO_COLETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isLiberadocoleta() {
        return liberadocoleta.get();
    }

    public BooleanProperty liberadocoletaProperty() {
        return liberadocoleta;
    }

    public void setLiberadocoleta(boolean liberadocoleta) {
        this.liberadocoleta.set(liberadocoleta);
    }

    @Transient
    public List<VSdDadosOfPendenteSKU> getCores() {
        return cores;
    }

    public void setCores(List<VSdDadosOfPendenteSKU> cores) {
        this.cores = cores;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO", insertable = false, updatable = false)
    public Of1 getOf1() {
        return of1.get();
    }

    public ObjectProperty<Of1> of1Property() {
        return of1;
    }

    public void setOf1(Of1 of1) {
        this.of1.set(of1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VSdDadosOfPendente that = (VSdDadosOfPendente) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
