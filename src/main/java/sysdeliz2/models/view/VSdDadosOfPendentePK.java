package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.CadFluxo;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class VSdDadosOfPendentePK extends BasicModel implements Serializable {

    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();

    public VSdDadosOfPendentePK() {
    }

    public VSdDadosOfPendentePK(String numero, CadFluxo setor) {
        this.numero.set(numero);
        this.setor.set(setor);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @OneToOne()
    @JoinColumn(name = "SETOR")
    public CadFluxo getSetor() {
        return setor.get();
    }

    public ObjectProperty<CadFluxo> setorProperty() {
        return setor;
    }

    public void setSetor(CadFluxo setor) {
        this.setor.set(setor);
    }

    @Override
    public String toString() {
        return numero.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VSdDadosOfPendentePK that = (VSdDadosOfPendentePK) o;
        return Objects.equals(numero, that.numero) && Objects.equals(setor, that.setor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero, setor);
    }
}