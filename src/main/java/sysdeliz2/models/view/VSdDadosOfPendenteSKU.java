package sysdeliz2.models.view;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_DADOS_OF_PENDENTE_SKU")
public class VSdDadosOfPendenteSKU {

    private final ObjectProperty<VSdDadosOfPendenteSKUPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty parte = new SimpleStringProperty();

    public VSdDadosOfPendenteSKU() {
    }

    public VSdDadosOfPendenteSKU(VSdDadosOfPendenteSKUPK id, String parte, BigDecimal qtde) {
        this.id.set(id);
        this.parte.set(parte);
        this.qtde.set(qtde.intValue());
    }

    @EmbeddedId
    public VSdDadosOfPendenteSKUPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdDadosOfPendenteSKUPK> idProperty() {
        return id;
    }

    public void setId(VSdDadosOfPendenteSKUPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }

    public StringProperty parteProperty() {
        return parte;
    }

    public void setParte(String parte) {
        this.parte.set(parte);
    }

}
