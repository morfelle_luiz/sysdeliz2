package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_DADOS_PEDIDO")
@TelaSysDeliz(descricao = "Pedidos", icon = "pedido_50.png")
public class VSdDadosPedido extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Número", width = 80)
    @ColunaFilter(descricao = "Número", coluna = "numero", filterClass = "sysdeliz2.model.ti.Pedido")
    private final ObjectProperty<Pedido> numero = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Marca", width = 180)
    @ColunaFilter(descricao = "Marca", coluna = "marca", filterClass = "sysdeliz2.models.ti.Marca")
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Coleção", width = 200)
    @ColunaFilter(descricao = "Coleção", coluna = "colecao", filterClass = "sysdeliz2.models.ti.Colecao")
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdeF = new SimpleIntegerProperty();
    private final IntegerProperty qtdeCanc = new SimpleIntegerProperty();
    private final IntegerProperty qtdeOrig = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valorFat = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorPend = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorCanc = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorOrig = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorDesc = new SimpleObjectProperty<>();
    private final StringProperty status = new SimpleStringProperty();

    private final ObjectProperty<BigDecimal> valorRecebido = new SimpleObjectProperty<>();
    
    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    public Pedido getNumero() {
        return numero.get();
    }
    
    public ObjectProperty<Pedido> numeroProperty() {
        return numero;
    }
    
    public void setNumero(Pedido numero) {
        this.numero.set(numero);
        super.setCodigoFilter(numero.getNumero());
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_F")
    public int getQtdeF() {
        return qtdeF.get();
    }
    
    public IntegerProperty qtdeFProperty() {
        return qtdeF;
    }
    
    public void setQtdeF(int qtdeF) {
        this.qtdeF.set(qtdeF);
    }
    
    @Column(name = "QTDE_CANC")
    public int getQtdeCanc() {
        return qtdeCanc.get();
    }
    
    public IntegerProperty qtdeCancProperty() {
        return qtdeCanc;
    }
    
    public void setQtdeCanc(int qtdeCanc) {
        this.qtdeCanc.set(qtdeCanc);
    }
    
    @Column(name = "QTDE_ORIG")
    public int getQtdeOrig() {
        return qtdeOrig.get();
    }
    
    public IntegerProperty qtdeOrigProperty() {
        return qtdeOrig;
    }
    
    public void setQtdeOrig(int qtdeOrig) {
        this.qtdeOrig.set(qtdeOrig);
    }
    
    @Column(name = "VALOR_FAT")
    public BigDecimal getValorFat() {
        return valorFat.get();
    }
    
    public ObjectProperty<BigDecimal> valorFatProperty() {
        return valorFat;
    }
    
    public void setValorFat(BigDecimal valorFat) {
        this.valorFat.set(valorFat);
    }
    
    @Column(name = "VALOR_PEND")
    public BigDecimal getValorPend() {
        return valorPend.get();
    }
    
    public ObjectProperty<BigDecimal> valorPendProperty() {
        return valorPend;
    }
    
    public void setValorPend(BigDecimal valorPend) {
        this.valorPend.set(valorPend);
    }
    
    @Column(name = "VALOR_CANC")
    public BigDecimal getValorCanc() {
        return valorCanc.get();
    }
    
    public ObjectProperty<BigDecimal> valorCancProperty() {
        return valorCanc;
    }
    
    public void setValorCanc(BigDecimal valorCanc) {
        this.valorCanc.set(valorCanc);
    }
    
    @Column(name = "VALOR_ORIG")
    public BigDecimal getValorOrig() {
        return valorOrig.get();
    }
    
    public ObjectProperty<BigDecimal> valorOrigProperty() {
        return valorOrig;
    }
    
    public void setValorOrig(BigDecimal valorOrig) {
        this.valorOrig.set(valorOrig);
    }
    
    @Column(name = "VALOR_DESC")
    public BigDecimal getValorDesc() {
        return valorDesc.get();
    }
    
    public ObjectProperty<BigDecimal> valorDescProperty() {
        return valorDesc;
    }
    
    public void setValorDesc(BigDecimal valorDesc) {
        this.valorDesc.set(valorDesc);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }

    @Transient
    public BigDecimal getValorRecebido() {
        return valorRecebido.get();
    }

    public ObjectProperty<BigDecimal> valorRecebidoProperty() {
        return valorRecebido;
    }

    public void setValorRecebido(BigDecimal valorRecebido) {
        this.valorRecebido.set(valorRecebido);
    }
}

