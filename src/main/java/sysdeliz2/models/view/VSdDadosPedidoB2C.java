package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.B2CPedido;
import sysdeliz2.models.sysdeliz.SdStatusPedidoB2C;
import sysdeliz2.models.sysdeliz.SdTranspPedidoB2C;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_DADOS_PEDIDO_B2C")
public class VSdDadosPedidoB2C implements Serializable {

    private final ObjectProperty<Pedido> numeroerp = new SimpleObjectProperty<>();
    private final ObjectProperty<B2CPedido> numero = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusPedidoB2C> codStatus = new SimpleObjectProperty<>();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty tipopgto = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> frete = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valordesc = new SimpleObjectProperty<>();
    private final StringProperty codrastreio = new SimpleStringProperty();
    private final ObjectProperty<SdTranspPedidoB2C> transporte = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final StringProperty tipoTransporte = new SimpleStringProperty();

    public VSdDadosPedidoB2C() {
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO_ERP", referencedColumnName = "NUMERO")
    public Pedido getNumeroerp() {
        return numeroerp.get();
    }

    public ObjectProperty<Pedido> numeroerpProperty() {
        return numeroerp;
    }

    public void setNumeroerp(Pedido numeroerp) {
        this.numeroerp.set(numeroerp);
    }


    @OneToOne
    @JoinColumn(name = "NUMERO")
    public B2CPedido getNumero() {
        return numero.get();
    }

    public ObjectProperty<B2CPedido> numeroProperty() {
        return numero;
    }

    public void setNumero(B2CPedido numero) {
        this.numero.set(numero);
    }

    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Column(name = "TIPO_PGTO")
    public String getTipopgto() {
        return tipopgto.get();
    }

    public StringProperty tipopgtoProperty() {
        return tipopgto;
    }

    public void setTipopgto(String tipopgto) {
        this.tipopgto.set(tipopgto);
    }

    @Column(name = "FRETE")
    public BigDecimal getFrete() {
        return frete.get();
    }

    public ObjectProperty<BigDecimal> freteProperty() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete.set(frete);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "VALOR_DESC")
    public BigDecimal getValordesc() {
        return valordesc.get();
    }

    public ObjectProperty<BigDecimal> valordescProperty() {
        return valordesc;
    }

    public void setValordesc(BigDecimal valordesc) {
        this.valordesc.set(valordesc);
    }

    @Column(name = "COD_RASTREIO")
    public String getCodrastreio() {
        return codrastreio.get();
    }

    public StringProperty codrastreioProperty() {
        return codrastreio;
    }

    public void setCodrastreio(String codrastreio) {
        this.codrastreio.set(codrastreio);
    }

    @OneToOne
    @JoinColumn(name = "TRANSPORTE")
    public SdTranspPedidoB2C getTransporte() {
        return transporte.get();
    }

    public ObjectProperty<SdTranspPedidoB2C> transporteProperty() {
        return transporte;
    }

    public void setTransporte(SdTranspPedidoB2C transporte) {
        this.transporte.set(transporte);
    }

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @OneToOne
    @JoinColumn(name = "COD_STATUS")
    public SdStatusPedidoB2C getCodStatus() {
        return codStatus.get();
    }

    public ObjectProperty<SdStatusPedidoB2C> codStatusProperty() {
        return codStatus;
    }

    public void setCodStatus(SdStatusPedidoB2C codStatus) {
        this.codStatus.set(codStatus);
    }

    @OneToOne
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "TIPO_TRANSPORTE")
    public String getTipoTransporte() {
        return tipoTransporte.get();
    }

    public StringProperty tipoTransporteProperty() {
        return tipoTransporte;
    }

    public void setTipoTransporte(String tipoTransporte) {
        this.tipoTransporte.set(tipoTransporte);
    }
}
