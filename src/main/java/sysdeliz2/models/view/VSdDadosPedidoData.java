package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_DADOS_PEDIDO_DATA")
@Immutable
public class VSdDadosPedidoData {
    
    private final ObjectProperty<VSdDadosPedidoDataPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> codcli = new SimpleObjectProperty<>();
    private final ObjectProperty<Represen> codrep = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> comped = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty periodo = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<Regiao> tabpre = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> perdesc = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty condicao = new SimpleStringProperty();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdef = new SimpleIntegerProperty();
    private final IntegerProperty qtdecanc = new SimpleIntegerProperty();
    private final IntegerProperty qtdeorig = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valorfat = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpend = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorcanc = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valororig = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valordesc = new SimpleObjectProperty<BigDecimal>();
    private final BooleanProperty comercial = new SimpleBooleanProperty();
    private final BooleanProperty financeiro = new SimpleBooleanProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty obse = new SimpleStringProperty();
    
    @EmbeddedId
    public VSdDadosPedidoDataPK getId() {
        return id.get();
    }
    
    public ObjectProperty<VSdDadosPedidoDataPK> idProperty() {
        return id;
    }
    
    public void setId(VSdDadosPedidoDataPK id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODCLI")
    public Entidade getCodcli() {
        return codcli.get();
    }
    
    public ObjectProperty<Entidade> codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(Entidade codcli) {
        this.codcli.set(codcli);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP")
    public Represen getCodrep() {
        return codrep.get();
    }
    
    public ObjectProperty<Represen> codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(Represen codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "COM_PED")
    public BigDecimal getComped() {
        return comped.get();
    }
    
    public ObjectProperty<BigDecimal> compedProperty() {
        return comped;
    }
    
    public void setComped(BigDecimal comped) {
        this.comped.set(comped);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }
    
    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }
    
    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAB_PRE")
    public Regiao getTabpre() {
        return tabpre.get();
    }
    
    public ObjectProperty<Regiao> tabpreProperty() {
        return tabpre;
    }
    
    public void setTabpre(Regiao tabpre) {
        this.tabpre.set(tabpre);
    }
    
    @Column(name = "PER_DESC")
    public BigDecimal getPerdesc() {
        return perdesc.get();
    }
    
    public ObjectProperty<BigDecimal> perdescProperty() {
        return perdesc;
    }
    
    public void setPerdesc(BigDecimal perdesc) {
        this.perdesc.set(perdesc);
    }
    
    @Column(name = "CONDICAO")
    public String getCondicao() {
        return condicao.get();
    }
    
    public StringProperty condicaoProperty() {
        return condicao;
    }
    
    public void setCondicao(String condicao) {
        this.condicao.set(condicao);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }
    
    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_F")
    public Integer getQtdef() {
        return qtdef.get();
    }
    
    public IntegerProperty qtdefProperty() {
        return qtdef;
    }
    
    public void setQtdef(Integer qtdef) {
        this.qtdef.set(qtdef);
    }
    
    @Column(name = "QTDE_CANC")
    public Integer getQtdecanc() {
        return qtdecanc.get();
    }
    
    public IntegerProperty qtdecancProperty() {
        return qtdecanc;
    }
    
    public void setQtdecanc(Integer qtdecanc) {
        this.qtdecanc.set(qtdecanc);
    }
    
    @Column(name = "QTDE_ORIG")
    public Integer getQtdeorig() {
        return qtdeorig.get();
    }
    
    public IntegerProperty qtdeorigProperty() {
        return qtdeorig;
    }
    
    public void setQtdeorig(Integer qtdeorig) {
        this.qtdeorig.set(qtdeorig);
    }
    
    @Column(name = "VALOR_FAT")
    public BigDecimal getValorfat() {
        return valorfat.get();
    }
    
    public ObjectProperty<BigDecimal> valorfatProperty() {
        return valorfat;
    }
    
    public void setValorfat(BigDecimal valorfat) {
        this.valorfat.set(valorfat);
    }
    
    @Column(name = "VALOR_PEND")
    public BigDecimal getValorpend() {
        return valorpend.get();
    }
    
    public ObjectProperty<BigDecimal> valorpendProperty() {
        return valorpend;
    }
    
    public void setValorpend(BigDecimal valorpend) {
        this.valorpend.set(valorpend);
    }
    
    @Column(name = "VALOR_CANC")
    public BigDecimal getValorcanc() {
        return valorcanc.get();
    }
    
    public ObjectProperty<BigDecimal> valorcancProperty() {
        return valorcanc;
    }
    
    public void setValorcanc(BigDecimal valorcanc) {
        this.valorcanc.set(valorcanc);
    }
    
    @Column(name = "VALOR_ORIG")
    public BigDecimal getValororig() {
        return valororig.get();
    }
    
    public ObjectProperty<BigDecimal> valororigProperty() {
        return valororig;
    }
    
    public void setValororig(BigDecimal valororig) {
        this.valororig.set(valororig);
    }
    
    @Column(name = "VALOR_DESC")
    public BigDecimal getValordesc() {
        return valordesc.get();
    }
    
    public ObjectProperty<BigDecimal> valordescProperty() {
        return valordesc;
    }
    
    public void setValordesc(BigDecimal valordesc) {
        this.valordesc.set(valordesc);
    }
    
    @Column(name = "COMERCIAL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean getComercial() {
        return comercial.get();
    }
    
    public BooleanProperty comercialProperty() {
        return comercial;
    }
    
    public void setComercial(boolean comercial) {
        this.comercial.set(comercial);
    }
    
    @Column(name = "FINANCEIRO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean getFinanceiro() {
        return financeiro.get();
    }
    
    public BooleanProperty financeiroProperty() {
        return financeiro;
    }
    
    public void setFinanceiro(boolean financeiro) {
        this.financeiro.set(financeiro);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @Column(name = "OBSE")
    public String getObse() {
        return obse.get();
    }
    
    public StringProperty obseProperty() {
        return obse;
    }
    
    public void setObse(String obse) {
        this.obse.set(obse);
    }
    
}