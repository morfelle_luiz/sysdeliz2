package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class VSdDadosPedidoDataPK implements Serializable {
    
    
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> entrega = new SimpleObjectProperty<LocalDate>();
    
    public VSdDadosPedidoDataPK() {
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getEntrega() {
        return entrega.get();
    }
    
    public ObjectProperty<LocalDate> entregaProperty() {
        return entrega;
    }
    
    public void setEntrega(LocalDate entrega) {
        this.entrega.set(entrega);
    }
}