package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.models.ti.TabPreco;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author lima.joao
 * @since 12/09/2019 13:36
 */
@Entity
@Immutable
@Table(name="V_SD_DADOS_PRODUTO")
@TelaSysDeliz(descricao = "Produto", icon = "produto (4).png")
public class VSdDadosProduto extends BasicModel implements Serializable {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty(this, "descricao");
    private final StringProperty codFam = new SimpleStringProperty(this, "codFam");
    @ExibeTableView(descricao = "Família", width = 80)
    @ColunaFilter(descricao = "Família", coluna = "familia")
    private final StringProperty familia = new SimpleStringProperty(this, "familia");
    private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
    private final StringProperty codlin = new SimpleStringProperty(this, "codlin");
    private final StringProperty linha = new SimpleStringProperty(this, "linha");
    private final StringProperty codEtq = new SimpleStringProperty(this, "codEtq");
    private final StringProperty modelagem = new SimpleStringProperty(this, "modelagem");
    private final StringProperty grupomodelagem = new SimpleStringProperty(this, "grupomodelagem");
    private final StringProperty codMarca = new SimpleStringProperty(this, "codMarca");
    private final StringProperty marca = new SimpleStringProperty(this, "marca");
    private final StringProperty codCol = new SimpleStringProperty(this, "codCol");
    private final StringProperty colecao = new SimpleStringProperty(this, "colecao");
    private final StringProperty faixa = new SimpleStringProperty(this, "faixa");
    private final StringProperty grade = new SimpleStringProperty(this, "grade");
    private final BooleanProperty ativo = new SimpleBooleanProperty(this, "ativo");
    private final StringProperty liberacao = new SimpleStringProperty(this, "liberacao");
    private final IntegerProperty partesMolde = new SimpleIntegerProperty(this, "partesMolde");
    private final IntegerProperty minimo = new SimpleIntegerProperty(this, "minimo");
    private final StringProperty obs = new SimpleStringProperty(this, "obs");
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<>();
    private final ObjectProperty<SdProduto> sdProduto = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> ipi = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> custo = new SimpleObjectProperty<>();
    private final StringProperty codfis = new SimpleStringProperty();
    private final StringProperty codtrib = new SimpleStringProperty();
    private final StringProperty tribIpi = new SimpleStringProperty();
    private final StringProperty tribPis = new SimpleStringProperty();
    private final StringProperty tribCofins = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty origem = new SimpleStringProperty();
    private final StringProperty codsped = new SimpleStringProperty();
    private final StringProperty tecidoPrincipal = new SimpleStringProperty();
    private final StringProperty descTecido = new SimpleStringProperty();
    private final StringProperty genero = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtEntrega = new SimpleObjectProperty<>();
    private final StringProperty codGrupoModelagem = new SimpleStringProperty();

    private List<TabPreco> listTabPreco = new ArrayList<>();

    public VSdDadosProduto() {
    }
    
    public VSdDadosProduto(String codigo) {
        this.setCodigo(codigo);
    }
    
    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @Column(name = "CODFAM")
    public String getCodFam() {
        return codFam.get();
    }

    public StringProperty codFamProperty() {
        return codFam;
    }

    public void setCodFam(String codFam) {
        this.codFam.set(codFam);
    }

    @Column(name = "FAMILIA")
    public String getFamilia() {
        return familia.get();
    }

    public StringProperty familiaProperty() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia.set(familia);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "CODLIN")
    public String getCodlin() {
        return codlin.get();
    }

    public StringProperty codlinProperty() {
        return codlin;
    }

    public void setCodlin(String codlin) {
        this.codlin.set(codlin);
    }

    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    @Column(name = "CODETIQ")
    public String getCodEtq() {
        return codEtq.get();
    }

    public StringProperty codEtqProperty() {
        return codEtq;
    }

    public void setCodEtq(String codEtq) {
        this.codEtq.set(codEtq);
    }

    @Column(name = "MODELAGEM")
    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }

    @Column(name = "GRUPOMODELAGEM")
    public String getGrupomodelagem() {
        return grupomodelagem.get();
    }

    public StringProperty grupomodelagemProperty() {
        return grupomodelagem;
    }

    public void setGrupomodelagem(String grupomodelagem) {
        this.grupomodelagem.set(grupomodelagem);
    }

    @Column(name = "CODMARCA")
    public String getCodMarca() {
        return codMarca.get();
    }

    public StringProperty codMarcaProperty() {
        return codMarca;
    }

    public void setCodMarca(String codMarca) {
        this.codMarca.set(codMarca);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "CODCOL")
    public String getCodCol() {
        return codCol.get();
    }

    public StringProperty codColProperty() {
        return codCol;
    }

    public void setCodCol(String codCol) {
        this.codCol.set(codCol);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }

    public StringProperty faixaProperty() {
        return faixa;
    }

    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }

    @Column(name = "GRADE")
    public String getGrade() {
        return grade.get();
    }

    public StringProperty gradeProperty() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade.set(grade);
    }
    
    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }
    
    public BooleanProperty ativoProperty() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
    
    @Column(name = "LIBERACAO")
    public String getLiberacao() {
        return liberacao.get();
    }
    
    public StringProperty liberacaoProperty() {
        return liberacao;
    }
    
    public void setLiberacao(String liberacao) {
        this.liberacao.set(liberacao);
    }
    
    @Column(name = "PARTESMOLDE")
    public int getPartesMolde() {
        return partesMolde.get();
    }
    
    public IntegerProperty partesMoldeProperty() {
        return partesMolde;
    }
    
    public void setPartesMolde(int partesMolde) {
        this.partesMolde.set(partesMolde);
    }
    
    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }
    
    public StringProperty obsProperty() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs.set(obs);
    }
    
    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }
    
    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }
    
    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public SdProduto getSdProduto() {
        return sdProduto.get();
    }
    
    public ObjectProperty<SdProduto> sdProdutoProperty() {
        return sdProduto;
    }
    
    public void setSdProduto(SdProduto sdProduto) {
        this.sdProduto.set(sdProduto);
    }
    
    @Column(name = "MINIMO")
    public int getMinimo() {
        return minimo.get();
    }
    
    public IntegerProperty minimoProperty() {
        return minimo;
    }
    
    public void setMinimo(int minimo) {
        this.minimo.set(minimo);
    }
    
    @Column(name = "IPI")
    public BigDecimal getIpi() {
        return ipi.get();
    }
    
    public ObjectProperty<BigDecimal> ipiProperty() {
        return ipi;
    }
    
    public void setIpi(BigDecimal ipi) {
        this.ipi.set(ipi);
    }
    
    @Column(name = "CODFIS")
    public String getCodfis() {
        return codfis.get();
    }
    
    public StringProperty codfisProperty() {
        return codfis;
    }
    
    public void setCodfis(String codfis) {
        this.codfis.set(codfis);
    }
    
    @Column(name = "CODTRIB")
    public String getCodtrib() {
        return codtrib.get();
    }
    
    public StringProperty codtribProperty() {
        return codtrib;
    }
    
    public void setCodtrib(String codtrib) {
        this.codtrib.set(codtrib);
    }
    
    @Column(name = "TRIB_IPI")
    public String getTribIpi() {
        return tribIpi.get();
    }
    
    public StringProperty tribIpiProperty() {
        return tribIpi;
    }
    
    public void setTribIpi(String tribIpi) {
        this.tribIpi.set(tribIpi);
    }
    
    @Column(name = "TRIB_PIS")
    public String getTribPis() {
        return tribPis.get();
    }
    
    public StringProperty tribPisProperty() {
        return tribPis;
    }
    
    public void setTribPis(String tribPis) {
        this.tribPis.set(tribPis);
    }
    
    @Column(name = "TRIB_COFINS")
    public String getTribCofins() {
        return tribCofins.get();
    }
    
    public StringProperty tribCofinsProperty() {
        return tribCofins;
    }
    
    public void setTribCofins(String tribCofins) {
        this.tribCofins.set(tribCofins);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "CUSTO")
    public BigDecimal getCusto() {
        return custo.get();
    }
    
    public ObjectProperty<BigDecimal> custoProperty() {
        return custo;
    }
    
    public void setCusto(BigDecimal custo) {
        this.custo.set(custo);
    }
    
    @Column(name = "ORIGEM")
    public String getOrigem() {
        return origem.get();
    }
    
    public StringProperty origemProperty() {
        return origem;
    }
    
    public void setOrigem(String origem) {
        this.origem.set(origem);
    }
    
    @Column(name = "CODSPED")
    public String getCodsped() {
        return codsped.get();
    }
    
    public StringProperty codspedProperty() {
        return codsped;
    }
    
    public void setCodsped(String codsped) {
        this.codsped.set(codsped);
    }

    @Column(name = "TECIDOPRINCIAL")
    public String getTecidoPrincipal() {
        return tecidoPrincipal.get();
    }

    public StringProperty tecidoPrincipalProperty() {
        return tecidoPrincipal;
    }

    public void setTecidoPrincipal(String tecidoPrincipal) {
        this.tecidoPrincipal.set(tecidoPrincipal);
    }

    @Column(name = "DESCTECIDO")
    public String getDescTecido() {
        return descTecido.get();
    }

    public StringProperty descTecidoProperty() {
        return descTecido;
    }

    public void setDescTecido(String descTecido) {
        this.descTecido.set(descTecido);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "DTENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEntrega() {
        return dtEntrega.get();
    }

    public ObjectProperty<LocalDate> dtEntregaProperty() {
        return dtEntrega;
    }

    public void setDtEntrega(LocalDate dtEntrega) {
        this.dtEntrega.set(dtEntrega);
    }

    @Column(name = "CODGRUPOMODELAGEM")
    public String getCodGrupoModelagem() {
        return codGrupoModelagem.get();
    }

    public StringProperty codGrupoModelagemProperty() {
        return codGrupoModelagem;
    }

    public void setCodGrupoModelagem(String codGrupoModelagem) {
        this.codGrupoModelagem.set(codGrupoModelagem);
    }

    @Transient
    public List<TabPreco> getListTabPreco() {
        return listTabPreco;
    }

    public void setListTabPreco(List<TabPreco> listTabPreco) {
        this.listTabPreco = listTabPreco;
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VSdDadosProduto that = (VSdDadosProduto) o;
        return Objects.equals(codigo, that.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
