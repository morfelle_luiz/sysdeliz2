package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_SD_DADOS_PRODUTO_BARRA")
public class VSdDadosProdutoBarra {

    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty barra = new SimpleStringProperty();

    public VSdDadosProdutoBarra() {
    }

    @Id
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }
}
