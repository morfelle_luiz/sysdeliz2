package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@TelaSysDeliz(descricao = "Produto", icon = "produto (4).png")
@Table(name = "V_SD_DADOS_PRODUTO_COR_TAM")
public class VSdDadosProdutoCorTam extends BasicModel implements Serializable, Comparable {

    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    @ExibeTableView(descricao = "Cor", width = 80)
    @ColunaFilter(descricao = "Cor", coluna = "cor")
    private final StringProperty cor = new SimpleStringProperty();
    @ExibeTableView(descricao = "Tamanho", width = 80)
    @ColunaFilter(descricao = "Tamanho", coluna = "tam")
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty sku = new SimpleStringProperty();
    private final StringProperty codmarca = new SimpleStringProperty();
    @ExibeTableView(descricao = "Marca", width = 80)
    @ColunaFilter(descricao = "Marca", coluna = "marca")
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty codcol = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final ObjectProperty<Colecao> colAtual = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty posicao = new SimpleIntegerProperty();
    private final BooleanProperty possuiPedido = new SimpleBooleanProperty();
    private final ObjectProperty<Linha> linhaObj = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> produtoObj = new SimpleObjectProperty<>();

    public VSdDadosProdutoCorTam() {
    }

    public VSdDadosProdutoCorTam(String codigo, String cor, List<VSdDadosProdutoCorTam> list) {
        VSdDadosProdutoCorTam produto = list.get(0);
        this.codigo.set(codigo);
        this.descricao.set(produto.getDescricao());
        this.cor.set(cor);
        this.codmarca.set(produto.getCodmarca());
        this.marca.set(produto.getMarca());
        this.codcol.set(produto.getCodcol());
        this.colecao.set(produto.getColecao());
        this.linha.set(produto.getLinha());
        this.qtde.set(list.stream().mapToInt(VSdDadosProdutoCorTam::getQtde).sum());
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Id
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }


    @Column(name = "SKU")
    public String getSku() {
        return sku.get();
    }

    public StringProperty skuProperty() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku.set(sku);
    }

    @Column(name = "CODMARCA")
    public String getCodmarca() {
        return codmarca.get();
    }

    public StringProperty codmarcaProperty() {
        return codmarca;
    }

    public void setCodmarca(String codmarca) {
        this.codmarca.set(codmarca);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "CODCOL")
    public String getCodcol() {
        return codcol.get();
    }

    public StringProperty codcolProperty() {
        return codcol;
    }

    public void setCodcol(String codcol) {
        this.codcol.set(codcol);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "POSICAO")
    public int getPosicao() {
        return posicao.get();
    }

    public IntegerProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao.set(posicao);
    }

    @OneToOne
    @JoinColumn(name = "COL_ATUAL")
    public Colecao getColAtual() {
        return colAtual.get();
    }

    public ObjectProperty<Colecao> colAtualProperty() {
        return colAtual;
    }

    public void setColAtual(Colecao colAtual) {
        this.colAtual.set(colAtual);
    }

    @OneToOne
    @JoinColumn(name = "LINHA", insertable = false, updatable = false)
    public Linha getLinhaObj() {
        return linhaObj.get();
    }

    public ObjectProperty<Linha> linhaObjProperty() {
        return linhaObj;
    }

    public void setLinhaObj(Linha linhaObj) {
        this.linhaObj.set(linhaObj);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "POSSUI_PEDIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPossuiPedido() {
        return possuiPedido.get();
    }

    public BooleanProperty possuiPedidoProperty() {
        return possuiPedido;
    }

    public void setPossuiPedido(boolean possuiPedido) {
        this.possuiPedido.set(possuiPedido);
    }

    @OneToOne
    @JoinColumn(name = "CODIGO", insertable = false, updatable = false)
    public VSdDadosProduto getProdutoObj() {
        return produtoObj.get();
    }

    public ObjectProperty<VSdDadosProduto> produtoObjProperty() {
        return produtoObj;
    }

    public void setProdutoObj(VSdDadosProduto produtoObj) {
        this.produtoObj.set(produtoObj);
    }

    @PostLoad
    public void setCodigoFilterObj() {
        setCodigoFilter(getCodigo() + "-" + getCor() + "-" + getTam());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VSdDadosProdutoCorTam)) return false;
        VSdDadosProdutoCorTam that = (VSdDadosProdutoCorTam) o;
        return Objects.equals(getCodigo(), that.getCodigo()) && Objects.equals(getCor(), that.getCor()) && Objects.equals(getTam(), that.getTam());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getCor(), getTam());
    }

    @Override
    public int compareTo(@NotNull Object o) {
        return 0;
    }
}
