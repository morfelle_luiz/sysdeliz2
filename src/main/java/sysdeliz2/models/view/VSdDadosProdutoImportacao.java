package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.TabFis;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_DADOS_PRODUTO_IMPORTACAO")
public class VSdDadosProdutoImportacao {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty codigoPy = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty descricion = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> precoDolar = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty genero = new SimpleStringProperty();
    private final StringProperty composicao = new SimpleStringProperty();
    private final StringProperty grupoModelagem = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pesoMinimo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoMaximo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> pesoMedio = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<TabFis> codfis = new SimpleObjectProperty<>();

    public VSdDadosProdutoImportacao() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "CODIGOPY")
    public String getCodigoPy() {
        return codigoPy.get();
    }

    public StringProperty codigoPyProperty() {
        return codigoPy;
    }

    public void setCodigoPy(String codigoPy) {
        this.codigoPy.set(codigoPy);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "DESCRICION")
    public String getDescricion() {
        return descricion.get();
    }

    public StringProperty descricionProperty() {
        return descricion;
    }

    public void setDescricion(String descricion) {
        this.descricion.set(descricion);
    }

    @Column(name = "PRECO_DOLAR")
    public BigDecimal getPrecodolar() {
        return precoDolar.get();
    }

    public ObjectProperty<BigDecimal> precodolarProperty() {
        return precoDolar;
    }

    public void setPrecodolar(BigDecimal precodolar) {
        this.precoDolar.set(precodolar);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "COMPOSICAO")
    public String getComposicao() {
        return composicao.get();
    }

    public StringProperty composicaoProperty() {
        return composicao;
    }

    public void setComposicao(String composicao) {
        this.composicao.set(composicao);
    }

    @Column(name = "GRUPO_MODELAGEM")
    public String getGrupoModelagem() {
        return grupoModelagem.get();
    }

    public StringProperty grupoModelagemProperty() {
        return grupoModelagem;
    }

    public void setGrupoModelagem(String grupoModelagem) {
        this.grupoModelagem.set(grupoModelagem);
    }

    @Column(name = "PESO_MINIMO")
    public BigDecimal getPesoMinimo() {
        return pesoMinimo.get();
    }

    public ObjectProperty<BigDecimal> pesoMinimoProperty() {
        return pesoMinimo;
    }

    public void setPesoMinimo(BigDecimal pesoMinimo) {
        this.pesoMinimo.set(pesoMinimo);
    }

    @Column(name = "PESO_MAXIMO")
    public BigDecimal getPesoMaximo() {
        return pesoMaximo.get();
    }

    public ObjectProperty<BigDecimal> pesoMaximoProperty() {
        return pesoMaximo;
    }

    public void setPesoMaximo(BigDecimal pesoMaximo) {
        this.pesoMaximo.set(pesoMaximo);
    }

    @Column(name = "PESO_MEDIO")
    public BigDecimal getPesoMedio() {
        return pesoMedio.get();
    }

    public ObjectProperty<BigDecimal> pesoMedioProperty() {
        return pesoMedio;
    }

    public void setPesoMedio(BigDecimal pesoMedio) {
        this.pesoMedio.set(pesoMedio);
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }

    @OneToOne
    @JoinColumn(name = "CODFIS")
//    @Transient
    public TabFis getCodfis() {
        return codfis.get();
    }

    public ObjectProperty<TabFis> codfisProperty() {
        return codfis;
    }

    public void setCodfis(TabFis codfis) {
        this.codfis.set(codfis);
    }
}
