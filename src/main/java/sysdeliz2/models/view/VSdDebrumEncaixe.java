package sysdeliz2.models.view;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_DEBRUM_ENCAIXE")
public class VSdDebrumEncaixe {
    
    private final StringProperty id = new SimpleStringProperty();
    private final IntegerProperty plano = new SimpleIntegerProperty();
    private final IntegerProperty idrisco = new SimpleIntegerProperty();
    private final StringProperty aplicacao = new SimpleStringProperty();
    private final StringProperty corprod = new SimpleStringProperty();
    private final StringProperty material = new SimpleStringProperty();
    private final StringProperty cori = new SimpleStringProperty();
    private final StringProperty lote = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final StringProperty partida = new SimpleStringProperty();
    private final StringProperty sequencia = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty gramatura = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> consdebpc = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consdebtotal = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty qtdefolhas = new SimpleIntegerProperty();
    private final IntegerProperty qtdepecas = new SimpleIntegerProperty();
    
    public VSdDebrumEncaixe() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @Column(name = "PLANO")
    public Integer getPlano() {
        return plano.get();
    }
    
    public IntegerProperty planoProperty() {
        return plano;
    }
    
    public void setPlano(Integer plano) {
        this.plano.set(plano);
    }
    
    @Column(name = "ID_RISCO")
    public Integer getIdrisco() {
        return idrisco.get();
    }
    
    public IntegerProperty idriscoProperty() {
        return idrisco;
    }
    
    public void setIdrisco(Integer idrisco) {
        this.idrisco.set(idrisco);
    }
    
    @Column(name = "APLICACAO")
    public String getAplicacao() {
        return aplicacao.get();
    }
    
    public StringProperty aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(String aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @Column(name = "COR_PROD")
    public String getCorprod() {
        return corprod.get();
    }
    
    public StringProperty corprodProperty() {
        return corprod;
    }
    
    public void setCorprod(String corprod) {
        this.corprod.set(corprod);
    }
    
    @Column(name = "MATERIAL")
    public String getMaterial() {
        return material.get();
    }
    
    public StringProperty materialProperty() {
        return material;
    }
    
    public void setMaterial(String material) {
        this.material.set(material);
    }
    
    @Column(name = "COR_I")
    public String getCori() {
        return cori.get();
    }
    
    public StringProperty coriProperty() {
        return cori;
    }
    
    public void setCori(String cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }
    
    public StringProperty partidaProperty() {
        return partida;
    }
    
    public void setPartida(String partida) {
        this.partida.set(partida);
    }
    
    @Column(name = "SEQUENCIA")
    public String getSequencia() {
        return sequencia.get();
    }
    
    public StringProperty sequenciaProperty() {
        return sequencia;
    }
    
    public void setSequencia(String sequencia) {
        this.sequencia.set(sequencia);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "GRAMATURA")
    public Integer getGramatura() {
        return gramatura.get();
    }
    
    public IntegerProperty gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(Integer gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "CONS_DEB_PC")
    public BigDecimal getConsdebpc() {
        return consdebpc.get();
    }
    
    public ObjectProperty<BigDecimal> consdebpcProperty() {
        return consdebpc;
    }
    
    public void setConsdebpc(BigDecimal consdebpc) {
        this.consdebpc.set(consdebpc);
    }
    
    @Column(name = "CONS_DEB_TOTAL")
    public BigDecimal getConsdebtotal() {
        return consdebtotal.get();
    }
    
    public ObjectProperty<BigDecimal> consdebtotalProperty() {
        return consdebtotal;
    }
    
    public void setConsdebtotal(BigDecimal consdebtotal) {
        this.consdebtotal.set(consdebtotal);
    }
    
    @Column(name = "QTDE_FOLHAS")
    public Integer getQtdefolhas() {
        return qtdefolhas.get();
    }
    
    public IntegerProperty qtdefolhasProperty() {
        return qtdefolhas;
    }
    
    public void setQtdefolhas(Integer qtdefolhas) {
        this.qtdefolhas.set(qtdefolhas);
    }
    
    @Column(name = "QTDE_PECAS")
    public Integer getQtdepecas() {
        return qtdepecas.get();
    }
    
    public IntegerProperty qtdepecasProperty() {
        return qtdepecas;
    }
    
    public void setQtdepecas(Integer qtdepecas) {
        this.qtdepecas.set(qtdepecas);
    }
    
}