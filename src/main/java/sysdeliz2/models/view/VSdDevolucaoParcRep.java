package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_DEVOLUCAO_PARC_REP")
public class VSdDevolucaoParcRep {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtpagto = new SimpleObjectProperty<LocalDate>();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> com1 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valordevolucao = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorcomissao = new SimpleObjectProperty<>();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty nomerep = new SimpleStringProperty();
    
    public VSdDevolucaoParcRep() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "DT_PAGTO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtpagto() {
        return dtpagto.get();
    }
    
    public ObjectProperty<LocalDate> dtpagtoProperty() {
        return dtpagto;
    }
    
    public void setDtpagto(LocalDate dtpagto) {
        this.dtpagto.set(dtpagto);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "COM1")
    public BigDecimal getCom1() {
        return com1.get();
    }
    
    public ObjectProperty<BigDecimal> com1Property() {
        return com1;
    }
    
    public void setCom1(BigDecimal com1) {
        this.com1.set(com1);
    }
    
    @Column(name = "VALORDEVOLUCAO")
    public BigDecimal getValordevolucao() {
        return valordevolucao.get();
    }
    
    public ObjectProperty<BigDecimal> valordevolucaoProperty() {
        return valordevolucao;
    }
    
    public void setValordevolucao(BigDecimal valordevolucao) {
        this.valordevolucao.set(valordevolucao);
    }
    
    @Column(name = "VALORCOMISSAO")
    public BigDecimal getValorcomissao() {
        return valorcomissao.get();
    }
    
    public ObjectProperty<BigDecimal> valorcomissaoProperty() {
        return valorcomissao;
    }
    
    public void setValorcomissao(BigDecimal valorcomissao) {
        this.valorcomissao.set(valorcomissao);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "NOMEREP")
    public String getNomerep() {
        return nomerep.get();
    }
    
    public StringProperty nomerepProperty() {
        return nomerep;
    }
    
    public void setNomerep(String nomerep) {
        this.nomerep.set(nomerep);
    }
    
}