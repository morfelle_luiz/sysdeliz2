package sysdeliz2.models.view;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_DEVOLUCAO_REP")
public class VSdDevolucaoRep {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty tipodev = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final StringProperty nfdup = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty cliente = new SimpleStringProperty();
    private final StringProperty tabela = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty representante = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty pgto = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> perdesc = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> vtotal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valcomi = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> perccomissao = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty tipoitem = new SimpleStringProperty();
    
    public VSdDevolucaoRep() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "TIPODEV")
    public String getTipodev() {
        return tipodev.get();
    }
    
    public StringProperty tipodevProperty() {
        return tipodev;
    }
    
    public void setTipodev(String tipodev) {
        this.tipodev.set(tipodev);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "NF_DUP")
    public String getNfdup() {
        return nfdup.get();
    }
    
    public StringProperty nfdupProperty() {
        return nfdup;
    }
    
    public void setNfdup(String nfdup) {
        this.nfdup.set(nfdup);
    }
    
    @Column(name = "DT_EMISSAO")
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }
    
    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }
    
    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "CLIENTE")
    public String getCliente() {
        return cliente.get();
    }
    
    public StringProperty clienteProperty() {
        return cliente;
    }
    
    public void setCliente(String cliente) {
        this.cliente.set(cliente);
    }
    
    @Column(name = "TABELA")
    public String getTabela() {
        return tabela.get();
    }
    
    public StringProperty tabelaProperty() {
        return tabela;
    }
    
    public void setTabela(String tabela) {
        this.tabela.set(tabela);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "REPRESENTANTE")
    public String getRepresentante() {
        return representante.get();
    }
    
    public StringProperty representanteProperty() {
        return representante;
    }
    
    public void setRepresentante(String representante) {
        this.representante.set(representante);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "PGTO")
    public String getPgto() {
        return pgto.get();
    }
    
    public StringProperty pgtoProperty() {
        return pgto;
    }
    
    public void setPgto(String pgto) {
        this.pgto.set(pgto);
    }
    
    @Column(name = "PER_DESC")
    public BigDecimal getPerdesc() {
        return perdesc.get();
    }
    
    public ObjectProperty<BigDecimal> perdescProperty() {
        return perdesc;
    }
    
    public void setPerdesc(BigDecimal perdesc) {
        this.perdesc.set(perdesc);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "V_TOTAL")
    public BigDecimal getVtotal() {
        return vtotal.get();
    }
    
    public ObjectProperty<BigDecimal> vtotalProperty() {
        return vtotal;
    }
    
    public void setVtotal(BigDecimal vtotal) {
        this.vtotal.set(vtotal);
    }
    
    @Column(name = "VALCOMI")
    public BigDecimal getValcomi() {
        return valcomi.get();
    }
    
    public ObjectProperty<BigDecimal> valcomiProperty() {
        return valcomi;
    }
    
    public void setValcomi(BigDecimal valcomi) {
        this.valcomi.set(valcomi);
    }
    
    @Column(name = "PERC_COMISSAO")
    public BigDecimal getPerccomissao() {
        return perccomissao.get();
    }
    
    public ObjectProperty<BigDecimal> perccomissaoProperty() {
        return perccomissao;
    }
    
    public void setPerccomissao(BigDecimal perccomissao) {
        this.perccomissao.set(perccomissao);
    }
    
    @Column(name = "TIPOITEM")
    public String getTipoitem() {
        return tipoitem.get();
    }
    
    public StringProperty tipoitemProperty() {
        return tipoitem;
    }
    
    public void setTipoitem(String tipoitem) {
        this.tipoitem.set(tipoitem);
    }
    
}