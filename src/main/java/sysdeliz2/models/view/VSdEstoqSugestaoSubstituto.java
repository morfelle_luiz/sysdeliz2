package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_ESTOQ_SUGESTAO_SUBSTITUTO")
@Immutable
public class VSdEstoqSugestaoSubstituto {
    
    private final ObjectProperty<VSdEstoqSugestaoSubstitutoPK> id = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<BigDecimal>();
    
    public VSdEstoqSugestaoSubstituto() {
    }
    
    @EmbeddedId
    public VSdEstoqSugestaoSubstitutoPK getId() {
        return id.get();
    }
    
    public ObjectProperty<VSdEstoqSugestaoSubstitutoPK> idProperty() {
        return id;
    }
    
    public void setId(VSdEstoqSugestaoSubstitutoPK id) {
        this.id.set(id);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
}