package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class VSdEstoqSugestaoSubstitutoPK implements Serializable {
    
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty insumo = new SimpleStringProperty();
    private final StringProperty cori = new SimpleStringProperty();
    private final ObjectProperty<Material> insumosubs = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corsub = new SimpleObjectProperty<>();
    
    public VSdEstoqSugestaoSubstitutoPK() {
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }
    
    public StringProperty insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }
    
    @Column(name = "COR_I")
    public String getCori() {
        return cori.get();
    }
    
    public StringProperty coriProperty() {
        return cori;
    }
    
    public void setCori(String cori) {
        this.cori.set(cori);
    }
    
    @OneToOne
    @JoinColumn(name = "INSUMO_SUBS")
    public Material getInsumosubs() {
        return insumosubs.get();
    }
    
    public ObjectProperty<Material> insumosubsProperty() {
        return insumosubs;
    }
    
    public void setInsumosubs(Material insumosubs) {
        this.insumosubs.set(insumosubs);
    }
    
    @OneToOne
    @JoinColumn(name = "COR_SUB")
    public Cor getCorsub() {
        return corsub.get();
    }
    
    public ObjectProperty<Cor> corsubProperty() {
        return corsub;
    }
    
    public void setCorsub(Cor corsub) {
        this.corsub.set(corsub);
    }
    
}