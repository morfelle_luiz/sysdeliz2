package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_ESTOQUE_MAT_COR_BR")
public class VSdEstoqueMatCorBr {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final ObjectProperty<Material> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    
    public VSdEstoqueMatCorBr() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public Material getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<Material> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Material codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }
    
    public ObjectProperty<Cor> corProperty() {
        return cor;
    }
    
    public void setCor(Cor cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
}