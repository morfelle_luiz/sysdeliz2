package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Entidade;

import javax.persistence.*;

@Entity
@Table(name = "V_SD_FACCOES_OF")
public class VSdFaccoesOf {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty parte = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<Entidade> faccao = new SimpleObjectProperty<>();
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "PARTE")
    public String getParte() {
        return parte.get();
    }
    
    public StringProperty parteProperty() {
        return parte;
    }
    
    public void setParte(String parte) {
        this.parte.set(parte);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FACCAO")
    public Entidade getFaccao() {
        return faccao.get();
    }
    
    public ObjectProperty<Entidade> faccaoProperty() {
        return faccao;
    }
    
    public void setFaccao(Entidade faccao) {
        this.faccao.set(faccao);
    }
    
    
}