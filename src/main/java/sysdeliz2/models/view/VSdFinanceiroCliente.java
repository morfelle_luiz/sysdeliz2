package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_FINANCEIRO_CLIENTE")
@Immutable
public class VSdFinanceiroCliente {
    
    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valorped = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpedpend = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpedfat = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> descontoped = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorreceber = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valoratrasado = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpago = new SimpleObjectProperty<BigDecimal>();
    
    public VSdFinanceiroCliente() {
    }
    
    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "VALOR_PED")
    public BigDecimal getValorped() {
        return valorped.get();
    }
    
    public ObjectProperty<BigDecimal> valorpedProperty() {
        return valorped;
    }
    
    public void setValorped(BigDecimal valorped) {
        this.valorped.set(valorped);
    }
    
    @Column(name = "VALOR_PED_PEND")
    public BigDecimal getValorpedpend() {
        return valorpedpend.get();
    }
    
    public ObjectProperty<BigDecimal> valorpedpendProperty() {
        return valorpedpend;
    }
    
    public void setValorpedpend(BigDecimal valorpedpend) {
        this.valorpedpend.set(valorpedpend);
    }
    
    @Column(name = "VALOR_PED_FAT")
    public BigDecimal getValorpedfat() {
        return valorpedfat.get();
    }
    
    public ObjectProperty<BigDecimal> valorpedfatProperty() {
        return valorpedfat;
    }
    
    public void setValorpedfat(BigDecimal valorpedfat) {
        this.valorpedfat.set(valorpedfat);
    }
    
    @Column(name = "DESCONTO_PED")
    public BigDecimal getDescontoped() {
        return descontoped.get();
    }
    
    public ObjectProperty<BigDecimal> descontopedProperty() {
        return descontoped;
    }
    
    public void setDescontoped(BigDecimal descontoped) {
        this.descontoped.set(descontoped);
    }
    
    @Column(name = "VALOR_RECEBER")
    public BigDecimal getValorreceber() {
        return valorreceber.get();
    }
    
    public ObjectProperty<BigDecimal> valorreceberProperty() {
        return valorreceber;
    }
    
    public void setValorreceber(BigDecimal valorreceber) {
        this.valorreceber.set(valorreceber);
    }
    
    @Column(name = "VALOR_ATRASADO")
    public BigDecimal getValoratrasado() {
        return valoratrasado.get();
    }
    
    public ObjectProperty<BigDecimal> valoratrasadoProperty() {
        return valoratrasado;
    }
    
    public void setValoratrasado(BigDecimal valoratrasado) {
        this.valoratrasado.set(valoratrasado);
    }
    
    @Column(name = "VALOR_PAGO")
    public BigDecimal getValorpago() {
        return valorpago.get();
    }
    
    public ObjectProperty<BigDecimal> valorpagoProperty() {
        return valorpago;
    }
    
    public void setValorpago(BigDecimal valorpago) {
        this.valorpago.set(valorpago);
    }
    
}