package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "V_SD_GESTAO_MAT_FALTANTE")
public class VSdGestaoMatFaltante {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final StringProperty tipolinha = new SimpleStringProperty();
    private final StringProperty obsv = new SimpleStringProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cori = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
    private final BooleanProperty substituto = new SimpleBooleanProperty();
    private final ObjectProperty<LocalDateTime> dhcriacao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhanalise = new SimpleObjectProperty<>();
    private final ObjectProperty<CadFluxo> setoratual = new SimpleObjectProperty<>();
    private final ObjectProperty<Entidade> faccao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<>();
    private final StringProperty fatura = new SimpleStringProperty();
    private final StringProperty observacao = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();

    public VSdGestaoMatFaltante() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "OBSV")
    public String getObsv() {
        return obsv.get();
    }
    
    public StringProperty obsvProperty() {
        return obsv;
    }
    
    public void setObsv(String obsv) {
        this.obsv.set(obsv);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR_I")
    public Cor getCori() {
        return cori.get();
    }
    
    public ObjectProperty<Cor> coriProperty() {
        return cori;
    }
    
    public void setCori(Cor cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SETOR")
    public CadFluxo getSetor() {
        return setor.get();
    }
    
    public ObjectProperty<CadFluxo> setorProperty() {
        return setor;
    }
    
    public void setSetor(CadFluxo setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "SUBSTITUTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isSubstituto() {
        return substituto.get();
    }
    
    public BooleanProperty substitutoProperty() {
        return substituto;
    }
    
    public void setSubstituto(Boolean substituto) {
        this.substituto.set(substituto);
    }
    
    @Column(name = "DH_CRIACAO")
    public LocalDateTime getDhcriacao() {
        return dhcriacao.get();
    }
    
    public ObjectProperty<LocalDateTime> dhcriacaoProperty() {
        return dhcriacao;
    }
    
    public void setDhcriacao(LocalDateTime dhcriacao) {
        this.dhcriacao.set(dhcriacao);
    }
    
    @Column(name = "DH_ANALISE")
    public LocalDateTime getDhanalise() {
        return dhanalise.get();
    }
    
    public ObjectProperty<LocalDateTime> dhanaliseProperty() {
        return dhanalise;
    }
    
    public void setDhanalise(LocalDateTime dhanalise) {
        this.dhanalise.set(dhanalise);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SETOR_ATUAL")
    public CadFluxo getSetoratual() {
        return setoratual.get();
    }
    
    public ObjectProperty<CadFluxo> setoratualProperty() {
        return setoratual;
    }
    
    public void setSetoratual(CadFluxo setoratual) {
        this.setoratual.set(setoratual);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FACCAO")
    public Entidade getFaccao() {
        return faccao.get();
    }

    public ObjectProperty<Entidade> faccaoProperty() {
        return faccao;
    }

    public void setFaccao(Entidade faccao) {
        this.faccao.set(faccao);
    }
    
    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "TIPO_LINHA")
    public String getTipolinha() {
        return tipolinha.get();
    }
    
    public StringProperty tipolinhaProperty() {
        return tipolinha;
    }
    
    public void setTipolinha(String tipolinha) {
        this.tipolinha.set(tipolinha);
    }

    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }

    public StringProperty paisProperty() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais.set(pais);
    }
}
