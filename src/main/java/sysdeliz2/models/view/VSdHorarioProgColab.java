package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.sysdeliz.SdSetorOp001;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "V_SD_HORARIO_PROG_COLAB")
public class VSdHorarioProgColab implements Serializable {
    
    private final StringProperty id = new SimpleStringProperty();
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<>();
    private final StringProperty pacote = new SimpleStringProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();
    private final StringProperty agrup = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();
    private final ObjectProperty<SdSetorOp001> setorOp = new SimpleObjectProperty<>();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> referencia = new SimpleObjectProperty<>();
    private final IntegerProperty seqColaborador = new SimpleIntegerProperty();
    private final IntegerProperty qtdePend = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tempoTotal = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<>();
    private final BooleanProperty inWork = new SimpleBooleanProperty();
    private final BooleanProperty working = new SimpleBooleanProperty();
    
    public VSdHorarioProgColab() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CELULA")
    public SdCelula getCelula() {
        return celula.get();
    }
    
    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }
    
    public void setCelula(SdCelula celula) {
        this.celula.set(celula);
    }
    
    @Column(name = "PACOTE")
    public String getPacote() {
        return pacote.get();
    }
    
    public StringProperty pacoteProperty() {
        return pacote;
    }
    
    public void setPacote(String pacote) {
        this.pacote.set(pacote);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }
    
    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(SdOperacaoOrganize001 operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name = "AGRUP")
    public String getAgrup() {
        return agrup.get();
    }
    
    public StringProperty agrupProperty() {
        return agrup;
    }
    
    public void setAgrup(String agrup) {
        this.agrup.set(agrup);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "QUEBRA")
    public int getQuebra() {
        return quebra.get();
    }
    
    public IntegerProperty quebraProperty() {
        return quebra;
    }
    
    public void setQuebra(int quebra) {
        this.quebra.set(quebra);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SETOR_OP")
    public SdSetorOp001 getSetorOp() {
        return setorOp.get();
    }
    
    public ObjectProperty<SdSetorOp001> setorOpProperty() {
        return setorOp;
    }
    
    public void setSetorOp(SdSetorOp001 setorOp) {
        this.setorOp.set(setorOp);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "REFERENCIA")
    public VSdDadosProduto getReferencia() {
        return referencia.get();
    }
    
    public ObjectProperty<VSdDadosProduto> referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(VSdDadosProduto referencia) {
        this.referencia.set(referencia);
    }
    
    @Column(name = "SEQ_COLABORADOR")
    public int getSeqColaborador() {
        return seqColaborador.get();
    }
    
    public IntegerProperty seqColaboradorProperty() {
        return seqColaborador;
    }
    
    public void setSeqColaborador(int seqColaborador) {
        this.seqColaborador.set(seqColaborador);
    }
    
    @Column(name = "QTDE_PEND")
    public int getQtdePend() {
        return qtdePend.get();
    }
    
    public IntegerProperty qtdePendProperty() {
        return qtdePend;
    }
    
    public void setQtdePend(int qtdePend) {
        this.qtdePend.set(qtdePend);
    }
    
    @Column(name = "TEMPO_TOTAL")
    public BigDecimal getTempoTotal() {
        return tempoTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tempoTotalProperty() {
        return tempoTotal;
    }
    
    public void setTempoTotal(BigDecimal tempoTotal) {
        this.tempoTotal.set(tempoTotal);
    }
    
    @Column(name = "DH_INI")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name = "DH_FIM")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.dhFim.set(dhFim);
    }
    
    @Column(name = "IN_WORK")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isInWork() {
        return inWork.get();
    }
    
    public BooleanProperty inWorkProperty() {
        return inWork;
    }
    
    public void setInWork(boolean inWork) {
        this.inWork.set(inWork);
    }
    
    @Column(name = "WORKING")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isWorking() {
        return working.get();
    }
    
    public BooleanProperty workingProperty() {
        return working;
    }
    
    public void setWorking(boolean working) {
        this.working.set(working);
    }
}
