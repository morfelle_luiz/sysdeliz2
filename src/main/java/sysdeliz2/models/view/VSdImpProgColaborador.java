package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class VSdImpProgColaborador {
    
    private final IntegerProperty codigo = new SimpleIntegerProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final IntegerProperty qtdeOper = new SimpleIntegerProperty();
    private final IntegerProperty qtdeImp = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty();
    
    public VSdImpProgColaborador() {
    }
    
    public VSdImpProgColaborador(Integer codigo, String nome, Integer qtdeOper, Integer qtdeImp, String status){
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.qtdeOper.set(qtdeOper);
        this.qtdeImp.set(qtdeImp);
        this.status.set(status);
    }
    
    public int getCodigo() {
        return codigo.get();
    }
    
    public IntegerProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }
    
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    public int getQtdeOper() {
        return qtdeOper.get();
    }
    
    public IntegerProperty qtdeOperProperty() {
        return qtdeOper;
    }
    
    public void setQtdeOper(int qtdeOper) {
        this.qtdeOper.set(qtdeOper);
    }
    
    public int getQtdeImp() {
        return qtdeImp.get();
    }
    
    public IntegerProperty qtdeImpProperty() {
        return qtdeImp;
    }
    
    public void setQtdeImp(int qtdeImp) {
        this.qtdeImp.set(qtdeImp);
    }
    
    public String getStatus() {
        return status.get();
    }
    
    public StringProperty statusProperty() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
}
