package sysdeliz2.models.view;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import org.hibernate.annotations.Immutable;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.SdPacote001;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.SQLException;

@Entity
@Immutable
@Table(name="V_SD_INFO_PROGRAMACAO_SETOR")
public class VSdInfoProgramacaoSetor {
    
    private final StringProperty programacao = new SimpleStringProperty();
    private final IntegerProperty setor = new SimpleIntegerProperty();
    private final IntegerProperty qtdeProgramada = new SimpleIntegerProperty();
    private final IntegerProperty ordemProg = new SimpleIntegerProperty();
    private final IntegerProperty codigoCelula = new SimpleIntegerProperty();
    private final StringProperty descCelula = new SimpleStringProperty();
    private final StringProperty codigoProduto = new SimpleStringProperty();
    private final StringProperty descProduto = new SimpleStringProperty();
    private final StringProperty codigoCor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty ofProg = new SimpleStringProperty();
    private final IntegerProperty qtdeOf = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> percProd = new SimpleObjectProperty<>();
    
    private final ListProperty<SdPacote001> pacotesLancamento = new SimpleListProperty<>();
    
    @Id
    @Column(name="PROGRAMACAO")
    public String getProgramacao() {
        return programacao.get();
    }
    
    public StringProperty programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(String programacao) {
        this.programacao.set(programacao);
    }
    
    @Column(name="SETOR")
    public int getSetor() {
        return setor.get();
    }
    
    public IntegerProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(int setor) {
        this.setor.set(setor);
    }
    
    @Column(name="QTDE_PROGRAMADA")
    public int getQtdeProgramada() {
        return qtdeProgramada.get();
    }
    
    public IntegerProperty qtdeProgramadaProperty() {
        return qtdeProgramada;
    }
    
    public void setQtdeProgramada(int qtdeProgramada) {
        this.qtdeProgramada.set(qtdeProgramada);
    }
    
    @Column(name="ORDEM_PROG")
    public int getOrdemProg() {
        return ordemProg.get();
    }
    
    public IntegerProperty ordemProgProperty() {
        return ordemProg;
    }
    
    public void setOrdemProg(int ordemProg) {
        this.ordemProg.set(ordemProg);
    }
    
    @Column(name="CELULA")
    public int getCodigoCelula() {
        return codigoCelula.get();
    }
    
    public IntegerProperty codigoCelulaProperty() {
        return codigoCelula;
    }
    
    public void setCodigoCelula(int codigoCelula) {
        this.codigoCelula.set(codigoCelula);
    }
    
    @Column(name="DESC_CELULA")
    public String getDescCelula() {
        return descCelula.get();
    }
    
    public StringProperty descCelulaProperty() {
        return descCelula;
    }
    
    public void setDescCelula(String descCelula) {
        this.descCelula.set(descCelula);
    }
    
    @Column(name="PRODUTO")
    public String getCodigoProduto() {
        return codigoProduto.get();
    }
    
    public StringProperty codigoProdutoProperty() {
        return codigoProduto;
    }
    
    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto.set(codigoProduto);
    }
    
    @Column(name="DESC_PRODUTO")
    public String getDescProduto() {
        return descProduto.get();
    }
    
    public StringProperty descProdutoProperty() {
        return descProduto;
    }
    
    public void setDescProduto(String descProduto) {
        this.descProduto.set(descProduto);
    }
    
    @Column(name="COR")
    public String getCodigoCor() {
        return codigoCor.get();
    }
    
    public StringProperty codigoCorProperty() {
        return codigoCor;
    }
    
    public void setCodigoCor(String codigoCor) {
        this.codigoCor.set(codigoCor);
    }
    
    @Column(name="DESC_COR")
    public String getDescCor() {
        return descCor.get();
    }
    
    public StringProperty descCorProperty() {
        return descCor;
    }
    
    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }
    
    @Column(name="TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name="OF_PROG")
    public String getOfProg() {
        return ofProg.get();
    }
    
    public StringProperty ofProgProperty() {
        return ofProg;
    }
    
    public void setOfProg(String ofProg) {
        this.ofProg.set(ofProg);
    }
    
    @Column(name="QTDE_OF")
    public int getQtdeOf() {
        return qtdeOf.get();
    }
    
    public IntegerProperty qtdeOfProperty() {
        return qtdeOf;
    }
    
    public void setQtdeOf(int qtdeOf) {
        this.qtdeOf.set(qtdeOf);
    }
    
    @Column(name="PERC_PROD")
    public BigDecimal getPercProd() {
        return percProd.get();
    }
    
    public ObjectProperty<BigDecimal> percProdProperty() {
        return percProd;
    }
    
    public void setPercProd(BigDecimal percProd) {
        this.percProd.set(percProd);
    }
    
    @Transient
    public ObservableList<SdPacote001> getPacotesLancamento() throws SQLException {
        if(pacotesLancamento.get() == null || pacotesLancamento.get().size() == 0){
            pacotesLancamento.set(DAOFactory.getSdPacoteLancamento001DAO().getByPacoteProg(this.programacao.get()));
        }
        return pacotesLancamento.get();
    }
    
    public ListProperty<SdPacote001> pacotesLancamentoProperty() throws SQLException {
        if(pacotesLancamento.get() == null || pacotesLancamento.get().size() == 0){
            pacotesLancamento.set(DAOFactory.getSdPacoteLancamento001DAO().getByPacoteProg(this.programacao.get()));
        }
        return pacotesLancamento;
    }
    
    public void setPacotesLancamento(ObservableList<SdPacote001> pacotesLancamento) {
        this.pacotesLancamento.set(pacotesLancamento);
    }
}
