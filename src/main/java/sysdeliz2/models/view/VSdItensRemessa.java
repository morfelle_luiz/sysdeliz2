package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_SD_ITENS_REMESSA")
public class VSdItensRemessa {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final StringProperty modelagem = new SimpleStringProperty();
    private final StringProperty grupomodelagem = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    
    public VSdItensRemessa() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "REMESSA")
    public String getRemessa() {
        return remessa.get();
    }
    
    public StringProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name = "FAMILIA")
    public String getFamilia() {
        return familia.get();
    }
    
    public StringProperty familiaProperty() {
        return familia;
    }
    
    public void setFamilia(String familia) {
        this.familia.set(familia);
    }
    
    @Column(name = "MODELAGEM")
    public String getModelagem() {
        return modelagem.get();
    }
    
    public StringProperty modelagemProperty() {
        return modelagem;
    }
    
    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }
    
    @Column(name = "GRUPOMODELAGEM")
    public String getGrupomodelagem() {
        return grupomodelagem.get();
    }
    
    public StringProperty grupomodelagemProperty() {
        return grupomodelagem;
    }
    
    public void setGrupomodelagem(String grupomodelagem) {
        this.grupomodelagem.set(grupomodelagem);
    }
    
    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }
    
    public StringProperty linhaProperty() {
        return linha;
    }
    
    public void setLinha(String linha) {
        this.linha.set(linha);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
}
