package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_LANCAMENTOS_REP")
public class VSdLancamentosRep {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty codrep = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtlan = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> debito = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> credito = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty descricao = new SimpleStringProperty();
    
    public VSdLancamentosRep() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "DT_LAN")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtlan() {
        return dtlan.get();
    }
    
    public ObjectProperty<LocalDate> dtlanProperty() {
        return dtlan;
    }
    
    public void setDtlan(LocalDate dtlan) {
        this.dtlan.set(dtlan);
    }
    
    @Column(name = "DEBITO")
    public BigDecimal getDebito() {
        return debito.get();
    }
    
    public ObjectProperty<BigDecimal> debitoProperty() {
        return debito;
    }
    
    public void setDebito(BigDecimal debito) {
        this.debito.set(debito);
    }
    
    @Column(name = "CREDITO")
    public BigDecimal getCredito() {
        return credito.get();
    }
    
    public ObjectProperty<BigDecimal> creditoProperty() {
        return credito;
    }
    
    public void setCredito(BigDecimal credito) {
        this.credito.set(credito);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
}