package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "v_sd_linha_bid")
public class VSdLinhaBid extends BasicModel implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final StringProperty codigo = new SimpleStringProperty();
        private final StringProperty marca = new SimpleStringProperty();

        public Pk() {
        }

        @Column(name = "codigo")
        public String getCodigo() {
            return codigo.get();
        }

        public StringProperty codigoProperty() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo.set(codigo);
        }

        @Column(name = "marca")
        public String getMarca() {
            return marca.get();
        }

        public StringProperty marcaProperty() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca.set(marca);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final StringProperty descricao = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();

    public VSdLinhaBid() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
        setCodigoFilter(id.getCodigo());
    }

    @Column(name = "descricao")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
        setDescricaoFilter(descricao);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Override
    public String toString() {
        return "[" + id.get().getCodigo() + "] " + descricao.get();
    }
}