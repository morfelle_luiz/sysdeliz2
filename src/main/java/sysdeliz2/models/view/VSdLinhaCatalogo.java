package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_LINHA_CATALOGO")
@TelaSysDeliz(descricao = "Linha", icon = "linha (4).png")
public class VSdLinhaCatalogo extends BasicModel implements Serializable {

    @Transient
    @ExibeTableView(descricao = "Código", width = 50)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private final StringProperty codigo = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private final StringProperty descricao = new SimpleStringProperty();
    @Transient
    @ExibeTableView(descricao = "Marca", width = 120)
    @ColunaFilter(descricao = "Marca", coluna = "marca", filterClass = "sysdeliz2.models.ti.Marca")
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();

    public VSdLinhaCatalogo() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        setCodigoFilter(codigo);
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        setDescricaoFilter(descricao);
        this.descricao.set(descricao);
    }

    @OneToOne
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Override
    public String toString() {
        return "[" + codigo.get() + "] " + descricao.get();
    }
}