package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_LOCAL_EXPEDICAO")
public class VSdLocalExpedicao implements Serializable {

    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty andar = new SimpleStringProperty();
    private final StringProperty rua = new SimpleStringProperty();
    private final IntegerProperty indice = new SimpleIntegerProperty();
    private final StringProperty posicao = new SimpleStringProperty();
    private final StringProperty lado = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final IntegerProperty estoque = new SimpleIntegerProperty();
    private final StringProperty tipoLinha = new SimpleStringProperty();
    private final StringProperty genero = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();

    public VSdLocalExpedicao() {
    }

    public VSdLocalExpedicao(String lado, String produto, String cor ) {
        this.lado.set(lado);
        this.produto.set(produto);
        this.cor.set(cor);
    }

    public VSdLocalExpedicao(String lado, String produto, String cor, String local ) {
        this.lado.set(lado);
        this.produto.set(produto);
        this.cor.set(cor);
        this.local.set(local);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "ANDAR")
    public String getAndar() {
        return andar.get();
    }

    public StringProperty andarProperty() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar.set(andar);
    }

    @Column(name = "RUA")
    public String getRua() {
        return rua.get();
    }

    public StringProperty ruaProperty() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua.set(rua);
    }

    @Column(name = "INDICE")
    public int getIndice() {
        return indice.get();
    }

    public IntegerProperty indiceProperty() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice.set(indice);
    }

    @Column(name = "POSICAO")
    public String getPosicao() {
        return posicao.get();
    }

    public StringProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao.set(posicao);
    }

    @Column(name = "LADO")
    public String getLado() {
        return lado.get();
    }

    public StringProperty ladoProperty() {
        return lado;
    }

    public void setLado(String lado) {
        this.lado.set(lado);
    }

    @Id
    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "ESTOQUE")
    public Integer getEstoque() {
        return estoque.get();
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "TIPO_LINHA")
    public String getTipoLinha() {
        return tipoLinha.get();
    }

    public StringProperty tipoLinhaProperty() {
        return tipoLinha;
    }

    public void setTipoLinha(String tipoLinha) {
        this.tipoLinha.set(tipoLinha);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }
}
