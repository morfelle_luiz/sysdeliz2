package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "V_SD_LOCAL_EXPEDICAO_QUANT")
@TelaSysDeliz(descricao = "Local", icon = "deposito_50.png")
public class VSdLocalExpedicaoQuant extends BasicModel {

    private final ObjectProperty<VSdLocalExpedicaoQuantPK> id = new SimpleObjectProperty<>();
    private final StringProperty andar = new SimpleStringProperty();
    private final StringProperty rua = new SimpleStringProperty();
    private final StringProperty posicao = new SimpleStringProperty();
    private final StringProperty lado = new SimpleStringProperty();
    @ExibeTableView(descricao = "Estoque", width = 150)
    @ColunaFilter(descricao = "Estoque", coluna = "estoque")
    private final IntegerProperty estoque = new SimpleIntegerProperty();
    private final IntegerProperty producao = new SimpleIntegerProperty();
    private final IntegerProperty acabamento = new SimpleIntegerProperty();
    private final IntegerProperty qtdeTotal = new SimpleIntegerProperty();

    public VSdLocalExpedicaoQuant() {
    }

    @EmbeddedId
    public VSdLocalExpedicaoQuantPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdLocalExpedicaoQuantPK> idProperty() {
        return id;
    }

    public void setId(VSdLocalExpedicaoQuantPK id) {
        this.id.set(id);
    }

    @Column(name = "ANDAR")
    public String getAndar() {
        return andar.get();
    }

    public StringProperty andarProperty() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar.set(andar);
    }

    @Column(name = "RUA")
    public String getRua() {
        return rua.get();
    }

    public StringProperty ruaProperty() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua.set(rua);
    }

    @Column(name = "POSICAO")
    public String getPosicao() {
        return posicao.get();
    }

    public StringProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao.set(posicao);
    }

    @Column(name = "LADO")
    public String getLado() {
        return lado.get();
    }

    public StringProperty ladoProperty() {
        return lado;
    }

    public void setLado(String lado) {
        this.lado.set(lado);
    }

    @Column(name = "ESTOQUE")
    public Integer getEstoque() {
        return estoque.get();
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "PRODUCAO")
    public int getProducao() {
        return producao.get();
    }

    public IntegerProperty producaoProperty() {
        return producao;
    }

    public void setProducao(int producao) {
        this.producao.set(producao);
    }

    @Column(name = "ACABAMENTO")
    public int getAcabamento() {
        return acabamento.get();
    }

    public IntegerProperty acabamentoProperty() {
        return acabamento;
    }

    public void setAcabamento(int acabamento) {
        this.acabamento.set(acabamento);
    }

    @Column(name = "QTDE_TOTAL")
    public int getQtdeTotal() {
        return qtdeTotal.get();
    }

    public IntegerProperty qtdeTotalProperty() {
        return qtdeTotal;
    }

    public void setQtdeTotal(int qtdeTotal) {
        this.qtdeTotal.set(qtdeTotal);
    }
}
