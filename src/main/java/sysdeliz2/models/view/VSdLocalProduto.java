package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_SD_LOCAL_PRODUTO")
public class VSdLocalProduto {

    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty piso = new SimpleStringProperty();
    private final StringProperty rua = new SimpleStringProperty();
    private final StringProperty posicao = new SimpleStringProperty();
    private final StringProperty lado = new SimpleStringProperty();
    private final StringProperty andar = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();

    public VSdLocalProduto() {
    }

    @Id
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "PISO")
    public String getPiso() {
        return piso.get();
    }

    public StringProperty pisoProperty() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso.set(piso);
    }

    @Column(name = "RUA")
    public String getRua() {
        return rua.get();
    }

    public StringProperty ruaProperty() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua.set(rua);
    }

    @Column(name = "POSICAO")
    public String getPosicao() {
        return posicao.get();
    }

    public StringProperty posicaoProperty() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao.set(posicao);
    }

    @Column(name = "LADO")
    public String getLado() {
        return lado.get();
    }

    public StringProperty ladoProperty() {
        return lado;
    }

    public void setLado(String lado) {
        this.lado.set(lado);
    }

    @Column(name = "ANDAR")
    public String getAndar() {
        return andar.get();
    }

    public StringProperty andarProperty() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar.set(andar);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }
}
