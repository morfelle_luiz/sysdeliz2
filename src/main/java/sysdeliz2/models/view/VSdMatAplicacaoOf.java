package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.ti.Pcpapl;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_MAT_APLICACAO_OF")
public class VSdMatAplicacaoOf {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cori = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacao = new SimpleObjectProperty<>();
    private final ObjectProperty<Pcpapl> aplicacaoft = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumopc = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumototal = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> minimoConsumo = new SimpleObjectProperty<BigDecimal>();
    
    public VSdMatAplicacaoOf() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR_I")
    public Cor getCori() {
        return cori.get();
    }
    
    public ObjectProperty<Cor> coriProperty() {
        return cori;
    }
    
    public void setCori(Cor cori) {
        this.cori.set(cori);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APLICACAO")
    public Pcpapl getAplicacao() {
        return aplicacao.get();
    }
    
    public ObjectProperty<Pcpapl> aplicacaoProperty() {
        return aplicacao;
    }
    
    public void setAplicacao(Pcpapl aplicacao) {
        this.aplicacao.set(aplicacao);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APLICACAO_FT")
    public Pcpapl getAplicacaoft() {
        return aplicacaoft.get();
    }
    
    public ObjectProperty<Pcpapl> aplicacaoftProperty() {
        return aplicacaoft;
    }
    
    public void setAplicacaoft(Pcpapl aplicacaoft) {
        this.aplicacaoft.set(aplicacaoft);
    }
    
    @Column(name = "CONSUMO_PC")
    public BigDecimal getConsumopc() {
        return consumopc.get();
    }
    
    public ObjectProperty<BigDecimal> consumopcProperty() {
        return consumopc;
    }
    
    public void setConsumopc(BigDecimal consumopc) {
        this.consumopc.set(consumopc);
    }
    
    @Column(name = "CONSUMO_TOTAL")
    public BigDecimal getConsumototal() {
        return consumototal.get();
    }
    
    public ObjectProperty<BigDecimal> consumototalProperty() {
        return consumototal;
    }
    
    public void setConsumototal(BigDecimal consumototal) {
        this.consumototal.set(consumototal);
    }

    @Column(name = "MINIMO_CONSUMO")
    public BigDecimal getMinimoConsumo() {
        return minimoConsumo.get();
    }

    public ObjectProperty<BigDecimal> minimoConsumoProperty() {
        return minimoConsumo;
    }

    public void setMinimoConsumo(BigDecimal minimoConsumo) {
        this.minimoConsumo.set(minimoConsumo);
    }
    
}