package sysdeliz2.models.view;

import javafx.beans.property.*;
import javafx.collections.ObservableMap;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.models.ti.Material;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_MATERIAIS_CORTE")
@Immutable
public class VSdMateriaisCorte extends BasicModel {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final ObjectProperty<Material> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cori = new SimpleObjectProperty<>();
    private final StringProperty lote = new SimpleStringProperty();
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> reserva = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> reservamt = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> resmtConsumir = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> resmtConsumirOrig = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> qtdeConsumido = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty folhas = new SimpleIntegerProperty();
    private final IntegerProperty folhasConsumidasOrig = new SimpleIntegerProperty();
    private final IntegerProperty folhasConsumidas = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> estoque = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> estoquemt = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> saldo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> saldomt = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty local = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty partida = new SimpleStringProperty();
    private final StringProperty sequencia = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baixado = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> baixadomt = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumo = new SimpleObjectProperty<BigDecimal>();
    private final IntegerProperty folhasCorpo = new SimpleIntegerProperty(0);
    private final IntegerProperty pecasCorpo = new SimpleIntegerProperty(0);
    private final MapProperty<String, BigDecimal> consumoOutraCor = new SimpleMapProperty<>();
    
    public VSdMateriaisCorte() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public Material getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<Material> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(Material codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_I")
    public Cor getCori() {
        return cori.get();
    }
    
    public ObjectProperty<Cor> coriProperty() {
        return cori;
    }
    
    public void setCori(Cor cori) {
        this.cori.set(cori);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }
    
    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "RESERVA")
    public BigDecimal getReserva() {
        return reserva.get();
    }
    
    public ObjectProperty<BigDecimal> reservaProperty() {
        return reserva;
    }
    
    public void setReserva(BigDecimal reserva) {
        this.reserva.set(reserva);
    }
    
    @Column(name = "RESERVA_MT")
    public BigDecimal getReservamt() {
        return reservamt.get();
    }
    
    public ObjectProperty<BigDecimal> reservamtProperty() {
        return reservamt;
    }
    
    public void setReservamt(BigDecimal reservamt) {
        this.resmtConsumir.set(reservamt);
        this.setQtdeConsumido(reservamt);
        this.reservamt.set(reservamt);
    }
    
    @Transient
    public BigDecimal getResmtConsumir() {
        return resmtConsumir.get();
    }
    
    public ObjectProperty<BigDecimal> resmtConsumirProperty() {
        return resmtConsumir;
    }
    
    public void setResmtConsumir(BigDecimal resmtConsumir) {
        this.resmtConsumir.set(resmtConsumir);
    }
    
    @Transient
    public BigDecimal getResmtConsumirOrig() {
        return resmtConsumirOrig.get();
    }
    
    public ObjectProperty<BigDecimal> resmtConsumirOrigProperty() {
        return resmtConsumirOrig;
    }
    
    public void setResmtConsumirOrig(BigDecimal resmtConsumirOrig) {
        this.resmtConsumirOrig.set(resmtConsumirOrig);
    }
    
    @Transient
    public BigDecimal getQtdeConsumido() {
        return qtdeConsumido.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeConsumidoProperty() {
        return qtdeConsumido;
    }
    
    public void setQtdeConsumido(BigDecimal qtdeConsumido) {
        this.qtdeConsumido.set(qtdeConsumido);
    }
    
    @Transient
    public Integer getFolhas() {
        return folhas.get();
    }
    
    public IntegerProperty folhasProperty() {
        return folhas;
    }
    
    public void setFolhas(Integer folhas) {
        this.folhas.set(folhas);
    }
    
    @Transient
    public Integer getFolhasConsumidas() {
        return folhasConsumidas.get();
    }
    
    public IntegerProperty folhasConsumidasProperty() {
        return folhasConsumidas;
    }
    
    public void setFolhasConsumidas(Integer folhasConsumidas) {
        this.folhasConsumidas.set(folhasConsumidas);
    }
    
    @Transient
    public Integer getFolhasConsumidasOrig() {
        return folhasConsumidasOrig.get();
    }
    
    public IntegerProperty folhasConsumidasOrigProperty() {
        return folhasConsumidasOrig;
    }
    
    public void setFolhasConsumidasOrig(Integer folhasConsumidasOrig) {
        this.folhasConsumidasOrig.set(folhasConsumidasOrig);
    }
    
    @Column(name = "ESTOQUE")
    public BigDecimal getEstoque() {
        return estoque.get();
    }
    
    public ObjectProperty<BigDecimal> estoqueProperty() {
        return estoque;
    }
    
    public void setEstoque(BigDecimal estoque) {
        this.estoque.set(estoque);
    }
    
    @Column(name = "ESTOQUE_MT")
    public BigDecimal getEstoquemt() {
        return estoquemt.get();
    }
    
    public ObjectProperty<BigDecimal> estoquemtProperty() {
        return estoquemt;
    }
    
    public void setEstoquemt(BigDecimal estoquemt) {
        this.estoquemt.set(estoquemt);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }
    
    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }
    
    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }
    
    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }
    
    public StringProperty partidaProperty() {
        return partida;
    }
    
    public void setPartida(String partida) {
        this.partida.set(partida);
    }
    
    @Column(name = "SEQUENCIA")
    public String getSequencia() {
        return sequencia.get();
    }
    
    public StringProperty sequenciaProperty() {
        return sequencia;
    }
    
    public void setSequencia(String sequencia) {
        this.sequencia.set(sequencia);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }
    
    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }
    
    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }
    
    @Column(name = "BAIXADO")
    public BigDecimal getBaixado() {
        return baixado.get();
    }
    
    public ObjectProperty<BigDecimal> baixadoProperty() {
        return baixado;
    }
    
    public void setBaixado(BigDecimal baixado) {
        this.baixado.set(baixado);
    }
    
    @Column(name = "BAIXADO_MT")
    public BigDecimal getBaixadomt() {
        return baixadomt.get();
    }
    
    public ObjectProperty<BigDecimal> baixadomtProperty() {
        return baixadomt;
    }
    
    public void setBaixadomt(BigDecimal baixadomt) {
        this.baixadomt.set(baixadomt);
    }
    
    @Column(name = "CONSUMO")
    public BigDecimal getConsumo() {
        return consumo.get();
    }
    
    public ObjectProperty<BigDecimal> consumoProperty() {
        return consumo;
    }
    
    public void setConsumo(BigDecimal consumo) {
        this.consumo.set(consumo);
    }
    
    @Column(name = "SALDO")
    public BigDecimal getSaldo() {
        return saldo.get();
    }
    
    public ObjectProperty<BigDecimal> saldoProperty() {
        return saldo;
    }
    
    public void setSaldo(BigDecimal saldo) {
        this.saldo.set(saldo);
    }
    
    @Column(name = "SALDO_MT")
    public BigDecimal getSaldomt() {
        return saldomt.get();
    }
    
    public ObjectProperty<BigDecimal> saldomtProperty() {
        return saldomt;
    }
    
    public void setSaldomt(BigDecimal saldomt) {
        this.saldomt.set(saldomt);
    }
    
    @Transient
    public ObservableMap<String, BigDecimal> getConsumoOutraCor() {
        return consumoOutraCor.get();
    }
    
    public MapProperty<String, BigDecimal> consumoOutraCorProperty() {
        return consumoOutraCor;
    }
    
    public void setConsumoOutraCor(ObservableMap<String, BigDecimal> consumoOutraCor) {
        this.consumoOutraCor.set(consumoOutraCor);
    }
    
    @Transient
    public int getFolhasCorpo() {
        return folhasCorpo.get();
    }
    
    public IntegerProperty folhasCorpoProperty() {
        return folhasCorpo;
    }
    
    public void setFolhasCorpo(int folhasCorpo) {
        this.folhasCorpo.set(folhasCorpo);
    }
    
    @Transient
    public int getPecasCorpo() {
        return pecasCorpo.get();
    }
    
    public IntegerProperty pecasCorpoProperty() {
        return pecasCorpo;
    }
    
    public void setPecasCorpo(int pecasCorpo) {
        this.pecasCorpo.set(pecasCorpo);
    }
}