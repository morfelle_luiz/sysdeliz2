package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_MATERIAIS_MALHA")
@TelaSysDeliz(descricao = "Material", icon = "material_50.png")
public class VSdMateriaisMalha extends BasicModel implements Serializable {

    @Transient
    private final StringProperty idjpa = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Material> codigoretorno = new SimpleObjectProperty<>();
    @Transient
    private final StringProperty unidade = new SimpleStringProperty();
    @Transient
    private final StringProperty unicom = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<Material> codigo = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<Material> fio = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> perc = new SimpleObjectProperty<>();
    @ExibeTableView(descricao = "Cor", width = 200)
    @ColunaFilter(descricao = "Cor", coluna = "cor")
    @Transient
    private final StringProperty corret = new SimpleStringProperty();
    @Transient
    private final ObjectProperty<BigDecimal> custotint = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> custotecel = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> custofio = new SimpleObjectProperty<>();
    @Transient
    private final ObjectProperty<BigDecimal> custototal = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> custototalTemp = new SimpleObjectProperty<>();

    @ExibeTableView(descricao = "Cor Fornecedor", width = 200)
    @ColunaFilter(descricao = "Cor Fornecedor", coluna = "corfor")
    @Transient
    private final StringProperty corfor = new SimpleStringProperty();
    @ExibeTableView(descricao = "Cod Fornecedor", width = 300)
    @ColunaFilter(descricao = "Cod Fornecedor", coluna = "codfor")
    @Transient
    private final ObjectProperty<Entidade> codfor = new SimpleObjectProperty<>();


    public VSdMateriaisMalha() {
    }

    @Id
    @Column(name = "idjpa")
    public String getIdjpa() {
        return idjpa.get();
    }

    public StringProperty idjpaProperty() {
        return idjpa;
    }

    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGORETORNO")
    public Material getCodigoretorno() {
        return codigoretorno.get();
    }

    public ObjectProperty<Material> codigoretornoProperty() {
        return codigoretorno;
    }

    public void setCodigoretorno(Material codigoretorno) {
        this.codigoretorno.set(codigoretorno);
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }

    @Column(name = "UNI_COM")
    public String getUnicom() {
        return unicom.get();
    }

    public StringProperty unicomProperty() {
        return unicom;
    }

    public void setUnicom(String unicom) {
        this.unicom.set(unicom);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public Material getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Material> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Material codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FIO")
    public Material getFio() {
        return fio.get();
    }

    public ObjectProperty<Material> fioProperty() {
        return fio;
    }

    public void setFio(Material fio) {
        this.fio.set(fio);
    }

    @Column(name = "CUSTOTINT")
    public BigDecimal getCustotint() {
        return custotint.get();
    }

    public ObjectProperty<BigDecimal> custotintProperty() {
        return custotint;
    }

    public void setCustotint(BigDecimal custotint) {
        this.custotint.set(custotint);
    }

    @Column(name = "CUSTOTECEL")
    public BigDecimal getCustotecel() {
        return custotecel.get();
    }

    public ObjectProperty<BigDecimal> custotecelProperty() {
        return custotecel;
    }

    public void setCustotecel(BigDecimal custotecel) {
        this.custotecel.set(custotecel);
    }

    @Column(name = "CUSTOFIO")
    public BigDecimal getCustofio() {
        return custofio.get();
    }

    public ObjectProperty<BigDecimal> custofioProperty() {
        return custofio;
    }

    public void setCustofio(BigDecimal custofio) {
        this.custofio.set(custofio);
    }

    @Column(name = "CUSTOTOTAL")
    public BigDecimal getCustototal() {
        return custototal.get();
    }

    public ObjectProperty<BigDecimal> custototalProperty() {
        return custototal;
    }

    public void setCustototal(BigDecimal custototal) {
        this.custototal.set(custototal);
    }

    @Transient
    public BigDecimal getCustototalTemp() {
        return custototalTemp.get();
    }

    public ObjectProperty<BigDecimal> custototalTempProperty() {
        return custototalTemp;
    }

    public void setCustototalTemp(BigDecimal custototalTemp) {
        this.custototalTemp.set(custototalTemp);
    }

    @Column(name = "PERC")
    public BigDecimal getPerc() {
        return perc.get();
    }

    public ObjectProperty<BigDecimal> percProperty() {
        return perc;
    }

    public void setPerc(BigDecimal perc) {
        this.perc.set(perc);
    }

    @Column(name = "COR")
    public String getCorret() {
        return corret.get();
    }

    public StringProperty corretProperty() {
        return corret;
    }

    public void setCorret(String corret) {
        this.corret.set(corret);
    }


    @Column(name = "CORFOR")
    public String getCorfor() {
        return corfor.get();
    }

    public StringProperty corforProperty() {
        return corfor;
    }

    public void setCorfor(String corfor) {
        this.corfor.set(corfor);
    }

    @OneToOne
    @JoinColumn(name = "CODFOR")
    public Entidade getCodfor() {
        return codfor.get();
    }

    public ObjectProperty<Entidade> codforProperty() {
        return codfor;
    }

    public void setCodfor(Entidade codfor) {
        this.codfor.set(codfor);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VSdMateriaisMalha other = (VSdMateriaisMalha) obj;
        return this.getCodfor() == other.getCodfor();
    }

    @PostLoad
    private void loadCustoTotal() {
        setCustototalTemp(this.getCustototal());
    }
}