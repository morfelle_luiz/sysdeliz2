package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_MATERIAIS_PARA_COLETA")
public class VSdMateriaisParaColeta {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cori = new SimpleObjectProperty<>();
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdeb = new SimpleObjectProperty<>();
    private final StringProperty situacao = new SimpleStringProperty();
    private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
    private final StringProperty lote = new SimpleStringProperty();
    private final BooleanProperty coleta = new SimpleBooleanProperty();
    private final BooleanProperty substituto = new SimpleBooleanProperty();
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty depagrupador = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final IntegerProperty idPcpftof = new SimpleIntegerProperty();
    
    public VSdMateriaisParaColeta() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "ORDEM")
    public Integer getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_I")
    public Cor getCori() {
        return cori.get();
    }
    
    public ObjectProperty<Cor> coriProperty() {
        return cori;
    }
    
    public void setCori(Cor cori) {
        this.cori.set(cori);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }
    
    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "QTDE_B")
    public BigDecimal getQtdeb() {
        return qtdeb.get();
    }
    
    public ObjectProperty<BigDecimal> qtdebProperty() {
        return qtdeb;
    }
    
    public void setQtdeb(BigDecimal qtdeb) {
        this.qtdeb.set(qtdeb);
    }
    
    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }
    
    public StringProperty situacaoProperty() {
        return situacao;
    }
    
    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SETOR")
    public CadFluxo getSetor() {
        return setor.get();
    }
    
    public ObjectProperty<CadFluxo> setorProperty() {
        return setor;
    }
    
    public void setSetor(CadFluxo setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }
    
    public StringProperty loteProperty() {
        return lote;
    }
    
    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
    @Column(name = "COLETA")
    @Convert(converter = BooleanAttributeConverter.class)
    public Boolean isColeta() {
        return coleta.get();
    }
    
    public BooleanProperty coletaProperty() {
        return coleta;
    }
    
    public void setColeta(Boolean coleta) {
        this.coleta.set(coleta);
    }
    
    @Column(name = "SUBSTITUTO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSubstituto() {
        return substituto.get();
    }
    
    public BooleanProperty substitutoProperty() {
        return substituto;
    }
    
    public void setSubstituto(boolean substituto) {
        this.substituto.set(substituto);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }
    
    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }
    
    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }
    
    public StringProperty localProperty() {
        return local;
    }
    
    public void setLocal(String local) {
        this.local.set(local);
    }
    
    @Column(name = "DEP_AGRUPADOR")
    public String getDepagrupador() {
        return depagrupador.get();
    }
    
    public StringProperty depagrupadorProperty() {
        return depagrupador;
    }
    
    public void setDepagrupador(String depagrupador) {
        this.depagrupador.set(depagrupador);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }
    
    public StringProperty faixaProperty() {
        return faixa;
    }
    
    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }
    
    @Column(name = "ID_PCPFTOF")
    public int getIdPcpftof() {
        return idPcpftof.get();
    }
    
    public IntegerProperty idPcpftofProperty() {
        return idPcpftof;
    }
    
    public void setIdPcpftof(int idPcpftof) {
        this.idPcpftof.set(idPcpftof);
    }
}