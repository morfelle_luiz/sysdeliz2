package sysdeliz2.models.view;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.Cor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_MATERIAIS_LOTE")
public class VSdMateriasLote {
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
    private final StringProperty lote = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> largura = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty partida = new SimpleStringProperty();
    private final StringProperty sequencia = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> gramatura = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty obs = new SimpleStringProperty();

    public VSdMateriasLote() {
    }

    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }

    public StringProperty idjpaProperty() {
        return idjpa;
    }

    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @JoinColumn(name = "COR")
    @OneToOne(fetch = FetchType.LAZY)
    public Cor getCor() {
        return cor.get();
    }

    public ObjectProperty<Cor> corProperty() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor.set(cor);
    }

    @Column(name = "LOTE")
    public String getLote() {
        return lote.get();
    }

    public StringProperty loteProperty() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }

    @Column(name = "LARGURA")
    public BigDecimal getLargura() {
        return largura.get();
    }

    public ObjectProperty<BigDecimal> larguraProperty() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura.set(largura);
    }

    @Column(name = "PARTIDA")
    public String getPartida() {
        return partida.get();
    }

    public StringProperty partidaProperty() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida.set(partida);
    }

    @Column(name = "SEQUENCIA")
    public String getSequencia() {
        return sequencia.get();
    }

    public StringProperty sequenciaProperty() {
        return sequencia;
    }

    public void setSequencia(String sequencia) {
        this.sequencia.set(sequencia);
    }

    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }

    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }

    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }

    @Column(name = "GRAMATURA")
    public BigDecimal getGramatura() {
        return gramatura.get();
    }

    public ObjectProperty<BigDecimal> gramaturaProperty() {
        return gramatura;
    }

    public void setGramatura(BigDecimal gramatura) {
        this.gramatura.set(gramatura);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }
}
