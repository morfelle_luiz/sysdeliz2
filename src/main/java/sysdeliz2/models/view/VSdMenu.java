package sysdeliz2.models.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="V_SD_MENUS")
@TelaSysDeliz(descricao = "Telas", icon = "cliente (4).png")
public class VSdMenu extends BasicModel {

    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "codTela")
    private StringProperty codTela = new SimpleStringProperty();
    @ExibeTableView(descricao = "Nome", width = 250)
    @ColunaFilter(descricao = "Nome", coluna = "nomeTela")
    private StringProperty nomeTela = new SimpleStringProperty();

    public VSdMenu() {
    }

    @Id
    @Column(name="COD_TELA")
    public String getCodTela() {
        return codTela.get();
    }

    public StringProperty codTelaProperty() {
        return codTela;
    }

    public void setCodTela(String codTela) {
        setCodigoFilter(codTela);
        this.codTela.set(codTela);
    }

    @Column(name="NOME_TELA")
    public String getNomeTela() {
        return nomeTela.get();
    }

    public StringProperty nomeTelaProperty() {
        return nomeTela;
    }

    public void setNomeTela(String nomeTela) {
        setDescricaoFilter(nomeTela);
        this.nomeTela.set(nomeTela);
    }
}
