package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;

import javax.persistence.*;

@Entity
@Table(name = "V_SD_MKT_PEDIDO")
public class VSdMktPedido {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty uncli = new SimpleStringProperty();
    private final StringProperty unped = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdeentped = new SimpleIntegerProperty();
    private final IntegerProperty qtdeentcli = new SimpleIntegerProperty();
    private final IntegerProperty estoque = new SimpleIntegerProperty();

    public VSdMktPedido() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }
    
    public ObjectProperty<Cor> corProperty() {
        return cor;
    }
    
    public void setCor(Cor cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name = "UN_CLI")
    public String getUncli() {
        return uncli.get();
    }
    
    public StringProperty uncliProperty() {
        return uncli;
    }
    
    public void setUncli(String uncli) {
        this.uncli.set(uncli);
    }
    
    @Column(name = "UN_PED")
    public String getUnped() {
        return unped.get();
    }
    
    public StringProperty unpedProperty() {
        return unped;
    }
    
    public void setUnped(String unped) {
        this.unped.set(unped);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_ENT_PED")
    public Integer getQtdeentped() {
        return qtdeentped.get();
    }
    
    public IntegerProperty qtdeentpedProperty() {
        return qtdeentped;
    }
    
    public void setQtdeentped(Integer qtdeentped) {
        this.qtdeentped.set(qtdeentped);
    }
    
    @Column(name = "QTDE_ENT_CLI")
    public Integer getQtdeentcli() {
        return qtdeentcli.get();
    }
    
    public IntegerProperty qtdeentcliProperty() {
        return qtdeentcli;
    }
    
    public void setQtdeentcli(Integer qtdeentcli) {
        this.qtdeentcli.set(qtdeentcli);
    }

    @Column(name = "ESTOQUE")
    public int getEstoque() {
        return estoque.get();
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque.set(estoque);
    }
}