package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "SD_MRP_ESTOQUE_B2B_001")
public class VSdMrpEstoqueB2b extends BasicModel implements Serializable {
    
    private final StringProperty id = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty faixa = new SimpleStringProperty();
    private final StringProperty descFaixa = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final StringProperty entrega = new SimpleStringProperty();
    private final StringProperty descEntrega = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtFim = new SimpleObjectProperty<>();
    private final BooleanProperty b2b = new SimpleBooleanProperty();
    private List<VSdEstoqueB2B> tamanhos = new ArrayList<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdeDigitado = new SimpleIntegerProperty(0);
    
    public VSdMrpEstoqueB2b() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }
    
    public StringProperty colecaoProperty() {
        return colecao;
    }
    
    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }
    
    @Column(name = "FAIXA")
    public String getFaixa() {
        return faixa.get();
    }
    
    public StringProperty faixaProperty() {
        return faixa;
    }
    
    public void setFaixa(String faixa) {
        this.faixa.set(faixa);
    }
    
    @Column(name = "DESC_FAIXA")
    public String getDescFaixa() {
        return descFaixa.get();
    }
    
    public StringProperty descFaixaProperty() {
        return descFaixa;
    }
    
    public void setDescFaixa(String descFaixa) {
        this.descFaixa.set(descFaixa);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name = "DESC_COR")
    public String getDescCor() {
        return descCor.get();
    }
    
    public StringProperty descCorProperty() {
        return descCor;
    }
    
    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }
    
    @Column(name = "ENTREGA")
    public String getEntrega() {
        return entrega.get();
    }
    
    public StringProperty entregaProperty() {
        return entrega;
    }
    
    public void setEntrega(String entrega) {
        this.entrega.set(entrega);
    }
    
    @Column(name = "DESC_ENTREGA")
    public String getDescEntrega() {
        return descEntrega.get();
    }
    
    public StringProperty descEntregaProperty() {
        return descEntrega;
    }
    
    public void setDescEntrega(String descEntrega) {
        this.descEntrega.set(descEntrega);
    }
    
    @Column(name = "DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtInicio() {
        return dtInicio.get();
    }
    
    public ObjectProperty<LocalDate> dtInicioProperty() {
        return dtInicio;
    }
    
    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio.set(dtInicio);
    }
    
    @Column(name = "DT_FIM")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtFim() {
        return dtFim.get();
    }
    
    public ObjectProperty<LocalDate> dtFimProperty() {
        return dtFim;
    }
    
    public void setDtFim(LocalDate dtFim) {
        this.dtFim.set(dtFim);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "B2B")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isB2b() {
        return b2b.get();
    }
    
    public BooleanProperty b2bProperty() {
        return b2b;
    }
    
    public void setB2b(boolean b2b) {
        this.b2b.set(b2b);
        super.setSelected(b2b);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO"),
            @JoinColumn(name = "COR", referencedColumnName = "COR"),
            @JoinColumn(name = "ENTREGA", referencedColumnName = "ENTREGA")
    })
    public List<VSdEstoqueB2B> getTamanhos() {
        return tamanhos;
    }
    
    public void setTamanhos(List<VSdEstoqueB2B> tamanhos) {
        this.tamanhos = tamanhos;
    }
    
    @Transient
    public int getQtdeDigitado() {
        return qtdeDigitado.get();
    }
    
    public IntegerProperty qtdeDigitadoProperty() {
        return qtdeDigitado;
    }
    
    public void setQtdeDigitado(int qtdeDigitado) {
        this.qtdeDigitado.set(qtdeDigitado);
    }
    
    @Transient
    public VSdEstoqueB2B getTamanho(String tamanho){
        Optional<VSdEstoqueB2B> estoqueTam = getTamanhos().stream().filter(estoque -> estoque.getId().getTam().equals(tamanho)).findFirst();
        return !estoqueTam.isPresent() ? null : estoqueTam.get();
    }
    
    @Transient
    public String toProduto() { return "[" + this.codigo.get() + "] " + this.descricao.get(); }
    
    @Transient
    public String toCor() {
        return "[" + this.cor.get() + "] " + this.descCor.get();
    }
}
