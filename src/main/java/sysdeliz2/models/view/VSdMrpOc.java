package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.sys.annotations.ExportExcel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_MRP_OC")
public class VSdMrpOc implements Serializable {
    @ExportExcel(nameToShow = "Of")
    private final StringProperty numero = new SimpleStringProperty();
    @ExportExcel(nameToShow = "O.C.")
    private final StringProperty ordcompra = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Lote")
    private final StringProperty periodo = new SimpleStringProperty();
    @ExportExcel(nameToShow = "Dt lote")
    private final ObjectProperty<LocalDate> dtlote = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> consumo = new SimpleObjectProperty<>();
    private final StringProperty unidade = new SimpleStringProperty();


    public VSdMrpOc() {
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Id
    @Column(name = "ORD_COMPRA")
    public String getOrdcompra() {
        return ordcompra.get();
    }

    public StringProperty ordcompraProperty() {
        return ordcompra;
    }

    public void setOrdcompra(String ordcompra) {
        this.ordcompra.set(ordcompra);
    }

    @Id
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Column(name = "DT_LOTE")
    public LocalDate getDtlote() {
        return dtlote.get();
    }

    public ObjectProperty<LocalDate> dtloteProperty() {
        return dtlote;
    }

    public void setDtlote(LocalDate dtlote) {
        this.dtlote.set(dtlote);
    }

    @Column(name = "CONSUMO")
    public BigDecimal getConsumo() {
        return consumo.get();
    }

    public ObjectProperty<BigDecimal> consumoProperty() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo.set(consumo);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }

    public StringProperty unidadeProperty() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
}
