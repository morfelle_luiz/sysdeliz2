package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_MRP_TINTURARIA")
public class VSdMrpTinturaria implements Serializable {
    
    private final StringProperty idJpa = new SimpleStringProperty();
    private final StringProperty statusmrp = new SimpleStringProperty();
    private final ObjectProperty<Of1> numeroof = new SimpleObjectProperty<>();
    private final StringProperty loteOf = new SimpleStringProperty();
    private final StringProperty statusof = new SimpleStringProperty();
    private final StringProperty setorof = new SimpleStringProperty();
    private final StringProperty partesetor = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corproduto = new SimpleObjectProperty<>();
    private final ObjectProperty<Material> subnivel = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corsubnivel = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtdenec = new SimpleObjectProperty<>();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corinsumo = new SimpleObjectProperty<>();
    private final StringProperty grpcomprador = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty tipoinsumo = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumoof = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumido = new SimpleObjectProperty<>();
    private final StringProperty consumidode = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty ordcompra = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> consumoestq = new SimpleObjectProperty<>();
    private final StringProperty unidadeestq = new SimpleStringProperty();
    private final ObjectProperty<Entidade> forinsumo = new SimpleObjectProperty<>();
    private final StringProperty origemMrp = new SimpleStringProperty();
    
    public VSdMrpTinturaria() {
    }
    
    public VSdMrpTinturaria(Material insumo, String deposito, BigDecimal sumConsumido, Long countConsumido) {
        this.insumo.set(insumo);
        this.deposito.set(deposito);
        this.consumido.set(sumConsumido);
        this.consumoestq.set(new BigDecimal(countConsumido));
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdJpa() {
        return idJpa.get();
    }
    
    public StringProperty idJpaProperty() {
        return idJpa;
    }
    
    public void setIdJpa(String idJpa) {
        this.idJpa.set(idJpa);
    }
    
    @Column(name = "STATUS_MRP")
    public String getStatusmrp() {
        return statusmrp.get();
    }
    
    public StringProperty statusmrpProperty() {
        return statusmrp;
    }
    
    public void setStatusmrp(String statusmrp) {
        this.statusmrp.set(statusmrp);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO_OF")
    public Of1 getNumeroof() {
        return numeroof.get();
    }
    
    public ObjectProperty<Of1> numeroofProperty() {
        return numeroof;
    }
    
    public void setNumeroof(Of1 numeroof) {
        this.numeroof.set(numeroof);
    }
    
    @Column(name = "LOTE_OF")
    public String getLoteOf() {
        return loteOf.get();
    }
    
    public StringProperty loteOfProperty() {
        return loteOf;
    }
    
    public void setLoteOf(String loteOf) {
        this.loteOf.set(loteOf);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusof() {
        return statusof.get();
    }
    
    public StringProperty statusofProperty() {
        return statusof;
    }
    
    public void setStatusof(String statusof) {
        this.statusof.set(statusof);
    }
    
    @Column(name = "SETOR_OF")
    public String getSetorof() {
        return setorof.get();
    }
    
    public StringProperty setorofProperty() {
        return setorof;
    }
    
    public void setSetorof(String setorof) {
        this.setorof.set(setorof);
    }
    
    @Column(name = "PARTE_SETOR")
    public String getPartesetor() {
        return partesetor.get();
    }
    
    public StringProperty partesetorProperty() {
        return partesetor;
    }
    
    public void setPartesetor(String partesetor) {
        this.partesetor.set(partesetor);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUTO")
    public Produto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        this.produto.set(produto);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_PRODUTO")
    public Cor getCorproduto() {
        return corproduto.get();
    }
    
    public ObjectProperty<Cor> corprodutoProperty() {
        return corproduto;
    }
    
    public void setCorproduto(Cor corproduto) {
        this.corproduto.set(corproduto);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUBNIVEL")
    public Material getSubnivel() {
        return subnivel.get();
    }
    
    public ObjectProperty<Material> subnivelProperty() {
        return subnivel;
    }
    
    public void setSubnivel(Material subnivel) {
        this.subnivel.set(subnivel);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_SUBNIVEL")
    public Cor getCorsubnivel() {
        return corsubnivel.get();
    }
    
    public ObjectProperty<Cor> corsubnivelProperty() {
        return corsubnivel;
    }
    
    public void setCorsubnivel(Cor corsubnivel) {
        this.corsubnivel.set(corsubnivel);
    }
    
    @Column(name = "QTDE_NEC")
    public BigDecimal getQtdenec() {
        return qtdenec.get();
    }
    
    public ObjectProperty<BigDecimal> qtdenecProperty() {
        return qtdenec;
    }
    
    public void setQtdenec(BigDecimal qtdenec) {
        this.qtdenec.set(qtdenec);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_INSUMO")
    public Cor getCorinsumo() {
        return corinsumo.get();
    }
    
    public ObjectProperty<Cor> corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(Cor corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @Column(name = "GRP_COMPRADOR")
    public String getGrpcomprador() {
        return grpcomprador.get();
    }
    
    public StringProperty grpcompradorProperty() {
        return grpcomprador;
    }
    
    public void setGrpcomprador(String grpcomprador) {
        this.grpcomprador.set(grpcomprador);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "TIPO_INSUMO")
    public String getTipoinsumo() {
        return tipoinsumo.get();
    }
    
    public StringProperty tipoinsumoProperty() {
        return tipoinsumo;
    }
    
    public void setTipoinsumo(String tipoinsumo) {
        this.tipoinsumo.set(tipoinsumo);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "CONSUMO_OF")
    public BigDecimal getConsumoof() {
        return consumoof.get();
    }
    
    public ObjectProperty<BigDecimal> consumoofProperty() {
        return consumoof;
    }
    
    public void setConsumoof(BigDecimal consumoof) {
        this.consumoof.set(consumoof);
    }
    
    @Column(name = "CONSUMIDO")
    public BigDecimal getConsumido() {
        return consumido.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoProperty() {
        return consumido;
    }
    
    public void setConsumido(BigDecimal consumido) {
        this.consumido.set(consumido);
    }
    
    @Column(name = "CONSUMIDO_DE")
    public String getConsumidode() {
        return consumidode.get();
    }
    
    public StringProperty consumidodeProperty() {
        return consumidode;
    }
    
    public void setConsumidode(String consumidode) {
        this.consumidode.set(consumidode);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "ORD_COMPRA")
    public String getOrdcompra() {
        return ordcompra.get();
    }
    
    public StringProperty ordcompraProperty() {
        return ordcompra;
    }
    
    public void setOrdcompra(String ordcompra) {
        this.ordcompra.set(ordcompra);
    }
    
    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "CONSUMO_ESTQ")
    public BigDecimal getConsumoestq() {
        return consumoestq.get();
    }
    
    public ObjectProperty<BigDecimal> consumoestqProperty() {
        return consumoestq;
    }
    
    public void setConsumoestq(BigDecimal consumoestq) {
        this.consumoestq.set(consumoestq);
    }
    
    @Column(name = "UNIDADE_ESTQ")
    public String getUnidadeestq() {
        return unidadeestq.get();
    }
    
    public StringProperty unidadeestqProperty() {
        return unidadeestq;
    }
    
    public void setUnidadeestq(String unidadeestq) {
        this.unidadeestq.set(unidadeestq);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOR_INSUMO")
    public Entidade getForinsumo() {
        return forinsumo.get();
    }
    
    public ObjectProperty<Entidade> forinsumoProperty() {
        return forinsumo;
    }
    
    public void setForinsumo(Entidade forinsumo) {
        this.forinsumo.set(forinsumo);
    }

    @Column(name = "ORI_MRP")
    public String getOrigemMrp() {
        return origemMrp.get();
    }

    public StringProperty origemMrpProperty() {
        return origemMrp;
    }

    public void setOrigemMrp(String origemMrp) {
        this.origemMrp.set(origemMrp);
    }
}