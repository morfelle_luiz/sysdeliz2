package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Material;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_MRP_TINTURARIA_ACUMULADO")
public class VSdMrpTinturariaAcumulado {
    
    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final StringProperty statusmrp = new SimpleStringProperty();
    private final StringProperty loteOf = new SimpleStringProperty();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corinsumo = new SimpleObjectProperty<>();
    private final StringProperty tipoinsumo = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty grpcomprador = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumoof = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumido = new SimpleObjectProperty<>();
    private final StringProperty consumidode = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty ordcompra = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> consumoestq = new SimpleObjectProperty<>();
    private final StringProperty unidadeestq = new SimpleStringProperty();
    private final ObjectProperty<Entidade> forinsumo = new SimpleObjectProperty<>();
    private final StringProperty origemMrp = new SimpleStringProperty();
    
    public VSdMrpTinturariaAcumulado() {
    }
    
    public VSdMrpTinturariaAcumulado(Material insumo, String deposito, BigDecimal sumConsumido, Long countConsumido) {
        this.insumo.set(insumo);
        this.deposito.set(deposito);
        this.consumido.set(sumConsumido);
        this.consumoestq.set(new BigDecimal(countConsumido));
    }
    
    public VSdMrpTinturariaAcumulado(Material insumo, Cor corinsumo, String tipoinsumo, String unidade, String unidadeestq, String deposito, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.corinsumo.set(corinsumo);
        this.tipoinsumo.set(tipoinsumo);
        this.deposito.set(deposito);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
    }
    
    public VSdMrpTinturariaAcumulado(String tipoinsumo, Material insumo, Cor corinsumo, String unidade, String unidadeestq, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.corinsumo.set(corinsumo);
        this.tipoinsumo.set(tipoinsumo);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
    }
    
    public VSdMrpTinturariaAcumulado(Material insumo, Cor corinsumo, String tipoinsumo, String unidade, String unidadeestq, String deposito, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode, String tonalidade,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.corinsumo.set(corinsumo);
        this.tipoinsumo.set(tipoinsumo);
        this.deposito.set(deposito);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.tonalidade.set(tonalidade);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
    }
    
    @Id
    @Column(name = "ID_JPA")
    public Integer getIdjpa() {
        return idjpa.get();
    }
    
    public IntegerProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(Integer idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "STATUS_MRP")
    public String getStatusmrp() {
        return statusmrp.get();
    }
    
    public StringProperty statusmrpProperty() {
        return statusmrp;
    }
    
    public void setStatusmrp(String statusmrp) {
        this.statusmrp.set(statusmrp);
    }
    
    @Column(name = "LOTE_OF")
    public String getLoteOf() {
        return loteOf.get();
    }
    
    public StringProperty loteOfProperty() {
        return loteOf;
    }
    
    public void setLoteOf(String loteOf) {
        this.loteOf.set(loteOf);
    }
    
    @OneToOne
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }
    
    @OneToOne
    @JoinColumn(name = "COR_INSUMO")
    public Cor getCorinsumo() {
        return corinsumo.get();
    }
    
    public ObjectProperty<Cor> corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(Cor corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @Column(name = "TIPO_INSUMO")
    public String getTipoinsumo() {
        return tipoinsumo.get();
    }
    
    public StringProperty tipoinsumoProperty() {
        return tipoinsumo;
    }
    
    public void setTipoinsumo(String tipoinsumo) {
        this.tipoinsumo.set(tipoinsumo);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "GRP_COMPRADOR")
    public String getGrpcomprador() {
        return grpcomprador.get();
    }
    
    public StringProperty grpcompradorProperty() {
        return grpcomprador;
    }
    
    public void setGrpcomprador(String grpcomprador) {
        this.grpcomprador.set(grpcomprador);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "CONSUMO_OF")
    public BigDecimal getConsumoof() {
        return consumoof.get();
    }
    
    public ObjectProperty<BigDecimal> consumoofProperty() {
        return consumoof;
    }
    
    public void setConsumoof(BigDecimal consumoof) {
        this.consumoof.set(consumoof);
    }
    
    @Column(name = "CONSUMIDO")
    public BigDecimal getConsumido() {
        return consumido.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoProperty() {
        return consumido;
    }
    
    public void setConsumido(BigDecimal consumido) {
        this.consumido.set(consumido);
    }
    
    @Column(name = "CONSUMIDO_DE")
    public String getConsumidode() {
        return consumidode.get();
    }
    
    public StringProperty consumidodeProperty() {
        return consumidode;
    }
    
    public void setConsumidode(String consumidode) {
        this.consumidode.set(consumidode);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "ORD_COMPRA")
    public String getOrdcompra() {
        return ordcompra.get();
    }
    
    public StringProperty ordcompraProperty() {
        return ordcompra;
    }
    
    public void setOrdcompra(String ordcompra) {
        this.ordcompra.set(ordcompra);
    }
    
    @Column(name = "DT_ENTREGA")
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "CONSUMO_ESTQ")
    public BigDecimal getConsumoestq() {
        return consumoestq.get();
    }
    
    public ObjectProperty<BigDecimal> consumoestqProperty() {
        return consumoestq;
    }
    
    public void setConsumoestq(BigDecimal consumoestq) {
        this.consumoestq.set(consumoestq);
    }
    
    @Column(name = "UNIDADE_ESTQ")
    public String getUnidadeestq() {
        return unidadeestq.get();
    }
    
    public StringProperty unidadeestqProperty() {
        return unidadeestq;
    }
    
    public void setUnidadeestq(String unidadeestq) {
        this.unidadeestq.set(unidadeestq);
    }
    
    @OneToOne
    @JoinColumn(name = "FOR_INSUMO")
    public Entidade getForinsumo() {
        return forinsumo.get();
    }
    
    public ObjectProperty<Entidade> forinsumoProperty() {
        return forinsumo;
    }
    
    public void setForinsumo(Entidade forinsumo) {
        this.forinsumo.set(forinsumo);
    }

    @Column(name = "ORI_MRP")
    public String getOrigemMrp() {
        return origemMrp.get();
    }

    public StringProperty origemMrpProperty() {
        return origemMrp;
    }

    public void setOrigemMrp(String origemMrp) {
        this.origemMrp.set(origemMrp);
    }
}