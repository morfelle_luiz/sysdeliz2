package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_NFS_MINUTA")
public class VSdNfsMinuta {
    
    private final StringProperty idview = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtEmissao = new SimpleObjectProperty<>();
    private final StringProperty transport = new SimpleStringProperty();
    private final StringProperty fatura = new SimpleStringProperty();
    private final IntegerProperty volumes = new SimpleIntegerProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    private final IntegerProperty lidos = new SimpleIntegerProperty();
    
    public VSdNfsMinuta() {
    }
    
    @Id
    @Column(name = "IDVIEW")
    public String getIdview() {
        return idview.get();
    }
    
    public StringProperty idviewProperty() {
        return idview;
    }
    
    public void setIdview(String idview) {
        this.idview.set(idview);
    }
    
    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEmissao() {
        return dtEmissao.get();
    }
    
    public ObjectProperty<LocalDate> dtEmissaoProperty() {
        return dtEmissao;
    }
    
    public void setDtEmissao(LocalDate dtEmissao) {
        this.dtEmissao.set(dtEmissao);
    }
    
    @Column(name = "TRANSPORT")
    public String getTransport() {
        return transport.get();
    }
    
    public StringProperty transportProperty() {
        return transport;
    }
    
    public void setTransport(String transport) {
        this.transport.set(transport);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "VOLUMES")
    public int getVolumes() {
        return volumes.get();
    }
    
    public IntegerProperty volumesProperty() {
        return volumes;
    }
    
    public void setVolumes(int volumes) {
        this.volumes.set(volumes);
    }
    
    @Column(name = "REMESSA")
    public String getRemessa() {
        return remessa.get();
    }
    
    public StringProperty remessaProperty() {
        return remessa;
    }
    
    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }
    
    @Column(name = "LIDOS")
    public int getLidos() {
        return lidos.get();
    }
    
    public IntegerProperty lidosProperty() {
        return lidos;
    }
    
    public void setLidos(int lidos) {
        this.lidos.set(lidos);
    }
    
    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}