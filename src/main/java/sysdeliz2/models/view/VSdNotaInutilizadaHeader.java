package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "v_sd_nota_inutilizada_header")
public class VSdNotaInutilizadaHeader extends BasicModel {

    private final StringProperty numnfi = new SimpleStringProperty();
    private final IntegerProperty codemp = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dataEmi = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dataSai = new SimpleObjectProperty<>();

    public VSdNotaInutilizadaHeader() {
    }

    @Id
    @Column(name = "NUMNFI")
    public String getNumnfi() {
        return numnfi.get();
    }

    public StringProperty numnfiProperty() {
        return numnfi;
    }

    public void setNumnfi(String numnfi) {
        this.numnfi.set(numnfi);
    }

    @Column(name = "CODEMP")
    public int getCodemp() {
        return codemp.get();
    }

    public IntegerProperty codempProperty() {
        return codemp;
    }

    public void setCodemp(int codemp) {
        this.codemp.set(codemp);
    }

    @Column(name = "DATEMI")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataEmi() {
        return dataEmi.get();
    }

    public ObjectProperty<LocalDate> dataEmiProperty() {
        return dataEmi;
    }

    public void setDataEmi(LocalDate dataEmi) {
        this.dataEmi.set(dataEmi);
    }

    @Column(name = "DATSAI")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDataSai() {
        return dataSai.get();
    }

    public ObjectProperty<LocalDate> dataSaiProperty() {
        return dataSai;
    }

    public void setDataSai(LocalDate dataSai) {
        this.dataSai.set(dataSai);
    }
}
