package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Deposito;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_OFS_COLETA_LIBERADA")
public class VSdOfsColetaLiberada {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<VSdDadosProduto>();
    private final IntegerProperty sequencia = new SimpleIntegerProperty();
    private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<CadFluxo>();
    private final ObjectProperty<Deposito> deposito = new SimpleObjectProperty<Deposito>();
    private final IntegerProperty totbaixado = new SimpleIntegerProperty();
    private final IntegerProperty totparcial = new SimpleIntegerProperty();
    private final IntegerProperty totcoletado = new SimpleIntegerProperty();
    private final IntegerProperty totaberto = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> perccoleta = new SimpleObjectProperty<BigDecimal>();
    
    public VSdOfsColetaLiberada() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @OneToOne
    @JoinColumn(name = "CODIGO")
    public VSdDadosProduto getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<VSdDadosProduto> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(VSdDadosProduto codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "SEQUENCIA")
    public Integer getSequencia() {
        return sequencia.get();
    }
    
    public IntegerProperty sequenciaProperty() {
        return sequencia;
    }
    
    public void setSequencia(Integer sequencia) {
        this.sequencia.set(sequencia);
    }
    
    @OneToOne
    @JoinColumn(name = "SETOR")
    public CadFluxo getSetor() {
        return setor.get();
    }
    
    public ObjectProperty<CadFluxo> setorProperty() {
        return setor;
    }
    
    public void setSetor(CadFluxo setor) {
        this.setor.set(setor);
    }
    
    @OneToOne
    @JoinColumn(name = "DEPOSITO")
    public Deposito getDeposito() {
        return deposito.get();
    }
    
    public ObjectProperty<Deposito> depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(Deposito deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "TOT_BAIXADO")
    public Integer getTotbaixado() {
        return totbaixado.get();
    }
    
    public IntegerProperty totbaixadoProperty() {
        return totbaixado;
    }
    
    public void setTotbaixado(Integer totbaixado) {
        this.totbaixado.set(totbaixado);
    }
    
    @Column(name = "TOT_PARCIAL")
    public Integer getTotparcial() {
        return totparcial.get();
    }
    
    public IntegerProperty totparcialProperty() {
        return totparcial;
    }
    
    public void setTotparcial(Integer totparcial) {
        this.totparcial.set(totparcial);
    }
    
    @Column(name = "TOT_COLETADO")
    public Integer getTotcoletado() {
        return totcoletado.get();
    }
    
    public IntegerProperty totcoletadoProperty() {
        return totcoletado;
    }
    
    public void setTotcoletado(Integer totcoletado) {
        this.totcoletado.set(totcoletado);
    }
    
    @Column(name = "TOT_ABERTO")
    public Integer getTotaberto() {
        return totaberto.get();
    }
    
    public IntegerProperty totabertoProperty() {
        return totaberto;
    }
    
    public void setTotaberto(Integer totaberto) {
        this.totaberto.set(totaberto);
    }
    
    @Column(name = "PERC_COLETA")
    public BigDecimal getPerccoleta() {
        return perccoleta.get();
    }
    
    public ObjectProperty<BigDecimal> perccoletaProperty() {
        return perccoleta;
    }
    
    public void setPerccoleta(BigDecimal perccoleta) {
        this.perccoleta.set(perccoleta);
    }
    
}
