package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_OFS_MRP_TINTURARIA")
public class VSdOfsMrpTinturaria {
    
    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final StringProperty insumo = new SimpleStringProperty();
    private final BooleanProperty principal = new SimpleBooleanProperty();
    private final StringProperty corinsumo = new SimpleStringProperty();
    private final ObjectProperty<Of1> numeroof = new SimpleObjectProperty<>();
    private final StringProperty loteOf = new SimpleStringProperty();
    private final StringProperty statusof = new SimpleStringProperty();
    private final StringProperty setorof = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corproduto = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty consumidoDe = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> estoque = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> compra = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> acomprar = new SimpleObjectProperty<>();
    private final StringProperty origemMrp = new SimpleStringProperty();
    
    public VSdOfsMrpTinturaria() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public Integer getIdjpa() {
        return idjpa.get();
    }
    
    public IntegerProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(Integer idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "INSUMO")
    public String getInsumo() {
        return insumo.get();
    }
    
    public StringProperty insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(String insumo) {
        this.insumo.set(insumo);
    }

    @Column(name = "PRINCIPAL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPrincipal() {
        return principal.get();
    }

    public BooleanProperty principalProperty() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal.set(principal);
    }

    @Column(name = "COR_INSUMO")
    public String getCorinsumo() {
        return corinsumo.get();
    }
    
    public StringProperty corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(String corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @OneToOne
    @JoinColumn(name = "NUMERO_OF")
    public Of1 getNumeroof() {
        return numeroof.get();
    }
    
    public ObjectProperty<Of1> numeroofProperty() {
        return numeroof;
    }
    
    public void setNumeroof(Of1 numeroof) {
        this.numeroof.set(numeroof);
    }
    
    @Column(name = "LOTE_OF")
    public String getLoteOf() {
        return loteOf.get();
    }
    
    public StringProperty loteOfProperty() {
        return loteOf;
    }
    
    public void setLoteOf(String loteOf) {
        this.loteOf.set(loteOf);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusof() {
        return statusof.get();
    }
    
    public StringProperty statusofProperty() {
        return statusof;
    }
    
    public void setStatusof(String statusof) {
        this.statusof.set(statusof);
    }
    
    @Column(name = "SETOR_OF")
    public String getSetorof() {
        return setorof.get();
    }
    
    public StringProperty setorofProperty() {
        return setorof;
    }
    
    public void setSetorof(String setorof) {
        this.setorof.set(setorof);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @OneToOne
    @JoinColumn(name = "PRODUTO")
    public Produto getProduto() {
        return produto.get();
    }
    
    public ObjectProperty<Produto> produtoProperty() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        this.produto.set(produto);
    }
    
    @OneToOne
    @JoinColumn(name = "COR_PRODUTO")
    public Cor getCorproduto() {
        return corproduto.get();
    }
    
    public ObjectProperty<Cor> corprodutoProperty() {
        return corproduto;
    }
    
    public void setCorproduto(Cor corproduto) {
        this.corproduto.set(corproduto);
    }
    
    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        return qtde.get();
    }
    
    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "CONSUMIDO_DE")
    public String getConsumidoDe() {
        return consumidoDe.get();
    }

    public StringProperty consumidoDeProperty() {
        return consumidoDe;
    }

    public void setConsumidoDe(String consumidoDe) {
        this.consumidoDe.set(consumidoDe);
    }

    @Column(name = "ESTOQUE")
    public BigDecimal getEstoque() {
        return estoque.get();
    }
    
    public ObjectProperty<BigDecimal> estoqueProperty() {
        return estoque;
    }
    
    public void setEstoque(BigDecimal estoque) {
        this.estoque.set(estoque);
    }
    
    @Column(name = "COMPRA")
    public BigDecimal getCompra() {
        return compra.get();
    }
    
    public ObjectProperty<BigDecimal> compraProperty() {
        return compra;
    }
    
    public void setCompra(BigDecimal compra) {
        this.compra.set(compra);
    }
    
    @Column(name = "A_COMPRAR")
    public BigDecimal getAcomprar() {
        return acomprar.get();
    }
    
    public ObjectProperty<BigDecimal> acomprarProperty() {
        return acomprar;
    }
    
    public void setAcomprar(BigDecimal acomprar) {
        this.acomprar.set(acomprar);
    }

    @Column(name = "ORI_MRP")
    public String getOrigemMrp() {
        return origemMrp.get();
    }

    public StringProperty origemMrpProperty() {
        return origemMrp;
    }

    public void setOrigemMrp(String origemMrp) {
        this.origemMrp.set(origemMrp);
    }
}