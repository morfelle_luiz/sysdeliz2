package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "V_SD_OFS_PARA_COLETA")
public class VSdOfsParaColeta {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final IntegerProperty itens = new SimpleIntegerProperty();
    private final StringProperty pais = new SimpleStringProperty();
    
    public VSdOfsParaColeta() {
    }
    
    public VSdOfsParaColeta(String numero, String setor, String deposito, String tipo, String periodo, String pais) {
        this.numero.set(numero);
        this.setor.set(setor);
        this.deposito.set(deposito);
        this.itens.set(999);
        this.tipo.set(tipo);
        this.periodo.set(periodo);
        this.pais.set(pais);
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }
    
    public StringProperty numeroProperty() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero.set(numero);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "ITENS")
    public Integer getItens() {
        return itens.get();
    }
    
    public IntegerProperty itensProperty() {
        return itens;
    }
    
    public void setItens(Integer itens) {
        this.itens.set(itens);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }

    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }

    public StringProperty paisProperty() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais.set(pais);
    }
}
