/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.view;

import javafx.beans.property.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author cristiano.diego
 */
public class VSdOfsParaProgramar {

    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty statusOf = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtEnv = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRet = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dtProg = new SimpleObjectProperty<>();
    private final StringProperty leadSetor = new SimpleStringProperty();
    private final StringProperty op = new SimpleStringProperty();
    private final StringProperty periodo = new SimpleStringProperty();
    private final StringProperty setor = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty codfam = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final StringProperty faccao = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final BooleanProperty programado = new SimpleBooleanProperty();
    private final StringProperty statusMat = new SimpleStringProperty();
    private final StringProperty codMat = new SimpleStringProperty();
    private final StringProperty material = new SimpleStringProperty();
    private final IntegerProperty totOper = new SimpleIntegerProperty();
    private final DoubleProperty minProdPc = new SimpleDoubleProperty();
    private final DoubleProperty minProd = new SimpleDoubleProperty();
    private final DoubleProperty diasProd = new SimpleDoubleProperty();

    public VSdOfsParaProgramar() {
    }

    public VSdOfsParaProgramar(String codcli, String statusOf, String dtEnv, String dtRet, String dtProg,
            String leadSetor, String op, String periodo, String setor, String referencia, String produto,
            String linha, String codfam, String familia, String faccao, Integer qtde, String programado, String statusMat, String codMat,
            String material, Integer totOper, Double minProdPc, Double minProd, Double diasProd) {
        this.codcli.set(codcli);
        this.statusOf.set(statusOf);
        this.dtEnv.set(dtEnv == null ? null : LocalDate.parse(dtEnv, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.dtRet.set(dtRet == null ? null : LocalDate.parse(dtRet, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        this.dtProg.set(dtProg == null ? null : LocalDateTime.parse(dtProg, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        this.leadSetor.set(leadSetor);
        this.op.set(op);
        this.periodo.set(periodo);
        this.setor.set(setor);
        this.referencia.set(referencia);
        this.produto.set(produto);
        this.linha.set(linha);
        this.codfam.set(codfam);
        this.familia.set(familia);
        this.faccao.set(faccao);
        this.qtde.set(qtde);
        this.programado.set(programado.equals("S"));
        this.statusMat.set(statusMat);
        this.codMat.set(codMat);
        this.material.set(material);
        this.totOper.set(totOper);
        this.minProdPc.set(minProdPc);
        this.minProd.set(minProd);
        this.diasProd.set(diasProd);
    }

    public final String getCodfam() {
        return codfam.get();
    }

    public final void setCodfam(String value) {
        codfam.set(value);
    }

    public StringProperty codfamProperty() {
        return codfam;
    }

    public final String getCodcli() {
        return codcli.get();
    }

    public final void setCodcli(String value) {
        codcli.set(value);
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public final String getStatusOf() {
        return statusOf.get();
    }

    public final void setStatusOf(String value) {
        statusOf.set(value);
    }

    public StringProperty statusOfProperty() {
        return statusOf;
    }
    
    public LocalDate getDtEnv() {
        return dtEnv.get();
    }
    
    public ObjectProperty<LocalDate> dtEnvProperty() {
        return dtEnv;
    }
    
    public void setDtEnv(LocalDate dtEnv) {
        this.dtEnv.set(dtEnv);
    }
    
    public LocalDate getDtRet() {
        return dtRet.get();
    }
    
    public ObjectProperty<LocalDate> dtRetProperty() {
        return dtRet;
    }
    
    public void setDtRet(LocalDate dtRet) {
        this.dtRet.set(dtRet);
    }
    
    public LocalDateTime getDtProg() {
        return dtProg.get();
    }
    
    public ObjectProperty<LocalDateTime> dtProgProperty() {
        return dtProg;
    }
    
    public void setDtProg(LocalDateTime dtProg) {
        this.dtProg.set(dtProg);
    }
    
    public final String getLeadSetor() {
        return leadSetor.get();
    }

    public final void setLeadSetor(String value) {
        leadSetor.set(value);
    }

    public StringProperty leadSetorProperty() {
        return leadSetor;
    }

    public final String getOp() {
        return op.get();
    }

    public final void setOp(String value) {
        op.set(value);
    }

    public StringProperty opProperty() {
        return op;
    }

    public final String getPeriodo() {
        return periodo.get();
    }

    public final void setPeriodo(String value) {
        periodo.set(value);
    }

    public StringProperty periodoProperty() {
        return periodo;
    }

    public final String getSetor() {
        return setor.get();
    }

    public final void setSetor(String value) {
        setor.set(value);
    }

    public StringProperty setorProperty() {
        return setor;
    }

    public final String getReferencia() {
        return referencia.get();
    }

    public final void setReferencia(String value) {
        referencia.set(value);
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public final String getProduto() {
        return produto.get();
    }

    public final void setProduto(String value) {
        produto.set(value);
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public final String getLinha() {
        return linha.get();
    }

    public final void setLinha(String value) {
        linha.set(value);
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public final String getFamilia() {
        return familia.get();
    }

    public final void setFamilia(String value) {
        familia.set(value);
    }

    public StringProperty familiaProperty() {
        return familia;
    }
    
    public String getFaccao() {
        return faccao.get();
    }
    
    public StringProperty faccaoProperty() {
        return faccao;
    }
    
    public void setFaccao(String faccao) {
        this.faccao.set(faccao);
    }
    
    public final int getQtde() {
        return qtde.get();
    }

    public final void setQtde(int value) {
        qtde.set(value);
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public final String getStatusMat() {
        return statusMat.get();
    }

    public final boolean isProgramado() {
        return programado.get();
    }

    public final void setProgramado(boolean value) {
        programado.set(value);
    }

    public BooleanProperty programadoProperty() {
        return programado;
    }

    public final void setStatusMat(String value) {
        statusMat.set(value);
    }

    public StringProperty statusMatProperty() {
        return statusMat;
    }

    public final String getCodMat() {
        return codMat.get();
    }

    public final void setCodMat(String value) {
        codMat.set(value);
    }

    public StringProperty codMatProperty() {
        return codMat;
    }

    public final String getMaterial() {
        return material.get();
    }

    public final void setMaterial(String value) {
        material.set(value);
    }

    public StringProperty materialProperty() {
        return material;
    }

    public final int getTotOper() {
        return totOper.get();
    }

    public final void setTotOper(int value) {
        totOper.set(value);
    }

    public IntegerProperty totOperProperty() {
        return totOper;
    }

    public final double getMinProdPc() {
        return minProdPc.get();
    }

    public final void setMinProdPc(double value) {
        minProdPc.set(value);
    }

    public DoubleProperty minProdPcProperty() {
        return minProdPc;
    }

    public final double getMinProd() {
        return minProd.get();
    }

    public final void setMinProd(double value) {
        minProd.set(value);
    }

    public DoubleProperty minProdProperty() {
        return minProd;
    }

    public final double getDiasProd() {
        return diasProd.get();
    }

    public final void setDiasProd(double value) {
        diasProd.set(value);
    }

    public DoubleProperty diasProdProperty() {
        return diasProd;
    }

}
