package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.sysdeliz.SdPacote001;

public class VSdOperParaLancamento {

    private final StringProperty ordemProd = new SimpleStringProperty(this, "ordemProd");
    private final ObjectProperty<SdProgramacaoPacote001> programacao = new SimpleObjectProperty<>(this, "programacao");
    private final ObjectProperty<SdPacote001> pacote = new SimpleObjectProperty<>(this, "pacote");
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>(this, "operacao");
    private final IntegerProperty qtdePacote = new SimpleIntegerProperty(this, "qtdePacote");
    //private final StringProperty dhInicioToString = new SimpleStringProperty();

    public VSdOperParaLancamento() {
    }

    public VSdOperParaLancamento(String ordemProd, SdPacote001 pacote, SdProgramacaoPacote001 programacao) {
        this.ordemProd.set(ordemProd);
        this.programacao.set(programacao);
        this.operacao.set(programacao.getOperacaoObject());
        this.pacote.set(pacote);
    }

    public String getOrdemProd() {
        return ordemProd.get();
    }

    public StringProperty ordemProdProperty() {
        return ordemProd;
    }

    public void setOrdemProd(String ordemProd) {
        this.ordemProd.set(ordemProd);
    }

    public SdProgramacaoPacote001 getProgramacao() {
        return programacao.get();
    }

    public ObjectProperty<SdProgramacaoPacote001> programacaoProperty() {
        return programacao;
    }

    public void setProgramacao(SdProgramacaoPacote001 programacao) {
        this.programacao.set(programacao);
    }

    public SdPacote001 getPacote() {
        return pacote.get();
    }

    public ObjectProperty<SdPacote001> pacoteProperty() {
        return pacote;
    }

    public void setPacote(SdPacote001 pacote) {
        this.pacote.set(pacote);
    }

    public SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }

    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }

    public void setOperacao(SdOperacaoOrganize001 operacao) {
        this.operacao.set(operacao);
    }
}
