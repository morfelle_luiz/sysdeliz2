package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name="V_SD_OPER_PEND_COLAB_V2")
public class VSdOperPendColab {
    
    private final StringProperty idJpa = new SimpleStringProperty();
    private final StringProperty pacote = new SimpleStringProperty();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty operacao = new SimpleIntegerProperty();
    private final StringProperty agrupado = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime>  dhInicio = new SimpleObjectProperty();
    private final ObjectProperty<LocalDateTime>  dhFim = new SimpleObjectProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tTotal = new SimpleObjectProperty<>();
    private final DoubleProperty percTempo = new SimpleDoubleProperty();
    
    
    @Id
    @Column(name="ID_JPA")
    public String getIdJpa() {
        return idJpa.get();
    }
    
    public StringProperty idJpaProperty() {
        return idJpa;
    }
    
    public void setIdJpa(String idJpa) {
        this.idJpa.set(idJpa);
    }
    
    @Column(name="PACOTE")
    public String getPacote() {
        return pacote.get();
    }
    
    public StringProperty pacoteProperty() {
        return pacote;
    }
    
    public void setPacote(String pacote) {
        this.pacote.set(pacote);
    }
    
    @Column(name="COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name="OPERACAO")
    public int getOperacao() {
        return operacao.get();
    }
    
    public IntegerProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(int operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name="DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    @Column(name="TEMPO")
    public BigDecimal getTempo() {
        return tempo.get();
    }
    
    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }
    
    public void setTempo(BigDecimal tempo) {
        this.tempo.set(tempo);
    }
    
    @Column(name="DH_INICIO", columnDefinition = "DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name="DH_FIM", columnDefinition = "DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.dhFim.set(dhFim);
    }
    
    @Column(name="QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name="T_TOTAL")
    public BigDecimal gettTotal() {
        return tTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tTotalProperty() {
        return tTotal;
    }
    
    public void settTotal(BigDecimal tTotal) {
        this.tTotal.set(tTotal);
    }
    
    @Column(name="AGRUPADO", columnDefinition = "CHAR")
    public String getAgrupado() {
        return agrupado.get();
    }
    
    public StringProperty agrupadoProperty() {
        return agrupado;
    }
    
    public void setAgrupado(String agrupador) {
        this.agrupado.set(agrupador);
    }
    
    @Transient
    public double getPercTempo() {
        return percTempo.get();
    }
    
    public DoubleProperty percTempoProperty() {
        return percTempo;
    }
    
    public void setPercTempo(double percTempo) {
        this.percTempo.set(percTempo);
    }
}
