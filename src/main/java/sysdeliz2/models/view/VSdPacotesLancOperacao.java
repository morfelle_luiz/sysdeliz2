package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name="V_SD_PACOTES_LANC_PROG")
public class VSdPacotesLancOperacao {
    
    private final BooleanProperty selected = new SimpleBooleanProperty(false);
    
    private final StringProperty idJpa = new SimpleStringProperty();
    private final StringProperty programacao = new SimpleStringProperty();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final IntegerProperty operacao = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty pacote = new SimpleIntegerProperty();
    private final IntegerProperty quebra = new SimpleIntegerProperty();
    private final IntegerProperty seqColaborador = new SimpleIntegerProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tTotal = new SimpleObjectProperty<>();
    private final DoubleProperty percTempo = new SimpleDoubleProperty();
    
    @Transient
    public boolean isSelected() {
        return selected.get();
    }
    
    public BooleanProperty selectedProperty() {
        return selected;
    }
    
    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
    
    @Id
    @Column(name="ID_JPA")
    public String getIdJpa() {
        return idJpa.get();
    }
    
    public StringProperty idJpaProperty() {
        return idJpa;
    }
    
    public void setIdJpa(String idJpa) {
        this.idJpa.set(idJpa);
    }
    
    @Column(name="PROGRAMACAO")
    public String getProgramacao() {
        return programacao.get();
    }
    
    public StringProperty programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(String programacao) {
        this.programacao.set(programacao);
    }
    
    @Column(name="COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name="OPERACAO")
    public int getOperacao() {
        return operacao.get();
    }
    
    public IntegerProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(int operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "QUEBRA")
    public int getQuebra() {
        return quebra.get();
    }
    
    public IntegerProperty quebraProperty() {
        return quebra;
    }
    
    public void setQuebra(int quebra) {
        this.quebra.set(quebra);
    }
    
    @Column(name = "SEQ_COLABORADOR")
    public int getSeqColaborador() {
        return seqColaborador.get();
    }
    
    public IntegerProperty seqColaboradorProperty() {
        return seqColaborador;
    }
    
    public void setSeqColaborador(int seqColaborador) {
        this.seqColaborador.set(seqColaborador);
    }
    
    @Column(name="PACOTE")
    public int getPacote() {
        return pacote.get();
    }
    
    public IntegerProperty pacoteProperty() {
        return pacote;
    }
    
    public void setPacote(int pacote) {
        this.pacote.set(pacote);
    }
    
    @Column(name="COR")
    public String getCor() {
        return cor.get();
    }
    
    public StringProperty corProperty() {
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor.set(cor);
    }
    
    @Column(name="DESC_COR")
    public String getDescCor() {
        return descCor.get();
    }
    
    public StringProperty descCorProperty() {
        return descCor;
    }
    
    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }
    
    @Column(name="TAM")
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Column(name="QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name="T_TOTAL")
    public BigDecimal gettTotal() {
        return tTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tTotalProperty() {
        return tTotal;
    }
    
    public void settTotal(BigDecimal tTotal) {
        this.tTotal.set(tTotal);
    }
    
    @Transient
    public double getPercTempo() {
        return percTempo.get();
    }
    
    public DoubleProperty percTempoProperty() {
        return percTempo;
    }
    
    public void setPercTempo(double percTempo) {
        this.percTempo.set(percTempo);
    }
}
