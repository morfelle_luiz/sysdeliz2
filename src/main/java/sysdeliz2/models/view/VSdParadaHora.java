package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdTiposParada001;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "V_SD_PARADA_HORA")
public class VSdParadaHora implements Serializable {
    
    private final StringProperty id = new SimpleStringProperty();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<>();
    private final ObjectProperty<SdTiposParada001> motivo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempo = new SimpleObjectProperty<>();
    private final IntegerProperty celula = new SimpleIntegerProperty();
    
    public VSdParadaHora() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name = "DH_INICIO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name = "DH_FIM")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.dhFim.set(dhFim);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MOTIVO")
    public SdTiposParada001 getMotivo() {
        return motivo.get();
    }
    
    public ObjectProperty<SdTiposParada001> motivoProperty() {
        return motivo;
    }
    
    public void setMotivo(SdTiposParada001 motivo) {
        this.motivo.set(motivo);
    }
    
    @Column(name = "TEMPO")
    public BigDecimal getTempo() {
        return tempo.get();
    }
    
    public ObjectProperty<BigDecimal> tempoProperty() {
        return tempo;
    }
    
    public void setTempo(BigDecimal tempo) {
        this.tempo.set(tempo);
    }
    
    @Column(name = "CELULA")
    public int getCelula() {
        return celula.get();
    }
    
    public IntegerProperty celulaProperty() {
        return celula;
    }
    
    public void setCelula(int celula) {
        this.celula.set(celula);
    }
}
