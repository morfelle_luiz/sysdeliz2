package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.Hibernate;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "V_SD_PED_CIDADES_REP_temp")
public class VSdPedCidadesRep implements Serializable {

    private final ObjectProperty<Represen> codRep = new SimpleObjectProperty<>(null);
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>(null);
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>(null);
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>(null);

    private final BooleanProperty colecaoAtual = new SimpleBooleanProperty(false);
    private final BooleanProperty colecaoPassada = new SimpleBooleanProperty(false);
    private final BooleanProperty colecaoAntiga = new SimpleBooleanProperty(false);
    private final BooleanProperty colecaoReferencia = new SimpleBooleanProperty(false);
    private final BooleanProperty ativa = new SimpleBooleanProperty(false);

    private final IntegerProperty totalCidades = new SimpleIntegerProperty(0);

    private final IntegerProperty pv = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> tf = new SimpleObjectProperty<>(BigDecimal.ZERO);

    private final BooleanProperty menor5 = new SimpleBooleanProperty(false);
    private final BooleanProperty entre5e10 = new SimpleBooleanProperty(false);
    private final BooleanProperty entre10e40 = new SimpleBooleanProperty(false);
    private final BooleanProperty entre40e80 = new SimpleBooleanProperty(false);
    private final BooleanProperty maior80 = new SimpleBooleanProperty(false);

    public VSdPedCidadesRep() {
    }

    public VSdPedCidadesRep(Cidade cidade, boolean isColAtual, boolean isColPas, boolean isColAnt, boolean isColRef, int pv, BigDecimal tf) {
        this.codCid.set(cidade);
        this.colecaoAtual.set(isColAtual);
        this.colecaoPassada.set(isColPas);
        this.colecaoAntiga.set(isColAnt);
        this.colecaoReferencia.set(isColRef);
        this.ativa.set(isColPas || isColAnt || isColRef);
        this.pv.set(pv);
        this.tf.set(tf);

        this.menor5.set(cidade.getPopCid() <= 5000);
        this.entre5e10.set(cidade.getPopCid() > 5000 && cidade.getPopCid() <= 10000);
        this.entre10e40.set(cidade.getPopCid() > 10000 && cidade.getPopCid() <= 40000);
        this.entre40e80.set(cidade.getPopCid() > 40000 && cidade.getPopCid() <= 80000);
        this.maior80.set(cidade.getPopCid() > 80000);
    }

    public VSdPedCidadesRep(Cidade cidade, int pv, BigDecimal tf) {
        this.codCid.set(cidade);
        this.pv.set(pv);
        this.tf.set(tf);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODREP")
    public Represen getCodRep() {
        return codRep.get();
    }

    public ObjectProperty<Represen> codRepProperty() {
        return codRep;
    }

    public void setCodRep(Represen codRep) {
        this.codRep.set(codRep);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COD_CID")
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "PV")
    public int getPv() {
        return pv.get();
    }

    public IntegerProperty pvProperty() {
        return pv;
    }

    public void setPv(int pv) {
        this.pv.set(pv);
    }

    @Column(name = "TF")
    public BigDecimal getTf() {
        return tf.get();
    }

    public ObjectProperty<BigDecimal> tfProperty() {
        return tf;
    }

    public void setTf(BigDecimal tf) {
        this.tf.set(tf);
    }

    @Transient
    public boolean isColecaoAtual() {
        return colecaoAtual.get();
    }

    public BooleanProperty colecaoAtualProperty() {
        return colecaoAtual;
    }

    public void setColecaoAtual(boolean colecaoAtual) {
        this.colecaoAtual.set(colecaoAtual);
    }

    @Transient
    public boolean isColecaoPassada() {
        return colecaoPassada.get();
    }

    public BooleanProperty colecaoPassadaProperty() {
        return colecaoPassada;
    }

    public void setColecaoPassada(boolean colecaoPassada) {
        this.colecaoPassada.set(colecaoPassada);
    }

    @Transient
    public boolean isColecaoAntiga() {
        return colecaoAntiga.get();
    }

    public BooleanProperty colecaoAntigaProperty() {
        return colecaoAntiga;
    }

    public void setColecaoAntiga(boolean colecaoAntiga) {
        this.colecaoAntiga.set(colecaoAntiga);
    }

    @Transient
    public boolean isColecaoReferencia() {
        return colecaoReferencia.get();
    }

    public BooleanProperty colecaoReferenciaProperty() {
        return colecaoReferencia;
    }

    public void setColecaoReferencia(boolean colecaoReferencia) {
        this.colecaoReferencia.set(colecaoReferencia);
    }

    @Transient
    public boolean isAtiva() {
        return ativa.get();
    }

    public BooleanProperty ativaProperty() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa.set(ativa);
    }

    @Transient
    public int getTotalCidades() {
        return totalCidades.get();
    }

    public IntegerProperty totalCidadesProperty() {
        return totalCidades;
    }

    public void setTotalCidades(int totalCidades) {
        this.totalCidades.set(totalCidades);
    }

    @Transient
    public boolean isMenor5() {
        return menor5.get();
    }

    public BooleanProperty menor5Property() {
        return menor5;
    }

    public void setMenor5(boolean menor5) {
        this.menor5.set(menor5);
    }

    @Transient
    public boolean isEntre5e10() {
        return entre5e10.get();
    }

    public BooleanProperty entre5e10Property() {
        return entre5e10;
    }

    public void setEntre5e10(boolean entre5e10) {
        this.entre5e10.set(entre5e10);
    }

    @Transient
    public boolean isEntre10e40() {
        return entre10e40.get();
    }

    public BooleanProperty entre10e40Property() {
        return entre10e40;
    }

    public void setEntre10e40(boolean entre10e40) {
        this.entre10e40.set(entre10e40);
    }

    @Transient
    public boolean isEntre40e80() {
        return entre40e80.get();
    }

    public BooleanProperty entre40e80Property() {
        return entre40e80;
    }

    public void setEntre40e80(boolean entre40e80) {
        this.entre40e80.set(entre40e80);
    }

    @Transient
    public boolean isMaior80() {
        return maior80.get();
    }

    public BooleanProperty maior80Property() {
        return maior80;
    }

    public void setMaior80(boolean maior80) {
        this.maior80.set(maior80);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        VSdPedCidadesRep that = (VSdPedCidadesRep) o;
        return Objects.equals(getCodRep(), that.getCodRep());
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
