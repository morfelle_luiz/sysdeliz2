package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "V_SD_PED_DATA_CIDADES_REP")
public class VSdPedDataCidadesRep implements Serializable {
    private final StringProperty codRep = new SimpleStringProperty();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final ObjectProperty<Cidade> codCid = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtEmissao = new SimpleObjectProperty<>();
    private final StringProperty marca = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final IntegerProperty pv = new SimpleIntegerProperty();
    private final BooleanProperty ativa = new SimpleBooleanProperty();
    private final ObjectProperty<BigDecimal> valorFixo = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final IntegerProperty pvFixo = new SimpleIntegerProperty(0);


    public VSdPedDataCidadesRep() {
    }

    public VSdPedDataCidadesRep(Cidade cidade) {
        this.codCid.set(cidade);
    }


    @Id
    @Column(name = "CODREP")
    public String getCodRep() {
        return codRep.get();
    }

    public StringProperty codRepProperty() {
        return codRep;
    }

    public void setCodRep(String codRep) {
        this.codRep.set(codRep);
    }

    @Id
    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @OneToOne
    @JoinColumn(name = "COLECAO", referencedColumnName = "CODIGO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Id
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "COD_CID")
    public Cidade getCodCid() {
        return codCid.get();
    }

    public ObjectProperty<Cidade> codCidProperty() {
        return codCid;
    }

    public void setCodCid(Cidade codCid) {
        this.codCid.set(codCid);
    }

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEmissao() {
        return dtEmissao.get();
    }

    public ObjectProperty<LocalDate> dtEmissaoProperty() {
        return dtEmissao;
    }

    public void setDtEmissao(LocalDate dtEmissao) {
        this.dtEmissao.set(dtEmissao);
    }

    @Id
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Transient
    public int getPv() {
        return pv.get();
    }

    public IntegerProperty pvProperty() {
        return pv;
    }

    public void setPv(int pv) {
        this.pv.set(pv);
    }

    @Column(name = "ATIVA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtiva() {
        return ativa.get();
    }

    public BooleanProperty ativaProperty() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa.set(ativa);
    }

    @Transient
    public BigDecimal getValorFixo() {
        return valorFixo.get();
    }

    public ObjectProperty<BigDecimal> valorFixoProperty() {
        return valorFixo;
    }

    public void setValorFixo(BigDecimal valorFixo) {
        this.valorFixo.set(valorFixo);
    }

    @Transient
    public int getPvFixo() {
        return pvFixo.get();
    }

    public IntegerProperty pvFixoProperty() {
        return pvFixo;
    }

    public void setPvFixo(int pvFixo) {
        this.pvFixo.set(pvFixo);
    }
}
