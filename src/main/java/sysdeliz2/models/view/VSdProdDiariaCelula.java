package sysdeliz2.models.view;

import javafx.beans.property.*;

import java.time.LocalDate;


public class VSdProdDiariaCelula {
    
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty dia = new SimpleStringProperty();
    private final StringProperty mes = new SimpleStringProperty();
    private final StringProperty ano = new SimpleStringProperty();
    private final StringProperty diaUtil = new SimpleStringProperty();
    
    private final StringProperty codCelula = new SimpleStringProperty();
    private final StringProperty celula = new SimpleStringProperty();
    private final StringProperty codLider = new SimpleStringProperty();
    private final StringProperty lider = new SimpleStringProperty();
    
    private final IntegerProperty qtdeOpers = new SimpleIntegerProperty();
    private final IntegerProperty medPecas = new SimpleIntegerProperty();
    
    private final DoubleProperty tmpProduzido = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramado = new SimpleDoubleProperty();
    private final DoubleProperty operacao = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEfic = new SimpleDoubleProperty();
    private final DoubleProperty eficiencia = new SimpleDoubleProperty();
    private final DoubleProperty tmpProdutivo = new SimpleDoubleProperty();
    private final DoubleProperty tmpProducao = new SimpleDoubleProperty();
    private final DoubleProperty produtividade = new SimpleDoubleProperty();
    private final DoubleProperty tmpParado = new SimpleDoubleProperty();
    
    private final DoubleProperty tmpProduzidoDia = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramadoDia = new SimpleDoubleProperty();
    private final DoubleProperty operacaoDia = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEficDia = new SimpleDoubleProperty();
    private final DoubleProperty eficienciaDia = new SimpleDoubleProperty();
    private final DoubleProperty tmpProdutivoDia = new SimpleDoubleProperty();
    private final DoubleProperty tmpProducaoDia = new SimpleDoubleProperty();
    private final DoubleProperty produtividadeDia = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadoDia = new SimpleDoubleProperty();
    
    private final DoubleProperty tmpProduzidoMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramadoMes = new SimpleDoubleProperty();
    private final DoubleProperty operacaoMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEficMes = new SimpleDoubleProperty();
    private final DoubleProperty eficienciaMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpProdutivoMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpProducaoMes = new SimpleDoubleProperty();
    private final DoubleProperty produtividadeMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadoMes = new SimpleDoubleProperty();
    
    private final DoubleProperty tmpProduzidoAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramadoAno = new SimpleDoubleProperty();
    private final DoubleProperty operacaoAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEficAno = new SimpleDoubleProperty();
    private final DoubleProperty eficienciaAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpProdutivoAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpProducaoAno = new SimpleDoubleProperty();
    private final DoubleProperty produtividadeAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadoAno = new SimpleDoubleProperty();
    
    public VSdProdDiariaCelula() {    }
    
    public VSdProdDiariaCelula(String dia, String mes, String ano, LocalDate data, String diaUtil,
                               String codCelula, String celula, String codLider, String lider,
                               Integer qtdeOpers, Integer medPecas,
                               Double tmpProduzido, Double tmpProgramado, Double operacao, Double tmpDeduzEfic, Double eficiencia, Double tmpProdutivo, Double tmpProducao, Double produtividade, Double tmpParado,
                               Double tmpProduzidoDia, Double tmpProgramadoDia, Double operacaoDia, Double tmpDeduzEficDia, Double eficienciaDia, Double tmpProdutivoDia, Double tmpProducaoDia, Double produtividadeDia, Double tmpParadoDia,
                               Double tmpProduzidoMes, Double tmpProgramadoMes, Double operacaoMes, Double tmpDeduzEficMes, Double eficienciaMes, Double tmpProdutivoMes, Double tmpProducaoMes, Double produtividadeMes, Double tmpParadoMes,
                               Double tmpProduzidoAno, Double tmpProgramadoAno, Double operacaoAno, Double tmpDeduzEficAno, Double eficienciaAno, Double tmpProdutivoAno, Double tmpProducaoAno, Double produtividadeAno, Double tmpParadoAno) {
        this.data.set(data);
        this.dia.set(dia);
        this.mes.set(mes);
        this.ano.set(ano);
        this.diaUtil.set(diaUtil);
        this.codCelula.set(codCelula);
        this.celula.set(celula);
        this.codLider.set(codLider);
        this.lider.set(lider);
        this.qtdeOpers.set(qtdeOpers);
        this.medPecas.set(medPecas);
        this.tmpProduzido.set(tmpProduzido);
        this.tmpProgramado.set(tmpProgramado);
        this.operacao.set(operacao);
        this.tmpDeduzEfic.set(tmpDeduzEfic);
        this.eficiencia.set(eficiencia);
        this.produtividade.set(produtividade);
        this.tmpProdutivo.set(tmpProdutivo);
        this.tmpProducao.set(tmpProducao);
        this.tmpParado.set(tmpParado);
        this.tmpProduzidoDia.set(tmpProduzidoDia);
        this.tmpProgramadoDia.set(tmpProgramadoDia);
        this.operacaoDia.set(operacaoDia);
        this.tmpDeduzEficDia.set(tmpDeduzEficDia);
        this.eficienciaDia.set(eficienciaDia);
        this.tmpProdutivoDia.set(tmpProdutivoDia);
        this.tmpProducaoDia.set(tmpProducaoDia);
        this.produtividadeDia.set(produtividadeDia);
        this.tmpParadoDia.set(tmpParadoDia);
        this.tmpProduzidoMes.set(tmpProduzidoMes);
        this.tmpProgramadoMes.set(tmpProgramadoMes);
        this.operacaoMes.set(operacaoMes);
        this.tmpDeduzEficMes.set(tmpDeduzEficMes);
        this.eficienciaMes.set(eficienciaMes);
        this.tmpProdutivoMes.set(tmpProdutivoMes);
        this.tmpProducaoMes.set(tmpProducaoMes);
        this.produtividadeMes.set(produtividadeMes);
        this.tmpParadoMes.set(tmpParadoMes);
        this.tmpProduzidoAno.set(tmpProduzidoAno);
        this.tmpProgramadoAno.set(tmpProgramadoAno);
        this.operacaoAno.set(operacaoAno);
        this.tmpDeduzEficAno.set(tmpDeduzEficAno);
        this.eficienciaAno.set(eficienciaAno);
        this.tmpProdutivoAno.set(tmpProdutivoAno);
        this.tmpProducaoAno.set(tmpProducaoAno);
        this.produtividadeAno.set(produtividadeAno);
        this.tmpParadoAno.set(tmpParadoAno);
        
    }
    
    public LocalDate getData() {
        return data.get();
    }
    
    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }
    
    public void setData(LocalDate data) {
        this.data.set(data);
    }
    
    public String getDia() {
        return dia.get();
    }
    
    public StringProperty diaProperty() {
        return dia;
    }
    
    public void setDia(String dia) {
        this.dia.set(dia);
    }
    
    public String getMes() {
        return mes.get();
    }
    
    public StringProperty mesProperty() {
        return mes;
    }
    
    public void setMes(String mes) {
        this.mes.set(mes);
    }
    
    public String getAno() {
        return ano.get();
    }
    
    public StringProperty anoProperty() {
        return ano;
    }
    
    public void setAno(String ano) {
        this.ano.set(ano);
    }
    
    public String getDiaUtil() {
        return diaUtil.get();
    }
    
    public StringProperty diaUtilProperty() {
        return diaUtil;
    }
    
    public void setDiaUtil(String diaUtil) {
        this.diaUtil.set(diaUtil);
    }
    
    public String getCodCelula() {
        return codCelula.get();
    }
    
    public StringProperty codCelulaProperty() {
        return codCelula;
    }
    
    public void setCodCelula(String codCelula) {
        this.codCelula.set(codCelula);
    }
    
    public String getCelula() {
        return celula.get();
    }
    
    public StringProperty celulaProperty() {
        return celula;
    }
    
    public void setCelula(String celula) {
        this.celula.set(celula);
    }
    
    public String getCodLider() {
        return codLider.get();
    }
    
    public StringProperty codLiderProperty() {
        return codLider;
    }
    
    public void setCodLider(String codLider) {
        this.codLider.set(codLider);
    }
    
    public String getLider() {
        return lider.get();
    }
    
    public StringProperty liderProperty() {
        return lider;
    }
    
    public void setLider(String lider) {
        this.lider.set(lider);
    }
    
    public int getQtdeOpers() {
        return qtdeOpers.get();
    }
    
    public IntegerProperty qtdeOpersProperty() {
        return qtdeOpers;
    }
    
    public void setQtdeOpers(int qtdeOpers) {
        this.qtdeOpers.set(qtdeOpers);
    }
    
    public int getMedPecas() {
        return medPecas.get();
    }
    
    public IntegerProperty medPecasProperty() {
        return medPecas;
    }
    
    public void setMedPecas(int medPecas) {
        this.medPecas.set(medPecas);
    }
    
    public double getTmpProduzido() {
        return tmpProduzido.get();
    }
    
    public DoubleProperty tmpProduzidoProperty() {
        return tmpProduzido;
    }
    
    public void setTmpProduzido(double tmpProduzido) {
        this.tmpProduzido.set(tmpProduzido);
    }
    
    public double getTmpProgramado() {
        return tmpProgramado.get();
    }
    
    public DoubleProperty tmpProgramadoProperty() {
        return tmpProgramado;
    }
    
    public void setTmpProgramado(double tmpProgramado) {
        this.tmpProgramado.set(tmpProgramado);
    }
    
    public double getEficiencia() {
        return eficiencia.get();
    }
    
    public DoubleProperty eficienciaProperty() {
        return eficiencia;
    }
    
    public void setEficiencia(double eficiencia) {
        this.eficiencia.set(eficiencia);
    }
    
    public double getTmpProdutivo() {
        return tmpProdutivo.get();
    }
    
    public DoubleProperty tmpProdutivoProperty() {
        return tmpProdutivo;
    }
    
    public void setTmpProdutivo(double tmpProdutivo) {
        this.tmpProdutivo.set(tmpProdutivo);
    }
    
    public double getTmpProducao() {
        return tmpProducao.get();
    }
    
    public DoubleProperty tmpProducaoProperty() {
        return tmpProducao;
    }
    
    public void setTmpProducao(double tmpProducao) {
        this.tmpProducao.set(tmpProducao);
    }
    
    public double getProdutividade() {
        return produtividade.get();
    }
    
    public DoubleProperty produtividadeProperty() {
        return produtividade;
    }
    
    public void setProdutividade(double produtividade) {
        this.produtividade.set(produtividade);
    }
    
    public double getTmpParado() {
        return tmpParado.get();
    }
    
    public DoubleProperty tmpParadoProperty() {
        return tmpParado;
    }
    
    public void setTmpParado(double tmpParado) {
        this.tmpParado.set(tmpParado);
    }
    
    public double getTmpProduzidoDia() {
        return tmpProduzidoDia.get();
    }
    
    public DoubleProperty tmpProduzidoDiaProperty() {
        return tmpProduzidoDia;
    }
    
    public void setTmpProduzidoDia(double tmpProduzidoDia) {
        this.tmpProduzidoDia.set(tmpProduzidoDia);
    }
    
    public double getTmpProgramadoDia() {
        return tmpProgramadoDia.get();
    }
    
    public DoubleProperty tmpProgramadoDiaProperty() {
        return tmpProgramadoDia;
    }
    
    public void setTmpProgramadoDia(double tmpProgramadoDia) {
        this.tmpProgramadoDia.set(tmpProgramadoDia);
    }
    
    public double getEficienciaDia() {
        return eficienciaDia.get();
    }
    
    public DoubleProperty eficienciaDiaProperty() {
        return eficienciaDia;
    }
    
    public void setEficienciaDia(double eficienciaDia) {
        this.eficienciaDia.set(eficienciaDia);
    }
    
    public double getTmpProdutivoDia() {
        return tmpProdutivoDia.get();
    }
    
    public DoubleProperty tmpProdutivoDiaProperty() {
        return tmpProdutivoDia;
    }
    
    public void setTmpProdutivoDia(double tmpProdutivoDia) {
        this.tmpProdutivoDia.set(tmpProdutivoDia);
    }
    
    public double getTmpProducaoDia() {
        return tmpProducaoDia.get();
    }
    
    public DoubleProperty tmpProducaoDiaProperty() {
        return tmpProducaoDia;
    }
    
    public void setTmpProducaoDia(double tmpProducaoDia) {
        this.tmpProducaoDia.set(tmpProducaoDia);
    }
    
    public double getProdutividadeDia() {
        return produtividadeDia.get();
    }
    
    public DoubleProperty produtividadeDiaProperty() {
        return produtividadeDia;
    }
    
    public void setProdutividadeDia(double produtividadeDia) {
        this.produtividadeDia.set(produtividadeDia);
    }
    
    public double getTmpParadoDia() {
        return tmpParadoDia.get();
    }
    
    public DoubleProperty tmpParadoDiaProperty() {
        return tmpParadoDia;
    }
    
    public void setTmpParadoDia(double tmpParadoDia) {
        this.tmpParadoDia.set(tmpParadoDia);
    }
    
    public double getTmpProduzidoMes() {
        return tmpProduzidoMes.get();
    }
    
    public DoubleProperty tmpProduzidoMesProperty() {
        return tmpProduzidoMes;
    }
    
    public void setTmpProduzidoMes(double tmpProduzidoMes) {
        this.tmpProduzidoMes.set(tmpProduzidoMes);
    }
    
    public double getTmpProgramadoMes() {
        return tmpProgramadoMes.get();
    }
    
    public DoubleProperty tmpProgramadoMesProperty() {
        return tmpProgramadoMes;
    }
    
    public void setTmpProgramadoMes(double tmpProgramadoMes) {
        this.tmpProgramadoMes.set(tmpProgramadoMes);
    }
    
    public double getEficienciaMes() {
        return eficienciaMes.get();
    }
    
    public DoubleProperty eficienciaMesProperty() {
        return eficienciaMes;
    }
    
    public void setEficienciaMes(double eficienciaMes) {
        this.eficienciaMes.set(eficienciaMes);
    }
    
    public double getTmpProdutivoMes() {
        return tmpProdutivoMes.get();
    }
    
    public DoubleProperty tmpProdutivoMesProperty() {
        return tmpProdutivoMes;
    }
    
    public void setTmpProdutivoMes(double tmpProdutivoMes) {
        this.tmpProdutivoMes.set(tmpProdutivoMes);
    }
    
    public double getTmpProducaoMes() {
        return tmpProducaoMes.get();
    }
    
    public DoubleProperty tmpProducaoMesProperty() {
        return tmpProducaoMes;
    }
    
    public void setTmpProducaoMes(double tmpProducaoMes) {
        this.tmpProducaoMes.set(tmpProducaoMes);
    }
    
    public double getProdutividadeMes() {
        return produtividadeMes.get();
    }
    
    public DoubleProperty produtividadeMesProperty() {
        return produtividadeMes;
    }
    
    public void setProdutividadeMes(double produtividadeMes) {
        this.produtividadeMes.set(produtividadeMes);
    }
    
    public double getTmpParadoMes() {
        return tmpParadoMes.get();
    }
    
    public DoubleProperty tmpParadoMesProperty() {
        return tmpParadoMes;
    }
    
    public void setTmpParadoMes(double tmpParadoMes) {
        this.tmpParadoMes.set(tmpParadoMes);
    }
    
    public double getTmpProduzidoAno() {
        return tmpProduzidoAno.get();
    }
    
    public DoubleProperty tmpProduzidoAnoProperty() {
        return tmpProduzidoAno;
    }
    
    public void setTmpProduzidoAno(double tmpProduzidoAno) {
        this.tmpProduzidoAno.set(tmpProduzidoAno);
    }
    
    public double getTmpProgramadoAno() {
        return tmpProgramadoAno.get();
    }
    
    public DoubleProperty tmpProgramadoAnoProperty() {
        return tmpProgramadoAno;
    }
    
    public void setTmpProgramadoAno(double tmpProgramadoAno) {
        this.tmpProgramadoAno.set(tmpProgramadoAno);
    }
    
    public double getEficienciaAno() {
        return eficienciaAno.get();
    }
    
    public DoubleProperty eficienciaAnoProperty() {
        return eficienciaAno;
    }
    
    public void setEficienciaAno(double eficienciaAno) {
        this.eficienciaAno.set(eficienciaAno);
    }
    
    public double getTmpProdutivoAno() {
        return tmpProdutivoAno.get();
    }
    
    public DoubleProperty tmpProdutivoAnoProperty() {
        return tmpProdutivoAno;
    }
    
    public void setTmpProdutivoAno(double tmpProdutivoAno) {
        this.tmpProdutivoAno.set(tmpProdutivoAno);
    }
    
    public double getTmpProducaoAno() {
        return tmpProducaoAno.get();
    }
    
    public DoubleProperty tmpProducaoAnoProperty() {
        return tmpProducaoAno;
    }
    
    public void setTmpProducaoAno(double tmpProducaoAno) {
        this.tmpProducaoAno.set(tmpProducaoAno);
    }
    
    public double getProdutividadeAno() {
        return produtividadeAno.get();
    }
    
    public DoubleProperty produtividadeAnoProperty() {
        return produtividadeAno;
    }
    
    public void setProdutividadeAno(double produtividadeAno) {
        this.produtividadeAno.set(produtividadeAno);
    }
    
    public double getTmpParadoAno() {
        return tmpParadoAno.get();
    }
    
    public DoubleProperty tmpParadoAnoProperty() {
        return tmpParadoAno;
    }
    
    public void setTmpParadoAno(double tmpParadoAno) {
        this.tmpParadoAno.set(tmpParadoAno);
    }
    
    public double getOperacao() {
        return operacao.get();
    }
    
    public DoubleProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(double operacao) {
        this.operacao.set(operacao);
    }
    
    public double getTmpDeduzEfic() {
        return tmpDeduzEfic.get();
    }
    
    public DoubleProperty tmpDeduzEficProperty() {
        return tmpDeduzEfic;
    }
    
    public void setTmpDeduzEfic(double tmpDeduzEfic) {
        this.tmpDeduzEfic.set(tmpDeduzEfic);
    }
    
    public double getOperacaoDia() {
        return operacaoDia.get();
    }
    
    public DoubleProperty operacaoDiaProperty() {
        return operacaoDia;
    }
    
    public void setOperacaoDia(double operacaoDia) {
        this.operacaoDia.set(operacaoDia);
    }
    
    public double getTmpDeduzEficDia() {
        return tmpDeduzEficDia.get();
    }
    
    public DoubleProperty tmpDeduzEficDiaProperty() {
        return tmpDeduzEficDia;
    }
    
    public void setTmpDeduzEficDia(double tmpDeduzEficDia) {
        this.tmpDeduzEficDia.set(tmpDeduzEficDia);
    }
    
    public double getOperacaoMes() {
        return operacaoMes.get();
    }
    
    public DoubleProperty operacaoMesProperty() {
        return operacaoMes;
    }
    
    public void setOperacaoMes(double operacaoMes) {
        this.operacaoMes.set(operacaoMes);
    }
    
    public double getTmpDeduzEficMes() {
        return tmpDeduzEficMes.get();
    }
    
    public DoubleProperty tmpDeduzEficMesProperty() {
        return tmpDeduzEficMes;
    }
    
    public void setTmpDeduzEficMes(double tmpDeduzEficMes) {
        this.tmpDeduzEficMes.set(tmpDeduzEficMes);
    }
    
    public double getOperacaoAno() {
        return operacaoAno.get();
    }
    
    public DoubleProperty operacaoAnoProperty() {
        return operacaoAno;
    }
    
    public void setOperacaoAno(double operacaoAno) {
        this.operacaoAno.set(operacaoAno);
    }
    
    public double getTmpDeduzEficAno() {
        return tmpDeduzEficAno.get();
    }
    
    public DoubleProperty tmpDeduzEficAnoProperty() {
        return tmpDeduzEficAno;
    }
    
    public void setTmpDeduzEficAno(double tmpDeduzEficAno) {
        this.tmpDeduzEficAno.set(tmpDeduzEficAno);
    }
}
