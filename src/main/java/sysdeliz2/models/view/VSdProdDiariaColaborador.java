package sysdeliz2.models.view;

import javafx.beans.property.*;

import java.time.LocalDate;


public class VSdProdDiariaColaborador {
    
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty dia = new SimpleStringProperty();
    private final StringProperty mes = new SimpleStringProperty();
    private final StringProperty ano = new SimpleStringProperty();
    private final StringProperty diaUtil = new SimpleStringProperty();
    
    private final StringProperty codCelula = new SimpleStringProperty();
    private final StringProperty celula = new SimpleStringProperty();
    private final StringProperty codColaborador = new SimpleStringProperty();
    private final StringProperty colaborador = new SimpleStringProperty();
    
    private final IntegerProperty qtdeOpers = new SimpleIntegerProperty();
    private final IntegerProperty medPecas = new SimpleIntegerProperty();
    
    private final DoubleProperty tmpRealizado = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramado = new SimpleDoubleProperty();
    private final DoubleProperty operacao = new SimpleDoubleProperty();
    private final DoubleProperty eficiencia = new SimpleDoubleProperty();
    private final DoubleProperty tmpDisponivel = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadaDeduz = new SimpleDoubleProperty();
    private final DoubleProperty produtividade = new SimpleDoubleProperty();
    private final DoubleProperty tmpParado = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEfic = new SimpleDoubleProperty();
    
    private final DoubleProperty tmpRealizadoMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramadoMes = new SimpleDoubleProperty();
    private final DoubleProperty operacaoMes = new SimpleDoubleProperty();
    private final DoubleProperty eficienciaMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpDisponivelMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadaDeduzMes = new SimpleDoubleProperty();
    private final DoubleProperty produtividadeMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadoMes = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEficMes = new SimpleDoubleProperty();
    
    private final DoubleProperty tmpRealizadoAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpProgramadoAno = new SimpleDoubleProperty();
    private final DoubleProperty operacaoAno = new SimpleDoubleProperty();
    private final DoubleProperty eficienciaAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpDisponivelAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadaDeduzAno = new SimpleDoubleProperty();
    private final DoubleProperty produtividadeAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpParadoAno = new SimpleDoubleProperty();
    private final DoubleProperty tmpDeduzEficAno = new SimpleDoubleProperty();
    
    public VSdProdDiariaColaborador() {    }
    
    public VSdProdDiariaColaborador(String dia, String mes, String ano, LocalDate data, String diaUtil,
                                    String codCelula, String celula, String codColaborador, String colaborador,
                                    Integer qtdeOpers, Integer medPecas,
                                    Double tmpRealizado, Double tmpProgramado, Double tmpDisponivel, Double tmpParadaDeduz, Double tmpParado, Double tmpDeduzEfic,
                                    Double tmpRealizadoMes, Double tmpProgramadoMes, Double tmpDisponivelMes, Double tmpParadaDeduzMes, Double tmpParadoMes, Double tmpDeduzEficMes,
                                    Double tmpRealizadoAno, Double tmpProgramadoAno, Double tmpDisponivelAno, Double tmpParadaDeduzAno, Double tmpParadoAno, Double tmpDeduzEficAno) {
        this.data.set(data);
        this.dia.set(dia);
        this.mes.set(mes);
        this.ano.set(ano);
        this.diaUtil.set(diaUtil);
        this.codCelula.set(codCelula);
        this.celula.set(celula);
        this.codColaborador.set(codColaborador);
        this.colaborador.set(colaborador);
        this.qtdeOpers.set(qtdeOpers);
        this.medPecas.set(medPecas);
        this.tmpRealizado.set(tmpRealizado);
        this.tmpProgramado.set(tmpProgramado);
        this.operacao.set(Math.round((tmpProgramado/ tmpRealizado)*100));
        this.eficiencia.set(Math.round((tmpProgramado/ (tmpDisponivel - tmpDeduzEfic))*100));
        this.tmpDisponivel.set(tmpDisponivel);
        this.tmpParadaDeduz.set(tmpParadaDeduz);
        this.produtividade.set(Math.round((tmpProgramado/(tmpDisponivel -tmpParadaDeduz))*100));
        this.tmpParado.set(tmpParado);
        this.tmpDeduzEfic.set(tmpDeduzEfic);
        this.tmpRealizadoMes.set(tmpRealizadoMes);
        this.tmpProgramadoMes.set(tmpProgramadoMes);
        this.operacaoMes.set(Math.round((tmpProgramadoMes/ tmpRealizadoMes)*100));
        this.eficienciaMes.set(Math.round((tmpProgramadoMes/ (tmpDisponivelMes - tmpDeduzEficMes))*100));
        this.tmpDisponivelMes.set(tmpDisponivelMes);
        this.tmpParadaDeduzMes.set(tmpParadaDeduzMes);
        this.produtividadeMes.set(Math.round((tmpProgramadoMes/(tmpDisponivelMes -tmpParadaDeduzMes))*100));
        this.tmpParadoMes.set(tmpParadoMes);
        this.tmpDeduzEficMes.set(tmpDeduzEficMes);
        this.tmpRealizadoAno.set(tmpRealizadoAno);
        this.tmpProgramadoAno.set(tmpProgramadoAno);
        this.operacaoAno.set(Math.round((tmpProgramadoAno/ tmpRealizadoAno)*100));
        this.eficienciaAno.set(Math.round((tmpProgramadoAno/ (tmpDisponivelAno - tmpDeduzEficAno))*100));
        this.tmpDisponivelAno.set(tmpDisponivelAno);
        this.tmpParadaDeduzAno.set(tmpParadaDeduzAno);
        this.produtividadeAno.set(Math.round((tmpProgramadoAno/(tmpDisponivelAno -tmpParadaDeduzAno))*100));
        this.tmpParadoAno.set(tmpParadoAno);
        this.tmpDeduzEficAno.set(tmpDeduzEficAno);
        
    }
    
    public LocalDate getData() {
        return data.get();
    }
    
    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }
    
    public void setData(LocalDate data) {
        this.data.set(data);
    }
    
    public String getDia() {
        return dia.get();
    }
    
    public StringProperty diaProperty() {
        return dia;
    }
    
    public void setDia(String dia) {
        this.dia.set(dia);
    }
    
    public String getMes() {
        return mes.get();
    }
    
    public StringProperty mesProperty() {
        return mes;
    }
    
    public void setMes(String mes) {
        this.mes.set(mes);
    }
    
    public String getAno() {
        return ano.get();
    }
    
    public StringProperty anoProperty() {
        return ano;
    }
    
    public void setAno(String ano) {
        this.ano.set(ano);
    }
    
    public String getDiaUtil() {
        return diaUtil.get();
    }
    
    public StringProperty diaUtilProperty() {
        return diaUtil;
    }
    
    public void setDiaUtil(String diaUtil) {
        this.diaUtil.set(diaUtil);
    }
    
    public String getCodCelula() {
        return codCelula.get();
    }
    
    public StringProperty codCelulaProperty() {
        return codCelula;
    }
    
    public void setCodCelula(String codCelula) {
        this.codCelula.set(codCelula);
    }
    
    public String getCelula() {
        return celula.get();
    }
    
    public StringProperty celulaProperty() {
        return celula;
    }
    
    public void setCelula(String celula) {
        this.celula.set(celula);
    }
    
    public String getCodColaborador() {
        return codColaborador.get();
    }
    
    public StringProperty codColaboradorProperty() {
        return codColaborador;
    }
    
    public void setCodColaborador(String codColaborador) {
        this.codColaborador.set(codColaborador);
    }
    
    public String getColaborador() {
        return colaborador.get();
    }
    
    public StringProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(String colaborador) {
        this.colaborador.set(colaborador);
    }
    
    public int getQtdeOpers() {
        return qtdeOpers.get();
    }
    
    public IntegerProperty qtdeOpersProperty() {
        return qtdeOpers;
    }
    
    public void setQtdeOpers(int qtdeOpers) {
        this.qtdeOpers.set(qtdeOpers);
    }
    
    public int getMedPecas() {
        return medPecas.get();
    }
    
    public IntegerProperty medPecasProperty() {
        return medPecas;
    }
    
    public void setMedPecas(int medPecas) {
        this.medPecas.set(medPecas);
    }
    
    public double getTmpRealizado() {
        return tmpRealizado.get();
    }
    
    public DoubleProperty tmpRealizadoProperty() {
        return tmpRealizado;
    }
    
    public void setTmpRealizado(double tmpRealizado) {
        this.tmpRealizado.set(tmpRealizado);
    }
    
    public double getTmpProgramado() {
        return tmpProgramado.get();
    }
    
    public DoubleProperty tmpProgramadoProperty() {
        return tmpProgramado;
    }
    
    public void setTmpProgramado(double tmpProgramado) {
        this.tmpProgramado.set(tmpProgramado);
    }
    
    public double getEficiencia() {
        return eficiencia.get();
    }
    
    public DoubleProperty eficienciaProperty() {
        return eficiencia;
    }
    
    public void setEficiencia(double eficiencia) {
        this.eficiencia.set(eficiencia);
    }
    
    public double getTmpDisponivel() {
        return tmpDisponivel.get();
    }
    
    public DoubleProperty tmpDisponivelProperty() {
        return tmpDisponivel;
    }
    
    public void setTmpDisponivel(double tmpDisponivel) {
        this.tmpDisponivel.set(tmpDisponivel);
    }
    
    public double getTmpParadaDeduz() {
        return tmpParadaDeduz.get();
    }
    
    public DoubleProperty tmpParadaDeduzProperty() {
        return tmpParadaDeduz;
    }
    
    public void setTmpParadaDeduz(double tmpParadaDeduz) {
        this.tmpParadaDeduz.set(tmpParadaDeduz);
    }
    
    public double getProdutividade() {
        return produtividade.get();
    }
    
    public DoubleProperty produtividadeProperty() {
        return produtividade;
    }
    
    public void setProdutividade(double produtividade) {
        this.produtividade.set(produtividade);
    }
    
    public double getTmpParado() {
        return tmpParado.get();
    }
    
    public DoubleProperty tmpParadoProperty() {
        return tmpParado;
    }
    
    public void setTmpParado(double tmpParado) {
        this.tmpParado.set(tmpParado);
    }
    
    public double getTmpRealizadoMes() {
        return tmpRealizadoMes.get();
    }
    
    public DoubleProperty tmpRealizadoMesProperty() {
        return tmpRealizadoMes;
    }
    
    public void setTmpRealizadoMes(double tmpRealizadoMes) {
        this.tmpRealizadoMes.set(tmpRealizadoMes);
    }
    
    public double getTmpProgramadoMes() {
        return tmpProgramadoMes.get();
    }
    
    public DoubleProperty tmpProgramadoMesProperty() {
        return tmpProgramadoMes;
    }
    
    public void setTmpProgramadoMes(double tmpProgramadoMes) {
        this.tmpProgramadoMes.set(tmpProgramadoMes);
    }
    
    public double getEficienciaMes() {
        return eficienciaMes.get();
    }
    
    public DoubleProperty eficienciaMesProperty() {
        return eficienciaMes;
    }
    
    public void setEficienciaMes(double eficienciaMes) {
        this.eficienciaMes.set(eficienciaMes);
    }
    
    public double getTmpDisponivelMes() {
        return tmpDisponivelMes.get();
    }
    
    public DoubleProperty tmpDisponivelMesProperty() {
        return tmpDisponivelMes;
    }
    
    public void setTmpDisponivelMes(double tmpDisponivelMes) {
        this.tmpDisponivelMes.set(tmpDisponivelMes);
    }
    
    public double getTmpParadaDeduzMes() {
        return tmpParadaDeduzMes.get();
    }
    
    public DoubleProperty tmpParadaDeduzMesProperty() {
        return tmpParadaDeduzMes;
    }
    
    public void setTmpParadaDeduzMes(double tmpParadaDeduzMes) {
        this.tmpParadaDeduzMes.set(tmpParadaDeduzMes);
    }
    
    public double getProdutividadeMes() {
        return produtividadeMes.get();
    }
    
    public DoubleProperty produtividadeMesProperty() {
        return produtividadeMes;
    }
    
    public void setProdutividadeMes(double produtividadeMes) {
        this.produtividadeMes.set(produtividadeMes);
    }
    
    public double getTmpParadoMes() {
        return tmpParadoMes.get();
    }
    
    public DoubleProperty tmpParadoMesProperty() {
        return tmpParadoMes;
    }
    
    public void setTmpParadoMes(double tmpParadoMes) {
        this.tmpParadoMes.set(tmpParadoMes);
    }
    
    public double getTmpRealizadoAno() {
        return tmpRealizadoAno.get();
    }
    
    public DoubleProperty tmpRealizadoAnoProperty() {
        return tmpRealizadoAno;
    }
    
    public void setTmpRealizadoAno(double tmpRealizadoAno) {
        this.tmpRealizadoAno.set(tmpRealizadoAno);
    }
    
    public double getTmpProgramadoAno() {
        return tmpProgramadoAno.get();
    }
    
    public DoubleProperty tmpProgramadoAnoProperty() {
        return tmpProgramadoAno;
    }
    
    public void setTmpProgramadoAno(double tmpProgramadoAno) {
        this.tmpProgramadoAno.set(tmpProgramadoAno);
    }
    
    public double getEficienciaAno() {
        return eficienciaAno.get();
    }
    
    public DoubleProperty eficienciaAnoProperty() {
        return eficienciaAno;
    }
    
    public void setEficienciaAno(double eficienciaAno) {
        this.eficienciaAno.set(eficienciaAno);
    }
    
    public double getTmpDisponivelAno() {
        return tmpDisponivelAno.get();
    }
    
    public DoubleProperty tmpDisponivelAnoProperty() {
        return tmpDisponivelAno;
    }
    
    public void setTmpDisponivelAno(double tmpDisponivelAno) {
        this.tmpDisponivelAno.set(tmpDisponivelAno);
    }
    
    public double getTmpParadaDeduzAno() {
        return tmpParadaDeduzAno.get();
    }
    
    public DoubleProperty tmpParadaDeduzAnoProperty() {
        return tmpParadaDeduzAno;
    }
    
    public void setTmpParadaDeduzAno(double tmpParadaDeduzAno) {
        this.tmpParadaDeduzAno.set(tmpParadaDeduzAno);
    }
    
    public double getProdutividadeAno() {
        return produtividadeAno.get();
    }
    
    public DoubleProperty produtividadeAnoProperty() {
        return produtividadeAno;
    }
    
    public void setProdutividadeAno(double produtividadeAno) {
        this.produtividadeAno.set(produtividadeAno);
    }
    
    public double getTmpParadoAno() {
        return tmpParadoAno.get();
    }
    
    public DoubleProperty tmpParadoAnoProperty() {
        return tmpParadoAno;
    }
    
    public void setTmpParadoAno(double tmpParadoAno) {
        this.tmpParadoAno.set(tmpParadoAno);
    }
    
    public double getTmpDeduzEfic() {
        return tmpDeduzEfic.get();
    }
    
    public DoubleProperty tmpDeduzEficProperty() {
        return tmpDeduzEfic;
    }
    
    public void setTmpDeduzEfic(double tmpDeduzEfic) {
        this.tmpDeduzEfic.set(tmpDeduzEfic);
    }
    
    public double getTmpDeduzEficMes() {
        return tmpDeduzEficMes.get();
    }
    
    public DoubleProperty tmpDeduzEficMesProperty() {
        return tmpDeduzEficMes;
    }
    
    public void setTmpDeduzEficMes(double tmpDeduzEficMes) {
        this.tmpDeduzEficMes.set(tmpDeduzEficMes);
    }
    
    public double getTmpDeduzEficAno() {
        return tmpDeduzEficAno.get();
    }
    
    public DoubleProperty tmpDeduzEficAnoProperty() {
        return tmpDeduzEficAno;
    }
    
    public void setTmpDeduzEficAno(double tmpDeduzEficAno) {
        this.tmpDeduzEficAno.set(tmpDeduzEficAno);
    }
    
    public double getOperacao() {
        return operacao.get();
    }
    
    public DoubleProperty operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(double operacao) {
        this.operacao.set(operacao);
    }
    
    public double getOperacaoMes() {
        return operacaoMes.get();
    }
    
    public DoubleProperty operacaoMesProperty() {
        return operacaoMes;
    }
    
    public void setOperacaoMes(double operacaoMes) {
        this.operacaoMes.set(operacaoMes);
    }
    
    public double getOperacaoAno() {
        return operacaoAno.get();
    }
    
    public DoubleProperty operacaoAnoProperty() {
        return operacaoAno;
    }
    
    public void setOperacaoAno(double operacaoAno) {
        this.operacaoAno.set(operacaoAno);
    }
}
