package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdColaborador;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_PROD_HORA_COLAB")
public class VSdProdHoraColab implements Serializable {
    
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpDisponivel = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpRealizado = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpOperacao = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpIntervalo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpDeduzProd = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpDeduzEfic = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpParado = new SimpleObjectProperty<>();
    
    public VSdProdHoraColab() {
    }
    
    @Id
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name = "TMP_DISPONIVEL")
    public BigDecimal getTmpDisponivel() {
        return tmpDisponivel.get();
    }
    
    public ObjectProperty<BigDecimal> tmpDisponivelProperty() {
        return tmpDisponivel;
    }
    
    public void setTmpDisponivel(BigDecimal tmpDisponivel) {
        this.tmpDisponivel.set(tmpDisponivel);
    }
    
    @Column(name = "TMP_REALIZADO")
    public BigDecimal getTmpRealizado() {
        return tmpRealizado.get();
    }
    
    public ObjectProperty<BigDecimal> tmpRealizadoProperty() {
        return tmpRealizado;
    }
    
    public void setTmpRealizado(BigDecimal tmpRealizado) {
        this.tmpRealizado.set(tmpRealizado);
    }
    
    @Column(name = "TMP_OPERACAO")
    public BigDecimal getTmpOperacao() {
        return tmpOperacao.get();
    }
    
    public ObjectProperty<BigDecimal> tmpOperacaoProperty() {
        return tmpOperacao;
    }
    
    public void setTmpOperacao(BigDecimal tmpOperacao) {
        this.tmpOperacao.set(tmpOperacao);
    }
    
    @Column(name = "TMP_INTERVALO")
    public BigDecimal getTmpIntervalo() {
        return tmpIntervalo.get();
    }
    
    public ObjectProperty<BigDecimal> tmpIntervaloProperty() {
        return tmpIntervalo;
    }
    
    public void setTmpIntervalo(BigDecimal tmpIntervalo) {
        this.tmpIntervalo.set(tmpIntervalo);
    }
    
    @Column(name = "TMP_DEDUZ_PROD")
    public BigDecimal getTmpDeduzProd() {
        return tmpDeduzProd.get();
    }
    
    public ObjectProperty<BigDecimal> tmpDeduzProdProperty() {
        return tmpDeduzProd;
    }
    
    public void setTmpDeduzProd(BigDecimal tmpDeduzProd) {
        this.tmpDeduzProd.set(tmpDeduzProd);
    }
    
    @Column(name = "TMP_DEDUZ_EFIC")
    public BigDecimal getTmpDeduzEfic() {
        return tmpDeduzEfic.get();
    }
    
    public ObjectProperty<BigDecimal> tmpDeduzEficProperty() {
        return tmpDeduzEfic;
    }
    
    public void setTmpDeduzEfic(BigDecimal tmpDeduzEfic) {
        this.tmpDeduzEfic.set(tmpDeduzEfic);
    }
    
    @Column(name = "TMP_PARADO")
    public BigDecimal getTmpParado() {
        return tmpParado.get();
    }
    
    public ObjectProperty<BigDecimal> tmpParadoProperty() {
        return tmpParado;
    }
    
    public void setTmpParado(BigDecimal tmpParado) {
        this.tmpParado.set(tmpParado);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
