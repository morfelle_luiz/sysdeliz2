package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdCelula;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOperacaoOrganize001;
import sysdeliz2.models.sysdeliz.SdPacoteLancamento001;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Immutable
@Table(name = "V_SD_PROD_HORA_OPERACAO")
public class VSdProdHoraOperacao {
    
    // Atributos Entity
    private final IntegerProperty idJpa = new SimpleIntegerProperty();
    private final ObjectProperty<SdCelula> celula = new SimpleObjectProperty<>();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dhFim = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> referencia = new SimpleObjectProperty<>();
    private final ObjectProperty<SdPacoteLancamento001> pacoteLanc = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<SdOperacaoOrganize001> operacao = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpOperacao = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpRealizado = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> eficiencia = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpOperacaoTotal = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tmpRealizadoTotal = new SimpleObjectProperty<>();
    
    // Atributos Objeto
    private final StringProperty codProduto = new SimpleStringProperty();
    private final StringProperty descProduto = new SimpleStringProperty();
    private final StringProperty codCor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final ObjectProperty<LocalTime> horaInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalTime> horaFim = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
    private final StringProperty pacoteProg = new SimpleStringProperty();
    private final ObjectProperty<SdColaborador> lider = new SimpleObjectProperty<>();
    private final StringProperty codFamilia = new SimpleStringProperty();
    private final StringProperty descFamilia = new SimpleStringProperty();
    
    @Id
    @Column(name = "ID_JPA")
    public int getIdJpa() {
        return idJpa.get();
    }
    
    public IntegerProperty idJpaProperty() {
        return idJpa;
    }
    
    public void setIdJpa(int idJpa) {
        this.idJpa.set(idJpa);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CELULA")
    public SdCelula getCelula() {
        return celula.get();
    }
    
    public ObjectProperty<SdCelula> celulaProperty() {
        return celula;
    }
    
    public void setCelula(SdCelula celula) {
        this.lider.set(celula != null ? celula.getLider() : null);
        this.celula.set(celula);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLABORADOR")
    public SdColaborador getColaborador() {
        return colaborador.get();
    }
    
    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(SdColaborador colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @Column(name = "DH_INICIO")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhInicio() {
        return dhInicio.get();
    }
    
    public ObjectProperty<LocalDateTime> dhInicioProperty() {
        return dhInicio;
    }
    
    public void setDhInicio(LocalDateTime dhInicio) {
        this.horaInicio.set(dhInicio.toLocalTime());
        this.data.set(dhInicio.toLocalDate());
        this.dhInicio.set(dhInicio);
    }
    
    @Column(name = "DH_FIM")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDhFim() {
        return dhFim.get();
    }
    
    public ObjectProperty<LocalDateTime> dhFimProperty() {
        return dhFim;
    }
    
    public void setDhFim(LocalDateTime dhFim) {
        this.horaFim.set(dhFim.toLocalTime());
        this.dhFim.set(dhFim);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "REFERENCIA")
    public VSdDadosProduto getReferencia() {
        return referencia.get();
    }
    
    public ObjectProperty<VSdDadosProduto> referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(VSdDadosProduto referencia) {
        this.codFamilia.set(referencia.getCodFam());
        this.descFamilia.set(referencia.getFamilia());
        this.codProduto.set(referencia.getCodigo());
        this.descProduto.set(referencia.getDescricao());
        this.referencia.set(referencia);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "PACOTE_PROG"),
            @JoinColumn(name = "PACOTE_LANC")
    })
    public SdPacoteLancamento001 getPacoteLanc() {
        return pacoteLanc.get();
    }
    
    public ObjectProperty<SdPacoteLancamento001> pacoteLancProperty() {
        return pacoteLanc;
    }
    
    public void setPacoteLanc(SdPacoteLancamento001 pacoteLanc) {
        this.codCor.set(pacoteLanc.getCor() != null ? pacoteLanc.getCor().getCor() : null);
        this.descCor.set(pacoteLanc.getCor() != null ? pacoteLanc.getCor().getDescricao() : null);
        this.tam.set(pacoteLanc.getTam());
        this.pacoteProg.set(pacoteLanc.getId().getCodigo());
        this.pacoteLanc.set(pacoteLanc);
    }
    
    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERACAO")
    public SdOperacaoOrganize001 getOperacao() {
        return operacao.get();
    }
    
    public ObjectProperty<SdOperacaoOrganize001> operacaoProperty() {
        return operacao;
    }
    
    public void setOperacao(SdOperacaoOrganize001 operacao) {
        this.operacao.set(operacao);
    }
    
    @Column(name = "TMP_OPERACAO")
    public BigDecimal getTmpOperacao() {
        return tmpOperacao.get();
    }
    
    public ObjectProperty<BigDecimal> tmpOperacaoProperty() {
        return tmpOperacao;
    }
    
    public void setTmpOperacao(BigDecimal tmpOperacao) {
        this.tmpOperacao.set(tmpOperacao);
    }
    
    @Column(name = "TMP_REALIZADO")
    public BigDecimal getTmpRealizado() {
        return tmpRealizado.get();
    }
    
    public ObjectProperty<BigDecimal> tmpRealizadoProperty() {
        return tmpRealizado;
    }
    
    public void setTmpRealizado(BigDecimal tmpRealizado) {
        this.tmpRealizado.set(tmpRealizado);
    }
    
    @Column(name = "EFICIENCIA")
    public BigDecimal getEficiencia() {
        return eficiencia.get();
    }
    
    public ObjectProperty<BigDecimal> eficienciaProperty() {
        return eficiencia;
    }
    
    public void setEficiencia(BigDecimal eficiencia) {
        this.eficiencia.set(eficiencia);
    }
    
    @Column(name = "TMP_OPERACAO_TOTAL")
    public BigDecimal getTmpOperacaoTotal() {
        return tmpOperacaoTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tmpOperacaoTotalProperty() {
        return tmpOperacaoTotal;
    }
    
    public void setTmpOperacaoTotal(BigDecimal tmpOperacaoTotal) {
        this.tmpOperacaoTotal.set(tmpOperacaoTotal);
    }
    
    @Column(name = "TMP_REALIZADO_TOTAL")
    public BigDecimal getTmpRealizadoTotal() {
        return tmpRealizadoTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tmpRealizadoTotalProperty() {
        return tmpRealizadoTotal;
    }
    
    public void setTmpRealizadoTotal(BigDecimal tmpRealizadoTotal) {
        this.tmpRealizadoTotal.set(tmpRealizadoTotal);
    }
    
    @Transient
    public String getCodProduto() {
        return codProduto.get();
    }
    
    public StringProperty codProdutoProperty() {
        return codProduto;
    }
    
    public void setCodProduto(String codProduto) {
        this.codProduto.set(codProduto);
    }
    
    @Transient
    public String getDescProduto() {
        return descProduto.get();
    }
    
    public StringProperty descProdutoProperty() {
        return descProduto;
    }
    
    public void setDescProduto(String descProduto) {
        this.descProduto.set(descProduto);
    }
    
    @Transient
    public String getCodCor() {
        return codCor.get();
    }
    
    public StringProperty codCorProperty() {
        return codCor;
    }
    
    public void setCodCor(String codCor) {
        this.codCor.set(codCor);
    }
    
    @Transient
    public String getDescCor() {
        return descCor.get();
    }
    
    public StringProperty descCorProperty() {
        return descCor;
    }
    
    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }
    
    @Transient
    public String getTam() {
        return tam.get();
    }
    
    public StringProperty tamProperty() {
        return tam;
    }
    
    public void setTam(String tam) {
        this.tam.set(tam);
    }
    
    @Transient
    public LocalTime getHoraInicio() {
        return horaInicio.get();
    }
    
    public ObjectProperty<LocalTime> horaInicioProperty() {
        return horaInicio;
    }
    
    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio.set(horaInicio);
    }
    
    @Transient
    public LocalTime getHoraFim() {
        return horaFim.get();
    }
    
    public ObjectProperty<LocalTime> horaFimProperty() {
        return horaFim;
    }
    
    public void setHoraFim(LocalTime horaFim) {
        this.horaFim.set(horaFim);
    }
    
    @Transient
    public String getPacoteProg() {
        return pacoteProg.get();
    }
    
    public StringProperty pacoteProgProperty() {
        return pacoteProg;
    }
    
    public void setPacoteProg(String pacoteProg) {
        this.pacoteProg.set(pacoteProg);
    }
    
    @Transient
    public SdColaborador getLider() {
        return lider.get();
    }
    
    public ObjectProperty<SdColaborador> liderProperty() {
        return lider;
    }
    
    public void setLider(SdColaborador lider) {
        this.lider.set(lider);
    }
    
    @Transient
    public String getCodFamilia() {
        return codFamilia.get();
    }
    
    public StringProperty codFamiliaProperty() {
        return codFamilia;
    }
    
    public void setCodFamilia(String codFamilia) {
        this.codFamilia.set(codFamilia);
    }
    
    @Transient
    public String getDescFamilia() {
        return descFamilia.get();
    }
    
    public StringProperty descFamiliaProperty() {
        return descFamilia;
    }
    
    public void setDescFamilia(String descFamilia) {
        this.descFamilia.set(descFamilia);
    }
    
    @Transient
    public LocalDate getData() {
        return data.get();
    }
    
    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }
    
    public void setData(LocalDate data) {
        this.data.set(data);
    }
}
