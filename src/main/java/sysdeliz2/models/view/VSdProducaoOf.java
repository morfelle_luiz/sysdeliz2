package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.converters.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "V_SD_PRODUCAO_OF")
public class VSdProducaoOf implements Serializable {
    
    private final StringProperty id = new SimpleStringProperty();
    private final StringProperty statusOf = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtEnv = new SimpleObjectProperty<>();//
    private final ObjectProperty<LocalDate> dtRet = new SimpleObjectProperty<>();//
    private final ObjectProperty<LocalDateTime> dtProg = new SimpleObjectProperty<>();//
    private final StringProperty op = new SimpleStringProperty();//
    private final StringProperty setor = new SimpleStringProperty();//
    private final StringProperty periodo = new SimpleStringProperty();//
    private final StringProperty referencia = new SimpleStringProperty();//
    private final StringProperty produto = new SimpleStringProperty();//
    private final StringProperty linha = new SimpleStringProperty();//
    private final StringProperty familia = new SimpleStringProperty();//
    private final StringProperty faccao = new SimpleStringProperty();//
    private final StringProperty statusMat = new SimpleStringProperty();//
    private final StringProperty codMat = new SimpleStringProperty();//
    private final StringProperty material = new SimpleStringProperty();//
    private final StringProperty codigo = new SimpleStringProperty();//
    private final IntegerProperty qtdePct = new SimpleIntegerProperty();//
    private final StringProperty tipoProg = new SimpleStringProperty();//
    private final StringProperty tipoLanc = new SimpleStringProperty();//
    private final StringProperty setores = new SimpleStringProperty();//
    private final IntegerProperty qtdeOpers = new SimpleIntegerProperty();//
    private final IntegerProperty qtdeColabs = new SimpleIntegerProperty();//
    private final IntegerProperty qtdeProg = new SimpleIntegerProperty();
    private final IntegerProperty qtdeProd = new SimpleIntegerProperty();
    private final IntegerProperty qtdePend = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> diasProg = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> diasProd = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> diasPend = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> percProd = new SimpleObjectProperty<>();//
    private final ObjectProperty<BigDecimal> eficOf = new SimpleObjectProperty<>();//
    
    public VSdProducaoOf() {
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusOf() {
        return statusOf.get();
    }
    
    public StringProperty statusOfProperty() {
        return statusOf;
    }
    
    public void setStatusOf(String statusOf) {
        this.statusOf.set(statusOf);
    }
    
    @Column(name = "DT_ENV")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEnv() {
        return dtEnv.get();
    }
    
    public ObjectProperty<LocalDate> dtEnvProperty() {
        return dtEnv;
    }
    
    public void setDtEnv(LocalDate dtEnv) {
        this.dtEnv.set(dtEnv);
    }
    
    @Column(name = "DT_RET")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRet() {
        return dtRet.get();
    }
    
    public ObjectProperty<LocalDate> dtRetProperty() {
        return dtRet;
    }
    
    public void setDtRet(LocalDate dtRet) {
        this.dtRet.set(dtRet);
    }
    
    @Column(name = "DT_PROG")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    public LocalDateTime getDtProg() {
        return dtProg.get();
    }
    
    public ObjectProperty<LocalDateTime> dtProgProperty() {
        return dtProg;
    }
    
    public void setDtProg(LocalDateTime dtProg) {
        this.dtProg.set(dtProg);
    }
    
    @Column(name = "OP")
    public String getOp() {
        return op.get();
    }
    
    public StringProperty opProperty() {
        return op;
    }
    
    public void setOp(String op) {
        this.op.set(op);
    }
    
    @Column(name = "SETOR")
    public String getSetor() {
        return setor.get();
    }
    
    public StringProperty setorProperty() {
        return setor;
    }
    
    public void setSetor(String setor) {
        this.setor.set(setor);
    }
    
    @Column(name = "PERIODO")
    public String getPeriodo() {
        return periodo.get();
    }
    
    public StringProperty periodoProperty() {
        return periodo;
    }
    
    public void setPeriodo(String periodo) {
        this.periodo.set(periodo);
    }
    
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }
    
    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }
    
    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }
    
    public StringProperty produtoProperty() {
        return produto;
    }
    
    public void setProduto(String produto) {
        this.produto.set(produto);
    }
    
    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }
    
    public StringProperty linhaProperty() {
        return linha;
    }
    
    public void setLinha(String linha) {
        this.linha.set(linha);
    }
    
    @Column(name = "FAMILIA")
    public String getFamilia() {
        return familia.get();
    }
    
    public StringProperty familiaProperty() {
        return familia;
    }
    
    public void setFamilia(String familia) {
        this.familia.set(familia);
    }
    
    @Column(name = "FACCAO")
    public String getFaccao() {
        return faccao.get();
    }
    
    public StringProperty faccaoProperty() {
        return faccao;
    }
    
    public void setFaccao(String faccao) {
        this.faccao.set(faccao);
    }
    
    @Column(name = "STATUS_MAT")
    public String getStatusMat() {
        return statusMat.get();
    }
    
    public StringProperty statusMatProperty() {
        return statusMat;
    }
    
    public void setStatusMat(String statusMat) {
        this.statusMat.set(statusMat);
    }
    
    @Column(name = "COD_MAT")
    public String getCodMat() {
        return codMat.get();
    }
    
    public StringProperty codMatProperty() {
        return codMat;
    }
    
    public void setCodMat(String codMat) {
        this.codMat.set(codMat);
    }
    
    @Column(name = "MATERIAL")
    public String getMaterial() {
        return material.get();
    }
    
    public StringProperty materialProperty() {
        return material;
    }
    
    public void setMaterial(String material) {
        this.material.set(material);
    }
    
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }
    
    public StringProperty codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "QTDE_PCT")
    public int getQtdePct() {
        return qtdePct.get();
    }
    
    public IntegerProperty qtdePctProperty() {
        return qtdePct;
    }
    
    public void setQtdePct(int qtdePct) {
        this.qtdePct.set(qtdePct);
    }
    
    @Column(name = "TIPO_PROG")
    public String getTipoProg() {
        return tipoProg.get();
    }
    
    public StringProperty tipoProgProperty() {
        return tipoProg;
    }
    
    public void setTipoProg(String tipoProg) {
        this.tipoProg.set(tipoProg);
    }
    
    @Column(name = "TIPO_LANC")
    public String getTipoLanc() {
        return tipoLanc.get();
    }
    
    public StringProperty tipoLancProperty() {
        return tipoLanc;
    }
    
    public void setTipoLanc(String tipoLanc) {
        this.tipoLanc.set(tipoLanc);
    }
    
    @Column(name = "SETORES")
    public String getSetores() {
        return setores.get();
    }
    
    public StringProperty setoresProperty() {
        return setores;
    }
    
    public void setSetores(String setores) {
        this.setores.set(setores);
    }
    
    @Column(name = "QTDE_OPERS")
    public int getQtdeOpers() {
        return qtdeOpers.get();
    }
    
    public IntegerProperty qtdeOpersProperty() {
        return qtdeOpers;
    }
    
    public void setQtdeOpers(int qtdeOpers) {
        this.qtdeOpers.set(qtdeOpers);
    }
    
    @Column(name = "QTDE_COLABS")
    public int getQtdeColabs() {
        return qtdeColabs.get();
    }
    
    public IntegerProperty qtdeColabsProperty() {
        return qtdeColabs;
    }
    
    public void setQtdeColabs(int qtdeColabs) {
        this.qtdeColabs.set(qtdeColabs);
    }
    
    @Column(name = "QTDE_PROG")
    public int getQtdeProg() {
        return qtdeProg.get();
    }
    
    public IntegerProperty qtdeProgProperty() {
        return qtdeProg;
    }
    
    public void setQtdeProg(int qtdeProg) {
        this.qtdeProg.set(qtdeProg);
    }
    
    @Column(name = "QTDE_PROD")
    public int getQtdeProd() {
        return qtdeProd.get();
    }
    
    public IntegerProperty qtdeProdProperty() {
        return qtdeProd;
    }
    
    public void setQtdeProd(int qtdeProd) {
        this.qtdeProd.set(qtdeProd);
    }
    
    @Column(name = "QTDE_PEND")
    public int getQtdePend() {
        return qtdePend.get();
    }
    
    public IntegerProperty qtdePendProperty() {
        return qtdePend;
    }
    
    public void setQtdePend(int qtdePend) {
        this.qtdePend.set(qtdePend);
    }
    
    @Column(name = "DIAS_PROG")
    public BigDecimal getDiasProg() {
        return diasProg.get();
    }
    
    public ObjectProperty<BigDecimal> diasProgProperty() {
        return diasProg;
    }
    
    public void setDiasProg(BigDecimal diasProg) {
        this.diasProg.set(diasProg);
    }
    
    @Column(name = "DIAS_PROD")
    public BigDecimal getDiasProd() {
        return diasProd.get();
    }
    
    public ObjectProperty<BigDecimal> diasProdProperty() {
        return diasProd;
    }
    
    public void setDiasProd(BigDecimal diasProd) {
        this.diasProd.set(diasProd);
    }
    
    @Column(name = "DIAS_PEND")
    public BigDecimal getDiasPend() {
        return diasPend.get();
    }
    
    public ObjectProperty<BigDecimal> diasPendProperty() {
        return diasPend;
    }
    
    public void setDiasPend(BigDecimal diasPend) {
        this.diasPend.set(diasPend);
    }
    
    @Column(name = "PERC_PROD")
    public BigDecimal getPercProd() {
        return percProd.get();
    }
    
    public ObjectProperty<BigDecimal> percProdProperty() {
        return percProd;
    }
    
    public void setPercProd(BigDecimal percProd) {
        this.percProd.set(percProd);
    }
    
    @Column(name = "EFIC_OF")
    public BigDecimal getEficOf() {
        return eficOf.get();
    }
    
    public ObjectProperty<BigDecimal> eficOfProperty() {
        return eficOf;
    }
    
    public void setEficOf(BigDecimal eficOf) {
        this.eficOf.set(eficOf);
    }
}
