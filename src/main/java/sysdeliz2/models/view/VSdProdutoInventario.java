package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.Inventario.SdStatusItemInventario;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_PRODUTO_INVENTARIO")
public class VSdProdutoInventario implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final IntegerProperty estoque = new SimpleIntegerProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final ObjectProperty<SdStatusItemInventario> status = new SimpleObjectProperty<>();

    public VSdProdutoInventario() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }


    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "ESTOQUE")
    public Integer getEstoque() {
        return estoque.get();
    }

    public IntegerProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque.set(estoque);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @OneToOne
    @JoinColumn(name = "STATUS")
    public SdStatusItemInventario getStatus() {
        return status.get();
    }

    public ObjectProperty<SdStatusItemInventario> statusProperty() {
        return status;
    }

    public void setStatus(SdStatusItemInventario status) {
        this.status.set(status);
    }
}
