package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdProgramacaoPacote002;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Immutable
@Table(name = "V_SD_PROGRAMACAO_PENDENTE")
public class VSdProgramacaoPendente {
    
    private final StringProperty id = new SimpleStringProperty();
    private final StringProperty referencia = new SimpleStringProperty();
    private final IntegerProperty colaborador = new SimpleIntegerProperty();
    private final ObjectProperty<SdProgramacaoPacote002> programacao = new SimpleObjectProperty<>();
    private final IntegerProperty qtdePend = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tempoTotal = new SimpleObjectProperty<>();
    
    public VSdProgramacaoPendente() {
    }
    
    public VSdProgramacaoPendente(String referencia, Integer colaborador, SdProgramacaoPacote002 programacao) {
        this.referencia.set(referencia);
        this.colaborador.set(colaborador);
        this.programacao.set(programacao);
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getId() {
        return id.get();
    }
    
    public StringProperty idProperty() {
        return id;
    }
    
    public void setId(String id) {
        this.id.set(id);
    }
    
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }
    
    public StringProperty referenciaProperty() {
        return referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }
    
    @Column(name = "COLABORADOR")
    public int getColaborador() {
        return colaborador.get();
    }
    
    public IntegerProperty colaboradorProperty() {
        return colaborador;
    }
    
    public void setColaborador(int colaborador) {
        this.colaborador.set(colaborador);
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "PACOTE", referencedColumnName = "PACOTE"),
            @JoinColumn(name = "OPERACAO", referencedColumnName = "OPERACAO"),
            @JoinColumn(name = "ORDEM", referencedColumnName = "ORDEM"),
            @JoinColumn(name = "QUEBRA", referencedColumnName = "QUEBRA"),
            @JoinColumn(name = "SETOR_OP", referencedColumnName = "SETOR_OP"),
    })
    public SdProgramacaoPacote002 getProgramacao() {
        return programacao.get();
    }
    
    public ObjectProperty<SdProgramacaoPacote002> programacaoProperty() {
        return programacao;
    }
    
    public void setProgramacao(SdProgramacaoPacote002 programacao) {
        this.programacao.set(programacao);
    }
    
    @Column(name = "QTDE_PEND")
    public int getQtdePend() {
        return qtdePend.get();
    }
    
    public IntegerProperty qtdePendProperty() {
        return qtdePend;
    }
    
    public void setQtdePend(int qtdePend) {
        this.qtdePend.set(qtdePend);
    }
    
    @Column(name = "TEMPO_TOTAL")
    public BigDecimal getTempoTotal() {
        return tempoTotal.get();
    }
    
    public ObjectProperty<BigDecimal> tempoTotalProperty() {
        return tempoTotal;
    }
    
    public void setTempoTotal(BigDecimal tempoTotal) {
        this.tempoTotal.set(tempoTotal);
    }
}
