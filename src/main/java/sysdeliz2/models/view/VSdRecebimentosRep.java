package sysdeliz2.models.view;

import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_RECEBIMENTOS_REP")
public class VSdRecebimentosRep {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty duplicata = new SimpleStringProperty();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> com2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<LocalDate> dtrecebe = new SimpleObjectProperty<LocalDate>();
    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valor2 = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valorpago = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty fatura = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> pedemissao = new SimpleObjectProperty<LocalDate>();
    private final StringProperty tabpre = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> pgto = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> perdesc = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valortipo = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valormarca = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> valornota = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> basecomissao = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> perccomissao = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> comissao = new SimpleObjectProperty<BigDecimal>();
    
    
    public VSdRecebimentosRep() {
    }
    
    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "DUPLICATA")
    public String getDuplicata() {
        return duplicata.get();
    }
    
    public StringProperty duplicataProperty() {
        return duplicata;
    }
    
    public void setDuplicata(String duplicata) {
        this.duplicata.set(duplicata);
    }
    
    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }
    
    public StringProperty codcliProperty() {
        return codcli;
    }
    
    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "COM2")
    public BigDecimal getCom2() {
        return com2.get();
    }
    
    public ObjectProperty<BigDecimal> com2Property() {
        return com2;
    }
    
    public void setCom2(BigDecimal com2) {
        this.com2.set(com2);
    }
    
    @Column(name = "DT_RECEBE")
    public LocalDate getDtrecebe() {
        return dtrecebe.get();
    }
    
    public ObjectProperty<LocalDate> dtrecebeProperty() {
        return dtrecebe;
    }
    
    public void setDtrecebe(LocalDate dtrecebe) {
        this.dtrecebe.set(dtrecebe);
    }
    
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }
    
    public StringProperty codrepProperty() {
        return codrep;
    }
    
    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }
    
    @Column(name = "PEDIDO")
    public String getPedido() {
        return pedido.get();
    }
    
    public StringProperty pedidoProperty() {
        return pedido;
    }
    
    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }
    
    @Column(name = "VALOR2")
    public BigDecimal getValor2() {
        return valor2.get();
    }
    
    public ObjectProperty<BigDecimal> valor2Property() {
        return valor2;
    }
    
    public void setValor2(BigDecimal valor2) {
        this.valor2.set(valor2);
    }
    
    @Column(name = "VALOR_PAGO")
    public BigDecimal getValorpago() {
        return valorpago.get();
    }
    
    public ObjectProperty<BigDecimal> valorpagoProperty() {
        return valorpago;
    }
    
    public void setValorpago(BigDecimal valorpago) {
        this.valorpago.set(valorpago);
    }
    
    @Column(name = "FATURA")
    public String getFatura() {
        return fatura.get();
    }
    
    public StringProperty faturaProperty() {
        return fatura;
    }
    
    public void setFatura(String fatura) {
        this.fatura.set(fatura);
    }
    
    @Column(name = "PED_EMISSAO")
    public LocalDate getPedemissao() {
        return pedemissao.get();
    }
    
    public ObjectProperty<LocalDate> pedemissaoProperty() {
        return pedemissao;
    }
    
    public void setPedemissao(LocalDate pedemissao) {
        this.pedemissao.set(pedemissao);
    }
    
    @Column(name = "TAB_PRE")
    public String getTabpre() {
        return tabpre.get();
    }
    
    public StringProperty tabpreProperty() {
        return tabpre;
    }
    
    public void setTabpre(String tabpre) {
        this.tabpre.set(tabpre);
    }
    
    @Column(name = "PGTO")
    public BigDecimal getPgto() {
        return pgto.get();
    }
    
    public ObjectProperty<BigDecimal> pgtoProperty() {
        return pgto;
    }
    
    public void setPgto(BigDecimal pgto) {
        this.pgto.set(pgto);
    }
    
    @Column(name = "PER_DESC")
    public BigDecimal getPerdesc() {
        return perdesc.get();
    }
    
    public ObjectProperty<BigDecimal> perdescProperty() {
        return perdesc;
    }
    
    public void setPerdesc(BigDecimal perdesc) {
        this.perdesc.set(perdesc);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "VALOR_TIPO")
    public BigDecimal getValortipo() {
        return valortipo.get();
    }
    
    public ObjectProperty<BigDecimal> valortipoProperty() {
        return valortipo;
    }
    
    public void setValortipo(BigDecimal valortipo) {
        this.valortipo.set(valortipo);
    }
    
    @Column(name = "VALOR_MARCA")
    public BigDecimal getValormarca() {
        return valormarca.get();
    }
    
    public ObjectProperty<BigDecimal> valormarcaProperty() {
        return valormarca;
    }
    
    public void setValormarca(BigDecimal valormarca) {
        this.valormarca.set(valormarca);
    }
    
    @Column(name = "VALOR_NOTA")
    public BigDecimal getValornota() {
        return valornota.get();
    }
    
    public ObjectProperty<BigDecimal> valornotaProperty() {
        return valornota;
    }
    
    public void setValornota(BigDecimal valornota) {
        this.valornota.set(valornota);
    }
    
    @Column(name = "BASE_COMISSAO")
    public BigDecimal getBasecomissao() {
        return basecomissao.get();
    }
    
    public ObjectProperty<BigDecimal> basecomissaoProperty() {
        return basecomissao;
    }
    
    public void setBasecomissao(BigDecimal basecomissao) {
        this.basecomissao.set(basecomissao);
    }
    
    @Column(name = "PERC_COMISSAO")
    public BigDecimal getPerccomissao() {
        return perccomissao.get();
    }
    
    public ObjectProperty<BigDecimal> perccomissaoProperty() {
        return perccomissao;
    }
    
    public void setPerccomissao(BigDecimal perccomissao) {
        this.perccomissao.set(perccomissao);
    }
    
    @Column(name = "COMISSAO")
    public BigDecimal getComissao() {
        return comissao.get();
    }
    
    public ObjectProperty<BigDecimal> comissaoProperty() {
        return comissao;
    }
    
    public void setComissao(BigDecimal comissao) {
        this.comissao.set(comissao);
    }
    
    
}