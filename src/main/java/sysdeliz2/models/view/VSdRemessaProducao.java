package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_REMESSA_PRODUCAO")
public class VSdRemessaProducao {
    
    private final StringProperty idview = new SimpleStringProperty();
    private final StringProperty coletor = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> medqtdepc = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> medminutos = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> medpcseg = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<LocalDate>();
    private final IntegerProperty totqtdepc = new SimpleIntegerProperty(0);
    private final IntegerProperty totrem = new SimpleIntegerProperty(0);
    
    public VSdRemessaProducao() {
    }
    
    @Id
    @Column(name = "ID_VIEW")
    public String getIdview() {
        return idview.get();
    }
    
    public StringProperty idviewProperty() {
        return idview;
    }
    
    public void setIdview(String idview) {
        this.idview.set(idview);
    }
    
    @Column(name = "COLETOR")
    public String getColetor() {
        return coletor.get();
    }
    
    public StringProperty coletorProperty() {
        return coletor;
    }
    
    public void setColetor(String coletor) {
        this.coletor.set(coletor);
    }
    
    @Column(name = "NOME")
    public String getNome() {
        return nome.get();
    }
    
    public StringProperty nomeProperty() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    @Column(name = "MED_QTDEPC")
    public BigDecimal getMedqtdepc() {
        return medqtdepc.get();
    }
    
    public ObjectProperty<BigDecimal> medqtdepcProperty() {
        return medqtdepc;
    }
    
    public void setMedqtdepc(BigDecimal medqtdepc) {
        this.medqtdepc.set(medqtdepc);
    }
    
    @Column(name = "MED_MINUTOS")
    public BigDecimal getMedminutos() {
        return medminutos.get();
    }
    
    public ObjectProperty<BigDecimal> medminutosProperty() {
        return medminutos;
    }
    
    public void setMedminutos(BigDecimal medminutos) {
        this.medminutos.set(medminutos);
    }
    
    @Column(name = "MED_PC_SEG")
    public BigDecimal getMedpcseg() {
        return medpcseg.get();
    }
    
    public ObjectProperty<BigDecimal> medpcsegProperty() {
        return medpcseg;
    }
    
    public void setMedpcseg(BigDecimal medpcseg) {
        this.medpcseg.set(medpcseg);
    }
    
    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getData() {
        return data.get();
    }
    
    public ObjectProperty<LocalDate> dataProperty() {
        return data;
    }
    
    public void setData(LocalDate data) {
        this.data.set(data);
    }
    
    @Column(name = "TOT_QTDEPC")
    public Integer getTotqtdepc() {
        return totqtdepc.get();
    }
    
    public IntegerProperty totqtdepcProperty() {
        return totqtdepc;
    }
    
    public void setTotqtdepc(Integer totqtdepc) {
        this.totqtdepc.set(totqtdepc);
    }
    
    @Column(name = "TOT_REM")
    public Integer getTotrem() {
        return totrem.get();
    }
    
    public IntegerProperty totremProperty() {
        return totrem;
    }
    
    public void setTotrem(Integer totrem) {
        this.totrem.set(totrem);
    }
    
    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
    
}