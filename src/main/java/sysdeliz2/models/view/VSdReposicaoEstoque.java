package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.SdReposicaoEstoque;

import javax.persistence.*;

@Entity
@Table(name = "V_SD_REPOSICAO_ESTOQUE")
public class VSdReposicaoEstoque {
    
    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final ObjectProperty<SdReposicaoEstoque> codigo = new SimpleObjectProperty<>();
    private final StringProperty dep0005 = new SimpleStringProperty();
    private final StringProperty dep0001 = new SimpleStringProperty();
    
    public VSdReposicaoEstoque() {
    }
    
    
    @Id
    @Column(name = "IDJPA")
    public int getIdjpa() {
        return idjpa.get();
    }
    
    public IntegerProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(int idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @OneToOne
    @JoinColumn(name = "CODIGO")
    public SdReposicaoEstoque getCodigo() {
        return codigo.get();
    }
    
    public ObjectProperty<SdReposicaoEstoque> codigoProperty() {
        return codigo;
    }
    
    public void setCodigo(SdReposicaoEstoque codigo) {
        this.codigo.set(codigo);
    }
    
    @Column(name = "DEP_0005")
    public String getDep0005() {
        return dep0005.get();
    }
    
    public StringProperty dep0005Property() {
        return dep0005;
    }
    
    public void setDep0005(String dep0005) {
        this.dep0005.set(dep0005);
    }
    
    @Column(name = "DEP_0001")
    public String getDep0001() {
        return dep0001.get();
    }
    
    public StringProperty dep0001Property() {
        return dep0001;
    }
    
    public void setDep0001(String dep0001) {
        this.dep0001.set(dep0001);
    }
}