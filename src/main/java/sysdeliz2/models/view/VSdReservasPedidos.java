package sysdeliz2.models.view;

import javafx.beans.property.*;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ReservaPedido;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author lima.joao
 * @since 14/10/2019 09:43
 */
@Entity
@Immutable
@Table(name = "V_SD_RESERVAS_PEDIDOS")
@SuppressWarnings("unused")
public class VSdReservasPedidos {

    private final StringProperty idJpa = new SimpleStringProperty(this, "idJpa");
    private final StringProperty numero = new SimpleStringProperty(this, "numero");
    private final StringProperty reserva = new SimpleStringProperty(this, "reserva");
    private final ObjectProperty<BigDecimal> qtde = new SimpleObjectProperty<>(this, "qtde");
    private final StringProperty cor = new SimpleStringProperty(this, "cor");
    private final StringProperty tam = new SimpleStringProperty(this, "tam");
    private final StringProperty codigo = new SimpleStringProperty(this, "codigo");
    private final StringProperty deposito = new SimpleStringProperty(this, "deposito");
    private final StringProperty status = new SimpleStringProperty(this, "status");
    private final StringProperty usuarioLeitura = new SimpleStringProperty(this, "usuarioLeitura");

    private final StringProperty codcli = new SimpleStringProperty(this, "codcli");
    private final StringProperty nome = new SimpleStringProperty(this, "nome");

    private final IntegerProperty ordem = new SimpleIntegerProperty(this, "ordem");
    
    public VSdReservasPedidos(
            String numero,
            String reserva,
            BigDecimal qtde,
            String cor,
            String tam,
            String codigo,
            String deposito,
            String status,
            String usuarioLeitura,
            String codcli,
            String nome,
            Integer ordem
    ){
        this.idJpa.set("");
        this.numero.set(numero);
        this.reserva.set(reserva);
        this.qtde.set(qtde);
        this.cor.set(cor);
        this.tam.set(tam);
        this.codigo.set(codigo);
        this.deposito.set(deposito);
        this.status.set(status);
        this.usuarioLeitura.set(usuarioLeitura);
        this.codcli.set(codcli);
        this.nome.set(nome);
        this.ordem.set(ordem);
    }

    public VSdReservasPedidos(){}

    @Id
    @Column(name = "ID_JPA")
    public String getIdJpa() {
        return idJpa.get();
    }

    public StringProperty idJpaProperty() {
        return idJpa;
    }

    public void setIdJpa(String idJpa) {
        this.idJpa.set(idJpa);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "RESERVA")
    public String getReserva() {
        return reserva.get();
    }

    public StringProperty reservaProperty() {
        return reserva;
    }

    public void setReserva(String reserva) {
        this.reserva.set(reserva);
    }

    @Column(name = "QTDE")
    public BigDecimal getQtde() {
        if(this.qtde.get() == null){
            return BigDecimal.ZERO;
        } else {
            return qtde.get();
        }
    }

    public ObjectProperty<BigDecimal> qtdeProperty() {
        return qtde;
    }

    public void setQtde(BigDecimal qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }

    public StringProperty depositoProperty() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "USUARIO_LEITURA")
    public String getUsuarioLeitura() {
        return usuarioLeitura.get();
    }

    public StringProperty usuarioLeituraProperty() {
        return usuarioLeitura;
    }

    public void setUsuarioLeitura(String usuarioLeitura) {
        this.usuarioLeitura.set(usuarioLeitura);
    }

    @Column(name="CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name="NOME")
    public String getNome() {
        return nome.get();
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    @Column(name="ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    public ReservaPedido toReservaPedido(){
        return new ReservaPedido(
                this.getNumero(),
                this.getReserva(),
                "",
                this.getDeposito(),
                this.getStatus(),
                this.getQtde().intValue(),
                this.getNome(),
                "",
                "",
                "",
                "");
    }
}
