package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_SEMANA_COLECAO")
public class VSdSemanaColecao implements Serializable {
    private final StringProperty semana = new SimpleStringProperty();
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtInicio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtFim = new SimpleObjectProperty<>();

    public VSdSemanaColecao() {
    }

    @Id
    @Column(name = "SEMANA")
    public String getSemana() {
        return semana.get();
    }

    public StringProperty semanaProperty() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana.set(semana);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "COLECAO", referencedColumnName = "CODIGO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "DTINICIO")
    public LocalDate getDtInicio() {
        return dtInicio.get();
    }

    public ObjectProperty<LocalDate> dtInicioProperty() {
        return dtInicio;
    }

    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio.set(dtInicio);
    }

    @Column(name = "DTFIM")
    public LocalDate getDtFim() {
        return dtFim.get();
    }

    public ObjectProperty<LocalDate> dtFimProperty() {
        return dtFim;
    }

    public void setDtFim(LocalDate dtFim) {
        this.dtFim.set(dtFim);
    }
}
