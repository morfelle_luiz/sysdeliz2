package sysdeliz2.models.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;

@Entity
@Table(name = "V_SD_TELAS_USUARIO")
public class VSdTelasUsuario {

    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty codtela = new SimpleStringProperty();
    private final StringProperty nometela = new SimpleStringProperty();
    private final StringProperty icone = new SimpleStringProperty();
    private final StringProperty path = new SimpleStringProperty();
    private final StringProperty nomeusu = new SimpleStringProperty();
    private final BooleanProperty favorita = new SimpleBooleanProperty();
    private final BooleanProperty menu = new SimpleBooleanProperty();
    private final StringProperty codmenu = new SimpleStringProperty();
    private final BooleanProperty menuprincipal = new SimpleBooleanProperty();
    private final BooleanProperty acesso = new SimpleBooleanProperty();

    public VSdTelasUsuario() {
    }

    @Id
    @Column(name = "IDJPA")
    public String getIdjpa() {
        return idjpa.get();
    }

    public StringProperty idjpaProperty() {
        return idjpa;
    }

    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }

    @Column(name = "COD_TELA")
    public String getCodtela() {
        return codtela.get();
    }

    public StringProperty codtelaProperty() {
        return codtela;
    }

    public void setCodtela(String codtela) {
        this.codtela.set(codtela);
    }

    @Column(name = "NOME_TELA")
    public String getNometela() {
        return nometela.get();
    }

    public StringProperty nometelaProperty() {
        return nometela;
    }

    public void setNometela(String nometela) {
        this.nometela.set(nometela);
    }

    @Column(name = "ICONE")
    public String getIcone() {
        return icone.get();
    }

    public StringProperty iconeProperty() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone.set(icone);
    }

    @Column(name = "PATH")
    public String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }

    public void setPath(String path) {
        this.path.set(path);
    }

    @Column(name = "NOME_USU")
    public String getNomeusu() {
        return nomeusu.get();
    }

    public StringProperty nomeusuProperty() {
        return nomeusu;
    }

    public void setNomeusu(String nomeusu) {
        this.nomeusu.set(nomeusu);
    }

    @Column(name = "FAVORITA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFavorita() {
        return favorita.get();
    }

    public BooleanProperty favoritaProperty() {
        return favorita;
    }

    public void setFavorita(boolean favorita) {
        this.favorita.set(favorita);
    }

    @Column(name = "COD_MENU")
    public String getCodmenu() {
        return codmenu.get();
    }

    public StringProperty codmenuProperty() {
        return codmenu;
    }

    public void setCodmenu(String codmenu) {
        this.codmenu.set(codmenu);
    }

    @Column(name = "MENU_PRINCIPAL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isMenuprincipal() {
        return menuprincipal.get();
    }

    public BooleanProperty menuprincipalProperty() {
        return menuprincipal;
    }

    public void setMenuprincipal(boolean menuprincipal) {
        this.menuprincipal.set(menuprincipal);
    }

    @Column(name = "MENU")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isMenu() {
        return menu.get();
    }

    public BooleanProperty menuProperty() {
        return menu;
    }

    public void setMenu(boolean menu) {
        this.menu.set(menu);
    }

    @Column(name = "ACESSO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAcesso() {
        return acesso.get();
    }

    public BooleanProperty acessoProperty() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso.set(acesso);
    }
}
