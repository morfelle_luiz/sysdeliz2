package sysdeliz2.models.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Bordero;
import sysdeliz2.models.ti.CadConta;
import sysdeliz2.models.ti.Receber;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_TITULOS_CONTRATO")
public class VSdTitulosContrato implements Serializable {
    private final ObjectProperty<Receber> numero = new SimpleObjectProperty<>();
    private final ObjectProperty<Bordero> bordero = new SimpleObjectProperty<>();
    private final ObjectProperty<CadConta> contaBordero = new SimpleObjectProperty<>();
    private final ObjectProperty<CadConta> contaBaixa = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtEmissao = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtVencto = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtPagto = new SimpleObjectProperty<>();

    public VSdTitulosContrato() {
    }

    @Id
    @OneToOne
    @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO")
    public Receber getNumero() {
        return numero.get();
    }

    public ObjectProperty<Receber> numeroProperty() {
        return numero;
    }

    public void setNumero(Receber numero) {
        this.numero.set(numero);
    }

    @OneToOne
    @JoinColumn(name = "BORDERO", referencedColumnName = "NUMERO")
    public Bordero getBordero() {
        return bordero.get();
    }

    public ObjectProperty<Bordero> borderoProperty() {
        return bordero;
    }

    public void setBordero(Bordero bordero) {
        this.bordero.set(bordero);
    }

    @OneToOne
    @JoinColumn(name = "CONTA_BORDERO")
    public CadConta getContaBordero() {
        return contaBordero.get();
    }

    public ObjectProperty<CadConta> contaBorderoProperty() {
        return contaBordero;
    }

    public void setContaBordero(CadConta contaBordero) {
        this.contaBordero.set(contaBordero);
    }

    @OneToOne
    @JoinColumn(name = "CONTA_BAIXA")
    public CadConta getContaBaixa() {
        return contaBaixa.get();
    }

    public ObjectProperty<CadConta> contaBaixaProperty() {
        return contaBaixa;
    }

    public void setContaBaixa(CadConta contaBaixa) {
        this.contaBaixa.set(contaBaixa);
    }

    @Column(name = "DT_EMISSAO")
    public LocalDate getDtEmissao() {
        return dtEmissao.get();
    }

    public ObjectProperty<LocalDate> dtEmissaoProperty() {
        return dtEmissao;
    }

    public void setDtEmissao(LocalDate dtEmissao) {
        this.dtEmissao.set(dtEmissao);
    }

    @Column(name = "DT_VENCTO")
    public LocalDate getDtVencto() {
        return dtVencto.get();
    }

    public ObjectProperty<LocalDate> dtVenctoProperty() {
        return dtVencto;
    }

    public void setDtVencto(LocalDate dtVencto) {
        this.dtVencto.set(dtVencto);
    }

    @Column(name = "DT_PAGTO")
    public LocalDate getDtPagto() {
        return dtPagto.get();
    }

    public ObjectProperty<LocalDate> dtPagtoProperty() {
        return dtPagto;
    }

    public void setDtPagto(LocalDate dtPagto) {
        this.dtPagto.set(dtPagto);
    }
}
