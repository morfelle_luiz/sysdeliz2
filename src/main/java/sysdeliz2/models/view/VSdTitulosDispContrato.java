package sysdeliz2.models.view;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Receber;
import sysdeliz2.utils.converters.BigDecimalAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_TITULOS_DISP_CONTRATO")
public class VSdTitulosDispContrato implements Serializable {
    private final ObjectProperty<Receber> numero = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtvencto = new SimpleObjectProperty<LocalDate>();
    private final StringProperty codcli = new SimpleStringProperty();
    private final StringProperty obs = new SimpleStringProperty();
    private final StringProperty sitcli = new SimpleStringProperty();
    private final StringProperty situacao = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final StringProperty dividaserasa = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> mediadiasatraso = new SimpleObjectProperty<>();

    public VSdTitulosDispContrato() {
    }


    @Id
    @OneToOne
    @JoinColumn(name = "NUMERO")
    public Receber getNumero() {
        return numero.get();
    }

    public ObjectProperty<Receber> numeroProperty() {
        return numero;
    }

    public void setNumero(Receber numero) {
        this.numero.set(numero);
    }

    @Column(name = "DT_EMISSAO")
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "DT_VENCTO")
    public LocalDate getDtvencto() {
        return dtvencto.get();
    }

    public ObjectProperty<LocalDate> dtvenctoProperty() {
        return dtvencto;
    }

    public void setDtvencto(LocalDate dtvencto) {
        this.dtvencto.set(dtvencto);
    }

    @Column(name = "CODCLI")
    public String getCodcli() {
        return codcli.get();
    }

    public StringProperty codcliProperty() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli.set(codcli);
    }

    @Column(name = "OBS")
    public String getObs() {
        return obs.get();
    }

    public StringProperty obsProperty() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs.set(obs);
    }

    @Column(name = "SIT_CLI")
    public String getSitcli() {
        return sitcli.get();
    }

    public StringProperty sitcliProperty() {
        return sitcli;
    }

    public void setSitcli(String sitcli) {
        this.sitcli.set(sitcli);
    }

    @Column(name = "SITUACAO")
    public String getSituacao() {
        return situacao.get();
    }

    public StringProperty situacaoProperty() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao.set(situacao);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }



    @Column(name = "DIVIDA_SERASA")
    public String getDividaserasa() {
        return dividaserasa.get();
    }

    public StringProperty dividaserasaProperty() {
        return dividaserasa;
    }

    public void setDividaserasa(String dividaserasa) {
        this.dividaserasa.set(dividaserasa);
    }

    @Column(name = "MEDIA_DIAS_ATRASO")
    public BigDecimal getMediadiasatraso() {
        return mediadiasatraso.get();
    }

    public ObjectProperty<BigDecimal> mediadiasatrasoProperty() {
        return mediadiasatraso;
    }

    public void setMediadiasatraso(BigDecimal mediadiasatraso) {
        this.mediadiasatraso.set(mediadiasatraso);
    }
}
