package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_ULT_COLECAO_MARCA")
public class VSdUltColecaoMarca implements Serializable {
    private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final IntegerProperty indice = new SimpleIntegerProperty();

    public VSdUltColecaoMarca() {
    }

    public VSdUltColecaoMarca(Colecao colecao, Marca marca) {
        this.colecao.set(colecao);
        this.marca.set(marca);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "COLECAO")
    public Colecao getColecao() {
        return colecao.get();
    }

    public ObjectProperty<Colecao> colecaoProperty() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao.set(colecao);
    }

    @Id
    @OneToOne
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }

    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }

    @Column(name = "INDICE")
    public int getIndice() {
        return indice.get();
    }

    public IntegerProperty indiceProperty() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice.set(indice);
    }
}
