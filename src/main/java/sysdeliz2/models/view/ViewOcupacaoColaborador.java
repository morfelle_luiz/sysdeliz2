/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.models.view;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdProgramacaoPacote001;
import sysdeliz2.models.sysdeliz.SdColaborador;

/**
 *
 * @author cristiano.diego
 */
public class ViewOcupacaoColaborador {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty tempoTurno = new SimpleStringProperty();
    private final StringProperty tempoLivre = new SimpleStringProperty();
    private final StringProperty tempoOcupado = new SimpleStringProperty();
    private final StringProperty eficienciaOperacao = new SimpleStringProperty();
    private final StringProperty polivalenciaOperacao = new SimpleStringProperty();
    private final StringProperty statusOcupacao = new SimpleStringProperty();

    private final ListProperty<SdProgramacaoPacote001> ocupacao = new SimpleListProperty<SdProgramacaoPacote001>();
    private final ObjectProperty<SdColaborador> colaborador = new SimpleObjectProperty<SdColaborador>();

    public ViewOcupacaoColaborador() {
    }

    public ViewOcupacaoColaborador(String codigo, String nome, String tempoLivre, String tempoTurno,
            String tempoOcupado, String eficienciaOperacao, String polivalenciaOperacao) {
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.tempoTurno.set(tempoTurno);
        this.tempoLivre.set(tempoLivre);
        this.tempoOcupado.set(tempoOcupado);
        this.eficienciaOperacao.set(eficienciaOperacao);
        this.polivalenciaOperacao.set(polivalenciaOperacao);
    }
    
    public ViewOcupacaoColaborador(String codigo, String nome, String tempoLivre, String tempoTurno,
                                   String tempoOcupado, String eficienciaOperacao, String polivalenciaOperacao,
                                   SdColaborador colaborador, String statusOcupacao) {
        this.codigo.set(codigo);
        this.nome.set(nome);
        this.tempoTurno.set(tempoTurno);
        this.tempoLivre.set(tempoLivre);
        this.tempoOcupado.set(tempoOcupado);
        this.eficienciaOperacao.set(eficienciaOperacao);
        this.polivalenciaOperacao.set(polivalenciaOperacao);
        this.colaborador.set(colaborador);
        this.statusOcupacao.set(statusOcupacao);
    }

    public final String getCodigo() {
        return codigo.get();
    }

    public final void setCodigo(String value) {
        codigo.set(value);
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getTempoLivre() {
        return tempoLivre.get();
    }

    public final void setTempoLivre(String value) {
        tempoLivre.set(value);
    }

    public StringProperty tempoLivreProperty() {
        return tempoLivre;
    }

    public final String getTempoOcupado() {
        return tempoOcupado.get();
    }

    public final void setTempoOcupado(String value) {
        tempoOcupado.set(value);
    }

    public StringProperty tempoOcupadoProperty() {
        return tempoOcupado;
    }

    public final String getEficienciaOperacao() {
        return eficienciaOperacao.get();
    }

    public final void setEficienciaOperacao(String value) {
        eficienciaOperacao.set(value);
    }

    public StringProperty eficienciaOperacaoProperty() {
        return eficienciaOperacao;
    }

    public final String getPolivalenciaOperacao() {
        return polivalenciaOperacao.get();
    }

    public final void setPolivalenciaOperacao(String value) {
        polivalenciaOperacao.set(value);
    }

    public StringProperty polivalenciaOperacaoProperty() {
        return polivalenciaOperacao;
    }

    public final ObservableList<SdProgramacaoPacote001> getOcupacao() {
        return ocupacao.get();
    }

    public final void setOcupacao(ObservableList<SdProgramacaoPacote001> value) {
        ocupacao.set(value);
    }

    public ListProperty<SdProgramacaoPacote001> ocupacaoProperty() {
        return ocupacao;
    }

    public final String getTempoTurno() {
        return tempoTurno.get();
    }

    public final void setTempoTurno(String value) {
        tempoTurno.set(value);
    }

    public StringProperty tempoTurnoProperty() {
        return tempoTurno;
    }

    public final String getStatusOcupacao() {
        return statusOcupacao.get();
    }

    public final void setStatusOcupacao(String value) {
        statusOcupacao.set(value);
    }

    public StringProperty statusOcupacaoProperty() {
        return statusOcupacao;
    }

    public final SdColaborador getColaborador() {
        return colaborador.get();
    }

    public final void setColaborador(SdColaborador value) {
        colaborador.set(value);
    }

    public ObjectProperty<SdColaborador> colaboradorProperty() {
        return colaborador;
    }

}
