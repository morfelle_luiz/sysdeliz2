package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_DADOS_PRODUTO_LOCAL")
public class VsdDadosProdutoLocal implements Serializable {

    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty entrega = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();

    public VsdDadosProdutoLocal() {
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao.get();
    }

    public StringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Id
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Column(name = "ENTREGA")
    public String getEntrega() {
        return entrega.get();
    }

    public StringProperty entregaProperty() {
        return entrega;
    }

    public void setEntrega(String entrega) {
        this.entrega.set(entrega);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

}
