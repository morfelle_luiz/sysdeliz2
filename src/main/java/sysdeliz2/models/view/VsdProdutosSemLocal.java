package sysdeliz2.models.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_PRODUTOS_SEM_LOCAL")
public class VsdProdutosSemLocal implements Serializable {

    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty tipolinha = new SimpleStringProperty();
    private final StringProperty genero = new SimpleStringProperty();
    private final StringProperty codlinha = new SimpleStringProperty();
    private final StringProperty familia = new SimpleStringProperty();
    private final IntegerProperty skus = new SimpleIntegerProperty();

    public VsdProdutosSemLocal() {
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "TIPO_LINHA")
    public String getTipolinha() {
        return tipolinha.get();
    }

    public StringProperty tipolinhaProperty() {
        return tipolinha;
    }

    public void setTipolinha(String tipolinha) {
        this.tipolinha.set(tipolinha);
    }

    @Column(name = "GENERO")
    public String getGenero() {
        return genero.get();
    }

    public StringProperty generoProperty() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    @Column(name = "COD_LINHA")
    public String getCodlinha() {
        return codlinha.get();
    }

    public StringProperty codlinhaProperty() {
        return codlinha;
    }

    public void setCodlinha(String codlinha) {
        this.codlinha.set(codlinha);
    }

    @Column(name = "FAMILIA")
    public String getFamilia() {
        return familia.get();
    }

    public StringProperty familiaProperty() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia.set(familia);
    }

    @Column(name = "SKUS")
    public Integer getSkus() {
        return skus.get();
    }

    public IntegerProperty skusProperty() {
        return skus;
    }

    public void setSkus(Integer skus) {
        this.skus.set(skus);
    }

}
