package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_B2B_PRODUTO_ATRIBUTOS")
public class VB2bProdutoAtributos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "REFERENCIA")
    @JsonIgnore
    private String referencia;
    @Id
    @Column(name = "CODATRIB")
    private String codatrib;
    @Column(name = "TIPOATRIB")
    private String tipoatrib;
    @Column(name = "ATRIBUTO")
    private String atributo;
    @Column(name = "DESCATRIB")
    private String descatrib;
    @Column(name = "ORDEMATRIB")
    private Integer ordematrib;

    public VB2bProdutoAtributos() {
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCodatrib() {
        return codatrib;
    }

    public void setCodatrib(String codatrib) {
        this.codatrib = codatrib;
    }

    public String getTipoatrib() {
        return tipoatrib;
    }

    public void setTipoatrib(String tipoatrib) {
        this.tipoatrib = tipoatrib;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo = atributo;
    }

    public String getDescatrib() {
        return descatrib;
    }

    public void setDescatrib(String descatrib) {
        this.descatrib = descatrib;
    }

    public Integer getOrdematrib() {
        return ordematrib;
    }

    public void setOrdematrib(Integer ordematrib) {
        this.ordematrib = ordematrib;
    }
}
