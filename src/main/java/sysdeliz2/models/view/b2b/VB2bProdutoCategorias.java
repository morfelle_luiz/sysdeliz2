package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_B2B_PRODUTO_CATEGORIAS")
public class VB2bProdutoCategorias implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "REFERENCIA")
    @JsonIgnore
    private String referencia;
    @Id
    @Column(name = "CATEG")
    private String categ;
    @Column(name = "DESCCATEG")
    private String descCateg;
    @Column(name = "ORDEMCATEG")
    private Integer ordemCateg;
    @Id
    @Column(name = "SECAO")
    private String secao;
    @Column(name = "DESCSECAO")
    private String descSecao;
    @Column(name = "ORDEMSECAO")
    private Integer ordemSecao;
    @Column(name = "EXIBIRMENU")
    private String exibirMenu;
    
    public VB2bProdutoCategorias() {
    }
    
    public void setReferencia(String REFERENCIA) {
        this.referencia = REFERENCIA;
    }
    
    public String getReferencia() {
        return referencia;
    }
    
    public void setCateg(String CATEG) {
        this.categ = CATEG;
    }
    
    public String getCateg() {
        return categ;
    }
    
    public void setDescCateg(String DESCCATEG) {
        this.descCateg = DESCCATEG;
    }
    
    public String getDescCateg() {
        return descCateg;
    }
    
    public void setOrdemCateg(Integer ORDEMCATEG) {
        this.ordemCateg = ORDEMCATEG;
    }
    
    public Integer getOrdemCateg() {
        return ordemCateg;
    }
    
    public void setSecao(String SECAO) {
        this.secao = SECAO;
    }
    
    public String getSecao() {
        return secao;
    }
    
    public void setDescSecao(String DESCSECAO) {
        this.descSecao = DESCSECAO;
    }
    
    public String getDescSecao() {
        return descSecao;
    }
    
    public void setOrdemSecao(Integer ORDEMSECAO) {
        this.ordemSecao = ORDEMSECAO;
    }
    
    public Integer getOrdemSecao() {
        return ordemSecao;
    }
    
    public void setExibirMenu(String EXIBIRMENU) {
        this.exibirMenu = EXIBIRMENU;
    }
    
    public String getExibirMenu() {
        return exibirMenu;
    }
}
