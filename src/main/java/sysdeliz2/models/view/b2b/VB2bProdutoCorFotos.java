package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_B2B_PRODUTO_COR_FOTOS")
public class VB2bProdutoCorFotos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "REFERENCIA")
    @JsonIgnore
    @Expose
    private String referencia;
    @Id
    @Column(name = "CODCOR")
    @JsonIgnore
    @Expose
    private String codCor;
    @Id
    @Column(name = "ORDEM")
    private Integer ordem;
    @Column(name = "IMAGEM")
    private String imagem;
    
    public VB2bProdutoCorFotos() {
    }
    
    public void setReferencia(String REFERENCIA) {
        this.referencia = REFERENCIA;
    }
    
    public String getReferencia() {
        return referencia;
    }
    
    public void setCodCor(String CODCOR) {
        this.codCor = CODCOR;
    }
    
    public String getCodCor() {
        return codCor;
    }
    
    public void setOrdem(Integer ORDEM) {
        this.ordem = ORDEM;
    }
    
    public Integer getOrdem() {
        return ordem;
    }
    
    public void setImagem(String IMAGEM) {
        this.imagem = IMAGEM;
    }
    
    public String getImagem() {
        return imagem;
    }
}
