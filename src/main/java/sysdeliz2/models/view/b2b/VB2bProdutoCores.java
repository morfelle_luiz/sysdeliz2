package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "V_B2B_PRODUTO_CORES")
public class VB2bProdutoCores implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "REFERENCIA")
    @JsonIgnore
    private String referencia;
    @Id
    @Column(name = "COR")
    private String cor;
    @Column(name = "DESCCOR")
    private String descCor;
    @Column(name = "ORDEMCOR")
    private Integer ordemCor;
    @Column(name = "IMAGECOR")
    private String imageCor;
    @Column(name = "CORMOST")
    @Convert(converter = BooleanAttributeConverter.class)
    @JsonIgnore
    private Boolean corMost;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "REFERENCIA"),
            @JoinColumn(name = "CODCOR")
    })
    private List<VB2bProdutoCorFotos> fotos;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "CODIGO"),
            @JoinColumn(name = "COR")
    })
    @OrderBy("ordemTam")
    private List<VB2bProdutoTamanhos> skus;
    
    public VB2bProdutoCores() {
    }
    
    public void setReferencia(String REFERENCIA) {
        this.referencia = REFERENCIA;
    }
    
    public String getReferencia() {
        return referencia;
    }
    
    public void setCor(String COR) {
        this.cor = COR;
    }
    
    public String getCor() {
        return cor;
    }
    
    public void setDescCor(String DESCCOR) {
        this.descCor = DESCCOR;
    }
    
    public String getDescCor() {
        return descCor;
    }
    
    public void setOrdemCor(Integer ORDEMCOR) {
        this.ordemCor = ORDEMCOR;
    }
    
    public Integer getOrdemCor() {
        return ordemCor;
    }
    
    public void setImageCor(String IMAGECOR) {
        this.imageCor = IMAGECOR;
    }
    
    public String getImageCor() {
        return imageCor;
    }
    
    public List<VB2bProdutoCorFotos> getFotos() {
        return fotos;
    }

    public void setFotos(List<VB2bProdutoCorFotos> fotos) {
        this.fotos = fotos;
    }
    
    public List<VB2bProdutoTamanhos> getSkus() {
        return skus;
    }

    public void setSkus(List<VB2bProdutoTamanhos> skus) {
        this.skus = skus;
    }

    public Boolean isCorMost() {
        return corMost;
    }

    public void setCorMost(Boolean corMost) {
        this.corMost = corMost;
    }
}
