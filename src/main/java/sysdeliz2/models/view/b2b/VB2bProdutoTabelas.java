package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "V_B2B_PRODUTO_TABELAS")
public class VB2bProdutoTabelas implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "REFERENCIA")
    @JsonIgnore
    private String referencia;
    @Id
    @Column(name = "CODIGO")
    private String codigo;
    @Column(name = "PRECO")
    private BigDecimal preco;
    
    public VB2bProdutoTabelas() {
    }
    
    public String getReferencia() {
        return referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    
    public String getCodigo() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }
}
