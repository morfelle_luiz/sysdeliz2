package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_B2B_PRODUTO_TAMANHOS")
public class VB2bProdutoTamanhos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "CODIGO")
    @JsonIgnore
    @Expose
    private String codigo;
    @Id
    @Column(name = "COR")
    @JsonIgnore
    @Expose
    private String cor;
    @Id
    @Column(name = "TAM")
    private String tam;
    @Column(name = "DESCTAM")
    private String descTam;
    @Column(name = "SKU")
    private String sku;
    @Column(name = "ORDEMTAM")
    private Integer ordemTam;
    
    public VB2bProdutoTamanhos() {
    }
    
    public void setCodigo(String CODIGO) {
        this.codigo = CODIGO;
    }
    
    public String getCodigo() {
        return codigo;
    }
    
    public void setCor(String COR) {
        this.cor = COR;
    }
    
    public String getCor() {
        return cor;
    }
    
    public void setTam(String TAM) {
        this.tam = TAM;
    }
    
    public String getTam() {
        return tam;
    }
    
    public void setDescTam(String DESCTAM) {
        this.descTam = DESCTAM;
    }
    
    public String getDescTam() {
        return descTam;
    }
    
    public void setSku(String SKU) {
        this.sku = SKU;
    }
    
    public String getSku() {
        return sku;
    }
    
    public void setOrdemTam(Integer ORDEMTAM) {
        this.ordemTam = ORDEMTAM;
    }
    
    public Integer getOrdemTam() {
        return ordemTam;
    }
}
