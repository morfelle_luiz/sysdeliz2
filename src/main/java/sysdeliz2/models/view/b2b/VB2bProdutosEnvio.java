package sysdeliz2.models.view.b2b;

import com.fasterxml.jackson.annotation.JsonIgnore;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "V_B2B_PRODUTOS_ENVIO")
@Cacheable(value = false)
public class VB2bProdutosEnvio extends BasicModel implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "REFERENCIA")
    private String referencia;//
    @Column(name = "REFNOME")
    private String pronome;//
    @Column(name = "PROCARAC")
    private String procarac;
    @Column(name = "PROESPTEC")
    private String proesptec;
    @Column(name = "PRODESC")
    private String prodesc;
    @Column(name = "MARCA")
    private String marca;//
    @Column(name = "DESCMARCA")
    private String descMarca;
    @Column(name = "PROSITUA")
    private String prositua;//
    @Column(name = "PROVIDEO")
    @Lob
    private String provideo;
    @Column(name = "REFUM")
    private String proum;
    @Column(name = "REFPESO")
    private BigDecimal propeso;
    @Column(name = "IMAGEM")
    private String imagem;
    @Column(name = "IMAGEMHOVER")
    private String imagemHover;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "REFERENCIA")
    private List<VB2bProdutoTabelas> tabelas;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "REFERENCIA")
    @OrderBy(value = "ORDEMATRIB")
    private List<VB2bProdutoAtributos> atributos;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "REFERENCIA")
    private List<VB2bProdutoCategorias> categorias;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "REFERENCIA")
    private List<VB2bProdutoCores> referencias;
    @Column(name = "STATUS")
    @JsonIgnore
    private String status;//
    @OneToOne
    @JoinColumn(name = "CODIGO")
    @JsonIgnore
    private Produto codigo;//
    
    public VB2bProdutosEnvio() {
    }
    
    public void setReferencia(String REFERENCIA) {
        this.referencia = REFERENCIA;
    }
    
    public String getReferencia() {
        return referencia;
    }
    
    public void setPronome(String REFNOME) {
        this.pronome = REFNOME;
    }
    
    public String getPronome() {
        return pronome;
    }
    
    public void setProcarac(String PROCARAC) {
        this.procarac = PROCARAC;
    }
    
    public String getProcarac() {
        return procarac;
    }
    
    public void setProesptec(String PROESPTEC) {
        this.proesptec = PROESPTEC;
    }
    
    public String getProesptec() {
        return proesptec;
    }
    
    public void setProdesc(String PRODESC) {
        this.prodesc = PRODESC;
    }
    
    public String getProdesc() {
        return prodesc;
    }
    
    public void setMarca(String MARCA) {
        this.marca = MARCA;
    }
    
    public String getMarca() {
        return marca;
    }
    
    public void setDescMarca(String DESCMARCA) {
        this.descMarca = DESCMARCA;
    }
    
    public String getDescMarca() {
        return descMarca;
    }
    
    public void setPrositua(String PROSITUA) {
        this.prositua = PROSITUA;
    }
    
    public String getPrositua() {
        return prositua;
    }
    
    public void setProvideo(String PROVIDEO) {
        this.provideo = PROVIDEO;
    }
    
    public String getProvideo() {
        return provideo;
    }
    
    public void setProum(String REFUM) {
        this.proum = REFUM;
    }
    
    public String getProum() {
        return proum;
    }
    
    public void setPropeso(BigDecimal REFPESO) {
        this.propeso = REFPESO;
    }
    
    public BigDecimal getPropeso() {
        return propeso;
    }
    
    public void setImagem(String IMAGEM) {
        this.imagem = IMAGEM;
    }
    
    public String getImagem() {
        return imagem;
    }
    
    public void setImagemHover(String IMAGEMHOVER) {
        this.imagemHover = IMAGEMHOVER;
    }
    
    public String getImagemHover() {
        return imagemHover;
    }
    
    public List<VB2bProdutoCategorias> getCategorias() {
        return categorias;
    }
    
    public void setCategorias(List<VB2bProdutoCategorias> categorias) {
        this.categorias = categorias;
    }
    
    public List<VB2bProdutoCores> getReferencias() {
        return referencias;
    }

    public void setReferencias(List<VB2bProdutoCores> referencias) {
        this.referencias = referencias;
    }
    
    public List<VB2bProdutoTabelas> getTabelas() {
        return tabelas;
    }
    
    public void setTabelas(List<VB2bProdutoTabelas> tabelas) {
        this.tabelas = tabelas;
    }

    public List<VB2bProdutoAtributos> getAtributos() {
        return atributos;
    }

    public void setAtributos(List<VB2bProdutoAtributos> atributos) {
        this.atributos = atributos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Produto getCodigo() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo = codigo;
    }
}
