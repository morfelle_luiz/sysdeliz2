package sysdeliz2.models.view.b2b;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_IMAGENS_PRODUTO")
public class VSdImagensProduto implements Serializable {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty arquivo = new SimpleStringProperty();

    public VSdImagensProduto() {
    }

    @Id
    @Column(name = "ID")
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @Column(name = "ARQUIVO")
    public String getArquivo() {
        return arquivo.get();
    }

    public StringProperty arquivoProperty() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo.set(arquivo);
    }
}