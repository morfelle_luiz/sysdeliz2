package sysdeliz2.models.view.b2c;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_B2C_PRODUTO_COR_FOTOS")
public class VB2cProdutoCorFotos implements Serializable {
    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty codCor = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty imagem = new SimpleStringProperty();

    public VB2cProdutoCorFotos() {
    }

    @Id
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }

    @Id
    @Column(name = "CODCOR")
    public String getCodCor() {
        return codCor.get();
    }

    public StringProperty codCorProperty() {
        return codCor;
    }

    public void setCodCor(String codCor) {
        this.codCor.set(codCor);
    }

    @Id
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "IMAGEM")
    public String getImagem() {
        return imagem.get();
    }

    public StringProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem.set(imagem);
    }
}
