package sysdeliz2.models.view.b2c;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "V_B2C_PRODUTO_CORES")
public class VB2cProdutoCores implements Serializable {

    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty descCor = new SimpleStringProperty();
    private final IntegerProperty ordemCor = new SimpleIntegerProperty();
    private final StringProperty imageCor = new SimpleStringProperty();
    private final BooleanProperty corMost = new SimpleBooleanProperty();
    private final BooleanProperty imagemSite = new SimpleBooleanProperty(false);
    private final IntegerProperty qtdeFotosSite = new SimpleIntegerProperty(0);

    private List<VB2cProdutoCorFotos> fotos = new ArrayList<>();

    public VB2cProdutoCores() {
    }

    @Id
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }

    @Id
    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "DESCCOR")
    public String getDescCor() {
        return descCor.get();
    }

    public StringProperty descCorProperty() {
        return descCor;
    }

    public void setDescCor(String descCor) {
        this.descCor.set(descCor);
    }

    @Column(name = "ORDEMCOR")
    public int getOrdemCor() {
        return ordemCor.get();
    }

    public IntegerProperty ordemCorProperty() {
        return ordemCor;
    }

    public void setOrdemCor(int ordemCor) {
        this.ordemCor.set(ordemCor);
    }

    @Column(name = "IMAGECOR")
    public String getImageCor() {
        return imageCor.get();
    }

    public StringProperty imageCorProperty() {
        return imageCor;
    }

    public void setImageCor(String imageCor) {
        this.imageCor.set(imageCor);
    }

    @Column(name = "CORMOST")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isCorMost() {
        return corMost.get();
    }

    public BooleanProperty corMostProperty() {
        return corMost;
    }

    public void setCorMost(boolean corMost) {
        this.corMost.set(corMost);
    }

    @OneToMany
    @JoinColumns({
            @JoinColumn(name = "REFERENCIA", referencedColumnName = "REFERENCIA"),
            @JoinColumn(name = "CODCOR", referencedColumnName = "COR")
    })
    public List<VB2cProdutoCorFotos> getFotos() {
        return fotos;
    }

    public void setFotos(List<VB2cProdutoCorFotos> fotos) {
        this.fotos = fotos;
    }

    @Transient
    public boolean isImagemSite() {
        return imagemSite.get();
    }

    public BooleanProperty imagemSiteProperty() {
        return imagemSite;
    }

    public void setImagemSite(boolean imagemSite) {
        this.imagemSite.set(imagemSite);
    }

    @Transient
    public int getQtdeFotosSite() {
        return qtdeFotosSite.get();
    }

    public IntegerProperty qtdeFotosSiteProperty() {
        return qtdeFotosSite;
    }

    public void setQtdeFotosSite(int qtdeFotosSite) {
        this.qtdeFotosSite.set(qtdeFotosSite);
    }
}
