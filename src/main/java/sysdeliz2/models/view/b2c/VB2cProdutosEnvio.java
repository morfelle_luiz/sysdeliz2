package sysdeliz2.models.view.b2c;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.utils.apis.b2cEcom.models.StatusEnvioProduto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "V_B2C_PRODUTOS_ENVIO")
public class VB2cProdutosEnvio extends BasicModel {

    private final StringProperty referencia = new SimpleStringProperty();
    private final StringProperty refnome = new SimpleStringProperty();
    private final StringProperty procarac = new SimpleStringProperty();
    private final StringProperty proesptec = new SimpleStringProperty();
    private final StringProperty prodesc = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty descmarca = new SimpleStringProperty();
    private final StringProperty desccolecao = new SimpleStringProperty();
    private final StringProperty colecao = new SimpleStringProperty();
    private final StringProperty imagem = new SimpleStringProperty();
    private final StringProperty imagemhover = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final BooleanProperty ativo = new SimpleBooleanProperty();
    private final StringProperty regiao = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty modelagem = new SimpleStringProperty();
    private final StringProperty categoria = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<>();
    private final ObjectProperty<SdProduto> sdProduto = new SimpleObjectProperty<>();
    private Map<String, Object> customAttributes = new HashMap<>();
    private List<VB2cProdutoCores> cores = new ArrayList<>();
    private StatusEnvioProduto statusEnvioProduto = new StatusEnvioProduto();

    public VB2cProdutosEnvio() {

    }

    @Id
    @Column(name = "REFERENCIA")
    public String getReferencia() {
        return referencia.get();
    }

    public StringProperty referenciaProperty() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }

    @Column(name = "REFNOME")
    public String getRefnome() {
        return refnome.get();
    }

    public StringProperty refnomeProperty() {
        return refnome;
    }

    public void setRefnome(String refnome) {
        this.refnome.set(refnome);
    }

    @Column(name = "PROCARAC")
    public String getProcarac() {
        return procarac.get();
    }

    public StringProperty procaracProperty() {
        return procarac;
    }

    public void setProcarac(String procarac) {
        this.procarac.set(procarac);
    }

    @Column(name = "PROESPTEC")
    public String getProesptec() {
        return proesptec.get();
    }

    public StringProperty proesptecProperty() {
        return proesptec;
    }

    public void setProesptec(String proesptec) {
        this.proesptec.set(proesptec);
    }

    @Column(name = "PRODESC")
    public String getProdesc() {
        return prodesc.get();
    }

    public StringProperty prodescProperty() {
        return prodesc;
    }

    public void setProdesc(String prodesc) {
        this.prodesc.set(prodesc);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "DESCMARCA")
    public String getDescmarca() {
        return descmarca.get();
    }

    public StringProperty descmarcaProperty() {
        return descmarca;
    }

    public void setDescmarca(String descmarca) {
        this.descmarca.set(descmarca);
    }

    @Column(name = "DESCCOLECAO")
    public String getDesccolecao() {
        return desccolecao.get();
    }

    public StringProperty desccolecaoProperty() {
        return desccolecao;
    }

    public void setDesccolecao(String desccolecao) {
        this.desccolecao.set(desccolecao);
    }

    @Column(name = "COLECAO")
    public String getColecao() {
        return colecao.get();
    }

    public StringProperty colecaoProperty() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao.set(colecao);
    }

    @Column(name = "IMAGEM")
    public String getImagem() {
        return imagem.get();
    }

    public StringProperty imagemProperty() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem.set(imagem);
    }

    @Column(name = "IMAGEMHOVER")
    public String getImagemhover() {
        return imagemhover.get();
    }

    public StringProperty imagemhoverProperty() {
        return imagemhover;
    }

    public void setImagemhover(String imagemhover) {
        this.imagemhover.set(imagemhover);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "ATIVO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtivo() {
        return ativo.get();
    }

    public BooleanProperty ativoProperty() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo.set(ativo);
    }

    @Column(name = "REGIAO")
    public String getRegiao() {
        return regiao.get();
    }

    public StringProperty regiaoProperty() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao.set(regiao);
    }

    @Column(name = "PRECO")
    public BigDecimal getPreco() {
        return preco.get();
    }

    public ObjectProperty<BigDecimal> precoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco.set(preco);
    }

    @Column(name = "PESO")
    public BigDecimal getPeso() {
        return peso.get();
    }

    public ObjectProperty<BigDecimal> pesoProperty() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }

    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    @Column(name = "MODELAGEM")
    public String getModelagem() {
        return modelagem.get();
    }

    public StringProperty modelagemProperty() {
        return modelagem;
    }

    public void setModelagem(String modelagem) {
        this.modelagem.set(modelagem);
    }

    @Column(name = "CATEGORIA")
    public String getCategoria() {
        return categoria.get();
    }

    public StringProperty categoriaProperty() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria.set(categoria);
    }

    @Transient
    public Map<String, Object> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(Map<String, Object> customAttributes) {
        this.customAttributes = customAttributes;
    }

    @OneToMany
    @JoinColumn(name = "REFERENCIA", referencedColumnName = "REFERENCIA")
    public List<VB2cProdutoCores> getCores() {
        return cores;
    }

    public void setCores(List<VB2cProdutoCores> cores) {
        this.cores = cores;
    }

    @OneToOne
    @JoinColumn(name = "REFERENCIA", referencedColumnName = "CODIGO")
    public SdProduto getSdProduto() {
        return sdProduto.get();
    }

    public ObjectProperty<SdProduto> sdProdutoProperty() {
        return sdProduto;
    }

    public void setSdProduto(SdProduto sdProduto) {
        this.sdProduto.set(sdProduto);
    }

    @Transient
    public StatusEnvioProduto getStatusEnvioProduto() {
        return statusEnvioProduto;
    }

    public void setStatusEnvioProduto(StatusEnvioProduto statusEnvioProduto) {
        this.statusEnvioProduto = statusEnvioProduto;
    }
}
