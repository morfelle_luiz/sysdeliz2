package sysdeliz2.models.view.comercial;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "v_sd_itens_lista_fav_cli")
public class VSdItensListaFavCli implements Serializable {

    private final ObjectProperty<VSdListaFavoritosCliente> lista = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> produto = new SimpleObjectProperty<>();
    private final BooleanProperty fv = new SimpleBooleanProperty();
    private final BooleanProperty estoque = new SimpleBooleanProperty();

    public VSdItensListaFavCli() {
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "codcli", referencedColumnName = "codcli"),
            @JoinColumn(name = "catalogo", referencedColumnName = "catalogo")
    })
    public VSdListaFavoritosCliente getLista() {
        return lista.get();
    }

    public ObjectProperty<VSdListaFavoritosCliente> listaProperty() {
        return lista;
    }

    public void setLista(VSdListaFavoritosCliente lista) {
        this.lista.set(lista);
    }

    @Id
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "codigo")
    public VSdDadosProduto getProduto() {
        return produto.get();
    }

    public ObjectProperty<VSdDadosProduto> produtoProperty() {
        return produto;
    }

    public void setProduto(VSdDadosProduto produto) {
        this.produto.set(produto);
    }

    @Column(name = "fv")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFv() {
        return fv.get();
    }

    public BooleanProperty fvProperty() {
        return fv;
    }

    public void setFv(boolean fv) {
        this.fv.set(fv);
    }

    @Column(name = "estoque")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEstoque() {
        return estoque.get();
    }

    public BooleanProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(boolean estoque) {
        this.estoque.set(estoque);
    }
}