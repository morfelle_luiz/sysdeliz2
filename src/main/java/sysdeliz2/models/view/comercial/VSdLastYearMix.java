package sysdeliz2.models.view.comercial;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "v_sd_lastyear_mix")
@Immutable
public class VSdLastYearMix implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
        private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
        private final ObjectProperty<Linha> linha = new SimpleObjectProperty<>();
        private final ObjectProperty<SdGrupoModelagem> grupoModelagem = new SimpleObjectProperty<>();
        private final ObjectProperty<Familia> familia = new SimpleObjectProperty<>();

        public Pk() {
        }

        @OneToOne
        @JoinColumn(name = "colecao")
        public Colecao getColecao() {
            return colecao.get();
        }

        public ObjectProperty<Colecao> colecaoProperty() {
            return colecao;
        }

        public void setColecao(Colecao colecao) {
            this.colecao.set(colecao);
        }

        @OneToOne
        @JoinColumn(name = "codmarca")
        public Marca getMarca() {
            return marca.get();
        }

        public ObjectProperty<Marca> marcaProperty() {
            return marca;
        }

        public void setMarca(Marca marca) {
            this.marca.set(marca);
        }

        @OneToOne
        @JoinColumn(name = "codfam")
        public Familia getFamilia() {
            return familia.get();
        }

        public ObjectProperty<Familia> familiaProperty() {
            return familia;
        }

        public void setFamilia(Familia familia) {
            this.familia.set(familia);
        }

        @OneToOne
        @JoinColumn(name = "codlin")
        public Linha getLinha() {
            return linha.get();
        }

        public ObjectProperty<Linha> linhaProperty() {
            return linha;
        }

        public void setLinha(Linha linha) {
            this.linha.set(linha);
        }

        @OneToOne
        @JoinColumn(name = "codgrupomodelagem")
        public SdGrupoModelagem getGrupoModelagem() {
            return grupoModelagem.get();
        }

        public ObjectProperty<SdGrupoModelagem> grupoModelagemProperty() {
            return grupoModelagem;
        }

        public void setGrupoModelagem(SdGrupoModelagem grupoModelagem) {
            this.grupoModelagem.set(grupoModelagem);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeProdutos = new SimpleIntegerProperty();
    private final IntegerProperty meta = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> tempoMedio = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorMinimo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> valorMaximo = new SimpleObjectProperty<>();

    public VSdLastYearMix() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "qtde_produtos")
    public Integer getQtdeProdutos() {
        return qtdeProdutos.get();
    }

    public IntegerProperty qtdeProdutosProperty() {
        return qtdeProdutos;
    }

    public void setQtdeProdutos(Integer qtdeProdutos) {
        this.qtdeProdutos.set(qtdeProdutos);
    }

    @Column(name = "tempo_medio")
    public BigDecimal getTempoMedio() {
        return tempoMedio.get();
    }

    public ObjectProperty<BigDecimal> tempoMedioProperty() {
        return tempoMedio;
    }

    public void setTempoMedio(BigDecimal tempoMedio) {
        this.tempoMedio.set(tempoMedio);
    }

    @Column(name = "meta")
    public Integer getMeta() {
        return meta.get();
    }

    public IntegerProperty metaProperty() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta.set(meta);
    }

    @Column(name = "valor_min")
    public BigDecimal getValorMinimo() {
        return valorMinimo.get();
    }

    public ObjectProperty<BigDecimal> valorMinimoProperty() {
        return valorMinimo;
    }

    public void setValorMinimo(BigDecimal valorMinimo) {
        this.valorMinimo.set(valorMinimo);
    }

    @Column(name = "valor_max")
    public BigDecimal getValorMaximo() {
        return valorMaximo.get();
    }

    public ObjectProperty<BigDecimal> valorMaximoProperty() {
        return valorMaximo;
    }

    public void setValorMaximo(BigDecimal valorMaximo) {
        this.valorMaximo.set(valorMaximo);
    }
}