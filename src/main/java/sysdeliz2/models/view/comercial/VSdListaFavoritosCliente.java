package sysdeliz2.models.view.comercial;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.comercial.SdCatalogo;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "v_sd_lista_favoritos_cliente")
public class VSdListaFavoritosCliente extends BasicModel implements Serializable {

    private final ObjectProperty<Entidade> cliente = new SimpleObjectProperty<>();
    private final ObjectProperty<SdCatalogo> catalogo = new SimpleObjectProperty<>();
    private final BooleanProperty enviado = new SimpleBooleanProperty();
    private final BooleanProperty comPedido = new SimpleBooleanProperty();
    private List<VSdItensListaFavCli> itens = new ArrayList<>();

    public VSdListaFavoritosCliente() {
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codcli")
    public Entidade getCliente() {
        return cliente.get();
    }

    public ObjectProperty<Entidade> clienteProperty() {
        return cliente;
    }

    public void setCliente(Entidade cliente) {
        this.cliente.set(cliente);
    }

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "catalogo")
    public SdCatalogo getCatalogo() {
        return catalogo.get();
    }

    public ObjectProperty<SdCatalogo> catalogoProperty() {
        return catalogo;
    }

    public void setCatalogo(SdCatalogo catalogo) {
        this.catalogo.set(catalogo);
    }

    @OneToMany(mappedBy = "lista", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<VSdItensListaFavCli> getItens() {
        return itens;
    }

    public void setItens(List<VSdItensListaFavCli> itens) {
        this.itens = itens;
    }

    @Column(name = "enviado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEnviado() {
        return enviado.get();
    }

    public BooleanProperty enviadoProperty() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado.set(enviado);
    }

    @Column(name = "com_pedido")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isComPedido() {
        return comPedido.get();
    }

    public BooleanProperty comPedidoProperty() {
        return comPedido;
    }

    public void setComPedido(boolean comPedido) {
        this.comPedido.set(comPedido);
    }

    @Transient
    public void refresh() {
            JPAUtils.getEntityManager().refresh(this);
    }
}