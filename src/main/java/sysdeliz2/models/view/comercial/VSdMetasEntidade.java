package sysdeliz2.models.view.comercial;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.sysdeliz.comercial.SdIndicadorComercial;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.executable.ValidateOnExecution;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "v_sd_metas_entidade")
public class VSdMetasEntidade implements Serializable {

    @Embeddable
    public static class VSdMetasEntidadePK implements Serializable {

        private final StringProperty cliente = new SimpleStringProperty();
        private final StringProperty tipo = new SimpleStringProperty();
        private final StringProperty job = new SimpleStringProperty();
        private final ObjectProperty<Colecao> colecao = new SimpleObjectProperty<>();
        private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
        private final ObjectProperty<SdIndicadorComercial> indicador = new SimpleObjectProperty<>();

        public VSdMetasEntidadePK() {
        }

        @Column(name = "cliente")
        public String getCliente() {
            return cliente.get();
        }

        public StringProperty clienteProperty() {
            return cliente;
        }

        public void setCliente(String cliente) {
            this.cliente.set(cliente);
        }

        @Column(name = "tipo")
        public String getTipo() {
            return tipo.get();
        }

        public StringProperty tipoProperty() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo.set(tipo);
        }

        @Column(name = "job")
        public String getJob() {
            return job.get();
        }

        public StringProperty jobProperty() {
            return job;
        }

        public void setJob(String job) {
            this.job.set(job);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "colecao")
        public Colecao getColecao() {
            return colecao.get();
        }

        public ObjectProperty<Colecao> colecaoProperty() {
            return colecao;
        }

        public void setColecao(Colecao colecao) {
            this.colecao.set(colecao);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "marca")
        public Marca getMarca() {
            return marca.get();
        }

        public ObjectProperty<Marca> marcaProperty() {
            return marca;
        }

        public void setMarca(Marca marca) {
            this.marca.set(marca);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "indicador")
        public SdIndicadorComercial getIndicador() {
            return indicador.get();
        }

        public ObjectProperty<SdIndicadorComercial> indicadorProperty() {
            return indicador;
        }

        public void setIndicador(SdIndicadorComercial indicador) {
            this.indicador.set(indicador);
        }

        @Transient
        public String toView() {
            return job.get() + " / " + tipo.get() + " / " + marca.get().toString() + " / " + colecao.get() + " / " + indicador.get().toView();
        }
    }

    private final ObjectProperty<VSdMetasEntidadePK> id = new SimpleObjectProperty<>();
    private final StringProperty gatilhoTipo = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final StringProperty acao = new SimpleStringProperty();

    public VSdMetasEntidade() {
    }

    @EmbeddedId
    public VSdMetasEntidadePK getId() {
        return id.get();
    }

    public ObjectProperty<VSdMetasEntidadePK> idProperty() {
        return id;
    }

    public void setId(VSdMetasEntidadePK id) {
        this.id.set(id);
    }

    @Column(name = "gatilho_tipo")
    public String getGatilhoTipo() {
        return gatilhoTipo.get();
    }

    public StringProperty gatilhoTipoProperty() {
        return gatilhoTipo;
    }

    public void setGatilhoTipo(String gatilhoTipo) {
        this.gatilhoTipo.set(gatilhoTipo);
    }

    @Column(name = "valor")
    public BigDecimal getValor() {
        return valor.get();
    }

    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }

    @Column(name = "acao")
    public String getAcao() {
        return acao.get();
    }

    public StringProperty acaoProperty() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao.set(acao);
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}