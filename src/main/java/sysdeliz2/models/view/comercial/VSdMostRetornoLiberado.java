package sysdeliz2.models.view.comercial;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.BooleanAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_MOST_RETORNO_LIBERADO")
public class VSdMostRetornoLiberado implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final StringProperty colVenda = new SimpleStringProperty();
        private final StringProperty codrep = new SimpleStringProperty();
        private final IntegerProperty mostruario = new SimpleIntegerProperty();
        private final StringProperty marca = new SimpleStringProperty();

        public Pk() {
        }

        @Column(name = "COL_VENDA")
        public String getColVenda() {
            return colVenda.get();
        }

        public StringProperty colVendaProperty() {
            return colVenda;
        }

        public void setColVenda(String colVenda) {
            this.colVenda.set(colVenda);
        }

        @Column(name = "CODREP")
        public String getCodrep() {
            return codrep.get();
        }

        public StringProperty codrepProperty() {
            return codrep;
        }

        public void setCodrep(String codrep) {
            this.codrep.set(codrep);
        }

        @Column(name = "MOSTRUARIO")
        public int getMostruario() {
            return mostruario.get();
        }

        public IntegerProperty mostruarioProperty() {
            return mostruario;
        }

        public void setMostruario(int mostruario) {
            this.mostruario.set(mostruario);
        }

        @Column(name = "MARCA")
        public String getMarca() {
            return marca.get();
        }

        public StringProperty marcaProperty() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca.set(marca);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty numero = new SimpleStringProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    private final BooleanProperty faturado = new SimpleBooleanProperty();
    private final StringProperty nfFatura = new SimpleStringProperty();
    private final BooleanProperty devolvido = new SimpleBooleanProperty();
    private final StringProperty nfDevolucao = new SimpleStringProperty();

    public VSdMostRetornoLiberado() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "NUMERO")
    public String getNumero() {
        return numero.get();
    }

    public StringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    @Column(name = "REMESSA")
    public String getRemessa() {
        return remessa.get();
    }

    public StringProperty remessaProperty() {
        return remessa;
    }

    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }

    @Column(name = "FATURADO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isFaturado() {
        return faturado.get();
    }

    public BooleanProperty faturadoProperty() {
        return faturado;
    }

    public void setFaturado(boolean faturado) {
        this.faturado.set(faturado);
    }

    @Column(name = "NF_FATURA")
    public String getNfFatura() {
        return nfFatura.get();
    }

    public StringProperty nfFaturaProperty() {
        return nfFatura;
    }

    public void setNfFatura(String nfFatura) {
        this.nfFatura.set(nfFatura);
    }

    @Column(name = "DEVOLVIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isDevolvido() {
        return devolvido.get();
    }

    public BooleanProperty devolvidoProperty() {
        return devolvido;
    }

    public void setDevolvido(boolean devolvido) {
        this.devolvido.set(devolvido);
    }

    @Column(name = "NF_DEVOLUCAO")
    public String getNfDevolucao() {
        return nfDevolucao.get();
    }

    public StringProperty nfDevolucaoProperty() {
        return nfDevolucao;
    }

    public void setNfDevolucao(String nfDevolucao) {
        this.nfDevolucao.set(nfDevolucao);
    }
}