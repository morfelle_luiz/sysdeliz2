package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "V_SD_DEVOLUCAO_MOST_REP")
@Immutable
@IdClass(VSdDevolucaoMostRepPK.class)
public class VSdDevolucaoMostRep implements Serializable {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty colvenda = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final ObjectProperty<Produto> codigo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor>  cor = new SimpleObjectProperty<>();
    private final StringProperty tam = new SimpleStringProperty();
    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtded = new SimpleIntegerProperty();
    private final StringProperty mostruario = new SimpleStringProperty();
    private final StringProperty pedido = new SimpleStringProperty();
    private final StringProperty remessa = new SimpleStringProperty();
    // transients
    private List<String> mostruarios = new ArrayList<>();
    private List<String> pedidos = new ArrayList<>();
    private List<String> remessas = new ArrayList<>();

    public VSdDevolucaoMostRep() {
    }

    @Id
    @Column(name = "CODREP")
    public String getCodrep() {
        return codrep.get();
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }

    @Id
    @Column(name = "COL_VENDA")
    public String getColvenda() {
        return colvenda.get();
    }

    public StringProperty colvendaProperty() {
        return colvenda;
    }

    public void setColvenda(String colvenda) {
        this.colvenda.set(colvenda);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    public Produto getCodigo() {
        return codigo.get();
    }

    public ObjectProperty<Produto> codigoProperty() {
        return codigo;
    }

    public void setCodigo(Produto codigo) {
        this.codigo.set(codigo);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COR")
    public Cor getCor() {
        return cor.get();
    }

    public ObjectProperty<Cor> corProperty() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor.set(cor);
    }

    @Column(name = "TAM")
    public String getTam() {
        return tam.get();
    }

    public StringProperty tamProperty() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam.set(tam);
    }

    @Id
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "QTDE_D")
    public Integer getQtded() {
        return qtded.get();
    }

    public IntegerProperty qtdedProperty() {
        return qtded;
    }

    public void setQtded(Integer qtded) {
        this.qtded.set(qtded);
    }

    @Column(name = "MOSTRUARIOS")
    public String getMostruario() {
        return mostruario.get();
    }

    public StringProperty mostruarioProperty() {
        return mostruario;
    }

    public void setMostruario(String mostruario) {
        this.mostruario.set(mostruario);
    }

    @Column(name = "PEDIDOS")
    public String getPedido() {
        return pedido.get();
    }

    public StringProperty pedidoProperty() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido.set(pedido);
    }

    @Column(name = "REMESSAS")
    public String getRemessa() {
        return remessa.get();
    }

    public StringProperty remessaProperty() {
        return remessa;
    }

    public void setRemessa(String remessa) {
        this.remessa.set(remessa);
    }

    @Transient
    public List<String> getMostruarios() {
        return mostruarios;
    }

    public void setMostruarios(List<String> mostruarios) {
        this.mostruarios = mostruarios;
    }

    @Transient
    public List<String> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<String> pedidos) {
        this.pedidos = pedidos;
    }

    @Transient
    public List<String> getRemessas() {
        return remessas;
    }

    public void setRemessas(List<String> remessas) {
        this.remessas = remessas;
    }

    @Transient
    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }

    @PostLoad
    private void postLoad() {
        setMostruarios(Arrays.asList(getMostruario().split(",")));
        setPedidos(Arrays.asList(getPedido().split(",")));
        setRemessas(Arrays.asList(getRemessa().split(",")));
    }
}