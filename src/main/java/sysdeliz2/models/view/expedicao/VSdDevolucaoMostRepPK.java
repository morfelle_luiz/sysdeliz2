package sysdeliz2.models.view.expedicao;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;

public class VSdDevolucaoMostRepPK implements Serializable {

    private final StringProperty codrep = new SimpleStringProperty();
    private final StringProperty colvenda = new SimpleStringProperty();
    private final StringProperty barra28 = new SimpleStringProperty();

    public VSdDevolucaoMostRepPK() {
    }

    public String getCodrep() {
        return codrep.get();
    }

    public StringProperty codrepProperty() {
        return codrep;
    }

    public void setCodrep(String codrep) {
        this.codrep.set(codrep);
    }

    public String getColvenda() {
        return colvenda.get();
    }

    public StringProperty colvendaProperty() {
        return colvenda;
    }

    public void setColvenda(String colvenda) {
        this.colvenda.set(colvenda);
    }

    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }
}
