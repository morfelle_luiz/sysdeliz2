package sysdeliz2.models.view.expedicao;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_DISTRIBUICAO_MOSTRUARIO")
public class VSdDistribuicaoMostruario implements Serializable {

    private final StringProperty colvenda = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty descmarca = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty desctipo = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty linha = new SimpleStringProperty();
    private final StringProperty grupomodelagem = new SimpleStringProperty();
    private final StringProperty caixas = new SimpleStringProperty();
    private final StringProperty menos = new SimpleStringProperty();
    private final StringProperty distribuicao = new SimpleStringProperty();

    public VSdDistribuicaoMostruario() {
    }

    @Id
    @Column(name = "COL_VENDA")
    public String getColvenda() {
        return colvenda.get();
    }

    public StringProperty colvendaProperty() {
        return colvenda;
    }

    public void setColvenda(String colvenda) {
        this.colvenda.set(colvenda);
    }

    @Id
    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Id
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "DESC_MARCA")
    public String getDescmarca() {
        return descmarca.get();
    }

    public StringProperty descmarcaProperty() {
        return descmarca;
    }

    public void setDescmarca(String descmarca) {
        this.descmarca.set(descmarca);
    }

    @Id
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "DESC_TIPO")
    public String getDesctipo() {
        return desctipo.get();
    }

    public StringProperty desctipoProperty() {
        return desctipo;
    }

    public void setDesctipo(String desctipo) {
        this.desctipo.set(desctipo);
    }

    @Id
    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "LINHA")
    public String getLinha() {
        return linha.get();
    }

    public StringProperty linhaProperty() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha.set(linha);
    }

    @Column(name = "GRUPOMODELAGEM")
    public String getGrupomodelagem() {
        return grupomodelagem.get();
    }

    public StringProperty grupomodelagemProperty() {
        return grupomodelagem;
    }

    public void setGrupomodelagem(String grupomodelagem) {
        this.grupomodelagem.set(grupomodelagem);
    }

    @Column(name = "CAIXAS")
    public String getCaixas() {
        return caixas.get();
    }

    public StringProperty caixasProperty() {
        return caixas;
    }

    public void setCaixas(String caixas) {
        this.caixas.set(caixas);
    }

    @Column(name = "MENOS")
    public String getMenos() {
        return menos.get();
    }

    public StringProperty menosProperty() {
        return menos;
    }

    public void setMenos(String menos) {
        this.menos.set(menos);
    }

    @Column(name = "DISTRIBUICAO")
    public String getDistribuicao() {
        return distribuicao.get();
    }

    public StringProperty distribuicaoProperty() {
        return distribuicao;
    }

    public void setDistribuicao(String distribuicao) {
        this.distribuicao.set(distribuicao);
    }

}