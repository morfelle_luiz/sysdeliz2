package sysdeliz2.models.view.expedicao;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "V_SD_ESTOQUE_PRODUTO_COR")
public class VSdEstoqueProdutoCor implements Serializable {

    private final StringProperty barra28 = new SimpleStringProperty();
    private final StringProperty codigo = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty estoque = new SimpleStringProperty();

    public VSdEstoqueProdutoCor() {
    }

    @Id
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }

    public StringProperty barra28Property() {
        return barra28;
    }

    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }

    @Column(name = "CODIGO")
    public String getCodigo() {
        return codigo.get();
    }

    public StringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }

}