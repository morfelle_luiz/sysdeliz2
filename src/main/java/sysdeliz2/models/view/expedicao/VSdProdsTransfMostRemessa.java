package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.comercial.SdTransfProdMostruario;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "v_sd_prods_transf_most_remessa")
public class VSdProdsTransfMostRemessa implements Serializable {

    @Embeddable
    public static class VSdProdsTransfMostRemessaPK implements Serializable {

        private final StringProperty colecao = new SimpleStringProperty();
        private final StringProperty representante = new SimpleStringProperty();
        private final StringProperty produto = new SimpleStringProperty();
        private final StringProperty marca = new SimpleStringProperty();
        private final IntegerProperty mostruario = new SimpleIntegerProperty();

        public VSdProdsTransfMostRemessaPK() {
        }

        // <editor-fold defaultstate="collapsed" desc="Getter/Setter">
        @Column(name = "COLECAO")
        public String getColecao() {
            return colecao.get();
        }

        public StringProperty colecaoProperty() {
            return colecao;
        }

        public void setColecao(String colecao) {
            this.colecao.set(colecao);
        }

        @Column(name = "REPRESENTANTE")
        public String getRepresentante() {
            return representante.get();
        }

        public StringProperty representanteProperty() {
            return representante;
        }

        public void setRepresentante(String representante) {
            this.representante.set(representante);
        }

        @Column(name = "PRODUTO")
        public String getProduto() {
            return produto.get();
        }

        public StringProperty produtoProperty() {
            return produto;
        }

        public void setProduto(String produto) {
            this.produto.set(produto);
        }

        @Column(name = "MARCA")
        public String getMarca() {
            return marca.get();
        }

        public StringProperty marcaProperty() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca.set(marca);
        }

        @Column(name = "MOSTRUARIO")
        public int getMostruario() {
            return mostruario.get();
        }

        public IntegerProperty mostruarioProperty() {
            return mostruario;
        }

        public void setMostruario(int mostruario) {
            this.mostruario.set(mostruario);
        }
        // </editor-fold>
    }

    private final ObjectProperty<VSdProdsTransfMostRemessa.VSdProdsTransfMostRemessaPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final StringProperty status = new SimpleStringProperty("P");
    private final StringProperty barra = new SimpleStringProperty();

    public VSdProdsTransfMostRemessa() {
    }

    @EmbeddedId
    public VSdProdsTransfMostRemessa.VSdProdsTransfMostRemessaPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdProdsTransfMostRemessa.VSdProdsTransfMostRemessaPK> idProperty() {
        return id;
    }

    public void setId(VSdProdsTransfMostRemessa.VSdProdsTransfMostRemessaPK id) {
        this.id.set(id);
    }

    @Column(name = "QTDE")
    public int getQtde() {
        return qtde.get();
    }

    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde.set(qtde);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "BARRA")
    public String getBarra() {
        return barra.get();
    }

    public StringProperty barraProperty() {
        return barra;
    }

    public void setBarra(String barra) {
        this.barra.set(barra);
    }

    public void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}