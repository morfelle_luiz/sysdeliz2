package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_PRODUTO_REPOSICAO")
public class VSdProdutoReposicao implements Serializable {

    private final IntegerProperty idjpa = new SimpleIntegerProperty();
    private final StringProperty produto = new SimpleStringProperty();
    private final StringProperty cor = new SimpleStringProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty local = new SimpleStringProperty();
    private final StringProperty status = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtemissao = new SimpleObjectProperty<>();
    private final StringProperty estoque = new SimpleStringProperty();

    public VSdProdutoReposicao() {
    }

    @Id
    @Column(name = "IDJPA")
    public Integer getIdjpa() {
        return idjpa.get();
    }

    public IntegerProperty idjpaProperty() {
        return idjpa;
    }

    public void setIdjpa(Integer idjpa) {
        this.idjpa.set(idjpa);
    }

    @Column(name = "PRODUTO")
    public String getProduto() {
        return produto.get();
    }

    public StringProperty produtoProperty() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto.set(produto);
    }

    @Column(name = "COR")
    public String getCor() {
        return cor.get();
    }

    public StringProperty corProperty() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor.set(cor);
    }

    @Column(name = "ORD")
    public Integer getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }

    public StringProperty tipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }

    public StringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    @Column(name = "LOCAL")
    public String getLocal() {
        return local.get();
    }

    public StringProperty localProperty() {
        return local;
    }

    public void setLocal(String local) {
        this.local.set(local);
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtemissao() {
        return dtemissao.get();
    }

    public ObjectProperty<LocalDate> dtemissaoProperty() {
        return dtemissao;
    }

    public void setDtemissao(LocalDate dtemissao) {
        this.dtemissao.set(dtemissao);
    }

    @Column(name = "ESTOQUE")
    public String getEstoque() {
        return estoque.get();
    }

    public StringProperty estoqueProperty() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque.set(estoque);
    }

}
