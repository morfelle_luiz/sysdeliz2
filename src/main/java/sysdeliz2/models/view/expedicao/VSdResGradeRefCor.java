package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_RES_GRADE_REF_COR_V2")
public class VSdResGradeRefCor implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<VSdResRefCorPedido> codigoCor = new SimpleObjectProperty<>();
        private final StringProperty tam = new SimpleStringProperty();

        public Pk() {
        }

        public Pk(VSdResRefCorPedido codigoCor, String tam) {
            setCodigoCor(codigoCor);
            setTam(tam);
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "REMESSA", insertable = false, updatable = false, referencedColumnName = "REMESSA"),
                @JoinColumn(name = "RES_STATUS", insertable = false, updatable = false, referencedColumnName = "RES_STATUS"),
                @JoinColumn(name = "CODCLI", insertable = false, updatable = false, referencedColumnName = "CODCLI"),
                @JoinColumn(name = "NUMERO", insertable = false, updatable = false, referencedColumnName = "NUMERO"),
                @JoinColumn(name = "CODIGO", insertable = false, updatable = false, referencedColumnName = "CODIGO"),
                @JoinColumn(name = "COR", insertable = false, updatable = false, referencedColumnName = "COR")
        })
        public VSdResRefCorPedido getCodigoCor() {
            return codigoCor.get();
        }

        public ObjectProperty<VSdResRefCorPedido> codigoCorProperty() {
            return codigoCor;
        }

        public void setCodigoCor(VSdResRefCorPedido codigoCor) {
            this.codigoCor.set(codigoCor);
        }

        @Column(name = "TAM")
        public String getTam() {
            return tam.get();
        }

        public StringProperty tamProperty() {
            return tam;
        }

        public void setTam(String tam) {
            this.tam.set(tam);
        }

    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final StringProperty marca = new SimpleStringProperty();
    private final StringProperty statusitem = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdec = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> desconto = new SimpleObjectProperty<>();
    private final IntegerProperty posicao = new SimpleIntegerProperty();
    private final StringProperty tipo = new SimpleStringProperty();
    private final StringProperty barra28 = new SimpleStringProperty();
    
    
    public VSdResGradeRefCor() {
    }
    
    public VSdResGradeRefCor(VSdResRefCorPedido codigoCor, String tam, String statusitem, LocalDate dtentrega, String marca,
                             Integer qtde, Integer qtdec, BigDecimal valor, Integer posicao, String tipo, BigDecimal desconto) {

        this.id.set(new Pk(codigoCor, tam));
        this.statusitem.set(statusitem);
        this.dtentrega.set(dtentrega);
        this.marca.set(marca);
        this.qtde.set(qtde);
        this.qtdec.set(qtdec);
        this.valor.set(valor);
        this.posicao.set(posicao);
        this.tipo.set(tipo);
        this.desconto.set(desconto);
    }


    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "STATUS_ITEM")
    public String getStatusitem() {
        return statusitem.get();
    }

    public StringProperty statusitemProperty() {
        return statusitem;
    }

    public void setStatusitem(String statusitem) {
        this.statusitem.set(statusitem);
    }

    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }

    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "DESCONTO")
    public BigDecimal getDesconto() {
        return desconto.get();
    }
    
    public ObjectProperty<BigDecimal> descontoProperty() {
        return desconto;
    }
    
    public void setDesconto(BigDecimal desconto) {
        this.desconto.set(desconto);
    }
    
    @Column(name = "POSICAO")
    @OrderBy("POSICAO ASC")
    public Integer getPosicao() {
        return posicao.get();
    }
    
    public IntegerProperty posicaoProperty() {
        return posicao;
    }
    
    public void setPosicao(Integer posicao) {
        this.posicao.set(posicao);
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "BARRA28")
    public String getBarra28() {
        return barra28.get();
    }
    
    public StringProperty barra28Property() {
        return barra28;
    }
    
    public void setBarra28(String barra28) {
        this.barra28.set(barra28);
    }
    
    @Transient
    public void refresh() {
        try {
            JPAUtils.getEntityManager().refresh(this);
        } catch (EntityNotFoundException exc) {
            exc.printStackTrace();
        }
    }
}