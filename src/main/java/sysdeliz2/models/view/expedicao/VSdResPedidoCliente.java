package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "V_SD_RES_PEDIDO_CLIENTE_V2")
public class VSdResPedidoCliente implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<VSdReservasAlocadas> remessa = new SimpleObjectProperty<>();
        private final ObjectProperty<Pedido> numero = new SimpleObjectProperty<>();

        public Pk() {
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "REMESSA", insertable = false, updatable = false, referencedColumnName = "REMESSA"),
                @JoinColumn(name = "RES_STATUS", insertable = false, updatable = false, referencedColumnName = "RES_STATUS"),
                @JoinColumn(name = "CODCLI", insertable = false, updatable = false, referencedColumnName = "CODCLI")
        })
        public VSdReservasAlocadas getRemessa() {
            return remessa.get();
        }

        public ObjectProperty<VSdReservasAlocadas> remessaProperty() {
            return remessa;
        }

        public void setRemessa(VSdReservasAlocadas remessa) {
            this.remessa.set(remessa);
        }

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "NUMERO")
        public Pedido getNumero() {
            return numero.get();
        }

        public ObjectProperty<Pedido> numeroProperty() {
            return numero;
        }

        public void setNumero(Pedido numero) {
            this.numero.set(numero);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<LocalDate> dtBase = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<Marca> marca = new SimpleObjectProperty<>();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdec = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private final BooleanProperty aplicadoDesconto = new SimpleBooleanProperty(false);
    private final IntegerProperty qtdePend = new SimpleIntegerProperty(0);
    private final IntegerProperty pendPeriodo = new SimpleIntegerProperty(0);
    private final ObjectProperty<BigDecimal> percAtend = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final BooleanProperty inferior = new SimpleBooleanProperty();
    private final BooleanProperty superior = new SimpleBooleanProperty();
    private final BooleanProperty naoDefinido = new SimpleBooleanProperty();
    private List<VSdResRefCorPedido> itens = new ArrayList<>();
    
    public VSdResPedidoCliente() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }

    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }

    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "DT_BASE")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtBase() {
        return dtBase.get();
    }
    
    public ObjectProperty<LocalDate> dtBaseProperty() {
        return dtBase;
    }
    
    public void setDtBase(LocalDate dtBase) {
        this.dtBase.set(dtBase);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    public Marca getMarca() {
        return marca.get();
    }
    
    public ObjectProperty<Marca> marcaProperty() {
        return marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @Column(name = "APLIC_DESC")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAplicadoDesconto() {
        return aplicadoDesconto.get();
    }
    
    public BooleanProperty aplicadoDescontoProperty() {
        return aplicadoDesconto;
    }
    
    public void setAplicadoDesconto(boolean aplicadoDesconto) {
        this.aplicadoDesconto.set(aplicadoDesconto);
    }
    
    @Column(name = "QTDE_PEND")
    public Integer getQtdePend() {
        return qtdePend.get();
    }
    
    public IntegerProperty qtdePendProperty() {
        return qtdePend;
    }
    
    public void setQtdePend(Integer qtdePend) {
        this.qtdePend.set(qtdePend);
    }
    
    @OneToMany(mappedBy = "id.pedido")
    public List<VSdResRefCorPedido> getItens() {
        return itens;
    }
    
    public void setItens(List<VSdResRefCorPedido> itens) {
        this.itens = itens;
    }
    
    @Column(name = "PEND_PERIODO")
    public Integer getPendPeriodo() {
        return pendPeriodo.get();
    }
    
    public IntegerProperty pendPeriodoProperty() {
        return pendPeriodo;
    }
    
    public void setPendPeriodo(Integer pendPeriodo) {
        this.pendPeriodo.set(pendPeriodo);
    }
    
    @Column(name = "PERC_ATEND")
    public BigDecimal getPercAtend() {
        return percAtend.get();
    }
    
    public ObjectProperty<BigDecimal> percAtendProperty() {
        return percAtend;
    }
    
    public void setPercAtend(BigDecimal percAtend) {
        this.percAtend.set(percAtend);
    }

    @Column(name = "IS_INFERIOR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isInferior() {
        return inferior.get();
    }

    public BooleanProperty inferiorProperty() {
        return inferior;
    }

    public void setInferior(boolean inferior) {
        this.inferior.set(inferior);
    }

    @Column(name = "IS_SUPERIOR")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSuperior() {
        return superior.get();
    }

    public BooleanProperty superiorProperty() {
        return superior;
    }

    public void setSuperior(boolean superior) {
        this.superior.set(superior);
    }

    @Column(name = "IS_NAODEFINIDO")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isNaoDefinido() {
        return naoDefinido.get();
    }

    public BooleanProperty naoDefinidoProperty() {
        return naoDefinido;
    }

    public void setNaoDefinido(boolean naoDefinido) {
        this.naoDefinido.set(naoDefinido);
    }

    @Transient
    public void refresh() {
        try {
            JPAUtils.getEntityManager().refresh(this);
        } catch (EntityNotFoundException exc) {
            exc.printStackTrace();
        }
    }
}