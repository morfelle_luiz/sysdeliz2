package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "V_SD_RES_REF_COR_PEDIDO_V2")
public class VSdResRefCorPedido implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<VSdResPedidoCliente> pedido = new SimpleObjectProperty<>();
        private final ObjectProperty<VSdDadosProduto> codigo = new SimpleObjectProperty<>();
        private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();

        public Pk() {
        }

        public Pk(VSdResPedidoCliente pedido, VSdDadosProduto codigo, Cor cor) {
            setPedido(pedido);
            setCodigo(codigo);
            setCor(cor);
        }

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "REMESSA", insertable = false, updatable = false, referencedColumnName = "REMESSA"),
                @JoinColumn(name = "RES_STATUS", insertable = false, updatable = false, referencedColumnName = "RES_STATUS"),
                @JoinColumn(name = "CODCLI", insertable = false, updatable = false, referencedColumnName = "CODCLI"),
                @JoinColumn(name = "NUMERO", insertable = false, updatable = false, referencedColumnName = "NUMERO")
        })
        public VSdResPedidoCliente getPedido() {
            return pedido.get();
        }

        public ObjectProperty<VSdResPedidoCliente> pedidoProperty() {
            return pedido;
        }

        public void setPedido(VSdResPedidoCliente pedido) {
            this.pedido.set(pedido);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "CODIGO")
        public VSdDadosProduto getCodigo() {
            return codigo.get();
        }

        public ObjectProperty<VSdDadosProduto> codigoProperty() {
            return codigo;
        }

        public void setCodigo(VSdDadosProduto codigo) {
            this.codigo.set(codigo);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "COR")
        public Cor getCor() {
            return cor.get();
        }

        public ObjectProperty<Cor> corProperty() {
            return cor;
        }

        public void setCor(Cor cor) {
            this.cor.set(cor);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final StringProperty statusitem = new SimpleStringProperty("P");
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final StringProperty marca = new SimpleStringProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdec = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private List<VSdResGradeRefCor> grade = new ArrayList<>();
    private final StringProperty tipo = new SimpleStringProperty();
    private final IntegerProperty faltaBarra = new SimpleIntegerProperty();
    
    public VSdResRefCorPedido() {
    }
    
    public VSdResRefCorPedido(VSdResPedidoCliente pedido, VSdDadosProduto codigo, Cor cor, String statusitem,
                              LocalDate dtentrega, String marca, Integer qtde, Integer qtdec,
                              BigDecimal valor, String tipo) {

        this.id.set(new Pk(pedido, codigo, cor));
        this.statusitem.set(statusitem);
        this.dtentrega.set(dtentrega);
        this.marca.set(marca);
        this.qtde.set(qtde);
        this.qtdec.set(qtdec);
        this.valor.set(valor);
        this.tipo.set(tipo);
    }


    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "STATUS_ITEM")
    public String getStatusitem() {
        return statusitem.get();
    }
    
    public StringProperty statusitemProperty() {
        return statusitem;
    }
    
    public void setStatusitem(String statusitem) {
        this.statusitem.set(statusitem);
    }
    
    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "MARCA")
    public String getMarca() {
        return marca.get();
    }
    
    public StringProperty marcaProperty() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    @Column(name = "QTDE")
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }
    
    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }
    
    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @OneToMany(mappedBy = "id.codigoCor")
    public List<VSdResGradeRefCor> getGrade() {
        return grade;
    }
    
    public void setGrade(List<VSdResGradeRefCor> grade) {
        this.grade = grade;
    }
    
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo.get();
    }
    
    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    @Column(name = "ORDEM")
    public int getOrdem() {
        return ordem.get();
    }
    
    public IntegerProperty ordemProperty() {
        return ordem;
    }
    
    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }
    
    @Column(name = "FALTA_BARRA")
    public int getFaltaBarra() {
        return faltaBarra.get();
    }
    
    public IntegerProperty faltaBarraProperty() {
        return faltaBarra;
    }
    
    public void setFaltaBarra(int faltaBarra) {
        this.faltaBarra.set(faltaBarra);
    }
    
    @Transient
    public void refresh() {
        try {
            JPAUtils.getEntityManager().refresh(this);
        } catch (EntityNotFoundException exc) {
            exc.printStackTrace();
        }
    }
}