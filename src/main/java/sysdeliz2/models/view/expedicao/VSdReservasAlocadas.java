package sysdeliz2.models.view.expedicao;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdStatusRemessa;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "V_SD_RESERVAS_ALOCADAS_V2")
public class VSdReservasAlocadas extends BasicModel implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final StringProperty remessa = new SimpleStringProperty();
        private final ObjectProperty<SdStatusRemessa> resstatus = new SimpleObjectProperty<>();
        private final ObjectProperty<VSdDadosCliente> codcli = new SimpleObjectProperty<>();

        public Pk() {
        }

        @Column(name = "REMESSA")
        public String getRemessa() {
            return remessa.get();
        }

        public StringProperty remessaProperty() {
            return remessa;
        }

        public void setRemessa(String remessa) {
            this.remessa.set(remessa);
        }

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "RES_STATUS")
        public SdStatusRemessa getResstatus() {
            return resstatus.get();
        }

        public ObjectProperty<SdStatusRemessa> resstatusProperty() {
            return resstatus;
        }

        public void setResstatus(SdStatusRemessa resstatus) {
            this.resstatus.set(resstatus);
        }

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "CODCLI")
        public VSdDadosCliente getCodcli() {
            return codcli.get();
        }

        public ObjectProperty<VSdDadosCliente> codcliProperty() {
            return codcli;
        }

        public void setCodcli(VSdDadosCliente codcli) {
            this.codcli.set(codcli);
        }

    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final BooleanProperty resautomatica = new SimpleBooleanProperty();
    private final BooleanProperty reslocked = new SimpleBooleanProperty();
    private final IntegerProperty marcas = new SimpleIntegerProperty();
    private final IntegerProperty qtdepedidos = new SimpleIntegerProperty();
    private final IntegerProperty qtde = new SimpleIntegerProperty();
    private final IntegerProperty qtdec = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> valor = new SimpleObjectProperty<>();
    private List<VSdResPedidoCliente> pedidos = new ArrayList<>();
    private final StringProperty observacao = new SimpleStringProperty();
    private final IntegerProperty prioridadecoleta = new SimpleIntegerProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final ObjectProperty<SdColaborador> coletor = new SimpleObjectProperty<>();
    private final StringProperty emissor = new SimpleStringProperty();
    
    public VSdReservasAlocadas() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "RES_AUTOMATICA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isResautomatica() {
        return resautomatica.get();
    }
    
    public BooleanProperty resautomaticaProperty() {
        return resautomatica;
    }
    
    public void setResautomatica(boolean resautomatica) {
        this.resautomatica.set(resautomatica);
    }
    
    @Column(name = "RES_LOCKED")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isReslocked() {
        return reslocked.get();
    }
    
    public BooleanProperty reslockedProperty() {
        return reslocked;
    }
    
    public void setReslocked(boolean reslocked) {
        this.reslocked.set(reslocked);
    }
    
    @Column(name = "MARCAS")
    public Integer getMarcas() {
        return marcas.get();
    }
    
    public IntegerProperty marcasProperty() {
        return marcas;
    }
    
    public void setMarcas(Integer marcas) {
        this.marcas.set(marcas);
    }
    
    @Column(name = "PEDIDOS")
    public Integer getQtdepedidos() {
        return qtdepedidos.get();
    }
    
    public IntegerProperty qtdepedidosProperty() {
        return qtdepedidos;
    }
    
    public void setQtdepedidos(Integer qtdepedidos) {
        this.qtdepedidos.set(qtdepedidos);
    }
    
    @Column(name = "QTDE", nullable = false)
    public Integer getQtde() {
        return qtde.get();
    }
    
    public IntegerProperty qtdeProperty() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde.set(qtde);
    }
    
    @Column(name = "QTDE_C")
    public Integer getQtdec() {
        return qtdec.get();
    }
    
    public IntegerProperty qtdecProperty() {
        return qtdec;
    }

    public void setQtdec(Integer qtdec) {
        this.qtdec.set(qtdec);
    }
    
    @Column(name = "VALOR")
    public BigDecimal getValor() {
        return valor.get();
    }
    
    public ObjectProperty<BigDecimal> valorProperty() {
        return valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor.set(valor);
    }
    
    @OneToMany(mappedBy = "id.remessa")
    public List<VSdResPedidoCliente> getPedidos() {
        return pedidos;
    }
    
    public void setPedidos(List<VSdResPedidoCliente> pedidos) {
        this.pedidos = pedidos;
    }
    
    @Column(name = "OBSERVACAO")
    public String getObservacao() {
        return observacao.get();
    }
    
    public StringProperty observacaoProperty() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao.set(observacao);
    }
    
    @Column(name = "PRIORIDADE_COLETA")
    public int getPrioridadecoleta() {
        return prioridadecoleta.get();
    }
    
    public IntegerProperty prioridadecoletaProperty() {
        return prioridadecoleta;
    }
    
    public void setPrioridadecoleta(int prioridadecoleta) {
        this.prioridadecoleta.set(prioridadecoleta);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLETOR")
    public SdColaborador getColetor() {
        return coletor.get();
    }
    
    public ObjectProperty<SdColaborador> coletorProperty() {
        return coletor;
    }
    
    public void setColetor(SdColaborador coletor) {
        this.coletor.set(coletor);
    }

    @Column(name = "EMISSOR")
    public String getEmissor() {
        return emissor.get();
    }

    public StringProperty emissorProperty() {
        return emissor;
    }

    public void setEmissor(String emissor) {
        this.emissor.set(emissor);
    }

    @Transient
    public void refresh() {
        try {
            JPAUtils.getEntityManager().refresh(this);
        } catch (EntityNotFoundException exc) {
            exc.printStackTrace();
        }
    }
}