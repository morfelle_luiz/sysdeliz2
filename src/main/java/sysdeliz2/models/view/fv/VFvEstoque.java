package sysdeliz2.models.view.fv;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "V_FV_ESTOQUE")
public class VFvEstoque implements Serializable {
    
    @Id
    @Column(name = "empresa")
    private String empresa;
    @Id
    @Column(name = "referencia")
    private String referencia;
    @Id
    @Column(name = "cor")
    private String cor;
    @Id
    @Column(name = "tam")
    private String tam;
    @Column(name = "estoque_limitado")
    private Integer estoqueLimitado;
    @Column(name = "quantidade")
    private BigDecimal quantidade;

    public VFvEstoque() {
    }

    public VFvEstoque(String empresa, String referencia, String cor, String tam, Integer estoqueLimitado, BigDecimal quantidade) {
        this.empresa = empresa;
        this.referencia = referencia;
        this.cor = cor;
        this.tam = tam;
        this.estoqueLimitado = estoqueLimitado;
        this.quantidade = quantidade;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTam() {
        return tam;
    }

    public void setTam(String tam) {
        this.tam = tam;
    }

    public Integer getEstoqueLimitado() {
        return estoqueLimitado;
    }

    public void setEstoqueLimitado(Integer estoqueLimitado) {
        this.estoqueLimitado = estoqueLimitado;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }
}