package sysdeliz2.models.view.mrptinturaria;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Material;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "V_SD_MRP_TINTURARIA_H")
public class VSdMrpTinturariaH implements Serializable {
    
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty statusmrp = new SimpleStringProperty();
    private final StringProperty numeroof = new SimpleStringProperty();
    private final StringProperty loteof = new SimpleStringProperty();
    private final StringProperty statusof = new SimpleStringProperty();
    private final StringProperty setorof = new SimpleStringProperty();
    private final StringProperty partesetor = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final StringProperty materialpai = new SimpleStringProperty();
    private final StringProperty cormaterialpai = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdenec = new SimpleObjectProperty<>();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final ObjectProperty<Cor> corinsumo = new SimpleObjectProperty<>();
    private final StringProperty grpcomprador = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty tipoinsumo = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty unidadeestq = new SimpleStringProperty();
    private final ObjectProperty<Entidade> forinsumo = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consofestoque = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consofcompra = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consoffalta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumidoestoque = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumidocompra = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consumidofalta = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consestqestoque = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consestqcompra = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> consestqfalta = new SimpleObjectProperty<>();
    private final StringProperty origemMrp = new SimpleStringProperty();
    
    public VSdMrpTinturariaH() {
    }
    
    public VSdMrpTinturariaH(String loteof, Material insumo, Cor corinsumo, String unidadeestq, Entidade forinsumo,
                             BigDecimal consestqestoque, BigDecimal consestqcompra, BigDecimal consestqfalta) {
        this.loteof.set(loteof);
        this.insumo.set(insumo);
        this.corinsumo.set(corinsumo);
        this.unidadeestq.set(unidadeestq);
        this.forinsumo.set(forinsumo);
        this.consestqestoque.set(consestqestoque);
        this.consestqcompra.set(consestqcompra);
        this.consestqfalta.set(consestqfalta);
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "STATUS_MRP")
    public String getStatusmrp() {
        return statusmrp.get();
    }
    
    public StringProperty statusmrpProperty() {
        return statusmrp;
    }
    
    public void setStatusmrp(String statusmrp) {
        this.statusmrp.set(statusmrp);
    }
    
    @Column(name = "NUMERO_OF")
    public String getNumeroof() {
        return numeroof.get();
    }
    
    public StringProperty numeroofProperty() {
        return numeroof;
    }
    
    public void setNumeroof(String numeroof) {
        this.numeroof.set(numeroof);
    }
    
    @Column(name = "LOTE_OF")
    public String getLoteof() {
        return loteof.get();
    }
    
    public StringProperty loteofProperty() {
        return loteof;
    }
    
    public void setLoteof(String loteof) {
        this.loteof.set(loteof);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusof() {
        return statusof.get();
    }
    
    public StringProperty statusofProperty() {
        return statusof;
    }
    
    public void setStatusof(String statusof) {
        this.statusof.set(statusof);
    }
    
    @Column(name = "SETOR_OF")
    public String getSetorof() {
        return setorof.get();
    }
    
    public StringProperty setorofProperty() {
        return setorof;
    }
    
    public void setSetorof(String setorof) {
        this.setorof.set(setorof);
    }
    
    @Column(name = "PARTE_SETOR")
    public String getPartesetor() {
        return partesetor.get();
    }
    
    public StringProperty partesetorProperty() {
        return partesetor;
    }
    
    public void setPartesetor(String partesetor) {
        this.partesetor.set(partesetor);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @Column(name = "MATERIAL_PAI")
    public String getMaterialpai() {
        return materialpai.get();
    }
    
    public StringProperty materialpaiProperty() {
        return materialpai;
    }
    
    public void setMaterialpai(String materialpai) {
        this.materialpai.set(materialpai);
    }
    
    @Column(name = "COR_MATERIAL_PAI")
    public String getCormaterialpai() {
        return cormaterialpai.get();
    }
    
    public StringProperty cormaterialpaiProperty() {
        return cormaterialpai;
    }
    
    public void setCormaterialpai(String cormaterialpai) {
        this.cormaterialpai.set(cormaterialpai);
    }
    
    @Column(name = "QTDE_NEC")
    public BigDecimal getQtdenec() {
        return qtdenec.get();
    }
    
    public ObjectProperty<BigDecimal> qtdenecProperty() {
        return qtdenec;
    }
    
    public void setQtdenec(BigDecimal qtdenec) {
        this.qtdenec.set(qtdenec);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_INSUMO")
    public Cor getCorinsumo() {
        return corinsumo.get();
    }
    
    public ObjectProperty<Cor> corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(Cor corinsumo) {
        this.corinsumo.set(corinsumo);
    }
    
    @Column(name = "GRP_COMPRADOR")
    public String getGrpcomprador() {
        return grpcomprador.get();
    }
    
    public StringProperty grpcompradorProperty() {
        return grpcomprador;
    }
    
    public void setGrpcomprador(String grpcomprador) {
        this.grpcomprador.set(grpcomprador);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "TIPO_INSUMO")
    public String getTipoinsumo() {
        return tipoinsumo.get();
    }
    
    public StringProperty tipoinsumoProperty() {
        return tipoinsumo;
    }
    
    public void setTipoinsumo(String tipoinsumo) {
        this.tipoinsumo.set(tipoinsumo);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "UNIDADE_ESTQ")
    public String getUnidadeestq() {
        return unidadeestq.get();
    }
    
    public StringProperty unidadeestqProperty() {
        return unidadeestq;
    }
    
    public void setUnidadeestq(String unidadeestq) {
        this.unidadeestq.set(unidadeestq);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOR_INSUMO")
    public Entidade getForinsumo() {
        return forinsumo.get();
    }
    
    public ObjectProperty<Entidade> forinsumoProperty() {
        return forinsumo;
    }
    
    public void setForinsumo(Entidade forinsumo) {
        this.forinsumo.set(forinsumo);
    }
    
    @Column(name = "CONS_OF_ESTOQUE")
    public BigDecimal getConsofestoque() {
        return consofestoque.get();
    }
    
    public ObjectProperty<BigDecimal> consofestoqueProperty() {
        return consofestoque;
    }
    
    public void setConsofestoque(BigDecimal consofestoque) {
        this.consofestoque.set(consofestoque);
    }
    
    @Column(name = "CONS_OF_COMPRA")
    public BigDecimal getConsofcompra() {
        return consofcompra.get();
    }
    
    public ObjectProperty<BigDecimal> consofcompraProperty() {
        return consofcompra;
    }
    
    public void setConsofcompra(BigDecimal consofcompra) {
        this.consofcompra.set(consofcompra);
    }
    
    @Column(name = "CONS_OF_FALTA")
    public BigDecimal getConsoffalta() {
        return consoffalta.get();
    }
    
    public ObjectProperty<BigDecimal> consoffaltaProperty() {
        return consoffalta;
    }
    
    public void setConsoffalta(BigDecimal consoffalta) {
        this.consoffalta.set(consoffalta);
    }
    
    @Column(name = "CONSUMIDO_ESTOQUE")
    public BigDecimal getConsumidoestoque() {
        return consumidoestoque.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoestoqueProperty() {
        return consumidoestoque;
    }
    
    public void setConsumidoestoque(BigDecimal consumidoestoque) {
        this.consumidoestoque.set(consumidoestoque);
    }
    
    @Column(name = "CONSUMIDO_COMPRA")
    public BigDecimal getConsumidocompra() {
        return consumidocompra.get();
    }
    
    public ObjectProperty<BigDecimal> consumidocompraProperty() {
        return consumidocompra;
    }
    
    public void setConsumidocompra(BigDecimal consumidocompra) {
        this.consumidocompra.set(consumidocompra);
    }
    
    @Column(name = "CONSUMIDO_FALTA")
    public BigDecimal getConsumidofalta() {
        return consumidofalta.get();
    }
    
    public ObjectProperty<BigDecimal> consumidofaltaProperty() {
        return consumidofalta;
    }
    
    public void setConsumidofalta(BigDecimal consumidofalta) {
        this.consumidofalta.set(consumidofalta);
    }
    
    @Column(name = "CONS_ESTQ_ESTOQUE")
    public BigDecimal getConsestqestoque() {
        return consestqestoque.get();
    }
    
    public ObjectProperty<BigDecimal> consestqestoqueProperty() {
        return consestqestoque;
    }
    
    public void setConsestqestoque(BigDecimal consestqestoque) {
        this.consestqestoque.set(consestqestoque);
    }
    
    @Column(name = "CONS_ESTQ_COMPRA")
    public BigDecimal getConsestqcompra() {
        return consestqcompra.get();
    }
    
    public ObjectProperty<BigDecimal> consestqcompraProperty() {
        return consestqcompra;
    }
    
    public void setConsestqcompra(BigDecimal consestqcompra) {
        this.consestqcompra.set(consestqcompra);
    }
    
    @Column(name = "CONS_ESTQ_FALTA")
    public BigDecimal getConsestqfalta() {
        return consestqfalta.get();
    }
    
    public ObjectProperty<BigDecimal> consestqfaltaProperty() {
        return consestqfalta;
    }
    
    public void setConsestqfalta(BigDecimal consestqfalta) {
        this.consestqfalta.set(consestqfalta);
    }

    @Column(name = "ORI_MRP")
    public String getOrigemMrp() {
        return origemMrp.get();
    }

    public StringProperty origemMrpProperty() {
        return origemMrp;
    }

    public void setOrigemMrp(String origemMrp) {
        this.origemMrp.set(origemMrp);
    }
}