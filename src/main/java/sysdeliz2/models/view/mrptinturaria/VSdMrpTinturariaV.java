package sysdeliz2.models.view.mrptinturaria;

import javafx.beans.property.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Material;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;
import sysdeliz2.utils.hibernate.JPAUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "V_SD_MRP_TINTURARIA_V")
public class VSdMrpTinturariaV extends BasicModel {

    // db attributes
    private final StringProperty idjpa = new SimpleStringProperty();
    private final StringProperty statusmrp = new SimpleStringProperty();
    private final StringProperty numeroof = new SimpleStringProperty();
    private final StringProperty loteof = new SimpleStringProperty();
    private final StringProperty statusof = new SimpleStringProperty();
    private final StringProperty setorof = new SimpleStringProperty();
    private final StringProperty partesetor = new SimpleStringProperty();
    private final StringProperty pais = new SimpleStringProperty();
    private final StringProperty materialpai = new SimpleStringProperty();
    private final StringProperty cormaterialpai = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> qtdenec = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<Material> insumo = new SimpleObjectProperty<>();
    private final BooleanProperty principal = new SimpleBooleanProperty();
    private final ObjectProperty<Cor> corinsumo = new SimpleObjectProperty<>();
    private final BooleanProperty contrastante = new SimpleBooleanProperty();
    private final StringProperty grpcomprador = new SimpleStringProperty();
    private final StringProperty unidade = new SimpleStringProperty();
    private final StringProperty tipoinsumo = new SimpleStringProperty();
    private final StringProperty tonalidade = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> consumoof = new SimpleObjectProperty<BigDecimal>();
    private final ObjectProperty<BigDecimal> consumido = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty consumidode = new SimpleStringProperty();
    private final StringProperty deposito = new SimpleStringProperty();
    private final StringProperty ordcompra = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> dtentrega = new SimpleObjectProperty<LocalDate>();
    private final ObjectProperty<BigDecimal> consumoestq = new SimpleObjectProperty<BigDecimal>();
    private final StringProperty unidadeestq = new SimpleStringProperty();
    private final ObjectProperty<Entidade> forinsumo = new SimpleObjectProperty<>();
    private final BooleanProperty emBarca = new SimpleBooleanProperty();
    private final StringProperty origemMrp = new SimpleStringProperty();
    // transient attributes
    private final StringProperty materialNecessidade = new SimpleStringProperty();
    private final StringProperty corMaterialNecessidade = new SimpleStringProperty();
    private final StringProperty rolosPar = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> custoTinturaria = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty corTinturaria = new SimpleStringProperty();
    private final ObjectProperty<BigDecimal> saldoLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumoUnitario = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> pesoRolo = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumidoBarca = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> consumidoEstqBarca = new SimpleObjectProperty<>(BigDecimal.ZERO);

    public VSdMrpTinturariaV() {
    }

    public VSdMrpTinturariaV(Material insumo, Boolean principal) {
        this.insumo.set(insumo);
        this.principal.set(principal);
    }

    public VSdMrpTinturariaV(String materialpai, String cormaterialpai) {
        this.materialpai.set(materialpai);
        this.cormaterialpai.set(cormaterialpai);
    }
    
    public VSdMrpTinturariaV(Material materialFilho, Cor corMaterialFilho) {
        this.insumo.set(materialFilho);
        this.corinsumo.set(corMaterialFilho);
    }
    
    public VSdMrpTinturariaV(Material insumo, Boolean principal, Cor corinsumo, Boolean contrastante, Boolean emBarca, String tipoinsumo, String unidade, String unidadeestq, String deposito, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.principal.set(principal);
        this.corinsumo.set(corinsumo);
        this.contrastante.set(contrastante);
        this.tipoinsumo.set(tipoinsumo);
        this.deposito.set(deposito);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
        this.emBarca.set(emBarca);
    }

    public VSdMrpTinturariaV(Material insumo, Boolean principal, Cor corinsumo, Boolean contrastante, Boolean emBarca, String tipoinsumo, String unidade, String unidadeestq, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.principal.set(principal);
        this.corinsumo.set(corinsumo);
        this.contrastante.set(contrastante);
        this.tipoinsumo.set(tipoinsumo);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
        this.emBarca.set(emBarca);
    }
    
    public VSdMrpTinturariaV(String tipoinsumo, Material insumo, Boolean principal, Cor corinsumo, Boolean contrastante, Boolean emBarca, String unidade, String unidadeestq, String ordcompra, LocalDate dtentrega, Entidade forinsumo, String consumidode,
                                     BigDecimal consumido, BigDecimal consumoestq, BigDecimal consumoof) {
        this.insumo.set(insumo);
        this.principal.set(principal);
        this.corinsumo.set(corinsumo);
        this.contrastante.set(contrastante);
        this.tipoinsumo.set(tipoinsumo);
        this.ordcompra.set(ordcompra);
        this.dtentrega.set(dtentrega);
        this.forinsumo.set(forinsumo);
        this.consumidode.set(consumidode);
        this.unidade.set(unidade);
        this.unidadeestq.set(unidadeestq);
        this.consumido.set(consumido);
        this.consumoestq.set(consumoestq);
        this.consumoof.set(consumoof);
        this.emBarca.set(emBarca);
    }
    
    @Id
    @Column(name = "ID_JPA")
    public String getIdjpa() {
        return idjpa.get();
    }
    
    public StringProperty idjpaProperty() {
        return idjpa;
    }
    
    public void setIdjpa(String idjpa) {
        this.idjpa.set(idjpa);
    }
    
    @Column(name = "STATUS_MRP")
    public String getStatusmrp() {
        return statusmrp.get();
    }
    
    public StringProperty statusmrpProperty() {
        return statusmrp;
    }
    
    public void setStatusmrp(String statusmrp) {
        this.statusmrp.set(statusmrp);
    }
    
    @Column(name = "NUMERO_OF")
    public String getNumeroof() {
        return numeroof.get();
    }
    
    public StringProperty numeroofProperty() {
        return numeroof;
    }
    
    public void setNumeroof(String numeroof) {
        this.numeroof.set(numeroof);
    }
    
    @Column(name = "LOTE_OF")
    public String getLoteof() {
        return loteof.get();
    }
    
    public StringProperty loteofProperty() {
        return loteof;
    }
    
    public void setLoteof(String loteof) {
        this.loteof.set(loteof);
    }
    
    @Column(name = "STATUS_OF")
    public String getStatusof() {
        return statusof.get();
    }
    
    public StringProperty statusofProperty() {
        return statusof;
    }
    
    public void setStatusof(String statusof) {
        this.statusof.set(statusof);
    }
    
    @Column(name = "SETOR_OF")
    public String getSetorof() {
        return setorof.get();
    }
    
    public StringProperty setorofProperty() {
        return setorof;
    }
    
    public void setSetorof(String setorof) {
        this.setorof.set(setorof);
    }
    
    @Column(name = "PARTE_SETOR")
    public String getPartesetor() {
        return partesetor.get();
    }
    
    public StringProperty partesetorProperty() {
        return partesetor;
    }
    
    public void setPartesetor(String partesetor) {
        this.partesetor.set(partesetor);
    }
    
    @Column(name = "PAIS")
    public String getPais() {
        return pais.get();
    }
    
    public StringProperty paisProperty() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    
    @Column(name = "MATERIAL_PAI")
    public String getMaterialpai() {
        return materialpai.get();
    }
    
    public StringProperty materialpaiProperty() {
        return materialpai;
    }
    
    public void setMaterialpai(String materialpai) {
        this.materialpai.set(materialpai);
    }
    
    @Column(name = "COR_MATERIAL_PAI")
    public String getCormaterialpai() {
        return cormaterialpai.get();
    }
    
    public StringProperty cormaterialpaiProperty() {
        return cormaterialpai;
    }
    
    public void setCormaterialpai(String cormaterialpai) {
        this.cormaterialpai.set(cormaterialpai);
    }
    
    @Column(name = "QTDE_NEC")
    public BigDecimal getQtdenec() {
        return qtdenec.get();
    }
    
    public ObjectProperty<BigDecimal> qtdenecProperty() {
        return qtdenec;
    }
    
    public void setQtdenec(BigDecimal qtdenec) {
        this.qtdenec.set(qtdenec);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSUMO")
    public Material getInsumo() {
        return insumo.get();
    }
    
    public ObjectProperty<Material> insumoProperty() {
        return insumo;
    }
    
    public void setInsumo(Material insumo) {
        this.insumo.set(insumo);
    }

    @Column(name = "PRINCIPAL")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isPrincipal() {
        return principal.get();
    }

    public BooleanProperty principalProperty() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal.set(principal);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COR_INSUMO")
    public Cor getCorinsumo() {
        return corinsumo.get();
    }
    
    public ObjectProperty<Cor> corinsumoProperty() {
        return corinsumo;
    }
    
    public void setCorinsumo(Cor corinsumo) {
        this.corinsumo.set(corinsumo);
    }

    @Column(name = "CONTRAST")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isContrastante() {
        return contrastante.get();
    }

    public BooleanProperty contrastanteProperty() {
        return contrastante;
    }

    public void setContrastante(boolean contrastante) {
        this.contrastante.set(contrastante);
    }

    @Column(name = "GRP_COMPRADOR")
    public String getGrpcomprador() {
        return grpcomprador.get();
    }
    
    public StringProperty grpcompradorProperty() {
        return grpcomprador;
    }
    
    public void setGrpcomprador(String grpcomprador) {
        this.grpcomprador.set(grpcomprador);
    }
    
    @Column(name = "UNIDADE")
    public String getUnidade() {
        return unidade.get();
    }
    
    public StringProperty unidadeProperty() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade.set(unidade);
    }
    
    @Column(name = "TIPO_INSUMO")
    public String getTipoinsumo() {
        return tipoinsumo.get();
    }
    
    public StringProperty tipoinsumoProperty() {
        return tipoinsumo;
    }
    
    public void setTipoinsumo(String tipoinsumo) {
        this.tipoinsumo.set(tipoinsumo);
    }
    
    @Column(name = "TONALIDADE")
    public String getTonalidade() {
        return tonalidade.get();
    }
    
    public StringProperty tonalidadeProperty() {
        return tonalidade;
    }
    
    public void setTonalidade(String tonalidade) {
        this.tonalidade.set(tonalidade);
    }
    
    @Column(name = "CONSUMO_OF")
    public BigDecimal getConsumoof() {
        return consumoof.get();
    }
    
    public ObjectProperty<BigDecimal> consumoofProperty() {
        return consumoof;
    }
    
    public void setConsumoof(BigDecimal consumoof) {
        this.consumoof.set(consumoof);
    }
    
    @Column(name = "CONSUMIDO")
    public BigDecimal getConsumido() {
        return consumido.get();
    }
    
    public ObjectProperty<BigDecimal> consumidoProperty() {
        return consumido;
    }
    
    public void setConsumido(BigDecimal consumido) {
        this.consumido.set(consumido);
    }
    
    @Column(name = "CONSUMIDO_DE")
    public String getConsumidode() {
        return consumidode.get();
    }
    
    public StringProperty consumidodeProperty() {
        return consumidode;
    }
    
    public void setConsumidode(String consumidode) {
        this.consumidode.set(consumidode);
    }
    
    @Column(name = "DEPOSITO")
    public String getDeposito() {
        return deposito.get();
    }
    
    public StringProperty depositoProperty() {
        return deposito;
    }
    
    public void setDeposito(String deposito) {
        this.deposito.set(deposito);
    }
    
    @Column(name = "ORD_COMPRA")
    public String getOrdcompra() {
        return ordcompra.get();
    }
    
    public StringProperty ordcompraProperty() {
        return ordcompra;
    }
    
    public void setOrdcompra(String ordcompra) {
        this.ordcompra.set(ordcompra);
    }
    
    @Column(name = "DT_ENTREGA")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtentrega() {
        return dtentrega.get();
    }
    
    public ObjectProperty<LocalDate> dtentregaProperty() {
        return dtentrega;
    }
    
    public void setDtentrega(LocalDate dtentrega) {
        this.dtentrega.set(dtentrega);
    }
    
    @Column(name = "CONSUMO_ESTQ")
    public BigDecimal getConsumoestq() {
        return consumoestq.get();
    }
    
    public ObjectProperty<BigDecimal> consumoestqProperty() {
        return consumoestq;
    }
    
    public void setConsumoestq(BigDecimal consumoestq) {
        this.consumoestq.set(consumoestq);
    }
    
    @Column(name = "UNIDADE_ESTQ")
    public String getUnidadeestq() {
        return unidadeestq.get();
    }
    
    public StringProperty unidadeestqProperty() {
        return unidadeestq;
    }
    
    public void setUnidadeestq(String unidadeestq) {
        this.unidadeestq.set(unidadeestq);
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOR_INSUMO")
    public Entidade getForinsumo() {
        return forinsumo.get();
    }
    
    public ObjectProperty<Entidade> forinsumoProperty() {
        return forinsumo;
    }
    
    public void setForinsumo(Entidade forinsumo) {
        this.forinsumo.set(forinsumo);
    }

    @Column(name = "EM_BARCA")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isEmBarca() {
        return emBarca.get();
    }

    public BooleanProperty emBarcaProperty() {
        return emBarca;
    }

    public void setEmBarca(boolean emBarca) {
        this.emBarca.set(emBarca);
    }

    @Column(name = "ORI_MRP")
    public String getOrigemMrp() {
        return origemMrp.get();
    }

    public StringProperty origemMrpProperty() {
        return origemMrp;
    }

    public void setOrigemMrp(String origemMrp) {
        this.origemMrp.set(origemMrp);
    }

    @Transient
    public String getMaterialNecessidade() {
        return materialNecessidade.get();
    }

    public StringProperty materialNecessidadeProperty() {
        return materialNecessidade;
    }

    public void setMaterialNecessidade(String materialNecessidade) {
        this.materialNecessidade.set(materialNecessidade);
    }

    @Transient
    public String getCorMaterialNecessidade() {
        return corMaterialNecessidade.get();
    }

    public StringProperty corMaterialNecessidadeProperty() {
        return corMaterialNecessidade;
    }

    public void setCorMaterialNecessidade(String corMaterialNecessidade) {
        this.corMaterialNecessidade.set(corMaterialNecessidade);
    }

    @Transient
    public String getRolosPar() {
        return rolosPar.get();
    }

    public StringProperty rolosParProperty() {
        return rolosPar;
    }

    public void setRolosPar(String rolosPar) {
        this.rolosPar.set(rolosPar);
    }

    @Transient
    public BigDecimal getCustoTinturaria() {
        return custoTinturaria.get();
    }

    public ObjectProperty<BigDecimal> custoTinturariaProperty() {
        return custoTinturaria;
    }

    public void setCustoTinturaria(BigDecimal custoTinturaria) {
        this.custoTinturaria.set(custoTinturaria);
    }

    @Transient
    public String getCorTinturaria() {
        return corTinturaria.get();
    }

    public StringProperty corTinturariaProperty() {
        return corTinturaria;
    }

    public void setCorTinturaria(String corTinturaria) {
        this.corTinturaria.set(corTinturaria);
    }

    @Transient
    public BigDecimal getSaldoLotes() {
        return saldoLotes.get();
    }

    public ObjectProperty<BigDecimal> saldoLotesProperty() {
        return saldoLotes;
    }

    public void setSaldoLotes(BigDecimal saldoLotes) {
        this.saldoLotes.set(saldoLotes);
    }

    @Transient
    public BigDecimal getConsumoUnitario() {
        return consumoUnitario.get();
    }

    public ObjectProperty<BigDecimal> consumoUnitarioProperty() {
        return consumoUnitario;
    }

    public void setConsumoUnitario(BigDecimal consumoUnitario) {
        this.consumoUnitario.set(consumoUnitario);
    }

    @Transient
    public BigDecimal getPesoRolo() {
        return pesoRolo.get();
    }

    public ObjectProperty<BigDecimal> pesoRoloProperty() {
        return pesoRolo;
    }

    public void setPesoRolo(BigDecimal pesoRolo) {
        this.pesoRolo.set(pesoRolo);
    }

    @Transient
    public BigDecimal getConsumidoBarca() {
        return consumidoBarca.get();
    }

    public ObjectProperty<BigDecimal> consumidoBarcaProperty() {
        return consumidoBarca;
    }

    public void setConsumidoBarca(BigDecimal consumidoBarca) {
        this.consumidoBarca.set(consumidoBarca);
    }

    @Transient
    public BigDecimal getConsumidoEstqBarca() {
        return consumidoEstqBarca.get();
    }

    public ObjectProperty<BigDecimal> consumidoEstqBarcaProperty() {
        return consumidoEstqBarca;
    }

    public void setConsumidoEstqBarca(BigDecimal consumidoEstqBarca) {
        this.consumidoEstqBarca.set(consumidoEstqBarca);
    }

    @Transient
    public final void refresh() {
        JPAUtils.getEntityManager().refresh(this);
    }
}