package sysdeliz2.models.view.pcp;

import javafx.beans.property.*;
import org.hibernate.annotations.*;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "v_sd_demanda_faccao")
public class VSdDemandaFaccao implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final BooleanProperty setorAtual = new SimpleBooleanProperty();
        private final BooleanProperty emProducao = new SimpleBooleanProperty();

        public Pk() {
        }

        @OneToOne
        @JoinColumn(name = "faccao")
        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }

        @OneToOne
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @Column(name = "setor_atual")
        @Convert(converter = BooleanAttributeConverter.class)
        public boolean isSetorAtual() {
            return setorAtual.get();
        }

        public BooleanProperty setorAtualProperty() {
            return setorAtual;
        }

        public void setSetorAtual(boolean setorAtual) {
            this.setorAtual.set(setorAtual);
        }

        @Column(name = "em_producao")
        @Convert(converter = BooleanAttributeConverter.class)
        public boolean isEmProducao() {
            return emProducao.get();
        }

        public BooleanProperty emProducaoProperty() {
            return emProducao;
        }

        public void setEmProducao(boolean emProducao) {
            this.emProducao.set(emProducao);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty ordemProduc = new SimpleIntegerProperty();
    private final BooleanProperty atrasado = new SimpleBooleanProperty();
    private final IntegerProperty diasAtraso = new SimpleIntegerProperty();
    private final BooleanProperty atrasadoOrig = new SimpleBooleanProperty();
    private final IntegerProperty diasAtrasoOrig = new SimpleIntegerProperty();
    private final IntegerProperty qtdePecas = new SimpleIntegerProperty();
    private final ObjectProperty<BigDecimal> leadDemanda = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> tempoDemanda = new SimpleObjectProperty<>();
    private final ObjectProperty<BigDecimal> capacidadeRecurso = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDemandaFaccao> demandaProxima = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> ultimoRetorno = new SimpleObjectProperty<>();

    public VSdDemandaFaccao() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "ordem_produc")
    public int getOrdemProduc() {
        return ordemProduc.get();
    }

    public IntegerProperty ordemProducProperty() {
        return ordemProduc;
    }

    public void setOrdemProduc(int ordemProduc) {
        this.ordemProduc.set(ordemProduc);
    }

    @Column(name = "atrasado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtrasado() {
        return atrasado.get();
    }

    public BooleanProperty atrasadoProperty() {
        return atrasado;
    }

    public void setAtrasado(boolean atrasado) {
        this.atrasado.set(atrasado);
    }

    @Column(name = "dd_atraso")
    public int getDiasAtraso() {
        return diasAtraso.get();
    }

    public IntegerProperty diasAtrasoProperty() {
        return diasAtraso;
    }

    public void setDiasAtraso(int diasAtraso) {
        this.diasAtraso.set(diasAtraso);
    }

    @Column(name = "atrasado_orig")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtrasadoOrig() {
        return atrasadoOrig.get();
    }

    public BooleanProperty atrasadoOrigProperty() {
        return atrasadoOrig;
    }

    public void setAtrasadoOrig(boolean atrasadoOrig) {
        this.atrasadoOrig.set(atrasadoOrig);
    }

    @Column(name = "dd_atraso_orig")
    public int getDiasAtrasoOrig() {
        return diasAtrasoOrig.get();
    }

    public IntegerProperty diasAtrasoOrigProperty() {
        return diasAtrasoOrig;
    }

    public void setDiasAtrasoOrig(int diasAtrasoOrig) {
        this.diasAtrasoOrig.set(diasAtrasoOrig);
    }

    @Column(name = "qtde_pc")
    public int getQtdePecas() {
        return qtdePecas.get();
    }

    public IntegerProperty qtdePecasProperty() {
        return qtdePecas;
    }

    public void setQtdePecas(int qtdePecas) {
        this.qtdePecas.set(qtdePecas);
    }

    @Column(name = "lead_demanda")
    public BigDecimal getLeadDemanda() {
        return leadDemanda.get();
    }

    public ObjectProperty<BigDecimal> leadDemandaProperty() {
        return leadDemanda;
    }

    public void setLeadDemanda(BigDecimal leadDemanda) {
        this.leadDemanda.set(leadDemanda);
    }

    @Column(name = "tempo_demanda")
    public BigDecimal getTempoDemanda() {
        return tempoDemanda.get();
    }

    public ObjectProperty<BigDecimal> tempoDemandaProperty() {
        return tempoDemanda;
    }

    public void setTempoDemanda(BigDecimal tempoDemanda) {
        this.tempoDemanda.set(tempoDemanda);
    }

    @Column(name = "capacidade_rec")
    public BigDecimal getCapacidadeRecurso() {
        return capacidadeRecurso.get();
    }

    public ObjectProperty<BigDecimal> capacidadeRecursoProperty() {
        return capacidadeRecurso;
    }

    public void setCapacidadeRecurso(BigDecimal capacidadeRecurso) {
        this.capacidadeRecurso.set(capacidadeRecurso);
    }

    @Column(name = "ult_retorno")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getUltimoRetorno() {
        return ultimoRetorno.get();
    }

    public ObjectProperty<LocalDate> ultimoRetornoProperty() {
        return ultimoRetorno;
    }

    public void setUltimoRetorno(LocalDate ultimoRetorno) {
        this.ultimoRetorno.set(ultimoRetorno);
    }

    @Transient
    public VSdDemandaFaccao getDemandaProxima() {
        return demandaProxima.get();
    }

    public ObjectProperty<VSdDemandaFaccao> demandaProximaProperty() {
        return demandaProxima;
    }

    public void setDemandaProxima(VSdDemandaFaccao demandaProxima) {
        this.demandaProxima.set(demandaProxima);
    }

    @PostLoad
    private void postLoad() {
        if (getId().isSetorAtual())
            setDemandaProxima(new FluentDao().selectFrom(VSdDemandaFaccao.class)
                    .where(eb -> eb.equal("id.faccao.codcli", getId().getFaccao().getCodcli())
                            .equal("id.setor.codigo", getId().getSetor().getCodigo())
                            .equal("id.emProducao", getId().isEmProducao())
                            .equal("id.setorAtual", false))
                    .singleResult());
    }
}
