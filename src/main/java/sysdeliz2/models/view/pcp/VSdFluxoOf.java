package sysdeliz2.models.view.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "v_sd_fluxo_of")
public class VSdFluxoOf implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<Of1> numero = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final ObjectProperty<LocalDate> dtEnvio = new SimpleObjectProperty<>();
        private final BooleanProperty setorAtual = new SimpleBooleanProperty();

        public Pk() {
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "numero")
        public Of1 getNumero() {
            return numero.get();
        }

        public ObjectProperty<Of1> numeroProperty() {
            return numero;
        }

        public void setNumero(Of1 numero) {
            this.numero.set(numero);
        }

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @Column(name = "dt_envio")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getDtEnvio() {
            return dtEnvio.get();
        }

        public ObjectProperty<LocalDate> dtEnvioProperty() {
            return dtEnvio;
        }

        public void setDtEnvio(LocalDate dtEnvio) {
            this.dtEnvio.set(dtEnvio);
        }

        @Column(name = "setor_atual")
        @Convert(converter = BooleanAttributeConverter.class)
        public boolean isSetorAtual() {
            return setorAtual.get();
        }

        public BooleanProperty setorAtualProperty() {
            return setorAtual;
        }

        public void setSetorAtual(boolean setorAtual) {
            this.setorAtual.set(setorAtual);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty leadSetor = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtRetorno = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeOf = new SimpleIntegerProperty();

    public VSdFluxoOf() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "lead_setor")
    public int getLeadSetor() {
        return leadSetor.get();
    }

    public IntegerProperty leadSetorProperty() {
        return leadSetor;
    }

    public void setLeadSetor(int leadSetor) {
        this.leadSetor.set(leadSetor);
    }

    @Column(name = "dt_retorno")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRetorno() {
        return dtRetorno.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProperty() {
        return dtRetorno;
    }

    public void setDtRetorno(LocalDate dtRetorno) {
        this.dtRetorno.set(dtRetorno);
    }

    @Column(name = "qtde_of")
    public int getQtdeOf() {
        return qtdeOf.get();
    }

    public IntegerProperty qtdeOfProperty() {
        return qtdeOf;
    }

    public void setQtdeOf(int qtdeOf) {
        this.qtdeOf.set(qtdeOf);
    }
}
