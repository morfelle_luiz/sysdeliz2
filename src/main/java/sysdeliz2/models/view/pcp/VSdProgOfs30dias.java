package sysdeliz2.models.view.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.converters.BooleanAttributeConverter;
import sysdeliz2.utils.converters.LocalDateAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "v_sd_prog_ofs_30_dias")
public class VSdProgOfs30dias implements Serializable {

    @Embeddable
    public static class Pk implements Serializable {

        private final ObjectProperty<LocalDate> data = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();
        private final StringProperty numero = new SimpleStringProperty();

        public Pk() {
        }

        @Column(name = "data")
        @Convert(converter = LocalDateAttributeConverter.class)
        public LocalDate getData() {
            return data.get();
        }

        public ObjectProperty<LocalDate> dataProperty() {
            return data;
        }

        public void setData(LocalDate data) {
            this.data.set(data);
        }

        @OneToOne
        @JoinColumn(name = "setor")
        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        @OneToOne
        @JoinColumn(name = "faccao")
        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }

        @Column(name = "numero")
        public String getNumero() {
            return numero.get();
        }

        public StringProperty numeroProperty() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero.set(numero);
        }
    }

    private final ObjectProperty<Pk> id = new SimpleObjectProperty<>();
    private final BooleanProperty diaUtil = new SimpleBooleanProperty();
    private final IntegerProperty semana = new SimpleIntegerProperty();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final IntegerProperty leadSetor = new SimpleIntegerProperty();
    private final ObjectProperty<LocalDate> dtEnvio = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRetorno = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> dtRetornoProg = new SimpleObjectProperty<>();
    private final IntegerProperty qtdeOf = new SimpleIntegerProperty();
    private final BooleanProperty setorAtual = new SimpleBooleanProperty();
    private final BooleanProperty atrasado = new SimpleBooleanProperty();
    private final StringProperty tipoOf = new SimpleStringProperty();
    private final IntegerProperty diasAtraso = new SimpleIntegerProperty();

    public VSdProgOfs30dias() {
    }

    @EmbeddedId
    public Pk getId() {
        return id.get();
    }

    public ObjectProperty<Pk> idProperty() {
        return id;
    }

    public void setId(Pk id) {
        this.id.set(id);
    }

    @Column(name = "dia_util")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isDiaUtil() {
        return diaUtil.get();
    }

    public BooleanProperty diaUtilProperty() {
        return diaUtil;
    }

    public void setDiaUtil(boolean diaUtil) {
        this.diaUtil.set(diaUtil);
    }

    @Column(name = "semana")
    public int getSemana() {
        return semana.get();
    }

    public IntegerProperty semanaProperty() {
        return semana;
    }

    public void setSemana(int semana) {
        this.semana.set(semana);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @Column(name = "lead_setor")
    public int getLeadSetor() {
        return leadSetor.get();
    }

    public IntegerProperty leadSetorProperty() {
        return leadSetor;
    }

    public void setLeadSetor(int leadSetor) {
        this.leadSetor.set(leadSetor);
    }

    @Column(name = "dt_envio")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtEnvio() {
        return dtEnvio.get();
    }

    public ObjectProperty<LocalDate> dtEnvioProperty() {
        return dtEnvio;
    }

    public void setDtEnvio(LocalDate dtEnvio) {
        this.dtEnvio.set(dtEnvio);
    }

    @Column(name = "dt_retorno")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRetorno() {
        return dtRetorno.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProperty() {
        return dtRetorno;
    }

    public void setDtRetorno(LocalDate dtRetorno) {
        this.dtRetorno.set(dtRetorno);
    }

    @Column(name = "dt_retorno_prog")
    @Convert(converter = LocalDateAttributeConverter.class)
    public LocalDate getDtRetornoProg() {
        return dtRetornoProg.get();
    }

    public ObjectProperty<LocalDate> dtRetornoProgProperty() {
        return dtRetornoProg;
    }

    public void setDtRetornoProg(LocalDate dtRetornoProg) {
        this.dtRetornoProg.set(dtRetornoProg);
    }

    @Column(name = "qtde_of")
    public int getQtdeOf() {
        return qtdeOf.get();
    }

    public IntegerProperty qtdeOfProperty() {
        return qtdeOf;
    }

    public void setQtdeOf(int qtdeOf) {
        this.qtdeOf.set(qtdeOf);
    }

    @Column(name = "setor_atual")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isSetorAtual() {
        return setorAtual.get();
    }

    public BooleanProperty setorAtualProperty() {
        return setorAtual;
    }

    public void setSetorAtual(boolean setorAtual) {
        this.setorAtual.set(setorAtual);
    }

    @Column(name = "atrasado")
    @Convert(converter = BooleanAttributeConverter.class)
    public boolean isAtrasado() {
        return atrasado.get();
    }

    public BooleanProperty atrasadoProperty() {
        return atrasado;
    }

    public void setAtrasado(boolean atrasado) {
        this.atrasado.set(atrasado);
    }

    @Column(name = "tipo_op")
    public String getTipoOf() {
        return tipoOf.get();
    }

    public StringProperty tipoOfProperty() {
        return tipoOf;
    }

    public void setTipoOf(String tipoOf) {
        this.tipoOf.set(tipoOf);
    }

    @Column(name = "dd_atraso")
    public int getDiasAtraso() {
        return diasAtraso.get();
    }

    public IntegerProperty diasAtrasoProperty() {
        return diasAtraso;
    }

    public void setDiasAtraso(int diasAtraso) {
        this.diasAtraso.set(diasAtraso);
    }
}
