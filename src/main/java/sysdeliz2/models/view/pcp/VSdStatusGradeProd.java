package sysdeliz2.models.view.pcp;

import javafx.beans.property.*;
import sysdeliz2.models.sysdeliz.Inventario.SdStatusInventario;
import sysdeliz2.models.sysdeliz.SdStatusProduto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "v_sd_status_grade_prod")
public class VSdStatusGradeProd implements Serializable {

    @Embeddable
    public static class VSdStatusGradeProdPK implements Serializable {

        private final ObjectProperty<VStatusProdutoCor> produtoCor = new SimpleObjectProperty<>();
        private final StringProperty tam = new SimpleStringProperty();

        public VSdStatusGradeProdPK() {
        }

        @ManyToOne
        @JoinColumns({
                @JoinColumn(name = "codigo", insertable = false, updatable = false, referencedColumnName = "codigo"),
                @JoinColumn(name = "cor", insertable = false, updatable = false, referencedColumnName = "cor")
        })
        public VStatusProdutoCor getProdutoCor() {
            return produtoCor.get();
        }

        public ObjectProperty<VStatusProdutoCor> produtoCorProperty() {
            return produtoCor;
        }

        public void setProdutoCor(VStatusProdutoCor produtoCor) {
            this.produtoCor.set(produtoCor);
        }

        @Column(name = "tam")
        public String getTam() {
            return tam.get();
        }

        public StringProperty tamProperty() {
            return tam;
        }

        public void setTam(String tam) {
            this.tam.set(tam);
        }
    }

    private final ObjectProperty<VSdStatusGradeProdPK> id = new SimpleObjectProperty<>();
    private final IntegerProperty ordem = new SimpleIntegerProperty();
    private final ObjectProperty<SdStatusProduto> statusPcp = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusProduto> statusComercial = new SimpleObjectProperty<>();

    public VSdStatusGradeProd() {
    }

    @EmbeddedId
    public VSdStatusGradeProdPK getId() {
        return id.get();
    }

    public ObjectProperty<VSdStatusGradeProdPK> idProperty() {
        return id;
    }

    public void setId(VSdStatusGradeProdPK id) {
        this.id.set(id);
    }

    @Column(name = "ordem")
    public int getOrdem() {
        return ordem.get();
    }

    public IntegerProperty ordemProperty() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem.set(ordem);
    }

    @OneToOne
    @JoinColumn(name = "status_pcp")
    public SdStatusProduto getStatusPcp() {
        return statusPcp.get();
    }

    public ObjectProperty<SdStatusProduto> statusPcpProperty() {
        return statusPcp;
    }

    public void setStatusPcp(SdStatusProduto statusPcp) {
        this.statusPcp.set(statusPcp);
    }

    @OneToOne
    @JoinColumn(name = "status_comercial")
    public SdStatusProduto getStatusComercial() {
        return statusComercial.get();
    }

    public ObjectProperty<SdStatusProduto> statusComercialProperty() {
        return statusComercial;
    }

    public void setStatusComercial(SdStatusProduto statusComercial) {
        this.statusComercial.set(statusComercial);
    }
}
