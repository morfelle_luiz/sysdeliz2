package sysdeliz2.models.view.pcp;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdStatusProduto;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Produto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "vproduto_cor")
public class VStatusProdutoCor implements Serializable {

    @Embeddable
    public static class VProdutoCorPK implements Serializable {

        private final ObjectProperty<Produto> produto = new SimpleObjectProperty<>();
        private final ObjectProperty<Cor> cor = new SimpleObjectProperty<>();

        public VProdutoCorPK() {
        }

        @OneToOne
        @JoinColumn(name = "codigo")
        public Produto getProduto() {
            return produto.get();
        }

        public ObjectProperty<Produto> produtoProperty() {
            return produto;
        }

        public void setProduto(Produto produto) {
            this.produto.set(produto);
        }

        @OneToOne
        @JoinColumn(name = "cor")
        public Cor getCor() {
            return cor.get();
        }

        public ObjectProperty<Cor> corProperty() {
            return cor;
        }

        public void setCor(Cor cor) {
            this.cor.set(cor);
        }
    }

    private final ObjectProperty<VProdutoCorPK> id = new SimpleObjectProperty<>();
    private final StringProperty sku = new SimpleStringProperty();
    private List<VSdStatusGradeProd> statusGrade = new ArrayList<>();
    private final ObjectProperty<SdStatusProduto> statusComercial = new SimpleObjectProperty<>();
    private final ObjectProperty<SdStatusProduto> statusPcp = new SimpleObjectProperty<>();

    public VStatusProdutoCor() {
    }

    @EmbeddedId
    public VProdutoCorPK getId() {
        return id.get();
    }

    public ObjectProperty<VProdutoCorPK> idProperty() {
        return id;
    }

    public void setId(VProdutoCorPK id) {
        this.id.set(id);
    }

    @Column(name = "sku")
    public String getSku() {
        return sku.get();
    }

    public StringProperty skuProperty() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku.set(sku);
    }
    @OneToMany(mappedBy = "id.produtoCor")
    @OrderBy("ordem ASC")

    public List<VSdStatusGradeProd> getStatusGrade() {
        return statusGrade;
    }

    public void setStatusGrade(List<VSdStatusGradeProd> statusGrade) {
        this.statusGrade = statusGrade;
    }

    @Transient
    public SdStatusProduto getStatusComercial() {
        return statusComercial.get();
    }

    public ObjectProperty<SdStatusProduto> statusComercialProperty() {
        return statusComercial;
    }

    public void setStatusComercial(SdStatusProduto statusComercial) {
        this.statusComercial.set(statusComercial);
    }

    @Transient
    public SdStatusProduto getStatusPcp() {
        return statusPcp.get();
    }

    public ObjectProperty<SdStatusProduto> statusPcpProperty() {
        return statusPcp;
    }

    public void setStatusPcp(SdStatusProduto statusPcp) {
        this.statusPcp.set(statusPcp);
    }

    @PostLoad
    private void postLoad() {

        List<SdStatusProduto> statusPcp = statusGrade.stream()
                .map(grade -> grade.getStatusPcp())
                .distinct()
                .collect(Collectors.toList());
        List<SdStatusProduto> statusComercial = statusGrade.stream()
                .map(grade -> grade.getStatusComercial())
                .distinct()
                .collect(Collectors.toList());
        SdStatusProduto _STATUS_PRODUTO_LIMITADO = new FluentDao().selectFrom(SdStatusProduto.class).where(eb -> eb.equal("codigo", "2")).singleResult();
        setStatusComercial(statusComercial.stream().filter(grade -> grade.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO :
                statusComercial.stream().allMatch(item -> item.getOrdem() == 0) ? statusComercial.get(0) : statusComercial.stream().filter(status -> status.getOrdem() != 0).findFirst().get());
        setStatusPcp(statusPcp.stream().filter(grade -> grade.getOrdem() != 0).count() > 1 ? _STATUS_PRODUTO_LIMITADO :
                statusPcp.stream().allMatch(item -> item.getOrdem() == 0) ? statusPcp.get(0) : statusPcp.stream().filter(status -> status.getOrdem() != 0).findFirst().get());

    }
}