/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.jasperreports.engine.JasperReport;

import java.util.ArrayList;

/**
 *
 * @author cristiano.diego
 */
public class AcessoSistema {

    private final StringProperty nome = new SimpleStringProperty();
    private final StringProperty apelido = new SimpleStringProperty();
    private final StringProperty usuario = new SimpleStringProperty();
    private final StringProperty usuarioTi = new SimpleStringProperty();
    private final StringProperty codFunTi = new SimpleStringProperty();
    private final StringProperty codUsuarioTi = new SimpleStringProperty();
    private final StringProperty displayName = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty telefone = new SimpleStringProperty();
    private final StringProperty horaAcesso = new SimpleStringProperty();
    private final StringProperty dataAcesso = new SimpleStringProperty();
    private final BooleanProperty isSistemaLiberado = new SimpleBooleanProperty(false);
    private final ListProperty<String> listPermissoesUsuario = new SimpleListProperty<String>();
    private final ListProperty<JasperReport> listJaspersSistema = new SimpleListProperty<>();

    public AcessoSistema(String pNome, String pApelido, String pUsuario, String pEmail, String pHora, String pData, String pDisplay, String pTelefone) {
        this.listJaspersSistema.set(FXCollections.observableList(new ArrayList()));
        this.dataAcesso.set(pData);
        this.email.set(pEmail);
        this.horaAcesso.set(pHora);
        this.usuario.set(pUsuario);
        this.nome.set(pNome);
        this.apelido.set(pApelido);
        this.displayName.set(pDisplay);
        this.telefone.set(pTelefone);
    }
    
    public AcessoSistema(String usuario) {
        this.usuario.set(usuario);
    }

    public final String getNome() {
        return nome.get();
    }

    public final void setNome(String value) {
        nome.set(value);
    }

    public StringProperty nomeProperty() {
        return nome;
    }

    public final String getUsuario() {
        return usuario.get();
    }

    public final void setUsuario(String value) {
        usuario.set(value);
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public final String getEmail() {
        return email.get();
    }

    public final void setEmail(String value) {
        email.set(value);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public final String getHoraAcesso() {
        return horaAcesso.get();
    }

    public final void setHoraAcesso(String value) {
        horaAcesso.set(value);
    }

    public StringProperty horaAcessoProperty() {
        return horaAcesso;
    }

    public final String getDataAcesso() {
        return dataAcesso.get();
    }

    public final void setDataAcesso(String value) {
        dataAcesso.set(value);
    }

    public StringProperty dataAcessoProperty() {
        return dataAcesso;
    }

    public final Boolean getIsSistemaLiberado() {
        return isSistemaLiberado.get();
    }

    public final void setIsSistemaLiberado(Boolean value) {
        isSistemaLiberado.set(value);
    }

    public BooleanProperty isSistemaLiberadoProperty() {
        return isSistemaLiberado;
    }

    public final ObservableList<JasperReport> getListJaspersSistema() {
        return listJaspersSistema.get();
    }

    public final void setListJaspersSistema(ObservableList<JasperReport> value) {
        listJaspersSistema.set(value);
    }

    public ListProperty<JasperReport> listJaspersSistemaProperty() {
        return listJaspersSistema;
    }

    public final String getApelido() {
        return apelido.get();
    }

    public final void setApelido(String value) {
        apelido.set(value);
    }

    public StringProperty apelidoProperty() {
        return apelido;
    }

    public final boolean isIsSistemaLiberado() {
        return isSistemaLiberado.get();
    }

    public final String getDisplayName() {
        return displayName.get();
    }

    public final void setDisplayName(String value) {
        displayName.set(value);
    }

    public StringProperty displayNameProperty() {
        return displayName;
    }

    public final String getUsuarioTi() {
        return usuarioTi.get();
    }

    public final void setUsuarioTi(String value) {
        usuarioTi.set(value);
    }

    public StringProperty usuarioTiProperty() {
        return usuarioTi;
    }

    public final String getCodFunTi() {
        return codFunTi.get();
    }

    public final void setCodFunTi(String value) {
        codFunTi.set(value);
    }

    public StringProperty codFunTiProperty() {
        return codFunTi;
    }

    public final String getCodUsuarioTi() {
        return codUsuarioTi.get();
    }

    public final void setCodUsuarioTi(String value) {
        codUsuarioTi.set(value);
    }

    public StringProperty codUsuarioTiProperty() {
        return codUsuarioTi;
    }

    public final ObservableList<String> getListPermissoesUsuario() {
        return listPermissoesUsuario.get();
    }

    public final void setListPermissoesUsuario(ObservableList<String> value) {
        listPermissoesUsuario.set(value);
    }

    public ListProperty<String> listPermissoesUsuarioProperty() {
        return listPermissoesUsuario;
    }
    
    public String getTelefone() {
        return telefone.get();
    }
    
    public StringProperty telefoneProperty() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone.set(telefone);
    }
    
    @Override
    public String toString() {
        return "AcessoSistema{" + "strNomeUsuario=" + nome + ", strApelidoUsuario=" + apelido + ", strLoginUsuario=" + usuario + ", strUsuarioTi=" + usuarioTi + ", strCodFunTi=" + codFunTi + ", strCodUsuarioTi=" + codUsuarioTi + ", strDisplayName=" + displayName + ", strEmailUsuario=" + email + ", strHoraAcessoUsuario=" + horaAcesso + ", strDataAcessoUsuario=" + dataAcesso + ", isSistemaLiberado=" + isSistemaLiberado + ", listJaspersSistema=" + listJaspersSistema + '}';
    }

}
