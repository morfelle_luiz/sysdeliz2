package sysdeliz2.utils;

import javafx.scene.image.ImageView;

/**
 * @author lima.joao
 * @since 29/08/2019 10:30
 */
abstract public class Constantes {

    public static final String PATH_IMG_ACTIVE   = "/images/icons/buttons/active (1).png";
    public static final String PATH_IMG_INACTIVE = "/images/icons/buttons/inactive (1).png";


    public static ImageView newImageViewActive(){
        return Constantes.newImageView(Constantes.PATH_IMG_ACTIVE);
    }

    public static ImageView newImageViewInactive(){
        return Constantes.newImageView(Constantes.PATH_IMG_INACTIVE);
    }

    public static ImageView newImageView(String path){
        return new ImageView(Constantes.class.getResource(path).toExternalForm());
    }

}