package sysdeliz2.utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelStyleUtils {

    public XSSFCellStyle yellowStyle = new XSSFWorkbook().createCellStyle();

    {
        yellowStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        yellowStyle.setAlignment(HorizontalAlignment.CENTER);
    }


    public XSSFCellStyle stylePadraoCenter = new XSSFWorkbook().createCellStyle();

    {
        stylePadraoCenter.setAlignment(HorizontalAlignment.CENTER);
    }

    public XSSFCellStyle redStyle = new XSSFWorkbook().createCellStyle();

    {
        redStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        redStyle.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        redStyle.setAlignment(HorizontalAlignment.CENTER);
    }

    public XSSFCellStyle blueStyleCenter = new XSSFWorkbook().createCellStyle();

    {
        blueStyleCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyleCenter.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
        blueStyleCenter.setAlignment(HorizontalAlignment.CENTER);
    }

    public XSSFCellStyle blueStyle = new XSSFWorkbook().createCellStyle();
    {
        blueStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
    }

    public XSSFCellStyle primaryStyle = new XSSFWorkbook().createCellStyle();
    {
        primaryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        primaryStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
    }


    public XSSFCellStyle amberStyle = new XSSFWorkbook().createCellStyle();
    {
        amberStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        amberStyle.setAlignment(HorizontalAlignment.CENTER);
        amberStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
    }

    public XSSFCellStyle successStyle = new XSSFWorkbook().createCellStyle();
    {
        successStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        successStyle.setAlignment(HorizontalAlignment.CENTER);
        successStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
    }

    public XSSFCellStyle blackStyle = new XSSFWorkbook().createCellStyle();
    {
        blackStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blackStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
    }

    public Font blackStyleFont = new XSSFWorkbook().createFont();
    {
        blackStyleFont.setColor(IndexedColors.BRIGHT_GREEN.getIndex());
        blackStyle.setFont(blackStyleFont);
    }

    public XSSFCellStyle blackCenterStyle = new XSSFWorkbook().createCellStyle();
    {
        blackCenterStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blackCenterStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        blackCenterStyle.setAlignment(HorizontalAlignment.CENTER);
        blackCenterStyle.setFont(blackStyleFont);
    }

}
