package sysdeliz2.utils;

import javafx.beans.property.Property;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class ExportExcel<T> {
    
    private List<Field> columnsClass = new ArrayList<>();
    private List<TableColumn> columnsTable = new ArrayList<>();
    private List<T> values = new ArrayList<>();
    private final Class<T> objectParam;
    private final TableView tableParam;
    
    // Initializing employees data to insert into the excel file
    public ExportExcel(TableView dadosToExport) {
        objectParam = null;
        tableParam = dadosToExport;
        this.loadColunasTableView(dadosToExport);
        
        //Array to Export
        values.addAll(dadosToExport.getItems());
    }
    
    public ExportExcel(ObservableList<T> dadosToExport, Class classType) {
        objectParam = classType != null ? classType : (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        tableParam = null;
        this.loadColunasClasse(objectParam);
        
        // Array to export
        values.addAll(dadosToExport);
    }
    
    public ExportExcel(List<T> dadosToExport, Class classType) {
        objectParam = classType != null ? classType : (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        tableParam = null;
        this.loadColunasClasse(objectParam);
        
        // Array to export
        values.addAll(dadosToExport);
    }
    
    private void loadColunasTableView(TableView classeModelo) {
        for (Object field : classeModelo.getColumns()) {
            columnsTable.add((TableColumn) field);
        }
    }
    
    private void loadColunasClasse(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            columnsClass.add(field);
        }
    }
    
    public ExportExcel exportArrayToExcel(String pathFile) throws IOException, InvalidFormatException, NoSuchFieldException, IllegalAccessException {
        return exportArrayToExcel(pathFile, null);
    }
    
    public ExportExcel exportArrayToExcel(String pathFile, String tabName) throws IOException, InvalidFormatException, NoSuchFieldException, IllegalAccessException {
        
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();
        
        // Create a Sheet
        Sheet sheet = workbook.createSheet(tabName != null ? tabName : objectParam.getName());
        
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        
        // Create a Row
        Row headerRow = sheet.createRow(0);
        
        // Create cells
        for (int i = 0; i < columnsClass.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnsClass.get(i).getName().toUpperCase());
            cell.setCellStyle(headerCellStyle);
        }
        
        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
        // Create Cell Style for formatting DateTime
        CellStyle dateTimeCellStyle = workbook.createCellStyle();
        dateTimeCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss"));
        // Create Cell Style for formatting Time
        CellStyle timeCellStyle = workbook.createCellStyle();
        timeCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
        // Create Cell Style for formatting Time
        CellStyle doubleCellStyle = workbook.createCellStyle();
        doubleCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
        // Create Cell Style for formatting Time
        CellStyle intCellStyle = workbook.createCellStyle();
        intCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
        // Create Cell Style for formatting Time
        CellStyle booleanCellStyle = workbook.createCellStyle();
        booleanCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
        
        // Create Other rows and cells with employees data
        int rowNum = 1;
        for (T classExport : values) {
            Row row = sheet.createRow(rowNum++);
            
            int columnIndex = 0;
            for (Field column : columnsClass) {
                column.setAccessible(true);
                String columnValue = String.valueOf(column.get(classExport));
                if (column.getType().getName().contains("Property")) {
                    columnValue = String.valueOf(((Property) column.get(classExport)).getValue());
                }
                row.createCell(columnIndex)
                        .setCellValue(columnValue);
                
                column.setAccessible(false);
                columnIndex++;
            }
            
        }
        
        // Resize all columns to fit the content size
        for (int i = 0; i < columnsClass.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(pathFile);
        workbook.write(fileOut);
        fileOut.close();
        
        // Closing the workbook
        workbook.close();
        
        return this;
    }
    
    public ExportExcel exportTableViewToExcel(String pathFile) throws IOException, InvalidFormatException, NoSuchFieldException, IllegalAccessException {
        
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();
        
        // Create a Sheet
        Sheet sheet = workbook.createSheet(tableParam.getId());
        
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        
        // Create a Row
        Row headerRow = sheet.createRow(0);
        
        // Create cells
        for (int i = 0; i < columnsTable.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnsTable.get(i).getText());
            cell.setCellStyle(headerCellStyle);
        }
        
        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
        
        // Create Other rows and cells with employees data
        for (int i = 0; i < values.size(); i++) {
            Object classExport = values.get(i);
            Row row = sheet.createRow(i + 1);
            
            int columnIndex = 0;
            for (TableColumn column : columnsTable) {
                String columnValue = String.valueOf(column.getCellObservableValue(classExport).getValue());
                System.out.println(columnValue);
                
                Cell createCell = row.createCell(columnIndex);
                createCell.setCellValue(columnValue);
                columnIndex++;
            }
            
        }
        
        // Resize all columns to fit the content size
        for (int i = 0; i < columnsClass.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(pathFile);
        workbook.write(fileOut);
        fileOut.close();
        
        // Closing the workbook
        workbook.close();
        
        return this;
    }
    
}
