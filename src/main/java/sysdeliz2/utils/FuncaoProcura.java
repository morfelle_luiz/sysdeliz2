/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sysdeliz2.controllers.fxml.SceneFuncaoProcuraController;

import java.io.IOException;

/**
 *
 * @author cristiano.diego
 */
public class FuncaoProcura extends Stage {

    public static final String SCENE_PROCURA_PRODUTO_MARKETING = "/sysdeliz2/controllers/fxml/SceneFuncaoProcura.fxml";
    public String stringReturn = "";
    SceneFuncaoProcuraController controllerProdutoMarketing = null;

    public String showProcura(String sceneOpen) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(sceneOpen));
        final BorderPane rootPane = (BorderPane) loader.load();
        Scene scene = new Scene(rootPane);
        scene.getStylesheets().add("/styles/bootstrap3.css");
        scene.getStylesheets().add("/styles/styles.css");

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setScene(scene);

        switch (sceneOpen) {
            case SCENE_PROCURA_PRODUTO_MARKETING:
                controllerProdutoMarketing = loader.<SceneFuncaoProcuraController>getController();
                break;
        }

        stage.showAndWait();

        switch (sceneOpen) {
            case SCENE_PROCURA_PRODUTO_MARKETING:
                stringReturn = controllerProdutoMarketing.getReturnValue();
                break;
        }

        return stringReturn;
    }

}
