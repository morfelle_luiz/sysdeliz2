package sysdeliz2.utils;

import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.EntradaPA.*;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosOfFinalizadas;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FuncoesEntradaPA {

    protected static String usuario;
    protected static FluentDao fluentDao;
    protected static NativeDAO nativeDao;
    protected static String deposito = "";
    protected static SdLotePa loteEscolhido;
    protected static List<SdCaixaPA> caixasComProblema = new ArrayList<>();

    public static boolean recebeLote(String usuario, SdLotePa lote) {
        FuncoesEntradaPA.usuario = usuario;
        loteEscolhido = lote;

        recebeCaixas(loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> eb.isIncompleta() && !eb.isRecebida())).collect(Collectors.toList()));
        recebeCaixas(loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> !eb.isIncompleta() && !eb.isRecebida())).collect(Collectors.toList()));
        if (caixasComProblema.size() > 0) {
            MessageBox.create(message -> {
                message.message("Algumas caixas tiveram problemas na entrada: " + caixasComProblema.stream().map(it -> it.getId() + "-" + it.getProduto()).reduce("\n-", String::concat));
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreen();
            });
            return false;
        } else {
            loteEscolhido.setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 6)).singleResult());
            loteEscolhido = new FluentDao().merge(loteEscolhido);
            MessageBox.create(message -> {
                message.message("Lote recebido com sucesso!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
            return true;
        }
    }

    private static void recebeCaixas(List<SdCaixaPA> listCaixas) {
        for (SdCaixaPA caixaPa : listCaixas) {
            recebeCaixa(caixaPa);
        }
    }

    public static void recebeCaixa(SdCaixaPA caixa) {
        if (realizarPresistsBanco(caixa)) {
            caixa.setRecebida(true);
            new FluentDao().merge(caixa);
        } else {
            caixasComProblema.add(caixa);
        }
    }

    public static boolean realizarPresistsBanco(SdCaixaPA sdCaixaPA) {

        int id = sdCaixaPA.getId();
        JPAUtils.getEntityManager().clear();
        sdCaixaPA = new FluentDao().selectFrom(SdCaixaPA.class).where(it -> it.equal("id", id)).singleResult();
        SdCaixaPA finalSdCaixaPA = sdCaixaPA;

        String periodo = "";

        VSdDadosOfPendente ofPendente = new FluentDao().selectFrom(VSdDadosOfPendente.class)
                .where(it -> it.equal("id.numero", finalSdCaixaPA.getItemlote().getNumero()).equal("id.setor.codigo", finalSdCaixaPA.getItemlote().getSetor())).singleResult();

        if (ofPendente != null) {
            periodo = ofPendente.getPeriodo();
        } else {
            VSdDadosOfFinalizadas ofFinalizada = new FluentDao().selectFrom(VSdDadosOfFinalizadas.class)
                    .where(it -> it
                            .equal("numero", finalSdCaixaPA.getItemlote().getNumero())
                            .equal("setor", finalSdCaixaPA.getItemlote().getSetor()))
                    .singleResult();
            periodo = ofFinalizada.getPeriodo();
        }

        if (!sdCaixaPA.isIncompleta()) {
            deposito = sdCaixaPA.isSegunda() ? "0007" : periodo.equals("M") ? "0011" : "0005";
        }

        try {
            nativeDao = new NativeDAO().createConnection();
            String lancamento = "";
            fluentDao = new FluentDao();
            for (SdItemCaixaPA itemCaixa : sdCaixaPA.getItensCaixa()) {

                if (sdCaixaPA.isSegunda()) {
                    lancamento = getProxLancamentoDefMov();
                }
                List<Faccao> faccaoList = (List<Faccao>) new FluentDao().selectFrom(Faccao.class)
                        .where(it -> it
                                .equal("id.tam", itemCaixa.getTam())
                                .equal("id.cor", itemCaixa.getCor())
                                .equal("id.codigo", itemCaixa.getProduto())
                                .equal("id.op", itemCaixa.getCaixa().getItemlote().getSetor())
                                .equal("id.numero", itemCaixa.getCaixa().getItemlote().getNumero())
                        )
                        .orderBy("id.mov", OrderType.ASC)
                        .resultList();
                if (faccaoList == null) {
                    MessageBox.create(message -> {
                        message.message("3 - ERRO FACCAO LIST");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreenMobile();
                    });
                    throw new SQLException();
                }

                if (!itemCaixa.getCaixa().getItemlote().isOrdemServico()) {
                    updateOfItem(itemCaixa);
                    updateFaccao(itemCaixa, faccaoList);
                }

                if (!sdCaixaPA.isIncompleta() && !sdCaixaPA.isPerdida()) {
                    updatePaItem(itemCaixa);
                    updatePaMov(itemCaixa);
                }
                if (sdCaixaPA.isSegunda()) {
                    updateDefMov(itemCaixa, lancamento, faccaoList);
                }

                addSysDelizLogNative(nativeDao, "Tela de Minuta", TipoAcao.CADASTRAR, "CX: " + itemCaixa.getCaixa().getId() + " - " + itemCaixa.getProduto() + "/" + itemCaixa.getTam() + "/" + itemCaixa.getCor(),
                        String.format("Item da caixa %s lido:\n" + (itemCaixa.getCaixa().isSegunda() ? "--CAIXA DE SEGUNDA--" : itemCaixa.getCaixa().isIncompleta() ? "--CAIXA INCOMPLETA--" : itemCaixa.getCaixa().isPerdida() ? "--CAIXA PERDIDAS--" : "--CAIXA BOA--") + "\n\nId: %s\nProduto: %s\nTam: %s\nCor: %s\nQtde: %s",
                                itemCaixa.getCaixa().getId(), itemCaixa.getId(), itemCaixa.getProduto(), itemCaixa.getTam(), itemCaixa.getCor(), itemCaixa.getQtde()));
                fluentDao.getEntityManager().flush();
            }
            if (!sdCaixaPA.getItemlote().isOrdemServico()) updateOf(sdCaixaPA);
            NativeDAO.closeConnection();

            SysLogger.addSysDelizLog("Tela de Minuta", TipoAcao.CADASTRAR, sdCaixaPA.getId() + " - " + sdCaixaPA.getProduto(),
                    String.format("Caixa %s lida e recebida:\n" + (sdCaixaPA.isSegunda() ? "--CAIXA DE SEGUNDA--" : sdCaixaPA.isIncompleta() ? "--CAIXA INCOMPLETA--" : sdCaixaPA.isPerdida() ? "--CAIXA PERDIDAS--" : "--CAIXA BOA--") + "\n\nProduto: %s\nQtde: %s\nPeso: %s",
                            sdCaixaPA.getId(), sdCaixaPA.getProduto(), sdCaixaPA.getQtde(), sdCaixaPA.getPeso()));
            return true;
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("Erro na Leitura!\n" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            SimpleMail.INSTANCE.addDestinatario("luiz@deliz.com.br")
                    .comAssunto("Erro na Entrada PA")
                    .comCorpo(e.getMessage() + "\n" + e.getCause() + "\n" + e.getLocalizedMessage() + "\n" + e.getClass() + "\n" + e.getStackTrace() + "\nCaixa: " + sdCaixaPA.getId())
                    .send();
            e.printStackTrace();
            fluentDao.rollbackTransaction();
            fluentDao = null;
            try {
                nativeDao.rollBackTransaction();
                nativeDao = null;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return false;
            }
            return false;
        }
    }

    private static String getProxLancamentoDefMov() throws Exception {
        try {
            Codigos codigos = new FluentDao().selectFrom(Codigos.class).where(it -> it.equal("codigosId.tabela", "DEF_MOV")).singleResult();
            if (codigos == null) {
                MessageBox.create(message -> {
                    message.message("2 - ERRO PROX DEF MOV");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreenMobile();
                });
                throw new SQLException("Erro ao buscar próximo código da DEF_MOV");
            }
            String prox = codigos.getProximo().toString();
            codigos.setProximo(codigos.getProximo().add(BigDecimal.ONE));
            new FluentDao().merge(codigos);
            return prox;
        } catch (SQLException throwables) {
            MessageBox.create(message -> {
                message.message("2 - ERRO PROX DEF MOV");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throwables.printStackTrace();
            throw new Exception("Erro ao buscar próximo código da DEF_MOV");
        }
    }

    private static void updateDefMov(SdItemCaixaPA itemCaixa, String lancamento, List<Faccao> faccaoList) throws Exception {
        try {
            Map<Defeito, List<SdBarraCaixaPA>> mapDefeitos = itemCaixa.getListBarra().stream().collect(Collectors.groupingBy(SdBarraCaixaPA::getDefeito));
            Faccao faccao = faccaoList.stream().filter(it -> it.getId().getCor().equals(itemCaixa.getCor()) && it.getId().getTam().equals(itemCaixa.getTam()) && it.getId().getCodigo().equals(itemCaixa.getProduto())).findFirst().get();
            CustoProduto custoProduto = new FluentDao()
                    .selectFrom(CustoProduto.class)
                    .where(it -> it
                            .equal("codigo", itemCaixa.getProduto())
                    ).singleResult();
            if (custoProduto == null || custoProduto.getCusto() == null) {
                custoProduto = new CustoProduto();
                custoProduto.setCusto(BigDecimal.ZERO);
            }

            CustoProduto finalCustoProduto = custoProduto;
            mapDefeitos.forEach((defeito, barras) -> {
                String sql = String.format("INSERT INTO DEF_MOV_001" +
                                "(DT_LAN, QUANT, CODIGO, CODCLI, NUMERO, ARTIGO, TAM, COR, PENDENTE, PRECO, SETOR, TIPO, REVISOR, PARTE, INSPETOR, LANCAMENTO) " +
                                "values " +
                                "(TO_DATE('%s', 'yyyy/mm/dd')," +
                                " '%s'," +
                                "'%s'," +
                                "'%s'," +
                                "'%s'," +
                                "'%s'," +
                                "'%s'," +
                                "'%s'," +
                                "'S'," +
                                "%s," +
                                "'%s'," +
                                "5," +
                                "00037," +
                                "'%s'," +
                                "00037," +
                                "'%s')",
                        LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")), barras.size(), defeito.getCodigo(), faccao.getId().getCodcli(),
                        itemCaixa.getCaixa().getItemlote().getNumero(), itemCaixa.getProduto(), itemCaixa.getTam(), itemCaixa.getCor(),
                        finalCustoProduto.getCusto().setScale(2, RoundingMode.HALF_EVEN), itemCaixa.getCaixa().getItemlote().getSetor(), faccao.getId().getParte(), StringUtils.lpad(lancamento, 6, "0")
                );
                try {
                    nativeDao.runNativeQueryUpdateWithouCommit(sql);
                } catch (SQLException throwables) {
                    MessageBox.create(message -> {
                        message.message("13 - ERRO DEFMOV ");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreenMobile();
                    });
                    throwables.printStackTrace();
                }
            });
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("14 - ERRO DEFMOV " + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            e.printStackTrace();
            throw new Exception();
        }
    }

    private static void updatePaMov(SdItemCaixaPA itemCaixa) throws Exception {

        try {
            fluentDao.mergeWithoutCommit(new PaMov(itemCaixa, usuario, deposito, "FC"));
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("12 - ERRO PAMOV " + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }

    }

    private static void updatePaItem(SdItemCaixaPA itemCaixa) throws Exception {
        try {
            PaIten paIten = new FluentDao()
                    .selectFrom(PaIten.class)
                    .where(it -> it
                            .equal("id.tam", itemCaixa.getTam())
                            .equal("id.cor", itemCaixa.getCor())
                            .equal("id.codigo", itemCaixa.getProduto())
                            .equal("id.deposito", deposito)
                    ).singleResult();

            if (paIten != null) {
                if (itemCaixa.getCaixa().getItemlote().isOrdemServico()) {
                    PaIten deposito001 = new FluentDao()
                            .selectFrom(PaIten.class)
                            .where(it -> it
                                    .equal("id.tam", itemCaixa.getTam()).equal("id.cor", itemCaixa.getCor()).equal("id.codigo", itemCaixa.getProduto()).equal("id.deposito", "0001")
                            ).singleResult();
                    deposito001.setQuantidade(deposito001.getQuantidade().subtract(BigDecimal.valueOf(itemCaixa.getQtde())));
                }
                paIten.setQuantidade(paIten.getQuantidade().add(new BigDecimal(itemCaixa.getQtde())));

                fluentDao.mergeWithoutCommit(paIten);
            } else {
                fluentDao.mergeWithoutCommit(new PaIten(itemCaixa, deposito));
            }
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("11 - ERRO PAITEM " + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }
    }

    private static void updateFaccao(SdItemCaixaPA itemCaixa, List<Faccao> faccaoList) throws Exception {
        try {
            if (faccaoList.size() == 1) {
                Faccao faccao = faccaoList.get(0);

                if (itemCaixa.getCaixa().isIncompleta())
                    faccao.setQuanti(faccao.getQuanti() + itemCaixa.getQtde());
                else if (itemCaixa.getCaixa().isPerdida())
                    faccao.setQuantf(faccao.getQuantf() + itemCaixa.getQtde());
                else if (itemCaixa.getCaixa().isSegunda())
                    faccao.setQuant2(faccao.getQuant2() + itemCaixa.getQtde());
                else
                    faccao.setQuant(faccao.getQuant() + itemCaixa.getQtde());

                faccao.setDtr(LocalDate.now());
                fluentDao.mergeWithoutCommit(faccao);
                criarFaccao3(faccao, itemCaixa);

            } else {
                int qtdeItemCaixa = itemCaixa.getQtde();
                for (Faccao faccao : faccaoList) {
                    int qtde = 0;
                    if (faccao.getQtorig() > (faccao.getQuant() + faccao.getQuant2() + faccao.getQuantf() + faccao.getQuanti())) {
                        if (itemCaixa.getCaixa().isIncompleta()) {
                            qtde = (itemCaixa.getQtde() + faccao.getQuanti()) > faccao.getQtorig() ? (faccao.getQtorig() - faccao.getQuanti()) : itemCaixa.getQtde();
                            faccao.setQuanti(faccao.getQuanti() + qtde);
                        } else if (itemCaixa.getCaixa().isPerdida()) {
                            qtde = (itemCaixa.getQtde() + faccao.getQuantf()) > faccao.getQtorig() ? (faccao.getQtorig() - faccao.getQuantf()) : itemCaixa.getQtde();
                            faccao.setQuantf(faccao.getQuantf() + qtde);
                        } else if (itemCaixa.getCaixa().isSegunda()) {
                            qtde = (itemCaixa.getQtde() + faccao.getQuant2()) > faccao.getQtorig() ? (faccao.getQtorig() - faccao.getQuant2()) : itemCaixa.getQtde();
                            faccao.setQuant2(faccao.getQuant2() + qtde);
                        } else {
                            qtde = (itemCaixa.getQtde() + faccao.getQuant()) > faccao.getQtorig() ? (faccao.getQtorig() - faccao.getQuant()) : itemCaixa.getQtde();
                            faccao.setQuant(faccao.getQuant() + qtde);
                        }
                    }
                    itemCaixa.setQtde(itemCaixa.getQtde() - qtde);
                    faccao.setDtr(LocalDate.now());
                    fluentDao.mergeWithoutCommit(faccao);
                    criarFaccao3(faccao, itemCaixa);
                }
                itemCaixa.setQtde(qtdeItemCaixa);
            }
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("5 - ERRO FACCAO " + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }
    }

    private static void criarFaccao3(Faccao faccao, SdItemCaixaPA itemCaixa) throws Exception {
        try {
            Faccao3 faccao3Existente = new FluentDao()
                    .selectFrom(Faccao3.class)
                    .where(it -> it
                            .equal("id.op", faccao.getId().getOp())
                            .equal("id.tam", faccao.getId().getTam())
                            .equal("id.numero", faccao.getId().getNumero())
                            .equal("id.cor", faccao.getId().getCor())
                            .equal("id.codigo", faccao.getId().getCodigo())
                            .equal("id.parte", faccao.getId().getParte())
                            .equal("id.mov", faccao.getId().getMov())
                            .equal("dtlan", LocalDate.now())
                    )
                    .singleResult();
            if (faccao3Existente != null) {
                if (itemCaixa.getCaixa().isPerdida())
                    faccao3Existente.setQuantf(faccao3Existente.getQuantf() + itemCaixa.getQtde());
                else if (itemCaixa.getCaixa().isIncompleta())
                    faccao3Existente.setQuanti(faccao3Existente.getQuanti() + itemCaixa.getQtde());
                else if (itemCaixa.getCaixa().isSegunda())
                    faccao3Existente.setQuant2(faccao3Existente.getQuant2() + itemCaixa.getQtde());
                else
                    faccao3Existente.setQuant(faccao3Existente.getQuant() + itemCaixa.getQtde());

                faccao3Existente.setCodcliant(faccao.getId().getCodcli());
                faccao3Existente.setIdant(faccao.getId().getIdentificador());
                fluentDao.mergeWithoutCommit(faccao3Existente);
            } else {
                fluentDao.mergeWithoutCommit(new Faccao3(faccao, deposito, itemCaixa));
            }
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("6 - ERRO FACCAO 3" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }
    }

    private static void updateOfItem(SdItemCaixaPA itemCaixa) throws Exception {
        try {
            OfIten ofIten = new FluentDao().selectFrom(OfIten.class).where(it -> it
                            .equal("id.numero", itemCaixa.getCaixa().getItemlote().getNumero())
                            .equal("id.tamanho", itemCaixa.getTam())
                            .equal("id.cor.cor", itemCaixa.getCor()))
                    .singleResult();
            if (itemCaixa.getCaixa().isIncompleta() || itemCaixa.getCaixa().isSegunda() || itemCaixa.getCaixa().isPerdida()) {
                ofIten.setQtdeC(ofIten.getQtdeC().add(new BigDecimal(itemCaixa.getQtde())));
                ofIten.setQtde(ofIten.getQtde().intValue() - itemCaixa.getQtde() <= 0 ? BigDecimal.ZERO : ofIten.getQtde().subtract(new BigDecimal(itemCaixa.getQtde())));
            } else {
                ofIten.setQtdeB(ofIten.getQtdeB().add(new BigDecimal(itemCaixa.getQtde())));
                ofIten.setQtde(ofIten.getQtde().intValue() - itemCaixa.getQtde() <= 0 ? BigDecimal.ZERO : ofIten.getQtde().subtract(new BigDecimal(itemCaixa.getQtde())));
            }
            fluentDao.mergeWithoutCommit(ofIten);
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("7 - ERRO OFITEM" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }
    }

    private static void updateOf(SdCaixaPA sdCaixaPA) throws Exception {
        try {
            Of2 of = new FluentDao().selectFrom(Of2.class).where(it -> it.equal("numero", sdCaixaPA.getItemlote().getNumero())).singleResult();
            of.setAtivos(of.getAtivos().subtract(new BigDecimal(sdCaixaPA.getQtde())));
            fluentDao.mergeWithoutCommit(of);
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("9 - ERRO OF" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }
    }

    public static void addSysDelizLogNative(NativeDAO nativeDao, String tela, TipoAcao acao, String codigoAcao, String descricaoAcao) throws Exception {

        try {
            nativeDao.runNativeQueryUpdateWithouCommit("insert into sd_log_sysdeliz_001 (DATA_LOG, USUARIO, TELA, ACAO, REFERENCIA, DESCRICAO)" +
                            "values (TO_DATE('%s', 'yyyy/mm/dd hh24:mi:ss'), '%s', '%s', '%s', '%s', '%s')",
                    LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")), SceneMainController.getAcesso() == null ? "sem.login" : SceneMainController.getAcesso().getUsuario(), tela, acao.tipoAcao, codigoAcao, descricaoAcao);
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("10 - ERRO LOG" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            throw new Exception();
        }

    }
}
