/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import static javafx.scene.layout.Region.USE_COMPUTED_SIZE;
import static javafx.scene.layout.Region.USE_PREF_SIZE;

/**
 *
 * @author cristiano.diego
 */
public class GUIUtils {

    public static final List<Integer> COLUNAS_TBL_RESERVAS = new ArrayList<Integer>(Arrays.asList(7, 9, 10, 7, 7, 48, 10, 10, 15));
    public static final List<Integer> COLUNAS_TBL_MARKETING_PEDIDO = new ArrayList<Integer>(Arrays.asList(10, 15, 40, 10, 30, 5, 10));
    public static final List<Integer> COLUNAS_TBL_ITENS_CAIXA_LANCAMENTO_RESERVA = new ArrayList<Integer>(Arrays.asList(20, 12, 50, 10, 8));

    public static void autoFitTable(TableView tableView, List<Integer> colunas) {
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        int i = 0;
        for (Object column : tableView.getColumns()) {
            ((TableColumn) column).setMaxWidth(1f * Integer.MAX_VALUE * colunas.get(i));
            i++;
        }
    }
    
    /**
     *
     * @default posicao TOP_RIGHT, tempo 5
     * @param title
     * @param message
     */
    public static void showMessageNotification(String title, String message) {
        showMessageNotification(title, message, Pos.TOP_RIGHT, 5);
    }
    
    public static void showMessageNotification(String title, String message, Pos posicao, Integer tempo) {
        Notifications alertFinnaly = Notifications.create()
                .title(title)
                .text(message)
                .graphic(null)
                .hideAfter(Duration.seconds(tempo))
                .position(posicao);
        alertFinnaly.showInformation();
    }
    
    public static void showMessageEficiencia(Double eficiencia, Double produtividade) {
        VBox vboxPrincipal = new VBox();
        HBox boxMessage = new HBox();
        ImageView iconMessage = new ImageView();
        Label messageSuccess = new Label();
        HBox boxEficiencia = new HBox();
        Label messageEficiencia = new Label();
        Label lbEficiencia = new Label();
        HBox boxAproveitamento = new HBox();
        Label messageAproveitamento = new Label();
        Label lbAproveitamento = new Label();
    
        vboxPrincipal.setMaxHeight(USE_COMPUTED_SIZE);
        vboxPrincipal.setMaxWidth(USE_COMPUTED_SIZE);
        vboxPrincipal.setMinHeight(USE_COMPUTED_SIZE);
        vboxPrincipal.setMinWidth(USE_COMPUTED_SIZE);
        vboxPrincipal.setPrefHeight(145.0);
        vboxPrincipal.setPrefWidth(415.0);
    
        boxMessage.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        boxMessage.setPrefHeight(38.0);
        boxMessage.setPrefWidth(580.0);
        boxMessage.setSpacing(10.0);
    
        iconMessage.setFitHeight(52.0);
        iconMessage.setFitWidth(52.0);
        iconMessage.setPickOnBounds(true);
        iconMessage.setPreserveRatio(true);
        iconMessage.setImage(new Image(GUIUtils.class.getResource("/images/icons/buttons/active (4).png").toExternalForm()));
    
        messageSuccess.setText("PRODUÇÃO LANÇADA COM SUCESSO!");
        messageSuccess.setFont(new Font("System Bold", 32.0));
        messageSuccess.setStyle("-fx-font-size: 18; -fx-font-weight: bold");
    
        VBox.setVgrow(boxEficiencia, javafx.scene.layout.Priority.ALWAYS);
        boxEficiencia.setAlignment(Pos.TOP_LEFT);
        boxEficiencia.setPrefHeight(100.0);
        boxEficiencia.setPrefWidth(200.0);
        boxEficiencia.setSpacing(10.0);
    
        boxAproveitamento.setAlignment(Pos.TOP_LEFT);
        boxAproveitamento.setPrefHeight(70.0);
        boxAproveitamento.setPrefWidth(200.0);
        boxAproveitamento.setSpacing(10.0);
        
        messageEficiencia.setText("EFICIÊNCIA DO PACOTE:");
        messageEficiencia.setFont(new Font(20.0));
        messageEficiencia.setStyle("-fx-font-size: 16");
    
        lbEficiencia.setStyle("-fx-text-fill: "+(eficiencia < .8 ? "red" : eficiencia >= 1 ? "green" : "orange")+"; -fx-font-size: 40; -fx-font-weight: bold");
        lbEficiencia.setText(StringUtils.toPercentualFormat(eficiencia,2));
        lbEficiencia.setFont(new Font("System Bold", 10.0));
        messageAproveitamento.setStyle("-fx-font-size: 18");
    
        messageAproveitamento.setText("APROVEITAMENTO DIA:");
        messageAproveitamento.setFont(new Font(20.0));
        messageAproveitamento.setStyle("-fx-font-size: 12");
    
        lbAproveitamento.setStyle("-fx-text-fill: "+(produtividade < .8 ? "red" : produtividade >= 1 ? "green" : "orange")+"; -fx-font-size: 40; -fx-font-weight: bold");
        lbAproveitamento.setText(StringUtils.toPercentualFormat(produtividade,2));
        lbAproveitamento.setFont(new Font("System Bold Italic", 6.0));
        messageAproveitamento.setStyle("-fx-font-size: 14");
        
        vboxPrincipal.setPadding(new Insets(10.0));
        boxEficiencia.setAlignment(Pos.TOP_LEFT);
        boxAproveitamento.setAlignment(Pos.TOP_LEFT);
    
        boxMessage.getChildren().add(iconMessage);
        boxMessage.getChildren().add(messageSuccess);
        vboxPrincipal.getChildren().add(boxMessage);
        boxEficiencia.getChildren().add(messageEficiencia);
        boxEficiencia.getChildren().add(lbEficiencia);
        boxAproveitamento.getChildren().add(messageAproveitamento);
        boxAproveitamento.getChildren().add(lbAproveitamento);
        vboxPrincipal.getChildren().add(boxEficiencia);
        vboxPrincipal.getChildren().add(boxAproveitamento);
        
        Notifications alertFinnaly = Notifications.create()
                .graphic(vboxPrincipal)
                .hideAfter(Duration.seconds(5))
                .position(Pos.CENTER);
        alertFinnaly.show();
    }
    
    public static void showMessageEficiencia(Double eficiencia, Double eficienciaDia, Double produtividade) {
    
        final AnchorPane anchorWindow = new AnchorPane();
        final Label lbTitleInfosColaborador = new Label();
        final ImageView imgSucessoLancamento = new ImageView();
        final Label lbTitleSucesso = new Label();
        final Label lbTitleEficienciaPacote = new Label();
        final Label lbEficienciaPacote = new Label();
        final Label lbTitleEficienciaDia = new Label();
        final Label lbEficienciaDia = new Label();
        final Label lbTitleProdutividade = new Label();
        final Label lbProdutividade = new Label();
        
        anchorWindow.setMaxHeight(USE_PREF_SIZE);
        anchorWindow.setMaxWidth(USE_PREF_SIZE);
        anchorWindow.setMinHeight(USE_PREF_SIZE);
        anchorWindow.setMinWidth(USE_PREF_SIZE);
        anchorWindow.setPrefHeight(234.0);
        anchorWindow.setPrefWidth(505.0);
        anchorWindow.getStylesheets().add("/styles/sysDelizDesktop.css");
        anchorWindow.setStyle("-fx-border-color: #F0F0F0; -fx-border-width: 2px; -fx-border-style: solid; -fx-background-color: #F5F5F5;");
        
        AnchorPane.setLeftAnchor(lbTitleInfosColaborador, -10.0);
        AnchorPane.setRightAnchor(lbTitleInfosColaborador, -10.0);
        AnchorPane.setTopAnchor(lbTitleInfosColaborador, -10.0);
        lbTitleInfosColaborador.setAlignment(javafx.geometry.Pos.CENTER);
        lbTitleInfosColaborador.setLayoutX(14.0);
        lbTitleInfosColaborador.setLayoutY(14.0);
        lbTitleInfosColaborador.setPrefHeight(100.0);
        lbTitleInfosColaborador.setPrefWidth(501.0);
        lbTitleInfosColaborador.getStyleClass().add("success");
        lbTitleInfosColaborador.setTextFill(javafx.scene.paint.Color.WHITE);
        lbTitleInfosColaborador.setFont(new Font("System Bold Italic", 20.0));
    
        AnchorPane.setLeftAnchor(imgSucessoLancamento, 0.0);
        AnchorPane.setTopAnchor(imgSucessoLancamento, 0.0);
        imgSucessoLancamento.setFitHeight(80.0);
        imgSucessoLancamento.setFitWidth(80.0);
        imgSucessoLancamento.setLayoutX(66.0);
        imgSucessoLancamento.setLayoutY(49.0);
        imgSucessoLancamento.setPickOnBounds(true);
        imgSucessoLancamento.setPreserveRatio(true);
        imgSucessoLancamento.setImage(new Image(GUIUtils.class.getResource("/images/icons/buttons/active (4).png").toExternalForm()));
    
        AnchorPane.setLeftAnchor(lbTitleSucesso, 100.0);
        AnchorPane.setRightAnchor(lbTitleSucesso, 0.0);
        AnchorPane.setTopAnchor(lbTitleSucesso, 0.0);
        lbTitleSucesso.setLayoutX(110.0);
        lbTitleSucesso.setLayoutY(20.0);
        lbTitleSucesso.setPrefHeight(86.0);
        lbTitleSucesso.setPrefWidth(463.0);
        lbTitleSucesso.setText("PRODUÇÃO LANÇADA COM SUCESSO!");
        lbTitleSucesso.setTextFill(javafx.scene.paint.Color.valueOf("#104013"));
        lbTitleSucesso.setWrapText(true);
        lbTitleSucesso.setFont(new Font("System Bold", 28.0));
        lbTitleSucesso.setPadding(new Insets(-2.0, 0.0, 0.0, 0.0));
    
        AnchorPane.setLeftAnchor(lbTitleEficienciaPacote, 0.0);
        lbTitleEficienciaPacote.setLayoutX(324.0);
        lbTitleEficienciaPacote.setLayoutY(110.0);
        lbTitleEficienciaPacote.setText("% OPERAÇÃO");
        lbTitleEficienciaPacote.setFont(new Font(20.0));
    
        AnchorPane.setLeftAnchor(lbEficienciaPacote, 0.0);
        lbEficienciaPacote.setLayoutX(324.0);
        lbEficienciaPacote.setLayoutY(121.0);
        lbEficienciaPacote.setPrefHeight(111.0);
        lbEficienciaPacote.setPrefWidth(330.0);
        lbEficienciaPacote.setText(StringUtils.toPercentualFormat(eficiencia,2));
        lbEficienciaPacote.setStyle("-fx-text-fill: "+(eficiencia < .8 ? "red" : eficiencia >= 1 ? "green" : "orange")+";");
        lbEficienciaPacote.setFont(new Font("System Bold", 76.0));
    
        AnchorPane.setRightAnchor(lbTitleEficienciaDia, 0.0);
        lbTitleEficienciaDia.setLayoutX(61.0);
        lbTitleEficienciaDia.setLayoutY(114.0);
        lbTitleEficienciaDia.setText("EFICIÊNCIA");
        lbTitleEficienciaDia.setFont(new Font(14.0));
    
        AnchorPane.setRightAnchor(lbEficienciaDia, 0.0);
        lbEficienciaDia.setAlignment(javafx.geometry.Pos.CENTER);
        lbEficienciaDia.setLayoutX(72.0);
        lbEficienciaDia.setLayoutY(133.0);
        lbEficienciaDia.setPrefHeight(30.0);
        lbEficienciaDia.setPrefWidth(109.0);
        lbEficienciaDia.getStyleClass().add((eficiencia < .8 ? "danger" : eficiencia >= 1 ? "success" : "warning"));
        lbEficienciaDia.setText(StringUtils.toPercentualFormat(eficienciaDia,2));
        lbEficienciaDia.setFont(new Font("System Bold", 24.0));
        lbEficienciaDia.setPadding(new Insets(-12.0, 2.0, -12.0, 2.0));
    
        AnchorPane.setRightAnchor(lbTitleProdutividade, 0.0);
        lbTitleProdutividade.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbTitleProdutividade.setLayoutX(75.0);
        lbTitleProdutividade.setLayoutY(166.0);
        lbTitleProdutividade.setText("PRODUTIVIDADE");
        lbTitleProdutividade.setTextAlignment(javafx.scene.text.TextAlignment.RIGHT);
        lbTitleProdutividade.setFont(new Font(14.0));
    
        AnchorPane.setRightAnchor(lbProdutividade, 0.0);
        lbProdutividade.setAlignment(javafx.geometry.Pos.CENTER);
        lbProdutividade.setLayoutX(72.0);
        lbProdutividade.setLayoutY(186.0);
        lbProdutividade.setPrefHeight(30.0);
        lbProdutividade.setPrefWidth(109.0);
        lbProdutividade.getStyleClass().add((eficiencia < .8 ? "danger" : eficiencia >= 1 ? "success" : "warning"));
        lbProdutividade.setText(StringUtils.toPercentualFormat(produtividade,2));
        lbProdutividade.setFont(new Font("System Bold", 24.0));
        lbProdutividade.setPadding(new Insets(-12.0, 2.0, -12.0, 2.0));
        
        anchorWindow.setPadding(new Insets(10.0));
        anchorWindow.getChildren().add(lbTitleInfosColaborador);
        anchorWindow.getChildren().add(imgSucessoLancamento);
        anchorWindow.getChildren().add(lbTitleSucesso);
        anchorWindow.getChildren().add(lbTitleEficienciaPacote);
        anchorWindow.getChildren().add(lbEficienciaPacote);
        anchorWindow.getChildren().add(lbTitleEficienciaDia);
        anchorWindow.getChildren().add(lbEficienciaDia);
        anchorWindow.getChildren().add(lbTitleProdutividade);
        anchorWindow.getChildren().add(lbProdutividade);
        
        Scene scene = new Scene(anchorWindow);
        Stage stage = new Stage();
        
        stage.centerOnScreen();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
    
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(5),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        stage.close();
                    }
                }));
        timeline.play();
    }
    
    public static void showMessage(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static Stage showMessageFullScreen(String message, AlertType type) {

        VBox main = new VBox();
        ImageView imageView = new ImageView();
        Label label = new Label();

        main.setAlignment(javafx.geometry.Pos.CENTER);
        main.setMaxHeight(USE_PREF_SIZE);
        main.setMaxWidth(USE_PREF_SIZE);
        main.setMinHeight(USE_PREF_SIZE);
        main.setMinWidth(USE_PREF_SIZE);
        main.setPrefHeight(686.0);
        main.setPrefWidth(1080.0);
        main.setSpacing(20.0);
        main.getStylesheets().add("/styles/sysDelizDesktop.css");

        imageView.setFitHeight(100.0);
        imageView.setFitWidth(100.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);

        if (type == AlertType.ERROR) {
            main.getStyleClass().add("danger");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert error.png")));
        } else if (type == AlertType.WARNING) {
            main.getStyleClass().add("warning");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert warning.png")));
        } else if (type == AlertType.INFORMATION) {
            main.getStyleClass().add("info");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert info.png")));
        } else if (type == AlertType.CONFIRMATION) {
            main.getStyleClass().add("success");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert success.png")));
        }

        VBox.setVgrow(label, javafx.scene.layout.Priority.ALWAYS);
        label.setAlignment(javafx.geometry.Pos.TOP_CENTER);
        label.setContentDisplay(javafx.scene.control.ContentDisplay.TOP);
        label.setPrefHeight(320.0);
        label.setPrefWidth(1013.0);
        label.setText(message);
        label.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        label.setWrapText(true);
        label.setFont(new Font("System Bold", 28.0));

        main.getChildren().add(imageView);
        main.getChildren().add(label);

        Scene scene = new Scene(main);
        Stage stage = new Stage();
        stage.setMaximized(true);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        return stage;
    }
    
    public static Stage showMessageFullScreenWithButtonClose(String message, AlertType type) {
        return showMessageFullScreenWithButtonClose(message, type, null);
    }
    
    public static Stage showMessageFullScreenWithButtonClose(String message, AlertType type, Button btnMessage) {
        
        VBox main = new VBox();
        ImageView imageView = new ImageView();
        Label label = new Label();
        ImageView imgBtnClose = new ImageView(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/cancel (3).png")));
        Button btnClose = new Button();
    
        Scene scene = new Scene(main);
        Stage stage = new Stage();
        stage.setMaximized(true);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        
        main.setAlignment(javafx.geometry.Pos.CENTER);
        main.setMaxHeight(USE_PREF_SIZE);
        main.setMaxWidth(USE_PREF_SIZE);
        main.setMinHeight(USE_PREF_SIZE);
        main.setMinWidth(USE_PREF_SIZE);
        main.setPrefHeight(686.0);
        main.setPrefWidth(1080.0);
        main.setSpacing(20.0);
        main.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        imageView.setFitHeight(100.0);
        imageView.setFitWidth(100.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        
        if (type == AlertType.ERROR) {
            main.getStyleClass().add("danger");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert error.png")));
        } else if (type == AlertType.WARNING) {
            main.getStyleClass().add("warning");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert warning.png")));
        } else if (type == AlertType.INFORMATION) {
            main.getStyleClass().add("info");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert info.png")));
        } else if (type == AlertType.CONFIRMATION) {
            main.getStyleClass().add("success");
            imageView.setImage(new Image(GUIUtils.class.getResourceAsStream("/images/icons/buttons/alert success.png")));
        }
        
        VBox.setVgrow(label, javafx.scene.layout.Priority.ALWAYS);
        label.setAlignment(javafx.geometry.Pos.TOP_CENTER);
        label.setContentDisplay(javafx.scene.control.ContentDisplay.TOP);
        label.setPrefHeight(320.0);
        label.setPrefWidth(1013.0);
        label.setText(message);
        label.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        label.setWrapText(true);
        label.setFont(new Font("System Bold", 36.0));
        
        main.getChildren().add(imageView);
        main.getChildren().add(label);
        
        if(btnMessage == null) {
            btnClose.setText("FECHAR");
            btnClose.setGraphic(imgBtnClose);
            btnClose.setPrefHeight(70.0);
            btnClose.setPrefWidth(200.0);
            btnClose.getStyleClass().add("primary");
            btnClose.setStyle("-fx-font-size:24");
            btnClose.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    stage.close();
                }
            });
            main.getChildren().add(btnClose);
        }else{
            btnMessage.setPrefHeight(70.0);
            btnMessage.getStyleClass().add("primary");
            btnMessage.setStyle("-fx-font-size:24");
            main.getChildren().add(btnMessage);
        }
        
        
        return stage;
    }
    
    public static void showMessageWithGrade(String message, String grade) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(message);

        TextArea textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setFont(new Font("Consolas", 11.0));
        textArea.setText(grade);
        textArea.setPrefWidth(400.0);
        textArea.setPrefHeight(30.0);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();
    }

    public static void showMessage(String message, AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static String[] showTextAreaDialogWithRemember(String message, String textDefault) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(message);

        TextArea textArea = new TextArea();
        textArea.setEditable(true);
        textArea.setWrapText(true);
        textArea.setText(textDefault);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        CheckBox lembrar = new CheckBox("Não abrir novamente para a marca.");
        lembrar.setSelected(true);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);
        expContent.add(lembrar, 0, 1);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

        String[] returnValue = {textArea.getText(), lembrar.selectedProperty().get() ? "true" : "false"};
        return returnValue;
    }

    public static String[] showTextAreaDialog(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(message);

        TextArea textArea = new TextArea();
        textArea.setEditable(true);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

        return textArea.getText().split("\n");
    }

    public static String showTextFieldDialog(String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(message);

        TextField textField = new TextField();
        textField.setEditable(true);
        TextFieldUtils.upperCase(textField);

        textField.setMaxWidth(Double.MAX_VALUE);
        textField.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textField, Priority.ALWAYS);
        GridPane.setHgrow(textField, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textField, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

        return textField.getText();
    }
    
    public static Integer showComboBoxDialog(String message, Map<String, Integer> opcoes) {
        Alert alert = new Alert(AlertType.NONE);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(message);
        
        List<String> descricoes = new ArrayList<>(opcoes.keySet());
        ComboBox<String> comboBox = new ComboBox<String>(FXCollections.observableList(descricoes));
        comboBox.setEditable(false);
        comboBox.getSelectionModel().select(0);
    
        comboBox.setMaxWidth(Double.MAX_VALUE);
        comboBox.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(comboBox, Priority.ALWAYS);
        GridPane.setHgrow(comboBox, Priority.ALWAYS);
        
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(comboBox, 0, 0);
        
        alert.getDialogPane().setContent(expContent);
        alert.getButtonTypes().add(ButtonType.APPLY);
        
        alert.showAndWait();
        return opcoes.get(comboBox.getValue());
    }

    public static void showException(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Erro Exceção");
        alert.setHeaderText("Aconteceu um erro no sistema!\nMensagem do sistema: " + ex.getMessage());
        alert.setContentText("Informe a informática com o seguinte erro abaixo:");

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("A mensagem de exceção foi:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);
    
        LocalLogger.addLog(ex.getMessage() + "::" + ex.getLocalizedMessage(), "::");

        alert.showAndWait();
    }

    public static ButtonType showMessageNoButtonFocus(String message, AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("SysDeliz");
        alert.setContentText(message);
        alert.setHeaderText(null);

        //ButtonType buttonFicar = new ButtonType("?");
        //ButtonType buttonOK = new ButtonType("OK");
        //alert.getButtonTypes().setAll(buttonFicar, buttonOK);
        Optional<ButtonType> result = alert.showAndWait();
        if (!result.isPresent()) {
            return ButtonType.NO;
        } else if (result.get() == ButtonType.OK) {
            showMessageNoButtonFocus(message, type);
        }
        return ButtonType.NO;
    }

    public static boolean showQuestionMessageDefaultSim(String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);

        ButtonType buttonSim = new ButtonType("Sim");
        ButtonType buttonNao = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonSim, buttonNao);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonSim) {
            return true;
        } else if (result.get() == buttonNao) {
            return false;
        } else {
            return false;
        }
    }

    public static boolean showQuestionMessageDefaultNao(String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);

        ButtonType buttonSim = new ButtonType("Sim");
        ButtonType buttonNao = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonNao, buttonSim);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonSim) {
            return true;
        } else if (result.get() == buttonNao) {
            return false;
        } else {
            return false;
        }
    }
    
    public static String showSaveFileDialog(List<FileChooser.ExtensionFilter> extendionFiles){
        FileChooser fileChooser = new FileChooser();
    
        //Set extension filter for text files
        fileChooser.getExtensionFilters().addAll(extendionFiles);
    
        //Show save file dialog
        File file = fileChooser.showSaveDialog(Globals.getMainStage());
        
        return file != null ? file.getAbsolutePath() : null;
    }

    public static String getStatusReserva(String codigo) {
        String retorno = "";

        switch (codigo) {
            case "A":
                retorno = "Aberto";
                break;
            case "I":
                retorno = "Impresso";
                break;
            case "P":
                retorno = "Lido";
                break;
            case "C":
                retorno = "Em Caixa";
                break;
            case "E":
                retorno = "Pronto Para Faturar";
                break;
            case "F":
                retorno = "Faturado";
                break;
        }

        return retorno;
    }

    public static String getStyleStatusReserva(String codigo) {
        String retorno = "";

        switch (codigo) {
            case "I":
                retorno = "statusImpresso";
                break;
            case "P":
                retorno = "statusLido";
                break;
            case "C":
                retorno = "statusLido";
                break;
            case "E":
                retorno = "statusExpedido";
                break;
            case "F":
                retorno = "statusFaturado";
                break;
        }

        return retorno;
    }

    public static Alert spinnerLoad() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("SysDeliz");
        alert.setContentText("Carregando...");

        ProgressIndicator spinner = new ProgressIndicator();

        spinner.setMaxWidth(Double.MAX_VALUE);
        spinner.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(spinner, Priority.ALWAYS);
        GridPane.setHgrow(spinner, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(spinner, 0, 0);

        alert.getDialogPane().setContent(expContent);
        alert.getButtonTypes().set(0, ButtonType.OK);
        return alert;
    }

}
