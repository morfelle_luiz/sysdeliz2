/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.stage.Stage;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.properties.Empresa;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.ti.Codigos;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.StringUtils;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author cristiano.diego
 */
public class Globals {
    
    public static List<Empresa> empresas = Arrays.asList(new Empresa("1000","Deliz Fashion Group", new Image("/images/logo deliz (1).png"), "03306680000164", new Image("/images/logo deliz (1).png"))
                                                       , new Empresa("2000","UP Wave",new Image("/images/logo-upwave.png"), "39304078000135", new Image("/images/icon-upwave.png")));
    
    private static AcessoSistema usuarioLogado;
    private static Stage stageMain;
    private static Empresa empresaLogada = empresas.get(0);

    private static String nomeUsuario;
    private static boolean portatil = false;

    private static SdColaborador colaboradorSistema = null;

    public static AcessoSistema getUsuarioLogado() {
        return usuarioLogado;
    }
    public static void setUsuarioLogado(AcessoSistema usuarioLogado) {
        Globals.usuarioLogado = usuarioLogado;
        Globals.nomeUsuario = usuarioLogado.getUsuario();
    }
    
    public static Stage getMainStage() {
        return stageMain;
    }
    public static void setMainStage(Stage stageMain) {
        Globals.stageMain = stageMain;
    }
    
    public static Empresa getEmpresaLogada() {
        return empresaLogada;
    }
    public static void setEmpresaLogada(Empresa empresaLogada) {
        Globals.empresaLogada = empresaLogada;
    }
    
    public static String getProximoCodigo(String tabela, String campo) {
        String codigo = null;
        Codigos tabelaCodigo = new FluentDao().selectFrom(Codigos.class)
                .where(eb -> eb
                        .equal("codigosId.tabela", tabela)
                        .equal("codigosId.campo", campo))
                .singleResult();
        JPAUtils.getEntityManager().refresh(tabelaCodigo);
        codigo = StringUtils.lpad(String.valueOf(tabelaCodigo.getProximo()), tabelaCodigo.getTamanho(), "0");
        tabelaCodigo.setProximo(tabelaCodigo.getProximo().add(BigDecimal.ONE));
        new FluentDao().merge(tabelaCodigo);
        return codigo;
    }

    public static String getNomeUsuario() {
        return nomeUsuario;
    }
    public static void setNomeUsuario(String nomeUsuario) {
        Globals.nomeUsuario = nomeUsuario;
    }

    public static boolean isPortatil() {
        return portatil;
    }
    public static void setPortatil(boolean portatil) {
        Globals.portatil = portatil;
    }

    public static SdColaborador getColaboradorSistema() {
        return colaboradorSistema;
    }
    public static void setColaboradorSistema(SdColaborador colaboradorSistema) {
        Globals.colaboradorSistema = colaboradorSistema;
    }

    public static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");

    public static SdColaborador getColaborador(String usuario) {
        return new FluentDao().selectFrom(SdColaborador.class)
                .where(it -> it
                        .equal("usuario", usuario)
                        .equal("ativo", true))
                .singleResult();
    }

    // <editor-fold defaultstate="collapsed" desc="Gatilhos para Regras Comerciais">
    public static List<GatilhosIndicadores> gatilhos = new ArrayList<>(Arrays.asList(
        new GatilhosIndicadores("LINHA", "sysdeliz2.models.ti.Linha")
    ));
    public static class GatilhosIndicadores {
        public String gatilho;
        public String classeGatilho;

        public GatilhosIndicadores(String gatilho, String classeGatilho) {
            this.gatilho = gatilho;
            this.classeGatilho = classeGatilho;
        }

        @Override
        public String toString() {
            return gatilho;
        }
    }
    public static GatilhosIndicadores getGatilho(String gatilho) {
        return gatilhos.stream().filter(gat -> gat.gatilho.equals(gatilho)).findFirst().orElse(null);
    }
    // </editor-fold>

}
