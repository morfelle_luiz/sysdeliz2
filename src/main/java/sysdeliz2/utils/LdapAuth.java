/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sysdeliz2.models.SdUsuario;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class LdapAuth {

    private final static String ldapURI = "ldap://divad01.nowar.corp:389/";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
    private final static String accountSuffex = "@nowar.corp";
    private static LdapContext ctx;

    public static AcessoSistema authenticateUserAndGetInfo(String user, String password) throws Exception {
        try {
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
            env.put(Context.PROVIDER_URL, ldapURI);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, user + accountSuffex);
            env.put(Context.SECURITY_CREDENTIALS, password);

            ctx = new InitialLdapContext(env, null);
            return getListUserInfo(user, ctx);
        } catch (javax.naming.AuthenticationException e) {
            String data = "dd/MM/yyyy";
            String hora = "HH:mm";
            String data1, hora1;
            java.util.Date agora = new java.util.Date();
            SimpleDateFormat formata = new SimpleDateFormat(data);
            data1 = formata.format(agora);
            formata = new SimpleDateFormat(hora);
            hora1 = formata.format(agora);
            return new AcessoSistema("", "", user, "", hora1, data1, "", "");
        }
    }
    
    private static void getAttributesInfo(String userName, LdapContext ctx, SearchControls searchControls) {
        System.out.println("userName: " + userName);
        String baseDN = "dc=nowar,dc=corp";
        try {
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> answer = ctx.search(baseDN, "sAMAccountName=" + userName, controls);
            if (answer.hasMore()) {
                Attributes attrs = answer.next().getAttributes();

                NamingEnumeration e = attrs.getAll();
                System.out.println("---------------------------------------");
                while (e.hasMoreElements()) {
                    Attribute attr = (Attribute) e.nextElement();

                    System.out.print(attr.getID() + " = ");
                    for (int i = 0; i < attr.size(); i++) {
                        if (i > 0) {
                            System.out.print(", ");
                        }
                        System.out.print(attr.get(i));
                    }
                    System.out.println();
                }
                System.out.println("---------------------------------------");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static AcessoSistema getListUserInfo(String userName, LdapContext ctx) throws NamingException, SQLException {
        String baseDN = "dc=nowar,dc=corp";
        AcessoSistema loginUsuario = null;
        String us_test;
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        NamingEnumeration<SearchResult> answer = ctx.search(baseDN, "(objectClass=user)", controls);
        while (answer.hasMoreElements()) {
            Attributes attrs = answer.next().getAttributes();
            us_test = attrs.get("samaccountname").get().toString();
            if (us_test.equals(userName)) {
                String data = "dd/MM/yyyy";
                String hora = "HH:mm";
                String data1, hora1;
                java.util.Date agora = new java.util.Date();
                SimpleDateFormat formata = new SimpleDateFormat(data);
                data1 = formata.format(agora);
                formata = new SimpleDateFormat(hora);
                hora1 = formata.format(agora);
                String telefone = attrs.get("telephoneNumber") != null ? attrs.get("telephoneNumber").toString().split(":")[1].trim() : "";

                loginUsuario = new AcessoSistema(attrs.get("name").toString().split(":")[1].trim(), attrs.get("givenname").toString().split(":")[1].trim(),
                        userName, attrs.get("mail") != null ? attrs.get("mail").toString().split(":")[1].trim() : " ", hora1, data1, attrs.get("displayName").toString().split(":")[1].trim(), telefone);
                NamingEnumeration grupos = attrs.get("memberOf").getAll();
                ObservableList<String> permissoes = FXCollections.observableArrayList();
                while (grupos.hasMore()) {
                    Object obj = grupos.next();
                    permissoes.add(obj.toString().replace(",OU=SysDelizOU,OU=Grupos,OU=Blumenau,DC=nowar,DC=corp","").replace("CN=",""));
                    if (obj.toString().contains("SysDeliz") || obj.toString().contains("SysUPWave")) {
                        loginUsuario.setIsSistemaLiberado(true);
                    }
                }
                loginUsuario.setListPermissoesUsuario(permissoes);
//                String[] dadosUsuarioBanco = DAOFactory.getUsuariosDAO().getDadosUsuario(loginUsuario.getNome());
//                loginUsuario.setCodFunTi(dadosUsuarioBanco[0]);
//                loginUsuario.setCodUsuarioTi(dadosUsuarioBanco[2]);
//                loginUsuario.setUsuarioTi(dadosUsuarioBanco[1]);
            }
        }
        return loginUsuario;
    }

    public static List<SdUsuario> getAllUsers() throws NamingException {
        List<SdUsuario> allUsers = new ArrayList<>();

        String baseDN = "dc=nowar,dc=corp";
        String grupo = Globals.getEmpresaLogada().getCodigo().equals("1000") ? "OU=SysDelizOU" : "CN=SysUPWave";
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        NamingEnumeration<SearchResult> answer = ctx.search(baseDN, "(objectClass=user)", controls);
        while (answer.hasMoreElements()) {
            if (answer.hasMore()) {
                Attributes attrs = answer.next().getAttributes();
                NamingEnumeration groups = attrs.get("memberOf") != null ? attrs.get("memberOf").getAll() : null;
                List<String> list = new ArrayList<>();
                if(groups != null){
                    while(groups.hasMore()){
                        list.addAll(Arrays.asList(groups.next().toString().split(",")));
                    }
                }
                if (list.stream().anyMatch(it -> it.equals(grupo))) {
                    allUsers.add(new SdUsuario(attrs.get("name").get().toString(),
                            attrs.get("userPrincipalName").get().toString().replace(accountSuffex, ""),
                            new ArrayList<>()));
                }
            }
        }
        return allUsers;
    }

}
