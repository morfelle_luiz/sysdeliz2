package sysdeliz2.utils;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoadingModalWorker {

    private final Stage mainStage = new Stage();
    private final VBox mainPane = new VBox();
    private final Label lbTitleModal = new Label();
    private final ProgressIndicator loadingBar = new ProgressIndicator();
    private boolean dialogVisible = false;
    private boolean cancelDialogShow = false;
    private Worker<?> worker;

    private ChangeListener<Worker.State> stateListener = new ChangeListener<Worker.State>() {
        @Override
        public void changed(ObservableValue<? extends Worker.State> observable, Worker.State old, Worker.State value) {
            switch (value) {
                case CANCELLED:
                case FAILED:
                case SUCCEEDED:
                    if (!dialogVisible) {
                        cancelDialogShow = true;
                        end();
                    } else if (old == Worker.State.SCHEDULED || old == Worker.State.RUNNING) {
                        end();
                    }
                    break;
                case SCHEDULED:
                    begin();
                    break;
                default: //no-op
                }
        }
    };

    private final void setWorker(final Worker<?> newWorker) {
        if (newWorker != worker) {
            if (worker != null) {
                worker.stateProperty().removeListener(stateListener);
                end();
            }

            worker = newWorker;

            if (newWorker != null) {
                newWorker.stateProperty().addListener(stateListener);
                if (newWorker.getState() == Worker.State.RUNNING || newWorker.getState() == Worker.State.SCHEDULED) {
                    // It is already running
                    begin();
                }
            }
        }
    }

    private void begin() {
        // Platform.runLater needs to be used to show the dialog because
        // the call begin() is going to be occurring when the worker is
        // notifying state listeners about changes.  If Platform.runLater
        // is not used, the call to show() will cause the worker to get
        // blocked during notification and it will prevent the worker
        // from performing any additional notification for state changes.
        //
        // Sine the dialog is hidden as a result of a change in worker
        // state, calling show() without wrapping it in Platform.runLater
        // will cause the progress dialog to run forever when the dialog
        // is attached to workers that start out with a state of READY.
        //
        // This also creates a case where the worker's state can change
        // to finished before the dialog is shown, resulting in an
        // an attempt to hide the dialog before it is shown.  It's
        // necessary to track whether or not this occurs, so flags are
        // set to indicate if the dialog is visible and if if the call
        // to show should still be allowed.
        cancelDialogShow = false;

        Platform.runLater(() -> {
            if (!cancelDialogShow) {
                loadingBar.progressProperty().bind(worker.progressProperty());
                lbTitleModal.textProperty().bind(worker.messageProperty());
                dialogVisible = true;
                if(!mainStage.isShowing()) {
                    mainStage.showAndWait();
                }
            }
        });
    }

    private void end() {
        loadingBar.progressProperty().unbind();
        lbTitleModal.textProperty().unbind();
        dialogVisible = false;
        mainStage.close();
    }

    public LoadingModalWorker(final Worker<?> worker) {
        this(worker, false);
    }

    public LoadingModalWorker(final Worker<?> worker, boolean wait){
        if (worker != null
                && (worker.getState() == Worker.State.CANCELLED
                || worker.getState() == Worker.State.FAILED
                || worker.getState() == Worker.State.SUCCEEDED)) {
            return;
        }
        this.setWorker(worker);

        this.initializeComponents();

        Scene scene = new Scene(mainPane);

        mainStage.setScene(scene);
        mainStage.setOpacity(0.7);
        mainStage.setMaximized(true);

        mainStage.initOwner(Globals.getMainStage());
        mainStage.initModality(Modality.WINDOW_MODAL);
        mainStage.initStyle(StageStyle.DECORATED);

        new Thread((Task<?>) worker).start();

        if(wait){
            try {
                if(mainStage.isShowing()){
                    mainStage.hide();
                }

                mainStage.showAndWait();
            }catch(Exception e){}
        }
    }

    private void initializeComponents() {
        mainPane.setAlignment(javafx.geometry.Pos.CENTER);
        mainPane.setPrefHeight(765.0);
        mainPane.setPrefWidth(1107.0);
        mainPane.setSpacing(10.0);
        mainPane.setStyle("-fx-background-color: rgba(0, 0, 0, 0.3);");

        lbTitleModal.setAlignment(javafx.geometry.Pos.CENTER);
        lbTitleModal.setPrefHeight(0.0);
        lbTitleModal.setPrefWidth(1107.0);
        lbTitleModal.setFont(new Font("System Bold", 16.0));
        lbTitleModal.setTextAlignment(TextAlignment.CENTER);
        lbTitleModal.setOpacity(1);

        loadingBar.setPrefHeight(93.0);
        loadingBar.setPrefWidth(1107.0);
        loadingBar.setOpacity(1);
    
        Screen screen = Screen.getPrimary();
        Rectangle2D screenBounds = screen.getVisualBounds();
        StackPane p = new StackPane();
    
        Rectangle r = RectangleBuilder.create()
                .height(screenBounds.getHeight()).width(screenBounds.getWidth())
                .fill(Color.web("black", 0.3))
                .build();
    
        //p.getChildren().addAll(r, ProgressIndicatorBuilder.create().maxWidth(500).maxHeight(500).build());
        p.getChildren().addAll(r, new ImageView(new Image(getClass().getResource("/images/loading.gif").toExternalForm())));
        
//        mainPane.getChildren().add(lbTitleModal);
        mainPane.getChildren().add(p);
    }

}
