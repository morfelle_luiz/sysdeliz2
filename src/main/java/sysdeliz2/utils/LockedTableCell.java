/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.application.Platform;
import javafx.scene.AccessibleAttribute;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableCell;
import javafx.scene.layout.Region;

/**
 *
 * @author cristiano.diego
 */
public abstract class LockedTableCell<T, S> extends TableCell<T, S> {

    {
        Platform.runLater(() -> {

            ScrollBar sc = (ScrollBar) getTableView().queryAccessibleAttribute(AccessibleAttribute.HORIZONTAL_SCROLLBAR);
            TableHeaderRow thr = (TableHeaderRow) getTableView().queryAccessibleAttribute(AccessibleAttribute.HEADER);
            Region headerNode = thr.getColumnHeaderFor(this.getTableColumn());

            sc.valueProperty().addListener((ob, o, n) -> {
                double doubleValue = n.doubleValue();
                headerNode.setTranslateX(doubleValue);
                headerNode.toFront();
                this.setTranslateX(doubleValue);
                this.toFront();
            });
        });
    }
}
