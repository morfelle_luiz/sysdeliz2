package sysdeliz2.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class LoggerUtils {

    public static String diferencaDeObjs(Object obj, Object old) throws IllegalAccessException {
        Map<String, String> diffs = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object fieldValue1 = field.get(old).toString();
            Object fieldValue2 = field.get(obj).toString();
            if (!fieldValue1.equals(fieldValue2))
                diffs.put("\n" + fieldName + " antigo(a) " + fieldValue1, "\n   " + fieldName + " novo(a)" + fieldValue2 + "\n");
        }
        return diffs.toString();
    }
}
