/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 * @author cristiano.diego
 */
public class Modals {
    
    public enum FXMLWindow {
        SceneFuncaoProcura("/sysdeliz2/controllers/fxml/SceneFuncaoProcura.fxml"),
        SceneCreateNewPart("/sysdeliz2/controllers/fxml/SceneCreateNewPart.fxml"),
        SceneSaveLiberacaoPcp("/sysdeliz2/controllers/fxml/SceneSaveLiberacaoPcp.fxml"),
        SceneHistLiberacaoPcp("/sysdeliz2/controllers/fxml/SceneHistLiberacaoPcp.fxml"),
        SceneHistRevMetaPcp("/sysdeliz2/controllers/fxml/SceneHistRevisaoPcp.fxml"),
        SceneProcurarRepresentante("/sysdeliz2/controllers/fxml/procura/SceneProcurarRepresentante.fxml"),
        SceneProcurarColecao("/sysdeliz2/controllers/fxml/procura/SceneProcurarColecao.fxml"),
        SceneProcurarLinha("/sysdeliz2/controllers/fxml/procura/SceneProcurarLinha.fxml"),
        SceneProcurarFamilia("/sysdeliz2/controllers/fxml/procura/SceneProcurarFamilia.fxml"),
        SceneProcurarMarca("/sysdeliz2/controllers/fxml/procura/SceneProcurarMarca.fxml"),
        SceneProcurarCliente("/sysdeliz2/controllers/fxml/procura/SceneProcurarCliente.fxml"),
        SceneProcurarCidade("/sysdeliz2/controllers/fxml/procura/SceneProcurarCidade.fxml"),
        SceneProcurarFornecedor("/sysdeliz2/controllers/fxml/procura/SceneProcurarFornecedor.fxml"),
        FinanceiroSceneCompPagamento("/sysdeliz2/controllers/fxml/financeiro/SceneCompPagamento.fxml"),
        ComprasObservacaoOc("/sysdeliz2/controllers/fxml/compras/SceneObservacaoOc.fxml"),
        ComprasAgendaReenvioOc("/sysdeliz2/controllers/fxml/compras/SceneAdicionarAgendaOc.fxml"),
        ObservacaoPcpProduto("/sysdeliz2/controllers/fxml/pcp/SceneProdutoObservacaoPcp.fxml"),
        FilterColecao("/sysdeliz2/controllers/fxml/procura/FilterColecao.fxml"),
        FilterProduto("/sysdeliz2/controllers/fxml/procura/FilterProduto.fxml"),
        FilterCliente("/sysdeliz2/controllers/fxml/procura/FilterCliente.fxml"),
        FilterRepresentante("/sysdeliz2/controllers/fxml/procura/FilterRepresentante.fxml"),
        FilterPeriodo("/sysdeliz2/controllers/fxml/procura/FilterPeriodo.fxml"),
        FilterMarca("/sysdeliz2/controllers/fxml/procura/FilterMarca.fxml"),
        FilterLinha("/sysdeliz2/controllers/fxml/procura/FilterLinha.fxml"),
        FilterEstampa("/sysdeliz2/controllers/fxml/procura/FilterEstampa.fxml"),
        FilterFamilia("/sysdeliz2/controllers/fxml/procura/FilterFamilia.fxml"),
        FilterCidade("/sysdeliz2/controllers/fxml/procura/FilterCidade.fxml"),
        FilterGrupoCli("/sysdeliz2/controllers/fxml/procura/FilterGrupoEconomico.fxml"),
        FilterSitCli("/sysdeliz2/controllers/fxml/procura/FilterSituacaoCliente.fxml"),
        ProdutoGradeProduto("/sysdeliz2/controllers/fxml/comercial/SceneComercialGestaoPedidosGradeProduto.fxml"),
        FilterTurno("/sysdeliz2/controllers/fxml/procura/FilterTurno.fxml"),
        FilterFuncaoColaborador("/sysdeliz2/controllers/fxml/procura/FilterFuncaoColaborador.fxml"),
        FilterGrupoOperacao("/sysdeliz2/controllers/fxml/procura/FilterGrupoOperacao.fxml"),
        FilterSetorOp("/sysdeliz2/controllers/fxml/procura/FilterSetorOp.fxml"),
        FilterColaborador("/sysdeliz2/controllers/fxml/procura/FilterColaborador.fxml"),
        FilterCelula("/sysdeliz2/controllers/fxml/procura/FilterCelula.fxml"),
        FilterFornecedor("/sysdeliz2/controllers/fxml/procura/FilterFornecedor.fxml"),
        FilterNivel("/sysdeliz2/controllers/fxml/procura/FilterNivel.fxml"),
        FilterColaboradorSenior("/sysdeliz2/controllers/fxml/procura/FilterColaboradorSenior.fxml"),
        FilterEstados("/sysdeliz2/controllers/fxml/procura/FilterEstados.fxml"),
        FilterSdCidade("/sysdeliz2/controllers/fxml/procura/FilterSdCidade.fxml"),
        FilterSdRegiao("/sysdeliz2/controllers/fxml/procura/FilterSdRegiao.fxml"),
        Keyboard("/sysdeliz2/controllers/fxml/pcp/shopfloor/NumericKeyboard.fxml"),
        CadastroHorarioLancamento("/sysdeliz2/controllers/fxml/pcp/shopfloor/SceneCadastraHorarioLancamento.fxml"),
        LancamentoParada("/sysdeliz2/controllers/fxml/pcp/shopfloor/ScenePcpLancamentoParada.fxml"),
        FilterSdCidadeSingleResult("/sysdeliz2/controllers/fxml/procura/FilterSdCidadeSingleResult.fxml"),
        ConsultaApontamentoOperacoesAgrupadas("/sysdeliz2/controllers/fxml/pcp/shopfloor/ScenePcpOperacoesAgrupadas.fxml");
        
        public String fxmlFile;
        
        FXMLWindow(String value) {
            fxmlFile = value;
        }
        
        public String getValor() {
            return fxmlFile;
        }
    }
    
    public StageStyle styleWindow = StageStyle.DECORATED;
    private String fileFxml;
    
    public Modals(FXMLWindow file) {
        if (file != null) {
            fileFxml = file.getValor();
        }
    }
    
    public void show(Object controller) throws IOException {
        
        FXMLLoader root = new FXMLLoader(getClass().getResource(fileFxml));
        root.setController(controller);
        Stage modalStage = new Stage();
        Scene scene;
        scene = new Scene(root.load());
        scene.getStylesheets().add("/styles/bootstrap2.css");
        scene.getStylesheets().add("/styles/bootstrap3.css");
        scene.getStylesheets().add("/styles/styles.css");
        scene.getStylesheets().add("/styles/stylePadrao.css");
        scene.getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");

        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/delizIcon.png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(styleWindow);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
    }
    
}
