package sysdeliz2.utils;

import com.thoughtworks.xstream.XStream;

import java.io.File;

public class NotaFiscalUtils {

    private static XStream xStream = new XStream();

    public static NotaFiscalXML readAndGetNF (File file) {
        xStream.alias("nfeProc", NotaFiscalXML.class);
        xStream.alias("NFe", NotaFiscalXML.NFe.class);
        xStream.alias("ide", NotaFiscalXML.Ide.class);
        xStream.alias("det", NotaFiscalXML.Det.class);
        xStream.alias("prod", NotaFiscalXML.Prod.class);

        xStream.addImplicitCollection(NotaFiscalXML.Ide.class,"NFref", NotaFiscalXML.NFref.class);
        xStream.addImplicitCollection(NotaFiscalXML.InfNFe.class,"detList","det" , NotaFiscalXML.Det.class);
        xStream.addImplicitCollection(NotaFiscalXML.Signature.SignedInfo.Reference.Transforms.class,"transformList","Transform" , NotaFiscalXML.Signature.SignedInfo.Reference.Transforms.Transform.class);

        xStream.useAttributeFor(NotaFiscalXML.class, "versao");
        xStream.useAttributeFor(NotaFiscalXML.class, "xmlns");
        xStream.useAttributeFor(NotaFiscalXML.NFe.class, "xmlns");
        xStream.useAttributeFor(NotaFiscalXML.Det.class, "nItem");
        xStream.useAttributeFor(NotaFiscalXML.InfNFe.class, "Id");
        xStream.useAttributeFor(NotaFiscalXML.InfNFe.class, "versao");
        xStream.useAttributeFor(NotaFiscalXML.Signature.class, "xmlns");
        xStream.useAttributeFor(sysdeliz2.utils.NotaFiscalXML.Signature.SignedInfo.CanonicalizationMethod.class, "Algorithm");
        xStream.useAttributeFor(sysdeliz2.utils.NotaFiscalXML.Signature.SignedInfo.SignatureMethod.class, "Algorithm");
        xStream.useAttributeFor(sysdeliz2.utils.NotaFiscalXML.Signature.SignedInfo.Reference.class, "URI");
        xStream.useAttributeFor(NotaFiscalXML.Signature.SignedInfo.Reference.Transforms.Transform.class, "Algorithm");
        xStream.useAttributeFor(sysdeliz2.utils.NotaFiscalXML.Signature.SignedInfo.Reference.DigestMethod.class, "Algorithm");
        xStream.useAttributeFor(NotaFiscalXML.ProtNFe.class, "xmlns");
        xStream.useAttributeFor(NotaFiscalXML.ProtNFe.class, "versao");

//        xStream.ignoreUnknownElements();

        return (NotaFiscalXML) xStream.fromXML(file);
    }

    public void returnXMLString () {
        xStream = new XStream();
        System.out.println(xStream.toXML(new NotaFiscalXML()));
    }
}
