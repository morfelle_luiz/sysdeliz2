package sysdeliz2.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NotaFiscalXML {

    @XStreamAsAttribute
    private String versao;
    @XStreamAsAttribute
    private String xmlns;

    private NFe NFe = new NFe();
    private ProtNFe protNFe = new ProtNFe();

    @XStreamAlias("NFe")
    public static class NFe {

        @XStreamAsAttribute
        private String xmlns;

        public InfNFe infNFe = new InfNFe();

        private Signature Signature = new Signature();

        public String getXmlns() {
            return xmlns;
        }

        public InfNFe getInfNFe() {
            return infNFe;
        }

        public NotaFiscalXML.Signature getSignature() {
            return Signature;
        }
    }

    public static class InfNFe {
        @XStreamAsAttribute
        private String Id;
        @XStreamAsAttribute
        private String versao;

        private Ide ide = new Ide();
        private Emit emit = new Emit();
        private Dest dest = new Dest();
        private AutXML autXML = new AutXML();
        private List<Det> detList = new ArrayList<>();
        private Total total = new Total();
        private Transp transp = new Transp();
        private Cobr cobr = new Cobr();
        private Pag pag = new Pag();
        private InfAdic infAdic = new InfAdic();
        private Exporta exporta = new Exporta();
        private InfRespTec infRespTec = new InfRespTec();

        public String getId() {
            return Id;
        }

        public String getVersao() {
            return versao;
        }

        public Ide getIde() {
            return ide;
        }

        public Emit getEmit() {
            return emit;
        }

        public Dest getDest() {
            return dest;
        }

        public AutXML getAutXML() {
            return autXML;
        }

        public List<Det> getDetList() {
            return detList;
        }

        public Total getTotal() {
            return total;
        }

        public Transp getTransp() {
            return transp;
        }

        public Cobr getCobr() {
            return cobr;
        }

        public Pag getPag() {
            return pag;
        }

        public InfAdic getInfAdic() {
            return infAdic;
        }

        public Exporta getExporta() {
            return exporta;
        }

        public InfRespTec getInfRespTec() {
            return infRespTec;
        }
    }

    public static class Ide {
        private String cUF;
        private String cNF;
        private String natOp;
        private String mod;
        private String serie;
        private String nNF;
        private String dhEmi;
        private String dhSaiEnt;
        private String tpNF;
        private String idDest;
        private String cMunFG;
        private String tpImp;
        private String tpEmis;
        private String cDV;
        private String tpAmb;
        private String finNFe;
        private String indFinal;
        private String indPres;
        private String procEmi;
        private String verProc;
        private List<NotaFiscalXML.NFref> NFref = new ArrayList<>();

        public String getcUF() {
            return cUF;
        }

        public String getcNF() {
            return cNF;
        }

        public String getNatOp() {
            return natOp;
        }

        public String getMod() {
            return mod;
        }

        public String getSerie() {
            return serie;
        }

        public String getnNF() {
            return nNF;
        }

        public String getDhEmi() {
            return dhEmi;
        }

        public String getDhSaiEnt() {
            return dhSaiEnt;
        }

        public String getTpNF() {
            return tpNF;
        }

        public String getIdDest() {
            return idDest;
        }

        public String getcMunFG() {
            return cMunFG;
        }

        public String getTpImp() {
            return tpImp;
        }

        public String getTpEmis() {
            return tpEmis;
        }

        public String getcDV() {
            return cDV;
        }

        public String getTpAmb() {
            return tpAmb;
        }

        public String getFinNFe() {
            return finNFe;
        }

        public String getIndFinal() {
            return indFinal;
        }

        public String getIndPres() {
            return indPres;
        }

        public String getProcEmi() {
            return procEmi;
        }

        public String getVerProc() {
            return verProc;
        }

        public List<NotaFiscalXML.NFref> getNFref() {
            return NFref;
        }
    }

    public static class NFref {
        private String refNFe;

        public String getRefNFe() {
            return refNFe;
        }
    }

    public static class Emit {
        private String CNPJ;
        private String xNome;
        private String xFant;

        private Endereco enderEmit = new Endereco();

        private String IE;
        private String CRT;

        public String getCNPJ() {
            return CNPJ;
        }

        public String getxNome() {
            return xNome;
        }

        public String getxFant() {
            return xFant;
        }

        public Endereco getEnderEmit() {
            return enderEmit;
        }

        public String getIE() {
            return IE;
        }

        public String getCRT() {
            return CRT;
        }
    }

    public static class Dest {
        private String idEstrangeiro;
        private String xNome;

        private Endereco enderDest = new Endereco();

        private String indIEDest;
        private String email;

        public String getIdEstrangeiro() {
            return idEstrangeiro;
        }

        public String getxNome() {
            return xNome;
        }

        public Endereco getEnderDest() {
            return enderDest;
        }

        public String getIndIEDest() {
            return indIEDest;
        }

        public String getEmail() {
            return email;
        }
    }

    public static class Endereco {
        private String xLgr;
        private String nro;
        private String xBairro;
        private String cMun;
        private String xMun;
        private String UF;
        private String CEP;
        private String cPais;
        private String xPais;
        private String fone;

        public String getxLgr() {
            return xLgr;
        }

        public String getNro() {
            return nro;
        }

        public String getxBairro() {
            return xBairro;
        }

        public String getcMun() {
            return cMun;
        }

        public String getxMun() {
            return xMun;
        }

        public String getUF() {
            return UF;
        }

        public String getCEP() {
            return CEP;
        }

        public String getcPais() {
            return cPais;
        }

        public String getxPais() {
            return xPais;
        }

        public String getFone() {
            return fone;
        }
    }

    public static class AutXML {
        private String CNPJ;

        public String getCNPJ() {
            return CNPJ;
        }
    }

    public static class Det {
        @XStreamAsAttribute
        private String nItem;

        private Prod prod = new Prod();

        private Imposto imposto = new Imposto();

        public String getnItem() {
            return nItem;
        }

        public Prod getProd() {
            return prod;
        }

        public Imposto getImposto() {
            return imposto;
        }
    }

    public static class Prod {
        private String cProd;
        private String cEAN;
        private String xProd;
        private String NCM;
        private String CFOP;
        private String uCom;
        private String qCom;
        private String vUnCom;
        private String vProd;
        private String cEANTrib;
        private String uTrib;
        private String qTrib;
        private String vUnTrib;
        private String indTot;
        private DetExport detExport = new DetExport();

        private BigDecimal qComLimite = BigDecimal.ZERO;
        private BigDecimal qTribLimite = BigDecimal.ZERO;


        public String getcProd() {
            return cProd;
        }

        public String getcEAN() {
            return cEAN;
        }

        public String getxProd() {
            return xProd;
        }

        public String getNCM() {
            return NCM;
        }

        public String getCFOP() {
            return CFOP;
        }

        public String getuCom() {
            return uCom;
        }

        public String getqCom() {
            if (qComLimite == null) {
                qComLimite = new BigDecimal(this.qCom);
            }
            return qCom;
        }

        public String getvUnCom() {
            return vUnCom;
        }

        public String getvProd() {
            return vProd;
        }

        public String getcEANTrib() {
            return cEANTrib;
        }

        public String getuTrib() {
            return uTrib;
        }

        public String getqTrib() {
            if (qTribLimite == null) {
                qTribLimite = new BigDecimal(this.qTrib);
            }
            return qTrib;
        }

        public String getvUnTrib() {
            return vUnTrib;
        }

        public String getIndTot() {
            return indTot;
        }

        public DetExport getDetExport() {
            return detExport;
        }

        public void setcProd(String cProd) {
            this.cProd = cProd;
        }

        public void setcEAN(String cEAN) {
            this.cEAN = cEAN;
        }

        public void setxProd(String xProd) {
            this.xProd = xProd;
        }

        public void setNCM(String NCM) {
            this.NCM = NCM;
        }

        public void setCFOP(String CFOP) {
            this.CFOP = CFOP;
        }

        public void setuCom(String uCom) {
            this.uCom = uCom;
        }

        public void setqCom(String qCom) {
            this.qCom = qCom;
            if (qComLimite == null) {
                qComLimite = new BigDecimal(this.qCom);
            }
        }

        public void setvUnCom(String vUnCom) {
            this.vUnCom = vUnCom;
        }

        public void setvProd(String vProd) {
            this.vProd = vProd;
        }

        public void setcEANTrib(String cEANTrib) {
            this.cEANTrib = cEANTrib;
        }

        public void setuTrib(String uTrib) {
            this.uTrib = uTrib;
        }

        public void setqTrib(String qTrib) {
            this.qTrib = qTrib;
            if (qTribLimite == null) {
                qTribLimite = new BigDecimal(this.qTrib);
            }
        }

        public void setvUnTrib(String vUnTrib) {
            this.vUnTrib = vUnTrib;
        }

        public void setIndTot(String indTot) {
            this.indTot = indTot;
        }

        public void setDetExport(DetExport detExport) {
            this.detExport = detExport;
        }

        public BigDecimal getqComLimite() {
            return qComLimite;
        }

        public void setqComLimite(BigDecimal qComLimite) {
            this.qComLimite = qComLimite;
        }

        public BigDecimal getqTribLimite() {
            return qTribLimite;
        }

        public void setqTribLimite(BigDecimal qTribLimite) {
            this.qTribLimite = qTribLimite;
        }
    }

    public static class DetExport {

        private ExportInd exportInd = new ExportInd();

        static class ExportInd {
            private String nRE;
            private String chNFe;
            private String qExport;

            public String getnRE() {
                return nRE;
            }

            public String getChNFe() {
                return chNFe;
            }

            public String getqExport() {
                return qExport;
            }
        }

        public ExportInd getExportInd() {
            return exportInd;
        }
    }

    public static class Imposto {
        private String vTotTrib;
        private ICMS ICMS = new ICMS();
        private IPI IPI = new IPI();
        private PIS PIS = new PIS();
        private COFINS COFINS = new COFINS();

        public String getvTotTrib() {
            return vTotTrib;
        }

        public NotaFiscalXML.ICMS getICMS() {
            return ICMS;
        }

        public NotaFiscalXML.IPI getIPI() {
            return IPI;
        }

        public NotaFiscalXML.PIS getPIS() {
            return PIS;
        }

        public NotaFiscalXML.COFINS getCOFINS() {
            return COFINS;
        }
    }

    public static class ICMS {

        private ICMS40 ICMS40 = new ICMS40();

        static class ICMS40 {
            private String orig;
            private String CST;

            public String getOrig() {
                return orig;
            }

            public String getCST() {
                return CST;
            }
        }

        public ICMS.ICMS40 getICMS40() {
            return ICMS40;
        }
    }

    public static class IPI {
        private String cEnq;
        private IPINT IPINT;

        static class IPINT {
            private String CST;

            public String getCST() {
                return CST;
            }
        }

        public String getcEnq() {
            return cEnq;
        }

        public IPI.IPINT getIPINT() {
            return IPINT;
        }
    }

    public static class PIS {

        private PISNT PISNT = new PISNT();

        static class PISNT {
            private String CST;

            public String getCST() {
                return CST;
            }
        }

        public PIS.PISNT getPISNT() {
            return PISNT;
        }
    }

    public static class COFINS {

        private COFINSNT COFINSNT = new COFINSNT();

        static class COFINSNT {
            private String CST;

            public String getCST() {
                return CST;
            }
        }

        public COFINS.COFINSNT getCOFINSNT() {
            return COFINSNT;
        }
    }

    public static class Total {

        private ICMSTot ICMSTot = new ICMSTot();

        public static class ICMSTot {

            private String vBC;
            private String vICMS;
            private String vICMSDeson;
            private String vFCP;
            private String vBCST;
            private String vST;
            private String vFCPST;
            private String vFCPSTRet;
            private String vProd;
            private String vFrete;
            private String vSeg;
            private String vDesc;
            private String vII;
            private String vIPI;
            private String vIPIDevol;
            private String vPIS;
            private String vCOFINS;
            private String vOutro;
            private String vNF;
            private String vTotTrib;

            public String getvBC() {
                return vBC;
            }

            public String getvICMS() {
                return vICMS;
            }

            public String getvICMSDeson() {
                return vICMSDeson;
            }

            public String getvFCP() {
                return vFCP;
            }

            public String getvBCST() {
                return vBCST;
            }

            public String getvST() {
                return vST;
            }

            public String getvFCPST() {
                return vFCPST;
            }

            public String getvFCPSTRet() {
                return vFCPSTRet;
            }

            public String getvProd() {
                return vProd;
            }

            public String getvFrete() {
                return vFrete;
            }

            public String getvSeg() {
                return vSeg;
            }

            public String getvDesc() {
                return vDesc;
            }

            public String getvII() {
                return vII;
            }

            public String getvIPI() {
                return vIPI;
            }

            public String getvIPIDevol() {
                return vIPIDevol;
            }

            public String getvPIS() {
                return vPIS;
            }

            public String getvCOFINS() {
                return vCOFINS;
            }

            public String getvOutro() {
                return vOutro;
            }

            public String getvNF() {
                return vNF;
            }

            public String getvTotTrib() {
                return vTotTrib;
            }
        }

        public Total.ICMSTot getICMSTot() {
            return ICMSTot;
        }
    }

    public static class Transp {
        private String modFrete;
        private Transporta transporta;
        private Vol vol;

        public static class Transporta {

            private String CNPJ;
            private String xNome;
            private String IE;
            private String xEnder;
            private String xMun;
            private String UF;

            public String getCNPJ() {
                return CNPJ;
            }

            public String getxNome() {
                return xNome;
            }

            public String getIE() {
                return IE;
            }

            public String getxEnder() {
                return xEnder;
            }

            public String getxMun() {
                return xMun;
            }

            public String getUF() {
                return UF;
            }
        }

        public static class Vol {

            private String qVol;
            private String esp;
            private String nVol;
            private String pesoL;
            private String pesoB;

            public String getqVol() {
                return qVol;
            }

            public String getEsp() {
                return esp;
            }

            public String getnVol() {
                return nVol;
            }

            public String getPesoL() {
                return pesoL;
            }

            public String getPesoB() {
                return pesoB;
            }
        }

        public String getModFrete() {
            return modFrete;
        }

        public Transporta getTransporta() {
            return transporta;
        }

        public Vol getVol() {
            return vol;
        }
    }

    public static class Cobr {

        private Fat fat = new Fat();
        private Dup dup = new Dup();

        static class Fat {

            private String nFat;
            private String vOrig;
            private String vDesc;
            private String vLiq;

            public String getnFat() {
                return nFat;
            }

            public String getvOrig() {
                return vOrig;
            }

            public String getvDesc() {
                return vDesc;
            }

            public String getvLiq() {
                return vLiq;
            }
        }

        public static class Dup {

            private String nDup;
            private String dVenc;
            private String vDup;

            public String getnDup() {
                return nDup;
            }

            public String getdVenc() {
                return dVenc;
            }

            public String getvDup() {
                return vDup;
            }
        }

        public Fat getFat() {
            return fat;
        }

        public Dup getDup() {
            return dup;
        }
    }

    public static class Pag {

        private DetPag detPag;
        private String vTroco;

        static class DetPag {

            private String tPag;
            private String vPag;

            public String gettPag() {
                return tPag;
            }

            public String getvPag() {
                return vPag;
            }
        }

        public DetPag getDetPag() {
            return detPag;
        }

        public String getvTroco() {
            return vTroco;
        }
    }

    public static class InfAdic {
        private String infCpl;

        public String getInfCpl() {
            return infCpl;
        }
    }

    public static class Exporta {
        private String UFSaidaPais;
        private String xLocExporta;

        public String getUFSaidaPais() {
            return UFSaidaPais;
        }

        public String getxLocExporta() {
            return xLocExporta;
        }
    }

    public static class InfRespTec {

        private String CNPJ;
        private String xContato;
        private String email;
        private String fone;

        public String getCNPJ() {
            return CNPJ;
        }

        public String getxContato() {
            return xContato;
        }

        public String getEmail() {
            return email;
        }

        public String getFone() {
            return fone;
        }
    }

    public static class Signature {
        @XStreamAsAttribute
        private String xmlns;
        private SignedInfo SignedInfo;
        private String SignatureValue;
        private KeyInfo KeyInfo;

        public static class SignedInfo {

            private CanonicalizationMethod CanonicalizationMethod;
            private SignatureMethod SignatureMethod;
            private Reference Reference;

            public static class CanonicalizationMethod {
                @XStreamAsAttribute
                private String Algorithm;

                public String getAlgorithm() {
                    return Algorithm;
                }
            }

            public static class SignatureMethod {
                @XStreamAsAttribute
                private String Algorithm;

                public String getAlgorithm() {
                    return Algorithm;
                }
            }

            public static class Reference {
                @XStreamAsAttribute
                private String URI;
                private Transforms Transforms;
                private DigestMethod DigestMethod;
                private String DigestValue;

                public static class Transforms {
                    private List<Transform> transformList = new ArrayList<>();

                    public static class Transform {
                        @XStreamAsAttribute
                        private String Algorithm;

                        public String getAlgorithm() {
                            return Algorithm;
                        }
                    }

                    public List<Transform> getTransformList() {
                        return transformList;
                    }
                }

                public static class DigestMethod {
                    @XStreamAsAttribute
                    private String Algorithm;

                    public String getAlgorithm() {
                        return Algorithm;
                    }
                }

                public String getURI() {
                    return URI;
                }

                public Signature.SignedInfo.Reference.Transforms getTransforms() {
                    return Transforms;
                }

                public Signature.SignedInfo.Reference.DigestMethod getDigestMethod() {
                    return DigestMethod;
                }

                public String getDigestValue() {
                    return DigestValue;
                }

            }

            public Signature.SignedInfo.CanonicalizationMethod getCanonicalizationMethod() {
                return CanonicalizationMethod;
            }

            public Signature.SignedInfo.SignatureMethod getSignatureMethod() {
                return SignatureMethod;
            }

            public Signature.SignedInfo.Reference getReference() {
                return Reference;
            }
        }

        public static class KeyInfo {
            private X509Data X509Data;

            static class X509Data {
                private String X509Certificate;

                public String getX509Certificate() {
                    return X509Certificate;
                }
            }

            public Signature.KeyInfo.X509Data getX509Data() {
                return X509Data;
            }
        }

        public String getXmlns() {
            return xmlns;
        }

        public Signature.SignedInfo getSignedInfo() {
            return SignedInfo;
        }

        public String getSignatureValue() {
            return SignatureValue;
        }

        public Signature.KeyInfo getKeyInfo() {
            return KeyInfo;
        }
    }

    public static class ProtNFe {
        @XStreamAsAttribute
        private String versao;
        @XStreamAsAttribute
        private String xmlns;
        private InfProt infProt;

        static class InfProt {
            private String tpAmb;
            private String verAplic;
            private String chNFe;
            private String dhRecbto;
            private String nProt;
            private String digVal;
            private String cStat;
            private String xMotivo;
        }
    }

    public String getVersao() {
        return versao;
    }

    public String getXmlns() {
        return xmlns;
    }

    public NFe getNFe() {
        return NFe;
    }

    public ProtNFe getProtNFe() {
        return protNFe;
    }

    public List<Det> getListProdutosComImpostos() {
        return getNFe().getInfNFe().getDetList();
    }

    public List<Prod> getListProdutos() {
        return getNFe().getInfNFe().getDetList().stream().map(Det::getProd).collect(Collectors.toList());
    }

    public SimpleListProperty<Prod> getListProdutosObservable() {
        return new SimpleListProperty<>(FXCollections.observableArrayList(getNFe().getInfNFe().getDetList().stream().map(Det::getProd).collect(Collectors.toList())));
    }

    public String getIdNota() {
        return getNFe().getInfNFe().getId();
    }

    @Override
    public String toString() {
        return getNFe().getInfNFe().getIde().getnNF() + " - " + getNFe().getInfNFe().getIde().getSerie();
    }
}
