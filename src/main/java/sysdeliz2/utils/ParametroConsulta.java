package sysdeliz2.utils;

public class ParametroConsulta {
    private int indice;
    private String field;
    private String tipoOrder;

    public ParametroConsulta(int indice, String field, String tipoOrder) {
        this.indice = indice;
        this.field = field;
        this.tipoOrder = tipoOrder;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getTipoOrder() {
        return tipoOrder;
    }

    public void setTipoOrder(String tipoOrder) {
        this.tipoOrder = tipoOrder;
    }
}