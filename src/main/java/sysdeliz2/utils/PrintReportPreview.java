/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.embed.swing.SwingNode;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;

import javax.swing.*;

/**
 *
 * @author cristiano.diego
 */
public class PrintReportPreview extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void showReport(JasperPrint print) {
        if(print.getPages().size() > 0){
            JRViewer viewer = new JRViewer(print);
            viewer.setOpaque(true);
            viewer.setVisible(true);
            this.add(viewer);
            this.setSize(700, 500);
            this.setVisible(true);
        }
    }

    public SwingNode getSwingNode(JasperPrint print) {

        SwingNode swingFrame = new SwingNode();
        JRViewer viewer = new JRViewer(print);
        viewer.setOpaque(true);
        viewer.setVisible(true);
        swingFrame.setContent(viewer);

        return swingFrame;
    }

}
