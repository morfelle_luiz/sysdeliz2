/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.embed.swing.SwingNode;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.swing.JRViewer;
import sysdeliz2.models.ConnectionFactory;
import sysdeliz2.utils.gui.dialog.FileChoice;

import javax.print.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author souza.carlos
 */
public class ReportUtils extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public enum ReportFile {
        ROMANEIO_COLETA_DEPOSITO("/relatorios/RomaneioColetaDeposito.jasper"),
        BID_REPRESENTANTE("/relatorios/BidRepresentante.jasper"),
        PROGRAMACAO_RECURSO("/relatorios/ProgramacaoFaccao.jasper"),
        ROMANEIO_COLETA_REMESSA("/relatorios/RomaneioRemessaCliente.jasper"),
        ESPELHO_PEDIDO_B2B("/relatorios/EspelhoPedido.jasper"),
        ETIQUETA_CONTEUDO_CAIXA_EXPEDICAO("/relatorios/EtqConteudoCaixaExpedicao.jasper"),
        ESPELHO_PEDIDO_ERP("/relatorios/EspelhoPedidoErp.jasper"),
        MINUTA_EXPEDICAO("/relatorios/MinutaExpedicao.jasper"),
        PLANEJAMENTO_ENCAIXE("/relatorios/PlanejamentoEncaixe.jasper"),
        PLANEJAMENTO_ENCAIXE_CONFERENCIA("/relatorios/PlanejamentoEncaixeConferencia.jasper"),
        PLANEJAMENTO_ENCAIXE_RC("/relatorios/PlanejamentoEncaixeRC.jasper"),
        ROMANEIO_MATERIAIS_ENCAIXE("/relatorios/RomaneioMateriaisEncaixe.jasper"),
        CARTEIRA_MOSTRUARIO("/relatorios/CarteirinhaMostruario.jasper"),
        CARTEIRA_MOSTRUARIO_COR_UNICA("/relatorios/CarteirinhaMostruarioCorUnica.jasper"),
        EXTRATO_COMISSAO_FECHAMENTO("/relatorios/ExtratoFechamentoComissaoRep.jasper"),
        ATENDIMENTO_CARTEIRA_PADRAO("/relatorios/AtendimentoCarteiraPadrao.jasper"),
        ATENDIMENTO_CARTEIRA_VALORES("/relatorios/AtendimentoCarteiraValores.jasper"),
        ATENDIMENTO_CARTEIRA_VALORES_CANC("/relatorios/AtendimentoCarteiraValoresCanc.jasper"),
        ATENDIMENTO_CARTEIRA_VALORES_CONTINUA("/relatorios/AtendimentoCarteiraValoresContinua.jasper"),
        ATENDIMENTO_CARTEIRA_MARCA("/relatorios/AtendimentoCarteiraMarca.jasper"),
        ATENDIMENTO_CARTEIRA_REPRESENTANTE("/relatorios/AtendimentoCarteiraRepresentante.jasper"),
        ESPELHO_MARKETING("/relatorios/EspelhoMarketing.jasper"),
        ATENDIMENTO_BID_CIDADES("/relatorios/RelatorioBidCidades.jasper"),
        FICHA_MATERIAIS_OF("/relatorios/ImpressaoConsumoFT.jasper"),
        BID_CLIENTES("/relatorios/BidClienteExcel.jasper"),
        RELATORIO_CIDADES("/relatorios/RelatorioCidadesRep.jasper"),
        ETIQUETA_CAIXA_PA("/relatorios/EtqCaixaEntradaPA.jasper"),
        ETIQUETA_CAIXA_SEGUNDA_PA("/relatorios/EtqCaixaEntradaSegundaPA.jasper"),
        ROMANEIO_PRODUTOS_ACABADOS("/relatorios/RomaneioEntradaProdutosAcabados.jasper"),
        ROMANEIO_PRODUTOS_ACABADOS_GRADE("/relatorios/RomaneioEntradaProdutosAcabadosComGrade.jasper"),
        GRADE_PRODUTOS_ACABADOS("/relatorios/EntradaProdutosAcabadosGrade.jasper"),
        SOLICITACAO_EXP("/relatorios/EtqSolicitacaoExp.jasper"),
        PRODUTOS_B2B_SEM_FOTO("/relatorios/ProdutosB2BSemFotos.jasper"),
        PRODUTOS_ESTOQUE_B2B("/relatorios/ProdutosEstoqueB2B.jasper"),
        CATALOGO_DIGITAL("/relatorios/CatalogoDigital.jasper"),
        SOLICITACOES_MARKETING("/relatorios/SolicitacoesMarketing.jasper"),
        NF_PROFORMA("/relatorios/ModeloNF.jasper");

        public String fxmlFile;

        ReportFile(String value) {
            fxmlFile = value;
        }

        public String value() {
            return fxmlFile;
        }
    }

    private List<JasperPrint> romaneiosParaImpressao = new ArrayList<>();
    private ConfigReport config = null;

    public ConfigReport config() {
        config = new ConfigReport();
        return config;
    }

    public class ConfigReport {

        public ConfigReport addReport(ReportFile file, ParameterReport... parameters) throws JRException, SQLException {
            JasperPrint jasperPrint;
            JasperReport jr;

            Map<String, Object> parametros = new HashMap<>();
            for (ParameterReport parameter : parameters)
                parametros.put(parameter.key, parameter.value);

            jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream(file.value()));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, ConnectionFactory.getEmpresaConnection());
            romaneiosParaImpressao.add(jasperPrint);

            return this;
        }

        public ConfigReport addReport(ReportFile file, List collection ,ParameterReport... parameters) throws JRException, SQLException {
            JasperPrint jasperPrint;
            JasperReport jr;

            Map<String, Object> parametros = new HashMap<>();
            for (ParameterReport parameter : parameters)
                parametros.put(parameter.key, parameter.value);

            jr = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream(file.value()));
            jasperPrint = JasperFillManager.fillReport(jr, parametros, new JRBeanCollectionDataSource(collection));
            romaneiosParaImpressao.add(jasperPrint);

            return this;
        }

        public ShowReport view() {
            return new ShowReport();
        }

    }

    public class ShowReport {

        public ShowReport print() throws IOException, JRException {
            jasperToPrinter(romaneiosParaImpressao);
            return this;
        }

        public ShowReport printWithDialog() throws IOException, JRException {
            jasperToPrinter(romaneiosParaImpressao, true);
            return this;
        }

        public ShowReport toPdfWithDialog() throws IOException, JRException {
            String fileName = null;
            fileName = FileChoice.show().stringSavePath();
            if (fileName != null)
                jasperToPdfNoOpenDialog(romaneiosParaImpressao, fileName);
            return this;
        }

        public ShowReport toPdfWithDialogAndOpen() throws IOException, JRException {
            String fileName = null;
            fileName = FileChoice.show().stringSavePath();
            if (fileName != null)
                jasperToPdfNoOpenDialog(romaneiosParaImpressao, fileName);

            File someFile = new File(fileName);
            Desktop.getDesktop().open(someFile);
            return this;
        }

        public ShowReport toPdf(String path) throws IOException, JRException {
            String fileName = path;
            if (fileName != null)
                savePdfFile(romaneiosParaImpressao, fileName);

            return this;
        }

        public ShowReport toView() {
            romaneiosParaImpressao.forEach(ReportUtils.this::showReport);
            return this;
        }

        public void viewPdf(String path) throws IOException {
            File someFile = new File(path);
            Desktop.getDesktop().open(someFile);
        }

        public ShowReport toExcelWithDialog() throws IOException, JRException {
            String fileName = null;
            fileName = FileChoice.show().stringSavePath();
            if (fileName != null)
                jasperToExcelOpenDialog(romaneiosParaImpressao.get(0), fileName);

            return this;
        }

        public SwingNode incorpore() {
            return getSwingNode(romaneiosParaImpressao.get(0));
        }

        public void printZplCode(String zplCodeEtiqueta) throws PrintException {
            printZplCode(zplCodeEtiqueta, null);
        }

        public void printZplCode(String zplCodeEtiqueta, String printerName) throws PrintException {
            PrintService ps = null;
            try {
                byte[] by = zplCodeEtiqueta.getBytes();
                DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                if (printerName != null) {
                    PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);
                    if (pss.length == 0)
                        throw new PrintException("O serviço de impressão no computador não pode ser iniciado, verifique com a equipe de TI.");

                    for (PrintService psRead : pss) {
                        if (psRead.getName().contains(printerName)) {
                            ps = psRead;
                            break;
                        }
                    }
                    if (ps == null)
                        throw new PrintException("Não foi possível encontrar a impressora instalada no computador, verifique a impressoa 'default' do computador ou se tem uma impressora ZPL instalada.");

                } else {
                    ps = PrintServiceLookup.lookupDefaultPrintService();
                    if (ps == null) {
                        throw new PrintException("Não foi encontrado uma impressora padrão em seu computador, verifique com a equipe de TI.");
                    }
                }

                DocPrintJob job = ps.createPrintJob();
                Doc doc = new SimpleDoc(by, flavor, null);

                job.print(doc, null);

            } catch (PrintException e) {
                throw new PrintException("Não foi possível imprimir a etiqueta na impressora: " + ps.getName());
            }
        }

    }

    public static class ParameterReport {

        private String key;
        private Object value;

        public ParameterReport(String key, Object value) {
            this.key = key;
            this.value = value;
        }
    }

    public void showReport(JasperPrint print) {
        if (print.getPages().size() > 0) {
            JRViewer viewer = new JRViewer(print);
            viewer.setOpaque(true);
            viewer.setVisible(true);
            this.add(viewer);
            this.setSize(1000, 700);
            this.setVisible(true);
        }
    }

    public SwingNode getSwingNode(JasperPrint print) {

        SwingNode swingFrame = new SwingNode();
        JRViewer viewer = new JRViewer(print);
        viewer.setOpaque(true);
        viewer.setVisible(true);
        swingFrame.setContent(viewer);

        return swingFrame;
    }

    public static void savePdfFile(List<JasperPrint> jasperPrintList, String filePath) throws JRException, FileNotFoundException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
        exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();
        File someFile = new File(filePath);
        if (someFile.getParentFile() != null) {
            someFile.getParentFile().mkdirs();
        }
        someFile.createNewFile();
//        someFile.mkdir();
        FileOutputStream fos = null;
        fos = new FileOutputStream(someFile);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////// FICOU ESSE CÓDIGO PARA BACKUP PARA CHAMADAS DO LEGADO

    public static void jasperToPdf(List<JasperPrint> jasperPrintList) throws JRException, FileNotFoundException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
        exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();
        File someFile = new File("C:\\SysDelizLocal\\TempReport.pdf");
        FileOutputStream fos = null;
        fos = new FileOutputStream(someFile);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
        Desktop.getDesktop().open(someFile);
    }

    public static void jasperToPdf(List<JasperPrint> jasperPrintList, String filename) throws JRException, FileNotFoundException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
        exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();
        File someFile = new File("C:\\SysDelizLocal\\" + filename + ".pdf");
        FileOutputStream fos = null;
        fos = new FileOutputStream(someFile);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
        Desktop.getDesktop().open(someFile);
    }

    public static void jasperToPdfNoOpen(List<JasperPrint> jasperPrintList, String fileName) throws JRException, FileNotFoundException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
        exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();
        File someFile = new File("C:\\SysDelizLocal\\local_files\\" + fileName + ".pdf");
        FileOutputStream fos = null;
        fos = new FileOutputStream(someFile);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
    }

    public static void jasperToPdfNoOpenDialog(List<JasperPrint> jasperPrintList, String fileName) throws JRException, FileNotFoundException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        //Add the list as a Parameter
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        //this will make a bookmark in the exported PDF for each of the reports
        exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.exportReport();
        File someFile = new File(fileName + ".pdf");
        FileOutputStream fos = null;
        fos = new FileOutputStream(someFile);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
    }

    public void jasperToExcelOpenDialog(JasperPrint jasperPrint, String fileName) throws JRException, IOException {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
////        JRXlsxExporter exporter = new JRXlsxExporter();
//////        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
//////        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
//////        //we set the one page per sheet parameter here
//////
////        exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, fileName);
////        exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
////        exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
////        exporter.exportReport();
////        File someFile = new File(fileName + ".xls");
////        FileOutputStream fos = null;
////        fos = new FileOutputStream(someFile);
////        fos.write(baos.toByteArray());
////        fos.flush();
////        fos.close();

        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(fileName + ".xlsx"));

        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
        configuration.setOnePagePerSheet(true);
        configuration.setDetectCellType(true);
        exporter.setConfiguration(configuration);
        exporter.exportReport();
    }

    public static void jasperToPrinter(List<JasperPrint> jasperPrintList) throws JRException, FileNotFoundException, IOException {
        //JRPdfExporter exporter = new JRPdfExporter();
        jasperToPrinter(jasperPrintList, false);
    }

    public static void jasperToPrinter(List<JasperPrint> jasperPrintList, Boolean printerDialog) throws JRException, FileNotFoundException, IOException {
        //JRPdfExporter exporter = new JRPdfExporter();
        for (JasperPrint jasperPrint : jasperPrintList) {
            JasperPrintManager.printReport(jasperPrint, printerDialog);
        }
    }

    public static void jasperToPrinter(JasperPrint jasperPrint) throws JRException, FileNotFoundException, IOException {
        //JRPdfExporter exporter = new JRPdfExporter();
        JasperPrintManager.printPage(jasperPrint, 0, false);
    }
//
//    public static void openReport(String titulo, InputStream inputStream, Map parametros, Connection conexao) throws JRException {
//        JasperPrint print = JasperFillManager.fillReport(inputStream, parametros, conexao);
//        viewReportFrame(titulo, print);
//    }
//
//    public static void openReport(String titulo, InputStream inputStream, Map parametros, Connection conexao, String tipo) throws JRException {
//        JasperPrint print = JasperFillManager.fillReport(inputStream, parametros, conexao);
//
//        viewReportFrame(titulo, print);
//    }
//
//    public static void openReport(String titulo, InputStream inputStream, Map parametros, JRDataSource dataSource) throws JRException {
//        JasperPrint print = JasperFillManager.fillReport(inputStream, parametros, dataSource);
//        viewReportFrame(titulo, print);
//    }
//
//    private static void viewReportFrame(String titulo, JasperPrint print) throws JRException {
//        JasperPrintManager.printReport(print, true);
//
//        JRViewer viewer = new JRViewer(print);
//        JFrame frameRelatorio = new JFrame(titulo);
//        frameRelatorio.add(viewer, BorderLayout.CENTER);
//
//        frameRelatorio.setSize(500, 500);
//        frameRelatorio.setExtendedState(JFrame.MAXIMIZED_BOTH);
//        frameRelatorio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        frameRelatorio.setFocusable(true);
//        frameRelatorio.setVisible(true);
//        frameRelatorio.setFocusableWindowState(true);
//    }
}
