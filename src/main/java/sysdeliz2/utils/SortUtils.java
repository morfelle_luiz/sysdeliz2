package sysdeliz2.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class SortUtils {

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static int sortTamanhos(String tam1, String tam2) {
        if (tam1.matches("[0-9]*")) {
            return Integer.compare(Integer.parseInt(tam1), Integer.parseInt(tam2));
        }
        if (tam1.equals("PP")) {
            if (!tam2.equals("PP")) return -1;
        }

        if (tam1.equals("P") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP"))) return 1;
            else return -1;
        }

        if (tam1.equals("M") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP") || (tam2.equals("P")))) return 1;
            else return -1;
        }

        if (tam1.equals("G") && !tam2.equals(tam1)) {
            if ((tam2.equals("PP") || (tam2.equals("P") || (tam2.equals("M")))))
                return 1;
            else return -1;
        }

        if (tam1.equals("GG") && !tam2.equals(tam1)) {
            if (tam2.equals("XGG")) return -1;
            else return 1;
        }

        if (tam1.equals("XGG") && !tam2.equals(tam1)) {
            return 1;
        }
        return 0;
    }
}
