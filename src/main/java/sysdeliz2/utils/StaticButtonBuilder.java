package sysdeliz2.utils;

import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.sys.ImageUtils;

/**
 * @author lima.joao
 * @since 14/08/2019 13:30
 */
public class StaticButtonBuilder {

    public static Button genericAddButton(){
        return StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16),
                "Adicionar novo registro",
                "Adicionar Novo",
                new String[]{"success", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );
    }

    public static Button genericInfoButton(){
        return StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16),
                "Visualizar informações",
                "Informações do Registro",
                new String[]{"info", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );
    }

    public static Button genericEditButton(){
        return StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16),
                "Editar registro selecionado",
                "Alteração do Registro",
                new String[]{"warning", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );
    }

    public static Button genericDeleteButton(){
        return StaticButtonBuilder.genericButtom(
                ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16),
                "Excluir registro selecionado",
                "Exclusão do Registro",
                new String[]{"danger", "xs"},
                ContentDisplay.GRAPHIC_ONLY
        );
    }

    public static Button genericButtom(ImageView icon, String toolTip, String buttonText, String[] styleClass, ContentDisplay display) {
//        Button btnButton = new Button();
        Button btnButton = FormButton.create(btn -> {
            btn.title(buttonText);
            btn.icon(icon);
            btn.tooltip(toolTip);
            btn.getStyleClass().addAll(styleClass);
            btn.contentDisplay(display);
        });
//        btnButton.setGraphic(icon);
//        btnButton.setTooltip(new Tooltip(toolTip));
//
//        btnButton.setText(buttonText);
//        for (String aClass : styleClass) {
//            btnButton.getStyleClass().add(aClass);
//        }
//
//        btnButton.contentDisplayProperty().set(display);
        return btnButton;
    }
}
