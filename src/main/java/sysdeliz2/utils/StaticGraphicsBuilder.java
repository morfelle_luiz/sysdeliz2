package sysdeliz2.utils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * @author lima.joao
 * @since 09/09/2019 11:30
 */
public class StaticGraphicsBuilder {

    private static String CSS = "/styles/sysDelizDesktop.css";
    public static String DEFAULT_STYLE = "-fx-background-color: #fff;";

    public static ImageView buildImageView(String resource, double fitHeight, double fitWidth, boolean pickOnBounds, boolean preserveRatio){
        ImageView imageView = new ImageView(String.valueOf(StaticGraphicsBuilder.class.getResource(resource)));
        imageView.setFitHeight(fitHeight);
        imageView.setFitWidth(fitWidth);
        imageView.setPickOnBounds(pickOnBounds);
        imageView.setPreserveRatio(preserveRatio);

        return imageView;
    }

    //<editor-fold desc="Builder para Border Pane">
    @SuppressWarnings("unused")
    public static BorderPane buildBorderPane(){
        return StaticGraphicsBuilder.buildBorderPane(546.0, 699);
    }

    public static BorderPane buildBorderPane(double perfHeight, double prefWidth){
        return StaticGraphicsBuilder.buildBorderPane(perfHeight, prefWidth, StaticGraphicsBuilder.DEFAULT_STYLE);
    }

    public static BorderPane buildBorderPane(double perfHeight, double prefWidth, String style){
        return StaticGraphicsBuilder.buildBorderPane(perfHeight, prefWidth, style, StaticGraphicsBuilder.CSS);
    }

    @SuppressWarnings("unused")
    private static BorderPane buildBorderPane(double perfHeight, double prefWidth, String style, String css){
        BorderPane borderPane = new BorderPane();
        borderPane.setPrefHeight(perfHeight);
        borderPane.setPrefWidth(prefWidth);
        borderPane.setStyle(style);
        borderPane.getStyleClass().add(String.valueOf(StaticGraphicsBuilder.class.getResource(css)));
        return borderPane;
    }
    //</editor-fold>

    //<editor-fold desc="Builder HBox">
    public static HBox buildHBox(double prefHeight, double prefWidth){
        return StaticGraphicsBuilder.buildHBox(prefHeight, prefWidth, StaticGraphicsBuilder.DEFAULT_STYLE);
    }

    public static HBox buildHBox(double prefHeight, double prefWidth, String style){
        return StaticGraphicsBuilder.buildHBox(prefHeight, prefWidth, style, Pos.CENTER_LEFT);
    }

    private static HBox buildHBox(double prefHeight, double prefWidth, String style, Pos position){
        return StaticGraphicsBuilder.buildHBox(prefHeight, prefWidth, style, position, 5.0);
    }

    public static HBox buildHBox(double prefHeight, double prefWidth, String style, Pos position, double spacing){
        HBox box = new HBox();
        box.setPrefHeight(prefHeight);
        box.setPrefWidth(prefWidth);
        box.setStyle(style);
        box.setAlignment(position);
        box.setSpacing(spacing);

        return box;
    }
    //</editor-fold>

    //<editor-fold desc="Builder VBox">
    public static VBox buildVBox(double prefHeight, double prefWidth){
        return StaticGraphicsBuilder.buildVBox(prefHeight, prefWidth, StaticGraphicsBuilder.DEFAULT_STYLE);
    }

    private static VBox buildVBox(double prefHeight, double prefWidth, String style){
        VBox box = new VBox();
        box.setPrefHeight(prefHeight);
        box.setPrefWidth(prefWidth);
        box.setStyle(style);
        box.setSpacing(5.0);

        return box;
    }
    //</editor-fold>

    public static TitledPane buildTitledPane(boolean animated, boolean collapsible, double prefHeight, double prefWidth, String title){
        TitledPane titledPane = new TitledPane();
        titledPane.setAnimated(animated);
        titledPane.setCollapsible(collapsible);
        titledPane.setPrefHeight(prefHeight);
        titledPane.setPrefWidth(prefWidth);
        titledPane.setText(title);
        return titledPane;
    }

    public static Button buildButton(String id, double prefHeight, double prefWidth, String text, EventHandler<ActionEvent> eventHandler){
        Button btn = new Button();
        btn.setMnemonicParsing(false);

        btn.setId(id);
        btn.setPrefHeight(prefHeight);
        btn.setPrefWidth(prefWidth);
        btn.setText(text);
        btn.setOnAction(eventHandler);

        return btn;
    }

    public static Pane buildPane(double prefHeight, double prefWidth) {
        Pane pane = new Pane();
        pane.setPrefHeight(prefHeight);
        pane.setMinHeight(prefHeight);

        pane.setPrefWidth(prefWidth);
        pane.setMinWidth(prefWidth);

        return pane;
    }

}
