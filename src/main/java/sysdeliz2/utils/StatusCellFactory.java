/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;
import sysdeliz2.models.ReservaPedido;

/**
 *
 * @author cristiano.diego
 */
public class StatusCellFactory implements Callback<TableColumn, TableCell> {

    @Override
    public TableCell call(TableColumn p) {

        TableCell cell = new TableCell<ReservaPedido, String>() {
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : getString());
                setGraphic(null);
                TableRow currentRow = getTableRow();
                ReservaPedido currentReserva = currentRow == null ? null : (ReservaPedido) currentRow.getItem();
                if (currentReserva != null) {
                    String status = currentReserva.getStrStatus();
                    clearPriorityStyle();
                    if (!isHover() && !isSelected() && !isFocused()) {
                        setPriorityStyle(status);
                    }
                }
            }

            @Override
            public void updateSelected(boolean upd) {
                super.updateSelected(upd);
                System.out.println("is update");
            }

            private void clearPriorityStyle() {
                ObservableList<String> styleClasses = getStyleClass();
                styleClasses.remove("statusImpresso");
                styleClasses.remove("statusLido");
                styleClasses.remove("statusExpedido");
                styleClasses.remove("statusFaturado");
                
            }

            private void setPriorityStyle(String priority) {
                switch (priority) {
                    case "I":
                        getStyleClass().add("statusImpresso");
                        break;
                    case "L":
                        getStyleClass().add("statusLido");
                        break;
                    case "E":
                        getStyleClass().add("statusExpedido");
                        break;
                    case "F":
                        getStyleClass().add("statusFaturado");
                        break;
                }
                System.out.println(getStyleClass());
            }

            private String getString() {
                return getItem() == null ? "" : getItem().toString();
            }
        };
        return cell;
    }
}
