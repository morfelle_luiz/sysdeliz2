package sysdeliz2.utils;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TableUtils {

    /**
     * Make table menu button visible and replace the context menu with a custom
     * context menu via reflection. The preferred height is modified so that an
     * empty header row remains visible. This is needed in case you remove all
     * columns, so that the menu button won't disappear with the row header.
     * IMPORTANT: Modification is only possible AFTER the table has been made
     * visible, otherwise you'd get a NullPointerException
     *
     * @param tableView
     */
    public static void addCustomTableMenu(TableView tableView) {

        // enable table menu
        tableView.setTableMenuButtonVisible(true);

        // replace internal mouse listener with custom listener
        setCustomContextMenu(tableView);

    }

    private static void setCustomContextMenu(TableView table) {

        TableViewSkin tableSkin = (TableViewSkin) table.getSkin();

        // get all children of the skin
        ObservableList<Node> children = tableSkin.getChildren();

        // find the TableHeaderRow child
        for (int i = 0; i < children.size(); i++) {

            Node node = children.get(i);

            if (node instanceof TableHeaderRow) {

                TableHeaderRow tableHeaderRow = (TableHeaderRow) node;

                // setting the preferred height for the table header row
                // if the preferred height isn't set, then the table header would disappear if
                // there are no visible columns
                // and with it the table menu button
                // by setting the preferred height the header will always be visible
                // note: this may need adjustments in case you have different heights in columns
                // (eg when you use grouping)
                // double defaultHeight = tableHeaderRow.getHeight();
                // tableHeaderRow.setPrefHeight(defaultHeight);
                for (Node child : tableHeaderRow.getChildren()) {

                    // child identified as cornerRegion in TableHeaderRow.java
                    if (child.getStyleClass().contains("show-hide-columns-button")) {

                        // get the context menu
                        ContextMenu columnPopupMenu = createContextMenu(table);

                        // replace mouse listener
                        child.setOnMousePressed(me -> {
                            // show a popupMenu which lists all columns
                            columnPopupMenu.show(child, Side.BOTTOM, 0, 0);
                            me.consume();
                        });
                    }
                }

            }
        }
    }

    /**
     * Create a menu with custom items. The important thing is that the menu
     * remains open while you click on the menu items.
     *
     * @param cm
     * @param table
     */
    private static ContextMenu createContextMenu(TableView table) {

        ContextMenu cm = new ContextMenu();

        // create new context menu
        CustomMenuItem cmi;

        // select all item
        Label showAll = new Label("Mostrar Todas");
        showAll.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                for (Object obj : table.getColumns()) {
                    ((TableColumn<?, ?>) obj).setVisible(true);
                }
            }

        });

        cmi = new CustomMenuItem(showAll);
        cmi.setHideOnClick(false);
        cm.getItems().add(cmi);

        // deselect all item
        Label hideAll = new Label("Esconder todas");
        hideAll.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                for (Object obj : table.getColumns()) {
                    ((TableColumn<?, ?>) obj).setVisible(false);
                }
            }

        });

        cmi = new CustomMenuItem(hideAll);
        cmi.setHideOnClick(false);
        cm.getItems().add(cmi);

        // select all item
        Label showSetores = new Label("Fluxos");
        showSetores.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                for (int i = 0; i < table.getColumns().size(); i++) {
                    if (i >= 34 && i <= 72) {
                        Object obj = table.getColumns().get(i);
                        if (((TableColumn<?, ?>) obj).isVisible()) {
                            ((TableColumn<?, ?>) obj).setVisible(false);
                        } else {
                            ((TableColumn<?, ?>) obj).setVisible(true);
                        }
                    }
                }
            }
        });

        cmi = new CustomMenuItem(showSetores);
        cmi.setHideOnClick(false);
        cm.getItems().add(cmi);

        // separator
        cm.getItems().add(new SeparatorMenuItem());

        // menu item for each of the available columns
        for (Object obj : table.getColumns()) {

            TableColumn<?, ?> tableColumn = (TableColumn<?, ?>) obj;

            CheckBox cb = new CheckBox(tableColumn.getText());
            cb.selectedProperty().bindBidirectional(tableColumn.visibleProperty());

            cmi = new CustomMenuItem(cb);
            cmi.setHideOnClick(false);

            cm.getItems().add(cmi);
        }

        return cm;
    }

    public static final List<Integer> COLUNAS_TBL_RESERVAS = new ArrayList<Integer>(Arrays.asList(7, 10, 48, 10, 10, 15));
    public static final List<Integer> COLUNAS_TBL_MARKETING_PEDIDO = new ArrayList<Integer>(Arrays.asList(10, 15, 40, 10, 30, 5, 10));
    public static final List<Integer> COLUNAS_TBL_ITENS_CAIXA_LANCAMENTO_RESERVA = new ArrayList<Integer>(Arrays.asList(20, 12, 50, 10, 8));
    public static final List<Integer> COLUNAS_TBL_GESTAO_PRODUCAO_GRADE_GLOBAL = new ArrayList<Integer>(Arrays.asList(2, 3, 5, 5, 5, 5, 5, 6, 5, 5, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6));
    public static final List<Integer> COLUNAS_TBL_COMERCIAL_BID_REPRESENTANTES = new ArrayList<Integer>(Arrays.asList(10, 20, 50, 30, 25, 30, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8));
    public static final List<Integer> COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_CLIENTES = new ArrayList<Integer>(Arrays.asList(10, 3, 7, 45, 4, 30, 15, 30));
    public static final List<Integer> COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_DUPLICATAS_CLIENTE = new ArrayList<Integer>(Arrays.asList(7, 7, 7, 5, 5, 10, 10, 40));
    public static final List<Integer> COLUNAS_TBL_FINANCEIRO_EMAIL_BOLETOS_EMAILS_ENVIADOS = new ArrayList<Integer>(Arrays.asList(12, 12, 35, 55, 15));
    public static final List<Integer> COLUNAS_TBL_COMERCIAL_BID_CLIENTE = new ArrayList<Integer>(Arrays.asList(3, 10, 60, 30, 5, 10, 50, 20));
    public static final List<Integer> COLUNAS_TBL_MEU_MENU_ROTEIROS = new ArrayList<Integer>(Arrays.asList(5, 9, 5, 35, 20, 45));
    public static final List<Integer> COLUNAS_TBL_ADMINISTRATIVO_ADMIN_ROTEIROS = new ArrayList<Integer>(Arrays.asList(2, 6, 18, 8, 8, 20, 30, 40, 40));
    public static final List<Integer> COLUNAS_TBL_FINANCEIRO_RECEBER_CARTAO_RECEBIMENTOS = new ArrayList<Integer>(Arrays.asList(10, 50, 15, 20, 20, 10));

    public static void autoFitTable(TableView tableView, List<Integer> colunas) {
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        int i = 0;
        for (Object column : tableView.getColumns()) {
            ((TableColumn) column).setMaxWidth(1f * Integer.MAX_VALUE * colunas.get(i));
            i++;
        }
    }

    public static void makeHeaderWrappable(TableColumn col, Pos posCol, TextAlignment tAlign) {
        Label label = new Label(col.getText());
        label.setStyle("-fx-padding: 2px;");
        label.setWrapText(true);
        label.setAlignment(posCol);
        label.setTextAlignment(tAlign);

        StackPane stack = new StackPane();
        stack.getChildren().add(label);
        stack.prefWidthProperty().bind(col.widthProperty().subtract(5));
        label.prefWidthProperty().bind(stack.prefWidthProperty());
        col.setText(col.getText());
        col.setGraphic(stack);
    }
}
