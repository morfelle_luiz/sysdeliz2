/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils;

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Parent;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.function.Consumer;

/**
 * @author diegoc
 */
public final class ToggleSwitch extends Parent {

    private BooleanProperty switchedOn = new SimpleBooleanProperty(false);

    private Color hexBackgroundRed = Color.valueOf("#ffdcd5");
    private Color hexBackgroundGreen = Color.valueOf("#c0f6d1");

    private TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(0.25));
    private FillTransition fillAnimation = new FillTransition(Duration.seconds(0.25));

    private ParallelTransition animation = new ParallelTransition(translateAnimation, fillAnimation);

    public BooleanProperty switchedOnProperty() {
        return switchedOn;
    }

    public ToggleSwitch(int height, int width) {

        Rectangle background = new Rectangle(width, height);
        background.setArcWidth(height);
        background.setArcHeight(height);
        background.setFill(hexBackgroundRed);
        background.setStroke(Color.LIGHTGRAY);

        Circle trigger = new Circle(height / 2);
        trigger.setCenterX(height / 2);
        trigger.setCenterY(height / 2);
        trigger.setFill(Color.WHITE);
        trigger.setStroke(Color.LIGHTGRAY);

        DropShadow shadow = new DropShadow();
        shadow.setRadius(2);
        trigger.setEffect(shadow);

        translateAnimation.setNode(trigger);
        fillAnimation.setShape(background);

        getChildren().addAll(background, trigger);

        switchedOn.addListener((obs, oldState, newState) -> {
            boolean isOn = newState.booleanValue();
            translateAnimation.setToX(isOn ? width - height : 0);
            fillAnimation.setFromValue(isOn ? hexBackgroundRed : hexBackgroundGreen);
            fillAnimation.setToValue(isOn ? hexBackgroundGreen : hexBackgroundRed);

            animation.play();
        });

        setOnMouseClicked(event -> {
            switchedOn.set(!switchedOn.get());
        });
    }

    public ToggleSwitch(int height, int width, String hexaColor) {

        Color hexColor = Color.valueOf(hexaColor);

        Rectangle background = new Rectangle(width, height);
        background.setArcWidth(height);
        background.setArcHeight(height);
        background.setFill(hexBackgroundRed);
        background.setStroke(Color.LIGHTGRAY);

        Circle trigger = new Circle(height / 2);
        trigger.setCenterX(height / 2);
        trigger.setCenterY(height / 2);
        trigger.setFill(hexColor);
        trigger.setStroke(Color.WHITE);

        DropShadow shadow = new DropShadow();
        shadow.setRadius(2);
        trigger.setEffect(shadow);

        translateAnimation.setNode(trigger);
        fillAnimation.setShape(background);

        getChildren().addAll(background, trigger);

        switchedOn.addListener((obs, oldState, newState) -> {
            boolean isOn = newState.booleanValue();
            translateAnimation.setToX(isOn ? width - height : 0);
            fillAnimation.setFromValue(isOn ? hexBackgroundRed : hexBackgroundGreen);
            fillAnimation.setToValue(isOn ? hexBackgroundGreen : hexBackgroundRed);

            animation.play();
        });

        setOnMouseClicked(event -> {
            switchedOn.set(!switchedOn.get());
        });
    }

    public BooleanProperty getSwitchedOn() {
        return switchedOn;
    }

    public static ToggleSwitch create(int height, int width, Consumer<ToggleSwitch> value) {
        ToggleSwitch ts = new ToggleSwitch(height, width);
        value.accept(ts);
        return ts;
    }

    public static ToggleSwitch create(int height, int width, String hexaColor, Consumer<ToggleSwitch> value) {
        ToggleSwitch ts = new ToggleSwitch(height, width, hexaColor);
        value.accept(ts);
        return ts;
    }
}
