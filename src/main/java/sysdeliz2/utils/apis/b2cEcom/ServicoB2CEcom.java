package sysdeliz2.utils.apis.b2cEcom;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okio.Buffer;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpResponseException;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer;
import se.akerfeldt.okhttp.signpost.SigningInterceptor;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.models.ti.TabTam;
import sysdeliz2.models.view.VSdDadosPedidoB2C;
import sysdeliz2.models.view.b2c.VB2cProdutoCorFotos;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;
import sysdeliz2.utils.apis.b2cEcom.models.*;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.CategoriesRequest;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.OrderItemRequest;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.ProductsRequest;
import sysdeliz2.utils.apis.b2cEcom.models.sendModels.*;
import sysdeliz2.utils.apis.b2cEcom.services.ServiceB2CEcom;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class ServicoB2CEcom {

    private final ServiceB2CEcom service;

    private List<Category> categories = new ArrayList<>();
    private List<Color> cores = new ArrayList<>();
    private List<Size> sizes = new ArrayList<>();

    public ServicoB2CEcom() {
        String CONSUMER_KEY = "1biy79fu7sj51j98zquj4qfqr2kh5ip7";
        String CONSUMER_SECRET = "3wq5xiv5juw2zpjwc1missjs73ogt37h";
        String ACESS_TOKEN = "jalf7gthxlrddj4faoz1sbewuqa7av8w";
        String ACESS_TOKEN_SECRET = "nxbmqpvpzg5w86u081etsidsjdsgggbe";
        OkHttpOAuthConsumer oAuthConsumer = new OkHttpOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        oAuthConsumer.setTokenWithSecret(ACESS_TOKEN, ACESS_TOKEN_SECRET);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new SigningInterceptor(oAuthConsumer)).connectTimeout(1, TimeUnit.MINUTES).readTimeout(1, TimeUnit.MINUTES).writeTimeout(1, TimeUnit.MINUTES).build();
        service = new Retrofit.Builder().baseUrl("https://dlz.com.br/").addConverterFactory(GsonConverterFactory.create()).client(client).build().create(ServiceB2CEcom.class);
    }

    public Boolean sendProducts(List<VB2cProdutosEnvio> produtos) throws IOException {
        AtomicBoolean result = new AtomicBoolean(true);

        getCategories();
        sendCategories(produtos);
        getCategories();

        getCores();
        sendCores(produtos);

        getTamanhos();
        sendTamanhos();
        try {
            produtos.forEach((it) -> it.setStatusEnvioProduto(new StatusEnvioProduto(200, false, "Envio realizado com sucesso!")));
            for (SendProduct sendProduct : getSimpleProductsToSend(produtos)) {
                sendProduct(sendProduct);
            }
//            produtos.forEach((it) -> it.setStatusEnvioProduto(new StatusEnvioProduto(302, true, "Tamanhos e quantidades enviadas com sucesso!")));

            List<SendProduct> configurables = getConfigurableProductsToSend(produtos);

            configurables.forEach((sendProduct) -> {

                List<Product> childrenProducts = getChildrenProducts(sendProduct.getProduct().getSku());
                sendProduct.getProduct().getExtensionAttributes().setConfigurableProductLinks(childrenProducts.stream().filter(it -> !it.getSku().equals(sendProduct.getProduct().getSku())).map(Product::getId).collect(Collectors.toList()));

                try {
                    removeImages(sendProduct);
                    sendProduct(sendProduct);
                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    System.out.println("ERRO");
                }
            });
//            produtos.forEach((it) -> it.setStatusEnvioProduto(new StatusEnvioProduto(301, true, "Produtos enviados com sucesso!")));

            for (SendProduct sendProduct : configurables) {
                List<SendProduct> productList = configurables.stream().filter(it -> !it.getProduct().getSku().equals(sendProduct.getProduct().getSku()) && it.getProduct().getSku().contains(sendProduct.getProduct().getSku().substring(0, sendProduct.getProduct().getSku().indexOf("-")))).collect(Collectors.toList());
                List<ProductLink> links = new ArrayList<>();
                for (int i = 0; i < productList.size(); i++)
                    links.add(new ProductLink(sendProduct.getProduct(), i + 1, productList.get(i).getProduct().getSku(), false));

                while (links.size() > 0) {
                    List<ProductLink> productLinks = new ArrayList<>(links.subList(0, Math.min(links.size(), 20)));
                    sendRequest(service.sendLinkProduct(sendProduct.getProduct().getSku(), new SendLinkProduct(productLinks)));
                    links.removeAll(productLinks);
                }
            }
            produtos.forEach((it) -> it.setStatusEnvioProduto(new StatusEnvioProduto(300, true, "Tamanhos e quantidades enviadas com sucesso!")));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result.get();
    }

    private void removeImages(SendProduct sendProduct) {

        List<MediaGalleryEntries> images = getImages(sendProduct.getProduct().getSku());
        if (images != null && images.size() > 0) {
            images.forEach((img) -> {
                sendRequest(service.deleteImages(sendProduct.getProduct().getSku(), img.getId()));
            });
        }
    }

    private Object sendRequest(Call call) {
        try {
            Response execute = call.execute();
            if (execute.code() != 200) {
                System.out.println(execute.code());
                System.out.println(bodyToString(call.request().body()));
                if (execute.errorBody() != null) System.out.println(execute.errorBody().string());
                throw new HttpResponseException(execute.code(), "Erro no Envio");
            }
            return execute.body();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private StatusEnvioProduto sendProduct(SendProduct sendProduct) throws HttpResponseException {
        try {
            String view = sendProduct.getMarca().equals("D") ? "default" : "flordelis_view";
            Call<SendProduct> call = service.sendProduct(sendProduct, view);
            Response execute = call.execute();
            if (execute.code() != 200) {
                if (sendProduct.getProduct().getMediaGalleryEntries().size() > 0) {
                    sendProduct.getProduct().getMediaGalleryEntries().get(0).getContent().setType("image/png");
                    call = service.sendProduct(sendProduct, view);
                    execute = call.execute();
                    if (execute.code() != 200) {
                        return new StatusEnvioProduto(execute.code(), false, execute.message());
                    }
                }
            }
            return new StatusEnvioProduto(execute.code(), true, "Enviado com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
            return new StatusEnvioProduto(300, false, "Time out");
        }
    }

    private void sendTamanhos() {
        List<TabTam> tamanhos = (List<TabTam>) new FluentDao().selectFrom(TabTam.class).get().resultList();
        for (TabTam tamanho : tamanhos) {
            if (sizes.stream().noneMatch(it -> it.getLabel().equals(tamanho.getTam()))) {
                sendRequest(service.sendSize(new SendSize(tamanho)));
                getTamanhos();
            }
        }
    }

    private List<MediaGalleryEntries> getImages(String sku) {
        try {
            Call<List<MediaGalleryEntries>> call = service.getImages(sku);
            Response<List<MediaGalleryEntries>> response = call.execute();
            return response.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void getTamanhos() {
        try {
            Call<List<Size>> call = service.getSizes();
            Response<List<Size>> response = call.execute();
            sizes = response.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendCores(List<VB2cProdutosEnvio> produtos) {
        for (VB2cProdutosEnvio produto : produtos) {
            for (VB2cProdutoCores cor : produto.getCores()) {
                if (cores.stream().noneMatch(it -> it.getLabel().equals(cor.getDescCor() + " - " + cor.getCor()))) {
                    sendRequest(service.sendColor(new SendColor(cor)));
                    getCores();
                }
            }
        }
    }

    public void sendCategories(List<VB2cProdutosEnvio> produtos) throws HttpResponseException {
        Map<Integer, List<String>> mapCategorias = new HashMap<>();
        mapCategorias.put(2, new ArrayList<>());
        mapCategorias.put(3, new ArrayList<>());
        for (VB2cProdutosEnvio produto : produtos) {
            if (!mapCategorias.get(produto.getMarca().equals("D") ? 2 : 3).contains(produto.getCategoria())) {
                mapCategorias.get(produto.getMarca().equals("D") ? 2 : 3).add(produto.getCategoria());
            }
        }
        mapCategorias.forEach((codMarca, list) -> {
            for (String categoria : list) {
                if (categories.stream().noneMatch(it -> it.getParentId().equals(codMarca) && categoria.equalsIgnoreCase(it.getName()))) {
                    SendCategory sendCategory = new SendCategory(codMarca, StringUtils.capitalize(categoria.toLowerCase()));
                    if (codMarca == 2) sendRequest(service.sendCategoryDlz(sendCategory));
                    else sendRequest(service.sendCategoryFlor(sendCategory));
                }
            }
        });
    }

    public void getCores() {
        try {
            Call<List<Color>> call = service.getColors();
            Response<List<Color>> response = call.execute();
            cores = response.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<SendProduct> getSimpleProductsToSend(List<VB2cProdutosEnvio> produtos) {
        List<SendProduct> productsToSend = new ArrayList<>();
        for (VB2cProdutosEnvio produto : produtos) {
            Category category = getCategory(produto);
            for (VB2cProdutoCores cor : produto.getCores()) {
                Color color = getColor(cor);
                List<Size> sizes = getSizes(produto, cor);
                for (Size size : sizes) {
                    productsToSend.add(new SendProduct(produto, cor, size, category, color, true));
                }
            }
        }
        return productsToSend;
    }

    private List<SendProduct> getConfigurableProductsToSend(List<VB2cProdutosEnvio> produtos) {
        List<SendProduct> productsToSend = new ArrayList<>();

        for (VB2cProdutosEnvio produto : produtos) {

            Category category = getCategory(produto);
            for (VB2cProdutoCores cor : produto.getCores()) {
                Color color = getColor(cor);
                List<Size> sizes = getSizes(produto, cor);
                SendProduct configurable = new SendProduct(produto, cor, null, category, color, false);
                configurable.getProduct().getExtensionAttributes().setConfigurableProductOptions(Collections.singletonList(new ConfigurableProductOption(sizes.stream().map(it -> Integer.parseInt(it.getValue())).collect(Collectors.toList()))));
                productsToSend.add(configurable);
            }
        }
        return productsToSend;
    }

    private Color getColor(VB2cProdutoCores cor) {
        return cores.stream().filter(it -> it.getLabel().equals(cor.getDescCor() + " - " + cor.getCor())).findFirst().orElse(null);
    }

    private Category getCategory(VB2cProdutosEnvio produto) {
        return categories.stream().filter(it -> it.getName().equalsIgnoreCase(produto.getCategoria())).findFirst().orElse(null);
    }

    @NotNull
    private List<Size> getSizes(VB2cProdutosEnvio produto, VB2cProdutoCores cor) {
        List<PaIten> estoque = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it
                        .equal("id.codigo", produto.getReferencia())
                        .equal("id.cor", cor.getCor())
                        .equal("id.deposito", "0001"))
                .resultList();
        if (estoque.size() == 0) return new ArrayList<>();
        return estoque.stream().map(it -> this.sizes.stream().filter(eb -> eb.getLabel().equals(it.getId().getTam())).findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public void getCategories() {
        try {
            Call<CategoriesRequest> call = service.getCategories();
            Response<CategoriesRequest> response = call.execute();
            categories = response.body().getCategories();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Product> getChildrenProducts(String codigo) {
        try {
            String url = "rest/default/V1/products?searchCriteria[filterGroups][0][filters][0][field]=sku&searchCriteria[filterGroups][0][filters][0][conditionType]=like&searchCriteria[filterGroups][0][filters][0][value]=" + codigo + "%25";
            Call<ProductsRequest> call = service.getProductsChildren(url);
            Response<ProductsRequest> execute = call.execute();
            if (execute.code() != 200) {
                System.out.println(execute.errorBody().string());
                throw new HttpResponseException(execute.code(), "Erro no envio do Produto");
            }
            return execute.body().getItems();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void sendImages(SendProduct sendProduct) {
        String[] split = sendProduct.getProduct().getSku().split("-");
        List<VB2cProdutoCorFotos> cores = (List<VB2cProdutoCorFotos>) new FluentDao()
                .selectFrom(VB2cProdutoCorFotos.class).where(it -> it
                        .equal("referencia", split[0])
                        .equal("codCor", split[1]))
                .orderBy("ordem", OrderType.ASC)
                .resultList();

        for (VB2cProdutoCorFotos corProd : cores) {
            try {
                sendImage(sendProduct, new MediaGalleryEntries(sendProduct.getProduct().getSku(), corProd.getOrdem(),
                        corProd.getOrdem() == 2 ? Arrays.asList("image", "small_image", "thumbnail") : new ArrayList<>(),
                        corProd.getImagem()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendImage(SendProduct sendProduct, MediaGalleryEntries mediaGalleryEntries) {
        try {
            Call<String> sendImageCall = service.sendImage(sendProduct.getProduct().getSku(), new SendImage(mediaGalleryEntries));
            Response<String> execute = sendImageCall.execute();

            if (execute.code() != 200) {
                System.out.println(execute.errorBody().string());
                mediaGalleryEntries.getContent().setType("image/png");
                sendImageCall = service.sendImage(sendProduct.getProduct().getSku(), new SendImage(mediaGalleryEntries));
                execute = sendImageCall.execute();

                if (execute.code() != 200) {
                    System.out.println(execute.errorBody().string());
                    throw new HttpResponseException(execute.code(), "Erro no envio da Imagem");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendCustomers() {
        List<SendCustomer> customersMarca = new ArrayList<>();
        Map<Entidade, List<Pedido>> codclisPedido = ((List<Pedido>) new FluentDao().selectFrom(Pedido.class).where(it -> it.isIn("tabPre.regiao", new String[]{"B2CD", "B2CF"})).resultList()).stream().collect(Collectors.groupingBy(Pedido::getCodcli));
        codclisPedido.forEach((ent, listPed) -> {
            if (ent.getSitCli().getCodigo().equals("BC")) {
                listPed.stream().map(it -> it.getTabPre().getRegiao().equals("B2CD") ? 1 : 2).distinct().forEach((codLoja) -> customersMarca.add(new SendCustomer(ent, codLoja)));
            }
        });
        customersMarca.forEach(this::sendCustomer);
    }

    public void sendCustomer(SendCustomer sendCustomer) {
        try {
            Call<Customer> customerCall = sendCustomer.getCustomer().getWebsiteId() == 1 ? service.sendCustomersDlz(sendCustomer) : service.sendCustomersFlor(sendCustomer);
            Response<Customer> execute = customerCall.execute();
            if (execute.code() != 200) {
                if (execute.errorBody() != null) System.out.println(execute.errorBody().string());
                System.out.println(execute.raw().message());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getConfigurableProducts() {
        try {
            Call<ProductsRequest> call = service.getProductsConfigurable();
            Response<ProductsRequest> execute = call.execute();
            if (execute.code() != 200) {
                if (execute.errorBody() != null) System.out.println(execute.errorBody().string());
                System.out.println(execute.raw().message());
            }
            ProductsRequest productsRequest = execute.body();
            return productsRequest.getItems().stream().map(Product::getSku).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Boolean updateOrderStatus(String marca, String id, StatusHistory newStatus) throws IOException {
        String view = marca.equals("D") ? "default" : "flordelis_view";
        Call<Boolean> call = service.updateOrderStatus(view, id, new SendStatusHistory(newStatus));
        Response<Boolean> execute = call.execute();
        if (execute.code() != 200) {
            newStatus.setIsCustomerNotified(0);
            call = service.updateOrderStatus(view, id, new SendStatusHistory(newStatus));
            execute = call.execute();
            if (execute.code() != 200) {
                throw new HttpResponseException(execute.code(), "Erro ao alterar o status do pedido");
            }
        }
        return execute.body();
    }



    public List<OrderItem> getItensOrder(String orderId) throws IOException {
        String url = "rest/default/V1/orders/items?searchCriteria[filterGroups][0][filters][0][field]=order_id&searchCriteria[filterGroups][0][filters][0][value]=" + orderId + "&searchCriteria[filterGroups][0][filters][0][conditionType]=equal";
        Call<OrderItemRequest> call = service.getOrderItens(url);
        Response<OrderItemRequest> execute = call.execute();
        if (execute.code() != 200) {
            execute.errorBody();
            throw new HttpResponseException(execute.code(), "Erro ao receber os itens do Pedido");
        }
        return execute.body().getItems();
    }

    public void shipOrder(OrderShip orderShip, VSdDadosPedidoB2C item) throws IOException {
        String view = item.getMarca().getCodigo().equals("D") ? "default" : "flordelis_view";

        Call<Integer> call = service.shipOrder(view,item.getNumero().getIdMagento(), orderShip);
        Response<Integer> execute = call.execute();
        if (execute.code() != 200) {
            throw new HttpResponseException(execute.code(), "Erro ao receber os itens do Pedido");
        }
    }

    private String bodyToString(final RequestBody request) {
        try {
            final Buffer buffer = new Buffer();
            if (request != null)
                request.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public Product getProductBySku(String sku) {
        return (Product) sendRequest(service.getProduct(sku));
    }
}
