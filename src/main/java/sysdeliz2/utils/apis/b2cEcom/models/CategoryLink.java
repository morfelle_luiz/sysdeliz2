package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class CategoryLink {

    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("category_id")
    @Expose
    private String categoryId;

    public CategoryLink() {
    }

    public CategoryLink(String categoryId, int position) {
        this.position = position;
        this.categoryId = categoryId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

}