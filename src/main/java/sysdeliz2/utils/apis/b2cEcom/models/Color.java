package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;

@Generated("jsonschema2pojo")
public class Color {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("sort_order")
    @Expose
    private Integer sortOrder;
    @SerializedName("is_default")
    @Expose
    private Boolean isDefault;
    @SerializedName("store_labels")
    @Expose
    private List<StoreLabel> storeLabels;

    public Color() {
    }

    public Color(VB2cProdutoCores cor) {
        this.label = cor.getDescCor() + " - " + cor.getCor();
        this.value = cor.getCor();
        storeLabels = new ArrayList<>(Arrays.asList(new StoreLabel(cor.getDescCor() + " - " + cor.getCor())));
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public List<StoreLabel> getStoreLabels() {
        return storeLabels;
    }

    public void setStoreLabels(List<StoreLabel> storeLabels) {
        this.storeLabels = storeLabels;
    }

}