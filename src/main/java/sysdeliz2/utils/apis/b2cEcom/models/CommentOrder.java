package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentOrder {
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("is_visible_on_front")
    @Expose
    private Integer isVisibleOnFront;

    public CommentOrder() {
    }

    public CommentOrder(String comment, Integer isVisibleOnFront) {
        this.comment = comment;
        this.isVisibleOnFront = isVisibleOnFront;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getIsVisibleOnFront() {
        return isVisibleOnFront;
    }

    public void setIsVisibleOnFront(Integer isVisibleOnFront) {
        this.isVisibleOnFront = isVisibleOnFront;
    }

}



