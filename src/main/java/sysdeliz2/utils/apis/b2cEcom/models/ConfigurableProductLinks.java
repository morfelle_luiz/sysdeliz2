package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ConfigurableProductLinks {

    @SerializedName("configurable_product_links")
    @Expose
    private List<Integer> configurableProductLinks = null;

    public ConfigurableProductLinks(List<Integer> configurableProductLinks) {
        this.configurableProductLinks = configurableProductLinks;
    }

    public List<Integer> getConfigurableProductLinks() {
        return configurableProductLinks;
    }

    public void setConfigurableProductLinks(List<Integer> configurableProductLinks) {
        this.configurableProductLinks = configurableProductLinks;
    }

}