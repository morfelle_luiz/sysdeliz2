package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ConfigurableProductOption {

    @SerializedName("attribute_id")
    @Expose
    private String attributeId;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("values")
    @Expose
    private List<Value> values = new ArrayList<>();

    public ConfigurableProductOption(List<Integer> valores) {
        this.attributeId = "191";
        this.label = "Tamanho";
        this.position = 0;
        this.values = new ArrayList<>(valores.stream().map(eb -> new Value(eb)).collect(Collectors.toList()));
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

}