package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Base64;

@Generated("jsonschema2pojo")
public class ContentMedia {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("base64_encoded_data")
    @Expose
    private String base64EncodedData;


    public ContentMedia() {
    }

    public ContentMedia(String base64EncodedData, String type, String name) {
        this.base64EncodedData = base64EncodedData;
        this.type = type;
        this.name = name;
    }

    public String getBase64EncodedData() {
        return base64EncodedData;
    }

    public void setBase64EncodedData(String base64EncodedData) {
        this.base64EncodedData = base64EncodedData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}