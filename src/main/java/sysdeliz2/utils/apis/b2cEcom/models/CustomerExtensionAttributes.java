package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class CustomerExtensionAttributes {

    @SerializedName("company_attributes")
    @Expose
    private CompanyAttributes companyAttributes;
    @SerializedName("assistance_allowed")
    @Expose
    private Integer assistanceAllowed;
    @SerializedName("is_subscribed")
    @Expose
    private Boolean isSubscribed;

    public CompanyAttributes getCompanyAttributes() {
        return companyAttributes;
    }

    public void setCompanyAttributes(CompanyAttributes companyAttributes) {
        this.companyAttributes = companyAttributes;
    }

    public Integer getAssistanceAllowed() {
        return assistanceAllowed;
    }

    public void setAssistanceAllowed(Integer assistanceAllowed) {
        this.assistanceAllowed = assistanceAllowed;
    }

    public Boolean getIsSubscribed() {
        return isSubscribed;
    }

    public void setIsSubscribed(Boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

}