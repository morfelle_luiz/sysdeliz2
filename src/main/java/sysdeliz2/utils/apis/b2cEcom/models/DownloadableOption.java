package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class DownloadableOption {

    @SerializedName("downloadable_links")
    @Expose
    private List<Object> downloadableLinks = null;

    public List<Object> getDownloadableLinks() {
        return downloadableLinks;
    }

    public void setDownloadableLinks(List<Object> downloadableLinks) {
        this.downloadableLinks = downloadableLinks;
    }

}