package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ExtensionAttributes {

    @SerializedName("website_ids")
    @Expose
    private List<Integer> websiteIds;
    @SerializedName("configurable_product_options")
    @Expose
    private List<ConfigurableProductOption> configurableProductOptions;
    @SerializedName("configurable_product_links")
    @Expose
    private List<Integer> configurableProductLinks;
    @SerializedName("category_links")
    @Expose
    private List<CategoryLink> categoryLinks;
    @SerializedName("stock_item")
    @Expose
    private StockItem stockItem;

    public List<Integer> getWebsiteIds() {
        return websiteIds;
    }

    public void setWebsiteIds(List<Integer> websiteIds) {
        this.websiteIds = websiteIds;
    }

    public List<ConfigurableProductOption> getConfigurableProductOptions() {
        return configurableProductOptions;
    }

    public void setConfigurableProductOptions(List<ConfigurableProductOption> configurableProductOptions) {
        this.configurableProductOptions = configurableProductOptions;
    }

    public List<Integer> getConfigurableProductLinks() {
        return configurableProductLinks;
    }

    public void setConfigurableProductLinks(List<Integer> configurableProductLinks) {
        this.configurableProductLinks = configurableProductLinks;
    }

    //    public List<ConfigurableProductLinks> getConfigurableProductLinks() {
//        return configurableProductLinks;
//    }
//
//    public void setConfigurableProductLinks(List<ConfigurableProductLinks> configurableProductLinks) {
//        this.configurableProductLinks = configurableProductLinks;
//    }

    public List<CategoryLink> getCategoryLinks() {
        return categoryLinks;
    }

    public void setCategoryLinks(List<CategoryLink> categoryLinks) {
        this.categoryLinks = categoryLinks;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }
}