package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemOrderShip {

    @SerializedName("order_item_id")
    @Expose
    private Integer orderItemId;
    @SerializedName("qty")
    @Expose
    private Integer qty;

    public ItemOrderShip() {
    }

    public ItemOrderShip(Integer orderItemId, Integer qty) {
        this.orderItemId = orderItemId;
        this.qty = qty;
    }

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

}


