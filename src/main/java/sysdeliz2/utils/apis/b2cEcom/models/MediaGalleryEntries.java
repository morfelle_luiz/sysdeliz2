package sysdeliz2.utils.apis.b2cEcom.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.io.FileUtils;
import sysdeliz2.utils.sys.ImageUtils;

@Generated("jsonschema2pojo")
public class MediaGalleryEntries {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("disabled")
    @Expose
    private Boolean disabled;
    @SerializedName("types")
    @Expose
    private List<String> types;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("content")
    @Expose
    private ContentMedia content;

    public MediaGalleryEntries() {
    }

    public MediaGalleryEntries(String label, Integer position, List<String> types, String file) throws IOException {
        this.mediaType = "image";
        this.label = label + position;
        this.position = position;
        this.disabled = false;
        if (types.size() > 0) this.types = types;
        this.file = getLabel();
        String base64 = "";

        base64 = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(file
                .replace("https://imagens.deliz.com.br/produtos/", "K:/Loja Virtual/imagens/produtos/")
                .replace("flordelis", "Flor De Lis"))));

        this.content = new ContentMedia(base64, "image/jpeg", getLabel());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public ContentMedia getContent() {
        return content;
    }

    public void setContent(ContentMedia content) {
        this.content = content;
    }
}