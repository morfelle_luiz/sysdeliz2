package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("additional_data")
    @Expose
    private String additionalData;
    @SerializedName("amount_refunded")
    @Expose
    private Double amountRefunded;
    @SerializedName("applied_rule_ids")
    @Expose
    private String appliedRuleIds;
    @SerializedName("base_amount_refunded")
    @Expose
    private Double baseAmountRefunded;
    @SerializedName("base_cost")
    @Expose
    private Double baseCost;
    @SerializedName("base_discount_amount")
    @Expose
    private Double baseDiscountAmount;
    @SerializedName("base_discount_invoiced")
    @Expose
    private Double baseDiscountInvoiced;
    @SerializedName("base_discount_refunded")
    @Expose
    private Double baseDiscountRefunded;
    @SerializedName("base_discount_tax_compensation_amount")
    @Expose
    private Double baseDiscountTaxCompensationAmount;
    @SerializedName("base_discount_tax_compensation_invoiced")
    @Expose
    private Double baseDiscountTaxCompensationInvoiced;
    @SerializedName("base_discount_tax_compensation_refunded")
    @Expose
    private Double baseDiscountTaxCompensationRefunded;
    @SerializedName("base_original_price")
    @Expose
    private Double baseOriginalPrice;
    @SerializedName("base_price")
    @Expose
    private Double basePrice;
    @SerializedName("base_price_incl_tax")
    @Expose
    private Double basePriceInclTax;
    @SerializedName("base_row_invoiced")
    @Expose
    private Double baseRowInvoiced;
    @SerializedName("base_row_total")
    @Expose
    private Double baseRowTotal;
    @SerializedName("base_row_total_incl_tax")
    @Expose
    private Double baseRowTotalInclTax;
    @SerializedName("base_tax_amount")
    @Expose
    private Double baseTaxAmount;
    @SerializedName("base_tax_before_discount")
    @Expose
    private Double baseTaxBeforeDiscount;
    @SerializedName("base_tax_invoiced")
    @Expose
    private Double baseTaxInvoiced;
    @SerializedName("base_tax_refunded")
    @Expose
    private Double baseTaxRefunded;
    @SerializedName("base_weee_tax_applied_amount")
    @Expose
    private Double baseWeeeTaxAppliedAmount;
    @SerializedName("base_weee_tax_applied_row_amnt")
    @Expose
    private Double baseWeeeTaxAppliedRowAmnt;
    @SerializedName("base_weee_tax_disposition")
    @Expose
    private Double baseWeeeTaxDisposition;
    @SerializedName("base_weee_tax_row_disposition")
    @Expose
    private Double baseWeeeTaxRowDisposition;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("discount_amount")
    @Expose
    private Double discountAmount;
    @SerializedName("discount_invoiced")
    @Expose
    private Double discountInvoiced;
    @SerializedName("discount_percent")
    @Expose
    private Double discountPercent;
    @SerializedName("discount_refunded")
    @Expose
    private Double discountRefunded;
    @SerializedName("event_id")
    @Expose
    private Double eventId;
    @SerializedName("ext_order_item_id")
    @Expose
    private String extOrderItemId;
    @SerializedName("free_shipping")
    @Expose
    private Double freeShipping;
    @SerializedName("gw_base_price")
    @Expose
    private Double gwBasePrice;
    @SerializedName("gw_base_price_invoiced")
    @Expose
    private Double gwBasePriceInvoiced;
    @SerializedName("gw_base_price_refunded")
    @Expose
    private Double gwBasePriceRefunded;
    @SerializedName("gw_base_tax_amount")
    @Expose
    private Double gwBaseTaxAmount;
    @SerializedName("gw_base_tax_amount_invoiced")
    @Expose
    private Double gwBaseTaxAmountInvoiced;
    @SerializedName("gw_base_tax_amount_refunded")
    @Expose
    private Double gwBaseTaxAmountRefunded;
    @SerializedName("gw_id")
    @Expose
    private Double gwId;
    @SerializedName("gw_price")
    @Expose
    private Double gwPrice;
    @SerializedName("gw_price_invoiced")
    @Expose
    private Double gwPriceInvoiced;
    @SerializedName("gw_price_refunded")
    @Expose
    private Double gwPriceRefunded;
    @SerializedName("gw_tax_amount")
    @Expose
    private Double gwTaxAmount;
    @SerializedName("gw_tax_amount_invoiced")
    @Expose
    private Double gwTaxAmountInvoiced;
    @SerializedName("gw_tax_amount_refunded")
    @Expose
    private Double gwTaxAmountRefunded;
    @SerializedName("discount_tax_compensation_amount")
    @Expose
    private Double discountTaxCompensationAmount;
    @SerializedName("discount_tax_compensation_canceled")
    @Expose
    private Double discountTaxCompensationCanceled;
    @SerializedName("discount_tax_compensation_invoiced")
    @Expose
    private Double discountTaxCompensationInvoiced;
    @SerializedName("discount_tax_compensation_refunded")
    @Expose
    private Double discountTaxCompensationRefunded;
    @SerializedName("is_qty_decimal")
    @Expose
    private Double isQtyDecimal;
    @SerializedName("is_virtual")
    @Expose
    private Double isVirtual;
    @SerializedName("item_id")
    @Expose
    private Double itemId;
    @SerializedName("locked_do_invoice")
    @Expose
    private Double lockedDoInvoice;
    @SerializedName("locked_do_ship")
    @Expose
    private Double lockedDoShip;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("no_discount")
    @Expose
    private Double noDiscount;
    @SerializedName("order_id")
    @Expose
    private Double orderId;
    @SerializedName("original_price")
    @Expose
    private Double originalPrice;
    @SerializedName("parent_item_id")
    @Expose
    private Double parentItemId;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("price_incl_tax")
    @Expose
    private Double priceInclTax;
    @SerializedName("product_id")
    @Expose
    private Double productId;
    @SerializedName("product_type")
    @Expose
    private String productType;
    @SerializedName("qty_backordered")
    @Expose
    private Double qtyBackordered;
    @SerializedName("qty_canceled")
    @Expose
    private Double qtyCanceled;
    @SerializedName("qty_invoiced")
    @Expose
    private Double qtyInvoiced;
    @SerializedName("qty_ordered")
    @Expose
    private Double qtyOrdered;
    @SerializedName("qty_refunded")
    @Expose
    private Double qtyRefunded;
    @SerializedName("qty_returned")
    @Expose
    private Double qtyReturned;
    @SerializedName("qty_shipped")
    @Expose
    private Double qtyShipped;
    @SerializedName("quote_item_id")
    @Expose
    private Double quoteItemId;
    @SerializedName("row_invoiced")
    @Expose
    private Double rowInvoiced;
    @SerializedName("row_total")
    @Expose
    private Double rowTotal;
    @SerializedName("row_total_incl_tax")
    @Expose
    private Double rowTotalInclTax;
    @SerializedName("row_weight")
    @Expose
    private Double rowWeight;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("store_id")
    @Expose
    private Double storeId;
    @SerializedName("tax_amount")
    @Expose
    private Double taxAmount;
    @SerializedName("tax_before_discount")
    @Expose
    private Double taxBeforeDiscount;
    @SerializedName("tax_canceled")
    @Expose
    private Double taxCanceled;
    @SerializedName("tax_invoiced")
    @Expose
    private Double taxInvoiced;
    @SerializedName("tax_percent")
    @Expose
    private Double taxPercent;
    @SerializedName("tax_refunded")
    @Expose
    private Double taxRefunded;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("weee_tax_applied")
    @Expose
    private String weeeTaxApplied;
    @SerializedName("weee_tax_applied_amount")
    @Expose
    private Double weeeTaxAppliedAmount;
    @SerializedName("weee_tax_applied_row_amount")
    @Expose
    private Double weeeTaxAppliedRowAmount;
    @SerializedName("weee_tax_disposition")
    @Expose
    private Double weeeTaxDisposition;
    @SerializedName("weee_tax_row_disposition")
    @Expose
    private Double weeeTaxRowDisposition;
    @SerializedName("weight")
    @Expose
    private Double weight;

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public Double getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(Double amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setAppliedRuleIds(String appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public Double getBaseAmountRefunded() {
        return baseAmountRefunded;
    }

    public void setBaseAmountRefunded(Double baseAmountRefunded) {
        this.baseAmountRefunded = baseAmountRefunded;
    }

    public Double getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(Double baseCost) {
        this.baseCost = baseCost;
    }

    public Double getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setBaseDiscountAmount(Double baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public Double getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }

    public void setBaseDiscountInvoiced(Double baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }

    public Double getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }

    public void setBaseDiscountRefunded(Double baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }

    public Double getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setBaseDiscountTaxCompensationAmount(Double baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public Double getBaseDiscountTaxCompensationInvoiced() {
        return baseDiscountTaxCompensationInvoiced;
    }

    public void setBaseDiscountTaxCompensationInvoiced(Double baseDiscountTaxCompensationInvoiced) {
        this.baseDiscountTaxCompensationInvoiced = baseDiscountTaxCompensationInvoiced;
    }

    public Double getBaseDiscountTaxCompensationRefunded() {
        return baseDiscountTaxCompensationRefunded;
    }

    public void setBaseDiscountTaxCompensationRefunded(Double baseDiscountTaxCompensationRefunded) {
        this.baseDiscountTaxCompensationRefunded = baseDiscountTaxCompensationRefunded;
    }

    public Double getBaseOriginalPrice() {
        return baseOriginalPrice;
    }

    public void setBaseOriginalPrice(Double baseOriginalPrice) {
        this.baseOriginalPrice = baseOriginalPrice;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Double getBasePriceInclTax() {
        return basePriceInclTax;
    }

    public void setBasePriceInclTax(Double basePriceInclTax) {
        this.basePriceInclTax = basePriceInclTax;
    }

    public Double getBaseRowInvoiced() {
        return baseRowInvoiced;
    }

    public void setBaseRowInvoiced(Double baseRowInvoiced) {
        this.baseRowInvoiced = baseRowInvoiced;
    }

    public Double getBaseRowTotal() {
        return baseRowTotal;
    }

    public void setBaseRowTotal(Double baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    public Double getBaseRowTotalInclTax() {
        return baseRowTotalInclTax;
    }

    public void setBaseRowTotalInclTax(Double baseRowTotalInclTax) {
        this.baseRowTotalInclTax = baseRowTotalInclTax;
    }

    public Double getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setBaseTaxAmount(Double baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public Double getBaseTaxBeforeDiscount() {
        return baseTaxBeforeDiscount;
    }

    public void setBaseTaxBeforeDiscount(Double baseTaxBeforeDiscount) {
        this.baseTaxBeforeDiscount = baseTaxBeforeDiscount;
    }

    public Double getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }

    public void setBaseTaxInvoiced(Double baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }

    public Double getBaseTaxRefunded() {
        return baseTaxRefunded;
    }

    public void setBaseTaxRefunded(Double baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }

    public Double getBaseWeeeTaxAppliedAmount() {
        return baseWeeeTaxAppliedAmount;
    }

    public void setBaseWeeeTaxAppliedAmount(Double baseWeeeTaxAppliedAmount) {
        this.baseWeeeTaxAppliedAmount = baseWeeeTaxAppliedAmount;
    }

    public Double getBaseWeeeTaxAppliedRowAmnt() {
        return baseWeeeTaxAppliedRowAmnt;
    }

    public void setBaseWeeeTaxAppliedRowAmnt(Double baseWeeeTaxAppliedRowAmnt) {
        this.baseWeeeTaxAppliedRowAmnt = baseWeeeTaxAppliedRowAmnt;
    }

    public Double getBaseWeeeTaxDisposition() {
        return baseWeeeTaxDisposition;
    }

    public void setBaseWeeeTaxDisposition(Double baseWeeeTaxDisposition) {
        this.baseWeeeTaxDisposition = baseWeeeTaxDisposition;
    }

    public Double getBaseWeeeTaxRowDisposition() {
        return baseWeeeTaxRowDisposition;
    }

    public void setBaseWeeeTaxRowDisposition(Double baseWeeeTaxRowDisposition) {
        this.baseWeeeTaxRowDisposition = baseWeeeTaxRowDisposition;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getDiscountInvoiced() {
        return discountInvoiced;
    }

    public void setDiscountInvoiced(Double discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Double getDiscountRefunded() {
        return discountRefunded;
    }

    public void setDiscountRefunded(Double discountRefunded) {
        this.discountRefunded = discountRefunded;
    }

    public Double getEventId() {
        return eventId;
    }

    public void setEventId(Double eventId) {
        this.eventId = eventId;
    }

    public String getExtOrderItemId() {
        return extOrderItemId;
    }

    public void setExtOrderItemId(String extOrderItemId) {
        this.extOrderItemId = extOrderItemId;
    }

    public Double getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(Double freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Double getGwBasePrice() {
        return gwBasePrice;
    }

    public void setGwBasePrice(Double gwBasePrice) {
        this.gwBasePrice = gwBasePrice;
    }

    public Double getGwBasePriceInvoiced() {
        return gwBasePriceInvoiced;
    }

    public void setGwBasePriceInvoiced(Double gwBasePriceInvoiced) {
        this.gwBasePriceInvoiced = gwBasePriceInvoiced;
    }

    public Double getGwBasePriceRefunded() {
        return gwBasePriceRefunded;
    }

    public void setGwBasePriceRefunded(Double gwBasePriceRefunded) {
        this.gwBasePriceRefunded = gwBasePriceRefunded;
    }

    public Double getGwBaseTaxAmount() {
        return gwBaseTaxAmount;
    }

    public void setGwBaseTaxAmount(Double gwBaseTaxAmount) {
        this.gwBaseTaxAmount = gwBaseTaxAmount;
    }

    public Double getGwBaseTaxAmountInvoiced() {
        return gwBaseTaxAmountInvoiced;
    }

    public void setGwBaseTaxAmountInvoiced(Double gwBaseTaxAmountInvoiced) {
        this.gwBaseTaxAmountInvoiced = gwBaseTaxAmountInvoiced;
    }

    public Double getGwBaseTaxAmountRefunded() {
        return gwBaseTaxAmountRefunded;
    }

    public void setGwBaseTaxAmountRefunded(Double gwBaseTaxAmountRefunded) {
        this.gwBaseTaxAmountRefunded = gwBaseTaxAmountRefunded;
    }

    public Double getGwId() {
        return gwId;
    }

    public void setGwId(Double gwId) {
        this.gwId = gwId;
    }

    public Double getGwPrice() {
        return gwPrice;
    }

    public void setGwPrice(Double gwPrice) {
        this.gwPrice = gwPrice;
    }

    public Double getGwPriceInvoiced() {
        return gwPriceInvoiced;
    }

    public void setGwPriceInvoiced(Double gwPriceInvoiced) {
        this.gwPriceInvoiced = gwPriceInvoiced;
    }

    public Double getGwPriceRefunded() {
        return gwPriceRefunded;
    }

    public void setGwPriceRefunded(Double gwPriceRefunded) {
        this.gwPriceRefunded = gwPriceRefunded;
    }

    public Double getGwTaxAmount() {
        return gwTaxAmount;
    }

    public void setGwTaxAmount(Double gwTaxAmount) {
        this.gwTaxAmount = gwTaxAmount;
    }

    public Double getGwTaxAmountInvoiced() {
        return gwTaxAmountInvoiced;
    }

    public void setGwTaxAmountInvoiced(Double gwTaxAmountInvoiced) {
        this.gwTaxAmountInvoiced = gwTaxAmountInvoiced;
    }

    public Double getGwTaxAmountRefunded() {
        return gwTaxAmountRefunded;
    }

    public void setGwTaxAmountRefunded(Double gwTaxAmountRefunded) {
        this.gwTaxAmountRefunded = gwTaxAmountRefunded;
    }

    public Double getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setDiscountTaxCompensationAmount(Double discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public Double getDiscountTaxCompensationCanceled() {
        return discountTaxCompensationCanceled;
    }

    public void setDiscountTaxCompensationCanceled(Double discountTaxCompensationCanceled) {
        this.discountTaxCompensationCanceled = discountTaxCompensationCanceled;
    }

    public Double getDiscountTaxCompensationInvoiced() {
        return discountTaxCompensationInvoiced;
    }

    public void setDiscountTaxCompensationInvoiced(Double discountTaxCompensationInvoiced) {
        this.discountTaxCompensationInvoiced = discountTaxCompensationInvoiced;
    }

    public Double getDiscountTaxCompensationRefunded() {
        return discountTaxCompensationRefunded;
    }

    public void setDiscountTaxCompensationRefunded(Double discountTaxCompensationRefunded) {
        this.discountTaxCompensationRefunded = discountTaxCompensationRefunded;
    }

    public Double getIsQtyDecimal() {
        return isQtyDecimal;
    }

    public void setIsQtyDecimal(Double isQtyDecimal) {
        this.isQtyDecimal = isQtyDecimal;
    }

    public Double getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Double isVirtual) {
        this.isVirtual = isVirtual;
    }

    public Double getItemId() {
        return itemId;
    }

    public void setItemId(Double itemId) {
        this.itemId = itemId;
    }

    public Double getLockedDoInvoice() {
        return lockedDoInvoice;
    }

    public void setLockedDoInvoice(Double lockedDoInvoice) {
        this.lockedDoInvoice = lockedDoInvoice;
    }

    public Double getLockedDoShip() {
        return lockedDoShip;
    }

    public void setLockedDoShip(Double lockedDoShip) {
        this.lockedDoShip = lockedDoShip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getNoDiscount() {
        return noDiscount;
    }

    public void setNoDiscount(Double noDiscount) {
        this.noDiscount = noDiscount;
    }

    public Double getOrderId() {
        return orderId;
    }

    public void setOrderId(Double orderId) {
        this.orderId = orderId;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Double getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(Double parentItemId) {
        this.parentItemId = parentItemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceInclTax() {
        return priceInclTax;
    }

    public void setPriceInclTax(Double priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public Double getProductId() {
        return productId;
    }

    public void setProductId(Double productId) {
        this.productId = productId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Double getQtyBackordered() {
        return qtyBackordered;
    }

    public void setQtyBackordered(Double qtyBackordered) {
        this.qtyBackordered = qtyBackordered;
    }

    public Double getQtyCanceled() {
        return qtyCanceled;
    }

    public void setQtyCanceled(Double qtyCanceled) {
        this.qtyCanceled = qtyCanceled;
    }

    public Double getQtyInvoiced() {
        return qtyInvoiced;
    }

    public void setQtyInvoiced(Double qtyInvoiced) {
        this.qtyInvoiced = qtyInvoiced;
    }

    public Double getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(Double qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public Double getQtyRefunded() {
        return qtyRefunded;
    }

    public void setQtyRefunded(Double qtyRefunded) {
        this.qtyRefunded = qtyRefunded;
    }

    public Double getQtyReturned() {
        return qtyReturned;
    }

    public void setQtyReturned(Double qtyReturned) {
        this.qtyReturned = qtyReturned;
    }

    public Double getQtyShipped() {
        return qtyShipped;
    }

    public void setQtyShipped(Double qtyShipped) {
        this.qtyShipped = qtyShipped;
    }

    public Double getQuoteItemId() {
        return quoteItemId;
    }

    public void setQuoteItemId(Double quoteItemId) {
        this.quoteItemId = quoteItemId;
    }

    public Double getRowInvoiced() {
        return rowInvoiced;
    }

    public void setRowInvoiced(Double rowInvoiced) {
        this.rowInvoiced = rowInvoiced;
    }

    public Double getRowTotal() {
        return rowTotal;
    }

    public void setRowTotal(Double rowTotal) {
        this.rowTotal = rowTotal;
    }

    public Double getRowTotalInclTax() {
        return rowTotalInclTax;
    }

    public void setRowTotalInclTax(Double rowTotalInclTax) {
        this.rowTotalInclTax = rowTotalInclTax;
    }

    public Double getRowWeight() {
        return rowWeight;
    }

    public void setRowWeight(Double rowWeight) {
        this.rowWeight = rowWeight;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getStoreId() {
        return storeId;
    }

    public void setStoreId(Double storeId) {
        this.storeId = storeId;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getTaxBeforeDiscount() {
        return taxBeforeDiscount;
    }

    public void setTaxBeforeDiscount(Double taxBeforeDiscount) {
        this.taxBeforeDiscount = taxBeforeDiscount;
    }

    public Double getTaxCanceled() {
        return taxCanceled;
    }

    public void setTaxCanceled(Double taxCanceled) {
        this.taxCanceled = taxCanceled;
    }

    public Double getTaxInvoiced() {
        return taxInvoiced;
    }

    public void setTaxInvoiced(Double taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }

    public Double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(Double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public Double getTaxRefunded() {
        return taxRefunded;
    }

    public void setTaxRefunded(Double taxRefunded) {
        this.taxRefunded = taxRefunded;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getWeeeTaxApplied() {
        return weeeTaxApplied;
    }

    public void setWeeeTaxApplied(String weeeTaxApplied) {
        this.weeeTaxApplied = weeeTaxApplied;
    }

    public Double getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setWeeeTaxAppliedAmount(Double weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Double getWeeeTaxAppliedRowAmount() {
        return weeeTaxAppliedRowAmount;
    }

    public void setWeeeTaxAppliedRowAmount(Double weeeTaxAppliedRowAmount) {
        this.weeeTaxAppliedRowAmount = weeeTaxAppliedRowAmount;
    }

    public Double getWeeeTaxDisposition() {
        return weeeTaxDisposition;
    }

    public void setWeeeTaxDisposition(Double weeeTaxDisposition) {
        this.weeeTaxDisposition = weeeTaxDisposition;
    }

    public Double getWeeeTaxRowDisposition() {
        return weeeTaxRowDisposition;
    }

    public void setWeeeTaxRowDisposition(Double weeeTaxRowDisposition) {
        this.weeeTaxRowDisposition = weeeTaxRowDisposition;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }


}