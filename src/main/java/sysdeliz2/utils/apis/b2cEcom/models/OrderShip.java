package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderShip {

    @SerializedName("items")
    @Expose
    private List<ItemOrderShip> items = new ArrayList<>();
    @SerializedName("notify")
    @Expose
    private Boolean notify;
    @SerializedName("appendComment")
    @Expose
    private Boolean appendComment;
    @SerializedName("comment")
    @Expose
    private CommentOrder comment;
    @SerializedName("tracks")
    @Expose
    private List<TrackOrderShip> tracks;

    public OrderShip() {
    }

    public OrderShip(Boolean notify, Boolean appendComment, CommentOrder comment) {
        this.notify = notify;
        this.appendComment = appendComment;
        this.comment = comment;
    }

    public List<ItemOrderShip> getItems() {
        return items;
    }

    public void setItems(List<ItemOrderShip> items) {
        this.items = items;
    }

    public Boolean getNotify() {
        return notify;
    }

    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

    public Boolean getAppendComment() {
        return appendComment;
    }

    public void setAppendComment(Boolean appendComment) {
        this.appendComment = appendComment;
    }

    public CommentOrder getComment() {
        return comment;
    }

    public void setComment(CommentOrder comment) {
        this.comment = comment;
    }

    public List<TrackOrderShip> getTracks() {
        return tracks;
    }

    public void setTracks(List<TrackOrderShip> tracks) {
        this.tracks = tracks;
    }
}
