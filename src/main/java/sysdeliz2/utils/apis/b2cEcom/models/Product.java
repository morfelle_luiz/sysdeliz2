package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;

@Generated("jsonschema2pojo")
public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attribute_set_id")
    @Expose
    private Integer attributeSetId;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("visibility")
    @Expose
    private Integer visibility;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("weight")
    @Expose
    private Double weight;
    @SerializedName("extension_attributes")
    @Expose
    private ExtensionAttributes extensionAttributes;
    @SerializedName("product_links")
    @Expose
    private List<ProductLink> productLinks;
    @SerializedName("options")
    @Expose
    private List<Object> options;
    @SerializedName("media_gallery_entries")
    @Expose
    private List<MediaGalleryEntries> mediaGalleryEntries;
    @SerializedName("tier_prices")
    @Expose
    private List<Object> tierPrices;
    @SerializedName("custom_attributes")
    @Expose
    private List<CustomAttribute> customAttributes;

    public Product() {
    }

    public Product(VB2cProdutosEnvio produto, String cor) {
        this.setSku(produto.getReferencia() + "-" + cor);
        this.setName(produto.getRefnome());
        this.setAttributeSetId(4);
        this.setPrice(produto.getPreco().doubleValue());
        this.setStatus(1);
        this.setVisibility(4);
        this.setTypeId("configurable");
        this.setWeight(produto.getPeso().doubleValue());
    }

    public Product(VB2cProdutosEnvio produto, String cor, String tamanho) {
        this.setSku(produto.getReferencia() + "-" + cor + "-" + tamanho);
        this.setName(produto.getRefnome() + " " + tamanho);
        this.setAttributeSetId(9);
        this.setPrice(produto.getPreco().doubleValue());
        this.setStatus(1);
        this.setVisibility(1);
        this.setTypeId("simple");
        this.setWeight(produto.getPeso().doubleValue());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(Integer attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public ExtensionAttributes getExtensionAttributes() {
        if(extensionAttributes == null) extensionAttributes = new ExtensionAttributes();
        return extensionAttributes;
    }

    public void setExtensionAttributes(ExtensionAttributes extensionAttributes) {
        this.extensionAttributes = extensionAttributes;
    }

    public List<ProductLink> getProductLinks() {
        return productLinks;
    }

    public void setProductLinks(List<ProductLink> productLinks) {
        this.productLinks = productLinks;
    }

    public List<Object> getOptions() {
        return options;
    }

    public void setOptions(List<Object> options) {
        this.options = options;
    }

    public List<MediaGalleryEntries> getMediaGalleryEntries() {
        if (mediaGalleryEntries == null) return new ArrayList<>();
        return mediaGalleryEntries;
    }

    public void setMediaGalleryEntries(List<MediaGalleryEntries> mediaGalleryEntries) {
        this.mediaGalleryEntries = mediaGalleryEntries;
    }

    public List<Object> getTierPrices() {
        return tierPrices;
    }

    public void setTierPrices(List<Object> tierPrices) {
        this.tierPrices = tierPrices;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }

}