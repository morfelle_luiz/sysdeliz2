package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ProductLink {

    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("link_type")
    @Expose
    private String linkType;
    @SerializedName("linked_product_sku")
    @Expose
    private String linkedProductSku;
    @SerializedName("linked_product_type")
    @Expose
    private String linkedProductType;
    @SerializedName("position")
    @Expose
    private Integer position;

    public ProductLink() {
    }

    public ProductLink(Product product, int posicao, String linkedSku, boolean isRelated) {
        this.setSku(product.getSku());
        this.setPosition(posicao);
        this.setLinkType(isRelated ? "related" : "upsell");
        this.setLinkedProductType(isRelated ? "simple" : "configurable");
        this.setLinkedProductSku(linkedSku);
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkedProductSku() {
        return linkedProductSku;
    }

    public void setLinkedProductSku(String linkedProductSku) {
        this.linkedProductSku = linkedProductSku;
    }

    public String getLinkedProductType() {
        return linkedProductType;
    }

    public void setLinkedProductType(String linkedProductType) {
        this.linkedProductType = linkedProductType;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

}