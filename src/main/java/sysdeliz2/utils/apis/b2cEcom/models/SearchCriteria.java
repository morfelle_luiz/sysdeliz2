package sysdeliz2.utils.apis.b2cEcom.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class SearchCriteria {

    @SerializedName("filter_groups")
    @Expose
    private List<Object> filterGroups = null;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;

    public List<Object> getFilterGroups() {
        return filterGroups;
    }

    public void setFilterGroups(List<Object> filterGroups) {
        this.filterGroups = filterGroups;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}