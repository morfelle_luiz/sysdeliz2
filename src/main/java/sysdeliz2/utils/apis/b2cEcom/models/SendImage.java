package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class SendImage {

    @SerializedName("entry")
    @Expose
    private MediaGalleryEntries entry;

    public SendImage(MediaGalleryEntries entry) {
        this.entry = entry;
    }

    public MediaGalleryEntries getEntry() {
        return entry;
    }

    public void setEntry(MediaGalleryEntries entry) {
        this.entry = entry;
    }

}