package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.ti.TabTam;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Generated("jsonschema2pojo")
public class Size {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("sort_order")
    @Expose
    private Integer sortOrder;
    @SerializedName("is_default")
    @Expose
    private Boolean isDefault;
    @SerializedName("store_labels")
    @Expose
    private List<StoreLabel> storeLabels;

    public Size() {
    }

    public Size(TabTam tamanho) {
        this.label = tamanho.getTam();
        this.value = tamanho.getTam();
        storeLabels = new ArrayList<>(Arrays.asList(new StoreLabel(tamanho.getTam())));
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public List<StoreLabel> getStoreLabels() {
        return storeLabels;
    }

    public void setStoreLabels(List<StoreLabel> storeLabels) {
        this.storeLabels = storeLabels;
    }

}