package sysdeliz2.utils.apis.b2cEcom.models;

import javafx.beans.property.*;

public class StatusEnvioProduto {
    private final IntegerProperty statusCode = new SimpleIntegerProperty();
    private final BooleanProperty enviado = new SimpleBooleanProperty(false);
    private final StringProperty mensagem = new SimpleStringProperty();

    public StatusEnvioProduto() {
    }

    public StatusEnvioProduto(int code, boolean enviado, String mensagem) {
        this.statusCode.set(code);
        this.enviado.set(enviado);
        this.mensagem.set(mensagem);
    }


    public int getStatusCode() {
        return statusCode.get();
    }

    public IntegerProperty statusCodeProperty() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode.set(statusCode);
    }

    public boolean isEnviado() {
        return enviado.get();
    }

    public BooleanProperty enviadoProperty() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado.set(enviado);
    }

    public String getMensagem() {
        return mensagem.get();
    }

    public StringProperty mensagemProperty() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem.set(mensagem);
    }
}
