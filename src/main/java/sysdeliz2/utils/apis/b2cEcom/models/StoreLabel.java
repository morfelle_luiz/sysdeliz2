package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreLabel {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("store_id")
    @Expose
    private int storeId;

    public StoreLabel(String label) {
        this.label = label;
        this.storeId = 0;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
}
