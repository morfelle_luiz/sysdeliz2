package sysdeliz2.utils.apis.b2cEcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackOrderShip {

    @SerializedName("track_number")
    @Expose
    private String trackNumber;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("carrier_code")
    @Expose
    private String carrierCode;

    public TrackOrderShip() {
    }

    public TrackOrderShip(String trackNumber, String title, String carrierCode) {
        this.trackNumber = trackNumber;
        this.title = title;
        this.carrierCode = carrierCode;
    }

    public String getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(String trackNumber) {
        this.trackNumber = trackNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }
}
