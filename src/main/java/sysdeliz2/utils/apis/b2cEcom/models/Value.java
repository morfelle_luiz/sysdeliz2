package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Value {

    @SerializedName("value_index")
    @Expose
    private Integer valueIndex;

    public Value(Integer valueIndex) {
        this.valueIndex = valueIndex;
    }

    public Integer getValueIndex() {
        return valueIndex;
    }

    public void setValueIndex(Integer valueIndex) {
        this.valueIndex = valueIndex;
    }

}