package sysdeliz2.utils.apis.b2cEcom.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class VaultPaymentToken {

    @SerializedName("entity_id")
    @Expose
    private Integer entityId;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("public_hash")
    @Expose
    private String publicHash;
    @SerializedName("payment_method_code")
    @Expose
    private String paymentMethodCode;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("expires_at")
    @Expose
    private String expiresAt;
    @SerializedName("gateway_token")
    @Expose
    private String gatewayToken;
    @SerializedName("token_details")
    @Expose
    private String tokenDetails;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_visible")
    @Expose
    private Boolean isVisible;

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getPublicHash() {
        return publicHash;
    }

    public void setPublicHash(String publicHash) {
        this.publicHash = publicHash;
    }

    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getGatewayToken() {
        return gatewayToken;
    }

    public void setGatewayToken(String gatewayToken) {
        this.gatewayToken = gatewayToken;
    }

    public String getTokenDetails() {
        return tokenDetails;
    }

    public void setTokenDetails(String tokenDetails) {
        this.tokenDetails = tokenDetails;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

}