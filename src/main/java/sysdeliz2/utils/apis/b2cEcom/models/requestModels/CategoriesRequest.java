package sysdeliz2.utils.apis.b2cEcom.models.requestModels;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.Category;
import sysdeliz2.utils.apis.b2cEcom.models.SearchCriteria;

@Generated("jsonschema2pojo")
public class CategoriesRequest {

    @SerializedName("items")
    @Expose
    private List<Category> categories = new ArrayList<Category>();
    @SerializedName("search_criteria")
    @Expose
    private SearchCriteria searchCriteria;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public SearchCriteria getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

}