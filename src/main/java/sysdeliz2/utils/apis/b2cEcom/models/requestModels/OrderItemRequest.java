package sysdeliz2.utils.apis.b2cEcom.models.requestModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class OrderItemRequest {

    @Expose
    @SerializedName("items")
    private List<OrderItem> items = new ArrayList<>();

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }
}
