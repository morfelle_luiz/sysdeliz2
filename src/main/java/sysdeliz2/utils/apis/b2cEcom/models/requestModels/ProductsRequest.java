package sysdeliz2.utils.apis.b2cEcom.models.requestModels;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.Product;
import sysdeliz2.utils.apis.b2cEcom.models.SearchCriteria;

@Generated("jsonschema2pojo")
public class ProductsRequest {

    @SerializedName("items")
    @Expose
    private List<Product> items;
    @SerializedName("search_criteria")
    @Expose
    private SearchCriteria searchCriteria;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;

    public List<Product> getItems() {
        return items;
    }

    public void setItems(List<Product> items) {
        this.items = items;
    }

    public SearchCriteria getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

}