package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.Category;

public class SendCategory {

    @SerializedName("category")
    @Expose
    private Category category;

    public SendCategory() {
    }

    public SendCategory(Integer parentId, String name) {
        this.category = new Category(parentId, name);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
