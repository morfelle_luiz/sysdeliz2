package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;
import sysdeliz2.utils.apis.b2cEcom.models.Color;

@Generated("jsonschema2pojo")
public class SendColor {

    @SerializedName("option")
    @Expose
    private Color color;

    public SendColor () {

    }

    public SendColor(VB2cProdutoCores cor) {
        this.color = new Color(cor);
    }

    public SendColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}