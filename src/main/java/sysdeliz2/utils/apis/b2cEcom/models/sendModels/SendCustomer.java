package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.apis.b2cEcom.models.Customer;

@Generated("jsonschema2pojo")
public class SendCustomer {

    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("redirectUrl")
    @Expose
    private String redirectUrl;

    public SendCustomer() {

    }

    public SendCustomer(Entidade it, int cod) {
       this.customer =  new Customer(it, cod);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}