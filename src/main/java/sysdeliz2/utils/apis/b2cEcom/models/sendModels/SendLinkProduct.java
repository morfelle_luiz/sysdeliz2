package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.ProductLink;

import java.util.List;

@Generated("jsonschema2pojo")
public class SendLinkProduct {

    @SerializedName("items")
    @Expose
    private List<ProductLink> items;

    public SendLinkProduct() {
    }

    public SendLinkProduct(List<ProductLink> list) {
        this.items = list;
    }


}