package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import javax.annotation.Generated;
import javax.persistence.criteria.JoinType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.B2CPedido;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.PedReservaBean;
import sysdeliz2.models.view.b2c.VB2cProdutoCorFotos;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;
import sysdeliz2.utils.apis.b2cEcom.ServicoB2CEcom;
import sysdeliz2.utils.apis.b2cEcom.models.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Generated("jsonschema2pojo")
public class SendProduct {

    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("saveOptions")
    @Expose
    private Boolean saveOptions;

    private transient String marca;

    public SendProduct() {
    }

    public SendProduct(VB2cProdutosEnvio produto, VB2cProdutoCores cor, Size tamanho, Category category, Color color, boolean simple) {
        if (simple) product = new Product(produto, cor.getCor(), tamanho.getLabel());
        else product = new Product(produto, cor.getCor());
        loadWebsiteId(produto);
        loadSearchAttributes(produto, color, tamanho, simple);
        loadStockItem(produto, cor.getCor(), tamanho == null ? "" : tamanho.getLabel(), simple);
        if (!simple) {
//            loadProductLinks(produto, cor.getCor());
            loadImages(produto, cor.getCor(), product.getSku());
            loadCategories(produto, category, cor.getCor());
            product.setStatus(product.getExtensionAttributes().getStockItem().getQty() == 0 ? 2 : 1);
            product.getExtensionAttributes().getStockItem().setQty(0);
        }
        this.marca = produto.getMarca();
    }

    private void loadImages(VB2cProdutosEnvio produto, String cor, String sku) {
        List<MediaGalleryEntries> mediaGalleryEntries = new ArrayList<>();
        List<VB2cProdutoCorFotos> cores = (List<VB2cProdutoCorFotos>) new FluentDao()
                .selectFrom(VB2cProdutoCorFotos.class).where(it -> it
                        .equal("referencia", produto.getReferencia())
                        .equal("codCor", cor))
                .orderBy("ordem", OrderType.ASC)
                .resultList();

        for (VB2cProdutoCorFotos corProd : cores) {
            if (!corProd.getImagem().contains("semimg"))
                try {
                    if (cores.size() >= 2 && produto.getMarca().equals("F")) {
                        mediaGalleryEntries.add(new MediaGalleryEntries(sku, corProd.getOrdem(),
                                corProd.getOrdem() == 2 ? Arrays.asList("image", "small_image") : corProd.getOrdem() == 1 ? Arrays.asList("thumbnail") : new ArrayList<>(),
                                corProd.getImagem()));
                    } else if (cores.size() >= 2) {
                        mediaGalleryEntries.add(new MediaGalleryEntries(sku, corProd.getOrdem(),
                                corProd.getOrdem() == 1 ? Arrays.asList("image", "small_image") : corProd.getOrdem() == 2 ? Arrays.asList("thumbnail") : new ArrayList<>(),
                                corProd.getImagem()));
                    } else {
                        mediaGalleryEntries.add(new MediaGalleryEntries(sku, corProd.getOrdem(),
                                corProd.getOrdem() == 1 ? Arrays.asList("image", "small_image", "thumbnail") : new ArrayList<>(),
                                corProd.getImagem()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        this.product.setMediaGalleryEntries(mediaGalleryEntries);
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getSaveOptions() {
        return saveOptions;
    }

    public void setSaveOptions(Boolean saveOptions) {
        this.saveOptions = saveOptions;
    }

    private void loadSearchAttributes(VB2cProdutosEnvio produto, Color color, Size tamanho, boolean simple) {
        List<CustomAttribute> customAttributeList = new ArrayList<>();
        customAttributeList.add(new CustomAttribute("url_key", this.product.getName() + "-" + this.product.getSku()));
        customAttributeList.add(new CustomAttribute("meta_title", produto.getSdProduto().getMetatitle()));
        customAttributeList.add(new CustomAttribute("meta_keyword", produto.getSdProduto().getMetakeyword()));
        customAttributeList.add(new CustomAttribute("meta_description", produto.getSdProduto().getMetadescription()));
        customAttributeList.add(new CustomAttribute("tax_class_id", "1"));
        customAttributeList.add(new CustomAttribute("color", color.getValue()));
        customAttributeList.add(new CustomAttribute("short_description", "<p>" + produto.getSdProduto().getShortdescription() + "</p>"));
        customAttributeList.add(new CustomAttribute("description", "<p>" + produto.getSdProduto().getLongdescription() + "</p>"));
        customAttributeList.add(new CustomAttribute("tabela_de_medidas", getCodigoTabelaMedida(produto.getSdProduto().getTabelamedida())));
        if (simple)
            customAttributeList.add(new CustomAttribute("size", tamanho.getValue()));
        this.product.setCustomAttributes(customAttributeList);
    }

    private String getCodigoTabelaMedida(String tabelamedida) {
        switch (tabelamedida) {
            case "CAMISAS_CAMISETAS_JAQUETAS_D":
                return "618";
            case "REGULAR_SLIM_RELAXED_CONFORT_D":
                return "619";
            case "TRICOT_D":
                return "659";
            case "SKINNY_D":
                return "620";
            default:
                return "341";
        }
    }

    private void loadCategories(VB2cProdutosEnvio produto, Category category, String cor) {

        if (new ServicoB2CEcom().getProductBySku(produto.getReferencia() + "-" + cor) == null)
            this.product.getExtensionAttributes().setCategoryLinks(
                    new ArrayList<>(
                            Arrays.asList(
                                    new CategoryLink(produto.getMarca().equals("D") ? "2" : "3", 0),
                                    new CategoryLink(String.valueOf(category.getId()), 1)
                            )
                    )
            );
    }

    private void loadWebsiteId(VB2cProdutosEnvio produto) {
        this.product.getExtensionAttributes().setWebsiteIds(new ArrayList<>(Arrays.asList(produto.getMarca().equals("D") ? 1 : 2)));
    }

    private void loadProductLinks(VB2cProdutosEnvio produto, String cor) {
        List<PaIten> estoque = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("id.codigo", produto.getReferencia()).equal("id.cor", cor).equal("id.deposito", "0001")).resultList();
        List<ProductLink> productLinks = new ArrayList<>();
        for (int i = 0; i < estoque.size(); i++) {
            productLinks.add(new ProductLink(this.product, i, this.product.getSku() + "-" + estoque.get(i).getId().getTam(), true));
        }
        this.product.setProductLinks(productLinks);
    }

    private void loadStockItem(VB2cProdutosEnvio produto, String cor, String tam, boolean simple) {
        List<PaIten> estoques = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it
                .equal("id.codigo", produto.getReferencia())
                .equal("id.cor", cor)
                .equal("id.tam", tam, TipoExpressao.AND, when -> simple)
                .equal("id.deposito", "0023")
        ).resultList();

        List<B2CPedido> pedidos = (List<B2CPedido>) new FluentDao().selectFrom(B2CPedido.class).join("itens", JoinType.LEFT).where(it -> it
                .equal("itens>id.codigo.codigo", produto.getSdProduto().getCodigo())
                .equal("itens>id.cor.cor", cor)
                .equal("itens>id.tam", tam, TipoExpressao.AND, when -> simple)
                .equal("codStatus", 1)
        ).resultList();

        Integer qtdeAdicional = pedidos.stream().reduce(0, (acc, it) -> acc + it.getItens().stream().map(eb -> eb.getQtde_f()).reduce(0, Integer::sum), Integer::sum);

        product.getExtensionAttributes().setStockItem(new StockItem(estoques.stream().map((it) -> it.getQuantidade().intValue()).reduce(0, Integer::sum) + qtdeAdicional));
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}