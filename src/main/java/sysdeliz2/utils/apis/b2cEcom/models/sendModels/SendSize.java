package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.models.ti.TabTam;
import sysdeliz2.utils.apis.b2cEcom.models.Color;
import sysdeliz2.utils.apis.b2cEcom.models.Size;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class SendSize {

    @SerializedName("option")
    @Expose
    private Size size;

    public SendSize(TabTam tamanho) {
        this.size = new Size(tamanho);
    }

    public SendSize () {

    }

    public SendSize(Size size) {
        this.size = size;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

}