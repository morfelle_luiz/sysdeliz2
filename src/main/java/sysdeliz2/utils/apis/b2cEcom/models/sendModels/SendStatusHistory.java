package sysdeliz2.utils.apis.b2cEcom.models.sendModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sysdeliz2.utils.apis.b2cEcom.models.StatusHistory;

@Generated("jsonschema2pojo")
public class SendStatusHistory {

    @SerializedName("statusHistory")
    @Expose
    private StatusHistory statusHistory;

    public SendStatusHistory() {
    }

    public SendStatusHistory(StatusHistory statusHistory) {
        this.statusHistory = statusHistory;
    }

    public StatusHistory getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(StatusHistory statusHistory) {
        this.statusHistory = statusHistory;
    }

}