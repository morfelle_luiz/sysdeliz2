package sysdeliz2.utils.apis.b2cEcom.services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import sysdeliz2.utils.apis.b2cEcom.models.*;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.CategoriesRequest;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.OrderItemRequest;
import sysdeliz2.utils.apis.b2cEcom.models.requestModels.ProductsRequest;
import sysdeliz2.utils.apis.b2cEcom.models.sendModels.*;

import java.util.List;

public interface ServiceB2CEcom {

    @GET("rest/default/V1/products?searchCriteria=0")
    Call<ProductsRequest> getProducts();

    @GET("rest/default/V1/products/{sku}")
    Call<Product> getProduct(@Path("sku") String sku);

    @GET("rest/default/V1/products?searchCriteria[filterGroups][0][filters][0][field]=type_id&searchCriteria[filterGroups][0][filters][0][value]=configurable&searchCriteria[page_size]=999")
    Call<ProductsRequest> getProductsConfigurable();

    @GET
    Call<ProductsRequest> getProductsChildren(@Url String product);

    @POST("rest/{view}/V1/products")
    Call<SendProduct> sendProduct(@Body SendProduct produto,@Path("view") String view );

    @GET("rest/default/V1/categories/list?searchCriteria[page_size]=99999")
    Call<CategoriesRequest> getCategories();

    @POST("rest/default/V1/categories")
    Call<SendCategory> sendCategoryDlz(@Body SendCategory category);

    @POST("rest/flordelis_view/V1/categories")
    Call<SendCategory> sendCategoryFlor(@Body SendCategory category);

    @DELETE("rest/default/V1/products/{product}")
    Call<ResponseBody> deleteProduct(@Path("product") String product);

    @POST("rest/default/V1/products/{sku}/links")
    Call<Boolean> sendLinkProduct(@Path("sku") String sku, @Body SendLinkProduct sendLinkProduct);

    @GET("rest/default/V1/products/attributes/color/options")
    Call<List<Color>> getColors();

    @GET("rest/default/V1/products/{sku}/media")
    Call<List<MediaGalleryEntries>> getImages(@Path("sku") String sku);

    @DELETE("rest/all/V1/products/{sku}/media/{id}")
    Call<Boolean> deleteImages(@Path("sku") String sku, @Path("id") Integer id);

    @POST("rest/default/V1/products/attributes/color/options")
    Call<String> sendColor(@Body SendColor sendColor);

    @GET("rest/default/V1/products/attributes/size/options")
    Call<List<Size>> getSizes();

    @POST("rest/default/V1/products/attributes/size/options")
    Call<String> sendSize(@Body SendSize sendSize);

    @POST("rest/default/V1/products/{sku}/media")
    Call<String> sendImage(@Path("sku") String sku, @Body SendImage sendImage);

    @POST("rest/default/V1/customers")
    Call<Customer> sendCustomersDlz(@Body SendCustomer sendCustomer);

    @POST("rest/flordelis_view/V1/customers")
    Call<Customer> sendCustomersFlor(@Body SendCustomer sendCustomer);

    @POST("/rest/{view}/V1/orders/{id}/comments")
    Call<Boolean> updateOrderStatus(@Path("view") String view, @Path("id") String id, @Body SendStatusHistory sendStatusHistory);

    @GET
    Call<OrderItemRequest> getOrderItens(@Url String url);

    @POST("rest/{view}/V1/order/{orderId}/ship")
    Call<Integer> shipOrder(@Path("view") String view, @Path("orderId") String orderId, @Body OrderShip orderShip);
}
