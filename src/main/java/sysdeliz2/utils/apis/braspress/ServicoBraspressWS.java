package sysdeliz2.utils.apis.braspress;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import sysdeliz2.utils.apis.braspress.models.CepRequest;
import sysdeliz2.utils.apis.braspress.models.Rota;
import sysdeliz2.utils.apis.braspress.services.ServiceBraspress;
import sysdeliz2.utils.apis.retrofit.RetrofitInitializer;

import java.io.IOException;
import java.net.ConnectException;

public class ServicoBraspressWS {
    
    private ServiceBraspress service = new RetrofitInitializer<ServiceBraspress>(ServiceBraspress.class, "http://dataservices.braspress.com.br/").get();
    
    public ServicoBraspressWS() {
    }
    
    public Rota post(String cep) throws IOException {
        CepRequest body = new CepRequest(cep.replaceAll("[^0-9]",""));
        String json = body.toJson();

        try {
            RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
            Response<Rota> response = service.post("dataservice", requestBody).execute();
            if (response != null && response.code() == 200)
                return response.body();
            else
                return null;
        } catch (ConnectException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
