package sysdeliz2.utils.apis.braspress.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CepRequest {
    
    @SerializedName("cep")
    private String cep;
    
    public CepRequest() {
    }
    
    public CepRequest(String cep) {
        this.cep = cep;
    }
    
    public String getCep() {
        return cep;
    }
    
    public void setCep(String cep) {
        this.cep = cep;
    }
    
    public String toJson()  {
        Type listType = new TypeToken<CepRequest>(){}.getType();
        return new Gson().toJson(this, listType);
    }
}
