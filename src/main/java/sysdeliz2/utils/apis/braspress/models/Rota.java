package sysdeliz2.utils.apis.braspress.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Rota {
    
    @SerializedName("IDFILIAL")
    private String idfilial;
    @SerializedName("Filial")
    private String filial;
    @SerializedName("ROTA")
    private String rota;
    @SerializedName("MENSAGEM")
    private String mensagem;
    
    public Rota() {
    }
    
    public Rota(String idfilial, String filial, String rota, String mensagem) {
        this.idfilial = idfilial;
        this.filial = filial;
        this.rota = rota;
        this.mensagem = mensagem;
    }
    
    public String getIdfilial() {
        return idfilial;
    }
    
    public void setIdfilial(String idfilial) {
        this.idfilial = idfilial;
    }
    
    public String getFilial() {
        return filial;
    }
    
    public void setFilial(String filial) {
        this.filial = filial;
    }
    
    public String getRota() {
        return rota;
    }
    
    public void setRota(String rota) {
        this.rota = rota;
    }
    
    public String getMensagem() {
        return mensagem;
    }
    
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    public String toJson()  {
        Type listType = new TypeToken<Rota>(){}.getType();
        return new Gson().toJson(this, listType);
    }
}
