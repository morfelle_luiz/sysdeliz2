package sysdeliz2.utils.apis.braspress.services;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sysdeliz2.utils.apis.braspress.models.Rota;

public interface ServiceBraspress {
    @POST("{context}/consultarotacep")
    @Headers({
            "Content-Type: application/json",
            "Authorization: Basic MDMzMDY2ODAwMDAxNjQ6MzMwNjY="
    })
    Call<Rota> post(@Path("context") String context, @Body RequestBody request);
}
