package sysdeliz2.utils.apis.cielo;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import retrofit2.Response;
import sysdeliz2.utils.apis.cielo.models.request.Sales;
import sysdeliz2.utils.apis.cielo.models.response.Cancel;
import sysdeliz2.utils.apis.cielo.models.response.Capture;
import sysdeliz2.utils.apis.cielo.models.response.Sale;
import sysdeliz2.utils.apis.cielo.services.ServiceCielo;
import sysdeliz2.utils.apis.retrofit.RetrofitInitializer;

import java.io.IOException;

public class WSCielo {

    private final String PATH_API = "https://api.cieloecommerce.cielo.com.br/";
    private final String PATH_QUERY = "https://apiquery.cieloecommerce.cielo.com.br/";
//    private final String PATH_API = "https://apisandbox.cieloecommerce.cielo.com.br"; //SANDBOX
//    private final String PATH_QUERY = "https://apiquerysandbox.cieloecommerce.cielo.com.br"; //SANBOX
    private final String CONTEXT_PATH = "1";

    private ServiceCielo service = new RetrofitInitializer<ServiceCielo>(ServiceCielo.class, PATH_API).get();
    
    public WSCielo() {
    }

    public Sale sale(Sales recebimento) throws IOException, HttpException {
        String json = recebimento.toJson();
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));

        Response<Sale> response = service.sale(CONTEXT_PATH, requestBody).execute();
        if (response.code() != 201)
            throw new HttpException(response);
        return response.body();
    }

    public Capture capture(String paymentId) throws IOException, HttpException {
        Response<Capture> response = service.capture(CONTEXT_PATH, paymentId).execute();
        if (response.code() != 200)
            throw new HttpException(response);
        return response.body();
    }

    public Cancel cancel(String paymentId, Integer amount) throws IOException, HttpException {
        Response<Cancel> response = service.cancel(CONTEXT_PATH, paymentId, amount).execute();
        if (response.code() != 200)
            throw new HttpException(response);
        return response.body();
    }

}
