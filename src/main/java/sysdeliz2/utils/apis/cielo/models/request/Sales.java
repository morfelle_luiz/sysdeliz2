package sysdeliz2.utils.apis.cielo.models.request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Sales {

    @SerializedName("MerchantOrderId")
    private String merchantOrderId;
    @SerializedName("Payment")
    private Payment payment;

    public Sales() {
    }

    public Sales(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    private class Payment {

        @SerializedName("Type")
        private String type = "CreditCard";
        @SerializedName("Capture")
        private Boolean capture = true;
        @SerializedName("Amount")
        private Integer amount;
        @SerializedName("Installments")
        private Integer installments;
        @SerializedName("SoftDescriptor")
        private String softDescriptor = "DELIZFASHION";
        @SerializedName("CreditCard")
        private CreditCard creditCard;

        public Payment() {
        }

        public Payment(Integer amount, Integer installments) {
            this.type = type;
            this.amount = amount;
            this.installments = installments;
            this.softDescriptor = softDescriptor;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean isCapture() {
            return capture;
        }

        public void setCapture(Boolean capture) {
            this.capture = capture;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public Integer getInstallments() {
            return installments;
        }

        public void setInstallments(Integer installments) {
            this.installments = installments;
        }

        public String getSoftDescriptor() {
            return softDescriptor;
        }

        public void setSoftDescriptor(String softDescriptor) {
            this.softDescriptor = softDescriptor;
        }

        public CreditCard getCreditCard() {
            return creditCard;
        }

        public void setCreditCard(CreditCard creditCard) {
            this.creditCard = creditCard;
        }

        private class CreditCard {

            @SerializedName("CardNumber")
            private String cardNumber;
            @SerializedName("Holder")
            private String holder;
            @SerializedName("ExpirationDate")
            private String expirationDate;
            @SerializedName("SecurityCode")
            private String securityCode;
            @SerializedName("Brand")
            private String brand;

            public CreditCard() {
            }

            public CreditCard(String cardNumber, String holder, String expirationDate, String securityCode, String brand) {
                this.cardNumber = cardNumber;
                this.holder = holder;
                this.expirationDate = expirationDate;
                this.securityCode = securityCode;
                this.brand = brand;
            }

            public String getCardNumber() {
                return cardNumber;
            }

            public void setCardNumber(String cardNumber) {
                this.cardNumber = cardNumber;
            }

            public String getHolder() {
                return holder;
            }

            public void setHolder(String holder) {
                this.holder = holder;
            }

            public String getExpirationDate() {
                return expirationDate;
            }

            public void setExpirationDate(String expirationDate) {
                this.expirationDate = expirationDate;
            }

            public String getSecurityCode() {
                return securityCode;
            }

            public void setSecurityCode(String securityCode) {
                this.securityCode = securityCode;
            }

            public String getBrand() {
                return brand;
            }

            public void setBrand(String brand) {
                this.brand = brand;
            }
        }

        public Payment addCreditCard(String cardNumber, String holder, String expirationDate,
                                     String securityCode, String brand) {
            this.creditCard = new CreditCard(cardNumber, holder, expirationDate,
                    securityCode, brand);

            return this;
        }
    }

    public Sales addPayment(Integer amount, Integer installments, String cardNumber, String holder,
                            String expirationDate, String securityCode, String brand) {
        this.payment = new Payment(amount, installments)
                .addCreditCard(cardNumber, holder, expirationDate, securityCode, brand);
        return this;
    }

    public String toJson() {
        Type listType = new TypeToken<Sales>(){}.getType();
        return new Gson().toJson(this, listType);
    }
}
