package sysdeliz2.utils.apis.cielo.models.response;

import com.google.gson.annotations.SerializedName;

public class Capture {

    @SerializedName("Status")
    private Integer status;
    @SerializedName("ReasonCode")
    private Integer reasonCode;
    @SerializedName("ReasonMessage")
    private String reasonMessage;
    @SerializedName("ProviderReturnCode")
    private String providerReturnCode;
    @SerializedName("ProviderReturnMessage")
    private String providerReturnMessage;
    @SerializedName("ReturnCode")
    private String returnCode;
    @SerializedName("ReturnMessage")
    private String returnMessage;
    @SerializedName("Tid")
    private String tid;
    @SerializedName("ProofOfSale")
    private String proofOfSale;
    @SerializedName("AuthorizationCode")
    private String authorizationCode;

    public Capture() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonMessage() {
        return reasonMessage;
    }

    public void setReasonMessage(String reasonMessage) {
        this.reasonMessage = reasonMessage;
    }

    public String getProviderReturnCode() {
        return providerReturnCode;
    }

    public void setProviderReturnCode(String providerReturnCode) {
        this.providerReturnCode = providerReturnCode;
    }

    public String getProviderReturnMessage() {
        return providerReturnMessage;
    }

    public void setProviderReturnMessage(String providerReturnMessage) {
        this.providerReturnMessage = providerReturnMessage;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getProofOfSale() {
        return proofOfSale;
    }

    public void setProofOfSale(String proofOfSale) {
        this.proofOfSale = proofOfSale;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }
}
