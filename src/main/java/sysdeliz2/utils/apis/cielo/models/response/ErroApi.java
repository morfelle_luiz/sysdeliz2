package sysdeliz2.utils.apis.cielo.models.response;

import com.google.gson.annotations.SerializedName;

public class ErroApi {

    @SerializedName("Code")
    private String code;
    @SerializedName("Message")
    private String message;

    public ErroApi() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
