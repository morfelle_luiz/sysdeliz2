package sysdeliz2.utils.apis.cielo.models.response;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Sale {

    @SerializedName("MerchantOrderId")
    private String merchantOrderId;
    @SerializedName("Payment")
    private Payment payment;

    public Sale() {
    }

    public Sale(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public class Payment {

        @SerializedName("Type")
        private String type;
        @SerializedName("Capture")
        private Boolean capture;
        @SerializedName("Amount")
        private Integer amount;
        @SerializedName("Installments")
        private Integer installments;
        @SerializedName("SoftDescriptor")
        private String softDescriptor;
        @SerializedName("CreditCard")
        private CreditCard creditCard;
        @SerializedName("ProofOfSale")
        private String proofOfSale;
        @SerializedName("Tid")
        private String tid;
        @SerializedName("AuthorizationCode")
        private String authorizationCode;
        @SerializedName("PaymentId")
        private String paymentId;
        @SerializedName("ECI")
        private String eci;
        @SerializedName("Status")
        private Integer status;
        @SerializedName("ReturnCode")
        private String returnCorde;
        @SerializedName("ReturnMessage")
        private String returnMessage;
        @SerializedName("tryautomaticcancellation")
        private Boolean tryAutomaticCancellation;

        public Payment() {
        }

        public Payment(Integer amount, Integer installments) {
            this.type = type;
            this.amount = amount;
            this.installments = installments;
            this.softDescriptor = softDescriptor;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean isCapture() {
            return capture;
        }

        public void setCapture(Boolean capture) {
            this.capture = capture;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public Integer getInstallments() {
            return installments;
        }

        public void setInstallments(Integer installments) {
            this.installments = installments;
        }

        public String getSoftDescriptor() {
            return softDescriptor;
        }

        public void setSoftDescriptor(String softDescriptor) {
            this.softDescriptor = softDescriptor;
        }

        public CreditCard getCreditCard() {
            return creditCard;
        }

        public void setCreditCard(CreditCard creditCard) {
            this.creditCard = creditCard;
        }

        public String getProofOfSale() {
            return proofOfSale;
        }

        public void setProofOfSale(String proofOfSale) {
            this.proofOfSale = proofOfSale;
        }

        public String getTid() {
            return tid;
        }

        public void setTid(String tid) {
            this.tid = tid;
        }

        public String getAuthorizationCode() {
            return authorizationCode;
        }

        public void setAuthorizationCode(String authorizationCode) {
            this.authorizationCode = authorizationCode;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getEci() {
            return eci;
        }

        public void setEci(String eci) {
            this.eci = eci;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getReturnCorde() {
            return returnCorde;
        }

        public void setReturnCorde(String returnCorde) {
            this.returnCorde = returnCorde;
        }

        public String getReturnMessage() {
            return returnMessage;
        }

        public void setReturnMessage(String returnMessage) {
            this.returnMessage = returnMessage;
        }

        public Boolean getTryAutomaticCancellation() {
            return tryAutomaticCancellation;
        }

        public void setTryAutomaticCancellation(Boolean tryAutomaticCancellation) {
            this.tryAutomaticCancellation = tryAutomaticCancellation;
        }

        public class CreditCard {

            @SerializedName("CardNumber")
            private String cardNumber;
            @SerializedName("Holder")
            private String holder;
            @SerializedName("ExpirationDate")
            private String expirationDate;
            @SerializedName("SecurityCode")
            private String securityCode;
            @SerializedName("Brand")
            private String brand;
            @SerializedName("SaveCard")
            private Boolean saveCard;

            public CreditCard() {
            }

            public String getCardNumber() {
                return cardNumber;
            }

            public void setCardNumber(String cardNumber) {
                this.cardNumber = cardNumber;
            }

            public String getHolder() {
                return holder;
            }

            public void setHolder(String holder) {
                this.holder = holder;
            }

            public String getExpirationDate() {
                return expirationDate;
            }

            public void setExpirationDate(String expirationDate) {
                this.expirationDate = expirationDate;
            }

            public String getSecurityCode() {
                return securityCode;
            }

            public void setSecurityCode(String securityCode) {
                this.securityCode = securityCode;
            }

            public String getBrand() {
                return brand;
            }

            public void setBrand(String brand) {
                this.brand = brand;
            }

            public Boolean getSaveCard() {
                return saveCard;
            }

            public void setSaveCard(Boolean saveCard) {
                this.saveCard = saveCard;
            }
        }
    }

    public String toJson() {
        Type listType = new TypeToken<Sale>(){}.getType();
        return new Gson().toJson(this, listType);
    }
}
