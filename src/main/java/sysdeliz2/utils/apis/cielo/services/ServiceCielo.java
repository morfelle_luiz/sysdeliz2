package sysdeliz2.utils.apis.cielo.services;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;
import sysdeliz2.utils.apis.cielo.models.response.Cancel;
import sysdeliz2.utils.apis.cielo.models.response.Capture;
import sysdeliz2.utils.apis.cielo.models.response.Sale;

public interface ServiceCielo {

    String MERCHANT_ID = "e4d890ad-6ade-4e22-9a69-87aa9cf870f7";
    String MERCHANT_KEY = "O0MFEHpFx8ZlclpQ0vS1ycdVDWsZVmCqT4KJlAPT";
//    String MERCHANT_ID = "8f9aa9d7-845f-4c0e-94d1-8afa31801907"; // SANDBOX
//    String MERCHANT_KEY = "QPFDOGCUFWERTGQPEOINDQZJAJEHENFGRKDQJQFZ"; // SANDBOX

    @POST("{context}/sales/")
    @Headers({
            "Content-Type: application/json",
            "MerchantId: " + MERCHANT_ID,
            "MerchantKey: " + MERCHANT_KEY
    })
    Call<Sale> sale(@Path("context") String context, @Body RequestBody request);

    @PUT("{context}/sales/{paymentId}/capture")
    @Headers({
            "Content-Type: application/json",
            "MerchantId: " + MERCHANT_ID,
            "MerchantKey: " + MERCHANT_KEY
    })
    Call<Capture> capture(@Path("context") String context, @Path("paymentId") String paymentId);

    @PUT("{context}/sales/{paymentId}/void")
    @Headers({
            "Content-Type: application/json",
            "MerchantId: " + MERCHANT_ID,
            "MerchantKey: " + MERCHANT_KEY
    })
    Call<Cancel> cancel(@Path("context") String context, @Path("paymentId") String paymentId, @Query("amount") Integer amount);
}
