package sysdeliz2.utils.apis.fluxogama;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.apache.http.client.HttpResponseException;
import retrofit2.Response;
import sysdeliz2.utils.apis.fluxogama.models.*;
import sysdeliz2.utils.apis.fluxogama.services.ServiceFluxogama;
import sysdeliz2.utils.apis.ibtech.models.ResponseEnvioFavorito;
import sysdeliz2.utils.apis.retrofit.RetrofitInitializer;

import java.io.IOException;
import java.util.List;

public class ServicoFluxogama {

    private ServiceFluxogama service = new RetrofitInitializer<ServiceFluxogama>(ServiceFluxogama.class, "https://deliz.fluxogama.com.br/", true).get();

    public ServicoFluxogama() {
    }

    public String tokenAccess() throws IOException, HttpResponseException {
        ObjectMapper jsonObject = new ObjectMapper();
        String json = jsonObject.writeValueAsString(new RequestToken());
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<GetTokenResponse> response = service.tokenAccess(requestBody).execute();
        if (response.code() == 200 && response.body().getSucesso())
            return (response.body().getRetorno()).getToken();

        if (response.code() == 400) {
            GetTokenResponse envelope =  new Gson().fromJson(response.errorBody().charStream(), GetTokenResponse.class);
            throw new HttpResponseException(response.code(), "Não foi possível se conectar ao sistema do Fluxogama, mensagem do sistema:\n[" +
                    envelope.getEnvelope().getNivel() + "] " + envelope.getEnvelope().getMensagens().get(0));
        }

        if (response.code() == 404)
            throw new HttpResponseException(response.code(), "Não foi possível se conectar com o caminho da consulta, contate o TI.");

        throw new HttpResponseException(response.code(), "Ocorreu um erro inesperado no acesso ao sistema do Fluxogama, tente mais tarde ou contate o TI. Erro: " + response.message());
    }

    public Boolean sendMix(List<RequestSendMix> senderMix) throws IOException, HttpResponseException {
        String token = tokenAccess();

        ObjectMapper jsonObject = new ObjectMapper();
        String json = jsonObject.writeValueAsString(senderMix);
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<String> response = service.sendMix("Bearer ".concat(token), requestBody).execute();
        if (response.code() == 200)
            return true;

        if (response.code() == 401)
            throw new HttpResponseException(response.code(), "Não foi possível se conectar com o usuário do integrador no sistema Fluxogama, contate o TI.");

        if (response.code() == 404)
            throw new HttpResponseException(response.code(), "Não foi possível se conectar com o caminho da consulta, contate o TI.");

        throw new HttpResponseException(response.code(), "Ocorreu um erro inesperado no acesso ao sistema do Fluxogama, tente mais tarde ou contate o TI. Erro: " + response.message());
    }
}
