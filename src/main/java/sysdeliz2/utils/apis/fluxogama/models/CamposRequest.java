package sysdeliz2.utils.apis.fluxogama.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CamposRequest implements Serializable {

    @JsonProperty("Marca")
    private String marca;
    @JsonProperty("Segmento / Linha")
    private String linha;
    @JsonProperty("Grupo Modelagem")
    private String grpModelagem;
    @JsonProperty("Meta")
    private Integer meta;
    @JsonProperty("Preço Médio")
    private String precoMedio;
    @JsonProperty("Célula")
    private String celula;
    @JsonProperty("Meta Célula")
    private String metaCelula;

    public CamposRequest() {
    }

    public CamposRequest(String marca, String linha, String grpModelagem, Integer meta, String precoMedio, String celula, String metaCelula) {
        this.marca = marca;
        this.linha = linha;
        this.grpModelagem = grpModelagem;
        this.meta = meta;
        this.precoMedio = precoMedio;
        this.celula = celula;
        this.metaCelula = metaCelula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getGrpModelagem() {
        return grpModelagem;
    }

    public void setGrpModelagem(String grpModelagem) {
        this.grpModelagem = grpModelagem;
    }

    public Integer getMeta() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta = meta;
    }

    public String getPrecoMedio() {
        return precoMedio;
    }

    public void setPrecoMedio(String precoMedio) {
        this.precoMedio = precoMedio;
    }

    public String getCelula() {
        return celula;
    }

    public void setCelula(String celula) {
        this.celula = celula;
    }

    public String getMetaCelula() {
        return metaCelula;
    }

    public void setMetaCelula(String metaCelula) {
        this.metaCelula = metaCelula;
    }
}
