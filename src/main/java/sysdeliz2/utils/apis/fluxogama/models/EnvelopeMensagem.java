package sysdeliz2.utils.apis.fluxogama.models;

import java.util.List;

public class EnvelopeMensagem {

    private Integer nivel;
    private List<String> mensagens;

    public EnvelopeMensagem() {
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public List<String> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<String> mensagens) {
        this.mensagens = mensagens;
    }
}
