package sysdeliz2.utils.apis.fluxogama.models;

public class GetTokenResponse {

    boolean sucesso;
    TokenAccess retorno;
    EnvelopeMensagem envelope;

    public GetTokenResponse() {
    }

    public boolean getSucesso() {
        return sucesso;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public TokenAccess getRetorno() {
        return retorno;
    }

    public void setRetorno(TokenAccess retorno) {
        this.retorno = retorno;
    }

    public EnvelopeMensagem getEnvelope() {
        return envelope;
    }

    public void setEnvelope(EnvelopeMensagem envelope) {
        this.envelope = envelope;
    }
}
