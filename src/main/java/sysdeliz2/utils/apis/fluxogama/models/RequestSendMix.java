package sysdeliz2.utils.apis.fluxogama.models;

import java.io.Serializable;
import java.util.List;

public class RequestSendMix implements Serializable {

    private String wsIdColecao;
    private Integer quantidade;
    private CamposRequest campos;

    public RequestSendMix() {
    }

    public RequestSendMix(String wsIdColecao, Integer quantidade, CamposRequest campos) {
        this.wsIdColecao = wsIdColecao;
        this.quantidade = quantidade;
        this.campos = campos;
    }

    public RequestSendMix(String wsIdColecao, Integer quantidade, String marca, String linha, String grpModelagem, Integer meta, String precoMedio, String celula, String metaCelula) {
        this.wsIdColecao = wsIdColecao;
        this.quantidade = quantidade;
        this.campos = new CamposRequest(marca, linha, grpModelagem, meta, precoMedio, celula, metaCelula);
    }

    public String getWsIdColecao() {
        return wsIdColecao;
    }

    public void setWsIdColecao(String wsIdColecao) {
        this.wsIdColecao = wsIdColecao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public CamposRequest getCampos() {
        return campos;
    }

    public void setCampos(CamposRequest campos) {
        this.campos = campos;
    }
}
