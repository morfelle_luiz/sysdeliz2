package sysdeliz2.utils.apis.fluxogama.models;

import java.io.Serializable;

public class RequestToken implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer chave = 1;
    private final String usuario = "integracao";
    private final String senha = "@3005";

    public RequestToken() {
    }

    public Integer getChave() {
        return chave;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getSenha() {
        return senha;
    }
}
