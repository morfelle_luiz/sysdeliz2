package sysdeliz2.utils.apis.fluxogama.models;

public class TokenAccess {

    private String token;

    public TokenAccess() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
