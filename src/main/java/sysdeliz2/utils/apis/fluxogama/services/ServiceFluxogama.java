package sysdeliz2.utils.apis.fluxogama.services;

import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.*;
import sysdeliz2.utils.apis.fluxogama.models.GetTokenResponse;

public interface ServiceFluxogama {
    @POST("rest/api/v1/autenticacao")
    @Headers({
            "Content-Type: application/json"
    })
    Call<GetTokenResponse> tokenAccess(@Body RequestBody request);

    @POST("rest/api/v1/remessa/mix")
    Call<String> sendMix(@Header("Authorization") String authorizationToken, @Body RequestBody request);
}
