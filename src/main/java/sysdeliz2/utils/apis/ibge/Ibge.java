package sysdeliz2.utils.apis.ibge;

import com.google.gson.JsonSyntaxException;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdCidade;
import sysdeliz2.models.sysdeliz.SdPoligonos;
import sysdeliz2.utils.apis.ibge.models.MalhaMultPolygon;
import sysdeliz2.utils.apis.ibge.models.MalhaPolygon;
import sysdeliz2.utils.apis.ibge.services.ServiceIbge;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *  //Segue exemplo de uso desta api para consumir os dados das cidades
 *
 *  GenericDao<SdCidade001> cidadesDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdCidade001.class);
 *
 *  try {
 *      List<SdCidade001> cidades = cidadesDao.simpleList();
 *      List<SdPoligonos001> poligonos001List = Ibge.getInstance().getPoligonoCidades(cidades);
 *
 *      GenericDao<SdPoligonos001> poligonos001GenericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdPoligonos001.class);
 *      poligonos001List.forEach(sdPoligonos001 -> {
 *          try {
 *              poligonos001GenericDao.save(sdPoligonos001);
 *          } catch (SQLException e) {
 *              e.printStackTrace();
 *          }
 *      });
 *  } catch (SQLException e) {
 *      e.printStackTrace();
 *  }
 *
 * @author lima.joao
 * @since 25/07/2019 08:10
 */
public class Ibge {

    private static Integer[] ESTADOS_WITH_POLYGON       = new Integer[]{11,  12,  13,  14,  17,  22,  23,  24,  27,  28,  31,  41,  43,  50,  51,  52,  53 };
    private static String[] ESTADOS_WITH_POLYGON_SIGLA  = new String[] {"RO","AC","AM","RR","TO","PI","CE","RN","AL","SE","MG","PR","RS","MS","MT","GO","DF"};

    private static Integer[] ESTADOS_WITH_MULTI_POLYGON      = new Integer[]{15,  16,  21,  25,  26,  29,  32,  33,  35,  42};
    private static String[] ESTADOS_WITH_MULTI_POLYGON_SIGLA = new String[] {"PA","AP","MA","PB","PE","BA","ES","RJ","SP","SC"};

    private static Ibge ibge;
    private static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    private static Retrofit retrofit;
    private static ServiceIbge service;

    private int count = 0;
    private int position = 0;

    public static Ibge getInstance(){
        if(ibge == null){
            ibge = new Ibge();
        }

        // Configura o log http
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                 .baseUrl("https://servicodados.ibge.gov.br/api/v2/")
                 .client(client)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();

        service = retrofit.create(ServiceIbge.class);

        return ibge;
    }

    @SuppressWarnings("unused")
    public List<SdPoligonos> getPoligonoEstados(){

        List<SdPoligonos> poligonos001List = new ArrayList<>();
        // Estados com Polygon simples
        count = 0;
        for (Integer ufId : ESTADOS_WITH_POLYGON) {
            try {
                Response<MalhaPolygon> response = service.loadMalhaPolygonById(ufId.toString(), "2", "application/vnd.geo+json").execute();

                List<List<Double>> coordinates = Objects.requireNonNull(response.body()).getFeatures().get(0).getGeometry().getCoordinates().get(0);

                for(List<Double> list : coordinates){
                    poligonos001List.add(new SdPoligonos(
                            BigDecimal.valueOf(list.get(0)), // Latitude
                            BigDecimal.valueOf(list.get(1)), // Longitude
                            null,
                            null,
                            ufId.toString(),
                            ESTADOS_WITH_POLYGON_SIGLA[count],
                            0
                    ));
                }
                count++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Estados com MultiPolygon

        count = 0;
        for (Integer ufId : ESTADOS_WITH_MULTI_POLYGON) {
            try {
                Response<MalhaMultPolygon> response = service.loadMalhaMultPolygonById(ufId.toString(), "2", "application/vnd.geo+json").execute();

                List<List<List<List<Double>>>> coordinates = Objects.requireNonNull(response.body()).getFeatures().get(0).getGeometry().getCoordinates();
                position = 0;
                coordinates.forEach(nivel1 -> {
                    nivel1.get(0).forEach(nivel3 -> {

                        int finalCount = count;
                        poligonos001List.add(new SdPoligonos(
                                BigDecimal.valueOf(nivel3.get(0)), // Latitude
                                BigDecimal.valueOf(nivel3.get(1)), // Longitude
                                null,
                                null,
                                ufId.toString(),
                                ESTADOS_WITH_MULTI_POLYGON_SIGLA[finalCount],
                                position
                        ));
                    });
                    position++;
                });
                count++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return poligonos001List;
    }

    @SuppressWarnings("unused")
    public List<SdPoligonos> getPoligonoCidades(List<SdCidade> cidades){

        List<SdPoligonos> poligonos001List = new ArrayList<>();

        // Estados com Polygon simples
        GenericDao<SdPoligonos> dao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdPoligonos.class);

        // Cidades com MultiPolygon
        cidades.forEach(sdCidade001 -> {
            try {
                Response<MalhaMultPolygon> response = service
                        .loadMalhaMultPolygonById(sdCidade001.getCodigo().toString(), "2", "application/vnd.geo+json")
                        .execute();

                List<List<List<Double>>> coordinates = Objects.requireNonNull(response.body()).getFeatures().get(0).getGeometry().getCoordinates().get(0);

                int position = 0;
                for(List<List<Double>> list : coordinates){
                    Integer finalPosition = position;
                    list.forEach(coord -> {
                        poligonos001List.add(new SdPoligonos(
                                BigDecimal.valueOf(coord.get(0)), // Latitude
                                BigDecimal.valueOf(coord.get(1)), // Longitude
                                null,
                                Integer.parseInt(sdCidade001.getCodigo()),
                                null,
                                null,
                                finalPosition
                        ));
                    });
                    position++;
                }
            } catch (JsonSyntaxException | IOException e) {

                try {
                    Response<MalhaPolygon> response = service
                            .loadMalhaPolygonById(sdCidade001.getCodigo().toString(), "2", "application/vnd.geo+json")
                            .execute();

                    List<List<Double>> coordinates = Objects.requireNonNull(response.body()).getFeatures().get(0).getGeometry().getCoordinates().get(0);

                    for(List<Double> coord : coordinates){
                        poligonos001List.add(new SdPoligonos(
                                BigDecimal.valueOf(coord.get(0)), // Latitude
                                BigDecimal.valueOf(coord.get(1)), // Longitude
                                null,
                                Integer.parseInt(sdCidade001.getCodigo()),
                                null,
                                null,
                                0
                        ));
                    }
                } catch (JsonSyntaxException | IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        return poligonos001List;
    }

    private Ibge(){}
}
