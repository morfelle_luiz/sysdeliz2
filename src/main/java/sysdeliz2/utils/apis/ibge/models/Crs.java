package sysdeliz2.utils.apis.ibge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 24/07/2019 14:44
 */
public class Crs implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("properties")
    @Expose
    private Properties properties;

    @SuppressWarnings("unused")
    public Crs() {}

    /**
     *
     * @param properties propertie
     * @param type type
     */
    @SuppressWarnings("unused")
    public Crs(String type, Properties properties) {
        super();
        this.type = type;
        this.properties = properties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SuppressWarnings("unused")
    public Crs withType(String type) {
        this.type = type;
        return this;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @SuppressWarnings("unused")
    public Crs withProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

}
