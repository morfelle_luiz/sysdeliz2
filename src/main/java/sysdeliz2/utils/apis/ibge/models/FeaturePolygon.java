package sysdeliz2.utils.apis.ibge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 24/07/2019 14:48
 */
public class FeaturePolygon implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("properties")
    @Expose
    private Properties properties;
    @SerializedName("geometry")
    @Expose
    private GeometryPolygon geometry;

    /**
     * No args constructor for use in serialization
     *
     */
    public FeaturePolygon() {}

    /**
     *
     * @param properties properties
     * @param type type
     * @param geometry geometry
     */
    public FeaturePolygon(String type, Properties properties, GeometryPolygon geometry) {
        super();
        this.type = type;
        this.properties = properties;
        this.geometry = geometry;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FeaturePolygon withType(String type) {
        this.type = type;
        return this;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public FeaturePolygon withProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

    public GeometryPolygon getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryPolygon geometry) {
        this.geometry = geometry;
    }

    public FeaturePolygon withGeometry(GeometryPolygon geometry) {
        this.geometry = geometry;
        return this;
    }

}