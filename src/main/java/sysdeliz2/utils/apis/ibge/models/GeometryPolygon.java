package sysdeliz2.utils.apis.ibge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 24/07/2019 15:04
 */
public class GeometryPolygon implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("coordinates")
    @Expose
    private List<List<List<Double>>> coordinates = null;

    /**
     * No args constructor for use in serialization
     *
     */
    @SuppressWarnings("unused")
    public GeometryPolygon() {}

    /**
     *
     * @param type type
     * @param coordinates coordinates
     */
    @SuppressWarnings("unused")
    public GeometryPolygon(String type, List<List<List<Double>>> coordinates) {
        super();
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SuppressWarnings("unused")
    public GeometryPolygon withType(String type) {
        this.type = type;
        return this;
    }

    @SuppressWarnings("unused")
    public List<List<List<Double>>> getCoordinates() {
        return coordinates;
    }

    @SuppressWarnings("unused")
    public void setCoordinates(List<List<List<Double>>> coordinates) {
        this.coordinates = coordinates;
    }

    @SuppressWarnings("unused")
    public GeometryPolygon withCoordinates(List<List<List<Double>>> coordinates) {
        this.coordinates = coordinates;
        return this;
    }

}