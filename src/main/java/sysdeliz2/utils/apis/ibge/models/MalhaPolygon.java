package sysdeliz2.utils.apis.ibge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 24/07/2019 14:44
 */
public class MalhaPolygon implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("crs")
    @Expose
    private Crs crs;
    @SerializedName("features")
    @Expose
    private List<FeaturePolygon> features;

    /**
     * No args constructor for use in serialization
     *
     */
    @SuppressWarnings("unused")
    public MalhaPolygon(){}

    /**
     *
     * @param crs crs
     * @param features features
     * @param type type
     */
    @SuppressWarnings("unused")
    public MalhaPolygon(String type, Crs crs, List<FeaturePolygon> features) {
        super();
        this.type = type;
        this.crs = crs;
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SuppressWarnings("unused")
    public MalhaPolygon withType(String type) {
        this.type = type;
        return this;
    }

    @SuppressWarnings("unused")
    public Crs getCrs() {
        return crs;
    }

    @SuppressWarnings("unused")
    public void setCrs(Crs crs) {
        this.crs = crs;
    }

    @SuppressWarnings("unused")
    public MalhaPolygon withCrs(Crs crs) {
        this.crs = crs;
        return this;
    }

    public List<FeaturePolygon> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeaturePolygon> features) {
        this.features = features;
    }

    @SuppressWarnings("unused")
    public MalhaPolygon withFeatures(List<FeaturePolygon> features) {
        this.features = features;
        return this;
    }

}