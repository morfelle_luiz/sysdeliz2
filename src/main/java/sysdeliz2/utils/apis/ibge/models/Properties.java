package sysdeliz2.utils.apis.ibge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 24/07/2019 14:45
 */
public class Properties implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("codarea")
    @Expose
    private String codarea;
    @SerializedName("centroide")
    @Expose
    private List<Double> centroide = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Properties() {
    }

    /**
     *
     * @param name name
     * @param codarea codarea
     * @param centroide centroide
     */
    public Properties(String name, String codarea, List<Double> centroide) {
        super();
        this.name = name;
        this.codarea = codarea;
        this.centroide = centroide;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unused")
    public Properties withName(String name) {
        this.name = name;
        return this;
    }

    @SuppressWarnings("unused")
    public String getCodarea() {
        return codarea;
    }

    @SuppressWarnings("unused")
    public void setCodarea(String codarea) {
        this.codarea = codarea;
    }

    @SuppressWarnings("unused")
    public Properties withCodarea(String codarea) {
        this.codarea = codarea;
        return this;
    }

    @SuppressWarnings("unused")
    public List<Double> getCentroide() {
        return centroide;
    }

    @SuppressWarnings("unused")
    public void setCentroide(List<Double> centroide) {
        this.centroide = centroide;
    }

    @SuppressWarnings("unused")
    public Properties withCentroide(List<Double> centroide) {
        this.centroide = centroide;
        return this;
    }

}