package sysdeliz2.utils.apis.ibge.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sysdeliz2.utils.apis.ibge.models.MalhaMultPolygon;
import sysdeliz2.utils.apis.ibge.models.MalhaPolygon;

/**
 * @author lima.joao
 * @since 24/07/2019 15:47
 */
public interface ServiceIbge {
    @GET("malhas/{id}")
    Call<MalhaPolygon> loadMalhaPolygonById(@Path("id") String id,  @Query("resolucao") String resolucao, @Query(value = "formato", encoded = true) String formato);

    @GET("malhas/{id}")
    Call<MalhaMultPolygon> loadMalhaMultPolygonById(@Path("id") String id, @Query("resolucao") String resolucao, @Query(value = "formato", encoded = true) String formato);

}