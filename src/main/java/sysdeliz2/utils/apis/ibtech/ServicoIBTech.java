package sysdeliz2.utils.apis.ibtech;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.apache.http.client.HttpResponseException;
import retrofit2.Response;
import sysdeliz2.utils.apis.ibtech.models.RequestAuth;
import sysdeliz2.utils.apis.ibtech.models.RequestEnviarFavoritos;
import sysdeliz2.utils.apis.ibtech.models.ResponseEnvioFavorito;
import sysdeliz2.utils.apis.ibtech.models.ResponseToken;
import sysdeliz2.utils.apis.ibtech.services.ServiceIBTech;
import sysdeliz2.utils.apis.retrofit.RetrofitInitializer;

import java.io.IOException;

public class ServicoIBTech {

    private ServiceIBTech service = new RetrofitInitializer<ServiceIBTech>(ServiceIBTech.class, "https://deliz.geovendas.app/IBTech_Geo/rest/").get();
    private ServiceIBTech serviceHomologacao = new RetrofitInitializer<ServiceIBTech>(ServiceIBTech.class, "https://deliz.geovendas.app/IBTech_Geo/rest/").get();

    public ServicoIBTech() {
    }

    public String getToken() throws IOException, HttpResponseException {
        ObjectMapper jsonObject = new ObjectMapper();
        String json = jsonObject.writeValueAsString(new RequestAuth("rep0126", "deliz2021"));
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<ResponseToken> response = service.getToken(requestBody).execute();
        if (response.code() == 200)
            if (response.body().getToken() != null && response.body().getToken().length() > 0)
                return response.body().getToken();
            else
                throw new HttpResponseException(response.code(), "Não foi possível obter o token de conexão: " + response.body().getMessage());

        throw new HttpResponseException(response.code(), "Não foi possível obter o token de conexão: " + response.message());
    }

    public void enviarFavorito(String token, RequestEnviarFavoritos envio) throws IOException, HttpResponseException {
        ObjectMapper jsonObject = new ObjectMapper();
        String json = jsonObject.writeValueAsString(envio);
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<ResponseEnvioFavorito> response = service.enviarFavoritos(token, requestBody).execute();
        if (response.code() == 200)
            if (response.body().getSuccess() != null && response.body().getSuccess().equals("true"))
                return;
            else
                throw new HttpResponseException(response.code(), "Não foi possível enviar a lista: " + response.body().getMessage());

        if (response.errorBody() != null) {
            ResponseEnvioFavorito responseError = new Gson().fromJson(response.errorBody().charStream(), ResponseEnvioFavorito.class);
            throw new HttpResponseException(response.code(), "Erro ao enviar a lista: " + responseError.getMessage());
        } else
            throw new HttpResponseException(response.code(), "Erro ao enviar a lista: " + response.message());
    }
}
