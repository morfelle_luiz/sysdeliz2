package sysdeliz2.utils.apis.ibtech.models;

import java.io.Serializable;

public class RequestAuth implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;
    private String password;

    public RequestAuth() {
    }

    public RequestAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
