package sysdeliz2.utils.apis.ibtech.models;

import sysdeliz2.models.view.b2b.VB2bProdutosEnvio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class RequestEnviarFavoritos implements Serializable {

    private SalesOrder salesOrder;

    public RequestEnviarFavoritos() {
    }

    public RequestEnviarFavoritos(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public static class SalesOrder {

        private String codRepres;
        private Cliente cliente;
        private TabelaPreco tabelaPreco;
        private Integer qtdePecas;
        private List<Item> itens;

        public SalesOrder() {
        }

        public SalesOrder(String codRepres, Cliente cliente, TabelaPreco tabelaPreco, Integer qtdePecas, List<Item> itens) {
            this.codRepres = codRepres;
            this.cliente = cliente;
            this.tabelaPreco = tabelaPreco;
            this.qtdePecas = qtdePecas;
            this.itens = itens;
        }

        public String getCodRepres() {
            return codRepres;
        }

        public void setCodRepres(String codRepres) {
            this.codRepres = codRepres;
        }

        public Cliente getCliente() {
            return cliente;
        }

        public void setCliente(Cliente cliente) {
            this.cliente = cliente;
        }

        public TabelaPreco getTabelaPreco() {
            return tabelaPreco;
        }

        public void setTabelaPreco(TabelaPreco tabelaPreco) {
            this.tabelaPreco = tabelaPreco;
        }

        public Integer getQtdePecas() {
            return qtdePecas;
        }

        public void setQtdePecas(Integer qtdePecas) {
            this.qtdePecas = qtdePecas;
        }

        public List<Item> getItens() {
            return itens;
        }

        public void setItens(List<Item> itens) {
            this.itens = itens;
        }
    }
    public static class Cliente {

        private String codCliente;

        public Cliente() {
        }

        public Cliente(String codCliente) {
            this.codCliente = codCliente;
        }

        public String getCodCliente() {
            return codCliente;
        }

        public void setCodCliente(String codCliente) {
            this.codCliente = codCliente;
        }


    }
    public static class TabelaPreco {

        private String codTabPreco;

        public TabelaPreco() {
        }

        public TabelaPreco(String codTabPreco) {
            this.codTabPreco = codTabPreco;
        }

        public String getCodTabPreco() {
            return codTabPreco;
        }

        public void setCodTabPreco(String codTabPreco) {
            this.codTabPreco = codTabPreco;
        }
    }
    public static class Item {
        private Produto produto;
        private Integer quantidade;
        private List<GradeSortida> gradeSortida;

        public Item() {
        }

        public Item(String produto, Integer quantidadeProd, String seqSortimento, String seqTamanho, String codTamanho, Integer quantidadeGrade) {
            this.produto = new Produto(produto);
            this.quantidade = quantidadeProd;
            this.gradeSortida = Arrays.asList(new GradeSortida(seqSortimento, seqTamanho, codTamanho, quantidadeGrade));
        }

        public Produto getProduto() {
            return produto;
        }

        public void setProduto(Produto produto) {
            this.produto = produto;
        }

        public Integer getQuantidade() {
            return quantidade;
        }

        public void setQuantidade(Integer quantidade) {
            this.quantidade = quantidade;
        }

        public List<GradeSortida> getGradeSortida() {
            return gradeSortida;
        }

        public void setGradeSortida(List<GradeSortida> gradeSortida) {
            this.gradeSortida = gradeSortida;
        }
    }
    public static class Produto {
        private String codReferencia;

        public Produto() {
        }

        public Produto(String codReferencia) {
            this.codReferencia = codReferencia;
        }

        public String getCodReferencia() {
            return codReferencia;
        }

        public void setCodReferencia(String codReferencia) {
            this.codReferencia = codReferencia;
        }
    }
    public static class GradeSortida {
        private String seqSortimento;
        private String seqTamanho;
        private String codTamanho;
        private Integer quantidade;

        public GradeSortida() {
        }

        public GradeSortida(String seqSortimento, String seqTamanho, String codTamanho, Integer quantidade) {
            this.seqSortimento = seqSortimento;
            this.seqTamanho = seqTamanho;
            this.codTamanho = codTamanho;
            this.quantidade = quantidade;
        }

        public String getSeqSortimento() {
            return seqSortimento;
        }

        public void setSeqSortimento(String seqSortimento) {
            this.seqSortimento = seqSortimento;
        }

        public String getSeqTamanho() {
            return seqTamanho;
        }

        public void setSeqTamanho(String seqTamanho) {
            this.seqTamanho = seqTamanho;
        }

        public String getCodTamanho() {
            return codTamanho;
        }

        public void setCodTamanho(String codTamanho) {
            this.codTamanho = codTamanho;
        }

        public Integer getQuantidade() {
            return quantidade;
        }

        public void setQuantidade(Integer quantidade) {
            this.quantidade = quantidade;
        }
    }
}
