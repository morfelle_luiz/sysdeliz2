package sysdeliz2.utils.apis.ibtech.models;

public class ResponseEnvioFavorito {

    String success;
    String message;

    public ResponseEnvioFavorito() {
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
