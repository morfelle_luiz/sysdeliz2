package sysdeliz2.utils.apis.ibtech.models;

public class ResponseToken {

    String succes;
    String token;
    String message;

    public ResponseToken() {
    }

    public String getSucces() {
        return succes;
    }

    public void setSucces(String succes) {
        this.succes = succes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
