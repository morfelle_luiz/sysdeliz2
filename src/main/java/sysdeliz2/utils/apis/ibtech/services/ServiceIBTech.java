package sysdeliz2.utils.apis.ibtech.services;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import sysdeliz2.utils.apis.ibtech.models.ResponseEnvioFavorito;
import sysdeliz2.utils.apis.ibtech.models.ResponseToken;

public interface ServiceIBTech {

    @POST("pedido/sugestao/auth")
    @Headers({
            "Content-Type: application/json"
    })
    Call<ResponseToken> getToken(@Body RequestBody request);

    @POST("pedido/sugestao/enviar")
    @Headers({
            "Content-Type: application/json"
    })
    Call<ResponseEnvioFavorito> enviarFavoritos(@Query("t") String token, @Body RequestBody request);

}
