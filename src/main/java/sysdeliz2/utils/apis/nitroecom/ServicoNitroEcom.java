package sysdeliz2.utils.apis.nitroecom;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.apache.http.client.HttpResponseException;
import retrofit2.Response;
import sysdeliz2.models.view.b2b.VB2bProdutosEnvio;
import sysdeliz2.utils.apis.nitroecom.models.*;
import sysdeliz2.utils.apis.nitroecom.services.ServiceNitroEcom;
import sysdeliz2.utils.apis.retrofit.RetrofitInitializer;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ServicoNitroEcom {

    private ServiceNitroEcom service = new RetrofitInitializer<ServiceNitroEcom>(ServiceNitroEcom.class, "https://lojista.deliz.com.br/").get();
    private ServiceNitroEcom serviceHomologacao = new RetrofitInitializer<ServiceNitroEcom>(ServiceNitroEcom.class, "https://deliz.nitroecom.com.br/").get();

    public ServicoNitroEcom() {
    }

    public Boolean post(List<VB2bProdutosEnvio> produtos) throws IOException, HttpResponseException {
        ObjectMapper jsonObject = new ObjectMapper();
        String json = jsonObject.writeValueAsString(new RequestProdutoNitro(produtos));
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<ResponseNitro> response = service.post(requestBody).execute();
        if (response.code() == 200)
            return response.body().getSuccess() != null && response.body().getSuccess().equals("true");

        throw new HttpResponseException(response.code(), "Não foi possível integrar o produto com a plataforma B2B.");
    }

    public Boolean sendCliente(RequestSendCliente senderCliente) throws IOException, HttpResponseException {
        String json = new Gson().toJson(senderCliente, new TypeToken<RequestSendCliente>(){}.getType());
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<ResponseNitro> response = service.sendCliente(requestBody).execute();
        if (response.code() == 200) {
            if (response.body().getSuccess() != null && response.body().getSuccess().equals("true")){
                return true;
            } else {
                throw new HttpResponseException(response.code(), "Não foi possível integrar o cliente com a plataforma B2B. Mensagem da API: "+response.body().getMensagem());
            }
        }

        throw new HttpResponseException(response.code(), "Não foi possível integrar o cliente com a plataforma B2B.");
    }

    public Boolean sendCatalogo(String json) throws IOException, HttpResponseException {
        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
        Response<ResponseNitro> response = service.sendCatalogo(requestBody).execute();
        if (response.code() == 200) {
            if (response.body().getSuccess() != null && response.body().getSuccess().equals("true")){
                return true;
            } else {
                throw new HttpResponseException(response.code(), "Não foi possível integrar o catálogo com a plataforma Catálogo Digital. Mensagem da API: "+response.body().getMensagem());
            }
        }

        throw new HttpResponseException(response.code(), "Não foi possível integrar o catálogo com a plataforma Catálogo Digital.");
    }

    public List<ResponseItensFavoritos> getFavoritosCatalogo(Integer catalogo) throws IOException, HttpResponseException {
        Response<ResponseFavoritos> response = service.getFavoritos(String.valueOf(catalogo), 999999).execute();
        if (response.code() == 200) {
            if ((response.body() != null ? response.body().getTotal_itens() : 0) > 0){
                return response.body().getItens();
            } else {
                throw new HttpResponseException(response.code(), "Não foram encontrados favoritos com o catálogo: " + catalogo);
            }
        } else {
            throw new HttpResponseException(response.code(), "Não foi possível obter os favoritos do catálogo: " + catalogo + "\n" + response.code() + ":" + response.message());
        }
    }

    public List<ResponseItensFavoritos> getFavoritosCliente(Integer catalogo, String codcli) throws IOException, HttpResponseException {
        Response<ResponseFavoritos> response = service.getFavoritos(String.valueOf(catalogo), codcli, 999999).execute();
        if (response.code() == 200) {
            return response.body().getItens();
        } else {
            throw new HttpResponseException(response.code(), "Não foi possível obter os favoritos do catálogo: " + catalogo + "\n" + response.code() + ":" + response.message());
        }
    }
}
