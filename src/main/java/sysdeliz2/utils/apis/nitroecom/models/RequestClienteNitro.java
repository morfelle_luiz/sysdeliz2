package sysdeliz2.utils.apis.nitroecom.models;

import java.io.Serializable;

public class RequestClienteNitro implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cnpj;
    private String inscricao;
    private String razao;
    private String fantasia;
    private String fone;
    private String celular;
    private String email;
    private String newsletter = "S";

    private String cep;
    private String endereco;
    private String numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String uf;

    public RequestClienteNitro() {
    }

    public RequestClienteNitro(String cnpj, String inscricao, String razao, String fantasia,
                               String fone, String celular, String email, String newsletter,
                               String cep, String endereco, String numero, String complemento,
                               String bairro, String cidade, String uf) {
        this.cnpj = cnpj;
        this.inscricao = inscricao;
        this.razao = razao;
        this.fantasia = fantasia;
        this.fone = fone;
        this.celular = celular;
        this.email = email;
        this.newsletter = newsletter;
        this.cep = cep;
        this.endereco = endereco;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
    }
}
