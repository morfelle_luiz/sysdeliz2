package sysdeliz2.utils.apis.nitroecom.models;

import sysdeliz2.models.view.b2b.VB2bProdutosEnvio;

import java.util.List;

public class RequestProdutoNitro {

    private List<VB2bProdutosEnvio> produtos;

    public RequestProdutoNitro() {
    }

    public RequestProdutoNitro(List<VB2bProdutosEnvio> produtos) {
        this.produtos = produtos;
    }

    public List<VB2bProdutosEnvio> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<VB2bProdutosEnvio> produtos) {
        this.produtos = produtos;
    }
}
