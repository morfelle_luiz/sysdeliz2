package sysdeliz2.utils.apis.nitroecom.models;

import java.util.List;

public class RequestSendCliente {

    private List<RequestClienteNitro> clientes;

    public RequestSendCliente() {
    }

    public RequestSendCliente(List<RequestClienteNitro> clientes) {
        this.clientes = clientes;
    }

    public List<RequestClienteNitro> getClientes() {
        return clientes;
    }

    public void setClientes(List<RequestClienteNitro> clientes) {
        this.clientes = clientes;
    }
}
