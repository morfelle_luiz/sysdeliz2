package sysdeliz2.utils.apis.nitroecom.models;

import java.io.Serializable;
import java.util.List;

public class ResponseFavoritos implements Serializable {

    private Integer total_itens;
    private Integer page;
    private Integer total_page;
    private List<ResponseItensFavoritos> itens;

    public ResponseFavoritos() {
    }

    public Integer getTotal_itens() {
        return total_itens;
    }

    public void setTotal_itens(Integer total_itens) {
        this.total_itens = total_itens;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    public void setTotal_page(Integer total_page) {
        this.total_page = total_page;
    }

    public List<ResponseItensFavoritos> getItens() {
        return itens;
    }

    public void setItens(List<ResponseItensFavoritos> itens) {
        this.itens = itens;
    }
}
