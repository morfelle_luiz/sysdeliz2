package sysdeliz2.utils.apis.nitroecom.models;

import java.io.Serializable;

public class ResponseItensFavoritos implements Serializable {

    private String lista_id;
    private String lista_nome;
    private String produto_codigo;
    private String catalogo_codigo;
    private String catalogo_nome;
    private String marca_codigo;
    private String marca_nome;
    private String cliente_codigo;
    private String cliente_cnpj;
    private String cliente_email;

    public ResponseItensFavoritos() {
    }

    public String getLista_id() {
        return lista_id;
    }

    public void setLista_id(String lista_id) {
        this.lista_id = lista_id;
    }

    public String getLista_nome() {
        return lista_nome;
    }

    public void setLista_nome(String lista_nome) {
        this.lista_nome = lista_nome;
    }

    public String getProduto_codigo() {
        return produto_codigo;
    }

    public void setProduto_codigo(String produto_codigo) {
        this.produto_codigo = produto_codigo;
    }

    public String getCatalogo_codigo() {
        return catalogo_codigo;
    }

    public void setCatalogo_codigo(String catalogo_codigo) {
        this.catalogo_codigo = catalogo_codigo;
    }

    public String getCatalogo_nome() {
        return catalogo_nome;
    }

    public void setCatalogo_nome(String catalogo_nome) {
        this.catalogo_nome = catalogo_nome;
    }

    public String getMarca_codigo() {
        return marca_codigo;
    }

    public void setMarca_codigo(String marca_codigo) {
        this.marca_codigo = marca_codigo;
    }

    public String getMarca_nome() {
        return marca_nome;
    }

    public void setMarca_nome(String marca_nome) {
        this.marca_nome = marca_nome;
    }

    public String getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(String cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }

    public String getCliente_cnpj() {
        return cliente_cnpj;
    }

    public void setCliente_cnpj(String cliente_cnpj) {
        this.cliente_cnpj = cliente_cnpj;
    }

    public String getCliente_email() {
        return cliente_email;
    }

    public void setCliente_email(String cliente_email) {
        this.cliente_email = cliente_email;
    }
}
