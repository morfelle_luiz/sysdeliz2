package sysdeliz2.utils.apis.nitroecom.models;

public class ResponseNitro {

    String success;
    String mensagem;

    public ResponseNitro() {
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
