package sysdeliz2.utils.apis.nitroecom.services;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;
import sysdeliz2.utils.apis.nitroecom.models.ResponseFavoritos;
import sysdeliz2.utils.apis.nitroecom.models.ResponseNitro;

public interface ServiceNitroEcom {
    @POST("webservice/produto/receive.php")
    @Headers({
            "Content-Type: application/json",
            "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMy4yODAuNDgxLzAwMDEtMzEiLCJuYW1lIjoiTml0cm8uQ29tIn0.BMxKkhcjqQ52PDeN4Ssu167T3vAByiDo7k2jj6xw4Nk"
    })
    Call<ResponseNitro> post(@Body RequestBody request);

    @POST("webservice/cliente/receive.php")
    @Headers({
            "Content-Type: application/json",
            "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMy4yODAuNDgxLzAwMDEtMzEiLCJuYW1lIjoiTml0cm8uQ29tIn0.BMxKkhcjqQ52PDeN4Ssu167T3vAByiDo7k2jj6xw4Nk"
    })
    Call<ResponseNitro> sendCliente(@Body RequestBody request);

    @POST("webservice/catalogo/receive.php")
    @Headers({
            "Content-Type: application/json",
            "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMy4yODAuNDgxLzAwMDEtMzEiLCJuYW1lIjoiTml0cm8uQ29tIn0.BMxKkhcjqQ52PDeN4Ssu167T3vAByiDo7k2jj6xw4Nk"
    })
    Call<ResponseNitro> sendCatalogo(@Body RequestBody request);

    @GET("webservice/catalogo/favoritos.php")
    @Headers({
            "Content-Type: application/json",
            "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMy4yODAuNDgxLzAwMDEtMzEiLCJuYW1lIjoiTml0cm8uQ29tIn0.BMxKkhcjqQ52PDeN4Ssu167T3vAByiDo7k2jj6xw4Nk"
    })
    Call<ResponseFavoritos> getFavoritos(@Query("catalogo") String catalogo, @Query("per_page") Integer perPage);

    @GET("webservice/catalogo/favoritos.php")
    @Headers({
            "Content-Type: application/json",
            "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMy4yODAuNDgxLzAwMDEtMzEiLCJuYW1lIjoiTml0cm8uQ29tIn0.BMxKkhcjqQ52PDeN4Ssu167T3vAByiDo7k2jj6xw4Nk"
    })
    Call<ResponseFavoritos> getFavoritos(@Query("catalogo") String catalogo, @Query("cli_codigo") String cli_codigo, @Query("per_page") Integer perPage);
}
