package sysdeliz2.utils.apis.organize;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.hibernate.JPAUtils;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalTime;

public class ServiceOrganize {
    
    // <editor-fold defaultstate="collapsed" desc="API Attributes">
    private final String API_PRODUCAO = "http://api.organizedobrasil.com.br";
    private final String MOD_AUTH = API_PRODUCAO + "/token";
    private final String MOD_GET_ROTEIRO = API_PRODUCAO + "/api/Roteiro";
    private final String MOD_GET_OPERACAO = API_PRODUCAO + "/api/Operacao?codigoRoteiro=";
    private final String USER_API = "Deliz.api";
    private final String PASS_API = "i8DrLd6iEJr4";
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Import Attributes">
    private OAuthToken authToken = null;
    private ApiErrorReturn message = null;
    private RoteiroAPI[] roteiros = null;
    private OperacaoAPI[] operacoes = null;
    private Integer codigoMaquina = 1;
    private Integer codigoOperacao = 1;
    GenericDao<SdRoteiroOrganize001> daoRoteiro = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdRoteiroOrganize001.class);
    GenericDao<SdOperacaoOrganize001> daoOperacao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdOperacaoOrganize001.class);
    GenericDao<SdEquipamentosOrganize001> daoEquipamento = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdEquipamentosOrganize001.class);
    GenericDao<SdSetorOp001> daoSetorOp = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdSetorOp001.class);
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="API methods">
    private HttpResponse executeHttpRequest(String jsonData, HttpPost httpPost) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpPost);
    }
    
    private HttpResponse executeHttpRequest(HttpGet httpGet) throws UnsupportedEncodingException, IOException {
        HttpResponse response = null;
        String line = "";
        StringBuffer result = new StringBuffer();
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(httpGet);
    }
    
    private void executeOAuth(String restUrl, String username, String password) {
        authToken = null;
        message = null;
        HttpPost httpPost = new HttpPost(restUrl);
        httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
        String entityValue = "grant_type=password&username=" + username + "&password=" + password;
        try {
            HttpResponse response = executeHttpRequest(entityValue, httpPost);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                authToken = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), OAuthToken.class);
                authToken.setTime_init(LocalTime.now());
            }
        } catch (UnsupportedEncodingException e) {
        
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpPost.releaseConnection();
        }
    }
    
    private void executeGetRoteiros(String restUrl, OAuthToken tokenAuth) {
        roteiros = null;
        message = null;
        HttpGet httpGet = new HttpGet(restUrl);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpGet.setHeader("Authorization", auth);
        httpGet.setHeader("Content-Type", "application/json; charset=iso-8859-1");
        try {
            HttpResponse response = executeHttpRequest(httpGet);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                roteiros = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), RoteiroAPI[].class);
            }
        } catch (UnsupportedEncodingException e) {
            GUIUtils.showException(e);
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpGet.releaseConnection();
        }
    }
    
    private void executeGetOperacoesRoteiro(String restUrl, OAuthToken tokenAuth, Integer codigoRoteiro) {
        operacoes = null;
        message = null;
        HttpGet httpGet = new HttpGet(restUrl + codigoRoteiro);
        String auth = new StringBuffer(tokenAuth.token_type).append(" ").append(tokenAuth.access_token).toString();
        httpGet.setHeader("Authorization", auth);
        httpGet.setHeader("Content-Type", "application/json; charset=iso-8859-1");
        try {
            HttpResponse response = executeHttpRequest(httpGet);
            if (response.getStatusLine().getStatusCode() != 200) {
                message = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), ApiErrorReturn.class);
            } else {
                operacoes = new Gson().fromJson(getJsonStr(response.getEntity().getContent()), OperacaoAPI[].class);
            }
        } catch (UnsupportedEncodingException e) {
            GUIUtils.showException(e);
        } catch (IOException e) {
            GUIUtils.showException(e);
        } catch (Exception e) {
            GUIUtils.showException(e);
        } finally {
            httpGet.releaseConnection();
        }
    }
    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Rules methods">
    private String getJsonStr(InputStream contentWsReturn) throws IOException {
        StringBuffer jsonStr = new StringBuffer();
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(contentWsReturn));
        while ((line = reader.readLine()) != null) {
            jsonStr.append(line);
        }
        return jsonStr.toString();
    }
    
    private boolean getErroConnectTimeApi() {
        LocalTime timeConnect = authToken.time_init.plusSeconds(authToken.expires_in.longValue());
        LocalTime timeNow = LocalTime.now();
        if (timeConnect.isBefore(timeNow)) {
            String statusConnect = this.connectApi();
            if (!statusConnect.equals("connected")) {
                GUIUtils.showMessage("Ocorreu ao se conectar com a API ORGANIZE.\n" +
                        "Error: " + message.getMessage(), Alert.AlertType.ERROR);
                return true;
            }
        }
        return false;
    }
    
    private String connectApi() {
        executeOAuth(MOD_AUTH, USER_API, PASS_API);
        if (message != null) {
            return message.getError_description();
        }
        return "connected";
    }
    
    private String apiGetRoteiros(OAuthToken token) {
        executeGetRoteiros(MOD_GET_ROTEIRO, authToken);
        if (message != null) {
            return message.getMessage();
        }
        return "attached";
    }
    
    private Boolean apiGetOperacoesRoteiro(OAuthToken token, Integer codigoRoteiro) {
        executeGetOperacoesRoteiro(MOD_GET_OPERACAO, authToken, codigoRoteiro);
        if (message != null) {
            GUIUtils.showMessage("Ocorreu um erro durante a importação das operações do roteiro.\n" +
                    "Error: " + message.getMessage(), Alert.AlertType.ERROR);
            return false;
        }
        return true;
    }
    
    private boolean checkConnectedApi() {
        //<editor-fold defaultstate="collapsed" desc="oAuth validation">
        if (authToken == null) {
            String statusConnect = this.connectApi();
            if (!statusConnect.equals("connected")) {
                GUIUtils.showMessage("Ocorreu ao se conectar com a API ORGANIZE.\n" +
                        "Error: " + message.getMessage(), Alert.AlertType.ERROR);
                return true;
            }
        }
        
        if (getErroConnectTimeApi()) {
            return true;
        }
        //</editor-fold>
        return false;
    }
    
    public static String ajusteCharset(String str){
        String convertText = str;
        convertText = convertText.replace("Ã£", "ã");
        convertText = convertText.replace("Ã§", "ç");
        convertText = convertText.replace("Ã¡", "á");
        convertText = convertText.replace("Ã­", "í");
        convertText = convertText.replace("Â°", "º");
        convertText = convertText.replace("Ãµ", "õ");
        convertText = convertText.replace("Ã³", "ó");
        convertText = convertText.replace("Â°", "ª");
        convertText = convertText.replace("Ãº", "ú");
        convertText = convertText.replace("Ã©", "é");
        convertText = convertText.replace("Ãª", "ê");
        convertText = convertText.replace("Ãƒ", "Ã");
        convertText = convertText.replace("ÃŠ", "Ê");
        convertText = convertText.replace("Ã©", "é");
        convertText = convertText.replace("Ãƒ", "Ã");
        convertText = convertText.replace("Ã¿", "Í");
        convertText = convertText.replace("Ãš", "Ú");
        convertText = convertText.toUpperCase();
        
        return convertText;
    }
    //</editor-fold>
    
    public ObservableList<SdRoteiroOrganize001> importRoteiros() {
        if (checkConnectedApi()) return null;
        
        //<editor-fold defaultstate="collapsed" desc="Get roteiros API">
        String statusGetRoteiros = apiGetRoteiros(authToken);
        if (!statusGetRoteiros.equals("attached")) {
            GUIUtils.showMessage("Erro na obtenção dos roteiros com a API do Organize.\n" +
                    "Error: " + statusGetRoteiros, Alert.AlertType.ERROR);
            return null;
        }
        
        ObservableList<SdRoteiroOrganize001> roteirosReturn = FXCollections.observableArrayList();
        for (RoteiroAPI roteiro : roteiros) {
            try {
                SdRoteiroOrganize001 roteiroBanco = (SdRoteiroOrganize001) daoRoteiro.initCriteria().addPredicateEq("codorg", roteiro.codigo).loadEntityByPredicate();
                if (roteiroBanco != null) {
                    String dataAtualizacaoRoteiro = roteiro.dataAlteracao;
                    if (roteiroBanco.getDtAlteracao().equals(dataAtualizacaoRoteiro)
                            || roteiroBanco.isAlteracaoMan()) {
                        continue;
                    }
                }
                roteiroBanco = new SdRoteiroOrganize001(
                        roteiro.getCodigo(),
                        roteiro.getReferencia(),
                        roteiro.getDescricao(),
                        roteiro.parte.getCodigo(),
                        roteiro.getParte().getDescricao(),
                        roteiro.getPercentual(),
                        roteiro.getSetor().getCodigo(),
                        roteiro.getSetor().getDescricao(),
                        roteiro.getTempo(),
                        roteiro.getTempoHora(),
                        roteiro.getCusto1(),
                        roteiro.getCusto2(),
                        roteiro.getCusto3(),
                        roteiro.getDataInclusao(),
                        roteiro.getDataAlteracao() != null ? roteiro.getDataAlteracao() : roteiro.getDataInclusao(),
                        null);
                roteiroBanco.setOperacoes(this.importOperacaoesRoteiro(roteiroBanco));
                roteirosReturn.add(roteiroBanco);
            } catch (SQLException ex) {
                GUIUtils.showException(ex);
            }
        }
        //</editor-fold>
        
        return roteirosReturn;
    }
    
    public ObservableList<SdOperRoteiroOrganize001> importOperacaoesRoteiro(SdRoteiroOrganize001 roteiro) {
        if (checkConnectedApi()) return null;
        
        if (apiGetOperacoesRoteiro(authToken, roteiro.getCodorg())) {
            ObservableList<SdOperRoteiroOrganize001> operacoesRoteiro = FXCollections.observableArrayList();
            for (OperacaoAPI operacao : operacoes) {
                try {
                    SdOperacaoOrganize001 operacaoBanco = (SdOperacaoOrganize001) daoOperacao.initCriteria().addPredicateEq("codorg", operacao.codigo).loadEntityByPredicate();
                    if (operacaoBanco == null || !operacao.getDataAlteracao().equals(operacaoBanco.getDtAlteracao())) {
                        SdEquipamentosOrganize001 equipamentoBanco = (SdEquipamentosOrganize001) daoEquipamento.initCriteria().addPredicateEq("codorg", operacao.getEquipamento().codigo).loadEntityByPredicate();
                        if (equipamentoBanco == null) {
                            equipamentoBanco = new SdEquipamentosOrganize001(String.valueOf(operacao.getEquipamento().codigo), operacao.getEquipamento().codigo, operacao.getEquipamento().descricao, operacao.getEquipamento().interferencia);
                        }
                        operacaoBanco = new SdOperacaoOrganize001(operacao.getCodigo(), operacao.getCodigo().toString(), operacao.descricao, operacao.tempo, operacao.tempoHora, operacao.custo,
                                operacao.concessao.doubleValue(), operacao.equipamento.codigo.toString(), operacao.dataInclusao, (operacao.dataAlteracao.equals("") ? operacao.dataInclusao : operacao.dataAlteracao),
                                operacao.compCostura, equipamentoBanco);
                    }
                    operacaoBanco.setDescricao(this.ajusteCharset(operacaoBanco.getDescricao()));
                    SdOperRoteiroOrganize001 operacoes = new SdOperRoteiroOrganize001(
                            roteiro.getCodorg(),
                            operacaoBanco,
                            operacao.ordem,
                            roteiro.getDescSetor().split(" ")[0].trim(),
                            (SdSetorOp001) daoSetorOp.initCriteria().addPredicateEq("codigo", operacao.setorOperacao.codigo).loadEntityByPredicate(),
                            operacao.tempo,
                            null);
                    operacoesRoteiro.add(operacoes);
                } catch (SQLException ex) {
                    GUIUtils.showException(ex);
                    return null;
                }
            }
            return operacoesRoteiro;
        }
        
        return null;
    }
    
    //<editor-fold defaultstate="collapsed" desc="API JSON class return">
    private class ApiErrorReturn {
        
        private String error;
        private String error_description;
        private String message;
        
        public ApiErrorReturn() {
        }
        
        public ApiErrorReturn(String error, String error_description, String message) {
            this.error = error;
            this.error_description = error_description;
            this.message = message;
        }
        
        public String getError() {
            return error;
        }
        
        public void setError(String error) {
            this.error = error;
        }
        
        public String getError_description() {
            return error_description;
        }
        
        public void setError_description(String error_description) {
            this.error_description = error_description;
        }
        
        public String getMessage() {
            return message;
        }
        
        public void setMessage(String message) {
            this.message = message;
        }
        
    }
    
    private class OAuthToken {
        
        private String access_token;
        private String token_type;
        private Integer expires_in;
        private LocalTime time_init;
        
        public OAuthToken() {
        }
        
        public OAuthToken(String access_token, String token_type, Integer expires_in, LocalTime time_init) {
            this.access_token = access_token;
            this.token_type = token_type;
            this.expires_in = expires_in;
            this.time_init = time_init;
        }
        
        public String getAccess_token() {
            return access_token;
        }
        
        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }
        
        public String getToken_type() {
            return token_type;
        }
        
        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }
        
        public Integer getExpires_in() {
            return expires_in;
        }
        
        public void setExpires_in(Integer expires_in) {
            this.expires_in = expires_in;
        }
        
        public LocalTime getTime_init() {
            return time_init;
        }
        
        public void setTime_init(LocalTime time_init) {
            this.time_init = time_init;
        }
        
    }
    
    private class ParteRoteiro {
        
        private Integer codigo;
        private String descricao;
        
        public ParteRoteiro() {
        }
        
        public ParteRoteiro(Integer codigo, String descricao) {
            this.codigo = codigo;
            this.descricao = descricao;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
    }
    
    private class SetorRoteiro {
        
        private Integer codigo;
        private String descricao;
        
        public SetorRoteiro() {
        }
        
        public SetorRoteiro(Integer codigo, String descricao) {
            this.codigo = codigo;
            this.descricao = descricao;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
    }
    
    private class SetorOperacao {
        
        private Integer codigo;
        private String descricao;
        
        public SetorOperacao() {
        }
        
        public SetorOperacao(Integer codigo, String descricao) {
            this.codigo = codigo;
            this.descricao = descricao;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
    }
    
    private class EquipamentoAPI {
        
        private Integer codigo;
        private String descricao;
        private Double interferencia;
        
        public EquipamentoAPI() {
        }
        
        public EquipamentoAPI(Integer codigo, String descricao, Double interferencia) {
            this.codigo = codigo;
            this.descricao = descricao;
            this.interferencia = interferencia;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
        public Double getInterferencia() {
            return interferencia;
        }
        
        public void setInterferencia(Double interferencia) {
            this.interferencia = interferencia;
        }
        
    }
    
    private class OperacaoAPI {
        
        private Integer codigo;
        private String descricao;
        private Integer ordem;
        private Double tempo;
        private Double tempoHora;
        private Double custo;
        private Integer concessao;
        private EquipamentoAPI equipamento;
        private String dataInclusao;
        private String dataAlteracao;
        private Double compCostura;
        private SetorOperacao setorOperacao;
        
        public OperacaoAPI() {
        }
        
        public OperacaoAPI(Integer codigo, String descricao, Integer ordem, Double tempo, Double tempoHora, Double custo, Integer concessao,
                           EquipamentoAPI equipamento, String dataInclusao, String dataAlteracao, Double compCostura, SetorOperacao setorOperacao) {
            this.codigo = codigo;
            this.descricao = descricao;
            this.ordem = ordem;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo = custo;
            this.concessao = concessao;
            this.equipamento = equipamento;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = (dataAlteracao != null && !dataAlteracao.isEmpty() && !dataAlteracao.equals("")) ? dataAlteracao : dataInclusao;
            this.compCostura = compCostura;
            this.setorOperacao = setorOperacao;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
        public Integer getOrdem() {
            return ordem;
        }
        
        public void setOrdem(Integer ordem) {
            this.ordem = ordem;
        }
        
        public Double getTempo() {
            return tempo;
        }
        
        public void setTempo(Double tempo) {
            this.tempo = tempo;
        }
        
        public Double getTempoHora() {
            return tempoHora;
        }
        
        public void setTempoHora(Double tempoHora) {
            this.tempoHora = tempoHora;
        }
        
        public Double getCusto() {
            return custo;
        }
        
        public void setCusto(Double custo) {
            this.custo = custo;
        }
        
        public Integer getConcessao() {
            return concessao;
        }
        
        public void setConcessao(Integer concessao) {
            this.concessao = concessao;
        }
        
        public EquipamentoAPI getEquipamento() {
            return equipamento;
        }
        
        public void setEquipamento(EquipamentoAPI equipamento) {
            this.equipamento = equipamento;
        }
        
        public String getDataInclusao() {
            return dataInclusao;
        }
        
        public void setDataInclusao(String dataInclusao) {
            this.dataInclusao = dataInclusao;
        }
        
        public String getDataAlteracao() {
            return dataAlteracao;
        }
        
        public void setDataAlteracao(String dataAlteracao) {
            this.dataAlteracao = dataAlteracao;
        }
        
        public Double getCompCostura() {
            return compCostura;
        }
        
        public void setCompCostura(Double compCostura) {
            this.compCostura = compCostura;
        }
        
        public SetorOperacao getSetorOperacao() {
            return setorOperacao;
        }
        
        public void setSetorOperacao(SetorOperacao setorOperacao) {
            this.setorOperacao = setorOperacao;
        }
        
    }
    
    private class RoteiroAPI {
        
        private Integer codigo;
        private String referencia;
        private String descricao;
        private ParteRoteiro parte;
        private Double percentual;
        private SetorRoteiro setor;
        private Double tempo;
        private Double tempoHora;
        private Double custo1;
        private Double custo2;
        private Double custo3;
        private String dataInclusao;
        private String dataAlteracao;
        private OperacaoAPI[] operacoes;
        
        public RoteiroAPI() {
        }
        
        public RoteiroAPI(Integer codigo, String referencia, String descricao, ParteRoteiro parte, Double percentual, SetorRoteiro setor, Double tempo, Double tempoHora,
                          Double custo1, Double custo2, Double custo3, String dataInclusao, String dataAlteracao) {
            this.codigo = codigo;
            this.referencia = referencia;
            this.descricao = descricao;
            this.parte = parte;
            this.percentual = percentual;
            this.setor = setor;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo1 = custo1;
            this.custo2 = custo2;
            this.custo3 = custo3;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = dataAlteracao;
        }
        
        public RoteiroAPI(Integer codigo, String referencia, String descricao, ParteRoteiro parte, Double percentual, SetorRoteiro setor, Double tempo, Double tempoHora,
                          Double custo1, Double custo2, Double custo3, String dataInclusao, String dataAlteracao, OperacaoAPI[] operacoes) {
            this.codigo = codigo;
            this.referencia = referencia;
            this.descricao = descricao;
            this.parte = parte;
            this.percentual = percentual;
            this.setor = setor;
            this.tempo = tempo;
            this.tempoHora = tempoHora;
            this.custo1 = custo1;
            this.custo2 = custo2;
            this.custo3 = custo3;
            this.dataInclusao = dataInclusao;
            this.dataAlteracao = dataAlteracao;
            this.operacoes = operacoes;
        }
        
        public Integer getCodigo() {
            return codigo;
        }
        
        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        public String getReferencia() {
            return referencia;
        }
        
        public void setReferencia(String referencia) {
            this.referencia = referencia;
        }
        
        public String getDescricao() {
            return descricao;
        }
        
        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
        public ParteRoteiro getParte() {
            return parte;
        }
        
        public void setParte(ParteRoteiro parte) {
            this.parte = parte;
        }
        
        public Double getPercentual() {
            return percentual;
        }
        
        public void setPercentual(Double percentual) {
            this.percentual = percentual;
        }
        
        public SetorRoteiro getSetor() {
            return setor;
        }
        
        public void setSetor(SetorRoteiro setor) {
            this.setor = setor;
        }
        
        public Double getTempo() {
            return tempo;
        }
        
        public void setTempo(Double tempo) {
            this.tempo = tempo;
        }
        
        public Double getTempoHora() {
            return tempoHora;
        }
        
        public void setTempoHora(Double tempoHora) {
            this.tempoHora = tempoHora;
        }
        
        public Double getCusto1() {
            return custo1;
        }
        
        public void setCusto1(Double custo1) {
            this.custo1 = custo1;
        }
        
        public Double getCusto2() {
            return custo2;
        }
        
        public void setCusto2(Double custo2) {
            this.custo2 = custo2;
        }
        
        public Double getCusto3() {
            return custo3;
        }
        
        public void setCusto3(Double custo3) {
            this.custo3 = custo3;
        }
        
        public String getDataInclusao() {
            return dataInclusao;
        }
        
        public void setDataInclusao(String dataInclusao) {
            this.dataInclusao = dataInclusao;
        }
        
        public String getDataAlteracao() {
            return dataAlteracao;
        }
        
        public void setDataAlteracao(String dataAlteracao) {
            this.dataAlteracao = dataAlteracao;
        }
        
        public OperacaoAPI[] getOperacoes() {
            return operacoes;
        }
        
        public void setOperacoes(OperacaoAPI[] operacoes) {
            this.operacoes = operacoes;
        }
        
    }
    //</editor-fold>
    
}