// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

package sysdeliz2.utils.apis.powerBi;

/**
 * Configuration class
 */
public abstract class Config {
	
	// Set this to true, to show debug statements in console
	public static final boolean DEBUG = true;
	
	//	Two possible Authentication methods: 
	//	- For authentication with master user credential choose MasterUser as AuthenticationType.
	//	- For authentication with app secret choose ServicePrincipal as AuthenticationType.
	//	More details here: https://aka.ms/EmbedServicePrincipal
	public static final String authenticationType = "MasterUser";
	
	//	Common configuration properties for both authentication types
	// Enter workspaceId / groupId
	public static final String workspaceId = "1f138511-03cd-4cf6-9544-ab232974414e";

	// The id of the report to embed.
	public static final String reportId = "4f11f4ab-bf21-4f83-87c9-abe25fc390d5";
	//6e2b5622-ef62-4267-98b3-2e54727951e2
	//4f11f4ab-bf21-4f83-87c9-abe25fc390d5

	// Enter Application Id / Client Id
	public static final String clientId = "66b3243c-bad8-4be7-994d-1aa0e1183bce";
	//66b3243c-bad8-4be7-994d-1aa0e1183bce
	//e9e1e853-6482-4b4c-8879-cc3a3aed61d0

	// Enter MasterUser credentials
	public static final String pbiUsername = "Luiz@sysdeliz.onmicrosoft.com";
	public static final String pbiPassword = "Sistemaxd1";

	// Enter ServicePrincipal credentials

	public static final String tenantId = "0dafa188-1eaa-4fb6-8549-9bfe13f60788";
	//0dafa188-1eaa-4fb6-8549-9bfe13f60788"

	public static final String appSecret = "eP11b9qK8HS0-peDL45arPH_W-p7~z6ryO";
	//eP11b9qK8HS0-peDL45arPH_W-p7~z6ryO
		
	//	DO NOT CHANGE
	public static final String authorityUrl = "https://login.microsoftonline.com/";
	public static final String scopeUrl = "https://analysis.windows.net/powerbi/api/.default";
	
	
	private Config(){
		//Private Constructor will prevent the instantiation of this class directly
		throw new IllegalStateException("Config class");
	}
	
}