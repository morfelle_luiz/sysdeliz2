package sysdeliz2.utils.apis.powerBi;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import sysdeliz2.utils.apis.powerBi.models.EmbedConfig;
import sysdeliz2.utils.apis.powerBi.models.EmbedToken;
import sysdeliz2.utils.apis.powerBi.services.AzureADService;
import sysdeliz2.utils.apis.powerBi.services.PowerBiService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

public class ServicoPowerBi {

    private static PowerBiService service;
    private static String initToken;
    private static final String url = "https://api.powerbi.com/v1.0/myorg/";
    private static OkHttpClient client;
    private static Retrofit retrofit;
    public static EmbedConfig reportEmbedConfig;

    public static ServicoPowerBi getInstance() {
        ServicoPowerBi servicoPowerBi = new ServicoPowerBi();
        getToken();
        return servicoPowerBi;
    }

    public static void getToken() {
        try {
            initToken = AzureADService.getAccessToken();
        } catch (MalformedURLException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static EmbedToken getEmbedToken() {

        retrofit2.Response<EmbedToken> response = null;

        String data = "{\n" +
                "  \"datasets\": [\n" +
                "    {\n" +
                "      \"id\": \"c5e9db57-ed8e-41fc-83cf-3d243e570de5\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"reports\": [\n" +
                "    {\n" +
                "      \"id\": \"4f11f4ab-bf21-4f83-87c9-abe25fc390d5\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        try {
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), data);
            response = service.postEmebedToken(body).execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.body();
    }


}
