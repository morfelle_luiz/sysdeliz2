// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

package sysdeliz2.utils.apis.powerBi.models;

import java.util.List;

/**
 * Properties for embedding the report 
 */
public class EmbedConfig {
	public List<ReportConfig> embedReports;

	public EmbedToken embedToken;

	public String errorMessage;
}
