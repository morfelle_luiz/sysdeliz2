// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

package sysdeliz2.utils.apis.powerBi.models;

/**
 * EmbedToken holds fields related to the embed token response
 */
public class EmbedToken {
	
	// token 
	public String token;
	
	// token id
	public String tokenId;

	// token expiration time
	public String expiration;
}
