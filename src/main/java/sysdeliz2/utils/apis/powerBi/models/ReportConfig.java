// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

package sysdeliz2.utils.apis.powerBi.models;

import org.json.simple.JSONObject;

/**
 * Properties for embedding the report 
 */
public class ReportConfig {

	public String reportId = "";

	public String embedUrl = "";

	public String reportName = "";

	public Boolean isEffectiveIdentityRolesRequired = false;

	public Boolean isEffectiveIdentityRequired = false;

	public Boolean enableRLS = false;

	public String username;

	public String roles;

	public JSONObject getJSONObject() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("reportId", reportId);
			jsonObj.put("embedUrl", embedUrl);
			jsonObj.put("reportName", reportName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

}
