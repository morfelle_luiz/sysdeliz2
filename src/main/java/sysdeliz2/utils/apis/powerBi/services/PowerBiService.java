package sysdeliz2.utils.apis.powerBi.services;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import sysdeliz2.models.view.VSdAtendCarteiraRep;
import sysdeliz2.utils.apis.powerBi.models.EmbedToken;
import sysdeliz2.utils.apis.powerBi.models.ReportConfig;

import java.util.List;

public interface PowerBiService {

    @Headers("Content-Type: application/json")
    @POST("datasets")
    Call<ResponseBody> postDataSets(@Body RequestBody body);

    @Headers("Content-Type: application/json")
    @POST("datasets")
    Call<VSdAtendCarteiraRep> postClientes(@Body List<VSdAtendCarteiraRep> clientes);

    @Headers("Content-Type: application/json")
    @POST("GenerateToken")
    Call<EmbedToken> postEmebedToken(@Body RequestBody body);

    @Headers("Content-Type: application/json")
    @GET("groups/{groupId}/reports/{reportId}")
    Call<ReportConfig> getReportConfig(@Path("groupId") String groupId, @Path("reportId") String reportId);
}
