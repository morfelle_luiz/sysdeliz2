package sysdeliz2.utils.apis.receitaws;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sysdeliz2.utils.apis.receitaws.models.ReceitaWS;
import sysdeliz2.utils.apis.receitaws.services.ServiceReceitaWS;

import java.util.Objects;

/**
 *
 * @author lima.joao
 * @since 25/07/2019 08:10
 */
public class ServicoReceitaWS {

    private static ServicoReceitaWS servicoReceitaWS;

    private static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    private static ServiceReceitaWS service;

    private int count = 0;
    private int position = 0;

    public static ServicoReceitaWS getInstance(){
        if(servicoReceitaWS == null){
            servicoReceitaWS = new ServicoReceitaWS();
        }

        // Configura o log http
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        //v1/cnpj/[cnpj]
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.receitaws.com.br/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(ServiceReceitaWS.class);

        return servicoReceitaWS;
    }

    @SuppressWarnings("unused")
    public ReceitaWS consultaCNPJ(String cnpj) throws Exception{
        Response<ReceitaWS> response = null;
        
        response = service.consultaCNPJ(cnpj).execute();
        return Objects.requireNonNull(response).body();
    }

    private ServicoReceitaWS(){}
}
