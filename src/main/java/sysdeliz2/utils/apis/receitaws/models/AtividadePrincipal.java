package sysdeliz2.utils.apis.receitaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
/**
 * @author lima.joao
 * @since 02/08/2019 10:12
 */
@SuppressWarnings("unused")
public class AtividadePrincipal implements Serializable {
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("code")
    @Expose
    private String code;

    private final static long serialVersionUID = -670111584585040952L;

    /**
     * No args constructor for use in serialization
     *
     */
    public AtividadePrincipal() {
    }

    /**
     *
     * @param text texto
     * @param code code
     */
    public AtividadePrincipal(String text, String code) {
        super();
        this.text = text;
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AtividadePrincipal withText(String text) {
        this.text = text;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AtividadePrincipal withCode(String code) {
        this.code = code;
        return this;
    }

}

/*

public class AtividadePrincipal{

}
 */