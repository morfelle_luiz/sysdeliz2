package sysdeliz2.utils.apis.receitaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/08/2019 10:14
 */
@SuppressWarnings("unused")
public class AtividadesSecundaria implements Serializable {
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("code")
    @Expose
    private String code;
    private final static long serialVersionUID = -8351682662817648705L;

    /**
     * No args constructor for use in serialization
     *
     */
    public AtividadesSecundaria() {
    }

    /**
     *
     * @param text text
     * @param code code
     */
    public AtividadesSecundaria(String text, String code) {
        super();
        this.text = text;
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AtividadesSecundaria withText(String text) {
        this.text = text;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AtividadesSecundaria withCode(String code) {
        this.code = code;
        return this;
    }
}
