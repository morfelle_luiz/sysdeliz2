package sysdeliz2.utils.apis.receitaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/08/2019 10:15
 */
@SuppressWarnings("unused")
public class Billing  implements Serializable {
    @SerializedName("free")
    @Expose
    private Boolean free;
    @SerializedName("database")
    @Expose
    private Boolean database;
    private final static long serialVersionUID = -1728255059569952969L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Billing() {
    }

    /**
     *
     * @param free livre
     * @param database bancodedados
     */
    public Billing(Boolean free, Boolean database) {
        super();
        this.free = free;
        this.database = database;
    }

    public Boolean getFree() {
        return free;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }

    public Billing withFree(Boolean free) {
        this.free = free;
        return this;
    }

    public Boolean getDatabase() {
        return database;
    }

    public void setDatabase(Boolean database) {
        this.database = database;
    }

    public Billing withDatabase(Boolean database) {
        this.database = database;
        return this;
    }

}
