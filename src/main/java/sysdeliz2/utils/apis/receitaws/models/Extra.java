package sysdeliz2.utils.apis.receitaws.models;

import java.io.Serializable;

/**
 * Segundo documentação do webservice, esta classe foi reservada para o futuro
 *
 * @author lima.joao
 * @since 02/08/2019 10:16
 */
@SuppressWarnings("unused")
public class Extra implements Serializable {
        private final static long serialVersionUID = 2777339541122963476L;
}
