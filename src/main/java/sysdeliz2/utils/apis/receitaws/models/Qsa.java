package sysdeliz2.utils.apis.receitaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author lima.joao
 * @since 02/08/2019 10:18
 */
@SuppressWarnings("unused")
public class Qsa implements Serializable {
    @SerializedName("qual")
    @Expose
    private String qual;
    @SerializedName("nome")
    @Expose
    private String nome;
    private final static long serialVersionUID = 7587526243810960848L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Qsa() {
    }

    /**
     *
     * @param nome nome
     * @param qual qual
     */
    public Qsa(String qual, String nome) {
        super();
        this.qual = qual;
        this.nome = nome;
    }

    public String getQual() {
        return qual;
    }

    public void setQual(String qual) {
        this.qual = qual;
    }

    public Qsa withQual(String qual) {
        this.qual = qual;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Qsa withNome(String nome) {
        this.nome = nome;
        return this;
    }

}
