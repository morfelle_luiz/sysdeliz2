package sysdeliz2.utils.apis.receitaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author lima.joao
 * @since 02/08/2019 10:20
 */
@SuppressWarnings("unused")
public class ReceitaWS implements Serializable {

    @SerializedName("atividade_principal")
    @Expose
    private List<AtividadePrincipal> atividadePrincipal = null;
    @SerializedName("data_situacao")
    @Expose
    private String dataSituacao;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("uf")
    @Expose
    private String uf;
    @SerializedName("telefone")
    @Expose
    private String telefone;
    @SerializedName("atividades_secundarias")
    @Expose
    private List<AtividadesSecundaria> atividadesSecundarias = null;
    @SerializedName("qsa")
    @Expose
    private List<Qsa> qsa = null;
    @SerializedName("situacao")
    @Expose
    private String situacao;
    @SerializedName("bairro")
    @Expose
    private String bairro;
    @SerializedName("logradouro")
    @Expose
    private String logradouro;
    @SerializedName("numero")
    @Expose
    private String numero;
    @SerializedName("cep")
    @Expose
    private String cep;
    @SerializedName("municipio")
    @Expose
    private String municipio;
    @SerializedName("porte")
    @Expose
    private String porte;
    @SerializedName("abertura")
    @Expose
    private String abertura;
    @SerializedName("natureza_juridica")
    @Expose
    private String naturezaJuridica;
    @SerializedName("fantasia")
    @Expose
    private String fantasia;
    @SerializedName("cnpj")
    @Expose
    private String cnpj;
    @SerializedName("ultima_atualizacao")
    @Expose
    private String ultimaAtualizacao;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("complemento")
    @Expose
    private String complemento;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("efr")
    @Expose
    private String efr;
    @SerializedName("motivo_situacao")
    @Expose
    private String motivoSituacao;
    @SerializedName("situacao_especial")
    @Expose
    private String situacaoEspecial;
    @SerializedName("data_situacao_especial")
    @Expose
    private String dataSituacaoEspecial;
    @SerializedName("capital_social")
    @Expose
    private String capitalSocial;
    @SerializedName("extra")
    @Expose
    private Extra extra;
    @SerializedName("billing")
    @Expose
    private Billing billing;
    private final static long serialVersionUID = 916268525855199647L;

    /**
     * No args constructor for use in serialization
     *
     */
    public ReceitaWS() {
    }

    /**
     *
     * @param dataSituacaoEspecial data
     * @param motivoSituacao motivo
     * @param cnpj cnpj
     * @param ultimaAtualizacao ultima
     * @param cep cep
     * @param capitalSocial capital
     * @param efr efr
     * @param situacao situacao
     * @param complemento complemento
     * @param uf uf
     * @param porte porte
     * @param status status
     * @param extra extra
     * @param atividadePrincipal atividadePrincipal
     * @param naturezaJuridica naturezaJuridica
     * @param fantasia fantasia
     * @param qsa qsa
     * @param numero numero
     * @param billing billing
     * @param bairro bairro
     * @param municipio municipio
     * @param email email
     * @param telefone telefone
     * @param tipo tipo
     * @param logradouro logradouro
     * @param dataSituacao dataSituacao
     * @param nome nome
     * @param atividadesSecundarias atividadesSecundarias
     * @param abertura abertura
     * @param situacaoEspecial situacaoEspecial
     */
    public ReceitaWS(List<AtividadePrincipal> atividadePrincipal, String dataSituacao, String nome, String uf, String telefone, List<AtividadesSecundaria> atividadesSecundarias, List<Qsa> qsa, String situacao, String bairro, String logradouro, String numero, String cep, String municipio, String porte, String abertura, String naturezaJuridica, String fantasia, String cnpj, String ultimaAtualizacao, String status, String tipo, String complemento, String email, String efr, String motivoSituacao, String situacaoEspecial, String dataSituacaoEspecial, String capitalSocial, Extra extra, Billing billing) {
        super();
        this.atividadePrincipal = atividadePrincipal;
        this.dataSituacao = dataSituacao;
        this.nome = nome;
        this.uf = uf;
        this.telefone = telefone;
        this.atividadesSecundarias = atividadesSecundarias;
        this.qsa = qsa;
        this.situacao = situacao;
        this.bairro = bairro;
        this.logradouro = logradouro;
        this.numero = numero;
        this.cep = cep;
        this.municipio = municipio;
        this.porte = porte;
        this.abertura = abertura;
        this.naturezaJuridica = naturezaJuridica;
        this.fantasia = fantasia;
        this.cnpj = cnpj;
        this.ultimaAtualizacao = ultimaAtualizacao;
        this.status = status;
        this.tipo = tipo;
        this.complemento = complemento;
        this.email = email;
        this.efr = efr;
        this.motivoSituacao = motivoSituacao;
        this.situacaoEspecial = situacaoEspecial;
        this.dataSituacaoEspecial = dataSituacaoEspecial;
        this.capitalSocial = capitalSocial;
        this.extra = extra;
        this.billing = billing;
    }

    public List<AtividadePrincipal> getAtividadePrincipal() {
        return atividadePrincipal;
    }

    public void setAtividadePrincipal(List<AtividadePrincipal> atividadePrincipal) {
        this.atividadePrincipal = atividadePrincipal;
    }

    public ReceitaWS withAtividadePrincipal(List<AtividadePrincipal> atividadePrincipal) {
        this.atividadePrincipal = atividadePrincipal;
        return this;
    }

    public String getDataSituacao() {
        return dataSituacao;
    }

    public void setDataSituacao(String dataSituacao) {
        this.dataSituacao = dataSituacao;
    }

    public ReceitaWS withDataSituacao(String dataSituacao) {
        this.dataSituacao = dataSituacao;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ReceitaWS withNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public ReceitaWS withUf(String uf) {
        this.uf = uf;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public ReceitaWS withTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public List<AtividadesSecundaria> getAtividadesSecundarias() {
        return atividadesSecundarias;
    }

    public void setAtividadesSecundarias(List<AtividadesSecundaria> atividadesSecundarias) {
        this.atividadesSecundarias = atividadesSecundarias;
    }

    public ReceitaWS withAtividadesSecundarias(List<AtividadesSecundaria> atividadesSecundarias) {
        this.atividadesSecundarias = atividadesSecundarias;
        return this;
    }

    public List<Qsa> getQsa() {
        return qsa;
    }

    public void setQsa(List<Qsa> qsa) {
        this.qsa = qsa;
    }

    public ReceitaWS withQsa(List<Qsa> qsa) {
        this.qsa = qsa;
        return this;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public ReceitaWS withSituacao(String situacao) {
        this.situacao = situacao;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public ReceitaWS withBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public ReceitaWS withLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public ReceitaWS withNumero(String numero) {
        this.numero = numero;
        return this;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public ReceitaWS withCep(String cep) {
        this.cep = cep;
        return this;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public ReceitaWS withMunicipio(String municipio) {
        this.municipio = municipio;
        return this;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public ReceitaWS withPorte(String porte) {
        this.porte = porte;
        return this;
    }

    public String getAbertura() {
        return abertura;
    }

    public void setAbertura(String abertura) {
        this.abertura = abertura;
    }

    public ReceitaWS withAbertura(String abertura) {
        this.abertura = abertura;
        return this;
    }

    public String getNaturezaJuridica() {
        return naturezaJuridica;
    }

    public void setNaturezaJuridica(String naturezaJuridica) {
        this.naturezaJuridica = naturezaJuridica;
    }

    public ReceitaWS withNaturezaJuridica(String naturezaJuridica) {
        this.naturezaJuridica = naturezaJuridica;
        return this;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public ReceitaWS withFantasia(String fantasia) {
        this.fantasia = fantasia;
        return this;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public ReceitaWS withCnpj(String cnpj) {
        this.cnpj = cnpj;
        return this;
    }

    public String getUltimaAtualizacao() {
        return ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(String ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    public ReceitaWS withUltimaAtualizacao(String ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ReceitaWS withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ReceitaWS withTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public ReceitaWS withComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ReceitaWS withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getEfr() {
        return efr;
    }

    public void setEfr(String efr) {
        this.efr = efr;
    }

    public ReceitaWS withEfr(String efr) {
        this.efr = efr;
        return this;
    }

    public String getMotivoSituacao() {
        return motivoSituacao;
    }

    public void setMotivoSituacao(String motivoSituacao) {
        this.motivoSituacao = motivoSituacao;
    }

    public ReceitaWS withMotivoSituacao(String motivoSituacao) {
        this.motivoSituacao = motivoSituacao;
        return this;
    }

    public String getSituacaoEspecial() {
        return situacaoEspecial;
    }

    public void setSituacaoEspecial(String situacaoEspecial) {
        this.situacaoEspecial = situacaoEspecial;
    }

    public ReceitaWS withSituacaoEspecial(String situacaoEspecial) {
        this.situacaoEspecial = situacaoEspecial;
        return this;
    }

    public String getDataSituacaoEspecial() {
        return dataSituacaoEspecial;
    }

    public void setDataSituacaoEspecial(String dataSituacaoEspecial) {
        this.dataSituacaoEspecial = dataSituacaoEspecial;
    }

    public ReceitaWS withDataSituacaoEspecial(String dataSituacaoEspecial) {
        this.dataSituacaoEspecial = dataSituacaoEspecial;
        return this;
    }

    public String getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public ReceitaWS withCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
        return this;
    }

    public Extra getExtra() {
        return extra;
    }

    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    public ReceitaWS withExtra(Extra extra) {
        this.extra = extra;
        return this;
    }

    public Billing getBilling() {
        return billing;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public ReceitaWS withBilling(Billing billing) {
        this.billing = billing;
        return this;
    }

}
