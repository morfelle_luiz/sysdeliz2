package sysdeliz2.utils.apis.receitaws.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import sysdeliz2.utils.apis.receitaws.models.ReceitaWS;

/**
 * @author lima.joao
 * @since 24/07/2019 15:47
 */
public interface ServiceReceitaWS {
    @GET("v1/cnpj/{cnpj}")
    Call<ReceitaWS> consultaCNPJ(@Path("cnpj") String cnpj);

}