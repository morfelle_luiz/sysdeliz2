package sysdeliz2.utils.apis.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.poi.ss.formula.ptg.ScalarConstantPtg;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.util.concurrent.TimeUnit;

public final class RetrofitInitializer<T> {
    
    Retrofit retrofit;
    private String baseUrl;
    private Boolean addConverter = true;
    private static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    private Class<T> classType;
    
    public RetrofitInitializer(Class<T> classType, String baseUrl) {
        this.baseUrl = baseUrl;
        this.classType = classType;
        init();
    }
    
    public RetrofitInitializer(Class<T> classType, String baseUrl, Boolean addConverter) {
        this.baseUrl = baseUrl;
        this.addConverter = addConverter;
        this.classType = classType;
        init();
    }
    
    private void init() {
        // Configura o log http
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .build();
        
        if(addConverter){
            this.retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        } else {
            this.retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient)
                    .build();
        }
    }
    
    public T get() {
        return retrofit.create(classType);
    }
}