package sysdeliz.apis.web.retrofit.extensions

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sysdeliz.apis.web.retrofit.utils.GenericResponse

/**
 * Calbacks usados para requisições assyncronas
 *
 * @author lima.joao
 * @since 11/12/2019 11:00
 */
fun <T> buildListCallback(genericResponse: GenericResponse<T>):Callback<List<T>>{
    return object : Callback<List<T>>{
        override fun onFailure(call: Call<List<T>>, t: Throwable) {
            genericResponse.fail(t.message?:"Erro não definido")
        }

        override fun onResponse(call: Call<List<T>>, response: Response<List<T>>) {
            response.body()?.let {
                val resposta : List<T> = it
                genericResponse.success(resposta)
            }
        }
    }
}

fun <T> buildSingleCallback(genericResponse: GenericResponse<T>):Callback<T>{
    return object : Callback<T>{
        override fun onFailure(call: Call<T>, t: Throwable) {
            genericResponse.fail(t.message?:"Erro não definido")
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            response.body()?.let {
                val resposta : T = it
                genericResponse.success(resposta)
            }
        }
    }
}
