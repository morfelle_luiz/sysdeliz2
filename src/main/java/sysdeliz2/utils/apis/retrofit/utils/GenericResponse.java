package sysdeliz2.utils.apis.retrofit.utils;

import java.util.List;

public interface GenericResponse<T> {
    
    void success(List<T> response);
    void success(T response);
    void fail(String log);
    
}
