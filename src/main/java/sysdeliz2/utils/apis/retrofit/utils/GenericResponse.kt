package sysdeliz.apis.web.retrofit.utils

/**
 *
 * @author lima.joao
 * @since 11/12/2019 11:01
 */
interface GenericResponse<T>{
    fun success(response: List<T>)
    fun success(response: T){}
    fun fail(log: String)
}