package sysdeliz2.utils.apis.retrofit.utils;

public class Resposta {
    
    private boolean situacao;
    private String mensagem;
    
    public Resposta() {
    }
    
    public boolean isSituacao() {
        return situacao;
    }
    
    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }
    
    public String getMensagem() {
        return mensagem;
    }
    
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
