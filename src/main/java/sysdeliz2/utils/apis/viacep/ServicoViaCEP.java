package sysdeliz2.utils.apis.viacep;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sysdeliz2.utils.apis.viacep.models.CEP;
import sysdeliz2.utils.apis.viacep.services.ServiceViaCEP;

import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author lima.joao
 * @since 25/07/2019 08:10
 */
public class ServicoViaCEP {

    private static ServicoViaCEP servicoViaCEP;

    private static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    private static ServiceViaCEP service;

    public static ServicoViaCEP getInstance(){
        if(servicoViaCEP == null){
            servicoViaCEP = new ServicoViaCEP();
        }

        // Configura o log http
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        //v1/cnpj/[cnpj]
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://viacep.com.br/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(ServiceViaCEP.class);

        return servicoViaCEP;
    }

    @SuppressWarnings("unused")
    public CEP consultaCEP(String cep){
        Response<CEP> response = null;
        try {
            response = service.consultaCEP(cep).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Objects.requireNonNull(response).body();
    }

    private ServicoViaCEP(){}
}
