package sysdeliz2.utils.apis.viacep.models;

/**
 * @author lima.joao
 * @since 02/08/2019 14:25
 */
public class CEP {
    // pripriedades do CEP
    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String localidade;
    private String uf;
    private String ibge;
    private String gia;

    /**
     * Cria um novo CEP vazio
     */
    public CEP() {
        this.logradouro = null;
        this.complemento = null;
        this.bairro = null;
        this.localidade = null;
        this.uf = null;
        this.ibge = null;
        this.gia = null;
    }

    /**
     * Cria um novo CEP completo
     * @param cep cep
     * @param logradouro Logradouro
     * @param complemento Complemento
     * @param bairro Bairro
     * @param localidade Localidade
     * @param uf Uf
     * @param ibge Ibge
     * @param gia Gia
     */
    public CEP(String cep, String logradouro, String complemento, String bairro, String localidade, String uf, String ibge, String gia) {
        this.cep = cep;
        this.logradouro = logradouro;
        this.complemento = complemento;
        this.bairro = bairro;
        this.localidade = localidade;
        this.uf = uf;
        this.ibge = ibge;
        this.gia = gia;
    }

    /**
     * Cria um novo CEP parcial
     * @param logradouro Logradouro
     * @param localidade Localidade
     * @param uf Uf
     */
    public CEP(String logradouro, String localidade, String uf) {
        this.logradouro = logradouro;
        this.localidade = localidade;
        this.uf = uf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getIbge() {
        return ibge;
    }

    public void setIbge(String ibge) {
        this.ibge = ibge;
    }

    public String getGia() {
        return gia;
    }

    public void setGia(String gia) {
        this.gia = gia;
    }
}
