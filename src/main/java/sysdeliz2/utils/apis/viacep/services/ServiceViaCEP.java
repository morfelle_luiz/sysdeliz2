package sysdeliz2.utils.apis.viacep.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import sysdeliz2.utils.apis.viacep.models.CEP;

/**
 * @author lima.joao
 * @since 24/07/2019 15:47
 */
public interface ServiceViaCEP {
    @GET("ws/{cep}/json/")
    Call<CEP> consultaCEP(@Path("cep") String cep);
}