package sysdeliz2.utils.converters;

import javafx.util.StringConverter;
import sysdeliz2.utils.sys.StringUtils;

import java.math.BigDecimal;

public class BigDecimalAttributeConverter extends StringConverter<BigDecimal>  {
    
        /** {@inheritDoc} */
        @Override public BigDecimal fromString(String value) {
            // If the specified value is null or zero-length, return null
            if (value == null) {
                return null;
            }
            
            value = value.trim();
            
            if (value.length() < 1) {
                return null;
            }
            
            return new BigDecimal(value.replace(".","").replace(",","."));
        }
        
        /** {@inheritDoc} */
        @Override public String toString(BigDecimal value) {
            // If the specified value is null, return a zero-length String
            if (value == null) {
                return "";
            }
            
            return StringUtils.toDecimalFormat((BigDecimal)value);
        }
}
