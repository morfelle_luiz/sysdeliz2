package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class BooleanAttributeXConverter implements AttributeConverter<Boolean, Character> {
    
    @Override
    public Character convertToDatabaseColumn(Boolean attribute) {
        if (attribute != null) {
            if (attribute) {
                return 'X';
            } else {
                return null;
            }
            
        }
        return null;
    }
    
    @Override
    public Boolean convertToEntityAttribute(Character dbData) {
        if (dbData == null) {
            return false;
        }else{
            return dbData.equals('X');
        }
    }
}
