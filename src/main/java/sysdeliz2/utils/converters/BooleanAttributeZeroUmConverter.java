package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class BooleanAttributeZeroUmConverter implements AttributeConverter<Boolean, Character> {
    
    @Override
    public Character convertToDatabaseColumn(Boolean attribute) {
        if (attribute != null) {
            if (attribute) {
                return '1';
            } else {
                return '0';
            }
            
        }
        return null;
    }
    
    @Override
    public Boolean convertToEntityAttribute(Character dbData) {
        if (dbData == null) {
            return false;
        }else{
            return dbData.equals('1');
        }
    }
}
