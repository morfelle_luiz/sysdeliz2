package sysdeliz2.utils.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.apache.xpath.operations.Bool;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BooleanZeroEUmAttributeDeserializer extends StdDeserializer<Boolean> {

    public BooleanZeroEUmAttributeDeserializer() {
        this(null);
    }

    public BooleanZeroEUmAttributeDeserializer(Class<LocalDate> t) {
        super(t);
    }

    @Override
    public Boolean deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return p.getText().equals("1");
    }
}