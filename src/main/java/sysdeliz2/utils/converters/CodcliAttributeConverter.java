package sysdeliz2.utils.converters;

import sysdeliz2.utils.sys.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CodcliAttributeConverter implements AttributeConverter<Integer, String> {
    
    @Override
    public String convertToDatabaseColumn(Integer attribute) {
        if (attribute != null) {
            return StringUtils.lpad(String.valueOf(attribute),5,"0");
//            return String.format("%05d", attribute);
        }
        return null;
    }
    
    @Override
    public Integer convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return Integer.valueOf(dbData);
        }
        return 0;
    }
}
