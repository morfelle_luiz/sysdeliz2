package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class IntegerAttributeConverter implements AttributeConverter<Integer, String> {


    @Override
    public String convertToDatabaseColumn(Integer attribute) {
        return attribute == null ? null : Integer.toString(attribute);
    }

    @Override
    public Integer convertToEntityAttribute(String dbData) {
        try {
            return Integer.parseInt(dbData == null ? "0" : dbData);
        } catch (Exception e) {
            throw new IllegalStateException("Invalid number: " + dbData);
        }
    }
}
