package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDateTime attribute) {
        return (attribute == null ? null : java.util.Date.from(attribute.atZone(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date dbData) {
        return (dbData == null ? null : dbData.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }
}