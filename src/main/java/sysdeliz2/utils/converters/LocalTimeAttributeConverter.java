package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

@Converter(autoApply = true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Date> {
    @Override
    public Date convertToDatabaseColumn(LocalTime attribute) {
        return (attribute == null ? null : java.util.Date.from(LocalDateTime.of(LocalDate.of(1900, 1, 1) ,attribute).atZone(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public LocalTime convertToEntityAttribute(Date dbTime) {
        return (dbTime == null ? null : dbTime.toInstant().atZone(ZoneId.systemDefault()).toLocalTime());
    }
}