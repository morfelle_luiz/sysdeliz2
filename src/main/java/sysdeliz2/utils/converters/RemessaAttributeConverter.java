package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RemessaAttributeConverter implements AttributeConverter<Integer, String> {
    
    @Override
    public String convertToDatabaseColumn(Integer attribute) {
        if (attribute != null) {
            return String.valueOf(attribute);
        }
        return null;
    }
    
    @Override
    public Integer convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return Integer.valueOf(dbData);
        }
        return 0;
    }
}
