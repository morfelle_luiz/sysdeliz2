package sysdeliz2.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatusReservaAttributeConverter implements AttributeConverter<String, Character> {
    
    @Override
    public Character convertToDatabaseColumn(String attribute) {
        switch(attribute) {
            case "Liberada":
                return 'L';
            case "Manual":
                return 'M';
            case "Não Atende Valor":
                return 'A';
            case "Bloqueado":
                return 'B';
            case "Bloqueado (Valor)":
                return 'C';
            case "Próximas Entregas Liberadas":
                return 'P';
            case "Próximas Entregas (Valor)":
                return 'R';
        }
        return null;
    }
    
    @Override
    public String convertToEntityAttribute(Character dbData) {
        switch(dbData) {
            case 'L':
                return "Liberada";
            case 'M':
                return "Manual";
            case 'A':
                return "Não Atende Valor";
            case 'B':
                return "Bloqueado";
            case 'C':
                return "Bloqueado (Valor)";
            case 'P':
                return "Próximas Entregas Liberadas";
            case 'R':
                return "Próximas Entregas (Valor)";
        }
        return null;
    }
}
