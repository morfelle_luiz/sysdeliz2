package sysdeliz2.utils.converters;

import sysdeliz2.models.sysdeliz.SdTela;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TipoTelaAttributeConverter implements AttributeConverter<SdTela.TipoTela, String> {
    @Override
    public String convertToDatabaseColumn(SdTela.TipoTela attribute) {
        if (attribute == null)
            return null;

        switch (attribute) {
            case JV:
                return "JV";

            case FX:
                return "FX";

            case KT:
                return "KT";

            default:
                throw new IllegalArgumentException(attribute + " not supported.");
        }
    }

    @Override
    public SdTela.TipoTela convertToEntityAttribute(String dbData) {
        if (dbData == null)
            return null;

        switch (dbData) {
            case "JV":
                return SdTela.TipoTela.JV;

            case "KT":
                return SdTela.TipoTela.KT;

            case "FX":
                return SdTela.TipoTela.FX;

            default:
                throw new IllegalArgumentException(dbData + " not supported.");
        }
    }
}