package sysdeliz2.utils.enums;

import java.util.regex.Pattern;

public enum CardType {
    UNKNOWN,
    elo("^(4011(78|79)\\d{10}|43(1274|8935)\\d{10}|45(1416|7393|763(1|2))\\d{10}|50(4175|6699|67[0-7][0-9]|9000)\\d{10}|50(9[0-9][0-9][0-9])\\d{10}|627780\\d{10}|63(6297|6368)\\d{10}|650(03([^4])|04([0-9])|05(0|1)|05([7-9])|06([0-9])|07([0-9])|08([0-9])|4([0-3][0-9]|8[5-9]|9[0-9])|5([0-9][0-9]|3[0-8])|9([0-6][0-9]|7[0-8])|7([0-2][0-9])|541|700|720|727|901)\\d{10}|65165([2-9])\\d{10}|6516([6-7][0-9])\\d{10}|65500([0-9])\\d{10}|6550([0-5][0-9])\\d{10}|655021\\d{10}|65505([6-7])\\d{10}|6516([8-9][0-9])\\d{10}|65170([0-4])\\d{10})$"),
    hipercard("^(606282\\d{10}(\\d{3})?)|(3841\\d{15})$"),
    visa("^4[0-9]{12}(?:[0-9]{3}){0,2}$"),
    mastercard("^(?:5[1-5]|2(?!2([01]|20)|7(2[1-9]|3))[2-7])\\d{14}$"),
    amex("^3[47][0-9]{13}$");

    private Pattern pattern;

    CardType() {
        this.pattern = null;
    }

    CardType(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    public static CardType detect(String cardNumber) {

        for (CardType cardType : CardType.values()) {
            if (null == cardType.pattern) {
                continue;
            }
            if (cardType.pattern.matcher(cardNumber).matches()) {
                return cardType;
            }
        }

        return UNKNOWN;
    }

}