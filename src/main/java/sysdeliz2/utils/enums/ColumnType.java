package sysdeliz2.utils.enums;

/**
 * @author lima.joao
 * @since 22/08/2019 10:07
 */
public enum ColumnType {
    TEXT,
    BOOLEAN,
    DECIMAL,
    NUMBER,
    MONETARY,
    PERCENT,
    DATE,
    DATETIME,
    TIME;
}
