package sysdeliz2.utils.enums;

/**
 * @author lima.joao
 * @since 22/08/2019 10:07
 */
public enum PredicateType {
    EQUAL,
    NOT_EQUAL,
    LIKE,
    NOT_LIKE,
    IS_NULL,
    IS_NOT_NULL,
    IN,
    NOT_IN,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN,
    LESS_THAN_OR_EQUAL,
    BEGIN,
    NO_BEGIN,
    END,
    NOT_END;
}