package sysdeliz2.utils.enums;

public enum ResultTypeFilter {
    SINGLE_RESULT, MULTIPLE_RESULT
}
