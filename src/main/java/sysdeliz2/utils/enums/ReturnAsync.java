package sysdeliz2.utils.enums;

public enum ReturnAsync {
    OK(1), EXCEPTION(0), ERROR(2), NOT_FOUND(3), CONNECTION_ERROR(4);
    
    public Integer value;
    
    ReturnAsync(Integer value) {
        this.value = value;
    }
    
    public Integer value() {
        return value;
    }

}
