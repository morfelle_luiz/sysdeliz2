package sysdeliz2.utils.enums;

public enum StatusProgramacao {
    LANCADO, PROGRAMADO, LIVRE, QUEBRADA, POLIVALENTE, AGRUPADA, FINALIZADO;
}
