package sysdeliz2.utils.enums;

public enum Style {
    DANGER("danger"), AMBER("amber"), WARNING("warning"), PRIMARY("primary"), INFO("info"),
    DEFAULT("default"), SECUNDARY("secundary"), DARK("dark"), SUCCESS("success"), MOSTRUARIO("mostruario");
    
    public String styleClass;
    
    Style(String value) {
        styleClass = value;
    }
    
    public Style size(String value){
        styleClass = value;
        return this;
    }
    
    public String getValue() {
        return styleClass;
    }
}
