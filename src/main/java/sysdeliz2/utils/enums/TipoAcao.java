package sysdeliz2.utils.enums;

/**
 * @author lima.joao
 * @since 22/08/2019 10:07
 */
public enum TipoAcao {
    CADASTRAR("CADASTRAR"), EXCLUIR("EXCLUIR"), MOVIMENTAR("MOVIMENTAR"), ENVIAR("ENVIAR"), EDITAR("EDITAR"), IMPRIMIR("IMPRIMIR"), ENTRAR("ENTRAR"), SALVAR("SALVAR"), REVISAR("REVISAR");

    public String tipoAcao;

    TipoAcao(String value) {
        tipoAcao = value;
    }

    public String getValor() {
        return tipoAcao;
    }
}
