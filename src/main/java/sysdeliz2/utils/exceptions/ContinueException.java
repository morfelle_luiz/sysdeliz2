package sysdeliz2.utils.exceptions;

public class ContinueException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ContinueException() {
    }

    public ContinueException(String message) {
        super(message);
    }

    public ContinueException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContinueException(Throwable cause) {
        super(cause);
    }

    public ContinueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
