/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.exceptions;

/**
 *
 * @author cristiano.diego
 */
public class EntidadeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EntidadeException() {
    }

    public EntidadeException(String message) {
        super(message);
    }

    public EntidadeException(Throwable cause) {
        super(cause);
    }

    public EntidadeException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntidadeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
