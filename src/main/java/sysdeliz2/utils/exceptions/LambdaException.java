package sysdeliz2.utils.exceptions;

public class LambdaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LambdaException() {
    }

    public LambdaException(String message) {
        super(message);
    }

    public LambdaException(String message, Throwable cause) {
        super(message, cause);
    }

    public LambdaException(Throwable cause) {
        super(cause);
    }

    public LambdaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
