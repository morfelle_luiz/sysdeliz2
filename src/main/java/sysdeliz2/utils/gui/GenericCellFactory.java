/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.gui;

import javafx.event.EventHandler;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;


/**
 *
 * @author lima.joao
 */
public class GenericCellFactory<T, E> extends TableCell<T, E>{
    
    private TextField textField;
    
    private final ICellFactoryListener<T, E> listener;
   
    public GenericCellFactory(){
        listener = null;
    }

    private GenericCellFactory(ICellFactoryListener listener){
        this.listener = listener;
    }
    
    @Override
    public void updateSelected(boolean selected) {
        super.updateSelected(selected); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void updateItem(E item, boolean empty) {
        super.updateItem(item, empty);
        
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setGraphic(textField);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                setText(getString());
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }
      
    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(String.valueOf(getItem()));
        setContentDisplay(ContentDisplay.TEXT_ONLY);

        if(listener != null){
            this.getTableColumn().setOnEditCommit(listener::onCancelEdit);
        }        
    }

    @Override
    public void commitEdit(E newValue) {
        if(listener != null){
            this.getTableColumn().setOnEditCommit(listener::onCommitEdit);
        }

        super.commitEdit(newValue);
    }

    @Override
    public void startEdit() {
        super.startEdit(); //To change body of generated methods, choose Tools | Templates.

        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
        textField.selectAll();

        if(listener != null){
            this.getTableColumn().setOnEditStart(listener::onStartEdit);
        }
    }
    
    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit( (E) textField.getText() );
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });
    }
    
    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
    
    public Callback<TableColumn<T, E>, TableCell<T, E>> buildCallback(){
        return this.buildCallback(null);
//        return new Callback<TableColumn<T, E>, TableCell<T, E>>(){
//            @Override
//            public TableCell call(TableColumn p){
//                return new GenericCellFactory();
//            }
//        };
    }
        
    public Callback<TableColumn<T, E>, TableCell<T, E>> buildCallback(ICellFactoryListener listener){
        return new Callback<TableColumn<T, E>, TableCell<T, E>>(){
            @Override
            public TableCell call(TableColumn p){
                return new GenericCellFactory(listener);
            }
        };
    }    
}