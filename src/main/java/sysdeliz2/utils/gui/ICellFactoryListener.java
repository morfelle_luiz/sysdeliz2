/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.gui;

import javafx.scene.control.TableColumn;

/**
 *
 * @author lima.joao
 */
public interface ICellFactoryListener<T, E> {
    
    //public void onUpdateSelected(TableColumn.CellEditEvent<T, S> event);
    
    //public void onUpdateItem(TableColumn.CellEditEvent<T, S> event);
    
    void onCancelEdit(TableColumn.CellEditEvent<T, E> event);
    
    void onCommitEdit(TableColumn.CellEditEvent<T, E> event);
    
    void onStartEdit(TableColumn.CellEditEvent<T, E> event);
    
}
