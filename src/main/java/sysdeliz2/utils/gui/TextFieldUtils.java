/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.gui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static javafx.scene.input.KeyCode.*;

/**
 * @author cristiano.diego
 */
public class TextFieldUtils {

    private static List<KeyCode> ignoreKeyCodes;

    static {
        ignoreKeyCodes = new ArrayList<>();
        Collections.addAll(ignoreKeyCodes, new KeyCode[]{F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12});
    }

    public static void ignoreKeys(final TextField textField) {
        textField.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (ignoreKeyCodes.contains(keyEvent.getCode())) {
                    keyEvent.consume();
                }
            }
        });
    }

    public static void inSql(TextField field) {
        field.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!field.getText().isEmpty()) {
                field.setText(field.getText().replace("'", ""));
                if (!newValue) {
                    field.setText("'" + field.getText().replace(",", "','") + "'");
                }
            }
        });
    }

    public static void upperCase(TextField field) {
        field.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
    }

    public static void upperCase(TextArea field) {
        field.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
    }

    public static void lowerCase(TextField field) {
        field.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toLowerCase());
            return change;
        }));
    }

    public static void integerNumberMask(TextField textField) {

        textField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("\\d*")) {
                textField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

    }

    public static void numberMask(TextField textField) {

        textField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            newValue = newValue.replaceAll(",", ".");
            if (newValue.length() > 0) {
                try {
                    Double.parseDouble(newValue);
                    textField.setText(newValue.replaceAll(",", "."));
                } catch (Exception e) {
                    textField.setText(oldValue);
                }
            }
        });
    }

    public static void integerMask(TextField textField) {

        textField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null) {
                if (!newValue.matches("\\d*")) {
                    textField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

    }

    public static void decimalMask(TextField textField) {

        textField.setAlignment(Pos.CENTER_RIGHT);
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null)
                    if (!newValue.matches("\\d{0,7}([\\,]\\d{0,2})?")) {
                        if (textField != null)
                            textField.setText(oldValue);
                    }
            }
        });

    }

    public static void cepMask(TextField textField) {

        String val = "";

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando

                if (textField.getText().length() == 6) {
                    textField.setText(textField.getText().substring(0, 5));
                    textField.positionCaret(textField.getText().length());
                }

            } else { // escrevendo

                if (textField.getText().length() == 9) {
                    event.consume();
                }

                if (textField.getText().length() == 5) {
                    textField.setText(textField.getText() + "-");
                    textField.positionCaret(textField.getText().length());
                }

            }
        });

        textField.setOnKeyReleased((KeyEvent evt) -> {

            if (!textField.getText().matches("\\d-*")) {
                textField.setText(textField.getText().replaceAll("[^\\d-]", ""));
                textField.positionCaret(textField.getText().length());
            }
        });

    }

    public static void dateMask(TextField textField) {

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando

                if (textField.getText().length() == 3) {
                    textField.setText(textField.getText().substring(0, 2));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 6) {
                    textField.setText(textField.getText().substring(0, 5));
                    textField.positionCaret(textField.getText().length());
                }

            } else { // escrevendo

                if (textField.getText().length() == 10) {
                    event.consume();
                }

                if (textField.getText().length() == 2) {
                    textField.setText(textField.getText() + "/");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 5) {
                    textField.setText(textField.getText() + "/");
                    textField.positionCaret(textField.getText().length());
                }

            }
        });

        textField.setOnKeyReleased((KeyEvent evt) -> {

            if (!textField.getText().matches("\\d/*")) {
                textField.setText(textField.getText().replaceAll("[^\\d/]", ""));
                textField.positionCaret(textField.getText().length());
            }
        });

    }

    public static void dateMask(DatePicker datePicker) {

        datePicker.getEditor().setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando
                if (datePicker.getEditor().getText().length() == 3) {
                    datePicker.getEditor().setText(datePicker.getEditor().getText().substring(0, 2));
                    datePicker.getEditor().positionCaret(datePicker.getEditor().getText().length());
                }
                if (datePicker.getEditor().getText().length() == 6) {
                    datePicker.getEditor().setText(datePicker.getEditor().getText().substring(0, 5));
                    datePicker.getEditor().positionCaret(datePicker.getEditor().getText().length());
                }

            } else { // escrevendo

                if (datePicker.getEditor().getText().length() == 10) {
                    event.consume();
                }

                if (datePicker.getEditor().getText().length() == 2) {
                    datePicker.getEditor().setText(datePicker.getEditor().getText() + "/");
                    datePicker.getEditor().positionCaret(datePicker.getEditor().getText().length());
                }
                if (datePicker.getEditor().getText().length() == 5) {
                    datePicker.getEditor().setText(datePicker.getEditor().getText() + "/");
                    datePicker.getEditor().positionCaret(datePicker.getEditor().getText().length());
                }

            }
        });

        datePicker.getEditor().setOnKeyReleased((KeyEvent evt) -> {

            if (!datePicker.getEditor().getText().matches("\\d/*")) {
                datePicker.getEditor().setText(datePicker.getEditor().getText().replaceAll("[^\\d/]", ""));
                datePicker.getEditor().positionCaret(datePicker.getEditor().getText().length());
            }
        });

    }

    public static void cpfMask(TextField textField) {

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando

                if (textField.getText().length() == 4) {
                    textField.setText(textField.getText().substring(0, 3));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 8) {
                    textField.setText(textField.getText().substring(0, 7));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 12) {
                    textField.setText(textField.getText().substring(0, 11));
                    textField.positionCaret(textField.getText().length());
                }

            } else { // escrevendo

                if (textField.getText().length() == 14) {
                    event.consume();
                }

                if (textField.getText().length() == 3) {
                    textField.setText(textField.getText() + ".");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 7) {
                    textField.setText(textField.getText() + ".");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 11) {
                    textField.setText(textField.getText() + "-");
                    textField.positionCaret(textField.getText().length());
                }

            }
        });

//        textField.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!textField.getText().matches("\\d.-*")) {
//                textField.setText(textField.getText().replaceAll("[^\\d.-]", ""));
//                textField.positionCaret(textField.getText().length());
//            }
//        });

    }

    public static void cnpjMask(TextField textField) {

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (textField == null)
                event.consume();

            if (textField.getText() == null)
                event.consume();

            if (event.getCharacter().trim().length() == 0) { // apagando

                if (textField.getText().length() == 3) {
                    textField.setText(textField.getText().substring(0, 2));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 7) {
                    textField.setText(textField.getText().substring(0, 6));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 11) {
                    textField.setText(textField.getText().substring(0, 10));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 16) {
                    textField.setText(textField.getText().substring(0, 15));
                    textField.positionCaret(textField.getText().length());
                }

            } else { // escrevendo

                if (textField.getText().length() == 18) {
                    event.consume();
                }

                if (textField.getText().length() == 2) {
                    textField.setText(textField.getText() + ".");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 6) {
                    textField.setText(textField.getText() + ".");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 10) {
                    textField.setText(textField.getText() + "/");
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 15) {
                    textField.setText(textField.getText() + "-");
                    textField.positionCaret(textField.getText().length());
                }

            }
        });

//        textField.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!textField.getText().matches("\\d./-*")) {
//                textField.setText(textField.getText().replaceAll("[^\\d./-]", ""));
//                textField.positionCaret(textField.getText().length());
//            }
//        });
//        textField.setOnKeyReleased((KeyEvent evt) -> {
//
//            if (!textField.getText().matches("\\d./-*")) {
//                textField.setText(textField.getText().replaceAll("[^\\d./-]", ""));
//                textField.positionCaret(textField.getText().length());
//            }
//        });

    }

    public static void cnpjCpfMask(final TextField textField) {

        textField.focusedProperty().addListener((observableValue, aBoolean, fieldChange) -> {
            String value = textField.getText();
            if (!fieldChange) {
                if (textField.getText().length() == 11) {
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})$", "$1.$2.$3-$4");
                }
                if (textField.getText().length() == 14) {
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})$", "$1.$2.$3/$4-$5");
                }
            }
            textField.setText(value);
            if (!textField.getText().equals(value)) {
                textField.setText("");
                textField.insertText(0, value);
            }

        });

        maxField(textField, 18);
    }

    public static void emailMask(TextField textField) {

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz._-@".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if ("@".equals(event.getCharacter()) && textField.getText().contains("@")) {
                event.consume();
            }

            if ("@".equals(event.getCharacter()) && textField.getText().length() == 0) {
                event.consume();
            }
        });

    }

    public static void phoneMask(TextField textField) {

        textField.setOnKeyTyped((KeyEvent event) -> {
            if ("0123456789".contains(event.getCharacter()) == false) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando
                if (textField.getText() == null)
                    return;

                if (textField.getText().length() == 10 && textField.getText().substring(9, 10).equals("-")) {
                    textField.setText(textField.getText().substring(0, 9));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 9 && textField.getText().substring(8, 9).equals("-")) {
                    textField.setText(textField.getText().substring(0, 8));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 4) {
                    textField.setText(textField.getText().substring(0, 3));
                    textField.positionCaret(textField.getText().length());
                }
                if (textField.getText().length() == 1) {
                    textField.setText("");
                }

            } else { //escrevendo

                if (textField.getText() == null)
                    return;

                if (textField.getText().length() == 14) {
                    event.consume();
                }

                if (textField.getText().length() == 0) {
                    textField.setText("(" + event.getCharacter());
                    textField.positionCaret(textField.getText().length());
                    event.consume();
                }
                if (textField.getText().length() == 3) {
                    textField.setText(textField.getText() + ") " + event.getCharacter());
                    textField.positionCaret(textField.getText().length());
                    event.consume();
                }
                if (textField.getText().length() == 8) {
                    textField.setText(textField.getText() + "-" + event.getCharacter());
                    textField.positionCaret(textField.getText().length());
                    event.consume();
                }
                if (textField.getText().length() == 9 && textField.getText().substring(8, 9) != "-") {
                    textField.setText(textField.getText() + "-" + event.getCharacter());
                    textField.positionCaret(textField.getText().length());
                    event.consume();
                }
                if (textField.getText().length() == 13 && textField.getText().substring(8, 9).equals("-")) {
                    textField.setText(textField.getText().substring(0, 8) + textField.getText().substring(9, 10) + "-" + textField.getText().substring(10, 13) + event.getCharacter());
                    textField.positionCaret(textField.getText().length());
                    event.consume();
                }

            }

        });

        textField.setOnKeyReleased((KeyEvent evt) -> {
            if (textField.getText() == null)
                return;

            if (!textField.getText().matches("\\d()-*")) {
                textField.setText(textField.getText().replaceAll("[^\\d()-]", ""));
                textField.positionCaret(textField.getText().length());
            }
        });

    }

    public static void timeMask(TextField textField) {
        String val = "";

        textField.setOnKeyTyped((KeyEvent event) -> {
            if (!"0123456789".contains(event.getCharacter())) {
                event.consume();
            }

            if (event.getCharacter().trim().length() == 0) { // apagando
                if (textField.getText().length() == 6) {
                    textField.setText(textField.getText().substring(0, 5));
                    textField.positionCaret(textField.getText().length());
                }
            } else { // escrevendo
                if (textField.getText().length() == 5) {
                    event.consume();
                }
                if (textField.getText().length() == 2) {
                    textField.setText(textField.getText() + ":");
                    textField.positionCaret(textField.getText().length());
                }

            }
        });

        textField.setOnKeyReleased((KeyEvent evt) -> {
            if (!textField.getText().matches("\\d:*")) {
                textField.setText(textField.getText().replaceAll("[^\\d:]", ""));
                textField.positionCaret(textField.getText().length());
            }
        });

    }

    public static void dateTimeMask(final TextField textField) {
        maxField(textField, 19);

        textField.lengthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() < 20) {
                    String value = textField.getText();
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("(\\d{2})(\\d)", "$1/$2");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})(\\d)", "$1/$2/$3");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})\\/(\\d{4})(\\d)", "$1/$2/$3 $4");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})\\/(\\d{4})\\ (\\d{2})(\\d)", "$1/$2/$3 $4:$5");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})\\/(\\d{4})\\ (\\d{2})\\:(\\d{2})(\\d)", "$1/$2/$3 $4:$5:$6");
                    textField.setText(value);
                    positionCaret(textField);
                }
            }
        });
    }

    /**
     * Monta a mascara para Moeda.
     *
     * @param textField TextField
     */
    public static void monetaryMask(final TextField textField) {
        textField.setAlignment(Pos.CENTER_RIGHT);
        textField.lengthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                String value = textField.getText();
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceAll("([0-9]{1})([0-9]{14})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{11})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{8})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{5})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{2})$", "$1,$2");
                textField.setText(value);
                positionCaret(textField);

                textField.textProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                        if (newValue != null)
                            if (newValue.length() > 17) {
                                textField.setText(oldValue);
                            }
                    }
                });
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean fieldChange) {
                if (!fieldChange) {
                    if (textField.getText() == null)
                        return;

                    final int length = textField.getText().length();
                    if (length > 0 && length < 3) {
                        textField.setText(textField.getText() + "00");
                    }
                }
            }
        });
    }

    /**
     * Devido ao incremento dos caracteres das mascaras eh necessario que o
     * cursor sempre se posicione no final da string.
     *
     * @param textField TextField
     */
    private static void positionCaret(final TextField textField) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // Posiciona o cursor sempre a direita.
                if (textField.getText() != null)
                    textField.positionCaret(textField.getText().length());
            }
        });
    }

    /**
     * @param textField TextField.
     * @param length    Tamanho do campo.
     */
    public static void maxField(final TextField textField, final Integer length) {
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (newValue != null)
                    if (newValue.length() > length)
                        textField.setText(oldValue);

            }
        });
    }

    public static void creditCardMask(TextField textField) {

        textField.setOnKeyReleased((KeyEvent evt) -> {
            String value = textField.getText();
            if (textField.getText().length() == 14) {
                value = value.replaceAll("^([0-9]{4})([0-9])","$1 $2");
                value = value.replaceAll("^([0-9]{4})([0-9]{4})([0-9])","$1 $2 $3");
                value = value.replaceAll("^([0-9]{4})([0-9]{4})([0-9]{4})([0-9])","$1 $2 $3 $4");
                value = value.replaceAll("[^0-9]", "");
                //value = value.replaceFirst("([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})$", "$1 $2 $3 $4");
            }
            textField.setText(value);
            if (!textField.getText().equals(value)) {
                textField.setText("");
                textField.insertText(0, value);
            }

        });
    }
}
