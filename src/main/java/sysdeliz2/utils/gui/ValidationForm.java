/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.gui;

import javafx.geometry.Point2D;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.FormFieldDate;
import sysdeliz2.utils.gui.components.FormFieldMultipleFind;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldText;

import java.math.BigDecimal;

/**
 *
 * @author cristiano.diego
 */
public class ValidationForm {

    private static Tooltip setToolTip(Control textField, String validationInformation) {
        ImageView iconTooltip = new ImageView(new Image(ValidationForm.class.getResource("/images/icons/buttons/inactive (1).png").toExternalForm()));
        iconTooltip.setFitWidth(12.0);
        iconTooltip.setFitHeight(12.0);
        ImageView iconRequiredField = new ImageView(new Image(ValidationForm.class.getResource("/images/icons/buttons/required indicator (.5).png").toExternalForm()));
        Point2D positionTextField = textField.localToScene(0.0, 0.0);
        Label lbContentMessage = new Label();
        lbContentMessage.setGraphic(iconTooltip);
        lbContentMessage.setText(validationInformation);
        lbContentMessage.getStyleClass().addAll("tooltip", "danger");

        HBox contentTooltip = new HBox();
        contentTooltip.setSpacing(0.0);
        contentTooltip.getChildren().addAll(iconRequiredField, lbContentMessage);

        Tooltip toolTipFieldRequired = new Tooltip();
        toolTipFieldRequired.getStyleClass().remove("tooltip");
        toolTipFieldRequired.setGraphic(contentTooltip);
        toolTipFieldRequired.setAutoHide(true);

        textField.setTooltip(toolTipFieldRequired);
        toolTipFieldRequired.show((Stage) textField.getScene().getWindow(),
                positionTextField.getX() + textField.getScene().getX() + textField.getScene().getWindow().getX() + textField.getWidth(),
                positionTextField.getY() + textField.getScene().getY() + textField.getScene().getWindow().getY());

        return toolTipFieldRequired;
    }

    public static void cleanValidation(Control textField) {
        textField.setTooltip(null);
    }

    public static void validationEmpty(TextField textField) throws FormValidationException {
        cleanValidation(textField);

        if (textField.textProperty().isNull().get()) {
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException("Field empty");
        } else if (textField.textProperty().get().isEmpty()) {
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException("Field empty");
        }
    }

    public static void validationLenght(TextField textField, Integer lenght) throws FormValidationException {
        cleanValidation(textField);

        if (textField.textProperty().isNull().get()) {
            textField.getStyleClass().add("no-validation");
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException("Field empty");
        } else if (textField.textProperty().get().isEmpty()) {
            textField.getStyleClass().add("no-validation");
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException("Field empty");
        } else if (textField.textProperty().length().greaterThan(lenght).get()) {
            textField.getStyleClass().add("no-validation");
            setToolTip(textField, "Campo com tamanho máximo de " + lenght + " caracteres.");
            throw new FormValidationException("Field not lenght");
        }
    }
    
    public static void validationEmpty(TextField textField, String fieldName) throws FormValidationException {
        cleanValidation(textField);
        
        if (textField.textProperty().isNull().get()) {
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException(fieldName+": Campo vazio");
        } else if (textField.textProperty().get().isEmpty()) {
            setToolTip(textField, "Campo Obrigatório");
            throw new FormValidationException(fieldName+": Campo vazio");
        }
    }
    
    public static void validationEmpty(FormFieldSingleFind formField) throws FormValidationException {
        cleanValidation(formField.code);
        
        if ( formField.value.getValue() == null || ((BasicModel) formField.value.getValue()).getCodigoFilter() == null) {
            setToolTip(formField.code, "Campo Obrigatório");
            throw new FormValidationException(formField.title.getText()+": Campo vazio");
        }
    }
    
    public static void validationEmpty(FormFieldMultipleFind formField) throws FormValidationException {
        cleanValidation(formField.code);
        
        if (formField.objectValues.size() == 0) {
            setToolTip(formField.code, "Campo Obrigatório");
            throw new FormValidationException(formField.title.getText()+": Campo vazio");
        }
    }
    
    public static void validationEmpty(FormFieldText formField) throws FormValidationException {
        cleanValidation(formField.textField);
        
        if (formField.value.getValue() == null) {
            setToolTip(formField.textField, "Campo Obrigatório");
            throw new FormValidationException(formField.title.getText()+": Campo vazio");
        }
    }

    public static void validationDecimal(FormFieldText formField) throws FormValidationException {
        cleanValidation(formField.textField);

        try {
            new BigDecimal(formField.value.get().replaceAll(",","."));
        } catch (NumberFormatException valid) {
            setToolTip(formField.textField, "Valor inválido para o campo");
            throw new FormValidationException(formField.title.getText()+": Valor inválido para o campo");
        }
    }
    
    public static void validationEmpty(FormFieldDate formField) throws FormValidationException {
        cleanValidation(formField.datePicker);
        
        if (formField.value.getValue() == null) {
            setToolTip(formField.datePicker, "Campo Obrigatório");
            throw new FormValidationException(formField.title.getText()+": Campo vazio");
        }
    }
    
}
