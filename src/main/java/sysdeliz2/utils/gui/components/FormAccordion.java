package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.Arrays;
import java.util.function.Consumer;

public class FormAccordion extends Accordion {

    // fx attributes section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final BooleanProperty visible = new SimpleBooleanProperty(true);

    public FormAccordion() {

        getStylesheets().add("/styles/stylePadrao.css");

        visibleProperty().bind(visible);
        disableProperty().bind(disable);
    }

    /**
     * Create com FormBox gerenciável
     *
     * @return
     */
    public static final FormAccordion create() {
        return new FormAccordion();
    }

    /**
     * FormBox criado somente tela
     *
     * @param create
     */
    public static final FormAccordion create(Consumer<FormAccordion> create) {
        FormAccordion box = new FormAccordion();
        create.accept(box);
        return box;
    }

    public final void size(double width, double hight) {
        setPrefSize(width, hight);
    }

    public final void width(double width) {
        setPrefWidth(width);
    }

    public final void maxWidthSize(double width) {
        setMaxWidth(width);
    }

    public final void maxHeightSize(double height) {
        setMaxHeight(height);
    }

    public final void height(double height) {
        setPrefHeight(height);
        setMaxHeight(height);
        setMinHeight(height);
    }

    public final void padding(double value) {
        setPadding(new Insets(value));
    }

    public final void expanded() {
        HBox.setHgrow(this, Priority.ALWAYS);
        VBox.setVgrow(this, Priority.ALWAYS);
    }

    public final void clear() {
        getChildren().clear();
    }

    public final void add(FormTitledPane node) {
        getPanes().add(node);
    }

    public final void add(FormTitledPane... node) {
        getPanes().addAll(node);
    }

    public final void add(Consumer<FormTitledPane> node) {
        FormTitledPane formBox = new FormTitledPane();
        node.accept(formBox);
        add(formBox);
    }

}