package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.function.Consumer;

public class FormBadges extends AnchorPane {
    
    private VBox badge = new VBox();
    private Node nodeBadged = null;
    
    public final BooleanProperty visible = new SimpleBooleanProperty(true);
    
    public FormBadges() {
        VBox.setVgrow(this, Priority.ALWAYS);
        HBox.setHgrow(this, Priority.ALWAYS);
        badge.visibleProperty().bind(visible);
    }
    
    public static final FormBadges create() {
        return new FormBadges();
    }
    
    public static final FormBadges create(Consumer<FormBadges> value) {
        FormBadges box = new FormBadges();
        value.accept(box);
        return box;
    }
    
    public final void badgedNode(Node nodeBadged) {
        this.nodeBadged = nodeBadged;
        AnchorPane.setBottomAnchor(this.nodeBadged, 0.0);
        AnchorPane.setLeftAnchor(this.nodeBadged, 0.0);
        AnchorPane.setRightAnchor(this.nodeBadged, 0.0);
        AnchorPane.setTopAnchor(this.nodeBadged, 0.0);
        getChildren().add(nodeBadged);
        getChildren().add(badge);
    }
    
    public final void position(Pos position, Double padding) {
        switch(position) {
            case TOP_RIGHT:
                setRightAnchor(badge, padding);
                setTopAnchor(badge, padding);
                break;
            case TOP_LEFT:
                setLeftAnchor(badge, padding);
                setTopAnchor(badge, padding);
                break;
            case BOTTOM_RIGHT:
                setRightAnchor(badge, padding);
                setBottomAnchor(badge, padding);
                break;
            case BOTTOM_LEFT:
                setLeftAnchor(badge, padding);
                setBottomAnchor(badge, padding);
                break;
        }
    }
    
    public final void text(FormFieldText badge) {
        badge.textField.getStyleClass().removeIf(s -> s.equals("xl") || s.equals("lg"));
        badge.textField.setPadding(new Insets(-5.0));
        this.badge.getChildren().add(badge.build());
    }
    
    public final void text(Consumer<FormLabel> value) {
        FormLabel label = new FormLabel();
        value.accept(label);
        label.setPadding(new Insets(1.0, 3.0, 1.0, 3.0));
        this.badge.getChildren().add(label);
    }

    public final void text(String value) {
        FormLabel label = new FormLabel();
        label.setPadding(new Insets(1.0, 3.0, 1.0, 3.0));
        label.setText(value);
        this.badge.getChildren().add(label);
    }
    
    public final void icon(FontAwesome.Glyph icon, Double size, Color color) {
        this.badge.getChildren().add(new FontAwesome().create(icon).size(size).color(color));
    }
    
    public final void image(ImageView image) {
        this.badge.getChildren().add(image);
    }

    public final void node(Node node) {
        this.badge.getChildren().add(node);
    }
}
