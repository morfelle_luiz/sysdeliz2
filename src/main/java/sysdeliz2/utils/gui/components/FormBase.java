package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

public class FormBase {
    
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final ObjectProperty<Node> tabForm = new SimpleObjectProperty<>(null);
    
}
