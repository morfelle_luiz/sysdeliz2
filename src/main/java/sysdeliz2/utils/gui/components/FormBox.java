package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.util.Arrays;
import java.util.function.Consumer;

public class FormBox extends VBox {

    // components section
    private final Label title;
    private final VBox root;
    private final VBox vBox;
    private final HBox hBox;
    private final FlowPane fBox;
    private final ScrollPane scrollPane;

    // logic section
    private Boolean horizontalMode = false;
    private Boolean flutuanteMode = false;

    // fx attributes section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final BooleanProperty visible = new SimpleBooleanProperty(true);

    public FormBox() {
        title = new Label(null);
        root = new VBox();
        vBox = new VBox();
        hBox = new HBox();
        fBox = new FlowPane();
        scrollPane = new ScrollPane();

        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/sysDelizDesktop.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStyleClass().addAll("vbox");
        visibleProperty().bind(visible);

        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        VBox.setVgrow(scrollPane, javafx.scene.layout.Priority.ALWAYS);

        title.getStyleClass().addAll("label-form-field-v");

        VBox.setVgrow(vBox, javafx.scene.layout.Priority.ALWAYS);
        VBox.setMargin(vBox, new Insets(0.0));
        VBox.setVgrow(hBox, javafx.scene.layout.Priority.ALWAYS);
        VBox.setMargin(hBox, new Insets(0.0));
        VBox.setVgrow(fBox, javafx.scene.layout.Priority.ALWAYS);
        VBox.setMargin(fBox, new Insets(0.0));

        fBox.setPrefWrapLength(0.0);
        fBox.setRowValignment(javafx.geometry.VPos.TOP);
        fBox.setVgap(5.0);
        fBox.setHgap(5.0);

        vBox.setSpacing(5.0);
        hBox.setSpacing(5.0);

        vBox.disableProperty().bind(disable);
        hBox.disableProperty().bind(disable);
        fBox.disableProperty().bind(disable);

        //root.getChildren().add(scrollPane);
        //getChildren().add(root);
    }

    /**
     * Create com FormBox gerenciável
     *
     * @return
     */
    public static final FormBox create() {
        return new FormBox();
    }

    /**
     * FormBox criado somente tela
     *
     * @param create
     */
    public static final FormBox create(Consumer<FormBox> create) {
        FormBox box = new FormBox();
        create.accept(box);
        return box;
    }

    public final void size(double width, double height) {
        setPrefSize(width, height);
    }

    public final void width(double width) {
        setPrefWidth(width);
    }

    public final void maxWidthSize(double width) {
        setMaxWidth(width);
    }

    public final void maxHeightSize(double height) {
        setMaxHeight(height);
    }

    public final void height(double height) {
        setPrefHeight(height);
        setMaxHeight(height);
        setMinHeight(height);
    }

    public final void title(String title) {
        this.title.setText(title);
        getChildren().add(0, this.title);
        this.cleanStyle();
        vBox.getStyleClass().add("corner-up-left");
        hBox.getStyleClass().add("corner-up-left");
        vBox.setPadding(new Insets(5.0));
        hBox.setPadding(new Insets(5.0));
    }

    public final void titleStyle(String styleClass) {
        this.title.getStyleClass().add(styleClass);
    }

    public final void border() {
        this.cleanStyle();
        vBox.getStyleClass().add(title.getText() == null ? "no-corner" : "corner-up-left");
        hBox.getStyleClass().add(title.getText() == null ? "no-corner" : "corner-up-left");
        fBox.getStyleClass().add(title.getText() == null ? "no-corner" : "corner-up-left");
        vBox.setPadding(new Insets(5.0));
        hBox.setPadding(new Insets(5.0));
        fBox.setPadding(new Insets(5.0));
    }

    public final void withoutSpace() {

        fBox.setVgap(1);
        fBox.setHgap(1);

        vBox.setSpacing(1);
        hBox.setSpacing(1);
    }

    public final void vertical() {
        horizontalMode = false;
        flutuanteMode = false;
        getChildren().add(vBox);
    }

    public final void padding(double value) {
        vBox.setPadding(new Insets(value));
        hBox.setPadding(new Insets(value));
        fBox.setPadding(new Insets(value));
    }

    public final void horizontal() {
        horizontalMode = true;
        flutuanteMode = false;
        getChildren().add(hBox);
    }

    public final void vSpace(double value) {
        horizontalMode = false;
        flutuanteMode = false;
        getChildren().clear();
        getChildren().add(vBox);
        height(value);
    }

    public final void hSpace(double value) {
        horizontalMode = true;
        flutuanteMode = false;
        getChildren().clear();
        getChildren().add(hBox);
        width(value);
    }

    public final void flutuante() {
        horizontalMode = false;
        flutuanteMode = true;
        getChildren().add(fBox);
    }

    public final void expanded() {
        HBox.setHgrow(this, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(this, javafx.scene.layout.Priority.ALWAYS);
    }

    public final void tooltip(String text) {
        title.setTooltip(new Tooltip(text));
    }

    public final void verticalScroll() {
        if (flutuanteMode) {
            final ScrollPane scroll = new ScrollPane();
            scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
            scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            scroll.setPadding(new Insets(10, 0, 0, 10));
            scroll.viewportBoundsProperty().addListener(new ChangeListener<Bounds>() {
                @Override
                public void changed(ObservableValue<? extends Bounds> ov, Bounds oldBounds, Bounds bounds) {
                    fBox.setPrefWidth(bounds.getWidth());
                    fBox.setPrefHeight(bounds.getHeight());
                }
            });
            getChildren().remove(fBox);
            scroll.setContent(fBox);
            VBox.setVgrow(scroll, javafx.scene.layout.Priority.ALWAYS);
            VBox.setMargin(scroll, new Insets(0.0));
            getChildren().add(scroll);
            return;
        }

        if (horizontalMode) {
            hBox.setMaxWidth(Double.MAX_VALUE);
            hBox.setMaxHeight(Double.MAX_VALUE);
            getChildren().remove(hBox);
            scrollPane.setContent(hBox);
        } else {
            vBox.setMaxWidth(Double.MAX_VALUE);
            vBox.setMaxHeight(Double.MAX_VALUE);
            getChildren().remove(vBox);
            scrollPane.setContent(vBox);
        }
        getChildren().add(scrollPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }

    public final void horizontalScroll() {
        if (flutuanteMode) {
            flutuante();
            return;
        }

        if (horizontalMode) {
            hBox.setMaxWidth(Double.MAX_VALUE);
            hBox.setMaxHeight(Double.MAX_VALUE);
            getChildren().remove(hBox);
            scrollPane.setContent(hBox);
        } else {
            vBox.setMaxWidth(Double.MAX_VALUE);
            vBox.setMaxHeight(Double.MAX_VALUE);
            getChildren().remove(vBox);
            scrollPane.setContent(vBox);
        }
        VBox.setVgrow(scrollPane, javafx.scene.layout.Priority.ALWAYS);
        getChildren().add(scrollPane);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }

    public final void bothScroll() {
        if (flutuanteMode) {
            flutuante();
            return;
        }

        if (horizontalMode) {
            hBox.setMaxWidth(Double.MAX_VALUE);
            hBox.setMaxHeight(Double.MAX_VALUE);
            hBox.minWidthProperty().bind(scrollPane.widthProperty().subtract(5));
            hBox.minHeightProperty().bind(scrollPane.heightProperty().subtract(5));
            getChildren().remove(hBox);
            scrollPane.setContent(hBox);
        } else {
            vBox.setMaxWidth(Double.MAX_VALUE);
            vBox.setMaxHeight(Double.MAX_VALUE);
            vBox.minWidthProperty().bind(scrollPane.widthProperty().subtract(5));
            vBox.minHeightProperty().bind(scrollPane.heightProperty().subtract(5));
            getChildren().remove(vBox);
            scrollPane.setContent(vBox);
        }
        getChildren().add(scrollPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }

    public final void clear() {
        vBox.getChildren().clear();
        hBox.getChildren().clear();
        fBox.getChildren().clear();
    }

    public final void alignment(Pos position) {
        vBox.setAlignment(position);
        hBox.setAlignment(position);
        root.setAlignment(position);
        setAlignment(position);
    }

    public final void alignment(VPos position) {
        fBox.setRowValignment(position);
    }

    public final void icon(Image icon) {
        if (title.getText() == null) {
            getChildren().add(0, this.title);
            this.cleanStyle();
            vBox.getStyleClass().add("corner-up-left");
            hBox.getStyleClass().add("corner-up-left");
            fBox.getStyleClass().add("corner-up-left");

            vBox.setPadding(new Insets(5.0));
            hBox.setPadding(new Insets(5.0));
            fBox.setPadding(new Insets(5.0));
        }

        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
    }

    public final VBox build() {
        return this;
    }

    public final void add(Node node) {
        if (horizontalMode)
            this.hBox.getChildren().add(node);
        else if (flutuanteMode)
            this.fBox.getChildren().add(node);
        else
            this.vBox.getChildren().add(node);
    }

    public final void addBtn(Consumer<FormButton> btnCreate) {
        FormButton box = new FormButton();
        btnCreate.accept(box);
        if (horizontalMode)
            this.hBox.getChildren().add(box);
        else if (flutuanteMode)
            this.fBox.getChildren().add(box);
        else
            this.vBox.getChildren().add(box);
    }

    public final void add(Node node, Integer index) {
        if (horizontalMode)
            this.hBox.getChildren().add(index, node);
        else if (flutuanteMode)
            this.fBox.getChildren().add(index, node);
        else
            this.vBox.getChildren().add(index, node);
    }

    public final void add(Consumer<FormBox> box) {
        FormBox formBox = new FormBox();
        box.accept(formBox);
        add(formBox);
    }

    public final ObservableList<Node> itens() {
        if (horizontalMode)
            return this.hBox.getChildren();
        else if (flutuanteMode)
            return this.fBox.getChildren();
        else
            return this.vBox.getChildren();
    }

    public final void add(Node... nodes) {
        Arrays.asList(nodes).forEach(node -> {
            if (horizontalMode)
                this.hBox.getChildren().add(node);
            else if (flutuanteMode)
                this.fBox.getChildren().add(node);
            else
                this.vBox.getChildren().add(node);
        });
    }

    public final void addVertical(Node... nodes) {
        vertical();
        Arrays.asList(nodes).forEach(node -> {
            if (horizontalMode)
                this.hBox.getChildren().add(node);
            else if (flutuanteMode)
                this.fBox.getChildren().add(node);
            else
                this.vBox.getChildren().add(node);
        });
    }

    public final void addHorizontal(Node... nodes) {
        horizontal();
        Arrays.asList(nodes).forEach(node -> {
            if (horizontalMode)
                this.hBox.getChildren().add(node);
            else if (flutuanteMode)
                this.fBox.getChildren().add(node);
            else
                this.vBox.getChildren().add(node);
        });
    }

    public final void add(Integer index, Node node) throws IndexOutOfBoundsException {
        if (horizontalMode)
            this.hBox.getChildren().add(index, node);
        if (flutuanteMode)
            this.fBox.getChildren().add(index, node);
        else
            this.vBox.getChildren().add(index, node);
    }

    public final void remove(Node node) {
        if (horizontalMode)
            this.hBox.getChildren().remove(node);
        else if (flutuanteMode)
            this.fBox.getChildren().remove(node);
        else
            this.vBox.getChildren().remove(node);
    }

    public final void remove(Integer index) throws IndexOutOfBoundsException {
        if (horizontalMode)
            this.hBox.getChildren().remove(index);
        else if (flutuanteMode)
            this.fBox.getChildren().remove(index);
        else
            this.vBox.getChildren().remove(index);
    }

    public final void addStyle(String styleClass) {
        vBox.getStyleClass().add(styleClass);
        hBox.getStyleClass().add(styleClass);
        fBox.getStyleClass().add(styleClass);
    }

    public final void background(Image image, BackgroundRepeat repeatX, BackgroundRepeat repeatY, BackgroundPosition position, BackgroundSize size) {
        Background back = new Background(new BackgroundImage(image, repeatX, repeatY, position, size));
        if (horizontalMode)
            hBox.setBackground(back);
        else if (flutuanteMode)
            fBox.setBackground(back);
        else
            vBox.setBackground(back);
    }

    public final void removeStyle(String styleClass) {
        vBox.getStyleClass().remove(styleClass);
        hBox.getStyleClass().remove(styleClass);
        fBox.getStyleClass().remove(styleClass);
    }

    public final void removeStyle(String... styleClass) {
        vBox.getStyleClass().removeAll(styleClass);
        hBox.getStyleClass().removeAll(styleClass);
        fBox.getStyleClass().removeAll(styleClass);
    }

    private void cleanStyle() {
        vBox.getStyleClass().remove("corner-up-left");
        vBox.getStyleClass().remove("no-corner");
        hBox.getStyleClass().remove("corner-up-left");
        hBox.getStyleClass().remove("no-corner");
        fBox.getStyleClass().remove("corner-up-left");
        fBox.getStyleClass().remove("no-corner");
    }

    public void spacing(double space) {
        if (horizontalMode) hBox.setSpacing(space);
        else if (flutuanteMode) fBox.setVgap(space);
        else vBox.setSpacing(space);
    }

    public int getChildrenSize() {
        return horizontalMode ? hBox.getChildren().size() : flutuanteMode ? fBox.getChildren().size() : vBox.getChildren().size();
    }

    public VBox getRoot() {
        return root;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }
}