package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.util.Arrays;
import java.util.function.Consumer;

public class FormBoxPane extends VBox {

    // components section
    private final BorderPane root;

    // fx attributes section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final BooleanProperty visible = new SimpleBooleanProperty(true);

    public FormBoxPane() {
        root = new BorderPane();
        
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/sysDelizDesktop.css");
        visibleProperty().bind(visible);

        VBox.setVgrow(root, Priority.ALWAYS);
        getChildren().add(root);
    }
    
    /**
     * Create com FormBox gerenciável
     *
     * @return
     */
    public static final FormBoxPane create() {
        return new FormBoxPane();
    }
    
    /**
     * FormBox criado somente tela
     *
     * @param create
     */
    public static final FormBoxPane create(Consumer<FormBoxPane> create) {
        FormBoxPane box = new FormBoxPane();
        create.accept(box);
        return box;
    }

    public final void expanded() {
        HBox.setHgrow(this, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(this, javafx.scene.layout.Priority.ALWAYS);
    }

    public final void top(Node node) {
        root.setTop(node);
        root.getTop().setStyle(root.getTop().getStyle().concat("-fx-padding: 5;"));
    }

    public final void left(Node node) {
        root.setLeft(node);
        root.getLeft().setStyle(root.getLeft().getStyle().concat("-fx-padding: 5;"));
    }

    public final void right(Node node) {
        root.setRight(node);
        root.getRight().setStyle(root.getRight().getStyle().concat("-fx-padding: 5;"));
    }

    public final void center(Node node) {
        root.setCenter(node);
        root.getCenter().setStyle(root.getCenter().getStyle().concat("-fx-padding: 5;"));
    }

    public final void bottom(Node node) {
        root.setBottom(node);
        root.getBottom().setStyle(root.getBottom().getStyle().concat("-fx-padding: 5;"));
    }

    public BorderPane getRoot() {
        return root;
    }
}