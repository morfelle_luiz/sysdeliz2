package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.function.Consumer;

public class FormButton extends Button {
    
    // fx section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    
    public FormButton() {
    
        getStylesheets().add("/styles/stylePadrao.css");
        disableProperty().bind(disable);
        focusTraversableProperty().bind(disable.not());

    }
    
    public static final FormButton create(){
        return new FormButton();
    }
    
    public static final FormButton create(Consumer<FormButton> create){
        FormButton formButton = new FormButton();
        create.accept(formButton);
        return formButton;
    }
    
    public final FormButton width(double width) {
        setPrefWidth(width);
        return this;
    }
    
    public final FormButton height(double height) {
        setPrefHeight(height);
        return this;
    }
    
    public final FormButton title(String title) {
        setText(title);
        return this;
    }

    public final FormButton icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        setGraphic(iconButton);
        return this;
    }
    
    public final void icon(ImageView icon) {
        setGraphic(icon);
    }
    
    public final void focusTraversable(Boolean value){
        setFocusTraversable(value);
    }
    
    public final FormButton tooltip(String tooltip) {
        setTooltip(new Tooltip(tooltip));
        return this;
    }
    
    public final void contentDisplay(ContentDisplay contentDisplay) {
        setContentDisplay(contentDisplay);
    }
    
    public final FormButton addStyle(String styleClass){
        getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormButton removeStyle(String styleClass){
        getStyleClass().remove(styleClass);
        return this;
    }
    
    public final void defaultButton(Boolean isDefault){
        setDefaultButton(isDefault);
    }
    
    public final void setAction(EventHandler<ActionEvent> value){
        setOnAction(value);
    }
    
    public final Button build() {
        return this;
    }
}