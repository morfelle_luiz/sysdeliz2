package sysdeliz2.utils.gui.components;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class FormCarousel extends HBox {
    
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    private final ListProperty<Node> nodesCarousel = new SimpleListProperty<>();
    
    private FormBox box = FormBox.create(box -> {
        box.vertical();
        box.expanded();
    });
    private FormButton btnAnterior = FormButton.create(btn -> {
       btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
       btn.tooltip("Anterior");
       btn.addStyle("lg");
       btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._48));
       btn.disable.bind(navegationIndex.isEqualTo(0));
       btn.setAction(evt -> {
           navegationIndex.set(navegationIndex.get() - 1);
           box.clear();
           box.add(nodesCarousel.get(navegationIndex.get()));
       });
    });
    private FormButton btnProximo = FormButton.create(btn -> {
       btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
       btn.tooltip("Próximo");
        btn.addStyle("lg");
       btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._48));
       btn.disable.bind(navegationIndex.isEqualTo(nodesCarousel.sizeProperty().subtract(1)));
       btn.setAction(evt -> {
           navegationIndex.set(navegationIndex.get() + 1);
           box.clear();
           box.add(nodesCarousel.get(navegationIndex.get()));
       });
    });
    
    public FormCarousel() {
        setAlignment(Pos.CENTER);
        getChildren().addAll(btnAnterior, box, btnProximo);
    
        HBox.setHgrow(this, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(this, javafx.scene.layout.Priority.ALWAYS);
    }
    
    public static final FormCarousel create() {
        return new FormCarousel();
    }
    
    public static final FormCarousel create(Consumer<FormCarousel> create) {
        FormCarousel box = new FormCarousel();
        create.accept(box);
        return box;
    }
    
    public final void setNodesCarousel(List<Node> nodes){
        nodesCarousel.set(FXCollections.observableList(nodes));
        box.clear();
        box.add(nodesCarousel.get(0));
    }
    
    public final void setNodesCarousel(Node... nodes){
        nodesCarousel.set(FXCollections.observableList(Arrays.asList(nodes)));
        box.clear();
        box.add(nodesCarousel.get(0));
    }
    
}
