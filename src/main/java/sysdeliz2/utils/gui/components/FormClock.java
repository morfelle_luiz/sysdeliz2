package sysdeliz2.utils.gui.components;

import javafx.animation.AnimationTimer;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import org.controlsfx.glyphfont.FontAwesome;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public class FormClock {
    
    // components section
    private final VBox root;
    private final Label textField;
    
    private AnimationTimer timer;
    private long lastTimerCall;
    
    public enum Alignment {CENTER, LEFT, RIGHT}
    
    public FormClock() {
        root = new VBox();
        textField = new Label();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setPadding(new Insets(0));
    
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override
            public void handle(final long now) {
                if (now > lastTimerCall + 1_000_000_000l) {
                    textField.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                    lastTimerCall = now;
                }
            }
        };
        timer.start();
        
        textField.setStyle("-fx-alignment: CENTER-LEFT");
        textField.setPadding(new Insets(5));
    
        root.getChildren().add(textField);
    }
    
    public static final FormClock create(){
        return new FormClock();
    }
    
    public static final FormClock create(Consumer<FormClock> value){
        FormClock box = new FormClock();
        value.accept(box);
        return box;
    }
    
    public final FormClock width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormClock icon(ImageView icon) {
        this.textField.setGraphic(icon);
        return this;
    }
    
    public final FormClock icon(FontAwesome.Glyph icon){
        root.getChildren().remove(root.getChildren().size() - 1);
        textField.setGraphic(new FontAwesome().create(icon));
        return this;
    }
    
    public final FormClock addStyle(String styleClass){
        textField.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormClock removeStyle(String styleClass){
        textField.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormClock toUpper(){
        textField.setText(textField.getText().toUpperCase());
        return this;
    }
    
    public final void alignment(FormClock.Alignment alignment){
        switch(alignment){
            case CENTER: textField.setStyle("-fx-alignment: CENTER"); break;
            case LEFT: textField.setStyle("-fx-alignment: CENTER-LEFT"); break;
            case RIGHT: textField.setStyle("-fx-alignment: CENTER-RIGHT"); break;
        }
    }
    
    public final VBox build() {
        return root;
    }
}