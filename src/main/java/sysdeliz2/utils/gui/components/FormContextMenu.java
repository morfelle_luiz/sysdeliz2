package sysdeliz2.utils.gui.components;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

import java.util.function.Consumer;

public class FormContextMenu extends ContextMenu {
    
    public FormContextMenu() {
    }
    
    public static final FormContextMenu create(){
        return new FormContextMenu();
    }
    
    public static final FormContextMenu create(Consumer<FormContextMenu> create){
        FormContextMenu contextMenu = new FormContextMenu();
        create.accept(contextMenu);
        return contextMenu;
    }
    
    public final void addItem(Consumer<MenuItem> menu){
        MenuItem item = new MenuItem();
        menu.accept(item);
        getItems().add(item);
    }

    public final void addItem(MenuItem menuItem) {
        getItems().add(menuItem);
    }
    
    public final void addSeparator(){
        getItems().add(new SeparatorMenuItem());
    }
}
