package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.function.Consumer;

public class FormControlButton extends Button {

    // fx section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final BooleanProperty isSelected = new SimpleBooleanProperty(false);
    public final BooleanProperty autoSelect = new SimpleBooleanProperty(true);
    public final StringProperty codeFilter = new SimpleStringProperty();

    // colors
    public final StringProperty bgColorStandard = new SimpleStringProperty();
    public final StringProperty bgColorSelected = new SimpleStringProperty();
    public final StringProperty textColor = new SimpleStringProperty();

    public FormControlButton() {
        getStylesheets().add("/styles/stylePadrao.css");
        getStyleClass().add("control-button");
        disableProperty().bind(disable);
        focusTraversableProperty().bind(disable.not());
    }
    
    public static final FormControlButton create(){
        return new FormControlButton();
    }
    
    public static final FormControlButton create(Consumer<FormControlButton> create){
        FormControlButton formButton = new FormControlButton();

        formButton.isSelected.addListener((observable, oldValue, newValue) -> {
            if(!newValue){
                formButton.setStyle("-fx-background-color: " + formButton.bgColorStandard.get() + ";");
            } else {
                formButton.setStyle("-fx-background-color: " + formButton.bgColorSelected.get() + ";");
            }
        });
        create.accept(formButton);
        formButton.setStyle("-fx-background-color:" + formButton.bgColorStandard.get()  + ";");
        if(formButton.autoSelect.get())
            formButton.setOnMouseClicked(evt -> {
                formButton.selectionControl();
            });
        return formButton;
    }
    
    public final FormControlButton width(double width) {
        setPrefWidth(width);
        return this;
    }
    
    public final FormControlButton height(double height) {
        setPrefHeight(height);
        return this;
    }
    
    public final FormControlButton title(String title) {
        setText(title);
        return this;
    }
    
    public final FormControlButton icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        setGraphic(iconButton);
        return this;
    }
    
    public final void icon(ImageView icon) {
        setGraphic(icon);
    }
    
    public final void focusTraversable(Boolean value){
        setFocusTraversable(value);
    }
    
    public final FormControlButton tooltip(String tooltip) {
        setTooltip(new Tooltip(tooltip));
        return this;
    }
    
    public final void contentDisplay(ContentDisplay contentDisplay) {
        setContentDisplay(contentDisplay);
    }
    
    public final FormControlButton addStyle(String styleClass){
        getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormControlButton removeStyle(String styleClass){
        getStyleClass().remove(styleClass);
        return this;
    }

    public final void setAction(EventHandler<ActionEvent> value){
        setOnAction(value);
    }

    public final void selectionControl(){
        isSelected.set(isSelected.not().get());
    }

    
    public final Button build() {
        return this;
    }
}