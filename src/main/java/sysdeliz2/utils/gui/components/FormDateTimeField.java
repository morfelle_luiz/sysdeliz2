package sysdeliz2.utils.gui.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.time.LocalDateTime;

public class FormDateTimeField extends VBox {
    
    private final HBox boxForm = new HBox();
    private final Label titleFormField = new Label();
    private final DatePicker data = new DatePicker();
    private final Spinner<Integer> hora = new Spinner<>(0,23,0);
    private final Spinner<Integer> minuto = new Spinner<>(0,59,0);
    private final Spinner<Integer> segundo = new Spinner<>(0,59,0);
    public final ObjectProperty<LocalDateTime> dateTime = new SimpleObjectProperty<>();
    
    public FormDateTimeField(String titleField) {
        createField(titleField, true, 80);
    }
    
    public FormDateTimeField(String titleField, Boolean editable) {
        createField(titleField, editable, 80);
    }
    
    public FormDateTimeField(String titleField, Boolean editable, Integer widht) {
        createField(titleField, editable, widht);
    }
    
    public FormDateTimeField(String titleField, Integer widht) {
        createField(titleField, true, widht);
    }
    
    private void createField(String titleField, Boolean editable, Integer width){
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
    
        titleFormField.setText(titleField.toUpperCase());
        titleFormField.getStyleClass().addAll("label-form-field-v");
        data.getStyleClass().addAll("text-field","text-field-form-field-v","form-field", !editable ? "no-editable" : "");
        data.setEditable(editable);
        hora.setEditable(editable);
        minuto.setEditable(editable);
        segundo.setEditable(editable);
        
        boxForm.getChildren().add(data);
        boxForm.getChildren().add(new Label(" "));
        boxForm.getChildren().add(hora);
        boxForm.getChildren().add(new Label(":"));
        boxForm.getChildren().add(minuto);
        boxForm.getChildren().add(new Label(":"));
        boxForm.getChildren().add(segundo);
        
        setPrefHeight(0.0);
        setPrefWidth(width.doubleValue());
        getChildren().add(titleFormField);
        getChildren().add(boxForm);
    }
}
