package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FormFieldCheckBoxGroup<T> {

    private final VBox root;
    private final Label title;
    private final HBox boxHField;
    private final VBox boxVField;
    private final FlowPane boxFField;

    private Boolean horizontalMode = false;
    private Boolean flutuanteMode = false;

    // fx components section
    public final ListProperty<OptionCheckBox> items = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final BooleanProperty editable = new SimpleBooleanProperty(true);

    public FormFieldCheckBoxGroup() {

        root = new VBox();
        title = new Label();
        boxHField = new HBox();
        boxVField = new VBox();
        boxFField = new FlowPane();

        root.getStylesheets().add("/styles/sysDelizDesktop.css");

        MenuItem menuItemMarcar = new MenuItem("Marcar Todos", ImageUtils.getIcon(ImageUtils.Icon.TODOS, ImageUtils.IconSize._16));
        menuItemMarcar.setOnAction(evt -> items.forEach(item -> item.setSelected(true)));
        MenuItem menuItemDesmarcar = new MenuItem("Desmarcar Todos", ImageUtils.getIcon(ImageUtils.Icon.TODOS, ImageUtils.IconSize._16));
        menuItemDesmarcar.setOnAction(evt -> items.forEach(item -> item.setSelected(true)));
        ContextMenu menu = new ContextMenu();
        menu.getItems().add(menuItemMarcar);
        menu.getItems().add(menuItemDesmarcar);
        title.setContextMenu(menu);

        title.getStyleClass().add("form-field");

        boxHField.setSpacing(3.0);
        boxHField.setPrefHeight(30.0);
        boxHField.getStyleClass().add("corner-up-left");
        boxHField.setPadding(new Insets(3.0));
        boxHField.disableProperty().bind(editable.not());

        boxVField.setSpacing(3.0);
        boxVField.getStyleClass().add("corner-up-left");
        boxVField.setPadding(new Insets(3.0));
        boxVField.disableProperty().bind(editable.not());

        boxFField.setVgap(3);
        boxFField.setHgap(10);
        boxFField.getStyleClass().add("corner-up-left");
        boxFField.setPadding(new Insets(3.0));
        boxFField.disableProperty().bind(editable.not());

        root.getChildren().add(title);
        root.getChildren().add(boxVField);

        items.clear();
    }

    public static final FormFieldCheckBoxGroup create() {
        return new FormFieldCheckBoxGroup();
    }

    public static final <T> FormFieldCheckBoxGroup create(Consumer<FormFieldCheckBoxGroup> value) {
        FormFieldCheckBoxGroup<T> box = new FormFieldCheckBoxGroup();
        value.accept(box);
        return box;
    }

    public final FormFieldCheckBoxGroup title(String title) {
        this.title.setText(title);
        return this;
    }

    public Pane getBox() {
        if (horizontalMode) return boxHField;
        if (flutuanteMode) return boxFField;
        return boxVField;
    }


    public final FormFieldCheckBoxGroup icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }

    public final void select(Integer index) {
        OptionCheckBox checkBox = items.get(index);
        if (!checkBox.isSelected()) {
            checkBox.setSelected(true);
        }
    }

    public final void horizontalMode() {
        horizontalMode = true;
        flutuanteMode = false;
        root.getChildren().remove(1);
        root.getChildren().add(boxHField);
        items.forEach(t -> {
            boxHField.getChildren().add(t);
        });
    }

    public final void flutuanteMode() {
        flutuanteMode = true;
        horizontalMode = false;
        root.getChildren().remove(1);
        root.getChildren().add(boxFField);
        items.forEach(t -> {
            boxFField.getChildren().add(t);
        });
    }


    public final void clear() {
        items.forEach(t -> {
            t.setSelected(false);
        });
    }

    public final List<OptionCheckBox> selecteds() {
        return items.stream().filter(OptionCheckBox::isSelected).collect(Collectors.toList());
    }

    public final OptionCheckBox option(String title, T value) {
        return new OptionCheckBox(title, value);
    }

    public final OptionCheckBox option(String title, T value, boolean selected) {
        return new OptionCheckBox(title, value, selected);
    }

    public final void items(OptionCheckBox... items) {
        this.items.clear();
        Arrays.asList(items).forEach(t -> {
            if (horizontalMode) {
                t.setPadding(new Insets(-4, 0, 1, 0));
                boxHField.getChildren().add(t);
            } else if (flutuanteMode)
                boxFField.getChildren().add(t);
            else
                boxVField.getChildren().add(t);

            this.items.add(t);
        });
    }

    public final void items(List<OptionCheckBox> items) {
        this.items.clear();
        items.forEach(t -> {
            if (horizontalMode) {
                t.setPadding(new Insets(-4, 0, 1, 0));
                boxHField.getChildren().add(t);
            } else if (flutuanteMode)
                boxFField.getChildren().add(t);
            else
                boxVField.getChildren().add(t);
            this.items.add(t);
        });
    }

    public final FormFieldCheckBoxGroup addStyle(String styleClass) {
        boxHField.getStyleClass().add(styleClass);
        boxVField.getStyleClass().add(styleClass);
        boxFField.getStyleClass().add(styleClass);
        return this;
    }

    public final FormFieldCheckBoxGroup removeStyle(String styleClass) {
        boxHField.getStyleClass().remove(styleClass);
        boxVField.getStyleClass().remove(styleClass);
        boxFField.getStyleClass().remove(styleClass);
        return this;
    }

    public final void configCheckBox(OptionCheckBox opt, Consumer<OptionCheckBox> value) {
        value.accept(opt);
    }

    public final void withoutTitle() {
        root.getChildren().remove(0);
        boxHField.getStyleClass().remove("corner-up-left");
        boxVField.getStyleClass().remove("corner-up-left");
        boxFField.getStyleClass().remove("corner-up-left");
    }

    public final VBox build() {
        return root;
    }

    public class OptionCheckBox extends CheckBox {

        private String title = null;
        public T valueOption = null;

        public OptionCheckBox(String title, T value) {
            this.title = title;
            this.valueOption = value;

            createCheckBox(false);
        }

        public OptionCheckBox(String title, T value, boolean selected) {
            this.title = title;
            this.valueOption = value;

            createCheckBox(selected);
        }

        private void createCheckBox(boolean selected) {
            setText(title);
            setSelected(selected);
        }
    }
}
