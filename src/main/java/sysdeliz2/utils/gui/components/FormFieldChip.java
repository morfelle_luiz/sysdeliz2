package sysdeliz2.utils.gui.components;

import com.jfoenix.controls.JFXChip;
import com.jfoenix.controls.JFXChipView;
import com.jfoenix.controls.JFXDefaultChip;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.models.generics.BasicModel;

import java.util.Arrays;
import java.util.function.Consumer;

public class FormFieldChip<T> {

    // components section
    private final VBox root;
    public final Label title;
    public final JFXChipView<T> chipValues;

    // fx components section
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty focusTraversable = new SimpleBooleanProperty(true);

    public FormFieldChip() {
        root = new VBox();
        HBox field = new HBox();
        title = new Label();
        chipValues = new JFXChipView<>();

        root.getStylesheets().add("/styles/stylePadrao.css");
        root.getStylesheets().add("/styles/sysDelizDesktop.css");

        field.getStyleClass().add("corner-up-left");
        field.setPadding(new Insets(2));
        VBox.setVgrow(field, Priority.ALWAYS);
        title.getStyleClass().add("form-field");
        title.setText("Title");

        chipValues.disableProperty().bind(editable.not());

        field.getChildren().add(chipValues);
        root.getChildren().add(title);
        root.getChildren().add(field);
    }

    public static final FormFieldChip create() {
        return new FormFieldChip();
    }

    public static final FormFieldChip create(Consumer<FormFieldChip> value) {
        FormFieldChip box = new FormFieldChip();
        value.accept(box);
        return box;
    }

    public final FormFieldChip width(double width) {
        root.setPrefWidth(width);
        return this;
    }

    public final FormFieldChip maxWidth(double width) {
        root.setMaxWidth(width);
        return this;
    }

    public final FormFieldChip height(double height) {
        root.setPrefHeight(height);
        return this;
    }

    public final FormFieldChip maxHeight(double height) {
        root.setMaxHeight(height);
        return this;
    }

    public final FormFieldChip title(String title) {
        this.title.setText(title);
        return this;
    }

    public final FormFieldChip icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }

    public final FormFieldChip editable(Boolean value) {
        editable.set(value);
        return this;
    }

    public final FormFieldChip addStyle(String styleClass) {
        chipValues.getStyleClass().add(styleClass);
        return this;
    }

    public final FormFieldChip removeStyle(String styleClass) {
        chipValues.getStyleClass().remove(styleClass);
        return this;
    }

    public final FormFieldChip removeStyle(String... styleClass) {
        chipValues.getStyleClass().removeAll(styleClass);
        return this;
    }

    public final void clearDefaultStyle() {
        chipValues.getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
    }

    public final void fontSize(Double size) {
        chipValues.setStyle(chipValues.getStyle() + "; -fx-font-size: " + size + ";");
    }

    public final void fontBold() {
        chipValues.setStyle(chipValues.getStyle() + "; -fx-font-weight: bold;");
    }

    public final void alignment(Pos alignment) {
        chipValues.setStyle("-fx-alignment: " + alignment.name().replace("_", "-"));
    }

    public final void expanded() {
        HBox.setHgrow(root, Priority.ALWAYS);
        VBox.setVgrow(root, Priority.ALWAYS);
    }

    public final void setVisible(boolean value) {
        root.setVisible(value);
    }

    public final void tooltip(String text) {
        chipValues.setTooltip(new Tooltip(text));
    }

    public final void id(String value) {
        chipValues.setId(value);
    }

    public final void focusTraversable(Boolean value) {
        focusTraversable.set(value);
    }

    public final void clear() {
        chipValues.getChips().clear();
    }

    public void requestFocus() {
        chipValues.requestFocus();
    }

    public final void keyReleased(EventHandler<? super KeyEvent> value) {
        chipValues.setOnKeyReleased(value);
    }

    public final void keyPressed(EventHandler<? super KeyEvent> value) {
        chipValues.setOnKeyPressed(value);
    }

    public final void mouseClicked(EventHandler<? super MouseEvent> value) {
        chipValues.setOnMouseClicked(value);
    }

    public final FormFieldChip withoutTitle() {
        root.getChildren().remove(0);
        return this;
    }

    public final void add(T... itens) {
        chipValues.getChips().addAll(Arrays.asList(itens));
    }

    public final ObservableList<T> itens() {
        return chipValues.getChips();
    }

    public final void focusedListener(ChangeListener<Boolean> listener) {
        chipValues.focusedProperty().addListener(listener);
    }

    public final void setPadding(double allSides) {
        root.setPadding(new Insets(allSides));
        chipValues.setPadding(new Insets(allSides));
    }

    public final void setPadding(double top, double right, double bottom, double left) {
        root.setPadding(new Insets(top, right, bottom, left));
        chipValues.setPadding(new Insets(top, right, bottom, left));
    }

    public final void setPadding(Insets insets) {
        root.setPadding(insets);
    }

    public void invisibleTitle() {
        this.title.setVisible(false);
    }

    public final void clickable(boolean clickable) {
        this.chipValues.setMouseTransparent(!clickable);
    }

    public final VBox build() {
        return root;
    }
}