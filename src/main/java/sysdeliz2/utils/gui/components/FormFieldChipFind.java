package sysdeliz2.utils.gui.components;

import com.jfoenix.controls.JFXChip;
import com.jfoenix.controls.JFXChipView;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FormFieldChipFind<T> {

    // components section
    private final VBox root;
    private final HBox boxChip;
    public final Label title;
    public final JFXChipView<T> chipView;

    // logic section
    private final Class<T> tClass;

    // fx attributes section
    public final ListProperty<T> objectValues = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final StringProperty textValue = new SimpleStringProperty();
    public final BooleanProperty postSelected = new SimpleBooleanProperty(false);
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final StringProperty codeReference = new SimpleStringProperty("codigo");
    public final List<DefaultFilter> defaults = new ArrayList<>();

    public FormFieldChipFind(Class classType) {
        
        tClass = classType;
        
        root = new VBox();
        boxChip = new HBox();
        title = new Label();
        HBox boxField = new HBox();
        chipView = new JFXChipView<>();
        Button find = new Button();
        
        root.getStylesheets().add("/styles/stylePadrao.css");
        root.setPrefWidth(150);
    
        this.getCodeReference();
        root.getStyleClass().add("chip-form");
        
        objectValues.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                textValue.set(newValue.stream().map(t -> ((BasicModel) t).codigoFilterProperty().getValue()).collect(Collectors.joining(",")));
                postSelected.set(!postSelected.get());
            }
        });

        title.getStyleClass().add("form-field");
        
        chipView.setPrefWidth(120.0);
        chipView.getStyleClass().add("chip-form");
        chipView.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        chipView.disableProperty().bind(editable.not());
        chipView.focusTraversableProperty().bind(editable);
        chipView.setConverter(new StringConverter<T>() {
            @Override
            public String toString(T object) {
                return ((BasicModel) object).getCodigoFilter();
            }

            @Override
            public T fromString(String string) {
                return new FluentDao().selectFrom(classType).where(eb -> eb.equal("codcli", string)).singleResult();
            }
        });
        chipView.getChips().addListener((ListChangeListener<T>) c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    for (T item : c.getAddedSubList()) {
                        if (item != null) {
                            objectValues.add(item);
                        }
                    }
                }

                if (c.wasRemoved()) {
                    for (T item : c.getRemoved()) {
                        if (item != null) {
                            objectValues.remove(item);
                        }
                    }
                }
            }
        });
        
        find.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        find.setMnemonicParsing(false);
        find.setOnAction(this::findOnAction);
        find.setPrefWidth(30.0);
        find.getStyleClass().add("warning");
        find.getStyleClass().add("last");
        find.setText("Procurar");
        find.setTooltip(new Tooltip("Procurar"));
        find.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
        find.disableProperty().bind(editable.not());

        boxField.getChildren().add(chipView);
        boxField.getChildren().add(find);
        
        root.getChildren().add(title);
        root.getChildren().add(boxField);
        root.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                chipView.requestFocus();
        });
    }
    
    private void getCodeReference(){
        Method[] methods = tClass.getDeclaredMethods();
        Field[] fields = tClass.getDeclaredFields();
        for (Method method : methods) {
            Id idAnnotation = method.getAnnotation(Id.class);
            if (idAnnotation != null){
                codeReference.set(method.getName().substring(3,4).toLowerCase().concat(method.getName().replaceAll("get[A-Z]","")));
                return;
            }
        }
        for (Field field : fields) {
            Id idAnnotation = field.getAnnotation(Id.class);
            if (idAnnotation != null){
                codeReference.set(field.getName());
                return;
            }
        }
    }
    
    private void codeOnKeyReleased(KeyEvent keyEvent) {
        if (editable.get()) {
            if (keyEvent.getCode() == KeyCode.F4)
                findOnAction(null);
        }
    }
    
    private void codeOnKeyPressed(KeyEvent keyEvent) {
    }
    
    public void findOnAction(ActionEvent actionEvent) {
        GenericFilter<T> findObject = null;
        try {
            findObject = new GenericFilter<T>(tClass, defaults);
            findObject.show(ResultTypeFilter.MULTIPLE_RESULT);
            objectValues.set(findObject.selectedsReturn);
            if (objectValues.size() > 0)
                postSelected.set(true);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    public static final FormFieldChipFind create(Class classType) {
        return new FormFieldChipFind(classType);
    }
    
    public static final <T> FormFieldChipFind create(Class classType, Consumer<FormFieldChipFind> value) {
        FormFieldChipFind<T> box = new FormFieldChipFind(classType);
        value.accept(box);
        return box;
    }
    
    public final FormFieldChipFind width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldChipFind title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldChipFind icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldChipFind withoutTitle() {
        this.root.getChildren().remove(this.title);
        return this;
    }
    
    public final FormFieldChipFind editable(Boolean value) {
        editable.set(value);
        return this;
    }
    
    public final FormFieldChipFind value(T value) {
        chipView.getChips().clear();
        if (value != null)
            chipView.getChips().add(value);
        return this;
    }
    
    public final Boolean isEmpty() {
        return chipView.getChips().size() == 0;
    }
    
    public final FormFieldChipFind keyReleased(EventHandler<? super KeyEvent> value) {
        chipView.setOnKeyReleased(value);
        return this;
    }
    
    public final T first() {
        return chipView.getChips().get(0);
    }
    
    public final void focusTraversable(Boolean value){
        chipView.setFocusTraversable(value);
    }
    
    public final FormFieldChipFind postSelected(ChangeListener<Boolean> listener) {
        postSelected.addListener(listener);
        return this;
    }
    
    public final void clear() {
        chipView.getChips().clear();
    }
    
    public final FormFieldChipFind<T> validate() throws FormValidationException {
        if (chipView.getChips().isEmpty())
            throw new FormValidationException("O campo " + title.getText() + " está vazio");
        return this;
    }
    
    public final String[] splitToSql(){
        return textValue.getValue() == null ? null : textValue.getValue().split(",");
    }

    public final VBox getRoot() {
        return root;
    }
    
    public final VBox build() {
        return root;
    }
}
