package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import sysdeliz2.models.sysdeliz.SdStatusProduto;
import sysdeliz2.utils.sys.ImageUtils;

import java.util.function.Consumer;
import java.util.function.Function;

public class FormFieldComboBox<T> {

    private final ComboBox<T> comboBox = new ComboBox();

    // fx components section
    public final ListProperty<T> items = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final ObjectProperty<T> value = this.comboBox.valueProperty(); //new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);

    // components section
    private final VBox root;
    private final Label title;
    private final HBox boxNodes;
    private final FormButton btnAdd = FormButton.create(btn -> {
        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        btn.disable.bind(this.editable.not());
    });
    
    // logic section
    private final Class<T> tClass;

    public ChangeListener<? super T> listener = (ChangeListener<T>) (observable, oldValue, newValue) -> {

    };
    
    public FormFieldComboBox(Class classType) {
    
        tClass = classType;
        
        root = new VBox();
        boxNodes = new HBox();
        title = new Label();
    
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        VBox.setVgrow(boxNodes, Priority.ALWAYS);

        title.getStyleClass().add("form-field");
        title.setText("Title");
        
        comboBox.setPrefWidth(150.0);
        comboBox.getStyleClass().add("form-field");
        comboBox.getStyleClass().add("corner-up-left");
        comboBox.setEditable(false);
        comboBox.itemsProperty().bind(items);
//        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//            if(newValue != null)
//                value.set(newValue);
//        });
        comboBox.disableProperty().bind(editable.not());
//        comboBox.valueProperty().bindBidirectional(value);

        value.addListener(listener);

        root.getChildren().add(title);
        boxNodes.getChildren().add(comboBox);
        root.getChildren().add(boxNodes);
    }
    
    public static final FormFieldComboBox create(Class classType){
        return new FormFieldComboBox(classType);
    }
    
    public static final <T> FormFieldComboBox create(Class classType, Consumer<FormFieldComboBox> value){
        FormFieldComboBox<T> box = new FormFieldComboBox(classType);
        value.accept(box);
        return box;
    }
    
    public final FormFieldComboBox width(double width) {
        comboBox.setPrefWidth(width);
//        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldComboBox title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldComboBox icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldComboBox select(int index) {
//        value.set(null);
        comboBox.getSelectionModel().select(index);
//        value.set(comboBox.getValue());
        return this;
    }

    public final FormFieldComboBox clearSelection() {
        value.set(null);
        comboBox.getSelectionModel().clearSelection();
        return this;
    }

    public final FormFieldComboBox select(T object) {
//        value.set(null);
        comboBox.getSelectionModel().select(object);
//        value.set(comboBox.getValue());
        return this;
    }
    
    public final FormFieldComboBox items(ObservableList<T> items) {
        this.items.set(items);
        return this;
    }
    
    public final FormFieldComboBox addStyle(String styleClass){
        comboBox.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldComboBox removeStyle(String styleClass){
        comboBox.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldComboBox withoutTitle() {
        root.getChildren().remove(0);
        return this;
    }

    public final FormFieldComboBox withAddButton(Function function) {
        btnAdd.setAction(evt -> function.apply(void.class));
        boxNodes.getChildren().add(0, btnAdd);
        return this;
    }

    public final FormFieldComboBox promptText(String text) {
        comboBox.setPromptText(text);
        return this;
    }

    public final void converter(StringConverter<T> converter) {
        comboBox.setConverter(converter);
    }

    public final void editable(Boolean value) {
        comboBox.setEditable(value);
    }

    public final void clear() {
        items.clear();
        value.set(null);
    }

    public final void addListener(ChangeListener<? super T> listener) {
        this.value.addListener(listener);
        this.listener = listener;
    }

    public void removeListener(ChangeListener listener) {
        value.removeListener(listener);
    }

    public final void getSelectionModel(ChangeListener<? super T> listener) {
        value.addListener(listener);
    }

    public final void setSelectionModelListener(ChangeListener<? super T> listener) {
        this.listener = listener;
        comboBox.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    public ComboBox<T> getComboBox() {
        return comboBox;
    }

    public final VBox build() {
        return root;
    }
}