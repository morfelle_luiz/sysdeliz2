package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.ValidationForm;

import java.time.LocalDate;
import java.util.function.Consumer;

public class FormFieldDate {
    
    // components section
    private final VBox root;
    public final Label title;
    public final DatePicker datePicker;
    
    // fx components section
    public final ObjectProperty<LocalDate> value = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    
    public FormFieldDate() {
        
        root = new VBox();
        title = new Label();
        datePicker = new DatePicker();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        title.getStyleClass().add("form-field");
        title.setText("Title");
        
        datePicker.setPrefWidth(120.0);
        datePicker.getStyleClass().add("form-field");
        datePicker.valueProperty().bindBidirectional(value);
        datePicker.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                datePicker.setValue(LocalDate.now());
        });
        datePicker.disableProperty().bind(editable.not());

        root.disableProperty().bind(disable);
        root.getChildren().add(title);
        root.getChildren().add(datePicker);
    }
    
    public static final FormFieldDate create(){
        return new FormFieldDate();
    }
    
    public static final FormFieldDate create(Consumer<FormFieldDate> value){
        FormFieldDate box = new FormFieldDate();
        value.accept(box);
        return box;
    }
    
    public final FormFieldDate width(double width) {
        root.setPrefWidth(width);
        datePicker.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldDate title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldDate icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldDate defaultValue(LocalDate value) {
        datePicker.setValue(value);
        return this;
    }
    
    public final FormFieldDate addStyle(String styleClass){
        datePicker.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldDate removeStyle(String styleClass){
        datePicker.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldDate withoutTitle() {
        this.root.getChildren().remove(title);
        return this;
    }
    
    public final FormFieldDate validate() throws FormValidationException {
        ValidationForm.validationEmpty(this);
        return this;
    }
    
    public final VBox build() {
        return root;
    }
}