package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.time.LocalDate;
import java.util.function.Consumer;

public class FormFieldDatePeriod {

    // components section
    private final VBox root;
    private final HBox boxDate;
    private final VBox boxVDate;
    private final Label title;
    private final DatePicker datePickerBegin;
    private final DatePicker datePickerEnd;
    private final Label labelBegin;
    private final Label labelEnd;

    // fx components section
    public final ObjectProperty<LocalDate> valueBegin = new SimpleObjectProperty<>();
    public final ObjectProperty<LocalDate> valueEnd = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final BooleanProperty vertical = new SimpleBooleanProperty(false);

    public FormFieldDatePeriod() {

        root = new VBox();
        boxDate = new HBox();
        boxVDate = new VBox();
        title = new Label();
        datePickerBegin = new DatePicker();
        datePickerEnd = new DatePicker();
        labelBegin = new Label();
        labelEnd = new Label();

        root.getStylesheets().add("/styles/sysDelizDesktop.css");

        title.getStyleClass().add("form-field");
        title.setText("Title");

        datePickerBegin.setId("date-picker-begin");
        datePickerBegin.setPrefWidth(120.0);
        datePickerBegin.getStyleClass().add("form-field");
        datePickerBegin.valueProperty().bindBidirectional(valueBegin);
        datePickerBegin.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                datePickerBegin.setValue(LocalDate.now());
        });
        datePickerBegin.disableProperty().bind(editable.not());

        datePickerEnd.setId("date-picker-end");
        datePickerEnd.setPrefWidth(120.0);
        datePickerEnd.getStyleClass().add("form-field");
        datePickerEnd.valueProperty().bindBidirectional(valueEnd);
        datePickerEnd.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                datePickerEnd.setValue(LocalDate.now());
            if (event.getCode() == KeyCode.C)
                datePickerEnd.setValue(datePickerBegin.getValue());
        });
        datePickerEnd.disableProperty().bind(editable.not());

        labelBegin.setMaxHeight(Double.MAX_VALUE);
        labelBegin.setMaxWidth(Double.MAX_VALUE);
        labelBegin.getStyleClass().add("input-group-prepend");
        labelBegin.setMinWidth(18.0);
        labelBegin.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        labelBegin.setText("De: ");

        labelEnd.setMaxHeight(Double.MAX_VALUE);
        labelEnd.setMaxWidth(Double.MAX_VALUE);
        labelEnd.getStyleClass().add("input-group-prepend");
        labelEnd.setMinWidth(18.0);
        labelEnd.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        labelEnd.setText("Até: ");

        boxDate.setSpacing(3.0);
        boxDate.setPadding(new Insets(3.0));
        boxDate.getStyleClass().add("corner-up-left");

        boxVDate.setSpacing(3.0);
        boxVDate.setPadding(new Insets(3.0));
        boxVDate.getStyleClass().add("corner-up-left");

        boxDate.getChildren().add(datePickerBegin);
        boxDate.getChildren().add(datePickerEnd);
        root.getChildren().add(title);
        root.getChildren().add(boxDate);
    }

    public static final FormFieldDatePeriod create() {
        return new FormFieldDatePeriod();
    }

    public static final FormFieldDatePeriod create(Consumer<FormFieldDatePeriod> value) {
        FormFieldDatePeriod box = new FormFieldDatePeriod();
        value.accept(box);
        return box;
    }

    public final FormFieldDatePeriod width(double width) {
        root.setPrefWidth(width);
        return this;
    }

    public final FormFieldDatePeriod title(String title) {
        this.title.setText(title);
        return this;
    }

    public final void setId(String id) {
        root.setId(id);
    }

    public final void verticalMode() {
        this.boxDate.getChildren().clear();

        this.boxVDate.getChildren().addAll(datePickerBegin, datePickerEnd);
        this.root.getChildren().remove(boxDate);
        this.root.getChildren().add(boxVDate);

        vertical.set(true);
    }

    public final FormFieldDatePeriod icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }

    public final FormFieldDatePeriod defaultValue(LocalDate begin, LocalDate end) {
        datePickerBegin.setValue(begin);
        datePickerEnd.setValue(end);
        return this;
    }

    public final FormFieldDatePeriod addStyle(String styleClass) {
        datePickerBegin.getStyleClass().add(styleClass);
        datePickerEnd.getStyleClass().add(styleClass);
        return this;
    }

    public final FormFieldDatePeriod removeStyle(String styleClass) {
        datePickerBegin.getStyleClass().remove(styleClass);
        datePickerEnd.getStyleClass().remove(styleClass);
        return this;
    }

    public final FormFieldDatePeriod withoutTitle() {
        this.title.setVisible(false);
        return this;
    }

    public final boolean validateExistPeriodo() {
        return datePickerBegin.getValue() != null && datePickerEnd.getValue() != null;
    }

    public final boolean validateEndGtBegin() {
        return datePickerBegin.getValue().isBefore(datePickerEnd.getValue()) || datePickerBegin.getValue().isEqual(datePickerEnd.getValue());
    }

    public final void clear() {
        datePickerBegin.setValue(null);
        datePickerEnd.setValue(null);
        valueBegin.set(null);
        valueEnd.set(null);
    }

    public void setPadding(Insets insets) {
        root.setPadding(insets);
    }

    public final FormFieldDatePeriod withLabels() {

        HBox boxBegin = new HBox();
        HBox boxEnd = new HBox();

        boxDate.getChildren().remove(datePickerBegin);
        boxDate.getChildren().remove(datePickerEnd);

        HBox.setHgrow(boxBegin, Priority.ALWAYS);
        HBox.setHgrow(boxEnd, Priority.ALWAYS);

//        boxBegin.setPrefWidth(0.0);
//        boxEnd.setPrefWidth(0.0);

        datePickerBegin.getStyleClass().add("last");
        datePickerEnd.getStyleClass().add("last");

        boxBegin.getChildren().add(labelBegin);
        boxBegin.getChildren().add(datePickerBegin);

        boxEnd.getChildren().add(labelEnd);
        boxEnd.getChildren().add(datePickerEnd);

        if (vertical.get()) {
            boxVDate.getChildren().add(boxBegin);
            boxVDate.getChildren().add(boxEnd);
        } else {
            boxDate.getChildren().add(boxBegin);
            boxDate.getChildren().add(boxEnd);
        }
        return this;
    }

    public final VBox build() {
        return root;
    }

    public DatePicker getDatePickerBegin() {
        return datePickerBegin;
    }
}