package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.function.Consumer;

public class FormFieldDateTime {
    
    // components section
    private final VBox root;
    private final Label title;
    private final TextField datePicker;
    
    // fx components section
    public final ObjectProperty<LocalDateTime> value = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    
    // logic
    private String format = "dd/MM/yyyy HH:mm:ss";
    
    public FormFieldDateTime() {
        
        root = new VBox();
        title = new Label();
        datePicker = new TextField();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setPrefWidth(140.0);

        title.getStyleClass().add("form-field");
        title.setText("Title");

        value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                datePicker.setText(value.getValue().format(DateTimeFormatter.ofPattern(format)));
            }
        });
        
        TextFieldUtils.dateTimeMask(datePicker);
        datePicker.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                try {
                    value.set(LocalDateTime.parse(newValue, DateTimeFormatter.ofPattern(format)));
                }catch (DateTimeParseException e){
                    value.set(null);
                }
            }
        });
        datePicker.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                datePicker.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern(format)));
        });
        datePicker.disableProperty().bind(editable.not());
        datePicker.getStyleClass().add("form-field");
        
        root.getChildren().add(title);
        root.getChildren().add(datePicker);
    }
    
    public static final FormFieldDateTime create(){
        return new FormFieldDateTime();
    }
    
    public static final FormFieldDateTime create(Consumer<FormFieldDateTime> value){
        FormFieldDateTime box = new FormFieldDateTime();
        value.accept(box);
        return box;
    }
    
    public final FormFieldDateTime title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldDateTime icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldDateTime addStyle(String styleClass){
        datePicker.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldDateTime removeStyle(String styleClass){
        datePicker.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldDateTime withoutTitle() {
        this.title.setVisible(false);
        return this;
    }
    
    public final void format(String format){
        this.format = format;
    }
    
    public final VBox build() {
        return root;
    }
    
}
