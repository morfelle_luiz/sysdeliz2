package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.function.Consumer;

public class FormFieldDateTimePeriod {
    
    // components section
    private final VBox root;
    private final HBox boxPeriod;
    private final Label title;
    private final TextField beginPicker;
    private final HBox beginBox;
    private final Label labelBegin;
    private final TextField endPicker;
    private final HBox endBox;
    private final Label labelEnd;
    
    // fx components section
    public final ObjectProperty<LocalDateTime> beginValue = new SimpleObjectProperty<>();
    public final ObjectProperty<LocalDateTime> endValue = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    
    // logic
    private String format = "dd/MM/yyyy HH:mm:ss";
    
    public FormFieldDateTimePeriod() {
        
        root = new VBox();
        boxPeriod = new HBox();
        title = new Label();
        beginPicker = new TextField();
        beginBox = new HBox();
        labelBegin = new Label("De");
        endPicker = new TextField();
        endBox = new HBox();
        labelEnd = new Label("Até");
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        boxPeriod.setSpacing(5.0);
        boxPeriod.setPadding(new Insets(2.0));
        boxPeriod.getStyleClass().add("corner-up-left");
        
        title.getStyleClass().add("form-field");
        title.setText("Title");
    
        labelBegin.setMaxHeight(Double.MAX_VALUE);
        labelBegin.setMaxWidth(Double.MAX_VALUE);
        labelBegin.getStyleClass().add("input-group-prepend");
        labelBegin.setMinWidth(18.0);
        labelBegin.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        beginValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                beginPicker.setText(beginValue.getValue().format(DateTimeFormatter.ofPattern(format)));
            }
        });
        TextFieldUtils.dateTimeMask(beginPicker);
        beginPicker.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                try {
                    beginValue.set(LocalDateTime.parse(newValue, DateTimeFormatter.ofPattern(format)));
                }catch (DateTimeParseException e){
                    beginValue.set(null);
                }
            }
        });
        beginPicker.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                beginPicker.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern(format)));
        });
        beginPicker.disableProperty().bind(editable.not());
        beginPicker.editableProperty().bind(editable);
        beginPicker.getStyleClass().add("last");
        HBox.setHgrow(beginPicker, Priority.ALWAYS);
        beginBox.getChildren().add(labelBegin);
        beginBox.getChildren().add(beginPicker);
    
        labelEnd.setMaxHeight(Double.MAX_VALUE);
        labelEnd.setMaxWidth(Double.MAX_VALUE);
        labelEnd.getStyleClass().add("input-group-prepend");
        labelEnd.setMinWidth(18.0);
        labelEnd.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        endValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                endPicker.setText(endValue.getValue().format(DateTimeFormatter.ofPattern(format)));
            }
        });
        TextFieldUtils.dateTimeMask(endPicker);
        endPicker.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                try {
                    endValue.set(LocalDateTime.parse(newValue, DateTimeFormatter.ofPattern(format)));
                }catch (DateTimeParseException e){
                    endValue.set(null);
                }
            }
        });
        endPicker.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.T)
                endPicker.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern(format)));
            else if (event.getCode() == KeyCode.C)
                endPicker.setText(beginValue.getValue().format(DateTimeFormatter.ofPattern(format)));
        });
        endPicker.disableProperty().bind(editable.not());
        endPicker.editableProperty().bind(editable);
        endPicker.getStyleClass().add("last");
        HBox.setHgrow(endPicker, Priority.ALWAYS);
        endBox.getChildren().add(labelEnd);
        endBox.getChildren().add(endPicker);
    
        boxPeriod.getChildren().add(beginBox);
        boxPeriod.getChildren().add(endBox);
        root.getChildren().add(title);
        root.getChildren().add(boxPeriod);
    }
    
    public static final FormFieldDateTimePeriod create(){
        return new FormFieldDateTimePeriod();
    }
    
    public static final FormFieldDateTimePeriod create(Consumer<FormFieldDateTimePeriod> value){
        FormFieldDateTimePeriod box = new FormFieldDateTimePeriod();
        value.accept(box);
        return box;
    }
    
    public final FormFieldDateTimePeriod title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldDateTimePeriod icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldDateTimePeriod addStyle(String styleClass){
        beginPicker.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldDateTimePeriod removeStyle(String styleClass){
        beginPicker.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldDateTimePeriod withoutTitle() {
        this.title.setVisible(false);
        return this;
    }
    
    public final void format(String format){
        this.format = format;
    }
    
    public final void clear(){
        beginValue.set(null);
        endValue.set(null);
        beginPicker.clear();
        endPicker.clear();
    }
    
    public final VBox build() {
        return root;
    }
    
}
