package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.function.Consumer;

public class FormFieldImage {

    // components section
    private final VBox root;
    private final Label title;
    public final VBox boxField;
    public final ImageView imageView;
    private final ScrollPane scrollPane;

    // fx components section
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    public final StringProperty pathImage = new SimpleStringProperty(null);

    public FormFieldImage() {
        
        root = new VBox();
        title = new Label();
        boxField = new VBox();
        imageView = new ImageView();
        scrollPane = new ScrollPane();
        
        root.getStylesheets().add("/styles/stylePadrao.css");
        
        title.getStyleClass().add("form-field");

        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        VBox.setVgrow(scrollPane, javafx.scene.layout.Priority.ALWAYS);

        boxField.getStyleClass().add("corner-up-left");
        boxField.setPadding(new Insets(3.0));
        boxField.getChildren().add(imageView);
        boxField.disableProperty().bind(disable);
        VBox.setVgrow(boxField, Priority.ALWAYS);

        imageView.setPreserveRatio(true);
        VBox.setVgrow(imageView, Priority.ALWAYS);

        root.getChildren().add(title);
        root.getChildren().add(boxField);
    }
    
    public static final FormFieldImage create(){
        return new FormFieldImage();
    }
    
    public static final FormFieldImage create(Consumer<FormFieldImage> value) {
        FormFieldImage box = new FormFieldImage();
        value.accept(box);
        return box;
    }

    public final void size(double width, double height) {
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
    }

    public final void width(double width) {
        imageView.setFitWidth(width);
        imageView.setPreserveRatio(true);
    }

    public final void height(double height) {
        imageView.setFitHeight(height);
        imageView.setPreserveRatio(true);
    }

    public void title(String title) {
        this.title.setText(title);
    }
    
    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
    }

    public final void expanded(){
        HBox.setHgrow(root, Priority.ALWAYS);
        VBox.setVgrow(root, Priority.ALWAYS);
    }

    public final void fitWidth(){
        imageView.fitWidthProperty().bind(boxField.prefWidthProperty());
    }

    public final void fitWidth(Double value){
        imageView.setFitWidth(value);
    }

    public final void fitHeight(){
        imageView.fitHeightProperty().bind(boxField.heightProperty());
    }
    public final void fitHeight(Double value){
        imageView.setFitHeight(value);
    }

    public final void withScrolls() {
        boxField.setMaxWidth(Double.MAX_VALUE);
        boxField.setMaxHeight(Double.MAX_VALUE);
        boxField.minWidthProperty().bind(scrollPane.widthProperty().subtract(5));
        boxField.minHeightProperty().bind(scrollPane.heightProperty().subtract(5));
        root.getChildren().remove(boxField);
        scrollPane.setContent(boxField);
        root.getChildren().add(scrollPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }

    public final void tooltip(String text) {
        title.setTooltip(new Tooltip(text));
    }

    public final void tooltip(ObservableValue<? extends String> observable) {
        Tooltip tooltip = new Tooltip();
        tooltip.textProperty().bind(observable);
        title.setTooltip(tooltip);
    }
    
    public final void withoutTitle() {
        root.getChildren().remove(0);
    }

    public final void clear() {
        imageView.setImage(null);
    }

    public final void image(String url) throws IOException {
        pathImage.set(url);
        FileInputStream fileImage = new FileInputStream(url);
        imageView.setImage(new Image(fileImage));
        fileImage.close();
    }

    public final void image(Image image) {
        pathImage.set(image.impl_getUrl());
        imageView.setImage(image);
    }

    public final void image(File file) throws IOException {
        pathImage.set(file.getAbsolutePath());
        FileInputStream fileImage = new FileInputStream(file);
        imageView.setImage(new Image(fileImage));
        fileImage.close();
    }

    public final void image(Image image, String url) throws IOException {
        pathImage.set(url);
        imageView.setImage(image);
    }

    public final void alignment(Pos pos){
        boxField.setAlignment(pos);
    }

    public final void mouseClicked(EventHandler<? super MouseEvent> value) {
        imageView.setOnMouseClicked(value);
    }

    public final VBox build() {
        return root;
    }
}
