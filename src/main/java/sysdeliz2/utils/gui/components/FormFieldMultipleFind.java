package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FormFieldMultipleFind<T> {
    
    // components section
    private final VBox root;
    public final Label title;
    private final HBox boxField;
    public final TextField code;
    private final Button find;
    
    // logic section
    private final Class<T> tClass;
    
    // fx attributes section
    public final ListProperty<T> objectValues = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final StringProperty textValue = new SimpleStringProperty();
    public final BooleanProperty postSelected = new SimpleBooleanProperty(false);
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final StringProperty codeReference = new SimpleStringProperty("codigo");
    public final List<DefaultFilter> defaults = new ArrayList<>();
    
    public FormFieldMultipleFind(Class classType) {
        
        tClass = classType;
        
        root = new VBox();
        title = new Label();
        boxField = new HBox();
        code = new TextField();
        find = new Button();
        
        root.getStylesheets().add("/styles/stylePadrao.css");
        root.setPrefWidth(150);
    
        this.getCodeReference();
        
        objectValues.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                textValue.set(newValue.stream().map(t -> ((BasicModel) t).codigoFilterProperty().getValue()).collect(Collectors.joining(",")));
                postSelected.set(!postSelected.get());
            }
        });

        if (BasicModel.class.isAssignableFrom(tClass)) {
            try {
                title.setText(tClass.getAnnotation(TelaSysDeliz.class).descricao());
            } catch (Exception ignored) {

            }
        }

        title.getStyleClass().add("form-field");
        
        code.setOnKeyReleased(this::codeOnKeyReleased);
        code.setPrefWidth(120.0);
        code.getStyleClass().add("input-group-apend");
        code.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        code.setTooltip(new Tooltip("Pressione F4 para procurar"));
        code.textProperty().bindBidirectional(textValue);
        code.editableProperty().bind(editable);
        code.focusTraversableProperty().bind(editable);
        code.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (code.getText() != null && !code.getText().equals(""))
                objectValues.set(FXCollections.observableList((List<T>) new FluentDao().selectFrom(tClass).where(whr -> whr.isIn(codeReference.getValue(), code.getText().split(","))).resultList()));
        });
        
        find.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        find.setMnemonicParsing(false);
        find.setOnAction(this::findOnAction);
        find.setPrefWidth(30.0);
        find.getStyleClass().add("warning");
        find.getStyleClass().add("last");
        find.setText("Procurar");
        find.setTooltip(new Tooltip("Procurar"));
        find.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
        find.disableProperty().bind(editable.not());
        
        boxField.getChildren().add(code);
        boxField.getChildren().add(find);
        
        root.getChildren().add(title);
        root.getChildren().add(boxField);
        root.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                code.requestFocus();
        });
    }
    
    private void getCodeReference(){
        Method[] methods = tClass.getDeclaredMethods();
        Field[] fields = tClass.getDeclaredFields();
        for (Method method : methods) {
            Id idAnnotation = method.getAnnotation(Id.class);
            if (idAnnotation != null){
                codeReference.set(method.getName().substring(3,4).toLowerCase().concat(method.getName().replaceAll("get[A-Z]","")));
                return;
            }
        }
        for (Field field : fields) {
            Id idAnnotation = field.getAnnotation(Id.class);
            if (idAnnotation != null){
                codeReference.set(field.getName());
                return;
            }
        }
    }
    
    private void codeOnKeyReleased(KeyEvent keyEvent) {
        if (editable.get()) {
            if (keyEvent.getCode() == KeyCode.F4)
                findOnAction(null);
        }
    }
    
    private void codeOnKeyPressed(KeyEvent keyEvent) {
    }
    
    public void findOnAction(ActionEvent actionEvent) {
        GenericFilter<T> findObject = null;
        try {
            findObject = new GenericFilter<T>(tClass, defaults);
            findObject.show(ResultTypeFilter.MULTIPLE_RESULT);
            objectValues.set(findObject.selectedsReturn);
            if (objectValues.size() > 0)
                postSelected.set(true);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }
    
    public static final FormFieldMultipleFind create(Class classType) {
        return new FormFieldMultipleFind(classType);
    }
    
    public static final <T> FormFieldMultipleFind create(Class classType, Consumer<FormFieldMultipleFind> value) {
        FormFieldMultipleFind<T> box = new FormFieldMultipleFind(classType);
        value.accept(box);
        return box;
    }
    
    public final FormFieldMultipleFind width(double width) {
        code.setPrefWidth(width);
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldMultipleFind title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldMultipleFind icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldMultipleFind addStyle(String styleClass) {
        code.getStyleClass().add(styleClass);
        find.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldMultipleFind removeStyle(String styleClass) {
        code.getStyleClass().remove(styleClass);
        find.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldMultipleFind toUpper() {
        TextFieldUtils.upperCase(code);
        return this;
    }
    
    public final FormFieldMultipleFind withoutTitle() {
        this.root.getChildren().remove(this.title);
        return this;
    }
    
    public final FormFieldMultipleFind editable(Boolean value) {
        editable.set(value);
        return this;
    }
    
    public final FormFieldMultipleFind limit(Integer value) {
        TextFieldUtils.maxField(code, value);
        return this;
    }
    
    public final FormFieldMultipleFind value(T value) {
        objectValues.clear();
        if (value != null)
            objectValues.add(value);
        return this;
    }
    
    public final Boolean isEmpty() {
        return objectValues.size() == 0;
    }
    
    public final FormFieldMultipleFind keyReleased(EventHandler<? super KeyEvent> value) {
        code.setOnKeyReleased(value);
        return this;
    }
    
    public final T first() {
        return objectValues.get(0);
    }
    
    public final void focusTraversable(Boolean value){
        code.setFocusTraversable(value);
    }
    
    public final FormFieldMultipleFind postSelected(ChangeListener<Boolean> listener) {
        postSelected.addListener(listener);
        return this;
    }
    
    public final void clear() {
        objectValues.clear();
        textValue.set(null);
    }
    
    public final FormFieldMultipleFind<T> validate() throws FormValidationException {
        ValidationForm.validationEmpty(this);
        return this;
    }
    
    public final String[] splitToSql(){
        return textValue.getValue() == null ? null : textValue.getValue().split(",");
    }

    public final VBox getRoot() {
        return root;
    }
    
    public final VBox build() {
        return root;
    }
}
