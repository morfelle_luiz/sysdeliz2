package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class FormFieldRadioButtonGroup<T> {
    
    private final VBox root;
    private final Label title;
    private final HBox boxHField;
    private final VBox boxVField;
    private final ToggleGroup groupRadios;
    private Boolean horizontalMode = false;
    
    // fx components section
    public final ListProperty<OptionRadioButton> items = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final ObjectProperty<OptionRadioButton> value = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    
    public FormFieldRadioButtonGroup() {
        
        root = new VBox();
        title = new Label();
        boxHField = new HBox();
        boxVField = new VBox();
        groupRadios = new ToggleGroup();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        title.getStyleClass().add("form-field");
        
        boxHField.setSpacing(5.0);
        boxHField.setPrefHeight(30.0);
        boxHField.getStyleClass().add("corner-up-left");
        boxHField.setPadding(new Insets(5.0));
        boxHField.disableProperty().bind(editable.not());
        
        boxVField.setSpacing(3.0);
        boxVField.getStyleClass().add("corner-up-left");
        boxVField.setPadding(new Insets(3.0));
        boxVField.disableProperty().bind(editable.not());
        
        root.getChildren().add(title);
        root.getChildren().add(boxVField);
        
    }
    
    public static final FormFieldRadioButtonGroup create() {
        return new FormFieldRadioButtonGroup();
    }
    
    public static final <T> FormFieldRadioButtonGroup create(Consumer<FormFieldRadioButtonGroup> value) {
        FormFieldRadioButtonGroup<T> box = new FormFieldRadioButtonGroup();
        value.accept(box);
        return box;
    }
    
    public final FormFieldRadioButtonGroup title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldRadioButtonGroup icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final void select(Integer index) {
        value.set(items.get(index));
        value.get().setSelected(true);
    }
    
    public final void horizontalMode() {
        horizontalMode = true;
        root.getChildren().remove(1);
        root.getChildren().add(boxHField);
        items.forEach(t -> {
            boxHField.getChildren().add(t);
        });
    }
    
    public final OptionRadioButton option(String title, T value) {
        return new OptionRadioButton(title, value);
    }
    
    public final OptionRadioButton option(String title, T value, Boolean selected) {
        return new OptionRadioButton(title, value, selected);
    }
    
    public final void items(OptionRadioButton... items) {
        this.items.clear();
        Arrays.asList(items).forEach(t -> {
            if (horizontalMode) {
                t.setPadding(new Insets(-4,0,1,0));
                boxHField.getChildren().add(t);
            } else
                boxVField.getChildren().add(t);
            this.items.add(t);
        });
        if (groupRadios.getSelectedToggle() == null)
            select(0);
    }
    
    public final void items(List<OptionRadioButton> items) {
        this.items.clear();
        items.forEach(t -> {
            if (horizontalMode) {
                t.setPadding(new Insets(-4,0,1,0));
                boxHField.getChildren().add(t);
            } else
                boxVField.getChildren().add(t);
            this.items.add(t);
        });
        if (groupRadios.getSelectedToggle() == null)
            select(0);
    }
    
    public final FormFieldRadioButtonGroup addStyle(String styleClass) {
        boxHField.getStyleClass().add(styleClass);
        boxVField.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldRadioButtonGroup removeStyle(String styleClass) {
        boxHField.getStyleClass().remove(styleClass);
        boxVField.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final void withoutTitle() {
        this.title.setVisible(false);
        boxHField.getStyleClass().remove("corner-up-left");
        boxVField.getStyleClass().remove("corner-up-left");
    }
    
    public final void configRadioButton(OptionRadioButton opt, Consumer<OptionRadioButton> value){
        value.accept(opt);
    }
    
    public final VBox build() {
        return root;
    }
    
    public class OptionRadioButton extends RadioButton {
        
        private String title = null;
        public T valueOption = null;
        private Boolean selected = false;
    
        public OptionRadioButton(String title, T value) {
            this.title = title;
            this.valueOption = value;
    
            createCheckBox();
        }
        
        public OptionRadioButton(String title, T value, Boolean selected) {
            this.title = title;
            this.valueOption = value;
            this.selected = selected;
            
            createCheckBox();
        }
        
        private void createCheckBox() {
            setText(title);
            setSelected(false);
            setMnemonicParsing(false);
            setToggleGroup(groupRadios);
            setSelected(this.selected);
            selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue)
                    value.set(this);
            });
        }
    }
}
