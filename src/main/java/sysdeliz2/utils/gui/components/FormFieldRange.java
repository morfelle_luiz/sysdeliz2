package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;

import java.util.function.Consumer;

public class FormFieldRange {

    // components section
    private final VBox root;
    private final HBox boxDate;
    private final Label title;
    private final FormFieldText fieldBegin;
    private final FormFieldText fieldEnd;

    // fx components section
    public final IntegerProperty valueBegin = new SimpleIntegerProperty();
    public final IntegerProperty valueEnd = new SimpleIntegerProperty();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);

    public FormFieldRange() {
        
        root = new VBox();
        boxDate = new HBox();
        title = new Label();
        fieldBegin = FormFieldText.create(field -> {
            field.withoutTitle();
            field.expanded();
            field.label("Min.");
            field.value.bindBidirectional(valueBegin, new NumberStringConverter());
            field.editable.bind(editable);
        });
        fieldEnd = FormFieldText.create(field -> {
            field.withoutTitle();
            field.expanded();
            field.label("Max.");
            field.value.bindBidirectional(valueEnd, new NumberStringConverter());
            field.editable.bind(editable);
            field.keyReleased(event -> {
                if (event.getCode() == KeyCode.C)
                    field.value.set(fieldBegin.value.get());
            });
        });
        
        root.getStylesheets().add("/styles/stylePadrao.css");
        
        title.getStyleClass().add("form-field");
        title.setText("");

        boxDate.setSpacing(3.0);
        boxDate.setPadding(new Insets(3.0));
        boxDate.getStyleClass().add("corner-up-left");
        
        boxDate.getChildren().add(fieldBegin.build());
        boxDate.getChildren().add(fieldEnd.build());
        root.getChildren().add(title);
        root.getChildren().add(boxDate);
    }
    
    public static final FormFieldRange create(){
        return new FormFieldRange();
    }
    
    public static final FormFieldRange create(Consumer<FormFieldRange> value){
        FormFieldRange box = new FormFieldRange();
        value.accept(box);
        return box;
    }
    
    public final FormFieldRange width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldRange title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldRange icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldRange defaultValue(Integer begin, Integer end) {
        fieldBegin.value.set(String.valueOf(begin));
        fieldEnd.value.set(String.valueOf(end));
        return this;
    }
    
    public final FormFieldRange addStyle(String styleClass){
        fieldBegin.addStyle(styleClass);
        fieldEnd.addStyle(styleClass);
        return this;
    }
    
    public final FormFieldRange removeStyle(String styleClass){
        fieldBegin.removeStyle(styleClass);
        fieldEnd.removeStyle(styleClass);
        return this;
    }
    
    public final FormFieldRange withoutTitle() {
        this.title.setVisible(false);
        return this;
    }
    
    public final boolean validateExistPeriodo(){
        return fieldBegin.value.get() != null && fieldEnd.value.get() != null;
    }
    
    public final boolean validateEndGtBegin(){
        return valueBegin.getValue().compareTo(valueEnd.getValue()) <= 0;
    }

    public FormFieldText getFieldBegin() {
        return fieldBegin;
    }

    public FormFieldText getFieldEnd() {
        return fieldEnd;
    }

    public final void clear(){
        fieldBegin.value.set("0");
        fieldEnd.value.set("100");
        valueBegin.set(0);
        valueEnd.set(100);
    }
    
    public final VBox build() {
        return root;
    }
}