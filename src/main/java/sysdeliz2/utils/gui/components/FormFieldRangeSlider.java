package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.control.RangeSlider;

import java.util.function.Consumer;

public class FormFieldRangeSlider extends VBox {

    // components section
    private final Label title;
    private final TextField textFieldLowValue;
    private final TextField textFieldHighValue;
    private final RangeSlider sliderValue;
    private final HBox hBox;
    private final HBox footerBox;
    private final VBox vBox;

    // fx components section
    public final DoubleProperty lowValue = new SimpleDoubleProperty(0.0);
    public final DoubleProperty highValue = new SimpleDoubleProperty(0.0);
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty focusTraversable = new SimpleBooleanProperty(true);

    public FormFieldRangeSlider() {

        sliderValue = new RangeSlider();
        title = new Label();
        textFieldLowValue = new TextField();
        textFieldHighValue = new TextField();
        hBox = new HBox();
        footerBox = new HBox();
        vBox = new VBox();

        getStylesheets().add("/styles/stylePadrao.css");

        title.getStyleClass().add("form-field");
        title.getStyleClass().add("label-form-field-v");
        title.setText("");

        configureTextField(textFieldLowValue, lowValue);
        configureTextField(textFieldHighValue, highValue);

        sliderValue.setOrientation(Orientation.HORIZONTAL);
        sliderValue.lowValueProperty().bindBidirectional(lowValue);
        sliderValue.highValueProperty().bindBidirectional(highValue);
//        sliderValue.setBlockIncrement(10);
        HBox.setHgrow(sliderValue, Priority.ALWAYS);
        VBox.setVgrow(sliderValue, Priority.ALWAYS);

        vBox.getStyleClass().add("corner-up-left");
        hBox.getStyleClass().add("corner-up-left");
        hBox.setPadding(new Insets(3.0));
        hBox.setSpacing(3);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(sliderValue);
        footerBox.getChildren().add(textFieldLowValue);
        footerBox.getChildren().add(textFieldHighValue);
        VBox.setVgrow(hBox, Priority.ALWAYS);

        getChildren().add(title);
        getChildren().add(hBox);
        getChildren().add(footerBox);
    }

    private void configureTextField(TextField textField, DoubleProperty valueToBind) {
        textField.setPrefWidth(100);
//        textField.getStyleClass().add("xs");
        textField.getStyleClass().add("form-field");
        textField.setStyle(textField.getStyle() + "; -fx-alignment: CENTER;");
        textField.textProperty().bindBidirectional(valueToBind, new NumberStringConverter());
        textField.editableProperty().bind(editable);
        textField.focusTraversableProperty().bind(editable.or(focusTraversable));
        textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                try {
                    Double value = Double.parseDouble(textField.getText().replace(".", "").replaceAll(",", "."));
                    if (value > sliderValue.getMax())
                        textField.setText(String.valueOf(sliderValue.getMax()));
                    else if (value < sliderValue.getMin())
                        textField.setText(String.valueOf(sliderValue.getMin()));

                } catch (NumberFormatException ex) {
                    textField.setText(String.valueOf(0));
                }
            }
        });
//        textField.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (newValue != null) {
//                try {
//                    Double value = Double.parseDouble(newValue.replace(".", "").replaceAll(",", "."));
//                    if (!textField.isFocused()) {
//                        if (value > sliderValue.getMax())
//                            textField.setText(String.valueOf(sliderValue.getMax()));
//                        else if (value < sliderValue.getMin())
//                            textField.setText(String.valueOf(sliderValue.getMin()));
//                    }
//
//                } catch (NumberFormatException ex) {
//                    textField.setText(String.valueOf(0));
//                }
//            }
//        });
        textField.setOnKeyReleased(event -> {
            if (event.getCode().equals(KeyCode.PLUS)
                    || event.getCode().equals(KeyCode.PAGE_UP)
                    || event.getCode().equals(KeyCode.UP)
                    || event.getCode().equals(KeyCode.KP_UP)) {
                sliderValue.incrementHighValue();
            } else if (event.getCode().equals(KeyCode.MINUS)
                    || event.getCode().equals(KeyCode.PAGE_DOWN)
                    || event.getCode().equals(KeyCode.DOWN)
                    || event.getCode().equals(KeyCode.KP_DOWN)) {
                sliderValue.decrementLowValue();
            }
        });
        editable.addListener((observable, oldValue, newValue) -> {
            if (!newValue)
                textField.getStyleClass().add("no-editable");
            else
                textField.getStyleClass().remove("no-editable");
        });
    }

    public static FormFieldRangeSlider create() {
        return new FormFieldRangeSlider();
    }

    public static FormFieldRangeSlider create(Consumer<FormFieldRangeSlider> value) {
        FormFieldRangeSlider box = new FormFieldRangeSlider();
        value.accept(box);
        return box;
    }

    public final FormFieldRangeSlider title(String title) {
        this.title.setText(title);
        return this;
    }

    public final FormFieldRangeSlider icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }

    public final FormFieldRangeSlider editable(Boolean value) {
        editable.set(value);
        return this;
    }

    public final FormFieldRangeSlider tickUnit(Double value) {
        sliderValue.setMajorTickUnit(value);
        return this;
    }

    public final FormFieldRangeSlider tickCount(Integer value) {
        sliderValue.setMinorTickCount(value);
        return this;
    }

    public final FormFieldRangeSlider showTickLabels(Boolean value) {
        sliderValue.setShowTickLabels(value);
        return this;
    }

    public final FormFieldRangeSlider showTickMarks(Boolean value, Double blockIncrement) {
        sliderValue.setShowTickMarks(value);
        sliderValue.setBlockIncrement(blockIncrement);
        return this;
    }

    public final FormFieldRangeSlider setRange(Double min, Double max) {
        sliderValue.setMin(min);
        sliderValue.setMax(max);

        return this;
    }

    public final FormFieldRangeSlider setRange(Double min, Double max, Double passo) {
        sliderValue.setMin(min);
        sliderValue.setMax(max);
        sliderValue.setBlockIncrement(passo);

        return this;
    }

    public final FormFieldRangeSlider integerIncrement(Integer increment) {
        lowValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                lowValue.set(newValue.intValue() * increment);
            }
        });

        return this;
    }

    public final FormFieldRangeSlider setOrientation(Orientation orientation) {
        getChildren().remove(1);
        sliderValue.setOrientation(orientation);
        if (orientation.equals(Orientation.VERTICAL)) {
            vBox.setPadding(new Insets(3.0));
            vBox.setSpacing(3);
            vBox.setAlignment(Pos.CENTER);
            vBox.getChildren().add(sliderValue);
            vBox.getChildren().add(textFieldLowValue);
            VBox.setVgrow(vBox, Priority.ALWAYS);
            getChildren().add(vBox);
        } else {
            hBox.setSpacing(3);
            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().add(sliderValue);
            hBox.getChildren().add(textFieldLowValue);
            VBox.setVgrow(hBox, Priority.ALWAYS);
            getChildren().add(hBox);
        }

        return this;
    }

    public final void setLowValue(Double lowValue) {
        this.lowValue.set(lowValue);
    }

    public RangeSlider getSlider() {
        return sliderValue;
    }

    public final void expanded() {
        HBox.setHgrow(this, Priority.ALWAYS);
        VBox.setVgrow(this, Priority.ALWAYS);
    }

    public final void focusTraversable(Boolean value) {
        focusTraversable.set(value);
    }

    public final void clear() {
        textFieldLowValue.clear();
        textFieldHighValue.clear();
        lowValue.set(0.0);
    }

    public void requestFocus() {
        textFieldLowValue.requestFocus();
    }

    public TextField getTextFieldLowValue() {
        return textFieldLowValue;
    }

    public TextField getTextFieldHighValue() {
        return textFieldHighValue;
    }

    public final FormFieldRangeSlider withoutTitle() {
        vBox.getStyleClass().remove("corner-up-left");
        hBox.getStyleClass().remove("corner-up-left");
        getChildren().remove(0);
        return this;
    }

}