package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.controlsfx.control.SegmentedBar;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.views.pcp.gestaomrp.mrptinturaria.MrpTinturariaExplosaoTab;

import javax.persistence.Id;
import javax.swing.text.Segment;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class FormFieldSegmentedBar extends FormBase {

    // components section
    public final VBox root;
    public final Label title;
    public final SegmentedBar<TypeSegment> segmentedBar;

    public FormFieldSegmentedBar() {

        root = new VBox();
        title = new Label();
        segmentedBar = new SegmentedBar();
        segmentedBar.setOrientation(Orientation.HORIZONTAL);
        segmentedBar.setSegmentViewFactory(TypeSegmentView::new);
        segmentedBar.setInfoNodeFactory(segment -> {
            Label label = new Label(segment.getText() + " : " + segment.getValue());
            label.setPadding(new Insets(2));
            return label;
        });

        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setPrefWidth(200);

        title.getStyleClass().add("form-field");

        root.getChildren().add(title);
        root.getChildren().add(segmentedBar);

    }

    public static final FormFieldSegmentedBar create() {
        return new FormFieldSegmentedBar();
    }

    public static final FormFieldSegmentedBar create(Consumer<FormFieldSegmentedBar> value) {
        FormFieldSegmentedBar box = new FormFieldSegmentedBar();
        value.accept(box);
        return box;
    }

    public final void width(double width) {
        root.setPrefWidth(width);
    }

    public final void title(String title) {
        this.title.setText(title);
    }

    public final void expanded() {
        HBox.setHgrow(root, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(root, javafx.scene.layout.Priority.ALWAYS);
    }

    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
    }

    public final void withoutTitle() {
        this.root.getChildren().remove(title);
    }

    public final void clear() {
        segmentedBar.getSegments().clear();
    }

    public final void orientation(Orientation orientation) {
        segmentedBar.setOrientation(orientation);
    }

    public final void addItem(String text, double value, Type type) {
        segmentedBar.getSegments().add(item(text, value, type));
    }

    public final void addItem(String text, double value, String hexaColor) {
        segmentedBar.getSegments().add(item(text, value, hexaColor));
    }

    public final void items(TypeSegment... items) {
        segmentedBar.getSegments().addAll(items);
    }

    public final TypeSegment item(String text, double value, Type type) {
        return new TypeSegment(value, text, type);
    }

    public final TypeSegment item(String text, double value, String hexaColor) {
        return new TypeSegment(value, text, hexaColor);
    }

    public final VBox build() {
        return root;
    }

    public enum Type { DANGER, SUCCESS, WARNING, INFO, PRIMARY, SECUNDARY, DARK, AMBER, DEFAULT }

    private class TypeSegmentView extends StackPane {

        private Label label;

        public TypeSegmentView(TypeSegment segment) {
            label = new Label();
            label.setStyle("-fx-font-weight: bold; -fx-text-fill: black; -fx-font-size: .9em;");
            label.setTextOverrun(OverrunStyle.ELLIPSIS);
            label.textProperty().bind(segment.textProperty().concat(" (").concat(segment.valueProperty().asString()).concat(")"));
            StackPane.setAlignment(label, Pos.CENTER_LEFT);

            getChildren().add(label);
            if (segment.hexaColor != null) {
                setStyle("-fx-background-color: "+segment.hexaColor+";");
            } else {
                switch (segment.type) {
                    case DANGER:
                        setStyle("-fx-background-color: #f5c6cb;");
                        break;
                    case SUCCESS:
                        setStyle("-fx-background-color: #c3e6cb;");
                        break;
                    case WARNING:
                        setStyle("-fx-background-color: #ffeeba;");
                        break;
                    case INFO:
                        setStyle("-fx-background-color: #bee5eb;");
                        break;
                    case PRIMARY:
                        setStyle("-fx-background-color: #b8daff;");
                        break;
                    case SECUNDARY:
                        setStyle("-fx-background-color: #d6d8db;");
                        break;
                    case DARK:
                        setStyle("-fx-background-color: #c6c8ca;");
                        break;
                    case AMBER:
                        setStyle("-fx-background-color: #ffe390;");
                        break;
                    case DEFAULT:
                        setStyle("-fx-background-color: #efefef;");
                        break;
                }
            }
            setPadding(new Insets(5));
            setPrefHeight(26);
        }

        @Override
        protected void layoutChildren() {
            super.layoutChildren();
            label.setVisible(label.prefWidth(-1) < getWidth() - getPadding().getLeft() - getPadding().getRight());
        }
    }

    private class TypeSegment extends SegmentedBar.Segment {

        Type type;
        String hexaColor;

        public TypeSegment(double value, String text, Type type) {
            super(value, text);
            this.type = type;
        }

        public TypeSegment(double value, String text, String hexaColor) {
            super(value, text);
            this.hexaColor = hexaColor;
        }

    }
}
