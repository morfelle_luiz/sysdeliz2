package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.controlsfx.control.SegmentedButton;
import org.controlsfx.glyphfont.FontAwesome;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class FormFieldSegmentedButton<T> {
    
    // components section
    private final VBox root;
    private final Label title;
    private final SegmentedButton segmentedButton;
    
    // fx components section
    public final ListProperty<T> items = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final ObjectProperty<T> value = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disable = new SimpleBooleanProperty(false);
    
    public FormFieldSegmentedButton() {
        
        root = new VBox();
        title = new Label();
        segmentedButton = new SegmentedButton();

        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        title.getStyleClass().add("form-field");
        title.setText("Title");
        
        value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                select(items.get().indexOf(newValue));
            }
        });
        
        segmentedButton.disableProperty().bind(editable.not().or(disable));
        segmentedButton.setToggleGroup(new ToggleGroup());
        
        root.getChildren().add(title);
        root.getChildren().add(segmentedButton);
    }
    
    public static final FormFieldSegmentedButton create() {
        return new FormFieldSegmentedButton();
    }
    
    public static final <T> FormFieldSegmentedButton create(Consumer<FormFieldSegmentedButton> value) {
        FormFieldSegmentedButton<T> box = new FormFieldSegmentedButton();
        value.accept(box);
        return box;
    }
    
    public final FormFieldSegmentedButton width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldSegmentedButton title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldSegmentedButton icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final void select(int index) {
        segmentedButton.getButtons().get(index).setSelected(true);
        value.set(items.get(index));
    }

    public final void select(T value) {
        int indexBtn = items.indexOf(value);
        segmentedButton.getButtons().get(indexBtn).setSelected(true);
        this.value.set(items.get(indexBtn));
    }
    
    public final FormFieldSegmentedButton options(@NotNull List<OptionButton> items) {
        this.items.clear();
        items.forEach(item -> {
            segmentedButton.getButtons().add(item);
            this.items.add(item.valueOption);
        });
        return this;
    }
    
    public final FormFieldSegmentedButton options(@NotNull OptionButton... a) {
        this.items.clear();
        Arrays.asList(a).forEach(item -> {
            segmentedButton.getButtons().add(item);
            this.items.add(item.valueOption);
        });
        return this;
    }
    
    public final FormFieldSegmentedButton add(@NotNull OptionButton a) {
        segmentedButton.getButtons().add(a);
        this.items.add(a.valueOption);
        return this;
    }
    
    public final OptionButton option(String title, T value) {
        return new OptionButton(title, value);
    }
    
    public final OptionButton option(FontAwesome.Glyph glyph, T value) {
        return new OptionButton(glyph, value);
    }
    
    public final OptionButton option(String title, T value, Style styleClass) {
        return new OptionButton(title, value, styleClass.value);
    }
    
    public final OptionButton option(String title, T value, String styleClass) {
        return new OptionButton(title, value, styleClass);
    }
    
    public final OptionButton option(FontAwesome.Glyph glyph, T value, Style styleClass) {
        return new OptionButton(glyph, value, styleClass.value);
    }
    
    public final FormFieldSegmentedButton addStyle(String styleClass) {
        segmentedButton.getStyleClass().add(styleClass);
        segmentedButton.getButtons().forEach(btn -> btn.getStyleClass().add(styleClass));
        return this;
    }
    
    public final FormFieldSegmentedButton removeStyle(String styleClass) {
        segmentedButton.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldSegmentedButton withoutTitle() {
        this.root.getChildren().remove(0);
        return this;
    }
    
    public final void getSelectionModel(ChangeListener<? super T> listener) {
        value.addListener(listener);
    }

    public final void alwaysSelected() {
        segmentedButton.getToggleGroup().selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null && oldValue != null)
                oldValue.setSelected(true);
        });
    }


    public final SegmentedButton properties() {
        return segmentedButton;
    }
    
    public final VBox build() {
        return root;
    }
    
    public enum Style {
        DEFAULT("default"),
        PRIMARY("primary"),
        SECUNDARY("secundary"),
        INFO("info"),
        SUCCESS("success"),
        WARNING("warning"),
        DANGER("danger"),
        AMBER("amber"),
        DARK("dark");
        
        String value;
        
        Style(String style) {
            value = style;
        }
    }

    public class OptionButton extends ToggleButton {
        
        private String title = null;
        private FontAwesome.Glyph glyph = null;
        private T valueOption = null;
        private String styleClass = Style.DEFAULT.value;
        
        public OptionButton(String title, T value) {
            this.title = title;
            this.valueOption = value;
            
            createToggleButton();
        }
        
        public OptionButton(FontAwesome.Glyph glyph, T value) {
            this.glyph = glyph;
            this.valueOption = value;
            
            createToggleButton();
        }
        
        public OptionButton(String title, T value, String styleClass) {
            this.title = title;
            this.valueOption = value;
            this.styleClass = styleClass;
            
            createToggleButton();
        }
        
        public OptionButton(FontAwesome.Glyph glyph, T value, String styleClass) {
            this.glyph = glyph;
            this.valueOption = value;
            this.styleClass = styleClass;
            
            createToggleButton();
        }
        
        private void createToggleButton() {
            setText(title == null ? null : title);
            setGraphic(glyph == null ? null : new FontAwesome().create(glyph).color(Color.BLACK));
            setToggleGroup(segmentedButton.getToggleGroup());
            setStyle("-fx-border-radius: 0");
            getStyleClass().add(styleClass);
            setOnAction(evt -> {
                value.set(valueOption);
            });
        }
    }
}