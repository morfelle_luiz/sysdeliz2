package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.window.GenericFilter;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class FormFieldSingleFind<T> extends FormBase {

    // components section
    public final VBox root;
    public final Label title;
    public final HBox boxField;
    public final TextField code;
    public final Label description;
    private final Button find;
    private String defaultCode = null;

    // logic section
    private final Class<T> tClass;

    // fx attributes section
    public final ObjectProperty<T> value = new SimpleObjectProperty<>(null);
    private final StringProperty codeValue = new SimpleStringProperty("");
    private final StringProperty descriptionValue = new SimpleStringProperty("");
    public final BooleanProperty postSelected = new SimpleBooleanProperty(false);
    public final StringProperty codeReference = new SimpleStringProperty("codigo");

    public final BooleanProperty autoFind = new SimpleBooleanProperty(true);

    public final List<DefaultFilter> defaults = new ArrayList<>();

    public FormFieldSingleFind(Class classType) {

        tClass = classType;
        try {
            value.set(tClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            GUIUtils.showException(e);
        }

        getCodeAttribute();

        root = new VBox();
        title = new Label();
        boxField = new HBox();
        code = new TextField();
        description = new Label();
        find = new Button();

        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setPrefWidth(200);

        value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                codeValue.set(((BasicModel) newValue).codigoFilterProperty().getValue());
                descriptionValue.set(((BasicModel) newValue).descricaoFilterProperty().getValue());
                postSelected.set(!postSelected.get());
            } else {
                descriptionValue.set("");
            }
        });

        title.getStyleClass().add("form-field");

        code.setOnKeyReleased(this::codeOnKeyReleased);
        code.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue && autoFind.get()) findByCode(code.getText());
        });
        code.setPrefWidth(60.0);
        code.setMinWidth(60.0);
        code.getStyleClass().add("input-group-apend");
        code.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        code.textProperty().bindBidirectional(codeValue);
        code.editableProperty().bind(editable);
        code.disableProperty().bind(disable);
        code.focusTraversableProperty().bind(editable);
        TextFieldUtils.upperCase(code);

        HBox.setHgrow(description, javafx.scene.layout.Priority.ALWAYS);
        description.setMaxWidth(Double.MAX_VALUE);
        description.setPrefHeight(30.0);
        description.getStyleClass().add("input-group-middle");
        description.getStyleClass().add("sm");
        description.setPadding(new Insets(0.0, 0.0, 0.0, 3.0));
        description.textProperty().bind(descriptionValue);
        description.setTooltip(new Tooltip("Pressione F4 para procurar"));
        description.setFocusTraversable(false);

        find.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        find.setMnemonicParsing(false);
        find.setOnAction(this::findOnAction);
        find.setPrefWidth(30.0);
        find.getStyleClass().add("warning");
        find.getStyleClass().add("last");
        find.setText("Procurar");
        find.setTooltip(new Tooltip("Procurar"));
        find.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
        find.disableProperty().bind(disable.or(editable.not()));

        boxField.getChildren().add(code);
        boxField.getChildren().add(description);
        boxField.getChildren().add(find);

        root.getChildren().add(title);
        root.getChildren().add(boxField);
        root.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                code.requestFocus();
        });
    }

    public final FormFieldSingleFind<T> toUpper() {
        TextFieldUtils.upperCase(code);
        return this;
    }

    public final void maxLength(int length) {
        TextFieldUtils.maxField(code, length);
    }

    private void getCodeAttribute() {
        Method[] methods = tClass.getDeclaredMethods();
        Field[] fields = tClass.getDeclaredFields();
        for (Method method : methods) {
            Id idAnnotation = method.getAnnotation(Id.class);
            if (idAnnotation != null) {
                codeReference.set(method.getName().substring(3, 4).toLowerCase().concat(method.getName().replaceAll("get[A-Z]", "")));
                return;
            }
        }
        for (Field field : fields) {
            Id idAnnotation = field.getAnnotation(Id.class);
            if (idAnnotation != null) {
                codeReference.set(field.getName());
                return;
            }
        }


    }

    private void codeOnKeyReleased(javafx.scene.input.KeyEvent keyEvent) {
        if (disable.not().and(editable).get()) {
            if (keyEvent.getCode() == KeyCode.F4)
                findOnAction(null);
//            else if (keyEvent.getCode() == KeyCode.ENTER) {
            else {
          findByCode(code.getText());
                if (value.get() != null) {
                    if (tabForm.get() != null)
                        tabForm.getValue().requestFocus();
                }
            }
        }

    }

    private void findOnAction(javafx.event.ActionEvent actionEvent) {
        GenericFilter<T> findObject = null;
        try {
            findObject = new GenericFilter<T>(tClass, defaults) {
            };
            findObject.show(ResultTypeFilter.SINGLE_RESULT);
            value.set(findObject.selectedReturn);

            if (value.get() != null)
                postSelected.set(!postSelected.get());
            else
                code.clear();
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void findByCode(String code) {
        try {
            value.set(new FluentDao().selectFrom(tClass).where(whr -> whr.equal(codeReference.getValue(), code)).singleResult());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            value.set(null);
        }
    }

    public static final FormFieldSingleFind create(Class classType) {
        return new FormFieldSingleFind(classType);
    }

    public static final <T> FormFieldSingleFind create(Class classType, Consumer<FormFieldSingleFind> value) {
        FormFieldSingleFind<T> box = new FormFieldSingleFind(classType);
        value.accept(box);
        return box;
    }

    public final void width(double width) {
        root.setPrefWidth(width);
    }

    public final void dividedWidth(double width) {
        root.setPrefWidth(width);
        description.setPrefWidth(width * 0.67);
        code.setPrefWidth(width * 0.33);
    }

    public final void title(String title) {
        this.title.setText(title);
    }

    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
    }

    public final void addStyle(String styleClass) {
        code.getStyleClass().add(styleClass);
        description.getStyleClass().add(styleClass);
        find.getStyleClass().add(styleClass);
    }

    public final void removeStyle(String styleClass) {
        code.getStyleClass().remove(styleClass);
        description.getStyleClass().remove(styleClass);
        find.getStyleClass().remove(styleClass);
    }

    public final void withoutTitle() {
        this.root.getChildren().remove(title);
    }

    public final void widthCode(Double value) {
        code.setMinWidth(value);
        code.setPrefWidth(value);
    }

    public final void focusTraversable(Boolean value) {
        code.setFocusTraversable(value);
    }

    public final void clear() {
        value.set(null);
        codeValue.set(null);
        descriptionValue.set(null);
        code.clear();
    }

    public final void withoutDescription(Double witdhCode) {
        boxField.getChildren().remove(1);
        code.setPrefWidth(witdhCode);
    }

    public final void withoutSearchButton() {
        boxField.getChildren().remove(find);
    }

    public final FormFieldSingleFind postSelected(ChangeListener<Boolean> listener) {
        postSelected.addListener(listener);
        return this;
    }

    public final void setDefaultCode(String code) {
        defaultCode = code;
        if (defaultCode != null)
            findByCode(defaultCode);
        else
            this.clear();
    }

    public final FormFieldSingleFind<T> validate() throws FormValidationException {
        ValidationForm.validationEmpty(this);
        return this;
    }

    public final VBox build() {
        return root;
    }
}
