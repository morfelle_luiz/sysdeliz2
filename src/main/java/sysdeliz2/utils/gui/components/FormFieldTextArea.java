package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.util.function.Consumer;

public class FormFieldTextArea {
    
    // components section
    private final VBox root;
    public final Label title;
    public final TextArea textField;
    
    // fx components section
    public final ObjectProperty<String> value = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty edited = new SimpleBooleanProperty(false);
    public final BooleanProperty focusTraversable = new SimpleBooleanProperty(true);
    
    public FormFieldTextArea() {
        
        root = new VBox();
        title = new Label();
        textField = new TextArea();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        title.getStyleClass().add("form-field");
        title.setText("Title");
        
        //textField.setPrefWidth(120.0);
        //textField.setPrefHeight(80.0);
        textField.setWrapText(true);
        textField.getStyleClass().add("form-field");
        textField.textProperty().bindBidirectional(value);
        textField.editableProperty().bind(editable);
        textField.focusTraversableProperty().bind(editable.or(focusTraversable));
        VBox.setVgrow(textField, Priority.ALWAYS);
        alignment(Pos.TOP_LEFT);
        
        editable.addListener((observable, oldValue, newValue) -> {
            if (!newValue)
                textField.getStyleClass().add("no-editable");
            else
                textField.getStyleClass().remove("no-editable");
        });
        root.getChildren().add(title);
        root.getChildren().add(textField);
    }
    
    public static final FormFieldTextArea create(){
        return new FormFieldTextArea();
    }
    
    public static final FormFieldTextArea create(Consumer<FormFieldTextArea> value){
        FormFieldTextArea box = new FormFieldTextArea();
        value.accept(box);
        return box;
    }
    
    public final FormFieldTextArea width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldTextArea height(double height) {
        textField.setPrefHeight(height);
        return this;
    }

    public final FormFieldTextArea minWidth(double width) {
        root.setMinWidth(width);
        return this;
    }

    public final FormFieldTextArea minHeight(double height) {
        textField.setMinHeight(height);
        return this;
    }
    
    public final FormFieldTextArea title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldTextArea icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldTextArea editable(Boolean value) {
        editable.set(value);
        return this;
    }
    
    public final FormFieldTextArea addStyle(String styleClass){
        textField.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormFieldTextArea removeStyle(String styleClass){
        textField.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormFieldTextArea toUpper(){
        TextFieldUtils.upperCase(textField);
        return this;
    }
    
    public final void alignment(Pos alignment){
        textField.setStyle("-fx-alignment: "+alignment.name().replace("_","-"));
    }
    
    public final void expanded(){
        HBox.setHgrow(root, Priority.ALWAYS);
        VBox.setVgrow(root, Priority.ALWAYS);
    }
    
    public final void id(String value){
        textField.setId(value);
    }
    
    public final void focusTraversable(Boolean value){
        focusTraversable.set(value);
    }
    
    public final void clear(){
        textField.clear();
        value.set(null);
    }
    
    public void requestFocus() {
        textField.requestFocus();
    }
    
    public final void keyReleased(EventHandler<? super KeyEvent> value){
        textField.setOnKeyReleased(value);
    }
    
    public final void mouseClicked(EventHandler<? super MouseEvent> value){
        textField.setOnMouseClicked(value);
    }
    
    public final FormFieldTextArea withoutTitle() {
        root.getChildren().remove(0);
        return this;
    }
    
    public final void focusedListener(ChangeListener<Boolean> listener){
        textField.focusedProperty().addListener(listener);
    }
    
    public final VBox build() {
        return root;
    }
}