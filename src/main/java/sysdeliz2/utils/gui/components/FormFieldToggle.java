package sysdeliz2.utils.gui.components;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import sysdeliz2.utils.ToggleSwitch;

import java.util.function.Consumer;

public class FormFieldToggle {
    
    // components section
    private final VBox root;
    private final Label title;
    private final HBox boxField;
    private final Label yesOrNo;
    private final ToggleSwitch toggleSwitch;
    
    // fx components section
    public final BooleanProperty value = new SimpleBooleanProperty(true);
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    
    public FormFieldToggle() {
        
        root = new VBox();
        title = new Label();
        boxField = new HBox();
        yesOrNo = new Label();
        toggleSwitch = new ToggleSwitch(18, 40);
        
        root.getStylesheets().add("/styles/stylePadrao.css");
        boxField.disableProperty().bind(editable.not());

        toggleSwitch.switchedOnProperty().bindBidirectional(value);
        toggleSwitch.setLayoutX(0.0);
        toggleSwitch.disableProperty().bind(editable.not());
        toggleSwitch.focusTraversableProperty().bind(editable);
        
        title.getStyleClass().add("form-field");
    
        yesOrNo.setPadding(new Insets(1.0, 0.0, 0.0, 0.0));
        yesOrNo.setAlignment(javafx.geometry.Pos.CENTER);
        yesOrNo.setMaxHeight(Double.MAX_VALUE);
        yesOrNo.setMaxWidth(Double.MAX_VALUE);
        yesOrNo.textProperty().bind(Bindings.when(toggleSwitch.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        yesOrNo.setFont(new Font("System Bold", 11.0));
    
        boxField.setPrefHeight(28.0);
        boxField.setSpacing(3.0);
        boxField.getStyleClass().add("corner-up-left");
        boxField.setPadding(new Insets(3.0));
        boxField.getChildren().add(toggleSwitch);
        boxField.getChildren().add(yesOrNo);
        
        root.getChildren().add(title);
        root.getChildren().add(boxField);
        
    }
    
    public static final FormFieldToggle create(){
        return new FormFieldToggle();
    }
    
    public static final FormFieldToggle create(Consumer<FormFieldToggle> value){
        FormFieldToggle box = new FormFieldToggle();
        value.accept(box);
        return box;
    }
    
    public final FormFieldToggle width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormFieldToggle title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldToggle icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormFieldToggle withoutTitle() {
        boxField.getStyleClass().remove("corner-up-left");
        root.getChildren().remove(0);
        return this;
    }
    
    public final void focusTraversable(Boolean value){
        toggleSwitch.setFocusTraversable(value);
    }
    
    public final FormFieldToggle changed(ChangeListener<? super Boolean> listener) {
        toggleSwitch.switchedOnProperty().addListener(listener);
        return this;
    }
    
    public final VBox build() {
        return root;
    }
    
}
