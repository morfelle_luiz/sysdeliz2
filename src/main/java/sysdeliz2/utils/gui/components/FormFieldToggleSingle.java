package sysdeliz2.utils.gui.components;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import org.controlsfx.control.ToggleSwitch;

import java.util.function.Consumer;

public class FormFieldToggleSingle {
    
    // components section
    private final HBox root;
    private final HBox graphicRoot;
    private final HBox titleRoot;
    private final Label title;
    private final Label yesOrNo;
    private final ToggleSwitch toggleSwitch;
    
    // fx components section
    public final BooleanProperty value = new SimpleBooleanProperty(true);
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    
    public FormFieldToggleSingle() {
        
        root = new HBox();
        graphicRoot = new HBox();
        titleRoot = new HBox();
        title = new Label();
        yesOrNo = new Label();
        //toggleSwitch = new ToggleSwitch(20, 40);
        toggleSwitch = new ToggleSwitch();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setAlignment(Pos.CENTER_LEFT);
        root.setSpacing(3.0);
        root.setPadding(new Insets(2.0));
        graphicRoot.setAlignment(Pos.CENTER_RIGHT);
        graphicRoot.setSpacing(3.0);
        titleRoot.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(titleRoot, Priority.ALWAYS);
        
        //toggleSwitch.switchedOnProperty().bindBidirectional(value);
        toggleSwitch.selectedProperty().bindBidirectional(value);
        toggleSwitch.disableProperty().bind(editable.not());
        toggleSwitch.focusTraversableProperty().bind(editable);
        toggleSwitch.setMinSize(40.0,20.0);
        toggleSwitch.setPrefSize(40.0,20.0);
        toggleSwitch.setMaxSize(40.0,20.0);
        toggleSwitch.getStyleClass().add("tsControls");
    
        title.setMaxHeight(Double.MAX_VALUE);
        title.setMaxWidth(Double.MAX_VALUE);
    
        //yesOrNo.setPadding(new Insets(1.0, 0.0, 0.0, 0.0));
        yesOrNo.setAlignment(javafx.geometry.Pos.CENTER);
        //yesOrNo.setMaxHeight(Double.MAX_VALUE);
        //yesOrNo.setMaxWidth(Double.MAX_VALUE);
        //yesOrNo.textProperty().bind(Bindings.when(toggleSwitch.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        yesOrNo.textProperty().bind(Bindings.when(toggleSwitch.selectedProperty()).then("SIM").otherwise("NÃO"));
        yesOrNo.setFont(new Font("System Bold", 11.0));
        yesOrNo.setPrefWidth(25.0);
    
        //root.setPrefHeight(26.0);
        titleRoot.getChildren().add(title);
        graphicRoot.getChildren().add(toggleSwitch);
        graphicRoot.getChildren().add(yesOrNo);
        root.getChildren().add(titleRoot);
        root.getChildren().add(graphicRoot);
    }
    
    public static final FormFieldToggleSingle create(){
        return new FormFieldToggleSingle();
    }
    
    public static final FormFieldToggleSingle create(Consumer<FormFieldToggleSingle> value){
        FormFieldToggleSingle box = new FormFieldToggleSingle();
        value.accept(box);
        return box;
    }
    
    public final FormFieldToggleSingle width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final void focusTraversable(Boolean value){
        toggleSwitch.setFocusTraversable(value);
    }
    
    public final FormFieldToggleSingle title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormFieldToggleSingle icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final void withoutYesNo() {
        graphicRoot.getChildren().remove(yesOrNo);
    }
    
    public final HBox build() {
        return root;
    }

    public final ToggleSwitch toggleSwitch() {
        return toggleSwitch;
    }
    
}
