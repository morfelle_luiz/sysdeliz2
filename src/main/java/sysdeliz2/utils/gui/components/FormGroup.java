package sysdeliz2.utils.gui.components;

import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class FormGroup extends VBox {
    
    private final Label titlePaneGroup;
    public final FlowPane contentPaneGroup;
    
    public FormGroup(String titleGroup) {
        contentPaneGroup = new FlowPane();
        titlePaneGroup = new Label(titleGroup.toUpperCase());
    
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
        getStyleClass().add("vbox");
        
        titlePaneGroup.setTextFill(javafx.scene.paint.Color.valueOf("#655959"));
        contentPaneGroup.setHgap(5.0);
        contentPaneGroup.setPrefHeight(0.0);
        contentPaneGroup.setPrefWidth(5000.0);
        contentPaneGroup.setVgap(5.0);
    
        getChildren().add(titlePaneGroup);
        getChildren().add(new Separator(Orientation.HORIZONTAL));
        getChildren().add(contentPaneGroup);
    }
}
