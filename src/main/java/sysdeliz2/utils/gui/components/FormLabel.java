package sysdeliz2.utils.gui.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.function.Consumer;

public class FormLabel extends Label {
    
    // components section
    
    // fx components section
    public final ObjectProperty<String> value = new SimpleObjectProperty<>();
    
    public FormLabel() {
        getStylesheets().add("/styles/sysDelizDesktop.css");
        getStylesheets().add("/styles/stylePadrao.css");
        
        textProperty().bindBidirectional(value);
        setPadding(new Insets(5));
    }
    
    public static final FormLabel create(){
        return new FormLabel();
    }
    
    public static final FormLabel create(Consumer<FormLabel> value){
        FormLabel box = new FormLabel();
        value.accept(box);
        return box;
    }
    
    public final FormLabel width(double width) {
        setPrefWidth(width);
        return this;
    }
    
    public final FormLabel icon(ImageView icon) {
        setGraphic(icon);
        return this;
    }
    
    public final FormLabel icon(FontAwesome.Glyph icon){
//        root.getChildren().remove(root.getChildren().size() - 1);
        setGraphic(new FontAwesome().create(icon));
        return this;
    }
    
    public final FormLabel addStyle(String styleClass){
        getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormLabel removeStyle(String styleClass){
        getStyleClass().remove(styleClass);
        return this;
    }
    
    public final FormLabel toUpper(){
        setText(getText().toUpperCase());
        return this;
    }
    
    public final void sizeText(Integer size) {
        setStyle(getStyle().concat("-fx-font-size: "+size+";"));
    }
    
    public final void fontColor(String color) {
        setStyle(getStyle().concat("-fx-text-fill: "+color+";"));
    }
    
    public final void boldText() {
        setStyle(getStyle().concat("-fx-font-weight: bold;"));
    }
    
    public final void borderRadius(Integer radius) {
        setStyle(getStyle().concat("-fx-border-radius: "+radius+"; -fx-background-radius: "+radius+";"));
    }
    
    public final void alignment(Pos alignment){
        setStyle(getStyle().concat("-fx-alignment: "+alignment.name().replaceAll("_","-")+";"));
    }

    public final void alignment(TextAlignment alignment){
        setStyle(getStyle().concat("-fx-text-alignment: "+alignment.name().replaceAll("_","-")+";"));
    }

    public final void tooltip(String text){
        setTooltip(new Tooltip(text));
    }

    public final void padding(double value){
        setPadding(new Insets(value));
    }

    public final void expanded() {
        HBox.setHgrow(this, Priority.ALWAYS);
        VBox.setVgrow(this, Priority.ALWAYS);
    }

    public final void clear() {
        value.set("");
    }

    public final void mouseClicked(EventHandler<? super MouseEvent> value){
        setOnMouseClicked(value);
    }
    
    public final void focusedListener(ChangeListener<Boolean> listener){
        focusedProperty().addListener(listener);
    }
    
    public final VBox build() {
        return new VBox(this);
    }
}