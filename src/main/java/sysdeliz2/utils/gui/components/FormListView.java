package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.lang.reflect.ParameterizedType;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FormListView<T> extends FormBase {
    
    // components section
    private final VBox root;
    private final VBox rootTitle;
    private final HBox rootRegisters;
    private final HBox headerBox;
    private final HBox titleBox;
    private final Label title;
    private final Label registerCount;
    private final ListView<T> registers = new ListView<>();
    private final CheckBox btnCheckAll;
    
    public final ListProperty<T> items = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty disabled = new SimpleBooleanProperty(false);
    
    public FormListView() {
        
        root = new VBox();
        rootTitle = new VBox();
        rootRegisters = new HBox();
        headerBox = new HBox();
        titleBox = new HBox();
        title = new Label();
        registerCount = new Label();
        btnCheckAll = new CheckBox("Selecionar Todos");
    
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
    
        HBox.setHgrow(titleBox, javafx.scene.layout.Priority.ALWAYS);
        title.getStyleClass().add("form-field");
        rootTitle.getChildren().add(title);
    
        registerCount.textProperty().bind(new SimpleStringProperty("Exibindo ").concat(items.sizeProperty()).concat(" registro(s)"));
        registerCount.setFont(new Font("System Italic", 12.0));
    
        VBox.setVgrow(rootRegisters, javafx.scene.layout.Priority.ALWAYS);
        HBox.setHgrow(registers, javafx.scene.layout.Priority.ALWAYS);
        registers.itemsProperty().bindBidirectional(items);
        registers.editableProperty().bind(editable);
        registers.disableProperty().bind(disabled);
        rootRegisters.getChildren().add(registers);
    
        btnCheckAll.disableProperty().bind(disabled.or(editable.not()).or(items.emptyProperty()));
        
        titleBox.getChildren().add(rootTitle);
        headerBox.getChildren().add(titleBox);
        headerBox.getChildren().add(registerCount);
        root.getChildren().add(headerBox);
        root.getChildren().add(rootRegisters);
    }
    
    public static final FormListView create() {
        return new FormListView();
    }
    
    public static final FormListView create(Consumer<FormListView> create) {
        FormListView box = new FormListView();
        create.accept(box);
        return box;
    }
    
    public final VBox build() {
        return root;
    }
    
    public final void size(double width, double hight) {
        root.setPrefSize(width, hight);
        
    }
    
    public final void width(double width) {
        root.setPrefWidth(width);
    }
    
    public final void height(double height) {
        root.setPrefHeight(height);
        
    }
    
    public final void title(String title) {
        this.title.setText(title);
    }
    
    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        
    }
    
    public final void expanded() {
        HBox.setHgrow(root, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(root, javafx.scene.layout.Priority.ALWAYS);
        
    }

    public final void withAddButtom(Class inputType) {
        FormButton btnAdd = FormButton.create(btn -> {
            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
            btn.addStyle("success");
            btn.disable.bind(editable.not());
            btn.setAction(evt -> addItem(inputType));
        });
        if (registers.getOrientation().equals(Orientation.VERTICAL)) {
            root.getChildren().add(btnAdd);
        } else {
            rootRegisters.getChildren().add(btnAdd);
        }
    }

    private void addItem(Class inputType) {
        InputBox<T> inputValue = InputBox.build(inputType, boxInput -> {
            boxInput.message("Digite o valor digitado:");
            boxInput.showAndWait();
        });
        T valorDigitado = inputValue.value.get();
        if (valorDigitado != null)
            this.registers.getItems().add(valorDigitado);

    }

    public final void clear() {
        items.clear();
    }
    
    public final void multipleSelection() {
        registers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        btnCheckAll.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (disabled.not().get()) {
                if (newValue)
                    registers.getSelectionModel().selectAll();
                else
                    registers.getSelectionModel().clearSelection();
            }
        });
        root.getChildren().add(btnCheckAll);
    }

    public final void withoutBtnCheckAll(){
        if(root.getChildren().contains(btnCheckAll)) root.getChildren().remove(btnCheckAll);
    }
    
    public final ListView<T> listProperties() {
        return registers;
    }
    
    public final void selectItem(Integer index) {
        registers.getSelectionModel().select(index);
        
    }
    
    public final void selectItem(T obj) {
        registers.getSelectionModel().select(obj);
        
    }
    
    public final void setItems(ObservableList<T> items) {
        this.items.set(items);
    }
    
    public final T selectedItem() {
        return registers.getSelectionModel().getSelectedItem();
    }
    
    public final ObservableList<T> selectedRegisters() {
        return registers.getSelectionModel().getSelectedItems();
    }
    
    public final ObservableList<T> notSelectedRegisters() {
        return FXCollections.observableList(registers.getItems().stream().filter(item -> registers.getSelectionModel().getSelectedItems().stream().noneMatch(selected -> item.equals(selected))).collect(Collectors.toList()));
    }

    public final void refresh(){
        registers.refresh();
    }

    public final FormListView<T> withoutTitle(){
        headerBox.getChildren().remove(0);
        return this;
    }
    
    public final FormListView<T> withoutCounter(){
        headerBox.getChildren().remove(1);
        return this;
    }

    public final void withoutHeader(){
        root.getChildren().remove(0);
    }
    
}
