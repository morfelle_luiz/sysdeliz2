package sysdeliz2.utils.gui.components;

import groovy.beans.Bindable;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.util.function.Consumer;

public class FormNavegation<T> extends VBox {
    
    private Button btnFirstRegister;
    private Button btnPreviusRegister;
    private Button btnNextRegister;
    private Button btnLastRegister;
    private Button btnAddRegister;
    private Button btnEditRegister;
    private Button btnDeleteRegister;
    private Button btnSave;
    private Button btnCancel;
    private Button btnReturn;
    private Button btnClear;
    private Button btnSend;
    private Button btnSendMail;
    private Button btnPrint;
    private Button btnAttach;
    private Button btnBlock;
    private Button btnDuplicate;
    
    private HBox navegation = new HBox();
    private HBox boxBtnsNavegation = new HBox();
    private HBox boxBtnsActions = new HBox();
    private HBox boxBtnsExtras = new HBox();
    
    public final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    public final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    public final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    public final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    public final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    public final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    public final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    
    public final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    public final ListProperty<T> listRegisters = new SimpleListProperty();
    public final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);
    public final ObjectProperty<T> selectedItem = new SimpleObjectProperty<>(null);
    public final ObjectProperty<T> oldItem = new SimpleObjectProperty<>(null);

    private TableView<T> tableView = null;
    
    public FormNavegation() {
        boxBtnsExtras.setAlignment(Pos.CENTER_LEFT);
        boxBtnsExtras.setSpacing(3.0);
        HBox.setHgrow(boxBtnsExtras, Priority.ALWAYS);
    
        navegation.setAlignment(Pos.CENTER_RIGHT);
        navegation.getChildren().addAll(boxBtnsNavegation, boxBtnsActions, boxBtnsExtras);


        HBox.setHgrow(navegation, Priority.ALWAYS);
        VBox.setVgrow(navegation, Priority.ALWAYS);
        getChildren().addAll(navegation, new Separator(Orientation.HORIZONTAL));
        getStylesheets().clear();
        getStylesheets().addAll("styles/stylePadrao.css");
    }
    
    public static final FormNavegation create() {
        return new FormNavegation();
    }
    
    public static final FormNavegation create(Consumer<FormNavegation> create) {
        FormNavegation box = new FormNavegation();
        create.accept(box);
        return box;
    }
    
    public final void withNavegation(FormTableView tableView) {
        this.withNavegation(tableView.tableview());
    }
    
    public final void withNavegation(TableView tableView) {
        this.tableView = tableView;
        listRegisters.bind(this.tableView.itemsProperty());
        this.tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedItem.set(newValue);
            navegationIndex.set(this.tableView.getItems().indexOf(newValue));
        });
        
        btnFirstRegister = new Button();
        btnFirstRegister.getStyleClass().addAll("sm", "secundary");
        btnFirstRegister.setGraphic(new ImageView(new Image(FormNavegation.class.getResource("/images/icons/buttons/navegation/first.png").toExternalForm())));
        btnFirstRegister.setTooltip(new Tooltip("Primeiro Registro"));
        btnFirstRegister.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnFirstRegister.setOnAction(evt -> {
            this.tableView.getSelectionModel().selectFirst();
        });
        
        btnPreviusRegister = new Button();
        btnPreviusRegister.getStyleClass().addAll("sm", "secundary");
        btnPreviusRegister.setGraphic(new ImageView(new Image(FormNavegation.class.getResource("/images/icons/buttons/navegation/previous.png").toExternalForm())));
        btnPreviusRegister.setTooltip(new Tooltip("Registro Anterior"));
        btnPreviusRegister.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnPreviusRegister.setOnAction(evt -> {
            this.tableView.getSelectionModel().selectPrevious();
        });
        
        btnNextRegister = new Button();
        btnNextRegister.getStyleClass().addAll("sm", "secundary");
        btnNextRegister.setGraphic(new ImageView(new Image(FormNavegation.class.getResource("/images/icons/buttons/navegation/next.png").toExternalForm())));
        btnNextRegister.setTooltip(new Tooltip("Próximo Registro"));
        btnNextRegister.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnNextRegister.setOnAction(evt -> {
            this.tableView.getSelectionModel().selectNext();
        });
        
        btnLastRegister = new Button();
        btnLastRegister.getStyleClass().addAll("sm", "secundary");
        btnLastRegister.setGraphic(new ImageView(new Image(FormNavegation.class.getResource("/images/icons/buttons/navegation/last.png").toExternalForm())));
        btnLastRegister.setTooltip(new Tooltip("Último Registro"));
        btnLastRegister.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnLastRegister.setOnAction(evt -> {
            this.tableView.getSelectionModel().selectLast();
        });
    
        disableButtonNavegation.bind(this.tableView.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));
    
        btnFirstRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnPreviusRegister.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnNextRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnLastRegister.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        
        boxBtnsNavegation.setAlignment(Pos.CENTER_LEFT);
        boxBtnsNavegation.setSpacing(2.0);
        boxBtnsNavegation.getChildren().addAll(btnFirstRegister, btnPreviusRegister, btnNextRegister, btnLastRegister);
    }
    
    public final void withActions(boolean canAdd, boolean canEdit, boolean canDelete) {
        boxBtnsActions.setAlignment(Pos.CENTER_LEFT);
        boxBtnsActions.setSpacing(3.0);
        
        if (canAdd || canEdit || canDelete) {
            boxBtnsActions.getChildren().add(new Separator(Orientation.VERTICAL));
        }
        
        if (canAdd) {
            this.btnAddRegister = new Button();
            this.btnAddRegister.getStyleClass().addAll("sm", "success");
            this.btnAddRegister.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/new register.png").toExternalForm())));
            this.btnAddRegister.setText("Adicionar Novo");
            this.btnAddRegister.disableProperty().bind(inEdition);
            boxBtnsActions.getChildren().add(btnAddRegister);
        }
        
        if (canEdit) {
            this.btnEditRegister = new Button();
            this.btnEditRegister.getStyleClass().addAll("sm", "warning");
            this.btnEditRegister.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/edit register.png").toExternalForm())));
            this.btnEditRegister.setText("Editar");
            this.btnEditRegister.disableProperty().bind(inEdition.or(selectedItem.isNull()));
            boxBtnsActions.getChildren().add(btnEditRegister);
        }
        
        if (canDelete) {
            this.btnDeleteRegister = new Button();
            this.btnDeleteRegister.getStyleClass().addAll("sm", "danger");
            this.btnDeleteRegister.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/delete register.png").toExternalForm())));
            this.btnDeleteRegister.setText("Excluir");
            this.btnDeleteRegister.disableProperty().bind(inEdition.or(selectedItem.isNull()));
            boxBtnsActions.getChildren().add(btnDeleteRegister);
        }
        
        if (canAdd || canEdit) {
            this.btnSave = new Button();
            this.btnSave.getStyleClass().addAll("sm", "success");
            this.btnSave.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/select.png").toExternalForm())));
            this.btnSave.setText("Salvar");
            this.btnSave.disableProperty().bind(inEdition.not());
            
            this.btnCancel = new Button();
            this.btnCancel.getStyleClass().addAll("sm", "danger");
            this.btnCancel.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/cancel.png").toExternalForm())));
            this.btnCancel.setText("Cancelar");
            this.btnCancel.disableProperty().bind(inEdition.not());
    
            boxBtnsActions.getChildren().addAll(new Separator(Orientation.VERTICAL), btnSave, btnCancel);
        }
        //boxBtnsActions.getChildren().add(boxBtnsExtras);
    }
    
    public final void withReturn(EventHandler<ActionEvent> value) {
        btnReturn = new Button();
        btnReturn.getStyleClass().addAll("sm", "secundary");
        btnReturn.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/return.png").toExternalForm())));
        btnReturn.setText("Retornar");
        btnReturn.setOnAction(value);
        btnReturn.disableProperty().bind(inEdition);
    
        navegation.getChildren().add(btnReturn);
    }
    
    public final void withClear(EventHandler<ActionEvent> value){
        this.btnClear = new Button();
        this.btnClear.getStyleClass().addAll("sm", "warning");
        this.btnClear.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/clean.png").toExternalForm())));
        this.btnClear.setTooltip(new Tooltip("Limpar"));
        this.btnClear.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnClear.setOnAction(value);
        
        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnClear);
    }
    
    public final void withSend(EventHandler<ActionEvent> value){
        this.btnSend = new Button();
        this.btnSend.getStyleClass().addAll("sm", "secundary");
        this.btnSend.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/send.png").toExternalForm())));
        this.btnSend.setTooltip(new Tooltip("Enviar"));
        this.btnSend.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnSend.setOnAction(value);
        
        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnSend);
    }
    
    public final void withSendMail(EventHandler<ActionEvent> value){
        withSendMail(new SimpleBooleanProperty(true).not(), value);
    }

    public final void withSendMail(BooleanBinding disable, EventHandler<ActionEvent> value){
        this.btnSendMail = new Button();
        this.btnSendMail.getStyleClass().addAll("sm", "info");
        this.btnSendMail.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/send mail.png").toExternalForm())));
        this.btnSendMail.setTooltip(new Tooltip("Enviar E-mail"));
        this.btnSendMail.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnSendMail.setOnAction(value);
        this.btnSendMail.disableProperty().bind(disable);

        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnSendMail);
    }

    public final void withPrint(EventHandler<ActionEvent> value){
        withSendMail(new SimpleBooleanProperty(true).not(), value);
    }

    public final void withPrint(BooleanBinding disable, EventHandler<ActionEvent> value){
        this.btnPrint = new Button();
        this.btnPrint.getStyleClass().addAll("sm", "primary");
        this.btnPrint.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/print.png").toExternalForm())));
        this.btnPrint.setTooltip(new Tooltip("Imprimir"));
        this.btnPrint.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnPrint.setOnAction(value);
        this.btnPrint.disableProperty().bind(disable);

        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnPrint);
    }
    
    public final void withAttach(EventHandler<ActionEvent> value){
        this.btnAttach = new Button();
        this.btnAttach.getStyleClass().addAll("sm", "secundary");
        this.btnAttach.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/attach.png").toExternalForm())));
        this.btnAttach.setTooltip(new Tooltip("Anexar"));
        this.btnAttach.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnAttach.setOnAction(value);
        
        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnAttach);
    }
    
    public final void withDuplicate(EventHandler<ActionEvent> value){
        withDuplicate(new SimpleBooleanProperty(true).not(), value);
    }

    public final void withDuplicate(BooleanBinding disable, EventHandler<ActionEvent> value){
        this.btnDuplicate = new Button();
        this.btnDuplicate.getStyleClass().addAll("sm", "secundary");
        this.btnDuplicate.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._16));
        this.btnDuplicate.setTooltip(new Tooltip("Duplicar Registro"));
        this.btnDuplicate.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnDuplicate.setOnAction(value);
        this.btnDuplicate.disableProperty().bind(disable);

        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnDuplicate);
    }
    
    public final void withBlock(EventHandler<ActionEvent> value){
        this.btnBlock = new Button();
        this.btnBlock.getStyleClass().addAll("sm", "danger");
        this.btnBlock.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/navegation/blocked.png").toExternalForm())));
        this.btnBlock.setTooltip(new Tooltip("Bloquear/Inativar"));
        this.btnBlock.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnBlock.setOnAction(value);
        
        if (boxBtnsExtras.getChildren().size() == 0)
            boxBtnsExtras.getChildren().add(new Separator(Orientation.VERTICAL));
        boxBtnsExtras.getChildren().add(btnClear);
    }
    
    public final void btnAddRegister(EventHandler<ActionEvent> value){
        btnAddRegister.setOnAction(value);
    }
    
    public final void btnEditRegister(EventHandler<ActionEvent> value){
        btnEditRegister.setOnAction(value);
    }
    
    public final void btnDeleteRegister(EventHandler<ActionEvent> value){
        btnDeleteRegister.setOnAction(value);
    }
    
    public final void btnSave(EventHandler<ActionEvent> value){
        btnSave.setOnAction(value);
    }
    
    public final void btnCancel(EventHandler<ActionEvent> value){
        btnCancel.setOnAction(value);
    }
    
}
