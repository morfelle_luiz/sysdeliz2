package sysdeliz2.utils.gui.components;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

public class FormOverlap extends StackPane {
    
    public FormOverlap() {
        VBox.setVgrow(this, Priority.ALWAYS);
        HBox.setHgrow(this, Priority.ALWAYS);
    }
    
    public static FormOverlap create() {
        return new FormOverlap();
    }
    
    public static FormOverlap create(Consumer<FormOverlap> value) {
        FormOverlap form = new FormOverlap();
        value.accept(form);
        return form;
    }
    
    public final void add(Node node) {
        getChildren().add(node);
    }
    
    public final void add(Node...nodes) {
        for (Node node : nodes)
            getChildren().add(node);
    }
}
