package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.glyphfont.FontAwesome;
import sysdeliz2.utils.gui.TextFieldUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.util.function.Consumer;

public class FormPasswordField {

    // components section
    private final VBox root;
    public final Label title;
    public final TextField textField;
    protected final HBox boxField;
    protected final Label labelField;
    protected final Label postLabelField;

    // fx components section
    public final StringProperty value = new SimpleStringProperty();
    public final BooleanProperty editable = new SimpleBooleanProperty(true);
    public final BooleanProperty focusTraversable = new SimpleBooleanProperty(true);

    public enum Alignment {CENTER, LEFT, RIGHT}

    public enum Mask {MONEY, DATE, TIME, INTEGER, DOUBLE, CEP, EMAIL, PHONE, CNPJ, CPF, CNPJCPF}

    public FormPasswordField() {
        
        root = new VBox();
        title = new Label();
        textField = new PasswordField();
        boxField = new HBox();
        labelField = new Label();
        postLabelField = new Label();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        title.getStyleClass().add("form-field");
        title.setText("Title");
        
        labelField.setMaxHeight(Double.MAX_VALUE);
        labelField.setMaxWidth(Double.MAX_VALUE);
        labelField.getStyleClass().add("input-group-prepend");
        labelField.setMinWidth(18.0);
        labelField.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        
        postLabelField.setMaxHeight(Double.MAX_VALUE);
        postLabelField.setMaxWidth(Double.MAX_VALUE);
        postLabelField.getStyleClass().add("input-group-apend");
        postLabelField.setMinWidth(18.0);
        postLabelField.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
        
        textField.setPrefWidth(120.0);
        textField.getStyleClass().add("form-field");
        textField.textProperty().bindBidirectional(value);
        textField.editableProperty().bind(editable);
        textField.focusTraversableProperty().bind(editable.or(focusTraversable));
        
        editable.addListener((observable, oldValue, newValue) -> {
            if (!newValue)
                textField.getStyleClass().add("no-editable");
            else
                textField.getStyleClass().remove("no-editable");
        });
        
        root.getChildren().add(title);
        root.getChildren().add(textField);
    }
    
    public static final FormPasswordField create() {
        return new FormPasswordField();
    }
    
    public static final FormPasswordField create(Consumer<FormPasswordField> value) {
        FormPasswordField box = new FormPasswordField();
        value.accept(box);
        return box;
    }
    
    public final FormPasswordField width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormPasswordField maxWidth(double width) {
        root.setMaxWidth(width);
        return this;
    }
    public final FormPasswordField height(double height) {
        root.setPrefHeight(height);
        return this;
    }

    public final FormPasswordField maxheight(double height) {
        root.setMaxHeight(height);
        return this;
    }

    public final void placeHolder(String text) {
        textField.setPromptText(text);
    }
    
    public final FormPasswordField title(String title) {
        this.title.setText(title);
        return this;
    }

    public final FormPasswordField icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final FormPasswordField editable(Boolean value) {
        editable.set(value);
        return this;
    }
    
    public final FormPasswordField addStyle(String styleClass) {
        textField.getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormPasswordField removeStyle(String styleClass) {
        textField.getStyleClass().remove(styleClass);
        return this;
    }
    
    public final void clearDefaultStyle() {
        textField.getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
    }
    
    public final FormPasswordField mask(Mask mask) {
        switch(mask) {
            case MONEY:
                TextFieldUtils.monetaryMask(textField);
                break;
            case CNPJ:
                TextFieldUtils.cnpjMask(textField);
                break;
            case CPF:
                TextFieldUtils.cpfMask(textField);
                break;
            case CNPJCPF:
                TextFieldUtils.cnpjCpfMask(textField);
                break;
            case DATE:
                TextFieldUtils.dateMask(textField);
                break;
            case DOUBLE:
                TextFieldUtils.decimalMask(textField);
                break;
            case EMAIL:
                TextFieldUtils.emailMask(textField);
                break;
            case INTEGER:
                TextFieldUtils.integerMask(textField);
                break;
            case PHONE:
                TextFieldUtils.phoneMask(textField);
                break;
            case TIME:
                TextFieldUtils.timeMask(textField);
                break;
            case CEP:
                TextFieldUtils.cepMask(textField);
                break;
        }
        
        return this;
    }
    public final void maxLength(int length){
        TextFieldUtils.maxField(textField,length);
    }

    public final FormPasswordField label(String label) {
        root.getChildren().remove(root.getChildren().size() - 1);
        labelField.setText(label);
        HBox.setHgrow(textField, Priority.ALWAYS);
        textField.setPrefWidth(0.0);
        textField.getStyleClass().add("last");
        boxField.getChildren().add(labelField);
        boxField.getChildren().add(textField);
        root.getChildren().add(boxField);
        return this;
    }
    
    public final FormPasswordField label(ImageView icon) {
        root.getChildren().remove(root.getChildren().size() - 1);
        labelField.setGraphic(icon);
        HBox.setHgrow(textField, Priority.ALWAYS);
        textField.setPrefWidth(0.0);
        textField.getStyleClass().add("last");
        boxField.getChildren().add(labelField);
        boxField.getChildren().add(textField);
        root.getChildren().add(boxField);
        return this;
    }
    
    public final FormPasswordField label(FontAwesome.Glyph icon) {
        root.getChildren().remove(root.getChildren().size() - 1);
        labelField.setGraphic(new FontAwesome().create(icon));
        labelField.setText(null);
        textField.getStyleClass().add("last");
        HBox.setHgrow(textField, Priority.ALWAYS);
        textField.setPrefWidth(0.0);
        boxField.getChildren().add(labelField);
        boxField.getChildren().add(textField);
        root.getChildren().add(boxField);
        return this;
    }
    
    public final FormPasswordField toUpper() {
        TextFieldUtils.upperCase(textField);
        return this;
    }
    
    public final void alignment(FormPasswordField.Alignment alignment) {
        switch(alignment) {
            case CENTER:
                textField.setStyle("-fx-alignment: CENTER");
                break;
            case LEFT:
                textField.setStyle("-fx-alignment: CENTER-LEFT");
                break;
            case RIGHT:
                textField.setStyle("-fx-alignment: CENTER-RIGHT");
                break;
        }
    }
    
    public final void alignment(Pos alignment) {
        textField.setStyle("-fx-alignment: " + alignment.name().replace("_", "-"));
    }

    public final void setValue(String value) {
        this.value.set(value);
    }
    
    public final void postLabel(String label) {
        if (labelField.getText().length() <= 0) {
            root.getChildren().remove(root.getChildren().size() - 1);
            postLabelField.setText(label);
            HBox.setHgrow(textField, Priority.ALWAYS);
            textField.setPrefWidth(0.0);
            textField.getStyleClass().add("input-group-apend");
            boxField.getChildren().add(textField);
            boxField.getChildren().add(postLabelField);
            root.getChildren().add(boxField);
        } else {
            textField.getStyleClass().removeAll("input-group-prepend", "last");
            textField.getStyleClass().add("input-group-middle");
            postLabelField.setText(label);
            if (!boxField.getChildren().contains(postLabelField))
                boxField.getChildren().add(postLabelField);
        }
    }
    
    public final void expanded() {
        HBox.setHgrow(root, Priority.ALWAYS);
        VBox.setVgrow(root, Priority.ALWAYS);
    }
    
    public final void tooltip(String text) {
        textField.setTooltip(new Tooltip(text));
    }
    
    public final void id(String value) {
        textField.setId(value);
    }
    
    public final void focusTraversable(Boolean value) {
        focusTraversable.set(value);
    }
    
    public final void clear() {
        textField.clear();
        value.set(null);
    }
    
    public void requestFocus() {
        textField.requestFocus();
    }
    
    public final void keyReleased(EventHandler<? super KeyEvent> value) {
        textField.setOnKeyReleased(value);
    }

    public final void keyPressed(EventHandler<? super KeyEvent> value) {
        textField.setOnKeyPressed(value);
    }
    
    public final void mouseClicked(EventHandler<? super MouseEvent> value) {
        textField.setOnMouseClicked(value);
    }
    
    public final FormPasswordField withoutTitle() {
        root.getChildren().remove(0);
        return this;
    }

    public final void focusedListener(ChangeListener<Boolean> listener) {
        textField.focusedProperty().addListener(listener);
    }
    
    public final void decimalField(Integer decimalLenght) {
        
        DecimalFormat format = new DecimalFormat();
        //format.setParseIntegerOnly(true);  // used while parsing the input text of textfield
        format.setParseBigDecimal(true);  // used while parsing the input text of textfield
        format.setMaximumFractionDigits(decimalLenght);  // used while formatting the doubleProperty's bound value as output text of textfield
        format.setGroupingUsed(false);  // disable thousand grouping
        format.setRoundingMode(RoundingMode.UP);  // set rounding mode
        
        textField.setTextFormatter(new TextFormatter<>(c ->
        {
            if (c.getControlNewText().isEmpty()) {
                return c;
            }
            
            ParsePosition parsePosition = new ParsePosition(0);
            Object object = format.parse(c.getControlNewText(), parsePosition);
            
            if (object == null || parsePosition.getIndex() < c.getControlNewText().length()) {
                return null;
            } else {
                return c;
            }
            
        }));
    }
    
    public final String[] splitToSql() {
        return value.getValue() == null ? null : value.getValue().split(",");
    }

    public Label getLabel(){
        return labelField;
    }
    
    public final VBox build() {
        return root;
    }
}