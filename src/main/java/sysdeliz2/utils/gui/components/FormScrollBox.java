package sysdeliz2.utils.gui.components;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

public class FormScrollBox extends VBox {
    
    private final ScrollPane scrollPane;
    private final FormBox boxPane;
    
    public enum TipoBox {HORIZONTAL, VERTICAL, FLOAT}
    
    public FormScrollBox() {
        scrollPane = new ScrollPane();
        boxPane = FormBox.create();
        
        VBox.setVgrow(scrollPane, javafx.scene.layout.Priority.ALWAYS);
        scrollPane.setHbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.NEVER);
    
        boxPane.setMinWidth(USE_COMPUTED_SIZE);
        boxPane.setMinHeight(USE_COMPUTED_SIZE);
        boxPane.setMaxWidth(USE_COMPUTED_SIZE);
        boxPane.setMaxHeight(USE_COMPUTED_SIZE);
    
        scrollPane.prefWidthProperty().bind(prefWidthProperty());
        scrollPane.prefHeightProperty().bind(prefHeightProperty());
        boxPane.prefHeightProperty().bind(scrollPane.prefHeightProperty());
        scrollPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            boxPane.width(newValue.doubleValue());
        });
        
        scrollPane.setContent(boxPane);
        getChildren().add(scrollPane);
    }
    
    /**
     * Create com FormBox gerenciável
     *
     * @return
     */
    public static final FormScrollBox create() {
        return new FormScrollBox();
    }
    
    /**
     * FormBox criado somente tela
     *
     * @param create
     */
    public static final FormScrollBox create(Consumer<FormScrollBox> create) {
        FormScrollBox box = new FormScrollBox();
        create.accept(box);
        return box;
    }
    
    public final void tipo(TipoBox tipo) {
        switch(tipo) {
            case HORIZONTAL:
                boxPane.horizontal();
                break;
            case VERTICAL:
                boxPane.vertical();
                break;
            case FLOAT:
                boxPane.flutuante();
                break;
        }
    }
    
    public final void add(Node node) {
        boxPane.add(node);
    }
    
    public final void add(Node... nodes) {
        boxPane.add(nodes);
    }
    
    public final void clear() {
        boxPane.clear();
    }
    
    public final void pannable(Boolean value) { scrollPane.setPannable(value); }
    
    public final void verticalScroll(){
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }
    
    public final void horizontalScroll(){
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    }
    
    public final void expanded() {
        HBox.setHgrow(this, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(this, javafx.scene.layout.Priority.ALWAYS);
    }
}
