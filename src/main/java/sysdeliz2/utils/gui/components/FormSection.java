package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

public class FormSection {
    
    // components section
    private final VBox root;
    private final Label title;
    private final VBox vBox;
    private final HBox hBox;
    
    // logic section
    private Boolean horizontalMode = false;
    
    // fx attributes section
    public final BooleanProperty disabled = new SimpleBooleanProperty(false);
    
    public FormSection() {
        
        title = new Label("Section");
        vBox = new VBox();
        hBox = new HBox();
        root = new VBox();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.getStyleClass().add("vbox");
        
        VBox.setVgrow(vBox, javafx.scene.layout.Priority.ALWAYS);
        VBox.setMargin(vBox, new Insets(0.0));
        VBox.setVgrow(hBox, javafx.scene.layout.Priority.ALWAYS);
        VBox.setMargin(hBox, new Insets(0.0));
        
        vBox.setSpacing(5.0);
        hBox.setSpacing(5.0);
        vBox.setPadding(new Insets(5.0));
        hBox.setPadding(new Insets(5.0));
        
        vBox.disableProperty().bind(disabled);
        hBox.disableProperty().bind(disabled);
        
        root.getChildren().add(title);
        root.getChildren().add(new Separator());
        
    }
    
    public static final FormSection create() {
        return new FormSection();
    }
    
    public static final VBox create(Consumer<FormSection> create){
        FormSection box = new FormSection();
        create.accept(box);
        return box.build();
    }
    
    public final FormSection size(double width, double hight) {
        root.setPrefSize(width, hight);
        return this;
    }
    
    public final FormSection width(double width) {
        root.setPrefWidth(width);
        return this;
    }
    
    public final FormSection height(double height) {
        root.setPrefHeight(height);
        return this;
    }
    
    public final FormSection title(String title) {
        this.title.setText(title);
        return this;
    }
    
    public final FormSection vertical() {
        root.getChildren().add(vBox);
        return this;
    }
    
    public final FormSection horizontal() {
        horizontalMode = true;
        root.getChildren().add(hBox);
        return this;
    }
    
    public final FormSection expanded() {
        HBox.setHgrow(root, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(root, javafx.scene.layout.Priority.ALWAYS);
        return this;
    }
    
    public final FormSection icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        return this;
    }
    
    public final VBox build() {
        return root;
    }
    
    public final void add(Node node) {
        if (horizontalMode)
            this.hBox.getChildren().add(node);
        else
            this.vBox.getChildren().add(node);
    }
    
    public final void add(Integer index, Node node) throws IndexOutOfBoundsException {
        if (horizontalMode)
            this.hBox.getChildren().add(index, node);
        else
            this.vBox.getChildren().add(index, node);
    }
    
    public final void remove(Node node) {
        if (horizontalMode)
            this.hBox.getChildren().remove(node);
        else
            this.vBox.getChildren().remove(node);
    }
    
    public final void remove(Integer index) throws IndexOutOfBoundsException {
        if (horizontalMode)
            this.hBox.getChildren().remove(index);
        else
            this.vBox.getChildren().remove(index);
    }
}