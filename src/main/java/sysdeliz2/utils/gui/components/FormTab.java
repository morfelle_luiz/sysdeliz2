package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

public class FormTab extends Tab {
    
    private final VBox box;
    public final BooleanProperty disabled = new SimpleBooleanProperty(false);
    
    public FormTab() {
        this.box = new VBox();
        this.box.setPadding(new Insets(5));
        this.box.setSpacing(5.0);
        
        setContent(this.box);
        setClosable(false);
        disableProperty().bind(disabled);
    }
    
    public static final FormTab create(){
        return new FormTab();
    }
    
    public static final FormTab create(Consumer<FormTab> value){
        FormTab newTab = new FormTab();
        value.accept(newTab);
        return newTab;
    }
    
    public final void title(String value){
        setText(value);
    }
    
    public final void icon(ImageView icon){
        setGraphic(icon);
    }
    
    public final void closable(){
        setClosable(true);
    }
    
    public final void close() {
       // super.get
    }
    
    public final void add(Node value){
        box.getChildren().add(value);
    }
    
    public final void add(Node...values){
        box.getChildren().addAll(values);
    }
    
    public final FormTab addStyle(String styleClass){
        getStyleClass().add(styleClass);
        return this;
    }
}
