package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ListChangeListener;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public class FormTabPane extends TabPane {
    
    public final BooleanProperty disabled = new SimpleBooleanProperty(false);
    
    private Tab currentDraggingTab = null;
    private final AtomicLong idGenerator = new AtomicLong();
    private final String draggingID = "DraggingTabPaneSupport-" + idGenerator.incrementAndGet();
    
    public FormTabPane() {
        
        setMaxHeight(Double.MAX_VALUE);
        setMaxWidth(Double.MAX_VALUE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(400.0);
        setPrefWidth(600.0);
        setTabClosingPolicy(TabClosingPolicy.ALL_TABS);
        disableProperty().bind(disabled);
        setSide(Side.TOP);
        
    }
    
    public static final FormTabPane create() {
        return new FormTabPane();
    }
    
    public static final FormTabPane create(Consumer<FormTabPane> value) {
        FormTabPane newPane = new FormTabPane();
        value.accept(newPane);
        return newPane;
    }
    
    public final void reorderTabs() {
    
        getTabs().forEach(this::addDragHandlers);
        getTabs().addListener((ListChangeListener.Change<? extends Tab> c) -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    c.getAddedSubList().forEach(this::addDragHandlers);
                    getTabs().forEach(tab -> tab.setId(String.valueOf(getTabs().indexOf(tab) + 1)));
                }
                if (c.wasRemoved()) {
                    c.getRemoved().forEach(this::removeDragHandlers);
                }
            }
        });
    
        // if we drag onto a tab pane (but not onto the tab graphic), add the tab to the end of the list of tabs:
        setOnDragOver(e -> {
            if (draggingID.equals(e.getDragboard().getString()) &&
                    currentDraggingTab != null &&
                    currentDraggingTab.getTabPane() != this) {
                e.acceptTransferModes(TransferMode.MOVE);
                getTabs().forEach(tab -> tab.setId(String.valueOf(getTabs().indexOf(tab) + 1)));
            }
        });
        setOnDragDropped(e -> {
            if (draggingID.equals(e.getDragboard().getString()) &&
                    currentDraggingTab != null &&
                    currentDraggingTab.getTabPane() != this) {
            
                currentDraggingTab.getTabPane().getTabs().remove(currentDraggingTab);
                getTabs().add(currentDraggingTab);
                currentDraggingTab.getTabPane().getSelectionModel().select(currentDraggingTab);
                getTabs().forEach(tab -> tab.setId(String.valueOf(getTabs().indexOf(tab) + 1)));
            }
        });
    }
    
    public final void side(Side value) {
        setSide(value);
    }
    
    public final void addTab(FormTab tab) {
        getTabs().add(tab);
    }
    
    public final void addTab(FormTab... tabs) {
        getTabs().addAll(tabs);
    }

    public final void addTab(Consumer<FormTab> tab) {
        FormTab formTab = new FormTab();
        tab.accept(formTab);
        getTabs().add(formTab);
    }
    
    public final void expanded() {
        VBox.setVgrow(this, Priority.ALWAYS);
        HBox.setHgrow(this, Priority.ALWAYS);
    }

    public final void clear() {
        getTabs().clear();
    }

    private void addDragHandlers (Tab tab){
        
        // move text to label graphic:
        if (tab.getText() != null && !tab.getText().isEmpty()) {
            Label label = new Label(tab.getText(), tab.getGraphic());
            tab.setText(null);
            tab.setGraphic(label);
        }
        
        Node graphic = tab.getGraphic();
        graphic.setOnDragDetected(e -> {
            Dragboard dragboard = graphic.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            // dragboard must have some content, but we need it to be a Tab, which isn't supported
            // So we store it in a local variable and just put arbitrary content in the dragbaord:
            content.putString(draggingID);
            dragboard.setContent(content);
            dragboard.setDragView(graphic.snapshot(null, null));
            currentDraggingTab = tab;
        });
        graphic.setOnDragOver(e -> {
            if (draggingID.equals(e.getDragboard().getString()) &&
                    currentDraggingTab != null &&
                    currentDraggingTab.getGraphic() != graphic) {
                e.acceptTransferModes(TransferMode.MOVE);
            }
        });
        graphic.setOnDragDropped(e -> {
            if (draggingID.equals(e.getDragboard().getString()) &&
                    currentDraggingTab != null &&
                    currentDraggingTab.getGraphic() != graphic) {
                
                int index = tab.getTabPane().getTabs().indexOf(tab);
                currentDraggingTab.getTabPane().getTabs().remove(currentDraggingTab);
                tab.getTabPane().getTabs().add(index, currentDraggingTab);
                currentDraggingTab.getTabPane().getSelectionModel().select(currentDraggingTab);
            }
        });
        graphic.setOnDragDone(e -> currentDraggingTab = null);
    }
    
    private void removeDragHandlers (Tab tab){
        tab.getGraphic().setOnDragDetected(null);
        tab.getGraphic().setOnDragOver(null);
        tab.getGraphic().setOnDragDropped(null);
        tab.getGraphic().setOnDragDone(null);
    }

}
