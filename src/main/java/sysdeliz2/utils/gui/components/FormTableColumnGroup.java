package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TreeTableColumn;

import java.util.Arrays;
import java.util.function.Consumer;

public class FormTableColumnGroup<S,T> extends FormTableColumn {
    
    public static enum Alignment {CENTER, LEFT, RIGHT}
    
//    private TableColumn column;
    public final BooleanProperty editable = new SimpleBooleanProperty(false);
    
    public FormTableColumnGroup(){
    
//        column = new TableColumn();
//        column.editableProperty().bind(editable);
    
        editableProperty().bind(editable);
        
    }
    
    public static final FormTableColumnGroup createGroup(){
        return new FormTableColumnGroup();
    }
    
    public static final <S, T> FormTableColumnGroup createGroup(Consumer<FormTableColumnGroup> create){
        FormTableColumnGroup<S, T> box = new FormTableColumnGroup();
        create.accept(box);
        return box;
    }
    
//    public final void title(String title){
////        column.setText(title);
//        setText(title);
//
//    }
//
//    public final void alignment(Alignment alignment){
////        switch(alignment){
////            case CENTER: column.setStyle("-fx-alignment: CENTER"); break;
////            case LEFT: column.setStyle("-fx-alignment: CENTER-LEFT"); break;
////            case RIGHT: column.setStyle("-fx-alignment: CENTER-RIGHT"); break;
////        }
//        switch(alignment){
//            case CENTER: setStyle("-fx-alignment: CENTER"); break;
//            case LEFT: setStyle("-fx-alignment: CENTER-LEFT"); break;
//            case RIGHT: setStyle("-fx-alignment: CENTER-RIGHT"); break;
//        }
//
//
//    }
//
//    public final void alignment(Pos alignment){
////        column.setStyle("-fx-alignment: "+alignment.name().replace("_","-"));
//        setStyle("-fx-alignment: "+alignment.name().replace("_","-"));
//
//
//    }
//
//    public final void width(double width){
////        column.setPrefWidth(width);
//        setPrefWidth(width);
//
//
//    }
//
//    public final void fontSize(int size){
//        setStyle("-fx-font-size: "+size);
////        column.setStyle("-fx-font-size: "+size);
//    }
//
//    public final FormTableColumnGroup build(){
//        return this;
////        return column;
//    }
//
//    public void order(Comparator<T> value){
//        setComparator(value);
////        column.setComparator(value);
//    }
    
    public void addColumn(TableColumn column){
        getColumns().add(column);
//        this.column.getColumns().add(column);
    }
    
    public void addColumn(FormTableColumn column){
        getColumns().add(column.build());
//        this.column.getColumns().add(column.build());
    }

    public void addColumn(TableColumn... column){
        getColumns().addAll(Arrays.asList(column));
//        this.column.getColumns().add(column);
    }

    public void addColumn(FormTableColumn... column){
        getColumns().addAll(Arrays.asList(column));
//        this.column.getColumns().add(column.build());
    }

    public void addColumn(FormTreeTableColumn column) {
        getColumns().add(column);
    }

    public void addColumn(FormTreeTableColumn... column) {
        getColumns().addAll(Arrays.asList(column));
    }
}
