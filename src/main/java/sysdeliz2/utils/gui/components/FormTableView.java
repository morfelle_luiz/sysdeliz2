package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Callback;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.Globals;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class FormTableView<T> extends FormBase {

    // components section
    private final VBox root;
    private final VBox rootTitle;
    private final HBox headerBox;
    private final HBox counterBox;
    private final HBox indiceBox;
    private final Label indice;
    private final Label title;
    private final Label registerCount;
    private final TableView<T> registers = new TableView();

    // logic section
    private Class<T> tClass;

    // fx section
    private final ListProperty<FormTableColumn> columns = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final ListProperty<T> items = new SimpleListProperty<T>(FXCollections.observableArrayList());
    public final BooleanProperty editable = new SimpleBooleanProperty(false);
    private final BooleanProperty comTotais = new SimpleBooleanProperty(false);

    public FormTableView(Class classType) {

        tClass = classType;

        root = new VBox();
        rootTitle = new VBox();
        headerBox = new HBox();
        counterBox = new HBox();
        indiceBox = new HBox();
        indice = new Label("Indice:");
        title = new Label();
        registerCount = new Label();

        root.getStylesheets().add("/styles/sysDelizDesktop.css");

        indiceBox.getChildren().add(indice);
        indiceBox.setSpacing(3);

        HBox.setHgrow(counterBox, javafx.scene.layout.Priority.ALWAYS);
        counterBox.setAlignment(Pos.BOTTOM_RIGHT);

        title.getStyleClass().add("form-field");
        rootTitle.getChildren().add(title);

        registerCount.textProperty().bind(new SimpleStringProperty("Exibindo ").concat(items.sizeProperty()).concat(" registro(s)"));
        registerCount.setFont(new Font("System Italic", 12.0));

        VBox.setVgrow(registers, javafx.scene.layout.Priority.ALWAYS);
        registers.setTableMenuButtonVisible(true);
        registers.itemsProperty().bind(items);
        registers.editableProperty().bind(editable);
        registers.disableProperty().bind(super.disable);
        //registers.

        counterBox.getChildren().add(registerCount);
        headerBox.getChildren().add(rootTitle);
        headerBox.getChildren().add(counterBox);
        root.getChildren().add(headerBox);
        root.getChildren().add(registers);
    }

    public static final FormTableView create(Class classType) {
        return new FormTableView(classType);
    }

    public static final FormTableView create(Class classType, Consumer<FormTableView> create) {
        FormTableView box = new FormTableView(classType);
        create.accept(box);
        return box;
    }

    public final void size(double width, double hight) {
        root.setPrefSize(width, hight);

    }

    public final void width(double width) {
        root.setPrefWidth(width);
    }

    public final void height(double height) {
        root.setPrefHeight(height);

    }

    public final void fontSize(int size) {
        registers.setStyle("-fx-font-size: " + size + ";" + registers.getStyle());

    }

    public final void title(String title) {
        this.title.setText(title);
    }

    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);

    }

    public final void expanded() {
        HBox.setHgrow(root, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(root, javafx.scene.layout.Priority.ALWAYS);

    }

    public final void clear() {
        items.clear();
    }

    public final void multipleSelection() {
        registers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    public final TableView<T> tableProperties() {
        return registers;
    }

    public final void selectItem(Integer index) {
        registers.getSelectionModel().select(index);

    }

    public final void selectItem(T obj) {
        registers.getSelectionModel().select(obj);

    }

    public final void setItems(ObservableList<T> items) {
        this.items.set(items);
    }

    public final void selectionModelItem(ChangeListener<? super T> listener) {
        registers.getSelectionModel().selectedItemProperty().addListener(listener);

    }

    public final void selectColumn() {
        CheckBox chboxCheckAll = new CheckBox();
        chboxCheckAll.setTooltip(new Tooltip("Marcar/Desmarcar todos"));
        chboxCheckAll.selectedProperty().addListener((observable, oldValue, newValue) -> {
            registers.getItems().forEach(item -> ((BasicModel) item).setSelected(newValue));
        });

        TableColumn<T, Boolean> clnSelect = new TableColumn();
        clnSelect.setEditable(true);
        clnSelect.setPrefWidth(30.0);
        clnSelect.setStyle("-fx-alignment: CENTER");
        clnSelect.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnSelect.setGraphic(chboxCheckAll);
        clnSelect.setSortable(true);

        clnSelect.setCellValueFactory((cellData) -> {
            T cellValue = cellData.getValue();
            BooleanProperty property = ((BasicModel) cellValue).selectedProperty();
            property.addListener((observable, oldValue, newValue) -> ((BasicModel) cellValue).setSelected(newValue));
            return property;
        });
        registers.getColumns().add(0, clnSelect);
    }

    public final T selectedItem() {
        return registers.getSelectionModel().getSelectedItem();
    }

    public final ObservableList<T> selectedRegisters() {
        return registers.getSelectionModel().getSelectedItems();
    }

    public final void addColumn(FormTableColumnGroup column) {
        registers.getColumns().add(column.build());
    }

    public final void addColumn(FormTableColumn column) {
        registers.getColumns().add(column.build());
        columns.add(column);
    }

//    public final void addColumn(TableColumn column) {
//        registers.getColumns().add(column);
//
//    }

    public final void columns(FormTableColumn... columns) {
        for (FormTableColumn column : columns) {
            registers.getColumns().add(column);
            this.columns.add(column);
        }
    }

//    public final void columns(TableColumn... columns) {
//        for (TableColumn column : columns) {
//            registers.getColumns().add(column);
//        }
//    }

    public final void withTotals() {
        comTotais.set(true);
    }

    public final void removeColumn(Integer index) {
        registers.getColumns().remove(index);
        registers.refresh();
    }

    public final void removeColumn(Integer begin, Integer end) {
        registers.getColumns().remove(begin, end);
        registers.refresh();
    }

    public final void removeColumn(FormTableColumn cln) {
        registers.getColumns().remove(cln);
        registers.refresh();
    }

    public final void clearColumns() {
        registers.getColumns().clear();
        registers.refresh();
    }
    
    public final void clearIndices() {
        indiceBox.getChildren().clear();
        root.getChildren().remove(indiceBox);
    }

    public final void indices(@NotNull List<ItemIndice> items) {
        indiceBox.getChildren().addAll(items);
        root.getChildren().add(indiceBox);
    }

    public final void indices(@NotNull ItemIndice... items) {
        indiceBox.getChildren().addAll(items);
        root.getChildren().add(indiceBox);
    }

    public final void addIndice(@NotNull ItemIndice item) {
        indiceBox.getChildren().add(item);
        if (!root.getChildren().contains(indiceBox)) root.getChildren().add(indiceBox);
    }

    public final void withoutHeader() {
        root.getChildren().remove(0);
    }

    public final void withoutTitle() {
        headerBox.getChildren().remove(0);
    }

    public final void withoutCounter() {
        headerBox.getChildren().remove(1);
    }

    public final TableView<T> tableview() {
        return registers;
    }

    public final void contextMenu(@NotNull ContextMenu contextMenu) {
        registers.setContextMenu(contextMenu);
    }

    public final ItemIndice indice(String codigo, String style, String description) {
        return new ItemIndice(description, FormFieldText.create(field -> {
            field.withoutTitle();
            field.width((codigo.length() * 5) + 20);
            field.alignment(Pos.CENTER);
            field.editable(false);
            field.value.set(codigo);
            field.addStyle("xs");
            if (style != null)
                field.addStyle(style);
        }).build());
    }

    public final ItemIndice indice(String texto, String style) {
        return new ItemIndice("", FormFieldText.create(field -> {
            field.withoutTitle();
            field.width((texto.length() * 6) + 20);
            field.alignment(Pos.CENTER);
            field.editable(false);
            field.value.set(texto);
            field.addStyle("xs");
            if (style != null)
                field.addStyle(style);
        }).build());
    }

    public final ItemIndice indice(ImageView icon, String description) {
        return new ItemIndice(description, icon);
    }

    public final ItemIndice indice(Color color, String description, Boolean circle) {
        return new ItemIndice(description, color, circle);
    }

    public final VBox build() {
        return root;
    }

    public final FormTableView addStyle(String styleClass) {
        registers.getStyleClass().add(styleClass);
        return this;
    }

    public final void refresh() {
        registers.refresh();
    }

    public final void factoryRow(Callback<TableView<T>, TableRow<T>> value) {
        registers.setRowFactory(value);
    }

    public final TableRow<T> setDragAndDrop(TableRow<T> row, Consumer<TableRow<T>> drag, Consumer<TableRow<T>> drop) {
        if (drag != null)
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();

                    drag.accept(row);

                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(Globals.SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
        row.setOnDragOver(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasContent(Globals.SERIALIZED_MIME_TYPE)) {
                if (row.getIndex() != ((Integer) db.getContent(Globals.SERIALIZED_MIME_TYPE)).intValue()) {
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    event.consume();
                }
            }
        });
        if (drop != null)
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(Globals.SERIALIZED_MIME_TYPE)) {
                    drop.accept(row);

                    event.setDropCompleted(true);
                    event.consume();
                }
            });
        return row;
    }

    public class ItemIndice extends Label {

        public ItemIndice(String text, Node graphic) {
            super(text, graphic);
        }

        public ItemIndice(String text, Color color, Boolean circle) {
            super(text);
            setGraphic(circle ? getCircle(color) : getRectangle(color));
        }

        private Circle getCircle(Color color) {
            Circle circle = new Circle();

            circle.setFill(color);
            circle.setLayoutX(170.0);
            circle.setLayoutY(147.0);
            circle.setRadius(8.0);
            circle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);
            circle.setStrokeWidth(0.0);

            return circle;
        }

        private Rectangle getRectangle(Color color) {
            Rectangle retangle = new Rectangle();

            retangle.setFill(color);
            retangle.setHeight(16.0);
            retangle.setLayoutX(191.0);
            retangle.setLayoutY(139.0);
            retangle.setSmooth(false);
            retangle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);
            retangle.setStrokeWidth(0.0);
            retangle.setWidth(24.0);

            return retangle;
        }
    }

}