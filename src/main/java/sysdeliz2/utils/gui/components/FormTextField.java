package sysdeliz2.utils.gui.components;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public abstract class FormTextField extends VBox {
    
    private final Label titleFormField = new Label();
    public final TextField tboxField = new TextField();
    
    public FormTextField(String titleField) {
        createField(titleField, true, 80);
    }
    
    protected FormTextField(String titleField, Boolean editable) {
        createField(titleField, editable, 80);
    }
    
    public FormTextField(String titleField, Boolean editable, Integer widht) {
        createField(titleField, editable, widht);
    }
    
    protected FormTextField(String titleField, Integer widht) {
        createField(titleField, true, widht);
    }
    
    private void createField(String titleField, Boolean editable, Integer width){
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
    
        titleFormField.setText(titleField.toUpperCase());
        titleFormField.getStyleClass().addAll("label-form-field-v");
        tboxField.getStyleClass().addAll("text-field","text-field-form-field-v","form-field", !editable ? "no-editable" : "");
        tboxField.setEditable(editable);
        tboxField.setPrefWidth(width.doubleValue());
        this.setMaskField(tboxField);
    
        setPrefHeight(0.0);
        setPrefWidth(width.doubleValue());
        getChildren().add(titleFormField);
        getChildren().add(tboxField);
    }
    
    protected abstract void setMaskField(TextField tboxField);
}
