package sysdeliz2.utils.gui.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.gui.MaskTextField;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class FormTimeField extends VBox {
    
    private final Label titleFormField = new Label();
    private final TextField tboxField = new TextField();
    public final StringProperty text = new SimpleStringProperty();
    public final ObjectProperty<LocalTime> time = new SimpleObjectProperty<>();
    
    public FormTimeField(String titleField) {
        createField(titleField, true, 80);
    }
    
    public FormTimeField(String titleField, Boolean editable) {
        createField(titleField, editable, 80);
    }
    
    public FormTimeField(String titleField, Boolean editable, Integer widht) {
        createField(titleField, editable, widht);
    }
    
    public FormTimeField(String titleField, Integer widht) {
        createField(titleField, true, widht);
    }
    
    private void createField(String titleField, Boolean editable, Integer width){
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
        
        titleFormField.setText(titleField.toUpperCase());
        titleFormField.getStyleClass().addAll("label-form-field-v");
        tboxField.getStyleClass().addAll("text-field","text-field-form-field-v","form-field", !editable ? "no-editable" : "");
        tboxField.setEditable(editable);
        tboxField.setPrefWidth(width.doubleValue());
        MaskTextField.timeField(tboxField);
        text.bind(tboxField.textProperty());
        
        setPrefHeight(0.0);
        setPrefWidth(width.doubleValue());
        getChildren().add(titleFormField);
        getChildren().add(tboxField);
    }
    
    public String getText() {
        return text.get();
    }
    
    public StringProperty textProperty() {
        return tboxField.textProperty();
    }
    
    public void setText(String text) {
        this.tboxField.setText(text);
    }
    
    public LocalTime getTime() {
        return LocalTime.parse(getText(), DateTimeFormatter.ofPattern("HH:mm"));
    }
    
    public ObjectProperty<LocalTime> timeProperty() {
        return time;
    }
    
    public void setTime(LocalTime time) {
        this.time.set(time);
        this.tboxField.setText(time.format(DateTimeFormatter.ofPattern("HH:mm")));
    }
}
