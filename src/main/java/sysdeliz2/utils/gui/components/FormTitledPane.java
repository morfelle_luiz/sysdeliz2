package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

public class FormTitledPane extends TitledPane {
    
    // components section
    //private final TitledPane root;
    private final VBox box;
    private final ImageView icon;
    private final HBox boxFilter;
    private final Separator separatorFilter;
    private final VBox boxButtonsFilter;
    public final Button find;
    public final Button clean;
    private final ImageView iconButtonFind;
    private final ImageView iconButtonClean;
    
    // fx variables section
    public final BooleanProperty collapsabled = new SimpleBooleanProperty(true);
    public final BooleanProperty disabled = new SimpleBooleanProperty(false);
    
    public FormTitledPane() {
        
        //root = new TitledPane();
        box = new VBox();
        icon = new ImageView();
        boxFilter = new HBox();
        separatorFilter = new Separator();
        boxButtonsFilter = new VBox();
        find = new Button();
        iconButtonFind = new ImageView();
        clean = new Button();
        iconButtonClean = new ImageView();
        
        getStylesheets().add("/styles/stylePadrao.css");
        expandedProperty().bindBidirectional(collapsabled);
        disableProperty().bind(disabled);
        setMaxHeight(Double.MAX_VALUE);
        setMaxWidth(Double.MAX_VALUE);
    
        box.setPadding(new Insets(5.0));
        box.setSpacing(5.0);
        setContent(box);
        
        icon.setFitHeight(16.0);
        icon.setFitWidth(16.0);
        icon.setPickOnBounds(true);
        icon.setPreserveRatio(true);
        icon.setImage(new Image(getClass().getResource("/images/icons/buttons/filter (1).png").toExternalForm()));
    
        iconButtonFind.setFitHeight(16.0);
        iconButtonFind.setFitWidth(16.0);
        iconButtonFind.setPickOnBounds(true);
        iconButtonFind.setPreserveRatio(true);
        iconButtonFind.setImage(new Image(getClass().getResource("/images/icons/buttons/search (1).png").toExternalForm()));
    
        iconButtonClean.setFitHeight(16.0);
        iconButtonClean.setFitWidth(16.0);
        iconButtonClean.setPickOnBounds(true);
        iconButtonClean.setPreserveRatio(true);
        iconButtonClean.setImage(new Image(getClass().getResource("/images/icons/buttons/clean (1).png").toExternalForm()));
    
        find.setAlignment(javafx.geometry.Pos.BASELINE_LEFT);
        find.setMnemonicParsing(false);
        find.setPrefWidth(85.0);
        find.getStyleClass().add("primary");
        find.setText("Consultar");
        find.setGraphic(iconButtonFind);
        
        HBox.setMargin(separatorFilter, new Insets(0,0,0, 5));
        separatorFilter.setOrientation(javafx.geometry.Orientation.VERTICAL);
    
        clean.setAlignment(javafx.geometry.Pos.BASELINE_LEFT);
        clean.setMnemonicParsing(false);
        clean.setPrefWidth(85.0);
        clean.getStyleClass().add("warning");
        clean.setText("Limpar");
        clean.setGraphic(iconButtonClean);
    
        boxButtonsFilter.setAlignment(javafx.geometry.Pos.BOTTOM_RIGHT);
        boxButtonsFilter.setSpacing(5.0);
        boxButtonsFilter.getChildren().add(find);
        boxButtonsFilter.getChildren().add(clean);
        
    }
    
    public static final FormTitledPane create() {
        return new FormTitledPane(){};
    }
    
    public static final FormTitledPane create(Consumer<FormTitledPane> create){
        FormTitledPane box = new FormTitledPane(){};
        create.accept(box);
        return box;
    }
    
    public final FormTitledPane width(double width) {
        setPrefWidth(width);
        setMinWidth(width);
        return this;
    }
    
    public final FormTitledPane height(double height) {
        setPrefHeight(height);
        return this;
    }
    
    public final FormTitledPane size(double width, double height) {
        setPrefSize(width, height);
        return this;
    }

    public final FormTitledPane padding(double padding) {
        box.setPadding(new Insets(padding));
        return this;
    }
    
    public final FormTitledPane title(String title) {
        setText(title);
        return this;
    }
    
    public final FormTitledPane filter() {
        getStyleClass().add("filter");
        setText("Filtros");
        setGraphic(icon);
        box.setPadding(new Insets(0.0));
        HBox.setHgrow(box, javafx.scene.layout.Priority.ALWAYS);
        boxFilter.setPadding(new Insets(5.0));
        boxFilter.setSpacing(5.0);
        boxFilter.getChildren().add(box);
        boxFilter.getChildren().add(separatorFilter);
        boxFilter.getChildren().add(boxButtonsFilter);
        setContent(boxFilter);
        setMinWidth(700.0);
        return this;
    }
    
    public final FormTitledPane fixed() {
        setCollapsible(false);
        return this;
    }
    
    public final FormTitledPane collapse(Boolean collapse) {
        setExpanded(collapse);
        return this;
    }
    
    public final FormTitledPane expanded() {
        HBox.setHgrow(this, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(this, javafx.scene.layout.Priority.ALWAYS);
        return this;
    }
    
    public final FormTitledPane icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        setGraphic(iconButton);
        return this;
    }
    
    public final FormTitledPane addStyle(String styleClass) {
        getStyleClass().add(styleClass);
        return this;
    }
    
    public final FormTitledPane removeStyle(String styleClass) {
        getStyleClass().remove(styleClass);
        return this;
    }
    
    public final void add(Node node) {
        this.box.getChildren().add(node);
    }

    public final void add(Consumer<FormBox> box) {
        FormBox formBox = new FormBox();
        box.accept(formBox);
        this.box.getChildren().add(formBox);
    }
    
    public final void addAll(Node...node) {
        this.box.getChildren().addAll(node);
    }
    
    public final void add(Integer index, Node node) throws IndexOutOfBoundsException {
        this.box.getChildren().add(index, node);
    }
    
    public final void remove(Node node) {
        this.box.getChildren().remove(node);
    }
    
    public final void remove(Integer index) throws IndexOutOfBoundsException {
        this.box.getChildren().remove(index);
    }
    
    public final TitledPane build() {
        return this;
    }

    public VBox getBox() {
        return box;
    }
}