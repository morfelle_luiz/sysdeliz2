package sysdeliz2.utils.gui.components;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import sysdeliz2.utils.ToggleSwitch;

public class FormToggleField extends VBox {
    
    private final Label titleFormField = new Label();
    private final Pane tboxToggleSwitch = new Pane();
    private final Label lbToggleActive = new Label("SIM");
    private final ToggleSwitch toggleSwitch = new ToggleSwitch(20, 50);
    public final BooleanProperty isSwitchedOn = new SimpleBooleanProperty(true);
    
    public FormToggleField(String titleField) {
        createField(titleField, true, true);
    }
    
    public FormToggleField(String titleField, Boolean defaultValue) {
        createField(titleField, defaultValue, true);
    }
    
    public FormToggleField(String titleField, Boolean defaultValue, Boolean capsTitle) {
        createField(titleField, defaultValue, capsTitle);
    }
    
    private void createField(String titleField, Boolean defaultValue, Boolean capsTitle){
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
        
        titleFormField.setText(capsTitle ? titleField.toUpperCase() : titleField);
        titleFormField.getStyleClass().addAll("label-form-field-v");
        lbToggleActive.textProperty().bind(Bindings.when(toggleSwitch.switchedOnProperty()).then("SIM").otherwise("NÃO"));
        lbToggleActive.setLayoutX(65.0);
        lbToggleActive.setLayoutY(5.0);
        lbToggleActive.setFont(new Font("System Bold", 12.0));
    
        toggleSwitch.setLayoutX(4.0);
        toggleSwitch.setLayoutY(4.0);
        toggleSwitch.switchedOnProperty().bindBidirectional(isSwitchedOn);
        isSwitchedOn.set(defaultValue);
        
        tboxToggleSwitch.setPrefHeight(30.0);
        tboxToggleSwitch.setPrefWidth(227.0);
        tboxToggleSwitch.getStyleClass().add("corner-up-left");
    
        tboxToggleSwitch.getChildren().add(toggleSwitch);
        tboxToggleSwitch.getChildren().add(lbToggleActive);
        
        setPrefHeight(50.0);
        setPrefWidth(100.0);
        getChildren().add(titleFormField);
        getChildren().add(tboxToggleSwitch);
    }
    
}
