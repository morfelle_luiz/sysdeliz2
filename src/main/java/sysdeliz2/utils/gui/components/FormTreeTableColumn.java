package sysdeliz2.utils.gui.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Consumer;

public class FormTreeTableColumn<S,T> extends TreeTableColumn {
    
//    private TableColumn column;
    public final BooleanProperty editable = new SimpleBooleanProperty(false);
    public final BooleanProperty withTotals = new SimpleBooleanProperty(false);
    public final BooleanProperty visible = new SimpleBooleanProperty(true);
    
    public FormTreeTableColumn(){
    
//        column = new TableColumn();
//        column.editableProperty().bind(editable);
        editableProperty().bind(editable);
        visibleProperty().bindBidirectional(visible);
        
    }
    
    public static final FormTreeTableColumn create(){
        return new FormTreeTableColumn();
    }
    
    public static final <S, T> FormTreeTableColumn create(Consumer<FormTreeTableColumn> create){
        FormTreeTableColumn<S, T> box = new FormTreeTableColumn();
        create.accept(box);
        return box;
    }
    
    public final void title(String title){
//        column.setText(title);
        setText(title);
    }
    
    public final void icon(ImageView icon){
//        column.setGraphic(icon);
        setGraphic(icon);
    }
    
    public final void withTotals(Boolean value){
        withTotals.set(value);
    }
    
    public final void alignment(Pos alignment){
//        column.setStyle("-fx-alignment: "+alignment.name().replace("_","-"));
        setStyle("-fx-alignment: "+alignment.name().replace("_","-"));
    }
    
    public final void width(double width){
//        column.setPrefWidth(width);
        setPrefWidth(width);
    }
    
    public final void fontSize(int size){
//        column.setStyle("-fx-font-size: "+size);
        setStyle("-fx-font-size: "+size);
    }
    
    public final void editable(){
        editable.set(true);
        setCellFactory(param -> {
            return new TreeTableCell<S, T>(){
                private TextField textField;
    
                @Override
                public void startEdit() {
                    super.startEdit();
        
                    if (textField == null) {
                        createTextField();
                    }
        
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    textField.selectAll();
                }
    
                @Override
                public void cancelEdit() {
                    super.cancelEdit();
        
                    setText(String.valueOf(getItem()));
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
    
                @Override
                public void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
        
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        if (isEditing()) {
                            if (textField != null) {
                                textField.setText(getString());
                            }
                            setGraphic(textField);
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        } else {
                            setText(getString());
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                        }
                    }
                }
    
                private void createTextField() {
                    textField = new TextField(getString());
                    textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
                    textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            
                        @Override
                        public void handle(KeyEvent t) {
                            if (t.getCode() == KeyCode.ENTER) {
                                commitEdit((T) textField.getText());
                            } else if (t.getCode() == KeyCode.ESCAPE) {
                                cancelEdit();
                            }
                        }
                    });
                }
    
                private String getString() {
                    return getItem() == null ? "" : getItem().toString();
                }
            };
        });
    }
    
    public final FormTreeTableColumn build(){
//        return column;
        return this;
    }
    
    public void hide() {
//        column.setVisible(false);
        visible.set(false);
    }
    
    public void unhide() {
//        column.setVisible(true);
        setVisible(true);
    }
    
    public void value(Callback<TreeTableColumn.CellDataFeatures<S,T>, ObservableValue<T>> value){
//        column.setCellValueFactory(value);
        setCellValueFactory(value);
    }
    
    public void format(Callback<TreeTableColumn<S,T>, TreeTableCell<S,T>> value){
//        column.setCellFactory(value);
        setCellFactory(value);
    }
    
    public void order(Comparator<T> value){
//        column.setComparator(value);
        setComparator(value);
    }
    
    public final FormTreeTableColumn style(String...styles){
//        column.getStyleClass().addAll(styles);
        getStyleClass().addAll(Arrays.asList(styles));
        return this;
    }
}
