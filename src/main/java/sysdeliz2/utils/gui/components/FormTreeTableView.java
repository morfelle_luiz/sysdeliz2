package sysdeliz2.utils.gui.components;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import sysdeliz2.models.generics.BasicModel;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class FormTreeTableView<T> extends FormBase {
    
    // components section
    private final VBox root;
    private final VBox rootTitle;
    private final HBox headerBox;
    private final HBox titleBox;
    private final HBox indiceBox;
    private final Label indice;
    private final Label title;
    private final TreeTableView<T> registers = new TreeTableView();
    
    // logic section
    private Class<T> tClass;
    
    // fx section
    private final ListProperty<FormTreeTableColumn> columns = new SimpleListProperty<>(FXCollections.observableArrayList());
    public final ObjectProperty<TreeItem<T>> treeRootItem = new SimpleObjectProperty<>();
    public final BooleanProperty editable = new SimpleBooleanProperty(false);
    
    public FormTreeTableView(Class classType) {
        
        tClass = classType;
        
        root = new VBox();
        rootTitle = new VBox();
        headerBox = new HBox();
        titleBox = new HBox();
        indiceBox = new HBox();
        indice = new Label("Indice:");
        title = new Label();
        
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        
        indiceBox.getChildren().add(indice);
        indiceBox.setSpacing(3);
        
        HBox.setHgrow(titleBox, javafx.scene.layout.Priority.ALWAYS);
        title.getStyleClass().add("form-field");
        rootTitle.getChildren().add(title);
        
        VBox.setVgrow(registers, javafx.scene.layout.Priority.ALWAYS);
        registers.setTableMenuButtonVisible(true);
        registers.editableProperty().bind(editable);
        //registers.rootProperty().bind(treeRootItem);
        registers.setShowRoot(false);
        
        titleBox.getChildren().add(rootTitle);
        headerBox.getChildren().add(titleBox);
        root.getChildren().add(headerBox);
        root.getChildren().add(registers);
    }
    
    public static final FormTreeTableView create(Class classType) {
        return new FormTreeTableView(classType);
    }
    
    public static final FormTreeTableView create(Class classType, Consumer<FormTreeTableView> create) {
        FormTreeTableView box = new FormTreeTableView(classType);
        create.accept(box);
        return box;
    }
    
    public final void size(double width, double hight) {
        root.setPrefSize(width, hight);
    }
    
    public final void width(double width) {
        root.setPrefWidth(width);
    }
    
    public final void height(double height) {
        root.setPrefHeight(height);
        
    }
    
    public final void title(String title) {
        this.title.setText(title);
    }
    
    public final void icon(Image icon) {
        ImageView iconButton = new ImageView();
        iconButton.setFitHeight(16.0);
        iconButton.setFitWidth(16.0);
        iconButton.setPickOnBounds(true);
        iconButton.setPreserveRatio(true);
        iconButton.setImage(icon);
        this.title.setGraphic(iconButton);
        
    }
    
    public final void expanded() {
        HBox.setHgrow(root, javafx.scene.layout.Priority.ALWAYS);
        VBox.setVgrow(root, javafx.scene.layout.Priority.ALWAYS);
        
    }
    
    public final void clear() {
        registers.setRoot(null);
    }
    
    public final void multipleSelection() {
        registers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    
    public final TreeTableView<T> properties() {
        return registers;
    }
    
    public final void selectItem(Integer index) {
        registers.getSelectionModel().select(index);
        
    }
    
    public final void selectItem(T obj) {
        //registers.getSelectionModel().select(obj);
        
    }
    
    public final void setItens(Function itens) {
        itens.apply(null);
    }
    
    public final void selectionModelItem(ChangeListener<? super T> listener) {
        //registers.getSelectionModel().selectedItemProperty().addListener(listener);
        
    }
    
    public final void selectColumn() {
        CheckBox chboxCheckAll = new CheckBox();
        chboxCheckAll.setTooltip(new Tooltip("Marcar/Desmarcar todos"));
        chboxCheckAll.selectedProperty().addListener((observable, oldValue, newValue) -> {
            //registers.getItems().forEach(item -> ((BasicModel) item).setSelected(newValue));
        });
        
        TableColumn<T, Boolean> clnSelect = new TableColumn();
        clnSelect.setEditable(true);
        clnSelect.setPrefWidth(25.0);
        clnSelect.setStyle("-fx-alignment: CENTER");
        clnSelect.setCellFactory((col) -> new CheckBoxTableCell<>());
        clnSelect.setGraphic(chboxCheckAll);
        clnSelect.setCellValueFactory((cellData) -> {
            T cellValue = cellData.getValue();
            BooleanProperty property = ((BasicModel) cellValue).selectedProperty();
            property.addListener((observable, oldValue, newValue) -> ((BasicModel) cellValue).setSelected(newValue));
            return property;
        });
        //registers.getColumns().add(0, clnSelect);
    }
    
    public final TreeItem<T> selectedItem() {
        return registers.getSelectionModel().getSelectedItem();
    }
    
    public final ObservableList<TreeItem<T>> selectedRegisters() {
        return registers.getSelectionModel().getSelectedItems();
    }
    
    public final void addColumn(TreeTableColumn<T, ?> column) {
        registers.getColumns().add(column);
    }

    public final void addColumn(FormTreeTableColumnGroup group) {
        registers.getColumns().add(group);
    }

    public final void columns(TreeTableColumn<T, ?>... columns) {
        for (TreeTableColumn column : columns) {
            this.addColumn(column);
        }
    }
    
    public final void removeColumn(Integer index) {
        registers.getColumns().remove(index);
        registers.refresh();
    }
    
    public final void removeColumn(Integer begin, Integer end) {
        registers.getColumns().remove(begin, end);
        registers.refresh();
    }
    
    public final void indices(@NotNull List<ItemIndice> items) {
        indiceBox.getChildren().addAll(items);
        root.getChildren().add(indiceBox);
    }
    
    public final void indices(@NotNull ItemIndice... items) {
        indiceBox.getChildren().addAll(items);
        root.getChildren().add(indiceBox);
    }
    
    public final void contextMenu(@NotNull ContextMenu contextMenu) {
        registers.setContextMenu(contextMenu);
    }
    
    public final ItemIndice indice(ImageView icon, String description) {
        return new ItemIndice(description, icon);
    }
    
    public final ItemIndice indice(Color color, String description, Boolean circle) {
        return new ItemIndice(description, color, circle);
    }
    
    public final ItemIndice indice(String codigo, String style, String description) {
        return new ItemIndice(description, FormFieldText.create(field -> {
            field.withoutTitle();
            field.width((codigo.length() * 5) + 20);
            field.alignment(Pos.CENTER);
            field.editable(false);
            field.value.set(codigo);
            field.addStyle("xs");
            if (style != null)
                field.addStyle(style);
        }).build());
    }
    
    public final VBox build() {
        return root;
    }
    
    public final FormTreeTableView addStyle(String styleClass) {
        registers.getStyleClass().add(styleClass);
        return this;
    }
    
    public final void refresh() {
        registers.refresh();
    }
    
    public final void factoryRow(Callback<TreeTableView<T>, TreeTableRow<T>> value) {
       registers.setRowFactory(value);
    }
    
    public class ItemIndice extends Label {
        
        public ItemIndice(String text, Node graphic) {
            super(text, graphic);
        }
        
        public ItemIndice(String text, Color color, Boolean circle) {
            super(text);
            setGraphic(circle ? getCircle(color) : getRectangle(color));
        }
        
        private Circle getCircle(Color color) {
            Circle circle = new Circle();
            
            circle.setFill(color);
            circle.setLayoutX(170.0);
            circle.setLayoutY(147.0);
            circle.setRadius(8.0);
            circle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);
            circle.setStrokeWidth(0.0);
            
            return circle;
        }
        
        private Rectangle getRectangle(Color color) {
            Rectangle retangle = new Rectangle();
            
            retangle.setFill(color);
            retangle.setHeight(16.0);
            retangle.setLayoutX(191.0);
            retangle.setLayoutY(139.0);
            retangle.setSmooth(false);
            retangle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);
            retangle.setStrokeWidth(0.0);
            retangle.setWidth(24.0);
            
            return retangle;
        }
    }

    public TreeTableView<T> getRegisters() {
        return registers;
    }
}