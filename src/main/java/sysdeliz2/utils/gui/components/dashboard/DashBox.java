package sysdeliz2.utils.gui.components.dashboard;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import sysdeliz2.utils.enums.Style;

import java.util.function.Consumer;

public class DashBox extends VBox {
    
    // components section
    private final VBox boxDados;
    protected final HBox boxField;
    protected final Label infoDash;
    protected final Label titleDash;
    protected final Label valueDash;
    private Glyph glyph = new FontAwesome().create(FontAwesome.Glyph.HOME);
    
    // fx components section
    public final StringProperty value = new SimpleStringProperty();
    
    public DashBox() {
        boxDados = new VBox();
        boxField = new HBox();
        infoDash = new Label();
        titleDash = new Label();
        valueDash = new Label();
        
        getStylesheets().add("/styles/sysDelizDesktop.css");
        setPadding(new Insets(3.0, 6.0, 3.0, 6.0));
        
        infoDash.setMaxHeight(Double.MAX_VALUE);
        infoDash.setMaxWidth(Double.MAX_VALUE);
        infoDash.setAlignment(Pos.TOP_LEFT);
    
        titleDash.setMaxHeight(Double.MAX_VALUE);
        titleDash.setMaxWidth(Double.MAX_VALUE);
        titleDash.setAlignment(Pos.BOTTOM_RIGHT);
    
        valueDash.setMaxHeight(Double.MAX_VALUE);
        valueDash.setMaxWidth(Double.MAX_VALUE);
        valueDash.textProperty().bind(value);
        valueDash.setAlignment(Pos.CENTER_RIGHT);
        VBox.setVgrow(valueDash, Priority.ALWAYS);
    
        boxDados.getChildren().add(valueDash);
        boxDados.getChildren().add(titleDash);
        boxField.getChildren().add(infoDash);
        boxField.getChildren().add(boxDados);
        getChildren().add(boxField);
    }
    
    public static final DashBox create() {
        return new DashBox();
    }
    
    public static final DashBox create(Consumer<DashBox> value) {
        DashBox box = new DashBox();
        value.accept(box);
        return box;
    }
    
    public final void addStyle(Style style) {
        getStyleClass().add(style.styleClass);
        infoDash.getStyleClass().add(style.styleClass);
        titleDash.getStyleClass().add(style.styleClass);
        valueDash.getStyleClass().add(style.styleClass);
        valueDash.getStyleClass().add(style.styleClass);
        glyph.getStyleClass().add(style.styleClass);
    }
    
    public final void removeStyle(Style style) {
        getStyleClass().remove(style.styleClass);
        infoDash.getStyleClass().remove(style.styleClass);
        titleDash.getStyleClass().remove(style.styleClass);
        valueDash.getStyleClass().remove(style.styleClass);
    }
    
    public final void clearStyle() {
        getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
        infoDash.getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
        titleDash.getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
        valueDash.getStyleClass().removeAll("danger", "amber", "warning", "primary", "info", "default", "secundary", "dark", "success", "mostruario");
    }
    
    public final void value(Double size) {
        valueDash.setStyle(valueDash.getStyle() + "-fx-font-size: "+size+";");
    }
    
    public final void value(String value, Double size) {
        valueDash.setText(value);
        valueDash.setStyle(valueDash.getStyle() + "-fx-font-size: "+size+";");
    }
    
    public final void title(String title, String sizeStyle) {
        titleDash.setText(title);
        titleDash.getStyleClass().add(sizeStyle);
    }
    
    public final void icon(String label, Double size) {
        infoDash.setText(label);
        infoDash.setText(null);
        infoDash.setStyle(infoDash.getStyle() + "-fx-font-size: "+size+";");
    }
    
    public final void icon(ImageView icon) {
        infoDash.setGraphic(icon);
        infoDash.setText(null);
    }
    
    public final void icon(FontAwesome.Glyph icon, Double size) {
        glyph.size(size);
        glyph.fontFamily(icon.name());
        infoDash.setGraphic(glyph);
        infoDash.setText(null);
    }
    
}
