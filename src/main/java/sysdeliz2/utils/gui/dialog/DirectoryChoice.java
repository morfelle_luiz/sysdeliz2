package sysdeliz2.utils.gui.dialog;

import javafx.stage.DirectoryChooser;
import sysdeliz2.utils.Globals;

import java.io.File;
import java.util.function.Consumer;

public class DirectoryChoice {
    
    //fx section
    private DirectoryChooser fileChooser = new DirectoryChooser();
    
    public DirectoryChoice() {
    
    }
    
    public static DirectoryChoice show(Consumer<DirectoryChoice> value) {
        DirectoryChoice box = new DirectoryChoice();
        value.accept(box);
        return box;
    }
    
    public static DirectoryChoice show() {
        DirectoryChoice box = new DirectoryChoice();
        return box;
    }
    
    public String stringPath(){
        return fileChooser.showDialog(Globals.getMainStage()).getAbsolutePath();
    }
    
    public File filePath(){
        return fileChooser.showDialog(Globals.getMainStage());
    }
    
}
