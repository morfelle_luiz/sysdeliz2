package sysdeliz2.utils.gui.dialog;

import javafx.stage.FileChooser;
import sysdeliz2.utils.Globals;

import java.io.File;
import java.util.function.Consumer;

public class FileChoice {
    
    //fx section
    private FileChooser fileChooser = new FileChooser();
    
    public FileChoice() {
    
    }
    
    public static FileChoice show(Consumer<FileChoice> value) {
        FileChoice box = new FileChoice();
        value.accept(box);
        return box;
    }
    
    public static FileChoice show() {
        FileChoice box = new FileChoice();
        return box;
    }
    
    public void addExtension(FileChooser.ExtensionFilter extension){
        fileChooser.getExtensionFilters().add(extension);
    }

    public void basePath(String baseUrl){
        fileChooser.setInitialDirectory(new File(baseUrl));
    }
    
    public void addAllExtension(FileChooser.ExtensionFilter...extensions){
        fileChooser.getExtensionFilters().addAll(extensions);
    }
    
    public String stringSavePath(){
        File path = fileChooser.showSaveDialog(Globals.getMainStage());
        if (path == null)
            return null;
        return path.getAbsolutePath();
    }
    
    public File fileSavePath(){
        return fileChooser.showSaveDialog(Globals.getMainStage());
    }

    public String stringOpenPath(){
        return fileChooser.showOpenDialog(Globals.getMainStage()).getAbsolutePath();
    }

    public File fileOpenPath(){
        return fileChooser.showOpenDialog(Globals.getMainStage());
    }
    
}
