package sysdeliz2.utils.gui.messages;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.utils.mails.SimpleMail;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;

public class ExceptionBox extends MessageBox {
    
    //components section
    private final VBox boxInput;
    private final Button buttonReport;
    private final Button buttonOk;
    private final HBox boxButtonReport;
    
    //switch components
    TextArea field = new TextArea();
    private String mailtext = "";
    
    public ExceptionBox() {
        boxInput = new VBox();
        buttonReport = new Button();
        buttonOk = new Button();
        boxButtonReport = new HBox();
        
        super.type(TypeMessageBox.RISK);
    
        VBox.setVgrow(field, Priority.ALWAYS);
        field.setEditable(false);
        field.setMaxWidth(Double.MAX_VALUE);
        field.setMaxHeight(Double.MAX_VALUE);
    
        boxInput.setPrefSize(600.0,400.0);
        boxInput.getChildren().add(new Label("Stack da exceção encontrada:"));
        boxInput.getChildren().add(field);
        VBox.setVgrow(boxInput, Priority.ALWAYS);
        super.root.getChildren().add(1, boxInput);
        
        buttonReport.getStyleClass().add("warning");
        buttonReport.setText("Reportar Erro");
        buttonReport.setOnAction(event -> {
            SimpleMail.INSTANCE.addDestinatario("diego@deliz.com.br")
                    .addCC("luiz@deliz.com.br")
                    .comAssunto("Exception report SysDeliz2 [Usuário]")
                    .comCorpo(mailtext)
                    .send();
            stage.close();
        });
        HBox.setHgrow(boxButtonReport, Priority.ALWAYS);
        boxButtonReport.getChildren().add(buttonReport);
        buttonOk.getStyleClass().add("danger");
        buttonOk.setText("OK");
        buttonOk.setPrefWidth(60.0);
        buttonOk.setOnAction(event -> stage.close());
        boxButtons.getChildren().clear();
        boxButtons.getChildren().add(boxButtonReport);
        boxButtons.getChildren().add(buttonOk);
    }
    
    public static ExceptionBox build() {
        return new ExceptionBox();
    }
    
    public static ExceptionBox build(Consumer<ExceptionBox> value) {
        ExceptionBox box = new ExceptionBox();
        value.accept(box);
        return box;
    }
    
    public final void exception(Exception ex){
        
        super.message("Aconteceu um erro no sistema!\nMensagem do sistema: " + ex.getMessage());
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();
        mailtext += "Exception reportado por: " + SceneMainController.getAcesso() == null ? "SEM LOGIN" : SceneMainController.getAcesso().getUsuario();
        mailtext += ex.getLocalizedMessage() + "\n\n";
        mailtext += exceptionText + "\n\n";
        field.setText(exceptionText);
    }
    
    public final void sendAutomaticMail(){
        SimpleMail.INSTANCE.addDestinatario("diego@deliz.com.br")
                .addCC("luiz@deliz.com.br")
                .comAssunto("Exception report SysDeliz2 [Usuário]")
                .comCorpo(mailtext)
                .send();
        stage.close();
    }
    
    public final void objects(Object...objects) {
        for (Object object : objects) {
            //mailtext += "Object: " + object.getClass().getName() + "\n" + new Gson().toJson(object) + "\n\n";
        }
    }
}
