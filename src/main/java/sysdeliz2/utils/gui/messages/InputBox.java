package sysdeliz2.utils.gui.messages;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.components.FormFieldDate;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldToggle;
import sysdeliz2.utils.hibernate.DefaultFilter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class InputBox<T> extends MessageBox {

    //components section
    private final VBox boxInput;
    private final Button buttonConfirm;
    private final Button buttonCancel;

    //switch components
    TextField field = new TextField();
    FormFieldToggle fieldToggle = FormFieldToggle.create().withoutTitle();
    FormFieldDate fieldDate = FormFieldDate.create().withoutTitle();
    public FormFieldSingleFind<T> fieldObject = null;
    
    //fx section
    public final ObjectProperty<T> value = new SimpleObjectProperty<>(null);

    public InputBox(Class<T> classType) {
        boxInput = new VBox();
        buttonConfirm = new Button();
        buttonCancel = new Button();

        // <editor-fold defaultstate="collapsed" desc="Create component">
        if (classType.getTypeName().contains("String")) {
            boxInput.getChildren().add(field);
        } else if (classType.getTypeName().toLowerCase().contains("int")) {
            TextFieldUtils.integerMask(field);
            boxInput.getChildren().add(field);
        } else if (classType.getTypeName().toLowerCase().contains("double")) {
            TextFieldUtils.decimalMask(field);
            boxInput.getChildren().add(field);
        } else if (classType.getTypeName().toLowerCase().contains("bigdecimal")) {
            TextFieldUtils.decimalMask(field);
            boxInput.getChildren().add(field);
        } else if (classType.getTypeName().toLowerCase().contains("bool")) {
            boxInput.getChildren().add(fieldToggle.build());
        } else if (classType.getTypeName().toLowerCase().contains("localdate")) {
            boxInput.getChildren().add(fieldDate.build());
        } else if (classType.getTypeName().toLowerCase().contains("localdatetime")) {

        } else if (classType.getTypeName().toLowerCase().contains("localtime")) {

        } else {
            fieldObject = FormFieldSingleFind.create(classType, field -> {
                field.width(370.0);
                field.withoutTitle();
            });
            boxInput.getChildren().add(fieldObject.build());
        }
        //</editor-fold>
        super.root.getChildren().add(1, boxInput);

        boxInput.setOnKeyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)) {
                buttonConfirm.fire();
            } else if (evt.getCode().equals(KeyCode.ESCAPE)) {
                buttonCancel.fire();
            }
        });

        buttonConfirm.getStyleClass().add("success");
        buttonConfirm.setText("Confirmar");
        buttonConfirm.setOnAction(event -> {
            if (classType.getTypeName().contains("String")) {
                value.set((T) field.getText());
            } else if (classType.getTypeName().toLowerCase().contains("int")) {
                value.set((T) Integer.valueOf(field.getText()));
            } else if (classType.getTypeName().toLowerCase().contains("bigdecimal")) {
                value.set((T) BigDecimal.valueOf(Double.valueOf(field.getText().replace(",", "."))));
            } else if (classType.getTypeName().toLowerCase().contains("double")) {
                value.set((T) Double.valueOf(field.getText()));
            } else if (classType.getTypeName().toLowerCase().contains("bool")) {
                value.set((T) fieldToggle.value);
            } else if (classType.getTypeName().toLowerCase().contains("localdate")) {
                value.set((T) fieldDate.value.get());
            } else if (classType.getTypeName().toLowerCase().contains("localdatetime")) {

            } else if (classType.getTypeName().toLowerCase().contains("localtime")) {

            } else {
                value.set((T) fieldObject.value.get());
            }
            stage.close();
        });
        buttonCancel.getStyleClass().add("danger");
        buttonCancel.setText("Cancelar");
        buttonCancel.setOnAction(event -> stage.close());

        boxButtons.getChildren().clear();
        boxButtons.getChildren().add(buttonConfirm);
        boxButtons.getChildren().add(buttonCancel);
    }

    public static <T> InputBox build(Class<T> classType, Consumer<InputBox> value) {
        InputBox<T> box = new InputBox(classType);
        value.accept(box);
        return box;
    }

    public final void setDefaultButtonConfirm() {
        buttonConfirm.setDefaultButton(true);
    }
}
