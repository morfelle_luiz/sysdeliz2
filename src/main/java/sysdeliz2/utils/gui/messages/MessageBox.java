package sysdeliz2.utils.gui.messages;

import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import javafx.util.Duration;
import org.controlsfx.tools.Utils;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.mails.SimpleMail;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class MessageBox {
    
    public enum TypeMessageBox {
        CLEAN(new TypeMessage("", "default")),
        DEFAULT(new TypeMessage("confirm.png", "default")),
        SUCCESS(new TypeMessage("confirm.png", "success-light")),
        ACTIVE(new TypeMessage("active.png", "default")),
        INACTIVE(new TypeMessage("inactive.png", "default")),
        ALERT(new TypeMessage("alert.png", "danger-light")),
        BLOCKED(new TypeMessage("blocked.png", "default")),
        CONFIRM(new TypeMessage("confirm.png", "default")),
        FORM_VALIDATION(new TypeMessage("form validation.png", "default")),
        ERROR(new TypeMessage("error.png", "danger-light")),
        IMPORTANT(new TypeMessage("important.png", "default")),
        INPUT(new TypeMessage("input.png", "default")),
        NO_ENTRY(new TypeMessage("no entry.png", "default")),
        PRIORITY(new TypeMessage("priority.png", "info-light")),
        QUESTION(new TypeMessage("question.png", "default")),
        RISK(new TypeMessage("risk.png", "default")),
        SHIELD(new TypeMessage("shield.png", "warning-light")),
        SIREN(new TypeMessage("siren.png", "danger-light")),
        WARNING(new TypeMessage("warning.png", "warning-light"));
        
        public TypeMessage type;
        
        TypeMessageBox(TypeMessage type) {
            this.type = type;
        }
        
        public TypeMessage getValor() {
            return type;
        }
    }
    
    protected final Stage stage = new Stage();
    protected Scene scene = null;
    
    protected final VBox root;
    private final HBox boxMessage;
    protected final ImageView icon;
    protected final Label message;
    protected final HBox boxButtons;
    private final Button button;
    
    private Pos position = Pos.CENTER;
    private Duration hideAfterDuration = Duration.seconds(5);
    private Screen screen = Screen.getPrimary();
    private StageStyle stageStyle = StageStyle.DECORATED;
    private String textMail = "";
    
    public MessageBox() {
        root = new VBox();
        boxMessage = new HBox();
        icon = new ImageView();
        message = new Label();
        boxButtons = new HBox();
        button = new Button();
        
        root.setMinHeight(80.0);
        root.setMinWidth(410.0);
        root.setMaxWidth(550.0);
        root.setSpacing(10.0);
        root.getStylesheets().add("/styles/stylePadrao.css");
        root.getStylesheets().add("/styles/sysDelizDesktop.css");
        root.setPadding(new Insets(10.0));
        
        boxMessage.setAlignment(Pos.CENTER_LEFT);
        boxMessage.setSpacing(5.0);
        
        icon.setPickOnBounds(true);
        icon.setPreserveRatio(true);
        
        HBox.setHgrow(message, javafx.scene.layout.Priority.ALWAYS);
        message.setMaxHeight(Double.MAX_VALUE);
        message.setMaxWidth(Double.MAX_VALUE);
        message.setWrapText(true);
        HBox.setMargin(message, new Insets(0.0, 0.0, 0.0, 3.0));
        
        boxButtons.setAlignment(Pos.BOTTOM_RIGHT);
        boxButtons.setSpacing(5.0);
        
        boxMessage.getChildren().add(icon);
        boxMessage.getChildren().add(message);
        root.getChildren().add(boxMessage);
        root.getChildren().add(boxButtons);
        
        standard();
        type(TypeMessageBox.DEFAULT);
    }
    
    private final void positionStage(Pos position) {
        double anchorX = 0, anchorY = 0;
        
        final double boxWidth = scene.getWidth();
        final double boxHeight = scene.getHeight();
        Rectangle2D screenBounds = screen.getVisualBounds();
        double startX = screenBounds.getMinX();
        double startY = screenBounds.getMinY();
        double screenWidth = screenBounds.getWidth();
        double screenHeight = screenBounds.getHeight();
        // get anchorX
        switch(position) {
            case TOP_LEFT:
            case CENTER_LEFT:
            case BOTTOM_LEFT:
                anchorX = startX;
                break;
            
            case TOP_CENTER:
            case CENTER:
            case BOTTOM_CENTER:
                anchorX = screenBounds.getMinX() + ((screenWidth / 2.0) - (boxWidth / 2.0)) - 7.5;
                break;
            
            default:
            case TOP_RIGHT:
            case CENTER_RIGHT:
            case BOTTOM_RIGHT:
                anchorX = startX + screenWidth - boxWidth - 15;
                break;
        }
        
        // get anchorY
        switch(position) {
            case TOP_LEFT:
            case TOP_CENTER:
            case TOP_RIGHT:
                anchorY = startY;
                break;
            
            case CENTER_LEFT:
            case CENTER:
            case CENTER_RIGHT:
                anchorY = startY + ((screenHeight / 2.0) - (boxHeight / 2.0));
                break;
            
            default:
            case BOTTOM_LEFT:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                anchorY = startY + screenHeight - boxHeight - startY - 30;
                break;
        }
        
        stage.setX(anchorX);
        stage.setY(anchorY);
    }
    
    public static final MessageBox create() {
        return new MessageBox();
    }
    
    public static final MessageBox create(Consumer<MessageBox> value) {
        MessageBox box = new MessageBox();
        value.accept(box);
        return box;
    }
    
    public final MessageBox message(String message) {
        this.message.setText(message);
        return this;
    }
    
    public final MessageBox type(TypeMessageBox type) {
        if (!type.equals(TypeMessageBox.CLEAN))
            icon(type.type.icon);
        else
            boxMessage.getChildren().remove(0);
        root.getStyleClass().remove("default");
        style(type.type.styleClass);
        return this;
    }
    
    public final MessageBox standard() {
        button.setDefaultButton(true);
        button.setMnemonicParsing(false);
        button.setPrefWidth(60.0);
        button.getStyleClass().add("secundary");
        button.setText("OK");
        button.setOnAction(event -> stage.close());
        boxButtons.getChildren().add(button);
        
        return this;
    }
    
    public final MessageBox icon(String icon) {
        Image image = new Image(getClass().getResource("/images/icons/messages/" + icon).toExternalForm());
        this.icon.setImage(image);
        
        return this;
    }
    
    public final MessageBox style(String style) {
        root.getStyleClass().add(style);
        return this;
    }
    
    public final MessageBox window(StageStyle stageStyle) {
        this.stageStyle = stageStyle;
        return this;
    }
    
    public final MessageBox position(Pos position) {
        this.position = position;
        
        return this;
    }
    
    public final MessageBox hideAfter(Duration duration) {
        this.hideAfterDuration = duration;
        return this;
    }
    
    public final void sendTi(Consumer<MessageBox> value){
        value.accept(this);
        SimpleMail.INSTANCE.addDestinatario("diego@deliz.com.br")
                .comAssunto("Erro no sistema SysDeliz2")
                .comCorpo(textMail)
                .send();
    }
    
    public final void textMail(String textMail){
        this.textMail = textMail;
    }
    
    public final void notification() {
        //boxButtons.getChildren().clear();
        root.getStyleClass().add("no-corner");
        MessageBox.NotificationPopupHandler.getInstance().show(this);
    }
    
    public final void show() {
        scene = new Scene(root);
        
        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initStyle(stageStyle);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.show();
        positionStage(position);
    }
    
    public final void showAndWait() {
        scene = new Scene(root);
        
        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(stageStyle);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.showAndWait();
        positionStage(position);
    }
    
    public void showFullScreen() {
        button.getStyleClass().add("xl");
        button.setDefaultButton(false);
        button.setCancelButton(false);
        message.setAlignment(Pos.CENTER);
        message.setWrapText(true);
        message.setStyle(message.getStyle() + "-fx-font-size: 18pt; -fx-text-alignment: CENTER;");
        message.setPadding(new Insets(20));
        root.setAlignment(Pos.CENTER);
        root.getChildren().clear();
        root.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.alignment(Pos.CENTER);
            box.add(icon);
            box.add(message);
            box.add(button);
        }));
        
        scene = new Scene(root);
        
        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.showAndWait();
    }

    public void showFullScreenMobile() {
        icon.setFitHeight(30);
        icon.setFitWidth(30);
        button.setDefaultButton(false);
        button.setCancelButton(false);
        message.setAlignment(Pos.CENTER);
        message.setWrapText(true);
        message.setStyle(message.getStyle() + "-fx-font-size: 12pt; -fx-text-alignment: CENTER;");
        message.setPadding(new Insets(20));
        root.setAlignment(Pos.CENTER);
        root.getChildren().clear();
        root.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.alignment(Pos.CENTER);
            box.add(icon);
            box.add(message);
            box.add(button);
        }));

        scene = new Scene(root);

        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.showAndWait();
    }

    public Label getMessage() {
        return message;
    }
    
    private static class TypeMessage {
        String icon;
        String styleClass;
        
        public TypeMessage(String icon, String styleClass) {
            this.icon = icon;
            this.styleClass = styleClass;
        }
    }
    
    /***************************************************************************
     * * Private support classes * *
     **************************************************************************/
    
    // not public so no need for JavaDoc
    private static final class NotificationPopupHandler {
        
        private static final MessageBox.NotificationPopupHandler INSTANCE = new MessageBox.NotificationPopupHandler();
        
        private double startX;
        private double startY;
        private double screenWidth;
        private double screenHeight;
        
        static final MessageBox.NotificationPopupHandler getInstance() {
            return INSTANCE;
        }
        
        private final Map<Pos, List<Popup>> popupsMap = new HashMap<>();
        private final double padding = 15;
        
        // for animating in the notifications
        private ParallelTransition parallelTransition = new ParallelTransition();
        
        public void show(MessageBox notification) {
            Window window;
            Rectangle2D screenBounds = notification.screen.getVisualBounds();
            startX = screenBounds.getMinX();
            startY = screenBounds.getMinY();
            screenWidth = screenBounds.getWidth();
            screenHeight = screenBounds.getHeight();
            
            window = Utils.getWindow(null);
            show(window, notification);
        }
        
        private void show(Window owner, final MessageBox notification) {
            // Stylesheets which are added to the scene of a popup aren't
            // considered for styling. For this reason, we need to find the next
            // window in the hierarchy which isn't a popup.
            Window ownerWindow = owner;
            while (ownerWindow instanceof PopupWindow) {
                ownerWindow = ((PopupWindow) ownerWindow).getOwnerWindow();
            }
            // need to install our CSS
            Scene ownerScene = ownerWindow.getScene();
            
            final Popup popup = new Popup();
            popup.setAutoFix(false);
            
            final Pos p = notification.position;
            
            popup.getContent().add(notification.root);
            popup.show(owner, 0, 0);
            
            // determine location for the popup
            double anchorX = 0, anchorY = 0;
            final double barWidth = notification.root.getWidth();
            final double barHeight = notification.root.getHeight();
            
            // get anchorX
            switch(p) {
                case TOP_LEFT:
                case CENTER_LEFT:
                case BOTTOM_LEFT:
                    anchorX = padding + startX;
                    break;
                
                case TOP_CENTER:
                case CENTER:
                case BOTTOM_CENTER:
                    anchorX = startX + (screenWidth / 2.0) - barWidth / 2.0 - padding / 2.0;
                    break;
                
                default:
                case TOP_RIGHT:
                case CENTER_RIGHT:
                case BOTTOM_RIGHT:
                    anchorX = startX + screenWidth - barWidth - padding;
                    break;
            }
            
            // get anchorY
            switch(p) {
                case TOP_LEFT:
                case TOP_CENTER:
                case TOP_RIGHT:
                    anchorY = padding + startY;
                    break;
                
                case CENTER_LEFT:
                case CENTER:
                case CENTER_RIGHT:
                    anchorY = startY + (screenHeight / 2.0) - barHeight / 2.0 - padding / 2.0;
                    break;
                
                default:
                case BOTTOM_LEFT:
                case BOTTOM_CENTER:
                case BOTTOM_RIGHT:
                    anchorY = startY + screenHeight - barHeight - padding;
                    break;
            }
            
            popup.setAnchorX(anchorX);
            popup.setAnchorY(anchorY);
            
            notification.button.setOnAction(event -> {
                createHideTimeline(popup, notification.root, p, Duration.ZERO).play();
            });
            
            addPopupToMap(p, popup);
            
            // begin a timeline to get rid of the popup
            Timeline timeline = createHideTimeline(popup, notification.root, p, notification.hideAfterDuration);
            timeline.play();
        }
        
        private void hide(Popup popup, Pos p) {
            popup.hide();
            removePopupFromMap(p, popup);
        }
        
        private Timeline createHideTimeline(final Popup popup, Node bar, final Pos p, Duration startDelay) {
            KeyValue fadeOutBegin = new KeyValue(bar.opacityProperty(), 1.0);
            KeyValue fadeOutEnd = new KeyValue(bar.opacityProperty(), 0.0);
            
            KeyFrame kfBegin = new KeyFrame(Duration.ZERO, fadeOutBegin);
            KeyFrame kfEnd = new KeyFrame(Duration.millis(500), fadeOutEnd);
            
            Timeline timeline = new Timeline(kfBegin, kfEnd);
            timeline.setDelay(startDelay);
            timeline.setOnFinished(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    hide(popup, p);
                }
            });
            
            return timeline;
        }
        
        private void addPopupToMap(Pos p, Popup popup) {
            List<Popup> popups;
            if (!popupsMap.containsKey(p)) {
                popups = new LinkedList<>();
                popupsMap.put(p, popups);
            } else {
                popups = popupsMap.get(p);
            }
            
            doAnimation(p, popup);
            
            // add the popup to the list so it is kept in memory and can be
            // accessed later on
            popups.add(popup);
        }
        
        private void removePopupFromMap(Pos p, Popup popup) {
            if (popupsMap.containsKey(p)) {
                List<Popup> popups = popupsMap.get(p);
                popups.remove(popup);
            }
        }
        
        private void doAnimation(Pos p, Popup changedPopup) {
            List<Popup> popups = popupsMap.get(p);
            if (popups == null) {
                return;
            }
            
            final double newPopupHeight = changedPopup.getContent().get(0).getBoundsInParent().getHeight();
            
            parallelTransition.stop();
            parallelTransition.getChildren().clear();
            
            final boolean isShowFromTop = isShowFromTop(p);
            
            // animate all other popups in the list upwards so that the new one
            // is in the 'new' area.
            // firstly, we need to determine the target positions for all popups
            double sum = 0;
            double targetAnchors[] = new double[popups.size()];
            for (int i = popups.size() - 1; i >= 0; i--) {
                Popup _popup = popups.get(i);
                
                final double popupHeight = _popup.getContent().get(0).getBoundsInParent().getHeight();
                
                if (isShowFromTop) {
                    if (i == popups.size() - 1) {
                        sum = startY + newPopupHeight + padding;
                    } else {
                        sum += popupHeight;
                    }
                    targetAnchors[i] = sum;
                } else {
                    if (i == popups.size() - 1) {
                        sum = changedPopup.getAnchorY() - popupHeight;
                    } else {
                        sum -= popupHeight;
                    }
                    
                    targetAnchors[i] = sum;
                }
            }
            
            // then we set up animations for each popup to animate towards the
            // target
            for (int i = popups.size() - 1; i >= 0; i--) {
                final Popup _popup = popups.get(i);
                final double anchorYTarget = targetAnchors[i];
                if (anchorYTarget < 0) {
                    _popup.hide();
                }
                final double oldAnchorY = _popup.getAnchorY();
                final double distance = anchorYTarget - oldAnchorY;
                
                Transition t = new MessageBox.NotificationPopupHandler.CustomTransition(_popup, oldAnchorY, distance);
                t.setCycleCount(1);
                parallelTransition.getChildren().add(t);
            }
            parallelTransition.play();
        }
        
        private boolean isShowFromTop(Pos p) {
            switch(p) {
                case TOP_LEFT:
                case TOP_CENTER:
                case TOP_RIGHT:
                    return true;
                default:
                    return false;
            }
        }
        
        class CustomTransition extends Transition {
            
            private WeakReference<Popup> popupWeakReference;
            private double oldAnchorY;
            private double distance;
            
            CustomTransition(Popup popup, double oldAnchorY, double distance) {
                popupWeakReference = new WeakReference<>(popup);
                this.oldAnchorY = oldAnchorY;
                this.distance = distance;
                setCycleDuration(Duration.millis(350.0));
            }
            
            @Override
            protected void interpolate(double frac) {
                Popup popup = popupWeakReference.get();
                if (popup != null) {
                    double newAnchorY = oldAnchorY + distance * frac;
                    popup.setAnchorY(newAnchorY);
                }
            }
            
        }
    }
}
