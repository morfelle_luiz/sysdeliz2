package sysdeliz2.utils.gui.messages;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.components.FormFieldTextArea;
import sysdeliz2.utils.sys.ImageUtils;

import java.util.function.Consumer;
import java.util.function.Function;

public class PopOver extends StackPane {

    public enum ReturnType {CANCEL, OK, YES, NO, CONTINUE, NEXT, PREVIOUS, FINISH, PRINT, OPEN, SEND}

    public final Stage modalStage;
    public final VBox box;
    public final HBox buttonsBox;
    public final Label title;
    public ReturnType typeClose = ReturnType.CANCEL;

    private Screen screen = Screen.getPrimary();
    public HBox boxFullButtons = new HBox();
    private Button closeFragment = new Button("Cancelar", ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
    private ImageView icon;

    Function<Void, Boolean> functionOnClose = unused -> true;

    private final VBox root;

    public PopOver() {
        
        HBox boxHeader = new HBox();
        icon = ImageUtils.getIcon(ImageUtils.Icon.FRAGMENT, ImageUtils.IconSize._32);
        this.title = new Label();
        this.root = new VBox();
        this.box = new VBox();
        this.buttonsBox = new HBox();
        this.modalStage = new Stage();
    
        this.root.setPrefHeight(300.0);
        this.root.setPrefWidth(200.0);
        
        HBox.setHgrow(this.title, javafx.scene.layout.Priority.ALWAYS);
        this.title.setMaxHeight(Double.MAX_VALUE);
        this.title.setMaxWidth(Double.MAX_VALUE);
        this.title.setPrefHeight(32.0);
//        this.title.setPrefWidth(5000.0);
        this.title.setTextFill(javafx.scene.paint.Color.WHITE);
        HBox.setMargin(this.title, new Insets(0.0));
        this.title.setFont(new Font(16.0));
    
        BorderPane.setAlignment(boxHeader, Pos.CENTER_LEFT);
        boxHeader.setAlignment(Pos.CENTER_LEFT);
//        boxHeader.setPrefHeight(0.0);
//        boxHeader.setPrefWidth(711.0);
        boxHeader.setSpacing(5.0);
        boxHeader.setStyle("-fx-background-color: -principal-color");
        boxHeader.setPadding(new Insets(5.0));
        boxHeader.getChildren().add(icon);
        boxHeader.getChildren().add(this.title);
        this.root.getChildren().add(boxHeader);
        
        VBox.setVgrow(this.box, javafx.scene.layout.Priority.ALWAYS);
        this.box.setSpacing(5.0);
        this.box.setPadding(new Insets(5.0));
        this.root.getChildren().add(this.box);
    
        closeFragment.getStyleClass().addAll( "danger");
        closeFragment.setOnAction(evt -> {
            close(ReturnType.CANCEL);
        });
        
        this.buttonsBox.setAlignment(Pos.CENTER_RIGHT);
        this.buttonsBox.setSpacing(5.0);
        boxFullButtons.setAlignment(Pos.CENTER_RIGHT);
        boxFullButtons.setSpacing(5.0);
        boxFullButtons.setPadding(new Insets(5.0));
        boxFullButtons.getChildren().add(this.buttonsBox);
        boxFullButtons.getChildren().add(closeFragment);
        this.root.getChildren().add(boxFullButtons);
        
        getChildren().add(this.root);
    }
    
    public ReturnType show(Consumer<PopOver> create) {
        create.accept(this);
    
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");
        
        Scene scene;
        scene = new Scene(this);
        Image applicationIcon = new Image(PopOver.class.getResourceAsStream("/images/logo deliz (2).png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
        return typeClose;
    }

    public ReturnType showAndContinue(Consumer<PopOver> create){
        create.accept(this);
    
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");
        
        Scene scene;
        scene = new Scene(this);
        Image applicationIcon = new Image(PopOver.class.getResourceAsStream("/images/logo deliz (2).png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.WINDOW_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.show();
        return typeClose;
    }

    public ReturnType information(String text_information) {

        size(500.0,400.0);
        title("Informações");
        icon = ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._32);
        box.getChildren().add(FormFieldTextArea.create(field -> {
            field.withoutTitle();
            field.expanded();
            field.editable.set(false);
            field.value.set(text_information);
        }).build());

        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");

        Scene scene;
        scene = new Scene(this);
        Image applicationIcon = new Image(PopOver.class.getResourceAsStream("/images/logo deliz (2).png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
        return typeClose;
    }

    public final void withoutHeaderBox() {
        root.getChildren().remove(0);
    }

    public final void size(Double width, Double height){
        this.root.setPrefHeight(height);
        this.root.setPrefWidth(width);
    }
    
    public final void title(String title){
        this.title.setText(title);
    }

    public final void position(Pos position) {
        double anchorX = 0, anchorY = 0;

        final double boxWidth = root.getWidth();
        final double boxHeight = root.getHeight();
        Rectangle2D screenBounds = screen.getVisualBounds();
        double startX = screenBounds.getMinX();
        double startY = screenBounds.getMinY();
        double screenWidth = screenBounds.getWidth();
        double screenHeight = screenBounds.getHeight();
        // get anchorX
        switch(position) {
            case TOP_LEFT:
            case CENTER_LEFT:
            case BOTTOM_LEFT:
                anchorX = startX;
                break;

            case TOP_CENTER:
            case CENTER:
            case BOTTOM_CENTER:
                anchorX = screenBounds.getMinX() + ((screenWidth / 2.0) - (boxWidth / 2.0)) - 7.5;
                break;

            default:
            case TOP_RIGHT:
            case CENTER_RIGHT:
            case BOTTOM_RIGHT:
                anchorX = startX + screenWidth - boxWidth - 15;
                break;
        }

        // get anchorY
        switch(position) {
            case TOP_LEFT:
            case TOP_CENTER:
            case TOP_RIGHT:
                anchorY = startY;
                break;

            case CENTER_LEFT:
            case CENTER:
            case CENTER_RIGHT:
                anchorY = startY + ((screenHeight / 2.0) - (boxHeight / 2.0));
                break;

            default:
            case BOTTOM_LEFT:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                anchorY = startY + screenHeight - boxHeight - startY - 30;
                break;
        }

        modalStage.setX(anchorX);
        modalStage.setY(anchorY);
    }

    public final void withoutCancelButton() {
        boxFullButtons.getChildren().remove(closeFragment);
    }

    public final Button getCancelButton() {
        return closeFragment;
    }
    
    public final void close(){
        close(ReturnType.OK);
    }
    
    public final void close(ReturnType typeClose){
        if (functionOnClose.apply(null)) {
            modalStage.close();
            this.typeClose = typeClose;
        }
    }

    public final void onClose(Function<Void, Boolean> functionOnClose) {
        this.functionOnClose = functionOnClose;
    }
    
}

