package sysdeliz2.utils.gui.messages;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.utils.gui.components.FormBox;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.function.Consumer;

public class QuestionBox<T> extends MessageBox {
    
    //components section
    public final Button buttonYes;
    public final Button buttonNo;
    
    // logic section
    private final Class<T> tClass;
    
    //fx section
    public final ObjectProperty<T> value = new SimpleObjectProperty<>(null);
    
    public QuestionBox(Class classType) {
        tClass = classType;
        
        buttonYes = new Button();
        buttonNo = new Button();

        value.set((T) Boolean.FALSE);

        buttonYes.getStyleClass().add("success");
        buttonYes.setText("Sim");
        buttonYes.setPrefWidth(50.0);
        buttonYes.setDefaultButton(true);
        buttonYes.setOnAction(event -> {
            value.set((T) Boolean.TRUE);
            stage.close();
        });
        buttonNo.getStyleClass().add("danger");
        buttonNo.setText("Não");
        buttonNo.setPrefWidth(50.0);
        buttonNo.setCancelButton(true);
        buttonNo.setOnAction(event -> {
            value.set((T) Boolean.FALSE);
            stage.close();
        });
        
        type(TypeMessageBox.QUESTION);
        
        boxButtons.getChildren().clear();
        boxButtons.getChildren().add(buttonYes);
        boxButtons.getChildren().add(buttonNo);
    }
    
    public static <T> QuestionBox build(Class classType, Consumer<QuestionBox> value) {
        QuestionBox<T> box = new QuestionBox(classType);
        value.accept(box);
        return box;
    }

    public static Boolean quest(String message) {
        QuestionBox box  = build(Boolean.class, mes -> {
            mes.message("Tem certeza que deseja " + message.trim());
            mes.showAndWait();
        });
        return ((Boolean) box.value.getValue());
    }

    public void showFullScreen() {
        buttonYes.getStyleClass().add("success");
        buttonYes.setText("Sim");
        buttonYes.setDefaultButton(true);
        buttonYes.setOnAction(event -> {
            value.set((T) Boolean.TRUE);
            stage.close();
        });
        buttonNo.getStyleClass().add("danger");
        buttonNo.setText("Não");
        buttonNo.setCancelButton(true);
        buttonNo.setOnAction(event -> {
            value.set((T) Boolean.FALSE);
            stage.close();
        });

        type(TypeMessageBox.QUESTION);

        message.setAlignment(Pos.CENTER);
        message.setWrapText(true);
        message.setStyle(message.getStyle() + "-fx-font-size: 18pt; -fx-text-alignment: CENTER;");
        message.setPadding(new Insets(20));
        root.setAlignment(Pos.CENTER);
        root.getChildren().clear();
        root.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.alignment(Pos.CENTER);
            box.add(icon);
            box.add(message);
            box.add(FormBox.create(boxButtons -> {
                boxButtons.horizontal();
                boxButtons.alignment(Pos.CENTER);
                boxButtons.add(buttonYes,buttonNo);
            }));
        }));

        Scene scene = new Scene(root);

        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.showAndWait();
    }

    public void showFullScreenMobile() {
        icon.setFitHeight(30);
        icon.setFitWidth(30);
        message.setAlignment(Pos.CENTER);
        message.setWrapText(true);
        message.setStyle(message.getStyle() + "-fx-font-size: 12pt; -fx-text-alignment: CENTER;");
        message.setPadding(new Insets(20));
        root.setAlignment(Pos.CENTER);
        root.getChildren().clear();
        root.getChildren().add(FormBox.create(box -> {
            box.vertical();
            box.alignment(Pos.CENTER);
            box.add(icon);
            box.add(message);
            box.add(FormBox.create(boxButtons -> {
                boxButtons.horizontal();
                boxButtons.alignment(Pos.CENTER);
                boxButtons.add(buttonYes,buttonNo);
            }));
        }));

        scene = new Scene(root);

        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        stage.getIcons().add(applicationIcon);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("SysDeliz 2");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.setOnCloseRequest((WindowEvent arg0) -> {
            stage.close();
        });
        stage.showAndWait();
    }
    
    public final QuestionBox defaultNo(){
        buttonYes.setDefaultButton(false);
        buttonNo.setCancelButton(false);
        buttonNo.setDefaultButton(true);
        buttonYes.setCancelButton(true);
        
        buttonYes.getStyleClass().add("danger");
        buttonNo.getStyleClass().add("success");
        
        buttonNo.setOnAction(event -> {
            value.set((T) Boolean.TRUE);
            stage.close();
        });
        buttonYes.setOnAction(event -> {
            value.set((T) Boolean.FALSE);
            stage.close();
        });
        return this;
    }
    
    public final void close(){
        stage.close();
    }
    
    public final void buttons(@NotNull Button...buttons){
        boxButtons.getChildren().clear();
        Arrays.asList(buttons).forEach(it -> {
            boxButtons.getChildren().add(it);
        });
    }
}
