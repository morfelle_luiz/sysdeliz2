package sysdeliz2.utils.gui.window;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.components.FormTableView;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class CustomSelect<T> extends WindowBase {

    private final Class<T> objectParam;
    private final ListProperty<T> listToSelect = new SimpleListProperty<>(FXCollections.observableArrayList());
    public T selectedReturn = null;

    public CustomSelect() throws Exception {
        this(null, null,"");
    }
    
    @Override
    public void closeWindow() {
    
    }
    
    public CustomSelect(ObservableList<T> listToSelect) throws Exception {
        this(null, listToSelect,"");
    }

    public CustomSelect(Class classType, ObservableList<T> listToSelect, String text) throws Exception {
        super();
        
        objectParam = classType != null ? classType : (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.listToSelect.set(listToSelect);
        this.listToSelect.forEach(item -> ((BasicModel) item).setSelected(false));
        ((BasicModel)this.listToSelect.get(0)).setSelected(true);
        
        init(text);
    }
    
    public static <T> CustomSelect create(Class<T> classType, ObservableList<T> listToSelect, String text) throws Exception {
        return new CustomSelect<T>(classType, listToSelect, text);
    }
    
    private void init(String text) throws Exception {
        icon.setImage(new Image(getClass().getResourceAsStream("/images/icons/" + objectParam.getAnnotation(TelaSysDeliz.class).icon())));
        title.setText(text);
        
        super.box.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.vertical();
            boxPrincipal.expanded();
            FormTableView formTable = FormTableView.create(objectParam, table -> {
                table.title("Selecione uma opção:");
                table.expanded();
                table.items.bind(listToSelect);
                table.selectItem(0);
                table.tableProperties().setOnMouseClicked(event -> {
                    if (event.getClickCount() >= 2){
                        selectedReturn = (T) table.selectedItem();

                        Stage main = (Stage) this.getScene().getWindow();
                        main.close();
                    }
                    if(event.getClickCount() == 1) selectedReturn = (T) table.selectedItem();
                });

                table.selectionModelItem((observable, oldValue, newValue) -> {
                    if (newValue != null)
                        selectedReturn = (T) newValue;

                });
                loadColumnsTable().forEach(column -> {
                    table.addColumn(column);
                });
            });
            boxPrincipal.add(formTable.build());
            boxPrincipal.add(FormButton.create(btnUsarEste -> {
                btnUsarEste.title("Usar Este");
                btnUsarEste.icon(new Image(getClass().getResource("/images/icons/buttons/use-this (1).png").toExternalForm()));
                btnUsarEste.addStyle("success");
                btnUsarEste.setAction(event -> {
                    selectedReturn = (T) formTable.selectedItem();
                    Stage main = (Stage) this.getScene().getWindow();
                    main.close();
                });
            }));
        }));
        
        show();
    }
    
    public void show() throws Exception {
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");
        
        Stage modalStage = new Stage();
        Scene scene;
        scene = new Scene(this);
        Image applicationIcon = new Image(getClass().getResourceAsStream("/images/logo deliz (2).png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
    }
    
    private List<FormTableColumn> loadColumnsTable() {
        List<FormTableColumn> columns = new ArrayList<>();
        Field[] atributosClass = objectParam.getDeclaredFields();
        for (Field field : atributosClass) {
            ExibeTableView exibirAtributo = field.getAnnotation(ExibeTableView.class);
            if (exibirAtributo != null) {
                FormTableColumn<T, Object> coluna = FormTableColumn.create();//new TableColumn<>(exibirAtributo.descricao());
                coluna.setText(exibirAtributo.descricao());
                coluna.setPrefWidth(exibirAtributo.width());
                coluna.setCellValueFactory(new PropertyValueFactory<T, Object>(field.getName()));
                coluna.setCellFactory(column -> new TableCell<T, Object>() {
                    @Override
                    protected void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            switch(exibirAtributo.columnType()) {
                                case BOOLEAN:
                                    boolean x = item == null || item.equals("") || item.equals("X");
                                    if (x) {
                                        setText(item.equals("X") ? "SIM" : "NÃO");
                                    } else {
                                        setText((Boolean) item ? "SIM" : "NÃO");
                                    }
                                    break;
                                case DECIMAL:
                                    setText(StringUtils.toDecimalFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case MONETARY:
                                    setText(StringUtils.toMonetaryFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case PERCENT:
                                    setText(StringUtils.toPercentualFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case NUMBER:
                                    setText(StringUtils.toIntegerFormat(((BigDecimal) item).intValue()));
                                    break;
                                case TIME:
                                    setText(StringUtils.toTimeFormat((LocalTime) item));
                                    break;
                                case DATE:
                                    setText(StringUtils.toDateFormat((LocalDate) item));
                                    break;
                                case DATETIME:
                                    setText(StringUtils.toDateTimeFormat((LocalDateTime) item));
                                    break;
                                case TEXT:
                                    setText(item.toString());
                                    break;
                            }
                        }
                    }
                });
                columns.add(coluna);
            }
        }
        return columns;
    }

}
