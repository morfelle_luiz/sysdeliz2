package sysdeliz2.utils.gui.window;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.controllers.views.procura.helpers.BasicFilter;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ResultTypeFilter;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.TextFieldUtils;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class GenericFilter<T> extends WindowBase {

    // <editor-fold defaultstate="collapsed" desc="Declaração dos componentes">
    private final BorderPane borderPane = new BorderPane();
    private final HBox hBox = new HBox();
    private final VBox vBox = new VBox();
    private final HBox hBox0 = new HBox();
    private final TitledPane titledPane = new TitledPane();
    private final ImageView imageView = new ImageView();
    private final HBox hBox1 = new HBox();
    private final VBox vBox0 = new VBox();
    private final HBox hBox2 = new HBox();
    private final VBox vBox1 = new VBox();
    private final Label label = new Label();
    private final ComboBox<BasicFilter> cboxColunaFiltro = new ComboBox<>();
    private final VBox vBox2 = new VBox();
    private final Label label0 = new Label();
    private final ComboBox cboxRegraFiltro = new ComboBox();
    private final HBox hBox3 = new HBox();
    private final VBox vBox3 = new VBox();
    private final Label label1 = new Label();
    private final HBox hBox4 = new HBox();
    private final TextField tboxValorFiltro = new TextField();
    private final Button btnFindFilter = new Button();
    private final ImageView imageView0 = new ImageView();
    private final HBox hBox5 = new HBox();
    private final Button btnWhereAnd = new Button();
    private final Button btnWhereOr = new Button();
    private final VBox vBox4 = new VBox();
    private final ListView listWheres = new ListView();
    private final Button btnLoad = new Button();
    private final ImageView imageView1 = new ImageView();
    private final TableView<T> tblFilters = new TableView();
    private final TableColumn<T, Boolean> clmSelected = new TableColumn();
    private final CheckBox chboxCheckAll = new CheckBox();
    private final HBox hBox6 = new HBox();
    private final HBox hBox7 = new HBox();
    private final VBox vBox5 = new VBox();
    private final ImageView imageView2 = new ImageView();
    private final Button btnUseThis = new Button();
    private final ImageView imageView3 = new ImageView();
//</editor-fold>

    private final Class<T> objectParam;
    private GenericDao<T> daoRegistros;
    private final ListProperty<BasicFilter> colunasConsulta = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<T> filterRegisters = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Map<String, Object>> wheresList = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ResultTypeFilter typeResultReturn = ResultTypeFilter.SINGLE_RESULT;
    private ObservableList<DefaultFilter> filtrosPadroes = FXCollections.observableArrayList();

    public T selectedReturn = null;
    public ObservableList<T> selectedsReturn = FXCollections.observableArrayList();

    public GenericFilter() throws SQLException {
        this(null, null);
    }

    @Override
    public void closeWindow() {

    }

    public GenericFilter(Class classType, List<DefaultFilter> defautls) throws SQLException {
        super();

        objectParam = classType != null ? classType : (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        daoRegistros = new GenericDaoImpl<>(objectParam);

        icon.setImage(new Image(getClass().getResourceAsStream("/images/icons/" + objectParam.getAnnotation(TelaSysDeliz.class).icon())));
        title.setText("Procurar " + objectParam.getAnnotation(TelaSysDeliz.class).descricao());
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/stylePadrao.css");
        getStylesheets().add(Globals.getEmpresaLogada().getCodigo().equals("1000") ? "/styles/sysDelizDesktop.css" : "/styles/upWaveDesktop.css");

        makeWindow();
        loadColunasTabela(objectParam);
        loadColunasFilter(objectParam);
        loadDefaultFilters(objectParam);

        cboxColunaFiltro.getSelectionModel().selectFirst();

        if (filtrosPadroes.size() > 0 || (defautls != null && defautls.size() > 0)) {
            daoRegistros = daoRegistros.initCriteria();
            for (DefaultFilter filtro : filtrosPadroes) {
                daoRegistros = daoRegistros.addPredicate(filtro.coluna, filtro.valor, filtro.predicate);
            }
            for (DefaultFilter entrada : defautls) {
                if (entrada.valor instanceof Object[])
                    daoRegistros = daoRegistros.addPredicate(entrada.coluna, (Object[]) entrada.valor, entrada.predicate);
                else
                    daoRegistros = daoRegistros.addPredicate(entrada.coluna, entrada.valor, entrada.predicate);
            }
            filterRegisters.set(daoRegistros.loadListByPredicate(20));
        } else
            filterRegisters.set(daoRegistros.list(20));
    }

    public void show(ResultTypeFilter typeResult) {
        wheresList.clear();
        typeResultReturn = typeResult;

        if (typeResultReturn == ResultTypeFilter.SINGLE_RESULT) {
            this.filterRegisters.forEach(t -> {
                ((BasicModel) t).setSelected(false);
            });
            this.clmSelected.setVisible(false);
        }

        Stage modalStage = new Stage();
        Scene scene;
        scene = new Scene(this);
        Image applicationIcon = Globals.getEmpresaLogada().getIconeEmpresa();
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
    }

    private void makeWindow() {
        box.getStyleClass().add("generic-filter");

        borderPane.setPrefHeight(720.0);
        borderPane.setPrefWidth(700.0);

        BorderPane.setAlignment(hBox, javafx.geometry.Pos.CENTER);
        hBox.setPrefHeight(101.0);
        hBox.setPrefWidth(699.0);

        HBox.setHgrow(vBox, javafx.scene.layout.Priority.ALWAYS);
        vBox.setPrefHeight(113.0);
        vBox.setPrefWidth(699.0);
        vBox.setSpacing(5.0);

        hBox0.setPrefHeight(225.0);
        hBox0.setPrefWidth(689.0);

        titledPane.setAnimated(false);
        titledPane.setCollapsible(false);
        titledPane.setPrefHeight(210.0);
        titledPane.setPrefWidth(880.0);
        titledPane.setText("Filtros");

        imageView.setFitHeight(150.0);
        imageView.setFitWidth(16.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        imageView.setImage(new Image(getClass().getResource("/images/icons/buttons/filter (1).png").toExternalForm()));
        titledPane.setGraphic(imageView);
        titledPane.setFont(new Font(11.0));

        hBox1.setPrefHeight(42.0);
        hBox1.setPrefWidth(687.0);
        hBox1.setSpacing(5.0);

        HBox.setHgrow(vBox0, javafx.scene.layout.Priority.ALWAYS);
        vBox0.setPrefHeight(128.0);
        vBox0.setPrefWidth(264.0);
        vBox0.setSpacing(5.0);

        hBox2.setPrefHeight(10.0);
        hBox2.setPrefWidth(482.0);
        hBox2.setSpacing(5.0);

        vBox1.setPrefHeight(34.0);
        vBox1.setPrefWidth(186.0);

        label.getStyleClass().add("form-field");
        label.setText("Por");

        cboxColunaFiltro.setPrefWidth(1292.0);
        cboxColunaFiltro.getStyleClass().add("sm");
        cboxColunaFiltro.getStyleClass().add("form-field");
        cboxColunaFiltro.itemsProperty().bind(colunasConsulta);
        cboxColunaFiltro.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            btnFindFilter.setDisable(true);
            if (newValue != null && !newValue.getFilterClass().isEmpty()) {
                btnFindFilter.setDisable(false);
            }
        });

        vBox2.setLayoutX(10.0);
        vBox2.setLayoutY(10.0);
        vBox2.setPrefHeight(63.0);
        vBox2.setPrefWidth(125.0);

        label0.getStyleClass().add("form-field");
        label0.setText("Regra");

        cboxRegraFiltro.setPrefWidth(1292.0);
        cboxRegraFiltro.getStyleClass().add("sm");
        cboxRegraFiltro.getStyleClass().add("form-field");
        cboxRegraFiltro.setItems(FXCollections.observableArrayList("Contém", "Igual", "Diferente", "Contido em", "Não contido em", "Não é nullo", "É nullo"));
        cboxRegraFiltro.getSelectionModel().selectFirst();

        hBox3.setPrefHeight(100.0);
        hBox3.setPrefWidth(200.0);
        hBox3.setSpacing(5.0);

        HBox.setHgrow(vBox3, javafx.scene.layout.Priority.ALWAYS);
        vBox3.setPrefHeight(1.0);
        vBox3.setPrefWidth(366.0);

        label1.getStyleClass().add("form-field");
        label1.setText("Valor");
        label1.setFont(new Font(11.0));

        hBox4.setPrefHeight(14.0);
        hBox4.setPrefWidth(366.0);

        tboxValorFiltro.setMinHeight(18.0);
        tboxValorFiltro.setPrefHeight(21.0);
        tboxValorFiltro.setPrefWidth(432.0);
        tboxValorFiltro.setFont(new Font(11.0));
        tboxValorFiltro.getStyleClass().add("sm");
        tboxValorFiltro.getStyleClass().add("form-field");
        tboxValorFiltro.getStyleClass().add("first-down");
        tboxValorFiltro.setOnKeyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER))
                btnLoad.fire();
        });
        TextFieldUtils.upperCase(tboxValorFiltro);

        btnFindFilter.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnFindFilter.setMnemonicParsing(false);
        btnFindFilter.setOnAction(this::btnFindFilterOnAction);
        btnFindFilter.setPrefWidth(30.0);
        btnFindFilter.setText("Consultar Filtro");
        btnFindFilter.getStyleClass().add("sm");
        btnFindFilter.getStyleClass().add("warning");
        btnFindFilter.getStyleClass().add("last");

        imageView0.setFitHeight(16.0);
        imageView0.setFitWidth(16.0);
        imageView0.setPickOnBounds(true);
        imageView0.setPreserveRatio(true);
        imageView0.setImage(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm()));
        btnFindFilter.setGraphic(imageView0);

        hBox5.setAlignment(javafx.geometry.Pos.TOP_RIGHT);
        hBox5.setPrefHeight(0.0);
        hBox5.setPrefWidth(366.0);
        hBox5.setSpacing(10.0);

        btnWhereAnd.setMnemonicParsing(false);
        btnWhereAnd.setOnAction(this::btnWhereAndOnAction);
        btnWhereAnd.setPrefHeight(22.0);
        btnWhereAnd.setPrefWidth(50.0);
        btnWhereAnd.setText("E");
        btnWhereAnd.getStyleClass().add("xs");
        btnWhereAnd.getStyleClass().add("info");

        btnWhereOr.setMnemonicParsing(false);
        btnWhereOr.setOnAction(this::btnWhereOrOnAction);
        btnWhereOr.setPrefHeight(22.0);
        btnWhereOr.setPrefWidth(50.0);
        btnWhereOr.setText("OU");
        btnWhereOr.getStyleClass().add("xs");
        btnWhereOr.getStyleClass().add("info");

        vBox4.setAlignment(javafx.geometry.Pos.BOTTOM_RIGHT);
        vBox4.setPrefHeight(128.0);
        vBox4.setPrefWidth(383.0);
        vBox4.setSpacing(5.0);

        listWheres.setMaxHeight(145.0);
        listWheres.setOnMouseClicked(this::listWheresOnMouseClicked);
        listWheres.setPrefHeight(145.0);
        listWheres.setPrefWidth(303.0);

        btnLoad.setMnemonicParsing(false);
        btnLoad.setOnAction(this::btnLoadOnAction);
        btnLoad.setText("Consultar");
        btnLoad.getStyleClass().add("sm");
        btnLoad.getStyleClass().add("default");

        imageView1.setFitHeight(16.0);
        imageView1.setFitWidth(16.0);
        imageView1.setPickOnBounds(true);
        imageView1.setPreserveRatio(true);
        imageView1.setImage(new Image(getClass().getResource("/images/icons/buttons/select (1).png").toExternalForm()));
        btnLoad.setGraphic(imageView1);
        titledPane.setContent(hBox1);
        VBox.setMargin(hBox0, new Insets(0.0));
        BorderPane.setMargin(hBox, new Insets(0.0, 0.0, 5.0, 0.0));
        borderPane.setTop(hBox);

        BorderPane.setAlignment(tblFilters, javafx.geometry.Pos.CENTER);
        tblFilters.setEditable(true);
        tblFilters.setOnMouseClicked(this::tblFiltersOnMouseClicked);
        tblFilters.setPrefHeight(200.0);
        tblFilters.setPrefWidth(200.0);
        tblFilters.setTableMenuButtonVisible(true);
        tblFilters.itemsProperty().bind(filterRegisters);
        tblFilters.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && typeResultReturn == ResultTypeFilter.SINGLE_RESULT) {
                ((BasicModel) oldValue).setSelected(false);
            }
            if (newValue != null) {
                ((BasicModel) newValue).setSelected(true);
            }
        });

        Tooltip ttCheckAll = new Tooltip("Selecionar todos");
        chboxCheckAll.setTooltip(ttCheckAll);
        chboxCheckAll.selectedProperty().addListener((observable, oldValue, newValue) -> {
            tblFilters.getItems().forEach(item -> ((BasicModel) item).setSelected(newValue));
        });

        clmSelected.setPrefWidth(38.0);
        clmSelected.setText("");
        clmSelected.setStyle("-fx-alignment: CENTER");
        BorderPane.setMargin(tblFilters, new Insets(0.0));
        borderPane.setCenter(tblFilters);
        clmSelected.setCellFactory((col) -> new CheckBoxTableCell<>());
        clmSelected.setGraphic(chboxCheckAll);
        clmSelected.setCellValueFactory((cellData) -> {
            T cellValue = cellData.getValue();
            BooleanProperty property = ((BasicModel) cellValue).selectedProperty();
            property.addListener((observable, oldValue, newValue) -> ((BasicModel) cellValue).setSelected(newValue));
            return property;
        });

        BorderPane.setAlignment(hBox6, javafx.geometry.Pos.CENTER);
        hBox6.setAlignment(javafx.geometry.Pos.BOTTOM_RIGHT);
        hBox6.setPrefHeight(38.0);
        hBox6.setPrefWidth(689.0);

        HBox.setHgrow(hBox7, javafx.scene.layout.Priority.ALWAYS);
        hBox7.setPrefHeight(100.0);
        hBox7.setPrefWidth(200.0);

        HBox.setHgrow(vBox5, javafx.scene.layout.Priority.ALWAYS);
        vBox5.setPrefHeight(200.0);
        vBox5.setPrefWidth(100.0);

        imageView2.setFitHeight(16.0);
        imageView2.setFitWidth(16.0);
        imageView2.setPickOnBounds(true);
        imageView2.setPreserveRatio(true);
        imageView2.setImage(new Image(getClass().getResource("/images/icons/buttons/check-all (1).png").toExternalForm()));
        chboxCheckAll.setGraphic(imageView2);
        chboxCheckAll.setPadding(new Insets(-2.0));

        btnUseThis.setMnemonicParsing(false);
        btnUseThis.setOnAction(this::btnUseThisOnAction);
        btnUseThis.setText("Usar este(s)");
        btnUseThis.getStyleClass().add("sm");
        btnUseThis.getStyleClass().add("success");

        imageView3.setFitHeight(16.0);
        imageView3.setFitWidth(16.0);
        imageView3.setPickOnBounds(true);
        imageView3.setPreserveRatio(true);
        imageView3.setImage(new Image(getClass().getResource("/images/icons/buttons/use-this (1).png").toExternalForm()));
        btnUseThis.setGraphic(imageView3);
        BorderPane.setMargin(hBox6, new Insets(0.0));
        borderPane.setBottom(hBox6);

        vBox1.getChildren().add(label);
        vBox1.getChildren().add(cboxColunaFiltro);
        hBox2.getChildren().add(vBox1);
        vBox2.getChildren().add(label0);
        vBox2.getChildren().add(cboxRegraFiltro);
        hBox2.getChildren().add(vBox2);
        vBox0.getChildren().add(hBox2);
        vBox3.getChildren().add(label1);
        hBox4.getChildren().add(tboxValorFiltro);
        hBox4.getChildren().add(btnFindFilter);
        vBox3.getChildren().add(hBox4);
        hBox3.getChildren().add(vBox3);
        vBox0.getChildren().add(hBox3);
        hBox5.getChildren().add(btnWhereAnd);
        hBox5.getChildren().add(btnWhereOr);
        vBox0.getChildren().add(hBox5);
        hBox1.getChildren().add(vBox0);
        vBox4.getChildren().add(listWheres);
        vBox4.getChildren().add(btnLoad);
        hBox1.getChildren().add(vBox4);
        hBox0.getChildren().add(titledPane);
        vBox.getChildren().add(hBox0);
        hBox.getChildren().add(vBox);
        tblFilters.getColumns().add(clmSelected);
        hBox7.getChildren().add(vBox5);
        hBox6.getChildren().add(hBox7);
        hBox6.getChildren().add(btnUseThis);

        box.getChildren().add(borderPane);
        Platform.runLater(tboxValorFiltro::requestFocus);
    }

    private void loadColunasTabela(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            ExibeTableView exibirAtributo = field.getAnnotation(ExibeTableView.class);
            if (exibirAtributo != null) {
                TableColumn<T, Object> coluna = new TableColumn<>(exibirAtributo.descricao());
                coluna.setPrefWidth(exibirAtributo.width());
                coluna.setCellValueFactory(new PropertyValueFactory<T, Object>(field.getName()));
                coluna.setCellFactory(column -> new TableCell<T, Object>() {
                    @Override
                    protected void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            switch (exibirAtributo.columnType()) {
                                case BOOLEAN:
                                    boolean x = item == null || item.equals("") || item.equals("X");
                                    if (x) {
                                        setText(item.equals("X") ? "SIM" : "NÃO");
                                    } else {
                                        setText((Boolean) item ? "SIM" : "NÃO");
                                    }
                                    break;
                                case DECIMAL:
                                    setText(StringUtils.toDecimalFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case MONETARY:
                                    setText(StringUtils.toMonetaryFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case PERCENT:
                                    setText(StringUtils.toPercentualFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                    break;
                                case NUMBER:
                                    setText(StringUtils.toIntegerFormat(((BigDecimal) item).intValue()));
                                    break;
                                case TIME:
                                    setText(StringUtils.toTimeFormat((LocalTime) item));
                                    break;
                                case DATE:
                                    setText(StringUtils.toDateFormat((LocalDate) item));
                                    break;
                                case DATETIME:
                                    setText(StringUtils.toDateTimeFormat((LocalDateTime) item));
                                    break;
                                case TEXT:
                                    setText(item.toString());
                                    break;
                            }
                        }
                    }
                });
                tblFilters.getColumns().add(coluna);
            }
        }
    }

    private void loadDefaultFilters(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            sysdeliz2.utils.sys.annotations.DefaultFilter exibirAtributo = field.getAnnotation(sysdeliz2.utils.sys.annotations.DefaultFilter.class);
            if (exibirAtributo != null) {
                filtrosPadroes.add(new DefaultFilter(exibirAtributo.value(), exibirAtributo.column(), exibirAtributo.typePredicate()));
            }
        }
    }

    private void loadColunasFilter(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        Arrays.stream(atributosClass)
                .filter(it -> it.isAnnotationPresent(ColunaFilter.class))
                .map(it -> it.getAnnotation(ColunaFilter.class))
                .sorted(Comparator.comparing(ColunaFilter::defaultFilter).reversed())
                .forEachOrdered(eb -> colunasConsulta.add(new BasicFilter(eb.coluna(), eb.descricao(), eb.filterClass())));
//        for (Field field : atributosClass) {
//            ColunaFilter colunaFilter = field.getAnnotation(ColunaFilter.class);
//            if (colunaFilter != null) {
//                BasicFilter basicFilter = new BasicFilter(colunaFilter.coluna(), colunaFilter.descricao(), colunaFilter.filterClass());
//                colunasConsulta.add(basicFilter);
//            }
//        }
    }

    private void btnFindFilterOnAction(javafx.event.ActionEvent actionEvent) {
        try {
            tboxValorFiltro.clear();
            Class classSubFilter = Class.forName(cboxColunaFiltro.getSelectionModel().getSelectedItem().getFilterClass());

            System.out.println(classSubFilter);
            GenericFilter<?> subFilter = new GenericFilter(classSubFilter, null) {
            };
            subFilter.show(Boolean.logicalOr(cboxRegraFiltro.getSelectionModel().getSelectedIndex() == 3, cboxRegraFiltro.getSelectionModel().getSelectedIndex() == 4)
                    ? ResultTypeFilter.MULTIPLE_RESULT
                    : ResultTypeFilter.SINGLE_RESULT);

            String returnSubFilter = Boolean.logicalOr(cboxRegraFiltro.getSelectionModel().getSelectedIndex() == 3, cboxRegraFiltro.getSelectionModel().getSelectedIndex() == 4)
                    ? subFilter.selectedsReturn.stream().map(item -> ((BasicModel) item).getCodigoFilter()).collect(Collectors.joining(","))
                    : ((BasicModel) subFilter.selectedReturn).getCodigoFilter();
            tboxValorFiltro.setText(returnSubFilter);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
            GUIUtils.showException(e);
        }
    }

    private void addClausulaWhere(boolean isOr) {
        String field = cboxColunaFiltro.getSelectionModel().getSelectedItem().getCampo();
        String descricao = cboxColunaFiltro.getSelectionModel().getSelectedItem().getDescricao();
        Map<String, Object> whereAdded = new HashMap<>();
        switch (cboxRegraFiltro.getSelectionModel().getSelectedIndex()) {
            case 0: // Contem
                // Verifica se o conteudo é separado por virgula
                whereAdded.put("field", field);
                whereAdded.put("relation", 2);
                whereAdded.put("value", tboxValorFiltro.getText().trim());
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " contém " + tboxValorFiltro.getText().trim());
                break;
            case 2: // Diferente
                whereAdded.put("field", field);
                whereAdded.put("relation", 1);
                whereAdded.put("value", tboxValorFiltro.getText().trim());
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " diferente " + tboxValorFiltro.getText().trim());
                break;
            case 1: // Igual
                whereAdded.put("field", field);
                whereAdded.put("relation", 0);
                whereAdded.put("value", tboxValorFiltro.getText().trim());
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " igual " + tboxValorFiltro.getText().trim());
                break;
            case 3: // Contido
                whereAdded.put("field", field);
                whereAdded.put("relation", 3);
                whereAdded.put("value", tboxValorFiltro.getText().trim());
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " contido em [" + tboxValorFiltro.getText().trim() + "]");
                break;
            case 4: // Não contido Em
                whereAdded.put("field", field);
                whereAdded.put("relation", 4);
                whereAdded.put("value", tboxValorFiltro.getText().trim());
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " não contido em [" + tboxValorFiltro.getText().trim() + "]");
                break;
            case 5: // É Nullo
                whereAdded.put("field", field);
                whereAdded.put("relation", 5);
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " é nullo");
                break;
            case 6: // Não Nullo
                whereAdded.put("field", field);
                whereAdded.put("relation", 6);
                whereAdded.put("orClause", isOr);
                wheresList.add(0, whereAdded);
                listWheres.getItems().add(0, (isOr ? "ou " : "e ") + descricao + " é não nullo");
                break;
        }
    }

    private void btnWhereAndOnAction(javafx.event.ActionEvent actionEvent) {
        // Não insere o filtro caso o conteudo não seja informado
        if (tboxValorFiltro.getText().isEmpty() && cboxRegraFiltro.getSelectionModel().getSelectedIndex() < 5) {
            return;
        }

        addClausulaWhere(false);

        tboxValorFiltro.clear();
        cboxColunaFiltro.requestFocus();
    }

    private void btnWhereOrOnAction(javafx.event.ActionEvent actionEvent) {
        // Não insere o filtro caso o conteudo não seja informado
        if (tboxValorFiltro.getText().isEmpty() && cboxRegraFiltro.getSelectionModel().getSelectedIndex() < 5) {
            return;
        }

        addClausulaWhere(true);

        tboxValorFiltro.clear();
        cboxColunaFiltro.requestFocus();
    }

    private void listWheresOnMouseClicked(javafx.scene.input.MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() > 1) {
            int indexClauseDelete = listWheres.getSelectionModel().getSelectedIndex();
            wheresList.remove(indexClauseDelete);
            listWheres.getItems().remove(indexClauseDelete);
        }
    }

    private void btnLoadOnAction(javafx.event.ActionEvent actionEvent) {
        AtomicReference<Exception> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                wheresList.forEach(whereClause -> {
                    switch ((int) whereClause.get("relation")) {
                        case 0: // Contem
                            // Verifica se o conteudo é separado por virgula
                            daoRegistros.addPredicateLike((String) whereClause.get("field"), (String) whereClause.get("value"), (Boolean) whereClause.get("orClause"));
                            break;
                        case 2: // Diferente
                            daoRegistros.addPredicateNe((String) whereClause.get("field"), (String) whereClause.get("value"), (Boolean) whereClause.get("orClause"));
                            break;
                        case 1: // Igual
                            if (((String) whereClause.get("field")).split("\\.").length > 1) {
                                daoRegistros.addPredicateEqPkEmbedded(((String) whereClause.get("field")).split("\\.")[0], ((String) whereClause.get("field")).split("\\.")[1], (String) whereClause.get("value"), (Boolean) whereClause.get("orClause"));
                            } else {
                                daoRegistros.addPredicateEq((String) whereClause.get("field"), (String) whereClause.get("value"), (Boolean) whereClause.get("orClause"));
                            }
                            break;
                        case 3: // Contido
                            if (((String) whereClause.get("field")).split("\\.").length > 1) {
                                daoRegistros.addPredicateInPkEmbedded(((String) whereClause.get("field")).split("\\.")[0], ((String) whereClause.get("field")).split("\\.")[1], ((String) whereClause.get("value")).split(","), (Boolean) whereClause.get("orClause"));
                            } else {
                                daoRegistros.addPredicateIn((String) whereClause.get("field"), ((String) whereClause.get("value")).split(","), (Boolean) whereClause.get("orClause"));
                            }
                            break;
                        case 4: // Não contido Em
                            if (((String) whereClause.get("field")).split("\\.").length > 1) {
                                daoRegistros.addPredicateNInPkEmbedded(((String) whereClause.get("field")).split("\\.")[0], ((String) whereClause.get("field")).split("\\.")[1], ((String) whereClause.get("value")).replaceAll(" ", "").split(","), (Boolean) whereClause.get("orClause"));
                            } else {
                                daoRegistros.addPredicateNIn((String) whereClause.get("field"), ((String) whereClause.get("value")).replaceAll(" ", "").split(","), (Boolean) whereClause.get("orClause"));
                            }
                            break;
                        case 5: // É Nullo
                            daoRegistros.addPredicateIsNull((String) whereClause.get("field"), (Boolean) whereClause.get("orClause"));
                            break;
                        case 6: // Não Nullo
                            daoRegistros.addPredicateIsNotNull((String) whereClause.get("field"), (Boolean) whereClause.get("orClause"));
                            break;
                    }
                });

                String field = cboxColunaFiltro.getSelectionModel().getSelectedItem().getCampo();
                switch (cboxRegraFiltro.getSelectionModel().getSelectedIndex()) {
                    case 0: // Contem
                        // Verifica se o conteudo é separado por virgula
                        daoRegistros.addPredicateLike(field, tboxValorFiltro.getText(), false);
                        break;
                    case 2: // Diferente
                        daoRegistros.addPredicateNe(field, tboxValorFiltro.getText(), false);
                        break;
                    case 1: // Igual
                        if (field.split("\\.").length > 1) {
                            daoRegistros.addPredicateEqPkEmbedded(field.split("\\.")[0], field.split("\\.")[1], tboxValorFiltro.getText(), false);
                        } else {
                            daoRegistros.addPredicateEq(field, tboxValorFiltro.getText(), false);
                        }
                        break;
                    case 3: // Contido
                        if (field.split("\\.").length > 1) {
                            daoRegistros.addPredicateInPkEmbedded(field.split("\\.")[0], field.split("\\.")[1], tboxValorFiltro.getText().split(","), false);
                        } else {
                            daoRegistros.addPredicateIn(field, tboxValorFiltro.getText().split(","), false);
                        }
                        break;
                    case 4: // Não contido Em
                        if (field.split("\\.").length > 1) {
                            daoRegistros.addPredicateNInPkEmbedded(field.split("\\.")[0], field.split("\\.")[1], tboxValorFiltro.getText().replaceAll(" ", "").split(","), false);
                        } else {
                            daoRegistros.addPredicateNIn(field, tboxValorFiltro.getText().replaceAll(" ", "").split(","), false);
                        }
                        break;
                    case 5: // É Nullo
                        daoRegistros.addPredicateIsNull(field, false);
                        break;
                    case 6: // Não Nullo
                        daoRegistros.addPredicateIsNotNull(field, false);
                        break;
                }

                for (DefaultFilter filtro : filtrosPadroes) {
                    daoRegistros.addPredicate(filtro.coluna, filtro.valor, filtro.predicate);
                }

                filterRegisters.setAll(this.daoRegistros.loadListByPredicate());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                exc.set(e);
                return ReturnAsync.ERROR.value;
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                this.daoRegistros.clearPredicate();
                this.wheresList.clear();
                this.listWheres.getItems().clear();
                this.tboxValorFiltro.clear();
            } else if (taskReturn.equals(ReturnAsync.ERROR.value)) {
                MessageBox.create(message -> {
                    message.message("Valor Informado não Corresponde ao Tipo de Dado.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.show();
                    message.position(Pos.CENTER);
                });
            }
        });
    }

    private void tblFiltersOnMouseClicked(javafx.scene.input.MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() > 1) {
            this.btnUseThisOnAction(null);
        }
    }

    private void btnUseThisOnAction(javafx.event.ActionEvent actionEvent) {

        selectedsReturn.setAll(filterRegisters.stream().filter(item -> ((BasicModel) item).isSelected()).collect(Collectors.toList()));
        selectedReturn = selectedsReturn.size() > 0 ? selectedsReturn.get(0) : null;

        Stage main = (Stage) btnUseThis.getScene().getWindow();
        main.close();
    }

}