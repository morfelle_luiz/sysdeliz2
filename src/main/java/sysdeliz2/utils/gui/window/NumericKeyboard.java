package sysdeliz2.utils.gui.window;

import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sysdeliz2.utils.gui.MaskTextField;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.StringUtils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class NumericKeyboard extends GridPane {
    
    public enum KeyboardValue {
        TIME,
        INTEGER,
        DECIMAL
    }
    
    private static KeyboardValue tipoValor = null;
    
    public static final ObjectProperty<LocalTime> timeKeyboard = new SimpleObjectProperty<>();
    public static final DoubleProperty doubleKeyboard = new SimpleDoubleProperty();
    public static final IntegerProperty integerKeyboard = new SimpleIntegerProperty();
    
    // <editor-fold defaultstate="collapsed" desc="GUI Components">
    private final ColumnConstraints constraintsCol1;
    private final ColumnConstraints constraintsCol2;
    private final ColumnConstraints constraintsCol3;
    private final RowConstraints constraintsRow1;
    private final RowConstraints constraintsRow2;
    private final RowConstraints constraintsRow3;
    private final RowConstraints constraintsRow4;
    private final RowConstraints constraintsRow5;
    
    private static final TextField tfieldValue = new TextField();
    private static final Button btnNum_1 = FormButton.create(btn -> {
        btn.title("1");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("1"));
        });
    });
    private static final Button btnNum_2 = FormButton.create(btn -> {
        btn.title("2");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("2"));
        });
    });
    private static final Button btnNum_3 = FormButton.create(btn -> {
        btn.title("3");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("3"));
        });
    });
    private static final Button btnNum_4 = FormButton.create(btn -> {
        btn.title("4");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("4"));
        });
    });
    private static final Button btnNum_5 = FormButton.create(btn -> {
        btn.title("5");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("5"));
        });
    });
    private static final Button btnNum_6 = FormButton.create(btn -> {
        btn.title("6");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("6"));
        });
    });
    private static final Button btnNum_7 = FormButton.create(btn -> {
        btn.title("7");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("7"));
        });
    });
    private static final Button btnNum_8 = FormButton.create(btn -> {
        btn.title("8");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("8"));
        });
    });
    private static final Button btnNum_9 = FormButton.create(btn -> {
        btn.title("9");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("9"));
        });
    });
    private static final Button btnVirgula = FormButton.create(btn -> {
        btn.title(",");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat(","));
        });
    });
    private static final Button btnNum_0 = FormButton.create(btn -> {
        btn.title("0");
        btn.setAction(evt -> {
            tfieldValue.setText(tfieldValue.getText().concat("0"));
        });
    });
    private static final Button btnOk = FormButton.create(btn -> {
        btn.title("OK");
        btn.addStyle("success");
        btn.setAction(evt -> {
            try {
                if (tipoValor == KeyboardValue.TIME) {
                    timeKeyboard.set(LocalTime.parse(tfieldValue.getText(), DateTimeFormatter.ofPattern("HH:mm")));
                } else if (tipoValor == KeyboardValue.DECIMAL) {
                    doubleKeyboard.set(Double.parseDouble(tfieldValue.getText().replace(",", ".")));
                } else {
                    integerKeyboard.set(Integer.parseInt(tfieldValue.getText()));
                }
        
                Stage main = (Stage) tfieldValue.getScene().getWindow();
                main.close();
            } catch (NumberFormatException | DateTimeParseException ex) {
                MessageBox.create(message -> {
                    message.message("O valor digitado não está no padrão do campo desejado, verifique o valor digitado!");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
        });
    });
    private static final Button btnBksp = FormButton.create(btn -> {
        btn.title("BKSP");
        btn.addStyle("warning");
        btn.setAction(evt -> {
            if(tfieldValue.getLength() > 0) {
                tfieldValue.setText(tfieldValue.getText().substring(0, tfieldValue.getText().length() - 1));
            }
        });
    });
    // </editor-fold>
    
    public NumericKeyboard() {
        
        constraintsCol1 = new ColumnConstraints();
        constraintsCol2 = new ColumnConstraints();
        constraintsCol3 = new ColumnConstraints();
        constraintsRow1 = new RowConstraints();
        constraintsRow2 = new RowConstraints();
        constraintsRow3 = new RowConstraints();
        constraintsRow4 = new RowConstraints();
        constraintsRow5 = new RowConstraints();
    
        getStylesheets().add("/styles/bootstrap2.css");
        getStylesheets().add("/styles/bootstrap3.css");
        getStylesheets().add("/styles/styles.css");
        getStylesheets().add("/styles/sysDelizDesktop.css");
        setMaxHeight(Double.MAX_VALUE);
        setMaxWidth(Double.MAX_VALUE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(320.0);
        setPrefWidth(240.0);
        setHgap(10.0);
        setVgap(10.0);
    
        constraintsCol1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsCol1.setMinWidth(10.0);
        constraintsCol1.setPrefWidth(100.0);
        constraintsCol2.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsCol2.setMinWidth(10.0);
        constraintsCol2.setPrefWidth(100.0);
        constraintsCol3.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsCol3.setMinWidth(10.0);
        constraintsCol3.setPrefWidth(100.0);
        constraintsRow1.setMinHeight(10.0);
        constraintsRow1.setPrefHeight(30.0);
        constraintsRow1.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsRow2.setMinHeight(10.0);
        constraintsRow2.setPrefHeight(30.0);
        constraintsRow2.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsRow3.setMinHeight(10.0);
        constraintsRow3.setPrefHeight(30.0);
        constraintsRow3.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsRow4.setMinHeight(10.0);
        constraintsRow4.setPrefHeight(30.0);
        constraintsRow4.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        constraintsRow5.setMinHeight(10.0);
        constraintsRow5.setPrefHeight(30.0);
        constraintsRow5.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        setPadding(new Insets(10.0));
    
        GridPane.setColumnSpan(tfieldValue, 2);
        GridPane.setHalignment(tfieldValue, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(tfieldValue, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setValignment(tfieldValue, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(tfieldValue, javafx.scene.layout.Priority.ALWAYS);
        tfieldValue.setAlignment(javafx.geometry.Pos.CENTER);
        tfieldValue.setPadding(new Insets(3.0));
    
        GridPane.setHalignment(btnNum_1, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_1, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_1, 1);
        GridPane.setValignment(btnNum_1, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_1, javafx.scene.layout.Priority.ALWAYS);
        btnNum_1.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_2, 1);
        GridPane.setHalignment(btnNum_2, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_2, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_2, 1);
        GridPane.setValignment(btnNum_2, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_2, javafx.scene.layout.Priority.ALWAYS);
        btnNum_2.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_3, 2);
        GridPane.setHalignment(btnNum_3, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_3, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_3, 1);
        GridPane.setValignment(btnNum_3, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_3, javafx.scene.layout.Priority.ALWAYS);
        btnNum_3.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setHalignment(btnNum_4, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_4, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_4, 2);
        GridPane.setValignment(btnNum_4, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_4, javafx.scene.layout.Priority.ALWAYS);
        btnNum_4.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_5, 1);
        GridPane.setHalignment(btnNum_5, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_5, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_5, 2);
        GridPane.setValignment(btnNum_5, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_5, javafx.scene.layout.Priority.ALWAYS);
        btnNum_5.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_6, 2);
        GridPane.setHalignment(btnNum_6, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_6, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_6, 2);
        GridPane.setValignment(btnNum_6, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_6, javafx.scene.layout.Priority.ALWAYS);
        btnNum_6.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setHalignment(btnNum_7, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_7, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_7, 3);
        GridPane.setValignment(btnNum_7, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_7, javafx.scene.layout.Priority.ALWAYS);
        btnNum_7.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_8, 1);
        GridPane.setHalignment(btnNum_8, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_8, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_8, 3);
        GridPane.setValignment(btnNum_8, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_8, javafx.scene.layout.Priority.ALWAYS);
        btnNum_8.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_9, 2);
        GridPane.setHalignment(btnNum_9, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_9, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_9, 3);
        GridPane.setValignment(btnNum_9, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_9, javafx.scene.layout.Priority.ALWAYS);
        btnNum_9.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setHalignment(btnVirgula, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnVirgula, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnVirgula, 4);
        GridPane.setValignment(btnVirgula, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnVirgula, javafx.scene.layout.Priority.ALWAYS);
        btnVirgula.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnNum_0, 1);
        GridPane.setHalignment(btnNum_0, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnNum_0, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnNum_0, 4);
        GridPane.setValignment(btnNum_0, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnNum_0, javafx.scene.layout.Priority.ALWAYS);
        btnNum_0.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnOk, 2);
        GridPane.setHalignment(btnOk, javafx.geometry.HPos.CENTER);
        GridPane.setHgrow(btnOk, javafx.scene.layout.Priority.ALWAYS);
        GridPane.setRowIndex(btnOk, 4);
        GridPane.setValignment(btnOk, javafx.geometry.VPos.CENTER);
        GridPane.setVgrow(btnOk, javafx.scene.layout.Priority.ALWAYS);
        btnOk.setMaxWidth(Double.MAX_VALUE);
    
        GridPane.setColumnIndex(btnBksp, 2);
        btnBksp.setMaxWidth(Double.MAX_VALUE);
    
        getColumnConstraints().add(constraintsCol1);
        getColumnConstraints().add(constraintsCol2);
        getColumnConstraints().add(constraintsCol3);
        getRowConstraints().add(constraintsRow1);
        getRowConstraints().add(constraintsRow2);
        getRowConstraints().add(constraintsRow3);
        getRowConstraints().add(constraintsRow4);
        getRowConstraints().add(constraintsRow5);
        getChildren().add(btnNum_1);
        getChildren().add(btnNum_2);
        getChildren().add(btnNum_3);
        getChildren().add(btnNum_4);
        getChildren().add(btnNum_5);
        getChildren().add(btnNum_6);
        getChildren().add(btnNum_7);
        getChildren().add(btnNum_8);
        getChildren().add(btnNum_9);
        getChildren().add(btnVirgula);
        getChildren().add(btnNum_0);
        getChildren().add(btnOk);
        getChildren().add(tfieldValue);
        getChildren().add(btnBksp);
    }
    
    public static NumericKeyboard show(KeyboardValue valueType, Object value) {
        NumericKeyboard window = new NumericKeyboard();
        tipoValor = valueType;
    
        setMobile();
        setDefaultValue(value);
        showWindow(window);
        return window;
    }
    
    private static void setMobile() {
        tfieldValue.getStyleClass().add("lg");
        btnNum_1.getStyleClass().add("lg");
        btnNum_2.getStyleClass().add("lg");
        btnNum_3.getStyleClass().add("lg");
        btnNum_4.getStyleClass().add("lg");
        btnNum_5.getStyleClass().add("lg");
        btnNum_6.getStyleClass().add("lg");
        btnNum_7.getStyleClass().add("lg");
        btnNum_8.getStyleClass().add("lg");
        btnNum_9.getStyleClass().add("lg");
        btnVirgula.getStyleClass().add("lg");
        btnNum_0.getStyleClass().add("lg");
        btnOk.getStyleClass().add("lg");
        btnBksp.getStyleClass().add("lg");
    }
    
    private static void setDefaultValue(Object value) {
        if(value == null) {
            tfieldValue.clear();
        }else{
            if (tipoValor == KeyboardValue.TIME) {
                tfieldValue.setText(StringUtils.toTimeFormat((LocalTime) value));
                MaskTextField.timeField(tfieldValue);
            } else if (tipoValor == KeyboardValue.DECIMAL) {
                tfieldValue.setText(StringUtils.toDecimalFormat((Double) value, 4));
                MaskTextField.numericDotField(tfieldValue);
            } else {
                tfieldValue.setText(StringUtils.toIntegerFormat((Integer) value));
                MaskTextField.numericField(tfieldValue);
            }
        }
    }
    
    private static void showWindow(NumericKeyboard window){
        Stage modalStage = new Stage();
        Scene scene;
        scene = new Scene(window);
    
        Image applicationIcon = new Image(NumericKeyboard.class.getResourceAsStream("/images/delizIcon.png"));
        modalStage.getIcons().add(applicationIcon);
        modalStage.setTitle("SysDeliz 2");
        modalStage.setScene(scene);
        modalStage.initStyle(StageStyle.DECORATED);
        modalStage.initModality(Modality.APPLICATION_MODAL);
        modalStage.setResizable(false);
        modalStage.setOnCloseRequest((WindowEvent arg0) -> {
            modalStage.close();
        });
        modalStage.showAndWait();
    }

}
