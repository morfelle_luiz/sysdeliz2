package sysdeliz2.utils.gui.window;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.daos.generics.implementation.GenericDaoImpl;
import sysdeliz2.daos.generics.interfaces.GenericDao;
import sysdeliz2.models.sysdeliz.SdLogSysdeliz001;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.StaticButtonBuilder;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.BuscaPadrao;
import sysdeliz2.utils.sys.annotations.ExibeTableView;
import sysdeliz2.utils.sys.annotations.TelaSysDeliz;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Comparator;

public abstract class WindowCadastroBasic<T> extends WindowBase {
    
    //<editor-fold defaultstate="collapsed" desc="Desclaração Componentes">
    private final TabPane tabpanePrincipal;
    protected final VBox cpaneFormulario;
    private final TextField tboxFiltroPadrao;
    private final TableView<T> tblRegistros;
    protected final TableColumn<T, T> clnAcoes;
    protected final Button btnFindDefault;
    protected final Button btnSalvar;
    protected final Button btnCancelar;
    //</editor-fold>
    
    private final Class<T> objectParam;
    private final GenericDao<T> daoRegistros;
    private final GenericDao<SdLogSysdeliz001> daoLogSysdeliz;
    private String colunaConsultaPadrao = "";
    
    private final ListProperty<T> listRegisters = new SimpleListProperty<>();
    protected T selectedItem;
    
    private final BooleanProperty disableButtonNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableNextNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disablePreviousNavegation = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnEditRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnDeleteRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnSaveRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty disableBtnCancelRegister = new SimpleBooleanProperty(true);
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final IntegerProperty navegationIndex = new SimpleIntegerProperty(0);

    @SuppressWarnings("unchecked")
    public WindowCadastroBasic(String titulo, Image icone) throws SQLException {
        super(titulo, icone);

        //@SuppressWarnings("unchecked")
        objectParam = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        daoRegistros = new GenericDaoImpl<>(objectParam);
        daoLogSysdeliz = new GenericDaoImpl<>(SdLogSysdeliz001.class);
        

        listRegisters.clear();
        listRegisters.set(daoRegistros.list());
        
        //<editor-fold defaultstate="collapsed" desc="Inicialização Objetos">
        tabpanePrincipal = new TabPane();
        Tab tabRegistros = new Tab();
        VBox vboxRegistros = new VBox();
        HBox hpaneRegistros = new HBox();
        HBox hpaneLeft = new HBox();
        Button btnNovoRegistro = new Button();
        Button btnNovoRegistroFormulario = new Button();
        HBox hpenaRight = new HBox();
        tboxFiltroPadrao = new TextField();
        btnFindDefault = new Button();
        TitledPane tpaneFiltrosAvancados = new TitledPane();
        FlowPane tpaneContentFiltrosAvancados = new FlowPane();
        VBox cpaneRegistros = new VBox();
        tblRegistros = new TableView<>();
        clnAcoes = new TableColumn<>();
        Label lbContadorRegistros = new Label();
        Tab tabFormulario = new Tab();
        VBox vpaneFormulario = new VBox();
        HBox hpaneFormulario = new HBox();
        HBox hpaneNavegacao = new HBox();
        Button btnPrimeiroRegistro = new Button();
        Button btnRegistroAnterior = new Button();
        Button btnProximoRegistro = new Button();
        Button btnUltimoRegistro = new Button();
        Button btnEditarRegistro = new Button();
        Button btnExcluirRegistro = new Button();
        btnSalvar = new Button();
        btnCancelar = new Button();
        Button btnRetornar = new Button();
        cpaneFormulario = new VBox();
        //</editor-fold>
        
        VBox.setVgrow(tabpanePrincipal, javafx.scene.layout.Priority.ALWAYS);
        tabpanePrincipal.setSide(javafx.geometry.Side.BOTTOM);
        tabpanePrincipal.setTabClosingPolicy(javafx.scene.control.TabPane.TabClosingPolicy.UNAVAILABLE);
        
        // <editor-fold defaultstate="collapsed" desc="Botões">
        btnPrimeiroRegistro.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnPrimeiroRegistro.setMnemonicParsing(false);
        btnPrimeiroRegistro.setTooltip(new Tooltip("Primeiro Registro"));
        btnPrimeiroRegistro.setOnAction(this::btnPrimeiroRegistroOnAction);
        btnPrimeiroRegistro.setText("Primeiro");
        btnPrimeiroRegistro.getStyleClass().addAll("button", "secundary");
        btnPrimeiroRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/first (2).png").toExternalForm())));
        HBox.setMargin(btnPrimeiroRegistro, new Insets(0.0));
        btnPrimeiroRegistro.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        
        btnRegistroAnterior.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnRegistroAnterior.setMnemonicParsing(false);
        btnRegistroAnterior.setTooltip(new Tooltip("Registro Anterior"));
        btnRegistroAnterior.setOnAction(this::btnRegistroAnteriorOnAction);
        btnRegistroAnterior.setText("Anterior");
        btnRegistroAnterior.getStyleClass().addAll("button", "secundary");
        btnRegistroAnterior.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/previous (2).png").toExternalForm())));
        HBox.setMargin(btnRegistroAnterior, new Insets(0.0));
        btnRegistroAnterior.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        
        btnProximoRegistro.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnProximoRegistro.setMnemonicParsing(false);
        btnProximoRegistro.setTooltip(new Tooltip("Próximo Registro"));
        btnProximoRegistro.setOnAction(this::btnProximoRegistroOnAction);
        btnProximoRegistro.setText("Próximo");
        btnProximoRegistro.getStyleClass().addAll("button", "secundary");
        btnProximoRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/next (2).png").toExternalForm())));
        HBox.setMargin(btnProximoRegistro, new Insets(0.0));
        btnProximoRegistro.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        
        btnUltimoRegistro.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnUltimoRegistro.setMnemonicParsing(false);
        btnUltimoRegistro.setTooltip(new Tooltip("Último Registro"));
        btnUltimoRegistro.setOnAction(this::btnUltimoRegistroOnAtion);
        btnUltimoRegistro.setText("Último");
        btnUltimoRegistro.getStyleClass().addAll("button", "secundary");
        btnUltimoRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/last (2).png").toExternalForm())));
        HBox.setMargin(btnUltimoRegistro, new Insets(0.0));
        btnUltimoRegistro.setPadding(new Insets(0.0, 1.0, 0.0, 1.0));
        
        btnNovoRegistro.setMnemonicParsing(false);
        btnNovoRegistro.setOnAction(this::btnNovoRegistroOnAction);
        btnNovoRegistro.setText("Adicionar Novo");
        btnNovoRegistro.getStyleClass().addAll("button", "success");
        btnNovoRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/new register (1).png").toExternalForm())));
        
        btnNovoRegistroFormulario.setMnemonicParsing(false);
        btnNovoRegistroFormulario.setOnAction(this::btnNovoRegistroOnAction);
        btnNovoRegistroFormulario.setText("Adicionar Novo");
        btnNovoRegistroFormulario.getStyleClass().addAll("button", "success");
        btnNovoRegistroFormulario.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/new register (1).png").toExternalForm())));
        
        btnEditarRegistro.setMnemonicParsing(false);
        btnEditarRegistro.setOnAction(this::btnEditarRegistroOnAction);
        btnEditarRegistro.getStyleClass().addAll("button", "warning");
        btnEditarRegistro.setText("Editar");
        btnEditarRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/edit register (1).png").toExternalForm())));
        
        btnExcluirRegistro.setMnemonicParsing(false);
        btnExcluirRegistro.setOnAction(this::btnExcluirRegistroOnAction);
        btnExcluirRegistro.getStyleClass().addAll("button", "danger");
        btnExcluirRegistro.setText("Excluir");
        btnExcluirRegistro.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/delete register (1).png").toExternalForm())));
        
        btnSalvar.setMnemonicParsing(false);
        btnSalvar.setOnAction(this::btnSalvarOnAction);
        btnSalvar.getStyleClass().addAll("button", "success");
        btnSalvar.setText("Salvar");
        btnSalvar.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/select (1).png").toExternalForm())));
        
        btnCancelar.setMnemonicParsing(false);
        btnCancelar.setOnAction(this::btnCancelarOnAction);
        btnCancelar.getStyleClass().addAll("button", "danger");
        btnCancelar.setText("Cancelar");
        btnCancelar.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/cancel (1).png").toExternalForm())));
        
        btnRetornar.setMnemonicParsing(false);
        btnRetornar.setOnAction(this::btnRetornarOnAction);
        btnRetornar.getStyleClass().addAll("button", "dark");
        btnRetornar.setText("Voltar");
        btnRetornar.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/return (1).png").toExternalForm())));
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Tab Registros">

        // <editor-fold defaultstate="collapsed" desc="Esqueleto Janela">
        tabRegistros.setClosable(false);
        tabRegistros.setText("Registros");
        vboxRegistros.setPrefHeight(200.0);
        vboxRegistros.setPrefWidth(100.0);
        vboxRegistros.setSpacing(5.0);
        vboxRegistros.setPadding(new Insets(5.0));
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Header Pane">
        hpaneRegistros.setPrefHeight(0.0);
        hpaneRegistros.setPrefWidth(689.0);
        
        HBox.setHgrow(hpaneLeft, javafx.scene.layout.Priority.ALWAYS);
        hpaneLeft.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        hpaneLeft.setPrefHeight(100.0);
        hpaneLeft.setPrefWidth(200.0);
        
        HBox.setHgrow(hpenaRight, javafx.scene.layout.Priority.ALWAYS);
        hpenaRight.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        hpenaRight.setPrefHeight(100.0);
        hpenaRight.setPrefWidth(200.0);
        
        tboxFiltroPadrao.setPrefWidth(220.0);
        tboxFiltroPadrao.getStyleClass().addAll("input-group-middle", "sm");
        loadColunaBuscaPadrao(objectParam);
        
        btnFindDefault.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
        btnFindDefault.setMnemonicParsing(false);
        btnFindDefault.setOnAction(this::btnFindDefaultOnAction);
        btnFindDefault.setText("Busca Padrão");
        btnFindDefault.getStyleClass().addAll("button", "dark", "last");
        btnFindDefault.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/find (1).png").toExternalForm())));
        
        hpaneLeft.getChildren().add(btnNovoRegistro);
        hpaneRegistros.getChildren().add(hpaneLeft);
        hpenaRight.getChildren().add(tboxFiltroPadrao);
        hpenaRight.getChildren().add(btnFindDefault);
        hpaneRegistros.getChildren().add(hpenaRight);
        vboxRegistros.getChildren().add(hpaneRegistros);
        
        tpaneFiltrosAvancados.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        tpaneFiltrosAvancados.setExpanded(false);
        tpaneFiltrosAvancados.setText("Filtros Avançados");
        tpaneContentFiltrosAvancados.setPrefHeight(200.0);
        tpaneContentFiltrosAvancados.setPrefWidth(200.0);
        Node contentBustaAvancada = setContentBuscaAvancada();
        if (contentBustaAvancada != null) {
            tpaneContentFiltrosAvancados.getChildren().add(contentBustaAvancada);
            tpaneFiltrosAvancados.setContent(tpaneContentFiltrosAvancados);
            tpaneFiltrosAvancados.setGraphic(new ImageView(new Image(getClass().getResource("/images/icons/buttons/filter (1).png").toExternalForm())));
            vboxRegistros.getChildren().add(tpaneFiltrosAvancados);
        }
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Content Pane">
        VBox.setVgrow(cpaneRegistros, javafx.scene.layout.Priority.ALWAYS);
        cpaneRegistros.setPrefHeight(200.0);
        cpaneRegistros.setPrefWidth(100.0);
        
        // <editor-fold defaultstate="collapsed" desc="Tabela Registros">
        VBox.setVgrow(tblRegistros, javafx.scene.layout.Priority.ALWAYS);
        tblRegistros.setEditable(true);
        tblRegistros.setPrefHeight(200.0);
        tblRegistros.setPrefWidth(200.0);
        tblRegistros.itemsProperty().bind(listRegisters);

        // <editor-fold defaultstate="collapsed" desc="Coluna AÇÕES">
        clnAcoes.setEditable(false);
        clnAcoes.setPrefWidth(108.0);
        clnAcoes.setResizable(false);
        clnAcoes.setSortable(false);
        clnAcoes.setText("Ações");
        clnAcoes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<T, T>, ObservableValue<T>>() {
            @Override
            public ObservableValue<T> call(TableColumn.CellDataFeatures<T, T> features) {
                return new ReadOnlyObjectWrapper<>(features.getValue());
            }
        });
        clnAcoes.setComparator(new Comparator<T>() {
            @Override
            public int compare(T p1, T p2) {
                return compare(p1, p2);
            }
        });
        clnAcoes.setCellFactory(new Callback<TableColumn<T, T>, TableCell<T, T>>() {
            @Override
            public TableCell<T, T> call(TableColumn<T, T> btnCol) {
                return new TableCell<T, T>() {
                    final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
                    final Button btnEditRow = StaticButtonBuilder.genericEditButton();
                    final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();

                    final HBox boxButtonsRow = new HBox(3);

                    @Override
                    public void updateItem(T seletedRow, boolean empty) {
                        super.updateItem(seletedRow, empty);
                        if (seletedRow != null) {
                            boxButtonsRow.getChildren().clear();
                            boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                            setGraphic(boxButtonsRow);
                            
                            btnInfoRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                bindData(selectedItem);
                                tabpanePrincipal.getSelectionModel().select(1);
                            });
                            btnEditRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                tabpanePrincipal.getSelectionModel().select(1);
                                btnEditarRegistroOnAction(null);
                            });
                            btnDeleteRow.setOnAction(event -> {
                                selectedItem = seletedRow;
                                btnExcluirRegistroOnAction(null);
                            });
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
            }
        });
        tblRegistros.getColumns().add(clnAcoes);
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Criação das Colunas da Tabela">
        this.loadColunasTabela(objectParam);
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Label Contador de Registros">
        lbContadorRegistros.setMaxWidth(Double.MAX_VALUE);
        lbContadorRegistros.textProperty().bind(new SimpleStringProperty("Mostrando ").concat(listRegisters.sizeProperty()).concat(" registros"));
        lbContadorRegistros.setTextFill(javafx.scene.paint.Color.valueOf("#655959"));
        lbContadorRegistros.setFont(new Font("System Italic", 11.0));
        //</editor-fold>

        cpaneRegistros.getChildren().add(tblRegistros);
        cpaneRegistros.getChildren().add(lbContadorRegistros);
        //</editor-fold>

        vboxRegistros.getChildren().add(cpaneRegistros);
        //</editor-fold>

        tabRegistros.setContent(vboxRegistros);
        tabpanePrincipal.getTabs().add(tabRegistros);
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Tab Formulário">
        // <editor-fold defaultstate="collapsed" desc="Esqueleto Janela">
        tabFormulario.setClosable(false);
        tabFormulario.setText("Formulário");
        vpaneFormulario.setSpacing(5.0);
        vpaneFormulario.setPadding(new Insets(5.0));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Header Pane">
        hpaneFormulario.setMinHeight(15.0);
        hpaneFormulario.setPrefHeight(15.0);
        hpaneFormulario.setPrefWidth(5000.0);
        hpaneFormulario.getStyleClass().add("sm");
        HBox.setHgrow(hpaneNavegacao, javafx.scene.layout.Priority.ALWAYS);
        hpaneNavegacao.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        hpaneNavegacao.setPrefHeight(20.0);
        hpaneNavegacao.setSpacing(2.0);
        
        hpaneNavegacao.getChildren().add(btnPrimeiroRegistro);
        hpaneNavegacao.getChildren().add(btnRegistroAnterior);
        hpaneNavegacao.getChildren().add(btnProximoRegistro);
        hpaneNavegacao.getChildren().add(btnUltimoRegistro);
        hpaneNavegacao.getChildren().add(new Separator(Orientation.VERTICAL));
        hpaneNavegacao.getChildren().add(btnNovoRegistroFormulario);
        hpaneNavegacao.getChildren().add(btnEditarRegistro);
        hpaneNavegacao.getChildren().add(btnExcluirRegistro);
        hpaneNavegacao.getChildren().add(new Separator(Orientation.VERTICAL));
        hpaneNavegacao.getChildren().add(btnSalvar);
        hpaneNavegacao.getChildren().add(btnCancelar);
        hpaneFormulario.getChildren().add(hpaneNavegacao);
        hpaneFormulario.getChildren().add(btnRetornar);
        
        vpaneFormulario.getChildren().add(hpaneFormulario);
        vpaneFormulario.getChildren().add(new Separator(Orientation.HORIZONTAL));
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Content Pane">
        VBox.setVgrow(cpaneFormulario, javafx.scene.layout.Priority.ALWAYS);
        
        vpaneFormulario.getChildren().add(cpaneFormulario);
        //</editor-fold>
        tabFormulario.setContent(vpaneFormulario);
        tabpanePrincipal.getTabs().add(tabFormulario);
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Bind/Init Componentes">
        tabpanePrincipal.getTabs().get(0).disableProperty().bind(inEdition);
        
        disableBtnSaveRegister.bind(inEdition.not());
        disableBtnCancelRegister.bind(inEdition.not());
        disableBtnEditRegister.bind(tblRegistros.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableBtnDeleteRegister.bind(tblRegistros.getSelectionModel().selectedItemProperty().isNull().or(inEdition));
        disableButtonNavegation.bind(tblRegistros.itemsProperty().isNull().or(inEdition));
        disablePreviousNavegation.bind(navegationIndex.isEqualTo(0));
        disableNextNavegation.bind(navegationIndex.greaterThanOrEqualTo(listRegisters.sizeProperty().subtract(1)));
        
        btnCancelar.disableProperty().bind(disableBtnCancelRegister);
        btnSalvar.disableProperty().bind(disableBtnSaveRegister);
        btnEditarRegistro.disableProperty().bind(disableBtnEditRegister);
        btnExcluirRegistro.disableProperty().bind(disableBtnDeleteRegister);
        btnRetornar.disableProperty().bind(inEdition);
        btnNovoRegistroFormulario.disableProperty().bind(inEdition);
        btnNovoRegistro.disableProperty().bind(inEdition);
        
        btnPrimeiroRegistro.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnRegistroAnterior.disableProperty().bind(disableButtonNavegation.or(disablePreviousNavegation));
        btnProximoRegistro.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        btnUltimoRegistro.disableProperty().bind(disableButtonNavegation.or(disableNextNavegation));
        
        tblRegistros.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            unbindData();
            bindData(newValue);
            selectedItem = newValue;
            navegationIndex.set(tblRegistros.getItems().indexOf(newValue));
        });
        tblRegistros.getSelectionModel().selectFirst();
        //</editor-fold>
        
        box.getChildren().add(tabpanePrincipal);
    }
    
    private void loadColunasTabela(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            ExibeTableView exibirAtributo = field.getAnnotation(ExibeTableView.class);
            if (exibirAtributo != null) {
                TableColumn<T, Object> coluna = new TableColumn<>(exibirAtributo.descricao().toUpperCase());
                coluna.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                coluna.setPrefWidth(exibirAtributo.width());
                coluna.setCellValueFactory(new PropertyValueFactory<T, Object>(field.getName()));
                coluna.setCellFactory(column -> {
                    return new TableCell<T, Object>() {
                        @Override
                        protected void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                switch(exibirAtributo.columnType()) {
                                    case BOOLEAN:
                                        setText((Boolean) item ? "SIM" : "NÃO");
                                        break;
                                    case DECIMAL:
                                        setText(StringUtils.toDecimalFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                        break;
                                    case MONETARY:
                                        setText(StringUtils.toMonetaryFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                        break;
                                    case PERCENT:
                                        setText(StringUtils.toPercentualFormat(((BigDecimal) item).doubleValue(), exibirAtributo.decimalValue()));
                                        break;
                                    case NUMBER:
                                        setText(StringUtils.toIntegerFormat(((BigDecimal) item).intValue()));
                                        break;
                                    case TEXT:
                                        setText(item.toString());
                                        break;
                                }
                            }
                        }
                    };
                });
                tblRegistros.getColumns().add(coluna);
            }
        }
    }
    
    private void loadColunaBuscaPadrao(Class<T> classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            BuscaPadrao buscarAtributo = field.getAnnotation(BuscaPadrao.class);
            if (buscarAtributo != null) {
                tboxFiltroPadrao.setPromptText("BUSCAR POR " + buscarAtributo.descricao().toUpperCase());
                colunaConsultaPadrao = field.getName();
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void btnFindDefaultOnAction(javafx.event.ActionEvent actionEvent) {
        try {
            listRegisters.set(daoRegistros.initCriteria().addPredicateLike(colunaConsultaPadrao, tboxFiltroPadrao.getText().toUpperCase()).loadListByPredicate());
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
        }
    }
    
    private void btnPrimeiroRegistroOnAction(javafx.event.ActionEvent actionEvent) {
        tblRegistros.getSelectionModel().selectFirst();
    }
    
    private void btnRegistroAnteriorOnAction(javafx.event.ActionEvent actionEvent) {
        tblRegistros.getSelectionModel().selectPrevious();
    }
    
    private void btnProximoRegistroOnAction(javafx.event.ActionEvent actionEvent) {
        tblRegistros.getSelectionModel().selectNext();
    }
    
    private void btnUltimoRegistroOnAtion(javafx.event.ActionEvent actionEvent) {
        tblRegistros.getSelectionModel().selectLast();
    }
    
    private void btnNovoRegistroOnAction(javafx.event.ActionEvent actionEvent) {
        try {
            unbindData();
            selectedItem = objectParam.newInstance();
            bindBidirectionalData(selectedItem);
            inEdition.set(true);
            
            if (tabpanePrincipal.getSelectionModel().getSelectedIndex() == 0) {
                tabpanePrincipal.getSelectionModel().select(1);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            GUIUtils.showException(e);
        }
    }
    
    private void btnEditarRegistroOnAction(javafx.event.ActionEvent actionEvent) {
        unbindData();
        inEdition.set(true);
        bindBidirectionalData(selectedItem);
    }
    
    private void btnExcluirRegistroOnAction(javafx.event.ActionEvent actionEvent) {
        if (GUIUtils.showQuestionMessageDefaultNao("Deseja realmente excluir o registro selecionado?")) {
            try {
                daoRegistros.delete(selectedItem);
                daoLogSysdeliz.save(
                        new SdLogSysdeliz001(LocalDateTime.now(),
                                SceneMainController.getAcesso().getUsuario(),
                                objectParam.getAnnotation(TelaSysDeliz.class).descricao(),
                                TipoAcao.EXCLUIR,
                                selectedItem.toString(),
                                "Excluído " + selectedItem.toString())
                );
                listRegisters.remove(selectedItem);
                tblRegistros.getSelectionModel().selectFirst();
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
        }
    }
    
    private void btnSalvarOnAction(javafx.event.ActionEvent actionEvent) {
        if (this.validationForm()) {
            
            unbindBidirectionalData(selectedItem);
            //T objectEdited = selectedItem;
            
            try {
                if (!listRegisters.contains(selectedItem)) {
                    selectedItem = daoRegistros.save(selectedItem);
                    daoLogSysdeliz.save(
                            new SdLogSysdeliz001(LocalDateTime.now(),
                                    SceneMainController.getAcesso().getUsuario(),
                                    objectParam.getAnnotation(TelaSysDeliz.class).descricao(),
                                    TipoAcao.CADASTRAR,
                                    selectedItem.toString(),
                                    "Cadastrado " + selectedItem.toString())
                    );
                    listRegisters.add(selectedItem);
                } else {
                    selectedItem = daoRegistros.update(selectedItem);
                    daoLogSysdeliz.save(
                            new SdLogSysdeliz001(LocalDateTime.now(),
                                    SceneMainController.getAcesso().getUsuario(),
                                    objectParam.getAnnotation(TelaSysDeliz.class).descricao(),
                                    TipoAcao.EDITAR,
                                    selectedItem.toString(),
                                    "Alterado " + selectedItem.toString())
                    );
                }
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                GUIUtils.showException(e);
            }
            inEdition.set(false);
            tblRegistros.refresh();
            tblRegistros.getSelectionModel().select(selectedItem);
        }
    }
    
    private void btnCancelarOnAction(javafx.event.ActionEvent actionEvent) {
        unbindBidirectionalData(selectedItem);
        clearForm();
        inEdition.set(false);
        tblRegistros.getSelectionModel().selectFirst();
    }
    
    private void btnRetornarOnAction(javafx.event.ActionEvent actionEvent) {
        tabpanePrincipal.getSelectionModel().select(0);
    }
    
    protected abstract void bindData(T selectItem);
    
    protected abstract void unbindData();
    
    protected abstract void bindBidirectionalData(T selectItem);
    
    protected abstract void unbindBidirectionalData(T selectItem);
    
    protected abstract void clearForm();
    
    protected abstract boolean validationForm();
    
    /**
     * Construção do conteúdo do filtro avançado
     *
     * @return Node
     */
    protected abstract Node setContentBuscaAvancada();
    
}
