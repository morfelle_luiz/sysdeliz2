package sysdeliz2.utils.gui.window.base;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdLogSysdeliz001;
import sysdeliz2.utils.StaticButtonBuilder;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.LocalLogger;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public abstract class CadastroBase<T> extends WindowBase {

    //<editor-fold defaultstate="collapsed" desc="Desclaração Componentes">
    protected FormFieldText tboxFiltroPadrao = FormFieldText.create(field -> {
        field.withoutTitle();
        field.width(250.0);
    });
    protected FormBox cpaneFormulario = FormBox.create(box -> {
        box.vertical();
        box.expanded();
    });

    protected FormTableView<T> tblRegistros;
    protected Button btnFindDefault;
    protected Button btnFindAdvanced;
    protected FormNavegation<T> navegation;
    //</editor-fold>
    protected ObjectProperty<T> selected = new SimpleObjectProperty<>();

    private final Class<T> objectParam;
    protected final ListProperty<T> listRegisters = new SimpleListProperty<>();

    @SuppressWarnings("unchecked")
    public CadastroBase(String titulo, Image icone) {
        super(titulo, icone, new String[]{"Registros", "Formulário"});
        super.tabs.setSide(Side.BOTTOM);
        super.tabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        objectParam = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        AtomicReference<List<T>> registros = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            registros.set((List<T>) new FluentDao().selectFrom(objectParam).get().resultList());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                listRegisters.clear();
                listRegisters.set(FXCollections.observableList(registros.get()));

                //<editor-fold defaultstate="collapsed" desc="Inicialização Objetos">
                // components tab listagem
                btnFindDefault = FormButton.create(btn -> {
                    btn.tooltip("Procura padrão de registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                    btn.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
                    btn.setOnAction(evt -> btnFindDefaultOnAction(tboxFiltroPadrao.value.getValue()));
                    btn.addStyle("dark");
                });
                btnFindAdvanced = FormButton.create(btn -> {
                    btn.tooltip("Procura avançada de registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.FILTRO, ImageUtils.IconSize._16));
                    btn.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
                    btn.setOnAction(evt -> btnFindAdvanced());
                    btn.addStyle("warning");
                });
                tblRegistros = FormTableView.create(objectParam, table -> {
                    table.withoutTitle();
                    table.expanded();
                    table.items.bind(listRegisters);
                    table.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Ações");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(108.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<T, T>, ObservableValue<T>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<T, T>() {
                                final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
                                final Button btnEditRow = StaticButtonBuilder.genericEditButton();
                                final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();

                                final HBox boxButtonsRow = new HBox(3);

                                @Override
                                public void updateItem(T seletedRow, boolean empty) {
                                    super.updateItem(seletedRow, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (seletedRow != null && !empty) {
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                                        setGraphic(boxButtonsRow);

                                        btnInfoRow.setOnAction(event -> {
                                            bindData(seletedRow);
                                            tabs.getSelectionModel().select(1);
                                        });
                                        btnEditRow.setOnAction(event -> {
                                            tabs.getSelectionModel().select(1);
                                            btnEditarRegistroOnAction(seletedRow);
                                        });
                                        btnDeleteRow.setOnAction(event -> {
                                            btnExcluirRegistroOnAction(seletedRow);
                                        });
                                    }
                                }
                            };
                        });
                    }).build());
                    table.columns(setColumnsTable());
                });
                Button btnNovoRegistro = FormButton.create(btn -> {
                    btn.setAction(evt -> {
                        super.tabs.getSelectionModel().select(1);
                        btnNovoRegistroOnAction();
                    });
                    btn.title("Adicionar Novo Registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                    btn.addStyle("success");
                });
                ((VBox) super.tabs.getTabs().get(0).getContent()).getChildren().add(FormBox.create(containerTab -> {
                    containerTab.vertical();
                    containerTab.expanded();
                    containerTab.add(FormBox.create(headerRegistrosTab -> {
                        headerRegistrosTab.horizontal();
                        headerRegistrosTab.add(FormBox.create(toolbarRegistrosTab -> {
                            toolbarRegistrosTab.horizontal();
                            toolbarRegistrosTab.expanded();
                            toolbarRegistrosTab.alignment(Pos.CENTER_LEFT);
                            toolbarRegistrosTab.add(btnNovoRegistro);
                        }));
                        headerRegistrosTab.add(FormBox.create(findRegistrosTab -> {
                            findRegistrosTab.horizontal();
                            findRegistrosTab.setId("box-find-filters");
                            findRegistrosTab.alignment(Pos.CENTER_RIGHT);
                            findRegistrosTab.add(tboxFiltroPadrao.build(), btnFindDefault);
                            Separator separator = new Separator(Orientation.VERTICAL);
                            separator.setId("separator-btn-find-advanced");
                            findRegistrosTab.add(separator, btnFindAdvanced);

                        }));
                    }));
                    containerTab.add(FormBox.create(contentRegistrosTab -> {
                        contentRegistrosTab.vertical();
                        contentRegistrosTab.expanded();
                        contentRegistrosTab.add(tblRegistros.build());
                    }));
                }));

                // componentes tab manutenção
                navegation = FormNavegation.create(nav -> {
                    nav.withNavegation(tblRegistros);
                    nav.withActions(true, true, true);
                    nav.withReturn(evt -> this.tabs.getSelectionModel().select(0));
                    nav.btnAddRegister(evt -> btnNovoRegistroOnAction());
                    nav.btnDeleteRegister(evt -> btnExcluirRegistroOnAction((T) nav.selectedItem.get()));
                    nav.btnEditRegister(evt -> btnEditarRegistroOnAction((T) nav.selectedItem.get()));
                    nav.btnSave(evt -> btnSalvarOnAction((T) selected.get()));
                    nav.btnCancel(evt -> btnCancelarOnAction((T) selected.get()));
                    nav.selectedItem.addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            unbindData();
                            bindData((T) newValue);
                        }
                    });
                });
                ((VBox) super.tabs.getTabs().get(1).getContent()).getChildren().add(FormBox.create(containerTab -> {
                    containerTab.vertical();
                    containerTab.expanded();
                    containerTab.add(navegation);
                    containerTab.add(cpaneFormulario);
                }));
                //</editor-fold>

                tblRegistros.tableProperties().getSelectionModel().selectFirst();
                initializeComponents();
            }
        });
    }

    public CadastroBase(String titulo, Image icone, int limite) {
        super(titulo, icone, new String[]{"Registros", "Formulário"});
        super.tabs.setSide(Side.BOTTOM);
        super.tabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        objectParam = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        AtomicReference<List<T>> registros = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            registros.set((List<T>) new FluentDao().selectFrom(objectParam).get().resultList(limite));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                listRegisters.clear();
                listRegisters.set(FXCollections.observableList(registros.get()));

                //<editor-fold defaultstate="collapsed" desc="Inicialização Objetos">
                // components tab listagem
                btnFindDefault = FormButton.create(btn -> {
                    btn.tooltip("Procura padrão de registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                    btn.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
                    btn.setOnAction(evt -> btnFindDefaultOnAction(tboxFiltroPadrao.value.getValue()));
                    btn.addStyle("dark");
                });
                btnFindAdvanced = FormButton.create(btn -> {
                    btn.tooltip("Procura avançada de registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.FILTRO, ImageUtils.IconSize._16));
                    btn.setContentDisplay(javafx.scene.control.ContentDisplay.GRAPHIC_ONLY);
                    btn.setOnAction(evt -> btnFindAdvanced());
                    btn.addStyle("warning");
                });
                tblRegistros = FormTableView.create(objectParam, table -> {
                    table.withoutTitle();
                    table.expanded();
                    table.items.bind(listRegisters);
                    table.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Ações");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(108.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<T, T>, ObservableValue<T>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<T, T>() {
                                final Button btnInfoRow = StaticButtonBuilder.genericInfoButton();
                                final Button btnEditRow = StaticButtonBuilder.genericEditButton();
                                final Button btnDeleteRow = StaticButtonBuilder.genericDeleteButton();

                                final HBox boxButtonsRow = new HBox(3);

                                @Override
                                public void updateItem(T seletedRow, boolean empty) {
                                    super.updateItem(seletedRow, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (seletedRow != null && !empty) {
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnInfoRow, btnEditRow, btnDeleteRow);
                                        setGraphic(boxButtonsRow);

                                        btnInfoRow.setOnAction(event -> {
                                            bindData(seletedRow);
                                            tabs.getSelectionModel().select(1);
                                        });
                                        btnEditRow.setOnAction(event -> {
                                            tabs.getSelectionModel().select(1);
                                            btnEditarRegistroOnAction(seletedRow);
                                        });
                                        btnDeleteRow.setOnAction(event -> {
                                            btnExcluirRegistroOnAction(seletedRow);
                                        });
                                    }
                                }
                            };
                        });
                    }).build());
                    table.columns(setColumnsTable());
                });
                Button btnNovoRegistro = FormButton.create(btn -> {
                    btn.setAction(evt -> {
                        super.tabs.getSelectionModel().select(1);
                        btnNovoRegistroOnAction();
                    });
                    btn.title("Adicionar Novo Registro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                    btn.addStyle("success");
                });
                ((VBox) super.tabs.getTabs().get(0).getContent()).getChildren().add(FormBox.create(containerTab -> {
                    containerTab.vertical();
                    containerTab.expanded();
                    containerTab.add(FormBox.create(headerRegistrosTab -> {
                        headerRegistrosTab.horizontal();
                        headerRegistrosTab.add(FormBox.create(toolbarRegistrosTab -> {
                            toolbarRegistrosTab.horizontal();
                            toolbarRegistrosTab.expanded();
                            toolbarRegistrosTab.alignment(Pos.CENTER_LEFT);
                            toolbarRegistrosTab.add(btnNovoRegistro);
                        }));
                        headerRegistrosTab.add(FormBox.create(findRegistrosTab -> {
                            findRegistrosTab.horizontal();
                            findRegistrosTab.setId("box-find-filters");
                            findRegistrosTab.alignment(Pos.CENTER_RIGHT);
                            findRegistrosTab.add(tboxFiltroPadrao.build(), btnFindDefault);
                            Separator separator = new Separator(Orientation.VERTICAL);
                            separator.setId("separator-btn-find-advanced");
                            findRegistrosTab.add(separator, btnFindAdvanced);

                        }));
                    }));
                    containerTab.add(FormBox.create(contentRegistrosTab -> {
                        contentRegistrosTab.vertical();
                        contentRegistrosTab.expanded();
                        contentRegistrosTab.add(tblRegistros.build());
                    }));
                }));

                // componentes tab manutenção
                navegation = FormNavegation.create(nav -> {
                    nav.withNavegation(tblRegistros);
                    nav.withActions(true, true, true);
                    nav.withReturn(evt -> this.tabs.getSelectionModel().select(0));
                    nav.btnAddRegister(evt -> btnNovoRegistroOnAction());
                    nav.btnDeleteRegister(evt -> btnExcluirRegistroOnAction((T) nav.selectedItem.get()));
                    nav.btnEditRegister(evt -> btnEditarRegistroOnAction((T) nav.selectedItem.get()));
                    nav.btnSave(evt -> btnSalvarOnAction((T) selected.get()));
                    nav.btnCancel(evt -> btnCancelarOnAction((T) selected.get()));
                    nav.selectedItem.addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            unbindData();
                            bindData((T) newValue);
                        }
                    });
                });
                ((VBox) super.tabs.getTabs().get(1).getContent()).getChildren().add(FormBox.create(containerTab -> {
                    containerTab.vertical();
                    containerTab.expanded();
                    containerTab.add(navegation);
                    containerTab.add(cpaneFormulario);
                }));
                //</editor-fold>

                tblRegistros.tableProperties().getSelectionModel().selectFirst();
                initializeComponents();
            }
        });
    }

    protected abstract void initializeComponents();

    private void btnNovoRegistroOnAction() {
        try {
            unbindData();
            navegation.inEdition.set(true);
            T novoRegistro = (T) objectParam.newInstance();
            bindBidirectionalData(novoRegistro);
            selected.set(novoRegistro);

            if (tabs.getSelectionModel().getSelectedIndex() == 0) {
                tabs.getSelectionModel().select(1);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), "CLASS_NAME:METHOD_NAME");
            ExceptionBox.build(ebox -> {
                ebox.exception(e);
                ebox.showAndWait();
            });
        }
    }

    private void btnExcluirRegistroOnAction(T selectedItem) {
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o cadastro?");
            message.showAndWait();
        }).value.get()) {
            try {
                new FluentDao().delete(selectedItem);
                new FluentDao().persist(new SdLogSysdeliz001(LocalDateTime.now(),
                        SceneMainController.getAcesso().getUsuario(),
                        objectParam.getName(),
                        TipoAcao.EXCLUIR,
                        selectedItem.toString(),
                        "Excluído " + selectedItem.toString()));
                listRegisters.remove(selectedItem);
                tblRegistros.tableProperties().getSelectionModel().selectFirst();
                MessageBox.create(message -> {
                    message.message("Cadastro excluído com sucesso");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                ExceptionBox.build(ebox -> {
                    ebox.exception(e);
                    ebox.showAndWait();
                });
            }
        }
    }

    private void btnEditarRegistroOnAction(T selectedItem) {
        unbindData();
        navegation.inEdition.set(true);
        bindBidirectionalData(selectedItem);
        selected.set(selectedItem);
    }

    private void btnSalvarOnAction(T selectedItem) {
        if (this.validationForm()) {
            unbindBidirectionalData(selectedItem);

            try {
                if (!listRegisters.contains(selectedItem)) {
                    selectedItem = new FluentDao().persist(selectedItem);
                    new FluentDao().persist(new SdLogSysdeliz001(LocalDateTime.now(),
                            SceneMainController.getAcesso().getUsuario(),
                            objectParam.getName(),
                            TipoAcao.CADASTRAR,
                            selectedItem.toString(),
                            "Cadastrado " + selectedItem.toString()));
                    listRegisters.add(selectedItem);
                } else {
                    selectedItem = new FluentDao().merge(selectedItem);
                    new FluentDao().persist(new SdLogSysdeliz001(LocalDateTime.now(),
                            SceneMainController.getAcesso().getUsuario(),
                            objectParam.getName(),
                            TipoAcao.EDITAR,
                            selectedItem.toString(),
                            "Alterado " + selectedItem.toString()));
                }
                navegation.inEdition.set(false);
                tblRegistros.refresh();
                tblRegistros.tableProperties().getSelectionModel().select(selectedItem);
                MessageBox.create(message -> {
                    message.message("Cadastro salvo com sucesso");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), this.getClass().getName() + ":" + this.getClass().getEnclosingMethod().getName());
                ExceptionBox.build(ebox -> {
                    ebox.exception(e);
                    ebox.showAndWait();
                });
            }
        }
    }

    private void btnCancelarOnAction(T selectItem) {
        unbindBidirectionalData(selectItem);
        //clearForm();
        navegation.inEdition.set(false);
        tblRegistros.tableProperties().getSelectionModel().selectFirst();
    }

    protected abstract FormTableColumn[] setColumnsTable();

    protected abstract void btnFindDefaultOnAction(String defaultValue);

    protected abstract void btnFindAdvanced();

    protected abstract void bindData(T selectItem);

    protected abstract void unbindData();

    protected abstract void bindBidirectionalData(T selectItem);

    protected abstract void unbindBidirectionalData(T selectItem);

    protected abstract void clearForm();

    protected abstract boolean validationForm();

}
