package sysdeliz2.utils.gui.window.base;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import sysdeliz2.utils.sys.ImageUtils;

public abstract class WindowBase extends StackPane {
    
    protected final VBox root;
    protected final VBox box;
    public final ImageView icon;
    public final Label title;
    protected final TabPane tabs;
    protected final Button btnClose;
    
    private final VBox boxImage;
    
    public WindowBase() {
        this(null, null, null, false);
    }
    
    public WindowBase(String title, Image icone) {
        this(title, icone, null, false);
    }
    
    public WindowBase(String title, Image icon, String[] tabs) {
            this(title, icon, tabs, false);
    }
    
    public WindowBase(String title, Image icon, String[] tabs, Boolean btnClose) {
        
        HBox boxHeader = new HBox();
        this.icon = new ImageView();
        this.title = new Label();
        this.root = new VBox();
        this.box = new VBox();
        this.tabs = new TabPane();
        this.boxImage = new VBox();
        this.btnClose = new Button();
        
        BorderPane.setAlignment(boxHeader, javafx.geometry.Pos.CENTER_LEFT);
        boxHeader.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        boxHeader.setPrefHeight(0.0);
        boxHeader.setPrefWidth(711.0);
        boxHeader.setSpacing(5.0);
        boxHeader.getStyleClass().add("header-window-base");


        this.icon.setFitHeight(52.0);
        this.icon.setFitWidth(52.0);
        this.icon.setPickOnBounds(true);
        this.icon.setPreserveRatio(true);
        try {
            this.icon.setImage(icon);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        this.boxImage.setPadding(new Insets(4));
//        this.boxImage.setStyle("-fx-background-color: #FFFFFF; -fx-background-radius: 3");
//        this.boxImage.getChildren().add(this.icon);
        
        HBox.setHgrow(this.title, javafx.scene.layout.Priority.ALWAYS);
        this.title.setMaxHeight(Double.MAX_VALUE);
        this.title.setMaxWidth(Double.MAX_VALUE);
        this.title.setPrefHeight(52.0);
        //this.title.setPrefWidth();
        this.title.setText(title);
        this.title.setTextFill(javafx.scene.paint.Color.WHITE);
        HBox.setMargin(this.title, new Insets(0.0));
        this.title.setFont(new Font(22.0));
        
        boxHeader.setPadding(new Insets(5.0));
        boxHeader.setSpacing(5.0);
        boxHeader.getChildren().add(this.icon);
        boxHeader.getChildren().add(this.title);
        if (btnClose) {
            this.btnClose.setText("Fechar");
            this.btnClose.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
            this.btnClose.setMinWidth(200.0);
            this.btnClose.setOnAction(event -> {
                closeWindow();
            });
            boxHeader.getChildren().add(this.btnClose);
        }
        root.getChildren().add(boxHeader);
        
        //BorderPane.setAlignment(box, javafx.geometry.Pos.CENTER);
        VBox.setVgrow(this.box, javafx.scene.layout.Priority.ALWAYS);
        this.box.setPrefHeight(600.0);
        this.box.setPrefWidth(100.0);
        this.box.setSpacing(5.0);
        this.box.setPadding(new Insets(5.0));
        
        if (tabs != null) {
            VBox.setVgrow(this.tabs, javafx.scene.layout.Priority.ALWAYS);
            for (String aba : tabs) {
                Tab abaWindow = new Tab(aba);
                abaWindow.setClosable(false);
                VBox boxTab = new VBox();
                boxTab.setSpacing(5.0);
                boxTab.setPadding(new Insets(5.0));
                abaWindow.setContent(boxTab);
                this.tabs.getTabs().add(abaWindow);
            }
        }
        
        root.getChildren().add(tabs != null && tabs.length > 0 ? this.tabs : box);
        getChildren().add(root);
    }

    public void setTitleWindow(String title) {

    }

    public abstract void closeWindow();
    
}

