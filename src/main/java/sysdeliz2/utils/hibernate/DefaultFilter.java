package sysdeliz2.utils.hibernate;

import sysdeliz2.utils.enums.PredicateType;

public class DefaultFilter {

    public Object valor;
    public String coluna;
    public PredicateType predicate = PredicateType.EQUAL;

    public DefaultFilter() {
    }

    public DefaultFilter(Object valor, String coluna) {
        this.valor = valor;
        this.coluna = coluna;
    }

    public DefaultFilter(Object valor, String coluna, PredicateType predicate) {
        this.valor = valor;
        this.coluna = coluna;
        this.predicate = predicate;
    }

}
