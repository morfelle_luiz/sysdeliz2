package sysdeliz2.utils.sys;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    private static DateUtils instance;
    private SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private DateUtils(){}

    public static DateUtils getInstance() {
        if(instance == null){
            instance = new DateUtils();
        }
        return instance;
    }

    public LocalDateTime getDate(String date){
        if(date == null){
            return null;
        }
        try {
            return LocalDateTime.ofInstant(simpleDateTimeFormat.parse(date).toInstant(), TimeZone.getDefault().toZoneId());
        }catch (ParseException e) {
            return LocalDateTime.now();
        }
    }

    public String getSimpleDate(String date){
        return DateTimeFormatter.ofPattern("dd/MM/yyyy").format(getDate(date));
    }

    public String getSimpleTime(String date){
        return DateTimeFormatter.ofPattern("HH:mm").format(getDate(date));
    }

    public String getUpdateTimestamp(String date){
        if(date == null || date.trim().equals("")){
            return null;
        }

        try{
            LocalDateTime dt = LocalDateTime.ofInstant(simpleDateTimeFormat.parse(date).toInstant(), TimeZone.getDefault().toZoneId());
            String formatada = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(dt);
            return "TO_TIMESTAMP('"+ formatada + "', 'YYYY-MM-DD HH24:MI:SS')";
        }catch (ParseException e) {
            return null;
        }
    }

    public String getToDate(String date){
        if(date == null || date.trim().equals("")){
            return null;
        }

        try{
            LocalDate dt = LocalDateTime.ofInstant(simpleDateFormat.parse(date).toInstant(), TimeZone.getDefault().toZoneId()).toLocalDate();
            String formatada = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dt);
            return "TO_DATE('"+ formatada + "', 'YYYY-MM-DD')";
        }catch (ParseException e) {
            return null;
        }
    }

    public static boolean between(LocalDate dateToCheck, LocalDate startDate, LocalDate endDate) {
        return dateToCheck.compareTo(startDate) >= 0 && dateToCheck.compareTo(endDate) <=0;
    }
}
