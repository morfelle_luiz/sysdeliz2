package sysdeliz2.utils.sys;

import javafx.beans.property.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sysdeliz2.utils.sys.annotations.ExportExcel;
import sysdeliz2.utils.sys.annotations.HideExportExcel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class ExportUtils {

    private List<Field> columnsClass = new ArrayList<>();
    private static Workbook workbook = null;
    public static XSSFCellStyle yellowStyle = null;
    public static XSSFCellStyle stylePadraoCenter = null;
    public static XSSFCellStyle redStyle = null;
    public static XSSFCellStyle blueStyleCenter = null;
    public static XSSFCellStyle blueStyle = null;
    public static XSSFCellStyle primaryStyle = null;
    public static XSSFCellStyle amberStyle = null;
    public static XSSFCellStyle successStyle = null;
    public static XSSFCellStyle blackStyle = null;
    public static XSSFCellStyle blackCenterStyle = null;
    public static Font blackStyleFont = null;

    // Initializing employees data to insert into the excel file
    public ExportUtils() {

        workbook = new XSSFWorkbook();

        yellowStyle = (XSSFCellStyle) workbook.createCellStyle();
        stylePadraoCenter = (XSSFCellStyle) workbook.createCellStyle();
        redStyle = (XSSFCellStyle) workbook.createCellStyle();
        blueStyleCenter = (XSSFCellStyle) workbook.createCellStyle();
        blueStyle = (XSSFCellStyle) workbook.createCellStyle();
        primaryStyle = (XSSFCellStyle) workbook.createCellStyle();
        amberStyle = (XSSFCellStyle) workbook.createCellStyle();
        successStyle = (XSSFCellStyle) workbook.createCellStyle();
        blackStyle = (XSSFCellStyle) workbook.createCellStyle();
        blackCenterStyle = (XSSFCellStyle) workbook.createCellStyle();

        blackStyleFont = workbook.createFont();

        yellowStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        yellowStyle.setAlignment(HorizontalAlignment.CENTER);

        stylePadraoCenter.setAlignment(HorizontalAlignment.CENTER);

        redStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        redStyle.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        redStyle.setAlignment(HorizontalAlignment.CENTER);

        blueStyleCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyleCenter.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
        blueStyleCenter.setAlignment(HorizontalAlignment.CENTER);


        blueStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());


        primaryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        primaryStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());

        amberStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        amberStyle.setAlignment(HorizontalAlignment.CENTER);
        amberStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

        successStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        successStyle.setAlignment(HorizontalAlignment.CENTER);
        successStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());

        blackStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blackStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());

        blackStyleFont.setColor(IndexedColors.BRIGHT_GREEN.getIndex());
        blackStyle.setFont(blackStyleFont);

        blackCenterStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blackCenterStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        blackCenterStyle.setAlignment(HorizontalAlignment.CENTER);
        blackCenterStyle.setFont(blackStyleFont);

    }


    public ExcelExportMode excel(String pathFile) {
        return new ExcelExportMode(pathFile);
    }

    public class ExcelExportMode {

        private String path = null;

        // Create a Workbook
        // new HSSFWorkbook() for generating `.xls` file

        public ExcelExportMode(String path) {
            this.path = path;
        }

        public <T> ExcelExportMode addSheet(String sheetName, Class classType, List<T> dadosToExport) throws IllegalAccessException {
            //new ExportUtils();
            new ExcelSheet<T>(workbook, sheetName, classType, dadosToExport);
            return this;
        }

        public ExcelExportMode addSheet(String sheetName, Consumer<ExcelSheet> func) throws IllegalAccessException {
            //new ExportUtils();
            ExcelSheet newSheet = new ExcelSheet();
            newSheet.compose(workbook, sheetName);
            func.accept(newSheet);
            return this;
        }

        public void export() throws IOException {
            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(path);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        }

        public Workbook getWorkbook() {
            return workbook;
        }
    }

    public class ExcelSheet<T> {

        // Create a Sheet
        public Sheet sheet;
        public CreationHelper createHelper;
        public Workbook workbook;

        private CellStyle generalCellStyle;
        private CellStyle dateCellStyle;
        private CellStyle dateTimeCellStyle;
        private CellStyle timeCellStyle;
        private CellStyle doubleCellStyle;
        private CellStyle intCellStyle;
        private CellStyle textCellStyle;

        public ExcelSheet() {

        }

        public ExcelSheet(Workbook workbook, String sheetName, Class classType, List<T> dadosToExport) throws IllegalAccessException {

            compose(workbook, sheetName);

            loadColunasClasse(classType);
            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Create a Row
            Row headerRow = sheet.createRow(0);
            // Create cells
            for (int i = 0; i < columnsClass.size(); i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columnsClass.get(i).getName().toUpperCase());
                cell.setCellStyle(headerCellStyle);
            }

            // Create Other rows and cells with employees data
            int rowNum = 1;
            for (T classExport : dadosToExport) {
                Row row = sheet.createRow(rowNum++);

                int columnIndex = 0;
                for (Field column : columnsClass) {
                    column.setAccessible(true);

                    String columnValue = String.valueOf(column.get(classExport));
                    if (column.getType().getName().contains("Property")) {
                        columnValue = String.valueOf(((Property) column.get(classExport)).getValue());
                    }
                    String typeName = column.getGenericType().getTypeName().toLowerCase();
                    cellFormatValue(row, columnIndex, columnValue, typeName);

                    column.setAccessible(false);
                    columnIndex++;
                }

            }

            andThen();
        }

        public void loadColunasClasse(Class classeModelo) {
            columnsClass.clear();
            Field[] atributosClass = classeModelo.getDeclaredFields();
            for (Field field : atributosClass) {
                if (!field.isAnnotationPresent(HideExportExcel.class))
                    columnsClass.add(field);
            }
        }

        public void drawRow(CellStyle style, T object, int rowNum) {
            Row row = sheet.createRow(rowNum);

            int columnIndex = 0;
            for (Field column : columnsClass) {
                column.setAccessible(true);

                try {
                    String columnValue = String.valueOf(column.get(object));
                    if (column.getType().getName().contains("Property")) {
                        columnValue = String.valueOf(((Property) column.get(object)).getValue());
                    }
                    String typeName = column.getGenericType().getTypeName().toLowerCase();
                    cellFormatValue(row, columnIndex, columnValue, typeName);
                    row.getCell(columnIndex).setCellStyle(style);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                column.setAccessible(false);
                columnIndex++;
            }

        }

        public void createHeader(Workbook workbook, int index, boolean withBorder) {
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            if (withBorder) {
                headerCellStyle.setBorderTop(BorderStyle.THIN);
                headerCellStyle.setBorderRight(BorderStyle.THIN);
                headerCellStyle.setBorderLeft(BorderStyle.THIN);
                headerCellStyle.setBorderBottom(BorderStyle.THIN);
            }
            // Create a Row
            Row headerRow = sheet.createRow(index);
            // Create cells
            for (int i = 0; i < columnsClass.size(); i++) {
                Cell cell = headerRow.createCell(i);
                if (columnsClass.get(i).isAnnotationPresent(ExportExcel.class)) {
                    cell.setCellValue(columnsClass.get(i).getAnnotation(ExportExcel.class).nameToShow().toUpperCase());
                } else {
                    cell.setCellValue(columnsClass.get(i).getName().toUpperCase());
                }
                cell.setCellStyle(headerCellStyle);
            }
        }

        public Cell cellFormatValue(Row row, int columnIndex, String columnValue, String typeName) {

            Cell cell = row.createCell(columnIndex);
            if (!columnValue.equals("null")) {
                if (typeName.contains("integer")) {
                    cell.setCellStyle(intCellStyle);
                    cell.setCellValue(Double.parseDouble(columnValue));
                } else if (typeName.contains("double")) {
                    cell.setCellStyle(doubleCellStyle);
                    cell.setCellValue(Double.parseDouble(columnValue));
                } else if (typeName.contains("bigdecimal")) {
                    cell.setCellStyle(doubleCellStyle);
                    cell.setCellValue(Double.parseDouble(columnValue));
                } else if (typeName.contains("localdatetime")) {
                    LocalDateTime localDateConvert = LocalDateTime.parse(columnValue, DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                    cell.setCellStyle(dateTimeCellStyle);
                    cell.setCellValue(Date.from(localDateConvert.atZone(ZoneId.systemDefault()).toInstant()));
                } else if (typeName.contains("localdate")) {
                    LocalDate localDateConvert = LocalDate.parse(columnValue, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                    cell.setCellStyle(dateCellStyle);
                    cell.setCellValue(Date.from(localDateConvert.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } else if (typeName.contains("localtime")) {
                    LocalTime localDateConvert = LocalTime.parse(columnValue, DateTimeFormatter.ofPattern("hh:mm:ss"));
                    cell.setCellStyle(timeCellStyle);
                    cell.setCellValue(Date.from(localDateConvert.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant()));
                } else if (typeName.contains("boolean")) {
                    cell.setCellStyle(textCellStyle);
                    cell.setCellValue(columnValue);
                } else {
                    cell.setCellStyle(generalCellStyle);
                    cell.setCellValue(columnValue);
                }
            } else {
                cell.setCellStyle(generalCellStyle);
                cell.setCellValue("");
            }

            return cell;
        }

        public void compose(Workbook workbook, String sheetName) {

            // Create a Sheet
            sheet = workbook.createSheet(sheetName);
            createHelper = workbook.getCreationHelper();
            this.workbook = workbook;

            // Create Cell Style for formatting Date
            generalCellStyle = workbook.createCellStyle();
            generalCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("General"));
            // Create Cell Style for formatting Date
            dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
            // Create Cell Style for formatting DateTime
            dateTimeCellStyle = workbook.createCellStyle();
            dateTimeCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss"));
            // Create Cell Style for formatting Time
            timeCellStyle = workbook.createCellStyle();
            timeCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
            // Create Cell Style for formatting double
            doubleCellStyle = workbook.createCellStyle();
            doubleCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#.##0,00"));
            // Create Cell Style for formatting int
            intCellStyle = workbook.createCellStyle();
            intCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("0"));
            // Create Cell Style for formatting bool
            textCellStyle = workbook.createCellStyle();
            textCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("@"));
        }

        public void andThen() {
            // Resize all columns to fit the content size
            for (int i = 0; i < columnsClass.size(); i++) {
                sheet.autoSizeColumn(i);
            }
        }

    }

    private void loadColunasClasse(Class classeModelo) {
        Field[] atributosClass = classeModelo.getDeclaredFields();
        for (Field field : atributosClass) {
            columnsClass.add(field);
        }
    }

    public static CellStyle setCellStyleSD(String text, ExcelSheet sheet) {

        XSSFCellStyle style = (XSSFCellStyle) sheet.workbook.createCellStyle();
        Font fontStyle = sheet.workbook.createFont();
        fontStyle.setColor(IndexedColors.BLACK.getIndex());
        fontStyle.setBold(true);

        switch (text) {

            case "IN":
                style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
                fontStyle.setColor(IndexedColors.WHITE.getIndex());
                break;

            case "G1":
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                break;

            case "G2":
                style.setFillForegroundColor(IndexedColors.BROWN.getIndex());
                fontStyle.setColor(IndexedColors.WHITE.getIndex());
                break;

            case "G3":
                style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                break;

            case "G4":
                style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
                fontStyle.setColor(IndexedColors.BLACK.getIndex());
                break;

            case "G5":
                style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
                break;

            case "F":
                style.setFillForegroundColor(IndexedColors.PINK.getIndex());
                break;

            case "D":
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                break;

            case "S":
                style.setFillForegroundColor(IndexedColors.VIOLET.getIndex());
                break;

            case "P":
                style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
                break;

            case "E":
                style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
                break;

            case "1":
                style.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
                break;

            case "2":
                style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
                break;

            case "3":
                style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                break;

            case "4":
                style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
                break;

            case "5":
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                break;

            default:
                style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
                fontStyle.setColor(IndexedColors.BRIGHT_GREEN.getIndex());
                break;
        }
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(fontStyle);
        return style;
    }



}
