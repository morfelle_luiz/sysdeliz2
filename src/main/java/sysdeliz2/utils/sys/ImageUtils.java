package sysdeliz2.utils.sys;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.batik.ext.awt.image.codec.SeekableStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImageUtils {

    public enum FitType {HEIGHT, WIDTH}

    public enum IconSize {
        _16(16), _24(24), _32(32), _48(48), _64(64), _80(80), _100(100), MANUAL(0);

        public double imageSize;

        IconSize(double value) {
            imageSize = value;
        }

        public IconSize size(double value) {
            imageSize = value;
            return this;
        }

        public double getValue() {
            return imageSize;
        }
    }

    public enum Icon {
        HOME("home"),
        LOJA_VIRTUAL("loja virtual"),
        EXPANDIR("expandir"),
        RECOLHER("recolher"),
        CLIENTE("cliente"),
        CIELO("cielo"),
        GETNET("getnet"),
        CVV("cvv"),
        VISA_CARD("visa card"),
        MASTER_CARD("master card"),
        HIPER_CARD("hiper card"),
        AMEX_CARD("amex card"),
        ELO_CARD("elo card"),
        LOJISTA("lojista"),
        PEDIDO("pedido"),
        RECEBER_CARTAO("recebe cartao"),
        CARTAO_INVALIDO("invalido card"),
        COPIAR("copiar"),
        FATURAR_PEDIDO("faturar pedido"),
        CANCELAR_PEDIDO("cancelar pedido"),
        ADICIONAR_PEDIDO("adicionar pedido"),
        ADICIONAR_CALENDARIO("adicionar calendario"),
        RECARREGAR_PEDIDO("recarregar pedido"),
        VISUALIZAR_PEDIDO("visualizar pedido"),
        ATENCAO_PEDIDO("atencao pedido"),
        CARREGANDO_PEDIDO("carregando pedido"),
        MANUTENCAO_PEDIDO("manutencao pedido"),
        MARKETING_PEDIDO("marketing pedido"),
        ENTRADA_PRODUTO("entrada produto"),
        SAIDA_PRODUTO("saida produto"),
        GESTAO_PEDIDO("gestao pedido"),
        ERP("erp"),
        TODOS("todos"),
        PROXIMO("proximo"),
        LIMPAR("limpar"),
        LAVANDERIA("lavanderia"),
        ANTERIOR("anterior"),
        CADASTROS("cadastros"),
        INCLUIR_CARRINHO("incluir carrinho"),
        PRODUTO("produto"),
        ROTEIRO_PRODUTO("roteiro produto"),
        CALENDARIO_PRODUTO("calendario produto"),
        SHOPFLOOR("shopfloor"),
        CELULA("celula"),
        COLABORADOR("colaborador"),
        FUNCOES_COLABORADOR("funcoes colaborador"),
        POLIVALENCIA("polivalencia"),
        TURNO("turno"),
        MODA("moda"),
        PARADA("parada"),
        GRUPO_OPERACOES("grupo operacoes"),
        OPERACOES("operacoes"),
        RECURSO_PRODUCAO("recurso producao"),
        MARKETING("marketing"),
        REGIAO("regiao"),
        LOCAL("local"),
        REPRESENTANTE("representante"),
        MARCA("marca"),
        REDES_SOCIAIS("redes sociais"),
        ENTIDADE("entidade"),
        LED_VERMELHO("led vermelho"),
        LED_VERDE("led verde"),
        SALVAR("salvar"),
        EXPEDICAO("expedicao"),
        LEITURA_BARRA("leitura barra"),
        COLETA("coleta"),
        MARCAR_COLABORADOR("marcar colaborador"),
        RESERVA_PEDIDO("reserva pedido"),
        IMPRIMIR_TAG("imprimir tag"),
        COMPRAS("compras"),
        NUMPAD("numpad"),
        ATUALIZAR("atualizar"),
        ATUALIZAR_CARRINHO("atualizar carrinho"),
        AVISAR("avisar"),
        AGRUPAR("agrupar"),
        QRCODE("qrcode"),
        EXCEL("excel"),
        ADICIONAR("adicionar"),
        CONTROLE_PRODUCAO("controle producao"),
        FATURAMENTO("faturamento"),
        LAST_YEAR("last year"),
        MIX_PRODUTOS("mix"),
        FILTRO("filtro"),
        GESTAO_ORDEM_COMPRA("gestao ordem compra"),
        ORDEM_COMPRA("ordem compra"),
        PCP("pcp"),
        GESTAO_PRODUCAO("gestao producao"),
        PORCENTO_PRODUTO("porcento produto"),
        INDICE_PROJECAO("indice projecao"),
        PRIORIZAR_PRODUTO("priorizar produto"),
        ETIQUETA_PRODUTO("etiqueta produto"),
        PROGRAMAR_PRODUCAO("programar producao"),
        AGENDA_ENTREGA("agenda entrega"),
        COMERCIAL("comercial"),
        META("meta"),
        TAG_TIPO("tag tipo"),
        BID("bid"),
        TECIDO("tecido"),
        TROCAR("trocar"),
        ALTERAR_STATUS("alterar status"),
        STATUS_VERMELHO("status_vermelho"),
        STATUS_PRETO("status_preto"),
        STATUS_VERDE("status_verde"),
        STATUS_AMARELO("status_amarelo"),
        STATUS_AZUL("status_azul"),
        INFORMACAO("informacao"),
        ABRIR("abrir"),
        PREMIO("bid premiado"),
        FINANCEIRO("financeiro"),
        BOLETO("boleto"),
        ENVIAR("enviar"),
        ADD_QTDE_MATERIAL("adicionar qtde material"),
        PAGTO_CARTAO("pagto cartao"),
        PAGAMENTO_CARTAO("pagamento cartao"),
        ADMINISTRATIVO("administrativo"),
        GESTAO_ENTREGA("gestao entrega"),
        ADICIONAR_ENTREGA("adicionar entrega"),
        GESTAO_ENTRADA("gestao entrada"),
        ADICIONAR_ENTRADA("adicionar entrada"),
        DADOS_COLABORADOR("dados colaborador"),
        RELATORIOS("relatorios"),
        CMR("crm"),
        ANALISE("analise"),
        DASHBOARD("dashboard"),
        COR("cor"),
        STATUS_SIM("active"),
        ALERT_ERROR("alert error"),
        ALERT_INFO("alert info"),
        ALERT_SUCCESS("alert success"),
        ALERT_WARNING("alert warning"),
        STATUS_NAO("inactive"),
        LARGURA("largura"),
        ALTURA("altura"),
        CATALOGO("catalogo"),
        EDITAR_IMAGEM("editar imagem"),
        PIN("pin"),
        MATERIAIS("materiais"),
        ENFESTO("enfesto"),
        ENCAIXE("encaixe"),
        ENFESTO_CORTE("enfesto corte"),
        SETOR("setor"),
        BAIXA_MATERIAL("baixa material"),
        BAIXAR_MATERIAL("baixar material"),
        DEPOSITO("deposito"),
        SUBSTITUIR("substituir"),
        COMISSAO("comissao"),
        EXECUTAR("executar"),
        GESTAO_MATERIAIS("gestao materiais"),
        GRUPO("grupo"),
        SUBGRUPO("subgrupo"),
        FRAGMENT("fragment"),
        CONDICAO_PAGAMENTO("condicao pagamento"),
        DESCONTO("desconto"),
        CANCEL("cancel"),
        TRASH("trash"),
        PRODUTO_FALTANTE("produto faltante"),
        EDIT("edit"),
        SEARCH("search"),
        CONFIRMAR("confirmar"),
        REGRAS_COMERCIAIS("regras comerciais"),
        IMPORT_DATABASE("import database"),
        ENVIAR_NUVEM("enviar nuvem"),
        MATERIAL("material"),
        IMPRIMIR("imprimir"),
        BLOQUEAR("bloquear"),
        BAIXAR("baixar"),
        INTERMITATE("intermitate"),
        ALMOXARIFADO("almoxarifado"),
        CONSUMO_SEM("consumo sem"),
        CONSUMO_BAIXADO("consumo baixado"),
        CONSUMO_ABERTO("consumo nao baixado"),
        CONSUMO_PARCIAL("consumo baixa parcial"),
        PROGRAMAR_COLETA("programar coleta"),
        MAGENTO("magento"),
        NOTA_FISCAL("nota fiscal"),
        LOGO_DLZ("logo dlz"),
        LOGO_FDL("logo fdl"),
        PDF("pdf"),
        CIDADE("cidade"),
        CAIXA_ABERTA("caixa aberta"),
        CAIXA_ABERTA2("caixa aberta2"),
        EXCLUIR_CAIXA("excluir caixa"),
        CAIXA("caixa"),
        CAIXA2("caixa2"),
        CAIXA_INCOMPLETA("caixa_incompleta"),
        CAIXA_VAZIA("caixa vazia"),
        FECHAR_CAIXA("fechar caixa"),
        PRODUTO_FAVORITO("produto favorito"),
        FAVORITO("estrela"),
        N_FAVORITO("estrela apagada"),
        PLANETA("globe"),
        EXPORTACAO("aviao"),
        ANEXO("anexo"),
        ORDEM_LIDA("ordem_lida"),
        RETORNAR("retornar"),
        TRANSPORTADORA("transportadora"),
        DESLIGAR("desligar"),
        NATUREZA("natureza"),
        USER("user"),
        LOCK("lock"),
        MALETA("maleta"),
        CALENDARIO("calendario"),
        AJUSTES("ajustes"),
        RODAR_ACAO("rodar acao"),
        CIMA("cima"),
        BAIXO("baixo"),
        LOGO_DELIZ_FASHION_GROUP("logo deliz group"),
        CALCULADORA("calculator");
        public String imageSize;

        Icon(String value) {
            imageSize = value;
        }

        public String getValue() {
            return imageSize;
        }
    }

    public enum ButtonIcon {
        SELECT("select"),
        FIND("find"),
        SEND("send"),
        SUBSTITUIR("substituir"),
        NEW_REGISTER("new register"),
        NEGATIVE("negative"),
        DELETE_REGISTER("delete register"),
        ACTIVE("active"),
        INACTIVE("inactive"),
        COLETA("coleta"),
        MAGENTO("magento");

        public String imageSize;

        ButtonIcon(String value) {
            imageSize = value;
        }

        public String getValue() {
            return imageSize;
        }
    }

    public static ImageView getIcon(Icon icon, IconSize size) {
        Image image = new Image(ImageUtils.class.getResource("/images/icons/" + icon.getValue() + (size.getValue() < 50 ? "_50" : "_100") + ".png").toExternalForm());
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(size.getValue());
        imageView.setFitWidth(size.getValue());

        return imageView;
    }

    public static ImageView getIcon(Icon icon, Double customSize) {
        Image image = new Image(ImageUtils.class.getResource("/images/icons/" + icon.getValue() + "_100.png").toExternalForm());
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(customSize);
        imageView.setFitWidth(customSize);

        return imageView;
    }

    public static Image getImage(Icon icon) {
        Image image = new Image(ImageUtils.class.getResource("/images/icons/" + icon.getValue() + "_100.png").toExternalForm());

        return image;
    }

    public static Image getImage(String path) {
        Image image = new Image(path);
        return image;
    }

    public static ImageView getIconButton(ButtonIcon iconButton, IconSize size) {
        Image image = new Image(ImageUtils.class.getResource("/images/icons/buttons/" + iconButton.getValue() + ".png").toExternalForm());
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(size.getValue());
        imageView.setFitWidth(size.getValue());

        return imageView;
    }

    public static Image getImageButton(ButtonIcon iconButton) {
        Image image = new Image(ImageUtils.class.getResource("/images/icons/buttons/" + iconButton.getValue() + ".png").toExternalForm());

        return image;
    }

    public static ImageView getImage(String url, Double customSize, FitType fitType) {
        Image image = new Image(ImageUtils.class.getResource("/images/sem_foto.png").toExternalForm());
        try {
            image = new Image(new FileInputStream(url));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            image = new Image(ImageUtils.class.getResource("/images/sem_foto.png").toExternalForm());
        }

        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        if (fitType.equals(FitType.HEIGHT))
            imageView.setFitHeight(customSize);
        else
            imageView.setFitWidth(customSize);
        return imageView;
    }

    public static ImageView getImageView(String url) {
        Image image = new Image(ImageUtils.class.getResource("/images/sem_foto.png").toExternalForm());
        try {
            image = new Image(new FileInputStream(url));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            image = new Image(ImageUtils.class.getResource("/images/sem_foto.png").toExternalForm());
        }

        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        return imageView;
    }

    public static Image noPhoto() {
        File loadNoImage = new File("C:\\SysDelizLocal\\local_files\\no-photo_retrato.jpg");
        try {
            return new Image(new FileInputStream(loadNoImage));
        } catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
            return null;
        }
    }
}
