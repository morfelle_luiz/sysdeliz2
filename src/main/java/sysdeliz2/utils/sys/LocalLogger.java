/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.sys;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cristiano.diego
 */
public class LocalLogger {

    private static final String fileLogError = "C:/SysDelizLocal/local_files/sysdeliz_error.log";
    private static final String fileLogProgramacao = "C:/SysDelizLocal/local_files/sysdeliz_programacao_of.log";
    private static final String fileLogApontamento = "C:/SysDelizLocal/local_files/sysdeliz_apontamento_producao.log";
    
    public static void addLog(String mensagem, String modulo) {
        try {
            LocalLogger.registerLogFile(modulo, mensagem, fileLogError);
        } catch (IOException ex) {
            Logger.getLogger(LocalLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addLogProgramacaoOf(String mensagem, String modulo) {
        try {
            LocalLogger.registerLogFile(modulo, mensagem, fileLogProgramacao);
        } catch (IOException ex) {
            Logger.getLogger(LocalLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addLogApontamento(String mensagem, String modulo) {
        try {
            LocalLogger.registerLogFile(modulo, mensagem, fileLogApontamento);
        } catch (IOException ex) {
            Logger.getLogger(LocalLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void registerLogFile(String modulo, String mensagem, String fileLog) throws IOException {
        BufferedWriter bufferLog = new BufferedWriter(new FileWriter(new File(fileLog), true));
        bufferLog.write(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "|" + modulo + "|" + mensagem);
        bufferLog.newLine();
        bufferLog.close();
    }
}
