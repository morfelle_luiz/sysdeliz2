package sysdeliz2.utils.sys;

import sysdeliz2.controllers.fxml.SceneLoginController;

import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LoginImageScanner {
    
    public List<String> getResourceFiles() throws IOException {
        List<String> filenames = new ArrayList<>();
        
        CodeSource src = SceneLoginController.class.getProtectionDomain().getCodeSource();
        if (src != null) {
            URL jar = src.getLocation();
            ZipInputStream zip = null;
            zip = new ZipInputStream(jar.openStream());
            while (true) {
                ZipEntry e = null;
                e = zip.getNextEntry();
                if (e == null)
                    break;
                String name = e.getName();
                if (name.startsWith("images/login/") && (name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".png") || name.toLowerCase().endsWith(".jpeg"))) {
                    filenames.add(name);
                }
            }
        }
        
        return filenames;
    }
}
