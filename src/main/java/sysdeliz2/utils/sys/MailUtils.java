/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysdeliz2.utils.sys;

import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.models.ClienteFaturaAReceber;
import sysdeliz2.models.FaturaAReceber;
import sysdeliz2.models.ReceberCartao;
import sysdeliz2.models.properties.GestaoCompra00X;
import sysdeliz2.utils.ContentIdGenerator;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author cristiano.diego
 */
public class MailUtils {

    private static final String host_mail = "serverexchange.nowar.corp";
    private static final String smtp_port = "587";
    private static final String from_mail = "relatorios@deliz.com.br";
    private static final String pass_mail = "d25m04";
    private static final String from_mail_s = "servicos@deliz.com.br";
    private static final String pass_mail_s = "d25m04";

    public enum AttachMail {
        FULL_BID("c:/SysDelizLocal/local_files/BidCompleto.pdf"),
        BID("c:/SysDelizLocal/local_files/Bid.pdf");

        public String attachFile;

        AttachMail(String value) {
            attachFile = value;
        }

        public String getValor() {
            return attachFile;
        }
    }

    public static void sendMailBid(AttachMail attach_file, String recept_mail, String body_mail, String represent) {
        String[] to = recept_mail.split(",");
        String subject = "BID - " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                + " " + represent;
        String filename = attach_file.getValor();

        // Set smtp properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail, pass_mail);
            }
        });

        try {

            List<Address> toMails = new ArrayList<>();
            if (Globals.getEmpresaLogada().getCodigo().equals("1000")) {
                toMails.add(new InternetAddress("ricardo@deliz.com.br"));
                toMails.add(new InternetAddress("luis@deliz.com.br"));
            }
            for (String mail : to) {
                toMails.add(new InternetAddress(mail));
            }
            Address[] mails = toMails.toArray(new InternetAddress[0]);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from_mail, "Relatórios SysDeliz"));
            message.setRecipients(Message.RecipientType.TO, mails);
            if (Globals.getEmpresaLogada().getCodigo().equals("1000"))
                message.setRecipients(Message.RecipientType.BCC, new Address[]{new InternetAddress("diego@deliz.com.br")});
            message.setSubject(subject);
            message.setSentDate(new Date());

            String cid = ContentIdGenerator.getContentId();
            String cid2 = ContentIdGenerator.getContentId();
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setText("<html><head>"
                            + "<title>BID - " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + represent + "</title>"
                            + "<meta charset=\"utf-8\">"
                            + "</head>\n"
                            + "<body><div>" + body_mail + "</div>"
                            + "<div><img src=\"cid:" + cid + "\" /><img src=\"cid:" + cid2 + "\" /></div>\n"
                            + "</body></html>",
                    "UTF-8", "html");
            MimeBodyPart attachmentPart = new MimeBodyPart();
            FileDataSource fileDataSource = new FileDataSource(filename) {
                @Override
                public String getContentType() {
                    return "application/octet-stream";
                }
            };
            MimeBodyPart imagePart = new MimeBodyPart();
            imagePart.attachFile("c:/SysDelizLocal/printBid.png");
            imagePart.setContentID("<" + cid + ">");
            imagePart.setDisposition(MimeBodyPart.INLINE);

            MimeBodyPart image2Part = new MimeBodyPart();
            image2Part.attachFile("c:/SysDelizLocal/printBidPremiado.png");
            image2Part.setContentID("<" + cid2 + ">");
            image2Part.setDisposition(MimeBodyPart.INLINE);

            attachmentPart.setDataHandler(new DataHandler(fileDataSource));
            attachmentPart.setFileName(fileDataSource.getName());
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(attachmentPart);
            multipart.addBodyPart(imagePart);
            multipart.addBodyPart(image2Part);
            message.setContent(multipart);
            Transport.send(message);

        } catch (MessagingException e) {

            e.printStackTrace();
            GUIUtils.showException(e);

        } catch (IOException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    public static void sendMailCompPag(String recept_mail, ReceberCartao billings) {
        String[] to = recept_mail.split(",");
        String subject = "Comprovante de pagamento. Pedido: " + billings.getPedido();

        // Set smtp properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail_s, pass_mail_s);
            }
        });

        try {

            List<Address> toMails = new ArrayList<>();
            for (String mail : to) {
                toMails.add(new InternetAddress(mail));
            }
            Address[] mails = toMails.toArray(new InternetAddress[0]);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from_mail_s, "Serviços SysDeliz"));
            message.setRecipients(Message.RecipientType.TO, mails);

            Address[] mailsCCO = new Address[]{new InternetAddress(Globals.getUsuarioLogado().getEmail().replace("mail: ", ""))};
            message.setRecipients(Message.RecipientType.BCC, mailsCCO);
            message.setSubject(subject);
            message.setSentDate(new Date());

            String cid = ContentIdGenerator.getContentId();
            MimeBodyPart messagePart = new MimeBodyPart();
            //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
            NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
            formatNumberValue.setMaximumFractionDigits(2);
            DateFormat formatDateValue = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
            String body_content
                    = "<!DOCTYPE html>"
                    + "<html lang=\"pt-br\">"
                    + "	<head>"
                    + "		<title>Comprovante de pagamento. Pedido: " + billings.getPedido() + "</title>"
                    + "		<meta charset=\"utf-8\">"
                    + "		<style type=\"text/css\">"
                    + "			body {"
                    + "				color: #444;"
                    + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                    + "			}"
                    + "			table {"
                    + "				background: #f5f5f5;"
                    + "				border-collapse: separate;"
                    + "				box-shadow: inset 0 1px 0 #fff;"
                    + "				font-size: 12px;"
                    + "				line-height: 14px;"
                    + "				text-align: left;"
                    + "			}"
                    + "			th {"
                    + "				background-color: #777;"
                    + "				border-left: 1px solid #555;"
                    + "				border-right: 1px solid #777;"
                    + "				border-top: 1px solid #555;"
                    + "				border-bottom: 1px solid #333;"
                    + "				box-shadow: inset 0 1px 0 #999;"
                    + "				color: #fff;"
                    + "			  	font-weight: bold;"
                    + "				padding: 5px 10px;"
                    + "				position: relative;"
                    + "				text-shadow: 0 1px 0 #000;	"
                    + "			}"
                    + "			td {"
                    + "				border-right: 1px solid #fff;"
                    + "				border-left: 1px solid #e8e8e8;"
                    + "				border-top: 1px solid #fff;"
                    + "				border-bottom: 1px solid #e8e8e8;"
                    + "				padding: 5px 10px;"
                    + "				position: relative;"
                    + "				transition: all 300ms;"
                    + "				text-transform: uppercase;"
                    + "			}"
                    + "			span {"
                    + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                    + "			}"
                    + "		</style>"
                    + "	</head>"
                    + "	<body>"
                    + "		<h3>Prezado cliente,</h3>"
                    + "		Informamos que foi efetuado o pagamento do seu pedido com um cartão de crédito: "
                    + "		<br/>"
                    + "		<table>"
                    + "			<thead>"
                    + "				<tr>"
                    + "					<th>TITULAR</th>"
                    + "					<th>DATA</th>"
                    + "					<th>PEDIDO</th>"
                    + "					<th>VALOR</th>"
                    + "					<th>PARCELA</th>"
                    + "					<th>CARTÃO</th>"
                    + "					<th>BANDEIRA</th>"
                    + "				</tr>"
                    + "			</thead>"
                    + "			<tbody>"
                    + "				<tr>"
                    + "					<td>" + billings.getNome() + "</td>"
                    + "					<td>" + billings.getData_receber() + "</td>"
                    + "					<td>" + billings.getPedido() + "</td>"
                    + "					<td>" + formatNumberValue.format(billings.getValor()) + "</td>"
                    + "					<td>" + billings.getParcelas() + "</td>"
                    + "					<td>" + billings.getCard_number() + "</td>"
                    + "					<td>" + billings.getBrand().toUpperCase() + "</td>"
                    + "				</tr>"
                    + "			</tbody>"
                    + "		</table>"
                    + "		<br/>"
                    + "         <div><img src=\"cid:" + cid + "\" /></div>\n"
                    + "		Em caso de contestação deste pagamento, entre em contato com o setor comercial e informe o seu TITULAR e o número do pedido "
                    + "		pelos telefones: <b>48.3641-1922</b> / (Whatsapp) <b>47.99123-1578</b><br/>"
                    + "		<br/>"
                    + "		Para qualquer informação a Deliz está à disposição nos seguintes contatos:<br/>"
                    + "		<span>"
                    + "			<b>Departamento Comercial</b><br/>"
                    + "			<b>Telefone Fixo:</b> (048) 3641-1922 | (048) 3641-1900<br/>"
                    + "			<b>Whatsapp:</b> (047) 9 9123-1578<br/>"
                    + "			<b>Email:</b> gerson@deliz.com.br | comercial@deliz.com.br<br/>"
                    + "			<b>Pessoa para Contato:</b> Gerson.<br/>"
                    + "		</span>"
                    + "	</body>"
                    + "</html>";
            //</editor-fold>
            messagePart.setText(""
                            + body_content,
                    "UTF-8", "html");

            MimeBodyPart imagePart = new MimeBodyPart();
            imagePart.attachFile("c:/SysDelizLocal/local_files/comp_pag.png");
            imagePart.setContentID("<" + cid + ">");
            imagePart.setDisposition(MimeBodyPart.INLINE);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(imagePart);
            message.setContent(multipart);
            Transport.send(message);

        } catch (MessagingException e) {

            e.printStackTrace();
            GUIUtils.showException(e);

        } catch (IOException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    public static void sendMailErroPersistencia(String authorization_code, String payment_id, String terminal_nsu,
                                                String acquirer_transaction_id, String verification_id, String status,
                                                String number_token, String access_token) {
        String subject = "Erro na persistencia do pagamento do cartão";

        // Set smtp properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail_s, pass_mail_s);
            }
        });

        try {

            List<Address> toMails = new ArrayList<>();
            toMails.add(new InternetAddress("diego@deliz.com.br"));
            Address[] mails = toMails.toArray(new InternetAddress[0]);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from_mail_s, "Serviços SysDeliz"));
            message.setRecipients(Message.RecipientType.TO, mails);

            Address[] mailsCCO = new Address[]{new InternetAddress(Globals.getUsuarioLogado().getEmail().replace("mail: ", ""))};
            message.setRecipients(Message.RecipientType.BCC, mailsCCO);
            message.setSubject(subject);
            message.setSentDate(new Date());

            String cid = ContentIdGenerator.getContentId();
            MimeBodyPart messagePart = new MimeBodyPart();
            //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
            NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
            formatNumberValue.setMaximumFractionDigits(2);
            DateFormat formatDateValue = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
            String body_content
                    = "<!DOCTYPE html>"
                    + "<html lang=\"pt-br\">"
                    + "	<head>"
                    + "		<title>ERROR</title>"
                    + "		<meta charset=\"utf-8\">"
                    + "	</head>"
                    + "	<body>"
                    + "Erro ao registrar pagamento no banco de dados.<br/>"
                    + "Authorization CODE: " + authorization_code + "<br/>"
                    + "Payment ID: " + payment_id + "<br/>"
                    + "Terminal NSU: " + terminal_nsu + "<br/>"
                    + "Acquirer Transaction ID: " + acquirer_transaction_id + "<br/>"
                    + "Verification ID: " + (verification_id == null ? "CARTAO NAO VERIFICADO" : verification_id) + "<br/>"
                    + "Return Payment STATUS: " + status + "<br/>"
                    + "Card Number TOKEN: " + number_token + "<br/>"
                    + "OAuth Access TOKEN: " + access_token + "<br/>"
                    + "Encaminhe as informações para o TI."
                    + "		<br/>"
                    + "		<br/>"
                    + "         <div><img src=\"cid:" + cid + "\" /></div>\n"
                    + "	</body>"
                    + "</html>";
            //</editor-fold>
            messagePart.setText(""
                            + body_content,
                    "UTF-8", "html");

            MimeBodyPart imagePart = new MimeBodyPart();
            imagePart.attachFile("C:/SysDelizLocal/local_files/printPagamentoView.png");
            imagePart.setContentID("<" + cid + ">");
            imagePart.setDisposition(MimeBodyPart.INLINE);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(imagePart);
            message.setContent(multipart);
            Transport.send(message);

        } catch (MessagingException e) {

            e.printStackTrace();
            GUIUtils.showException(e);

        } catch (IOException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    public static void sendMailBoletoAberto(String recept_mail, ClienteFaturaAReceber cliente) throws UnsupportedEncodingException, MessagingException, ParseException {
        String subject = "Deliz - Duplicatas à vencer";
        String[] to_emails = recept_mail.split(";");
        Address[] to_clients = new Address[to_emails.length];
        for (int i = 0; i < to_emails.length; i++) {
            to_clients[i] = new InternetAddress(to_emails[i].trim());
        }

        // Set SMTP properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        //properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail, pass_mail);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from_mail, "Não Responder | Financeiro | Deliz"));
        message.setRecipients(Message.RecipientType.TO, to_clients);
        message.setSubject(subject);
        message.setSentDate(new Date());

        //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
        String body_content
                = "<!DOCTYPE html>"
                + "<html lang=\"pt-br\">"
                + "	<head>"
                + "		<title>Deliz - Duplicatas à vencer</title>"
                + "		<meta charset=\"utf-8\">"
                + "		<style type=\"text/css\">"
                + "			body {"
                + "				color: #444;"
                + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "                     .radius10{\n"
                + "    border-radius:10px;\n"
                + "    -moz-border-radius:10px;\n"
                + "    -webkit-border-radius:10px;\n"
                + "}\n"
                + ".sombra{\n"
                + "    box-shadow: 1px 1px 2px #855;\n"
                + "    -moz-box-shadow: 1px 1px 2px #855;\n"
                + "    -webkit-box-shadow: 1px 1px 2px #855;\n"
                + "}\n"
                + ".erro{\n"
                + "    background: #efcec9;\n"
                + "    border:1px solid #ad3f30;\n"
                + "    color:#943728;\n"
                + "    padding:10px 10px 10px 68px;\n"
                + "    min-height:48px;\n"
                + "}"
                + "			table {"
                + "				background: #f5f5f5;"
                + "				border-collapse: separate;"
                + "				box-shadow: inset 0 1px 0 #fff;"
                + "				font-size: 12px;"
                + "				line-height: 14px;"
                + "				text-align: left;"
                + "			}"
                + "			th {"
                + "				background-color: #777;"
                + "				border-left: 1px solid #555;"
                + "				border-right: 1px solid #777;"
                + "				border-top: 1px solid #555;"
                + "				border-bottom: 1px solid #333;"
                + "				box-shadow: inset 0 1px 0 #999;"
                + "				color: #fff;"
                + "			  	font-weight: bold;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				text-shadow: 0 1px 0 #000;	"
                + "			}"
                + "			td {"
                + "				border-right: 1px solid #fff;"
                + "				border-left: 1px solid #e8e8e8;"
                + "				border-top: 1px solid #fff;"
                + "				border-bottom: 1px solid #e8e8e8;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				transition: all 300ms;"
                + "				text-transform: uppercase;"
                + "			}"
                + "			span {"
                + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "		</style>"
                + "	</head>"
                + "	<body>"
                + "		<h3>Prezado cliente,</h3>"
                + "		Informamos que temos duplicata(s) de sua compra com a DELIZ à vencer: "
                + "		<br/>"
                + "		<table>"
                + "			<thead>"
                + "				<tr>"
                + "					<th>TITULAR</th>"
                + "					<th>CNPJ</th>"
                + "					<th>NOTA FISCAL</th>"
                + "					<th>TIPO DE DOCUMENTO</th>"
                + "					<th>PARCELA</th>"
                + "					<th>VALOR</th>"
                + "					<th>VENCIMENTO</th>"
                + "				</tr>"
                + "			</thead>"
                + "			<tbody>";
        DateFormat formatDateValue = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        formatNumberValue.setMaximumFractionDigits(2);
        for (FaturaAReceber fatura : cliente.faturasProperty().get()) {
            if (fatura.getStatus().equals("A")) {
                body_content = body_content
                        + "				<tr>"
                        + "					<td>" + cliente.getCliente() + "</td>"
                        + "					<td>" + cliente.getCnpj() + "</td>"
                        + "					<td>" + fatura.getFatura() + "</td>"
                        + "					<td>Duplicata</td>"
                        + "					<td>" + fatura.getDuplicata() + "</td>"
                        + "					<td>" + formatNumberValue.format(fatura.getValor_duplicata()) + "</td>"
                        + "					<td><b>" + formatDateValue.format(format.parse(fatura.getDt_vencto())) + "</b></td>"
                        + "				</tr>";
            }
        }
        body_content = body_content
                + "			</tbody>"
                + "		</table>"
                + "		<br/>"
                + "		Informe seu <b>CNPJ</b> e <b>Razão Social</b> caso não tenha recebido o referido título até o momento, ou entre em  contato com o nosso "
                + "		departamento financeiro pelos telefones: <b>48.3641-1900</b> / (Whatsapp) <b>47.99187-0822</b> ou <b>47.99187-1161</b><br/>"
                + "		Favor desconsiderar o presente, caso as duplicatas não pertençam ao seu CNPJ.<br/>"
                + "		<br/>"
                + "		Para qualquer informação a Deliz está à disposição nos seguintes contatos:<br/>"
                + "		<span>"
                + "			<b>Departamento Financeiro | Contas a Receber</b><br/>"
                + "			<b>Telefone Fixo:</b> (048) 3641-1914 | (048) 3641-1913<br/>"
                + "			<b>Whatsapp:</b> (047)  9 9187-1161 | (047) 9 9187-0822<br/>"
                + "			<b>Email:</b> tainara@deliz.com.br | damielle@deliz.com.br<br/>"
                + "			<b>Pessoas para Contato:</b> Tainara e Damielle.<br/>"
                + "		</span>"
                + "	</body>"
                + "</html>";
        //</editor-fold>

        MimeBodyPart messagePart = new MimeBodyPart();
        messagePart.setText(""
                        + body_content,
                "UTF-8", "html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        message.setContent(multipart);
        Transport.send(message);
    }

    public static void sendMailBoletoVencido(String recept_mail, ClienteFaturaAReceber cliente) throws MessagingException, UnsupportedEncodingException, ParseException {
        String subject = "Deliz - Aviso de boletos em aberto";
        String[] to_emails = recept_mail.split(";");
        Address[] to_clients = new Address[to_emails.length];
        for (int i = 0; i < to_emails.length; i++) {
            to_clients[i] = new InternetAddress(to_emails[i].trim());
        }

        // Set SMTP properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        //properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail, pass_mail);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from_mail, "Não Responder | Financeiro | Deliz"));
        message.setRecipients(Message.RecipientType.TO, to_clients);
        message.setSubject(subject);
        message.setSentDate(new Date());

        //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
        String body_content
                = "<!DOCTYPE html>"
                + "<html lang=\"pt-br\">"
                + "	<head>"
                + "		<title>Deliz - Aviso de boletos em aberto</title>"
                + "		<meta charset=\"utf-8\">"
                + "		<style type=\"text/css\">"
                + ".radius10{\n"
                + "    border-radius:10px;\n"
                + "    -moz-border-radius:10px;\n"
                + "    -webkit-border-radius:10px;\n"
                + "}\n"
                + ".sombra{\n"
                + "    box-shadow: 1px 1px 2px #855;\n"
                + "    -moz-box-shadow: 1px 1px 2px #855;\n"
                + "    -webkit-box-shadow: 1px 1px 2px #855;\n"
                + "}\n"
                + ".erro{\n"
                + "    background: #efcec9;\n"
                + "    border:1px solid #ad3f30;\n"
                + "    color:#943728;\n"
                + "    padding:10px 10px 10px 68px;\n"
                + "    min-height:48px;\n"
                + "}"
                + "			body {"
                + "				color: #444;"
                + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "			table {"
                + "				background: #f5f5f5;"
                + "				border-collapse: separate;"
                + "				box-shadow: inset 0 1px 0 #fff;"
                + "				font-size: 12px;"
                + "				line-height: 14px;"
                + "				text-align: left;"
                + "			}"
                + "			th {"
                + "				background-color: #777;"
                + "				border-left: 1px solid #555;"
                + "				border-right: 1px solid #777;"
                + "				border-top: 1px solid #555;"
                + "				border-bottom: 1px solid #333;"
                + "				box-shadow: inset 0 1px 0 #999;"
                + "				color: #fff;"
                + "			  	font-weight: bold;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				text-shadow: 0 1px 0 #000;	"
                + "			}"
                + "			td {"
                + "				border-right: 1px solid #fff;"
                + "				border-left: 1px solid #e8e8e8;"
                + "				border-top: 1px solid #fff;"
                + "				border-bottom: 1px solid #e8e8e8;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				transition: all 300ms;"
                + "				text-transform: uppercase;"
                + "			}"
                + "			span {"
                + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "		</style>"
                + "	</head>"
                + "	<body>"
                + "		<h3>Prezado cliente,</h3>"
                + "		Buscando manter um relacionamento próximo e sólido, a empresa Deliz veio por meio deste informar que o título abaixo encontra-se em aberto: "
                + "		<br/>"
                + "		<table>"
                + "			<thead>"
                + "				<tr>"
                + "					<th>NATUREZA</th>"
                + "					<th>TITULAR</th>"
                + "					<th>CNPJ</th>"
                + "					<th>NOTA FISCAL</th>"
                + "					<th>TIPO DE DOCUMENTO</th>"
                + "					<th>PARCELA</th>"
                + "					<th>VALOR ORIGINAL</th>"
                + "					<th>VENCIMENTO ORIGINAL</th>"
                + "				</tr>"
                + "			</thead>"
                + "			<tbody>";
        DateFormat formatDateValue = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        formatNumberValue.setMaximumFractionDigits(2);
        for (FaturaAReceber fatura : cliente.faturasProperty().get()) {
            if (fatura.getStatus().equals("B")) {
                body_content = body_content
                        + "				<tr>"
                        + "					<td>Venda de Mercadoria</td>"
                        + "					<td>" + cliente.getCliente() + "</td>"
                        + "					<td>" + cliente.getCnpj() + "</td>"
                        + "					<td>" + fatura.getFatura() + "</td>"
                        + "					<td>Duplicata</td>"
                        + "					<td>" + fatura.getDuplicata() + "</td>"
                        + "					<td>" + formatNumberValue.format(fatura.getValor_duplicata()) + "</td>"
                        + "					<td>" + formatDateValue.format(format.parse(fatura.getDt_vencto())) + "</td>"
                        + "				</tr>";
            }
        }
        body_content = body_content
                + "			</tbody>"
                + "		</table>"
                + "		<br/>"
                + "		Comunicamos que a não quitação do título em até 02 dias, implicará no envio a cartório.<br/>"
                + "		Favor desconsiderar o presente, caso o debito já tenha sido liquidado.<br/>"
                + "		<br/>"
                + "		Para qualquer informação a Deliz está à disposição nos seguintes contatos:<br/>"
                + "		<span>"
                + "			<b>Departamento Financeiro | Contas a Receber</b><br/>"
                + "			<b>Telefone Fixo:</b> (048) 3641-1914 | (048) 3641-1913<br/>"
                + "			<b>Whatsapp:</b> (047)  9 9187-1161 | (047) 9 9187-0822<br/>"
                + "			<b>Email:</b> tainara@deliz.com.br | damielle@deliz.com.br<br/>"
                + "			<b>Pessoas para Contato:</b> Tainara e Damielle.<br/>"
                + "		</span>"
                + "	</body>"
                + "</html>";
        //</editor-fold>

        MimeBodyPart messagePart = new MimeBodyPart();
        messagePart.setText(""
                        + body_content,
                "UTF-8", "html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        message.setContent(multipart);
        Transport.send(message);
    }

    public static void sendOcAConfirmar(List<String> to_emails, List<GestaoCompra00X> ordensCompra) throws UnsupportedEncodingException, MessagingException, ParseException {
        String subject = "Deliz - Ordens de Compra para Faturamento";

        //String[] to_emails = recept_mail.split(";");
        Address[] to_clients = new Address[to_emails.size()];
        for (int i = 0; i < to_emails.size(); i++) {
            to_clients[i] = new InternetAddress(to_emails.get(i).trim());
        }

        // Set SMTP properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        //properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail_s, pass_mail_s);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from_mail_s, SceneMainController.getAcesso().getDisplayName()));
        message.setReplyTo(new Address[]{new InternetAddress(
                SceneMainController.getAcesso().getEmail(),
                SceneMainController.getAcesso().getDisplayName())});
        message.setRecipients(Message.RecipientType.TO, to_clients);
        //message.setRecipients(Message.RecipientType.TO, new Address[]{new InternetAddress("diego@deliz.com.br")});
        message.setSubject(subject);
        message.setSentDate(new Date());

        //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
        String body_content
                = "<!DOCTYPE html>"
                + "<html lang=\"pt-br\">"
                + "	<head>"
                + "		<title>Deliz - Duplicatas à vencer</title>"
                + "		<meta charset=\"utf-8\">"
                + "		<style type=\"text/css\">"
                + "			body {"
                + "				color: #444;"
                + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "                     .radius10{\n"
                + "    border-radius:10px;\n"
                + "    -moz-border-radius:10px;\n"
                + "    -webkit-border-radius:10px;\n"
                + "}\n"
                + ".sombra{\n"
                + "    box-shadow: 1px 1px 2px #855;\n"
                + "    -moz-box-shadow: 1px 1px 2px #855;\n"
                + "    -webkit-box-shadow: 1px 1px 2px #855;\n"
                + "}\n"
                + ".erro{\n"
                + "    background: #efcec9;\n"
                + "    border:1px solid #ad3f30;\n"
                + "    color:#943728;\n"
                + "    padding:10px 10px 10px 68px;\n"
                + "    min-height:48px;\n"
                + "}"
                + "			table {"
                + "				background: #f5f5f5;"
                + "				border-collapse: separate;"
                + "				box-shadow: inset 0 1px 0 #fff;"
                + "				font-size: 12px;"
                + "				line-height: 14px;"
                + "				text-align: left;"
                + "			}"
                + "			th {"
                + "				background-color: #777;"
                + "				border-left: 1px solid #555;"
                + "				border-right: 1px solid #777;"
                + "				border-top: 1px solid #555;"
                + "				border-bottom: 1px solid #333;"
                + "				box-shadow: inset 0 1px 0 #999;"
                + "				color: #fff;"
                + "			  	font-weight: bold;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				text-shadow: 0 1px 0 #000;	"
                + "			}"
                + "			td {"
                + "				border-right: 1px solid #fff;"
                + "				border-left: 1px solid #e8e8e8;"
                + "				border-top: 1px solid #fff;"
                + "				border-bottom: 1px solid #e8e8e8;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				transition: all 300ms;"
                + "				text-transform: uppercase;"
                + "			}"
                + "			span {"
                + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "		</style>"
                + "	</head>"
                + "	<body>"
                + "		<h3>Prezado fornecedor,</h3>"
                + "		Necessitamos da sua confirmação das datas de faturamento dos pedidos que seguem abaixo: <br/>"
                + "             Lembrando que precisamos de datas precisas afim de conseguirmos um bom planejamento de produção e por consequência um duradouro trabalho entre as partes. "
                + "             Atrasos consideráveis e o não retorno deste e-mail será relatado ao comprador responsável como item de avaliação do fornecedor."
                + "		<br/>"
                + "		<table>"
                + "			<thead>"
                + "				<tr>"
                + "					<th>PEDIDO</th>"
                + "					<th>O.C. DELIZ</th>"
                + "					<th>MATERIAL</th>"
                + "					<th>COR</th>"
                + "					<th>UNIDADE</th>"
                + "					<th>QTDE</th>"
                + "					<th>VALOR</th>"
                + "					<th>DATA FATURAMENTO</th>"
                + "				</tr>"
                + "			</thead>"
                + "			<tbody>";
        NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        formatNumberValue.setMaximumFractionDigits(2);
        for (GestaoCompra00X ordemCompra : ordensCompra) {
            body_content = body_content
                    + "				<tr>"
                    + "					<td>" + ordemCompra.getPedFornecedor() + "</td>"
                    + "					<td>" + ordemCompra.getNumero() + "</td>"
                    + "					<td>" + ordemCompra.getDescricao() + "</td>"
                    + "					<td>" + ordemCompra.getCor() + "</td>"
                    + "					<td>" + ordemCompra.getUnidade() + "</td>"
                    + "					<td>" + ordemCompra.getQtde() + "</td>"
                    + "					<td>" + formatNumberValue.format(ordemCompra.getValor()) + "</td>"
                    + "					<td><b>" + ordemCompra.getDtFaturaAsString() + "</b></td>"
                    + "				</tr>";
        }
        body_content = body_content
                + "			</tbody>"
                + "		</table>"
                + "		<br/>"
                + "		<br/>"
                + "		Ficamos no aguardo do seu retorno.<br/>"
                + "             Atenciosamente,<br/>"
                + "		<span>"
                + "			<b>" + SceneMainController.getAcesso().getDisplayName() + "</b><br/>"
                + "			<b>Telefone Fixo:</b> (048) 3641-1900<br/>"
                + "			<b>Whatsapp:</b> (047)  9 9187-0830<br/>"
                + "			<b>Email:</b> " + SceneMainController.getAcesso().getEmail() + "<br/>"
                + "			<b>Site:</b> www.deliz.com.br<br/>"
                + "		</span>"
                + "	</body>"
                + "</html>";
        //</editor-fold>

        MimeBodyPart messagePart = new MimeBodyPart();
        messagePart.setText(""
                        + body_content,
                "UTF-8", "html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        message.setContent(multipart);
        Transport.send(message);
    }

    public static void sendOcAtrasada(List<String> to_emails, List<GestaoCompra00X> ordensCompra) throws UnsupportedEncodingException, MessagingException, ParseException {
        String subject = "Deliz - Ordens de Compra em Atraso";

        //String[] to_emails = recept_mail.split(";");
        Address[] to_clients = new Address[to_emails.size()];
        for (int i = 0; i < to_emails.size(); i++) {
            to_clients[i] = new InternetAddress(to_emails.get(i).trim());
        }

        // Set SMTP properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host_mail);
        properties.put("mail.smtp.port", smtp_port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        //properties.put("mail.debug", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from_mail_s, pass_mail_s);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from_mail_s, SceneMainController.getAcesso().getDisplayName()));
        message.setReplyTo(new Address[]{new InternetAddress(
                SceneMainController.getAcesso().getEmail(),
                SceneMainController.getAcesso().getDisplayName())});
        message.setRecipients(Message.RecipientType.TO, to_clients);
        //message.setRecipients(Message.RecipientType.TO, new Address[]{new InternetAddress("diego@deliz.com.br")});
        message.setSubject(subject);
        message.setSentDate(new Date());

        //<editor-fold defaultstate="collapsed" desc="String body_content = ...">
        String body_content
                = "<!DOCTYPE html>"
                + "<html lang=\"pt-br\">"
                + "	<head>"
                + "		<title>Deliz - Duplicatas à vencer</title>"
                + "		<meta charset=\"utf-8\">"
                + "		<style type=\"text/css\">"
                + "			body {"
                + "				color: #444;"
                + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "                     .radius10{\n"
                + "    border-radius:10px;\n"
                + "    -moz-border-radius:10px;\n"
                + "    -webkit-border-radius:10px;\n"
                + "}\n"
                + ".sombra{\n"
                + "    box-shadow: 1px 1px 2px #855;\n"
                + "    -moz-box-shadow: 1px 1px 2px #855;\n"
                + "    -webkit-box-shadow: 1px 1px 2px #855;\n"
                + "}\n"
                + ".erro{\n"
                + "    background: #efcec9;\n"
                + "    border:1px solid #ad3f30;\n"
                + "    color:#943728;\n"
                + "    padding:10px 10px 10px 68px;\n"
                + "    min-height:48px;\n"
                + "}"
                + "			table {"
                + "				background: #f5f5f5;"
                + "				border-collapse: separate;"
                + "				box-shadow: inset 0 1px 0 #fff;"
                + "				font-size: 12px;"
                + "				line-height: 14px;"
                + "				text-align: left;"
                + "			}"
                + "			th {"
                + "				background-color: #777;"
                + "				border-left: 1px solid #555;"
                + "				border-right: 1px solid #777;"
                + "				border-top: 1px solid #555;"
                + "				border-bottom: 1px solid #333;"
                + "				box-shadow: inset 0 1px 0 #999;"
                + "				color: #fff;"
                + "			  	font-weight: bold;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				text-shadow: 0 1px 0 #000;	"
                + "			}"
                + "			td {"
                + "				border-right: 1px solid #fff;"
                + "				border-left: 1px solid #e8e8e8;"
                + "				border-top: 1px solid #fff;"
                + "				border-bottom: 1px solid #e8e8e8;"
                + "				padding: 5px 10px;"
                + "				position: relative;"
                + "				transition: all 300ms;"
                + "				text-transform: uppercase;"
                + "			}"
                + "			span {"
                + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                + "			}"
                + "		</style>"
                + "	</head>"
                + "	<body>"
                + "		<h3>Prezado fornecedor,</h3>"
                + "		Necessitamos da sua confirmação das datas de faturamento dos pedidos que seguem abaixo: <br/>"
                + "             Lembrando que precisamos de datas precisas afim de conseguirmos um bom planejamento de produção e por consequência um duradouro trabalho entre as partes. "
                + "             Atrasos consideráveis e o não retorno deste e-mail será relatado ao comprador responsável como item de avaliação do fornecedor."
                + "		<br/>"
                + "		<br/>"
                + "		<table>"
                + "			<thead>"
                + "				<tr>"
                + "					<th>PEDIDO</th>"
                + "					<th>O.C. DELIZ</th>"
                + "					<th>MATERIAL</th>"
                + "					<th>COR</th>"
                + "					<th>UNIDADE</th>"
                + "					<th>QTDE</th>"
                + "					<th>VALOR</th>"
                + "					<th>DATA FATURAMENTOs</th>"
                + "				</tr>"
                + "			</thead>"
                + "			<tbody>";
        NumberFormat formatNumberValue = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        formatNumberValue.setMaximumFractionDigits(2);
        for (GestaoCompra00X ordemCompra : ordensCompra) {
            body_content = body_content
                    + "				<tr>"
                    + "					<td>" + ordemCompra.getPedFornecedor() + "</td>"
                    + "					<td>" + ordemCompra.getNumero() + "</td>"
                    + "					<td>" + ordemCompra.getDescricao() + "</td>"
                    + "					<td>" + ordemCompra.getCor() + "</td>"
                    + "					<td>" + ordemCompra.getUnidade() + "</td>"
                    + "					<td>" + ordemCompra.getQtde() + "</td>"
                    + "					<td>" + formatNumberValue.format(ordemCompra.getValor()) + "</td>"
                    + "					<td><b>" + ordemCompra.getDtFaturaAsString() + "</b></td>"
                    + "				</tr>";
        }
        body_content = body_content
                + "			</tbody>"
                + "		</table>"
                + "		<br/>"
                + "		<br/>"
                + "		Ficamos no aguardo do seu retorno.<br/>"
                + "             Atenciosamente,<br/>"
                + "		<span>"
                + "			<b>" + SceneMainController.getAcesso().getDisplayName() + "</b><br/>"
                + "			<b>Telefone Fixo:</b> (048) 3641-1900<br/>"
                + "			<b>Whatsapp:</b> (047)  9 9187-0830<br/>"
                + "			<b>Email:</b> " + SceneMainController.getAcesso().getEmail() + "<br/>"
                + "			<b>Site:</b> www.deliz.com.br<br/>"
                + "		</span>"
                + "	</body>"
                + "</html>";
        //</editor-fold>

        MimeBodyPart messagePart = new MimeBodyPart();
        messagePart.setText(""
                        + body_content,
                "UTF-8", "html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        message.setContent(multipart);
        Transport.send(message);
    }

}
