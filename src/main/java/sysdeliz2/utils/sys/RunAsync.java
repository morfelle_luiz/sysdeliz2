package sysdeliz2.utils.sys;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ProgressIndicator;

import java.util.function.Consumer;
import java.util.function.ToIntFunction;

public class RunAsync<P> {
    
    private Task animationWorker;
    private Task<Integer> taskWorker;
    
    private final ProgressIndicator progressIndicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
    private Boolean animation = false;
    
    /**
     * Placing a listener on this list allows to get notified BY the result when the task has finished.
     */
    public ObservableList<Integer> resultNotificationList = FXCollections.observableArrayList();
    
    public Integer resultValue;
    
    /**
     *
     */
    public RunAsync() {
    }
    
    /**
     *
     */
    public RunAsync addTaskEndNotification(Consumer<Integer> c) {
        resultNotificationList.addListener((ListChangeListener<? super Integer>) n -> {
            resultNotificationList.clear();
            c.accept(resultValue);
        });
        //animation = true;
        return this;
    }
    
    /**
     *
     */
    public RunAsync exec(ToIntFunction func) {
        if (animation)
            setupAnimationThread();
        setupWorkerThread(func);
        return this;
    }
    
    public void exec(ToIntFunction func, Boolean wait) {
        if (animation)
            setupAnimationThread();
        setupWorkerThread(func);
    }
    
    /**
     *
     */
    private void setupAnimationThread() {
        
        animationWorker = new Task() {
            @Override
            protected Object call() throws Exception {
                /*
                This is activated when we have a "progressing" indicator.
                This thread is used to advance progress every XXX milliseconds.
                In case of an INDETERMINATE_PROGRESS indicator, it's not of use.
                for (int i=1;i<=100;i++) {
                    Thread.sleep(500);
                    updateMessage();
                    updateProgress(i,100);
                }
                */
                return true;
            }
        };
        
        progressIndicator.setProgress(0);
        progressIndicator.progressProperty().unbind();
        progressIndicator.progressProperty().bind(animationWorker.progressProperty());
        
        animationWorker.messageProperty().addListener((observable, oldValue, newValue) -> {
            // Do something when the animation value ticker has changed
        });
        
        new Thread(animationWorker).start();
    }
    
    /**
     *
     */
    private void setupWorkerThread(ToIntFunction<P> func) {
            taskWorker = new Task<Integer>() {
                @Override
                public Integer call() {
                    return func.applyAsInt((P) "123");
                }
            };
    
            EventHandler<WorkerStateEvent> eh = event -> {
                if (animation)
                    animationWorker.cancel(true);
                progressIndicator.progressProperty().unbind();
                try {
                    resultValue = taskWorker.get();
                    resultNotificationList.add(resultValue);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            };
    
            taskWorker.setOnSucceeded(eh);
            taskWorker.setOnFailed(eh);
            Thread modal = new Thread(taskWorker);
            modal.start();
    }
    
    /**
     * For those that like beans :)
     */
    public Integer getResultValue() {
        return resultValue;
    }
}
