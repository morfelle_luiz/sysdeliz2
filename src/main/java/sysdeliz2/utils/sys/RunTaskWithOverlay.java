package sysdeliz2.utils.sys;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.stage.Screen;

public class RunTaskWithOverlay {
    
    private Task taskWorker;
    
    private final StackPane parent;
    private final ProgressIndicator progressIndicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
    private final Pane overlay = getOverlay();
    
    /**
     * Placing a listener on this list allows to get notified BY the result when the task has finished.
     */
    public ObservableList<Object> resultNotificationList = FXCollections.observableArrayList();
    
    public Object resultValue;

    /**
     *
     */
    public RunTaskWithOverlay(StackPane parent) {
        this.parent = parent;
        this.parent.getChildren().add(this.overlay);
    }
    
    /**
     *
     */
    public void exec(Runnable func) {
        setupWorkerThread(func);
    }
    
    /**
     *
     */
    private void setupWorkerThread(Runnable func) {
            taskWorker = new Task() {
                @Override
                protected Object call() throws Exception {
                    Platform.runLater(func);
                    return null;
                }
            };
    
            EventHandler<WorkerStateEvent> eh = event -> {
                progressIndicator.progressProperty().unbind();
                this.parent.getChildren().remove(this.overlay);
                try {
                    resultValue = taskWorker.get();
                    resultNotificationList.add(resultValue);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            };
    
            taskWorker.setOnSucceeded(eh);
            taskWorker.setOnFailed(eh);
            new Thread(taskWorker).start();
    }
    
    /**
     * For those that like beans :)
     */
    public Object getResultValue() {
        return resultValue;
    }
    
    private Pane getOverlay() {
        Screen screen = Screen.getPrimary();
        Rectangle2D screenBounds = screen.getVisualBounds();
        StackPane p = new StackPane();
        
        progressIndicator.maxHeight(400);
        progressIndicator.maxWidth(400);
        
        Rectangle r = RectangleBuilder.create()
                .height(screenBounds.getHeight()).width(screenBounds.getWidth())
                .fill(Color.web("black", 0.3))
                .build();
        
        //p.getChildren().addAll(r, ProgressIndicatorBuilder.create().maxWidth(500).maxHeight(500).build());
        p.getChildren().addAll(r, new ImageView(new Image(getClass().getResource("/images/loading.gif").toExternalForm())));
        return p;
    }
}
