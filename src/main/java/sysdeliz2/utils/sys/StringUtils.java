package sysdeliz2.utils.sys;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

/**
 * Validate Strings
 */
public class StringUtils {

    private static Locale BRAZIL = new Locale("pt", "BR");
    private static NumberFormat formatDecimalValue = NumberFormat.getNumberInstance(BRAZIL);
    private static NumberFormat formatIntegerValue = NumberFormat.getIntegerInstance(BRAZIL);
    private static NumberFormat formatPercentValue = NumberFormat.getPercentInstance(BRAZIL);
    private static NumberFormat formatMonetaryValue = NumberFormat.getCurrencyInstance(BRAZIL);
    private static DateTimeFormatter formatDateTimeValue = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm", BRAZIL);
    private static DateTimeFormatter formatDateTimeDescValue = DateTimeFormatter.ofPattern("E, dd/MMM/yyyy HH:mm", BRAZIL);
    private static DateTimeFormatter formatShortDateTimeValue = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm", BRAZIL);
    private static DateTimeFormatter formatShortDateValue = DateTimeFormatter.ofPattern("dd/MM/yy", BRAZIL);
    private static DateTimeFormatter formatDateValue = DateTimeFormatter.ofPattern("dd/MM/yyyy", BRAZIL);
    private static DateTimeFormatter formatTimeValue = DateTimeFormatter.ofPattern("HH:mm", BRAZIL);

    /**
     * Validate if the String is empty or only white space
     *
     * @param str String for validate
     * @return true if empty; false if contain values
     */
    public static boolean isEmptyOrNull(String str) {
        if (str == null) {
            return true;
        }

        return str.trim().length() == 0;
    }

    /**
     * Return the separator line used by the operating system
     *
     * @return string
     */
    public static String newLine() {
        return System.getProperty("line.separator");
    }

    public static boolean isNotInt(String value) {
        try {
            Integer valueInt = Integer.parseInt(value);
        } catch (Exception ex) {
            return true;
        }
        return false;
    }

    public static String toDecimalFormat(double value, int fraction_digits) {
        formatDecimalValue.setRoundingMode(RoundingMode.HALF_EVEN);
        formatDecimalValue.setMaximumFractionDigits(fraction_digits);
        formatDecimalValue.setMinimumFractionDigits(fraction_digits);
        return formatDecimalValue.format(value);
    }

    public static String toDecimalFormat(BigDecimal value) {
        formatDecimalValue.setRoundingMode(RoundingMode.HALF_EVEN);
        return formatDecimalValue.format(value.doubleValue());
    }

    public static String unformatDecimal(String value) {
        return value.replace(".", "").replace(",", ".");
    }

    public static String toMonetaryFormat(double value, int fraction_digits) {
        formatMonetaryValue.setRoundingMode(RoundingMode.HALF_EVEN);
        formatMonetaryValue.setMaximumFractionDigits(fraction_digits);
        return formatMonetaryValue.format(value);
    }

    public static String toMonetaryFormat(BigDecimal value, int fraction_digits) {
        formatMonetaryValue.setRoundingMode(RoundingMode.HALF_EVEN);
        formatMonetaryValue.setMaximumFractionDigits(fraction_digits);
        return formatMonetaryValue.format(value.doubleValue());
    }

    public static String toPercentualFormat(double value, int fraction_digits) {
        formatPercentValue.setRoundingMode(RoundingMode.HALF_EVEN);
        formatPercentValue.setMaximumFractionDigits(fraction_digits);
        return formatPercentValue.format(value);
    }

    public static String toIntegerFormat(int value) {
        formatIntegerValue.setRoundingMode(RoundingMode.HALF_EVEN);
        return formatIntegerValue.format(value);
    }

    public static String toIntegerFormat(long value) {
        formatIntegerValue.setRoundingMode(RoundingMode.HALF_EVEN);
        return formatIntegerValue.format(value);
    }

    public static String toDateTimeFormat(LocalDateTime value) {
        return value.format(formatDateTimeValue);
    }

    public static String toDateTimeDescFormat(LocalDateTime value) {
        return value.format(formatDateTimeDescValue);
    }

    public static String toShortDateTimeFormat(LocalDateTime value) {
        return value.format(formatShortDateTimeValue);
    }

    public static String toTimeFormat(LocalTime value) {
        return value.format(formatTimeValue);
    }

    public static String toDateFormat(LocalDate value) {
        return value.format(formatDateValue);
    }

    public static String toShortDateFormat(LocalDate value) {
        if (value == null) return "";
        return value.format(formatShortDateValue);
    }

    public static String formatCpfCnpj(String docto) {
        if (docto == null)
            return null;

        if (docto.length() > 18) {
            return "Invalido";
        }

        String retorno = "";
        retorno = docto.replaceAll("[^0-9]", "");

        if (retorno.length() == 11) {
            retorno = formatCpf(retorno);
//            retorno = retorno.replaceFirst("(\\d{3})(\\d)", "$1.$2");
//            retorno = retorno.replaceFirst("(\\d{3})(\\d)", "$1.$2");
//            retorno = retorno.replaceFirst("(\\d{3})(\\d)", "$1-$2");
        } else {
            retorno = formatCnpj(retorno);
//            retorno = retorno.replaceFirst("(\\d{2})(\\d)", "$1.$2");
//            retorno = retorno.replaceFirst("(\\d{3})(\\d)", "$1.$2");
//            retorno = retorno.replaceFirst("(\\d{3})(\\d)", "$1/$2");
//            retorno = retorno.replaceFirst("(\\d{4})(\\d)", "$1-$2");
        }
        return retorno;
    }

    public static String formatCep(String cep) {

        String retorno = cep.replaceAll("[^0-9]", "");
        retorno = retorno.replaceFirst("(\\d{5})(\\d)", "$1-$2");
        return retorno;
    }

    public static String unformatCep(String cep) {

        String retorno = cep.replace(".", "").replace("-", "");
        return retorno;
    }

    public static String formatFone(String fone) {
        String value = fone == null ? "" : fone;

        if (value.length() == 8) {
            value = "00" + value;
        }

        value = value.replaceAll("[^0-9]", "");

        int tam = value.length();

        value = value.replaceFirst("(\\d{2})(\\d)", "($1)$2");
        value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");

        if (tam > 10) {
            value = value.replaceAll("-", "");
            value = value.replaceFirst("(\\d{5})(\\d)", "$1-$2");
        }

        return value;
    }

    public static String formatCpf(String cpf) {
        String value = cpf;
        if (value.length() > 11)
            value = value.substring(0, 11);

        value = lpad(value, 11, "0");
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1-$2");
        return value;
    }

    public static String formatCnpj(String cnpj) {
        String value = cnpj;
        value = lpad(value, 14, "0");
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{2})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1/$2");
        value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");

        return value;
    }

    public static String unformatCpf(String cnpj) {
        if (cnpj != null) {
            String value = cnpj.replace(".", "").replace("-", "");
            return value;
        }

        return null;
    }

    public static String unformatCnpj(String cnpj) {
        if (cnpj != null) {
            String value = cnpj.replace(".", "").replace("/", "").replace("-", "");
            return value;
        }

        return null;
    }

    public static String unformatCpfCnpj(String cnpj) {
        if (cnpj != null) {
            String value = cnpj.replace(".", "").replace("/", "").replace("-", "");
            return value;
        }

        return null;
    }

    public static String lpad(String value, Integer size, String padStr) {
        String valuePed = org.apache.commons.lang.StringUtils.leftPad(value, size, padStr);

        return valuePed;
    }

    public static String rpad(String value, Integer size, String padStr) {
        String valuePed = org.apache.commons.lang.StringUtils.rightPad(value, size, padStr);

        return valuePed;
    }

    public static String lpad(Integer value, Integer size, String padStr) {
        String valuePed = org.apache.commons.lang.StringUtils.leftPad(String.valueOf(value), size, padStr);

        return valuePed;
    }

    public static String rpad(Integer value, Integer size, String padStr) {
        String valuePed = org.apache.commons.lang.StringUtils.rightPad(String.valueOf(value), size, padStr);

        return valuePed;
    }

    public static String nvl(String value, String elseValue) {
        if (value == null)
            return elseValue;

        return value;
    }

    public static String capitalize(String value) {
        try {
            return value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }

    public static String capitalizeAll(String value) {
        try {
            return Arrays.stream(value.split(" ")).reduce("", (start, acum) -> start + " " + capitalize(acum.trim()));
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }

    public static String capitalizeWithoutLowerCase(String value) {
        try {
            return value.substring(0, 1).toUpperCase() + value.substring(1);
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }

    public static String capitalizeAllWithoutLowerCase(String value) {
        try {
            return Arrays.stream(value.split(" ")).reduce("", (start, acum) -> start + " " + capitalizeWithoutLowerCase(acum.trim()));
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }
}
