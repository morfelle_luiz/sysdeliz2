package sysdeliz2.utils.sys;

import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdLogSysdeliz001;
import sysdeliz2.utils.enums.TipoAcao;

import java.sql.SQLException;
import java.time.LocalDateTime;

public class SysLogger {
    
//    private static final GenericDao<SdLogSysdeliz001> daoLogSysDeliz = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLogSysdeliz001.class);
//    private static final GenericDao<Log> daoLogTi = new GenericDaoImpl<>(JPAUtils.getEntityManager(), SdLogSysdeliz001.class);
    
    public static void addFileLog(String mensagem, String modulo){
        LocalLogger.addLog(mensagem, modulo);
    }
    
    public static void addFileLogProgramacaoOf(String mensagem, String modulo){
        LocalLogger.addLogProgramacaoOf(mensagem, modulo);
    }
    
    public static void addFileLogApontamento(String mensagem, String modulo){
        LocalLogger.addLogApontamento(mensagem, modulo);
    }
    
    public static void addTableLog(String tela, TipoAcao acao, String codigoAcao, String descricaoAcao) {
        SdLogSysdeliz001 logSys = new SdLogSysdeliz001(
                LocalDateTime.now(),
                SceneMainController.getAcesso().getUsuario(),
                tela,
                acao,
                codigoAcao,
                descricaoAcao
        );
        
        try {
            new FluentDao().persist(logSys);
//            daoLogSysDeliz.save(logSys);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), tela);
        }
    }
    
    public static void addSysDelizLog(String tela, TipoAcao acao, String codigoAcao, String descricaoAcao) {
        SdLogSysdeliz001 logSys = new SdLogSysdeliz001(
                LocalDateTime.now(),
                SceneMainController.getAcesso() == null ? "sem.login" : SceneMainController.getAcesso().getUsuario(),
                tela,
                acao,
                codigoAcao,
                descricaoAcao
        );
        
        try {
            new FluentDao().persist(logSys);
//            daoLogSysDeliz.save(logSys);
        } catch (SQLException e) {
            e.printStackTrace();
            LocalLogger.addLog(e.getMessage() + "::" + e.getLocalizedMessage(), tela);
        }
    }


    
    public static void addFullLog(String tela, TipoAcao acao, String codigoAcao, String descricaoAcao){
        addFileLog(descricaoAcao, tela);
        addTableLog(tela, acao, codigoAcao, descricaoAcao);
    }
    
}
