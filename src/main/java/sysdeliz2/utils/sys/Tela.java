package sysdeliz2.utils.sys;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Tela {
    
    private final StringProperty identificador = new SimpleStringProperty();
    private final StringProperty descricao = new SimpleStringProperty();
    private final StringProperty controller = new SimpleStringProperty();
    private final BooleanProperty lista = new SimpleBooleanProperty(true);
    private final BooleanProperty edita = new SimpleBooleanProperty(true);
    private final BooleanProperty insere = new SimpleBooleanProperty(true);
    private final BooleanProperty exlcui = new SimpleBooleanProperty(true);
    private final BooleanProperty favorita = new SimpleBooleanProperty(false);
    
    public Tela() {
    }
    
    public Tela(String identificador, String descricao) {
        this.identificador.set(identificador);
        this.descricao.set(descricao);
    }
    
    public String getIdentificador() {
        return identificador.get();
    }
    
    public StringProperty identificadorProperty() {
        return identificador;
    }
    
    public void setIdentificador(String identificador) {
        this.identificador.set(identificador);
    }
    
    public String getDescricao() {
        return descricao.get();
    }
    
    public StringProperty descricaoProperty() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    public String getController() {
        return controller.get();
    }
    
    public StringProperty controllerProperty() {
        return controller;
    }
    
    public void setController(String controller) {
        this.controller.set(controller);
    }
    
    public boolean isLista() {
        return lista.get();
    }
    
    public BooleanProperty listaProperty() {
        return lista;
    }
    
    public void setLista(boolean lista) {
        this.lista.set(lista);
    }
    
    public boolean isEdita() {
        return edita.get();
    }
    
    public BooleanProperty editaProperty() {
        return edita;
    }
    
    public void setEdita(boolean edita) {
        this.edita.set(edita);
    }
    
    public boolean isInsere() {
        return insere.get();
    }
    
    public BooleanProperty insereProperty() {
        return insere;
    }
    
    public void setInsere(boolean insere) {
        this.insere.set(insere);
    }
    
    public boolean isExlcui() {
        return exlcui.get();
    }
    
    public BooleanProperty exlcuiProperty() {
        return exlcui;
    }
    
    public void setExlcui(boolean exlcui) {
        this.exlcui.set(exlcui);
    }
    
    public boolean isFavorita() {
        return favorita.get();
    }
    
    public BooleanProperty favoritaProperty() {
        return favorita;
    }
    
    public void setFavorita(boolean favorita) {
        this.favorita.set(favorita);
    }
}
