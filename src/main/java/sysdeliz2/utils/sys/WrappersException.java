package sysdeliz2.utils.sys;

import java.util.function.Consumer;

public class WrappersException {
    @FunctionalInterface
    public interface ConsumerCheckException<T> {
        void accept(T elem) throws Exception;
    }
    
    public static <T> Consumer<T> rethrow(ConsumerCheckException<T> c) {
        return elem -> {
            try {
                c.accept(elem);
            } catch (Exception ex) {
                /**
                 * within sneakyThrow() we cast to the parameterized type T.
                 * In this case that type is RuntimeException.
                 * At runtime, however, the generic types have been erased, so
                 * that there is no T type anymore to cast to, so the cast
                 * disappears.
                 */
                try {
                    WrappersException.<RuntimeException>sneakyThrow(ex);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        };
    }
    
    /**
     * Reinier Zwitserloot who, as far as I know, had the first mention of this
     * technique in 2009 on the java posse mailing list.
     * http://www.mail-archive.com/javaposse@googlegroups.com/msg05984.html
     */
    public static <T extends Throwable> T sneakyThrow(Throwable t) throws Throwable {
        throw (T) t;
    }
}
