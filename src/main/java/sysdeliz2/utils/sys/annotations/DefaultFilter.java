package sysdeliz2.utils.sys.annotations;

import sysdeliz2.utils.enums.PredicateType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD})
public @interface DefaultFilter {
    String column();
    String value();
    PredicateType typePredicate() default PredicateType.EQUAL;
}
