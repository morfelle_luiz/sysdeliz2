package sysdeliz2.utils.sys.annotations;


import sysdeliz2.utils.enums.ColumnType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD})
public @interface ExibeTableView {
    String descricao();
    int width();
    ColumnType columnType() default ColumnType.TEXT;
    int decimalValue() default 0;
}

