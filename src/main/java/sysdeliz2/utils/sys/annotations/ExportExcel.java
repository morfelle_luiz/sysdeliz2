package sysdeliz2.utils.sys.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD})
public @interface ExportExcel {
    String nameToShow();
    String typeField() default "";
}
