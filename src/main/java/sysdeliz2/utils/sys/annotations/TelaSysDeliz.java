package sysdeliz2.utils.sys.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface TelaSysDeliz {
    String descricao();
    String icon() default "";
}

