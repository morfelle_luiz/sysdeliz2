package sysdeliz2.utils.validator;

import br.com.validators.simple.enums.Severity;

import java.util.Arrays;
import java.util.Objects;

@Deprecated
public class SimpleMessage {

    private final String message, category;
    private final Severity severity;
    // Usado quando a message possuir marcações como %s
    private final Object[] messageParameters;


    public SimpleMessage(String category, String message, Object... messageParameters) {
        this(category, message, Severity.ERROR, messageParameters);
    }

    public SimpleMessage(String category, String message, Severity severity, Object... messageParameters) {
        this.category = category;
        this.message = message;
        this.messageParameters = messageParameters;
        this.severity = severity;
    }

    public String getMessage() {
        // Devolve a string formatada.
        if (messageParameters != null && messageParameters.length > 0) {
            return String.format(message, messageParameters);
        }
        return message;
    }

    public Severity getSeverity() {
        return severity;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "category: " + this.category +
                "\nmessage: " + this.message +
                "\nseverity: " + this.severity +
                "\nparameters: " + this.messageParameters.toString();
    }

    public String toShowString(){
        return "[" + this.category + "] " + this.message;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(category) ^ Objects.hashCode(message) ^ Objects.hash(messageParameters)
                ^ Objects.hashCode(severity);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SimpleMessage other = (SimpleMessage) obj;
        return Objects.equals(category, other.category) && Objects.equals(message, other.message)
                && ((messageParameters == null && other.messageParameters == null)
                || Arrays.equals(messageParameters, other.messageParameters))
                && Objects.equals(severity, other.severity);
    }
}
