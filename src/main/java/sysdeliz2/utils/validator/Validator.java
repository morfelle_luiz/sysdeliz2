package sysdeliz2.utils.validator;

import br.com.validators.simple.enums.Severity;
import javafx.scene.control.Alert;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class Validator {

    private List<SimpleMessage> messages;
    private boolean hasError = false;
    private boolean hasInfo = false;
    private boolean hasWarn = false;

    private int height = 150;
    private int width = 300;
    private int perc = 5;

    private String stringErrors = "";

    public Validator() {
        messages = new ArrayList<>();
    }

    private Validator check(Boolean expresison, SimpleMessage message) {

        if (expresison) {
            messages.add(message);
        }

        return this;
    }

    private void verify() {
        hasError = false;
        hasInfo = false;
        hasWarn = false;

        messages.forEach(message -> {
            switch (message.getSeverity()) {
                case ERROR:
                    hasError = true;
                case INFO:
                    hasInfo = true;
                case WARN:
                    hasWarn = true;
            }
        });
    }

    public Validator addIf(Boolean expression, SimpleMessage message) {
        return check(expression, message);
    }

    public Validator ensure(Boolean expression, SimpleMessage message) {
        return check(!expression, message);
    }

    public Boolean hasError() {
        verify();
        return hasError;
    }

    public Boolean hasInfo() {
        verify();
        return hasInfo;
    }

    public Boolean hasWarn() {
        verify();
        return hasWarn;
    }

    public List<String> getErrors() {
        List<String> mErrors = new ArrayList<>();
        messages.forEach(message -> {
            if (message.getSeverity() == Severity.ERROR) {
                mErrors.add(message.toShowString());
            }
        });
        return mErrors;
    }

    public List<String> getInfo() {
        List<String> mInfo = new ArrayList<>();
        messages.forEach(message -> {
            if (message.getSeverity() == Severity.ERROR) {
                mInfo.add(String.format("[%s] %s", message.getSeverity(), message.getMessage()));
            }
        });
        return mInfo;
    }

    public List<String> getWarn() {
        List<String> mWarn = new ArrayList<>();
        messages.forEach(message -> {
            if (message.getSeverity() == Severity.ERROR) {
                mWarn.add(String.format("[%s] %s", message.getSeverity(), message.getMessage()));
            }
        });
        return mWarn;
    }

    private void showAlert(String message, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("SysDeliz");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.getDialogPane().setStyle(" -fx-max-width:" + width + "px; -fx-max-height: " + height + "px; -fx-pref-width: " + width + "px; -fx-pref-height: "  + height +  "px;");

        alert.showAndWait();
    }

    public void showAlertErrors() {

        height = 200;
        width  = 400;

        List<String> errors = getErrors();
        StringBuilder exibir = new StringBuilder();
        int count = 0;

        for (String msg : errors ){
            exibir.append(msg).append("\n");
            count++;
        }

        if(count > 1){
            height += 5 * count;
            width += 10 * count;
        }

        this.showAlert(exibir.toString(), Alert.AlertType.ERROR);
    }


}
