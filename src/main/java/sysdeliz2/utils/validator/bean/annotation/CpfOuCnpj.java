package sysdeliz2.utils.validator.bean.annotation;

import sysdeliz2.utils.validator.bean.validation.DelizCpfOuCnpjValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * Restrição que pode ser associada a objetos em que o método
 * {@linkplain #toString()} represente um CPF or CNPJ.
 *
 * @author lima.joao
 * @since 02/08/2019 13:05
 */
@Documented
@Retention(value = RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD})
@Constraint(validatedBy = {DelizCpfOuCnpjValidator.class})
public @interface CpfOuCnpj {
    String message() default "CPF/CNPJ inválido";

    boolean formatted() default false;

    boolean ignoreRepeated() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
