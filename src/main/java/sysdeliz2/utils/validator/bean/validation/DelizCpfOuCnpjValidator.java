package sysdeliz2.utils.validator.bean.validation;

import br.com.caelum.stella.bean.validation.logic.AnnotationMessageProducer;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import sysdeliz2.utils.validator.bean.annotation.CpfOuCnpj;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author lima.joao
 * @since 02/08/2019 13:04
 */
public class DelizCpfOuCnpjValidator implements ConstraintValidator<CpfOuCnpj, String> {

    private CPFValidator cpfValidator;
    private CNPJValidator cnpjValidator;


    @Override
    public void initialize(CpfOuCnpj cpfOuCnpj) {
        AnnotationMessageProducer messageProducer = new AnnotationMessageProducer(cpfOuCnpj);

        cpfValidator  = new CPFValidator(messageProducer, cpfOuCnpj.formatted(), cpfOuCnpj.ignoreRepeated());
        cnpjValidator = new CNPJValidator(messageProducer, cpfOuCnpj.formatted());
    }

    @Override
    public boolean isValid(String documento, ConstraintValidatorContext context) {
        if(documento != null){
            if(documento.trim().length() == 0) {
                return true;
            } else {
                String docto = documento.replaceAll("[^0-9]", "");

                if(docto.length() == 14){
                    return cnpjValidator.invalidMessagesFor(docto).isEmpty();
                } else {
                    return cpfValidator.invalidMessagesFor(docto).isEmpty();
                }
            }
        } else {
            return true;
        }
    }
}