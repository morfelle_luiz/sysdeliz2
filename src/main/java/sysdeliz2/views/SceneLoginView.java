package sysdeliz2.views;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import sysdeliz2.controllers.fxml.SceneLoginController;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.Marca;
import sysdeliz2.models.properties.Empresa;
import sysdeliz2.models.sysdeliz.Inventario.SdInventario;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.AcessoSistema;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.LdapAuth;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.io.*;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Random;
import java.util.function.ToIntFunction;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SceneLoginView extends SceneLoginController {

    private AcessoSistema usuarioLogado;

    private Parent load = null;

    private final ObjectProperty<Empresa> empresaSelecionada = new SimpleObjectProperty<>(null);

    private final Rectangle2D screenBounds = Screen.getPrimary().getBounds();

    private Task<Integer> taskWorker;
    public Integer resultValue;
    private FXMLLoader root;
    BooleanProperty loaded = new SimpleBooleanProperty(false);

    //<editor-fold desc="Fields">
    private final FormFieldText usuarioField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.USER, ImageUtils.IconSize._16));
        field.width(screenBounds.getWidth() * 0.18);
        field.placeHolder("Usuário");
        field.value.set(Globals.getNomeUsuario());
        field.textField.setFont(Font.font("Verdana", 13));
    });

    private final FormPasswordField senhaField = FormPasswordField.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LOCK, ImageUtils.IconSize._16));
        field.width(screenBounds.getWidth() * 0.18);
        field.placeHolder("Senha");
        field.textField.setFont(Font.font("Verdana", 13));
    });
    //</editor-fold>

    //<editor-fold desc="Label">
    private final FormLabel lblVersion = FormLabel.create(lbl -> {
        FileReader frVerLocal;
        try {
            frVerLocal = new FileReader("C:/SysDelizLocal/SysDeliz2/sys.version");

            BufferedReader reader = new BufferedReader(frVerLocal);
            String line;
            StringBuilder localVersion = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                localVersion.append(line);
            }
            lbl.setText("v. " + localVersion);
        } catch (IOException ex) {
            Logger.getLogger(SceneLoginController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    });
    //</editor-fold>

    //<editor-fold desc="Button">
    private final Button loginButton = FormButton.create(btn -> {
        btn.setText("Entrar");
        btn.addStyle("sm");
        btn.setStyle("-fx-background-color: #802046; -fx-text-fill: #F7F7F7");
        btn.disable.unbind();
        btn.setOnAction(evt -> {
            autenticarUsuario();
        });
        btn.width(screenBounds.getWidth() * 0.18);
    });
    //</editor-fold>

    //<editor-fold desc="Box">
    private final FormBox boxErro = FormBox.create(box -> {
        box.vertical();
        box.alignment(Pos.CENTER);
        box.height(50);
    });

    private final FormBox boxInfosLogin = FormBox.create(box -> {
        box.vertical();
        box.add(FormBox.create(boxLabel -> {
            boxLabel.horizontal();
            boxLabel.alignment(Pos.CENTER);
            boxLabel.width(screenBounds.getWidth() * 0.18);
            boxLabel.add(new Separator(Orientation.HORIZONTAL));
            boxLabel.add(FormLabel.create(lbl -> {
                lbl.setText("Acesse a sua conta");
                lbl.setTextFill(Color.valueOf("#6D6E72"));
                lbl.alignment(Pos.CENTER);
            }));
            boxLabel.add(new Separator(Orientation.HORIZONTAL));
        }));
        box.add(FormBox.create(boxFieldsLogin -> {
            boxFieldsLogin.vertical();
            boxFieldsLogin.alignment(Pos.CENTER);
            boxFieldsLogin.setPadding(new Insets(7, 0, 7, 0));
            boxFieldsLogin.add(FormBox.create(boxUsuario -> {
                boxUsuario.horizontal();
                boxUsuario.expanded();
                boxUsuario.alignment(Pos.CENTER);
                boxUsuario.setPadding(new Insets(7, 0, 7, 0));
                boxUsuario.add(usuarioField.build());
            }));
            boxFieldsLogin.add(FormBox.create(boxSenha -> {
                boxSenha.horizontal();
                boxSenha.expanded();
                boxSenha.alignment(Pos.CENTER);
                boxSenha.setPadding(new Insets(7, 0, 7, 0));
                boxSenha.add(senhaField.build());
            }));
        }));
        box.add(FormBox.create(boxButton -> {
            boxButton.vertical();
            boxButton.alignment(Pos.CENTER);
            boxButton.add(loginButton);
        }));
        box.add(boxErro);
    });
    //</editor-fold>

    public SceneLoginView() {
        setupWorkerThread(task -> {
            JPAUtils.getEntityManager();
            return ReturnAsync.OK.value();
        });
        init();
    }

    private void setupWorkerThread(ToIntFunction func) {
        taskWorker = new Task<Integer>() {
            @Override
            public Integer call() {
                return func.applyAsInt("123");
            }
        };

        EventHandler<WorkerStateEvent> eh = event -> {
            loaded.set(true);
        };
        taskWorker.setOnSucceeded(eh);
        taskWorker.setOnFailed(eh);
        Thread modal = new Thread(taskWorker);
        modal.start();
    }

    private void init() {
        int codFoto = new Random().nextInt(4);
        int codMarca = new Random().nextInt(2);
        getChildren().add(FormBadges.create(badge -> {
            badge.position(Pos.BOTTOM_RIGHT, 10.0);
            badge.node(lblVersion);
            badge.badgedNode(FormBadges.create(bg1 -> {
                bg1.position(Pos.BOTTOM_LEFT, 20.0);
                try {
                    ImageView logoMarca = new ImageView(new Image(new FileInputStream("K:\\Java_NFE\\Imagens Background\\" + (codMarca == 1 ? "Dlz" : "FlorDeLis") + ".png")));
                    logoMarca.autosize();
                    logoMarca.setPreserveRatio(true);
                    logoMarca.setFitHeight(screenBounds.getHeight() * 0.09);
                    bg1.node(logoMarca);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bg1.badgedNode(FormBox.create(principal -> {
                    principal.horizontal();
                    principal.expanded();
                    principal.alignment(Pos.CENTER_RIGHT);
                    principal.add(FormBox.create(boxImg -> {
                        boxImg.horizontal();
                        boxImg.add(FormBox.create(boxImage -> {
                            boxImage.vertical();
                            boxImage.alignment(Pos.CENTER);
                            boxImage.setMaxWidth(screenBounds.getWidth() * 0.78);
                            boxImage.setPrefWidth(screenBounds.getWidth() * 0.78);
                            boxImage.setMinWidth(0);

                            ImageView imageBackground = new ImageView();
                            imageBackground.autosize();

                            Image bg = null;
                            try {
                                bg = new Image(new FileInputStream("K:\\Java_NFE\\Imagens Background\\" + (codMarca == 1 ? "D" : "F") + codFoto + ".jpg"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            imageBackground.setImage(bg);
                            imageBackground.setPreserveRatio(true);

                            imageBackground.setFitHeight(screenBounds.getHeight() * 0.97);
                            boxImage.add(imageBackground);
                        }));
                    }));
                    principal.add(FormBox.create(boxLogin -> {
                        boxLogin.vertical();
                        boxLogin.expanded();
                        boxLogin.setMaxWidth(screenBounds.getWidth() * 0.22);
                        boxLogin.setMinWidth(screenBounds.getWidth() * 0.22);
                        boxLogin.setPrefWidth(screenBounds.getWidth() * 0.22);
                        boxLogin.setBackground(new Background(new BackgroundFill(Color.valueOf("#F0F0F0"), CornerRadii.EMPTY, Insets.EMPTY)));
                        boxLogin.alignment(Pos.CENTER);
                        boxLogin.add(FormBox.create(boxContent -> {
                            boxContent.vertical();
                            boxContent.add(FormBox.create(boxLogo -> {
                                boxLogo.vertical();
                                boxLogo.alignment(Pos.CENTER);
                                ImageView logoDeliz = new ImageView(ImageUtils.getImage(ImageUtils.Icon.LOGO_DELIZ_FASHION_GROUP));
                                logoDeliz.setFitHeight(screenBounds.getHeight() * 0.18);
                                logoDeliz.setFitWidth(screenBounds.getWidth() * 0.18);
                                boxLogo.add(logoDeliz);
                                boxLogo.setPadding(new Insets(5, 0, 15, 0));
                            }));
                            boxContent.add(boxInfosLogin);
                        }));
                    }));
                }));
            }));
        }));
        Platform.runLater(senhaField::requestFocus);
        setOnKeyReleased(event -> {
            if (event.getCode().equals(KeyCode.ENTER) && empresaSelecionada.isNull().get()) autenticarUsuario();
            else if (event.getCode().equals(KeyCode.ENTER)) loadSistema();
        });
    }

    private void selecionarEmpresa() {
        boxInfosLogin.clear();
        boxInfosLogin.add(FormBox.create(boxLabel -> {
            boxLabel.horizontal();
            boxLabel.alignment(Pos.CENTER);
            boxLabel.width(screenBounds.getWidth() * 0.18);
            boxLabel.add(new Separator(Orientation.HORIZONTAL));
            boxLabel.add(FormLabel.create(lbl -> {
                lbl.setText("Selecione a empresa para acesso");
                lbl.setTextFill(Color.valueOf("#6D6E72"));
                lbl.alignment(Pos.CENTER);
            }));
            boxLabel.add(new Separator(Orientation.HORIZONTAL));
        }));
        boxInfosLogin.add(FormBox.create(boxFieldsLogin -> {
            boxFieldsLogin.vertical();
            boxFieldsLogin.alignment(Pos.CENTER);
            boxFieldsLogin.setPadding(new Insets(5, 0, 5, 0));
            for (Empresa empresa : Globals.empresas) {
                boxFieldsLogin.add(FormBox.create(boxBtn -> {
                    boxBtn.horizontal();
                    boxBtn.alignment(Pos.CENTER);
                    boxBtn.setPadding(new Insets(7, 0, 0, 0));
                    boxBtn.add(FormControlButton.create(btn -> {
                        ImageView iv = new ImageView(empresa.getAltIconeEmpresa());
                        iv.setFitWidth(20);
                        iv.setFitHeight(25);
                        btn.icon(iv);
                        btn.removeStyle("control-button");
                        btn.addStyle("md");
                        btn.width(screenBounds.getWidth() * 0.18);
                        btn.autoSelect.set(false);
                        btn.bgColorSelected.set("#B33D66");
                        btn.bgColorStandard.set("#E3E7FC");
                        btn.setText(empresa.getNome());

                        btn.setOnMouseClicked(event -> {
                            if (event.getButton().equals(MouseButton.PRIMARY)) {
                                loadSistema();
                            }
                        });

                        btn.focusedProperty().addListener((observable, oldValue, newValue) -> {
                            if (empresaSelecionada.get() != empresa && newValue) {
                                empresaSelecionada.set(empresa);
                            }
                        });

                        empresaSelecionada.addListener((observable, oldValue, newValue) -> {
                            btn.isSelected.set(newValue == empresa);
                        });

                        btn.isSelected.addListener((observable, oldValue, newValue) -> {
                            if (empresaSelecionada.get() != empresa) {
                                if (newValue) empresaSelecionada.set(empresa);
                            }
                        });
                    }));
                }));
            }
        }));
//        boxInfosLogin.add(FormBox.create(boxButton -> {
//            boxButton.vertical();
//            boxButton.setPadding(new Insets(12, 0, 0, 0));
//            boxButton.alignment(Pos.CENTER);
//
//            boxButton.add(FormButton.create(btn -> {
//                btn.setText("Acessar");
//                btn.addStyle("sm");
//                btn.width(screenBounds.getWidth() * 0.18);
//                btn.setOnAction(evt -> {
//                    loadSistema();
//                });
//            }));
//        }));
        Platform.runLater(() -> empresaSelecionada.set(Globals.empresas.get(0)));
    }

    private void autenticarUsuario() {
        boxErro.clear();

        if (usuarioField.value.isEmpty().get() || senhaField.value.isEmpty().get()) {
            return;
        }
        try {
            usuarioLogado = LdapAuth.authenticateUserAndGetInfo(usuarioField.value.getValue(), senhaField.value.getValue());

            if (usuarioLogado == null || usuarioLogado.getNome().equals("")) {
                boxErro.add(FormLabel.create(lbl -> {
                    lbl.setText("Usuário/Senha inválidos ou não encontrados.\nConfirme os dados de acesso.");
                    lbl.setStyle("-fx-text-fill: red;");
                    lbl.setId("lbl-erro");
                }));
                senhaField.clear();
                senhaField.requestFocus();
            } else if (!usuarioLogado.isIsSistemaLiberado()) {
                boxErro.add(FormLabel.create(lbl -> {
                    lbl.setText("Seu usuário não tem permissão para acesso ao sistema.\nEntre em contato com o setor de TI.");
                    lbl.setStyle("-fx-text-fill: red;");
                    lbl.setId("lbl-erro");
                }));
                senhaField.clear();
                senhaField.requestFocus();
            } else {
                usuarioField.textField.setDisable(true);
                senhaField.textField.setDisable(true);
                Globals.setUsuarioLogado(usuarioLogado);
                if (usuarioLogado.getListPermissoesUsuario().containsAll(Arrays.asList("SysDeliz", "SysUPWave"))) {
                    this.selecionarEmpresa();
                } else {
                    if (usuarioLogado.getListPermissoesUsuario().contains("SysDeliz")) {
                        empresaSelecionada.set(Globals.empresas.get(0));
                    } else if (usuarioLogado.getListPermissoesUsuario().contains("SysUPWave")) {
                        empresaSelecionada.set(Globals.empresas.get(1));
                    } else {
                        MessageBox.create(message -> {
                            message.message("Você não possui acesso! Favor entrar em contato com a TI.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showAndWait();
                        });
                        Globals.getMainStage().close();
                        return;
                    }
                    loadSistema();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SceneLoginController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void loadSistema() {

        Globals.setEmpresaLogada(empresaSelecionada.get());
        if (empresaSelecionada.get().getCodigo().equals("2000")) {
            Globals.getMainStage().getIcons().clear();
            Globals.getMainStage().getIcons().add(new Image(getClass().getResourceAsStream("/images/icon-upwave.png")));
        }

        try {
            String[] dadosUsuarioBanco = DAOFactory.getUsuariosDAO().getDadosUsuario(Globals.getUsuarioLogado().getUsuario());
            Globals.getUsuarioLogado().setCodFunTi(dadosUsuarioBanco[0]);
            Globals.getUsuarioLogado().setCodUsuarioTi(dadosUsuarioBanco[2]);
            Globals.getUsuarioLogado().setUsuarioTi(dadosUsuarioBanco[1]);
            Globals.setColaboradorSistema(Globals.getColaborador(Globals.getNomeUsuario()));
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }

//        if (loaded.not().get()) {
//            telaCarregamento();
//            return;
//        }

        carregaSistema();
    }

    private void telaCarregamento() {
        getChildren().clear();
        getChildren().add(FormBox.create(princ -> {
            princ.vertical();
            princ.expanded();
            princ.alignment(Pos.CENTER);
            princ.setBackground(new Background(new BackgroundFill(Color.valueOf(empresaSelecionada.get().getCodigo().equals("1000") ? "#802046" : "#000000"), CornerRadii.EMPTY, Insets.EMPTY)));
            princ.add(FormBox.create(boxLogo -> {
                boxLogo.vertical();
                ImageView iv = new ImageView(ImageUtils.getImage(empresaSelecionada.get().getCodigo().equals("1000") ? "/images/fg_bg_r.jpg" : "/images/logo-upwave.jpg"));
                iv.autosize();
                iv.setPreserveRatio(true);
                iv.setFitWidth(screenBounds.getWidth() * 0.25);
                boxLogo.add(iv);
                boxLogo.alignment(Pos.CENTER);
                boxLogo.setPadding(new Insets(5, 0, 5, 0));
            }));

            ImageView gif = new ImageView(getClass().getResource("/images/loading-white.gif").toExternalForm());
            gif.autosize();
            gif.setPreserveRatio(true);
            gif.setFitWidth(70);
            princ.add(gif);
        }));


    }

    private void carregaSistema() {
        try {
            root = new FXMLLoader(getClass().getResource("/sysdeliz2/controllers/fxml/SceneMain.fxml"));
            root.setController(new SceneMainController(usuarioLogado));
            load = root.load();
            Globals.getMainStage().getScene().setRoot(load);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
