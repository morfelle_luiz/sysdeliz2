package sysdeliz2.views.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.cadastros.CadastroDepositoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.Deposito;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CadastroDepositoView extends CadastroDepositoController {

    //<editor-fold defaultstate="colapsed" desc="Bean">
    private final ListProperty<Deposito> depositosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private Deposito oldDeposito = new Deposito();
    //</editor-fold>

    //<editor-fold defaultstate="colapsed" desc="Table">
    private final FormTableView<Deposito> tblDepositos = FormTableView.create(FormTableView.class, table -> {
        table.title("Depositos");
        table.expanded();
        table.items.bind(depositosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(320);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));

                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Prioridade");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrioridade()));

                }).build(), /*Prioridade*/
                FormTableColumn.create(cln -> {
                    cln.title("Codcli");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));

                }).build(), /*Codcli*/
                FormTableColumn.create(cln -> {
                    cln.title("País");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPais()));

                }).build(), /*País*/
                FormTableColumn.create(cln -> {
                    cln.title("Posse");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPosse()));

                }).build(), /*Posse*/
                FormTableColumn.create(cln -> {
                    cln.title("CodDep");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCoddep()));

                }).build(), /*CodDep*/
                FormTableColumn.create(cln -> {
                    cln.title("Tipest");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipest()));

                }).build(), /*Tipest*/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo_Mat");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipoMat()));

                }).build(), /*Tipo_mat*/
                FormTableColumn.create(cln -> {
                    cln.title("Grupo Depósito");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<Deposito, Deposito>, ObservableValue<Deposito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGrupoDep()));

                }).build() /*Grupo Depósito*/

        );

    });
    //</editor-fold>

    //<editor-fold defaultstate="colapsed" desc="FilterField">
    private final FormFieldMultipleFind<Deposito> codigoFilterField = FormFieldMultipleFind.create(Deposito.class, field -> {
        field.title("Código");
    });
    private final FormFieldText codCliFilterField = FormFieldText.create(field -> {
        field.title("CodCli");
        field.width(300);
        field.editable(true);
    });
    private final FormFieldText paisFilterField = FormFieldText.create(field -> {
        field.title("País");
        field.width(300);
        field.editable(true);
    });
    private final FormFieldText grupoDepFilterField = FormFieldText.create(field -> {
        field.title("Grupo Depósito");
        field.width(300);
        field.editable(true);
    });
    private final FormFieldSegmentedButton<String> tipoMatSegBttn = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo de Material");
        field.options(
                field.option("Material", "M", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Tecidos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                field.option("P. Acabados", "E", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Todos", "A",FormFieldSegmentedButton.Style.DANGER)

        );
        field.select(3);
    });
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<Deposito> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblDepositos);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((Deposito) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarCadastro((Deposito) nav.selectedItem.get()));
        nav.btnSave(evt -> {
            try {
                salvarCadastro((Deposito) nav.selectedItem.get());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((Deposito) newValue);
            }
        });
    });
    // </editor-fold>//

    //<editor-fold defaultstate="colapsed" desc="Fields">


    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.maxLength(50);
    });


    private final FormFieldText prioridadeField = FormFieldText.create(field -> {
        field.title("Prioridade");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.mask(FormFieldText.Mask.INTEGER);
        field.maxLength(1);
    });

    private final FormFieldText codCliField = FormFieldText.create(field -> {
        field.title("CodCli");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.mask(FormFieldText.Mask.INTEGER);
        field.maxLength(5);
    });

    private final FormFieldComboBox<String> paisField = FormFieldComboBox.create(String.class, field -> {
        field.title("País");
        field.editable.bind(navegation.inEdition);
        field.items(FXCollections.observableList(Arrays.asList("BR", "PY", "ND")));
        field.select(0);
    });

    private final FormFieldText posseField = FormFieldText.create(field -> {
        field.title("Posse");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.maxLength(1);
    });

    private final FormFieldText tipestField = FormFieldText.create(field -> {
        field.title("Tipest");
        field.width(300);
        field.editable(false);
        field.value.setValue("1");
    });

    private final FormFieldComboBox<String> codDepField = FormFieldComboBox.create(String.class, field -> {
        field.title("Cod Dep");
        field.editable.bind(navegation.inEdition);
        field.items(FXCollections.observableList(Arrays.asList("1-200", "1-300", "1-500")));
        field.select(0);
        field.getSelectionModel((observable, oldValue, newValue) -> {
            if (newValue != null)
                if (newValue.equals("1-200")) tipestField.value.setValue("1");
                else if (newValue.equals("1-300")) tipestField.value.setValue("1");
                else tipestField.value.setValue("3");
        });
    });

    private final FormFieldComboBox<String> tipoMatField = FormFieldComboBox.create(String.class, field -> {
        field.title("Tipo Material");
        field.editable.bind(navegation.inEdition);
        field.items(FXCollections.observableList(Arrays.asList("M", "T", "E")));
        field.select(0);
    });

    private final FormFieldText grupoDepField = FormFieldText.create(field -> {
        field.title("Grupo Depósito");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.mask(FormFieldText.Mask.INTEGER);
        field.maxLength(5);
    });

    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(300);
        field.editable.bind(navegation.inEdition);
        field.mask(FormFieldText.Mask.INTEGER);
        field.maxLength(6);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                grupoDepField.value.setValue(field.value.getValue());
        });
    });
    //</editor-fold>


    public CadastroDepositoView() {
        super("Cadastro de Depósito", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }


    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFiltersField -> {
                        boxFiltersField.horizontal();
                        boxFiltersField.add(codigoFilterField.build());
                        boxFiltersField.add(codCliFilterField.build());
                        boxFiltersField.add(paisFilterField.build());
                        boxFiltersField.add(tipoMatSegBttn.build());
                        boxFiltersField.add(grupoDepFilterField.build());
                    }));
                    filter.find.setOnAction(evt -> {

                        new RunAsyncWithOverlay(this).exec(task -> {
                            JPAUtils.clearEntitys(depositos);
                            getDepositos(codigoFilterField.objectValues.getValue().stream().map(Deposito::getCodigo).toArray(),
                                    codCliFilterField.value.getValue(),
                                    paisFilterField.value.getValue(),
                                    tipoMatSegBttn.value.getValue(),
                                    grupoDepFilterField.value.getValue());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                depositosBean.set(FXCollections.observableList(super.depositos));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        codigoFilterField.clear();
                        codCliFilterField.clear();
                        paisFilterField.clear();
                        tipoMatSegBttn.select(3);
                        grupoDepFilterField.clear();
                    });
                }));
            }));
            principal.add(tblDepositos.build());
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.title("Dados Gerais");
            section.vertical();
            section.add(FormBox.create(box -> {
                box.horizontal();
                box.add(codigoField.build());
                box.add(descricaoField.build());
                box.add(prioridadeField.build());

            }));
            section.add(FormBox.create(box -> {
                box.horizontal();
                box.add(codCliField.build());
                box.add(posseField.build());
                box.add(tipoMatField.build());
                box.add(paisField.build());
            }));
            section.add(FormBox.create(box -> {
                box.horizontal();
                box.add(codDepField.build());
                box.add(tipestField.build());
                box.add(grupoDepField.build());
            }));
        }));
    }

    private void novoCadastro() {
        navegation.inEdition.set(true);
        navegation.selectedItem.set(new Deposito());
        limparCampos();
        tipestField.value.setValue("1");
    }

    private void excluirCadastro(Deposito item) {
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Você deseja excluir o depósito?");
            message.showAndWait();
        }).value.get()) {
            oldDeposito = getValues();
            SysLogger.addSysDelizLog("Cadastro Depósito", TipoAcao.EXCLUIR, oldDeposito.getCodigo(), "Depósito Deletado -- Código: " + oldDeposito.getCodigo() + " -- Descrição: " + oldDeposito.getDescricao());
            new FluentDao().delete(item);
            depositos.remove(item);
            tblDepositos.refresh();
            cancelarCadastro();
            MessageBox.create(message -> {
                message.message("Depósito excluído com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void abrirCadastro(Deposito item) {
        navegation.selectedItem.set(item);
        tabs.getSelectionModel().select(1);
        carregaDados(item);
    }

    private void carregaDados(Deposito item) {
        if (item != null)
            if (item.getCodigo() != null) {
                codigoField.value.setValue(item.getCodigo()!= null ? item.getCodigo() : "");
                descricaoField.value.setValue(item.getDescricao()!= null ? item.getDescricao() : "");
                prioridadeField.value.set(item.getPrioridade()!= null ? item.getPrioridade() : "");
                codCliField.value.setValue(item.getCodcli() != null ? item.getCodcli() : "");
                paisField.value.set(item.getPais()!= null ? item.getPais() : "");
                posseField.value.setValue(item.getPosse()!= null ? item.getPosse() : "");
                codDepField.select(item.getCoddep()!= null ? item.getCoddep() : "");
//                codDepField.value.setValue(item.getCoddep()!= null ? item.getCoddep() : "");
                tipestField.value.setValue(item.getTipest()!= null ? item.getTipest() : "");
                tipoMatField.value.setValue(item.getTipoMat()!= null ? item.getTipoMat() : "");
                grupoDepField.value.setValue(item.getGrupoDep()!= null ? item.getGrupoDep() : "");
            }
    }

    private void editarCadastro(Deposito item) {
        navegation.inEdition.set(true);
        oldDeposito = getValues();
        abrirCadastro(item);
        MessageBox.create(message -> {
            message.message("Ativado modo edição");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarCadastro(Deposito item) throws IllegalAccessException {

        item = getValues();

        if (!existCodigo(codigoField.value.getValue())) {
            try {
                item = new FluentDao().persist(item);
                depositos.add(item);
                navegation.selectedItem.set(item);
                MessageBox.create(message -> {
                    message.message("Depósito salvo com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro Depósito", TipoAcao.CADASTRAR, item.getCodigo(),
                        "Depósito Cadastrado  -- Código: " + item.getCodigo() +
                                " -- Descrição: " + item.getDescricao());
            } catch (SQLException e) {

                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        } else {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Depósito já existe, deseja altera-lo?");
                message.showAndWait();
            }).value.get()) {
                navegation.selectedItem.set(item);
                item = new FluentDao().merge(item);

                MessageBox.create(message -> {
                    message.message("Depósito alterado com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro Depósito", TipoAcao.EDITAR, item.getCodigo(),
                        "Depósito Alterado -- Código: " + item.getCodigo() +
                                " -- Descrição: " + item.getDescricao() + valoresAlterados(item));

            } else {
                cancelarCadastro();
            }
        }
        tblDepositos.refresh();
        navegation.inEdition.set(false);
    }

    private boolean existCodigo(String codigo) {
        return new FluentDao().selectFrom(Deposito.class).where(eb -> eb.equal("codigo", codigo)).singleResult() != null;
    }

    private void cancelarCadastro() {
        navegation.inEdition.set(false);
        navegation.selectedItem.set(null);
        limparCampos();
        depositosBean.set(FXCollections.observableList(super.depositos));
        if (!tblDepositos.items.isEmpty()) tblDepositos.selectItem(0);
    }

    private void limparCampos() {
        codigoField.value.setValue("");
        descricaoField.clear();
        prioridadeField.value.setValue("");
        codCliField.value.setValue("");
        paisField.select(0);
        posseField.clear();
        codDepField.select(0);
        tipestField.clear();
        tipoMatField.select(3);
        grupoDepField.value.setValue("");
    }

    private Deposito getValues() {
        Deposito values = new Deposito();

        values.setCodigo(codigoField.value.getValue());
        values.setDescricao(descricaoField.value.getValue());
        values.setPrioridade(prioridadeField.value.getValue());
        values.setCodcli(codCliField.value.getValue());
        values.setPais(paisField.value.getValue());
        values.setPosse(posseField.value.getValue());
        values.setCoddep(codDepField.value.getValue());
        values.setTipest(tipestField.value.getValue());
        values.setTipoMat(tipoMatField.value.getValue());
        values.setGrupoDep(grupoDepField.value.getValue());

        return values;
    }

    private String valoresAlterados(Deposito item) throws IllegalAccessException {
        Map<String, String> diffs = new HashMap<>();
        for (Field field : item.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object fieldValue1 = field.get(oldDeposito);
            Object fieldValue2 = field.get(item);
            if (!fieldValue1.equals(fieldValue2))
                diffs.put("\n" + fieldName + " antigo(a) " + fieldValue1, "\n   " + fieldName + " novo(a)" + fieldValue2 + "\n");
        }

        return diffs.toString();
    }
}


