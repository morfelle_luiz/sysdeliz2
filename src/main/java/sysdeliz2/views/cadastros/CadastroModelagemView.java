package sysdeliz2.views.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.cadastros.CadastroModelagemController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.sysdeliz.SdModelagem;
import sysdeliz2.models.ti.EtqProd;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import java.sql.SQLException;

import static sysdeliz2.utils.LoggerUtils.diferencaDeObjs;

public class CadastroModelagemView extends CadastroModelagemController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    protected ListProperty<EtqProd> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected ListProperty<VSdDadosProduto> produtosEtqBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected ListProperty<EtqProd> produtosModelagemBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    protected ListProperty<EtqProd> produtosGrupoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<EtqProd> tblProdutos = FormTableView.create(EtqProd.class, table -> {
        table.editable.set(false);
        table.title("Produtos");
        table.expanded();
        table.items.bind(produtosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<EtqProd, EtqProd>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(EtqProd item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(320);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGrupo()));
                }).build(), /*Grupo*/
                FormTableColumn.create(cln -> {
                    cln.title("Modelagem");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getModelagem()));
                }).build() /*Modelagem*/
        );
    });
    private final FormTableView<VSdDadosProduto> tblProdutosEtq = FormTableView.create(VSdDadosProduto.class, table -> {
        table.editable.set(false);
        table.title("Produtos");
        table.expanded();
        table.items.bind(produtosEtqBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFamilia()));
                }).build(), /*Família*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));

                }).build(), /*Marca*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build() /*Coleção*/
        );
    });
    private final FormTableView<EtqProd> tblModelagem = FormTableView.create(EtqProd.class, table -> {
        table.editable.set(false);
        table.title("Produtos com essa Modelagem");
        table.expanded();
        table.items.bind(produtosModelagemBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(320);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build() /*Descrição*/
        );
    });
    private final FormTableView<EtqProd> tblGrupo = FormTableView.create(EtqProd.class, table -> {
        table.editable.set(false);
        table.title("Produtos com esse Grupo");
        table.expanded();
        table.items.bind(produtosGrupoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(320);
                    cln.value((Callback<TableColumn.CellDataFeatures<EtqProd, EtqProd>, ObservableValue<EtqProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build() /*Descrição*/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="VBox">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox cadastroTab = (VBox) super.tabs.getTabs().get(2).getContent();

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Navegation">
    private final FormNavegation<EtqProd> navegationEtq = FormNavegation.create(nav -> {
        nav.withNavegation(tblProdutos);
        nav.setMaxWidth(800);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((EtqProd) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarCadastro((EtqProd) nav.selectedItem.get()));
        nav.btnSave(evt -> {
            try {
                salvarCadastro();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((EtqProd) newValue);
            }
        });
    });
    private final FormNavegation<SdGrupoModelagem> navegationGrupo = FormNavegation.create(nav -> {
        nav.withActions(true, true, true);
        nav.btnAddRegister(evt -> novoGrupo());
        nav.btnDeleteRegister(evt -> excluirGrupo((SdGrupoModelagem) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarGrupo());
        nav.btnSave(evt -> {
            try {
                cadastrarGrupo();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        nav.btnCancel(evt -> cancelarGrupo());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDadosGrupo((SdGrupoModelagem) newValue);
            }
        });
    });
    private final FormNavegation<SdModelagem> navegationModelagem = FormNavegation.create(nav -> {
        nav.withActions(true, true, true);
        nav.btnAddRegister(evt -> novaModelagem());
        nav.btnDeleteRegister(evt -> excluirModelagem((SdModelagem) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarModelagem());
        nav.btnSave(evt -> {
            try {
                cadastrarModelagem();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        nav.btnCancel(evt -> cancelarModelagem());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDadosModelagem((SdModelagem) newValue);
            }
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<EtqProd> codigoFilter = FormFieldMultipleFind.create(EtqProd.class, field -> {
        field.title("Código");

    });

    private final FormFieldMultipleFind<SdModelagem> modelagemFilter = FormFieldMultipleFind.create(SdModelagem.class, field -> {
        field.title("Cód. Modelagem");

    });

    private final FormFieldMultipleFind<SdGrupoModelagem> grupoFilter = FormFieldMultipleFind.create(SdGrupoModelagem.class, field -> {
        field.title("Cód. Grupo");

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Codigo");
        field.width(100);
        field.editable.set(false);
    });
    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(300);
        field.toUpper();
        field.editable.bind(navegationEtq.inEdition);
    });
    private final FormFieldText obsField = FormFieldText.create(field -> {
        field.title("Obs");
        field.width(300);
        field.editable.bind(navegationEtq.inEdition);
    });
    private final FormFieldSingleFind<SdModelagem> modelagemField = FormFieldSingleFind.create(SdModelagem.class, field -> {
        field.title("Modelagem");
        field.editable.bind(navegationEtq.inEdition);
        field.width(300);
    });
    private final FormFieldSingleFind<SdGrupoModelagem> grupoField = FormFieldSingleFind.create(SdGrupoModelagem.class, field -> {
        field.title("Grupo");
        field.editable.bind(navegationEtq.inEdition);

    });

    private final FormFieldText cadastroGrupoField = FormFieldText.create(field -> {
        field.title("Grupo");
        field.width(250);
        field.toUpper();
        field.editable.bind(navegationGrupo.inEdition);
        field.alignment(Pos.BOTTOM_RIGHT);
    });
    private final FormFieldText cadastroGrupoCodigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(50);
        field.editable.set(false);
    });

    private final FormFieldText cadastroModelagemField = FormFieldText.create(field -> {
        field.title("Modelagem");
        field.width(250);
        field.toUpper();
        field.editable.bind(navegationModelagem.inEdition);
        field.alignment(Pos.BOTTOM_RIGHT);
    });
    private final FormFieldText cadastroModelagemCodigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(50);
        field.toUpper();
        field.editable.set(false);
    });

    private final FormFieldSingleFind<SdModelagem> findModelagemCadastro = FormFieldSingleFind.create(SdModelagem.class, field -> {
        field.title("Modelagem");
        field.codeReference.set("codigo");
        field.widthCode(300.0);
    });
    private final FormFieldSingleFind<SdGrupoModelagem> findGrupoCadastro = FormFieldSingleFind.create(SdGrupoModelagem.class, field -> {
        field.title("Grupo");
        field.codeReference.set("codigo");
        field.widthCode(300.0);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Dados">
    protected EtqProd oldEtq = new EtqProd();
    protected SdModelagem oldModelagem = new SdModelagem();
    protected SdGrupoModelagem oldGrupo = new SdGrupoModelagem();
    // </editor-fold>

    public CadastroModelagemView() {
        super("Cadastro de Modelagem", ImageUtils.getImage(ImageUtils.Icon.PRODUTO), new String[]{"Listagem", "Manutenção", "Cadastros"});
        initListagem();
        initManutencao();
        initCadastros();
    }

    //<editor-fold desc="Start">
    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(codigoFilter.build(), modelagemFilter.build(), grupoFilter.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            getProdutos(codigoFilter.objectValues.isEmpty() || codigoFilter.textValue.getValue().equals("") ? null : codigoFilter.objectValues.getValue().stream().map(EtqProd::getCodigo).toArray(),
                                    modelagemFilter.objectValues.isEmpty() || modelagemFilter.textValue.getValue().equals("") ? null : modelagemFilter.objectValues.getValue().stream().map(SdModelagem::getModelagem).toArray(),
                                    grupoFilter.objectValues.isEmpty() || grupoFilter.textValue.getValue().equals("") ? null : grupoFilter.objectValues.getValue().stream().map(SdGrupoModelagem::getGrupo).toArray());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                produtosBean.set(FXCollections.observableList(produtos));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        codigoFilter.clear();
                        modelagemFilter.clear();
                        grupoFilter.clear();
                    });
                }));
            }));
            principal.add(tblProdutos.build());
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(navegationEtq);
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.title("Dados Gerais");
            section.vertical();
            section.add(FormBox.create(box -> {
                box.vertical();
                box.add(FormBox.create(dados1 -> {
                    dados1.horizontal();
                    dados1.add(codigoField.build());
                    dados1.add(descricaoField.build());
                    dados1.add(obsField.build());
                }));
                box.add(FormBox.create(dados2 -> {
                    dados2.horizontal();
                    dados2.add(grupoField.build());
                    dados2.add(modelagemField.build());
                }));
            }));
        }));
        manutencaoTab.getChildren().add(tblProdutosEtq.build());
    }

    private void initCadastros() {
        cadastroTab.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.width(900);
                col1.add(navegationGrupo);

                col1.add(FormBox.create(boxCol1 -> {
                    boxCol1.horizontal();
                    boxCol1.width(300);
                    boxCol1.add(FormBox.create(boxGrupo -> {
                        boxGrupo.vertical();
                        boxGrupo.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(findGrupoCadastro.build());
                        }));
                        boxGrupo.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(cadastroGrupoCodigoField.build(), cadastroGrupoField.build());
                            findGrupoCadastro.value.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    getProdutosGrupo(newValue);
                                    produtosGrupoBean.set(FXCollections.observableList(produtosGrupo));
                                    navegationGrupo.selectedItem.set(newValue);
                                }
                            });
                            findGrupoCadastro.disable.bind(navegationGrupo.inEdition);
                        }));
                    }));
                    boxCol1.add(FormBox.create(boxTblGrupo -> {
                        boxTblGrupo.horizontal();
                        boxTblGrupo.add(tblGrupo.build());
                    }));
                }));
            }));

            principal.add(new Separator(Orientation.VERTICAL));

            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.width(900);
                col2.add(navegationModelagem);

                col2.add(FormBox.create(boxCol2 -> {
                    boxCol2.horizontal();
                    boxCol2.width(300);
                    boxCol2.add(FormBox.create(boxModelagem -> {
                        boxModelagem.vertical();
                        boxModelagem.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(findModelagemCadastro.build());
                        }));
                        boxModelagem.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(cadastroModelagemCodigoField.build(), cadastroModelagemField.build());
                            findModelagemCadastro.value.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    getProdutosModelagem(newValue);
                                    produtosModelagemBean.set(FXCollections.observableList(produtosModelagem));
                                    navegationModelagem.selectedItem.set(newValue);
                                }
                            });
                            findModelagemCadastro.disable.bind(navegationModelagem.inEdition);
                        }));
                    }));
                    boxCol2.add(FormBox.create(boxTblModelagem -> {
                        boxTblModelagem.horizontal();
                        boxTblModelagem.add(tblModelagem.build());
                    }));
                }));
            }));
        }));
    }
    //</editor-fold>

    //<editor-fold desc="Etq Prod Functions">

    private void novoCadastro() {
        navegationEtq.inEdition.set(true);
        navegationEtq.selectedItem.set(new EtqProd());
        limparCampos();
        produtosEtqBean.clear();
    }

    private void excluirCadastro(EtqProd etq) {
        if (produtosEtqBean.size() == 0) {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Você deseja excluir a etiqueta?");
                message.showAndWait();
            }).value.get()) {
                oldEtq = getValuesEtq();
                SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.EXCLUIR, oldEtq.getCodigo(), "Etiqueta deletada! -- Código: " + oldEtq.getCodigo() + " -- Descrição: " + oldEtq.getDescricao());
                new FluentDao().delete(etq);
                produtos.remove(etq);
                cancelarCadastro();
                produtosEtqBean.clear();
                MessageBox.create(message -> {
                    message.message("Etiqueta excluída com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        } else {
            MessageBox.create(message -> {
                message.message("Modelagem atrelada a algum produto, não é possível excluir.");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
                message.position(Pos.CENTER);
            });
        }
    }

    private void abrirCadastro(EtqProd etq) {
        navegationEtq.selectedItem.set(etq);
        tabs.getSelectionModel().select(1);
        carregaDados(etq);
    }

    private void carregaDados(EtqProd etq) {
        if (etq != null && etq.getCodigo() != null) {
            codigoField.value.setValue(etq.getCodigo());
            descricaoField.value.setValue(etq.getDescricao());
            obsField.value.setValue(etq.getObs());
            grupoField.value.setValue(etq.getCodGrupo());
            modelagemField.value.setValue(etq.getCodModelagem());

            getProdutosEtiqueta(etq);
            produtosEtqBean.set(FXCollections.observableList(produtosEtiqueta));
        }
    }

    private void editarCadastro(EtqProd etq) {
        navegationEtq.inEdition.set(true);
        oldEtq = getValuesEtq();
        abrirCadastro(etq);
        MessageBox.create(message -> {
            message.message("Ativado modo edição");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarCadastro() throws IllegalAccessException {

        EtqProd etq = getValuesEtq();

        if (grupoField.value.getValue() != null && modelagemField.value.getValue() != null) {
            if (codigoField.value.getValue() == null || codigoField.value.getValue().isEmpty()) {
                try {
                    etq.setCodigo(StringUtils.lpad(Globals.getProximoCodigo("ETQ_PROD", "CODIGO"), 4, "0"));
                    etq = new FluentDao().persist(etq);
                    navegationEtq.selectedItem.set(etq);
                    carregaDados(etq);
                    MessageBox.create(message -> {
                        message.message("Etiqueta salva com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.CADASTRAR, etq.getCodigo(),
                            "Etiqueta Cadastrada -- Código: " + etq.getCodigo() +
                                    " -- Descrição: " + etq.getDescricao());

                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            } else {
                if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Salvar a alteração?");
                    message.showAndWait();
                }).value.get()) {
                    etq = new FluentDao().merge(etq);
                    navegationEtq.selectedItem.set(etq);
                    MessageBox.create(message -> {
                        message.message("Etiqueta alterada com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.EDITAR, etq.getCodigo(),
                            "Etiqueta Alterada -- Código: " + etq.getCodigo() +
                                    " -- Descrição: " + etq.getDescricao() + diferencaDeObjs(etq, oldEtq));

                } else cancelarCadastro();
            }
            refreshEtq();
            navegationEtq.inEdition.set(false);
        } else MessageBox.create(message -> {
            message.message("Campo de Grupo e Modelagem não podem estar vazios!");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.showAndWait();
            message.position(Pos.CENTER);
        });

    }

    private void cancelarCadastro() {
        navegationEtq.inEdition.set(false);
        navegationEtq.selectedItem.set(null);
        limparCampos();
        produtosBean.set(FXCollections.observableList(super.produtos));
        if (!tblProdutos.items.isEmpty()) tblProdutos.selectItem(0);
    }

    private void limparCampos() {
        codigoField.clear();
        descricaoField.clear();
        obsField.clear();
        modelagemField.clear();
        grupoField.clear();
    }

    private EtqProd getValuesEtq() {
        EtqProd values = new EtqProd();
        values.setCodigo(codigoField.value.getValue() == null ? "0" : codigoField.value.getValue());
        values.setDescricao(descricaoField.value.getValue() == null ? "" : descricaoField.value.getValue());
        values.setObs(obsField.value.getValue() == null ? "" : obsField.value.getValue());
        values.setModelagem(modelagemField.value.getValue() == null ? "" : modelagemField.value.getValue().getModelagem());
        values.setGrupo(grupoField.value.getValue() == null ? "" : grupoField.value.getValue().getGrupo());
        values.setCodGrupo(grupoField.value.getValue() == null ? new SdGrupoModelagem() : grupoField.value.getValue());
        values.setCodModelagem(modelagemField.value.getValue() == null ? new SdModelagem() : modelagemField.value.getValue());
        return values;
    }

    private void refreshEtq() {
        tblProdutos.refresh();
        limparCampos();
        carregaDados(navegationEtq.selectedItem.get());
    }

    //</editor-fold>

    //<editor-fold desc="Modelagem Functions">
    private void novaModelagem() {
        navegationModelagem.inEdition.set(true);
        navegationModelagem.selectedItem.set(new SdModelagem());
        cadastroModelagemCodigoField.clear();
        cadastroModelagemField.clear();
        findModelagemCadastro.clear();
    }

    private void carregaDadosModelagem(SdModelagem modelagem) {
        if (modelagem != null) {
            cadastroModelagemField.clear();
            cadastroModelagemCodigoField.clear();
            cadastroModelagemCodigoField.value.setValue(String.valueOf(modelagem.getCodigo()));
            cadastroModelagemField.value.setValue(modelagem.getModelagem());
        }
    }

    private void cadastrarModelagem() throws IllegalAccessException {
        SdModelagem modelagem = navegationModelagem.selectedItem.get();
        modelagem.setModelagem(cadastroModelagemField.value.getValue());

        if (cadastroModelagemCodigoField.value.getValue() == null) {
            try {
                modelagem = new FluentDao().persist(modelagem);
                navegationModelagem.selectedItem.set(modelagem);
                carregaDadosModelagem(getCodigoNewModelagem(modelagem));
                MessageBox.create(message -> {
                    message.message("Modelagem salva com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.CADASTRAR, modelagem.getModelagem(),
                        "Modelagem Cadastrada -- Código: " + modelagem.getCodigo() +
                                " -- Descrição: " + modelagem.getModelagem());
            } catch (PersistenceException e) {
                MessageBox.create(message -> {
                    message.message("Modelagem com esse nome já existente!");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.position(Pos.CENTER);
                    message.showAndWait();
                });
                JPAUtils.clearEntity(oldModelagem);
                carregaDadosModelagem(oldModelagem);
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
                JPAUtils.clearEntity(oldModelagem);
                carregaDadosModelagem(oldModelagem);
            }
        } else {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Salvar a alteração?");
                message.showAndWait();
            }).value.get()) {
                try {
                    if (produtosModelagem.size() > 0) {
                        for (EtqProd etq : produtosModelagem) {
                            etq.setModelagem(modelagem.getModelagem());
                            etq.setCodModelagem(modelagem);
                            new FluentDao().merge(etq);
                        }
                        refreshEtq();
                    }
                    modelagem = new FluentDao().merge(modelagem);
                    navegationModelagem.selectedItem.set(modelagem);
                    carregaDadosModelagem(modelagem);
                    MessageBox.create(message -> {
                        message.message("Modelagem alterada com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.EDITAR, modelagem.getModelagem(),
                            "Etiqueta Alterada -- Código: " + modelagem.getCodigo() +
                                    " -- Descrição: " + modelagem.getModelagem() + diferencaDeObjs(modelagem, oldModelagem));
                } catch (RollbackException e) {
                    MessageBox.create(message -> {
                        message.message("Modelagem com esse nome já existente!");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.position(Pos.CENTER);
                        message.showAndWait();
                    });
                    carregaDadosModelagem(oldModelagem);
                }
            } else {
                carregaDadosModelagem(oldModelagem);
            }
        }
        tblModelagem.refresh();
        navegationModelagem.inEdition.set(false);
    }

    private void excluirModelagem(SdModelagem modelagem) {
        if (navegationModelagem.selectedItem.get() != null) {
            if (!produtosModelagem.isEmpty())
                MessageBox.create(message -> {
                    message.message("Modelagem atrelada a algum produto, impossível a exclusão.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                    message.position(Pos.CENTER);
                });
            else {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Tem Certeza que deseja excluir a modelagem : " + modelagem.getModelagem() + " ?");
                    message.showAndWait();
                }).value.get())) {
                    new FluentDao().delete(modelagem);

                    MessageBox.create(message -> {
                        message.message("Modelagem " + modelagem.getModelagem() + " excluída!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Tela Cadastro de Modelagem", TipoAcao.EXCLUIR, modelagem.getModelagem(),
                            "Modelagem " + modelagem.getModelagem() + " excluída da tabela de Modelagens");
                }
                cadastroModelagemCodigoField.clear();
                cadastroModelagemField.clear();
                findModelagemCadastro.clear();
            }
        }
    }

    private void editarModelagem() {
        if (navegationModelagem.selectedItem.get() != null) {
            navegationModelagem.inEdition.set(true);
            oldModelagem = getValuesModelagem();
            MessageBox.create(message -> {
                message.message("Ativado modo edição");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private SdModelagem getValuesModelagem() {
        SdModelagem modelagem = new SdModelagem();
        modelagem.setCodigo(cadastroModelagemCodigoField.value.getValue() == null ? 0 : Integer.parseInt(cadastroModelagemCodigoField.value.getValue()));
        modelagem.setModelagem(cadastroModelagemField.value.getValue() == null ? "" : cadastroModelagemField.value.getValue());
        return modelagem;
    }

    private void cancelarModelagem() {
        navegationModelagem.selectedItem.set(null);
        navegationModelagem.inEdition.set(false);
    }

    //</editor-fold>

    //<editor-fold desc="Grupo Functions">
    private void novoGrupo() {
        navegationGrupo.inEdition.set(true);
        navegationGrupo.selectedItem.set(new SdGrupoModelagem());
        findGrupoCadastro.clear();
        cadastroGrupoField.clear();
        cadastroGrupoCodigoField.clear();
    }

    private void carregaDadosGrupo(SdGrupoModelagem grupo) {
        if (grupo != null) {
            cadastroGrupoField.clear();
            cadastroGrupoCodigoField.clear();
            cadastroGrupoCodigoField.value.setValue(String.valueOf(grupo.getCodigo()));
            cadastroGrupoField.value.setValue(grupo.getGrupo());
        }
    }

    private void cadastrarGrupo() throws IllegalAccessException {

        SdGrupoModelagem grupo = navegationGrupo.selectedItem.get();
        grupo.setGrupo(cadastroGrupoField.value.getValue());

        if (cadastroGrupoCodigoField.value.getValue() == null) {
            try {
                grupo = new FluentDao().persist(grupo);
                navegationGrupo.selectedItem.set(grupo);
                carregaDadosGrupo(getCodigoNewGrupo(grupo));
                MessageBox.create(message -> {
                    message.message("Grupo salvo com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.CADASTRAR, grupo.getGrupo(),
                        "Grupo Cadastrado -- Código: " + grupo.getCodigo() +
                                " -- Descrição: " + grupo.getGrupo());
            } catch (PersistenceException e) {
                MessageBox.create(message -> {
                    message.message("Grupo com esse nome já existente!");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.position(Pos.CENTER);
                    message.showAndWait();
                });
                JPAUtils.clearEntity(oldGrupo);
                carregaDadosGrupo(oldGrupo);
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
                JPAUtils.clearEntity(oldGrupo);
                carregaDadosGrupo(oldGrupo);
            }
        } else {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Salvar a alteração?");
                message.showAndWait();
            }).value.get()) {
                try {
                    if (produtosGrupo.size() > 0) {
                        for (EtqProd etq : produtosGrupo) {
                            etq.setGrupo(grupo.getGrupo());
                            etq.setCodGrupo(grupo);
                            new FluentDao().merge(etq);
                        }
                        refreshEtq();
                    }
                    grupo = new FluentDao().merge(grupo);
                    navegationGrupo.selectedItem.set(grupo);
                    MessageBox.create(message -> {
                        message.message("Grupo alterado com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Cadastro de Modelagem", TipoAcao.EDITAR, grupo.getGrupo(),
                            "Etiqueta Alterada -- Código: " + grupo.getCodigo() +
                                    " -- Descrição: " + grupo.getGrupo() + diferencaDeObjs(grupo, oldGrupo));
                } catch (RollbackException e) {
                    MessageBox.create(message -> {
                        message.message("Grupo com esse nome já existente!");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.position(Pos.CENTER);
                        message.showAndWait();
                    });
                    carregaDadosGrupo(oldGrupo);
                }
            } else {
                carregaDadosGrupo(oldGrupo);
            }
        }
        navegationGrupo.inEdition.set(false);
    }

    private void excluirGrupo(SdGrupoModelagem grupo) {
        if (navegationGrupo.selectedItem.get() != null) {
            if (!produtosGrupo.isEmpty())
                MessageBox.create(message -> {
                    message.message("Grupo atrelado a algum produto, impossível a exclusão.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                    message.position(Pos.CENTER);
                });
            else {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Tem Certeza que deseja excluir o grupo : " + grupo.getGrupo() + " ?");
                    message.showAndWait();
                }).value.get())) {
                    new FluentDao().delete(grupo);

                    MessageBox.create(message -> {
                        message.message("Grupo " + grupo.getGrupo() + " excluído!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    SysLogger.addSysDelizLog("Tela Cadastro de Modelagem", TipoAcao.EXCLUIR, grupo.getGrupo(),
                            "Grupo " + grupo.getGrupo() + " excluído da tabela de Grupos");
                    cadastroGrupoCodigoField.clear();
                    cadastroGrupoField.clear();
                    findGrupoCadastro.clear();
                    refreshEtq();
                }
            }
        }
        navegationGrupo.inEdition.set(false);
    }

    private void editarGrupo() {
        if (navegationGrupo.selectedItem.get() != null) {
            navegationGrupo.inEdition.set(true);
            oldGrupo = getValuesGrupo();
            MessageBox.create(message -> {
                message.message("Ativado modo edição");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private SdGrupoModelagem getValuesGrupo() {
        SdGrupoModelagem grupo = new SdGrupoModelagem();
        grupo.setCodigo(cadastroGrupoCodigoField.value.getValue() == null ? 0 : Integer.parseInt(cadastroGrupoCodigoField.value.getValue()));
        grupo.setGrupo(cadastroGrupoField.value.getValue() == null ? "" : cadastroGrupoField.value.getValue());
        return grupo;
    }

    private void cancelarGrupo() {
        navegationGrupo.selectedItem.set(null);
        navegationGrupo.inEdition.set(false);
    }

    //</editor-fold>
}
