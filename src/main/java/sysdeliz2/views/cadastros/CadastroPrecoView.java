package sysdeliz2.views.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.*;
import sysdeliz2.controllers.views.cadastros.CadastroPrecoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CadastroPrecoView extends CadastroPrecoController {

    private List<Regiao> listRegioes = new ArrayList<>();
    private List<TabPreco> tabsImportadas = new ArrayList<>();
    private BigDecimal valorAntigo = BigDecimal.ZERO;

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<VSdDadosProduto> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdDadosProduto> tblProdutos = FormTableView.create(VSdDadosProduto.class, table -> {
        table.title("Produtos");
        table.expanded();
        table.items.bind(produtosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(280);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFamilia()));
                }).build(), /*CodFam*/
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinha()));
                }).build(), /*Família*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                }).build() /*Marca*/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<VSdDadosProduto> codigoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Código");
        field.width(250);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(250);
    });
    private final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(250);
    });
    private final FormFieldMultipleFind<Linha> linhaFilter = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha");
        field.width(250);
    });
    private final FormFieldMultipleFind<Familia> familiaFilter = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Familia");
        field.width(250);
    });
    private final FormFieldMultipleFind<Regiao> regioesFilter = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Tabelas de Preço");
        field.width(250);
        field.toUpper();
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button btnAdicionarTabelasPreco = FormButton.create(btn -> {
        btn.setText("Adicionar");
        btn.addStyle("success");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> {
            adicionarTabelas(regioesFilter.objectValues);
        });
    });
    private Button btnImportarExcel = FormButton.create(btn -> {
        btn.setText("Importar Excel");
        btn.addStyle("success");
        btn.tooltip("codProduto - codTabela - Preço");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            importExcel();
        });
    });
    private Button btnRemoverTodasRegioes = FormButton.create(btn -> {
        btn.title("Remover todas as Regiões");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.addStyle("danger");
        btn.setOnAction(evt -> {
            tblProdutos.removeColumn(5, listRegioes.size() + 5);
            listRegioes.clear();
        });
    });
    private Button btnLimparTabela = FormButton.create(btn -> {
        btn.title("Limpar Produtos");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
        btn.addStyle("primary");
        btn.setOnAction(evt -> {
            produtos.clear();
            tblProdutos.clear();
        });
    });

    private Button btnConfirmarImport = FormButton.create(btn -> {
        btn.title("Confirmar Importação");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.disableProperty().unbind();
        btn.setDisable(true);
        btn.setOnAction(evt -> {
            salvarTabelasImportadas();
        });
    });
    // </editor-fold>

    public CadastroPrecoView() {
        super("Cadastro de Preços", ImageUtils.getImage(ImageUtils.Icon.PRODUTO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(marcaFilter.build(), codigoFilter.build(), colecaoFilter.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(familiaFilter.build(), linhaFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarProdutos(marcaFilter.objectValues.isEmpty() ? null : marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        colecaoFilter.objectValues.isEmpty() ? null : colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                        linhaFilter.objectValues.isEmpty() ? null : linhaFilter.objectValues.stream().map(Linha::getCodigo).toArray(),
                                        familiaFilter.objectValues.isEmpty() ? null : familiaFilter.objectValues.stream().map(Familia::getCodigo).toArray(),
                                        codigoFilter.objectValues.isEmpty() ? null : codigoFilter.objectValues.stream().map(VSdDadosProduto::getCodigo).toArray()

                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    produtosBean.set(FXCollections.observableArrayList(produtos));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            marcaFilter.clear();
                            colecaoFilter.clear();
                            linhaFilter.clear();
                            familiaFilter.clear();
                            codigoFilter.clear();
                            tabsImportadas.clear();
                        });
                    }));
                }));
                boxHeader.add(FormBox.create(boxAddRegiao -> {
                    boxAddRegiao.vertical();
                    boxAddRegiao.alignment(Pos.BOTTOM_LEFT);
                    boxAddRegiao.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.title("Adicionar Tabelas");
                        linha.alignment(Pos.BOTTOM_LEFT);
                        linha.add(regioesFilter.build(), btnAdicionarTabelasPreco);
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblProdutos.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(btnImportarExcel, btnRemoverTodasRegioes, btnLimparTabela, btnConfirmarImport);
            }));
        }));
    }

    private void adicionarTabelas(List<Regiao> regioes) {
        for (Regiao regiao : regioes) {
            if (listRegioes.stream().noneMatch(it -> it.equals(regiao))) {
                if (listRegioes.size() > 7) {
                    MessageBox.create(message -> {
                        message.message("Limite de Tabelas por vez excedido!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                    return;
                }
                listRegioes.add(regiao);
                tblProdutos.addColumn(criarColunas(regiao));
            }
        }
    }

    private FormTableColumn criarColunas(Regiao regiao) {
        return FormTableColumn.create(cln -> {
            cln.title(regiao.getDescricao());
            cln.setId(regiao.getRegiao());
            cln.width(150);
            cln.alignment(Pos.CENTER_LEFT);
            cln.setGraphic(FormButton.create(button -> {
                button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                button.addStyle("danger");
                button.tooltip("Remover coluna");
                button.setMaxHeight(10.0);
                button.setMaxWidth(10.0);
                button.setAlignment(Pos.CENTER_LEFT);
                button.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");
                button.setOnAction(evt -> {
                    tblProdutos.removeColumn(cln);
                    listRegioes.remove(listRegioes.stream().filter(it -> it.getRegiao().equals(cln.getId())).findFirst().get());
                });
            }));
            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
            cln.format(param -> {
                return new TableCell<VSdDadosProduto, VSdDadosProduto>() {
                    @Override
                    protected void updateItem(VSdDadosProduto item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (!empty) {
                            setGraphic(FormFieldText.create(field -> {
                                field.editable(true);
                                field.width(100);
                                field.withoutTitle();
                                TabPreco tabPreco = getTabPreco(item, regiao);
                                field.value.setValue(StringUtils.toMonetaryFormat(tabPreco.getPreco00(), 2));
                                field.keyPressed(event -> {
                                    if (event.getCode().equals(KeyCode.ENTER)) {
                                        try {
                                            Robot robot = new Robot();
                                            robot.keyPress(9);
                                        } catch (AWTException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                field.focusedListener((observable, oldValue, newValue) -> {
                                    if (newValue) {
                                        valorAntigo = new BigDecimal(field.value.getValue().replaceAll(",", ".").replace("R$", "").trim());
                                    } else {
                                        BigDecimal valorNovo = new BigDecimal(field.value.getValue().replaceAll(",", ".").replace("R$", "").trim());
                                        if (!valorNovo.equals(valorAntigo)) {
                                            salvarPreco(item, valorNovo, tabPreco);
                                            field.value.setValue(StringUtils.toMonetaryFormat(tabPreco.getPreco00(), 2));
                                        }
                                    }
                                });
                            }).build());
                        }
                    }
                };
            });
        });
    }

    private void salvarPreco(VSdDadosProduto item, BigDecimal valorNovo, TabPreco tabPreco) {
        tabPreco.setPreco00(valorNovo);
        tabPreco.setData(LocalDate.now());
        tabPreco.getId().setCodigo(item);
        tabPreco.setDescProduto(item.getDescricao());
        SysLogger.addSysDelizLog("Cadastro de Preços", TipoAcao.CADASTRAR, item.getCodigo(), "Preço: " + StringUtils.toMonetaryFormat(tabPreco.getPreco00(), 3) + " do produto: " + item.getCodigo() + " " + item.getDescricao() +
                " Cadastrado na tabela " + tabPreco.getDescricao());
        new FluentDao().merge(tabPreco);
    }

    protected void importExcel() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir Arquivo");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel File", "*.xls*", "*.xlsx"));
        List<Regiao> regioesToAdd = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);

        AtomicReference<Workbook> workbook = new AtomicReference<>();
        File fileToOpen = fileChooser.showOpenDialog(new Stage());
        if (fileToOpen == null) {
            return;
        }

        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                File file = new File(fileToOpen.getAbsolutePath());
                workbook.set(WorkbookFactory.create(new FileInputStream(file)));
                Sheet sheet = workbook.get().getSheetAt(0);
                Iterator<Row> rowIterator = sheet.rowIterator();
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    String codProduto = null;
                    String codTabela = null;
                    String preco = null;

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        switch (cell.getColumnIndex()) {
                            case 0:
                                if (cell.getCellType().equals(CellType.STRING)) codProduto = cell.getStringCellValue();
                                else codProduto = String.valueOf(cell.getNumericCellValue());
                                break;
                            case 1:
                                if (cell.getCellType().equals(CellType.STRING)) codTabela = cell.getStringCellValue();
                                else codTabela = String.valueOf((int) cell.getNumericCellValue());
                                break;
                            case 2:
                                if (cell.getCellType().equals(CellType.STRING)) preco = cell.getStringCellValue();
                                else preco = df.format(cell.getNumericCellValue());
                                break;
                        }
                    }

                    if (codProduto != null && codTabela != null && preco != null) {
                        VSdDadosProduto produto = buscarProdutoPorcodigo(codProduto);
                        Regiao regiao = buscarRegiao(codTabela);

                        if (produto != null && regiao != null) {
                            TabPreco tab = new TabPreco(produto, regiao);
                            BigDecimal bg = new BigDecimal(preco.replace(",", "."));
                            tab.setPreco00(bg);
                            produto.getListTabPreco().add(tab);
                            tabsImportadas.add(tab);
                            if (!produtos.contains(produto)) produtos.add(produto);
                            if (!listRegioes.contains(regiao)) regioesToAdd.add(regiao);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                try {
                    produtosBean.set(FXCollections.observableArrayList(produtos));
                    adicionarTabelas(regioesToAdd);
                    if (tabsImportadas.size() > 0) btnConfirmarImport.setDisable(false);
                    workbook.get().close();
                    MessageBox.create(message -> {
                        message.message("Excel importado com sucesso");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void salvarTabelasImportadas() {
        new RunAsyncWithOverlay(this).exec(task -> {
            for (TabPreco tab : tabsImportadas) {
                new FluentDao().merge(tab);
                SysLogger.addSysDelizLog("Cadastro de Preços", TipoAcao.CADASTRAR, tab.getId().getCodigo().getCodigo(), "Preço: " + StringUtils.toMonetaryFormat(tab.getPreco00(), 3) + " do produto: " + tab.getId().getCodigo().getCodigo() + " " + tab.getId().getCodigo().getDescricao() +
                        " Cadastrado na tabela " + tab.getDescricao());
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tabsImportadas.clear();
                btnConfirmarImport.setDisable(true);
                MessageBox.create(message -> {
                    message.message("Preços importados com sucesso");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }
}
