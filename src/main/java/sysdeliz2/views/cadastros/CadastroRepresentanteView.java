package sysdeliz2.views.cadastros;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.cadastros.CadastroRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdColecaoMarcaRepresen;
import sysdeliz2.models.sysdeliz.SdMarcasRepresen;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.LoggerUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CadastroRepresentanteView extends CadastroRepresentanteController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<Represen> representantesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private Represen representanteSelecionado = new Represen();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Dados">
    protected Represen oldRepresen = new Represen();
    protected boolean cnpjValido = false;
    public final BooleanProperty liberarCampos = new SimpleBooleanProperty(false);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<Represen> tblRepresentantes = FormTableView.create(Represen.class, table -> {
        table.title("Representantes");
        table.expanded();
        table.items.bind(representantesBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Represen, Represen>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(Represen item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodRep()));

                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(310);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));

                }).build(), /*Nome*/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));

                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Responsável");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRespon()));
                }).build(), /*Responsável*/
                FormTableColumn.create(cln -> {
                    cln.title("CNPJ/CPF");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(
                            param.getValue().getCnpj() != null ? param.getValue().getCnpj() : param.getValue().getCpf()));

                }).build(), /*CNPJ*/
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Represen, Represen>() {
                            @Override
                            protected void updateItem(Represen item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item.isAtivo())
                                        setGraphic(ImageUtils.getIconButton(ImageUtils.ButtonIcon.ACTIVE, ImageUtils.IconSize._16));
                                    else
                                        setGraphic(ImageUtils.getIconButton(ImageUtils.ButtonIcon.INACTIVE, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(), /*Ativo*/
                FormTableColumn.create(cln -> {
                    cln.title("Endereço");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEndereco()));

                }).build(), /*Endereço*/
                FormTableColumn.create(cln -> {
                    cln.title("CEP");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCep()));
                    cln.format(param -> {
                        return new TableCell<Represen, CadCep>() {
                            @Override
                            protected void updateItem(CadCep item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getCep());
                                }
                            }
                        };
                    });
                }).build(), /*CEP*/
                FormTableColumn.create(cln -> {
                    cln.title("Telefone");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTelefone()));

                }).build() /*Telefone*/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="VBox">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<Represen> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblRepresentantes);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((Represen) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> {
            editarCadastro((Represen) nav.selectedItem.get());
        });
        nav.btnSave(evt -> {
            salvarCadastro();
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                representanteSelecionado = (Represen) newValue;
                carregaDados(representanteSelecionado);
            }
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Represen> codRepFilter = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Código Representante");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.equals("")) field.clear();
        });
    });
    protected final FormFieldText nomeResponFilter = FormFieldText.create(field -> {
        field.title("Nome Responsável");
        field.width(250);
        field.toUpper();
        field.value.addListener((observable, oldValue, newValue) -> {
            if (representantes.size() > 0 && newValue != null) {
                if (oldValue != null && oldValue.length() > newValue.length()) {
                    representantesBean.set(FXCollections.observableList(representantes));
                }
                representantesBean.set(FXCollections.observableList(
                        representantesBean.stream()
                                .filter(entry -> entry.getRespon() != null && !entry.getRespon().isEmpty())
                                .filter(entry -> entry.getRespon()
                                        .contains(newValue))
                                .collect(Collectors.toList())));
            }
        });
    });
    protected final FormFieldText nomeRepFilter = FormFieldText.create(field -> {
        field.title("Nome Representante");
        field.width(250);
        field.toUpper();
        field.value.addListener((observable, oldValue, newValue) -> {
            if (representantes.size() > 0 && newValue != null) {
                if (oldValue != null && oldValue.length() > newValue.length()) {
                    representantesBean.set(FXCollections.observableList(representantes));
                }
                representantesBean.set(FXCollections.observableList(
                        representantesBean.stream()
                                .filter(entry -> entry.getNome() != null && !entry.getNome().isEmpty())
                                .filter(entry -> entry.getNome()
                                        .contains(newValue))
                                .collect(Collectors.toList())));
            }
        });
    });
    private final FormFieldSegmentedButton<String> tipoSegBtn = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo");
        field.options(
                field.option("TODOS", "T", FormFieldSegmentedButton.Style.WARNING),
                field.option("1", "1", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("2", "2", FormFieldSegmentedButton.Style.SECUNDARY)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> ativoSegBtn = FormFieldSegmentedButton.create(field -> {
        field.title("Ativo");
        field.options(
                field.option("TODOS", "T", FormFieldSegmentedButton.Style.WARNING),
                field.option("SIM", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("NÃO", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">
    private final FormFieldText codRepField = FormFieldText.create(field -> {
        field.title("Código Representante");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.maxLength(5);
        field.editable.set(false);
    });
    private final FormFieldToggle ativoField = FormFieldToggle.create(field -> {
        field.title("Ativo");
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText nomeRepField = FormFieldText.create(field -> {
        field.title("Nome");
        field.width(350);
        field.maxLength(100);
        field.toUpper();
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText nomeResponField = FormFieldText.create(field -> {
        field.title("Nome Responsável");
        field.width(300);
        field.maxLength(40);
        field.toUpper();
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText cnpjField = FormFieldText.create(field -> {
        field.title("CNPJ/CPF");
        field.width(250);
        field.mask(FormFieldText.Mask.CNPJCPF);
        field.maxLength(18);
        field.editable.bind(navegation.inEdition);
        field.value.setValue("");
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && oldValue != null) {
                if (newValue.length() < oldValue.length()) {
                    liberarCampos.set(false);
                    cnpjValido = false;
                }
                if (newValue.equals("")) {
                    limparCampos();
                }
            }
        });
    });
    private final FormFieldText cepField = FormFieldText.create(field -> {
        field.title("CEP");
        field.maxLength(8);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText enderecoField = FormFieldText.create(field -> {
        field.title("Endereço");
        field.width(350);
        field.toUpper();
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText inscricaoField = FormFieldText.create(field -> {
        field.title("Inscrição");
        field.maxLength(18);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText regiaoField = FormFieldText.create(field -> {
        field.title("Região");
        field.width(100);
        field.maxLength(5);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText bairroField = FormFieldText.create(field -> {
        field.title("Bairro");
        field.width(200);
        field.maxLength(35);
        field.toUpper();
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText emailField = FormFieldText.create(field -> {
        field.title("E-Mail");
        field.mask(FormFieldText.Mask.EMAIL);
        field.width(300);
        field.maxLength(100);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText telefoneField = FormFieldText.create(field -> {
        field.title("Telefone");
//        field.maxLength(18);
//        field.mask(FormFieldText.Mask.PHONE);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText celularField = FormFieldText.create(field -> {
        field.title("Celular");
        field.maxLength(18);
        field.mask(FormFieldText.Mask.PHONE);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText comissaoFatField = FormFieldText.create(field -> {
        field.title("Comissão Fat");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.width(150);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText comissaoRecField = FormFieldText.create(field -> {
        field.title("Comissão Rec");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.width(150);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText comissaoFat2Field = FormFieldText.create(field -> {
        field.title("Comissão Fat 2");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.width(150);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText comissaoRec2Field = FormFieldText.create(field -> {
        field.title("Comissão Rec 2");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.width(150);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldDate dtoNascField = FormFieldDate.create(field -> {
        field.title.setText("Data de Nascimento");
        field.editable.bind(liberarCampos);
    });
    private final FormFieldSingleFind<Entidade> codCliField = FormFieldSingleFind.create(Entidade.class, field -> {
        field.title("CodCli");
        field.width(350);
        field.maxLength(5);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldComboBox<String> tipoField = FormFieldComboBox.create(String.class, field -> {
        field.title("Tipo");
        field.editable.bind(liberarCampos);
        field.items(FXCollections.observableArrayList(Arrays.asList("1","2")));
    });
    private final FormFieldDate admissaoField = FormFieldDate.create(field -> {
        field.title("Data de Admissão");
        field.editable.bind(liberarCampos);
    });
    private final FormFieldComboBox<String> grupoField = FormFieldComboBox.create(String.class, field -> {
        field.title("Grupo");
        field.editable.bind(liberarCampos);
        field.items(FXCollections.observableArrayList(Arrays.asList("2","3")));
    });
    private final FormFieldText obsField = FormFieldText.create(field -> {
        field.title("Observação");
        field.width(300);
        field.editable.bind(liberarCampos);
    });
    private final FormFieldText impRendaField = FormFieldText.create(field -> {
        field.title("Imposto de Renda");
        field.editable.bind(liberarCampos);
    });
    private final FormFieldMultipleFind<Marca> newMarcaField = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Código Marca");
        field.editable.bind(liberarCampos);
        field.toUpper();
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TabPane">
    private final FormTabPane tabPane = FormTabPane.create(pane -> {
        pane.addTab();
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final Button btnAddMarca = FormButton.create(btn -> {
        btn.addStyle("sm").addStyle("success");
        btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEW_REGISTER, ImageUtils.IconSize._16));
        btn.title("Adicionar");
        btn.disable.bind(liberarCampos.not().or(nomeRepField.value.isEmpty()));
        newMarcaField.code.setOnKeyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)){
                btn.fire();
            }
        });
        btn.setOnAction(evt -> {
            if (!newMarcaField.objectValues.getValue().isEmpty() && !newMarcaField.textValue.getValue().isEmpty()) {
                adicionarMarca();
                newMarcaField.clear();
            }

        });
    });
    private final Button btnValidarCnpj = FormButton.create(btn -> {
        btn.addStyle("sm").addStyle("secundary");
        btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
        btn.title("Buscar");
        btn.disable.bind(navegation.inEdition.not());
        btn.setOnAction(evt -> {
            String codigo = cnpjField.value.getValue();
            if (codigo != null && (codigo.length() == 18 || codigo.length() == 14) && !cnpjValido) {
                buscarRepresentanteExistente(codigo);
                MessageBox.create(message -> {
                    message.message("PARA CADASTRAR AS MARCAS, PRIMEIRO INSIRA UM NOME PARA O REPRESENTANTE!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    });
    // </editor-fold>

    public CadastroRepresentanteView() {
        super("Cadastro de Representante", ImageUtils.getImage(ImageUtils.Icon.REPRESENTANTE), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(box -> {
                box.horizontal();
                box.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.horizontal();
                        boxFilter.add(codRepFilter.build(), nomeRepFilter.build(), nomeResponFilter.build(), ativoSegBtn.build(), tipoSegBtn.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            getRepresentantes(codRepFilter.objectValues.getValue().stream().map(Represen::getCodRep).toArray(),
                                    nomeResponFilter.value.getValue(),
                                    nomeRepFilter.value.getValue(),
                                    ativoSegBtn.value.getValue(),
                                    tipoSegBtn.value.getValue());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                representantesBean.set(FXCollections.observableList(representantes));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        codRepFilter.clear();
                        nomeResponFilter.value.setValue("");
                        nomeRepFilter.clear();
                        ativoSegBtn.select(0);
                        tipoSegBtn.select(0);
                    });
                }));
            }));
            principal.add(tblRepresentantes.build());
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(FormBoxPane.create(content -> {
            content.top(navegation);
            content.center(FormBox.create(principal -> {
                principal.horizontal();
                principal.expanded();
                principal.add(FormBox.create(col1 -> {
                    col1.vertical();
                    col1.expanded();
                    col1.width(800);
                    col1.add(FormBox.create(boxDadosPrincipais -> {
                        boxDadosPrincipais.vertical();
                        boxDadosPrincipais.title("Dados Principais");
                        boxDadosPrincipais.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.alignment(Pos.BOTTOM_LEFT);
                            box1.add(cnpjField.build(), btnValidarCnpj, ativoField.build());
                        }));
                        boxDadosPrincipais.add(FormBox.create(box2 -> {
                            box2.horizontal();
                            box2.add(codRepField.build(), nomeRepField.build(), nomeResponField.build());
                        }));
                        boxDadosPrincipais.add(FormBox.create(box3 -> {
                            box3.horizontal();
                            box3.add(inscricaoField.build(), codCliField.build(), obsField.build());
                        }));
                    }));
                    col1.add(FormBox.create(dadosComplementares -> {
                        dadosComplementares.vertical();
                        dadosComplementares.title("Dados Complementares");
                        dadosComplementares.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(tipoField.build(), grupoField.build(), impRendaField.build());
                        }));
                        dadosComplementares.add(FormBox.create(box2 -> {
                            box2.title("Datas");
                            box2.horizontal();
                            box2.add(dtoNascField.build(), admissaoField.build());
                        }));
                    }));
                    col1.add(FormBox.create(boxComissao -> {
                        boxComissao.vertical();
                        boxComissao.title("Comissão");
                        boxComissao.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(comissaoFatField.build(), comissaoFat2Field.build(), comissaoRecField.build(), comissaoRec2Field.build());
                        }));
                    }));
                    col1.add(FormBox.create(boxDadosContato -> {
                        boxDadosContato.vertical();
                        boxDadosContato.add(FormBox.create(boxEndereco -> {
                            boxEndereco.title("Endereço");
                            boxEndereco.horizontal();
                            boxEndereco.add(FormBox.create(box1 -> {
                                box1.horizontal();
                                box1.add(cepField.build(), enderecoField.build(), bairroField.build(), regiaoField.build());
                            }));
                        }));
                        boxDadosContato.add(FormBox.create(boxContato -> {
                            boxContato.horizontal();
                            boxContato.title("Contato");
                            boxContato.add(FormBox.create(box1 -> {
                                box1.horizontal();
                                box1.add(emailField.build(), celularField.build(), telefoneField.build());
                            }));
                        }));
                    }));
                }));
                principal.add(FormBox.create(col2 -> {
                    col2.vertical();
                    col2.expanded();
                    col2.border();
                    col2.add(FormBox.create(boxAddMarca -> {
                        boxAddMarca.alignment(Pos.BOTTOM_LEFT);
                        boxAddMarca.title("Marcas e Coleções");
                        boxAddMarca.horizontal();
                        boxAddMarca.add(newMarcaField.build(), btnAddMarca);
                    }));
                    col2.add(FormBox.create(boxTab -> {
                        boxTab.vertical();
                        boxTab.add(tabPane);
                    }));
                }));
            }));
        }));
    }

    private void novoCadastro() {
        navegation.selectedItem.setValue(new Represen());
        limparCampos();
        navegation.inEdition.set(true);
        cnpjValido = false;
    }

    private void abrirCadastro(Represen representante) {
        navegation.selectedItem.set(representante);
        tabs.getSelectionModel().select(1);
        carregaDados(representante);
    }

    private void editarCadastro(Represen representante) {
        if (representante != null) {
            navegation.inEdition.set(true);
            liberarCampos.setValue(true);
            oldRepresen = representanteSelecionado.clone();
            abrirCadastro(representante);
            MessageBox.create(message -> {
                message.message("Ativado modo edição");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void salvarCadastro() {
        if (camposObrigatorios().length() > 0) {
            MessageBox.create(message -> {
                message.message("Antes de concluir preencha os campos a seguir: \n" + camposObrigatorios());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
                message.position(Pos.CENTER);
            });
        } else {
            getValoresCampos();
            if(representanteSelecionado.getTelefone() != null && representanteSelecionado.getTelefone().length() > 18) {
                MessageBox.create(message -> {
                    message.message("Campo \"Telefone\" possui caracteres demais, o tamanho máximo é 18.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            if(representanteSelecionado.getCelular() != null && representanteSelecionado.getCelular().length() > 18) {
                MessageBox.create(message -> {
                    message.message("Campo \"Celular\" possui caracteres demais, o tamanho máximo é 15.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            representanteSelecionado = salvarRepresentante(representanteSelecionado);

//            logsColecaoMarca();
            navegation.selectedItem.setValue(representanteSelecionado);
            navegation.inEdition.set(false);
            liberarCampos.set(false);

        }
    }

    private Represen salvarRepresentante(Represen representante) {
        try {
            if (representante.getCodRep() == null) {
                representante.setCodRep(Globals.getProximoCodigo("REPRESEN", "CODREP"));
            }
            representante = new FluentDao().merge(representante);
            SysLogger.addSysDelizLog("Cadastro de Representante", TipoAcao.CADASTRAR, representante.getCodRep(),
                    "Representante atualizado -- Código: " + representante.getCodRep() + LoggerUtils.diferencaDeObjs(representante, oldRepresen));
            MessageBox.create(message -> {
                message.message("Atualização de cadastro de representante concluída com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.show();
                message.position(Pos.TOP_RIGHT);
            });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return representante;
    }

    private String camposObrigatorios() {
        StringBuilder camposVazios = new StringBuilder();
        if (nomeRepField.value.getValue() == null || nomeRepField.value.getValue().equals("")) {
            camposVazios.append("- Nome do Representante\n");
        }
        if (nomeResponField.value.getValue() == null || nomeResponField.value.getValue().equals("")) {
            camposVazios.append("- Nome do Responsável\n");
        }
        if (inscricaoField.value.getValue() == null || inscricaoField.value.getValue().equals("")) {
            camposVazios.append("- Inscrição\n");
        }
        if (tipoField.value.getValue() == null || tipoField.value.getValue().equals("")) {
            camposVazios.append("- Tipo\n");
        }
        if (cepField.value.getValue() == null || cepField.value.getValue().equals("")) {
            camposVazios.append("- CEP\n");
        }
        if (cepField.value.getValue() != null || !cepField.value.getValue().equals("")) {
            CadCep cep = new FluentDao().selectFrom(CadCep.class).where(eb -> eb.equal("cep", cepField.value.getValue())).findAny();
            if (cep == null) {
                camposVazios.append("- CEP INEXISTENTE\n");
            }
        }
        if (enderecoField.value.getValue() == null || enderecoField.value.getValue().equals("")) {
            camposVazios.append("- Endereço\n");
        }

        return camposVazios.toString();
    }

    private void carregaDados(Represen representante) {
        if (representante != null) {

            cnpjValido = true;
            cnpjField.value.setValue(representante.getCnpj() == null ? representante.getCpf() : representante.getCnpj());
            ativoField.value.setValue(representante.isAtivo());
            codRepField.value.setValue(representante.getCodRep());
            nomeRepField.value.setValue(representante.getNome());
            nomeResponField.value.setValue(representante.getRespon());
            inscricaoField.value.setValue(representante.getInscricao());
            codCliField.value.setValue(representante.getCodCli() == null ? new Entidade() :
                    new FluentDao().selectFrom(Entidade.class).where(it -> it.equal("codcli", Integer.parseInt(representante.getCodCli()))).singleResult());
            obsField.value.setValue(representante.getObs());
            dtoNascField.value.setValue(representante.getDtNascto());
            admissaoField.value.setValue(representante.getAdmissao());
            tipoField.select(representante.getTipo());
            grupoField.select(representante.getGrupo());
            impRendaField.value.setValue(representante.getImpRenda() == null ? null : representante.getImpRenda().toString());
            comissaoFatField.value.setValue(representante.getComissaoFat() == null ? null : representante.getComissaoFat().toString());
            comissaoRecField.value.setValue(representante.getComissaoRec() == null ? null : representante.getComissaoRec().toString());
            comissaoFat2Field.value.setValue(representante.getComissaoFat2() == null ? null : representante.getComissaoFat2().toString());
            comissaoRec2Field.value.setValue(representante.getComissaoRec2() == null ? null : representante.getComissaoRec2().toString());
            cepField.value.setValue(representante.getCep() == null ? "" : representante.getCep().getCep());
            enderecoField.value.setValue(representante.getEndereco());
            bairroField.value.setValue(representante.getBairro());
            regiaoField.value.setValue(representante.getRegiao());
            emailField.value.setValue(representante.getEmail());
            celularField.value.setValue(representante.getCelular());
            telefoneField.value.setValue(representante.getTelefone());
            criarBoxMarcasEColecoes();
        }
    }

    private void cancelarCadastro() {
        tblRepresentantes.refresh();
        navegation.inEdition.set(false);
        liberarCampos.set(false);
        if (representanteSelecionado == null || representanteSelecionado.getCodRep() == null) {
            navegation.selectedItem.setValue(null);
            limparCampos();
            if (!tblRepresentantes.items.isEmpty()) tblRepresentantes.selectItem(0);
        }
        carregaDados(representanteSelecionado);
    }

    private void excluirCadastro(Represen representante) {
        if (representante != null) {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Você deseja excluir o Representante?");
                message.showAndWait();
            }).value.get()) {
                SysLogger.addSysDelizLog("Cadastro de Representante", TipoAcao.EXCLUIR, representanteSelecionado.getCodRep(), "Representante excluído -- Código: " + representanteSelecionado.getCodRep() + " -- Descrição: " + representanteSelecionado.getNome());
                representantes.remove(representante);
                new FluentDao().delete(representante);
                representanteSelecionado = null;
                cancelarCadastro();
                MessageBox.create(message -> {
                    message.message("Representante excluído com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        }
    }

    private void limparCampos() {
        cnpjField.value.setValue("");
        ativoField.value.set(true);
        codRepField.clear();
        nomeRepField.clear();
        nomeResponField.clear();
        inscricaoField.clear();
        codCliField.clear();
        obsField.clear();
        dtoNascField.value.setValue(LocalDate.of(1000, 1, 1));
        tipoField.clear();
        admissaoField.value.setValue(LocalDate.of(1000, 1, 1));
        impRendaField.clear();
        comissaoFatField.clear();
        comissaoRecField.clear();
        comissaoFat2Field.clear();
        comissaoRec2Field.clear();
        cepField.clear();
        enderecoField.clear();
        bairroField.clear();
        regiaoField.clear();
        emailField.clear();
        celularField.clear();
        telefoneField.clear();
        newMarcaField.clear();
        criarBoxMarcasEColecoes();
    }

    private void getValoresCampos() {
        representanteSelecionado.setCnpj(cnpjField.value.getValue());
        representanteSelecionado.setCpf(cnpjField.value.getValue().length() > 14 ? "" : cnpjField.value.getValue());
        representanteSelecionado.setAtivo(ativoField.value.getValue());
        representanteSelecionado.setCodRep(codRepField.value.getValue());
        representanteSelecionado.setNome(nomeRepField.value.getValue());
        representanteSelecionado.setRespon(nomeResponField.value.getValue());
        representanteSelecionado.setInscricao(inscricaoField.value.getValue());
        representanteSelecionado.setCodCli(String.format("%05d", codCliField.value.getValue().getCodcli()));
        representanteSelecionado.setObs(obsField.value.getValue());
        if (dtoNascField.value.getValue() != null) representanteSelecionado.setDtNascto(dtoNascField.value.getValue());
        if (admissaoField.value.getValue() != null) representanteSelecionado.setAdmissao(admissaoField.value.getValue());
        representanteSelecionado.setTipo(tipoField.value.getValue());
        representanteSelecionado.setImpRenda(impRendaField.value.getValue() == null ? null : BigDecimal.valueOf(Double.parseDouble(impRendaField.value.getValue().replaceAll(",", "."))));
        representanteSelecionado.setComissaoFat(comissaoFatField.value.getValue() == null ? null : BigDecimal.valueOf(Double.parseDouble(comissaoFatField.value.getValue().replaceAll(",", "."))));
        representanteSelecionado.setComissaoRec(comissaoRecField.value.getValue() == null ? null : BigDecimal.valueOf(Double.parseDouble(comissaoRecField.value.getValue().replaceAll(",", "."))));
        representanteSelecionado.setComissaoFat2(comissaoFat2Field.value.getValue() == null ? null : BigDecimal.valueOf(Double.parseDouble(comissaoFat2Field.value.getValue().replaceAll(",", "."))));
        representanteSelecionado.setComissaoRec2(comissaoRec2Field.value.getValue() == null ? null : BigDecimal.valueOf(Double.parseDouble(comissaoRec2Field.value.getValue().replaceAll(",", "."))));
        representanteSelecionado.setCep(new FluentDao().selectFrom(CadCep.class).where(eb -> eb.equal("cep", cepField.value.getValue())).findAny());
        representanteSelecionado.setEndereco(enderecoField.value.getValue());
        representanteSelecionado.setBairro(bairroField.value.getValue());
        representanteSelecionado.setRegiao(regiaoField.value.getValue());
        representanteSelecionado.setEmail(emailField.value.getValue());
        representanteSelecionado.setCelular(celularField.value.getValue());
        representanteSelecionado.setTelefone(telefoneField.value.getValue());
    }

    private void criarBoxMarcasEColecoes() {
        tabPane.getTabs().clear();
        representanteSelecionado.getMarcas().forEach(marca -> {
            ListProperty<SdColecaoMarcaRepresen> colecoesBean = new SimpleListProperty<>(FXCollections.observableArrayList(marca.getColecoes()));
            FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
                field.title.setText("Coleção");
                field.editable.bind(liberarCampos);
                field.width(200.0);
                field.textValue.setValue("");

            });
            FormFieldSingleFind<Entidade> gerenteFilter = FormFieldSingleFind.create(Entidade.class, field -> {
                field.title("Gerente");
                field.width(400.0);
                field.editable.bind(liberarCampos);
                field.value.setValue(null);
            });
            FormTableView<SdColecaoMarcaRepresen> tblColecoes = FormTableView.create(SdColecaoMarcaRepresen.class, table -> {
                table.title("Coleção");
                table.expanded();
                table.height(600);
                table.items.bind(colecoesBean);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getCodigo()));
                            cln.alignment(Pos.CENTER);
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Descrição");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getDescricao()));

                        }).build(), /*Descrição*/
                        FormTableColumn.create(cln -> {
                            cln.title("Seq.");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getSequencia()));
                            cln.alignment(Pos.CENTER);
                        }).build(), /*Sequência*/
                        FormTableColumn.create(cln -> {
                            cln.title("Início Vendas");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getInicioVig() == null ? "" : StringUtils.toDateFormat(LocalDate.parse(param.getValue().getId().getColecao().getInicioVig().toString()))));
                            cln.alignment(Pos.CENTER);
                        }).build(), /*Início Venda*/
                        FormTableColumn.create(cln -> {
                            cln.title("Fim Vendas");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getFimVig() == null ? "" : StringUtils.toDateFormat(LocalDate.parse(param.getValue().getId().getColecao().getFimVig().toString()))));
                            cln.alignment(Pos.CENTER);
                        }).build(), /*Fim Venda*/
                        FormTableColumn.create(cln -> {
                            cln.title("Gerente");
                            cln.width(280.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdColecaoMarcaRepresen, SdColecaoMarcaRepresen>, ObservableValue<SdColecaoMarcaRepresen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGerente()));
                        }).build() /*Gerente*/
                );
                table.contextMenu(FormContextMenu.create(menu -> {
                    menu.addItem(menuItem -> {
                        menuItem.setText("Editar Gerente");
                        menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADMINISTRATIVO, ImageUtils.IconSize._16));
                        menuItem.disableProperty().bind(liberarCampos.not());
                        menuItem.setOnAction(evt -> {
                            SdColecaoMarcaRepresen colecaoSelecionada = (SdColecaoMarcaRepresen) table.selectedItem();
                            if (colecaoSelecionada != null) {
                                InputBox<Entidade> gerenteColecao = InputBox.build(Entidade.class, boxInput -> {
                                    boxInput.message("Selecione o gerente para a coleção "+colecaoSelecionada.getId().getColecao().toString());
                                    boxInput.showAndWait();
                                });

                                if (gerenteColecao.value.get() != null) {
                                    colecaoSelecionada.setGerente(gerenteColecao.value.get());
                                    new FluentDao().merge(colecaoSelecionada);
                                    SysLogger.addSysDelizLog("Representante", TipoAcao.EDITAR, colecaoSelecionada.getId().getMarcaRepresen().getId().getCodrep().getCodRep(),
                                            "Cadastrado o gerente " + gerenteColecao.value.get().toString() + " para o representante na marca " + colecaoSelecionada.getId().getMarcaRepresen().getId().getMarca().toString() +
                                            " e coleção " + colecaoSelecionada.getId().getColecao().toString());
                                    MessageBox.create(message -> {
                                        message.message("Gerente cadastrado para a coleção.");
                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                        message.position(Pos.TOP_RIGHT);
                                        message.notification();
                                    });
                                    table.refresh();
                                }
                            }
                        });
                    });
                }));
            });

            tabPane.addTab(FormTab.create(tab -> {
                tab.title(marca.getId().getMarca().getDescricao());
                tab.add(FormBox.create(box -> {
                    box.vertical();
                    box.add(FormBox.create(boxMarca -> {
                        boxMarca.horizontal();
                        boxMarca.alignment(Pos.BOTTOM_LEFT);
                        boxMarca.add(FormFieldToggle.create(it -> {
                            it.title("Ativo");
                            it.editable.bind(liberarCampos);
                            it.value.bindBidirectional(marca.ativoProperty());
                        }).build());
                        boxMarca.add(FormFieldText.create(it -> {
                            it.title.setText("Código");
                            it.value.setValue(marca.getId().getMarca().getCodigo());
                            it.editable.set(false);
                        }).build());
                        boxMarca.add(FormFieldText.create(it -> {
                            it.title.setText("Nome");
                            it.width(250);
                            it.value.setValue(marca.getId().getMarca().getDescricao());
                            it.editable.set(false);
                        }).build());
                        boxMarca.add(FormButton.create(btnRemoveMarca -> {
                            btnRemoveMarca.title("Remover Marca");
                            btnRemoveMarca.addStyle("danger");
                            btnRemoveMarca.disable.bind(liberarCampos.not());
                            btnRemoveMarca.setOnAction(evt -> {
                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Remover Marca?");
                                    message.showAndWait();
                                }).value.get())) {
                                    removerMarca(marca);
                                }
                            });
                        }).build());
                    }));
                    box.add(FormBox.create(boxColecao -> {
                        boxColecao.horizontal();
                        boxColecao.add(colecaoFilter.build());
                        boxColecao.add(gerenteFilter.build());
                    }));
                    box.add(FormBox.create(boxColecao -> {
                        boxColecao.horizontal();
                        boxColecao.add(FormButton.create(btnAddColecao -> {
                            btnAddColecao.title("Adicionar Coleções");
                            btnAddColecao.addStyle("success");
                            btnAddColecao.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_PEDIDO, ImageUtils.IconSize._16));
                            btnAddColecao.disable.bind(liberarCampos.not());
                            colecaoFilter.code.setOnKeyReleased(evt -> {
                                if (evt.getCode().equals(KeyCode.ENTER)) {
                                    btnAddColecao.fire();
                                }
                            });
                            btnAddColecao.setOnAction(evt -> {
                                if (colecaoFilter.objectValues.getValue() != null) {
                                    if (gerenteFilter.value.getValue() == null) {
                                        MessageBox.create(message -> {
                                            message.message("Campo do gerente não pode estar vazio!");
                                            message.type(MessageBox.TypeMessageBox.WARNING);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                        return;
                                    }
                                    adicionarColecoes(marca, colecoesBean, colecaoFilter, gerenteFilter);
                                }
                            });
                        }).build());
                        boxColecao.add(FormButton.create(btnRemoveColecao -> {
                            btnRemoveColecao.title("Remover Coleção");
                            btnRemoveColecao.addStyle("warning");
                            btnRemoveColecao.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCELAR_PEDIDO, ImageUtils.IconSize._16));
                            btnRemoveColecao.disable.bind(liberarCampos.not());
                            btnRemoveColecao.setOnAction(evt -> {
                                if (tblColecoes.selectedItem() != null) {
                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                        message.message("Remover Coleção?");
                                        message.showAndWait();
                                    }).value.get())) {
                                        removerColecao(marca, colecoesBean, tblColecoes);
                                    }
                                }
                            });
                        }).build());
                        boxColecao.add(FormButton.create(btnRemoveTodasColecoes -> {
                            btnRemoveTodasColecoes.title("Remover Todas as Coleções");
                            btnRemoveTodasColecoes.addStyle("danger");
                            btnRemoveTodasColecoes.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btnRemoveTodasColecoes.disable.bind(liberarCampos.not());
                            btnRemoveTodasColecoes.setOnAction(evt -> {
                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Remover todas as coleções?");
                                    message.showAndWait();
                                }).value.get())) {
                                    removerTodasColecoes(marca, colecoesBean, tblColecoes);
                                }
                            });
                        }).build());
                    }));
                    box.add(tblColecoes.build());
                }));
            }));
        });
    }

    private void removerTodasColecoes(SdMarcasRepresen marca, ListProperty<SdColecaoMarcaRepresen> colecoesBean, FormTableView<SdColecaoMarcaRepresen> tblColecoes) {
        marca.getColecoes().clear();
        colecoesBean.clear();
        tblColecoes.refresh();
    }

    private void removerColecao(SdMarcasRepresen marca, ListProperty<SdColecaoMarcaRepresen> colecoesBean, FormTableView<SdColecaoMarcaRepresen> tblColecoes) {
        if (tblColecoes.selectedItem() == null) {
            MessageBox.create(message -> {
                message.message("Nenhuma coleção selecionada!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        SdColecaoMarcaRepresen toDelete = tblColecoes.selectedItem();
        marca.getColecoes().remove(toDelete);
        colecoesBean.remove(toDelete);
        tblColecoes.refresh();
    }

    private void adicionarColecoes(SdMarcasRepresen marca, ListProperty<SdColecaoMarcaRepresen> colecoesBean, FormFieldMultipleFind<Colecao> colecaoFilter, FormFieldSingleFind<Entidade> gerente) {
        List<Colecao> colecoesNovas = new ArrayList<>(colecaoFilter.objectValues.getValue());
        colecoesNovas.forEach(colecaoNova -> {
            if (marca.getColecoes().stream().noneMatch(colecoes -> colecoes.getId().getColecao().getCodigo().equals(colecaoNova.getCodigo()))) {
                SdColecaoMarcaRepresen colecaoToAdd = new SdColecaoMarcaRepresen(marca, colecaoNova, gerente.value.getValue());
                marca.getColecoes().add(colecaoToAdd);
                colecoesBean.add(colecaoToAdd);
            }
        });
        colecaoFilter.clear();
        gerente.clear();
    }

    private void adicionarMarca() {
        List<Marca> marcasNovas = new ArrayList<>(newMarcaField.objectValues.getValue());
        marcasNovas.forEach(marcaNova -> {
            if (representanteSelecionado.getMarcas().stream().noneMatch(it -> it.getId().getMarca().getCodigo().equals(marcaNova.getCodigo()))) {
                representanteSelecionado.getMarcas().add(new SdMarcasRepresen(representanteSelecionado, marcaNova));
                criarBoxMarcasEColecoes();
            }
        });
    }

    private void removerMarca(SdMarcasRepresen marca) {
        representanteSelecionado.getMarcas().remove(marca);
        criarBoxMarcasEColecoes();
    }

    protected void buscarRepresentanteExistente(String codigo) {
        Represen representante = new FluentDao().selectFrom(Represen.class).where(it -> it.equal("cnpj", codigo)).singleResult();

        if (representante != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("CPF/CNPJ já atrelado a outro representante.\nDeseja edtita-lo?");
                message.showAndWait();
            }).value.get())) {
                navegation.selectedItem.setValue(representante);
                cnpjValido = true;
                liberarCampos.set(true);
                oldRepresen = representanteSelecionado.clone();
            } else {
                cnpjField.value.setValue("");
                cnpjValido = false;
            }

        } else {
            buscaEntidadeExistente(codigo);
        }
    }

    protected void buscaEntidadeExistente(String cnpj) {

        Entidade entidade = new FluentDao().selectFrom(Entidade.class)
                .where(it -> it
                        .equal("cnpj", cnpj)
                ).singleResult();
        Represen representante = new Represen();

        if (entidade == null) representante.setCnpj(cnpj);
        else {
            representante.setCnpj(entidade.getCnpj());
            representante.setCep(entidade.getCep());
            representante.setCodCli(entidade.getCodcli().toString());
            representante.setNome(entidade.getNome());
            representante.setInscricao(entidade.getInscricao());
            representante.setTipo(entidade.getTipo());
            representante.setGrupo(String.valueOf(3));
            representante.setInscricao(entidade.getInscricao());
            representante.setObs(entidade.getObs());
            representante.setEndereco(entidade.getEndereco());
            representante.setBairro(entidade.getBairro());
            representante.setDtNascto(entidade.getDtNasc());
            representante.setTelefone(entidade.getTelefone());
            representante.setEmail(entidade.getEmail());
        }
        navegation.selectedItem.setValue(representante);
        cnpjValido = true;
        liberarCampos.set(true);
    }

}
