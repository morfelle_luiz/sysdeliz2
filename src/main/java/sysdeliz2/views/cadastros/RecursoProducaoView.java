package sysdeliz2.views.cadastros;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.controllers.views.cadastros.RecursoProducaoController;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdTurno;
import sysdeliz2.models.sysdeliz.cadastros.SdFamiliasRecurso;
import sysdeliz2.models.sysdeliz.cadastros.SdRecursoProducao;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.exceptions.ContinueException;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class RecursoProducaoView extends RecursoProducaoController {

    // <editor-fold defaultstate="collapsed" desc="local">
    private final BigDecimal decimalCem = new BigDecimal(100);
    private final BooleanProperty novoRegistro = new SimpleBooleanProperty(false);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tabs">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabCadastro = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="listagem">
    private final FormFieldMultipleFind<Familia> fieldFilterFamilia = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Famílias");
        field.toUpper();
        field.width(200);
    });
    private final FormFieldMultipleFind<CadFluxo> fieldFilterSetor = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setores");
        field.toUpper();
        field.width(180);
    });
    private final FormFieldMultipleFind<VSdDadosEntidade> fieldFilterFornecedor = FormFieldMultipleFind.create(VSdDadosEntidade.class, field -> {
        field.title("Fornecedores");
        field.toUpper();
        field.width(220);
    });
    private final FormFieldMultipleFind<SdTurno> fieldFilterTurno = FormFieldMultipleFind.create(SdTurno.class, field -> {
        field.title("Turnos");
        field.toUpper();
        field.width(130);
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(170);
    });
    private final FormFieldMultipleFind<SdColaborador> fieldFilterControlador = FormFieldMultipleFind.create(SdColaborador.class, field -> {
        field.title("Controlador");
        field.toUpper();
        field.width(170);
    });
    private final FormFieldSegmentedButton<String> fieldFilterAtivo = FormFieldSegmentedButton.create(field -> {
        field.title("Ativo");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormContextMenu contextTblRecursos = FormContextMenu.create(menu -> {
        menu.addItem(menuItem -> {
            menuItem.setText("Abrir Famílias");
            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
            menuItem.setOnAction(evt -> {
                if (this.tblRecursos.selectedItem() != null) {
                    new Fragment().show(fragment -> {
                        fragment.title("Famílias do Recurso");
                        fragment.size(300.0, 250.0);

                        fragment.box.getChildren().add(FormBox.create(content -> {
                            content.vertical();
                            content.expanded();
                            content.add(FormTableView.create(SdFamiliasRecurso.class, table -> {
                                table.title("Famílias");
                                table.expanded();
                                table.items.set(FXCollections.observableList(this.tblRecursos.selectedItem().getFamilias()));
                                table.columns(
                                        FormTableColumn.create(cln -> {
                                            cln.title("Família");
                                            cln.width(250.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdFamiliasRecurso, SdFamiliasRecurso>, ObservableValue<SdFamiliasRecurso>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFamilia()));
                                        }).build() /*Família*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Priorid.");
                                            cln.width(40.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdFamiliasRecurso, SdFamiliasRecurso>, ObservableValue<SdFamiliasRecurso>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrioridade()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Prioridade*/
                                );
                            }).build());
                        }));
                    });
                }
            });
        });
        menu.addSeparator();
        menu.addItem(menuItem -> {
            menuItem.setText("Duplicar Recursos da Coleção");
            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._16));
            menuItem.setOnAction(evt -> {
                if (this.tblRecursos.selectedItem() != null) {
                    SdRecursoProducao recurso = this.tblRecursos.selectedItem();
                    duplicarRecurso(recurso);
                }
            });
        });
    });
    private final FormTableView<SdRecursoProducao> tblRecursos = FormTableView.create(SdRecursoProducao.class, table -> {
        table.title("Recursos");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(110.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdRecursoProducao, SdRecursoProducao>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Recurso");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Recurso");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Recurso");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdRecursoProducao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnVisualizar.setOnAction(evt -> {
                                        tabs.getSelectionModel().select(1);
                                        table.selectItem(item);
                                        abrirRecurso(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        tabs.getSelectionModel().select(1);
                                        table.selectItem(item);
                                        editarRecurso(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        table.selectItem(item);
                                        excluirRecurso(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Turno");
                    cln.width(190.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTurno()));
                }).build() /*Turno*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(240.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornecedor");
                    cln.width(340.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFornecedor()));
                }).build() /*Fornecedor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetorExcia()));
                }).build() /*Setor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Equip/Cost");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEquipamentos()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Equip/Cost*/,
                FormTableColumn.create(cln -> {
                    cln.title("Efic.");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEficiencia()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdRecursoProducao, BigDecimal>(){
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                }
                            }
                        };
                    });
                }).build() /*Efic.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Famílias");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFamilias().size()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Famílias*/,
                FormTableColumn.create(cln -> {
                    cln.title("Controlador");
                    cln.width(260.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getControlador()));
                }).build() /*Controlador*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(45);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRecursoProducao, SdRecursoProducao>, ObservableValue<SdRecursoProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtivo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdRecursoProducao, Boolean>(){
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Ativo*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdRecursoProducao>() {
                @Override
                protected void updateItem(SdRecursoProducao item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        setContextMenu(contextTblRecursos);
                    }
                }
            };
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cadastro">
    private final FormNavegation<SdRecursoProducao> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblRecursos);
        nav.withActions(true, true, true);
        nav.btnSave(evt -> salvarRecurso((SdRecursoProducao) nav.selectedItem.get()));
        nav.btnCancel(evt -> cancelarFormulario());
        nav.btnAddRegister(evt -> adicionarRecurso());
        nav.btnEditRegister(evt -> editarRecurso((SdRecursoProducao) nav.selectedItem.get()));
        nav.btnDeleteRegister(evt -> excluirRecurso((SdRecursoProducao) nav.selectedItem.get()));
        nav.withDuplicate(nav.inEdition.not().not(), evt -> copiarRecurso((SdRecursoProducao) nav.selectedItem.get()));

        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                abrirRecurso((SdRecursoProducao) newValue);
            }
        });

        nav.withReturn(evt -> {
            nav.inEdition.set(false);
            tabs.getSelectionModel().select(0);
        });
    });
    private final FormFieldSingleFind<CadFluxo> fieldSetorExcia = FormFieldSingleFind.create(CadFluxo.class, field -> {
        field.title("Setor Excia");
        field.toUpper();
        field.width(320);
        field.editable.bind(navegation.inEdition.and(novoRegistro));
    });
    private final FormFieldSingleFind<VSdDadosEntidade> fieldFornecedor = FormFieldSingleFind.create(VSdDadosEntidade.class, field -> {
        field.title("Fornecedor");
        field.toUpper();
        field.width(400);
        field.editable.bind(navegation.inEdition.and(novoRegistro));
    });
    private final FormFieldSingleFind<SdTurno> fieldTurno = FormFieldSingleFind.create(SdTurno.class, field -> {
        field.title("Turno");
        field.toUpper();
        field.width(280);
        field.editable.bind(navegation.inEdition.and(novoRegistro));
    });
    private final FormFieldSingleFind<Colecao> fieldColecao = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(280);
        field.editable.bind(navegation.inEdition.and(novoRegistro));
    });
    private final FormFieldSingleFind<SdColaborador> fieldControlador = FormFieldSingleFind.create(SdColaborador.class, field -> {
        field.title("Controlador");
        field.toUpper();
        field.width(300);
        field.defaults.add(new DefaultFilter("121", "codigoFuncao"));
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldMultipleFind<Familia> fieldFamilia = FormFieldMultipleFind.create(Familia.class, field -> {
        field.withoutTitle();
        field.width(150);
        field.toUpper();
        field.editable.bind(navegation.inEdition);
    });
    private final FormTableView<SdFamiliasRecurso> tblFamilias = FormTableView.create(SdFamiliasRecurso.class, table -> {
        table.title("Famílias");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFamiliasRecurso, SdFamiliasRecurso>, ObservableValue<SdFamiliasRecurso>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFamilia()));
                }).build() /*Família*/,
                FormTableColumn.create(cln -> {
                    cln.title("Prioridade");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFamiliasRecurso, SdFamiliasRecurso>, ObservableValue<SdFamiliasRecurso>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFamiliasRecurso, SdFamiliasRecurso>(){
                            @Override
                            protected void updateItem(SdFamiliasRecurso item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.alignment(Pos.CENTER);
                                        field.addStyle("xs");
                                        field.value.bindBidirectional(item.prioridadeProperty(), new NumberStringConverter());
                                        field.editable.bind(navegation.inEdition);
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Prioridade*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFamiliasRecurso, SdFamiliasRecurso>, ObservableValue<SdFamiliasRecurso>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdFamiliasRecurso, SdFamiliasRecurso>(){
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluirFamilia = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir família do recurso");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(navegation.inEdition.not());
                            });
                            @Override
                            protected void updateItem(SdFamiliasRecurso item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluirFamilia.setOnAction(evt -> {
                                        excluirFamilia(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluirFamilia);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    private final FormFieldText fieldEquipamentos = FormFieldText.create(field -> {
        field.title("Equips/Costureiras");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.INTEGER);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTempoEquipamento = FormFieldText.create(field -> {
        field.title("Tempo/Peças Equip.");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.postLabel("p/ dia");
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTempoFila = FormFieldText.create(field -> {
        field.title("Tempo Fila");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.postLabel("min");
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldEficiencia = FormFieldText.create(field -> {
        field.title("Eficiência");
        field.toUpper();
        field.width(70);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.DOUBLE);
        field.postLabel("%");
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldMargem = FormFieldText.create(field -> {
        field.title("Margem");
        field.toUpper();
        field.width(65);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.DOUBLE);
        field.postLabel("%");
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle fieldAtivo = FormFieldToggle.create(field -> {
        field.title("Ativo");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldSegmentedButton<SdRecursoProducao.TipoCapacidade> fieldTipoCapacidade = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo Capacidade");
        field.options(
                field.option("Por Minuto", SdRecursoProducao.TipoCapacidade.M, FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Por Peça", SdRecursoProducao.TipoCapacidade.P, FormFieldSegmentedButton.Style.INFO)
        );
        field.select(0);
        field.editable.bind(navegation.inEdition);
    });
    // </editor-fold>

    public RecursoProducaoView() {
        super("Recurso de Produção", ImageUtils.getImage(ImageUtils.Icon.RECURSO_PRODUCAO), new String[]{"Listagem", "Cadastro"});
        initListagem();
        initCadastro();
    }

    // ---------------------  LISTAGEM -------------------
    private void initListagem() {
        tabListagem.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(FormBox.create(top -> {
                top.horizontal();
                top.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(fields -> fields.addHorizontal(fieldFilterFamilia.build(), fieldFilterSetor.build(), fieldFilterTurno.build(), fieldFilterAtivo.build()));
                    filter.add(fields -> fields.addHorizontal(fieldFilterFornecedor.build(), fieldFilterColecao.build(), fieldFilterControlador.build()));
                    filter.find.setOnAction(evt -> {
                        Object[] familias = fieldFilterFamilia.objectValues.stream().map(rec -> rec.getCodigo()).toArray();
                        Object[] setores = fieldFilterSetor.objectValues.stream().map(rec -> rec.getCodigo()).toArray();
                        Object[] colecoes = fieldFilterColecao.objectValues.stream().map(rec -> rec.getCodigo()).toArray();
                        Object[] turnos = fieldFilterTurno.objectValues.stream().map(rec -> rec.getCodigo()).toArray();
                        Object[] fornecedores = fieldFilterFornecedor.objectValues.stream().map(rec -> rec.getCodcli()).toArray();
                        Object[] controladores = fieldFilterControlador.objectValues.stream().map(con -> con.getCodigo()).toArray();
                        String ativo = fieldFilterAtivo.value.getValue() == null ? "A" : fieldFilterAtivo.value.getValue();
                        procurarRecursos(familias, setores, ativo, colecoes, turnos, fornecedores, controladores);
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterFamilia.clear();
                        fieldFilterSetor.clear();
                        fieldFilterFornecedor.clear();
                        fieldFilterTurno.clear();
                        fieldFilterColecao.clear();
                        fieldFilterControlador.clear();
                        fieldFilterAtivo.select(0);
                    });
                }));
            }));
            content.center(tblRecursos.build());
        }));
    }

    private void procurarRecursos(Object[] familias, Object[] setores, String ativo, Object[] colecoes, Object[] turnos, Object[] fornecedores, Object[] controladores) {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<List<SdRecursoProducao>> recursosRwo = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            JPAUtils.clearEntitys(tblRecursos.items.get());
            recursosRwo.set(getRecursos(familias, setores, ativo, colecoes, turnos, fornecedores, controladores));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblRecursos.items.set(FXCollections.observableList(recursosRwo.get()));
                tblRecursos.refresh();
            }
        });
    }

    // --------------------- CADASTRO --------------------
    private void initCadastro() {
        tabCadastro.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(navegation);
            content.center(FormBox.create(container -> {
                container.vertical();
                container.expanded();
                container.add(fields -> fields.addHorizontal(fieldColecao.build(), fieldSetorExcia.build()));
                container.add(fields -> fields.addHorizontal(fieldTurno.build(), fieldFornecedor.build()));
                container.add(new Separator(Orientation.HORIZONTAL));
                container.add(fields -> fields.addHorizontal(fieldEquipamentos.build(), fieldTipoCapacidade.build(), fieldTempoEquipamento.build()));
                container.add(fields -> fields.addHorizontal(fieldAtivo.build(), fieldTempoFila.build()));
                container.add(fields -> fields.addHorizontal(fieldEficiencia.build(), fieldMargem.build(), fieldControlador.build()));
                container.add(formBox -> {
                    formBox.horizontal();
                    formBox.expanded();
                    formBox.add(boxFamilias -> {
                        boxFamilias.title("Famílias");
                        boxFamilias.vertical();
                        boxFamilias.add(fields -> fields.addHorizontal(fieldFamilia.build(), FormButton.create(btnAddFamilia -> {
                            btnAddFamilia.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btnAddFamilia.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                            btnAddFamilia.addStyle("success");
                            btnAddFamilia.disable.bind(navegation.inEdition.not());
                            btnAddFamilia.setAction(evt -> adicionarFamilias(fieldFamilia.objectValues.get()));
                        })));
                        boxFamilias.add(tblFamilias.build());
                    });
                });
            }));
        }));
    }

    private void adicionarFamilias(ObservableList<Familia> familias) {
        if (familias.size() > 0) {
            familias.forEach(familia -> {
                if (navegation.selectedItem.get().getFamilias().stream().noneMatch(familiaRecurso -> familiaRecurso.getId().getFamilia().getCodigo().equals(familia.getCodigo()))) {
                    navegation.selectedItem.get().getFamilias().add(new SdFamiliasRecurso(navegation.selectedItem.get(), familia, 0));
                }
            });
            tblFamilias.refresh();
            fieldFamilia.clear();
            MessageBox.create(message -> {
                message.message("Família(s) adicionada(s) com sucesso. Você deve clicar em Salvar para persistir os registros.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void abrirRecurso(SdRecursoProducao recurso) {
        if (recurso != null) {
            fieldSetorExcia.value.set(recurso.getSetorExcia());
            fieldFornecedor.value.set(recurso.getFornecedor());
            fieldTurno.value.set(recurso.getTurno());
            fieldColecao.value.set(recurso.getColecao());
            fieldTipoCapacidade.select(recurso.getTipoCapacidade());
            fieldEquipamentos.value.set(StringUtils.toIntegerFormat(recurso.getEquipamentos()));
            fieldTempoEquipamento.value.set(StringUtils.toDecimalFormat(recurso.getTempoEquipamento()));
            fieldTempoFila.value.set(StringUtils.toDecimalFormat(recurso.getTempoFila()));
            fieldEficiencia.value.set(StringUtils.toDecimalFormat(recurso.getEficiencia().multiply(decimalCem)));
            fieldMargem.value.set(StringUtils.toDecimalFormat(recurso.getMargem().multiply(decimalCem)));
            fieldAtivo.value.set(recurso.isAtivo());
            fieldControlador.value.set(recurso.getControlador());
            tblFamilias.items.set(FXCollections.observableList(recurso.getFamilias()));
        }
    }

    private void excluirRecurso(SdRecursoProducao recurso) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o recurso selecionado?");
            message.showAndWait();
        }).value.get())) {
            AtomicReference<Exception> exRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                delete(recurso);
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    tblRecursos.items.remove(recurso);
                    tblRecursos.refresh();
                    cancelarFormulario();
                    MessageBox.create(message -> {
                        message.message("Recurso excluído com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            });
        }
    }

    private void editarRecurso(SdRecursoProducao recurso) {
        navegation.inEdition.set(true);
        abrirRecurso(recurso);
        MessageBox.create(message -> {
            message.message("Abrindo modo edição!");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void adicionarRecurso() {
        SdRecursoProducao novoRecurso = new SdRecursoProducao();
        navegation.selectedItem.set(novoRecurso);
        abrirRecurso(novoRecurso);
        cancelarFormulario();
        navegation.inEdition.set(true);
        novoRegistro.set(true);
    }

    private void cancelarFormulario() {
        fieldSetorExcia.clear();
        fieldFornecedor.clear();
        fieldTurno.clear();
        fieldColecao.clear();
        fieldControlador.clear();
        fieldTipoCapacidade.select(0);
        fieldEquipamentos.value.set(StringUtils.toIntegerFormat(0));
        fieldTempoEquipamento.value.set(StringUtils.toDecimalFormat(BigDecimal.ZERO));
        fieldTempoFila.value.set(StringUtils.toDecimalFormat(BigDecimal.ZERO));
        fieldEficiencia.value.set(StringUtils.toDecimalFormat(BigDecimal.ZERO));
        fieldMargem.value.set(StringUtils.toDecimalFormat(BigDecimal.ZERO));
        fieldAtivo.value.set(false);
        tblFamilias.clear();

        novoRegistro.set(false);
        navegation.inEdition.set(false);
    }

    private void salvarRecurso(SdRecursoProducao recurso) {
        try {
            fieldSetorExcia.validate();
            fieldColecao.validate();
            fieldFornecedor.validate();
            fieldTurno.validate();
            fieldControlador.validate();
            fieldEquipamentos.validate();
            fieldEquipamentos.validateDecimal();
            fieldTempoEquipamento.validate();
            fieldTempoEquipamento.validateDecimal();
            fieldTempoFila.validate();
            fieldTempoFila.validateDecimal();
            fieldEficiencia.validate();
            fieldEficiencia.validateDecimal();
            fieldMargem.validate();
            fieldMargem.validateDecimal();
            if (novoRegistro.get() && exist(fieldSetorExcia.value.get().getCodigo(), fieldColecao.value.get().getCodigo(), fieldFornecedor.value.get().getCodcli(), fieldTurno.value.get().getCodigo()))
                throw new FormValidationException("O recurso cadastrado já existe!");
            if (recurso.getFamilias().size() == 0)
                throw new FormValidationException("Você deve cadastrar ao menos uma família para o recurso!");
            if (recurso.getTipoCapacidade() == null)
                throw new FormValidationException("Você deve selecionar o tipo de capacidade diária do recurso!");
        } catch (FormValidationException valid) {
            MessageBox.create(message -> {
                message.message(valid.getMessage());
                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<SdRecursoProducao> recursoRwo = new AtomicReference<>(recurso);
        new RunAsyncWithOverlay(this).exec(task -> {
            recursoRwo.get().setSetorExcia(fieldSetorExcia.value.getValue());
            recursoRwo.get().setFornecedor(fieldFornecedor.value.getValue());
            recursoRwo.get().setColecao(fieldColecao.value.getValue());
            recursoRwo.get().setTurno(fieldTurno.value.getValue());
            recursoRwo.get().setControlador(fieldControlador.value.getValue());
            recursoRwo.get().setEquipamentos(fieldEquipamentos.decimalValue().intValue());
            recursoRwo.get().setTempoEquipamento(fieldTempoEquipamento.decimalValue());
            recursoRwo.get().setTempoFila(fieldTempoFila.decimalValue());
            recursoRwo.get().setEficiencia(fieldEficiencia.decimalValue().divide(decimalCem, 5, RoundingMode.HALF_UP));
            recursoRwo.get().setMargem(fieldMargem.decimalValue().divide(decimalCem, 5, RoundingMode.HALF_UP));
            recursoRwo.get().setAtivo(fieldAtivo.value.getValue());
            recursoRwo.get().setTipoCapacidade(fieldTipoCapacidade.value.get());

            try {
                String recursoCadastrado = recursoRwo.get().toLog();
                if (novoRegistro.get()) {
                    recursoRwo.set(save(recursoRwo.get()));
                    SysLogger.addSysDelizLog("Recursos de Produção", TipoAcao.CADASTRAR, recursoRwo.get().codigo(), "Cadastrado novo recurso: " + (recursoCadastrado.length() > 3975 ? recursoCadastrado.substring(0, 3974) : recursoCadastrado));
                } else {
                    update(recursoRwo.get());
                    SysLogger.addSysDelizLog("Recursos de Produção", TipoAcao.EDITAR, recursoRwo.get().codigo(), "Recurso editado: " + (recursoCadastrado.length() > 3975 ? recursoCadastrado.substring(0, 3974) : recursoCadastrado));
                }

                return ReturnAsync.OK.value;
            } catch (SQLException exc) {
                exRwo.set(exc);
                return ReturnAsync.EXCEPTION.value;
            }
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Registro salvo com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                if (!tblRecursos.items.contains(recursoRwo.get())) {
                    tblRecursos.items.add(recursoRwo.get());
                }
                tblRecursos.refresh();
                navegation.inEdition.set(false);
                novoRegistro.set(false);
                tblRecursos.selectItem(recursoRwo.get());
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(message -> {
                    message.exception(exRwo.get());
                    message.showAndWait();
                });
            }
        });

    }

    private void copiarRecurso(SdRecursoProducao recurso) {
        if(recurso != null) {
            navegation.inEdition.set(true);
            novoRegistro.set(true);
            SdRecursoProducao novoRecurso = new SdRecursoProducao();
            novoRecurso.setFornecedor(recurso.getFornecedor());
            novoRecurso.setTurno(recurso.getTurno());
            novoRecurso.setMargem(recurso.getMargem());
            novoRecurso.setEficiencia(recurso.getEficiencia());
            novoRecurso.setTempoFila(recurso.getTempoFila());
            novoRecurso.setTempoEquipamento(recurso.getTempoEquipamento());
            novoRecurso.setSetorExcia(recurso.getSetorExcia());
            novoRecurso.setAtivo(recurso.isAtivo());
            novoRecurso.setControlador(recurso.getControlador());
            novoRecurso.setEquipamentos(recurso.getEquipamentos());
            novoRecurso.setTipoCapacidade(recurso.getTipoCapacidade());
            recurso.getFamilias().forEach(familia -> {
                novoRecurso.getFamilias().add(new SdFamiliasRecurso(novoRecurso, familia.getId().getFamilia(), familia.getPrioridade()));
            });

            fieldColecao.clear();
            abrirRecurso(novoRecurso);
            navegation.selectedItem.set(novoRecurso);
        }
    }

    private void duplicarRecurso(SdRecursoProducao recurso) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente duplicar todos os recursos da coleção " + recurso.getColecao().toString());
            message.showAndWait();
        }).value.get())) {
            Colecao novaColecao = (Colecao) InputBox.build(Colecao.class, boxInput -> {
                boxInput.message("Informe a coleção para duplicar os recursos:");
                boxInput.showAndWait();
            }).value.getValue();
            if (novaColecao == null)
                return;

            AtomicReference<Exception> exRwo = new AtomicReference<>();
            AtomicReference<List<SdRecursoProducao>> recursosRwo = new AtomicReference<>();
            AtomicReference<List<SdRecursoProducao>> novosRecursos = new AtomicReference<>(new ArrayList<>());
            new RunAsyncWithOverlay(this).exec(task -> {
                recursosRwo.set(getRecursosColecao(recurso.getColecao().getCodigo()));
                try {
                    recursosRwo.get().forEach(recursoColecao -> {
                        try {
                            SdRecursoProducao novoRecurso = new SdRecursoProducao();
                            novoRecurso.setColecao(novaColecao);
                            novoRecurso.setFornecedor(recursoColecao.getFornecedor());
                            novoRecurso.setTurno(recursoColecao.getTurno());
                            novoRecurso.setMargem(recursoColecao.getMargem());
                            novoRecurso.setEficiencia(recursoColecao.getEficiencia());
                            novoRecurso.setTempoFila(recursoColecao.getTempoFila());
                            novoRecurso.setTempoEquipamento(recursoColecao.getTempoEquipamento());
                            novoRecurso.setSetorExcia(recursoColecao.getSetorExcia());
                            novoRecurso.setAtivo(recursoColecao.isAtivo());
                            novoRecurso.setControlador(recursoColecao.getControlador());
                            novoRecurso.setEquipamentos(recursoColecao.getEquipamentos());
                            novoRecurso.setTipoCapacidade(recursoColecao.getTipoCapacidade());
                            recursoColecao.getFamilias().forEach(familia -> {
                                novoRecurso.getFamilias().add(new SdFamiliasRecurso(novoRecurso, familia.getId().getFamilia(), familia.getPrioridade()));
                            });

                            if (exist(novoRecurso.getSetorExcia().getCodigo(), novoRecurso.getColecao().getCodigo(), novoRecurso.getFornecedor().getCodcli(), novoRecurso.getTurno().getCodigo()))
                                throw new ContinueException();

                            save(novoRecurso);
                            SysLogger.addSysDelizLog("Recursos de Produção", TipoAcao.CADASTRAR, novoRecurso.codigo(), "Duplicado recurso de " + recursoColecao.codigo() + ": " + novoRecurso.toLog());
                            novosRecursos.get().add(novoRecurso);
                        } catch (ContinueException e) {
                        } catch (SQLException e) {
                            exRwo.set(e);
                            throw new BreakException();
                        }
                    });
                } catch (BreakException e) {
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Recursos da coleção " + recurso.getColecao().toString() + " duplicados para a coleção " + novaColecao.toString());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    tblRecursos.items.get().addAll(novosRecursos.get());
                    tblRecursos.refresh();
                }
            });
        }
    }

    private void excluirFamilia(SdFamiliasRecurso familia) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Deseja realmente excluir a família " + familia.getId().getFamilia().toString() + " do recurso selecionado?");
                message.showAndWait();
            }).value.get())) {
            try {
                deleteFamilia(familia);
                navegation.selectedItem.get().getFamilias().remove(familia);
                tblFamilias.refresh();
                MessageBox.create(message -> {
                    message.message("Família marcada para remoção. É necessário você salvar para persistir a exclusão.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }


}
