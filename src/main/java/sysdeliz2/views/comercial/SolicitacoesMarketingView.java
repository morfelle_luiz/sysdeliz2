package sysdeliz2.views.comercial;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.comercial.SolicitacoesMarketingController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.apps.mkt.*;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedido;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.models.view.VSdDadosColecaoAtual;
import sysdeliz2.models.view.VSdDadosPedido;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.Transient;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SolicitacoesMarketingView extends SolicitacoesMarketingController {

    private Marca marcaSelecionada = null;

    //<editor-fold desc="Variaveis FTP">
    private static final String server = "ftp.deliz.com.br";
    private static final int port = 21;
    private static final String user = "mkt@imagens.deliz.com.br";
    private static final String pass = "Deliz.d25m04";
    //</editor-fold>

    //<editor-fold desc="Lists">
    private final ListProperty<MktSolicitacao> solicitacoesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<MktItemSolicitacao> itensSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<MktItemSolicitacao> materiaisBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<MktEmail> emailsBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<MktTipoSolicitacao> tiposSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<MktImagemSolicitacao> imagensSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    private List<Marca> marcas;
    //</editor-fold>

    //<editor-fold desc="Filter">
    private final FormFieldMultipleFind<Entidade> clienteFilter = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
    });
    private final FormFieldMultipleFind<Represen> representanteFilter = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
    });
    private final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
    });
    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldComboBox<MktStatusSolicitacao> statusSolicFilter = FormFieldComboBox.create(MktStatusSolicitacao.class, field -> {
        field.title("Status");
        field.items(FXCollections.observableArrayList(statusList));
        field.items.add(0, new MktStatusSolicitacao(0));
        field.width(250);
        field.getComboBox().setPromptText("Selecione...");
    });
    private final FormFieldComboBox<MktStatusSolicitacao> statusSolicMaterialFilter = FormFieldComboBox.create(MktStatusSolicitacao.class, field -> {
        field.title("Status");
        field.items(FXCollections.observableArrayList(statusList));
        field.items.add(0, new MktStatusSolicitacao(0));
        field.getComboBox().setPromptText("Selecione...");
    });

    private final FormFieldMultipleFind<Marca> marcaMaterialFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldMultipleFind<MktTipoSolicitacao> materialFilter = FormFieldMultipleFind.create(MktTipoSolicitacao.class, field -> {
        field.title("Material");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> imgAdicionadaSegBtn = FormFieldSegmentedButton.create(field -> {
        field.title("Imagem Adicionada");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> enviadoSegBtn = FormFieldSegmentedButton.create(field -> {
        field.title("Enviado ao Fornecedor");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> comImgSegBtn = FormFieldSegmentedButton.create(field -> {
        field.title("Apenas com Imagem");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    //</editor-fold>

    //<editor-fold desc="Fields">
    private final FormFieldText idSolicitacaoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });
    private final FormFieldText statusField = FormFieldText.create(field -> {
        field.title("Status");
        field.width(300);
        field.editable.set(false);
    });
    private final FormFieldText representanteField = FormFieldText.create(field -> {
        field.title("Representante");
        field.width(245);
        field.editable.set(false);
    });
    private final FormFieldText clienteField = FormFieldText.create(field -> {
        field.title("Cliente");
        field.width(300);
        field.editable.set(false);
    });
    private final FormFieldText qtdeField = FormFieldText.create(field -> {
        field.title("Qtde");
        field.editable.set(false);
    });
    private final FormFieldText tiposItensField = FormFieldText.create(field -> {
        field.title("Tipos");
        field.editable.set(false);
        field.width(300);
    });
    private final FormFieldText dataCriacaoField = FormFieldText.create(field -> {
        field.title("Data Criação");
        field.editable.set(false);
    });
    private final FormFieldText dataUltimaField = FormFieldText.create(field -> {
        field.title("Data Ult. Alteração");
        field.editable.set(false);
    });

    private final Slider sliderStatus = new Slider(statusList.stream().map(MktStatusSolicitacao::getCodigo).min(Integer::compareTo).get(), statusList.stream().map(MktStatusSolicitacao::getCodigo).max(Integer::compareTo).get(), 2);
    //</editor-fold>

    //<editor-fold desc="Tables">
    private final FormTableView<MktSolicitacao> tblSolicitacoes = FormTableView.create(MktSolicitacao.class, table -> {
        table.expanded();
        table.title("Solicitações");
        table.items.bind(solicitacoesBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                new RunAsyncWithOverlay(this).exec(task -> {
                    carregaItens((MktSolicitacao) newValue);
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        itensSolicitacaoBean.set(FXCollections.observableArrayList(itensSolicitacao));
                        carregaCampos((MktSolicitacao) newValue);
                    }
                });
            }
        });
        table.indices(
                statusList.stream().map(it -> table.indice(it.getStatus(), it.getStyle())).collect(Collectors.toList())
        );
        table.factoryRow(param -> new TableRow<MktSolicitacao>() {
            @Override
            protected void updateItem(MktSolicitacao item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    getStyleClass().add("table-row-" + item.getStatus().getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-primary", "table-row-info", "table-row-secundary");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(150);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktSolicitacao, MktSolicitacao>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final FormButton btnLiberarMkt = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                            btn.tooltip("Liberar Marketing");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("primary");
                        });
                        final FormButton btnAdicionarPedido = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_ENTREGA, ImageUtils.IconSize._16));
                            btn.tooltip("Adicionar Marketing ao pedido do Cliente");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("warning");
                        });
                        final FormButton btnOfVerificada = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._16));
                            btn.tooltip("Nota Fiscal Verificada pelo Comercial");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("info");
                        });
                        final FormButton btnCancelarSolicitacao = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Cancelar Solicitação");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("danger");
                        });

                        @Override
                        protected void updateItem(MktSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {

                                btnLiberarMkt.setOnAction(evt -> {
                                    liberarMarketing(item);
                                });
                                btnAdicionarPedido.setOnAction(evt -> {
                                    adicionarMarketingPedido(item);
                                });
                                btnOfVerificada.setOnAction(evt -> {
                                    verificarOfSolicitacao(item);
                                });

                                btnCancelarSolicitacao.setOnAction(evt -> {
                                    cancelarSolicitacao(item);
                                });

                                btnLiberarMkt.disable.set(item.getStatus().getCodigo() != 2);
                                btnAdicionarPedido.disable.set(item.getStatus().getCodigo() != 4 && item.getStatus().getCodigo() != 5);
                                btnCancelarSolicitacao.disable.set(item.getStatus().getCodigo() != 2);
                                btnOfVerificada.disable.set((item.getStatus().getCodigo() != 4 && item.getStatus().getCodigo() != 5) || item.isOfVerificada());

                                boxButtonsRow.setAlignment(Pos.CENTER);
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnLiberarMkt, btnAdicionarPedido, btnOfVerificada, btnCancelarSolicitacao);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(300);
                    cln.order(Comparator.comparing(o -> ((Represen) o).getNome()));
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(300);
                    cln.order(Comparator.comparing(o -> ((VSdDadosCliente) o).getRazaosocial()));
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getCodigo() + " - " + param.getValue().getStatus().getStatus()));
                    cln.width(150);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt Solicitação");
                    cln.width(90);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getData())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Ult Alteração");
                    cln.width(90);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDataAtualizacao())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Ped. Pendente");
                    cln.width(90);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktSolicitacao, MktSolicitacao>() {
                        @Override
                        protected void updateItem(MktSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                VSdDadosColecaoAtual colecaoAtual = (VSdDadosColecaoAtual) new FluentDao().selectFrom(VSdDadosColecaoAtual.class).where(eb -> eb.equal("marca", item.getMarca().getCodigo())).singleResult();
                                String colecao = "";
                                if (colecaoAtual != null) {
                                    colecao = colecaoAtual.getColecao().getCodigo();
                                }
                                String finalColecao = colecao;
                                List<VSdDadosPedido> pedidos = (List<VSdDadosPedido>) new FluentDao().selectFrom(VSdDadosPedido.class).where(it -> it
                                        .equal("numero.codcli.codcli", item.getCodcli().getCodcli())
                                        .equal("status", "PENDENTE")
                                        .between("numero.periodo.prazo", ((Object) "0000"), "9999")
                                        .equal("numero.tabPre.usaFv", true)
                                        .equal("marca.codigo", item.getMarca().getCodigo())
                                        .equal("colecao.codigo", finalColecao, TipoExpressao.AND, when -> colecaoAtual != null)
                                ).resultList();

                                setGraphic(ImageUtils.getIcon(pedidos.size() > 0 ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Nf Verificada");
                    cln.width(90);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktSolicitacao, MktSolicitacao>, ObservableValue<MktSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktSolicitacao, MktSolicitacao>() {
                        @Override
                        protected void updateItem(MktSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(ImageUtils.getIcon(item.isOfVerificada() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                            }
                        }
                    });
                }).build()
        );
    });

    private final FormTableView<MktItemSolicitacao> tblItensSolicitacao = FormTableView.create(MktItemSolicitacao.class, table -> {
        table.items.bind(itensSolicitacaoBean);
        table.expanded();
        table.title("Itens");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cód");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getOrdem())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(150);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getTipo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Imagem");
                    cln.width(170);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(box -> {
                                    box.vertical();
                                    box.expanded();
                                    box.size(160, 120);
                                    box.alignment(Pos.CENTER);
                                    if (item.getTipo().isKits()) {
                                        box.add(new Label("Material Sem Imagem"));
                                    } else
                                        box.add(FormFieldImage.create(img -> {
                                            img.withoutTitle();
                                            img.alignment(Pos.CENTER);
                                            img.size(160, 120);

                                            box.setOnMouseClicked(evt -> {
                                                if (item.getImagem() != null && item.getTipo().isImagem() && evt.getButton().equals(MouseButton.PRIMARY)) {
                                                    showImageCompleta(item.getImagem().getUrl());
                                                }
                                            });
                                            if (item.getImagem() != null) img.image(item.getImagem().getImagem());
                                        }).build());
                                }));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Informações");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(box -> {
                                    box.vertical();
                                    box.expanded();
                                    box.add(boxInfos -> {
                                        boxInfos.vertical();
                                        boxInfos.expanded();
                                        boxInfos.title("Material");
                                        if (item.getTipo().isAltura())
                                            boxInfos.add(new Label("Altura: " + item.getAltura() + " cm"));
                                        if (item.getTipo().isLargura())
                                            boxInfos.add(new Label("Largura: " + item.getLargura() + " cm"));
                                        if (item.getTipo().isQtde())
                                            boxInfos.add(new Label("Qtde: " + item.getQtde()));
                                    });
                                    if (item.getTipo().isImagem()) {
                                        box.add(img -> {
                                            img.vertical();
                                            img.expanded();
                                            img.title("Imagem");
                                            img.add(new Label(item.getPathImagem() == null ? "Não Anexada" : "Anexada", ImageUtils.getIcon(item.getPathImagem() == null ? ImageUtils.Icon.STATUS_NAO : ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16)));
                                            img.add(new Label(item.isEnviadoFornecedor() ? "Enviada" : "Não Enviada", ImageUtils.getIcon(item.isEnviadoFornecedor() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16)));
                                        });
                                    }
                                }));
                            }
                        }
                    });
                }).build()

        );
    });

    private final FormTableView<MktItemSolicitacao> tblMateriais = FormTableView.create(MktItemSolicitacao.class, table -> {
        table.items.bind(materiaisBean);
        table.expanded();
        table.title("Materiais");
        table.selectColumn();
        table.editable.set(true);
        table.indices(
                statusList.stream().map(it -> table.indice(it.getStatus(), it.getStyle())).collect(Collectors.toList())
        );
        table.factoryRow(param -> new TableRow<MktItemSolicitacao>() {
            @Override
            protected void updateItem(MktItemSolicitacao item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    getStyleClass().add("table-row-" + item.getSolicitacao().getStatus().getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-primary", "table-row-info");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        final FormBox boxButtonsRow = FormBox.create(box -> {
                            box.vertical();
                            box.expanded();
                            box.setSpacing(5);
                        });

                        final FormButton btnAnexarFoto = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._24));
                            btn.tooltip("Anexar Imagem");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("warning");
                        });

                        final FormButton btnRemoverFoto = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            btn.tooltip("Remover imagem");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("danger");
                        });

                        final FormButton btnEnviarFornecedor = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                            btn.tooltip("Enviar para o fornecedor");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("primary");
                        });

                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                btnAnexarFoto.setOnAction(evt -> adicionarFotoFTP(item));
                                btnRemoverFoto.setOnAction(evt -> removerFotoFTP(item));
                                btnEnviarFornecedor.setOnAction(evt -> notificaFornecedor(item));

                                boxButtonsRow.setDisable(item.getSolicitacao().getStatus().getCodigo() != 3 || !item.getTipo().isImagem());

                                btnAnexarFoto.disable.set(item.getPathImagem() != null);

                                btnRemoverFoto.disable.set(item.getPathImagem() == null || item.isEnviadoFornecedor());
                                btnEnviarFornecedor.disable.set(item.getPathImagem() == null || item.isEnviadoFornecedor());

                                boxButtonsRow.setAlignment(Pos.CENTER);
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnAnexarFoto, btnEnviarFornecedor, btnRemoverFoto);
                                setGraphic(boxButtonsRow.build());
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Solicitação");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getSolicitacao().getId())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getSolicitacao().getMarca().getDescricao())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(220);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getTipo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(320);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSolicitacao().getCodcli().toString()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(200);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSolicitacao().getStatus().getStatus()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Imagem");
                    cln.width(170);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(box -> {
                                    box.vertical();
                                    box.expanded();
                                    box.alignment(Pos.CENTER);
                                    if (item.getTipo().isKits()) {
                                        box.add(new Label("Material Sem Imagem"));
                                    } else
                                        box.add(FormFieldImage.create(img -> {
                                            img.withoutTitle();
                                            img.alignment(Pos.CENTER);
                                            img.size(160, 120);

//                                            String urlImagem = item.getTipo().isImagem() ? item.getImagem().getUrl() : item.getMarca().getCodigo().equals("D") ? item.getTipo().getImgPadraoD() : item.getTipo().getImgPadraoF();
                                            box.setOnMouseClicked(evt -> {
                                                if (item.getImagem() != null && item.getTipo().isImagem() && evt.getButton().equals(MouseButton.PRIMARY)) {
                                                    showImageCompleta(item.getImagem().getUrl());
                                                }
                                            });
                                            if (item.getImagem() != null) img.image(item.getImagem().getImagem());
                                        }).build());
                                }));
                            }
                        }
                    });
                }).build(),
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Situação");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Imagem Anexada");
                                cln.width(120);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktItemSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.getPathImagem() != null ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._32));
                                        }
                                    }
                                });
                            }).build()
                    );
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Enviado Fornecedor");
                                cln.width(120);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktItemSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.isEnviadoFornecedor() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._32));
                                        }
                                    }
                                });
                            }).build()
                    );
                }),
                FormTableColumn.create(cln -> {
                    cln.title("Informações");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(box -> {
                                    box.vertical();
                                    box.expanded();
                                    box.add(boxInfos -> {
                                        boxInfos.vertical();
                                        boxInfos.expanded();
                                        boxInfos.alignment(Pos.CENTER);
                                        if (item.getTipo().isAltura())
                                            boxInfos.add(new Label("Altura: " + item.getAltura() + " cm"));
                                        if (item.getTipo().isLargura())
                                            boxInfos.add(new Label("Largura: " + item.getLargura() + " cm"));
                                        if (item.getTipo().isQtde())
                                            boxInfos.add(new Label("Qtde: " + item.getQtde()));
                                    });
                                }));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Data Criação");
                    cln.width(120);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getSolicitacao().getData())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Imagem Anexada");
                    cln.width(170);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktItemSolicitacao, MktItemSolicitacao>, ObservableValue<MktItemSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktItemSolicitacao, MktItemSolicitacao>() {
                        @Override
                        protected void updateItem(MktItemSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(box -> {
                                    box.vertical();
                                    box.expanded();
                                    box.alignment(Pos.CENTER);
                                    box.add(FormFieldImage.create(img -> {
                                        img.withoutTitle();
                                        img.alignment(Pos.CENTER);
                                        img.size(160, 120);
                                        Image image = null;
                                        try {
                                            image = new Image(item.getPathImagem(), 160, 120, true, true, true);
                                        } catch (Exception e) {
                                            image = null;
                                        }
                                        Image finalImage = image;
                                        box.setOnMouseClicked(evt -> {
                                            if (finalImage != null && item.getTipo().isImagem() && evt.getButton().equals(MouseButton.PRIMARY)) {
                                                showImageCompleta(item.getPathImagem());
                                            }
                                        });
                                        if (image != null) img.image(image);
                                    }).build());
                                }));
                            }
                        }
                    });
                }).build()
        );
    });

    private final FormTableView<MktEmail> tblEmails = FormTableView.create(MktEmail.class, table -> {
        table.items.bind(emailsBean);
        table.expanded();
        table.title("E-Mails");
//        table.indices(
//                Arrays.stream(MktEmail.TipoEmail.values()).map(it -> table.indice(it.toString(), it.getStyle())).collect(Collectors.toList())
//        );
        table.factoryRow(param -> new TableRow<MktEmail>() {
            @Override
            protected void updateItem(MktEmail item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty && item.getTipo() != null) {
                    getStyleClass().add("table-row-" + MktEmail.TipoEmail.valueOf(item.getTipo()).getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-primary", "table-row-info");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktEmail, MktEmail>, ObservableValue<MktEmail>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktEmail, MktEmail>() {
                        final HBox boxButtonsRow = new HBox(3);

                        final FormButton btnRemoverEmail = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Remover E-Mail");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("danger");
                        });

                        @Override
                        protected void updateItem(MktEmail item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                btnRemoverEmail.setOnAction(evt -> removerEmail(item));

                                boxButtonsRow.setAlignment(Pos.CENTER);

                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnRemoverEmail);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Email");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktEmail, MktEmail>, ObservableValue<MktEmail>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktEmail, MktEmail>() {
                        @Override
                        protected void updateItem(MktEmail item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormFieldText.create(field -> {
                                    field.editable.set(false);
                                    field.withoutTitle();
                                    field.width(150);
                                    field.value.set(item.getEmail());
                                    field.textField.setOnMouseClicked(evt -> {
                                        if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2) {
                                            field.editable.set(true);
                                        }
                                    });
                                    field.textField.setOnKeyPressed(evt -> {
                                        if (evt.getCode().equals(KeyCode.ENTER)) {
                                            handleAlterarEmail(item, field.value.getValueSafe());
                                            field.editable.set(false);
                                        }
                                    });
                                    field.textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                                        if (!newValue) {
                                            handleAlterarEmail(item, field.value.getValueSafe());
                                            field.editable.set(false);
                                        }
                                    });
                                }).build());
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(60);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktEmail, MktEmail>, ObservableValue<MktEmail>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktEmail, MktEmail>() {
                        @Override
                        protected void updateItem(MktEmail item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormButton.create(btn -> {
                                    btn.addStyle(item.isAtivo() ? "success" : "danger");
                                    btn.icon(ImageUtils.getIcon(item.isAtivo() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.setOnAction(evt -> {
                                        handleStatusEmail(item);
                                    });
                                }));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktEmail, MktEmail>, ObservableValue<MktEmail>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktEmail, MktEmail>() {
                        @Override
                        protected void updateItem(MktEmail item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormFieldComboBox.create(MktEmail.TipoEmail.class, field -> {
                                    field.withoutTitle();
                                    field.width(250);
                                    field.getComboBox().setPromptText("Selecionar...");
                                    field.items(new SimpleListProperty<>(FXCollections.observableArrayList(Arrays.asList(MktEmail.TipoEmail.values()))));
                                    if (item.getTipo() != null && !item.getTipo().equals("")) {
                                        field.value.set(MktEmail.TipoEmail.valueOf(item.getTipo()));
                                    }
                                    field.value.addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null) {
                                            handleTipoEmail(((MktEmail.TipoEmail) newValue), item);
                                        }
                                    });
                                }).build());
                            }
                        }
                    });
                }).build()

        );
    });

    private final FormTableView<MktTipoSolicitacao> tblTiposSolicitacao = FormTableView.create(MktTipoSolicitacao.class, table -> {
        table.items.bind(tiposSolicitacaoBean);
        table.title("Materiais Disponíveis");
        table.expanded();
        table.factoryRow(param -> new TableRow<MktTipoSolicitacao>() {
            @Override
            protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setContextMenu(FormContextMenu.create(cmenu -> {
                        cmenu.addItem(it -> {
                            it.setText("Mudar Código Marketing");
                            it.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.MARKETING, ImageUtils.IconSize._16));
                            it.disableProperty().bind(item.emEdicaoProperty().not().or(item.comCodigoMktProperty().not()));
                            it.setOnAction(evt -> {
                                mudarCodigoMarketingFrag(item);
                            });
                        });

                        cmenu.addItem(it -> {
                            it.setText("Alterar Imagem Padrão");
                            it.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EDITAR_IMAGEM, ImageUtils.IconSize._16));
                            it.disableProperty().bind(item.emEdicaoProperty().not().or(item.imagemProperty()).or(item.kitsProperty()));
                            it.setOnAction(evt -> {
                                mudarImagemPadraoFrag(item);
                            });
                        });
                    }));
                }
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(100);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final Button btnRemoverItem = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Excluir tipo de solicitação");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("danger");
                        });

                        final Button btnEditar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                            btn.tooltip("Editar tipo de solicitação");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("warning");
                        });

                        final Button btnCancelar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._16));
                            btn.tooltip("Cancelar Alterações");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("dark");
                        });

                        final Button btnConfirmar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                            btn.tooltip("Confirmar Alterações no tipo de solicitação");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("success");
                        });

                        @Override
                        protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {

                                btnRemoverItem.setOnAction(evt -> {
                                    removerTipoSolicitacao(item);
                                });

                                btnEditar.setOnAction(evt -> {
                                    item.setEmEdicao(true);
                                    tblTiposSolicitacao.refresh();
                                });

                                btnCancelar.setOnAction(evt -> {
                                    item.setEmEdicao(false);
                                    JPAUtils.getEntityManager().refresh(item);
                                    tblTiposSolicitacao.refresh();
                                });

                                btnConfirmar.setOnAction(evt -> {
                                    item.setEmEdicao(false);
                                    salvarAlteracoesTipo(item);
                                });

                                btnEditar.disableProperty().bind(item.emEdicaoProperty());
                                btnConfirmar.disableProperty().bind(item.emEdicaoProperty().not());

                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnRemoverItem, item.isEmEdicao() ? btnCancelar : btnEditar, btnConfirmar);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getId())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(170);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                        @Override
                        protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.value.setValue(item.getTipo());
                                    field.editable.bind(item.emEdicaoProperty());
                                    field.focusedListener((observable, oldValue, newValue) -> {
                                        if (!newValue) {
                                            item.setTipo(field.value.getValueSafe());
                                        }
                                    });
                                }).build());
                            }
                        }
                    });
                }).build()
        );
    });

    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Box">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabMateriais = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox tabControleTipos = (VBox) super.tabs.getTabs().get(2).getContent();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final FormButton btnAdicionarImg = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.title("Adicionar Imagem");
        btn.setOnAction(evt -> {
            adicionarImagem();
        });
    });

    private final FormButton btnRemoverTodasImagens = FormButton.create(btn -> {
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.title("Remover todas as imagens");
        btn.setOnAction(evt -> {
            removerTodasImagens();
        });
    });

    private final FormButton btnAdicionarMaterial = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.title("Adicionar Material");
        btn.setOnAction(evt -> {
            adicionarNovoTipoSolicitacao();
        });
    });

    private final FormButton btnAnexarImagem = FormButton.create(btn -> {
        btn.addStyle("warning");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._24));
        btn.title("Anexar Imagem");
        btn.setOnAction(evt -> {
            MktItemSolicitacao[] itens = tblMateriais.items.stream().filter(BasicModel::isSelected).toArray(MktItemSolicitacao[]::new);
            if (verificaAcoesMateriais("adicionar imagem ao material", itens)) {
                adicionarFotoFTP(itens);
                tblMateriais.refresh();
            }
        });
    });

    private final FormButton btnEnviarFornecedor = FormButton.create(btn -> {
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.title("Enviar Fornecedor");
        btn.setOnAction(evt -> {
            MktItemSolicitacao[] itens = tblMateriais.items.stream().filter(BasicModel::isSelected).toArray(MktItemSolicitacao[]::new);
            if (verificaAcoesMateriais("enviar para o fornecedor", itens)) {
                notificaFornecedor(itens);
                tblMateriais.refresh();
            }
        });
    });

    private final FormButton btnRemoveImgMaterial = FormButton.create(btn -> {
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.title("Remover Imagem");
        btn.setOnAction(evt -> {
            MktItemSolicitacao[] itens = tblMateriais.items.stream().filter(BasicModel::isSelected).toArray(MktItemSolicitacao[]::new);
            if (verificaAcoesMateriais("remover imagem do material", itens)) {
                removerFotoFTP(itens);
                tblMateriais.refresh();
            }
        });
    });

    private final FormButton btnAddEmail = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.title("Adicionar E-mail");
        btn.setOnAction(evt -> {
            adicionarEmail();
        });
    });
    // </editor-fold>

    public SolicitacoesMarketingView() {
        super("Solicitações de Marketing", ImageUtils.getImage(ImageUtils.Icon.MARKETING), new String[]{"Listagem", "Materiais", "Controle"});
        marcas = (List<Marca>) new FluentDao().selectFrom(Marca.class).where(it -> it.isIn("codigo", new String[]{"D", "F"})).resultList();
        initListagem();
        initMateriais();
        initControleTipos();
        super.tabs.getTabs().get(2).selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue && tiposSolicitacaoBean.size() == 0)
                carregaDadosTipos();
        });
    }

    //<editor-fold desc="Funcoes Init">
    private void initListagem() {

        tabListagem.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(clienteFilter.build(), representanteFilter.build(), colecaoFilter.build(), marcaFilter.build(), statusSolicFilter.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarSolicitacoes(
                                    clienteFilter.objectValues.stream().map(Entidade::getStringCodcli).toArray(),
                                    representanteFilter.objectValues.stream().map(Represen::getCodRep).toArray(),
                                    colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                    marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                    statusSolicFilter.value.getValue()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                solicitacoesBean.set(FXCollections.observableArrayList(solicitacoes));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        clienteFilter.clear();
                        representanteFilter.clear();
                        statusSolicFilter.getComboBox().getSelectionModel().clearSelection();
                    });
                }));
            });
            root.add(content -> {
                content.horizontal();
                content.expanded();
                content.add(tblSolicitacoes.build());
                content.add(section -> {
                    section.vertical();
                    section.add(boxResumo -> {
                        boxResumo.vertical();
                        boxResumo.add(linha -> {
                            linha.horizontal();
                            linha.add(idSolicitacaoField.build(), statusField.build(), qtdeField.build());
                        });
                        boxResumo.add(linha -> {
                            linha.horizontal();
                            linha.add(representanteField.build(), clienteField.build());
                        });
                        boxResumo.add(linha -> {
                            linha.horizontal();
                            linha.add(dataCriacaoField.build(), dataUltimaField.build(), tiposItensField.build());
                        });
                    });
                    section.add(tblItensSolicitacao.build());
                });
            });
        }));
    }

    private void initMateriais() {
        tabMateriais.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(materialFilter.build(), marcaMaterialFilter.build(), statusSolicMaterialFilter.build(), imgAdicionadaSegBtn.build(), enviadoSegBtn.build(), comImgSegBtn.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarMateriais(
                                    materialFilter.objectValues.stream().map(it -> it.getId()).toArray(),
                                    marcaMaterialFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                    imgAdicionadaSegBtn.value.getValue(),
                                    enviadoSegBtn.value.getValue(),
                                    comImgSegBtn.value.getValue()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                materiaisBean.set(new SimpleListProperty<>(FXCollections.observableArrayList(materiais)));
                                materiaisBean.forEach(material -> {
                                    material.selectedProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue && material.getSolicitacao().getStatus().getCodigo() != 3) {
                                            material.setSelected(false);
                                            MessageBox.create(message -> {
                                                message.message("Mude o status da solicitação para \"Desenvolvimento Marketing\" para selecioná-la");
                                                message.type(MessageBox.TypeMessageBox.WARNING);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        }
                                    });
                                });
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        materialFilter.clear();
                        marcaMaterialFilter.clear();
                        imgAdicionadaSegBtn.select(0);
                        enviadoSegBtn.select(0);
                    });
                }));
            });
            root.add(content -> {
                content.vertical();
                content.expanded();
                content.add(tblMateriais.build());
            });
            root.add(footer -> {
                footer.horizontal();
                footer.add(btnAnexarImagem, btnEnviarFornecedor, btnRemoveImgMaterial);
            });
        }));
    }

    private void initControleTipos() {
        tabControleTipos.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();
            root.add(boxTipos -> {
                boxTipos.vertical();
                boxTipos.expanded();
                boxTipos.add(FormTabPane.create(tpane -> {
                    tpane.expanded();
                    tpane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            marcaSelecionada = marcas.stream().filter(it -> it.getCodigo().equals(newValue.getId())).findFirst().orElse(null);
                        }
                    });
                    marcas.forEach(marca -> {
                        tpane.addTab(FormTab.create(tab -> {
                            tab.setId(marca.getCodigo());
                            tab.title(marca.getDescricao());
                            tab.icon(ImageUtils.getIcon(marca.getCodigo().equals("D") ? ImageUtils.Icon.LOGO_DLZ : ImageUtils.Icon.LOGO_FDL, ImageUtils.IconSize._16));
                            tab.setContent(FormBox.create(fbox -> {
                                fbox.flutuante();
                                fbox.expanded();
                                fbox.verticalScroll();

                                imagensSolicitacaoBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                    if (newValue == null || newValue.intValue() == 0) return;
                                    fbox.clear();
                                    desenhaImagens(marca, fbox);
                                });
                            }));
                        }));
                    });
                }));
                boxTipos.add(linha -> {
                    linha.horizontal();
                    linha.add(btnAdicionarImg, btnRemoverTodasImagens);
                });
                boxTipos.add(tblTiposSolicitacao.build());
                boxTipos.add(boxBtn -> {
                    boxBtn.horizontal();
                    boxBtn.add(btnAdicionarMaterial);
                });
            });
            root.add(boxImgs -> {
                boxImgs.vertical();

                boxImgs.add(tblEmails.build());
                boxImgs.add(boxBtn -> {
                    boxBtn.horizontal();
                    boxBtn.add(btnAddEmail);
                });
            });
        }));
    }
    //</editor-fold>

    private void carregaCampos(MktSolicitacao solicitacao) {
        idSolicitacaoField.value.setValue(String.valueOf(solicitacao.getId()));
        statusField.value.setValue(solicitacao.getStatus().getCodigo() + " - " + solicitacao.getStatus().getStatus());
        representanteField.value.setValue(solicitacao.getCodrep().getCodRep() + " - " + solicitacao.getCodrep().getRespon());
        clienteField.value.setValue(solicitacao.getCodcli().getCodcli() + " - " + solicitacao.getCodcli().getRazaosocial());
        qtdeField.value.setValue(String.valueOf(solicitacao.getItens().stream().reduce(0, (a, b) -> a + b.getQtde(), Integer::sum)));
        tiposItensField.value.setValue(solicitacao.getItens().stream().map(it -> it.getTipo().getTipo()).distinct().collect(Collectors.joining(",")));
        dataCriacaoField.value.setValue(StringUtils.toShortDateFormat(solicitacao.getData()));
        dataUltimaField.value.setValue(StringUtils.toShortDateFormat(solicitacao.getDataAtualizacao()));
    }

    private void liberarMarketing(MktSolicitacao solicitacao) {

        List<VSdDadosPedido> pedidos = (List<VSdDadosPedido>) new FluentDao().selectFrom(VSdDadosPedido.class).where(it -> it
                .equal("numero.codcli.codcli", solicitacao.getCodcli().getCodcli())
                .equal("status", "PENDENTE")
                .between("numero.periodo.prazo", ((Object) "0000"), "9999")
                .equal("numero.tabPre.usaFv", true)
                .equal("marca.codigo", solicitacao.getMarca().getCodigo())
                .equal("colecao.codigo", ((VSdDadosColecaoAtual) new FluentDao().selectFrom(VSdDadosColecaoAtual.class).where(eb -> eb.equal("marca", solicitacao.getMarca().getCodigo())).singleResult()).getColecao().getCodigo())
        ).resultList();

        if (pedidos.size() == 0) {
            MessageBox.create(message -> {
                message.message("Cliente não possui nenhum pedido pendente.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (pedidos.stream().noneMatch(it -> it.getNumero().getFinanceiro().equals("1"))) {
            MessageBox.create(message -> {
                message.message("Pedido do cliente " + solicitacao.getCodcli().getCodcli() + " - " + solicitacao.getCodcli().getRazaosocial() + " não foi liberado pelo financeiro ainda, aguarde ou entre em contato com o financeiro!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (solicitacao.getItens().stream().noneMatch(it -> it.getTipo().isFornecedor())) {
            mudarStatusSolicitacao(solicitacao, 5, false);
            return;
        }

        mudarStatusSolicitacao(solicitacao, 3, false);
        notificaMarketing(solicitacao);
    }

    private void mudarStatusSolicitacao(MktSolicitacao solicitacao, int novoStatus, boolean direto) {

        MktStatusSolicitacao statusSolicitacao = statusList.stream().filter(it -> it.getCodigo() == novoStatus).findFirst().orElse(null);

        if (statusSolicitacao == null) return;

        if (!direto) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja mudar o status para: " + statusSolicitacao.getStatus());
                message.showAndWait();
            }).value.get())) {
                return;
            }
        }

        solicitacao.setStatus(statusSolicitacao);
        solicitacao.setDataAtualizacao(LocalDate.now());
        solicitacao = new FluentDao().merge(solicitacao);

        SysLogger.addSysDelizLog("Solicitações Marketing", TipoAcao.EDITAR, String.valueOf(solicitacao.getId()), "Status da Solicitação: " + solicitacao.getId() + " teve status alterado para " + statusSolicitacao.getCodigo() + " - " + statusSolicitacao.getStatus());
        tblSolicitacoes.refresh();
        tblItensSolicitacao.refresh();
        carregaCampos(solicitacao);
        MessageBox.create(message -> {
            message.message("Status atualizado para " + statusSolicitacao.getStatus() + " com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void adicionarMarketingPedido(MktSolicitacao solicitacao) {

        if (!solicitacao.isOfVerificada() && solicitacao.getItens().stream().noneMatch(it -> it.getTipo().isKits())) {
            MessageBox.create(message -> {
                message.message("Para adionar o item de marketing ao pedido, a OF tem que ser verificada.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        List<VSdDadosPedido> pedidos = (List<VSdDadosPedido>) new FluentDao().selectFrom(VSdDadosPedido.class).where(it -> it
                .equal("numero.codcli.codcli", solicitacao.getCodcli().getCodcli())
                .equal("status", "PENDENTE")
                .between("numero.periodo.prazo", ((Object) "0000"), "9999")
                .equal("numero.tabPre.usaFv", true)
                .equal("marca.codigo", solicitacao.getMarca().getCodigo())
                .equal("colecao.codigo", ((VSdDadosColecaoAtual) new FluentDao().selectFrom(VSdDadosColecaoAtual.class).where(eb -> eb.equal("marca", solicitacao.getMarca().getCodigo())).singleResult()).getColecao().getCodigo())
        ).resultList();

        AtomicReference<VSdDadosPedido> pedido = new AtomicReference<>();
        mudarStatusSolicitacao(solicitacao, 5, true);

        if (pedidos == null || pedidos.size() == 0) {
            MessageBox.create(message -> {
                message.message("Nenhum pedido pendente disponível");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.show();
            });
            return;
        } else if (pedidos.size() == 1)
            pedido.set(pedidos.get(0));
        else {
            new Fragment().show(fg -> {
                final FormTableView<VSdDadosPedido> tblPedidos = FormTableView.create(VSdDadosPedido.class, table -> {
                    table.title("Pedidos");
                    table.expanded();
                    table.items.bind(new SimpleListProperty(FXCollections.observableArrayList(pedidos)));
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Número");
                                cln.width(100);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero().getNumero()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Cliente");
                                cln.width(200);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero().getCodcli().toString()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Representante");
                                cln.width(200);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero().getCodrep().toString()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Data Emissão");
                                cln.width(100);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getNumero().getDt_emissao())));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Qtde");
                                cln.width(80);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeOrig()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Valor");
                                cln.width(80);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorOrig()));
                            }).build()
                    );
                });

                fg.title("Selecione o Pedido do Cliente");
                fg.size(800.0, 600.0);
                fg.buttonsBox.getChildren().add(FormButton.create(btn -> {
                    btn.addStyle("success");
                    btn.title("Confirmar");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btn.setOnAction(evt -> {
                        if (tblPedidos.tableview().getSelectionModel().getSelectedItem() == null) {
                            MessageBox.create(message -> {
                                message.message("Nenhum pedido selecionado!");
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            return;
                        }
                        pedido.set(tblPedidos.tableview().getSelectionModel().getSelectedItem());
                        fg.close();
                    });
                }));
                fg.box.getChildren().add(FormBox.create(box -> {
                    box.vertical();
                    box.expanded();
                    box.add(tblPedidos.build());
                }));
            });
        }

        if (pedido.get() == null) return;

        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja adicionar os itens de marketing ao pedido " + pedido.get().getNumero().getNumero() + "?");
            message.showAndWait();
        }).value.get())) {
            return;
        }

        if (solicitacao.getItens().stream().anyMatch(it -> it.getTipo().isKits())) {
            if (!adicionarMarketing(pedido.get().getNumero(), solicitacao.getItens().stream().map(MktItemSolicitacao::getQtde).reduce(Integer::sum).get()))
                return;
        }

        Map<String, List<MktItemSolicitacao>> collectByCodigoMkt = solicitacao.getItens()
                .stream()
                .filter(it -> it.getTipo().isComCodigoMkt())
                .collect(Collectors.groupingBy(it -> it
                        .getSolicitacao().getMarca().getCodigo().equals("D") ?
                        it.getTipo().getCodMktD() :
                        it.getTipo().getCodMKtF()));

        collectByCodigoMkt.forEach((codigoMKt, list) -> {
            try {
                new FluentDao().persist(new SdMktPedido(pedido.get(), codigoMKt, list.size(), Globals.getNomeUsuario()));
            } catch (SQLException e) {
                MessageBox.create(message -> {
                    message.message("Não foi possível adicionar os itens de marketing ao pedido do cliente.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                mudarStatusSolicitacao(solicitacao, 4, true);
            }
        });
        mudarStatusSolicitacao(solicitacao, 6, true);

    }

    @NotNull
    private FormTableColumn createControlColumn(Field field) {
        return FormTableColumn.create(cln -> {
            cln.title(field.getName());
            cln.width(80);
            cln.alignment(Pos.CENTER);
            cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
            cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                @Override
                protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                    super.updateItem(item, empty);
                    FormButton btnHandleStatus = FormButton.create(btn -> {
                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    });
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        try {
                            BooleanProperty property = (BooleanProperty) field.get(item);
                            btnHandleStatus.disable.bind(item.emEdicaoProperty().not());
                            btnHandleStatus.addStyle(property.get() ? "success" : "danger");
                            btnHandleStatus.icon(ImageUtils.getIcon(property.get() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                            btnHandleStatus.setOnAction(evt -> handleBooleanAttribute(property));
                            cln.title(property.getName().equals("") ? "C/" + StringUtils.capitalizeWithoutLowerCase(field.getName()) : property.getName());
                            setGraphic(btnHandleStatus);
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            });
        }).build();
    }

    private void carregaDadosTipos() {

        ListProperty<MktTipoSolicitacao> tipos = new SimpleListProperty<>();
        ListProperty<MktImagemSolicitacao> imgs = new SimpleListProperty<>();
        ListProperty<MktEmail> emails = new SimpleListProperty<>();

        new RunAsyncWithOverlay(this).exec(task -> {
            tipos.set(FXCollections.observableArrayList((List<MktTipoSolicitacao>) new FluentDao().selectFrom(MktTipoSolicitacao.class).get().resultList()));
            imgs.set(FXCollections.observableArrayList((List<MktImagemSolicitacao>) new FluentDao().selectFrom(MktImagemSolicitacao.class).get().resultList()));
            emails.set(FXCollections.observableArrayList((List<MktEmail>) new FluentDao().selectFrom(MktEmail.class).get().resultList()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tiposSolicitacaoBean.set(tipos);
                imagensSolicitacaoBean.set(imgs);
                emailsBean.set(emails);
                for (Field field : MktTipoSolicitacao.class.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Transient.class) || !field.getType().isAssignableFrom(BooleanProperty.class))
                        continue;
                    field.setAccessible(true);
                    tblTiposSolicitacao.addColumn(createControlColumn(field));
                }
                tblTiposSolicitacao.addColumn(FormTableColumnGroup.createGroup(group -> {
                    group.title("Códigos de Mkt");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("DLZ");
                                cln.width(80);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.getCodMktD() == null || item.getCodMktD().equals("") ? ImageUtils.Icon.STATUS_NAO : ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._24));
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Flor de Lis");
                                cln.width(80);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.getCodMKtF() == null || item.getCodMKtF().equals("") ? ImageUtils.Icon.STATUS_NAO : ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._24));
                                        }
                                    }
                                });
                            }).build());
                }).build());
                tblTiposSolicitacao.addColumn(FormTableColumnGroup.createGroup(group -> {
                    group.title("Imagem Padrão");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("DLZ");
                                cln.width(80);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.getImgPadraoD() == null || item.getImgPadraoD().equals("") ? ImageUtils.Icon.STATUS_NAO : ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._24));
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Flor de Lis");
                                cln.width(80);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<MktTipoSolicitacao, MktTipoSolicitacao>, ObservableValue<MktTipoSolicitacao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<MktTipoSolicitacao, MktTipoSolicitacao>() {
                                    @Override
                                    protected void updateItem(MktTipoSolicitacao item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.getImgPadraoF() == null || item.getImgPadraoF().equals("") ? ImageUtils.Icon.STATUS_NAO : ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._24));
                                        }
                                    }
                                });
                            }).build());
                }).build());
                marcaSelecionada = marcas.get(0);
            }
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Funcoes Imagens Mkt">
    private void removerTodasImagens() {
        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja remover todas as imagens da marca " + marcaSelecionada.getDescricao() + "?");
            message.showAndWait();
        }).value.get())) {
            return;
        }
        List<MktImagemSolicitacao> toDelete = new ArrayList<>(imagensSolicitacaoBean.stream().filter(it -> it.getMarca().equals(marcaSelecionada)).collect(Collectors.toList()));
        for (MktImagemSolicitacao delete : toDelete) {
            removerImagem(delete);
        }
    }

    private void adicionarImagem() {

        List<File> files = new FileChooser().showOpenMultipleDialog(this.getScene().getWindow());

        if (files == null || files.size() == 0) return;
        List<MktImagemSolicitacao> imagensToAdd = new ArrayList<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            for (File file : files) {
                try {
                    connectFtp(file, true);
                    MktImagemSolicitacao mktImagemSolicitacao = new MktImagemSolicitacao("https://imagens.deliz.com.br/integracao/" + file.getName(), marcaSelecionada, 0);
                    mktImagemSolicitacao = new FluentDao().persist(mktImagemSolicitacao);
                    imagensToAdd.add(mktImagemSolicitacao);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                imagensSolicitacaoBean.addAll(imagensToAdd);
                MessageBox.create(message -> {
                    message.message("Imagens adicionadas com sucesso a marca " + marcaSelecionada.getDescricao());
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }

    private void showImageCompleta(String image) {
        new Fragment().show(fg -> {
            fg.modalStage.setFullScreen(true);
            fg.title("Imagem");

            fg.box.setBackground(new Background(new BackgroundImage(new Image(image, 0, fg.modalStage.getHeight(), true, true, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(100, 100, true, true, true, false))));
        });
    }

    private void removerImagem(MktImagemSolicitacao img) {

        List<MktItemSolicitacao> itens = (List<MktItemSolicitacao>) new FluentDao().selectFrom(MktItemSolicitacao.class).where(it -> it.equal("imagem.id", img.getId()).notEqual("solicitacao.status.codigo", 6)).resultList();
        if (itens.size() > 0) {
            MessageBox.create(message -> {
                message.message("Essa imagem não pode ser excluída pois está associada a uma solicitação aberta.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        try {
            new FluentDao().delete(img);
            imagensSolicitacaoBean.remove(img);
            connectFtp(new File(img.getUrl()), false);
        } catch (IOException e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Houve um erro ao excluír a imagem!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        MessageBox.create(message -> {
            message.message("Imagem excluída com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void desenhaImagens(Marca marca, FormBox fbox) {
        imagensSolicitacaoBean.stream().filter(it -> it.getMarca().equals(marca)).sorted(Comparator.comparing(MktImagemSolicitacao::getDirecao).thenComparing(MktImagemSolicitacao::getId)).forEach(img -> fbox.add(
                FormBadges.create(bgIconRemove -> {
                    bgIconRemove.position(Pos.TOP_RIGHT, 10.0);
                    ImageView removeIcon = ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32);
                    removeIcon.setOnMouseClicked(evt -> {
                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Tem certeza que deseja excluir essa imagem?");
                            message.showAndWait();
                        }).value.get())) {
                            removerImagem(img);
                        }
                    });
                    bgIconRemove.node(removeIcon);
                    bgIconRemove.badgedNode(
                            FormBadges.create(bgIdImg -> {
                                bgIdImg.position(Pos.TOP_LEFT, 10.0);
                                bgIdImg.text(lb -> {
                                    lb.boldText();
                                    lb.sizeText(25);
                                    lb.addStyle("info");
                                    lb.borderRadius(50);
                                    lb.value.set(String.valueOf(img.getId()));
                                });
                                bgIdImg.badgedNode(
                                        FormBox.create(boxImg -> {
                                            boxImg.vertical();
                                            boxImg.border();
                                            boxImg.setMaxWidth(400);
                                            VBox.setMargin(boxImg, new Insets(10, 0, 0, 0));
                                            boxImg.setBackground(new Background(new BackgroundFill(Color.FLORALWHITE, CornerRadii.EMPTY, Insets.EMPTY)));
                                            boxImg.add(imagem -> {
                                                imagem.vertical();
                                                imagem.height(200);
                                                imagem.setOnMouseClicked(evt -> {
                                                    if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2) {
                                                        showImageCompleta(img.getUrl());
                                                    }
                                                });
                                                Image image = new Image(img.getUrl(), 0, 600, true, true, true);
                                                imagem.setBackground(new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(100, 100, true, true, true, false))));
                                            });

                                            boxImg.add(boxFooter -> {
                                                boxFooter.vertical();
                                                boxFooter.setBackground(new Background(new BackgroundFill(Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY)));
                                                boxFooter.border();
                                                GridPane grid = new GridPane();
                                                grid.setHgap(10);
                                                grid.setVgap(10);
                                                boxFooter.add(grid);
                                                grid.add(FormLabel.create(lbl -> {
                                                    lbl.setText(img.getUrl().replaceAll("https://imagens.deliz.com.br/integracao/", "").replaceAll(".jpg", "").replaceAll(".png", ""));
                                                    lbl.sizeText(19);
                                                    lbl.boldText();
                                                }), 0, 0);
                                                Hyperlink acessar_imagem = new Hyperlink("Acessar Imagem");
                                                acessar_imagem.setOnAction(evt -> {
                                                    try {
                                                        Desktop.getDesktop().browse(new URL(img.getUrl()).toURI());
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    } catch (URISyntaxException e) {
                                                        e.printStackTrace();
                                                    }
                                                });
                                                grid.add(acessar_imagem, 0, 1);
                                                grid.add(FormButton.create(btn -> {
                                                    btn.icon(ImageUtils.getIcon(img.isAtivo() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._24));
                                                    btn.title(img.isAtivo() ? "Ativo" : "Inativo");
                                                    btn.addStyle(img.isAtivo() ? "success" : "danger");
                                                    btn.setOnAction(evt -> handleAtivoImagem(img, btn));
                                                    btn.width(150);
                                                }), 1, 0);
                                                grid.add(FormButton.create(btn -> {
                                                    btn.icon(ImageUtils.getIcon(img.getDirecao().equals("H") ? ImageUtils.Icon.LARGURA : ImageUtils.Icon.ALTURA, ImageUtils.IconSize._24));
                                                    btn.title(img.getDirecao().equals("H") ? "Horizontal" : "Vertical");
                                                    btn.addStyle(img.getDirecao().equals("H") ? "warning" : "primary");
                                                    btn.setOnAction(evt -> handleOrientacaoImagem(img, btn));
                                                    btn.width(150);
                                                }), 1, 1);
                                            });
                                        }));
                            }));
                })));
    }

    private void handleAtivoImagem(MktImagemSolicitacao img, FormButton btn) {
        img.setAtivo(!img.isAtivo());
        img = new FluentDao().merge(img);
        btn.icon(ImageUtils.getIcon(img.isAtivo() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._24));
        btn.title(img.isAtivo() ? "Ativo" : "Inativo");
        btn.addStyle(img.isAtivo() ? "success" : "danger");
        btn.removeStyle(!img.isAtivo() ? "success" : "danger");
        MessageBox.create(message -> {
            message.message("Direção da Imagem alterada com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void handleOrientacaoImagem(MktImagemSolicitacao img, FormButton btn) {
        img.setDirecao(img.getDirecao().equals("H") ? "V" : "H");
        img = new FluentDao().merge(img);
        btn.icon(ImageUtils.getIcon(img.getDirecao().equals("H") ? ImageUtils.Icon.LARGURA : ImageUtils.Icon.ALTURA, ImageUtils.IconSize._24));
        btn.title(img.getDirecao().equals("H") ? "Horizontal" : "Vertical");
        btn.addStyle(img.getDirecao().equals("H") ? "warning" : "primary");
        btn.removeStyle(!img.getDirecao().equals("H") ? "warning" : "primary");
        MessageBox.create(message -> {
            message.message("Status da imagem alterada com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Funcoes Tipo Solicitacao">
    private void removerTipoSolicitacao(MktTipoSolicitacao item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja excluir?");
            message.showAndWait();
        }).value.get())) {
            try {
                new FluentDao().delete(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tiposSolicitacaoBean.remove(item);
            MessageBox.create(message -> {
                message.message("Tipo excluído com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void handleBooleanAttribute(BooleanProperty booleanProperty) {
        booleanProperty.set(!booleanProperty.get());
        tblTiposSolicitacao.refresh();
    }

    private void salvarAlteracoesTipo(MktTipoSolicitacao item) {
        if (item.getTipo() == null || item.getTipo().equals("")) {
            MessageBox.create(message -> {
                message.message("O campo \"Tipo\" não pode estar vazio.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (item.getId() == 0) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja salvar esse novo Material?");
                message.showAndWait();
            }).value.get())) {
                return;
            }
            try {
                item = new FluentDao().persist(item);
                tblTiposSolicitacao.refresh();
                MessageBox.create(message -> {
                    message.message("Material criado e salvo com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        item = new FluentDao().merge(item);
        tblTiposSolicitacao.refresh();
        MessageBox.create(message -> {
            message.message("Alterações no Material salvas!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void adicionarNovoTipoSolicitacao() {
        MktTipoSolicitacao tipoSolicitacao = new MktTipoSolicitacao();
        tipoSolicitacao.setEmEdicao(true);
        tiposSolicitacaoBean.add(tipoSolicitacao);
        MessageBox.create(message -> {
            message.message("Novo Tipo de Solicitação adicionado");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Funcoes FTP">
    private void removerFotoFTP(MktItemSolicitacao... itens) {
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                for (MktItemSolicitacao item : itens) {
                    String pathImagem = item.getPathImagem();
                    List<MktItemSolicitacao> itensComMesmaImagem = (List<MktItemSolicitacao>) new FluentDao().selectFrom(MktItemSolicitacao.class).where(it -> it.equal("pathImagem", pathImagem)).resultList();
                    itensComMesmaImagem.remove(item);

                    if (itensComMesmaImagem.size() == 0) {
                        connectFtp(new File(pathImagem), false);
                    }

                    item.setPathImagem(null);
                    item = new FluentDao().merge(item);
                    SysLogger.addSysDelizLog("Solicitações Marketing", TipoAcao.EDITAR, String.valueOf(item.getSolicitacao().getId()), "Item " + item.getOrdem() + " da Solicitação: " + item.getSolicitacao().getId() + " teve sua foto removida do FTP!");
                }
            } catch (IOException e) {
                return ReturnAsync.EXCEPTION.value();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Imagem anexada com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                tblItensSolicitacao.refresh();
                tblMateriais.refresh();
            } else {
                MessageBox.create(message -> {
                    message.message("Houve um erro ao se comunicar com o FTP");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }

    private void adicionarFotoFTP(MktItemSolicitacao... itens) {
        File file = new FileChooser().showOpenDialog(Globals.getMainStage());
        if (file == null) return;

        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                connectFtp(file, true);
                for (MktItemSolicitacao item : itens) {
                    item.setPathImagem("https://imagens.deliz.com.br/integracao/" + file.getName());
                    item = new FluentDao().merge(item);
                    SysLogger.addSysDelizLog("Solicitações Marketing", TipoAcao.EDITAR, String.valueOf(item.getSolicitacao().getId()), "Item " + item.getOrdem() + " da Solicitação: " + item.getSolicitacao().getId() + " teve sua foto adicionada!");
                }
                return ReturnAsync.OK.value;
            } catch (Exception e) {
                return ReturnAsync.EXCEPTION.value();
            }
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Imagem anexada com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                tblItensSolicitacao.refresh();
                tblMateriais.refresh();
            } else {
                MessageBox.create(message -> {
                    message.message("Houve um erro ao se comunicar com o FTP");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }

    private void connectFtp(File file, boolean addOperation) throws IOException {
        FTPClient clientFTP = new FTPClient();
        try {
            clientFTP.connect(server, port);
            clientFTP.login(user, pass);
            clientFTP.enterLocalPassiveMode();
            clientFTP.changeWorkingDirectory("integracao");
            clientFTP.setFileType(FTP.BINARY_FILE_TYPE);

            boolean done;

            if (addOperation) {
                done = clientFTP.storeFile(file.getName(), Files.newInputStream(file.toPath()));
            } else {
                done = clientFTP.deleteFile(file.getPath().replace("https:\\imagens.deliz.com.br\\integracao\\", ""));
            }

            if (!done) throw new IOException();
        } finally {
            try {
                clientFTP.logout();
                clientFTP.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Funcoes Envio de Email">
    private void notificaMarketing(MktSolicitacao solicitacao) {
        List<String> emailsMkt = ((List<MktEmail>) new FluentDao().selectFrom(MktEmail.class).where(it -> it.like("tipo", "M" + solicitacao.getMarca().getCodigo())).resultList()).stream().map(MktEmail::getEmail).collect(Collectors.toList());
        if (emailsMkt.size() == 0) {
            MessageBox.create(message -> {
                message.message("Nenhum email de Marketing cadastrado para essa marca!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        String email = emailsMkt.remove(0);

        SimpleMail.INSTANCE
                .addDestinatario(email)
                .addCC(emailsMkt)
                .comCorpoHtml(() -> criaCorpoHtmlEmail(solicitacao))
                .send();
    }

    private String criaCorpoHtmlEmail(MktSolicitacao solicitacao) {
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Email SysDeliz</title>\n" +
                "    <style>\n" +
                "        table {\n" +
                "          border-collapse: collapse;\n" +
                "          width: 80%;\n" +
                "        }\n" +
                "        \n" +
                "        td, th {\n" +
                "          border: 1px solid #dddddd;\n" +
                "          text-align: left;\n" +
                "          padding: 5px;\n" +
                "        }\n" +
                "        \n" +
                "        tr:nth-child(even) {\n" +
                "          background-color: #dddddd;\n" +
                "        }\n" +
                "        </style>\n" +
                "</head>");
        sb.append("<body>\n" +
                "    <div>\n" +
                "        <h2>Nova Solicitação " + solicitacao.getId() + " </h2>\n" +
                "        \n" +
                "        <span style=\"margin-bottom: 25px;\">Olá, seguem os itens referentes a nova solicitação " + solicitacao.getId() + ".</span><br>");
        sb.append("<table>\n" +
                "            <tr>\n" +
                "                <th>Material</th>\n" +
                "                <th>Qtde</th>\n" +
                "                <th>Altura</th>\n" +
                "                <th>Largura</th>\n" +
                "                <th>Marca</th>\n" +
                "                <th>Imagem</th>\n" +
                "            </tr>\n");
        for (MktItemSolicitacao item : solicitacao.getItens()) {
            sb.append("<tr>" +
                    "<td>" + item.getTipo().getTipo() + "</td>\n" +
                    "<td>" + item.getQtde() + "</td>" +
                    "<td>" + (item.getAltura() == null ? "0" : (item.getAltura().toString() + " cm")) + "</td>" +
                    "<td>" + (item.getLargura() == null ? "0" : (item.getLargura().toString() + " cm")) + "</td>" +
                    "<td>" + item.getMarca().getDescricao() + "</td>" +
                    "<td>" + (item.getImagem() == null ? "Sem Img" : (item.getImagem().getUrl())) + "</td>" +
                    "</tr>\n");
        }
        sb.append("</table>\n" +
                "    </div>\n" +
                "</body>\n" +
                "</html>");
        return sb.toString();
    }

    private void notificarComercial(MktSolicitacao solicitacao) {
        List<String> emailsMkt = ((List<MktEmail>) new FluentDao().selectFrom(MktEmail.class).where(it -> it.like("tipo", "C")).resultList()).stream().map(MktEmail::getEmail).collect(Collectors.toList());
        if (emailsMkt.size() == 0) {
            MessageBox.create(message -> {
                message.message("Nenhum email do Comercial cadastrado!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        String email = emailsMkt.remove(0);
        SimpleMail.INSTANCE
                .addDestinatario(email)
                .addCC(emailsMkt)
                .comAssunto("Solicitação de Marketing " + solicitacao.getId())
                .comCorpo("<html><head>"
                        + "<meta charset=\"utf-8\">"
                        + "</head>\n"
                        + "<body><div><h2>Solicitação " + solicitacao.getId() + " enviada para o fornecedor!.</h2>"
                        + "</div>"
                        + "</body></html>")
                .send();
    }

    private void notificaFornecedor(MktItemSolicitacao... itens) {

        if (Arrays.stream(itens).allMatch(MktItemSolicitacao::isEnviadoFornecedor)) {
            MessageBox.create(message -> {
                message.message("Todos os itens selecionados já foram enviados para o fornecedor.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });

            for (MktItemSolicitacao itemSolicitacao : itens) {
                itemSolicitacao.setSelected(false);
            }

            return;
        }

        new RunAsyncWithOverlay(this).exec(task -> {

            Arrays.stream(itens).filter(it -> !it.isEnviadoFornecedor()).collect(Collectors.groupingBy(it -> it.getTipo().isFornPadrao())).forEach((isFornPadrao, list) -> {
                List<String> emailsFornecedor = ((List<MktEmail>) new FluentDao().selectFrom(MktEmail.class).where(it -> it.equal("tipo", "F")).resultList()).stream().map(it -> it.getEmail()).collect(Collectors.toList());
                if (!isFornPadrao) {

                }
                if (emailsFornecedor.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Nenhum email de Fornecedor cadastrado para essa marca!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    return;
                }
                String email = emailsFornecedor.remove(0);
                String filePath = "C:/SysDelizLocal/local_files/SolicitacoesMkt.pdf";
                try {
                    new ReportUtils().config().addReport(ReportUtils.ReportFile.SOLICITACOES_MARKETING, new ReportUtils.ParameterReport("itens", Arrays.stream(itens).map(it -> it.getSolicitacao().getId() + "" + it.getOrdem()).collect(Collectors.joining(",")))).view().toPdf(filePath);
                } catch (IOException | JRException | SQLException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                File file = new File(filePath);
                SimpleMail.INSTANCE
                        .addDestinatario(email)
                        .addCC(emailsFornecedor)
                        .comAssunto("Nova Solicitação de Marketing")
                        .addAttachment(file)
                        .comCorpo("<html><head>"
                                + "<meta charset=\"utf-8\">"
                                + "</head>\n"
                                + "<body><div>" +
                                "<h2>Nova Solicitação realizada.</h2><br/>" +
                                "<h3>Segue em anexo todas as informações referentes aos itens da nova solicitação.</h3><br/><br/>"
                                + "Qualquer dúvida ou necessidade estamos a disposição para atendê-lo.<br/><br/>"
                                + "Atenciosamente,<br/>"
                                + "Fone: (48) 3641-1900<br/>"
                                + "Whatsapp: (47) 9 9187-1164<br/>"
                                + "E-mail: comercial@deliz.com.br<br/>"
                                + "www.deliz.com.br</span>"
                                + "</div>"
                                + "</body></html>")
                        .send();
                file.delete();
                for (MktItemSolicitacao item : itens) {
                    item.setEnviadoFornecedor(true);
                    if (item.getSolicitacao().getItens().stream().allMatch(it -> it.isEnviadoFornecedor())) {
                        item.getSolicitacao().setStatus(statusList.stream().filter(it -> it.getCodigo() == 4).findFirst().get());
                        item.setSolicitacao(new FluentDao().merge(item.getSolicitacao()));
                    }
                    item = new FluentDao().merge(item);
                }
            });
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Email enviado para o fornecedor com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                tblMateriais.refresh();
                for (MktItemSolicitacao itemSolicitacao : itens) {
                    itemSolicitacao.setSelected(false);
                }
            }
        });
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Funcoes Email Mkt">
    private void handleTipoEmail(MktEmail.TipoEmail newValue, MktEmail item) {
        item.setTipo(newValue.name());
        if (item.getEmail() != null && !item.getEmail().equals("") && item.getTipo() != null && !item.getTipo().equals("")) {
            item = new FluentDao().merge(item);
            tblEmails.refresh();
        }
    }

    private void handleStatusEmail(MktEmail item) {
        item.setAtivo(!item.isAtivo());
        if (item.getEmail() != null && !item.getEmail().equals("") && item.getTipo() != null && !item.getTipo().equals("")) {
            item = new FluentDao().merge(item);
            tblEmails.refresh();
        }
    }

    private void handleAlterarEmail(MktEmail item, String novoEmail) {
        if (novoEmail.equals(item.getEmail())) return;
        item.setEmail(novoEmail);
        if (item.getEmail() != null && !item.getEmail().equals("") && item.getTipo() != null && !item.getTipo().equals("")) {
            item = new FluentDao().merge(item);
            tblEmails.refresh();
        }
    }

    private void removerEmail(MktEmail item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja remover esse email?");
            message.showAndWait();
        }).value.get())) {
            try {
                new FluentDao().delete(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
            emailsBean.remove(item);
            tblEmails.refresh();
            MessageBox.create(message -> {
                message.message("E-Mail removido com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void adicionarEmail() {
        if (emailsBean.stream().anyMatch(it -> it.getEmail().equals("") && it.getTipo().equals(""))) {
            MessageBox.create(message -> {
                message.message("Termine de inserir os valores no novo emails antes de adicionar um novo.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        emailsBean.add(new MktEmail());
        tblEmails.refresh();
    }
    // </editor-fold>

    private boolean verificaAcoesMateriais(String acao, MktItemSolicitacao... itens) {
        if (itens.length == 0) {
            MessageBox.create(message -> {
                message.message("Nenhum material selecionado!\nSelecione ao menos um.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return false;
        }

        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja " + acao + "?");
            message.showAndWait();
        }).value.get())) {
            return true;
        }

        return false;
    }

    private void mudarCodigoMarketingFrag(MktTipoSolicitacao item) {
        new Fragment().show(fg -> {
            fg.title("Mudar Código de Marketing");
            fg.size(750.0, 100.0);

            final FormFieldSingleFind<VSdDadosProduto> mktDlzFilter = FormFieldSingleFind.create(VSdDadosProduto.class, field -> {
                field.title("Código Mkt DLZ");
                field.dividedWidth(300);
                VSdDadosProduto produto = new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codigo", item.getCodMktD())).singleResult();
                if (produto != null)
                    field.value.setValue(produto);
            });

            final FormFieldSingleFind<VSdDadosProduto> mktFlorFilter = FormFieldSingleFind.create(VSdDadosProduto.class, field -> {
                field.title("Código MKt Flor de Lis");
                field.dividedWidth(300);
                VSdDadosProduto produto = new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codigo", item.getCodMKtF())).singleResult();
                if (produto != null)
                    field.value.setValue(produto);
            });

            FormButton btnConfirmar = FormButton.create(btn -> {
                btn.title("Confirmar");
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    if (mktDlzFilter.value.getValue() != null)
                        item.setCodMktD(mktDlzFilter.value.getValue().getCodigo());
                    if (mktFlorFilter.value.getValue() != null)
                        item.setCodMKtF(mktFlorFilter.value.getValue().getCodigo());
                    new FluentDao().merge(item);
                    tblTiposSolicitacao.refresh();
                    fg.close();
                });
            });

            fg.box.getChildren().add(FormBox.create(root -> {
                root.horizontal();
                root.add(mktDlzFilter.build());
                root.add(mktFlorFilter.build());
            }));

            fg.buttonsBox.getChildren().add(btnConfirmar);
        });
    }

    private void mudarImagemPadraoFrag(MktTipoSolicitacao item) {
        new Fragment().show(fg -> {
            fg.title("Mudar Imagem Padrão");
            fg.size(750.0, 100.0);

            FormButton btnConfirmar = FormButton.create(btn -> {
                btn.title("Confirmar");
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    // TODO: 12/08/2022  Salvar no FTP em pasta diferente e remover imagem antiga
                    new FluentDao().merge(item);
                    tblTiposSolicitacao.refresh();
                    fg.close();
                });
            });

            fg.box.getChildren().add(FormBox.create(root -> {
                root.horizontal();
                root.add(createBoxImgPadrao(item, item.imgPadraoDProperty(), "DLZ"));
                root.add(createBoxImgPadrao(item, item.imgPadraoFProperty(), "FLOR DE LIS"));
            }));

            fg.buttonsBox.getChildren().add(btnConfirmar);
        });
    }

    private void cancelarSolicitacao(MktSolicitacao item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja cancelar essa solicitação?");
            message.showAndWait();
        }).value.get())) {
            item.setStatus(statusList.stream().filter(it -> it.getCodigo() == 7).findFirst().get());
            item = new FluentDao().merge(item);
            tblSolicitacoes.refresh();
            MessageBox.create(message -> {
                message.message("Solicitação cancelada com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Solicitações Marketing", TipoAcao.EDITAR, String.valueOf(item.getId()), "Solicitação " + item.getId() + " de Marketing cancelada.");
        }
    }

    private void verificarOfSolicitacao(MktSolicitacao item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja verificar a Of de marketing desse cliente?");
            message.showAndWait();
        }).value.get())) {
            item.setOfVerificada(true);
            item = new FluentDao().merge(item);
            tblSolicitacoes.refresh();
            MessageBox.create(message -> {
                message.message("Of do cliente verificada");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Solicitações Marketing", TipoAcao.EDITAR, String.valueOf(item.getId()), "Of da solicitação foi verificada");
        }
    }

    @NotNull
    private Consumer<FormBox> createBoxImgPadrao(MktTipoSolicitacao item, StringProperty stringProperty, String marca) {
        return boxMarca -> {
            FormBox boxImage = FormBox.create(boxImg -> {
                boxImg.height(400);
                boxImg.setOnMouseClicked(evt -> {
                    if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2) {
                        showImageCompleta(stringProperty.getValueSafe().equals("") ? "https://imagens.deliz.com.br/produtos/semimg.jpg" : stringProperty.getValueSafe());
                    }
                });
                Image image = new Image(stringProperty.getValueSafe().equals("") ? "https://imagens.deliz.com.br/produtos/semimg.jpg" : stringProperty.getValueSafe(), 0, 600, true, true, true);
                boxImg.setBackground(new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(100, 100, true, true, true, false))));
            });
            boxMarca.vertical();
            boxMarca.title(marca);
            boxMarca.add(boxImage.build());
            boxMarca.add(footer -> {
                footer.horizontal();
                footer.add(FormFieldText.create(field -> {
                    field.editable.set(false);
                    field.width(300);
                    field.title("Imagem Padrão " + marca);
                    field.value.bind(stringProperty);
                    field.textField.setOnMouseClicked(evt -> {
                        if ((evt.getClickCount() == 2 && evt.getButton().equals(MouseButton.PRIMARY)) || evt.getButton().equals(MouseButton.SECONDARY)) {
                            File file = new FileChooser().showOpenDialog(this.getScene().getWindow());
                            if (file == null || !file.exists()) return;

                            stringProperty.setValue(file.getAbsolutePath());
                        }
                    });
                    stringProperty.addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            try {
                                boxImage.setBackground(new Background(new BackgroundImage(new Image(new FileInputStream(stringProperty.getValue()), 0, 800, true, true),
                                        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(100, 100, true, true, true, false))));
                            } catch (FileNotFoundException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                    field.width(250);
                }).build());
            });
        };
    }
}
