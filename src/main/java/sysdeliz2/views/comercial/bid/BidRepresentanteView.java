package sysdeliz2.views.comercial.bid;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.views.comercial.bid.BidRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdPropriedade;
import sysdeliz2.models.sysdeliz.comercial.SdBid;
import sysdeliz2.models.sysdeliz.comercial.SdBil;
import sysdeliz2.models.sysdeliz.comercial.VSdBidCidade;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class BidRepresentanteView extends BidRepresentanteController {

    // <editor-fold defaultstate="collapsed" desc="locals">
    private final BigDecimal limiteMeta = new BigDecimal(0.8);
    private final BigDecimal sucessoMeta = BigDecimal.ONE;
    private String textoSalvo = "";
    private BigDecimal bigDecimal100 = new BigDecimal(100);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tabs">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabBid = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components listagem">
    private final FormFieldMultipleFind<Represen> fieldFilterRepresentante = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representantes");
        field.width(220.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marcas");
        field.width(120.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleções");
        field.width(180.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Regiao> fieldFilterRegiao = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Região");
        field.width(100.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<TabUf> fieldFilterUf = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("UF");
        field.width(100.0);
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldFilterGlobais = FormFieldSegmentedButton.create(field -> {
        field.title("Globais");
        field.options(
                field.option("Sem Globais", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Com Globais", "B", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Somente Globais", "C", FormFieldSegmentedButton.Style.DEFAULT)
        );
        field.select(0);
    });
    private final FormTableView<SdBid> tblBid = FormTableView.create(SdBid.class, table -> {
        table.title("Representantes");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdBid, SdBid>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnAbrirBid = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Abrir BID do representante");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });

                            @Override
                            protected void updateItem(SdBid item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnAbrirBid.setOnAction(evt -> {
                                        tblNavegation.selectItem(item);
                                        tabs.getSelectionModel().select(1);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAbrirBid);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cód.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodrep()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cód.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRepresent()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Responsável");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build() /*Responsável*/,
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUfrep()));
                    cln.alignment(Pos.CENTER);
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Região");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRegiao()));
                }).build() /*Região*/,
                FormTableColumn.create(cln -> {
                    cln.title("Gerente");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGerente()));
                    cln.hide();
                }).build() /*Gerente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodmar()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodcol()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Mostruário");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMostr()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mostruário*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Total de Peças (TP)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTpmta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>(){
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toIntegerFormat(item.intValue()));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTprcm()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>(){
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toIntegerFormat(item.intValue()));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTpRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Total de Peças (TP)*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Total Financeiro (TF)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(90.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTfmta()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(90.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTfrcm()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTfRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Total Financeiro (TF)*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Clientes (PV)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPvmta()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPvrcm()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPvRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Clientes (PV)*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Clientes Novos (CLN)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getClnmta()));
                                cln.alignment(Pos.CENTER);
                                cln.hide();
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getClnrcm()));
                                cln.alignment(Pos.CENTER);
                                cln.hide();
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getClnRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.hide();
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Clientes Novos (CLN)*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Preço Médio (PM)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPmmta()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPmrcm()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPmRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Preço Médio (PM)*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Prof. Referência (PR)");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Mta");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrmta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>(){
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item.multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP)));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Mta*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Real");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrrcm()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>(){
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item.multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP)));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Real*/,
                            FormTableColumn.create(cln -> {
                                cln.title("% Mta");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrRelMta()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<SdBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            getStyleClass().removeAll("table-row-success", "table-row-warning");
                                            if (item != null && !empty) {
                                                setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                                if (item.compareTo(BigDecimal.ONE) >= 0)
                                                    getStyleClass().add("table-row-success");
                                                else if (item.compareTo(new BigDecimal(.8)) >= 0)
                                                    getStyleClass().add("table-row-warning");
                                            }
                                        }
                                    };
                                });
                            }).build() /*% Mta*/
                    );
                }).build() /*Prof. Referência (PR)*/
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="componets bid">
    private final FormTableView<VSdBidCidade> tblBidCidade = FormTableView.create(VSdBidCidade.class, table -> {
        table.title("Cidades");
        table.expanded();
        table.fontSize(9);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidade, VSdBidCidade>, ObservableValue<VSdBidCidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCidade().getCodEst().getId().getSiglaEst()));
                    cln.alignment(Pos.CENTER);
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(170.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidade, VSdBidCidade>, ObservableValue<VSdBidCidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCidade().getNomeCid()));
                }).build() /*Cidade*/,
                FormTableColumn.create(cln -> {
                    cln.title("IPC");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidade, VSdBidCidade>, ObservableValue<VSdBidCidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCidade().getIpc()));
                    cln.alignment(Pos.CENTER);
                }).build() /*IPC*/,
                FormTableColumn.create(cln -> {
                    cln.title("Class.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidade, VSdBidCidade>, ObservableValue<VSdBidCidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCidade().getClasse()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdBidCidade, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("classeCidadeV", "classeCidadeP", "classeCidadeS", "classeCidadeE");
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add(item.equals("VV") || item.equals("V") ? "classeCidadeV" : item.equals("P") ? "classeCidadeP" : item.equals("S") ? "classeCidadeS" : "classeCidadeE");
                                }
                            }
                        };
                    });
                }).build() /*Class.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Sts");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidade, VSdBidCidade>, ObservableValue<VSdBidCidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAtendimentoGrupos()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdBidCidade, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-amber", "table-row-primary");
                                if (item != null && !empty) {
                                    setText(item.toString());
                                    getStyleClass().add(item == 1 ? "table-row-success" : item == 2 ? "table-row-primary" : item == 3 ? "table-row-warning" : item == 4 ? "table-row-amber" : "table-row-danger");
                                }
                            }
                        };
                    });
                }).build() /*Sts*/
        );
    });
    private final FormTableView<SdBid> tblNavegation = FormTableView.create(SdBid.class, table -> {
        table.withoutHeader();
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.items.bind(tblBid.items);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cód.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdBid, SdBid>() {
                            @Override
                            protected void updateItem(SdBid item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-danger", "table-row-info", "table-row-warning");
                                if (item != null && !empty) {
                                    setText(item.getId().getCodrep());
                                    getStyleClass().add(item.getId().getCodmar().equals("D") ? "table-row-info" : item.getId().getCodmar().equals("F") ? "table-row-danger" : "table-row-warning");
                                }
                            }
                        };
                    });
                }).build() /*Cód.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Responsável");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build() /*Responsável*/,
                FormTableColumn.create(cln -> {
                    cln.title("Represen.");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBid, SdBid>, ObservableValue<SdBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRepresent()));
                }).build() /*Represen.*/
        );
        table.indices(
                table.indice("DLZ", "info", ""),
                table.indice("Flor de Lis", "danger", "")
        );
    });
    private final FormNavegation<SdBid> navBid = FormNavegation.create(nav -> {
        nav.withNavegation(tblNavegation);
        nav.withSendMail(tblBid.tableProperties().getSelectionModel().selectedItemProperty().isNull().and(tblBid.items.emptyProperty()),
                evt -> enviarBid(tblNavegation.items.stream().filter(SdBid::isSelected).collect(Collectors.toList())));
        nav.withPrint(tblBid.tableProperties().getSelectionModel().selectedItemProperty().isNull().and(tblBid.items.emptyProperty()),
                evt -> imprimirBid(tblNavegation.items.stream().filter(SdBid::isSelected).collect(Collectors.toList())));

        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                loadBid((SdBid) newValue);
            }
        });
    });
    private final FormBox boxBid = FormBox.create(boxBid -> {
        boxBid.vertical();
        boxBid.expanded();
    });
    // <editor-fold defaultstate="collapsed" desc="representante">
    private final StringProperty beanRepresentante = new SimpleStringProperty();
    private final StringProperty beanRegiao = new SimpleStringProperty();
    private final StringProperty beanSemana = new SimpleStringProperty();
    private final StringProperty beanColecao = new SimpleStringProperty();
    private final ImageView beanLogoMarca = ImageUtils.getImage("C:\\SysDelizLocal\\local_files\\fg_bg_t_2x.bmp", 70.0, ImageUtils.FitType.HEIGHT);
    private final FormBox boxRepresentante = FormBox.create(box -> {
        box.horizontal();
        box.add(dados -> {
            dados.horizontal();
            dados.expanded();
            dados.add(values -> {
                values.vertical();
                values.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.width(400.0);
                    field.label("Repr.");
                    field.addStyle("xs");
                    field.editable(false);
                    field.value.bind(beanRepresentante);
                }).build());
                values.add(sub -> sub.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.width(250.0);
                            field.label("Região");
                            field.addStyle("xs");
                            field.editable(false);
                            field.alignment(Pos.CENTER);
                            field.value.bind(beanRegiao);
                        }).build(),
                        FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.width(150);
                            field.label("Semana");
                            field.addStyle("xs");
                            field.editable(false);
                            field.alignment(Pos.CENTER);
                            field.value.bind(beanSemana);
                        }).build()));
                values.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.width(250.0);
                    field.label("Coleção");
                    field.addStyle("xs");
                    field.editable(false);
                    field.value.bind(beanColecao);
                }).build());
            });
            dados.add(space -> {
                space.vertical();
                space.expanded();
            });
            dados.add(beanLogoMarca);
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="clientes novos/mostruario">
    private final FormFieldText fieldMostruario = FormFieldText.create(field -> {
        field.title("Mostruário");
        field.width(94);
        field.editable(false);
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldClientesReativados = FormFieldText.create(field -> {
        field.title("Cli. Reativados");
        field.width(94);
        field.editable(false);
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldRealizadoClientesNovos = FormFieldText.create(field -> {
        field.title("Clientes Novos");
        field.width(94);
        field.editable(false);
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldLastYearClientesNovos = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(94);
        field.editable(false);
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldMetaClientesNovos = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(94);
        field.editable(false);
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldAtendMetaClientesNovos = FormFieldText.create(field -> {
        field.title("% Meta");
        field.width(94);
        field.editable(false);
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormBox boxTitleMostruario = FormBox.create(boxTitle -> {
        boxTitle.horizontal();
        boxTitle.addStyle("proposicaoDlz");
        boxTitle.alignment(Pos.CENTER);
        boxTitle.add(FormLabel.create(label -> {
            label.value.set("ABERTURA (Arrancada)");
        }).build());
    });
    private final FormBox boxIndicadorMostruario = FormBox.create(boxIndicador -> {
        boxIndicador.vertical();
        boxIndicador.addStyle("proposicaoDlz");
        boxIndicador.spacing(5);
        boxIndicador.padding(3);
        boxIndicador.add(title -> {
            title.horizontal();
            title.padding(0);
            title.add(FormFieldText.create(field -> {
                field.withoutTitle();
                field.width(194);
                field.editable(false);
                field.alignment(Pos.CENTER);
                field.addStyle("dark");
                field.value.set("Clientes Novos / Mostruário");
            }).build());
        });
        boxIndicador.add(mostruario -> {
            mostruario.horizontal();
            mostruario.padding(0);
            mostruario.add(fieldMostruario.build());
            mostruario.add(fieldClientesReativados.build());
        });
        boxIndicador.add(clientes -> {
            clientes.horizontal();
            clientes.add(fieldRealizadoClientesNovos.build());
            clientes.add(fieldLastYearClientesNovos.build());
        });
        boxIndicador.add(metas -> {
            metas.horizontal();
            metas.add(fieldMetaClientesNovos.build());
            metas.add(fieldAtendMetaClientesNovos.build());
        });
    });
    private final FormBox boxMostCliNovos = FormBox.create(box -> {
        box.vertical();
        box.add(boxTitleMostruario);
        box.add(BoxIndicadores -> {
            BoxIndicadores.horizontal();
            BoxIndicadores.height(200);
            BoxIndicadores.alignment(Pos.CENTER);
            BoxIndicadores.add(boxIndicadorMostruario);
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="proposicao">
    // ------- PV
    private final FormFieldText fieldPvPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPvLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPvRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPvMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPvDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- CD
    private final FormFieldText fieldCdPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldCdLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldCdRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldCdMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldCdDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- PR
    private final FormFieldText fieldPrPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPrLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPrRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPrMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPrDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- PG
    private final FormFieldText fieldPgPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPgLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPgRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPgMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPgDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- BOX
    private final FormBox boxTitleProposicao = FormBox.create(boxTitle -> {
        boxTitle.horizontal();
        boxTitle.addStyle("proposicaoDlz");
        boxTitle.alignment(Pos.CENTER);
        boxTitle.add(FormLabel.create(label -> {
            label.value.set("PROPOSIÇÃO (A Jornada)");
        }).build());
    });
    private final FormBox boxPvProposicao = FormBox.create(boxPv -> {
        boxPv.vertical();
        boxPv.addStyle("proposicaoDlz");
        boxPv.addStyle("pvDlz");
        boxPv.size(200.0, 200.0);
        boxPv.spacing(2);
        boxPv.padding(.0);
        boxPv.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("PV");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Pontos de Venda");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxPv.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPvPotencial.build());
            values.add(fieldPvLastYear.build());
        });
        boxPv.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPvRealizado.build());
            values.add(fieldPvMeta.build());
        });
        boxPv.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPvDesempenho.build());
        });
    });
    private final FormBox boxCdProposicao = FormBox.create(boxCd -> {
        boxCd.vertical();
        boxCd.addStyle("proposicaoDlz");
        boxCd.addStyle("cdDlz");
        boxCd.size(200.0, 200.0);
        boxCd.spacing(2);
        boxCd.padding(.0);
        boxCd.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("CD");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Cidades Alvo");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxCd.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldCdPotencial.build());
            values.add(fieldCdLastYear.build());
        });
        boxCd.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldCdRealizado.build());
            values.add(fieldCdMeta.build());
        });
        boxCd.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldCdDesempenho.build());
        });
    });
    private final FormBox boxPrProposicao = FormBox.create(boxPr -> {
        boxPr.vertical();
        boxPr.addStyle("proposicaoDlz");
        boxPr.addStyle("prDlz");
        boxPr.size(200.0, 200.0);
        boxPr.spacing(2);
        boxPr.padding(.0);
        boxPr.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("PR");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Profundidade Referência");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxPr.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPrPotencial.build());
            values.add(fieldPrLastYear.build());
        });
        boxPr.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPrRealizado.build());
            values.add(fieldPrMeta.build());
        });
        boxPr.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPrDesempenho.build());
        });
    });
    private final FormBox boxPgProposicao = FormBox.create(boxPg -> {
        boxPg.vertical();
        boxPg.addStyle("proposicaoDlz");
        boxPg.addStyle("pgDlz");
        boxPg.size(200.0, 200.0);
        boxPg.spacing(2);
        boxPg.padding(.0);
        boxPg.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("PG");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Profundidade Grade");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxPg.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPgPotencial.build());
            values.add(fieldPgLastYear.build());
        });
        boxPg.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPgRealizado.build());
            values.add(fieldPgMeta.build());
        });
        boxPg.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPgDesempenho.build());
        });
    });
    private final FormBox boxProposicao = FormBox.create(box -> {
        box.vertical();
        box.expanded();
        box.add(boxTitleProposicao);
        box.add(boxIndicadores -> {
            boxIndicadores.horizontal();
            boxIndicadores.alignment(Pos.CENTER);
            boxIndicadores.add(boxPvProposicao);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxCdProposicao);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxPrProposicao);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxPgProposicao);
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="performance">
    // ------- PP
    private final FormFieldText fieldPpPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPpLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPpRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPpMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPpDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- VP
    private final FormFieldText fieldVpPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldVpLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldVpRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldVpMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldVpDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- PM
    private final FormFieldText fieldPmPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPmLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPmRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPmMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPmDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- TP
    private final FormFieldText fieldTpPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTpLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTpRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTpMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTpDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // ------- TF
    private final FormFieldText fieldTfPotencial = FormFieldText.create(field -> {
        field.title("Potencial");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTfLastYear = FormFieldText.create(field -> {
        field.title("Last Year");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTfRealizado = FormFieldText.create(field -> {
        field.title("Realizado");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.fontBold();
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTfMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.width(97);
        field.editable(false);
        field.addStyle("xs");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTfDesempenho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Desempenho");
        field.width(194);
        field.editable(false);
        field.fontBold();
        field.addStyle("danger");
        field.setPadding(0);
        field.alignment(Pos.CENTER);
    });
    // -------- BOX
    private final FormBox boxTitlePerformance = FormBox.create(boxTitle -> {
        boxTitle.horizontal();
        boxTitle.addStyle("performanceDlz");
        boxTitle.alignment(Pos.CENTER);
        boxTitle.add(FormLabel.create(label -> {
            label.value.set("PERFORMANCE (A Chegada)");
        }).build());
    });
    private final FormBox boxPpPerformance = FormBox.create(boxPp -> {
        boxPp.vertical();
        boxPp.addStyle("performanceDlz");
        boxPp.addStyle("tpDlz");
        boxPp.size(200.0, 200.0);
        boxPp.spacing(2);
        boxPp.padding(.0);
        boxPp.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("PP");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Peças por Pedido");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxPp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPpPotencial.build());
            values.add(fieldPpLastYear.build());
        });
        boxPp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPpRealizado.build());
            values.add(fieldPpMeta.build());
        });
        boxPp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPpDesempenho.build());
        });
    });
    private final FormBox boxVpPerformance = FormBox.create(boxVp -> {
        boxVp.vertical();
        boxVp.addStyle("performanceDlz");
        boxVp.addStyle("vpDlz");
        boxVp.size(200.0, 200.0);
        boxVp.spacing(2);
        boxVp.padding(.0);
        boxVp.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("VP");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("R$ por Pedido");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxVp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldVpPotencial.build());
            values.add(fieldVpLastYear.build());
        });
        boxVp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldVpRealizado.build());
            values.add(fieldVpMeta.build());
        });
        boxVp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldVpDesempenho.build());
        });
    });
    private final FormBox boxPmPerformance = FormBox.create(boxPm -> {
        boxPm.vertical();
        boxPm.addStyle("performanceDlz");
        boxPm.addStyle("pmDlz");
        boxPm.size(200.0, 200.0);
        boxPm.spacing(2);
        boxPm.padding(.0);
        boxPm.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("PM");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Preço Médio");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxPm.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPmPotencial.build());
            values.add(fieldPmLastYear.build());
        });
        boxPm.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPmRealizado.build());
            values.add(fieldPmMeta.build());
        });
        boxPm.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldPmDesempenho.build());
        });
    });
    private final FormBox boxTpPerformance = FormBox.create(boxTp -> {
        boxTp.vertical();
        boxTp.addStyle("performanceDlz");
        boxTp.addStyle("tpDlz");
        boxTp.size(200.0, 200.0);
        boxTp.spacing(2);
        boxTp.padding(.0);
        boxTp.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("TP");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Total de Peças");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxTp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTpPotencial.build());
            values.add(fieldTpLastYear.build());
        });
        boxTp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTpRealizado.build());
            values.add(fieldTpMeta.build());
        });
        boxTp.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTpDesempenho.build());
        });
    });
    private final FormBox boxTfPerformance = FormBox.create(boxTf -> {
        boxTf.vertical();
        boxTf.addStyle("performanceDlz");
        boxTf.addStyle("tfDlz");
        boxTf.size(200.0, 200.0);
        boxTf.spacing(2);
        boxTf.padding(.0);
        boxTf.add(head -> {
            head.horizontal();
            head.expanded();
            head.alignment(Pos.TOP_LEFT);
            head.spacing(0);
            head.padding(3);
            head.add(values -> {
                values.vertical();
                values.alignment(Pos.TOP_RIGHT);
                values.spacing(0);
                values.padding(0);
                values.add(FormLabel.create(label -> {
                    label.value.set("TF");
                    label.width(194.0);
                    label.sizeText(26);
                    label.boldText();
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
                values.add(FormLabel.create(label -> {
                    label.value.set("Total Financeiro");
                    label.width(194.0);
                    label.padding(0);
                    label.alignment(Pos.CENTER_RIGHT);
                }).build());
            });
        });
        boxTf.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTfPotencial.build());
            values.add(fieldTfLastYear.build());
        });
        boxTf.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTfRealizado.build());
            values.add(fieldTfMeta.build());
        });
        boxTf.add(values -> {
            values.horizontal();
            values.padding(3);
            values.add(fieldTfDesempenho.build());
        });
    });
    private final FormBox boxPerformance = FormBox.create(box -> {
        box.vertical();
        box.add(boxTitlePerformance);
        box.add(boxIndicadores -> {
            boxIndicadores.horizontal();
            boxIndicadores.alignment(Pos.CENTER);
            boxIndicadores.add(boxPpPerformance);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxVpPerformance);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxPmPerformance);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxTpPerformance);
            boxIndicadores.add(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
            boxIndicadores.add(boxTfPerformance);
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="bil">
    private final FormBox boxIndicadoresBil = FormBox.create(boxIndicadores -> {
        boxIndicadores.horizontal();
        boxIndicadores.alignment(Pos.CENTER);
        boxIndicadores.horizontalScroll();
        boxIndicadores.height(170);
        boxIndicadores.alignment(Pos.CENTER);
    });
    private final FormBox boxTitleIndicadoresBil = FormBox.create(boxTitle -> {
        boxTitle.horizontal();
        boxTitle.addStyle("performanceDlz");
        boxTitle.alignment(Pos.CENTER);
        boxTitle.add(FormLabel.create(label -> {
            label.value.set("Balanceamento de Indicadores de Linhas");
        }).build());
    });
    private final FormBox boxBil = FormBox.create(box -> {
        box.vertical();
        box.add(boxTitleIndicadoresBil);
        box.add(boxIndicadoresBil);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cidades">
    private final FormLabel fieldQtdeCidadesVV = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldQtdeCidadesV = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldQtdeCidadesP = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldQtdeCidadesS = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldQtdeCidadesE = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldQtdeCidadesColecaoAtual = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldCidadesColecaoAtual = FormLabel.create(label -> {
        label.value.set("");
        label.sizeText(9);
        label.alignment(Pos.CENTER_LEFT);
    });
    private final FormLabel fieldQtdeCidadesColecaoAnterior = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldCidadesColecaoAnterior = FormLabel.create(label -> {
        label.value.set("");
        label.sizeText(9);
        label.alignment(Pos.CENTER_LEFT);
    });
    private final FormLabel fieldQtdeCidadesColecaoPassada = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldCidadesColecaoPassada = FormLabel.create(label -> {
        label.value.set("");
        label.sizeText(9);
        label.alignment(Pos.CENTER_LEFT);
    });
    private final FormLabel fieldQtdeCidadesColecaoLastYear = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    private final FormLabel fieldCidadesColecaoLastYear = FormLabel.create(label -> {
        label.value.set("");
        label.sizeText(9);
        label.alignment(Pos.CENTER_LEFT);
    });
    private final FormLabel fieldQtdeCidadesSemAtendimento = FormLabel.create(label -> {
        label.value.set("(0)");
        label.sizeText(10);
        label.alignment(Pos.CENTER);
        label.boldText();
        label.width(40.0);
    });
    // </editor-fold>
    // </editor-fold>

    public BidRepresentanteView() {
        super("BID Representante", ImageUtils.getImage(ImageUtils.Icon.REPRESENTANTE), new String[]{"Listagem", "BID"});
        initListagem();
        initBid();
    }

    // ----------- LISTAGEM ----------------
    public void initListagem() {
        tabListagem.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(fields -> fields.addHorizontal(fieldFilterRepresentante.build(), fieldFilterMarca.build(), fieldFilterColecao.build()));
                    filter.add(fields -> fields.addHorizontal(fieldFilterRegiao.build(), fieldFilterUf.build(), fieldFilterGlobais.build()));
                    filter.find.setOnAction(evt -> {
                        procurarBid(fieldFilterRepresentante.objectValues.get(), fieldFilterMarca.objectValues.get(), fieldFilterColecao.objectValues.get(),
                                fieldFilterRegiao.objectValues.get(), fieldFilterUf.objectValues.get(), fieldFilterGlobais.value.get());
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterRepresentante.clear();
                        fieldFilterMarca.clear();
                        fieldFilterColecao.clear();
                        fieldFilterRegiao.clear();
                        fieldFilterUf.clear();
                        fieldFilterGlobais.select(0);
                    });
                }));
            });
            content.add(tblBid.build());
        }));
    }

    private void procurarBid(ObservableList<Represen> representante, ObservableList<Marca> marca,
                             ObservableList<Colecao> colecao, ObservableList<Regiao> regiao, ObservableList<TabUf> uf, String globais) {
        Object[] representantes = representante.stream().map(Represen::getCodRep).toArray();
        Object[] marcas = marca.stream().map(Marca::getCodigo).toArray();
        Object[] colecoes = colecao.stream().map(Colecao::getCodigo).toArray();
        Object[] regioes = regiao.stream().map(Regiao::getRegiao).toArray();
        Object[] ufs = uf.stream().map(tabUf -> tabUf.getId().getSiglaEst()).toArray();

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<List<SdBid>> registros = new AtomicReference<>(new ArrayList<>());
        new RunAsyncWithOverlay(this).exec(task -> {
            registros.set(getBids(representantes, marcas, colecoes, regioes, ufs, globais));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblBid.setItems(FXCollections.observableList(registros.get()));
                tblBid.refresh();
                if (registros.get().size() > 0)
                    tblNavegation.selectItem(0);
            }
        });
    }

    // ----------- BID ----------------
    public void initBid() {
        tabBid.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(navBid);
            content.left(tblNavegation.build());
            content.center(FormBox.create(boxCenter -> {
                boxCenter.vertical();
                boxCenter.bothScroll();
                boxCenter.expanded();
                boxCenter.add(boxBid);
                boxCenter.add(boxBil);
            }));
            content.right(FormBox.create(boxCidade -> {
                boxCidade.vertical();
                boxCidade.width(360.0);
                boxCidade.title("SICA - SInalização da CApilaridade");
                boxCidade.icon(ImageUtils.getImage(ImageUtils.Icon.LOCAL));
                boxCidade.add(FormTitledPane.create(tpane -> {
                    tpane.title("Classe Cidade");
                    tpane.icon(ImageUtils.getImage(ImageUtils.Icon.LOCAL));
                    tpane.collapse(true);
                    tpane.addStyle("info");
                    tpane.add(boxClasse -> {
                        boxClasse.vertical();
                        boxClasse.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("VV - Vitrine Virtual");
                            field.width(100.0);
                            field.addStyle("xs").addStyle("classeCidadeV");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesVV.build(), FormLabel.create(label -> {
                            label.value.set("2 lojas");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                        boxClasse.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("V - Vitrine");
                            field.width(100.0);
                            field.addStyle("xs").addStyle("classeCidadeV");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesV.build(), FormLabel.create(label -> {
                            label.value.set("6 à 12 lojas");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                        boxClasse.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("P - Premium");
                            field.width(100.0);
                            field.addStyle("xs").addStyle("classeCidadeP");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesP.build(), FormLabel.create(label -> {
                            label.value.set("3 à 6 lojas");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                        boxClasse.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("S - Special");
                            field.width(100.0);
                            field.addStyle("xs").addStyle("classeCidadeS");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesS.build(), FormLabel.create(label -> {
                            label.value.set("1 à 3 lojas");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                        boxClasse.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("E - Extra");
                            field.width(100.0);
                            field.addStyle("xs").addStyle("classeCidadeE");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesE.build(), FormLabel.create(label -> {
                            label.value.set("- lojas");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                    });
                }));
                boxCidade.add(FormTitledPane.create(tpane -> {
                    tpane.title("Status Coleções");
                    tpane.icon(ImageUtils.getImage(ImageUtils.Icon.VISUALIZAR_PEDIDO));
                    tpane.collapse(false);
                    tpane.addStyle("success");
                    tpane.add(boxStatus -> {
                        boxStatus.vertical();
                        boxStatus.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("1");
                            field.width(20.0);
                            field.addStyle("xs").addStyle("success");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesColecaoAtual.build(), fieldCidadesColecaoAtual));
                        boxStatus.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("2");
                            field.width(20.0);
                            field.addStyle("xs").addStyle("primary");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesColecaoAnterior.build(), fieldCidadesColecaoAnterior));
                        boxStatus.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("3");
                            field.width(20.0);
                            field.addStyle("xs").addStyle("warning");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesColecaoPassada.build(), fieldCidadesColecaoPassada));
                        boxStatus.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("4");
                            field.width(20.0);
                            field.addStyle("xs").addStyle("amber");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesColecaoLastYear.build(), fieldCidadesColecaoLastYear));
                        boxStatus.add(container -> container.addHorizontal(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.value.set("5");
                            field.width(20.0);
                            field.addStyle("xs").addStyle("danger");
                            field.alignment(Pos.CENTER);
                        }).build(), fieldQtdeCidadesSemAtendimento.build(), FormLabel.create(label -> {
                            label.value.set(" SEM CLIENTES/CLIENTES INATIVOS");
                            label.sizeText(9);
                            label.alignment(Pos.CENTER_LEFT);
                        }).build()));
                    });
                }));
                boxCidade.add(tblBidCidade.build());
            }));
        }));

        boxBid.add(boxRepresentante);
        boxBid.add(dados -> dados.addHorizontal(boxMostCliNovos, boxProposicao));
        boxBid.add(boxPerformance);
    }

    public void loadBid(SdBid bid) {
        if (bid == null)
            return;

        // ------ cabeçalho bid
        beanRepresentante.set("[" + bid.getId().getCodrep() + "] " + bid.getRepresent());
        beanRegiao.set(bid.getRegiao());
        beanSemana.set(bid.getSemhoje() + "/" + bid.getSemtotal());
        beanColecao.set("[" + bid.getId().getCodcol() + "] " + bid.getColecao());
        try {
            beanLogoMarca.setImage(new Image(new FileInputStream("C:\\SysDelizLocal\\local_files\\" + (bid.getId().getCodmar().equals("F") ? "flordelis" : "dlz") + ".bmp")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            beanLogoMarca.setImage(new Image(ImageUtils.class.getResource("/images/sem_foto.png").toExternalForm()));
            MessageBox.create(message -> {
                message.message("Não foi possível carregar o logo da marca " + bid.getMarca() + ". Favor verificar com o suporte.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }

        // <editor-fold defaultstate="collapsed" desc="mostruário e clientes novos">
        boxTitleMostruario.removeStyle("proposicaoDlz", "proposicaoFdl");
        boxTitleMostruario.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        boxIndicadorMostruario.removeStyle("proposicaoDlz", "proposicaoFdl");
        boxIndicadorMostruario.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        fieldMostruario.value.set(bid.getMostr().toString());
        fieldClientesReativados.value.set(bid.getRatrcm().toString());
        fieldRealizadoClientesNovos.value.set(bid.getClnrcm().toString());
        fieldRealizadoClientesNovos.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getClnRelLy().doubleValue(), 2));
        fieldLastYearClientesNovos.value.set(bid.getClnly().toString());
        fieldMetaClientesNovos.value.set(bid.getClnmta().toString());
        fieldAtendMetaClientesNovos.value.set(StringUtils.toPercentualFormat(bid.getClnRelMta().doubleValue(), 2));
        fieldAtendMetaClientesNovos.removeStyle("danger", "warning", "success").addStyle(bid.getClnRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getClnRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="proposição">
        boxTitleProposicao.removeStyle("proposicaoDlz", "proposicaoFdl");
        boxTitleProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        // ------ PV
        boxPvProposicao.removeStyle("proposicaoDlz", "proposicaoFdl", "pvDlz", "pvFdl");
        boxPvProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        boxPvProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "pvDlz" : "pvFdl");
        fieldPvPotencial.value.set(StringUtils.toIntegerFormat(bid.getPvrpm().intValue()));
        fieldPvLastYear.value.set(StringUtils.toIntegerFormat(bid.getPvly().intValue()));
        fieldPvRealizado.value.set(StringUtils.toIntegerFormat(bid.getPvrcm().intValue()));
        fieldPvRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getPvRelLy().doubleValue(), 2));
        fieldPvMeta.value.set(StringUtils.toIntegerFormat(bid.getPvmta().intValue()));
        fieldPvDesempenho.value.set(StringUtils.toPercentualFormat(bid.getPvRelMta().doubleValue(), 2));
        fieldPvDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getPvRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getPvRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ CD
        boxCdProposicao.removeStyle("proposicaoDlz", "proposicaoFdl", "cdDlz", "cdFdl");
        boxCdProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        boxCdProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "cdDlz" : "cdFdl");
        fieldCdPotencial.value.set(StringUtils.toIntegerFormat(bid.getCdrpm().intValue()));
        fieldCdLastYear.value.set(StringUtils.toIntegerFormat(bid.getCdly().intValue()));
        fieldCdRealizado.value.set(StringUtils.toIntegerFormat(bid.getCdrcm().intValue()));
        fieldCdRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getCdRelLy().doubleValue(), 2));
        fieldCdMeta.value.set(StringUtils.toIntegerFormat(bid.getCdmta().intValue()));
        fieldCdDesempenho.value.set(StringUtils.toPercentualFormat(bid.getCdRelMta().doubleValue(), 2));
        fieldCdDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getCdRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getCdRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ PR
        boxPrProposicao.removeStyle("proposicaoDlz", "proposicaoFdl", "prDlz", "prFdl");
        boxPrProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        boxPrProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "prDlz" : "prFdl");
        fieldPrPotencial.value.set(bid.getPrrpm() == null ? "0.00" : bid.getPrrpm().multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP).toString());
        fieldPrLastYear.value.set(bid.getPrly() == null ? "0.00" : bid.getPrly().multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP).toString());
        fieldPrRealizado.value.set(bid.getPrrcm() == null ? "0.00" : bid.getPrrcm().multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP).toString());
        fieldPrRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getPrRelLy().doubleValue(), 2));
        fieldPrMeta.value.set(bid.getPrmta() == null ? "0.00" : bid.getPrmta().multiply(bigDecimal100).setScale(2, RoundingMode.HALF_UP).toString());
        fieldPrDesempenho.value.set(StringUtils.toPercentualFormat(bid.getPrRelMta().doubleValue(), 2));
        fieldPrDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getPrRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getPrRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ PG
        boxPgProposicao.removeStyle("proposicaoDlz", "proposicaoFdl", "pgDlz", "pgFdl");
        boxPgProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "proposicaoDlz" : "proposicaoFdl");
        boxPgProposicao.addStyle(bid.getId().getCodmar().equals("D") ? "pgDlz" : "pgFdl");
        fieldPgPotencial.value.set(bid.getPgrpm().toString());
        fieldPgLastYear.value.set(bid.getPgly().toString());
        fieldPgRealizado.value.set(bid.getPgrcm().toString());
        fieldPgRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getPgRelLy().doubleValue(), 2));
        fieldPgMeta.value.set(bid.getPgmta().toString());
        fieldPgDesempenho.value.set(StringUtils.toPercentualFormat(bid.getPgRelMta().doubleValue(), 2));
        fieldPgDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getPgRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getPgRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="performance">
        boxTitlePerformance.removeStyle("performanceDlz", "performanceFdl");
        boxTitlePerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        // ------ PP
        boxPpPerformance.removeStyle("performanceDlz", "performanceFdl", "tpDlz", "tpFdl");
        boxPpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        boxPpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "tpDlz" : "tpFdl");
        fieldPpPotencial.value.set(StringUtils.toIntegerFormat(bid.getPprpm().intValue()));
        fieldPpLastYear.value.set(StringUtils.toIntegerFormat(bid.getPply().intValue()));
        fieldPpRealizado.value.set(StringUtils.toIntegerFormat(bid.getPprcm().intValue()));
        fieldPpRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getPpRelLy().doubleValue(), 2));
        fieldPpMeta.value.set(StringUtils.toIntegerFormat(bid.getPpmta().intValue()));
        fieldPpDesempenho.value.set(StringUtils.toPercentualFormat(bid.getPpRelMta().doubleValue(), 2));
        fieldPpDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getPpRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getPpRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ VP
        boxVpPerformance.removeStyle("performanceDlz", "performanceFdl", "vpDlz", "vpFdl");
        boxVpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        boxVpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "vpDlz" : "vpFdl");
        fieldVpPotencial.value.set(StringUtils.toMonetaryFormat(bid.getVprpm(), 2));
        fieldVpLastYear.value.set(StringUtils.toMonetaryFormat(bid.getVply(), 2));
        fieldVpRealizado.value.set(StringUtils.toMonetaryFormat(bid.getVprcm(), 2));
        fieldVpRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getVpRelLy().doubleValue(), 2));
        fieldVpMeta.value.set(StringUtils.toMonetaryFormat(bid.getVpmta(), 2));
        fieldVpDesempenho.value.set(StringUtils.toPercentualFormat(bid.getVpRelMta().doubleValue(), 2));
        fieldVpDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getVpRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getVpRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ PM
        boxPmPerformance.removeStyle("performanceDlz", "performanceFdl", "pmDlz", "pmFdl");
        boxPmPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        boxPmPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "pmDlz" : "pmFdl");
        fieldPmPotencial.value.set(StringUtils.toMonetaryFormat(bid.getPmrpm(), 2));
        fieldPmLastYear.value.set(StringUtils.toMonetaryFormat(bid.getPmly(), 2));
        fieldPmRealizado.value.set(StringUtils.toMonetaryFormat(bid.getPmrcm(), 2));
        fieldPmRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getPmRelLy().doubleValue(), 2));
        fieldPmMeta.value.set(StringUtils.toMonetaryFormat(bid.getPmmta(), 2));
        fieldPmDesempenho.value.set(StringUtils.toPercentualFormat(bid.getPmRelMta().doubleValue(), 2));
        fieldPmDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getPmRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getPmRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ TP
        boxTpPerformance.removeStyle("performanceDlz", "performanceFdl", "tpDlz", "tpFdl");
        boxTpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        boxTpPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "tpDlz" : "tpFdl");
        fieldTpPotencial.value.set(StringUtils.toIntegerFormat(bid.getTprpm().intValue()));
        fieldTpLastYear.value.set(StringUtils.toIntegerFormat(bid.getTply().intValue()));
        fieldTpRealizado.value.set(StringUtils.toIntegerFormat(bid.getTprcm().intValue()));
        fieldTpRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getTpRelLy().doubleValue(), 2));
        fieldTpMeta.value.set(StringUtils.toIntegerFormat(bid.getTpmta().intValue()));
        fieldTpDesempenho.value.set(StringUtils.toPercentualFormat(bid.getTpRelMta().doubleValue(), 2));
        fieldTpDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getTpRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getTpRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // ------ TF
        boxTfPerformance.removeStyle("performanceDlz", "performanceFdl", "tfDlz", "tfFdl");
        boxTfPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        boxTfPerformance.addStyle(bid.getId().getCodmar().equals("D") ? "tfDlz" : "tfFdl");
        fieldTfPotencial.value.set(StringUtils.toMonetaryFormat(bid.getTfrpm(), 2));
        fieldTfLastYear.value.set(StringUtils.toMonetaryFormat(bid.getTfly(), 2));
        fieldTfRealizado.value.set(StringUtils.toMonetaryFormat(bid.getTfrcm(), 2));
        fieldTfRealizado.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bid.getTfRelLy().doubleValue(), 2));
        fieldTfMeta.value.set(StringUtils.toMonetaryFormat(bid.getTfmta(), 2));
        fieldTfDesempenho.value.set(StringUtils.toPercentualFormat(bid.getTfRelMta().doubleValue(), 2));
        fieldTfDesempenho.removeStyle("danger", "warning", "success").addStyle(bid.getTfRelMta().compareTo(limiteMeta) < 0 ? "danger" : bid.getTfRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="bil">
        boxIndicadoresBil.clear();
        boxTitleIndicadoresBil.removeStyle("performanceDlz", "performanceFdl");
        boxTitleIndicadoresBil.addStyle(bid.getId().getCodmar().equals("D") ? "performanceDlz" : "performanceFdl");
        bid.getBil().forEach(SdBil::refresh);
        bid.getBil().sort(Comparator.comparingInt(SdBil::getSeqLinha));
        for (SdBil bil : bid.getBil()) {
            boxIndicadoresBil.add(boxIndicador -> {
                boxIndicador.vertical();
                boxIndicador.addStyle("performance" + (bid.getId().getCodmar().equals("D") ? "Dlz" : "Fdl"));
                boxIndicador.spacing(0);
                boxIndicador.padding(3);
                boxIndicador.add(title -> {
                    title.horizontal();
                    title.padding(0);
                    title.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(200);
                        field.editable(false);
                        field.fontBold();
                        field.alignment(Pos.CENTER);
                        field.value.set("[" + bil.getId().getLinha().getCodigo() + "] " + bil.getId().getLinha().getSdLinhaDesc());
                    }).build());
                });
                boxIndicador.add(header -> {
                    header.horizontal();
                    header.spacing(0);
                    header.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(120);
                        field.editable(false);
                        field.addStyle("xs").addStyle("dark");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER);
                        field.value.set("REALIZADO");
                    }).build());
//                    header.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(70);
//                        field.editable(false);
//                        field.addStyle("xs").addStyle("dark");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set("META");
//                    }).build());
//                    header.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(60);
//                        field.editable(false);
//                        field.addStyle("xs").addStyle("dark");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set("% META");
//                    }).build());
                    header.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(80);
                        field.editable(false);
                        field.addStyle("xs").addStyle("dark");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER);
                        field.value.set("LAST YEAR");
                    }).build());
                });
                boxIndicador.add(pecas -> {
                    pecas.horizontal();
                    pecas.spacing(0);
                    pecas.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("TP");
                        field.width(120);
                        field.editable(false);
                        field.addStyle("xs");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(StringUtils.toIntegerFormat(bil.getTpRcm().intValue()));
                        field.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bil.getTpRelLy().doubleValue(), 2));
                    }).build());
//                    pecas.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(70);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER_RIGHT);
//                        field.value.set(bil.getTpMta().toString());
//                    }).build());
//                    pecas.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(60);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set(StringUtils.toPercentualFormat(bil.getTpRelMta().doubleValue(), 2));
//                        field.removeStyle("danger","warning","success").addStyle(bil.getTpRelMta().compareTo(limiteMeta) < 0 ? "danger" : bil.getTpRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
//                    }).build());
                    pecas.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(80);
                        field.editable(false);
                        field.addStyle("xs");
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(StringUtils.toIntegerFormat(bil.getTpLy().intValue()));
                    }).build());
                });
                boxIndicador.add(financeiro -> {
                    financeiro.horizontal();
                    financeiro.spacing(0);
                    financeiro.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("TF");
                        field.width(120);
                        field.editable(false);
                        field.addStyle("xs");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(StringUtils.toMonetaryFormat(bil.getTfRcm(), 2));
                        field.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bil.getTfRelLy().doubleValue(), 2));
                    }).build());
//                    financeiro.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(70);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER_RIGHT);
//                        field.value.set(StringUtils.toMonetaryFormat(bil.getTfMta(), 2));
//                    }).build());
//                    financeiro.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(60);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set(StringUtils.toPercentualFormat(bil.getTfRelMta().doubleValue(), 2));
//                        field.removeStyle("danger","warning","success").addStyle(bil.getTfRelMta().compareTo(limiteMeta) < 0 ? "danger" : bil.getTfRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
//                    }).build());
                    financeiro.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(80);
                        field.editable(false);
                        field.addStyle("xs");
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(StringUtils.toMonetaryFormat(bil.getTfLy(), 2));
                    }).build());
                });
                boxIndicador.add(pr -> {
                    pr.horizontal();
                    pr.spacing(0);
                    pr.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("PR");
                        field.width(120);
                        field.editable(false);
                        field.addStyle("xs");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(bil.getPrRcm() == null ? "0.00" : bil.getPrRcm().multiply(bigDecimal100).toString());
                        field.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bil.getPrRelLy().doubleValue(), 2));
                    }).build());
//                    pr.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(80);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER_RIGHT);
//                        field.value.set(bil.getPrMta().toString());
//                    }).build());
//                    pr.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(60);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set(StringUtils.toPercentualFormat(bil.getPrRelMta().doubleValue(), 2));
//                        field.removeStyle("danger","warning","success").addStyle(bil.getPrRelMta().compareTo(limiteMeta) < 0 ? "danger" : bil.getPrRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
//                    }).build());
                    pr.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(80);
                        field.editable(false);
                        field.addStyle("xs");
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(bil.getPrLy() == null ? "0.00" : bil.getPrLy().multiply(bigDecimal100).toString());
                    }).build());
                });
                boxIndicador.add(pg -> {
                    pg.horizontal();
                    pg.spacing(0);
                    pg.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("PG");
                        field.width(120);
                        field.editable(false);
                        field.addStyle("xs");
                        field.fontBold();
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(bil.getPgRcm().toString());
                        field.tooltip("Atingimento do Last Year:\n" + StringUtils.toPercentualFormat(bil.getPgRelLy().doubleValue(), 2));
                    }).build());
//                    pg.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(80);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER_RIGHT);
//                        field.value.set(bil.getPgMta().toString());
//                    }).build());
//                    pg.add(FormFieldText.create(field -> {
//                        field.withoutTitle();
//                        field.width(60);
//                        field.editable(false);
//                        field.addStyle("xs");
//                        field.fontBold();
//                        field.setPadding(0);
//                        field.alignment(Pos.CENTER);
//                        field.value.set(StringUtils.toPercentualFormat(bil.getPgRelMta().doubleValue(), 2));
//                        field.removeStyle("danger","warning","success").addStyle(bil.getPgRelMta().compareTo(limiteMeta) < 0 ? "danger" : bil.getPgRelMta().compareTo(sucessoMeta) >= 0 ? "success" : "warning");
//                    }).build());
                    pg.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(80);
                        field.editable(false);
                        field.addStyle("xs");
                        field.setPadding(0);
                        field.alignment(Pos.CENTER_RIGHT);
                        field.value.set(bil.getPgLy().toString());
                    }).build());
                });
            });
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="cidades">
        bid.getCidades().sort(Comparator.comparing(cid -> cid.getId().getCidade().getNomeCid(), Comparator.naturalOrder()));
        tblBidCidade.items.set(FXCollections.observableList(bid.getCidades()));

        VSdBidCidade cidade = bid.getCidades().size() > 0 ? bid.getCidades().get(0) : null;
        fieldCidadesColecaoAtual.clear();
        fieldQtdeCidadesColecaoAtual.clear();
        fieldCidadesColecaoAnterior.clear();
        fieldQtdeCidadesColecaoAnterior.clear();
        fieldCidadesColecaoPassada.clear();
        fieldQtdeCidadesColecaoPassada.clear();
        fieldCidadesColecaoLastYear.clear();
        fieldQtdeCidadesColecaoLastYear.clear();
        fieldQtdeCidadesSemAtendimento.clear();
        fieldQtdeCidadesVV.clear();
        fieldQtdeCidadesV.clear();
        fieldQtdeCidadesP.clear();
        fieldQtdeCidadesS.clear();
        fieldQtdeCidadesE.clear();

        if (cidade != null) {
            // colecao atual
            fieldCidadesColecaoAtual.value.set("[" + cidade.getId().getBid().getId().getCodcol() + "] " + cidade.getId().getBid().getColecao());
            fieldQtdeCidadesColecaoAtual.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getAtendimentoGrupos() == 1).count() + ")");
            // colecao anterior
            fieldCidadesColecaoAnterior.value.set(cidade.getColAnt().toString());
            fieldQtdeCidadesColecaoAnterior.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getAtendimentoGrupos() == 2).count() + ")");
            fieldQtdeCidadesColecaoAnterior.tooltip("Peças: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getPecasAnt).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().mapToDouble(cid -> cid.getValorAnt().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getGruposAnt).sum());
            // colecao passada
            fieldCidadesColecaoPassada.value.set(cidade.getColPass().toString());
            fieldQtdeCidadesColecaoPassada.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getAtendimentoGrupos() == 3).count() + ")");
            fieldQtdeCidadesColecaoPassada.tooltip("Peças: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getPecasPass).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().mapToDouble(cid -> cid.getValorPass().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getGruposPass).sum());
            // colecao last year
            fieldCidadesColecaoLastYear.value.set(cidade.getColLy().toString());
            fieldQtdeCidadesColecaoLastYear.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getAtendimentoGrupos() == 4).count() + ")");
            fieldQtdeCidadesColecaoLastYear.tooltip("Peças: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getPecasLy).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().mapToDouble(cid -> cid.getValorLy().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().mapToInt(VSdBidCidade::getGruposLy).sum());
            // sem cidades
            fieldQtdeCidadesSemAtendimento.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getAtendimentoGrupos() == 5).count() + ")");
            // classe VV
            fieldQtdeCidadesVV.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("VV")).count() + ")");
            fieldQtdeCidadesVV.tooltip("Peças: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("VV")).mapToInt(VSdBidCidade::getPecasAtual).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("VV")).mapToDouble(cid -> cid.getValorAtual().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("VV")).mapToInt(VSdBidCidade::getGruposAtual).sum());
            // classe V
            fieldQtdeCidadesV.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("V")).count() + ")");
            fieldQtdeCidadesV.tooltip("Peças: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("V")).mapToInt(VSdBidCidade::getPecasAtual).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("V")).mapToDouble(cid -> cid.getValorAtual().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("V")).mapToInt(VSdBidCidade::getGruposAtual).sum());
            // classe P
            fieldQtdeCidadesP.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("P")).count() + ")");
            fieldQtdeCidadesP.tooltip("Peças: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("P")).mapToInt(VSdBidCidade::getPecasAtual).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("P")).mapToDouble(cid -> cid.getValorAtual().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("P")).mapToInt(VSdBidCidade::getGruposAtual).sum());
            // classe S
            fieldQtdeCidadesS.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("S")).count() + ")");
            fieldQtdeCidadesS.tooltip("Peças: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("S")).mapToInt(VSdBidCidade::getPecasAtual).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("S")).mapToDouble(cid -> cid.getValorAtual().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("S")).mapToInt(VSdBidCidade::getGruposAtual).sum());
            // classe E
            fieldQtdeCidadesE.value.set("(" + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("E")).count() + ")");
            fieldQtdeCidadesE.tooltip("Peças: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("E")).mapToInt(VSdBidCidade::getPecasAtual).sum() + "\n" +
                    "Valor: " + StringUtils.toMonetaryFormat(bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("E")).mapToDouble(cid -> cid.getValorAtual().doubleValue()).sum(), 2) + "\n" +
                    "PV: " + bid.getCidades().stream().filter(cid -> cid.getId().getCidade().getClasse().equals("E")).mapToInt(VSdBidCidade::getGruposAtual).sum());
        }
        // </editor-fold>
    }

    private void enviarBid(List<SdBid> reps) {
        if (reps.size() == 0 && tblNavegation.selectedItem() == null) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um registro para impressão.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        List<SdBid> bids = new ArrayList<>(reps);
        if (bids.size() == 0)
            bids.add(tblNavegation.selectedItem());

        new Fragment().show(fragment -> {
            fragment.title("Enviar E-mail");
            fragment.size(700.0, 500.0);

            final HTMLEditor editorEmail = new HTMLEditor();
            fragment.box.getChildren().add(editorEmail);
            final Button btnEnviarExtrato = FormButton.create(btnEnviar -> {
                btnEnviar.title("Enviar");
                btnEnviar.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                btnEnviar.addStyle("success");
                btnEnviar.setAction(evtEnviar -> {
                    for (SdBid bid : bids) {
                        loadBid(bid);
                        WritableImage image = boxBid.snapshot(new SnapshotParameters(), null);
                        File fileImageBid = new File("C:/SysDelizLocal/local_files/bidRepresentante_" + bid.getId().getCodrep() + "_" + bid.getId().getCodcol() + "_" + bid.getId().getCodmar() + ".png");
                        try {
                            // print da tela para corpo do e-mail.
                            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", fileImageBid);

                            // export pdf para anexo do e-mail
                            String fileName = "C:/SysDelizLocal/local_files/bidRepresentante_" + bid.getId().getCodrep() + "_" + bid.getId().getCodcol() + "_" + bid.getId().getCodmar() + ".pdf";
                            new ReportUtils().config()
                                    .addReport(ReportUtils.ReportFile.BID_REPRESENTANTE, new ReportUtils.ParameterReport[]{new ReportUtils.ParameterReport("pCodrep", bid.getId().getCodrep()),
                                            new ReportUtils.ParameterReport("pColecao", bid.getId().getCodcol()),
                                            new ReportUtils.ParameterReport("pMarca", bid.getId().getCodmar())})
                                    .view().toPdf(fileName);

                            // email do bid
                            SimpleMail emailSender = SimpleMail.INSTANCE;
                            String[] mailsRep = bid.getEmailrep().replaceAll(",", ";").split(";");
                            for (String mail : mailsRep)
                                emailSender.addDestinatario(mail);
//                            emailSender.addDestinatario("ddefaveri@gmail.com");

                            String bodyEmail = editorEmail.getHtmlText();
                            bodyEmail = bodyEmail.replaceAll("\\$\\{respon}", bid.getNome());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{represen}", bid.getRepresent());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{data}", StringUtils.toDateFormat(LocalDate.now()));
                            bodyEmail = bodyEmail.replaceAll("\\$\\{colecao}", bid.getColecao());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{marca}", bid.getMarca());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{codrep}", bid.getId().getCodrep());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{gerente}", bid.getGerente());
                            bodyEmail = bodyEmail.replaceAll("\\$\\{email_gerente}", bid.getEmailger());
                            if (bodyEmail.contains("${imagem}"))
                                bodyEmail = bodyEmail.replaceAll("\\$\\{imagem}", "<div><img src=\"cid:imagbid\" /></div>");

                            String finalBodyEmail = bodyEmail;
                            emailSender.addReplyTo(Globals.getUsuarioLogado().getEmail())
                                    .setSender(Globals.getUsuarioLogado().getDisplayName())
                                    .addCC(Globals.getUsuarioLogado().getEmail())
                                    .addCCO("diego@deliz.com.br")
                                    .comAssunto("BID Representante - " + bid.getColecao() + " - " + bid.getMarca())
                                    .comMultipartBody(mimeMultipart -> {
                                        try {
                                            BodyPart messageBodyPartHtml = new MimeBodyPart();
                                            BodyPart imageBodyPart = new MimeBodyPart();

                                            // Primeira parte - o conteúdo
                                            String htmlText = finalBodyEmail;
                                            messageBodyPartHtml.setContent(htmlText, "text/html");
                                            messageBodyPartHtml.setHeader("charset", "utf-8");
                                            mimeMultipart.addBodyPart(messageBodyPartHtml);

                                            if (editorEmail.getHtmlText().contains("${imagem}")) {
                                                MimeBodyPart imagePart = new MimeBodyPart();
                                                imageBodyPart.setDataHandler(new DataHandler(new FileDataSource(fileImageBid)));
                                                imageBodyPart.setHeader("Content-ID", "<imagbid>");
                                                imageBodyPart.setDisposition(MimeBodyPart.INLINE);
                                                mimeMultipart.addBodyPart(imageBodyPart);
                                            }
                                        } catch (MessagingException | NullPointerException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                        return mimeMultipart;
                                    })
                                    .addAttachment(fileName)
                                    .send();

                            SysLogger.addSysDelizLog("BID Representante", TipoAcao.ENVIAR, bid.getId().getCodrep(), "Enviado BID " + bid.getColecao() + " - " + bid.getMarca());
                            MessageBox.create(message -> {
                                message.message("Enviado BID para " + bid.getId().getCodrep() + " / " + bid.getColecao() + " - " + bid.getMarca());
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (IOException | JRException | SQLException e) {
                            e.printStackTrace();
                            MessageBox.create(message -> {
                                message.message("Erro de excpetion para o rep: " + bid.getId().getCodrep() + " / " + bid.getColecao() + " - " + bid.getMarca());
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showAndWait();
                            });
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    }

                    tblNavegation.items.forEach(SdBid::unselect);
                    fragment.close();
                });
            });
            fragment.buttonsBox.getChildren().add(FormButton.create(sBtn -> {
                sBtn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                sBtn.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                sBtn.addStyle("info");
                sBtn.setOnAction(event -> {
                    new Fragment().information("Para textos automáticos, utilize os seguintes códigos:\n\n" +
                            "${respon} - para exibir o nome do responsável.\n" +
                            "${data} - para exibir a data atual.\n" +
                            "${colecao} - para exibir a descrição da coleção do BID.\n" +
                            "${marca} - para exibir a descrição da marca do BID.\n" +
                            "${codrep} - para exibir o código do representante.\n" +
                            "${gerente} - para exibir o nome do gerente do representante.\n" +
                            "${email_gerente} - para exibir o e-mail do gerente.\n" +
                            "${represen} - para exibir a razão social do representante.\n" +
                            "${imagem} - para exibir no corpo do e-mail o bid.\n");
                });
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(sBtn -> {
                sBtn.setText("Carregar Padrão");
                sBtn.addStyle("secundary");
                sBtn.icon(ImageUtils.getIcon(ImageUtils.Icon.BAIXAR, ImageUtils.IconSize._16));
                sBtn.setOnAction(event -> {
                    carregarTexto(editorEmail);
                });
            }));
            fragment.buttonsBox.getChildren().add(new Separator(Orientation.VERTICAL));
            fragment.buttonsBox.getChildren().add(btnEnviarExtrato);
        });

    }

    private void imprimirBid(List<SdBid> reps) {
        if (reps.size() == 0 && tblNavegation.selectedItem() == null) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um registro para impressão.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        List<SdBid> bids = new ArrayList<>(reps);
        if (bids.size() == 0)
            bids.add(tblNavegation.selectedItem());


        for (SdBid bid : bids) {
            try {
                new ReportUtils().config()
                        .addReport(ReportUtils.ReportFile.BID_REPRESENTANTE, new ReportUtils.ParameterReport[]{new ReportUtils.ParameterReport("pCodrep", bid.getId().getCodrep()),
                                new ReportUtils.ParameterReport("pColecao", bid.getId().getCodcol()),
                                new ReportUtils.ParameterReport("pMarca", bid.getId().getCodmar())})
                        .view().toView();
            } catch (JRException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }

        MessageBox.create(message -> {
            message.message("BIDs abertos para visualização!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.showAndWait();
        });
        tblNavegation.items.forEach(SdBid::unselect);
    }

    private void carregarTexto(HTMLEditor editorEmail) {
        if (textoSalvo == null || textoSalvo.equals("")) {
            SdPropriedade propriedade = new FluentDao().selectFrom(SdPropriedade.class).where(it -> it
                    .equal("idTela", SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId())
                    .equal("propriedade", "EMAILPADRAO")
            ).singleResult();
            if (propriedade != null && propriedade.getValor() != null) textoSalvo = propriedade.getValor();
        }
        editorEmail.setHtmlText(textoSalvo);
    }

    private void salvarTexto(String htmlText) {
        textoSalvo = htmlText;
        SdPropriedade propriedade = new FluentDao().selectFrom(SdPropriedade.class).where(it -> it
                .equal("usuario", Globals.getUsuarioLogado().getUsuario())
                .equal("idTela", SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId())
                .equal("propriedade", "EMAILREP")
        ).singleResult();
        if (propriedade == null) {
            propriedade = new SdPropriedade(Globals.getUsuarioLogado().getUsuario(), SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId(), "EMAILREP", htmlText);
            try {
                propriedade = new FluentDao().persist(propriedade);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            JPAUtils.getEntityManager().refresh(propriedade);
            propriedade.setValor(htmlText);
            propriedade = new FluentDao().merge(propriedade);
        }


        MessageBox.create(message -> {
            message.message("Texto salvo com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

}
