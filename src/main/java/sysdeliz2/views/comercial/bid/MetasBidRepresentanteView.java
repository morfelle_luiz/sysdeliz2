package sysdeliz2.views.comercial.bid;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.bid.MetasBidRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.comercial.SdMetasBid;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdLinhaBid;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MetasBidRepresentanteView extends MetasBidRepresentanteController {

    // <editor-fold defaultstate="collapsed" desc="local">
    private final List<VSdLinhaBid> linhasBid = (List<VSdLinhaBid>) new FluentDao().selectFrom(VSdLinhaBid.class).get().resultList();
    private List<SdMetasBid> metasSelecionadas = new ArrayList<>();
    private Boolean editarLinha = false;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tabs">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabCadastro = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="listagem">
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(200.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(100.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Represen> fieldFilterRepresentante = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(250.0);
        field.toUpper();
    });
    private final FormTableView<SdMetasBid> tblMetas = FormTableView.create(SdMetasBid.class, table -> {
        table.title("Metas Representantes");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(110.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMetasBid, SdMetasBid>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnAbrir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Abrir Meta Representante");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Alterar Meta Representante");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Meta Representante");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdMetasBid item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnAbrir.setOnAction(evt -> {
                                        table.selectItem(item);
                                        tabs.getSelectionModel().select(1);
                                        abrirMetaRepresentante(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        table.selectItem(item);
                                        editarMetaRepresentante(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirMetaRepresentante(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAbrir, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTipo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMetasBid, String>(){
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-primary","table-row-success");
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add(item.equals("MTA") ? "table-row-success" : item.equals("RPM") ? "table-row-primary" : "");
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getRepresentante().getCodRep()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(320.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getRepresentante().getNome()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(210.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca()));
                }).build() /*Marca*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Metas");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("PM");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPm()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*PM*/,
                            FormTableColumn.create(cln -> {
                                cln.title("CD");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCd()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*CD*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPv()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PV*/,
                            FormTableColumn.create(cln -> {
                                cln.title("KA");
                                cln.width(50.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getKa()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*KA*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PP");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPp()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("VP");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVp()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*VP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("TP");
                                cln.width(90.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTp()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*TP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(120.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTf()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*TF*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PG");
                                cln.width(50.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPg()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PG*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PR");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPr()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PR*/,
                            FormTableColumn.create(cln -> {
                                cln.title("CLN");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCln()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*CLN*/
                    );
                }).build() /*Metas*/
        );
        table.indices(
                table.indice("Meta", "success", ""),
                table.indice("Real Potencial", "primary", "")
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cadastro">
    private final FormNavegation<SdMetasBid> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblMetas);
        nav.withActions(true, true, true);
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirMetaRepresentante((SdMetasBid) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarMetaRepresentante((SdMetasBid) nav.selectedItem.get()));
        nav.btnSave(evt -> salvarCadastro((SdMetasBid) nav.selectedItem.get()));
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                abrirMetaRepresentante((SdMetasBid) newValue);
            }
        });
        nav.withReturn(evt -> {
            nav.inEdition.set(false);
            tabs.getSelectionModel().select(0);
        });
    });
    private final FormTableView<SdMetasBid> tblMetasLinhas = FormTableView.create(SdMetasBid.class, table -> {
        table.title("Linhas");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(80.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMetasBid, SdMetasBid>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Alterar Meta Linha");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                                btn.disable.bind(navegation.inEdition.not());
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Meta Linha");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            @Override
                            protected void updateItem(SdMetasBid item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnEditar.setOnAction(evt -> {
                                        editarMetaLinha(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirMetaRepresentante(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTipo()));
                    cln.format(param -> {
                        return new TableCell<SdMetasBid, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    linhasBid.stream().filter(meta -> meta.getId().getCodigo().equals(item)).findAny().ifPresent(linha -> setText(linha.getDescricao()));
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Metas");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("PM");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPm()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*PM*/,
                            FormTableColumn.create(cln -> {
                                cln.title("CD");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCd()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*CD*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPv()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PV*/,
                            FormTableColumn.create(cln -> {
                                cln.title("KA");
                                cln.width(50.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getKa()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*KA*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PP");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPp()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("VP");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVp()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*VP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("TP");
                                cln.width(90.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTp()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*TP*/,
                            FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(120.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTf()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdMetasBid, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*TF*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PG");
                                cln.width(50.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPg()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PG*/,
                            FormTableColumn.create(cln -> {
                                cln.title("PR");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPr()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*PR*/,
                            FormTableColumn.create(cln -> {
                                cln.title("CLN");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMetasBid, SdMetasBid>, ObservableValue<SdMetasBid>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCln()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*CLN*/
                    );
                }).build() /*Metas*/
        );
    });
    private final FormFieldComboBox<VSdLinhaBid> fieldLinhaBid = FormFieldComboBox.create(VSdLinhaBid.class, field -> {
        field.title("Linha");
        field.width(200.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldSingleFind<Represen> fieldRepresentante = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(450.0);
        field.editable.bind(navegation.inEdition);
        field.toUpper();
    });
    private final FormFieldSingleFind<Colecao> fieldColecao = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(300.0);
        field.editable.bind(navegation.inEdition);
        field.toUpper();
    });
    private final FormFieldSingleFind<Marca> fieldMarca = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(220.0);
        field.editable.bind(navegation.inEdition);
        field.toUpper();
        field.postSelected((observable, oldValue, newValue) -> {
           if (newValue != null) {
               fieldLinhaBid.items(FXCollections.observableList(linhasBid.stream().filter(linha -> linha.getId().getMarca().equals(((Marca) field.value.get()).getCodigo())).collect(Collectors.toList())));
           }
        });
    });
    //---- PM
    private final FormFieldText fieldPmMta = FormFieldText.create(field -> {
        field.title("PM");
        field.tooltip("Preço Médio");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPmRpm = FormFieldText.create(field -> {
        field.title("PM");
        field.tooltip("Preço Médio");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPmLinha = FormFieldText.create(field -> {
        field.title("PM");
        field.tooltip("Preço Médio");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- CD
    private final FormFieldText fieldCdMta = FormFieldText.create(field -> {
        field.title("CD");
        field.tooltip("Cidades Alvo");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldCdRpm = FormFieldText.create(field -> {
        field.title("CD");
        field.tooltip("Cidades Alvo");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldCdLinha = FormFieldText.create(field -> {
        field.title("CD");
        field.tooltip("Cidades Alvo");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- PV
    private final FormFieldText fieldPvMta = FormFieldText.create(field -> {
        field.title("PV");
        field.tooltip("Pontos de Venda");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPvRpm = FormFieldText.create(field -> {
        field.title("PV");
        field.tooltip("Pontos de Venda");
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPvLinha = FormFieldText.create(field -> {
        field.title("PV");
        field.tooltip("Pontos de Venda");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- KA
    private final FormFieldText fieldKaMta = FormFieldText.create(field -> {
        field.title("KA");
        field.tooltip("Key Accounts");
        field.alignment(Pos.CENTER);
        field.width(50.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldKaRpm = FormFieldText.create(field -> {
        field.title("KA");
        field.tooltip("Key Accounts");
        field.alignment(Pos.CENTER);
        field.width(50.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldKaLinha = FormFieldText.create(field -> {
        field.title("KA");
        field.tooltip("Key Accounts");
        field.alignment(Pos.CENTER);
        field.width(50.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- PP
    private final FormFieldText fieldPpMta = FormFieldText.create(field -> {
        field.title("PP");
        field.tooltip("Peças por Pedido");
        field.alignment(Pos.CENTER);
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPpRpm = FormFieldText.create(field -> {
        field.title("PP");
        field.tooltip("Peças por Pedido");
        field.alignment(Pos.CENTER);
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPpLinha = FormFieldText.create(field -> {
        field.title("PP");
        field.tooltip("Peças por Pedido");
        field.alignment(Pos.CENTER);
        field.width(90.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- VP
    private final FormFieldText fieldVpMta = FormFieldText.create(field -> {
        field.title("VP");
        field.tooltip("Valor (R$) por Pedido");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldVpRpm = FormFieldText.create(field -> {
        field.title("VP");
        field.tooltip("Valor (R$) por Pedido");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldVpLinha = FormFieldText.create(field -> {
        field.title("VP");
        field.tooltip("Valor (R$) por Pedido");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- TP
    private final FormFieldText fieldTpMta = FormFieldText.create(field -> {
        field.title("TP");
        field.tooltip("Total de Peças");
        field.alignment(Pos.CENTER);
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTpRpm = FormFieldText.create(field -> {
        field.title("TP");
        field.tooltip("Total de Peças");
        field.alignment(Pos.CENTER);
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTpLinha = FormFieldText.create(field -> {
        field.title("TP");
        field.tooltip("Total de Peças");
        field.alignment(Pos.CENTER);
        field.width(130.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- TF
    private final FormFieldText fieldTfMta = FormFieldText.create(field -> {
        field.title("TF");
        field.tooltip("Total Financeiro");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(160.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTfRpm = FormFieldText.create(field -> {
        field.title("TF");
        field.tooltip("Total Financeiro");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(160.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldTfLinha = FormFieldText.create(field -> {
        field.title("TF");
        field.tooltip("Total Financeiro");
        field.alignment(Pos.CENTER_RIGHT);
        field.label("R$");
        field.width(160.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- PG
    private final FormFieldText fieldPgMta = FormFieldText.create(field -> {
        field.title("PG");
        field.tooltip("Profundidade de Grade");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPgRpm = FormFieldText.create(field -> {
        field.title("PG");
        field.tooltip("Profundidade de Grade");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPgLinha = FormFieldText.create(field -> {
        field.title("PG");
        field.tooltip("Profundidade de Grade");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- PR
    private final FormFieldText fieldPrMta = FormFieldText.create(field -> {
        field.title("PR");
        field.tooltip("Profundidade de Referência");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPrRpm = FormFieldText.create(field -> {
        field.title("PR");
        field.tooltip("Profundidade de Referência");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldPrLinha = FormFieldText.create(field -> {
        field.title("PR");
        field.tooltip("Profundidade de Referência");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(navegation.inEdition);
    });
    //---- CLN
    private final FormFieldText fieldClnMta = FormFieldText.create(field -> {
        field.title("CLN");
        field.tooltip("Clientes Novos");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldClnRpm = FormFieldText.create(field -> {
        field.title("CLN");
        field.tooltip("Clientes Novos");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText fieldClnLinha = FormFieldText.create(field -> {
        field.title("CLN");
        field.tooltip("Clientes Novos");
        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.bind(navegation.inEdition);
    });
    // </editor-fold>

    public MetasBidRepresentanteView() {
        super("Metas BID Representante", ImageUtils.getImage(ImageUtils.Icon.META), new String[]{"Listagem", "Cadastro"});
        initListagem();
        initCadastro();
    }

    // ------------- LISTAGEM ----------------
    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(fields -> fields.addHorizontal(fieldFilterColecao.build(), fieldFilterMarca.build(), fieldFilterRepresentante.build()));
                    filter.find.setOnAction(evt -> {
                        procurarMetasRepresentante(fieldFilterColecao.objectValues.get(), fieldFilterMarca.objectValues.get(), fieldFilterRepresentante.objectValues.get());
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterColecao.clear();
                        fieldFilterMarca.clear();
                        fieldFilterRepresentante.clear();
                    });
                }));
            });
            content.add(tblMetas.build());
        }));
    }

    private void procurarMetasRepresentante(ObservableList<Colecao> colecao, ObservableList<Marca> marca, ObservableList<Represen> representante) {

        Object[] colecoes = colecao.stream().map(Colecao::getCodigo).toArray();
        Object[] marcas = marca.stream().map(Marca::getCodigo).toArray();
        Object[] representantes = representante.stream().map(Represen::getCodRep).toArray();

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<List<SdMetasBid>> metas = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            metas.set(getMetasRepresentantes(colecoes, marcas, representantes));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblMetas.setItems(FXCollections.observableList(metas.get()));
            }
        });

    }

    private void excluirMetaRepresentante(SdMetasBid item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir a meta selecionada?");
            message.showAndWait();
        }).value.get())) {
            excluirMetaBid(item);
            tblMetas.items.remove(item);
            tblMetasLinhas.items.remove(item);
            metasSelecionadas.remove(item);
            tblMetas.refresh();
            tblMetasLinhas.refresh();
            MessageBox.create(message -> {
                message.message("Meta excluída com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void editarMetaRepresentante(SdMetasBid item) {
        navegation.inEdition.set(true);
        tabs.getSelectionModel().select(1);
        abrirMetaRepresentante(item);
    }

    private void abrirMetaRepresentante(SdMetasBid item) {

        if (item.getId().getRepresentante() != null) {
            metasSelecionadas = tblMetas.items.stream().filter(meta -> meta.getId().getRepresentante().getCodRep().equals(item.getId().getRepresentante().getCodRep())
                            && meta.getId().getColecao().getCodigo().equals(item.getId().getColecao().getCodigo())
                            && meta.getId().getMarca().getCodigo().equals(item.getId().getMarca().getCodigo()))
                    .collect(Collectors.toList());
        }

        fieldRepresentante.value.set(item.getId().getRepresentante());
        fieldColecao.value.set(item.getId().getColecao());
        fieldMarca.value.set(item.getId().getMarca());
        // <editor-fold defaultstate="collapsed" desc="meta">
        SdMetasBid meta = metasSelecionadas.stream().filter(mta -> mta.getId().getTipo().equals("MTA")).findFirst().orElse(new SdMetasBid());
        fieldPmMta.setValue(StringUtils.toDecimalFormat(meta.getPm().doubleValue(), 2));
        fieldCdMta.setValue(meta.getCd().toString());
        fieldPvMta.setValue(meta.getPv().toString());
        fieldKaMta.setValue(meta.getKa().toString());
        fieldPpMta.setValue(meta.getPp().toString());
        fieldVpMta.setValue(StringUtils.toDecimalFormat(meta.getVp().doubleValue(), 2));
        fieldTpMta.setValue(meta.getTp().toString());
        fieldTfMta.setValue(StringUtils.toDecimalFormat(meta.getTf().doubleValue(), 2));
        fieldPgMta.setValue(StringUtils.toDecimalFormat(meta.getPg().doubleValue(), 2));
        fieldPrMta.setValue(StringUtils.toDecimalFormat(meta.getPr().doubleValue(), 4));
        fieldClnMta.setValue(meta.getCln().toString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="potencial">
        SdMetasBid potencial = metasSelecionadas.stream().filter(mta -> mta.getId().getTipo().equals("RPM")).findFirst().orElse(new SdMetasBid());
        fieldPmRpm.setValue(StringUtils.toDecimalFormat(potencial.getPm().doubleValue(), 2));
        fieldCdRpm.setValue(potencial.getCd().toString());
        fieldPvRpm.setValue(potencial.getPv().toString());
        fieldKaRpm.setValue(potencial.getKa().toString());
        fieldPpRpm.setValue(potencial.getPp().toString());
        fieldVpRpm.setValue(StringUtils.toDecimalFormat(potencial.getVp().doubleValue(), 2));
        fieldTpRpm.setValue(potencial.getTp().toString());
        fieldTfRpm.setValue(StringUtils.toDecimalFormat(potencial.getTf().doubleValue(), 2));
        fieldPgRpm.setValue(StringUtils.toDecimalFormat(potencial.getPg().doubleValue(), 2));
        fieldPrRpm.setValue(StringUtils.toDecimalFormat(potencial.getPr().doubleValue(), 4));
        fieldClnRpm.setValue(potencial.getCln().toString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="linhas">
        //fieldLinhaBid.items(FXCollections.observableList(linhasBid.stream().filter(linha -> linha.getId().getMarca().equals(item.getId().getMarca().getCodigo())).collect(Collectors.toList())));
        fieldPmLinha.setValue("0,00");
        fieldCdLinha.setValue("0");
        fieldPvLinha.setValue("0");
        fieldKaLinha.setValue("0");
        fieldPpLinha.setValue("0");
        fieldVpLinha.setValue("0,00");
        fieldTpLinha.setValue("0");
        fieldTfLinha.setValue("0,00");
        fieldPgLinha.setValue("0,00");
        fieldPrLinha.setValue("0,0000");
        fieldClnLinha.setValue("0");
        tblMetasLinhas.items.set(FXCollections.observableList(metasSelecionadas.stream().filter(mta -> Boolean.logicalAnd(!mta.getId().getTipo().equals("MTA"), !mta.getId().getTipo().equals("RPM"))).collect(Collectors.toList())));
        // </editor-fold>
    }

    // ------------- LISTAGEM ----------------
    private void initCadastro() {
        tabCadastro.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(navegation);
            content.center(FormBox.create(container -> {
                container.vertical();
                container.expanded();
                container.add(fields -> fields.addHorizontal(fieldRepresentante.build()));
                container.add(fields -> fields.addHorizontal(fieldColecao.build(), fieldMarca.build()));
                container.add(new Separator(Orientation.HORIZONTAL));
                container.add(boxMeta -> {
                    boxMeta.horizontal();
                    boxMeta.title("Meta");
                    boxMeta.add(fieldPmMta.build(), fieldCdMta.build(), fieldPvMta.build(),
                            fieldKaMta.build(), fieldPpMta.build(), fieldVpMta.build(),
                            fieldTpMta.build(), fieldTfMta.build(), fieldPgMta.build(),
                            fieldPrMta.build(), fieldClnMta.build());
                });
                container.add(boxRpm -> {
                    boxRpm.horizontal();
                    boxRpm.title("Real Potencial");
                    boxRpm.add(fieldPmRpm.build(), fieldCdRpm.build(), fieldPvRpm.build(),
                            fieldKaRpm.build(), fieldPpRpm.build(), fieldVpRpm.build(),
                            fieldTpRpm.build(), fieldTfRpm.build(), fieldPgRpm.build(),
                            fieldPrRpm.build(), fieldClnRpm.build());
                });
                container.add(boxLinha -> {
                    boxLinha.vertical();
                    boxLinha.expanded();
                    boxLinha.title("Metas Linhas");
                    boxLinha.add(fields -> fields.addHorizontal(fieldLinhaBid.build(),
                            fieldPmLinha.build(), fieldCdLinha.build(), fieldPvLinha.build(),
                            fieldKaLinha.build(), fieldPpLinha.build(), fieldVpLinha.build(),
                            fieldTpLinha.build(), fieldTfLinha.build(), fieldPgLinha.build(),
                            fieldPrLinha.build(), fieldClnLinha.build(),
                            FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("lg").addStyle("success");
                                btn.disable.bind(navegation.inEdition.not());
                                btn.setAction(evt -> {
                                    try {
                                        if (fieldRepresentante.value.get() == null)
                                            throw new FormValidationException("É necessário que você defina o representante para a meta!");
                                        if (fieldColecao.value.get() == null)
                                            throw new FormValidationException("É necessário que você defina a coleção para a meta!");
                                        if (fieldMarca.value.get() == null)
                                            throw new FormValidationException("É necessário que você defina a marca para a meta!");
                                        if (fieldLinhaBid.value.get() == null)
                                            throw new FormValidationException("É necessário que você selecione uma linha para cadastro de meta!");
                                        if (tblMetasLinhas.items.stream().filter(linha -> linha.getId().getTipo().equals(fieldLinhaBid.value.get().getId().getCodigo())).count() > 0 && !editarLinha)
                                            throw new FormValidationException("A linha selecionada já tem uma meta cadastrada!");
                                        if (fieldPmLinha.value.get() == null || fieldCdLinha.value.get() == null || fieldPvLinha.value.get() == null ||
                                                fieldKaLinha.value.get() == null || fieldPpLinha.value.get() == null || fieldVpLinha.value.get() == null ||
                                                fieldTpLinha.value.get() == null || fieldTfLinha.value.get() == null || fieldPgLinha.value.get() == null ||
                                                fieldPrLinha.value.get() == null || fieldClnLinha.value.get() == null)
                                            throw new FormValidationException("Você precisa definir um valor para os indicadores na meta da linha!\nCaso o indicador não tenha meta, preencher com o valor zero (0).");


                                        SdMetasBid meta = new SdMetasBid();
                                        if (editarLinha) {
                                            meta = tblMetasLinhas.selectedItem();
                                        } else {
                                            meta.setId(new SdMetasBid.Pk(fieldRepresentante.value.get(), fieldColecao.value.get(), fieldMarca.value.get(), fieldLinhaBid.value.getValue().getId().getCodigo()));
                                        }
                                        meta.setPm(fieldPmLinha.decimalValue());
                                        meta.setCd(fieldCdLinha.decimalValue());
                                        meta.setPv(fieldPvLinha.decimalValue());
                                        meta.setKa(fieldKaLinha.decimalValue());
                                        meta.setPp(fieldPpLinha.decimalValue());
                                        meta.setVp(fieldVpLinha.decimalValue());
                                        meta.setTp(fieldTpLinha.decimalValue());
                                        meta.setTf(fieldTfLinha.decimalValue());
                                        meta.setPg(fieldPgLinha.decimalValue());
                                        meta.setPr(fieldPrLinha.decimalValue());
                                        meta.setCln(fieldClnLinha.decimalValue());

                                        if (!editarLinha)
                                            tblMetasLinhas.items.add(meta);
                                        tblMetasLinhas.refresh();
                                        fieldPmLinha.setValue("0,00");
                                        fieldCdLinha.setValue("0");
                                        fieldPvLinha.setValue("0");
                                        fieldKaLinha.setValue("0");
                                        fieldPpLinha.setValue("0");
                                        fieldVpLinha.setValue("0,00");
                                        fieldTpLinha.setValue("0");
                                        fieldTfLinha.setValue("0,00");
                                        fieldPgLinha.setValue("0,00");
                                        fieldPrLinha.setValue("0,0000");
                                        fieldClnLinha.setValue("0");
                                    } catch (FormValidationException valid) {
                                        MessageBox.create(message -> {
                                            message.message(valid.getMessage());
                                            message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                                            message.showAndWait();
                                        });
                                        return;
                                    }
                                });
                            })));
                    boxLinha.add(tblMetasLinhas.build());
                });
            }));
        }));
    }

    private void salvarCadastro(SdMetasBid item) {
        try {
            if (fieldRepresentante.value.get() == null)
                throw new FormValidationException("É necessário que você defina o representante para a meta!");
            if (fieldColecao.value.get() == null)
                throw new FormValidationException("É necessário que você defina a coleção para a meta!");
            if (fieldMarca.value.get() == null)
                throw new FormValidationException("É necessário que você defina a marca para a meta!");
            if (fieldPmMta.value.get() == null || fieldCdMta.value.get() == null || fieldPvMta.value.get() == null ||
                    fieldKaMta.value.get() == null || fieldPpMta.value.get() == null || fieldVpMta.value.get() == null ||
                    fieldTpMta.value.get() == null || fieldTfMta.value.get() == null || fieldPgMta.value.get() == null ||
                    fieldPrMta.value.get() == null || fieldClnMta.value.get() == null)
                throw new FormValidationException("Você precisa definir um valor para os indicadores de meta!\nCaso o indicador não tenha valor, preencher com o valor zero (0).");
            if (fieldPmRpm.value.get() == null || fieldCdRpm.value.get() == null || fieldPvRpm.value.get() == null ||
                    fieldKaRpm.value.get() == null || fieldPpRpm.value.get() == null || fieldVpRpm.value.get() == null ||
                    fieldTpRpm.value.get() == null || fieldTfRpm.value.get() == null || fieldPgRpm.value.get() == null ||
                    fieldPrRpm.value.get() == null || fieldClnRpm.value.get() == null)
                throw new FormValidationException("Você precisa definir um valor para os indicadores de real potencial!\nCaso o indicador não tenha valor, preencher com o valor zero (0).");

            // carga dos objectos
            AtomicReference<SdMetasBid> meta = new AtomicReference<>(new SdMetasBid());
            meta.get().setId(new SdMetasBid.Pk(fieldRepresentante.value.get(), fieldColecao.value.get(), fieldMarca.value.get(), "MTA"));
            AtomicReference<SdMetasBid> potencial = new AtomicReference<>(new SdMetasBid());
            potencial.get().setId(new SdMetasBid.Pk(fieldRepresentante.value.get(), fieldColecao.value.get(), fieldMarca.value.get(), "RPM"));
            if (metasSelecionadas.size() > 0) {
                metasSelecionadas.stream().filter(mta -> mta.getId().getTipo().equals("MTA")).findAny().ifPresent(mta -> meta.set(mta));
                metasSelecionadas.stream().filter(mta -> mta.getId().getTipo().equals("RPM")).findAny().ifPresent(mta -> potencial.set(mta));
            }

            // meta
            meta.get().setPm(fieldPmMta.decimalValue());
            meta.get().setCd(fieldCdMta.decimalValue());
            meta.get().setPv(fieldPvMta.decimalValue());
            meta.get().setKa(fieldKaMta.decimalValue());
            meta.get().setPp(fieldPpMta.decimalValue());
            meta.get().setVp(fieldVpMta.decimalValue());
            meta.get().setTp(fieldTpMta.decimalValue());
            meta.get().setTf(fieldTfMta.decimalValue());
            meta.get().setPg(fieldPgMta.decimalValue());
            meta.get().setPr(fieldPrMta.decimalValue());
            meta.get().setCln(fieldClnMta.decimalValue());

            // rpm
            potencial.get().setPm(fieldPmRpm.decimalValue());
            potencial.get().setCd(fieldCdRpm.decimalValue());
            potencial.get().setPv(fieldPvRpm.decimalValue());
            potencial.get().setKa(fieldKaRpm.decimalValue());
            potencial.get().setPp(fieldPpRpm.decimalValue());
            potencial.get().setVp(fieldVpRpm.decimalValue());
            potencial.get().setTp(fieldTpRpm.decimalValue());
            potencial.get().setTf(fieldTfRpm.decimalValue());
            potencial.get().setPg(fieldPgRpm.decimalValue());
            potencial.get().setPr(fieldPrRpm.decimalValue());
            potencial.get().setCln(fieldClnRpm.decimalValue());

            // persistencia
            new FluentDao().persist(meta.get());
            SysLogger.addSysDelizLog("Metas BID", TipoAcao.EDITAR, meta.get().getId().getRepresentante().getCodRep(),
                    meta.get().toLog());
            new FluentDao().persist(potencial.get());
            SysLogger.addSysDelizLog("Metas BID", TipoAcao.EDITAR, potencial.get().getId().getRepresentante().getCodRep(),
                    potencial.get().toLog());
            for (SdMetasBid linha : tblMetasLinhas.items) {
                new FluentDao().persist(linha);
                SysLogger.addSysDelizLog("Metas BID", TipoAcao.EDITAR, linha.getId().getRepresentante().getCodRep(),
                        linha.toLog());
            }

            if (metasSelecionadas.size() == 0) {
                tblMetas.items.add(meta.get());
                tblMetas.items.add(potencial.get());
            }
            tblMetas.refresh();
            navegation.inEdition.set(false);
            MessageBox.create(message -> {
                message.message("Meta salva com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
        catch (FormValidationException valid) {
            MessageBox.create(message -> {
                message.message(valid.getMessage());
                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                message.showAndWait();
            });
            return;
        }
        catch (javax.persistence.PersistenceException exist) {
            JPAUtils.getEntityManager().getTransaction().rollback();
            exist.printStackTrace();
            if (exist.getCause().getCause().getMessage().contains("restrição exclusiva")) {
                MessageBox.create(message -> {
                    message.message("Já existe uma meta definida para este representante nesta coleção e marca. Você pode utilizar o filtro na aba listagem para ver a meta cadastrada!");
                    message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                    message.showAndWait();
                });
            } else {
                ExceptionBox.build(message -> {
                    message.exception(exist);
                    message.showAndWait();
                });
            }
            return;
        }
        catch (SQLException throwables) {
            JPAUtils.getEntityManager().getTransaction().rollback();
            throwables.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(throwables);
                message.showAndWait();
            });
        }
    }

    private void novoCadastro() {
        cancelarCadastro();
        navegation.inEdition.set(true);
        SdMetasBid meta = new SdMetasBid();
        meta.setId(new SdMetasBid.Pk());
        abrirMetaRepresentante(meta);
    }

    private void cancelarCadastro() {
        metasSelecionadas.clear();
        fieldRepresentante.clear();
        fieldColecao.clear();
        fieldMarca.clear();
        fieldPmLinha.setValue("0,00");
        fieldCdLinha.setValue("0");
        fieldPvLinha.setValue("0");
        fieldKaLinha.setValue("0");
        fieldPpLinha.setValue("0");
        fieldVpLinha.setValue("0,00");
        fieldTpLinha.setValue("0");
        fieldTfLinha.setValue("0,00");
        fieldPgLinha.setValue("0,00");
        fieldPrLinha.setValue("0,0000");
        fieldClnLinha.setValue("0");
        fieldPmRpm.setValue("0,00");
        fieldCdRpm.setValue("0");
        fieldPvRpm.setValue("0");
        fieldKaRpm.setValue("0");
        fieldPpRpm.setValue("0");
        fieldVpRpm.setValue("0,00");
        fieldTpRpm.setValue("0");
        fieldTfRpm.setValue("0,00");
        fieldPgRpm.setValue("0,00");
        fieldPrRpm.setValue("0,0000");
        fieldClnRpm.setValue("0");
        fieldPmMta.setValue("0,00");
        fieldCdMta.setValue("0");
        fieldPvMta.setValue("0");
        fieldKaMta.setValue("0");
        fieldPpMta.setValue("0");
        fieldVpMta.setValue("0,00");
        fieldTpMta.setValue("0");
        fieldTfMta.setValue("0,00");
        fieldPgMta.setValue("0,00");
        fieldPrMta.setValue("0,0000");
        fieldClnMta.setValue("0");
        navegation.inEdition.set(false);
    }

    private void editarMetaLinha(SdMetasBid item) {
        tblMetasLinhas.selectItem(item);
        editarLinha = true;
        fieldLinhaBid.select(fieldLinhaBid.items.stream().filter(linha -> linha.getId().getCodigo().equals(item.getId().getTipo())).findFirst().get());
        fieldPmLinha.setValue(StringUtils.toDecimalFormat(item.getPm().doubleValue(), 2));
        fieldCdLinha.setValue(item.getCd().toString());
        fieldPvLinha.setValue(item.getPv().toString());
        fieldKaLinha.setValue(item.getKa().toString());
        fieldPpLinha.setValue(item.getPp().toString());
        fieldVpLinha.setValue(StringUtils.toDecimalFormat(item.getVp().doubleValue(), 2));
        fieldTpLinha.setValue(item.getTp().toString());
        fieldTfLinha.setValue(StringUtils.toDecimalFormat(item.getTf().doubleValue(), 2));
        fieldPgLinha.setValue(StringUtils.toDecimalFormat(item.getPg().doubleValue(), 2));
        fieldPrLinha.setValue(StringUtils.toDecimalFormat(item.getPr().doubleValue(), 4));
        fieldClnLinha.setValue(item.getCln().toString());
    }
}
