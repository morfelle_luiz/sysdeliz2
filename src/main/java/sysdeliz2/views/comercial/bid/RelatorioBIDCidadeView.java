package sysdeliz2.views.comercial.bid;

import com.google.common.collect.Sets;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.comercial.bid.RelatorioBIDCidadeController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.sysdeliz.SdBidRep;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.models.view.VSdBidCidadesRep;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ExportUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class RelatorioBIDCidadeView extends RelatorioBIDCidadeController {

    // <editor-fold defaultstate="collapsed" desc="Variaveis Globais">
    private String[] colecoes;
    private Map<String, Long> mapPerfisCidades;
    private List<String> header = Arrays.asList("CodRep", "Nome Representante", "CodCid", "Nome Cidade", "UF", "População", "IPC", "Classe", "Status", "Marca");
    List<FormControlButton> perfisList = new ArrayList<>();
    List<FormControlButton> colecaoList = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bean">
    ListProperty<VSdBidCidadesRep> cidadesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    ListProperty<SdBidRep> listRepresentantesComTotais = new SimpleListProperty<>(FXCollections.observableArrayList());
    List<String> headerRep = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final Button exportExcelBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setText("Exportar Excel");
        btn.addStyle("success");
        btn.width(250.0);
        btn.setOnAction(evt -> {
            if (!cidadesBean.isEmpty()) {
                String filePath = GUIUtils.showSaveFileDialog(Collections.singletonList(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx")));
                if (filePath != null && !filePath.equals("")) {
                    new RunAsyncWithOverlay(this).exec(task -> {
                        exportarExcel(filePath);
                        return ReturnAsync.OK.value;
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            MessageBox.create(message -> {
                                message.message("Excel exportado com sucesso!");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.show();
                                message.position(Pos.TOP_RIGHT);
                            });
                        }
                    });
                }
            }
        });
    });

    private final Button printPDFbtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PDF, ImageUtils.IconSize._24));
        btn.setText("Imprimir");
        btn.addStyle("danger");
        btn.width(250.0);
        btn.setOnAction(evt -> {
            imprimirPdf();
        });
    });

    private final Button clearBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
        btn.setText("Limpar Filtros");
        btn.addStyle("primary");
        btn.width(250.0);
        btn.setOnAction(evt -> {
            colecaoList.forEach(it -> it.isSelected.set(false));
            perfisList.forEach(it -> it.isSelected.set(false));
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Cidade> cidadeCodFilter = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cod Cidade");
        field.width(200);
        field.codeReference.set("nomeCid");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Represen> codRepFilter = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("CodRep");
        field.width(150);
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<TabUf> estadoFilter = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("Estado");
        field.width(150);
        field.toUpper();
        field.codeReference.set("sigla");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(150);
        field.toUpper();
    });
    private final FormFieldToggle incluirSemRepresentanteFilter = FormFieldToggle.create(field -> {
        field.title("Incluir Sem Representante");
        field.value.setValue(false);
    });
    private final FormFieldToggle apenasSemRepresentanteFilter = FormFieldToggle.create(field -> {
        field.title("Apenas Sem Representante");
        field.value.setValue(false);
    });
    private final FormFieldText cidadeNomeFilter = FormFieldText.create(field -> {
        field.title("Nome Cidade");
        field.value.setValue("");
        field.width(250);
        field.toUpper();
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filtrarCidades(newValue, oldValue);
            }
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdBidCidadesRep> tblCidades = FormTableView.create(VSdBidCidadesRep.class, table -> {
        table.withoutHeader();
        table.items.bind(cidadesBean);
        table.multipleSelection();
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("CodRep");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(!param.getValue().naoTemRep() ? param.getValue().getId().getCodRep().getCodRep() : ""));

                }).build(), /*CodRep*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Representante");
                    cln.width(290);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(!param.getValue().naoTemRep() ? param.getValue().getId().getCodRep().getNome() : "SEM REPRESENTANTE"));
                }).build(), /*Nome Representante*/
                FormTableColumn.create(cln -> {
                    cln.title("CodCid");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getCodCid()));
                }).build(), /*Nome Representante*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Cidade");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getNomeCid()));
                }).build(), /*Nome Cidade*/
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getCodEst().toString()));
                }).build(), /*UF*/
                FormTableColumn.create(cln -> {
                    cln.title("População");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getPopCid()));
                }).build(), /*População*/
                FormTableColumn.create(cln -> {
                    cln.title("IPC");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getIpc() != null ? param.getValue().getId().getCodCid().getIpc() : ""));
                }).build(), /*IPC*/
                FormTableColumn.create(cln -> {
                    cln.title("Classe");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getClasse()));
                    cln.format(param -> {
                        return new TableCell<VSdBidCidadesRep, String>() {

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("S")) getStyleClass().add("classe-cid-s");
                                    if (item.equals("E")) getStyleClass().add("classe-cid-e");
                                    if (item.equals("P")) getStyleClass().add("classe-cid-p");
                                    if (item.equals("V") || item.equals("VV")) getStyleClass().add("classe-cid-v");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("classe-cid-s", "classe-cid-e", "classe-cid-p", "classe-cid-v");
                            }
                        };
                    });
                }).build(), /*Classe*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.format(param -> {
                        return new TableCell<VSdBidCidadesRep, Integer>() {

                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText("");
                                clear();
                                if (item != null && !empty) {
                                    if (item.equals(1))
                                        setStyle("-fx-background-color: #18ff00; -fx-text-fill: #18ff00;");
                                    if (item.equals(2))
                                        setStyle("-fx-background-color: #43dfff; -fx-text-fill: #43dfff;");
                                    if (item.equals(3))
                                        setStyle("-fx-background-color: #f1ef20; -fx-text-fill: #f1ef20;");
                                    if (item.equals(4))
                                        setStyle("-fx-background-color: #fa9e41; -fx-text-fill: #fa9e41;");
                                    if (item.equals(5))
                                        setStyle("-fx-background-color: #ff0000; -fx-text-fill: #ff0000;");
                                }
                            }

                            private void clear() {
                                setStyle("");
                            }
                        };
                    });

                }).build(), /*Status*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdBidCidadesRep, VSdBidCidadesRep>, ObservableValue<VSdBidCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodMarca().getCodigo().equals("S") ? "" : param.getValue().getId().getCodMarca().getDescricao()));
                }).build() /*Marca*/

        );
        table.factoryRow(param -> {
            return new TableRow<VSdBidCidadesRep>() {
                @Override
                protected void updateItem(VSdBidCidadesRep item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.naoTemRep()) {
                            getStyleClass().add("table-row-danger");
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
    });

    private final FormTableView<SdBidRep> tblPorRepresentante = FormTableView.create(SdBidRep.class, table -> {
        table.withoutHeader();
        try {
            colecoes = DAOFactory.getColecaoDAO().getTresUltimasColecoes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        table.items.bind(listRepresentantesComTotais);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("CodRep");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRepresentante().getCodRep()));

                }).build(), /*CodRep*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Representante");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRepresentante().getNome()));
                }).build(), /*Nome Representante*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("QTD Cidades");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getQtdCidades())));

                }).build(), /*QTD Cidades*/
                FormTableColumn.create(cln -> {
                    cln.title(getSiglaColecao(0));
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getCidadesColecaoAtual())));
                    cln.getStyleClass().add("colecao-w21-header");
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title(getSiglaColecao(1));
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getCidadesColecaoAnterior())));
                    cln.getStyleClass().add("colecao-s21-header");
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title(getSiglaColecao(2));
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getCidadesColecaoPassada())));
                    cln.getStyleClass().add("colecao-ss21-header");
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title(getSiglaColecao(3));
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getCidadesColecaoRef())));
                    cln.getStyleClass().add("colecao-w20-header");
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("S/Clientes");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBidRep, SdBidRep>, ObservableValue<SdBidRep>>) param -> new ReadOnlyObjectWrapper(String.valueOf(param.getValue().getCidadesSemClientes())));
                    cln.getStyleClass().add("colecao-s-clientes-header");
                }).build()
        );
    });

    private String getSiglaColecao(int i) {
        if (colecoes == null) return "";
        String[] array = colecoes[i].split(" ");
        StringBuilder sb = new StringBuilder();

        sb.append(array[0].charAt(0));
        if (array.length > 2) sb.append(array[1].charAt(0));
        sb.append(array[array.length - 1].substring(2));
        return sb.toString();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">

    private final int widthFields = 270;
    private final int widthNumberFields = 160;

    private final FormFieldText totalVVField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalVField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalPField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalEField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalSField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });

    private final FormFieldText totalCidades = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesAtendidas = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesColAtual = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesColAnterior = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesColPassada = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesColReferencia = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesSemClientes = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesDeixadasAtender = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });
    private final FormFieldText totalCidadesSemRepField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable.setValue(false);
        field.width(widthNumberFields);
    });

    private final FormControlButton vitrineVirtualChooser = FormControlButton.create(btn -> {
        btn.title("VV - VIT. VIRTUAL");
        btn.width(widthFields);
        btn.codeFilter.set("VV");
        btn.bgColorStandard.set("#00BFFF");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton vitrineChooser = FormControlButton.create(btn -> {
        btn.title("V - VITRINE");
        btn.width(widthFields);
        btn.codeFilter.set("V");
        btn.bgColorStandard.set("#00BFFF");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton premiumChooser = FormControlButton.create(btn -> {
        btn.title("P - PREMIUM");
        btn.width(widthFields);
        btn.codeFilter.set("P");
        btn.bgColorStandard.set("#ff9a03");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton extraChooser = FormControlButton.create(btn -> {
        btn.title("E - EXTRA");
        btn.width(widthFields);
        btn.codeFilter.set("E");
        btn.bgColorStandard.set("#979597");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton specialChooser = FormControlButton.create(btn -> {
        btn.title("S - SPECIAL");
        btn.width(widthFields);
        btn.codeFilter.set("S");
        btn.bgColorStandard.set("#893f9a");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });

    private final FormFieldText totalCidadesChooser = FormFieldText.create(field -> {
        field.textField.setText("TOTAL CIDADES");
        field.editable(false);
        field.withoutTitle();
        field.textField.setAlignment(Pos.CENTER);
        field.textField.setStyle("-fx-background-color: #94ecff; -fx-text-fill: #000000;");
        field.width(widthFields);
    });

    private final FormControlButton cidadesAtendidasChooser = FormControlButton.create(btn -> {
        btn.title("CIDADES ATENDIDAS");
        btn.width(widthFields);
        btn.codeFilter.set("0");
        btn.bgColorStandard.set("#09800b");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton colecaoAtualChooser = FormControlButton.create(btn -> {
        btn.title(colecoes[0]);
        btn.width(widthFields);
        btn.codeFilter.set("1");
        btn.bgColorStandard.set("#18ff00");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton colecaoAnteriorChooser = FormControlButton.create(btn -> {
        btn.title(colecoes[1]);
        btn.width(widthFields);
        btn.codeFilter.set("2");
        btn.bgColorStandard.set("#43dfff");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton colecaoPassadaChooser = FormControlButton.create(btn -> {
        btn.title(colecoes[2]);
        btn.width(widthFields);
        btn.codeFilter.set("3");
        btn.bgColorStandard.set("#fff200");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton colecaoReferenciaChooser = FormControlButton.create(btn -> {
        btn.title(colecoes[3]);
        btn.width(widthFields);
        btn.codeFilter.set("4");
        btn.bgColorStandard.set("#ff9a03");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });

    private final FormControlButton semClientesChooser = FormControlButton.create(btn -> {
        btn.title("CIDADES SEM CLIENTES");
        btn.width(widthFields);
        btn.codeFilter.set("5");
        btn.bgColorStandard.set("#ff0000");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton deixadasAtenderChooser = FormControlButton.create(btn -> {
        btn.title("DEIXADAS DE ATENDER");
        btn.width(widthFields);
        btn.codeFilter.set("7");
        btn.bgColorStandard.set("#ae21ff");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });
    private final FormControlButton semRepresentanteChooser = FormControlButton.create(btn -> {
        btn.title("CIDADES SEM REPRESENTANTE");
        btn.codeFilter.set("6");
        btn.width(widthFields);
        btn.bgColorStandard.set("#ff626f");
        btn.bgColorSelected.set("#6db3c3");
        btn.isSelected.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) filtrarCidadesPorTipo();
        });
    });

    // </editor-fold>

    public RelatorioBIDCidadeView() {
        super("Relatório BID Cidades", ImageUtils.getImage(ImageUtils.Icon.CIDADE));
        init();
    }

    private void init() {
        addListenersToggles();
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.expanded();
                col1.maxWidthSize(1000);
                col1.add(FormBox.create(boxFiltro -> {
                    boxFiltro.horizontal();
                    boxFiltro.hSpace(100);
                    boxFiltro.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(FormBox.create(boxFilters -> {
                                boxFilters.vertical();
                                boxFilters.add(FormBox.create(linha1 -> {
                                    linha1.horizontal();
                                    linha1.add(marcaFilter.build(), codRepFilter.build(), estadoFilter.build());
                                }));
                                boxFilters.add(FormBox.create(linha2 -> {
                                    linha2.horizontal();
                                    linha2.add(cidadeNomeFilter.build(), cidadeCodFilter.build());
                                }));
                            }));
                            boxFields.add(FormBox.create(boxToggle -> {
                                boxToggle.vertical();
                                boxToggle.add(apenasSemRepresentanteFilter.build(), incluirSemRepresentanteFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarCidades(marcaFilter.objectValues.isEmpty() ? null : marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        codRepFilter.objectValues.isEmpty() ? null : codRepFilter.objectValues.stream().map(Represen::getCodRep).toArray(),
                                        cidadeCodFilter.objectValues.isEmpty() ? null : cidadeCodFilter.objectValues.stream().map(Cidade::getCodCid).toArray(),
                                        cidadeNomeFilter.value.getValue() == null ? "" : cidadeNomeFilter.value.getValue(),
                                        estadoFilter.objectValues.isEmpty() ? null : estadoFilter.objectValues.stream().map(it -> it.getId().getSiglaEst()).toArray(),
                                        incluirSemRepresentanteFilter.value.getValue(),
                                        apenasSemRepresentanteFilter.value.getValue());
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    colecaoList.forEach(it -> it.isSelected.set(false));
                                    perfisList.forEach(it -> it.isSelected.set(false));
                                    prepararListas();
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            marcaFilter.clear();
                            codRepFilter.clear();
                            cidadeNomeFilter.value.setValue("");
                            estadoFilter.clear();
                            cidadeCodFilter.clear();
                            incluirSemRepresentanteFilter.value.setValue(false);
                        });
                    }));
                    boxFiltro.add(FormBox.create(boxTotalSemRep -> {
                        boxTotalSemRep.vertical();
                        boxTotalSemRep.alignment(Pos.BOTTOM_CENTER);
                        boxTotalSemRep.add(exportExcelBtn, printPDFbtn, clearBtn);
                    }));
                }));
                col1.add(FormBox.create(center -> {
                    center.vertical();
                    center.expanded();
                    center.add(tblCidades.build());
                }));
                col1.add(FormBox.create(footer -> {
                    footer.horizontal();
                    footer.add();
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.add(FormBox.create(box1 -> {
                    box1.horizontal();
                    box1.add(FormBox.create(boxPerfis -> {
                        boxPerfis.vertical();
                        boxPerfis.alignment(Pos.BOTTOM_LEFT);
                        boxPerfis.add(FormBox.create(colunaBox1 -> {
                            colunaBox1.vertical();
                            colunaBox1.title("PERFIL");
                            colunaBox1.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(vitrineVirtualChooser.build());
                                linha1.add(totalVVField.build());
                            }));
                            colunaBox1.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(vitrineChooser.build());
                                linha2.add(totalVField.build());
                            }));
                            colunaBox1.add(FormBox.create(linha3 -> {
                                linha3.horizontal();
                                linha3.add(premiumChooser.build());
                                linha3.add(totalPField.build());
                            }));
                            colunaBox1.add(FormBox.create(linha4 -> {
                                linha4.horizontal();
                                linha4.add(extraChooser.build());
                                linha4.add(totalEField.build());
                            }));
                            colunaBox1.add(FormBox.create(linha5 -> {
                                linha5.horizontal();
                                linha5.add(specialChooser.build());
                                linha5.add(totalSField.build());
                            }));
                        }));
                    }));
                    box1.add(FormBox.create(boxResumo -> {
                        boxResumo.vertical();
                        boxResumo.add(FormBox.create(colunaBox1 -> {
                            colunaBox1.vertical();
                            colunaBox1.title("LEGENDA");
                            colunaBox1.add(FormBox.create(linha0 -> {
                                linha0.horizontal();
                                linha0.add(totalCidadesChooser.build());
                                linha0.add(totalCidades.build());
                            }));
                            colunaBox1.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(cidadesAtendidasChooser.build());
                                linha1.add(totalCidadesAtendidas.build());
                            }));
                            colunaBox1.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(colecaoAtualChooser.build());
                                linha2.add(totalCidadesColAtual.build());
                            }));
                            colunaBox1.add(FormBox.create(linha3 -> {
                                linha3.horizontal();
                                linha3.add(colecaoAnteriorChooser.build());
                                linha3.add(totalCidadesColAnterior.build());
                            }));
                            colunaBox1.add(FormBox.create(linha4 -> {
                                linha4.horizontal();
                                linha4.add(colecaoPassadaChooser.build());
                                linha4.add(totalCidadesColPassada.build());
                            }));
                            colunaBox1.add(FormBox.create(linha5 -> {
                                linha5.horizontal();
                                linha5.add(colecaoReferenciaChooser.build());
                                linha5.add(totalCidadesColReferencia.build());
                            }));
                            colunaBox1.add(FormBox.create(linha6 -> {
                                linha6.horizontal();
                                linha6.add(semClientesChooser.build());
                                linha6.add(totalCidadesSemClientes.build());
                            }));
                            colunaBox1.add(FormBox.create(linha7 -> {
                                linha7.horizontal();
                                linha7.add(deixadasAtenderChooser.build());
                                linha7.add(totalCidadesDeixadasAtender.build());
                            }));
                            colunaBox1.add(FormBox.create(linha8 -> {
                                linha8.horizontal();
                                linha8.add(semRepresentanteChooser.build());
                                linha8.add(totalCidadesSemRepField.build());
                            }));
                        }));
                    }));
                }));
                perfisList.addAll(Arrays.asList(vitrineChooser, vitrineVirtualChooser, premiumChooser, extraChooser, specialChooser));
                colecaoList.addAll(Arrays.asList(cidadesAtendidasChooser, colecaoAtualChooser, colecaoAnteriorChooser, colecaoPassadaChooser, colecaoReferenciaChooser, semClientesChooser, deixadasAtenderChooser, semRepresentanteChooser));
                col2.add(FormBox.create(box2 -> {
                    box2.vertical();
                    box2.expanded();
                    box2.add(tblPorRepresentante.build());
                }));
            }));

        }));
    }

    private void addListenersToggles() {
        apenasSemRepresentanteFilter.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) incluirSemRepresentanteFilter.value.setValue(false);
        });
        incluirSemRepresentanteFilter.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) apenasSemRepresentanteFilter.value.setValue(false);
        });
    }

    private void prepararListas() {
        cidadesBean.set(FXCollections.observableArrayList(cidades));
        limparCampos();
        carregarCampos();
    }

    private void limparCampos() {
        listRepresentantesComTotais.clear();
        totalSField.clear();
        totalVField.clear();
        totalPField.clear();
        totalVVField.clear();
        totalEField.clear();
        totalCidades.clear();
        totalCidadesAtendidas.clear();
        totalCidadesColAtual.clear();
        totalCidadesColAnterior.clear();
        totalCidadesColPassada.clear();
        totalCidadesColReferencia.clear();
        totalCidadesSemClientes.clear();
    }

    private void carregarCampos() {
        mapPerfisCidades = cidadesBean.stream().collect(Collectors.groupingBy(it -> it.getId().getCodCid().getClasse(), Collectors.counting()));

        totalSField.value.setValue(String.valueOf(mapPerfisCidades.getOrDefault("S", 0L)));
        totalVField.value.setValue(String.valueOf(mapPerfisCidades.getOrDefault("V", 0L)));
        totalPField.value.setValue(String.valueOf(mapPerfisCidades.getOrDefault("P", 0L)));
        totalVVField.value.setValue(String.valueOf(mapPerfisCidades.getOrDefault("VV", 0L)));
        totalEField.value.setValue(String.valueOf(mapPerfisCidades.getOrDefault("E", 0L)));

        totalCidades.value.setValue(String.valueOf(cidadesBean.size()));
        totalCidadesAtendidas.value.setValue(String.valueOf(cidadesBean.size() - cidadesBean.stream().filter(it -> it.getStatus() == 5).count() - cidadesBean.stream().filter(it -> it.getStatus() == 6).count()));
        totalCidadesColAtual.value.setValue(String.valueOf(cidadesBean.stream().filter(it -> it.getStatus() == 1).count()));
        totalCidadesColAnterior.value.setValue(String.valueOf(cidadesBean.stream().filter(it -> it.getStatus() == 2).count()));
        totalCidadesColPassada.value.setValue(String.valueOf(cidadesBean.stream().filter(it -> it.getStatus() == 3).count()));
        totalCidadesColReferencia.value.setValue(String.valueOf(cidadesBean.stream().filter(it -> it.getStatus() == 4).count()));
        totalCidadesSemClientes.value.setValue(String.valueOf(cidadesBean.stream().filter(it -> !it.naoTemRep()).filter(it -> it.getStatus() == 5).count()));

        totalCidadesSemRepField.value.setValue(String.valueOf(cidadesBean.stream().filter(VSdBidCidadesRep::naoTemRep).count()));
        gerarBidRep();

    }

    private void gerarBidRep() {
        listRepresentantesComTotais.clear();
        List<String> listCodrep = cidadesBean.stream().filter(it -> !it.naoTemRep()).map(it -> it.getId().getCodRep().getCodRep()).distinct().collect(Collectors.toList());

        for (String codRep : listCodrep) {
            Optional<VSdBidCidadesRep> bidD = cidadesBean.stream().filter(it -> it.getId().getCodRep().getCodRep().equals(codRep)).filter(eb -> eb.getId().getCodMarca().getCodigo().equals("D")).findFirst();
            Optional<VSdBidCidadesRep> bidF = cidadesBean.stream().filter(it -> it.getId().getCodRep().getCodRep().equals(codRep)).filter(eb -> eb.getId().getCodMarca().getCodigo().equals("F")).findFirst();

            checaMarcaRep(codRep, bidD);
            checaMarcaRep(codRep, bidF);
        }
    }

    private void checaMarcaRep(String codRep, Optional<VSdBidCidadesRep> bid) {
        if (bid.isPresent()) {
            List<VSdBidCidadesRep> cidadesFiltradas = cidadesBean.stream().filter(it -> it.getId().getCodRep().getCodRep().equals(codRep) && it.getId().getCodMarca().getCodigo().equals(bid.get().getId().getCodMarca().getCodigo())).collect(Collectors.toList());
            adicionarBidRep(cidadesFiltradas, bid.get());
        }
    }

    private void adicionarBidRep(List<VSdBidCidadesRep> cidadesFiltradas, VSdBidCidadesRep representante) {
        SdBidRep representanteComTotais = new SdBidRep();

        representanteComTotais.setRepresentante(representante.getId().getCodRep());
        representanteComTotais.setCidade(representante.getId().getCodCid());
        representanteComTotais.setMarca(representante.getId().getCodMarca());
        representanteComTotais.setQtdCidades(cidadesFiltradas.size());
        representanteComTotais.setCidadesColecaoAtual(cidadesFiltradas.stream().filter(it -> it.getStatus() == 1).count());
        representanteComTotais.setCidadesColecaoAnterior(cidadesFiltradas.stream().filter(it -> it.getStatus() == 2).count());
        representanteComTotais.setCidadesColecaoPassada(cidadesFiltradas.stream().filter(it -> it.getStatus() == 3).count());
        representanteComTotais.setCidadesColecaoRef(cidadesFiltradas.stream().filter(it -> it.getStatus() == 4).count());
        representanteComTotais.setCidadesSemClientes(cidadesFiltradas.stream().filter(it -> it.getStatus() == 5).count());

        listRepresentantesComTotais.add(representanteComTotais);
    }

    private void filtrarCidades(String newValue, String oldValue) {
        if (oldValue != null && (newValue.length() < oldValue.length())) {
            cidadesBean.set(FXCollections.observableList(cidades));
        }
        ObservableList<VSdBidCidadesRep> subentries = FXCollections.observableArrayList();
        for (VSdBidCidadesRep entry : cidadesBean) {
            if (entry.getId().getCodCid().getNomeCid().toUpperCase().contains(newValue)) {
                subentries.add(entry);
            }
        }
        tblCidades.refresh();
        cidadesBean.set(subentries);
        carregarCampos();
    }

    private void filtrarCidadesPorTipo() {
        ObservableList<VSdBidCidadesRep> subentries = FXCollections.observableArrayList();
        Set<String> perfis = perfisList.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());
        Set<String> colecoes = colecaoList.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());

        if (colecoes.contains("0")) {
            colecoes.addAll(Sets.newHashSet("1", "2", "3", "4"));
        }
        cidadesBean.set(FXCollections.observableArrayList(cidades));

        if (perfis.size() > 0 || colecoes.size() > 0) {
            for (VSdBidCidadesRep cidade : cidadesBean) {
                if ((perfis.size() == 0 || perfis.stream().anyMatch(it -> cidade.getId().getCodCid().getClasse().equals(it))) && (colecoes.stream().anyMatch(eb -> cidade.getStatus() == Integer.parseInt(eb)) || colecoes.size() == 0)) {
                    subentries.add(cidade);
                }
            }
            cidadesBean.set(subentries);
        }
        carregarCampos();
        tblCidades.refresh();
    }

    //<editor-fold desc="Excel">
    private void exportarExcel(String filePath) {
        try {
            ExportUtils.ExcelExportMode excelExportMode = new ExportUtils()
                    .excel(filePath)
                    .addSheet("Cidades BID Representante", sheet -> {
                        CellStyle headerCellStyle = criarHeader(sheet);
                        int rowNum = 1;
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "VV - VIT. VIRTUAL", totalVVField.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "V - VITRINE", totalVField.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "P - PREMIUM", totalPField.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "E - EXTRA", totalEField.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "S - SPECIAL", totalSField.value.getValue());
                        rowNum += 2;
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "TOTAL CIDADES", totalCidades.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "CIDADES ATENDIDAS", totalCidadesAtendidas.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, colecoes[0], totalCidadesColAtual.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, colecoes[1], totalCidadesColAnterior.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, colecoes[2], totalCidadesColPassada.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, colecoes[3], totalCidadesColReferencia.value.getValue());
                        rowNum = createExcelCellFromAppendTable(sheet, rowNum, "CIDADES SEM CLIENTES", totalCidadesSemClientes.value.getValue());

                        rowNum = 1;
                        for (VSdBidCidadesRep cidade : cidadesBean) {
                            Row row;
                            if (sheet.sheet.getRow(rowNum) == null) row = sheet.sheet.createRow(rowNum);
                            else row = sheet.sheet.getRow(rowNum);
                            rowNum++;

                            sheet.cellFormatValue(row, 0, cidade.naoTemRep() ? "" : cidade.getId().getCodRep().getCodRep(), "string");
                            sheet.cellFormatValue(row, 1, cidade.naoTemRep() ? "SEM REPRESENTANTE" : cidade.getId().getCodRep().getNome(), "string");
                            sheet.cellFormatValue(row, 2, cidade.getId().getCodCid().getCodCid(), "string");
                            sheet.cellFormatValue(row, 3, cidade.getId().getCodCid().getNomeCid(), "string");
                            sheet.cellFormatValue(row, 4, cidade.getId().getCodCid().getCodEst().toString(), "string");
                            sheet.cellFormatValue(row, 5, String.valueOf(cidade.getId().getCodCid().getPopCid()), "int");
                            sheet.cellFormatValue(row, 6, String.valueOf(cidade.getId().getCodCid().getIpc()), "int");
                            sheet.cellFormatValue(row, 7, cidade.getId().getCodCid().getClasse(), "string");
                            sheet.cellFormatValue(row, 8, "", "string");
                            sheet.cellFormatValue(row, 9, cidade.naoTemRep() ? "" : cidade.getId().getCodMarca().getDescricao(), "string");
                            row.getCell(7).setCellStyle(ExportUtils.setCellStyleSD(cidade.getId().getCodCid().getClasse(), sheet));
                            row.getCell(8).setCellStyle(ExportUtils.setCellStyleSD(String.valueOf(cidade.getStatus()), sheet));
                        }
                        if (sheet.sheet.getRow(7) == null) sheet.sheet.createRow(7);
                        Cell cell1 = sheet.sheet.getRow(7).createCell(11);
                        cell1.setCellValue("LEGENDA");
                        cell1.setCellStyle(headerCellStyle);

                        for (int i = 0; i <= header.size() + 5; i++) {
                            sheet.sheet.autoSizeColumn(i);
                        }
                    });

            excelExportMode.export();
        } catch (IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private CellStyle criarHeader(ExportUtils.ExcelSheet sheet) {
        Font headerFont = sheet.workbook.createFont();
        headerFont.setBold(true);
        CellStyle headerCellStyle = sheet.workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow = sheet.sheet.createRow(0);

        for (int i = 0; i < header.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(header.get(i).toUpperCase());
            cell.setCellStyle(headerCellStyle);
        }
        Cell cell = headerRow.createCell(11);
        cell.setCellValue("PERFIL");
        cell.setCellStyle(headerCellStyle);
        return headerCellStyle;
    }

    private int createExcelCellFromAppendTable(ExportUtils.ExcelSheet sheet, int rowNum, String text, String valor) {
        XSSFCellStyle style = (XSSFCellStyle) sheet.workbook.createCellStyle();
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        Font fontStyle = sheet.workbook.createFont();
        fontStyle.setColor(IndexedColors.BLACK.getIndex());
        fontStyle.setBold(true);

        if (text.equals("TOTAL CIDADES")) style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
        else if (text.equals("CIDADES ATENDIDAS")) {
            style.setFillForegroundColor(IndexedColors.DARK_GREEN.getIndex());
            fontStyle.setColor(IndexedColors.WHITE.getIndex());
        } else if (text.equals(colecoes[0])) style.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        else if (text.equals(colecoes[1])) style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        else if (text.equals(colecoes[2])) style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        else if (text.equals(colecoes[3])) style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        else if (text.equals("CIDADES SEM CLIENTES")) style.setFillForegroundColor(IndexedColors.RED.getIndex());

        else if (text.equals("VV - VIT. VIRTUAL")) style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
        else if (text.equals("V - VITRINE")) style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
        else if (text.equals("P - PREMIUM")) style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        else if (text.equals("E - EXTRA")) style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        else if (text.equals("S - SPECIAL")) {
            style.setFillForegroundColor(IndexedColors.VIOLET.getIndex());
            fontStyle.setColor(IndexedColors.WHITE.getIndex());
        } else style.setFillForegroundColor(IndexedColors.WHITE.getIndex());

        style.setFont(fontStyle);

        Row row;
        if (sheet.sheet.getRow(rowNum) == null) row = sheet.sheet.createRow(rowNum);
        else row = sheet.sheet.getRow(rowNum);

        sheet.cellFormatValue(row, 11, text, "string");
        sheet.sheet.getRow(rowNum).getCell(11).setCellStyle(style);
        sheet.cellFormatValue(row, 12, valor, "string");

        return ++rowNum;
    }
    //</editor-fold>

    private void imprimirPdf() {
        if (!cidadesBean.isEmpty()) {
            String filePath = GUIUtils.showSaveFileDialog(Collections.singletonList(new FileChooser.ExtensionFilter("Arquivo PDF (.pdf)", "*.pdf")));
            if (filePath != null && !filePath.equals("")) {
                new RunAsyncWithOverlay(this).exec(task -> {
                    try {
                        new ReportUtils().config()
                                .addReport(ReportUtils.ReportFile.RELATORIO_CIDADES,
                                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()),
                                        new ReportUtils.ParameterReport("codrep", codRepFilter.objectValues.getValue().stream().map(Represen::getCodRep).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                                        new ReportUtils.ParameterReport("codMar", marcaFilter.objectValues.getValue().stream().map(Marca::getCodigo).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                                        new ReportUtils.ParameterReport("codEst", estadoFilter.objectValues.getValue().stream().map(TabUf::getSigla).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                                        new ReportUtils.ParameterReport("codCid", cidadeCodFilter.objectValues.getValue().stream().map(Cidade::getCodCid).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                                        new ReportUtils.ParameterReport("nomeCid", cidadeNomeFilter.value.getValueSafe().equals("") ? null : cidadeNomeFilter.value.getValue()),
                                        new ReportUtils.ParameterReport("classe", perfisList.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.joining(","))),
                                        new ReportUtils.ParameterReport("status", colecaoList.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.joining(","))),
                                        new ReportUtils.ParameterReport("semRep", apenasSemRepresentanteFilter.value.getValue()  == true ? "S" : "N"),
                                        new ReportUtils.ParameterReport("inclui", incluirSemRepresentanteFilter.value.getValue()  == true ? "S" : "N")

                                )
                                .view()
                                .toPdf(filePath);
                    } catch (JRException e) {
                        e.printStackTrace();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        MessageBox.create(message -> {
                            message.message("PDF exportado com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.show();
                            message.position(Pos.TOP_RIGHT);
                        });
                    }
                });
            }
        }
    }
}
