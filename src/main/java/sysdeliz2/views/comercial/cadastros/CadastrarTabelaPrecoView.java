package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.cadastros.CadastrarTabelaPrecoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.Regiao;
import sysdeliz2.models.view.VSdGestaoMatFaltante;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;

import static sysdeliz2.utils.LoggerUtils.diferencaDeObjs;

public class CadastrarTabelaPrecoView extends CadastrarTabelaPrecoController {

    protected final ListProperty<Regiao> indicesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private Regiao oldRegiao = new Regiao();

    // <editor-fold defaultstate="collapsed" desc="Declaração: Filter">
    private final FormFieldSegmentedButton<String> ativoSegButton = FormFieldSegmentedButton.create(ff -> {
        ff.title("Ativo");
        ff.options(
                ff.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                ff.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                ff.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        ff.select(0);
    });
    private final FormFieldSegmentedButton<String> reservaSegButton = FormFieldSegmentedButton.create(ff -> {
        ff.title("Reserva");
        ff.options(
                ff.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                ff.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                ff.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        ff.select(0);
    });
    private final FormFieldSegmentedButton<String> mktSegButton = FormFieldSegmentedButton.create(ff -> {
        ff.title("MKT");
        ff.options(
                ff.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                ff.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                ff.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        ff.select(0);
    });
    private final FormFieldMultipleFind<Regiao> filterCodigoField = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Código");

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Declaração: Table">
    private final FormTableView<Regiao> tableListagem = FormTableView.create(Regiao.class, table -> {
        table.title("Cadastrados");
        table.expanded();
        table.items.bind(indicesBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Regiao, Regiao>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(Regiao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Região");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRegiao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Padrão");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().isPadrao()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Índice");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getIndice()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Sd Ativo");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSdAtivo()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Usa Reserva");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().isUsaReserva()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Usa Mkt");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().isUsaMkt()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Obs 2503");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getObs2503()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com1");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCom1()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com2");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCom2()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Obs");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getObs()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Sd Região");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdRegiao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Sd Substituta");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Regiao, Regiao>, ObservableValue<Regiao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdSubstituta()));
                }).build()
        );

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<Regiao> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tableListagem);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((Regiao) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarCadastro((Regiao) nav.selectedItem.get()));
        nav.btnSave(evt -> {
            try {
                salvarCadastro((Regiao) nav.selectedItem.get());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((Regiao) newValue);
            }
        });
    });
    // </editor-fold>//

    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields">
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle padraoField = FormFieldToggle.create(field -> {
        field.title("Padrão");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText indiceField = FormFieldText.create(field -> {
        field.title("Índice");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle liberadoField = FormFieldToggle.create(field -> {
        field.title("Liberado");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText obs2503Field = FormFieldText.create(field -> {
        field.title("Obs 2503");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText com1Field = FormFieldText.create(field -> {
        field.title("Com1");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText com2Field = FormFieldText.create(field -> {
        field.title("Com2");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText obsField = FormFieldText.create(field -> {
        field.title("Obs");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText sdRegiaoField = FormFieldText.create(field -> {
        field.title("Sd Região");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText sdSubstitutaField = FormFieldText.create(field -> {
        field.title("Sd Substituta");
        field.width(300);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle sdAtivoField = FormFieldToggle.create(field -> {
        field.title("Sd Ativo");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle sdUsaReservaField = FormFieldToggle.create(field -> {
        field.title("Usa Reserva");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle usaMktField = FormFieldToggle.create(field -> {
        field.title("Usa Mkt");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });

    // </editor-fold>


    public CadastrarTabelaPrecoView() {
        super("Cadastrar Tabela de Preços", ImageUtils.getImage(ImageUtils.Icon.CADASTROS), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(FormBox.create(boxFilterCodigo -> {
                            boxFilterCodigo.horizontal();
                            boxFilterCodigo.add(filterCodigoField.build());
                            boxFilterCodigo.expanded();
                        }));
                        box.add(FormBox.create(boxSegButtonFilter -> {
                            boxSegButtonFilter.horizontal();
                            boxSegButtonFilter.add(ativoSegButton.build(),
                                    reservaSegButton.build(),
                                    mktSegButton.build());
                        }));
                    }));

                    filter.find.setOnAction(event -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            JPAUtils.clearEntitys(indices);
                            filtrar(filterCodigoField.objectValues.isEmpty() || filterCodigoField.textValue.getValue().equals("") ? null : filterCodigoField.textValue.getValue().split(",")
                                    , ativoSegButton.value.getValue(), reservaSegButton.value.getValue(), mktSegButton.value.getValue());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskRes -> {
                            if (taskRes.equals(ReturnAsync.OK.value)) {
                                indicesBean.set(FXCollections.observableList(super.indices));
                            }
                        });
                    });
                    filter.clean.setOnAction(event -> {
                        filterCodigoField.clear();
                        ativoSegButton.select(0);
                        reservaSegButton.select(0);
                        mktSegButton.select(0);
                    });
                }));
            }));
            principal.add(tableListagem.build());
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.title("Dados Gerais");
            section.vertical();
            section.add(FormBox.create(box -> {
                box.horizontal();
                box.add(codigoField.build());
                box.add(descricaoField.build());
                box.add(indiceField.build());
            }));
        }));
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.title("Dados Complementares");
            section.horizontal();
            section.add(FormBox.create(box -> {
                box.vertical();
                box.width(150);
                box.add(padraoField.build(), liberadoField.build(), sdAtivoField.build());
            }));
            section.add(FormBox.create(box -> {
                box.vertical();
                box.width(150);
                box.add(sdUsaReservaField.build(), usaMktField.build());
            }));
            section.add(FormBox.create(boxFields -> {
                boxFields.vertical();
                boxFields.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(obsField.build(), obs2503Field.build(), com1Field.build());
                }));
                boxFields.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(com2Field.build(), sdRegiaoField.build(), sdSubstitutaField.build());
                }));
            }));

        }));
    }

    private void novoCadastro() {
        navegation.inEdition.set(true);
        navegation.selectedItem.set(new Regiao());
        limparCampos();
    }

    private void excluirCadastro(Regiao item) {
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Você deseja excluir o tabela de preços?");
            message.showAndWait();
        }).value.get()) {
            oldRegiao = getValues();
            SysLogger.addSysDelizLog("Cadastro de Tabela de Preço", TipoAcao.EXCLUIR, oldRegiao.getRegiao(), "Tabela de Preço Deletadata -- Código: " + oldRegiao.getRegiao() + " -- Descrição: " + oldRegiao.getDescricao());
            new FluentDao().delete(item);
            indices.remove(item);
            tableListagem.refresh();
            cancelarCadastro();
            MessageBox.create(message -> {
                message.message("Tabela de Preços excluída com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void abrirCadastro(Regiao item) {
        navegation.selectedItem.set(item);
        tabs.getSelectionModel().select(1);
        carregaDados(item);
    }

    private void carregaDados(Regiao item) {
        if (item != null)
            if (item.getRegiao() != null) {
                codigoField.value.setValue(item.getRegiao());
                descricaoField.value.setValue(item.getDescricao());
                padraoField.value.set(item.isPadrao());
                indiceField.value.setValue(item.getIndice() == null ? "" : item.getIndice().toString());
                liberadoField.value.set(item.isLiberado());
                obs2503Field.value.setValue(item.getObs2503());
                com1Field.value.setValue(item.getCom1().toString());
                com2Field.value.setValue(item.getCom2().toString());
                obsField.value.setValue(item.getObs());
                sdRegiaoField.value.setValue(item.getSdRegiao());
                sdSubstitutaField.value.setValue(item.getSdSubstituta());
                sdAtivoField.value.set(item.isSdAtivo());
                sdUsaReservaField.value.set(item.isUsaReserva());
                usaMktField.value.set(item.isUsaMkt());
            }
    }

    private void editarCadastro(Regiao item) {
        navegation.inEdition.set(true);
        oldRegiao = getValues();
        abrirCadastro(item);
        MessageBox.create(message -> {
            message.message("Ativado modo edição");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarCadastro(Regiao item) throws IllegalAccessException {

        item = getValues();

        if (buscaCodigo(codigoField.value.getValue())) {
            try {
                new FluentDao().persist(item);
                indices.add(item);
                navegation.selectedItem.set(item);
                MessageBox.create(message -> {
                    message.message("Tabela de Preços salva com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro de Tabela de Preço", TipoAcao.CADASTRAR, item.getRegiao(),
                        "Tabela de Preço Cadastrada -- Código: " + item.getRegiao() +
                                " -- Descrição: " + item.getDescricao());
            } catch (SQLException e) {

                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        } else {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tabela de Preços já existe, deseja altera-la?");
                message.showAndWait();
            }).value.get()) {
                navegation.selectedItem.set(item);
                new FluentDao().merge(item);

                MessageBox.create(message -> {
                    message.message("Tabela de Preços alterada com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Cadastro de Tabela de Preço", TipoAcao.EDITAR, item.getRegiao(),
                        "Tabela de Preço Alterada -- Código: " + item.getRegiao() +
                                " -- Descrição: " + item.getDescricao() + diferencaDeObjs(item, oldRegiao));

            } else {
                cancelarCadastro();
            }
        }
        tableListagem.refresh();
        navegation.inEdition.set(false);
    }

    private boolean buscaCodigo(String codigo) {
        return new FluentDao().selectFrom(Regiao.class).where(eb -> eb.equal("regiao", codigo)).singleResult() == null;
    }

    private void cancelarCadastro() {
        navegation.inEdition.set(false);
        navegation.selectedItem.set(null);
        limparCampos();
        indicesBean.set(FXCollections.observableList(super.indices));
        if (!tableListagem.items.isEmpty()) tableListagem.selectItem(0);
    }

    private void limparCampos() {
        codigoField.clear();
        descricaoField.clear();
        padraoField.value.set(false);
        indiceField.clear();
        liberadoField.value.set(false);
        obs2503Field.clear();
        com1Field.clear();
        com2Field.clear();
        obsField.clear();
        sdRegiaoField.clear();
        sdSubstitutaField.clear();
        sdAtivoField.value.set(false);
        sdUsaReservaField.value.set(false);
        usaMktField.value.set(false);
    }

    private Regiao getValues() {
        Regiao values = new Regiao();

        values.setRegiao(codigoField.value.getValue());
        values.setDescricao(descricaoField.value.getValue() == null ? "" : descricaoField.value.getValue());
        values.setPadrao(padraoField.value.getValue());
        values.setIndice(indiceField.value.getValue() != null ? new BigDecimal(indiceField.value.getValue()) : BigDecimal.ZERO);
        values.setLiberado(liberadoField.value.getValue());
        values.setObs2503(obs2503Field.value.getValue() == null ? "" : obs2503Field.value.getValue());
        values.setCom1(com1Field.value.getValue() != null ? new BigDecimal(com1Field.value.getValue()) : BigDecimal.ZERO);
        values.setCom2(com2Field.value.getValue() != null ? new BigDecimal(com2Field.value.getValue()) : BigDecimal.ZERO);
        values.setObs(obsField.value.getValue() == null ? "" : obsField.value.getValue());
        values.setSdRegiao(sdRegiaoField.value.getValue() == null ? "" : sdRegiaoField.value.getValue());
        values.setSdSubstituta(sdSubstitutaField.value.getValue() == null ? "" : sdSubstitutaField.value.getValue());
        values.setSdAtivo(sdAtivoField.value.getValue());
        values.setUsaReserva(sdUsaReservaField.value.getValue());
        values.setUsaMkt(usaMktField.value.getValue());

        return values;
    }
}
