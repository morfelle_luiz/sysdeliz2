package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.*;
import sysdeliz2.controllers.views.comercial.cadastros.CadastroComboController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.ProdCombo;
import sysdeliz2.models.ti.Regiao;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class CadastroComboView extends CadastroComboController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty combosBean = new SimpleListProperty(FXCollections.observableArrayList());
    private List<ProdCombo> comboToAdd = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<ProdCombo> comboTbl = FormTableView.create(ProdCombo.class, table -> {
        table.title("Combos");
        table.items.bind(combosBean);
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<ProdCombo, ProdCombo>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnRemove = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                btn.tooltip("Excluir combo");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("danger");
                            });

                            @Override
                            protected void updateItem(ProdCombo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnRemove.setOnAction(evt -> {
                                        excluirCombo(item, false);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnRemove);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Prod Original");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProdori()));
                }).build(), /*Prod Original*/
                FormTableColumn.create(cln -> {
                    cln.title("Prod. Irmão");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProdirm()));
                }).build(), /*Prod. Irmão*/
                FormTableColumn.create(cln -> {
                    cln.title("Tab. Preço");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTabela()));
                }).build(), /*Prod. Irmão*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Inicial");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<ProdCombo, ProdCombo>() {
                            @Override
                            protected void updateItem(ProdCombo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(true);
                                        field.width(100);
                                        field.withoutTitle();
                                        field.value.setValue(String.valueOf(item.getQtdecombo().intValue()));
                                        AtomicReference<BigDecimal> qtdeAntiga = new AtomicReference<>(item.getQtdecombo());
                                        field.keyPressed(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                try {
                                                    Robot robot = new Robot();
                                                    robot.keyPress(9);
                                                } catch (AWTException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        field.focusedListener((observable, oldValue, newValue) -> {
                                            if (newValue) {
                                                qtdeAntiga.set(new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValueSafe()));
                                            } else {
                                                BigDecimal qtdeNova = new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValueSafe().replace(",", "."));
                                                if (!qtdeNova.equals(qtdeAntiga.get())) {
                                                    item.setQtdecombo(qtdeNova);
                                                    new FluentDao().merge(item);
                                                    SysLogger.addSysDelizLog("Cadastro de Combo", TipoAcao.EDITAR, item.getId().getProdori(), "Quantidade inicial alterada de " + qtdeAntiga.get() + " para " + qtdeNova + " na tabela " + item.getId().getTabela());
                                                    MessageBox.create(message -> {
                                                        message.message("Quantidade inicial atualizada com sucesso!");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                    field.value.setValue(String.valueOf(item.getQtdecombo().intValue()));
                                                }
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build(), /*Prod. Irmão*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Final");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<ProdCombo, ProdCombo>() {
                            @Override
                            protected void updateItem(ProdCombo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(true);
                                        field.width(100);
                                        field.withoutTitle();
                                        field.value.setValue(String.valueOf(item.getQtdeate().intValue()));
                                        AtomicReference<BigDecimal> qtdeAntiga = new AtomicReference<>(item.getQtdeate());
                                        field.keyPressed(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                try {
                                                    Robot robot = new Robot();
                                                    robot.keyPress(9);
                                                } catch (AWTException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        field.focusedListener((observable, oldValue, newValue) -> {
                                            if (newValue) {
                                                qtdeAntiga.set(new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValueSafe()));
                                            } else {
                                                BigDecimal qtdeNova = new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValueSafe().replace(",", "."));
                                                if (!qtdeNova.equals(qtdeAntiga.get())) {
                                                    item.setQtdeate(qtdeNova);
                                                    new FluentDao().merge(item);
                                                    field.value.setValue(String.valueOf(item.getQtdeate().intValue()));
                                                    SysLogger.addSysDelizLog("Cadastro de Combo", TipoAcao.EDITAR, item.getId().getProdori(), "Quantidade final alterada de " + qtdeAntiga.get() + " para " + qtdeNova + " na tabela " + item.getId().getTabela());
                                                    MessageBox.create(message -> {
                                                        message.message("Quantidade final atualizada com sucesso!");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                }
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build(), /*Prod. Irmão*/
                FormTableColumn.create(cln -> {
                    cln.title("Preço");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<ProdCombo, ProdCombo>, ObservableValue<ProdCombo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<ProdCombo, ProdCombo>() {
                            @Override
                            protected void updateItem(ProdCombo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(true);
                                        field.width(100);
                                        field.withoutTitle();
                                        field.value.setValue(StringUtils.toMonetaryFormat(item.getPrecopeca(), 2));
                                        AtomicReference<BigDecimal> valorAntigo = new AtomicReference<>(item.getPrecopeca());
                                        field.keyPressed(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                try {
                                                    Robot robot = new Robot();
                                                    robot.keyPress(9);
                                                } catch (AWTException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        field.focusedListener((observable, oldValue, newValue) -> {
                                            if (newValue) {
                                                valorAntigo.set(new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValue().replaceAll(",", ".").replace("R$", "").trim()));
                                            } else {
                                                BigDecimal valorNovo = new BigDecimal(field.value.getValueSafe().equals("") ? "0" : field.value.getValue().replaceAll(",", ".").replace("R$", "").trim());
                                                if (!valorNovo.equals(valorAntigo.get())) {
                                                    item.setPrecopeca(valorNovo);
                                                    new FluentDao().merge(item);
                                                    SysLogger.addSysDelizLog("Cadastro de Combo", TipoAcao.EDITAR, item.getId().getProdori(), "Preço final alterado de " + valorAntigo.get() + " para " + valorNovo + " na tabela " + item.getId().getTabela());
                                                    MessageBox.create(message -> {
                                                        message.message("Preço atualizada com sucesso!");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                    field.value.setValue(StringUtils.toMonetaryFormat(item.getPrecopeca(), 2));
                                                }
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<VSdDadosProduto> prodOrigialFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Prod. Original");
        field.toUpper();
        field.width(220);
    });
    private final FormFieldMultipleFind<VSdDadosProduto> prodIrmaoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Prod. Irmão");
        field.toUpper();
        field.width(220);
    });
    private final FormFieldMultipleFind<Regiao> tabPrecoFilter = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Tab. Preço");
        field.toUpper();
        field.width(220);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private Button importExcelBtn = FormButton.create(btn -> {
        btn.setText("Importar Excel");
        btn.addStyle("success");
        btn.tooltip("PROD/PROD_IRMAO/TABELA/QTDE/PRECO/(QTDE_FINAL)");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            importarExcel();
        });
    });

    private Button btnConfirmarImport = FormButton.create(btn -> {
        btn.title("Confirmar Importação");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.disableProperty().unbind();
        btn.setDisable(true);
        btn.setOnAction(evt -> {
            salvarCombosImportados();
        });
    });

    private Button btnLimparTabela = FormButton.create(btn -> {
        btn.title("Limpar Produtos");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
        btn.addStyle("primary");
        btn.setOnAction(evt -> {
            listCombo.clear();
            comboTbl.clear();
            btnConfirmarImport.setDisable(true);
        });
    });

    private Button btnExcluirMarcados = FormButton.create(btn -> {
        btn.title("Excluir Combos Marcados");
        btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.addStyle("danger");
        btn.setOnAction(evt -> {
            List<ProdCombo> selecteds = comboTbl.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
            if (selecteds.size() == 0) {
                MessageBox.create(message -> {
                    message.message("Selecione um combo pelo menos");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            }
            selecteds.forEach(it -> excluirCombo(it,true));
        });
    });
    // </editor-fold>

    public CadastroComboView() {
        super("Cadastrar Combos", ImageUtils.getImage(ImageUtils.Icon.CADASTROS));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(prodOrigialFilter.build(), prodIrmaoFilter.build(), tabPrecoFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarCombos(
                                        prodOrigialFilter.objectValues.isEmpty() ? new String[]{} : prodOrigialFilter.objectValues.getValue().stream().map(VSdDadosProduto::getCodigo).toArray(),
                                        prodIrmaoFilter.objectValues.isEmpty() ? new String[]{} : prodIrmaoFilter.objectValues.getValue().stream().map(VSdDadosProduto::getCodigo).toArray(),
                                        tabPrecoFilter.objectValues.isEmpty() ? new String[]{} : tabPrecoFilter.objectValues.getValue().stream().map(Regiao::getRegiao).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    combosBean.set(FXCollections.observableArrayList(listCombo));
                                    btnConfirmarImport.setDisable(true);
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            prodIrmaoFilter.clear();
                            prodOrigialFilter.clear();
                            tabPrecoFilter.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(comboTbl.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(importExcelBtn, btnLimparTabela, btnConfirmarImport, btnExcluirMarcados);
            }));
        }));
    }

//    private void addNewCombo(ProdCombo combo) {
//        comboTbl.addColumn(FormTableColumnGroup.createGroup(gp -> {
//            gp.addColumn(FormTableColumn.create(cln -> {
//                cln.title("Qtde Init");
//                cln.width(70);
//                cln.value((Callback<TableColumn.CellDataFeatures<ProdutoIrmao, ProdutoIrmao>, ObservableValue<ProdutoIrmao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
//                cln.format(param -> {
//                    return new TableCell<ProdutoIrmao, ProdutoIrmao>(){
//                        @Override
//                        protected void updateItem(ProdutoIrmao item, boolean empty) {
//                            super.updateItem(item, empty);
//                            setText(null);
//                            if (item != null && !empty) {
//                                gp.title(combo.getId().getProdori() + " - " + combo.getId().getProdirm() +  " / " + combo.getId().getTabela());
//                                gp.setGraphic(FormButton.create(button -> {
//                                    button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
//                                    button.addStyle("danger");
//                                    button.tooltip("Remover coluna");
//                                    button.setMaxHeight(10.0);
//                                    button.setMaxWidth(10.0);
//                                    button.setAlignment(Pos.CENTER_LEFT);
//                                    button.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");
//                                    button.setOnAction(evt -> {
//                                        comboTbl.removeColumn(gp);
//                                    });
//                                }));
//                                setGraphic(FormFieldText.create(field -> {
//                                    field.withoutTitle();
//                                    field.width(50);
//                                    field.value.set(combo.getQtdecombo().toString());
//                                }).build());
//                            }
//                        }
//                    };
//                });
//            }).build());
//
//            gp.addColumn(FormTableColumn.create(cln -> {
//                cln.title("Qtde Até");
//                cln.width(70);
//                cln.value((Callback<TableColumn.CellDataFeatures<ProdutoIrmao, ProdutoIrmao>, ObservableValue<ProdutoIrmao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
//                cln.format(param -> {
//                    return new TableCell<ProdutoIrmao, ProdutoIrmao>(){
//                        @Override
//                        protected void updateItem(ProdutoIrmao item, boolean empty) {
//                            super.updateItem(item, empty);
//                            setText(null);
//                            if (item != null && !empty) {
//                                setGraphic(FormFieldText.create(field -> {
//                                    field.withoutTitle();
//                                    field.width(50);
//                                    field.value.set(combo.getQtdeate().toString());
//                                }).build());
//                            }
//                        }
//                    };
//                });
//            }).build());
//
//            gp.addColumn(FormTableColumn.create(cln -> {
//                cln.title("Preço Peça");
//                cln.width(70);
//                cln.value((Callback<TableColumn.CellDataFeatures<ProdutoIrmao, ProdutoIrmao>, ObservableValue<ProdutoIrmao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
//                cln.format(param -> {
//                    return new TableCell<ProdutoIrmao, ProdutoIrmao>(){
//                        @Override
//                        protected void updateItem(ProdutoIrmao item, boolean empty) {
//                            super.updateItem(item, empty);
//                            setText(null);
//                            if (item != null && !empty) {
//                                setGraphic(FormFieldText.create(field -> {
//                                    field.withoutTitle();
//                                    field.width(50);
//                                    field.value.set(combo.getPrecopeca().toString());
//                                }).build());
//                            }
//                        }
//                    };
//                });
//            }).build());
//        }));
//    }

    private void importarExcel() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir Arquivo");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Excel File", "*.xls*", "*.xlsx"));
        List<Regiao> regioesToAdd = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);

        AtomicReference<Workbook> workbook = new AtomicReference<>();
        File fileToOpen = fileChooser.showOpenDialog(new Stage());
        if (fileToOpen == null) {
            return;
        }
        listCombo.clear();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                File file = new File(fileToOpen.getAbsolutePath());
                workbook.set(WorkbookFactory.create(new FileInputStream(file)));
                Sheet sheet = workbook.get().getSheetAt(0);
                Iterator<Row> rowIterator = sheet.rowIterator();
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    String prodOriginal = null;
                    String prodIrmao = null;
                    String tabela = null;
                    String qtde = null;
                    String preco = null;
                    String qtdeAte = "";

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        switch (cell.getColumnIndex()) {
                            case 0:
                                if (cell.getCellType().equals(CellType.STRING))
                                    prodOriginal = cell.getStringCellValue();
                                else prodOriginal = String.valueOf(cell.getNumericCellValue());
                                break;
                            case 1:
                                if (cell.getCellType().equals(CellType.STRING)) prodIrmao = cell.getStringCellValue();
                                else prodIrmao = String.valueOf((int) cell.getNumericCellValue());
                                break;
                            case 2:
                                if (cell.getCellType().equals(CellType.STRING)) tabela = cell.getStringCellValue();
                                else tabela = df.format(cell.getNumericCellValue());
                                break;
                            case 3:
                                if (cell.getCellType().equals(CellType.STRING)) qtde = cell.getStringCellValue();
                                else qtde = df.format(cell.getNumericCellValue());
                                break;
                            case 4:
                                if (cell.getCellType().equals(CellType.STRING)) preco = cell.getStringCellValue();
                                else preco = df.format(cell.getNumericCellValue());
                                break;
                            case 5:
                                if (cell.getCellType().equals(CellType.STRING))
                                    qtdeAte = Integer.valueOf(cell.getStringCellValue()) == 0 || cell.getStringCellValue() == null ? "99999" : cell.getStringCellValue();
                                else
                                    qtdeAte = df.format(cell.getNumericCellValue() == 0.0 ? 99999 : cell.getNumericCellValue());
                                break;
                        }
                    }

                    if (prodOriginal != null && prodIrmao != null && tabela != null && qtde != null && preco != null) {
                        String codigoProduto = prodOriginal;
                        VSdDadosProduto prod = new FluentDao().selectFrom(VSdDadosProduto.class).where(it -> it.equal("codigo", codigoProduto)).singleResult();
                        String finalProdOriginal = prodOriginal;
                        String finalProdIrmao = prodIrmao;
                        String finalTabela = tabela;
                        String finalQtde = qtde;
                        String finalQtdeAte = qtdeAte == null || qtdeAte.equals("") ? "99999" : qtdeAte;
                        if (prod != null && (
                                listCombo.stream().noneMatch(it ->
                                        it.getId().getProdori().equals(finalProdOriginal) &&
                                                it.getId().getProdirm().equals(finalProdIrmao) &&
                                                it.getId().getTabela().equals(finalTabela) &&
                                                it.getQtdecombo().equals(new BigDecimal(finalQtde.replace(",", ".")))))) {
                            ProdCombo combo = new ProdCombo(prodOriginal, prodIrmao, tabela, new BigDecimal(qtde.replace(",", ".")), new BigDecimal(preco.replace(",", ".")), new BigDecimal(finalQtdeAte.replace(",", ".")));
                            comboToAdd.add(combo);
                            listCombo.add(combo);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                try {
                    combosBean.set(FXCollections.observableArrayList(listCombo));
                    if (comboToAdd.size() > 0) btnConfirmarImport.setDisable(false);
                    workbook.get().close();
                    MessageBox.create(message -> {
                        message.message("Excel importado com sucesso");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void salvarCombosImportados() {
        new RunAsyncWithOverlay(this).exec(task -> {
            for (ProdCombo combo : comboToAdd) {
                new FluentDao().merge(combo);
                SysLogger.addSysDelizLog("Cadastro de Combos", TipoAcao.CADASTRAR, combo.getId().getProdori(), "Combo do Produto " + combo.getId().getProdori() + " com o produto" + combo.getId().getProdirm() +
                        " salvo na tabela " + combo.getId().getTabela() + " com o preço " + combo.getPrecopeca() + " de " + combo.getQtdecombo() + " peças até " + combo.getQtdeate());
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                comboToAdd.clear();
                btnConfirmarImport.setDisable(true);
                MessageBox.create(message -> {
                    message.message("Preços importados com sucesso");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }

    private void excluirCombo(ProdCombo item, boolean emMassa) {
        if (!emMassa) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja excluir esse combo?");
                message.showAndWait();
            }).value.get())) {
                return;
            }
        }

        new FluentDao().delete(item);
        combosBean.remove(item);
        comboTbl.refresh();
        MessageBox.create(message -> {
            message.message("Combo excluído com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
}

