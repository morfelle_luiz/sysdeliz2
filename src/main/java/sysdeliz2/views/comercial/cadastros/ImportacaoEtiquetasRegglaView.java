package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.cadastros.ImportacaoEtiquetasController;
import sysdeliz2.models.sysdeliz.SdCliPaludo;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.ArrayList;
import java.util.List;

public class ImportacaoEtiquetasRegglaView extends ImportacaoEtiquetasController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<SdCliPaludo> etiquetasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdCliPaludo> etiquetasToAdd = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdCliPaludo> tblEtiquetas = FormTableView.create(SdCliPaludo.class, table -> {
        table.title("Etiquetas");
        table.expanded();
        table.items.bind(etiquetasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código Item");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodItem()));
                }).build(), /*CodItem*/
                FormTableColumn.create(cln -> {
                    cln.title("Barra Cli");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBarracli()));
                }).build(), /*Barra Cli*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição Item");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescitem()));
                }).build(), /*Descrição Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor Item");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCoritem()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Tamanho");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTamitem()));
                }).build(), /*Tamanho Item*/
                FormTableColumn.create(cln -> {
                    cln.title("NP");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNp()));
                }).build(), /*NP*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor Parcela");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValparcela()));
                }).build(), /*Valor Parcela*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor Total");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValtotal()));
                }).build(), /*Valor Total*/
                FormTableColumn.create(cln -> {
                    cln.title("Lote");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliPaludo, SdCliPaludo>, ObservableValue<SdCliPaludo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                }).build() /*Lote*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdCliPaludo>() {
                @Override
                protected void updateItem(SdCliPaludo item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                    if (item != null && !empty && etiquetasToAdd.contains(item)) {
                        getStyleClass().add("table-row-success");
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Produto> produtoField = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Cod Produto");
        field.width(200);
    });

    private final FormFieldMultipleFind<Cor> corFilter = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor");
        field.width(200);
    });

    private final FormFieldText codigoItemFilter = FormFieldText.create(field -> {
        field.title("Codigo Item");
        field.width(200);

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button btnImportarArquivo = FormButton.create(btn -> {
        btn.setText("Importar Arquivo");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            importarArquivoReggla(etiquetasToAdd, etiquetasBean);
            tblEtiquetas.refresh();
        });
    });

    private Button btnSalvarEtiquetas = FormButton.create(btn -> {
        btn.setText("Salvar Etiquetas");
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            salvarEtiquetas(etiquetasToAdd);
            tblEtiquetas.refresh();
        });
    });

    private Button btnCancelarImportacao = FormButton.create(btn -> {
        btn.setText("Cancelar Importação");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            cancelarImportacao(etiquetasToAdd, etiquetasBean);
            tblEtiquetas.refresh();
        });
    });

    // </editor-fold>

    public ImportacaoEtiquetasRegglaView() {
        super("Importação Etiquetas Reggla", ImageUtils.getImage(ImageUtils.Icon.ETIQUETA_PRODUTO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(produtoField.build(), corFilter.build(), codigoItemFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscaEtiquetasReggla(
                                        produtoField.objectValues.stream().map(Produto::getCodigo).toArray(),
                                        corFilter.objectValues.stream().map(Cor::getCor).toArray(),
                                        codigoItemFilter.value.get()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    etiquetasBean.set(FXCollections.observableArrayList(etiquetasReggla));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            produtoField.clear();
                            corFilter.clear();
                            codigoItemFilter.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblEtiquetas.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(btnImportarArquivo, btnSalvarEtiquetas, btnCancelarImportacao);
            }));
        }));
    }

}
