package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.cadastros.ImportacaoEtiquetasController;
import sysdeliz2.models.sysdeliz.SdCliDullius;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.ArrayList;
import java.util.List;

public class ImportacaoEtiquetasReisenView extends ImportacaoEtiquetasController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<SdCliDullius> etiquetasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdCliDullius> etiquetasToAdd = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdCliDullius> tblEtiquetas = FormTableView.create(SdCliDullius.class, table -> {
        table.title("Etiquetas");
        table.expanded();
        table.items.bind(etiquetasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código Item");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCoditem()));
                }).build(), /*CodItem*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição Item");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescitem()));
                }).build(), /*Descrição Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor Item");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCoritem()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor à Vista");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorvista()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Parcela à Vista");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParcelavista()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Núm. Parcelas à Vista");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumparcelavuita()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor a Prazo");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorprazo()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Parcela a Prazo");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParcelaprazo()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Núm. Parcelas a Prazo");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumparcelaprazo()));
                }).build(), /*Cor Item*/
                FormTableColumn.create(cln -> {
                    cln.title("Quantidade");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*NP*/
                FormTableColumn.create(cln -> {
                    cln.title("Tamanho");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCliDullius, SdCliDullius>, ObservableValue<SdCliDullius>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                }).build() /*Tamanho Item*/
        );
        table.factoryRow(param -> new TableRow<SdCliDullius>() {
            @Override
            protected void updateItem(SdCliDullius item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty && etiquetasToAdd.contains(item)) {
                    getStyleClass().add("table-row-success");
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Produto> produtoField = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Cod Produto");
        field.width(200);
    });

    private final FormFieldMultipleFind<Cor> corFilter = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor");
        field.width(200);
    });

    private final FormFieldText codigoItemFilter = FormFieldText.create(field -> {
        field.title("Codigo Item");
        field.width(200);

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button btnImportarArquivo = FormButton.create(btn -> {
        btn.setText("Importar Arquivo");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            importarArquivoReisen(etiquetasToAdd,etiquetasBean);
            tblEtiquetas.refresh();
        });
    });

    private Button btnSalvarEtiquetas = FormButton.create(btn -> {
        btn.setText("Salvar Etiquetas");
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            salvarEtiquetas(etiquetasToAdd);
            tblEtiquetas.refresh();
        });
    });

    private Button btnCancelarImportacao = FormButton.create(btn -> {
        btn.setText("Cancelar Importação");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            cancelarImportacao(etiquetasToAdd, etiquetasBean);
            tblEtiquetas.refresh();
        });
    });

    // </editor-fold>

    public ImportacaoEtiquetasReisenView() {
        super("Importação Etiquetas Reisen", ImageUtils.getImage(ImageUtils.Icon.ETIQUETA_PRODUTO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(produtoField.build(), corFilter.build(), codigoItemFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscaEtiquetasReisen(
                                        produtoField.objectValues.stream().map(Produto::getCodigo).toArray(),
                                        corFilter.objectValues.stream().map(Cor::getCor).toArray(),
                                        codigoItemFilter.value.get()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    etiquetasBean.set(FXCollections.observableArrayList(etiquetasReisen));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            produtoField.clear();
                            corFilter.clear();
                            codigoItemFilter.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblEtiquetas.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(btnImportarArquivo, btnSalvarEtiquetas,btnCancelarImportacao);
            }));
        }));
    }


}
