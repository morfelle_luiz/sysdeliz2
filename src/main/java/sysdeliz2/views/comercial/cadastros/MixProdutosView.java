package sysdeliz2.views.comercial.cadastros;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.apache.http.client.HttpResponseException;
import sysdeliz2.controllers.views.comercial.cadastros.MixProdutosController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdColabCelula001;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.sysdeliz.comercial.SdMixColecao;
import sysdeliz2.models.sysdeliz.comercial.SdMixProdutos;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.view.comercial.VSdLastYearMix;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.apis.fluxogama.ServicoFluxogama;
import sysdeliz2.utils.apis.fluxogama.models.RequestSendMix;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MixProdutosView extends MixProdutosController {

    // <editor-fold defaultstate="collapsed" desc="local">
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty novoMix = new SimpleBooleanProperty(false);
    private final BooleanProperty novaFamilia = new SimpleBooleanProperty(false);
    private final ListProperty<SdMixColecao> listaMixColecao = new SimpleListProperty(FXCollections.observableArrayList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cadastro">
    private final VBox tabCadastro = (VBox) super.box;
    private final FormFieldSingleFind<Colecao> fieldColecao = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(320.0);
        field.postSelected((observable, oldValue, newValue) -> carregarColecao((Colecao) field.value.get()));
    });
    private final FormFieldComboBox<Marca> fieldMarca = FormFieldComboBox.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200.0);
        field.editable.bind(emEdicao);
        field.withAddButton(o -> adicionarMarca());
        field.getSelectionModel((observable, oldValue, newValue) -> selecionarMarca((Marca) newValue));
    });
    private final FormFieldComboBox<Linha> fieldLinha = FormFieldComboBox.create(Linha.class, field -> {
        field.title("Linha");
        field.width(320.0);
        field.editable.bind(emEdicao.and(fieldMarca.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.withAddButton(o -> adicionarLinha());
        field.getSelectionModel((observable, oldValue, newValue) -> selecionarLinha((Linha) newValue));
    });
    private final FormFieldComboBox<SdGrupoModelagem> fieldGrupoModelagem = FormFieldComboBox.create(SdGrupoModelagem.class, field -> {
        field.title("Modelagem");
        field.width(370.0);
        field.editable.bind(emEdicao.and(fieldLinha.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.withAddButton(o -> adicionarModelagem());
        field.getSelectionModel((observable, oldValue, newValue) -> selecionarModelagem((SdGrupoModelagem) newValue));
    });
    private final FormFieldText fieldQtdeMostruario = FormFieldText.create(field -> {
        field.title("Qtde Mostruario");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.INTEGER);
        field.editable.bind(emEdicao.and(fieldGrupoModelagem.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
    });
    private final FormFieldComboBox<Integer> fieldIdMix = FormFieldComboBox.create(Integer.class, field -> {
        field.title("Mix");
        field.width(80);
        field.editable.bind(emEdicao.and(novoMix.not()).and(fieldGrupoModelagem.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.withAddButton(o -> adicionarMix());
        field.getSelectionModel((observable, oldValue, newValue) -> selecionarMix((Integer) newValue));
    });
    private final FormFieldText fieldQtdeProdutos = FormFieldText.create(field -> {
        field.title("Qtde Produtos");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.INTEGER);
        field.editable.bind(emEdicao.and(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleased(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYProdutos();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de produtos");
    });
    private final FormFieldText fieldMeta = FormFieldText.create(field -> {
        field.title("Meta");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.editable.bind(emEdicao.and(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleased(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYMeta();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de meta");
    });
    private final FormFieldRangeDouble fieldRangeValor = FormFieldRangeDouble.create(field -> {
        field.title("Intervalo PM (R$)");
        field.width(200.0);
        field.editable.bind(emEdicao.and(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleasedBegin(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYValores();
        });
        field.keyReleasedEnd(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYValores();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de valores");
    });
    private final FormTableView<SdMixColecao> tblMixColecao = FormTableView.create(SdMixColecao.class, table -> {
        table.title("Mixes");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getLinha()));
                }).build() /*Linha*/,
                FormTableColumn.create(cln -> {
                    cln.title("Grupo Mod.");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getGrupoModelagem()));
                }).build() /*Grupo Mod.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Mix");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMix()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mix*/,
                FormTableColumnGroup.createGroup(grp -> {
                    grp.title("Intervalor Valor");
                    grp.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Inicio");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorInicial()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdColabCelula001, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Inicio*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Fim");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorFinal()));
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.format(param -> {
                                    return new TableCell<SdColabCelula001, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Fim*/
                    );
                }).build() /*Intervalo Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Refs.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReferencias()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Refs.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Meta");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMeta()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Meta*/,
                FormTableColumn.create(cln -> {
                    cln.title("Mostr.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMostruario()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mostr.*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixColecao, SdMixColecao>, ObservableValue<SdMixColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMixColecao, SdMixColecao>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir mix da modelagem");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdMixColecao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirMix(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /*Act*/
        );
    });
    private final FormFieldComboBox<Familia> fieldFamiliaMix = FormFieldComboBox.create(Familia.class, field -> {
        field.title("Família");
        field.width(160.0);
        field.editable.bind(emEdicao.and(novaFamilia.not()).and(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.withAddButton(o -> adicionarFamiliaMix());
        field.getSelectionModel((observable, oldValue, newValue) -> selecionarFamiliaMix((Familia) newValue));
    });
    private final FormFieldText fieldQtdeProdutosFamilia = FormFieldText.create(field -> {
        field.title("Qtde Produtos");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.mask(FormFieldText.Mask.INTEGER);
        field.editable.bind(emEdicao.and(fieldFamiliaMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleased(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYProdutosFamilia();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de produtos");
    });
    private final FormFieldText fieldTempoMedio = FormFieldText.create(field -> {
        field.title("Tempo Médio");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.postLabel("min");
        field.editable.bind(emEdicao.and(fieldFamiliaMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleased(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYTempoMedio();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de tempo médio");
    });
    private final FormFieldText fieldMetaFamilia = FormFieldText.create(field -> {
        field.title("Meta");
        field.toUpper();
        field.width(120);
        field.alignment(Pos.CENTER);
        field.editable.bind(emEdicao.and(fieldFamiliaMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
        field.keyReleased(evt -> {
            if (evt.isControlDown() && evt.getCode().equals(KeyCode.L) && field.editable.get())
                carregaLYMetaFamilia();
        });
        field.tooltip("Pressione Ctrl + L para obter dados do last year de meta");
    });
    private final FormTableView<SdMixProdutos> tblMixProduto = FormTableView.create(SdMixProdutos.class, table -> {
        table.title("Famílias do Mix");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixProdutos, SdMixProdutos>, ObservableValue<SdMixProdutos>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFamilia()));
                }).build() /*Família*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde. Prod.");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixProdutos, SdMixProdutos>, ObservableValue<SdMixProdutos>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeProdutos()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Qtde. Prod.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Meta");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixProdutos, SdMixProdutos>, ObservableValue<SdMixProdutos>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMeta()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Meta*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tempo Méd.");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixProdutos, SdMixProdutos>, ObservableValue<SdMixProdutos>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTempoMedio()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMixProdutos, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Tempo Méd.*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMixProdutos, SdMixProdutos>, ObservableValue<SdMixProdutos>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMixProdutos, SdMixProdutos>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir mix da família.");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdMixProdutos item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirMixFamilia(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    public MixProdutosView() {
        super("Mix de Produtos", ImageUtils.getImage(ImageUtils.Icon.MIX_PRODUTOS));
        initCadastro();
    }

    //------------------ CADASTRO -----------------
    private void initCadastro() {
        tabCadastro.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(FormBox.create(top -> {
                top.horizontal();
                top.alignment(Pos.BOTTOM_LEFT);
                top.add(fieldColecao.build());
                top.add(FormButton.create(btn -> {
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.COPIAR, ImageUtils.IconSize._24));
                    btn.tooltip("Copiar mix da coleção para outra coleção.");
                    btn.addStyle("lg").addStyle("dark");
                    btn.disable.bind(listaMixColecao.emptyProperty());
                    btn.setAction(evt -> copiarMix());
                }));
                top.add(FormButton.create(btn -> {
                    btn.title("Enviar Fluxogama");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                    btn.addStyle("lg").addStyle("info");
                    btn.disable.bind(listaMixColecao.emptyProperty());
                    btn.setAction(evt -> enviarParaFluxogama());
                }));
            }));
            content.center(FormBox.create(container -> {
                container.vertical();
                container.expanded();
                container.add(FormTitledPane.create(tpconfig -> {
                    tpconfig.title("Configuração Marca/Linha");
                    tpconfig.collapsabled.bind(emEdicao);
                    tpconfig.addStyle("success");
                    tpconfig.add(fields -> fields.addHorizontal(
                            fieldMarca.build(),
                            fieldLinha.build()));

                }));
                container.add(FormTitledPane.create(tpconfig -> {
                    tpconfig.title("Configuração Modelagem");
                    tpconfig.collapsabled.bind(emEdicao.and(fieldLinha.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
                    tpconfig.addStyle("warning");
                    tpconfig.add(fields -> fields.addHorizontal(fieldGrupoModelagem.build(), fieldQtdeMostruario.build()));
                }));
                container.add(FormTitledPane.create(tpconfig -> {
                    tpconfig.title("Configuração Modelagem");
                    tpconfig.collapsabled.bind(emEdicao.and(fieldGrupoModelagem.getComboBox().getSelectionModel().selectedItemProperty().isNull().not()));
                    tpconfig.addStyle("primary");
                    tpconfig.add(fields -> {
                        fields.horizontal();
                        fields.alignment(Pos.TOP_LEFT);
                        fields.add(fieldIdMix.build(),
                                fieldQtdeProdutos.build(),
                                fieldMeta.build(),
//                                FormButton.create(btn -> {
//                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
//                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LAST_YEAR, ImageUtils.IconSize._24));
//                                    btn.tooltip("Obter dados do last year de valores");
//                                    btn.disable.bind(BooleanBinding.booleanExpression(emEdicao.and(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull().not())).not());
//                                    btn.setAction(evt -> carregaLYValores());
//                                }),
                                fieldRangeValor.build(),
                                FormButton.create(btn -> {
                                    btn.title("Add Mix");
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.MIX_PRODUTOS, ImageUtils.IconSize._32));
                                    btn.disable.bind(emEdicao.not().or(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull()));
                                    btn.addStyle("primary").addStyle("lg");
                                    btn.setAction(evt -> gravarMix());
                                }));
                    });
                }));
                container.add(tblMixColecao.build());
            }));
            content.right(FormBox.create(left -> {
                left.vertical();
                left.width(800.0);
                left.add(FormTitledPane.create(tpBox -> {
                    tpBox.title("Famílias Mix");
                    tpBox.expanded();
                    tpBox.collapsabled.bind(tblMixColecao.items.emptyProperty().not());
                    tpBox.addStyle("info");
                    tpBox.add(fields -> fields.addHorizontal(
                            fieldFamiliaMix.build(),
                            fieldQtdeProdutosFamilia.build(),
                            fieldMetaFamilia.build(),
                            fieldTempoMedio.build(),
                            FormButton.create(btn -> {
                                btn.title("Add Família");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO_FAVORITO, ImageUtils.IconSize._32));
                                btn.disable.bind(emEdicao.not().or(fieldIdMix.getComboBox().getSelectionModel().selectedItemProperty().isNull()));
                                btn.addStyle("info").addStyle("lg");
                                btn.setAction(evt -> gravarFamilia());
                            })
                    ));
                    tpBox.add(tblMixProduto.build());
                }));
            }));
            content.bottom(FormBox.create(toolbar -> {
                toolbar.horizontal();
                toolbar.expanded();
                toolbar.alignment(Pos.BOTTOM_LEFT);
                toolbar.add(FormButton.create(btn -> {
                    btn.title("Salvar Mix");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
                    btn.addStyle("lg").addStyle("success");
                    btn.disable.bind(listaMixColecao.emptyProperty());
                    btn.setAction(evt -> salvarMixColecao());
                }));
                toolbar.add(FormButton.create(btn -> {
                    btn.title("Cancelar");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                    btn.addStyle("danger");
                    btn.disable.bind(listaMixColecao.emptyProperty());
                    btn.setAction(evt -> cancelarCadastro());
                }));
            }));
        }));
    }

    ////
    private void carregarColecao(Colecao colecao) {
        if (colecao != null) {
            listaMixColecao.clear();
            limparCamposMarcaLinha();

            AtomicReference<Exception> exRwo = new AtomicReference<>();
            AtomicReference<Collection<? extends SdMixColecao>> listRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                listRwo.set(getMixColecao(colecao.getCodigo()));

                if (listRwo.get().size() > 0) {
                    SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.EDITAR, colecao.getCodigo(), "Abrindo mix da coleção para edição.");
                } else {
                    SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.CADASTRAR, colecao.getCodigo(), "Abrindo coleção para criação do mix.");
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    listaMixColecao.addAll(listRwo.get());
                    emEdicao.set(true);

                    fieldMarca.items.set(FXCollections.observableList(listaMixColecao.stream()
                            .map(mix -> mix.getId().getMarca())
                            .distinct()
                            .collect(Collectors.toList())));
                    if (fieldMarca.items.size() > 0)
                        fieldMarca.select(0);
                }
            });

        }
    }

    ////
    private void selecionarMarca(Marca marca) {
        if (marca != null) {
            limparCamposModelagem();
            fieldLinha.items.set(FXCollections.observableList(listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(marca.getCodigo()))
                    .map(mix -> mix.getId().getLinha())
                    .distinct()
                    .collect(Collectors.toList())));
            if (fieldLinha.items.size() > 0)
                fieldLinha.select(0);
        }
    }

    private Object adicionarMarca() {
        List<SdColecaoMarca001> marcasColecao = (List<SdColecaoMarca001>) new FluentDao().selectFrom(SdColecaoMarca001.class)
                .where(eb -> eb
                        .equal("id.colecao.codigo", fieldColecao.value.get().getCodigo()))
                .resultList();

        InputBox<Marca> iboxMarca = InputBox.build(Marca.class, boxInput -> {
            boxInput.message("Selecione a marca para cadastro!");
            boxInput.setDefaultButtonConfirm();
            boxInput.fieldObject.defaults.add(new DefaultFilter(
                    marcasColecao.stream().map(colMar -> colMar.getId().getMarca().getCodigo()).distinct().toArray(),
                    "codigo", PredicateType.IN));
            boxInput.showAndWait();
        });

        if (iboxMarca.value.get() != null) {
            if (fieldMarca.items.stream().noneMatch(item -> item.getCodigo().equals(iboxMarca.value.get().getCodigo()))) {
                fieldMarca.items.add(iboxMarca.value.get());
            }
            fieldMarca.select(iboxMarca.value.get());
        }
        return null;
    }

    ////
    private void selecionarLinha(Linha linha) {
        if (linha != null) {
            limparCamposModelagem();
            fieldGrupoModelagem.items.set(FXCollections.observableList(listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(linha.getCodigo()))
                    .map(mix -> mix.getId().getGrupoModelagem())
                    .distinct()
                    .collect(Collectors.toList())));
            if (fieldGrupoModelagem.items.size() > 0)
                fieldGrupoModelagem.select(0);
        }
    }

    private Object adicionarLinha() {
        InputBox<Linha> iboxLinha = InputBox.build(Linha.class, boxInput -> {
            boxInput.message("Selecione a linha do produto para cadastro!");
            boxInput.setDefaultButtonConfirm();
            boxInput.fieldObject.defaults.add(new DefaultFilter(Boolean.TRUE, "sdAtivo"));
            boxInput.fieldObject.defaults.add(new DefaultFilter(fieldMarca.value.get().getCodigo(), "sdMarca"));
            boxInput.showAndWait();
        });

        if (iboxLinha.value.get() != null) {
            if (fieldLinha.items.stream().noneMatch(item -> item.getCodigo().equals(iboxLinha.value.get().getCodigo()))) {
                fieldLinha.items.add(iboxLinha.value.get());
            }
            fieldLinha.select(iboxLinha.value.get());
        }
        return null;
    }

    ////
    private void selecionarModelagem(SdGrupoModelagem modelagem) {
        if (modelagem != null) {
            fieldIdMix.items.set(FXCollections.observableList(listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                            && mix.getId().getGrupoModelagem().getCodigo() == modelagem.getCodigo())
                    .map(mix -> mix.getId().getMix())
                    .distinct()
                    .collect(Collectors.toList())));
            listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                            && mix.getId().getGrupoModelagem().getCodigo() == modelagem.getCodigo())
                    .map(mix -> mix.getMostruario())
                    .distinct()
                    .findFirst()
                    .ifPresent(qtdeMostruario -> {
                        fieldQtdeMostruario.value.set(String.valueOf(qtdeMostruario));
                    });
            tblMixColecao.items.set(FXCollections.observableList(listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                            && mix.getId().getGrupoModelagem().getCodigo() == modelagem.getCodigo())
                    .collect(Collectors.toList())));
            if (fieldIdMix.items.size() > 0)
                fieldIdMix.select(0);
        }
    }

    private Object adicionarModelagem() {
        InputBox<SdGrupoModelagem> iboxModelagem = InputBox.build(SdGrupoModelagem.class, boxInput -> {
            boxInput.message("Selecione a modelagem do produto para cadastro!");
            boxInput.setDefaultButtonConfirm();
            boxInput.showAndWait();
        });

        if (iboxModelagem.value.get() != null) {
            if (fieldGrupoModelagem.items.stream().noneMatch(item -> item.getCodigo() == iboxModelagem.value.get().getCodigo())) {
                fieldGrupoModelagem.items.add(iboxModelagem.value.get());
            }
            fieldGrupoModelagem.select(iboxModelagem.value.get());
        }
        return null;
    }

    ////
    private void selecionarMix(Integer idMix) {
        limparCamposMix();
        if (idMix != null) {
            listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                            && mix.getId().getGrupoModelagem().getCodigo() == fieldGrupoModelagem.value.get().getCodigo()
                            && mix.getId().getMix() == idMix)
                    .findFirst()
                    .ifPresent(mix -> {
                        tblMixColecao.selectItem(mix);

                        fieldQtdeProdutos.value.set(String.valueOf(mix.getReferencias()));
                        fieldMeta.value.set(String.valueOf(mix.getMeta()));
                        fieldRangeValor.valueBegin.set(mix.getValorInicial().doubleValue());
                        fieldRangeValor.valueEnd.set(mix.getValorFinal().doubleValue());

                        fieldFamiliaMix.items.set(FXCollections.observableList(mix.getFamilias().stream()
                                .map(familia -> familia.getId().getFamilia())
                                .distinct()
                                .collect(Collectors.toList())));
                        tblMixProduto.items.set(FXCollections.observableList(mix.getFamilias().stream()
                                .collect(Collectors.toList())));
                        if (fieldFamiliaMix.items.size() > 0)
                            fieldFamiliaMix.select(0);
                    });
        }
    }

    private Object adicionarMix() {
        Integer idMix = Integer.parseInt(Globals.getProximoCodigo("SD_MIX_COLECAO", "MIX"));
        fieldIdMix.items.add(idMix);
        fieldIdMix.select(idMix);
        limparCamposMix();
        tblMixColecao.tableProperties().getSelectionModel().clearSelection();

        novoMix.set(true);
        return null;
    }

    ////---------------
    private void gravarMix() {
        try {
            fieldQtdeMostruario.validate();
            fieldQtdeMostruario.validateDecimal();
            fieldQtdeProdutos.validate();
            fieldQtdeProdutos.validateDecimal();
            fieldMeta.validate();
            fieldMeta.validateDecimal();
            if (!fieldRangeValor.validateExistPeriodo())
                throw new FormValidationException("O campo de intervalo de valores deve estar com os dois valores preenchidos.");
            if (!fieldRangeValor.validateEndGtBegin())
                throw new FormValidationException("O campo de início do intervalor de valores não pode ser maior que o de fim.");
        } catch (FormValidationException valid) {
            MessageBox.create(message -> {
                message.message(valid.getMessage());
                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                message.showAndWait();
            });
            return;
        }

        SdMixColecao mixColecao = listaMixColecao.stream()
                .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                        && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                        && mix.getId().getGrupoModelagem().getCodigo() == fieldGrupoModelagem.value.get().getCodigo()
                        && mix.getId().getMix() == fieldIdMix.value.get())
                .findFirst()
                .orElse(null);

        if (mixColecao == null) {
            mixColecao = new SdMixColecao(fieldColecao.value.get(), fieldMarca.value.get(), fieldLinha.value.get(), fieldGrupoModelagem.value.get(), fieldIdMix.value.get());
            listaMixColecao.add(mixColecao);
            tblMixColecao.items.add(mixColecao);
        }
        mixColecao.setMeta(fieldMeta.decimalValue().intValue());
        mixColecao.setReferencias(fieldQtdeProdutos.decimalValue().intValue());
        mixColecao.setMostruario(fieldQtdeMostruario.decimalValue().intValue());
        mixColecao.setValorInicial(new BigDecimal(fieldRangeValor.valueBegin.getValue()));
        mixColecao.setValorFinal(new BigDecimal(fieldRangeValor.valueEnd.getValue()));

        tblMixColecao.refresh();
        tblMixColecao.selectItem(mixColecao);
        novoMix.set(false);
    }

    private void limparCampos() {
        listaMixColecao.clear();
        fieldColecao.clear();
        limparCamposMarcaLinha();
        emEdicao.set(false);
    }

    private void limparCamposMarcaLinha() {
        fieldLinha.getComboBox().getSelectionModel().clearSelection();
        fieldLinha.clear();
        fieldMarca.getComboBox().getSelectionModel().clearSelection();
        fieldMarca.clear();
        limparCamposModelagem();
    }

    private void limparCamposModelagem() {
        fieldGrupoModelagem.getComboBox().getSelectionModel().clearSelection();
        fieldGrupoModelagem.clear();
        fieldQtdeMostruario.clear();
        fieldIdMix.getComboBox().getSelectionModel().clearSelection();
        fieldIdMix.clear();
        tblMixColecao.clear();
        limparCamposMix();
    }

    private void limparCamposMix() {
        fieldQtdeProdutos.clear();
        fieldMeta.clear();
        fieldRangeValor.clear();

        tblMixProduto.clear();
        fieldFamiliaMix.getComboBox().getSelectionModel().clearSelection();
        fieldFamiliaMix.clear();
        limparCamposFamilia();
    }

    private void excluirMix(SdMixColecao mix) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o mix?");
            message.showAndWait();
        }).value.get())) {
            deleteMixColecao(mix);
            listaMixColecao.remove(mix);
            tblMixColecao.items.remove(mix);

            tblMixColecao.refresh();
            MessageBox.create(message -> {
                message.message("Mix exclído com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void salvarMixColecao() {

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            listaMixColecao.forEach(this::saveMixColecao);
            SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.CADASTRAR, fieldColecao.value.get().getCodigo(), "Finalizado gravação do mix.");
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Mix salvo com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });

    }

    private void cancelarCadastro() {
        limparCampos();
    }

    //------------------ FAMILIA ----------------------
    private void selecionarFamiliaMix(Familia familia) {
        if (familia != null) {
            listaMixColecao.stream()
                    .filter(mix -> mix.getId().getMarca().getCodigo().equals(fieldMarca.value.get().getCodigo())
                            && mix.getId().getLinha().getCodigo().equals(fieldLinha.value.get().getCodigo())
                            && mix.getId().getGrupoModelagem().getCodigo() == fieldGrupoModelagem.value.get().getCodigo()
                            && mix.getId().getMix() == fieldIdMix.value.get())
                    .findFirst()
                    .ifPresent(mix -> {
                        mix.getFamilias().stream()
                                .filter(mixFam -> mixFam.getId().getFamilia().getCodigo().equals(familia.getCodigo()))
                                .findFirst()
                                .ifPresent(mixFam -> {
                                    fieldQtdeProdutosFamilia.value.set(String.valueOf(mixFam.getQtdeProdutos()));
                                    fieldMetaFamilia.value.set(String.valueOf(mixFam.getMeta()));
                                    fieldTempoMedio.value.set(String.valueOf(mixFam.getTempoMedio()));
                                });
                    });
        }
    }

    private Object adicionarFamiliaMix() {
        InputBox<Familia> iboxFamilia = InputBox.build(Familia.class, boxInput -> {
            boxInput.message("Selecione a família do produto para cadastro!");
            boxInput.setDefaultButtonConfirm();
            boxInput.showAndWait();
        });

        if (iboxFamilia.value.get() != null) {
            if (fieldFamiliaMix.items.stream().noneMatch(item -> item.getCodigo().equals(iboxFamilia.value.get().getCodigo()))) {
                fieldFamiliaMix.items.add(iboxFamilia.value.get());
            }
            fieldFamiliaMix.select(iboxFamilia.value.get());
        }

        limparCamposFamilia();
        novaFamilia.set(true);

        SdMixColecao mix = tblMixColecao.selectedItem();
        fieldQtdeProdutosFamilia.value.set(String.valueOf(mix.getReferencias() - mix.getFamilias().stream().mapToInt(fam -> fam.getQtdeProdutos()).sum()));
        fieldMetaFamilia.value.set(String.valueOf(mix.getMeta() - mix.getFamilias().stream().mapToInt(fam -> fam.getMeta()).sum()));

        return null;
    }

    private void limparCamposFamilia() {
        fieldQtdeProdutosFamilia.clear();
        fieldMetaFamilia.clear();
        fieldTempoMedio.clear();
    }

    private void gravarFamilia() {
        SdMixColecao mix = tblMixColecao.selectedItem();

        try {
            fieldQtdeProdutosFamilia.validate();
            fieldQtdeProdutosFamilia.validateDecimal();
            fieldMetaFamilia.validate();
            fieldMetaFamilia.validateDecimal();
            fieldTempoMedio.validate();
            fieldTempoMedio.validateDecimal();

            if (mix.getReferencias() - mix.getFamilias().stream().mapToInt(fam -> fam.getQtdeProdutos()).sum() - fieldQtdeProdutosFamilia.decimalValue().intValue() < 0)
                throw new FormValidationException("O total de referências das famílias está maior que a modelagem.");
            if (mix.getMeta() - mix.getFamilias().stream().mapToInt(fam -> fam.getMeta()).sum() - fieldMetaFamilia.decimalValue().intValue() < 0)
                throw new FormValidationException("O total das metas das famílias está maior que a modelagem.");

        } catch (FormValidationException valid) {
            MessageBox.create(message -> {
                message.message(valid.getMessage());
                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                message.showAndWait();
            });
            return;
        }

        SdMixProdutos mixFamilia = mix.getFamilias().stream()
                .filter(mixFam -> mixFam.getId().getFamilia().getCodigo().equals(fieldFamiliaMix.value.get().getCodigo()))
                .findFirst()
                .orElse(null);

        if (mixFamilia == null) {
            mixFamilia = new SdMixProdutos(mix, fieldFamiliaMix.value.get());
            mix.getFamilias().add(mixFamilia);
            tblMixProduto.items.add(mixFamilia);
        }
        mixFamilia.setMeta(fieldMetaFamilia.decimalValue().intValue());
        mixFamilia.setQtdeProdutos(fieldQtdeProdutosFamilia.decimalValue().intValue());
        mixFamilia.setTempoMedio(fieldTempoMedio.decimalValue());

        tblMixProduto.refresh();
        novaFamilia.set(false);
    }

    private void excluirMixFamilia(SdMixProdutos mix) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o mix da família?");
            message.showAndWait();
        }).value.get())) {
            deleteFamilia(mix);
            tblMixColecao.selectedItem().getFamilias().remove(mix);
            tblMixProduto.items.remove(mix);
            tblMixProduto.refresh();

            MessageBox.create(message -> {
                message.message("Mix de família excluído com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    ////---------------
    private void enviarParaFluxogama() {
        if (listaMixColecao.size() > 0) {
            List<RequestSendMix> senders = new ArrayList<>();
            for (SdMixColecao sdMixColecao : listaMixColecao) {
                if (!sdMixColecao.isEnviadoFluxogama())
                    if (sdMixColecao.getFamilias().size() > 0)
                        for (SdMixProdutos familia : sdMixColecao.getFamilias()) {
                            senders.add(new RequestSendMix(sdMixColecao.getId().getColecao().getCodigo(),
                                    familia.getQtdeProdutos(),
                                    sdMixColecao.getId().getMarca().getCodigo().toLowerCase(),
                                    sdMixColecao.getId().getLinha().getCodigo().toLowerCase(),
                                    String.valueOf(sdMixColecao.getId().getGrupoModelagem().getCodigo()),
                                    sdMixColecao.getMeta(),
                                    StringUtils.toMonetaryFormat(sdMixColecao.getValorInicial(), 2).concat("-").concat(StringUtils.toMonetaryFormat(sdMixColecao.getValorFinal(), 2)),
                                    familia.getId().getFamilia().getCodigo().toLowerCase(),
                                    String.valueOf(familia.getMeta())));
                        }
                    else
                        senders.add(new RequestSendMix(sdMixColecao.getId().getColecao().getCodigo(),
                                sdMixColecao.getReferencias(),
                                sdMixColecao.getId().getMarca().getCodigo().toLowerCase(),
                                sdMixColecao.getId().getLinha().getCodigo().toLowerCase(),
                                String.valueOf(sdMixColecao.getId().getGrupoModelagem().getCodigo()),
                                sdMixColecao.getMeta(),
                                StringUtils.toMonetaryFormat(sdMixColecao.getValorInicial(), 2).concat("-").concat(StringUtils.toMonetaryFormat(sdMixColecao.getValorFinal(), 2)),
                                "001",
                                String.valueOf(sdMixColecao.getMeta())));
            }

            if (senders.size() > 0) {
                try {
                    new ServicoFluxogama().sendMix(senders);
                    String mixEnviados = listaMixColecao.stream().filter(mix -> !mix.isEnviadoFluxogama()).map(mix -> mix.getId().getGrupoModelagem().toString()).distinct().collect(Collectors.joining(", "));
                    listaMixColecao.forEach(mix -> {
                        mix.sendMixFluxogama();
                        saveMixColecao(mix);
                    });

                    MessageBox.create(message -> {
                        message.message("Mix enviado com sucesso para o Fluxogama com as modelagens " + mixEnviados);
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } catch (HttpResponseException e) {
                    MessageBox.create(message -> {
                        message.message(e.getMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        } else {
            MessageBox.create(message -> {
                message.message("É necessário carregar uma coleção com cadastro de modelagens para envio.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
        }
    }

    private void copiarMix() {

        if (listaMixColecao.size() > 0) {

            InputBox<Colecao> novaColecao = InputBox.build(Colecao.class, boxInput -> {
                boxInput.message("Selecione a coleção para cópia.");
                boxInput.showAndWait();
            });

            if (novaColecao.value.get() != null) {
                Colecao colecaoMix = novaColecao.value.get();
                SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.CADASTRAR, colecaoMix.getCodigo(), "Criando cópia do mix da coleção " + listaMixColecao.stream().map(mix -> mix.getId().getColecao().getCodigo()).distinct().collect(Collectors.joining()));

                for (SdMixColecao sdMixColecao : listaMixColecao) {
                    SdMixColecao novoMixColecao = new SdMixColecao(colecaoMix, sdMixColecao.getId().getMarca(), sdMixColecao.getId().getLinha(), sdMixColecao.getId().getGrupoModelagem(), sdMixColecao.getId().getMix(),
                            sdMixColecao.getValorInicial(), sdMixColecao.getValorFinal(), sdMixColecao.getMeta(), sdMixColecao.getReferencias(), sdMixColecao.getMostruario());
                    for (SdMixProdutos familia : sdMixColecao.getFamilias()) {
                        novoMixColecao.getFamilias().add(new SdMixProdutos(novoMixColecao, familia.getId().getFamilia(), familia.getQtdeProdutos(), familia.getMeta(), familia.getTempoMedio()));
                    }
                    saveMixColecao(novoMixColecao);
                }


                SysLogger.addSysDelizLog("Cadastro de Mix", TipoAcao.CADASTRAR, colecaoMix.getCodigo(), "Finalizado cópia do mix!");
                MessageBox.create(message -> {
                    message.message("Mix copiado com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                fieldColecao.value.set(colecaoMix);
            }
        } else {
            MessageBox.create(message -> {
                message.message("É necessário carregar uma coleção com cadastro de modelagens para copiar.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
        }
    }

    ///-------------------------------------
    private void carregaLYProdutosFamilia() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Integer> valueLy = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(),
                    fieldFamiliaMix.value.get().getCodigo());
            valueLy.set(lastYear.stream().mapToInt(VSdLastYearMix::getQtdeProdutos).sum());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldQtdeProdutosFamilia.value.set(String.valueOf(valueLy.get()));
            }
        });
    }

    private void carregaLYMetaFamilia() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Integer> valueLy = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(),
                    fieldFamiliaMix.value.get().getCodigo());
            valueLy.set(lastYear.stream().mapToInt(VSdLastYearMix::getMeta).sum());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldMetaFamilia.value.set(String.valueOf(valueLy.get()));
            }
        });
    }

    private void carregaLYTempoMedio() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Double> valueLy = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(),
                    fieldFamiliaMix.value.get().getCodigo());
            valueLy.set(lastYear.stream().mapToDouble(ly -> ly.getTempoMedio().doubleValue()).sum());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldTempoMedio.value.set(StringUtils.toDecimalFormat(valueLy.get(), 4));
            }
        });
    }

    private void carregaLYMeta() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Integer> valueLy = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(), "");
            valueLy.set(lastYear.stream().mapToInt(VSdLastYearMix::getMeta).sum());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldMeta.value.set(String.valueOf(valueLy.get()));
            }
        });
    }

    private void carregaLYValores() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Double> valueLy1 = new AtomicReference<>();
        AtomicReference<Double> valueLy2 = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(), "");
            valueLy1.set(lastYear.stream().mapToDouble(ly -> ly.getValorMinimo().doubleValue()).min().getAsDouble());
            valueLy2.set(lastYear.stream().mapToDouble(ly -> ly.getValorMaximo().doubleValue()).max().getAsDouble());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldRangeValor.valueBegin.set(valueLy1.get());
                fieldRangeValor.valueEnd.set(valueLy2.get());
            }
        });
    }

    private void carregaLYProdutos() {
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<Integer> valueLy = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            List<VSdLastYearMix> lastYear = getLastYearMix(fieldMarca.value.get().getCodigo(),
                    fieldColecao.value.get().getCodigo(),
                    fieldLinha.value.get().getCodigo(),
                    fieldGrupoModelagem.value.get().getCodigo(), "");
            valueLy.set(lastYear.stream().mapToInt(VSdLastYearMix::getQtdeProdutos).sum());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldQtdeProdutos.value.set(String.valueOf(valueLy.get()));
            }
        });

    }
}
