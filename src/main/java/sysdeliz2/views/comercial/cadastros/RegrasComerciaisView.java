package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.cadastros.RegrasComerciaisController;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.comercial.SdIndicadorComercial;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.view.comercial.VSdMetasEntidade;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class RegrasComerciaisView extends RegrasComerciaisController {

    // <editor-fold defaultstate="collapsed" desc="view">
    private final VBox tabClientes = (VBox) super.tabs.getTabs().get(0).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tabs">
    private ClienteTab clienteTab = new ClienteTab();
    // </editor-fold>

    public RegrasComerciaisView() {
        super("Cadastro de Metas/Regras Comerciais", ImageUtils.getImage(ImageUtils.Icon.REGRAS_COMERCIAIS), new String[]{"Clientes"});
        initClientes();
    }

    private void initClientes() {
        tabClientes.getChildren().add(clienteTab);
        clienteTab.loadDados(null, null);
    }

    public class ClienteTab extends VBox {

        // <editor-fold defaultstate="collapsed" desc="local">
        private String valorGatilhoSelecionado = "";
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="components">
        private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
            field.title("Coleção");
            field.toUpper();
            field.width(150.0);
        });
        private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
            field.title("Marca");
            field.toUpper();
            field.width(90.0);
        });
        public final FormTableView<VSdMetasEntidade> tblRegrasCliente = FormTableView.create(VSdMetasEntidade.class, table -> {
            table.title("Regras/Metas");
            table.expanded();
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Tipo");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTipo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Tipo*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Indicador");
                        cln.width(120.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getIndicador().toView()));
                    }).build() /*Indicador*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Valor*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cód.");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getCodigo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cód.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Coleção");
                        cln.width(130.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao().getDescricao()));
                    }).build() /*Coleção*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cód.");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca().getCodigo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cód.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Marca");
                        cln.width(80.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca().getDescricao()));
                    }).build() /*Marca*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Job");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMetasEntidade, VSdMetasEntidade>, ObservableValue<VSdMetasEntidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getJob()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Job*/
            );
        });
        private final FormNavegation<VSdMetasEntidade> navegation = FormNavegation.create(nav -> {
            nav.withNavegation(tblRegrasCliente);
            nav.withActions(true, true, true);
            nav.btnAddRegister(evt -> novoCadastro());
            nav.btnDeleteRegister(evt -> excluirCadastro((VSdMetasEntidade) nav.selectedItem.get()));
            nav.btnEditRegister(evt -> editarCadastro((VSdMetasEntidade) nav.selectedItem.get()));
            nav.btnSave(evt -> salvarCadastro((VSdMetasEntidade) nav.selectedItem.get()));
            nav.btnCancel(evt -> cancelarCadastro());
            nav.selectedItem.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    carregaDados((VSdMetasEntidade) newValue);
                }
            });
        });
        private final FormFieldSingleFind<Colecao> fieldColecao = FormFieldSingleFind.create(Colecao.class, field -> {
            field.title("Coleção");
            field.toUpper();
            field.width(350.0);
            field.editable.bind(this.navegation.inEdition);
        });
        private final FormFieldSingleFind<Marca> fieldMarca = FormFieldSingleFind.create(Marca.class, field -> {
            field.title("Marca");
            field.toUpper();
            field.width(240.0);
            field.editable.bind(this.navegation.inEdition);
        });
        private final FormFieldSingleFind<SdIndicadorComercial> fieldIndiceComercial = FormFieldSingleFind.create(SdIndicadorComercial.class, field -> {
            field.title("Índice");
            field.width(260.0);
            field.toUpper();
            field.editable.bind(this.navegation.inEdition);
        });
        private final FormFieldText fieldValorIndiceComercial = FormFieldText.create(field -> {
            field.title("Valor");
            field.width(100.0);
            field.decimalField(2);
            field.toUpper();
            field.editable.bind(this.navegation.inEdition);
        });
        private final FormFieldComboBox<Globals.GatilhosIndicadores> fieldTipo = FormFieldComboBox.create(Globals.GatilhosIndicadores.class, field -> {
            field.title("Gatilho");
            field.width(100.0);
            field.editable.bind(this.navegation.inEdition);
            field.items.set(FXCollections.observableList(Globals.gatilhos));
        });
        private final FormBox fieldValorTipo = FormBox.create(lst -> {
            lst.horizontal();
            lst.expanded();
        });
        private final FormFieldText fieldJob = FormFieldText.create(field -> {
            field.title("Módulo");
            field.width(90.0);
            field.toUpper();
            field.editable.bind(this.navegation.inEdition);
        });
        private final FormFieldTextArea fieldAcao = FormFieldTextArea.create(field -> {
            field.title("Ação Gatilho");
            field.expanded();
            field.editable.bind(this.navegation.inEdition);
        });
        // </editor-fold>

        public ClienteTab() {
            VBox.setVgrow(this, Priority.ALWAYS);
            HBox.setHgrow(this, Priority.ALWAYS);
            getChildren().add(FormBoxPane.create(content -> {
                content.expanded();
                content.left(FormBox.create(container -> {
                    container.vertical();
                    container.expanded();
                    container.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(filterBox -> {
                            filterBox.horizontal();
                            filterBox.add(fields -> fields.addHorizontal(fieldFilterColecao.build(), fieldFilterMarca.build()));
                        }));
                        filter.find.setOnAction(evt -> {
                            loadDados(fieldFilterColecao.objectValues.get().stream().map(col -> col.getCodigo()).toArray(),
                                    fieldFilterMarca.objectValues.get().stream().map(mar -> mar.getCodigo()).toArray());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterMarca.clear();
                            fieldFilterColecao.clear();
                        });
                    }));
                    container.add(tblRegrasCliente.build());
                }));
                content.center(FormBox.create(container -> {
                    container.vertical();
                    container.expanded();
                    container.add(navegation);
                    container.add(fields -> fields.addHorizontal(fieldColecao.build(), fieldMarca.build()));
                    container.add(fields -> fields.addHorizontal(fieldIndiceComercial.build(), fieldValorIndiceComercial.build()));
                    container.add(fields -> fields.addHorizontal(fieldTipo.build(), FormBox.create(boxField -> {
                        boxField.title("Val. Gatilho");
                        boxField.horizontal();
                        boxField.expanded();
                        boxField.add(FormButton.create(btn -> {
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                            btn.addStyle("success");
                            btn.disable.bind(navegation.inEdition.not());
                            btn.setAction(evt -> adicionarValorGatilho());
                        }));
                        boxField.add(fieldValorTipo);
                    })));
                    container.add(fields -> fields.addHorizontal(fieldJob.build()));
                    container.add(fields -> {
                        fields.addHorizontal(fieldAcao.build());
                        fields.expanded();
                    });
                }));
            }));
        }

        public void loadDados(Object[] colecao, Object[] marca) {
            Object[] colecoes = colecao != null ? colecao : new Object[]{};
            Object[] marcas = marca != null ? marca : new Object[]{};
            AtomicReference<List<VSdMetasEntidade>> metasEntidadeBean = new AtomicReference<>(new ArrayList<>());
            new RunAsyncWithOverlay(getNode()).exec(task -> {
                metasEntidadeBean.set(getMetasEntidade(colecoes, marcas));
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    tblRegrasCliente.items.set(FXCollections.observableList(metasEntidadeBean.get()));
                }
            });
        }

        private void novoCadastro() {
            tblRegrasCliente.tableProperties().getSelectionModel().clearSelection();
            cancelarCadastro();
            navegation.inEdition.set(true);
        }

        private void excluirCadastro(VSdMetasEntidade itemSelecionado) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Deseja realmente excluir a regra selecionada?");
                    message.showAndWait();
                }).value.get())) {
                return;
            }

            AtomicReference<Exception> exRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(getNode()).exec(task -> {
                try {
                    excluirMeta(
                            itemSelecionado.getId().getCliente(),
                            itemSelecionado.getId().getColecao().getCodigo(),
                            itemSelecionado.getId().getIndicador().getCodigo(),
                            itemSelecionado.getId().getJob(),
                            itemSelecionado.getId().getMarca().getCodigo(),
                            itemSelecionado.getId().getTipo());
                } catch (SQLException e) {
                    e.printStackTrace();
                    exRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Registro excluído com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    loadDados(fieldFilterColecao.objectValues.get().stream().map(col -> col.getCodigo()).toArray(),
                            fieldFilterMarca.objectValues.get().stream().map(mar -> mar.getCodigo()).toArray());
                    tblRegrasCliente.refresh();
                    if (tblRegrasCliente.items.size() > 0)
                        tblRegrasCliente.selectItem(0);
                } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    ExceptionBox.build(message -> {
                        message.exception(exRwo.get());
                        message.showAndWait();
                    });
                }
            });
        }

        private void editarCadastro(VSdMetasEntidade itemSelecionado) {
            navegation.inEdition.set(true);
            carregaDados(itemSelecionado);
        }

        private void salvarCadastro(VSdMetasEntidade novoItem) {
            try {
                if (fieldColecao.value.get() == null)
                    throw new FormValidationException("Deve ser informado um valor para a coleção!");
                if (fieldMarca.value.get() == null)
                    throw new FormValidationException("Deve ser informado um valor para a marca!");
                if (fieldIndiceComercial.value.get() == null)
                    throw new FormValidationException("Deve ser informado o índice comercial para a meta!");
                if (fieldValorIndiceComercial.value.get() == null || fieldValorIndiceComercial.value.get().length() == 0)
                    throw new FormValidationException("Um valor para o índice deve ser informado!");
                if (valorGatilhoSelecionado == null || valorGatilhoSelecionado.length() == 0)
                    throw new FormValidationException("Devem ser selecionados 1 ou mais valores para o gatilho.");
                if (fieldJob.value.get() == null || fieldJob.value.get().length() == 0)
                    throw new FormValidationException("É necessário informar o módulo que será utilizado a regra.");
            } catch (FormValidationException ex) {
                MessageBox.create(message -> {
                    message.message(ex.getMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
                return;
            }

            AtomicReference<Exception> exRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(getNode()).exec(task -> {
                try {
                    salvarMetaEntidade(
                            fieldColecao.value.get().getCodigo(),
                            fieldMarca.value.get().getCodigo(),
                            fieldIndiceComercial.value.get().getCodigo(),
                            fieldValorIndiceComercial.value.get().replaceAll(",", "."),
                            fieldTipo.value.get().gatilho,
                            valorGatilhoSelecionado,
                            fieldJob.value.get(),
                            fieldAcao.value.get()
                    );
                } catch (SQLException ex) {
                    exRwo.set(ex);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Registro salvo com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    navegation.inEdition.set(false);
                    if (tblRegrasCliente.selectedItem() != null) {
                        JPAUtils.getEntityManager().refresh(tblRegrasCliente.selectedItem());
                    } else {
                        loadDados(fieldFilterColecao.objectValues.get().stream().map(col -> col.getCodigo()).toArray(),
                                fieldFilterMarca.objectValues.get().stream().map(mar -> mar.getCodigo()).toArray());
                    }
                    tblRegrasCliente.refresh();
                } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    ExceptionBox.build(exceptionBox -> {
                        exceptionBox.exception(exRwo.get());
                        exceptionBox.showAndWait();
                    });
                }
            });
        }

        private void cancelarCadastro() {
            navegation.inEdition.set(false);
            if (tblRegrasCliente.selectedItem() != null) {
                carregaDados(tblRegrasCliente.selectedItem());
            } else {
                fieldColecao.clear();
                fieldMarca.clear();
                fieldIndiceComercial.clear();
                fieldValorIndiceComercial.clear();
                fieldTipo.select(0);
                fieldJob.clear();
                fieldAcao.clear();
                valorGatilhoSelecionado = "";
                exibirValoresGatilho(valorGatilhoSelecionado);
            }

        }

        private void carregaDados(VSdMetasEntidade itemSelecionado) {
            if (itemSelecionado != null) {
                fieldColecao.value.set(itemSelecionado.getId().getColecao());
                fieldMarca.value.set(itemSelecionado.getId().getMarca());
                fieldIndiceComercial.value.set(itemSelecionado.getId().getIndicador());
                fieldValorIndiceComercial.value.set(StringUtils.toDecimalFormat(itemSelecionado.getValor()));
                fieldTipo.select(Globals.getGatilho(itemSelecionado.getId().getTipo()));
                fieldJob.value.set(itemSelecionado.getId().getJob());
                fieldAcao.value.set(itemSelecionado.getAcao());
                valorGatilhoSelecionado = itemSelecionado.getGatilhoTipo();
                exibirValoresGatilho(valorGatilhoSelecionado);
            }
        }

        private void exibirValoresGatilho(String valorGatilho) {
            fieldValorTipo.clear();
            Arrays.asList(valorGatilho.split(",")).forEach(valor -> {
                if (valor != null && valor.length() > 0) {
                    fieldValorTipo.add(boxItem -> {
                        boxItem.horizontal();
                        boxItem.border();
                        boxItem.alignment(Pos.CENTER);
                        boxItem.add(FormLabel.create(label -> {
                            label.value.set(valor);
                        }).build());
                        boxItem.add(FormButton.create(btn -> {
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.addStyle("danger").addStyle("xs");
                            btn.disable.bind(navegation.inEdition.not());
                            btn.setAction(evt -> {
                                valorGatilhoSelecionado = valorGatilhoSelecionado.replaceAll("(," + valor + "|" + valor + ",|" + valor + ")", "").replaceAll("", "");
                                exibirValoresGatilho(valorGatilhoSelecionado);
                            });
                        }));
                    });
                }
            });
        }

        private void adicionarValorGatilho() {
            try {
                Class tClass = Class.forName(fieldTipo.value.get().classeGatilho);
                InputBox valueGatilho = InputBox.build(tClass, boxInput -> {
                    boxInput.message("Entre com o valor:");
                    boxInput.showAndWait();
                });

                if (valueGatilho.value.get() != null) {
                    valorGatilhoSelecionado = valorGatilhoSelecionado.concat(valorGatilhoSelecionado.length() > 0 ? "," : "")
                            .concat(fieldTipo.value.get().classeGatilho.split("\\.").length > 1
                                    ? ((BasicModel) valueGatilho.value.get()).getCodigoFilter()
                                    : valueGatilho.value.get().toString());
                    exibirValoresGatilho(valorGatilhoSelecionado);
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }
}
