package sysdeliz2.views.comercial.cadastros;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.cadastros.TransfCarteiraRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosEntidadeMarca;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.Estados;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.PersistenceException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransfCarteiraRepresentanteView extends TransfCarteiraRepresentanteController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    protected final ListProperty<VSdDadosEntidadeMarca> clientesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    List<String> estados = Stream.of(Estados.values())
            .map(Estados::name)
            .collect(Collectors.toList());

    protected final List<VSdDadosEntidadeMarca> selecionados = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="FilterFields">

    private final FormFieldSingleFind<Marca> fieldMarcaFilter = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
        field.value.setValue(null);
        field.maxLength(2);
    });
    private final FormFieldSingleFind<Represen> fieldCodRepFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Código Representante");
        field.width(400);
//        field.mask(FormFieldSingleFind.Mask.INTEGER);
        field.maxLength(5);
    });
    private final FormFieldMultipleFind<Entidade> fieldCodCliFilter = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
    });
    private final FormFieldComboBox<String> fieldEstadoFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Estados");
        field.items(FXCollections.observableList(estados));
    });
    private final FormFieldMultipleFind<Cidade> fieldCidadeFilter = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.width(200);
        field.toUpper();
        field.codeReference.set("nomeCid");

    });
    private final FormFieldSingleFind<Represen> fieldSearchRep = FormFieldSingleFind.create(Represen.class, field -> {
        field.width(300);
        field.withoutTitle();
        field.maxLength(5);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdDadosEntidadeMarca> tblCliente = FormTableView.create(VSdDadosEntidadeMarca.class, table -> {
        table.expanded();
        table.selectColumn();
        table.title("Clientes");
        table.editable.set(true);
        table.items.bind(clientesBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Codcli");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));

                }).build(), /*Código Cliente*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Cliente");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));

                }).build(), /*Nome Rep*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Fantasia");
                    cln.width(280);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFantasia()));

                }).build(), /*Fantasia*/
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNomecid()));

                }).build(), /*Cidade*/
                FormTableColumn.create(cln -> {
                    cln.title("Estado");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodest()));

                }).build(), /*Estado*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Rep");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNomerep()));

                }).build(), /*Nome Rep*/
                FormTableColumn.create(cln -> {
                    cln.title("Código Rep");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep()));

                }).build(), /*Código Rep*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosEntidadeMarca, VSdDadosEntidadeMarca>, ObservableValue<VSdDadosEntidadeMarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));

                }).build() /*Marca*/
        );
    });

    // </editor-fold>

    public TransfCarteiraRepresentanteView() {
        super("Transf. Carteira Representante", ImageUtils.getImage(ImageUtils.Icon.REPRESENTANTE));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(filterBox -> {
                        filterBox.vertical();
                        filterBox.add(FormBox.create(box -> {
                            box.vertical();
                            box.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(fieldMarcaFilter.build(), fieldCodCliFilter.build(), fieldCidadeFilter.build());
                            }));
                            box.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(fieldCodRepFilter.build(), fieldEstadoFilter.build());
                                estados.add(0, "Nenhum");
                                fieldEstadoFilter.select(0);
                            }));
                        }));
                    }));
                    fieldCidadeFilter.objectValues.addListener((observable, oldValue, newValue) -> {
                        if (newValue != null)
                            if (newValue.size() == 1) {
                                fieldEstadoFilter.select(newValue.get(0).getCodEst().getId().getSiglaEst());
                            } else {
                                fieldEstadoFilter.select(0);
                            }
                    });

                    filter.find.disableProperty().bind(fieldMarcaFilter.value.isNull().or(fieldMarcaFilter.value.isEqualTo("")));

                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            filtrar(fieldCidadeFilter.objectValues.isEmpty() || fieldCidadeFilter.textValue.getValue().equals("") ? null : fieldCidadeFilter.objectValues.stream().map(Cidade::getCodCid).toArray(),
                                    fieldCodCliFilter.objectValues.isEmpty() || fieldCodCliFilter.textValue.getValue().equals("") ? null : fieldCodCliFilter.objectValues.stream().map(Entidade::getStringCodcli).toArray(),
                                    fieldEstadoFilter.value.getValue().equals("Nenhum") ? "" : fieldEstadoFilter.value.getValue(),
                                    fieldCodRepFilter.value.getValue() == null ? new Represen() : fieldCodRepFilter.value.getValue(),
                                    fieldMarcaFilter.value.getValue() == null ? "" : fieldMarcaFilter.value.getValue().getCodigo());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                clientesBean.set(FXCollections.observableList(super.clientes));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldEstadoFilter.select(0);
                        fieldCodCliFilter.clear();
                        fieldCidadeFilter.clear();
                        fieldMarcaFilter.clear();
                        fieldCodRepFilter.clear();
                        fieldSearchRep.clear();
                    });
                }));

            }));
            principal.add(tblCliente.build());
            principal.add(FormBox.create(buttonBox -> {
                buttonBox.horizontal();
                buttonBox.add(fieldSearchRep.build());

                buttonBox.add(FormButton.create(transfButton -> {
                    transfButton.addStyle("info");
                    transfButton.title("Transferir");
                    transfButton.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                    transfButton.setOnAction(event -> {
                        List<VSdDadosEntidadeMarca> selecionados = tblCliente.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                        Represen represenDestino = fieldSearchRep.value.getValue();

                        if (selecionados.size() > 0) {
                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Deseja transferir os clientes selecionados para o representate " + represenDestino.getNome() + " ?");
                                message.showAndWait();
                            }).value.get()) {
                                atualizarTabelas(fieldMarcaFilter.value.getValue(), selecionados, represenDestino);
                                tblCliente.refresh();
                                MessageBox.create(message -> {
                                    message.message("Clientes transferidos");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                            }
                        } else {
                            MessageBox.create(message -> {
                                message.message("Nenhum cliente selecionado, selecione ao menos um");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        }

                    });
                }).build());
            }));

        }));

    }

    private void atualizarTabelas(Marca marca, List<VSdDadosEntidadeMarca> selecionados, Represen represenDestino) {

        try {
            for (VSdDadosEntidadeMarca cliente : selecionados) {

                if (!cliente.getCodrep().equals(represenDestino.getCodRep())) {

                    if (Globals.getEmpresaLogada().getCodigo().equals("1000")) {

                        verificaRepOrigem(marca, cliente);

                        verificaRepDestino(marca, represenDestino, cliente);

                    } else {
                        new NativeDAO().runNativeQueryUpdate(String.format
                                ("update cli_com_001\n" +
                                                "   set codrep = '%s'\n" +
                                                " where codcli = '%s'\n" +
                                                "   and codrep = '%s'\n",
                                        represenDestino.getCodRep(), cliente.getCodcli(), cliente.getCodrep()));
                    }

                    new NativeDAO().runNativeQueryUpdate(String.format
                            ("update sd_marcas_entidade_001\n" +
                                            "   set codrep = '%s'\n" +
                                            " where codcli = '%s'\n" +
                                            "   and marca = '%s'",
                                    represenDestino.getCodRep(), cliente.getCodcli(), cliente.getCodmarca()));

                    clientes.remove(cliente);
                    SysLogger.addSysDelizLog("Transferência de Carteira de Representante", TipoAcao.EDITAR, cliente.getCodcli(),
                            String.format("Trasferência de Representante do Cliente: %s \n Representante antigo: %s - %s \n Representante novo: %s - %s",
                                    cliente.getCodcli(), cliente.getCodrep(), cliente.getNomerep(), represenDestino.getCodRep(), represenDestino.getNome()));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void verificaRepDestino(Marca marca, Represen represenDestino, VSdDadosEntidadeMarca cliente) {
        CliCom origem = new FluentDao()
                .selectFrom(CliCom.class).where(it -> it
                        .equal("id.codcli", cliente.getCodcli())
                        .equal("id.codrep", cliente.getCodrep())
                        .equal("marca", marca.getCodigo())).singleResult();

        CliCom dest = new FluentDao()
                .selectFrom(CliCom.class).where(it -> it
                        .equal("id.codcli", cliente.getCodcli())
                        .equal("id.codrep", represenDestino.getCodRep())
                        .equal("marca", marca.getCodigo().equals("D") ? "F" : "D")).singleResult();

        try {
            if (origem != null && dest == null) {
                CliCom outroRep = new FluentDao()
                        .selectFrom(CliCom.class).where(it -> it
                                .equal("id.codcli", cliente.getCodcli())
                                .equal("id.codrep", represenDestino.getCodRep())
                                .and( eb2 -> eb2
                                        .equal("marca", marca.getCodigo(), TipoExpressao.OR, true)
                                        .equal("marca", "1", TipoExpressao.OR, true))
                        ).singleResult();
                if (outroRep != null) {
                    new FluentDao().delete(origem);
                } else {
                    new NativeDAO().runNativeQueryUpdate(String.format(
                            "update cli_com_001\n" +
                                    "   set codrep = '%s'\n" +
                                    " where codcli = '%s'\n" +
                                    "   and codrep = '%s'\n" +
                                    "   and marca = '%s'\n",
                            represenDestino.getCodRep(), cliente.getCodcli(), cliente.getCodrep(), marca.getCodigo()));
                }
            } else if (origem == null && dest == null) {
                CliCom outroRep = new FluentDao()
                        .selectFrom(CliCom.class).where(it -> it
                                .equal("id.codcli", cliente.getCodcli())
                                .equal("id.codrep", represenDestino.getCodRep())
                                .and( eb2 -> eb2
                                        .equal("marca", marca.getCodigo(), TipoExpressao.OR, true)
                                        .equal("marca", "1", TipoExpressao.OR, true))
                        ).singleResult();
                if (outroRep == null) {
                    CliCom newClicom = new CliCom(cliente.getCodcli(), represenDestino.getCodRep(), marca.getCodigo(), BigDecimal.ZERO, BigDecimal.ZERO);
                    new FluentDao().persist(newClicom);
                }
            } else if (origem == null) {
                new NativeDAO().runNativeQueryUpdate(String.format
                        ("update cli_com_001\n" +
                                        "   set marca = 1\n" +
                                        " where codcli = '%s'\n" +
                                        "   and codrep = '%s'\n" +
                                        "   and marca = '%s'\n",
                                cliente.getCodcli(), represenDestino.getCodRep(), marca.getCodigo().equals("D") ? "F" : "D"));
            } else {

                new NativeDAO().runNativeQueryUpdate(String.format
                        ("delete from cli_com_001\n" +
                                        " where codcli = '%s'\n" +
                                        "   and codrep = '%s'\n" +
                                        "   and marca = '%s'\n",
                                cliente.getCodcli(), cliente.getCodrep(), marca.getCodigo()));

                new NativeDAO().runNativeQueryUpdate(String.format
                        ("update cli_com_001\n" +
                                        "   set marca = 1\n" +
                                        " where codcli = '%s'\n" +
                                        "   and codrep = '%s'\n" +
                                        "   and marca = '%s'\n",
                                cliente.getCodcli(), represenDestino.getCodRep(), marca.getCodigo().equals("D") ? "F" : "D"));
            }
        } catch (SQLException | PersistenceException e) {
            e.printStackTrace();
        }

    }

    private void verificaRepOrigem(Marca marca, VSdDadosEntidadeMarca cliente) {
        CliCom entidadeOrigem = new FluentDao()
                .selectFrom(CliCom.class).where(it -> it
                        .equal("id.codcli", cliente.getCodcli())
                        .equal("id.codrep", cliente.getCodrep())).singleResult();

        if (entidadeOrigem != null && entidadeOrigem.getMarca().equals("1")) {
            entidadeOrigem.setMarca(marca.getCodigo().equals("D") ? "F" : "D");
            new FluentDao().merge(entidadeOrigem);
        }
    }

}
