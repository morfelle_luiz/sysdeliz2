package sysdeliz2.views.comercial.cid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.http.client.HttpResponseException;
import sysdeliz2.controllers.views.comercial.cid.CatalogoDigitalController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.comercial.*;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdLinhaCatalogo;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.apis.nitroecom.ServicoNitroEcom;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.dialog.FileChoice;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class CatalogoDigitalView extends CatalogoDigitalController {

    // <editor-fold defaultstate="collapsed" desc="beans">
    private final ListProperty<SdCatalogo> catalogosBean = new SimpleListProperty<>();
    private final ListProperty<SdLookbookCatalogo> lookbooksBean = new SimpleListProperty<>();
    private final ListProperty<SdLookbookCatalogo> lookbooksEdicaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdProdutosLookbook> produtosLookbookBean = new SimpleListProperty<>();
    private final ListProperty<SdConceitoCatalogo> conceitosBean = new SimpleListProperty<>();
    private final ListProperty<SdProdutosConceito> produtosConceitoBean = new SimpleListProperty<>();
    private final BooleanProperty emEdicao = new SimpleBooleanProperty();
    private SdCatalogo catalogoEmEdicao = new SdCatalogo();
    private final String baseLocalPath = "K:\\Loja Virtual\\imagens\\catalogo";
    private final String baseFtpPath = "https://imagens.deliz.com.br/catalogo";
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components">
    // containers
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabManutencao = (VBox) super.tabs.getTabs().get(1).getContent();
    // fields filter
    private final FormFieldText fieldFilterAno = FormFieldText.create(field -> {
        field.title("Ano");
        field.width(65.0);
        field.mask(FormFieldText.Mask.INTEGER);
    });
    private final FormFieldText fieldFilterTitulo = FormFieldText.create(field -> {
        field.title("Título");
        field.width(170.0);
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(80.0);
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldFilterEnvio = FormFieldSegmentedButton.create(field -> {
        field.title("Envio");
        field.options(
                field.option("Todos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Enviado", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Pedente", "N", FormFieldSegmentedButton.Style.WARNING)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> fieldFilterStatus = FormFieldSegmentedButton.create(field -> {
        field.title("Status");
        field.options(
                field.option("Todos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Ativo", "1", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Inativo", "0", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // tables listagem
    private final FormTableView<SdCatalogo> tblCatalogos = FormTableView.create(SdCatalogo.class, table -> {
        table.title("Catálogos");
        table.expanded();
        table.items.bind(catalogosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Capa");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdCatalogo, SdCatalogo>() {
                            @Override
                            protected void updateItem(SdCatalogo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && item.getImagemLocal() != null && !empty) {
                                    try {
                                        ImageView imag = new ImageView(new Image(new FileInputStream(item.getImagemLocal())));
                                        imag.setPreserveRatio(true);
                                        imag.setFitHeight(100.0);
                                        setGraphic(imag);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                        setText(item.getImagem());
                                    }
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Capa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Título");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build() /*Título*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ano");
                    cln.width(45.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAno()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ano*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(45.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.format(param -> {
                        return new TableCell<SdCatalogo, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(ImageUtils.getIcon(item ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO,
                                            ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Status*/,
                FormTableColumn.create(cln -> {
                    cln.title("Enviado");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().isStatusEnvio()));
                    cln.format(param -> {
                        return new TableCell<SdCatalogo, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Envio*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdCatalogo, SdCatalogo>() {
                            final VBox boxButtonsRow = new VBox(3);
                            final Button btnVerCatalogo = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Abrir Catálogo");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditarCatalogo = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Catálogo");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluirCatalogo = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Catálogo");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });
                            final Button btnEnviarCatalogo = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                btn.tooltip("Enviar Catálogo");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                            });
                            final Button btnImprimirCatalogo = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                btn.tooltip("Gerar PDF");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });

                            @Override
                            protected void updateItem(SdCatalogo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVerCatalogo.setOnAction(evt -> {
                                        abrirCatalogo(item);
                                    });
                                    btnEditarCatalogo.setOnAction(evt -> {
                                        editarCatalogo(item);
                                    });
                                    btnExcluirCatalogo.setOnAction(evt -> {
                                        excluirCatalogo(item);
                                        catalogosBean.remove(item);
                                    });
                                    btnEnviarCatalogo.setOnAction(evt -> {
                                        enviarCatalogo(item);
                                    });
                                    btnImprimirCatalogo.setOnAction(evt -> {
                                        imprimirCatalogo(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVerCatalogo, btnEditarCatalogo, btnExcluirCatalogo, btnEnviarCatalogo, btnImprimirCatalogo);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selecionarCatalogo((SdCatalogo) newValue);
            }
        });
    });
    private final FormTableView<SdLookbookCatalogo> tblLookbook = FormTableView.create(SdLookbookCatalogo.class, table -> {
        table.title("Lookbooks");
        table.expanded();
        table.items.bind(lookbooksBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ordem");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ordem*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Título");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo()));
                }).build() /*Título*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null)
                selecionarLookbook((SdLookbookCatalogo) newValue);
        });
    });
    private final FormTableView<SdProdutosLookbook> tblProdutosLookbook = FormTableView.create(SdProdutosLookbook.class, table -> {
        table.title("Produtos Lookbook");
        table.items.bind(produtosLookbookBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(240.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosLookbook, SdProdutosLookbook>, ObservableValue<SdProdutosLookbook>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                }).build() /*Produto*/
        );
    });
    private final FormBox boxImagensLookbook = FormBox.create(formBox -> {
        formBox.title("Imagens Lookbook");
        formBox.flutuante();
        formBox.verticalScroll();
        formBox.expanded();
    });
    private final FormBox boxImagemConceito = FormBox.create(formBox -> {
        formBox.title("Imagem Conceito");
        formBox.vertical();
        formBox.expanded();
    });
    private final FormTableView<SdConceitoCatalogo> tblConceitos = FormTableView.create(SdConceitoCatalogo.class, table -> {
        table.title("Conceitos");
        table.items.bind(conceitosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ordem");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdConceitoCatalogo, SdConceitoCatalogo>, ObservableValue<SdConceitoCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ordem*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdConceitoCatalogo, SdConceitoCatalogo>, ObservableValue<SdConceitoCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Título");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdConceitoCatalogo, SdConceitoCatalogo>, ObservableValue<SdConceitoCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo()));
                }).build() /*Título*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdConceitoCatalogo, SdConceitoCatalogo>, ObservableValue<SdConceitoCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinhaJson()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Linha*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdConceitoCatalogo, SdConceitoCatalogo>, ObservableValue<SdConceitoCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdConceitoCatalogo, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(ImageUtils.getIcon(item ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO,
                                            ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Status*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null)
                selecionarConceito((SdConceitoCatalogo) newValue);
        });
    });
    private final FormTableView<SdProdutosConceito> tblProdutosConceito = FormTableView.create(SdProdutosConceito.class, table -> {
        table.title("Produtos Conceito");
        table.items.bind(produtosConceitoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(240.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosConceito, SdProdutosConceito>, ObservableValue<SdProdutosConceito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                }).build() /*Produto*/
        );
    });
    // fields catalogo
    private final FormFieldText fieldCodigo = FormFieldText.create(field -> {
        field.title("Código");
        field.editable(false);
        field.width(60);
        field.addStyle("warning");
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTitulo = FormFieldText.create(field -> {
        field.title("Título");
        field.width(250);
        field.editable.bind(emEdicao);
    });
    private final FormFieldTextArea fieldDescricao = FormFieldTextArea.create(field -> {
        field.title("Descrição");
        field.editable.bind(emEdicao);
        field.height(150);
    });
    private final FormFieldSingleFind<Marca> fieldMarca = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.editable.bind(emEdicao);
        field.width(240);
    });
    private final FormFieldSingleFind<Colecao> fieldColecao = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.editable.bind(emEdicao);
        field.width(280);
        field.postSelected((observable, oldValue, newValue) -> {
            if (newValue != null)
                sugestaoPeriodoNovidade();
        });
    });
    private final FormFieldText fieldAno = FormFieldText.create(field -> {
        field.title("Ano");
        field.editable.bind(emEdicao);
        field.width(70);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldImagem = FormFieldText.create(field -> {
        field.title("Imagem");
        field.editable(false);
        field.width(350);
        field.tooltip("Clique 2x para abrir a imagem.\nPressione CTRL + DELETE para excluir a imagem.");
        field.mouseClicked(evt -> {
            if (evt.getClickCount() == 2) {
                abrirImagemCatalogo();
            }
        });
        field.keyReleased(evt -> {
            if (emEdicao.get() && evt.isControlDown() && evt.getCode().equals(KeyCode.DELETE))
                field.clear();
        });
    });
    private final FormFieldText fieldVideo = FormFieldText.create(field -> {
        field.title("Vídeo");
        field.editable(false);
        field.width(280);
        field.tooltip("Clique 2x para abrir o vídeo.\nPressione CTRL + DELETE para excluir o vídeo.");
        field.mouseClicked(evt -> {
            if (evt.getClickCount() == 2) {
                abrirVideo(field.value.get());
            }
        });
        field.keyReleased(evt -> {
            if (emEdicao.get() && evt.isControlDown() && evt.getCode().equals(KeyCode.DELETE) && emEdicao.get())
                field.clear();
        });
    });
    private final FormFieldDatePeriod fieldPeriodoNovidade = FormFieldDatePeriod.create(field -> {
        field.title("Novidade");
        field.editable.bind(emEdicao);
    });
    private final FormFieldToggle fieldStatus = FormFieldToggle.create(field -> {
        field.title("Status");
        field.value.set(true);
        field.editable.bind(emEdicao);
    });
    private final FormFieldToggle fieldDestaque = FormFieldToggle.create(field -> {
        field.title("Destaque");
        field.value.set(false);
        field.editable.bind(emEdicao);
    });
    // view manutencao
    private final FormTabPane panelConceitos = FormTabPane.create(pane -> {
        pane.expanded();
    });
    private final FormTableView<SdLookbookCatalogo> tblLookbooks = FormTableView.create(SdLookbookCatalogo.class, table -> {
        table.withoutHeader();
        table.expanded();
        table.items.bind(lookbooksEdicaoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("#");
                    cln.width(40);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText("LB-" + item);
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*#*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pincipal");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, SdLookbookCatalogo>() {
                            @Override
                            protected void updateItem(SdLookbookCatalogo item, boolean empty) {
                                super.updateItem(item, empty);
                                setGraphic(null);
                                if (item != null && item.getImagens().size() > 0 && !empty) {
                                    setGraphic(ImageUtils.getImage(item.getImagens().get(0).getImagemLocal(), 70.0, ImageUtils.FitType.WIDTH));
                                }
                            }
                        };
                    });
                }).build() /*Pincipal*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Título");
                    cln.width(250.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo()));
                }).build() /*Título*/,
                FormTableColumn.create(cln -> {
                    cln.title("Vídeo");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVideo()));
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormButton.create(btn -> {
                                        btn.title("Abrir");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, 20.0));
                                        btn.addStyle("primary");
                                        btn.setAction(evt -> {
                                            abrirVideo(item);
                                        });
                                    }));
                                }
                            }
                        };
                    });
                }).build() /*Vídeo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dest.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDestaque()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Destaque*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Ativo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinha()));
                }).build() /*Linha*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pins");
                    cln.width(55.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, SdLookbookCatalogo>() {
                            @Override
                            protected void updateItem(SdLookbookCatalogo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getProdutos().size() + " Pin(s)");
                                }
                            }
                        };
                    });
                }).build() /*Pins*/,
                FormTableColumn.create(cln -> {
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLookbookCatalogo, SdLookbookCatalogo>, ObservableValue<SdLookbookCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdLookbookCatalogo, SdLookbookCatalogo>() {
                            final VBox boxButtonsRow = new VBox(3);
                            final Button btnAbrirLookbook = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Abrir Lookbook");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });
                            final Button btnExcluirLookbook = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Lookbook");
                                btn.disable.bind(emEdicao.not());
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdLookbookCatalogo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnAbrirLookbook.setOnAction(evt -> {
                                        abrirLookbook(item);
                                    });
                                    btnExcluirLookbook.setOnAction(evt -> {
                                        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Deseja realmente excluir o lookbook?");
                                            message.showAndWait();
                                        }).value.get()) {
                                            excluirLookbook(item);
                                            table.refresh();
                                        }
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAbrirLookbook, btnExcluirLookbook);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    public CatalogoDigitalView() {
        super("Catálogo Digital", ImageUtils.getImage(ImageUtils.Icon.CATALOGO), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }

    // <editor-fold defaultstate="collapsed" desc="LISTAGEM">
    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(leftContainer -> {
                leftContainer.vertical();
                leftContainer.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(fieldsFilter -> {
                            fieldsFilter.horizontal();
                            fieldsFilter.add(FormBox.create(box1 -> {
                                box1.vertical();
                                box1.add(fieldFilterTitulo.build());
                                box1.add(FormBox.create(box2 -> {
                                    box2.horizontal();
                                    box2.add(fieldFilterAno.build(), fieldFilterMarca.build());
                                }));
                            }));
                            fieldsFilter.add(FormBox.create(box1 -> {
                                box1.vertical();
                                box1.add(fieldFilterEnvio.build());
                                box1.add(fieldFilterStatus.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            findCatalogos(fieldFilterTitulo.value.get(), fieldFilterAno.value.get(), fieldFilterMarca.objectValues.get(), fieldFilterEnvio.value.get(), fieldFilterStatus.value.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterAno.clear();
                            fieldFilterTitulo.clear();
                            fieldFilterMarca.clear();
                            fieldFilterEnvio.select(0);
                            fieldFilterStatus.select(0);
                        });
                    }));
                }));
                leftContainer.add(tblCatalogos.build());
            }));
            content.add(FormBox.create(rightContainer -> {
                rightContainer.vertical();
                rightContainer.expanded();
                rightContainer.add(FormBox.create(boxLookbooks -> {
                    boxLookbooks.horizontal();
                    boxLookbooks.expanded();
                    boxLookbooks.title("Lookbooks");
                    boxLookbooks.add(FormBox.create(boxTables -> {
                        boxTables.vertical();
                        boxTables.add(tblLookbook.build());
                        boxTables.add(tblProdutosLookbook.build());
                    }));
                    boxLookbooks.add(boxImagensLookbook);
                }));
                rightContainer.add(FormBox.create(boxConceito -> {
                    boxConceito.horizontal();
                    boxConceito.expanded();
                    boxConceito.add(FormBox.create(boxConceitos -> {
                        boxConceitos.vertical();
                        boxConceitos.title("Conceitos");
                        boxConceitos.add(tblConceitos.build());
                        boxConceitos.add(tblProdutosConceito.build());
                    }));
                    boxConceito.add(boxImagemConceito);
                }));
            }));
        }));
    }

    private void findCatalogos(String titulo, String ano, ObservableList<Marca> marcas, String statusEnvio, String statusServidor) {
        new RunAsyncWithOverlay(this).exec(task -> {
            findCatalogos(titulo, ano, marcas.stream().map(Marca::getCodigo).toArray(), statusEnvio, statusServidor);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                catalogosBean.clear();
                lookbooksBean.clear();
                produtosLookbookBean.clear();
                boxImagensLookbook.clear();
                conceitosBean.clear();
                produtosConceitoBean.clear();

                catalogosBean.set(FXCollections.observableList(catalogos));
            }
        });
    }

    private void selecionarCatalogo(SdCatalogo catalogo) {
        lookbooksBean.set(FXCollections.observableList(catalogo.getLookbooks()));
        lookbooksBean.sort(Comparator.comparing(look -> Integer.parseInt(look.getOrdem())));
        conceitosBean.set(FXCollections.observableList(catalogo.getConceitos()));
        conceitosBean.sort(Comparator.comparing(conct -> Integer.parseInt(conct.getOrdem())));

        boxImagemConceito.clear();
        tblConceitos.refresh();
        tblConceitos.tableProperties().getSelectionModel().selectFirst();
        boxImagensLookbook.clear();
        tblLookbook.refresh();
        tblLookbook.tableProperties().getSelectionModel().selectFirst();
    }

    private void selecionarLookbook(SdLookbookCatalogo lookbook) {
        produtosLookbookBean.set(FXCollections.observableList(lookbook.getProdutos()));
        tblProdutosLookbook.refresh();

        boxImagensLookbook.clear();
        lookbook.getImagens().sort((o1, o2) -> o1.getOrdem().compareTo(o2.getOrdem()));
        lookbook.getImagens().forEach(imagem -> {
            try {
                ImageView imag = new ImageView(new Image(new FileInputStream(imagem.getImagemLocal())));
                imag.setPreserveRatio(true);
                imag.setFitHeight(250.0);
                boxImagensLookbook.add(imag);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                boxImagensLookbook.add(FormLabel.create(lb -> lb.value.set(imagem.getImagem())));
            }
        });
    }

    private void selecionarConceito(SdConceitoCatalogo conceito) {
        boxImagemConceito.clear();
        produtosConceitoBean.set(FXCollections.observableList(conceito.getProdutos()));
        tblProdutosConceito.refresh();
        try {
            ImageView imag = new ImageView(new Image(new FileInputStream(conceito.getImagemLocal())));
            imag.setPreserveRatio(true);
            imag.setFitHeight(400.0);
            boxImagemConceito.clear();
            boxImagemConceito.add(imag);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            boxImagemConceito.add(FormLabel.create(lb -> lb.value.set(conceito.getImagem())));
        }

    }

    private void editarCatalogo(SdCatalogo catalogo) {
        abrirCatalogo(catalogo);
        emEdicao.set(true);
//        SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EDITAR, String.valueOf(catalogo.getCodigo()), "Abrindo o catálo no modo edição.");
    }

    private void abrirCatalogo(SdCatalogo catalogo) {
        unbind(catalogoEmEdicao);
        panelConceitos.getTabs().clear();
        lookbooksEdicaoBean.clear();
        catalogoEmEdicao = catalogo;
        bind(catalogoEmEdicao);

        catalogoEmEdicao.getLookbooks().forEach(JPAUtils::refresh);
        catalogoEmEdicao.getLookbooks().sort(Comparator.comparing(look -> Integer.parseInt(look.getOrdem())));
        lookbooksEdicaoBean.addAll(catalogoEmEdicao.getLookbooks());
        catalogoEmEdicao.getConceitos().sort(Comparator.comparing(conct -> Integer.parseInt(conct.getOrdem())));
        for (SdConceitoCatalogo conceito : catalogoEmEdicao.getConceitos()) {
            adicionarTabConceito(conceito);
        }

        tabs.getSelectionModel().select(1);
    }

    private void limparSelecaoCatalogo() {
        lookbooksBean.clear();
        conceitosBean.clear();
        produtosLookbookBean.clear();
        boxImagensLookbook.clear();
        produtosConceitoBean.clear();
        boxImagemConceito.clear();
    }

    private void imprimirCatalogo(SdCatalogo catalogo) {

        String catalogoPath = "";
        catalogoPath = FileChoice.show(box -> {
            box.addExtension(new FileChooser.ExtensionFilter("Arquivos PDF", "*.pdf"));
        }).stringSavePath();
        if (catalogoPath == null || catalogoPath.length() == 0)
            return;

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        String finalCatalogoPath = catalogoPath;
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                new ReportUtils().config().addReport(ReportUtils.ReportFile.CATALOGO_DIGITAL,
                        new ReportUtils.ParameterReport[]{
                                new ReportUtils.ParameterReport("p_catalogo", catalogo.getCodigo())
                        }).view().toPdf(finalCatalogoPath);
            } catch (IOException | JRException | SQLException e) {
                exRwo.set(e);
                return ReturnAsync.EXCEPTION.value;
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Geração do catálogo finalizada!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                exRwo.get().printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(exRwo.get());
                    message.showAndWait();
                });
            }
        });
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="MANUTENÇÃO">
    private void initManutencao() {
        tabManutencao.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(leftContainer -> {
                leftContainer.vertical();
                leftContainer.add(FormBox.create(boxCatalogo -> {
                    boxCatalogo.horizontal();
                    boxCatalogo.title("Catálogo");
                    boxCatalogo.add(FormBox.create(boxFields -> {
                        boxFields.vertical();
                        boxFields.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                btn.addStyle("success").addStyle("lg");
                                btn.disable.bind(emEdicao);
                                btn.setAction(evt -> adicionarNovoCatalogo());
                            }));
                            box1.add(fieldCodigo.build(), fieldMarca.build(), fieldDestaque.build(), fieldStatus.build());
                        }));
                        boxFields.add(fieldTitulo.build());
                        boxFields.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(fieldColecao.build(), fieldPeriodoNovidade.build());
                        }));
                        boxFields.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.alignment(Pos.BOTTOM_LEFT);
                            box1.add(fieldAno.build(), fieldVideo.build(), FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                btn.addStyle("success");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarVideo(fieldVideo));
                            }));
                        }));
                    }));
                    boxCatalogo.add(FormBox.create(boxFields -> {
                        boxFields.vertical();
                        boxFields.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.alignment(Pos.BOTTOM_LEFT);
                            box1.add(fieldImagem.build(), FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._24));
                                btn.addStyle("info");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarImagemCatalogo());
                            }));
                        }));
                        boxFields.add(fieldDescricao.build());
                    }));
                }));
                leftContainer.add(FormBox.create(boxConceito -> {
                    boxConceito.vertical();
                    boxConceito.expanded();
                    boxConceito.title("Conceitos Catálogo");
                    boxConceito.add(FormButton.create(btn -> {
                        btn.title("Adicionar Conceito");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                        btn.addStyle("success");
                        btn.disable.bind(emEdicao.not());
                        btn.setAction(evt -> adicionarNovoConceito());
                    }));
                    boxConceito.add(panelConceitos);
                }));
            }));
            content.add(FormBox.create(rightContainer -> {
                rightContainer.vertical();
                rightContainer.expanded();
                rightContainer.add(container -> {
                    container.vertical();
                    container.expanded();
                    container.title("Lookbooks Catálogo");
                    container.add(FormButton.create(btn -> {
                        btn.title("Adicionar Lookbook");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                        btn.addStyle("success");
                        btn.disable.bind(emEdicao.not());
                        btn.setAction(evt -> adicionarNovoLookbook());
                    }));
                    container.add(tblLookbooks.build());
                });
                rightContainer.add(toolbarFooter -> {
                    toolbarFooter.horizontal();
                    toolbarFooter.alignment(Pos.BOTTOM_RIGHT);
                    toolbarFooter.addBtn(btn -> {
                        btn.title("Excluir Catálogo");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                        btn.disable.bind(emEdicao.not());
                        btn.addStyle("warning");
                        btn.setAction(evt -> excluirCatalogo(catalogoEmEdicao));
                    });
                    toolbarFooter.addBtn(btn -> {
                        btn.title("Salvar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
                        btn.disable.bind(emEdicao.not());
                        btn.addStyle("success");
                        btn.setAction(evt -> {
                            if (salvarCatalogo(catalogoEmEdicao))
                                emEdicao.set(false);
                        });
                    });
                    toolbarFooter.addBtn(btn -> {
                        btn.title("Salvar e Publicar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                        btn.disable.bind(emEdicao.not());
                        btn.addStyle("primary");
                        btn.setAction(evt -> salvarEPublicarCatalogo(catalogoEmEdicao));
                    });
                    toolbarFooter.addBtn(btn -> {
                        btn.title("Cancelar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btn.disable.bind(emEdicao.not());
                        btn.addStyle("danger");
                        btn.setAction(evt -> cancelarCatalogo());
                    });
                });
            }));
        }));
    }

    private void salvarEPublicarCatalogo(SdCatalogo catalogo) {
        if (!salvarCatalogo(catalogo))
            return;

        enviarCatalogo(catalogo);
    }

    private void enviarCatalogo(SdCatalogo catalogo) {
        // <editor-fold defaultstate="collapsed" desc="validações">
        try {
            // <editor-fold defaultstate="collapsed" desc="catálogo">
            if (catalogo.getMarca() == null)
                throw new FormValidationException("Marca: É necessário informar a marca do catálogo.");
            if (catalogo.getNome() == null)
                throw new FormValidationException("Título: É necessário informar o título do catálogo.");
            if (catalogo.getColecao() == null)
                throw new FormValidationException("Coleção: É necessário informar a coleção do catálogo.");
            if (catalogo.getAno() == null)
                throw new FormValidationException("Ano: É necessário informar o ano do catálogo.");
            if (catalogo.getImagem() == null)
                throw new FormValidationException("Imagem: É necessário informar a imagem de capa do catálogo.");
//            if (catalogo.getDescricao() == null || catalogo.getDescricao().length() == 0)
//                throw new FormValidationException("Descrição: É necessário informar a descrição do catálogo.");
            if (catalogo.getNovidadeDe() == null || catalogo.getNovidadeAte() == null)
                throw new FormValidationException("Novidade: É necessário informar as datas de novidade do catálogo.");
            if (catalogo.getNovidadeDe().isAfter(catalogo.getNovidadeAte()))
                throw new FormValidationException("Novidade: A data final deve ser maior ou igual a data de início.");
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="conceito">
            if (catalogoEmEdicao.getConceitos().size() == 0)
                throw new FormValidationException("Conceitos: É necessário cadastrar ao mesmo um conceito.");
            for (SdConceitoCatalogo conceito : catalogoEmEdicao.getConceitos()) {
                if (conceito.getImagem() == null || conceito.getImagem().isEmpty())
                    throw new FormValidationException("Conceito " + conceito.getOrdem() + " > Imagem: O campo deve estar preenchido.");
                if (conceito.getImagemMobile() == null || conceito.getImagemMobile().isEmpty())
                    throw new FormValidationException("Conceito " + conceito.getOrdem() + " > Imagem Mobile: O campo deve estar preenchido.");
                if (conceito.getTitulo() == null || conceito.getTitulo().isEmpty())
                    throw new FormValidationException("Conceito " + conceito.getOrdem() + " > Título: O campo deve estar preenchido.");
//                if (conceito.getDescricao() == null || conceito.getDescricao().isEmpty())
//                    throw new FormValidationException("Conceito " + conceito.getOrdem() + " > Descrição: O campo deve estar preenchido.");
                if (conceito.getLinha() == null)
                    throw new FormValidationException("Conceito " + conceito.getOrdem() + " > Linha: O campo deve estar preenchido.");
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="lookbook">
            if (catalogoEmEdicao.getLookbooks().size() == 0)
                throw new FormValidationException("Lookbooks: É necessário cadastrar ao mesmo um lookbook.");
            for (SdLookbookCatalogo lookbook : catalogoEmEdicao.getLookbooks()) {
                if (lookbook.getImagens() == null || lookbook.getImagens().isEmpty())
                    throw new FormValidationException("Lookbook " + lookbook.getOrdem() + " > Imagens: Você deve cadastrar ao menos uma imagem para o lookbook.");
                if (lookbook.getTitulo() == null || lookbook.getTitulo().isEmpty())
                    throw new FormValidationException("Lookbook " + lookbook.getOrdem() + " > Título: O campo deve estar preenchido.");
//                if (lookbook.getDescricao() == null || lookbook.getDescricao().isEmpty())
//                    throw new FormValidationException("Lookbook " + lookbook.getOrdem() + " > Descrição: O campo deve estar preenchido.");
                if (lookbook.getLinha() == null)
                    throw new FormValidationException("Lookbook " + lookbook.getOrdem() + " > Linha: O campo deve estar preenchido.");
            }
            // </editor-fold>
        } catch (FormValidationException valid) {
            MessageBox.create(message -> {
                message.message("Existem informações obrigatórias não preenchidas:\n" + valid.getMessage());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        // </editor-fold>

        String jsonCatalogo = null;
        try {
            jsonCatalogo = new ObjectMapper().writeValueAsString(new CatalogoEnvio(Arrays.asList(catalogo)));
            System.out.println(jsonCatalogo);
            new ServicoNitroEcom().sendCatalogo(jsonCatalogo);
            catalogo.setStatusEnvio(true);
            new FluentDao().merge(catalogo);
            MessageBox.create(message -> {
                message.message("Catálogo publicado no catálogo digital.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            emEdicao.set(false);
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.ENVIAR, String.valueOf(catalogoEmEdicao.getCodigo()), "Enviado catálogo para Catálogo Digital!");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        } catch (HttpResponseException e) {
            e.printStackTrace();
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.ENVIAR, String.valueOf(catalogoEmEdicao.getCodigo()), "Erro ao enviar catálogo para servidor: " + e.getMessage());
            MessageBox.create(message -> {
                message.message(e.getMessage());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private boolean salvarCatalogo(SdCatalogo catalogoEmEdicao) {
        try {
            saveCatalogo(catalogoEmEdicao);
            MessageBox.create(message -> {
                message.message("Catálogo salvo no banco de dados.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.SALVAR, String.valueOf(catalogoEmEdicao.getCodigo()), "Dados do catálogo salvos!");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
            return false;
        }
    }

    private void excluirCatalogo(SdCatalogo catalogo) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o catálogo?");
            message.showAndWait();
        }).value.get())) {
            new FluentDao().delete(catalogo);
            cancelarCatalogo();
            limparSelecaoCatalogo();
            MessageBox.create(message -> {
                message.message("Catálogo excluído com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EXCLUIR, String.valueOf(catalogo.getCodigo()), "Excluído catálogo");

        }
    }

    private void cancelarCatalogo() {
        unbind(catalogoEmEdicao);
        catalogoEmEdicao = new SdCatalogo();
        bind(catalogoEmEdicao);
        panelConceitos.getTabs().clear();
        lookbooksEdicaoBean.clear();
        emEdicao.set(false);

        tabs.getSelectionModel().select(0);
    }

    private void sugestaoPeriodoNovidade() {
        if (fieldMarca.value.get() == null) {
            MessageBox.create(message -> {
                message.message("Por favor, selecione primeiro a marca do mostruário.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            fieldColecao.clear();
            return;
        }

        if (!fieldPeriodoNovidade.validateExistPeriodo()) {
            SdColecaoMarca001 colecaoMarca = getColecaoMarca(fieldMarca.value.get().getCodigo(), fieldColecao.value.get().getCodigo());
            if (colecaoMarca == null) {
                MessageBox.create(message -> {
                    message.message("Não foi possível encontrar o cadastro da coleção " + fieldColecao.value.get().getCodigo() + " para a marca " + fieldMarca.value.get().getCodigo());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
                return;
            }

            fieldPeriodoNovidade.valueBegin.set(colecaoMarca.getInicioVend().toLocalDate());
            fieldPeriodoNovidade.valueEnd.set(colecaoMarca.getFimVend().toLocalDate());
        }
    }

    private void adicionarNovoCatalogo() {
        cancelarCatalogo();
        emEdicao.set(true);
        lookbooksEdicaoBean.set(FXCollections.observableList(catalogoEmEdicao.getLookbooks()));
    }

    private void adicionarImagemCatalogo() {
        if (catalogoEmEdicao.getMarca() == null || catalogoEmEdicao.getColecao() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa definir a MARCA e a COLEÇÃO do catálogo para adicionar a imagem.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        File pathFile = FileChoice.show(box -> {
            box.basePath(baseLocalPath.concat("\\").concat(catalogoEmEdicao.getColecao().getCodigo()).concat("\\").concat(catalogoEmEdicao.getMarca().getCodigo()));
            box.addExtension(new FileChooser.ExtensionFilter("Arquivos de Imagens", "*.png", "*.jpg"));
        }).fileOpenPath();
        if (pathFile != null) {
            String fileName = pathFile.getName();
            catalogoEmEdicao.setImagem(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat(fileName)));
            catalogoEmEdicao.setImagemLocal(pathFile.getAbsolutePath());
        }
    }

    private void abrirImagemCatalogo() {
        if (catalogoEmEdicao.getImagem() == null)
            return;

        new Fragment().show(fragment -> {
            fragment.title("Imagem Catálogo");
            fragment.size(550.0, 700.0);

            final FormFieldImage fieldImagemCarregada = FormFieldImage.create(img -> {
                img.withoutTitle();
                img.expanded();
                try {
                    img.image(catalogoEmEdicao.getImagemLocal());
                } catch (FileNotFoundException e) {
                    MessageBox.create(message -> {
                        message.message("Não foi possível encontrar o arquivo: " + catalogoEmEdicao.getImagemLocal());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            });
            fragment.box.getChildren().add(fieldImagemCarregada.build());
            fieldImagemCarregada.fitHeight(650.0);
        });
    }

    private void adicionarVideo(FormFieldText field) {
        field.value.set((String) InputBox.build(String.class, boxInput -> {
            boxInput.message("URL do Vídeo");
            boxInput.showAndWait();
        }).value.getValue());
    }

    private void abrirVideo(String url) {
        new Fragment().show(fragment -> {
            fragment.title("Vídeo Produto");
            fragment.size(600.0, 550.0);
            WebView viewVideo = new WebView();
            viewVideo.getEngine().load(url);
            fragment.box.getChildren().add(viewVideo);
        });
    }

    private void bind(SdCatalogo catalogo) {
        if (catalogo == null)
            return;

        fieldDestaque.value.set(catalogo.getDestaque());
        fieldStatus.value.set(catalogo.getStatus());
        fieldDestaque.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                catalogo.setDestaque(newValue);
            }
        });
        fieldStatus.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                catalogo.setStatus(newValue);
            }
        });
        fieldPeriodoNovidade.valueBegin.bindBidirectional(catalogo.novidadeDeProperty());
        fieldPeriodoNovidade.valueEnd.bindBidirectional(catalogo.novidadeAteProperty());
        fieldVideo.value.bindBidirectional(catalogo.videoProperty());
        fieldImagem.value.bindBidirectional(catalogo.imagemProperty());
        fieldAno.value.bindBidirectional(catalogo.anoProperty());
        fieldMarca.value.bindBidirectional(catalogo.marcaProperty());
        fieldColecao.value.bindBidirectional(catalogo.colecaoProperty());
        fieldDescricao.value.bindBidirectional(catalogo.descricaoProperty());
        fieldTitulo.value.bindBidirectional(catalogo.nomeProperty());
        fieldCodigo.value.bind(catalogo.codigoProperty().asString());
    }

    private void unbind(SdCatalogo catalogo) {
        if (catalogo == null)
            return;

        fieldPeriodoNovidade.valueBegin.unbindBidirectional(catalogo.novidadeDeProperty());
        fieldPeriodoNovidade.valueEnd.unbindBidirectional(catalogo.novidadeAteProperty());
        fieldVideo.value.unbindBidirectional(catalogo.videoProperty());
        fieldImagem.value.unbindBidirectional(catalogo.imagemProperty());
        fieldAno.value.unbindBidirectional(catalogo.anoProperty());
        fieldColecao.value.unbindBidirectional(catalogo.colecaoProperty());
        fieldMarca.value.unbindBidirectional(catalogo.marcaProperty());
        fieldDescricao.value.unbindBidirectional(catalogo.descricaoProperty());
        fieldTitulo.value.unbindBidirectional(catalogo.nomeProperty());
        fieldCodigo.value.unbind();
    }

    // -------------------------------- LOOKBOOK -----------------------------------------
    private void adicionarNovoLookbook() {
        Boolean createOnly = true;
        if (catalogoEmEdicao.getLookbooks().size() == 0 && emEdicao.get()) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Deseja cadastrar os lookbooks utilizando as imagens como referência?");
                message.showAndWait();
            }).value.get())) {
                if (catalogoEmEdicao.getMarca() == null || catalogoEmEdicao.getColecao() == null) {
                    MessageBox.create(message -> {
                        message.message("Você precisa definir a MARCA e a COLEÇÃO do catálogo para criar os lookbooks automático.");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showAndWait();
                    });
                    return;
                }

                createOnly = false;
            }
        }

        if (createOnly) {
            SdLookbookCatalogo lookbook = new SdLookbookCatalogo();
            lookbook.setCatalogo(catalogoEmEdicao);
            catalogoEmEdicao.getLookbooks().add(lookbook);
            lookbooksEdicaoBean.add(lookbook);
            lookbook.setOrdem(String.valueOf(catalogoEmEdicao.getLookbooks().size()));
            abrirLookbook(lookbook);
        } else {
            List<SdLookbookCatalogo> lookbooksCatalogo = new ArrayList<>();
            AtomicReference<String> lookbooksSemImagem = new AtomicReference<>("");
            new RunAsyncWithOverlay(this).exec(task -> {
                String lookbookPath = baseLocalPath.concat("\\").concat(catalogoEmEdicao.getColecao().getCodigo()).concat("\\").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("\\").concat("Lookbook");
                File folderLookbooks = new File(lookbookPath);
                List<String> lookbookImagens = Arrays.asList(folderLookbooks.list()).stream().filter(cts -> cts.startsWith("LB")).collect(Collectors.toList());
                List<String> lookbookImagensReduzido = lookbookImagens.stream().map(fileName -> fileName.toUpperCase().replace(".PNG", "").split("_")[0]).distinct().collect(Collectors.toList());
                lookbookImagensReduzido.sort(Comparator.comparingInt(o -> Integer.parseInt(o.replaceAll("[^0-9]", ""))));

                String imagensComProblema = "";
                for (String nameLookbook : lookbookImagensReduzido)
                    if (lookbookImagens.stream().noneMatch(img -> img.startsWith("LB".concat(String.valueOf(lookbookImagensReduzido.indexOf(nameLookbook) + 1)).concat("_"))))
                        imagensComProblema = imagensComProblema.concat("LB".concat(String.valueOf(lookbookImagensReduzido.indexOf(nameLookbook) + 1)));
                if (imagensComProblema.length() > 0) {
                    lookbooksSemImagem.set(imagensComProblema);
                    return ReturnAsync.NOT_FOUND.value;
                }

                lookbookImagensReduzido.forEach(look -> {
                    SdLookbookCatalogo lookbook = new SdLookbookCatalogo();
                    lookbook.setCatalogo(catalogoEmEdicao);
                    catalogoEmEdicao.getLookbooks().add(lookbook);
                    lookbook.setOrdem(String.valueOf(catalogoEmEdicao.getLookbooks().size()));

                    lookbookImagens.stream().filter(img -> img.startsWith(look.concat("_"))).forEach(file -> {
                        SdImagensLookbookPK pkImagemLookbook = new SdImagensLookbookPK();
                        pkImagemLookbook.setLookbook(lookbook);
                        pkImagemLookbook.setOrdem(String.valueOf(lookbook.getImagens().size() + 1));
                        SdImagensLookbook imagemLookbook = new SdImagensLookbook();
                        imagemLookbook.setId(pkImagemLookbook);
                        imagemLookbook.setImagem(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Lookbook").concat("/").concat(file)));
                        imagemLookbook.setImagemLocal(lookbookPath + "/" + file);
                        imagemLookbook.postLoad();
                        lookbook.getImagens().add(imagemLookbook);
                    });
                    lookbooksCatalogo.add(lookbook);
                });

                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    lookbooksEdicaoBean.addAll(lookbooksCatalogo);
                } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                    MessageBox.create(message -> {
                        message.message("Existem lookbooks sem imagem na pasta local. Verifique os sequenciamentos das imagens na pasta respectiva da coleção e marca.\n" +
                                "Imagens não existentes: " + lookbooksSemImagem.get());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                }
            });
        }
    }

    private void abrirLookbook(SdLookbookCatalogo lookbook) {
        final ListProperty<SdProdutosLookbook> produtosLookbook = new SimpleListProperty<>(FXCollections.observableArrayList());
        produtosLookbook.addAll(lookbook.getProdutos());
        final FormFieldText fieldCodigoLookbook = FormFieldText.create(field -> {
            field.title("Código");
            field.editable(false);
            field.width(60);
            field.addStyle("warning");
            field.alignment(Pos.CENTER);
            field.value.bind(lookbook.codigoProperty().asString());
        });
        final FormFieldText fieldTituloLookbook = FormFieldText.create(field -> {
            field.title("Título");
            field.width(250);
            field.editable.bind(emEdicao);
            field.value.bindBidirectional(lookbook.tituloProperty());
        });
        final FormFieldTextArea fieldDescricaoLookbook = FormFieldTextArea.create(field -> {
            field.title("Descrição");
            field.editable.bind(emEdicao);
            field.height(150);
            field.value.bindBidirectional(lookbook.descricaoProperty());
        });
        final FormFieldText fieldVideoLookbook = FormFieldText.create(field -> {
            field.title("Vídeo");
            field.editable(false);
            field.width(100.0);
            field.tooltip("Clique 2x para abrir o vídeo.\nPressione CTRL + DELETE para excluir o vídeo.");
            field.mouseClicked(evt -> {
                if (evt.getClickCount() == 2) {
                    abrirVideo(field.value.get());
                }
            });
            field.value.bindBidirectional(lookbook.videoProperty());
            field.keyReleased(evt -> {
                if (emEdicao.get() && evt.isControlDown() && evt.getCode().equals(KeyCode.DELETE) && emEdicao.get()) {
                    field.clear();
                }
            });
        });
        final FormFieldToggle fieldStatusLookbook = FormFieldToggle.create(field -> {
            field.title("Status");
            field.value.set(true);
            field.editable.bind(emEdicao);
            field.value.set(lookbook.getStatus());
            field.value.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    lookbook.setStatus(newValue);
                }
            });
        });
        final FormFieldToggle fieldDestaqueLookbook = FormFieldToggle.create(field -> {
            field.title("Destaque");
            field.value.set(false);
            field.editable.bind(emEdicao);
            field.value.set(lookbook.getDestaque());
            field.value.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    lookbook.setDestaque(newValue);
                }
            });
        });
        final FormFieldSingleFind<VSdLinhaCatalogo> fieldLinhaLookbook = FormFieldSingleFind.create(VSdLinhaCatalogo.class, field -> {
            field.title("Linha");
            field.editable.bind(emEdicao);
            field.width(280);
            field.value.bindBidirectional(lookbook.linhaProperty());
        });
        final FormBox boxImagens = FormBox.create(box -> {
            box.expanded();
            box.flutuante();
            box.verticalScroll();
        });

        lookbook.getImagens().sort(Comparator.comparing(SdImagensLookbook::getOrdem));
        for (SdImagensLookbook imagem : lookbook.getImagens()) {
            adicionarImagensLookbookView(lookbook, boxImagens, imagem, produtosLookbook);
        }

        new Fragment().show(fragment -> {
            fragment.title("Cadastro de Lookbook - LB-" + lookbook.getOrdem());
            fragment.size(1000.0, 750.0);

            fragment.onClose(t -> {
                tblLookbooks.refresh();
                return true;
            });
            fragment.box.getChildren().add(FormBox.create(containerTab -> {
                containerTab.vertical();
                containerTab.expanded();
                containerTab.add(FormBox.create(boxFieldsTab -> {
                    boxFieldsTab.horizontal();
                    boxFieldsTab.expanded();
                    boxFieldsTab.add(left -> {
                        left.vertical();
                        left.width(700.0);
                        left.add(toolbarImagens -> {
                            toolbarImagens.vertical();
                            toolbarImagens.add(FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                btn.addStyle("success").addStyle("lg");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarImagemLookbook(lookbook, boxImagens, produtosLookbook));
                            }));
                        });
                        left.add(boxImagens);
                        left.add(FormLabel.create(lb -> {
                            lb.setText("Clique 2x na primeira imagem para criação dos pins.");
                        }));
                    });
                    boxFieldsTab.add(right -> {
                        right.vertical();
                        right.add(fields -> {
                            fields.horizontal();
                            fields.add(fieldCodigoLookbook.build(), fieldTituloLookbook.build());
                        });
                        right.add(fields -> {
                            fields.horizontal();
                            fields.alignment(Pos.BOTTOM_LEFT);
                            fields.add(fieldVideoLookbook.build(), FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                btn.addStyle("success");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarVideo(fieldVideoLookbook));
                            }), fieldStatusLookbook.build(), fieldDestaqueLookbook.build());
                        });
                        right.add(fieldDescricaoLookbook.build());
                        right.add(fieldLinhaLookbook.build());
                        right.add(FormTableView.create(SdProdutosLookbook.class, table -> {
                            table.title("Produtos");
                            table.expanded();
                            table.items.bind(produtosLookbook);
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Produto");
                                        cln.width(190.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosLookbook, SdProdutosLookbook>, ObservableValue<SdProdutosLookbook>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                                    }).build() /*Produto*/,
                                    FormTableColumnGroup.createGroup(group -> {
                                        group.title("Localização");
                                        group.alignment(Pos.CENTER);
                                        group.addColumn(FormTableColumn.create(cln -> {
                                            cln.title("Eixo X");
                                            cln.width(40.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosLookbook, SdProdutosLookbook>, ObservableValue<SdProdutosLookbook>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEixoX()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Eixo X*/);
                                        group.addColumn(FormTableColumn.create(cln -> {
                                            cln.title("Eixo Y");
                                            cln.width(40.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosLookbook, SdProdutosLookbook>, ObservableValue<SdProdutosLookbook>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEixoY()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Eixo Y*/);
                                    }) /*eixos*/,
                                    FormTableColumn.create(cln -> {
                                        cln.width(40.0);
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosLookbook, SdProdutosLookbook>, ObservableValue<SdProdutosLookbook>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                        cln.format(param -> {
                                            return new TableCell<SdProdutosLookbook, SdProdutosLookbook>() {
                                                final HBox boxButtonsRow = new HBox(3);
                                                final Button btnExcluirProduto = FormButton.create(btn -> {
                                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                                    btn.tooltip("Excluir produto do conceito");
                                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                    btn.addStyle("xs").addStyle("danger");
                                                    btn.disable.bind(emEdicao.not());
                                                });

                                                @Override
                                                protected void updateItem(SdProdutosLookbook item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    setGraphic(null);
                                                    if (item != null && !empty) {

                                                        btnExcluirProduto.setOnAction(evt -> {
                                                            excluirProdutoLookbook(lookbook, item, produtosLookbook);
                                                        });
                                                        boxButtonsRow.getChildren().clear();
                                                        boxButtonsRow.getChildren().addAll(btnExcluirProduto);
                                                        setGraphic(boxButtonsRow);
                                                    }
                                                }
                                            };
                                        });
                                    }).build()
                            );
                        }).build());
                    });
                }));
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Excluir Lookbook");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                btn.addStyle("warning");
                btn.disable.bind(emEdicao.not());
                btn.setAction(evt -> {
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o lookbook?");
                        message.showAndWait();
                    }).value.get()) {
                        excluirLookbook(lookbook);
                        fragment.close();
                    } else {
                        evt.consume();
                    }
                });
            }));
        });
    }

    private void excluirImagemLookbook(SdLookbookCatalogo lookbook, FormBox boxImagens, SdImagensLookbook imagemLookbook, FormBox boxImagem, ListProperty<SdProdutosLookbook> produtosLookbook) {
        if (imagemLookbook.getId().getOrdem().equals("1") && lookbook.getProdutos().size() > 0) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Você está excluindo a imagem de refência para os produtos do lookbook. Deseja realmente excluir a imagem? Serão excluídos os produtos vinculados a esse lookbook.");
                message.showAndWait();
            }).value.get())) {
                return;
            } else {
                lookbook.getProdutos().clear();
                produtosLookbook.clear();
            }
        }

        lookbook.getImagens().remove(imagemLookbook);
        boxImagens.remove(boxImagem);

        AtomicReference<Integer> ordem = new AtomicReference<>(1);
        lookbook.getImagens().forEach(img -> {
            img.getId().setOrdem(String.valueOf(ordem.getAndSet(ordem.get() + 1)));
        });
    }

    private void excluirLookbook(SdLookbookCatalogo lookbook) {
        try {
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_lookbook_catalogo_001 where catalogo = '%s' and codigo = '%s'", lookbook.getCatalogo().getCodigo(), lookbook.getCodigo()));
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EXCLUIR, String.valueOf(catalogoEmEdicao.getCodigo()), "Marcando LOOKBOOK " + lookbook.getCodigo() + " (" + lookbook.getTitulo() + ") para exclusão.");
            catalogoEmEdicao.getLookbooks().remove(lookbook);
            lookbooksBean.remove(lookbook);
            lookbooksEdicaoBean.remove(lookbook);

            AtomicReference<Integer> ordem = new AtomicReference<>(1);
            catalogoEmEdicao.getLookbooks().forEach(look -> look.setOrdem(String.valueOf(ordem.getAndSet(ordem.get() + 1))));
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void excluirProdutoLookbook(SdLookbookCatalogo lookbook, SdProdutosLookbook produto, ListProperty<SdProdutosLookbook> produtosLookbook) {
        try {
            new NativeDAO().runNativeQueryUpdate("delete from sd_produtos_lookbook_001 where catalogo = '%s' and lookbook = '%s' and codigo = '%s' and ordem = '%s'",
                    produto.getId().getLookbook().getCatalogo().getCodigo(), produto.getId().getLookbook().getCodigo(), produto.getId().getCodigo().getCodigo(), produto.getId().getOrdem());
            lookbook.getProdutos().remove(produto);
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EXCLUIR, String.valueOf(catalogoEmEdicao.getCodigo()), "Marcando produto " + produto.getId().getCodigo().getCodigo() + " do LOOKBOOK " + lookbook.getCodigo() + " (" + lookbook.getTitulo() + ") para exclusão.");

            produtosLookbook.remove(produto);
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void adicionarImagemLookbook(SdLookbookCatalogo lookbook, FormBox boxImagens, ListProperty<SdProdutosLookbook> produtosLookbook) {
        if (catalogoEmEdicao.getMarca() == null || catalogoEmEdicao.getColecao() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa definir a MARCA e a COLEÇÃO do catálogo para adicionar a imagem.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        File pathFile = FileChoice.show(box -> {
            box.basePath(baseLocalPath.concat("\\").concat(catalogoEmEdicao.getColecao().getCodigo()).concat("\\").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("\\").concat("Lookbook"));
            box.addExtension(new FileChooser.ExtensionFilter("Arquivos de Imagens", "*.png", "*.jpg"));
        }).fileOpenPath();
        String fileName = pathFile.getName();

        SdImagensLookbookPK pkImagemLookbook = new SdImagensLookbookPK();
        pkImagemLookbook.setLookbook(lookbook);
        pkImagemLookbook.setOrdem(String.valueOf(lookbook.getImagens().size() + 1));
        SdImagensLookbook imagemLookbook = new SdImagensLookbook();
        imagemLookbook.setId(pkImagemLookbook);
        imagemLookbook.setImagem(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Lookbook").concat("/").concat(fileName)));
        imagemLookbook.setImagemLocal(pathFile.getAbsolutePath());
        lookbook.getImagens().add(imagemLookbook);

        adicionarImagensLookbookView(lookbook, boxImagens, imagemLookbook, produtosLookbook);
    }

    private void adicionarImagensLookbookView(SdLookbookCatalogo lookbook, FormBox boxImagens, SdImagensLookbook imagemLookbook, ListProperty<SdProdutosLookbook> produtosLookbook) {
        boxImagens.add(boxImagem -> {
            boxImagem.vertical();

            try {
                File imagemLb = new File(imagemLookbook.getImagemLocal());
                BufferedImage bufferedImage = ImageIO.read(imagemLb);
                boxImagem.add(FormFieldImage.create(imagem -> {
                    imagem.withoutTitle();
                    imagem.height(200.0);
                    try {
                        imagem.image(imagemLb);
                        imagem.mouseClicked(evt -> {
                            criarPinLookbook(lookbook, imagemLookbook, produtosLookbook, evt);
                        });
                    } catch (FileNotFoundException e) {
                        MessageBox.create(message -> {
                            message.message("Não foi possível encontrar o arquivo: " + catalogoEmEdicao.getImagemLocal());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                }).build());
                boxImagem.add(boxFooterImage -> {
                    boxFooterImage.vertical();
                    boxFooterImage.add(FormLabel.create(lb -> {
                        lb.setText("Dim: " + bufferedImage.getWidth() + "x" + bufferedImage.getHeight());
                        lb.setPadding(new Insets(-1));
                        lb.sizeText(10);
                    }));
                    boxFooterImage.add(FormButton.create(btn -> {
                        btn.title("Excluir Imagem");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                        btn.addStyle("danger").addStyle("xs");
                        btn.disable.bind(emEdicao.not());
                        btn.setAction(evt -> excluirImagemLookbook(lookbook, boxImagens, imagemLookbook, boxImagem, produtosLookbook));
                    }));
                });
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    }

    private void criarPinLookbook(SdLookbookCatalogo lookbook, SdImagensLookbook imagemLookbook, ListProperty<SdProdutosLookbook> produtosLookbook, MouseEvent evt) {
        if (evt.getClickCount() == 2 && imagemLookbook.getId().getOrdem().equals("1")) {
            new Fragment().show(fragment -> {
                fragment.title("Pins Imagem Lookbook");
                fragment.size(1050.0, 900.0);

                final FormFieldImage fieldImagemCarregada = FormFieldImage.create(img -> {
                    img.withoutTitle();
                    try {
                        img.image(imagemLookbook.getImagemLocal());
                        addPinImagemLookbook(img, produtosLookbook);
                        img.mouseClicked(evtPin -> {
                            if (evtPin.getClickCount() > 1 && emEdicao.get()) {
                                InputBox<Produto> produtoSelecionado = InputBox.build(Produto.class, boxInput -> {
                                    boxInput.message("Selecione o produto:");
                                    boxInput.showAndWait();
                                });
                                if (produtoSelecionado.value.get() == null)
                                    return;

                                SdProdutosLookbookPK pkProdutoLookbook = new SdProdutosLookbookPK();
                                pkProdutoLookbook.setLookbook(lookbook);
                                pkProdutoLookbook.setCodigo(produtoSelecionado.value.get());
                                pkProdutoLookbook.setOrdem(produtosLookbook.stream().mapToInt(prod -> prod.getId().getOrdem()).max().orElse(0) + 1);
                                SdProdutosLookbook produtoLookbook = new SdProdutosLookbook();
                                produtoLookbook.setId(pkProdutoLookbook);
                                produtoLookbook.setEixoX(new BigDecimal(evtPin.getX()));
                                produtoLookbook.setEixoY(new BigDecimal(evtPin.getY()));

                                lookbook.getProdutos().add(produtoLookbook);
                                produtosLookbook.add(produtoLookbook);

                                try {
                                    addPinImagemLookbook(img, produtosLookbook);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            }
                        });
                    } catch (FileNotFoundException e) {
                        MessageBox.create(message -> {
                            message.message("Não foi possível encontrar o arquivo: " + catalogoEmEdicao.getImagemLocal());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
                fragment.box.getChildren().add(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.verticalScroll();
                    content.add(FormLabel.create(lb -> {
                        lb.setText("Clique 2x na imagem para criar o pin e selecionar o produto.");
                        lb.setPadding(new Insets(-1));
                        lb.sizeText(10);
                    }));
                    content.add(fieldImagemCarregada.build());
                }));
            });
        }
    }

    private void addPinImagemLookbook(FormFieldImage image, ListProperty<SdProdutosLookbook> produtosLookbook) throws IOException {
        File imagemArte = new File(image.pathImage.get());
        BufferedImage bufferedImage = ImageIO.read(imagemArte);
        for (SdProdutosLookbook produto : produtosLookbook) {
            Graphics graphics = bufferedImage.getGraphics();
            graphics.drawImage(ImageIO.read(new File("C:\\SysDelizLocal\\local_files\\pin.png")), produto.getEixoX().intValue() - 15, produto.getEixoY().intValue() - 15, null);
        }
        //ImageIO.write(bufferedImage, "png", new File("C:\\Temp\\teste.png"));

        //ImageIO.write(bufferedImage, "png", imagemArte);
        Image imageBuffer = SwingFXUtils.toFXImage(bufferedImage, null);
        image.image(imageBuffer, image.pathImage.get());
    }

    // -------------------------------- CONCEITO -----------------------------------------
    private void adicionarNovoConceito() {
        Boolean createOnly = true;
        if (catalogoEmEdicao.getConceitos().size() == 0 && emEdicao.get()) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Deseja cadastrar os conceitos utilizando as imagens como referência?");
                message.showAndWait();
            }).value.get())) {
                if (catalogoEmEdicao.getMarca() == null || catalogoEmEdicao.getColecao() == null) {
                    MessageBox.create(message -> {
                        message.message("Você precisa definir a MARCA e a COLEÇÃO do catálogo para criar os conceitos automático.");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showAndWait();
                    });
                    return;
                }

                createOnly = false;
            }
        }

        if (createOnly) {
            SdConceitoCatalogo conceito = new SdConceitoCatalogo();
            conceito.setCatalogo(catalogoEmEdicao);
            catalogoEmEdicao.getConceitos().add(conceito);
            conceito.setOrdem(String.valueOf(catalogoEmEdicao.getConceitos().size()));
            adicionarTabConceito(conceito);
        } else {
            List<SdConceitoCatalogo> conceitosCatalogo = new ArrayList<>();
            AtomicReference<String> conceitosSemImagem = new AtomicReference<>("");
            new RunAsyncWithOverlay(this).exec(task -> {
                String conceitoPath = baseLocalPath.concat("\\").concat(catalogoEmEdicao.getColecao().getCodigo()).concat("\\").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("\\").concat("Conceito");
                File folderConceitos = new File(conceitoPath);
                List<String> conceitosImagens = Arrays.asList(folderConceitos.list()).stream().filter(cts -> cts.startsWith("CT")).collect(Collectors.toList());
                List<String> conceitosImagensReduzido = conceitosImagens.stream().map(fileName -> fileName.toUpperCase().replace(".PNG", "").split("_")[0]).distinct().collect(Collectors.toList());
                conceitosImagensReduzido.sort(Comparator.comparingInt(o -> Integer.parseInt(o.replaceAll("[^0-9]", ""))));

                String imagensComProblema = "";
                for (String nameConceito : conceitosImagensReduzido)
                    if (conceitosImagens.stream().noneMatch(img -> img.startsWith("CT".concat(String.valueOf(conceitosImagensReduzido.indexOf(nameConceito) + 1)).concat("_"))))
                        imagensComProblema = imagensComProblema.concat("CT".concat(String.valueOf(conceitosImagensReduzido.indexOf(nameConceito) + 1)));
                if (imagensComProblema.length() > 0) {
                    conceitosSemImagem.set(imagensComProblema);
                    return ReturnAsync.NOT_FOUND.value;
                }

                conceitosImagensReduzido.forEach(conct -> {
                    SdConceitoCatalogo conceito = new SdConceitoCatalogo();
                    conceito.setCatalogo(catalogoEmEdicao);
                    catalogoEmEdicao.getConceitos().add(conceito);
                    conceito.setOrdem(String.valueOf(catalogoEmEdicao.getConceitos().size()));

                    conceitosImagens.stream().filter(img -> img.toUpperCase().equals(conct.concat("_D.PNG"))).findAny().ifPresent(file -> {
                        conceito.setImagem(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Conceito").concat("/").concat(file)));
                        conceito.setImagemLocal(conceitoPath + "/" + file);
                    });
                    conceitosImagens.stream().filter(img -> img.toUpperCase().equals(conct.concat("_M.PNG"))).findAny().ifPresent(file -> {
                        conceito.setImagemMobile(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Conceito").concat("/").concat(file)));
                        conceito.setImagemMobileLocal(conceitoPath + "/" + file);
                    });
                    conceitosCatalogo.add(conceito);
                });

                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    conceitosCatalogo.forEach(conceito -> adicionarTabConceito(conceito));
                } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                    MessageBox.create(message -> {
                        message.message("Existem conceitos sem imagem na pasta local. Verifique os sequenciamentos das imagens na pasta respectiva da coleção e marca.\n" +
                                "Imagens não existentes: " + conceitosSemImagem.get());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                }
            });
        }
    }

    private void adicionarTabConceito(SdConceitoCatalogo conceito) {
        final ListProperty<SdProdutosConceito> produtosConceito = new SimpleListProperty<>(FXCollections.observableArrayList());
        produtosConceito.addAll(conceito.getProdutos());
        final FormFieldText fieldCodigoConceito = FormFieldText.create(field -> {
            field.title("Código");
            field.editable(false);
            field.width(60);
            field.addStyle("warning");
            field.alignment(Pos.CENTER);
            field.value.bind(conceito.codigoProperty().asString());
        });
        final FormFieldText fieldTituloConceito = FormFieldText.create(field -> {
            field.title("Título");
            field.width(250);
            field.editable.bind(emEdicao);
            field.value.bindBidirectional(conceito.tituloProperty());
        });
        final FormFieldTextArea fieldDescricaoConceito = FormFieldTextArea.create(field -> {
            field.title("Descrição");
            field.editable.bind(emEdicao);
            field.expanded();
            field.value.bindBidirectional(conceito.descricaoProperty());
        });
        final FormFieldSingleFind<VSdLinhaCatalogo> fieldLinhaConceito = FormFieldSingleFind.create(VSdLinhaCatalogo.class, field -> {
            field.title("Linha");
            field.editable.bind(emEdicao);
            field.width(280);
            field.value.bindBidirectional(conceito.linhaProperty());
        });
        final FormFieldToggle fieldStatusConceito = FormFieldToggle.create(field -> {
            field.title("Status");
            field.value.set(true);
            field.editable.bind(emEdicao);
            field.value.set(conceito.getStatus());
            field.value.addListener((observable, oldValue, newValue) -> {
                if (newValue != null)
                    conceito.setStatus(newValue);
            });
        });

        final FormFieldText fieldImagemConceitoDesktop = FormFieldText.create(field -> {
            field.title("Caminho Imagem");
            field.editable(false);
            field.addStyle("xs");
            field.expanded();
            field.value.bindBidirectional(conceito.imagemProperty());
        });
        final FormFieldImage fieldImagemDesktop = FormFieldImage.create(img -> {
            img.withoutTitle();
            img.fitWidth(200.0);
            img.mouseClicked(evt -> cliqueImagemConceito(evt, conceito, produtosConceito, "D"));
            if (conceito.getImagemLocal() != null) {
                try {
                    img.image(conceito.getImagemLocal());
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });
        final FormFieldText fieldImagemConceitoMobile = FormFieldText.create(field -> {
            field.title("Caminho Imagem");
            field.editable(false);
            field.expanded();
            field.addStyle("xs");
            field.value.bindBidirectional(conceito.imagemMobileProperty());
        });
        final FormFieldImage fieldImagemMobile = FormFieldImage.create(img -> {
            img.withoutTitle();
            img.fitWidth(200.0);
            img.mouseClicked(evt -> cliqueImagemConceito(evt, conceito, produtosConceito, "M"));
            if (conceito.getImagemMobileLocal() != null) {
                try {
                    img.image(conceito.getImagemMobileLocal());
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });

        final FormTableView<SdProdutosConceito> tblPinsConceito = FormTableView.create(SdProdutosConceito.class, table -> {
            table.title("Produtos");
            table.expanded();
            table.items.bind(produtosConceito);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Produto");
                        cln.width(250.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosConceito, SdProdutosConceito>, ObservableValue<SdProdutosConceito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                    }).build() /*Produto*/,
                    FormTableColumnGroup.createGroup(group -> {
                        group.title("Localização");
                        group.alignment(Pos.CENTER);
                        group.addColumn(FormTableColumn.create(cln -> {
                            cln.title("Eixo X");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosConceito, SdProdutosConceito>, ObservableValue<SdProdutosConceito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEixoX()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Eixo X*/);
                        group.addColumn(FormTableColumn.create(cln -> {
                            cln.title("Eixo Y");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosConceito, SdProdutosConceito>, ObservableValue<SdProdutosConceito>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEixoY()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Eixo Y*/);
                    }) /*eixos*/,
                    FormTableColumn.create(cln -> {
                        cln.width(40.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdProdutosConceito, SdProdutosConceito>, ObservableValue<SdProdutosConceito>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<SdProdutosConceito, SdProdutosConceito>() {
                                final HBox boxButtonsRow = new HBox(3);
                                final Button btnExcluirProduto = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Excluir produto do conceito");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("danger");
                                    btn.disable.bind(emEdicao.not());
                                });

                                @Override
                                protected void updateItem(SdProdutosConceito item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {

                                        btnExcluirProduto.setOnAction(evt -> {
                                            excluirProdutoConceito(conceito, item, produtosConceito);
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnExcluirProduto);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            };
                        });
                    }).build()
            );
            table.factoryRow(param -> {
                return new TableRow<SdProdutosConceito>() {
                    @Override
                    protected void updateItem(SdProdutosConceito item, boolean empty) {
                        super.updateItem(item, empty);
                        getStyleClass().removeAll("table-row-info", "table-row-warning");
                        if (item != null && !empty) {
                            getStyleClass().add(item.getId().getTipo().equals("D") ? "table-row-info" : "table-row-warning");
                        }
                    }
                };
            });
            table.indices(
                    table.indice("Desktop", "info", ""),
                    table.indice("Mobile", "warning", "")
            );
        });

        fieldStatusConceito.value.set(conceito.getStatus());
        final FormTab tabConceito = FormTab.create(tab -> {
            tab.textProperty().bind(new SimpleStringProperty("CT").concat(conceito.ordemProperty()));
            tab.closable();
            tab.setOnCloseRequest(evt -> {
                if (emEdicao.get()) {
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o lookbook?");
                        message.showAndWait();
                    }).value.get()) {
                        excluirConceito(conceito);
                    } else {
                        evt.consume();
                    }
                } else {
                    evt.consume();
                }
            });
            tab.add(FormBox.create(boxFieldsTab -> {
                boxFieldsTab.horizontal();
                boxFieldsTab.expanded();
                boxFieldsTab.add(left -> {
                    left.vertical();
                    left.width(350.0);
                    left.add(fields -> {
                        fields.horizontal();
                        fields.add(fieldCodigoConceito.build(), fieldTituloConceito.build());
                    });
                    left.add(fields -> {
                        fields.horizontal();
                        fields.add(fieldStatusConceito.build());
                    });
                    left.add(fieldDescricaoConceito.build());
                    left.add(fieldLinhaConceito.build());
                });
                boxFieldsTab.add(center -> {
                    center.vertical();
                    center.width(250.0);
                    center.verticalScroll();
                    center.add(imgDesktop -> {
                        imgDesktop.vertical();
                        imgDesktop.title("Imagem Desktop");
                        imgDesktop.expanded();
                        imgDesktop.add(boxFields -> {
                            boxFields.horizontal();
                            boxFields.alignment(Pos.BOTTOM_LEFT);
                            boxFields.add(fieldImagemConceitoDesktop.build(), FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                                btn.addStyle("info").addStyle("xs");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarImagemConceito(fieldImagemDesktop, conceito, "D"));
                            }));
                        });
                        imgDesktop.add(fieldImagemDesktop.build());
                        imgDesktop.add(FormLabel.create(lb -> {
                            lb.setText("Clique 2x na imagem para selecionar os produtos.");
                            lb.setPadding(new Insets(-1));
                            lb.sizeText(10);
                        }));
                    });
                    center.add(imagemMobile -> {
                        imagemMobile.vertical();
                        imagemMobile.title("Imagem Mobile");
                        imagemMobile.expanded();
                        imagemMobile.add(boxFields -> {
                            boxFields.horizontal();
                            boxFields.alignment(Pos.BOTTOM_LEFT);
                            boxFields.add(fieldImagemConceitoMobile.build(), FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                                btn.addStyle("info").addStyle("xs");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> adicionarImagemConceito(fieldImagemMobile, conceito, "M"));
                            }));
                        });
                        imagemMobile.add(fieldImagemMobile.build());
                        imagemMobile.add(FormLabel.create(lb -> {
                            lb.setText("Clique 2x na imagem para selecionar os produtos.");
                            lb.setPadding(new Insets(-1));
                            lb.sizeText(10);
                        }));
                    });
                });
                boxFieldsTab.add(right -> {
                    right.vertical();
                    right.add(tblPinsConceito.build());
                });
            }));
        });
        panelConceitos.addTab(tabConceito);
        panelConceitos.getSelectionModel().select(tabConceito);
    }

    private void excluirConceito(SdConceitoCatalogo conceito) {
        try {
            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_conceito_catalogo_001 where catalogo = '%s' and codigo = '%s'", conceito.getCatalogo().getCodigo(), conceito.getCodigo()));
            SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EXCLUIR, String.valueOf(catalogoEmEdicao.getCodigo()), "Marcando CONCEITO " + conceito.getCodigo() + " (" + conceito.getTitulo() + ") para exclusão.");
            catalogoEmEdicao.getConceitos().remove(conceito);
            conceitosBean.remove(conceito);

            AtomicReference<Integer> ordem = new AtomicReference<>(1);
            catalogoEmEdicao.getConceitos().forEach(look -> look.setOrdem(String.valueOf(ordem.getAndSet(ordem.get() + 1))));
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void excluirProdutoConceito(SdConceitoCatalogo conceito, SdProdutosConceito produto, ListProperty<SdProdutosConceito> produtosConceito) {
        conceito.getProdutos().remove(produto);
        conceito.postLoad();
        SysLogger.addSysDelizLog("Catálogo Digital", TipoAcao.EXCLUIR, String.valueOf(catalogoEmEdicao.getCodigo()), "Marcando produto " + produto.getId().getCodigo().getCodigo() + " do CONCEITO " + conceito.getCodigo() + " (" + conceito.getTitulo() + ") para exclusão.");
        produtosConceito.remove(produto);
    }

    private void addPinImagemConceito(FormFieldImage image, List<SdProdutosConceito> produtosConceito) throws IOException {
        File imagemArte = new File(image.pathImage.get());
        BufferedImage bufferedImage = ImageIO.read(imagemArte);
        for (SdProdutosConceito produto : produtosConceito) {
            Graphics graphics = bufferedImage.getGraphics();
            graphics.drawImage(ImageIO.read(new File("C:\\SysDelizLocal\\local_files\\pin.png")), produto.getEixoX().intValue() - 15, produto.getEixoY().intValue() - 15, null);
        }
        //ImageIO.write(bufferedImage, "png", new File("C:\\Temp\\teste.png"));

        //ImageIO.write(bufferedImage, "png", imagemArte);
        Image imageBuffer = SwingFXUtils.toFXImage(bufferedImage, null);
        image.image(imageBuffer, image.pathImage.get());
    }

    private void cliqueImagemConceito(MouseEvent evento, SdConceitoCatalogo conceito, ListProperty<SdProdutosConceito> produtosConceito, String tipo) {
        if (evento.getClickCount() == 2) {
            if (tipo.equals("D")) {
                if (conceito.getImagemLocal() == null || conceito.getImagemLocal().isEmpty())
                    return;
            } else {
                if (conceito.getImagemMobileLocal() == null || conceito.getImagemMobileLocal().isEmpty())
                    return;
            }
            String descTipo = tipo.equals("D") ? "Desktop" : "Mobile";

            new Fragment().show(fragment -> {
                fragment.title("Imagem Conceito " + descTipo);
                fragment.size(600.0, 550.0);

                final FormFieldImage fieldImagemCarregada = FormFieldImage.create(img -> {
                    img.withoutTitle();
                    try {
                        img.image(tipo.equals("D") ? conceito.getImagemLocal() : conceito.getImagemMobileLocal());
                        addPinImagemConceito(img, produtosConceito.stream().filter(item -> item.getId().getTipo().equals(tipo)).collect(Collectors.toList()));
                        img.mouseClicked(evt -> {
                            if (evt.getClickCount() > 1 && emEdicao.get()) {
                                InputBox<Produto> produtoSelecionado = InputBox.build(Produto.class, boxInput -> {
                                    boxInput.message("Selecione o produto:");
                                    boxInput.showAndWait();
                                });
                                if (produtoSelecionado.value.get() == null)
                                    return;

                                SdProdutosConceitoPK pkProdutoConceito = new SdProdutosConceitoPK();
                                pkProdutoConceito.setConceito(conceito);
                                pkProdutoConceito.setCodigo(produtoSelecionado.value.get());
                                pkProdutoConceito.setTipo(tipo);
                                pkProdutoConceito.setOrdem(produtosConceito.stream().mapToInt(prod -> prod.getId().getOrdem()).max().orElse(0) + 1);
                                SdProdutosConceito produtoConceito = new SdProdutosConceito();
                                produtoConceito.setId(pkProdutoConceito);
                                produtoConceito.setEixoX(new BigDecimal(evt.getX()));
                                produtoConceito.setEixoY(new BigDecimal(evt.getY()));

                                conceito.getProdutos().add(produtoConceito);
                                produtosConceito.add(produtoConceito);
                                try {
                                    addPinImagemConceito(img, produtosConceito.stream().filter(item -> item.getId().getTipo().equals(tipo)).collect(Collectors.toList()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            }
                        });
                    } catch (FileNotFoundException e) {
                        MessageBox.create(message -> {
                            message.message("Não foi possível encontrar o arquivo: " + catalogoEmEdicao.getImagemLocal());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
                fragment.box.getChildren().add(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.add(FormLabel.create(lb -> {
                        lb.setText("Clique 2x na imagem para criar o pin e selecionar o produto.");
                        lb.setPadding(new Insets(-1));
                        lb.sizeText(10);
                    }));
                    content.add(fieldImagemCarregada.build());
                }));
            });
        }
    }

    private void adicionarImagemConceito(FormFieldImage fieldImage, SdConceitoCatalogo conceito, String tipo) {
        if (catalogoEmEdicao.getMarca() == null || catalogoEmEdicao.getColecao() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa definir a MARCA e a COLEÇÃO do catálogo para adicionar a imagem.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        File pathFile = FileChoice.show(box -> {
            box.basePath(baseLocalPath.concat("\\").concat(catalogoEmEdicao.getColecao().getCodigo()).concat("\\").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("\\").concat("Conceito"));
            box.addExtension(new FileChooser.ExtensionFilter("Arquivos de Imagens", "*.png", "*.jpg"));
        }).fileOpenPath();
        String fileName = pathFile.getName();

        if (tipo.equals("D")) {
            conceito.setImagem(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Conceito").concat("/").concat(fileName)));
            conceito.setImagemLocal(pathFile.getAbsolutePath());
        } else {
            conceito.setImagemMobile(baseFtpPath.concat("/").concat(catalogoEmEdicao.getColecao().getCodigo().concat("/").concat(catalogoEmEdicao.getMarca().getCodigo()).concat("/").concat("Conceito").concat("/").concat(fileName)));
            conceito.setImagemMobileLocal(pathFile.getAbsolutePath());
        }
        try {
            fieldImage.image(pathFile.getPath());
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }
    // </editor-fold>

    private class CatalogoEnvio {

        public List<SdCatalogo> catalogos = new ArrayList<>();

        public CatalogoEnvio(List<SdCatalogo> catalogos) {
            this.catalogos = catalogos;
        }
    }

}
