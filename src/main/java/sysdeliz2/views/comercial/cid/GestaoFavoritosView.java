package sysdeliz2.views.comercial.cid;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.apache.http.client.HttpResponseException;
import sysdeliz2.controllers.views.comercial.cid.GestaoFavoritosController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.comercial.SdCatalogo;
import sysdeliz2.models.sysdeliz.comercial.SdItensListaFavCli;
import sysdeliz2.models.sysdeliz.comercial.SdListaFavoritosCliente;
import sysdeliz2.models.sysdeliz.comercial.SdListaFavoritosClientePK;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.comercial.VSdItensListaFavCli;
import sysdeliz2.models.view.comercial.VSdListaFavoritosCliente;
import sysdeliz2.models.view.fv.VFvEstoque;
import sysdeliz2.utils.apis.ibtech.ServicoIBTech;
import sysdeliz2.utils.apis.ibtech.models.RequestEnviarFavoritos;
import sysdeliz2.utils.apis.nitroecom.ServicoNitroEcom;
import sysdeliz2.utils.apis.nitroecom.models.ResponseItensFavoritos;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class GestaoFavoritosView extends GestaoFavoritosController {

    // <editor-fold defaultstate="collapsed" desc="view">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabRanking = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox tabImportacao = (VBox) super.tabs.getTabs().get(2).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="listagem components">
    private final FormFieldMultipleFind<Entidade> fieldFilterCliente = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
        field.width(200.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<SdCatalogo> fieldFilterCatalogo = FormFieldMultipleFind.create(SdCatalogo.class, field -> {
        field.title("Catálogo");
        field.width(120.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<TabUf> fieldFilterEstado = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("UF");
        field.width(80.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarcas = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marcas");
        field.width(100.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(130.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Represen> fieldFilterRepresentante = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(160.0);
        field.toUpper();
    });
    private final FormTableView<VSdListaFavoritosCliente> tblListasClientes = FormTableView.create(VSdListaFavoritosCliente.class, table -> {
        table.title("Listas Clientes");
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("FV");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().isEnviado()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdListaFavoritosCliente, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Enviado*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ped.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().isComPedido()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdListaFavoritosCliente, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Com Ped.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Catálogo");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCatalogo().toString()));
                }).build() /*Catálogo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCatalogo().getMarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(170.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCatalogo().getColecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCliente().getCodcli()));
                    cln.format(param -> {
                        return new TableCell<VSdListaFavoritosCliente, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.lpad(item, 5, "0"));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(325);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCliente().getNome()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(190.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCliente().getCep().getCidade().getNomeCid()));
                }).build() /*Cidade*/,
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCliente().getCep().getCidade().getCodEst().getId().getSiglaEst()));
                    cln.alignment(Pos.CENTER);
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cod. Rep.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdListaFavoritosCliente, VSdListaFavoritosCliente>() {
                            @Override
                            protected void updateItem(VSdListaFavoritosCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getCliente().getMarcas().stream().filter(marca -> marca.getId().getMarca().getCodigo().equals(item.getCatalogo().getMarca().getCodigo()))
                                            .findFirst().get().getCodrep().getCodRep());
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Cod. Rep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(255);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdListaFavoritosCliente, VSdListaFavoritosCliente>, ObservableValue<VSdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdListaFavoritosCliente, VSdListaFavoritosCliente>() {
                            @Override
                            protected void updateItem(VSdListaFavoritosCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getCliente().getMarcas().stream().filter(marca -> marca.getId().getMarca().getCodigo().equals(item.getCatalogo().getMarca().getCodigo()))
                                            .findFirst().get().getCodrep().getNome());
                                }
                            }
                        };
                    });
                }).build() /*Representante*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                this.tblItensLista.items.set(FXCollections.observableList(((VSdListaFavoritosCliente) table.selectedItem()).getItens()));
            }
        });
    });
    private final FormTableView<VSdItensListaFavCli> tblItensLista = FormTableView.create(VSdItensListaFavCli.class, table -> {
        table.title("Referências Favoritadas");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdItensListaFavCli, VSdItensListaFavCli>, ObservableValue<VSdItensListaFavCli>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                    cln.order(Comparator.comparing(o -> ((Produto) o).getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdItensListaFavCli, VSdItensListaFavCli>, ObservableValue<VSdItensListaFavCli>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getLinha()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Linha*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdItensListaFavCli>() {
                @Override
                protected void updateItem(VSdItensListaFavCli item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().remove("table-row-danger");
                    if (item != null && !empty && !item.isFv())
                        getStyleClass().add("table-row-danger");

                }
            };
        });
        table.indices(
                table.indice("Nsão existente no FV", "danger", "")
        );
    });
    private final FormFieldSegmentedButton<String> fieldSelecionar = FormFieldSegmentedButton.create(field -> {
        field.withoutTitle();
        field.options(
                field.option("Selecionar", "X", FormFieldSegmentedButton.Style.DEFAULT),
                field.option("Todos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Não Enviados e Sem Pedido", "G", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não Enviados", "E", FormFieldSegmentedButton.Style.WARNING),
                field.option("Sem Pedido", "P", FormFieldSegmentedButton.Style.AMBER)
        );
        field.disable.bind(this.tblListasClientes.items.emptyProperty());
        field.select(0);
        field.addStyle("xs");
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tblListasClientes.items.forEach(VSdListaFavoritosCliente::unselect);
                switch ((String) newValue) {
                    case "T":
                        tblListasClientes.items.forEach(VSdListaFavoritosCliente::select);
                        break;
                    case "G":
                        tblListasClientes.items.stream().filter(lst -> !lst.isComPedido() && !lst.isEnviado()).forEach(VSdListaFavoritosCliente::select);
                        break;
                    case "E":
                        tblListasClientes.items.stream().filter(lst -> !lst.isEnviado()).forEach(VSdListaFavoritosCliente::select);
                        break;
                    case "P":
                        tblListasClientes.items.stream().filter(lst -> !lst.isComPedido()).forEach(VSdListaFavoritosCliente::select);
                        break;
                }
            }
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="importacao componentes">
    private final FormFieldMultipleFind<SdCatalogo> fieldImportacaoCatalogo = FormFieldMultipleFind.create(SdCatalogo.class, field -> {
        field.title("Catálogo");
        field.width(120.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldImportacaoMarcas = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marcas");
        field.width(100.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldImportacaoColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(130.0);
        field.toUpper();
    });
    private final FormTableView<SdCatalogo> tblCatalogos = FormTableView.create(SdCatalogo.class, table -> {
        table.title("Catálogos");
        table.selectColumn();
        table.editable.set(true);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Catálogo");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build() /*Catálogo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(170.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCatalogo, SdCatalogo>, ObservableValue<SdCatalogo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build() /*Coleção*/
        );
    });
    private final FormTableView<SdListaFavoritosCliente> tblListasClientesImportadas = FormTableView.create(SdListaFavoritosCliente.class, table -> {
        table.title("Listas Clientes");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Lista");
                    cln.width(230.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdListaFavoritosCliente, SdListaFavoritosCliente>() {
                            @Override
                            protected void updateItem(SdListaFavoritosCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText("#" + item.getId().getLista() + " - " + item.getNomeLista());
                                }
                            }
                        };
                    });
                }).build() /*Lista*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(320.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCliente()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCliente().getCep().getCidade().getNomeCid()));
                }).build() /*Cidade*/,
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCliente().getCep().getCidade().getCodEst().getId().getSiglaEst()));
                    cln.alignment(Pos.CENTER);
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdListaFavoritosCliente, SdListaFavoritosCliente>() {
                            @Override
                            protected void updateItem(SdListaFavoritosCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getId().getCliente().getMarcas().stream().filter(marca -> marca.getId().getMarca().getCodigo().equals(item.getId().getCatalogo().getMarca().getCodigo()))
                                            .findFirst().get().getCodrep().toString());
                                }
                            }
                        };
                    });
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Favoritos");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdListaFavoritosCliente, SdListaFavoritosCliente>, ObservableValue<SdListaFavoritosCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItens().size()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Favoritos*/
        );
    });
    // </editor-fold>

    public GestaoFavoritosView() {
        super("Gestão de Favoritos do CID", ImageUtils.getImage(ImageUtils.Icon.PRODUTO_FAVORITO), new String[]{"Listagem", "Ranking", "Importação"});
        initListagem();
        initRanking();
        initImportacao();
    }

    // ---------------------- LISTAGEM -----------------------------
    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(contentFilter -> {
                        contentFilter.vertical();
                        contentFilter.expanded();
                        contentFilter.add(boxFields -> boxFields.addHorizontal(fieldFilterCliente.build(), fieldFilterEstado.build(),
                                fieldFilterMarcas.build(), fieldFilterColecao.build()));
                        contentFilter.add(boxFields -> boxFields.addHorizontal(fieldFilterCatalogo.build(), fieldFilterRepresentante.build()));
                    }));
                    filter.find.setOnAction(evt -> {

                        fieldSelecionar.select(0);
                        procurarListasImportadas(
                                fieldFilterCliente.objectValues.get(),
                                fieldFilterCatalogo.objectValues.get(),
                                fieldFilterColecao.objectValues.get(),
                                fieldFilterEstado.objectValues.get(),
                                fieldFilterMarcas.objectValues.get(),
                                fieldFilterRepresentante.objectValues.get()
                        );
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterCliente.clear();
                        fieldFilterEstado.clear();
                        fieldFilterCatalogo.clear();
                        fieldFilterMarcas.clear();
                        fieldFilterColecao.clear();
                        fieldFilterRepresentante.clear();

                        tblItensLista.clear();
                        tblListasClientes.clear();
                        fieldSelecionar.select(0);
                    });
                }));
            });
            content.add(container -> {
                container.horizontal();
                container.expanded();
                container.add(boxListas -> {
                    boxListas.vertical();
                    boxListas.add(tblListasClientes.build());
                    boxListas.add(toolbarListas -> {
                        toolbarListas.horizontal();
                        toolbarListas.add(selectOpt -> {
                            selectOpt.horizontal();
                            selectOpt.expanded();
                            selectOpt.add(fieldSelecionar.build());
                        });
                        toolbarListas.add(FormButton.create(btnAtualizarItens -> {
                            btnAtualizarItens.title("Atualizar Favoritos");
                            btnAtualizarItens.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPORT_DATABASE, ImageUtils.IconSize._24));
                            btnAtualizarItens.addStyle("warning");
                            btnAtualizarItens.disable.bind(tblListasClientes.items.emptyProperty());
                            btnAtualizarItens.setAction(evt -> atualizarProdutosFavoritos(tblListasClientes.items.stream().filter(VSdListaFavoritosCliente::isSelected).collect(Collectors.toList())));
                        }));
                        toolbarListas.add(FormButton.create(btnAtualizarItens -> {
                            btnAtualizarItens.title("Enviar p/ Representante");
                            btnAtualizarItens.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR_NUVEM, ImageUtils.IconSize._24));
                            btnAtualizarItens.addStyle("info");
                            btnAtualizarItens.disable.bind(tblListasClientes.items.emptyProperty());
                            btnAtualizarItens.setAction(evt -> enviarFavoritos(tblListasClientes.items.stream().filter(VSdListaFavoritosCliente::isSelected).collect(Collectors.toList())));
                        }));
                    });
                });
                container.add(boxProdutos -> {
                    boxProdutos.vertical();
                    boxProdutos.expanded();
                    boxProdutos.add(tblItensLista.build());
                });
            });
        }));
    }

    private void enviarFavoritos(List<VSdListaFavoritosCliente> listasSelecionadas) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente enviar as listas de favoritos dos clientes selecionados para o representante?");
            message.showAndWait();
        }).value.get())) {
            if (listasSelecionadas.size() == 0) {
                MessageBox.create(message -> {
                    message.message("Você precisa primeiro selecionar os clientes que deseja enviar as listas para o representante.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            AtomicReference<String> listasEnviadas = new AtomicReference<>("");
            AtomicReference<String> listasNaoEnviadas = new AtomicReference<>("");
            new RunAsyncWithOverlay(this).exec(task -> {

                for (VSdListaFavoritosCliente listaSelecionada : listasSelecionadas) {
                    Represen representante = listaSelecionada.getCliente().getMarcas().stream()
                            .filter(marca -> marca.getId().getMarca().getCodigo().equals(listaSelecionada.getCatalogo().getMarca().getCodigo()))
                            .findFirst()
                            .get().getCodrep();

                    try {
                        RequestEnviarFavoritos.SalesOrder prePedido = new RequestEnviarFavoritos.SalesOrder();
                        prePedido.setCliente(new RequestEnviarFavoritos.Cliente(listaSelecionada.getCliente().getStringCodcli()));
                        prePedido.setCodRepres(representante.getCodRep());
                        prePedido.setTabelaPreco(new RequestEnviarFavoritos.TabelaPreco(listaSelecionada.getCatalogo().getMarca().getCodigo()));
                        prePedido.setQtdePecas(0);
                        prePedido.setItens(new ArrayList<>());

                        AtomicReference<Integer> itensEnviados = new AtomicReference<>(0);
                        for (VSdItensListaFavCli itemFavorito : listaSelecionada.getItens().stream().filter(VSdItensListaFavCli::isFv).collect(Collectors.toList())) {
                            if (itemFavorito.isEstoque()) {
                                List<VFvEstoque> estoqueProduto = (List<VFvEstoque>) new FluentDao().selectFrom(VFvEstoque.class)
                                        .where(eb -> eb.equal("referencia", itemFavorito.getProduto().getCodigo())
                                                .greaterThanOrEqualTo("quantidade", BigDecimal.ZERO))
                                        .orderBy("quantidade", OrderType.DESC)
                                        .resultList();
                                estoqueProduto.stream().findFirst().ifPresent(produtoFv -> {
                                    prePedido.getItens().add(new RequestEnviarFavoritos.Item(produtoFv.getReferencia(), 0, produtoFv.getCor(), produtoFv.getTam(), produtoFv.getTam(), 0));
                                    itensEnviados.getAndSet(itensEnviados.get() + 1);
                                });
                            }
                        }
                        RequestEnviarFavoritos enviarFavoritos = new RequestEnviarFavoritos(prePedido);

                        if (itensEnviados.get() > 0) {
                            String token = new ServicoIBTech().getToken();
                            new ServicoIBTech().enviarFavorito(token, enviarFavoritos);

                            SysLogger.addSysDelizLog("Gestao Favoritos", TipoAcao.ENVIAR,
                                    StringUtils.lpad(listaSelecionada.getCliente().getCodcli(), 5, "0"),
                                    "Enviando favoritos do cliente no catálogo " + listaSelecionada.getCatalogo().getNome() +
                                            " na coleção " + listaSelecionada.getCatalogo().getColecao() + " e marca " + listaSelecionada.getCatalogo().getMarca() +
                                            " com " + itensEnviados + " item(ns) favoritado(s) para o representante " + representante.toString());
                            new NativeDAO().runNativeQueryUpdate("update sd_lista_favoritos_cliente_001 set dt_envio = sysdate, enviado = 'S' where codcli = '%s' and catalogo = '%s'",
                                    StringUtils.lpad(listaSelecionada.getCliente().getCodcli(), 5, "0"), listaSelecionada.getCatalogo().getCodigo());
                            listasEnviadas.set(listasEnviadas.get().concat("Enviado favoritos do cliente " + listaSelecionada.getCliente().toString() +
                                    " para o rep. " + representante.toString() +
                                    " com " + itensEnviados + " produto(s)") +
                                    System.lineSeparator());
                            listaSelecionada.refresh();
                        } else {
                            listasNaoEnviadas.set(listasNaoEnviadas.get().concat("Lista do cliente " + listaSelecionada.getCliente().toString())
                                    .concat(" do catálogo " + listaSelecionada.getCatalogo().getNome())
                                    .concat(" não enviada por não ter itens disponíveis") + System.lineSeparator());
                        }

                    } catch (HttpResponseException e) {
                        e.printStackTrace();
                        listasNaoEnviadas.set(listasNaoEnviadas.get().concat("Ocorreu um erro no envio da lista do cliente: ".concat(listaSelecionada.getCliente().toString()).concat(". Erro: ").concat(e.getMessage())) + System.lineSeparator());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listasNaoEnviadas.set(listasNaoEnviadas.get().concat("Ocorreu um erro no envio da lista do cliente: ".concat(listaSelecionada.getCliente().toString()).concat(". Erro: ").concat(e.getMessage())) + System.lineSeparator());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        listasNaoEnviadas.set(listasNaoEnviadas.get().concat("Ocorreu um erro no envio da lista do cliente: ".concat(listaSelecionada.getCliente().toString()).concat(". Erro: ").concat(throwables.getMessage())) + System.lineSeparator());
                    }
                }

                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    new Fragment().show(fragment -> {
                        fragment.title("Envio de Favoritos");
                        fragment.size(900.0, 650.0);
                        fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                            field.withoutTitle();
                            field.expanded();
                            field.editable.set(false);
                            field.value.set(listasEnviadas.get());
                        }).build());
                        fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                            field.title("Listas/Clientes não enviados");
                            field.expanded();
                            field.editable.set(false);
                            field.value.set(listasNaoEnviadas.get());
                        }).build());
                    });
                    tblListasClientes.refresh();
                }
            });
        }
    }

    private void atualizarProdutosFavoritos(List<VSdListaFavoritosCliente> listasSelecionadas) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente atualizar as listas de favoritos dos clientes selecionados?");
            message.showAndWait();
        }).value.get())) {

            if (listasSelecionadas.size() == 0) {
                MessageBox.create(message -> {
                    message.message("Você precisa primeiro selecionar os clientes que deseja atualizar as listas.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            AtomicReference<String> listasAtualizadas = new AtomicReference<>("");
            AtomicReference<String> listasNaoAtualizadas = new AtomicReference<>("");
            new RunAsyncWithOverlay(this).exec(task -> {
                for (VSdListaFavoritosCliente listaSelecionada : listasSelecionadas) {
                    List<SdListaFavoritosCliente> listasClientes = (List<SdListaFavoritosCliente>) new FluentDao().selectFrom(SdListaFavoritosCliente.class)
                            .where(eb -> eb.equal("id.cliente.codcli", listaSelecionada.getCliente().getCodcli())
                                    .equal("id.catalogo.codigo", listaSelecionada.getCatalogo().getCodigo()))
                            .resultList();

                    final AtomicReference<Integer> countImportClientes = new AtomicReference<>(0);
                    for (SdListaFavoritosCliente listaCliente : listasClientes) {
                        new FluentDao().delete(listaCliente);
                    }
                    try {
                        List<ResponseItensFavoritos> itensFavoritos = new ServicoNitroEcom().getFavoritosCliente(listaSelecionada.getCatalogo().getCodigo(),
                                StringUtils.lpad(listaSelecionada.getCliente().getCodcli(), 5, "0"));
                        Map<String, Set<String>> listasCliente = itensFavoritos.stream()
                                .collect(Collectors.groupingBy(ResponseItensFavoritos::getCliente_codigo, Collectors.mapping(ResponseItensFavoritos::getLista_id, Collectors.toSet())));
                        listasCliente.forEach((codcli, listas) -> {
                            listas.forEach(lista -> {
                                try {
                                    List<ResponseItensFavoritos> listaFavoritos = itensFavoritos.stream()
                                            .filter(favoritos -> favoritos.getLista_id().equals(lista)
                                                    && favoritos.getCliente_codigo().equals(codcli))
                                            .collect(Collectors.toList());

                                    ResponseItensFavoritos cabecalhoLista = listaFavoritos.get(0);
                                    Entidade cliente = new FluentDao().selectFrom(Entidade.class).where(eb -> eb.equal("codcli", cabecalhoLista.getCliente_codigo())).singleResult();
                                    if (cliente == null)
                                        throw new BreakException();
                                    SdCatalogo catalogo = new FluentDao().selectFrom(SdCatalogo.class).where(eb -> eb.equal("codigo", cabecalhoLista.getCatalogo_codigo())).singleResult();
                                    if (catalogo == null)
                                        throw new BreakException();

                                    SdListaFavoritosClientePK idListaCliente = new SdListaFavoritosClientePK();
                                    idListaCliente.setCliente(cliente);
                                    idListaCliente.setCatalogo(catalogo);
                                    idListaCliente.setLista(Integer.parseInt(cabecalhoLista.getLista_id()));
                                    SdListaFavoritosCliente listaAtualizadaCliente = new SdListaFavoritosCliente();
                                    listaAtualizadaCliente.setId(idListaCliente);
                                    listaAtualizadaCliente.setNomeLista(cabecalhoLista.getLista_nome() != null ? cabecalhoLista.getLista_nome() : cabecalhoLista.getCatalogo_nome());

                                    SdListaFavoritosCliente finalListaCliente = listaAtualizadaCliente;
                                    listaFavoritos.stream().map(item -> item.getProduto_codigo()).distinct().forEach(codigoProduto -> {
                                        SdItensListaFavCli itemFavorito = new SdItensListaFavCli();
                                        itemFavorito.setLista(finalListaCliente);
                                        itemFavorito.setProduto(new FluentDao().selectFrom(Produto.class).where(eb -> eb.equal("codigo", codigoProduto)).singleResult());
                                        finalListaCliente.getItens().add(itemFavorito);
                                    });

                                    System.out.println(finalListaCliente.getId().getCliente().getCodcli() + "/" + finalListaCliente.getId().getCatalogo().getCodigo() + "/" + finalListaCliente.getId().getLista());
                                    new FluentDao().persist(finalListaCliente);
                                    SysLogger.addSysDelizLog("Gestao Favoritos", TipoAcao.EDITAR,
                                            StringUtils.lpad(finalListaCliente.getId().getCliente().getCodcli(), 5, "0"),
                                            "Atualizado favoritos da lista " + finalListaCliente.getId().getLista() + " do cliente no catálogo " + finalListaCliente.getId().getCatalogo().getNome() +
                                                    " na coleção " + finalListaCliente.getId().getCatalogo().getColecao() + " e marca " + finalListaCliente.getId().getCatalogo().getMarca() +
                                                    " com " + finalListaCliente.getItens().size() + " item(ns) favoritado(s).");
                                    countImportClientes.set(countImportClientes.get() + 1);
                                } catch (BreakException e) {
                                } catch (SQLException throwables) {
                                    throwables.printStackTrace();
                                    listasNaoAtualizadas.set(listasNaoAtualizadas.get().concat("Ocorreu um erro na atualização da lista: ".concat(lista).concat(". Erro: ").concat(throwables.getMessage())) + System.lineSeparator());
                                }
                            });
                        });
                    } catch (HttpResponseException e) {
                        e.printStackTrace();
                        listasNaoAtualizadas.set(listasNaoAtualizadas.get().concat("Ocorreu um erro na atualização do cliente: ".concat(listaSelecionada.getCliente().toString()).concat(". Erro: ").concat(e.getMessage())) + System.lineSeparator());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listasNaoAtualizadas.set(listasNaoAtualizadas.get().concat("Ocorreu um erro na atualização do cliente: ".concat(listaSelecionada.getCliente().toString()).concat(". Erro: ").concat(e.getMessage())) + System.lineSeparator());
                    }

                    listasAtualizadas.set(listasAtualizadas.get().concat("Atualizado " + countImportClientes.get() + " lista(s) do cliente " + listaSelecionada.getCliente().toString()) + System.lineSeparator());
                    listaSelecionada.refresh();
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    new Fragment().show(fragment -> {
                        fragment.title("Atualização de Favoritos");
                        fragment.size(900.0, 650.0);
                        fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                            field.withoutTitle();
                            field.expanded();
                            field.editable.set(false);
                            field.value.set(listasAtualizadas.get());
                        }).build());
                        fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                            field.title("Listas/Clientes não atualizados");
                            field.expanded();
                            field.editable.set(false);
                            field.value.set(listasNaoAtualizadas.get());
                        }).build());
                    });
                    tblListasClientes.refresh();
                    if (tblListasClientes.selectedItem() != null)
                        tblItensLista.items.set(FXCollections.observableList(tblListasClientes.selectedItem().getItens()));
                }
            });
        }
    }

    private void procurarListasImportadas(ObservableList<Entidade> cliente, ObservableList<SdCatalogo> catalogo,
                                          ObservableList<Colecao> colecao, ObservableList<TabUf> uf, ObservableList<Marca> marca,
                                          ObservableList<Represen> representante) {

        Object[] clientes = cliente.stream().map(Entidade::getCodcli).toArray();
        Object[] catalogos = catalogo.stream().map(SdCatalogo::getCodigo).toArray();
        Object[] colecoes = colecao.stream().map(Colecao::getCodigo).toArray();
        Object[] marcas = marca.stream().map(Marca::getCodigo).toArray();
        Object[] ufs = uf.stream().map(it -> it.getId().getSiglaEst()).toArray();
        Object[] representantes = representante.stream().map(Represen::getCodRep).toArray();

        new RunAsyncWithOverlay(this).exec(task -> {
            getListasClientes(clientes, catalogos, colecoes, marcas, ufs, representantes);
            listasClientes.forEach(VSdListaFavoritosCliente::unselect);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblListasClientes.items.set(FXCollections.observableList(super.listasClientes));
            }
        });

    }

    // --------------------- RANKING -------------------------------
    private void initRanking() {

    }

    // ---------------------- IMPORTAÇÃO ----------------------------
    private void initImportacao() {
        tabImportacao.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(boxFilter -> boxFilter.addHorizontal(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(filterContente -> {
                    filterContente.vertical();
                    filterContente.expanded();
                    filterContente.add(boxFields -> boxFields.addHorizontal(fieldImportacaoCatalogo.build(), fieldImportacaoMarcas.build(), fieldImportacaoColecao.build()));
                }));
                filter.find.setOnAction(evt -> {
                    procurarCatalogos(fieldImportacaoMarcas.objectValues.get(), fieldImportacaoCatalogo.objectValues.get(), fieldImportacaoColecao.objectValues.get());
                });
                filter.clean.setOnAction(evt -> {
                    fieldImportacaoCatalogo.clear();
                    fieldImportacaoMarcas.clear();
                    fieldImportacaoColecao.clear();
                    tblCatalogos.clear();
                    tblListasClientesImportadas.clear();
                });
            })));
            content.add(container -> {
                container.horizontal();
                container.expanded();
                container.add(tblCatalogos.build());
                container.add(right -> {
                    right.vertical();
                    right.add(FormButton.create(btnImportar -> {
                        btnImportar.title("Importar Listas");
                        btnImportar.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPORT_DATABASE, ImageUtils.IconSize._32));
                        btnImportar.addStyle("lg").addStyle("primary");
                        btnImportar.disable.bind(tblCatalogos.items.emptyProperty());
                        btnImportar.setAction(evt -> importarListas(super.catalogos.stream().filter(SdCatalogo::isSelected).collect(Collectors.toList())));
                    }));
                    right.add(tblListasClientesImportadas.build());
                });
            });
        }));
    }

    private void importarListas(List<SdCatalogo> catalogosSelecionados) {
        if (catalogosSelecionados.size() == 0) {
            MessageBox.create(message -> {
                message.message("Você precisa selecionar os catálogos para importação das listas.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.showAndWait();
            });
            return;
        }

        final AtomicReference<Integer> countImportClientes = new AtomicReference<>(0);
        final AtomicReference<List<SdListaFavoritosCliente>> listasImportadas = new AtomicReference<>(new ArrayList<>());
        final AtomicReference<Exception> retornoExcpt = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            for (SdCatalogo catalogoSelecionado : catalogosSelecionados) {
                try {
                    List<ResponseItensFavoritos> itensFavoritos = new ServicoNitroEcom().getFavoritosCatalogo(catalogoSelecionado.getCodigo());
                    Map<String, Set<String>> listasCliente = itensFavoritos.stream()
                            .collect(Collectors.groupingBy(ResponseItensFavoritos::getCliente_codigo, Collectors.mapping(ResponseItensFavoritos::getLista_id, Collectors.toSet())));
                    listasCliente.forEach((codcli, listas) -> {
                        listas.forEach(lista -> {
                            try {
                                SdListaFavoritosCliente listaCliente = new FluentDao().selectFrom(SdListaFavoritosCliente.class)
                                        .where(eb -> eb
                                                .equal("id.cliente.codcli", codcli)
                                                .equal("id.catalogo.codigo", catalogoSelecionado.getCodigo())
                                                .equal("id.lista", lista))
                                        .singleResult();

                                if (listaCliente == null) {
                                    List<ResponseItensFavoritos> listaFavoritos = itensFavoritos.stream()
                                            .filter(favoritos -> favoritos.getLista_id().equals(lista)
                                                    && favoritos.getCliente_codigo().equals(codcli))
                                            .collect(Collectors.toList());

                                    ResponseItensFavoritos cabecalhoLista = listaFavoritos.get(0);
                                    Entidade cliente = new FluentDao().selectFrom(Entidade.class).where(eb -> eb.equal("codcli", cabecalhoLista.getCliente_codigo())).singleResult();
                                    if (cliente == null)
                                        throw new BreakException();
                                    SdCatalogo catalogo = new FluentDao().selectFrom(SdCatalogo.class).where(eb -> eb.equal("codigo", cabecalhoLista.getCatalogo_codigo())).singleResult();
                                    if (catalogo == null)
                                        throw new BreakException();

                                    SdListaFavoritosClientePK idListaCliente = new SdListaFavoritosClientePK();
                                    idListaCliente.setCliente(cliente);
                                    idListaCliente.setCatalogo(catalogo);
                                    idListaCliente.setLista(Integer.parseInt(cabecalhoLista.getLista_id()));
                                    listaCliente = new SdListaFavoritosCliente();
                                    listaCliente.setId(idListaCliente);
                                    listaCliente.setNomeLista(cabecalhoLista.getLista_nome() != null ? cabecalhoLista.getLista_nome() : cabecalhoLista.getCatalogo_nome());

                                    SdListaFavoritosCliente finalListaCliente = listaCliente;
                                    listaFavoritos.stream().map(item -> item.getProduto_codigo()).distinct().forEach(codigoProduto -> {
                                        SdItensListaFavCli itemFavorito = new SdItensListaFavCli();
                                        itemFavorito.setLista(finalListaCliente);
                                        itemFavorito.setProduto(new FluentDao().selectFrom(Produto.class).where(eb -> eb.equal("codigo", codigoProduto)).singleResult());
                                        finalListaCliente.getItens().add(itemFavorito);
                                    });

                                    System.out.println(finalListaCliente.getId().getCliente().getCodcli() + "/" + finalListaCliente.getId().getCatalogo().getCodigo() + "/" + finalListaCliente.getId().getLista());
                                    new FluentDao().persist(finalListaCliente);
                                    SysLogger.addSysDelizLog("Gestao Favoritos", TipoAcao.CADASTRAR,
                                            StringUtils.lpad(finalListaCliente.getId().getCliente().getCodcli(), 5, "0"),
                                            "Importado favoritos do cliente no catálogo " + finalListaCliente.getId().getCatalogo().getNome() +
                                                    " na coleção " + finalListaCliente.getId().getCatalogo().getColecao() + " e marca " + finalListaCliente.getId().getCatalogo().getMarca() +
                                                    " com " + finalListaCliente.getItens().size() + " item(ns) favoritado(s).");
                                    countImportClientes.set(countImportClientes.get() + 1);
                                    listasImportadas.get().add(finalListaCliente);
                                }
                            } catch (BreakException e) {
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        });
                    });
                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    retornoExcpt.set(e);
                    return ReturnAsync.NOT_FOUND.value;
                } catch (IOException e) {
                    e.printStackTrace();
                    retornoExcpt.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
            }

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message(retornoExcpt.get().getMessage());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(message -> {
                    message.exception(retornoExcpt.get());
                    message.showAndWait();
                });
            }

            System.out.println("Total de " + countImportClientes + " listas importadas...");
            MessageBox.create(message -> {
                message.message("Total de " + countImportClientes + " listas importadas...");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            tblListasClientesImportadas.items.set(FXCollections.observableList(listasImportadas.get()));

        });

    }

    private void procurarCatalogos(ObservableList<Marca> marca, ObservableList<SdCatalogo> catalogo, ObservableList<Colecao> colecao) {
        Object[] marcas = marca.stream().map(Marca::getCodigo).toArray();
        Object[] catalogos = catalogo.stream().map(SdCatalogo::getCodigo).toArray();
        Object[] colecoes = colecao.stream().map(Colecao::getCodigo).toArray();

        tblListasClientesImportadas.clear();
        new RunAsyncWithOverlay(this).exec(task -> {
            getCatalogos(marcas, colecoes, catalogos);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblCatalogos.items.set(FXCollections.observableList(super.catalogos));
            }
        });
    }
}
